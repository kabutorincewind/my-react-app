

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HIV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentHerpes.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a7rRKRwAsiO1SImg1+Wbc33KmmCT1p6LXYQChmvQQANUoqoM/iFMfJjmh9BCsBj3u9TenSO22gS+bluyyakyjlVSwK5W3bhRNqmmQ6mIdmogsR5gg6K+f7yy0Oh+bDSXVLAFDtVoorBMZY+LCE43XCSbxwJo+L77LqGuRp28s7hb03EgDMQ8rz84noKxa0QATmNIdeBvHuQrNviGBpfrdokLF0MMECeUH9AM7n53x4ILHaa9anYbfIAYDPCnVLL1aeu28bzrrkkVFoeK3cn1CBUwt3mY2l+PlV1YGLFerdGg3CTiJTsYPG2hNY/Xgoga8xFs48BDDxOxnguNV3CtacW/oP4n6hUt6Ge2Ac5Z+QtxRM0UL6u3QJWvdZtr9xOJujmlLkMuNofbF05wvsbaVFGedu7s++Tf8sNeHEEMabFMlmBprXUGGtt21hElca4DOqOzWzy0PAGmITpiUFxmh8bt8Ao8FDfhx+VDQUxwS2/Fu2leRMC3jpMDsmenAkcKDCOk2L/G6wrKW2T9eq+YVNhieMnWG8Ccx95HxyfoOyIA8d9yCxKH+2gXpuk71ajPuInBf4QLoja8Dr3XNozuSTydMghljRUQoPRpJRqsG1RE7PCohoy7AcRWln5OIi/EmiWzwASCZmwrvWa5LStIZgy6IbyWmKHOkWLNSTl5L5Mq1Ff0ibdLFtGrxBxm+vMxayEoD8KQSgtE7iu48YvAJoLkY/WuY5A0kfyo9Ufg1pFstEHmkhk3aUxDSm/lPJRMgdfp5RsQvGlDad819RnLGmqX/uILjAFsu9v8qJslKOVzZ2n/maKsZFhiBQSHLn1VV7S/shIsJJenTaB3tCyWCrIcNoC43vxLiVVZKlnouAV4vA+1qYmff2AHbPHKgKlCJRecivWBL9IydHflqRZT8H4o7DRDh0G5mx4CmlRyMdgDfgtzoLSH21/FXF3mjb21glJ1MqKQLgxCCor2eS3kIEG3m4fTo+7wRZ2HfU4gaTb7VIOp7qywP1mkxJDUmUi6VWYDHR9sKGGNAFP9oIdAxMdfPApnnpIw96uS4A05VTVl5t/5bJltyJKsnCEcHsoZTTR8jRGE8hBdQIcnxRXntvz1zEHMXYgIq43LzCZGukc20UDBWRw6Q/sdyW0YLrS09pjwFLkcyYyIpGlWjfvBpudkCGZSc6ufxfLGzV8r1HH3JIYCNK0JMU9iZl4mWWJk8ZRSfmgjsWxmP8/YDAcrTr4+O+lBwagpS+7NtNWQ+ks64vEbgxziDN6jQjOfnDpi/qkFY0PdaJRlTD+yZdEZqs2X3ZfdEReD88jLM69exfLfD5stjT0cLG2+MySnYB9GMVDgd09amwvxOjhfHksqdP9TfJR86cEQg2kIRYqPxKzwqUjoDiEVM7XlC1ylbGjJFx0BFOZPQY5ioIDu1IRvgciXbafQu0akGFThwxpliVTJDp4gBLgDH3rsylI/fBXy8wWSkyrQJC4ncavu+qLEyXTyV44uW03KjoD2xgk72nw9eMZnXI/RGOi0IApd83OP8NsIeiSaNR4i14RdAdqejLFcksaBemVOXAKWXN8+kWtqocbqUn+irc/AbH6+L5spPP6wucCoyabkpU0CDuoOFjLjQtBiLlrs53Y9EfHbZLqJGRReUZWxYsACJSzdFGsLwg5ZEXdHBHvhz5FMLxvK+NLIwqQ0hNimqDxCDJFGX+IuMUTiPSYYa/TDuaO1ct9v4neZfczbLb8V53uKSeC6lHQ929Np+o3NBV+PTvnr3Nd2/wOp6UwIH8X2og/iREF0TxfFT3gxlSb8rAUxgit8LumYz2/RMVuhzd78BrqaqGRpTfCYxAD8241pGGJN+OQ+BI4fthqDQ4cCwvXlcQvPJ8HT+6vbRIQG0czkPeOjrAF6SvJIOKR9L97Jq+5BVRHFEwWdB054apRwy2J+iDP1bvViDN5++eVGWhSyayY1Dpe2t2mHe8MNAvXIheF/1x0tIMer3OUeKcjroU7VCqtiGZMJSG4iCpq4GNOU+ZmAmfLK/vEYSe7FbAsRQ2Q83aWHpcPwuDTBXZ5qVxezMnT+evpSEQMmYp62gSFhXW79HX/Bpf8Mj/2a/FfReHM+YO7JMzoCpeVvaG8qtaT/AAYD1AHSsym7ISZegLEeCpWgmZRWK/RBdCsk85CnFidO08wARWW5VdPgU9f4zNkwQLqYiQeeJYm7Stteb1/o5cF+j2nb8QzRpM+wdcca49+acNVrhL6LpXQ4j6xigSvBvNGDfSE0Zxt/BmT/fXhe6AyryuCCa1gUHhA1ruVTIDNB3UuMxZW9hVZMklMAbQRc59dsdTzCOaZyJIYnAuPUrwpE+0sDlKwJX79Fr0tGhr7fi2Vsx2zyM3ovMdMA3g2SOm9WaROHGxm5PqPDyvwpIbSZ+iBf+szfEJ9tFsf0TQqxPlDq9fxhOm5knZasES8hEgRIGX1UGFwz9fpJI0Yvv5xIlyV8O96alAJWpvQa/prYG0YXF9ouejwYp/23pU7snVWXUdXg8JDqA52oSkLF7UYfqFIzvhsOqgpP2dGQyXGZGXt+/mVtF6fSI7npZu6uytC9gZXFuS1wlzmDqQf492Ju/1i1v+klC3Qkn4Od1ocMICgSJObKa0sil07zDg25CltBnm3WNdu6plsWYFPFB58zASv9LdDbzbz8ftO+RvtOhm48czrNHR8DlkJxByAnRjVsTqvO+4gXbGhGdQJZ2/U6X+SXpueVPszMK0RXRR7a7hfUQ4dk+ta8obEKB4rQZDdYkBA740ecKBScJEpkWvDGZhezQuJOSB5bRP5yq/VX+SD1diVryTT6Y2BUu5Jty3pJNb5XB0LTagbXpJ3Eex92EfASRJ0OW0BL2Sm9P3Bz2F3YPxBJNj25fF2PUrnfieAIX1ADz5gbRThDzKL7c5H+to9TM58ujxt0l07SRncvckYrrTawIIHxJSsLM55JiqddmRylCz4aWf/c8SoRN5PVP9fpeyYQATq8Yl4j4OYmaOPGrfHt3XBL9CDBNyLkqwwPFmIOC4fd+2/vBR5FeAclbDgPueU1n0MW8afYsWtyHYGtruAyqd6cYH0dWVWnfv7MPpADQeYg5n8oU2ss6mn0X0NSZy7AUMB+r/ZaqrrmFclzHw3UOare6lAeuQZHrVxkexe465RD32l5hNmU/ix/K2JT+1LOUx6MEly3OQ3WFZfycm4tXS+eHVsBLBgw9r89zik7X07P00nigQv82NyGJXyeiPJaxNuY8Y6IZhGXyNJ+T1aSe9PbaGu8hTeUWVMa5zoPE4BCzMcukeRL0jWq+657knYL48bRhFdpNL5rbujWlGuurEYdED6hGpx59gdBf9f7uvPKRX/GZ6cjv8qyoZ7x5UfTIQmcPDWxXxLhA4NVSQur/Dn6L86O+cS7fibej/eGRrQiZTf8yM/h+fqOAPpRK1K25x/O+FrVsOWJQcf1cIzODoQE8T62J7ne0hF6SKy4osCWCRx5RW/B9WZvMJ/CnbnMecRvOjGCu4xRQM/D8WsoXsdia7f1HlnndVHJl3ZWP9yXeCBOuMp4GTWLy0oXZR/HXVOz7MkecTNHfj5EEgkPUR5kdD3zxFq3se3cWjbzccfFa+QsFBFeabZQj9f3xt300+TBkDas+KD7CxPI27Ojr2MPNmCRdhoVHDQ/fcXDJDcAN+sju+DoIiH1mnTUTwBzGSQujQKaV6n+U2rYPlwkyHvXDM9x1g3oGnTi21NztHDYJyhqo2v3v/ynIm+t67ZnVl334KGsABLZlAWFrq1gqcZeOQnDXxxM2Yl65Rn8shu6wtOQDhxEAiWhYa59leuxGmhnq82WSLXXRYxI8SiIyfKd2zu5qC3rOt3L7b8HrbtT67R92bxNIw2/y3jYpWo1isa089Pgb6LywnABWiz1ateffSqIi7DTB1Ud1VU8o+atznq7NFuBq9s6jVsOHbKqktmgbPDU0drrQvNUuGAcu3QMsyE7E9Gk4c+My+Z1mbJqdM3S2lwZnmb1ca2jv10XelFu8bwdt0d0JMoNZ9XT70xdtTCVRKt+8o/iO3bkBf8KBgG8k1mL5+rofB4FlP+aRMRsBqckC70kt3KOd9jIO5YpSdr7tOrMEc7RglXs14xf42p1+8JjFEqVIOfiixca25spj73QEoCjcJbReBwaJ79PlIMvtJ51k6l6s80BmPg7rlAZGPLWkjxTPWd9UkirSlso8WYFdm/sU0kxno4/nRgH7PUgdBGFmVxyT8A+NX/WzuSTFZlERyWtRKSHlk8ykoWbKY23WNLv13bkG4oX1VuNwi9d1JMV4pY+/6sATWFaAxHPP25Snsic6UkB1+ZKMIpEo6pJnh31d0kRgoFPvjX/Jthfwi/n4pf4FFUjc4VL5hiTdEtdzPOW7hqh8GmHN2KJ+WK2l3f5j0+TXcelVkiCq1y6Qv2FIdISc6Kbfw6sxEqxyegjXmWx3JASe6zqLf+X7xFtAD/nH8lo/FTuZRU+poWEtDPPppWzn6/8842z91GYTp6676150ZV8HNk6J3yDJS1JZatPAwu3W0tWtGmQuwrcRPx2c3RCEjklC0lXzy+QOXElv32tEOLv24IcoViqh7Qf+zGWwWk1hBAZUs7WNZm89X67K/1pgd7weGB1bvK7OPuvMwZqGSEbXPeIH1VljyLOl4LESP4k4I9vPA8GLj5D8ChkKcP5M6+A3Cc8sY6WR/emWydMcKryA2xWRvBiFTd2xXPXanM8On4yV1PMaDsaOKFYSnEkRO4WetlxheuwY47/YPKOC4vb4DcQdDjLp+gn28i2cZTeYMs/iq0lDL68xVV8yoXjWbn008oTenq6nWp4cQ2QKT1Bllibk7M59e+hIPnIb14XdYhsu7GlmqRdGYfIRUal9wSEfJSQh5td8K3+EtuI/vNr5Uhth8MvrPcO4CnBnt2rx03VYRzzxaYDejDJBiUZez7zq90DPj4D/lwVnATtn6cfTOjU5knsjcB1Ni2aeGTCmLT73NL586fLBYfQgp36TC/rxTu+FNPU5RQ77T0PIlQbIcMSfEyHRm9Sy2ludKKPAmkVLTKNpJKoCXbCWUQYZDCY9wkomz1TF9q44JbUXSz8kR488UKuu96YnWe0qohMfz7/EQo+wNESGa660xfRvjsl2Zrg82odAVTl/D9AXRYrBgGqS1F+JJbLQjJxXhOap51/rRvxdVIjVhyfsWWs3B1JBeVtZBCIITD40a1AYzw5bi4Y5aHiGQysmc6YmmiqWCTk90fv1e6Jd8u46oAh9W3IAE7lFq5/7diLC3eB0u2DMCwT0GLadiPi2JQ5rUoOV57u0V7FBq6Gcm2/5z++TKnbxN9fPpGTrxrWj2jBy+hvtG0NWcgGa/iCHTweIO43T4K6kDmKtnZJIKlrj8sZVOXUFCQvKhp910WcWJVdpYJPnh6StJDkjq/omfxSMfvbda1QzxduYNe3HCV6wtAcTHBU+fqYc3HC2beF3stqeg6y5I1GtjVcX1MSR2MffD0WMHoIFytro0xjj2C+EIaRWae+1kL9rRRU7Ib6uaW0/zYb2sLu1ufpyw7NU7J74KHqH845bnHOru5ES3yvHsPorK3Daj3KlGJ51TwKDQXupVkkfRL7b3rDrMMUvgbGUgBrjg7OhoXrEetgiJJZJxXrn7FAFrNa71Aw4rpTOgE3hwkoHAefu+ou0sBkilGMFnssMNMG7HkKknA/B4qClNz1b7d0vj2DHhJEj2FcgqEC2jEdE4zBN2INW5jVLEqE36dL9sqNL9OjG7eCqHrxq0XNeQSApGBTeJYYzcJhI5u1qNzR30+UhPmzka/qH4i3QrJ2udUT4lptL29D1UhHlqZvtWcFXtxtnhf25y0nkRnbCYV3WhXW4RkDL0Tf6b0uvw7Le0rS0Dc/CDAtcrrRSsibTSkrupJRy46wBLg8zk3iQJsto8UmRz3vb8C8Wm5+WYPw1XucNp/8XVQUFXgHIYbW2m+oSzbcT2MC7S4qEgfsrIt/hN+qVZOzCF7CQdkgl2dtpPlNl8j3MD7PAtCY3vbKB0RVhGSe4SrC3GLaGM+puHAy7Y5Q6TfXml0T41Mwgw/NY95v4y8TAJsXBD+hiMpvv5ll137AK1PuLnIiknOuvkt0Xx75fsMbWTnhkKddkQZnS0H5gyNyrFg0jyLg3+Ml/8/1HWWJIOOVTFJsJ8twMxOEjLlIRUJyBAS0eKfBzqNdSFi0apC9ymHhJYEpaJ+LZbEl6P3a2QMfeqxsbn2p8R6IAfKvZAMhqmzySZz8/GLMtzBJ4F0jm3jm+a/ydFnGaxdlZSdSv+OEtsF8scnFEVrCFYQPjd+ISGBQQOmQdfi9h/an8+3Razb7HvqRX04nxFjIaswS/Vl5RmBDN70ZqMH6lFcOOo98uVZC63/L+qO4cjielgUJ78k5kPqr3l2defhIWpM/XhhQnnBIEBQysRTSwXxxNiUkM0M5AJuYWGUC6nueni4B+Bbmx5907cXrJXoLNsqfdvx9eTmi+KsOO8ZOnJ8z16p/3nh7vF7Chom3aTsfdSv51G0oSXt98uDUK3ErbfpGVzahUnmNn5lAiKVeH0eikecTzft60O4Cx8Gx9HnTCYJQUNQ7VLW/CKw5ggHowqKnErhh8SzDVyFq6/k+kDgYBS8z+FDvUqUFMd1Tf16g+J4eWBe0s7CqAce60tO2SOeB2pq7PWxqB8hfwP9GuBlo5ZIJU+zwdmpV1bu6NQUB+pUaXdsPvqUrhR1Z3E02CXHSnheIWTK4XTY68QqnRwicdq3TVSe/LXAl/LnigytlSr1h/WilysNvN/2AQxx9mchvDVu1f+nThCQxdNvz1qSMDysbZtVexQVUBcnsP7Yt741+frllGx+kSv/ajtK8b8PWWxEr91A7BOweLmRWBMBtOhys7cQcfPbd9wgh9GYCKoSZM7ifQtI95ZNtP/e1TshbRf+3cQm6bcu5DuAXZx3ePSQxGFkgcQ/vNWFCODeStyEC5nuMKZMVnUboE+78Ca/zpL+9UH3HvTVkW12CrUPDsT9wT5U3np6F+ZsklE0+FlKKDCH6ll/0cEA/hrFr9BzNH+ciBtTnOLT8DedE471YAwQr48WZUrnoe2wORFMmmQpkt0DhboxnRQZBiDsgSGYT/kb56cwH3fOdvmEBpKvAQTbGgCU1hnZ3HqBxUHPeZtwxTbbFjTyq3uqlh4LPNPwm21oVd8o98H5pmgGa0hPt6b7qgOaBQf2qRPio33igXWZz4W5bdodvWpLCG9OewdbKIBR0FKTPfLN5Kjzh2ClcIXo6tiu7vqTj4uqDHPyrBz7QjiB1XHJTH10MLd9lRJZ6e0di+seNAFPOE9xHKmxCSV0Sxu9aexk8oITKLaWiFx/uBj8MTN1OWKnZK3PLBSjK+tT85+WQ6lRr1FPxBbEybxUjcyvXWhnJcaUkgfqpnepyZ0MUoP+fP5NP3179Kw0Tbv+aasxzrgtbls+HlQ00GgqIQCrW956Kj3T4mzSu0XEnp7bZ7MkGf7TJkF2W8Al/eNainWzoIbRt0h9T61FG1I7BSLhz0pL2moEGCH+FGmAxAoaAWHqNIXTS0i+0K6mi9jIcFXsgFpk7fJXX9UwCCGFWNmCZI8JQCWqzac12msJMnSTJImsa11bzEVD586pmYVZV9+d/XNmBGBe/RKEik/sWAYBiFDEhF/KDV0XiVp6XxYfnrlhREv6hGPBwIgGETFygrR4Q6bJhGnDVdiikDPltYe8wvY3lZAcTzRkhqeULdDye3wm2qTiBGzKzZen4NCaL345duv8g7KCOdYVt//s30yIYe64A443W6Alf17N6BXgDntwN2Rg01jZ/v8mV84AJhYVkZ83BQ5USp476V+rq9xet75QitsI82eMfhTZXzSf0M4JF" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>Herpes Drugs in Development (Archived)</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugOrganism"><font color="White" size="3"><b>Organism</b></font></td><td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugOrgName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugOrgAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugOrgNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>Epstein-Barr virus</td><td>Combination</td><td>&nbsp;</td><td>Phenylbutyrate and Valganciclovir </td><td>&nbsp;</td><td>Phase 2</td><td>Unknown</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Epstein-Barr virus</td><td>Combination</td><td>HemaQuest Pharmaceuticals Inc.</td><td>Ganciclovir and Arginine Butyrate</td><td>&nbsp;</td><td>Phase 2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>Epstein-Barr virus</td><td>Virus Replication Inhibitor</td><td>GSK</td><td>Valtrex&trade; </td><td>Valacyclovir</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl04_Link" href="CompoundDetails.aspx?AIDSNO=070982">070982</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Herpes simplex virus</td><td>Blocks Virus morphogenesis</td><td>DeNova Research</td><td>Botox&trade;</td><td>Botulinum Toxin A; Onabotulinumtoxin A</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Herpes simplex virus</td><td>DNA Helicase Inhibitor</td><td>AiCuris </td><td>Pritelivir</td><td>AIC-316</td><td>Phase 2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Herpes simplex virus</td><td>Entry Inhibitor</td><td>Shaman Pharmaceuticals</td><td>Virend&trade;</td><td>Crofelemer; SP-303T</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl07_Link" href="CompoundDetails.aspx?AIDSNO=071298">071298</a>                             
                            </td>
	</tr><tr>
		<td>Herpes simplex virus</td><td>Helicase-primase Inhibitor</td><td>Astellas Pharma Inc</td><td>ASP2151</td><td>&nbsp;</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl08_Link" href="CompoundDetails.aspx?AIDSNO=501812">501812</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Herpes simplex virus</td><td>Unknown</td><td>Zicam</td><td>Zicam&trade;</td><td>Ionic Zinc</td><td>Phase 3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Herpes simplex virus</td><td>Virucidal</td><td>NanoBio Corporation; GlaxoSmithKline</td><td>NB-001</td><td>&nbsp;</td><td>Phase 3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Herpes simplex virus</td><td>Virus Replication Inhibitor</td><td>Parkedale Pharmaceuticals</td><td>Vira-A&trade;</td><td>Vidarabine</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl11_Link" href="CompoundDetails.aspx?AIDSNO=007328">007328</a>                             
                            </td>
	</tr><tr>
		<td>Herpes simplex virus</td><td>Virus Replication Inhibitor</td><td>Gilead Sciences</td><td>Forvade&trade;</td><td>Cidofovir gel</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl12_Link" href="CompoundDetails.aspx?AIDSNO=001049">001049</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Herpes simplex virus</td><td>Virus Replication Inhibitor</td><td>Oclassen Pharmaceuticals</td><td>Fialuridine</td><td>FIAU</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl13_Link" href="CompoundDetails.aspx?AIDSNO=070971">070971</a>                             
                            </td>
	</tr><tr>
		<td>Herpes simplex virus</td><td>Virus Replication Inhibitor</td><td>Beech Tree Labs, Inc.</td><td>BTL-TML-HSV</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Human cytomegalovirus</td><td>HCMV Maturation Inhibitor</td><td>Bayer</td><td>BAY 38-4766</td><td>&nbsp;</td><td>Preclinical</td><td>&nbsp;</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl15_Link" href="CompoundDetails.aspx?AIDSNO=085779">085779</a>                             
                            </td>
	</tr><tr>
		<td>Human cytomegalovirus</td><td>HCMV Replication Inhibitor</td><td>Trifecta Pharmaceuticals </td><td>Artesunate</td><td>&nbsp;</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl16_Link" href="CompoundDetails.aspx?AIDSNO=112081">112081</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Human cytomegalovirus</td><td>HCMV Terminase-Complex Inhibitor</td><td>AiCuris </td><td>Letermovir</td><td>AIC246 </td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl17_Link" href="CompoundDetails.aspx?AIDSNO=499605">499605</a>                             
                            </td>
	</tr><tr>
		<td>Human cytomegalovirus</td><td>UL97 Kinase Inhibitor</td><td>GlaxoSmithKline, ViroPharma</td><td>Maribavir</td><td>Benzimidavir; 1263W94; G1263</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl18_Link" href="CompoundDetails.aspx?AIDSNO=070966">070966</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Human cytomegalovirus</td><td>Virus Replication Inhibitor</td><td>Gilead Sciences</td><td>Preveon&trade;; Hepsera&trade;</td><td>Adefovir Dipivoxil; bis-POM PMEA</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=028595">028595</a>                             
                            </td>
	</tr><tr>
		<td>Human cytomegalovirus</td><td>Virus Replication Inhibitor</td><td>Chimerix</td><td>Brincidofovir</td><td>CMX001; Cidofovir prodrug</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl20_Link" href="CompoundDetails.aspx?AIDSNO=104906">104906</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Human cytomegalovirus</td><td>Virus Replication Inhibitor</td><td>Microbiotix, Inc.; Karmanos Cancer Institute</td><td>Cyclopropavir </td><td>CPV;ZSM-I-62 MBX400</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl21_Link" href="CompoundDetails.aspx?AIDSNO=165691">165691</a>                             
                            </td>
	</tr><tr>
		<td>Kaposi sarcoma virus</td><td>Matrix Metalloproteinase Inhibitor</td><td>Bristol-Myers Squibb</td><td>Rebimastat</td><td>BMS-275291</td><td>Phase 2/3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Kaposi sarcoma virus</td><td>Protease Inhibitor</td><td>Merck</td><td>Crixivan&trade;</td><td>Indinavir</td><td>Phase 2</td><td>Active, Not Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl23_Link" href="CompoundDetails.aspx?AIDSNO=005824">005824</a>                             
                            </td>
	</tr><tr>
		<td>Kaposi sarcoma virus</td><td>Tyrosine Kinase Inhibitor</td><td>Novartis</td><td>Gleevec&trade;</td><td>Imatinib Mesylate</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl24_Link" href="CompoundDetails.aspx?AIDSNO=553065">553065</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Kaposi sarcoma virus</td><td>VEGF Modulator</td><td>PTC Therapeutics</td><td>PTC299 </td><td>&nbsp;</td><td>Phase 1/2 </td><td>Terminated</td><td>&nbsp;</td>
	</tr>
</table>
            </div>
            <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
            </div>                       
        </div>            		
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2794C6A8" /></form>
</body>
</html>
