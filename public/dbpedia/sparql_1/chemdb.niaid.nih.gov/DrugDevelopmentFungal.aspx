

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HIV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentFungal.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tk3UFaIeHHFFrLf0PnSr3XU+o+/tHqf/GSw3OK4VBIt3IU2CeoT3SRT3RDIrKT1jRO0RYQvVZnmF9WCNYPqYOcj4z0h7w/Ak5oJMFduzVQOsCpaH+/vLyCl+t8JYhwHdSJS2Kdi2sO6DNRDBshm3wEte8Kd/a9n2YpRQUZC8+cJvVq1JJ4I1e0Iij2Di3WzD51t1gcatD176KrWF9iLkY46Kkkk5zGBzLgjQYPSUsVS+t+hrxywn62y0zmDTahlu+LLyc+kJagrad0KDfNv5wpYfeTLs7fBXHrDzjT8avoyCdhCvCXJCI4BvrfoX2hL+RG4m2nhIqlku0LOl/SRVZ5mbnnpwBXK7xijP1Xr38+kVwm+68oKmosi3B4gj0vdQdrz3X7WnSs9gU7FK3riiJcgKFjMMet+Hx8FdIQD0+tTYnviLHiUY7dEPkZmyDTf++Itthwe///kOzvyyOTXvhmxc5U1yDOEgEmUja2bmLAr4DBTyhRRyQOD1+N2LID1Ui+kfK/Pq7S8GrxxBluafH8fHA2Lht+FV942PpgAuJWkGjgKrceoUKfDL2R06nNgvWj60GD4Qi6uQ8+v8FrDbbrecRXAF3G7fcrCgYcyyYrs4qk8GAmiUCUMJ8TAIujXu/xlk26Ei85j2SVSWIJejhm1c+Ovx/qt1OHvV5yr6IxluE6fVWlBJ70gh+zGSpkWcpP31Xogfh/7lpFREJoee187nqdsjWUnbMVYLdElmBcHRDXBVHqEnysHcUQ7Sj8DtHbK73sA117YV27MZC9h5mv49jq0G3eVIube9ykrumIGjcISS5O5+arVNucZjOBwdYA/J1OIRkhiwW0Ru649bcDejL5LmJYhYOjhpQZgBEcZ73bRfnri0Ge6RAuIkLpyLv8yZJ2cPt+YIkJRlo72hqUxaRMlN/2fR3RyMyCJPrr9dGkfVly75QOsA+9tK6VLt5nix4d09Z9Q0iRkmNF9cCQemq82Fj6n5cVtjuXL2W8+4+hVjnoX1ErCZ7SH2/vu1rkIHfHG3S/Xo3LlbfSOzrJM0kR2z01DTZqsaPOgpIToXrnuZHqZAJoChPm772VD8RUJTDihYTvkCOn5Fk3t0AKJ4NYtAPxq/GW5hcgQOAFrRi2/+H7KVUuKGSjsYjChmOj5wEbp0LcZuBx1JzcKqK2maJ1seOQa1lhxLQEudh2OuKJKWknxZK16xYeEOmbggkoBSv//bCqBc2vMo+WHs9bnj2aFv0Hw6XYCr9qL7T1VFgFhMLzPRXA+G0RCNJHHJ7ULXpKq9IdT+uo4LkGrNpnDGvOD/rmdAOCwqkLS5M84VOsGLJ2gfPlxH1uKgaOdsbcSE+LScdorZNvSfJ1vsycUAsQCPr2XpKY3OkBXyHj+bzs3VYTAb7rZuW7hyyt9dYX/3ZIME20HfzM3RUlxpYqj70q40C7E7qfrgIYwLClCMPhS2jHSonglsTET/RMsoPKRqPknG27liJnPxDVLLlu+7qRMM0bUvMHQBT9Zt480eHjm2nmeOKLdEPldIEcwoYmRdLjn2VY7McjvAsruTzv9v+KefCOiYq7Pgn7RNc1jUF0XIZjGRT46rYHsXbjDqutGD8m8IfepMN6WUnKUxaT8/xY3FSsEKf+zsOX14TfkF+HSdDpii8KoirS8nV9mIrisxrdjzV8DN/mQB//fuOL6e/r+URTytcCj8XFhWfXC4L1wThelS+wzYUD4Cb5BRcTzJnZbE5/oz7m93EXgN4PIQYnBIk43za6517WSYt1CHsDegGizBz8SQ3O7BSlF5r05aXbNK/teGK0E9eVbmEB4BeI/36Xz9icn/+CZEU084tjWcIGATp1Fk6U82Jgu2hKQI5TMmwYc8v2kggGna5uEmYun4fx6zivhGthBE+IoJj/q7EYzdjIku2T+7lV1tq9R+9rrQpfruBRnDZZToeqdM1ld6tDafxJ19tEocNu+EkW3I4tdQO8sXMDVqRGYfYa7ouSbrFHdat1vBbqvMKZCAGagbuzDI2fuWDaC3FLE7HdoUv+8yl33vDKJXl41dAevG7WwFX7tfko5T95w9v/ARACbpSOYtgE7dWPpLY0WqAQ2991kYbwSs479XsS14U1yEiF2+1/yVUudKyn06K4jcHSVlCSZTsmnZ/raWnYAiRSX30lwwaXN7Q7t4si1X+wkJ8BdfjTnm5hk8cjhOZejWg0X4pfiHNaYC6oeJOpS2mdSqkj6vvOxTUJ4A7sZP7uzsdEbuFIW9o0/3XKgc6sxhpoQosO3Dj55Qxko+CikIa8I70iY+M5mHq6R40gARF3QpQXq7q5n5UtquCKDCAZJzmNxuEQu0WayhQCAcHNssFVS3fPhTrlTcNzKpIqw2Y3tCW1LTI4YxQMEeFHNEVQkOKGSIOL7ltHGFN5lZTvKIjhyWZJDVO84TmCZg5mvW1eRcLBjQWz/aoYcFXgO4c8KTYrPf3aa1VleLVKailkUr3jfxxIsJimcRVHuo6h6FHwKBG4fcIwn2+uSR4XQ5x7k/diVAFLev6ri6hK3fb2JXoY+O9OhBTwGUkJ8H6aRbsnhnBGm9PkYMFVWPAi6Tykf6HC14hZdmZLjk40KPWizCsE2mCxIY7Q/4tnoprFGPFaJ8RozqfreazMQehHIjOpXipZoa2i0YVPOhe5244At5vH0bXtJTmdJM/lqOqHQ+QNKkLztwaHhlrjDN9y4kaJyUguCDhnmJrlBewd6/Xt+SfuYX1g0z3aSIYX7iYiAtPHHIwR+olbQLsF7wfG5CYBLPDCmBLauuow0i8Rvqei9KpS97MTD7cgggsTzMLn6n5rzte8WrKXhZy5SGDmNQ/WIT3hUD98XTzskVCqWplB93P+QUddx7DxIVCLgdzqaS+Jb4TB654wcievyy1iTk2rk+WMWLKbyNRfaIYHwv7iCfkcZosw3Dz85DM+aa+6KLKcbPQjOn+3CRFo6nk/MuaKIG3PGm6cZocLH4uKu9EMOdrpj/syenq26AN46ZVFI7m9x0MVv0OXX8CZ7HoWfS8pRSKnPAK5MNSF1kAcjVVy/sqOwL6OuJPjRaXLUichUD8R9lVC3zVNeNVGWTWXQ6B3kQPwFJ+Kr6m/DBpWOVSehDKHPoRYc3o63DK7jycFi1yZAPvecI6lhnFUrmyI8uPGkyssogV+JY473SpoCGov1DOR6vzRdg6tNzVSkmESJG+IeQ9+gevQU/P7ngF9Z6Fvxg8IECNTPnY8gDCi+M1YAyV14Qtp67leG6TtLGlxyypXv25FkZxQY3JtxZX1H3ALUT1qNjhdKruAOLGyPkGwQIXlxhgqClOcSyb+Hj7cwKS5F3wGq6bm+iZ2LFeb1gOx8j3m3yZArJXCylHoXxVCjQ8Fpq9KcxfQNfmKoa4iMG/h4qcZpdOpZvzkcbEoeeLxHpP5V7l9opYZXsGXOFzbAai2JuNgJv2dBDbqX+3LGADi99tOuiXRL1pUNy1X84ZhpsHh/4LhqKUHhyh0YePoi3oGxds5qD0PJ3WzVMFK1xhAsXRMcklTKiR0WKEUB78P5ayOvHvTaKrRLzACGc7Oo+4+n4tbsTbMfP6uHdX6o0TS1EUvNBBVlxuHX5f5PeZdn+CrMjHAW9AeVOSgyLYrr5AfW7IYavxU6Wu7FNm3XzmSOyGAWd+cRD/X59hTzXu5mX07aQ5J/0suAIA8JMCWobxhghRKharC66XnTKoxPQmsutrytUgLsZoLn468lecYiHjs/w12OiPVNH5jy+p+pHvzPV7am5CpQJ1hDXdz2l0LFauItMYjav22R4cemH8UHmNod2Am9trZH7oaUeG80r/4i396nzJmjTfe3JyJDLqxKLqhgx6TPItnw3pMVTXp9KlahHbJKR/TK04bTxFqJ9UoHd7UNrHDsM8a9Tc1pgI6QS39VEc+P4sApbDTMlaArXLJoNoJ/7Vq3iW+T/snZXmAHDZUEykN9AnNTSN2bDaIYpl6HgQZky13I5qAVXBMtQ3jc7DuaKIgKDFgVoXmg3U8SLfocY5DfO/NNsnWi/+SSfplrYoHLkpRZ9H8lCWdFtGL9xv5jtY7cisQBA/v9xY3rB/9PM1x1eiHOOmFVYBbSwcE6ddPPrpoDXGe8JgG762B/mbHXxTo3bmupwtU2yb7Vmiwj+x5Y8SJBy15T1f8AZOMSp7dsyh/rmF4WXTM4m+2m0iqLRoxAza3goDRI/DQbSnQNSlFiVdmJZXFoMTSq+J9af4WS7ATwCZrqLIFOqG4ydl312n7YXG9LtF7qZyw722JxSJFvQaGBe4WLYhyEZATFcQgYdTIxOSdl0ZX+gPw4TY0aM1pPvckqC56c+Xju+02VCnruPr/6WJ3w3sU5DmJ8/vagpk2NDIKVvm9ZjVWUPtSjIufsB6cQ4XsJiFr9OpFvoiVlDMtAC9Hr9T8JfTr1Fm1UI+FFBfR/wW1RSTbcYRam/GslRNH7vL55Re8pY6p0Fmq4DOytmCiUBJGwjyvpGkj4qkc4KsLxRp04UTgw18FCX42QIyoW2L8ni2GgBJw0bBZNy5yV7Ar9xbGtFR5/qF1YRrn41gr4nU3/XNnqoVdlEWCQxSqtegW1HbH06i0o+1tI8g1FBRo1lOqce4yiFYpHEPeXX4iYqVNHrnJhk3Ir0+BNC52t/eohy9FZJiwPejTExMjxrtGGplL/l9/pI5D+ammHsIESP2jKmNOy2y1ZLhMOTo5/NVFvz9ABn+PcZYo+tYYkCyXKU/yxMdWHf0H/rY0xdYg+11mSDcGy4FWSWnq72HTTTX7fcCLXnPKwJV7Fq82tXOV1zr4ndvcK9JV1qsflUVb4Q0R+kiAhXIVijKzftzxb/zoJ8J1sbcahsgJ2pL6yL6TlWqjJGAoQ4IVUAlIL3uNhQ2kkbnQG3NlQPm2BrcXTTPMWfSfuWP02pHNGbBU8XkNdUqJPUhFVtUoLtqwJXCXxxbZtU6cMfN/70tWaOkMTXE9EdIq/pACK7RncoP9cEX/l3rfX+k6sr6HoyAa0Beh6jzKQYBgSMIbh3gJkGIyfsOxXBNk8bALyFOeNGCi3mkpRI+w6QWkT4smgKqh4tPxhBoTamKJysUKlbjto+KsoTCdPiX3WVa6j1WvuzZj1iqho/tSkslAQ2lRxV0RktePRedLU/uOWhEQPEkKj+0eYvnG2YhReBmfQivOEji5EccSCRRlDfyUdc5RM7ByCMW7g5kUHeGumD5G8tYuaO/0+ZHclzhXbYSzVw+LUxJLb+Bn9niXygsdNhBb+OwIAZIaLSxjRll3YhQBBOssiAvWTCDP1yYY8vIbrbbUn1qAhs4vsjZb63RnXvOegQxA9wFo8ryXQV6rrr00TUeQhFPb7hiWN5C+AzNDAP8JRUnK6zmot89k7bOt5Tfscghr2CuzW8byv18q4envC9fFAAxCBs9a2HcVV87izlcEoFpCxV3muWOyqDsteuGzmIHwuyInIErAxR+lEtALlwNsiddVhGc77kWHl45uPf/Uk4dXIMi0t/j8/srTiiFxZQrG0hItrSZzulrkoDeQV9+USoSTfBo9UqufTKVh6BEO+EpmL4jHr9T+G9PGBoPJclli0YbwiipRxAEqg7uuiooEWKQrx1u246iA0IY+/JgSSiX/toXSwKbOlsYHjhldj9X4tvY/7IvNtkPl0LN4h2HhOJaeb18NUJGF9sPeCZtqblgGEmuYWcW+mG/MBnpkPmdrvyldzFQWCZSZQ4BXyLJmdjs3LCvy22aI+b/5xQOCpAiwemIxhNSJvkJl8Bgrnb3RH/DLUJPSnSHHvKT07Os8r/+zUeHn7ZrUQMXIq4rKivhDTOWreDRM7Fp3DeNVQdW+fMAMzOT9EWd7F1g4wNC3+4cOaNNL343We9k5sRkzPrT50FmmogZuPFUbPHFVzN4900PoMnWEx5mv367AAwslG71Evfj0QZuX4JvJZbta9OOE5fcdHFEvo9TETMlQ73jKvEy0EsGJ+WNDoIA36T0w5mEEsioYLEsPuz1Ye5HSdEW2Ww/u0PZkerQA1y3oCWpbugXQmBFWodQujLL4rZ2eBKahP6h5cZ6WTbVh8aqkw9lPE5L+bgmnLIcB0FFDJMTeQKaM1gKwFlKJe3SsPK/6krU1oGkN3Bijv7bZr0M1Z29c7xK9FJvqmiR/4uP1w32e269ojT6PBzA7GvhMeTcRka8BzJTt1i8BPXQv64fxPXQSK7ScXtaXkgAuC2+6mcdvMoJcYmEBnNn/g3uhabGf7RWfCvZTTnpc20nUwNxH9xvuaQyUhkRZUlvRU6r0jpwGdH4K/oROOZW0SfS40QXYQ5rg4KihQHpwDQPrPQ8LZJRalS17to7DVpAeGV8GoQrlypfIaOvOxFC3N+y0RpgmPQWYJGW4evVKmGNrVs4gm76Z60uEVvwP5nowzqmR7XJy5GXeaHyUfy+W6wQVEBaeiLu+iViAJ1JZknHxXWH097+t4pX5UzY1RegnqDtcC301CafXhcSs7O47rueLRGip/PQbCpykAIX3cfo4U4UWCj20FmMc8noVYiphLFhGfibSPJCTgnB4p4ExNliOTdxbd33+QYENVCrP7t+cN+34/ZWx5wWvQ65+1c5tP14E7T2uEJ4m0gNd0iGr2XZZg04u6USCQHBccIjAXNRznI76icAdp6rdgPJwX3kXyDazfeyijxVZcCFdIxxKBRi52cwlkjjg+eedOZ40Mw3zgTlAP89hNLVabTVMVjqaETyZYOrpq/0ZIdTyqLA9VHKjzCP7lI6FfhaK3mPxJeteBRNN1o3+25I260Iw4hZg==" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>Fungal Drugs in Development (Archived)</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugOrganism"><font color="White" size="3"><b>Organism</b></font></td><td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugOrgName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugOrgAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugOrgNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>Aspergillus spp.</td><td>Combination</td><td>Pfizer</td><td>Eraxis&trade; and Vfend&trade;</td><td>Anidulafungin and Voriconazole</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl02_Link" href="CompoundDetails.aspx?AIDSNO=549947">549947</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Aspergillus spp.</td><td>Wall/Membrane Disruptor</td><td>Basilea Pharmaceutica</td><td>Cresemba&trade; </td><td>Isavuconazole; ASP9766; BAL-4815; BAL-8557(pro-drug)</td><td>FDA Approved in 2015</td><td>&nbsp;</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl03_Link" href="CompoundDetails.aspx?AIDSNO=416566">416566</a>                             
                            </td>
	</tr><tr>
		<td>Aspergillus spp.</td><td>Wall/Membrane Disruptor</td><td>Gilead Sciences; Nexstar Pharmaceuticals</td><td>Nebulised Ambisome&trade;</td><td>Nebulised Liposomal Amphotericin B</td><td>Phase 2/3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl04_Link" href="CompoundDetails.aspx?AIDSNO=000096">000096</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Aspergillus spp.</td><td>Wall/Membrane Disruptor</td><td>Kaken Pharmaceutical Co.</td><td>SPK-843</td><td>SPA-S-843</td><td>Phase 3</td><td>Unknown</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl05_Link" href="CompoundDetails.aspx?AIDSNO=121440">121440</a>                             
                            </td>
	</tr><tr>
		<td>Candida spp.</td><td>Cell wall synthesis inhibitor</td><td>Scynexis, Inc.</td><td>SCY-078</td><td>MK-3118</td><td>Phase 2</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Candida spp.</td><td>Combination</td><td>Almirall, S.A.</td><td>LAS41003</td><td>LAS189962 and LAS189961</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Candida spp.</td><td>Combination</td><td>MethylGene</td><td>MGCD290 and Fluconazole </td><td>MG-3290 and Fluconazole </td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl08_Link" href="CompoundDetails.aspx?AIDSNO=496028">496028</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Candida spp.</td><td>Enhance inflammatory response</td><td>AM-Pharma</td><td>Human Lactoferrin</td><td>hLF1-11</td><td>Phase 1/2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl09_Link" href="CompoundDetails.aspx?AIDSNO=028450">028450</a>                             
                            </td>
	</tr><tr>
		<td>Candida spp.</td><td>Leucyl-tRNA synthetase inhibitor</td><td>Anacor Pharmaceuticals</td><td>AN-2718</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl10_Link" href="CompoundDetails.aspx?AIDSNO=471686">471686</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Candida spp.</td><td>Sterol demethylase inhibitor</td><td>Palau Pharma/GlaxoSmithKline</td><td>Albaconazole</td><td>UR-9825, W-0027</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl11_Link" href="CompoundDetails.aspx?AIDSNO=024107">024107</a>                             
                            </td>
	</tr><tr>
		<td>Candida spp.</td><td>Sterol demethylase inhibitor</td><td>Viamet Pharmaceuticals, Inc.</td><td>VT-1161</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Candida spp.</td><td>Wall/Membrane Disruptor</td><td>Ferrer Internacional S.A.</td><td>Arasertaconazole</td><td>R-isomer of Sertaconazole</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl13_Link" href="CompoundDetails.aspx?AIDSNO=008889">008889</a>                             
                            </td>
	</tr><tr>
		<td>Candida spp.</td><td>Wall/Membrane Disruptor</td><td>Pacgen Biopharmaceuticals</td><td>PAC-113</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cryptococcus spp.</td><td>Unknown</td><td>Alkermes</td><td>Lobradimil</td><td>RMP-7</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Cryptococcus spp.</td><td>Wall/Membrane Disruptor</td><td>Schering-Plough</td><td>Genaconazole</td><td>SCH 39304; SM-8668</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl16_Link" href="CompoundDetails.aspx?AIDSNO=007341">007341</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cryptococcus spp.</td><td>Wall/Membrane Disruptor</td><td>Kaken Pharmaceutical Co.; Proaparts srl </td><td>SPK-843</td><td>SPA-S-843</td><td>Phase 3</td><td>Unknown</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl17_Link" href="CompoundDetails.aspx?AIDSNO=121440">121440</a>                             
                            </td>
	</tr><tr>
		<td>Pneumocystis jirovecii</td><td>Combination</td><td>GlaxoSmithKline</td><td>Clindamycin and Primaquine</td><td>&nbsp;</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl18_Link" href="CompoundDetails.aspx?AIDSNO=000295">000295</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Pneumocystis jirovecii</td><td>Combination</td><td>GlaxoSmithKline</td><td>Dapsone and Trimethoprim</td><td>&nbsp;</td><td>Phase 3</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=000310">000310</a>                             
                            </td>
	</tr><tr>
		<td>Pneumocystis jirovecii</td><td>DNA Synthesis Inhibitor</td><td>Immtech Pharmaceuticals, Inc</td><td>Pafuramidine maleate </td><td>DB289; Prodrug of DB75</td><td>Phase 3</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl20_Link" href="CompoundDetails.aspx?AIDSNO=022387">022387</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Pneumocystis jirovecii</td><td>Succinate Dehydrogenase Inhibitor</td><td>GlaxoSmithKline</td><td>Sitamaquine</td><td>WR 6026</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl21_Link" href="CompoundDetails.aspx?AIDSNO=007904">007904</a>                             
                            </td>
	</tr>
</table>
            </div>
            <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
            </div>            
        </div>            		
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="86C53766" /></form>
</body>
</html>
