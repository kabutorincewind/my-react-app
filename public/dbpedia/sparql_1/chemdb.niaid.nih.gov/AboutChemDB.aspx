

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - About NIAID ChemDB</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="AboutChemDB.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uDBZRWyoDOdoa+1AUjM2lWM4bf+OyhIrI41lyVvwPXn8Vd8om5Q6zpIrTvz2bFmvK14PTqp+3gK7v3GEzWHiM+AYvSiFazhmL+W/TISdIQjO2TtrSd4Uc9rgD7o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C71CD114" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>About NIAID ChemDB</h1>

        <p>The ChemDB HIV, Opportunistic Infection and Tuberculosis Therapeutics Database is a tool developed by NIAID to compile and annotate preclinical data on compounds with potential therapeutic action against HIV/AIDS and related opportunistic infections. This database is continually updated with information extracted from published literature, including the structure and activity of compounds that have been tested against HIV, HIV enzymes or opportunistic pathogens.</p>

        <p>Entries in the database are organized according to chemical properties and biological activity. Biological activity is broken down into three categories: activity against HIV in a cellular system, activity against HIV enzymes in a cell-free system, and activity against opportunistic pathogens associated with AIDS.</p>

        <p>Opportunistic pathogens included in this database are: </p>
        <ul style="margin-bottom:0">
            <li>SIV</li>
            <li>FIV</li>
            <li><em>Mycobacterium</em> spp.</li>
            <li>Human Cytomegalovirus (HCMV)*</li>
            <li>Epstein-Barr virus*</li>
            <li>Herpes simplex virus 1*</li>
            <li>Herpes simplex virus 2*</li>
            <li>Kaposi sarcoma virus*</li>
            <li>Hepatitis A virus*</li>
            <li>Hepatitis B virus*</li>
            <li>Hepatitis C virus*</li>
            <li><em>Pneumocystis carinni</em>*</li>
            <li><em>Cryptococcus</em> spp.*</li>
            <li><em>Candida</em> spp.*</li>
            <li><em>Aspergillus</em> spp.*</li>
            <li>Microsporidia*</li>
            <li><em>Toxoplasma gondii</em>*</li>
            <li><em>Cryptosporidium parvum</em>*</li>
            <li><em>Plasmodium</em> spp.*</li>    
        </ul>
        <br />
        <p style="margin-top:0em">* Pathogen data is not currently being updated.</p>
      
        <p>Inclusion or absence of a compound within this database does not imply endorsement or lack thereof by DAIDS, NIAID, NIH or DHHS. Use of this database is for scientific research purposes only and the data should not be used as the basis for any clinical decision.</p>

        <p>The ChemDB HIV, Opportunistic Infection and Tuberculosis Therapeutics Database team cannot provide samples of any compound described in this database. In some cases, samples may be available through the <a href="http://dtp.nci.nih.gov/branches/dscb/repo_open.html" target="_blank">NCI/DTP Open Chemical Repository</a>.</p>
		<br />
		
		<h2>Add a Link to this Site</h2>
		<p><a href="https://chemdb.niaid.nih.gov/" target="_blank">NIAID Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></p>
        <p><em>Copy the code below to add a link to this site</em>:</p>
        <div id="copylink">
            &lt;a href="https://chemdb.niaid.nih.gov/" target="_blank">NIAID Division of AIDS Anti-HIV/OI/TB Therapeutics Database>&lt;/a>
        </div>
        <br /> 		
				
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
