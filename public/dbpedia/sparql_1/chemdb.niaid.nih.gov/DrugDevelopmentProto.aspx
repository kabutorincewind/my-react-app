

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HIV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentProto.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="K6U9xVUH8eaI9aRCyBdKzsyD1mb0HpfvZsaY27jjc0PqvzkD/bH9eebtOu47qumR35An4YJXJVvF20aR8AxCDEYG/3rfQWGBFJSCeoE7hpoXg+2ghfen+0aKdVBt/KUHZe+8agrErOriXN5hqIJKptwIVeDFCALiTVLI4AD+i+Uwmg07ePsvaqaiTggW29th4NPIHF6qP5YkH9xXxU9KfYVbhG+7VP0BZ3+ED6Ag7ttuUL1pTLADV9J5P/BPLlQsIOwr7xZoFyIEJ6EHDYMy+fP1rrToon05z0f6BnUIyWJl97Q+H7UggiS6w9O56edZMJhRl0ALNZIsyLSamp5oumYNQGwYjK/TOKvlzFRN/sd0+y4zgqLoTLwktrlTW0MKm/CnGiRM4qOd9tTDYJXB3CHsyfFHcRam7PgBDmJPtnB7nuFsD0BRuAoFXDhlmmI0bXsP7M5em/EIvCYD8Ea1rToGy3j0Qk2Q5IKyt1NM/MKU+K9oMTb+0xDGlS/1KhU2TuOW4Z6P7XLNbdtwYvPxBjwk9rE+V4Od6V0NCOkpb1jzzyWAuN2brjrIVbSt+BIIAZlguEi31MPdVRb4xNGQXtM3M2uUMRaCbHlj9vDHjuNGbRgDq7sAjtQIrxBfxk27biJmK0ZXUaj95iryvVPUOLjiGBhUt4z+8uZHW0zdOdjg7R12nyPqXSrQUxf2YZwElFgPrWJPChuTxSrwX1sJWg+gAANHviSEoBH2E1xDHkmbjP7BH/oXJam7gEPcSbuAitMVX2au+eAq/0npT5QuUuRXOnMtgDCSvZPuMVbyO0fL+nW6QSYcP2cDSnBpgYiNh+WzNPEoAd0vgKNraMCAF5taVrwUfsX8JBx2XC9x5zQ5ng3FRECdHPn7wd91BAcXaTV+IFtAQjp9ZTr3lk0HqJJtFHgPaYw3/kYuR2EqP9D8Ut/Ji5FLYWeTlW/3Rb4peYaRw9eeYgbaDII3MPNts4T+19jHUU7fj9TtUbzqQih2CEwQ5ROpGXKz8zEUmcJrYiMWmeEXkXaB+/9KSSzwTbNhQuDcAxSpNF5ZQMBoRKRKiRELPKH0Vf0eNRUFSKUqldyL83x+HsTritl3E9+UvNY6VyPaHY95rD1bd10ObxqXctoZG8n1xnm65U5ez916K3YK5FslHrQbffztGgnm+P6dLEQPvRY2OBnUsFbWjCogdaul1x8mwTkp8Na9hFWefg2Iw6lRiQ47ktcJjIS06sRsPnd/STWkZBRCv/+03kqzpR4Yd41bOt5zEGKgHD26y2Mei/R0OGSxccNUpfeSRnzY7I4jqstb1tNom5jgGk6lLc3Y6NVvvxg7OkwWIpy80mFZLPx03amHVqVKMeCcwoutAZbrl7FvVeqwkvynHsFhVYOheutmBC3kiQNBI2IHQMqqQenz7IbfU4HL1ES6IbOT4G1ZL8rKrMDeP/FBWAClii0nXMUzOij3JMRBZ+ArsZO4F/SHYHIxLl3neGB2TT6xJiqIMrMQH5p07ZM9bdmwCwpuY3LDI1/Vj+bafFPSkRC5vglWSef8U5lS3thcWf7n2kFDIMPgHYlULJgZVHt0VOTwC2BnzV5AociQbTZOTZx3lv3h7jJFL4rMZIseajyT+w5zINH5g51ewLAQOCyy6pZdbvo8lrWvnGqYiBSdqn0HCwmnpBzgOVEPAKKuHCy5xG18RN0+Q0FsLoERsEv3wWIqWfez+K3zLlueSp7poArsX2Z5VDscOFjj8vToX3G3qaWkMLYt7OBdKCCjkNLHfiTFEgxDiUqKZTPjHSQ7pvf52umpuorpkZwMm4voRrPMLHBgV6BSYt27UFkBs7GgEmDiC++GYEQtONo9odd7zrW4MWOwyMJb+kKbA5WOO59F7oOF4mMVOB4FNfa+Ta/d+UGMBKS84NxVXkED87XHUVqF0rQDeRkcqKXaWw+pJku82Bc0sl+kWdnnQCPuQjBeI8DD5ouorFp7T3lnAqTIu7aaKGPjFhW8y5rju9ejhcx6V8NzyfBiHdZEJsgOiCs34N+1aO0HcQ0LyTnKWVRudckOhCn3G401SqS8l+0wuvVhb4+wFYJmKz46rIOMlFO4Yi3ydO01C3eQKBQkL9OXESrYcras3oXgIktMFmoXgtUSdW1DblEmzLzBzervg2JdN9rIZ/iTHCXBEHpy8aS+i5tG17b7Yd61L6Za3iz85hzkqaocy3KtfsyE8CXT3shYHxBhPu2MOGs7aedOyNA89OISn7eJKYB8J4CBewPwWglfN5wEgeFZRmbN1mE+/z3ee7AzCUext7PIwbfhp5GdSsHdSxWBGdCx0BioRUDaF+qF5sxPnQHjLnwLLW3AdBniKKr3aRClwGfQipjXfWpY5YAm5aFwh2tSka1J+Q7JkgXJkbXV0jxq6Ivvfa/CC008+/l9ZRgqTUKRGK+t3DYdp2XLcm3wlm2L69DhPZkCd9pMwtiJGXHjuyDw980D7jpXzbGDTdGArAHlzxarJG8VV97xvPXMYuuelktZ9hrS8YblSGXuON8/Hou/cKWesKrrQll2w5Q+tQRQKMqsVw9ghWB2TqNO5zM4Nc5+jn6LXoDQFB5g0BIC8z1bnFNtLesVIYmS/YPHxX6tI8gIXSbINGP9ODE8uIb6tuarYTYF/9aPC4ZNchToeaKHiIEfkcKDObDdSiDfSCcA0V9Fi4IZ03NuGT1ZOnvkECXTTbrUPMGXdEA3tRffwNye8G19SmmYiO1K44vNSNL+HLLtRyDliyD/y5cUsYO48t+pb2GhY8FxMln/XBfqkKuw1+pe8Lp5fht02SyEUBnMGu+d6CJ7nKrlsPhXtVMCUQcJVFILnauwqPsirVntNwOd5RlD7RImJM2QTfEzCN2eHVwVD/ShVjB+cL8z9kip/yGQD7ptaS0pGpxKaehRqNrr80F5BhmuR59VkmignT6g1m+zlh2U6Ecjoyr4SvXd5gJwn6vZu0v4ym1VbwZnIkJLS+vQ7uQTjDSq3kl3hsGnlQ5amuVihGkw9LW/29ySuhmWlF+KAH4RwxvX8rRCQQ1BEuwxireXa1PNCqK1J3BWWZaJr0ffbSARzWkXnUJQ6ROrPbsukbbkgyvEa0/xBCJWGyYvYcbSJULcHAibTELXfJ90T3xVaHzaFb6bJAwf5iu+txfN+h2DlnTnE61gVjlOMBGbZ61uzSUrKED5EOE5YWaZczKYuSYjQsnAlAD7OioFNqnCWf0d2nET6kPwRTMOvJ3TK6sAnYIcBtcprpm9Tc1ZxZsRCicsSOpMUj/hTGrMuD249g19c5PA0TYdtSGBslBd7tY/YCXDrueS8gdWpZqEcNG8C1NYxMVAFt0yaPuVRsote1OgHsW8PEAtIxf4mn46I7eisoVbrr8r8gnzzj34+w1C3FlHm/0rUt2iN+TUmK3VxAbBwCM/txreW6I+AnawJcSAM1KGTEYxRqxoaUy6NcgZW2nTZuBaBGdI3+aYkA/cMFLSP0PjFSFTwTb0AzOKPuxvhdqcxH2X3kqVYKRdQyW3az0AdzKITDTGmKlwrO7UQaFy+NVwj24TBjRL7k3Qzma12HPLXvmp901etgwAke+mFrBBI6upApd/xACBXAMpXc4AIbCQ2tl3izLBdLkSihyjWsJUDBVeQpsyJsepAZDHmkXYIfrOxRA7EgrMoQB1+NsyvxCJ09PRB7MVMkz+HlExYNr7Fl/weNTCFSbLsfgJiG/pDkJuFTfnLrQAFjqlsdo6x9eE99mZAF9VsJOgt5qsGofZ5NyE75i6gg9Zap5LJ+NVDfoYNSRfGzb12r3XBBD7WJKbOUfXf+oLDxkmKZnzBJPkn+aNlC9D9apTne3xVtW3nwWA+Zy8XWwJiizkm0X0un/a6tOleKf7ybmGxhbtnjnts/osN1WEUy+rDuPIUh6psmZb7l9kf/zAfHeKQAiG99qed2Gjr/FvCCmI42aCSvZYHW7qIl4Pr3qhdL+ww+tzyTxscVjFBFKpJzYfZfAIXHFhiwE8HQYnBh0dbj03Q7NuVOYGGnvVQ8/QdxSYzSnAhtnYvBM0JNL2WLH6lerxYtmojY/bLmbUmNcFoXNnUqP+9sQhNgA9EdWFniIhWxbqU5XR7XgV4g+NuaWNm8rneF6zpwa50KhPFUN/8+rMI5eDcM2vf3kJ2VUg4iL34woeDnZfdadFYdXik5zFk6TSJshwuAd6zs7g+kXED7XUhCTVu/7mM4EckE8TUr1aQZc8nnTL1/o+pgTM+hOfzE7JXu9TRlXkjfRXHrb0YRw3Q35m2ckd97vCdm321dQXnqsH/sgd8GwFw6tyKo6tUEEZMRSL/mUfcaWfkxZNzzkcxYy38rbmOlkMXzV09Le77cHBIBTzUNXbYXibc2AXrBD96F6IfPTMXPy2dtfqAepQUtHaXtetEbASvRybRH18pm6jh3Qrk1lcuC2WwVeATglpA1d+1k2EXff7S5yKFCeQj8RT3wqZp1X53YKEdT+SEUVW54NvQTWTjUK/5woeFxQ91vCxQLlOuwC1I8Rm74XkzVcGDm+Y9SpFLb5VObS5MYOTtw3wNliEVKWfDbm8+lNM326kSKlXg5m3Hx5kuhSOFj1bUytG0ajWjXqn5k69c7gC/Giv3x/Jgr7LRk+q+Eb57qNOAUgYgfMZG40jzbPhwA0iMp1dxycgY7KNG9ajjqpOZO8H8JIbMbsbKAb12lYJ8Fs862q5H6RKSlslnkUIv64lxukOvh9vEaF+3REo3EOWJP7bQhHR0EDvgCQDJpkF9w18eemNQU7ZKIeHW5hGsWyl9K24Ne1bNL1/Q4MWVo3xK4zq4X7r7yVFAgcoKt8y6oV0HX1Nyth3wN3zfvTxgIINlfiz89qj5icgOPY1CwtdMxiHXJG1dKtZsng2Ol8oy/+BDRGyKgnAZcg6h4FU2N0BI3hDJtuPdYja+mBJcb9Xa5huBmhHMHfyuTPD+wAec6v4fW/j5kBM1E0/HyENJ96XeQA/V6S+oF5tZDJKDrfKrpxVKk6zgNPecYPu6fGCUWyb8aqp+PRilZs2eYV8LOuqjiKIYnVPIVYz9XLVGVDr8+i4uvoiJYgzE8BKk2QniX5NTxYd0dEFeqwknziIkPVFg1dMpV/H3kYuv/DG5EWhG13qM9/gmktFMKZYzpUtdHREXKnzTLOV4UsbG8rLLWnrCa/LwWyd4EHw23fEo3ZB9NSJPultYZ5pMhnQUiFnDRogYdUdRWeksku1dqJ6xO55tPGcYJlFX0AH4Ko2Q17AFZQ5g1SQUJRsxzQSnvraJSLYgGtsyAO6MdK7Wk8Xb/eRmjsOFwce9xd/YsiMlr0C6B23AnbWQO1r0luebMdnGVlYlXV1lFjhmudJlTajybkm8XGIsYcaEVRQPQi3Ijm5U3z3yY0FaUdPHcYaNTk7bT7MC1u3we9Nc/Ff3z42GNcBRZxAdTGgArCafGz49NQUPQuFnHcdGtKBbGREq2Cg9ev/gpvVoDtvNztdf0ggKMsgyGutBkxQyuUERjMZ0YF2GmglzHGAI2rIDUJyecwPf2SCz5cQW9ORamuQ9LOam1G0AjG5Ag9MXzgXWjfwczP7X3x1OPCOFRdRPlt5I/rpztPtPOp8sD4Z5I61ycyKz50TQBlDY9lkcTJJRjnT+4VRpDj19p+tdNKNXy1q1z2IFqk/7Cl8E6Ws3Dlclvoqj6gbx4w4oShBBTBPywtgMOQITH56mfyOHghQYSWdxX3vEgmyu3uf3G/XQRlwa/nFuDc0dv1pBLHKtHJyN34AYtjI8f5c3mzLUKoedC3H9musit2TovTn/dlF5qCVwTn6YO35ErkOvtR1k6JF8dQr6I6aI5vLBFyrz0zLQsFCe32eIaR57r6rKigUYTPZwo3LEFTLZllNSccjqM5MCpiMwZ7a+c6Y3kqzIsBAZA3M/LLKbQv/sebL5HIa0vkFG9OeHPqbM2ymKhnlAVYiNSX1AfNR0iRQ+/ltdtxO0MiXa+0PtG8sbZn7HctLrxXGzRHB8aWrCH65WlryKdg=" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>Protozoal Drugs in Development (Archived)</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugOrganism"><font color="White" size="3"><b>Organism</b></font></td><td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugOrgName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugOrgAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugOrgNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Antimalarial Aminoquinoline</td><td>Sanofi</td><td>Ferroquine</td><td>SR-97193</td><td>Phase 2</td><td>Terminated</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl02_Link" href="CompoundDetails.aspx?AIDSNO=517288">517288</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Antimalarial Endoperoxides</td><td>GlaxoSmithKline</td><td>Tafenoquine; Etaquine</td><td>WR 238605; SB-252263</td><td>Phase 3</td><td>Not Yet Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl03_Link" href="CompoundDetails.aspx?AIDSNO=006901">006901</a>                             
                            </td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Antimalarial Spiroindolone</td><td>Novartis</td><td>KAE-609</td><td>NITD-609</td><td>Phase 2</td><td>Ongoing, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Antimalarial Trioxolane</td><td>Medicines for Malaria Venture</td><td>Artefenomel</td><td>OZ-439; MMV-08/1019</td><td>Phase 2b</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Antimalarial aminopyridine</td><td>Medicines for Malaria Venture</td><td>MMV390048</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Aspartic endopeptidase inhibitor</td><td>Actelion Pharmaceuticals</td><td>ACT-451840</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Combination</td><td>Policlinique Medicale Universitaire</td><td>Cotrifazid</td><td>Rifampicin and Cotrimoxazole and Isoniazid </td><td>Phase 2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Combination</td><td>Pfizer</td><td>Azithromycin and Chloroquine</td><td>AZCQ; Zithromax and Chloroquine; Zmax and Chloroquine</td><td>Phase 3</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Combination</td><td>Holley Pharm; Holleykin Pharm</td><td>Eurartesim; Artekin&trade;; Duocotecxin&trade;</td><td>Dihydroartemisinin and Piperaquine</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Combination</td><td>Jomma Pharma</td><td>Fosmidomycin-Piperaquine</td><td>&nbsp;</td><td>Phase 2a</td><td>Ongoing, Not Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Combination</td><td>Jomaa Pharma</td><td>Fosclin; JP-01</td><td>Clindamycin and Fosmidomycin</td><td>Phase 3</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>DNA Synthesis Inhibitor</td><td>Pfizer; Mission Pharmacal</td><td>Tindamax&trade;; Fasigyn&trade;; Simplotan&trade;</td><td>Tinidazole</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl13_Link" href="CompoundDetails.aspx?AIDSNO=007940">007940</a>                             
                            </td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Dihydroorotate dehydrogenase inhibitor</td><td>Medicines for Malaria Venture; Takeda</td><td>DSM265</td><td>&nbsp;</td><td>Phase 2a</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Electron Transport Chain Inhibitor</td><td>GlaxoSmithKline</td><td>GSK932121</td><td>&nbsp;</td><td>Phase 1</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>Plasmodium spp.</td><td>Hemozoin Inhibitor</td><td>Tulane University Health Center</td><td>AQ-13</td><td>Aminoquinoline</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl16_Link" href="CompoundDetails.aspx?AIDSNO=532415">532415</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Plasmodium spp.</td><td>Inhibition of Phospholipid Metabolism </td><td>Sanofi-Aventis</td><td>SAR97276A</td><td>1,12-Bis[5-(2-hydroxyethyl)-4-methyl-1,3-thiazol-3-ium]dodecane dibromide</td><td>Phase 2</td><td>Terminated</td><td>&nbsp;</td>
	</tr><tr>
		<td>Toxoplasma gondii</td><td>Protein Synthesis Inhibitor (binds to 50S subunit)</td><td>Rhone-Poulenc Pharmaceuticals</td><td>Rovamycine&trade;</td><td>Spiramycin</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl18_Link" href="CompoundDetails.aspx?AIDSNO=007350">007350</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Trypanosoma cruzi</td><td>Membrane Disruptor</td><td>Drugs for Neglected Diseases; Eisai Co., Ltd.</td><td>E1224</td><td>Prodrug of Ravuconazole</td><td>Phase 2</td><td>Unknown</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=057176">057176</a>                             
                            </td>
	</tr>
</table>
            </div>
            <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
            </div>            
        </div>            		
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AB8118F2" /></form>
</body>
</html>
