

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - HIV Drugs in Development Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DrugDevelopmentTB.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6tjUjKzs2Ez3/M1MUIJLmZA9bBwX8eutyhxRTAcdX0QtYO+LwQ6k3Xvy+WtZCvqrxD51/w/7GEbk9cRipv132lATB04f/noz35gYGHNMi0lGequjn6eVRd7BC+u8OUabR9gOPIVRfuPHAVx/mU9XcMxa7nD1zWwDtXUINFT1sEZCbC2ubzhPY/QJ2cVr9n4TNi8eLVj9T8r6+Z172lYAv0xe6I3euoWanSM4eObLr/gkO1TkllynPJsi1B479BUtf7qaGvugCbIJlbnJ2ojoOdEi+orptXDIS0JuYwzilyuHnvNfg6uszZWSjl7UKaiYi3WdfaTJ1bHGZjeY6y9noRYntLtGU9ZKFJ48VwuHa7UVIdjRCpRKsLy5rluoHt3fZvqVZAXEIYgrytX1IpDIF7kWnDe6jcW0DjW6sLQZ4mWkGcVIcHqBtjIh6kX/Sd7PrrSEFnL3CDRqasapbZvnPvt+pcHdcouWgLW4uRq1uo7XmnHf1cvEdt9gL/PhQzUe3Dizfw5eWfQ0jW2Db5YFYS05JoKbgenT8pNrSbZZcNWYjety7PK24ivYBuMF/hF/tTJNtA/+FgiIHfcRy4fBVtqb2s499g5+GJeYxs1Zqj6A0+7htF2W3j5Jxsjacf8g47M/QU3zClCDa3Clz43m9dL0XMX30tnR2cn5/csHTkA2V5mnk+HABu4djIhbhzd5mV5eL528lNAnO1Ic11mYx3Bczh5jJ160qLQ06EcAQrUgJh93nx1Ml66Ih9RsmnZjC4020KrgAW7+6gbDhHTB3Ldk6oQxHVYq+jzPY4coj9GQaA72hccgngw9sYSldEUdrd/zgdoqXdfpj2MxcOncXHd1HV8iTdso+SKMYBf8s3QyoMxtoTZgdwB4D1cokPzwB5MYh7VPi8Ha8TfRtZnDofPLXZ/5dIY/2YrfiuFjgWWbeXAdutT15pyKTpNWRkTpJ23Foc1Xz+lVeL8zaQPv+gKJ8QBMYD8FY1gSi5hCeqd+ZlxVaOlKIHhCJTwZwVnqjNSA6wsUoByzOX668ifbNQ3Kr0bynLGNdfxEqZvFnmX81imxB72wMnG+N7vUZp/f5Tq2QLT8OL+YEhsIoys8FGFQp8UD8oXAGA5uKFc/GdFuiJkmleCK7gpNZtp58bINAkR3L+dxOKkpy8nDullKFMikzet4NN4TDdU4ufXi3QdvE+BGKL69jqMBWXHVCGn8aljrH+4VDyt681EVXaES5VsOcOB2pzcX25gwRiDcTZFk8Ikjei/wDl5Jkl4Cv3/scxEnCn8u6A3R0gN7BwauFfN0JFPU+VIzAKyBW89fJNK5rOzBlm3Vim/4E3Qt0AlbRCcLMLY/NriTEGdbgwmC+J5K2VBF/n4J3T7EYl6Ca/XeoJ5kQ0H996VkEqiNl8KlSTg63ZZFUWmVlc56P7B+K0a21m+ikIKX7TTTAaCFgoqADvHbHQ0BqvJKQAXd7gP2fy5tSEUkm5pQ3OeQKDnFgm4bD7DQKNCl7vzG8rtJKssez1zPEaYndIhp7y8YzIDylh2ps2WeR7W1LL5ypNYrxfmeOOQ62xKoT7XdCsy3b0u4Hzo1UezvVfdV1FIprd7J6s4ByCAda6V2aFn5+Zh4be7TGM9ptyzxPdnaff7l7d3RQo8mf/hTp0Iix2amQbMNhk/MPAuntAEh2z08IlpC91ca5v5mCBkXRza+PbENZV2n5LXwIUv/HamG4puo71ju1sZKaM7/xptC6/qdS86ZA3TofI+TAZOVM3lPZ1eYdCXsj4kWZJC9BFddwU+qvAGdw1fomvwqy7QncX8Ape79pFgiNbOa4x6KFzYzChnP0LN0uUhsyRiPukATcWHDl3NGTESy94vrmsjd8wVqggFQJHfJDxDtcDhNmJsGC7yyWl1JV83cnLC78ql19dNWHXG+WxAnSTkC2gstTR2/Uli7RtPn5kYBzSjXbFQ3Htkb1X7sHMKN6wzwm8Ki2tJTg9yc3SIkRQffZ9SkgYd5JUO4RSzuMzWjfJ4vYWjdWTzLCDFYIiF6er7FBxO26t6AoOusuwnZ0prhiqEy84yQTpg4hopzNTclOd4PzdWF/wFRebWaAyK0jMK8HGTL2xHLQ5aA+toRC69yXWxYTupYHSYQwrXVNQFjtsw4UGTETKXgyXy5NIv7P5Nj2Y8bUwsJkte8rH0hFonxRvtv0Vt9Lb11rUwwwTfklTC2C/EFHzZwmfBlXS7Rpxxdn+wsjUsVT9ocT4IWPalu7OEDeBybLu6pFjd0P5T2BQakgjlM7Lk9TDnY/1807QMOxiuvIMKHHxMORPBCw8jZ202o4RoOMJ/JDm61VpPem//KbOjRQERDUfidUCalwoho9oZ8y8LDm3yBuy5IfqavYYk1DNi7ebKmeTFUkY4nwyUI6LmGMXS72orC9zLtuTd7Fh/29MYPOAG2lbqwBs2Ssci5J1i2nAb3/eLoLvRFQ5wKaGNrN5PlrlCZm53FX4RtR72SstCd7ClfzwN8ciNqYBkoRXibb4mhLpKMkiPXPf/OdLr/FmNtVZFTEEKqtUkBBCOuZDDcK8VJWv9+zGQWjpCsp3p1mblxTridycJ1ogCsPdoT7YIDO/zjBBeW4kj0fMOMAJjAn4NtPs54u7SQF1j3et4lCh6p9r1DuxWNVD3KzD5d6tA0Ht77SrkvUdEV8om95gwYDiG6ZxfRnJMUZAJp25v+9ulaVoyYL93Bovv5pyeKOo2XhtfVmhewirUmyNx9R3ZPsQAnD9/7dOGYJOcTbON1/7C/5q22gjwFTSntAymz5fObud0vE8vfJ5WOIQfTdpgg0yaJd22fCwRMKx7RFU9vGbMeyLMj1TFfbIycikf/T+EH1RqJg1ag52y1ng9Nu2kMJKZHphWiuNbL7L5X0JaKMnuqA2QOTtD2BREOj+6zDtP+P3qtEaMo5Aya+UQvT6AAVHcHCAXIEoDi7UsVZf1Etj2LD3UsnfDHwfFMXuobzQL0DJhlSjJ3ZCEFmEjiw+tV7jntX3R5XWZBHX6SJGND6Bu58r91Jnu3Atcip43otLnFBfanVFRN5NFpd0fl5r6udSrEUbS9vsn6kWsCZUwJAYGajmgZjD+wbcXchSazVpGneKLP53YvnFdeny6nfMXAzr5zAUeEmrFM/QD7Ncu65/MBnIBuHGto1qlgCMKCmeCe4Mp2M6UaK0srYw8zpoQX1QmJuEAZ0qk9LUNNZKCkj1IzCFKkkiBnYlaseQtpSLl4/Tk+UxCOPpYA7WxHTBA9fEO1sdGfc7Zz7cLaVvtsG1lojKwUX3CMwwkYUI2iZnSoqnZZqEkqFUfWuVqpFGakoTc7BKCqYCYcpxtYCZdJQ1io/zKrMgfiIP/s3zWbsRtXh8VhPj6eLr/3enDYnOkEmYF4zbCYly87gt1kBsOQxIXGIee4Lgm6wzkw28MqCtc9nMkKk4NcJVL4J/nPHlakqgyaSn+zB0cv/vZJh0qbfTsx9T1kKQcf8DBfkZV0A9Ks1vBjjjdP3HbfaghqYL4uyiDFQHlW8g0/Xk7T1xwTKKwDzqQUG+pJEz1KD8m51NJbKI5mOzNBBxIyFMG/TiRydUka1oDjRN+5a1+uvTak/KCplPmnW1Vcr2jtB9gNwAQ1wFOkWMebeMb8Rx9S9I2/M6Rz7TNj3tc1bdhC+C6CODOyLUFvixqvESTOk7bxtuTZ8rmNTe5XYh4cfZEOE01I9e+bH+oUWyrD4bv5qP0OJXRadq3vvtzsGt+cKYcYI+CMR6MxvOPh0JQBGfsPyaNWBTPCIS2AoTcdBpTTZE8+foAyQehxcI3gDc9GqMZW5Xr5z9/+DvdUwMISVD33fsQP3dbKS7vv2u0bSkT9W4/2cT9M/gKMGYPV7kTqYTE+TzpZpHg/BwdQp4eoFp51KUngY4fsfZuzltH4aFPqHfmkcmoKvHrnyouj732IstR4H+kD3HinBsaMWdIlK1fhtIg51LjJnR+z0OrhFGPcb5x3WNZj4Bl6oR571ClP9LHLdLEAMW/WbPDgGU1ZXc++L23pqNRBmXkPpp1lHgD+RaROyC2IslatsTMfgsUfvyjCiQuLBO8X9ulq4L/bGBrjUfjqabvKYQOCJ6duDdWzSmie8aOzSLkW2SLkuW1NtRxcDXQeB3/f+jU1SoCMOIkxM+cXNzszrKkVRXn2OAjAyewsNN3mDVg/0bz8tooPjqTlxWU9Ceprb9XwJgjbP9PVwSMaDBKk1Vk8jY6fDvOi/G6xXoJ2rVeXmZZk51NXHJ0RYxWHdhscy5IfkaPwim6JrOMvgbu7HjSmGtQVFKASvIo4kI2UImxbE9ZlyFIDe6xljS2MrH7EO7q8lm12zQjNIht5RtAXnDgcrpFbGMcOWvZk+iAF9GdGOxaabV50J+97Gsp9cn1Za90wpR9fU0LFyZG2uoxbWvV5g8XGgEnas/FisDB4pT0xKHyRGiwnCbzP9uqpHW0z0NsH+0yxtvQUGZg4nVhGQOqb0Jt2od7DSbjcmzf38IDHK9Qeyu7Yp7WOxetRxNFctf2HPEkAQV3Vb3rc3lFWyyHEJRh5i/0Tr08dYCRG5y/BGOm5nSbNpwVgSFg2OKMJKb0PhNpsXhkw8fdTjiMTpHJEI3nkR5+ZAYQetZVcQjmw3F1twmGInEkX3kjBtWyoDV8EaXaAqZYoZrMabKVlf2Ym0EgMgIuPoj+QTF1v8gU44Z7xyKbX1LPqmz5mZpfUptxiHHqOn1zplyWcJvBSx1qxF5/zDe3W326GFSKPC6bTauTv0JBhwOcNqsTuwvz16ouE0X8zcF9TT4wiRBmdcF3rml4XrpC8wyP5/DWoSdyYKrbKgh7VIyK7BQPjeXeUcQ3/kEdwQMLYxNzBbmYbXPE/Z2WDPof29y0X13taqRbTEgkl5BzMlTtkuzg6JaBKDhZNNiiTGD4aQC5ZpvvXZseADlKNtdfzP8ZUllNDkQnI9JdWtDGtiRKCig6ZrmpT7G3oMaw1HTdkn0TgCK7f17GA6gcHgYp97CWpj59KHX9DVGFvXhpi6pxqGeMeq/dXApl1Y7oyF8GGQnngWb95b8UaU0BWYUDJBEJ14Cqhw/Pn20und5pOOOtDnDoOSElgmGWdUkjdr7wCiYEA5h709/+3Au6eYs+WPcDnCzKY8rxKCi/aEf5tTbGnThQQJt+9OKBncd9+P+fUu6qf5WBOh+rdn7LzViLAfnfsdFAf629EF1K0ZfOX8YcLJpHt8lBYx5bOdzepzXEgJZBBzPZfZYaHDKZbAGF+SpO/+44RYw3Ko1Oxb6dLv+ljvO/4qA8eCbmkut2rmGgt/ix+Koo1AqYH2wxP/iVKUMPSXdgIpVSm0jqJ1XIUH1qG0uxOYjFCK6oSDmC4Agp8MklHSlLneV+fTCXHW75G3XWkPeInKZwoUed+Pj8/25+1Qiu1aKcxowJitNQ+A4GMSJhD5P7/PBJYLPf8As25nt2VCojOw7aIHFQAtI50TUtcgPGZWpjsAuAvRf/w6DPu90V2/+vQLzS+kKhsODYHlWkmV1vc/RvfZYsdsWDXCsjH2X/YbB8WmgJvqqAEOD0Lnho4nPsPxr0fmqhOOuE/iwko66dTW6+xKjuPUaMKwtpoGxSXdvmNkVKsJ8Bq9ncdY7IHsQAjAYbZ2X21nYd6LYyGefU1ujyKPJB07Q6ya7kT0EuS1zrigtk1IMq1x1SWy35cGmQe2nLHZJj5/GGHBi4gZNMZh+PYBqrHKpQ5Hi8x3HWqGi3omM/3kHoDiNtVYTj2X2AX9k8Ztzoyjr2aLpxQ1SM8SqS6QNPccs7UeChCmDI5nhHx9Ko8y7r1fE7b8SvCqsciCyiKVrEABC3qZ+Z+VkxWTWnuM0x6QwU+Bo1BedrvYUBANuSszNeXapP2A8HVhgWF+vWieckX3V7R+6iVSmqD+c26T1J6yDHh2U9/19s7krKTxM1ak4HPBuKG/AvwT5cObZLBOEHeyCfvhqQeJpp+62MrQiqYqR+axi5WrMKRTsfKXKg68XJWUdBBG9tQNAP89ajkTtBxUPQ2zkX6LU9W8SnLE26CWSkMHl/gcj9BB4ET8m5QWRosYOtHdthGzy+iquguPoEbuzVWq2dDEJUT1GF6sf22BpxJgmKJCp12JmU2Z8QfB/Uf4cV7HkrCqoH6RwZNM1dq+q3X4C2LISdCTdjw8bPq5YPdtxrKPSqAyXU9AvTN5pCDu20GJiTt48HfhrtgvVkJ1NqlAKYlNs1BKHUcfH/BwGj/IwcQWm8iwDaLnwDFCf9mDFJnQ8+43ABcF8ZJi0lTGU07NFMkS35qJ31FE1/FEToxp75VljmnLGYzjPdj7gHnxesHusy8BahMrL6Hys7qAaGPJFIgxYNYlT72vKopX7Oko9CJ4zgv6L5LIdRN77P3YYK4VNRc8xrYfZShnJDjaKJR6eYX78+ILO8nP6uzsgyJLdXSrQnC2HoQAcTUS60dV/Cp6dRnHeSHp2Qe9omisezPTzOTu+8twp0R0JR7T9pCo9r+Afxur2vLaLH2kY7MBdccz9emCsxlkd7t0FXiYTQRm1dl05xqHuydoPGl1sOqbHb+WtBo0zYb8qOIX8ycO7Z4I1wTBEgVbnBtA39Ox7XsrN0MLA49Ktyj3yXfW9OTQU8cVy73u0PJ4Oickirz9BZXV0GRWn9bP6YEOFs6+MTTHnFgBz4g5lgTedLtyUwA53hBvlo9RAS+h230aOHMDYhR3LgDvy/wLu15uJDMVrxMyqSF8GdavghoeEy8CHZFPL0d0DnGO9i1mWZCuWAx5QIoZWLcLYO39OYxBhKDc8SXMRVvP+oJJEfE+27z54PyXFKDJUbN/xg+DmBgu9XDowZnQ7GMkCsuH7ELqFUW0Y3uda05HxEtP3G55M4/xNOE+p9ffY9WYvqWmsE6uDcH/J3ZvIExG5AmVrG6GZ6lFArp9HPAu6Cwe138NpHQ86Vl5hUwnRf85qE3yrOA/bh1KTWEwLbdKcBrxUg/VR5KRr/lc3bBbc2eahT6hNjNz8JvzvJ+WBsONu5WMYziVpfavTdxjDm2vdTI5FRhfh8HvenWTylhatCS9TmIZvsB4OGfUhzKu3+a8rTpCuO7JFUAlWJI3KJ+8Zoyis/IP2naT9h4OOvSYJj0Jtv0l/QUPC5Moo7J+OhjW5TpX/0NQ8MKZsuD5HI4EA7oYwwwtqYPAAnBcaVNVLZfDUrHE4TnnyT/riih8pF1loBkeWsw4TrdC2/wQ9VUnxUPYod4odeFB+WDY6H+w51IRnxGihxsZ2dsbcNvsvpQyQZ48xzf8tfbG054YOHnRKW1ge3Kb6xONv4Puy907qULGz8rFpFp8yff0j9mvqQDv22FEt5P" />

<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="memo/Highlights.aspx">Announcements</a> <img src="images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memo/memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	            
        
<div id="content">	
	<div id="description">
		<h1>TB Drugs in Development</h1>
		<br />
		<div id="drugsindev">
            <div id="druglist">
                <table cellspacing="0" rules="all" bordercolor="#115169" border="1" id="ctl00__mainContent_dgDrugDev">
	<tr align="center" valign="middle" bgcolor="#396F9D">
		<td class="ColDrugClass"><font color="White" size="3"><b>Drug Class</b></font></td><td class="ColDrugCompany"><font color="White" size="3"><b>Company</b></font></td><td class="ColDrugName"><font color="White" size="3"><b>Drug Name</b></font></td><td class="ColDrugAltName"><font color="White" size="3"><b>Alt Name</b></font></td><td class="ColDrugStatus"><font color="White" size="3"><b>Drug Status</b></font></td><td class="ColDrugNotes"><font color="White" size="3"><b>Notes</b></font></td><td class="ColAIDSNo"><font color="White" size="3"><b>AIDS#</b></font></td>
	</tr><tr>
		<td>Cell Wall Synthesis Inhibitor</td><td>University Medical Center Groningen</td><td>Invanz</td><td>Ertapenem</td><td>Phase 2</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cell Wall Synthesis Inhibitor</td><td>Asubio Pharma</td><td>Farom&trade;</td><td>Faropenem</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl03_Link" href="CompoundDetails.aspx?AIDSNO=545326">545326</a>                             
                            </td>
	</tr><tr>
		<td>Cell Wall Synthesis Inhibitor</td><td>Johns Hopkins University </td><td>Merrem&trade;</td><td>Meropenem; SM-7338</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl04_Link" href="CompoundDetails.aspx?AIDSNO=007788">007788</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cell Wall Synthesis Inhibitor</td><td>Nearmedic Plus LLC; OCT LLC</td><td>PBTZ169</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl05_Link" href="CompoundDetails.aspx?AIDSNO=573761">573761</a>                             
                            </td>
	</tr><tr>
		<td>Cell Wall Synthesis Inhibitor</td><td>Sequella</td><td>SQ-109</td><td>&nbsp;</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl06_Link" href="CompoundDetails.aspx?AIDSNO=207396">207396</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cell Wall Synthesis Inhibitor</td><td>Global Alliance for TB Drug Development</td><td>TBA-7371</td><td>&nbsp;</td><td>Phase 1</td><td>Recruiting</td><td>&nbsp;</td>
	</tr><tr>
		<td>Cell wall synthesis inhibitor</td><td>Global Alliance for TB Drug Development; Novartis</td><td>TBA-354</td><td>&nbsp;</td><td>Phase 1</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl08_Link" href="CompoundDetails.aspx?AIDSNO=510946">510946</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Cytochrome bc1 Complex Inhibitor</td><td>Qurient</td><td>Q-203</td><td>&nbsp;</td><td>Phase 1</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr>
		<td>DNA Replication Inhibitor/TB Cell division inhibitor</td><td>Daiichi Pharm; Sanofi-Aventis; Ortho-Mcneil</td><td> Levaquin&trade;; Tavanic&trade;</td><td>S-(-)-Ofloxacin; DR-3355; RWJ-25213; Cravit; L-Ofloxacin;  Levofloxacin; LVX</td><td>Phase 3</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl10_Link" href="CompoundDetails.aspx?AIDSNO=002307">002307</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>DNA Replication Inhibitor/TB Cell division inhibitor</td><td>Bayer; Schering-Plough </td><td> Avelox&trade;</td><td>Moxifloxacin; MXFX; BAY 12-8039; Actira; Avelox</td><td>Phase 3</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl11_Link" href="CompoundDetails.aspx?AIDSNO=070017">070017</a>                             
                            </td>
	</tr><tr>
		<td>DNA Replication Inhibitor/TB Cell division inhibitor</td><td>Bayer</td><td>Flagyl&trade;</td><td>Metronidazole; Bayer 5360;  Clont; Danizol; Gineflavir; Metric 21;  Trichazol; Trichopal; Trivazol; Vagilen</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl12_Link" href="CompoundDetails.aspx?AIDSNO=007953">007953</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>DNA Replication Inhibitor/TB Cell division inhibitor</td><td>Parke-Davis; Kyorin Pharm; Pfizer</td><td>Tequin&trade;</td><td>Gatifloxacin; GTFX; BMS-206584; AM-1155; CG 5501; PD-135432; GAT</td><td>Phase 1/2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl13_Link" href="CompoundDetails.aspx?AIDSNO=044913">044913</a>                             
                            </td>
	</tr><tr>
		<td>Intracellular NO Release</td><td>Pathogenesis Corp; Chiron</td><td>Pretomanid</td><td>PA-824</td><td>Phase 3</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl14_Link" href="CompoundDetails.aspx?AIDSNO=007331">007331</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Leucyl-tRNA Synthetase (LeuRS) Inhibitor</td><td>GlaxoSmithKline</td><td> GSK3036656</td><td>GSK070</td><td>Phase 1</td><td>Completed</td><td>&nbsp;</td>
	</tr><tr>
		<td>Monoclonal Antibody</td><td>National University, Singapore</td><td>Pascolizumab</td><td>Anti-IL-4 Antibody</td><td>Phase 2</td><td>Ongoing</td><td>&nbsp;</td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Mycolic acid biosynthesis inhibitor</td><td>Otsuka Pharm</td><td>Delamanid</td><td>OPC-67683</td><td>Phase 3</td><td>Ongoing</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl17_Link" href="CompoundDetails.aspx?AIDSNO=182259">182259</a>                             
                            </td>
	</tr><tr>
		<td>Protein Synthesis Inhibitor/TB growth Inhibitor</td><td>AstraZeneca</td><td>AZD-5847</td><td>AZD2563; Posizolid</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl18_Link" href="CompoundDetails.aspx?AIDSNO=554767">554767</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Protein Synthesis Inhibitor/TB growth Inhibitor</td><td>Pharmacia & Upjohn, Inc.; Pfizer</td><td>Sutezolid</td><td>PNU-100480; PF-02341272; Oxazolidininone</td><td>Phase 2</td><td>Completed</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl19_Link" href="CompoundDetails.aspx?AIDSNO=045415">045415</a>                             
                            </td>
	</tr><tr>
		<td>Protein Synthesis Inhibitor/TB growth Inhibitor</td><td>LegoChem Biosciences, Inc.</td><td>LCB01-0371</td><td>&nbsp;</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl20_Link" href="CompoundDetails.aspx?AIDSNO=581214">581214</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Protein Synthesis Inhibitor/TB growth Inhibitors</td><td>Pharmacia & Upjohn, Inc.; Pfizer</td><td>Zyvox&trade;</td><td>Linezolid; PNU-100766; LZD</td><td>Phase 3</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl21_Link" href="CompoundDetails.aspx?AIDSNO=070944">070944</a>                             
                            </td>
	</tr><tr>
		<td>Pyruvate-ferredoxin oxidoreductase inhibitor</td><td>Weill Medical College of Cornell University</td><td>Alinia&trade; ;  Nizonide&trade;</td><td>Nitazoxanide</td><td>Phase 2</td><td>Recruiting</td><td>
                                <a id="ctl00__mainContent_dgDrugDev_ctl22_Link" href="CompoundDetails.aspx?AIDSNO=057131">057131</a>                             
                            </td>
	</tr><tr bgcolor="#F2F2F2">
		<td>Unidentified</td><td>Scientific Center for Anti-infectious Drugs, Kazakhstan</td><td>FS-1</td><td>&nbsp;</td><td>Phase 3</td><td>Recruiting</td><td>&nbsp;</td>
	</tr>
</table>
            </div>
        </div>            		
           <div id="drugtblinks">
                <br /><br />
                Some useful sites where additional information on these drugs may be found:
                <br /><br />
                <a href="http://aidsinfo.nih.gov" target="_blank"><img src="images/logo_wht_medium.gif" alt="AIDS Info Gov Logo" border="0" /></a>
                <br /><br />
                <a href="http://www.clinicaltrials.gov" target="_blank"><img src="images/ctgov_big_ttl.gif" alt="Clinical Trials Gov Logo" border="0" /></a>                
          
                <br /><br />
                Other TB links of Interest:
                <br /><br />
                <a href="http://new.tballiance.org/home/home-live.php" target="_blank">Global Alliance for TB Drug Development</a>
                <br />
                <a href="http://www.newtbdrugs.org" target="_blank">Stop TB Partnership>Working Group on New TB Drugs</a>
                <br />
                <a href="http://www.who.int/tb/en/" target="_blank">WHO|Tuberculosis</a>
                <br />
            </div>
    </div>					
</div>    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="SiteMap.aspx">Site Map</a> | 
        	<a href="help/Help.aspx">Help</a> | 
        	<a href="ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B8C35225" /></form>
</body>
</html>
