

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-04-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ll5+jNATHhTSXMhASQbD+fLp0WdHmwiJBIpaTdBTdF4l3pWqahJphSxT0WRaVVV7HzFBlp03UKD9BLlKjKqSlYM018UttZ9LOpzALd6rL3Gc4xZHfTc05viPLOI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="38CC9FC3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 1, 2009 - April 14, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">1)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19364856?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anidulafungin is fungicidal and exerts a variety of post-antifungal effects against Candida albicans, C. glabrata, C. parapsilosis and C. krusei isolates.</a></p>

    <p class="NoSpacing">Nguyen KT, Ta P, Hoang BT, Cheng S, Hao B, Nguyen MH, Clancy CJ.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Apr 13. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19364856 [PubMed - as supplied by publisher]    </p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">2)       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19345718?ordinalpos=90&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of 7,8-diaminopelargonic acid aminotransferase from Mycobacterium tuberculosis by chiral and achiral anologs of its substrate: Biological implications.</a></p>

    <p class="NoSpacing">Mann S, Colliandre L, Labesse G, Ploux O.</p>

    <p class="NoSpacing">Biochimie. 2009 Apr 2. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19345718 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3)       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19343064?ordinalpos=97&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery and antibacterial activity of glabramycin A-C from Neosartorya glabra by an antisense strategy.</a></p>

    <p class="NoSpacing">Jayasuriya H, Zink D, Basilio A, Vicente F, Collado J, Bills G, Goldman ML, Motyl M, Huber J, Dezeny G, Byrne K, Singh SB.</p>

    <p class="NoSpacing">J Antibiot (Tokyo). 2009 Apr 3. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19343064 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">4)       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19299129?ordinalpos=106&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and biological evaluation of new enantiomerically pure azole derivatives as inhibitors of Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Castagnolo D, Radi M, Dessì F, Manetti F, Saddi M, Meleddu R, De Logu A, Botta M.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Apr 15;19(8):2203-5. Epub 2009 Feb 28.</p>

    <p class="NoSpacing">PMID: 19299129 [PubMed - in process]          </p> 

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">5)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19307123?ordinalpos=100&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Comparing anti-HIV, antibacterial, antifungal, micellar, and cytotoxic properties of tricarboxylato dendritic amphiphiles.</a></p>

    <p class="NoSpacing">Macri RV, Karlovská J, Doncel GF, Du X, Maisuria BB, Williams AA, Sugandhi EW, Falkinham JO 3rd, Esker AR, Gandour RD.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Apr 15;17(8):3162-8. Epub 2009 Mar 5.</p>

    <p class="NoSpacing">PMID: 19307123 [PubMed - in process]            </p>     

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
