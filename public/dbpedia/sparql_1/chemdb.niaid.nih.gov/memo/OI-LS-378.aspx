

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-378.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ndi8l8Hm+5aeS0pI9I27sGspxIgsaU/kHCDtes1IoRIiMfQ4btbpXdAfMo0QgvnFC1im5370ohwiyfj4gYoxfnSbMoELkJK6B5Bid8AUNy0+TFfKeNBvjMddYR8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8AD63BDC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-378-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61222   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Antibacterial and antimycobacterial activities of South African Salvia species and isolated compounds from S. chamelaeagnea</p>

    <p>          Kamatou, GPP, Van Vuuren, SF, Van Heerden, FR, Seaman, T, and Viljoen, AM</p>

    <p>          South African Journal of Botany <b>2007</b>.  In Press, Corrected Proof: 372</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XN9-4P18BNF-1/2/d60d84089ac378f84a1bd3085a0b9b3f">http://www.sciencedirect.com/science/article/B7XN9-4P18BNF-1/2/d60d84089ac378f84a1bd3085a0b9b3f</a> </p><br />

    <p>2.     61223   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Synthesis and structure-antituberculosis activity relationship of 1H-indole-2,3-dione derivatives</p>

    <p>          Karali, Nilgun, Gursoy, Aysel, Kandemirli, Fatma, Shvets, Nathaly, Kaynak, FBetul, Ozbey, Suheyla, Kovalishyn, Vasyl, and Dimoglo, Anatholy</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 3316</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NWNCPD-7/2/c381ddc059c0f7e155dac29e4e8322cd">http://www.sciencedirect.com/science/article/B6TF8-4NWNCPD-7/2/c381ddc059c0f7e155dac29e4e8322cd</a> </p><br />

    <p>3.     61224   OI-LS-378; WOS-OI-6/29/2007</p>

    <p class="memofmt1-2">          Cryptosporidium Parvum: Identification and Characterization of an Acid Phosphatase</p>

    <p>          Aguirre-Garcia, M. and Okhuysen, P.</p>

    <p>          Parasitology Research <b>2007</b>.  101(1): 85-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246099600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246099600011</a> </p><br />

    <p>4.     61225   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          In search of cyclooxygenase inhibitors, anti-Mycobacterium tuberculosis and anti-malarial drugs from Thai flora and microbes</p>

    <p>          Gale, George A, Kirtikara, Kanyawim, Pittayakhajonwut, Pattama, Sivichai, Somsak, Thebtaranonth, Yodhathai, Thongpanchang, Chawanee, and Vichai, Vanicha</p>

    <p>          Pharmacology &amp; Therapeutics <b>2007</b>.  In Press, Accepted Manuscript: 746</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TBG-4NS0KJB-1/2/496798be4be230b30ba0c5d88961c023">http://www.sciencedirect.com/science/article/B6TBG-4NS0KJB-1/2/496798be4be230b30ba0c5d88961c023</a> </p><br />

    <p>5.     61226   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of thiazolidine-2-one 1,1-dioxide as inhibitors of Escherichia coli [beta]-ketoacyl-ACP-synthase III (FabH)</p>

    <p>          Alhamadsheh, Mamoun M, Waters, Norman C, Huddler, Donald P, Kreishman-Deitrick, Mara, Florova, Galina, and Reynolds, Kevin A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(4): 879-883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MG5WB2-3/2/159b30b0ade24fc2d9dc664079f2e978">http://www.sciencedirect.com/science/article/B6TF9-4MG5WB2-3/2/159b30b0ade24fc2d9dc664079f2e978</a> </p><br />

    <p>6.     61227   OI-LS-378; WOS-OI-6/29/2007</p>

    <p class="memofmt1-2">          New Rifamycins for the Treatment of Bacterial Infections</p>

    <p>          Combrink, K. and Lynch, A.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(5): 475-485</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400001</a> </p><br />

    <p>7.     61228   OI-LS-378; WOS-OI-6/29/2007</p>

    <p class="memofmt1-2">          Nucleoside Natural Products and Related Analogs With Potential Therapeutic Properties as Antibacterial and Antiviral Agents</p>

    <p>          Ichikawa, S. and Matsuda, A.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(5): 487-498</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400002</a> </p><br />

    <p>8.     61229   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Antifungal dibenzofuran bis(bibenzyl)s from the liverwort Asterella angusta: Reports on Structure Elucidation</p>

    <p>          Qu, Jianbo, Xie, Chunfeng, Guo, Huaifang, Yu, Wentao, and Lou, Hongxiang</p>

    <p>          Phytochemistry  <b>2007</b>.  68(13): 1767-1774</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TH7-4NYJS2N-2/2/17be57f56bc7562335f0c0a3dd7579e1">http://www.sciencedirect.com/science/article/B6TH7-4NYJS2N-2/2/17be57f56bc7562335f0c0a3dd7579e1</a> </p><br />

    <p>9.     61230   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          The antifungal activity of twenty-four southern African Combretum species (Combretaceae)</p>

    <p>          Masoko, P, Picard, J, and Eloff, JN</p>

    <p>          South African Journal of Botany <b>2007</b>.  73(2): 173-183</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XN9-4N9N07B-1/2/25898896b786e8117684d1f3aba22551">http://www.sciencedirect.com/science/article/B7XN9-4N9N07B-1/2/25898896b786e8117684d1f3aba22551</a> </p><br />

    <p>10.   61231   OI-LS-378; WOS-OI-6/29/2007</p>

    <p class="memofmt1-2">          Inhibition of Mycobacterial Arylamine N-Acetyltransferase Contributes to Anti-Mycobacterial Activity of Warburgia Salutaris</p>

    <p>          Madikane, V. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(10): 3579-3586</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246257900029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246257900029</a> </p><br />

    <p>11.   61232   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Hazel (Corylus avellana L.) leaves as source of antimicrobial and antioxidative compounds</p>

    <p>          Oliveira, Ivo, Sousa, Anabela, Valentao, Patricia, Andrade, Paula B, Ferreira, Isabel CFR, Ferreres, Federico, Bento, Albino, Seabra, Rosa, Estevinho, Leticia, and Pereira, Jose Alberto</p>

    <p>          Food Chemistry  <b>2007</b>.  In Press, Corrected Proof: 3853</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T6R-4NSX08G-6/2/29c2491aebe93d480d54bea2f0f3cddc">http://www.sciencedirect.com/science/article/B6T6R-4NSX08G-6/2/29c2491aebe93d480d54bea2f0f3cddc</a> </p><br />

    <p>12.   61233   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Development of Carboxylic Acid Replacements in Indole-N Acetamide Inhibitors of Hepatitis C Virus NS5B Polymerase</p>

    <p>          Stansfield, Ian, Pompei, Marco, Conte, Immacolata, Ercolani, Caterina, Migliaccio, Giovanni, Jairaj, Mark, Giuliano, Claudio, Rowley, Michael, and Narjes, Frank</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4P4NPFH-6/2/45a70b2d9fa76d3e3af5042f380690fc">http://www.sciencedirect.com/science/article/B6TF9-4P4NPFH-6/2/45a70b2d9fa76d3e3af5042f380690fc</a> </p><br />
    <br clear="all">

    <p>13.   61234   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Interferon-based treatment of chronic hepatitis C: Interferons - 1957-2007 - Interferons: from discovery to mechanism of action and clinical applications</p>

    <p>          Souvignet, Claude, Lejeune, Olivier, and Trepo, Christian</p>

    <p>          Biochimie <b>2007</b>.  89(6-7): 894-898</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VRJ-4NNWCMJ-4/2/70edeb7d24f8eefe84f0d2cfc6923d3a">http://www.sciencedirect.com/science/article/B6VRJ-4NNWCMJ-4/2/70edeb7d24f8eefe84f0d2cfc6923d3a</a> </p><br />

    <p>14.   61235   OI-LS-378; EMBASE-OI-7/9/2007</p>

    <p class="memofmt1-2">          Inhibition of an Innate Antiviral Response by Human Cytomegalovirus UL97 Kinase is Antagonized by Maribavir</p>

    <p>          Prichard, Mark, Daily, Shannon, Perry, Amie, Hartline, Caroll, and Kern, Earl</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A41-A42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-18/2/0e602eba053cabb191719c14c1fa138f">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-18/2/0e602eba053cabb191719c14c1fa138f</a> </p><br />

    <p>15.   61236   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Hepatitis C Virus Proteins</p>

    <p>          Dubuisson, J.</p>

    <p>          World Journal of Gastroenterology <b>2007</b>.  13(17): 2406-2415</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600003</a> </p><br />

    <p>16.   61237   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Cell Culture Systems for the Hepatitis C Virus</p>

    <p>          Duverlie, G. and Wychowski, C.</p>

    <p>          World Journal of Gastroenterology <b>2007</b>.  13(17): 2442-2445</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600007</a> </p><br />

    <p>17.   61238   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          A Computational Model of the Inhibition of Mycobacterium Tuberculosis Atpase by a New Drug Candidate R207910</p>

    <p>          De Jonge, M. <i>et al.</i></p>

    <p>          Proteins-Structure Function and Bioinformatics <b>2007</b>.  67(4 ): 971-980</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246415700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246415700017</a> </p><br />

    <p>18.   61239   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Antimycobacterial Activity of Lichens</p>

    <p>          Gupta, V. <i>et al.</i></p>

    <p>          Pharmaceutical Biology <b>2007</b>.  45(3): 200-204</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246442700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246442700006</a> </p><br />

    <p>19.   61240   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Synthesis and Studies of Catechol-Containing Mycobactin S and T Analogs</p>

    <p>          Walz, A., Mollmann, U., and Miller, M.</p>

    <p>          Organic &amp; Biomolecular Chemistry <b>2007</b>.  5(10): 1621-1628</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247005700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247005700029</a> </p><br />

    <p>20.   61241   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Inhibition of Intracellular Hepatitis C Virus Replication by the Pkr Pathway</p>

    <p>          Chang, J. <i>et al.</i></p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A20 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320300062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320300062</a> </p><br />

    <p>21.   61242   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Interferon-Beta Is Activated by Hepatitis C Virus Ns5b and Inhibited by Ns4a, Ns4b, and Ns5a</p>

    <p>          Moriyama, M., Kato, N., and Otsuka, M.</p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A21 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320300064">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320300064</a> </p><br />

    <p>22.   61243   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          The Interferon Inducing Pathways and the Hepatitis C Virus</p>

    <p>          Meurs, E. and Breiman, A.</p>

    <p>          World Journal of Gastroenterology <b>2007</b>.  13(17): 2446-2454</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246933600008</a> </p><br />

    <p>23.   61244   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Bone Morphogenetic Protein-7 and Interferon-Alpha Synergistically Suppress Hepatitis C Virus Replicon</p>

    <p>          Sakamoto, N. <i>et al.</i></p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  357(2): 467-473</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246253700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246253700023</a> </p><br />

    <p>24.   61245   OI-LS-378; WOS-OI-7/6/2007</p>

    <p class="memofmt1-2">          Flying Under the Radar: the Immunobiology of Hepatitis C</p>

    <p>          Dustin, L. and Rice, C.</p>

    <p>          Annual Review of Immunology <b>2007</b>.  25: 71-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246437100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246437100004</a> </p><br />

    <p>25.   61246   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Biocidal effect of bleach on Mycobacterium tuberculosis: a safety measure</p>

    <p>          Githui, WA, Matu, SW, Tunge, N, and Juma, E</p>

    <p>          Int J Tuberc Lung Dis <b>2007</b>.  11(7): 798-802</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17609057&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17609057&amp;dopt=abstract</a> </p><br />

    <p>26.   61247   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          [1,2,3]Triazolo[4,5-h]quinolones. A new class of potent antitubercular agents against multidrug resistant Mycobacterium tuberculosis strains</p>

    <p>          Carta, A, Palomba, M, Paglietti, G, Molicotti, P, Paglietti, B, Cannas, S, and Zanetti, S</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17604166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17604166&amp;dopt=abstract</a> </p><br />

    <p>27.   61248   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Antimicrobial resistance prevention initiative--an update: proceedings of an expert panel on resistance</p>

    <p>          Moellering, RC Jr, Graybill, JR, McGowan, JE Jr, and Corey, L</p>

    <p>          Am J Med <b>2007</b>.  120(7): S4-25; quiz S26-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602911&amp;dopt=abstract</a> </p><br />

    <p>28.   61249   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Growth Inhibition of Mycobacterium bovis, Mycobacterium tuberculosis and Mycobacterium avium In Vitro: Effect of 1-beta-d-2&#39;-Arabinofuranosyl and 1-(2&#39;-Deoxy-2&#39;-fluoro-beta-d-2&#39;-ribofuranosyl) Pyrimidine Nucleoside Analogs</p>

    <p>          Johar, M, Manning, T, Tse, C, Desroches, N, Agrawal, B, Kunimoto, DY, and Kumar, R</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602465&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602465&amp;dopt=abstract</a> </p><br />

    <p>29.   61250   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Probing mechanisms of resistance to the tuberculosis drug isoniazid: Conformational changes caused by inhibition of InhA, the enoyl reductase from Mycobacterium tuberculosis</p>

    <p>          Kruh, NA, Rawat, R, Ruzsicska, BP, and Tonge, PJ</p>

    <p>          Protein Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600151&amp;dopt=abstract</a> </p><br />

    <p>30.   61251   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Ribavirin Up-Regulates the Activity of Double-Stranded RNA-Activated Protein Kinase and Enhances the Action of Interferon- alpha against Hepatitis C Virus</p>

    <p>          Liu, WL, Su, WC, Cheng, CW, Hwang, LH, Wang, CC, Chen, HL, Chen, DS, and Lai, MY</p>

    <p>          J Infect Dis <b>2007</b>.  196(3): 425-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597457&amp;dopt=abstract</a> </p><br />

    <p>31.   61252   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          High resolution crystal structures of Mycobacterium tuberculosis adenosine kinase: Insights into the mechanism and specificity of this novel prokaryotic enzyme</p>

    <p>          Reddy, MC, Palaninathan, SK, Shetty, ND, Owen, JL, Watson, MD, and Sacchettini, JC</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597075&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597075&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>32.   61253   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Functional characterization by genetic complementation of aroB-encoded dehydroquinate synthase from Mycobacterium tuberculosis H37Rv and its heterologous expression and purification</p>

    <p>          de Mendonca, JD, Ely, F, Palma, MS, Frazzon, J, Basso, LA, and Santos, DS</p>

    <p>          J Bacteriol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17586643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17586643&amp;dopt=abstract</a> </p><br />

    <p>33.   61254   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Synthesis and chemical characterisation of target identification reagents based on an inhibitor of human cell invasion by the parasite Toxoplasma gondii</p>

    <p>          Evans, KM, Haraldsen, JD, Pearson, RJ, Slawin, AM, Ward, GE, and Westwood, NJ</p>

    <p>          Org Biomol Chem <b>2007</b>.  5(13): 2063-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17581649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17581649&amp;dopt=abstract</a> </p><br />

    <p>34.   61255   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          LspA-independent action of globomycin on Mycobacterium tuberculosis</p>

    <p>          Banaiee, N, Jacobs, WR, and Ernst, JD</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17579235&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17579235&amp;dopt=abstract</a> </p><br />

    <p>35.   61256   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Cation induced differential effect on structural and functional properties of Mycobacterium tuberculosis alpha-isopropylmalate synthase</p>

    <p>          Singh, K and Bhakuni, V</p>

    <p>          BMC Struct Biol <b>2007</b>.  7(1): 39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17577419&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17577419&amp;dopt=abstract</a> </p><br />

    <p>36.   61257   OI-LS-378; PUBMED-OI-7/9/2007</p>

    <p class="memofmt1-2">          Host factors involved in the replication of hepatitis C virus</p>

    <p>          Moriishi, K and Matsuura, Y</p>

    <p>          Rev Med Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17563922&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17563922&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
