

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-07-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ysRjCqv7G/Rbx4dTKG0lHlCeenpGiw/pcyfWtheVyK9J5jSvkbHdZx8QhWwtebjsZH88xgc9UosDksAevSax6/p3jcTxQDGlinosgEahCw/XQkCCjKPZwy4Oes4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="02EA548B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  June 24 - July 7, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19581097?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, antiviral activity and molecular modeling of oxoquinoline derivatives.</a>  Santos FD, Abreu P, Castro HC, Paixão IC, Cirne-Santos CC, Giongo V, Barbosa JE, Simonetti BR, Garrido V, Bou-Habib DC, Silva DD, Batalha PN, Temerozo JR, Souza TM, Nogueira CM, Cunha AC, Rodrigues CR, Ferreira VF, de Souza MC.  Bioorg Med Chem. 2009 Jun 24. [Epub ahead of print]  PMID: 19581097</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19565596?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Multivalent Manno-Glyconanoparticles Inhibit DC-SIGN-Mediated HIV-1 Trans-Infection of Human T Cells.</a>Martínez-Ávila O, Bedoya LM, Marradi M, Clavel C, Alcamí J, Penadés S. Chembiochem. 2009 Jun 29. [Epub ahead of print] No abstract available.  PMID: 19565596</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19559732?ordinalpos=35&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">CD4-BFFI: A Novel, Bifunctional HIV-1 Entry Inhibitor with High and Broad Anti-HIV-1 Potency.</a> Jekle A, Chow E, Kopetzki E, Ji C, Yan MJ, Nguyen R, Sankuratri S, Cammack N, Heilek G. Antiviral Res. 2009 Jun 24. [Epub ahead of print] PMID: 19559732</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19556424?ordinalpos=41&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">IL-27 inhibition of HIV-1 involves an intermediate induction of type I IFN.</a> Greenwell-Wild T, Vazquez N, Jin W, Rangel Z, Munson P, Wahl SM. Blood. 2009 Jun 25. [Epub ahead of print] PMID: 19556424</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19552380?ordinalpos=54&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Concise and Versatile Multicomponent Synthesis of Multisubstituted Polyfunctional Dihydropyrroles.</a> Zhu Q, Jiang H, Li J, Liu S, Xia C, Zhang M. J Comb Chem. 2009 Jun 24. [Epub ahead of print] PMID: 19552380</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19557157?ordinalpos=40&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Epigenetic regulation of HIV-1 latency by cytosine methylation.</a> Kauder SE, Bosque A, Lindqvist A, Planelles V, Verdin E. PLoS Pathog. 2009 Jun;5(6):e1000495. Epub 2009 Jun 26. PMID: 19557157</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19548857?ordinalpos=58&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Rilpivirine: a novel non-nucleoside reverse transcriptase inhibitor.</a> Garvey L, Winston A. Expert Opin Investig Drugs. 2009 Jul;18(7):1035-41. PMID: 19548857</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19527233?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">CCL20/MIP3alpha is a novel anti-HIV-1 molecule of the human female reproductive tract.</a> Ghosh M, Shen Z, Schaefer TM, Fahey JV, Gupta P, Wira CR. Am J Reprod Immunol. 2009 Jul;62(1):60-71. PMID: 19527233</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19364860?ordinalpos=109&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cytotoxicological analysis of a gp120 binding aptamer with cross-clade human immunodeficiency virus type 1 entry inhibition properties: comparison to conventional antiretrovirals.</a> Lopes de Campos WR, Coopusamy D, Morris L, Mayosi BM, Khati M. Antimicrob Agents Chemother. 2009 Jul;53(7):3056-64. Epub 2009 Apr 13. PMID: 19364860</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266747800008">GSH and analogs in antiviral therapy.</a> Fraternale, A; Paoletti, MF; Casabianca, A; Nencioni, L; Garaci, E; Palamara, AT; Magnani, M.  MOLECULAR ASPECTS OF MEDICINE, 2009; 30(1-2): 99-110. </p>

    <p class="title">11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000266716400004">SYNTHESIS AND BIOLOGICAL EVALUATION OF NOVEL SUBSTITUTEDN1-[1-BENZYL-3-(3-TERT-BUTYLCARBAMOYL-OCTAHYDROISOQUINOLIN-2Y L)-2-HYDROXY-PROPYL]-2-[(2-OXO-2H-CHROMENE-3-CARBONYL)AMINO] SUCCINAMIDE ANALOGS AS ANTI-VIRAL AND ANTI-HIV AGENTS.</a>  Reddy, YT; Reddy, PN; Crooks, PA; De Clercq, E; Rao, GVP; Rajitha, B.  HETEROCYCLIC COMMUNICATIONS, 2008; 14(6): 419-426.</p>

    <p class="title">12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000267014100012">Characterization of HIV-1 Enzyme Reverse Transcriptase Inhibition by the Compound 6-Chloro-1,4-Dihydro-4-Oxo-1-(beta-D-Ribofuranosyl) Quinoline-3-Carboxylic Acid Through Kinetic and In Silico Studies.</a>  Souza, TML; Rodrigues, DQ; Ferreira, VF; Marques, IP; Santos, FD; Cunha, AC; de Souza, MCBV; Frugulhetti, ICDP; Bou-Habib, DC; Fontes, CFL.  CURRENT HIV RESEARCH, 2009; 7(3): 327-335. </p>

	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
