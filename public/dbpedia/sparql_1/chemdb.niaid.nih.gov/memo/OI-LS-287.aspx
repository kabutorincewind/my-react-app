

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-287.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eVPkEpHtlN5YTZcnODIEwRKuxBYW2qP4xdMR2gHIoPVvJhzojeEqtFkUqcXqpv9ZVub64UkqJeY99kTOvO9GxyGZ0V9JaHLZEIDZG9YQOZ6/a5zLKZ9mU8rcODg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AA9564C5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-287</b>  </p>

<p>    1.    42656   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p><b>           Polyamine analogues with antitrypanosomal and antimicrosporidial activity selectively inhibit trypanothione reductase</b> </p>

<p>           Ward, Tracey, Lopez, Christina, Bacchi, Cyrus J, and Woster, Patrick M </p>

<p>           CONFERENCE: Abstracts of Papers, 226th ACS National Meeting, New York, NY, United States, September 7-11, 2003 2003:  MEDI-335 </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>    2.    42657   OI-LS-287; WOS-OI-1/10/2004 </p>

<p class="memofmt1-2">           Pangelin, an antimycobacterial coumarin from Ducrosia anethifolia</p>

<p>           Stavri, M, Mathew, KT, Bucar, F, and Gibbons, S </p>

<p>           PLANTA MED 2003. 69(10): 956-959 </p>

<p>           HYPERLINK<a href=":%20http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187419100015">: http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187419100015</a> </p><br />

<p>    3.    42658   OI-LS-287; WOS-OI-1/10/2004 </p>

<p><b>           Evaluation of some Egyptian plant species for in vitro antimycobacterial and cytotoxic activities</b> </p>

<p>           Abou, El Seoud KAEH, Bibby, MC, Shoeib, N, and Wright, CW </p>

<p>           PHARM BIOL 2003. 41(6): 463-465 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187290800013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187290800013</a>  </p><br />

<p>    4.    42659   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p class="memofmt1-2">           Synthesis and antitrypanosomal activity of aza-analogs of furamidine</p>

<p>           Ismail, Mohamed A, Brun, Reto, and Boykin, David W </p>

<p>           CONFERENCE: Abstracts of Papers, 225th ACS National Meeting, New Orleans, LA, United States, March 23-27, 2003 2003:  MEDI-059 </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>    5.    42660   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p class="memofmt1-2">           EthR, a repressor of the TetR/CamR family implicated in ethionamide resistance in mycobacteria, octamerizes cooperatively on its operator</p>

<p>           Engohang-ndong, Jean, Baillat, David, Aumercier, Marc, Bellefontaine, Flore, Besra, Gurdyal S, Locht, Camille, and Baulard, Alain R </p>

<p>           Molecular Microbiology 2003. 51(1): 175-188 </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>    6.    42661   OI-LS-287; WOS-OI-1/10/2004 </p>

<p class="memofmt1-2">           In vitro studies on the toxicity of isoniazid in different cell lines</p>

<p>           Schwab, CE and Tuschl, H </p>

<p>           HUM EXP TOXICOL 2003. 22(11): 607-615 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187341400005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187341400005</a>  </p><br />

<p>    7.    42662   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p class="memofmt1-2">           Dissectol A, an unusual monoterpene glycoside from Incarvillea dissectifoliola</p>

<p>           Chen, Wei, Shen, Yuemao, and Xu, Jianchu </p>

<p>           Planta Medica  2003. 69(6): 579-582 </p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    8.    42663   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p><b>           Crystal structure, three-dimensional structure and sequences of acylhomoserine lactone synthases involved in the microbial quorum sensing system, and applications to drug screening and drug design</b> ((USA)) </p>

<p>           Churchill, Mair EA, Von Bodman, Susanne B, Schweizer, Herbert P, Gould, Ty A, Hoang, Tung Thanh, Murphy, Frank V, and Watson, William T </p>

<p>           PATENT: US 2003119162 A1;  ISSUE DATE: 20030626 </p>

<p>           APPLICATION: 2002-58274; PP: 114 pp. </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>    9.    42664   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p class="memofmt1-2">           Mutational analysis of cell wall biosynthesis in Mycobacterium avium</p>

<p>           Laurent, Jean-Pierre, Hauge, Kirsten, Burnside, Kellie, and Cangelosi, Gerard </p>

<p>           Journal of Bacteriology 2003. 185(16): 5003-5006 </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>  10.    42665   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p class="memofmt1-2">           Albumin nanoparticles improved the stability, nuclear accumulation and anticytomegaloviral activity of a phosphodiester oligonucleotide</p>

<p>           Arnedo, A, Irache, JM, Merodio, M, and Espuelas Millan, MS </p>

<p>           Journal of Controlled Release 2004.  94(1): 217-227 </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>  11.    42666   OI-LS-287; WOS-OI-1/10/2004 </p>

<p class="memofmt1-2">           Antifungal 3-hydroxy fatty acids from Lactobacillus plantarum MiLAB 14</p>

<p>           Sjogren, J, Magnusson, J, Broberg, A, Schnurer, J, and Kenne, L </p>

<p>           APPL ENVIRON MICROB 2003. 69(12):  7554-7557 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187234000078">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187234000078</a> </p><br />

<p>  12.    42667   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           Molecular detection of resistance to antituberculous therapy</p>

<p>           Marttila, HJ and Soini, H </p>

<p>           Clin Lab Med 2003. 23(4): 823-41, v-vi </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14711094&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14711094&amp;dopt=abstract</a> </p><br />

<p>  13.    42668   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p><b>           Antiviral agents based on nitrogen heterocycles</b>((The MacFarlane Burnet Institute for Medical Research and Public Health Limited, Australia) </p>

<p>           Anderson, David Andrew and Gazina, Elena Vladimirovna </p>

<p>           PATENT: WO 2003063869 A1;  ISSUE DATE: 20030807 </p>

<p>           APPLICATION: 2003; PP: 47 pp. </p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>  14.    42669   OI-LS-287; SCIFIND-OI-01/07/2004 </p>

<p><b>           Pyrimidinylbenzothiadiazine dioxides as virucides for hepatitis C virus</b>((Smithkline Beecham Corporation, USA) </p>

<p>           Dhanak, Dashyant, Duffy, Kevin J, Sarisky, Robert T, Shaw, Antony N, and Tedesco, Rosanna </p>

<p>           PATENT: WO 2003037262 A2;  ISSUE DATE: 20030508 </p>

<p>           APPLICATION: 2002; PP: 51 pp. </p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    42670   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           Crystal Structure of MshB from Mycobacterium tuberculosis, a Deacetylase Involved in Mycothiol Biosynthesis</b> </p>

<p>           McCarthy, AA, Peterson, NA, Knijff, R, and Baker, EN </p>

<p>           J Mol Biol 2004. 335(4): 1131-41 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698305&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698305&amp;dopt=abstract</a> </p><br />

<p>  16.    42671   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           Synthesis of galactopyranosyl amino alcohols as a new class of antitubercular and antifungal agents</b> </p>

<p>           Tewari, N, Tiwari, VK, Tripathi, RP, Chaturvedi, V, Srivastava, A, Srivastava, R, Shukla, PK, Chaturvedi, AK, Gaikwad, A, Sinha, S, and Srivastava, BS </p>

<p>           Bioorg Med Chem Lett 2004. 14(2): 329-32 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14698152&amp;dopt=abstract</a> </p><br />

<p>  17.    42672   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           A polyketide synthase catalyzes the last condensation step of mycolic acid biosynthesis in mycobacteria and related organisms</b> </p>

<p>           Portevin, D,  De Sousa-D&#39;Auria, C, Houssin, C, Grimaldi, C, Chami, M, Daffe, M, and Guilhot, C </p>

<p>           Proc Natl Acad Sci U S A 2004. 101(1): 314-319 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695899&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695899&amp;dopt=abstract</a> </p><br />

<p>  18.    42673   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           Antimycobacterial agents. 1. Thio analogues of purine</b> </p>

<p>           Pathak, AK, Pathak, V, Seitz, LE, Suling, WJ, and Reynolds, RC </p>

<p>           J Med Chem 2004. 47(1): 273-6 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695841&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695841&amp;dopt=abstract</a> </p><br />

<p>  19.    42674   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           Folate-Synthesizing Enzyme System as Target for Development of Inhibitors and Inhibitor Combinations against Candida albicans-Synthesis and Biological Activity of New 2,4-Diaminopyrimidines and 4&#39;-Substituted 4-Aminodiphenyl Sulfones</p>

<p>           Otzen, T, Wempe, EG, Kunz, B, Bartels, R, Lehwark-Yvetot, G, Hansel, W, Schaper, KJ, and Seydel, JK </p>

<p>           J Med Chem 2004. 47(1): 240-53 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695838&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695838&amp;dopt=abstract</a> </p><br />

<p>  20.    42675   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           NMR structural characterization of Peptide inhibitors bound to the hepatitis C virus NS3 protease: design of a new p2 substituent</p>

<p>           Goudreau, N, Cameron, DR, Bonneau, P, Gorys, V, Plouffe, C, Poirier, M, Lamarre, D, and Llinas-Brunet, M </p>

<p>           J Med Chem 2004. 47(1): 123-32 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695826&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695826&amp;dopt=abstract</a> </p><br />

<p>  21.    42676   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           Discovery of alpha,gamma-Diketo Acids as Potent Selective and Reversible Inhibitors of Hepatitis C Virus NS5b RNA-Dependent RNA Polymerase</p>

<p>           Summa, V, Petrocchi, A, Pace, P, Matassa, VG, De, Francesco R, Altamura, S, Tomei, L, Koch, U, and Neuner, P </p>

<p>           J Med Chem 2004. 47(1): 14-7 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695815&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695815&amp;dopt=abstract</a> </p><br />

<p>  22.    42677   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           Solid-Phase Synthesis and Screening of Macrocyclic Nucleotide-Hybrid Compounds Targeted to Hepatitis C NS5B</b> </p>

<p>           Smietana, M, Johnson, RB, Wang, QM, and Kool, ET </p>

<p>           Chemistry 2004. 10(1): 173-81 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14695562&amp;dopt=abstract</a> </p><br />

<p>  23.    42678   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           In vitro postantibiotic effects of rifapentine, isoniazid, and moxifloxacin against Mycobacterium tuberculosis</b> </p>

<p>           Chan, CY, Au-Yeang, C, Yew, WW, Leung, CC, and Cheng, AF </p>

<p>           Antimicrob Agents Chemother 2004.  48(1): 340-3 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693563&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693563&amp;dopt=abstract</a> </p><br />

<p>  24.    42679   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           Pyridines and pyrimidines mediating activity against an efflux-negative strain of Candida albicans through putative inhibition of lanosterol demethylase</p>

<p>           Buurman, ET, Blodgett, AE, Hull, KG, and Carcanague, D </p>

<p>           Antimicrob Agents Chemother 2004.  48(1): 313-8 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693556&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693556&amp;dopt=abstract</a> </p><br />

<p>  25.    42680   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p><b>           Antimicrobial activity of Rubus chamaemorus leaves</b> </p>

<p>           Thiem, B and Goslinska, O </p>

<p>           Fitoterapia 2004. 75(1): 93-5 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693229&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14693229&amp;dopt=abstract</a> </p><br />

<p>  26.    42681   OI-LS-287; PUBMED-OI-1/12/2004 </p>

<p class="memofmt1-2">           Targeting persistence: the next frontier in TB drug development</p>

<p>           Anon </p>

<p>           GMHC Treat Issues 2003. 17(10): 5 </p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14689927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14689927&amp;dopt=abstract</a> </p><br />

<p> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
