

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-06-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Z24a7lap2kgUXkMuw1P8ucCVf6LYXCsOQsiNFUELPQDNyjlIk7pHuS6PHuTOWfn+CZaboY50WjbVdiyNfqnIVAhxHY+nYUS3fWxy340EDjbRTGBdl+vrKD9Tz1s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9CF29C3C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 6 - June 19, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24887478">Design and Synthesis of Lipid-coupled Inositol 1,2,3,4,5,6-Hexakisphosphate Derivatives Exhibiting High-affinity Binding for the HIV-1 MA Domain.</a> Tateishi, H., K. Anraku, R. Koga, Y. Okamoto, M. Fujita, and M. Otsuka. Organic &amp; Biomolecular Chemistry, 2014. 12(27): p. 5006-5022. PMID[24887478].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24732776">Ibudilast (AV411), and Its AV1013 Analog, Reduce HIV-1 Replication and Neuronal Death Induced by HIV-1 and Morphine.</a> El-Hage, N., M. Rodriguez, E.M. Podhaizer, S. Zou, S.M. Dever, S.E. Snider, P.E. Knapp, P.M. Beardsley, and K.F. Hauser. AIDS, 2014. 28(10): p. 1409-1419. PMID[24732776].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24794751">Synthesis and Biological Evaluation of CHX-DAPYs as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Yan, Z.H., H.Q. Wu, W.X. Chen, Y. Wu, H.R. Piao, Q.Q. He, F.E. Chen, E. De Clercq, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry, 2014. 22(12): p. 3220-3226. PMID[24794751].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24804575">Synthesis of an Apionucleoside Family and Discovery of a Prodrug with anti-Hiv Activity.</a> Toti, K.S., M. Derudas, F. Pertusati, D. Sinnaeve, F. Van den Broeck, L. Margamuljana, J.C. Martins, P. Herdewijn, J. Balzarini, C. McGuigan, and S. Van Calenbergh. The Journal of Organic Chemistry, 2014. 79(11): p. 5097-5112. PMID[24804575].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24820959">Identification of Biologically Active, Hiv Tar Rna-binding Small Molecules Using Small Molecule Microarrays.</a> Sztuba-Solinska, J., S.R. Shenoy, P. Gareiss, L.R. Krumpe, S.F. Le Grice, B.R. O&#39;Keefe, and J.S. Schneekloth, Jr. Journal of the American Chemical Society, 2014. 136(23): p. 8402-8410. PMID[24820959].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24793360">Investigation of a Novel Series of 2-Hydroxyisoquinoline-1,3(2H,4H)-diones as Human Immunodeficiency Virus Type 1 Integrase Inhibitors.</a> Suchaud, V., F. Bailly, C. Lion, C. Calmels, M.L. Andreola, F. Christ, Z. Debyser, and P. Cotelle. Journal of Medicinal Chemistry, 2014. 57(11): p. 4640-4660. PMID[24793360].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24932481">miRNA-1236 Inhibits HIV-1 Infection of Monocytes by Repressing Translation of Cellular Factor VprBP.</a> Ma, L., C.J. Shen, E.A. Cohen, S.D. Xiong, and J.H. Wang. PloS One, 2014. 9(6): p. e99535. PMID[24932481].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24769349">Discovery of Novel Diarylpyrimidines as Potent HIV NNRTIs via a Structure-guided Core-refining Approach.</a> Li, X., W. Chen, Y. Tian, H. Liu, P. Zhan, E. De Clercq, C. Pannecouque, J. Balzarini, and X. Liu. European Journal of Medicinal Chemistry, 2014. 80: p. 112-121. PMID[24769349].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24769348">New Indolylarylsulfones as Highly Potent and Broad Spectrum HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Famiglini, V., G. La Regina, A. Coluccia, S. Pelliccia, A. Brancale, G. Maga, E. Crespan, R. Badia, B. Clotet, J.A. Este, R. Cirilli, E. Novellino, and R. Silvestri. European Journal of Medicinal Chemistry, 2014. 80: p. 101-111. PMID[24769348].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=18057750">Synthesis and HIV-1 Integrase Inhibition of Novel bis- or tetra-Coumarin Analogues.</a> Chiang, C.C., J.F. Mouscadet, H.J. Tsai, C.T. Liu, and L.Y. Hsu. Chemical &amp; Pharmaceutical Bulletin, 2007. 55(12): p. 1740-1743. PMID[18057750].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0606-061914.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335671000009">Delta(9)-Tetrahydrocannabinol Treatment During Human Monocyte Differentiation Reduces Macrophage Susceptibility to HIV-1 Infection.</a> Williams, J.C., S. Appelberg, B.A. Goldberger, T.W. Klein, J.W. Sleasman, and M.M. Goodenow. Journal of Neuroimmune Pharmacology, 2014. 9(3): p. 369-379. ISI[000335671000009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335636600018">First Report on Isolation of Methyl Gallate with Antioxidant, anti-HIV-1 and HIV-1 Enzyme Inhibitory Activities from a Mushroom (Pholiota adiposa).</a> Wang, C.R., R. Zhou, T.B. Ng, J.H. Wong, W.T. Qiao, and F. Liu. Environmental Toxicology and Pharmacology, 2014. 37(2): p. 626-637. ISI[000335636600018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335912300010">Rational Design and Synthesis of Novel Thiazolidin-4-ones as Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Murugesan, V., N. Makwana, R. Suryawanshi, R. Saxena, R. Tripathi, R. Paranjape, S. Kulkarni, and S.B. Katti. Bioorganic &amp; Medicinal Chemistry, 2014. 22(12): p. 3159-3170. ISI[000335912300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335912300009">Design and Discovery of Flavonoid-based HIV-1 Integrase Inhibitors Targeting Both the Active Site and the Interaction with LEDGF/p75.</a> Li, B.W., F.H. Zhang, E. Serrao, H. Chen, T.W. Sanchez, L.M. Yang, N. Neamati, Y.T. Zheng, H. Wang, and Y.Q. Long. Bioorganic &amp; Medicinal Chemistry, 2014. 22(12): p. 3146-3158. ISI[000335912300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335559100008">Multicomponent Diversity-oriented Synthesis of Symmetrical and Unsymmetrical 1,4-Dihydropyridines in Recyclable Glycine Nitrate (GlyNO(3)) Ionic Liquid: A Mechanistic Insight Using Q-TOF, ESI-MS/MS.</a> Kumar, R., N.H. Andhare, A. Shard, Richa, and A.K. Sinha. RSC Advances, 2014. 4(37): p. 19111-19121. ISI[000335559100008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336023400019">Design, Synthesis, and Biological Evaluation of Novel Trifluoromethyl Indoles as Potent HIV-1 NNRTIs with an Improved Drug Resistance Profile.</a> Jiang, H.X., D.M. Zhuang, Y. Huang, X.X. Cao, J.H. Yao, J.Y. Li, J.Y. Wang, C. Zhang, and B. Jiang. Organic &amp; Biomolecular Chemistry, 2014. 12(21): p. 3446-3458. ISI[000336023400019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000335879300021">Design, Synthesis, and Optimization of Balanced Dual NK1/NK3 Receptor Antagonists.</a> Hanessian, S., T. Jennequin, N. Boyer, V. Babonneau, U. Soma, C.M. la Cour, M.J. Millan, and G. De Nanteuil. ACS Medicinal Chemistry Letters, 2014. 5(5): p. 550-555. ISI[000335879300021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0606-061914.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
