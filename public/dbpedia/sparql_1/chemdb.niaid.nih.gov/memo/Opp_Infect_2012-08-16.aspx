

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-08-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LKb0KtqAjkJmV8EuqDxq6nt6uluG26tI7lxbsVoLpXuGPxNW0PCVv8lUpq0Ike69EYkeNXrMQXtIOemSP4Rm1nGR1VNnoll/4x+lc/VSNqyllf1Jls8CubFyIB4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0E08CB1D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 3 - August 16, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22575887">Synthesis and Synergistic Antifungal Activities of a Pyrazoline Based Ligand and Its Copper(II) and Nickel(II) Complexes with Conventional Antifungals</a>. Ali, I., W.A. Wani, A. Khan, A. Haque, A. Ahmad, K. Saleem, and N. Manzoor. Microbial Pathogenesis, 2012. 53(2): p. 66-73. PMID[22575887].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22863111">Design of a Novel Antimicrobial Peptide Activated by Virulent Proteases.</a> Aoki, W., N. Kitahara, N. Miura, H. Morisaka, K. Kuroda, and M. Ueda. Chemical Biology &amp; Drug Design, 2012. <b>[[Epub ahead of print]]</b>. PMID[22863111].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22850947">Terbinafine Inhibits Cryptococcus Neoformans Growth and Modulates Fungal Morphology.</a> Guerra, C.R., K. Ishida, M. Nucci, and S. Rozental. Memorias do Instituto Oswaldo Cruz, 2012. 107(5): p. 582-590. PMID[22850947].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615293">An Integrated Approach for Identification and Target Validation of Antifungal Compounds Active against Erg11p.</a> Hoepfner, D., S. Karkare, S.B. Helliwell, M. Pfeifer, M. Trunzer, S. De Bonnechose, A. Zimmerlin, J. Tao, D. Richie, A. Hofmann, S. Reinker, M. Frederiksen, N.R. Movva, J.A. Porter, N.S. Ryder, and C.N. Parker. Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4233-4240. PMID[22615293].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22850990">In Vitro Synergism of Simvastatin and Fluconazole against Candida Species.</a> Menezes, E.A., A.A. Vasconcelos Junior, C.L. Silva, F.X. Plutarco, C. Cunha Mda, and F.A. Cunha. Revista do Instituto de Medicina Tropical de Sao Paulo, 2012. 54(4): p. 197-199. PMID[22850990].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22853621">Synthesis and Screening of Antibacterial and Antifungal Activity of 5-Chloro-1,3-benzoxazol-2(3H)-one Derivatives.</a> Modiya, P.R. and C.N. Patel. Organic and Medicinal Chemistry Letters, 2012. 2(1): p. 29. PMID[22853621].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22878559">Mutual Influence of Backbone Proline Substitution and Lipophilic Tail Character on the Biological Activity of Simplified Analogues of Caspofungin.</a> Mulder, M.P., P. Fodran, J. Kemmink, E.J. Breukink, J.A. Kruijtzer, A.J. Minnaard, and R.M. Liskamp. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22878559].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22860593">Antifungal Effect of Mexican Oregano (Lippia Berlandieri Schauer) Essential Oil on a Wheat Flour-Based Medium.</a> Portillo-Ruiz, M.C., R.A. Sanchez, S.V. Ramos, J.V. Munoz, and G.V. Nevarez-Moorillon. Journal of Food Science, 2012. 77(8): p. M441-445. PMID[22860593].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22683240">Synthesis and Evaluation of Antimicrobial Activity of 4H-Pyrimido[2,1-b]benzothiazole, pyrazole and benzylidene Derivatives of Curcumin.</a> Sahu, P.K., S.K. Gupta, D. Thavaselvam, and D.D. Agarwal. European Journal of Medicinal Chemistry, 2012. 54: p. 366-378. PMID[22683240].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22806196">Bioprospecting Micromonospora from Kaziranga National Park of India and Their Anti-infective Potential.</a> Talukdar, M., A. Duarah, S. Talukdar, M.B. Gohain, R. Debnath, A. Yadav, D.K. Jha, and T.C. Bora. World journal of Microbiology &amp; Biotechnology, 2012. 28(8): p. 2703-2712. PMID[22806196]. [<b>PubMed</b>]. OI_0803-081612.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22608674">Pyrimidin-2(1H)-ones Based Inhibitors of Mycobacterium tuberculosis Orotate Phosphoribosyltransferase.</a> Breda, A., P. Machado, L.A. Rosado, A.A. Souto, D.S. Santos, and L.A. Basso. European Journal of Medicinal Chemistry, 2012. 54C: p. 113-122. PMID[22608674].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22877928">Anti-mycobacterial Natural Products from the Canadian Medicinal Plant Juniperus communis.</a> Carpenter, C.D., T. O&#39;Neill, N. Picot, J.A. Johnson, G.A. Robichaud, D. Webster, and C.A. Gray. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[22877928].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615332">Old Drugs, New Purpose: Retooling Existing Drugs for Optimized Treatment of Resistant Tuberculosis.</a> Dooley, K.E., C.D. Mitnick, M. Ann Degroote, E. Obuku, V. Belitsky, C.D. Hamilton, M. Makhene, S. Shah, J.C. Brust, N. Durakovic, and E. Nuermberger. Clinical Infectious, 2012. 55(4): p. 572-581. PMID[22615332].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22512569">Perspectives on the Development of Novel Potentially Active Quinolones against Tuberculosis and Cancer.</a> Facchinetti, V., C.R. Gomes, M.V. de Souza, and T.R. Vasconcelos. Mini Reviews in Medicinal Chemistry, 2012. 12(9): p. 866-874. PMID[22512569].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22872445">Antibiotic Susceptibility Profile of Mycobacterium avium Subspecies Hominissuis Is Altered in Low-Iron Conditions.</a> Kopinc, R. and A. Lapanje. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22872445].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22875849">Sulfamethoxazole Enhances the Antimycobacterial Activity of Rifampicin.</a> Macingwana, L., B. Baker, A.H. Ngwane, C. Harper, M.F. Cotton, A. Hesseling, A.H. Diacon, P. van Helden, and I. Wiid. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22875849].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22658084">Novel Polycyclic &#39;Cage&#39;-1,2-Diamines as Potential Anti-tuberculosis Agents.</a> Onajole, O.K., Y. Coovadia, H.G. Kruger, G.E. Maguire, M. Pillay, and T. Govender. European Journal of Medicinal Chemistry, 2012. 54: p. 1-9. PMID[22658084].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22695129">Synthesis, Antitubercular and Antimicrobial Evaluation of 3-(4-Chlorophenyl)-4-substituted pyrazole Derivatives.</a> Pathak, R.B., P.T. Chovatia, and H.H. Parekh. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(15): p. 5129-5133. PMID[22695129].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22867883">Evaluation of the in Vitro and Intracellular Efficacy of New Monosubstituted Sulfonylureas against Extensively Drug-Resistant Tuberculosis.</a> Wang, D., L. Pan, G. Cao, H. Lei, X. Meng, J. He, M. Dong, Z. Li, and Z. Liu. International Journal of Antimicrobial Agents, 2012. <b>[Epub ahead of print]</b>. PMID[22867883].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22750008">Synthesis, Antimalarial Activity and Cytotoxicity of 10-Aminoethylether Derivatives of Artemisinin.</a> Cloete, T.T., J. Wilma Breytenbach, C. Kock, P.J. Smith, J.C. Breytenbach, and D. N&#39;Da D. Bioorganic &amp; Medicinal Chemistry, 2012. 20(15): p. 4701-4709. PMID[22750008].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22889559">Tandem Synthesis and in Vitro Antiplasmodial Evaluation of New Naphtho[2,1-d]thiazole Derivatives.</a> Cohen, A., P. Verhaeghe, M.D. Crozet, S. Hutter, P. Rathelot, P. Vanelle, and N. Azas. European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22889559].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22781704">New Quinoline-5,8-dione and Hydroxynaphthoquinone Derivatives Inhibit a Chloroquine Resistant Plasmodium falciparum Strain.</a> Hussain, H., S. Specht, S.R. Sarite, A. Hoerauf, and K. Krohn. European Journal of Medicinal Chemistry, 2012. 54: p. 936-942. PMID[22781704].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22853877">Anti-plasmodial Action of De Novo-designed, Cationic, Lysine-branched, Amphipathic, Helical Peptides.</a> Kaushik, N.K., J. Sharma, and D. Sahal. Malaria Journal, 2012. 11(1): p. 256. PMID[22853877].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22659195">Antiplasmodial Activity of the Andiroba (Carapa guianensis Aubl., Meliaceae) Oil and Its Limonoid-Rich Fraction.</a> Miranda Junior, R.N., M.F. Dolabela, M.N. da Silva, M.M. Povoa, and J.G. Maia. Journal of Ethnopharmacology, 2012. 142(3): p. 679-683. PMID[22659195].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22749280">Benzoheterocyclic Amodiaquine Analogues with Potent Antiplasmodial Activity: Synthesis and Pharmacological Evaluation.</a> Ongarora, D.S., J. Gut, P.J. Rosenthal, C.M. Masimirembwa, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(15): p. 5046-5050. PMID[22749280]. [<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22683112">Novel Cinnamic Acid/4-Aminoquinoline Conjugates Bearing Non-proteinogenic Amino Acids: Towards the Development of Potential Dual Action Antimalarials.</a> Perez, B.C., C. Teixeira, M. Figueiras, J. Gut, P.J. Rosenthal, J.R. Gomes, and P. Gomes. European Journal of Medicinal Chemistry, 2012. 54: p. 887-899. PMID[22683112].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22652224">New N-Arylamino biquinoline Derivatives: Synthesis, Antimicrobial, Antituberculosis, and Antimalarial Evaluation.</a> Shah, N.M., M.P. Patel, and R.G. Patel. European Journal of Medicinal Chemistry, 2012. 54: p. 239-247. PMID[22652224].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0803-081612.</p>

    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306449700011">The Effect of Various Topical Peri-implantitis Antiseptics on Staphylococcus Epidermidis, Candida albicans, and Streptococcus sanguinis.</a> Burgers, R., C. Witecy, S. Hahnel, and M. Gosau. Archives of Oral Biology, 2012. 57(7): p. 940-947. ISI[000306449700011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306101300026">Construction and Functionalization of Fused Pyridine Ring Leading to Novel Compounds as Potential Antitubercular Agents.</a> Dulla, B., B.J. Wan, S.G. Franzblau, R. Kapavarapu, O. Reiser, J. Iqbal, and M. Pal. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4629-4635. ISI[000306101300026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306232800003">Antibacterial, Antimycelial and Phytochemical Analysis of Ricinus communis Linn, Trigonella foenum grecum Linn and Delonix regia (Bojer Ex Hook.) Raf of Pakistan.</a> Khursheed, R., A. Naz, E. Naz, H. Sharif, and G.H. Rizwani. Romanian Biotechnological Letters, 2012. 17(3): p. 7237-7244. ISI[000306232800003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306366000018">The Epithelium-produced Growth Factor Midkine Has Fungicidal Properties.</a> Nordin, S.L., A. Sonesson, M. Malmsten, M. Morgelin, and A. Egesten. Journal of Antimicrobial Chemotherapy, 2012. 67(8): p. 1927-1936. ISI[000306366000018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306274700011">Combination of Bioactive Moieties with Different Heteroatom(S): Application of the Suzuki Cross-coupling Reaction.</a> Patel, R.V., J.K. Patel, P. Kumari, and K.H. Chikhalia. Heteroatom Chemistry, 2012. 23(4): p. 399-410. ISI[000306274700011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306376600003">Synthesis and Application of Some Novel Antimicrobial Monoazonaphthalimide Dyes: Synthesis and Characterisation.</a> Shaki, H., K. Gharanjig, S. Rouhani, A. Khosravi, and J. Fakhar. Coloration Technology, 2012. 128(4): p. 270-275. ISI[000306376600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306203100007">A Rhodanine Derivative CCR-11 Inhibits Bacterial Proliferation by Inhibiting the Assembly and Gtpase Activity of Ftsz.</a> Singh, P., B. Jindal, A. Surolia, and D. Panda. Biochemistry, 2012. 51(27): p. 5434-5442. ISI[000306203100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306125100005">Molecular Identification and Antifungal Susceptibilities of Black Aspergillus Isolates from Otomycosis Cases in Hungary.</a> Szigeti, G., S. Kocsube, I. Doczi, L. Bereczki, C. Vagvolgyi, and J. Varga. Mycopathologia, 2012. 174(2): p. 143-147. ISI[000306125100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
    <p> </p>
    
    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306101300076">Neamphamide B, New Cyclic Depsipeptide, as an Anti-Dormant Mycobacterial Substance from a Japanese Marine Sponge of Neamphius Sp.</a> Yamano, Y., M. Arai, and M. Kobayashi. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4877-4881. ISI[000306101300076].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0803-081612.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
