

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-08-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MUPlvUFcB0l2H+kOfFtYkq1O/PA+f1ZJhbZcmDeM2MY61QMQaqfN0MKqDCuIaTcU99X5LxjQ1sPwQrhsdyGTa4aEj/OjeH7IqMQLM3msrlfEmLvxo1fZK9Uk6pY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E6ABF730" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  August 4-August 19, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20715285">A Novel Synthetic Route for the Anti-HIV Drug MC-1220 and its Analogues.</a> Loksha YM, Globisch D, Loddo R, Collu G, La Colla P, Pedersen EB. ChemMedChem. 2010 Aug 16. [Epub ahead of print] PMID: 20715285</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20710065">Phenylboronic-acid-based carbohydrate binders as antiviral therapeutics: monophenylboronic acids.</a> Trippier PC, McGuigan C, Balzarini J. Antivir Chem Chemother. 2010 Aug 11;20(6):249-57.PMID: 20710065</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20714805">From Pichia anomala killer toxin through killer antibodies to killer peptides for a comprehensive anti-infective strategy.</a> Polonelli L, Magliani W, Ciociola T, Giovati L, Conti S. Antonie Van Leeuwenhoek. 2010 Aug 18. [Epub ahead of print] PMID: 20714805</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20698683">Antisense-Induced Guanine Quadruplexes Inhibit Reverse Transcription by HIV-1 Reverse Transcriptase.</a> Hagihara M, Yamauchi L, Seo A, Yoneda K, Senda M, Nakatani K. J Am Chem Soc. 2010 Aug 18;132(32):11171-8. PMID: 20698683</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20638854">Anti-HIV and antiplasmodial activity of original flavonoid derivatives.</a> Casano G, Dumètre A, Pannecouque C, Hutter S, Azas N, Robin M. Bioorg Med Chem. 2010 Aug 15;18(16):6012-23. Epub 2010 Jun 25. PMID: 20638854</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20538590">Novel recombinant engineered gp41 N-terminal heptad repeat trimers and their potential as anti-HIV-1 therapeutics or microbicides.</a> Chen X, Lu L, Qi Z, Lu H, Wang J, Yu X, Chen Y, Jiang S. J Biol Chem. 2010 Aug 13;285(33):25506-15. Epub 2010 Jun 10.PMID: 20538590</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20643553">Concise synthesis of 5,6-dihydrovaltrate leading to enhanced Rev-export inhibitory congener.</a> Tamura S, Fujiwara K, Shimizu N, Todo S, Murakami N. Bioorg Med Chem. 2010 Aug 15;18(16):5975-80. Epub 2010 Jul 1.PMID: 20643553</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20719956">Design of a Potent D-peptide HIV-1 Entry Inhibitor with a Strong Barrier to Resistance.</a> Welch BD, Francis JN, Redman JS, Paul S, Weinstock MT, Reeves JD, Lie YS, Whitby FG, Eckert DM, Hill CP, Root MJ, Kay MS. J Virol. 2010 Aug 18. [Epub ahead of print]</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20137956">Biomimetic synthesis and anti-HIV activity of dimeric phloroglucinols.</a> Chauthe SK, Bharate SB, Sabde S, Mitra D, Bhutani KK, Singh IP. Bioorg Med Chem. 2010 Mar 1;18(5):2029-36. Epub 2010 Jan 15.PMID: 20137956</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280413600005">HIV-1 RT Inhibitors with a Novel Mechanism of Action: NNRTIs that Compete with the Nucleotide Substrate</a>. Maga, G; Radi, M; Gerard, MA; Botta, M; Ennifar, E.  VIRUSES-BASEL, 2010; 17(4):  880-899.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280159900004">Solvent-Free Synthesis of Pyrimidine Nucleoside-Aminophosphonate</a>.  Zhang, XY; Qu, YY; Fan, XS; Bores, C; Feng, D; Andrei, G; Snoeck, R; De Clercq, E; Loiseau, PM.  NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2010; 29(8): 616-627.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280340900019">Inhibition of HIV-1 Replication and Dimerization Interference by Dual Inhibitory RNAs.</a>  Sanchez-Luque, FJ; Reyes-Darias, JA; Puerta-Fernandez, E; Berzal-Herranz, A.  MOLECULES, 2010; 15(7): 4757-4772.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280373800006">Azaphilone and Isocoumarin Derivatives from the Endophytic Fungus Penicillium sclerotiorum PSU-A13</a>.  Arunpanichlert, J; Rukachaisirikul, V; Sukpondma, Y; Phongpaichit, S; Tewtrakul, S; Rungjindamai, N; Sakayaroj, J.  CHEMICAL &amp; PHARMACEUTICAL BULLETIN, 2010; 58(8): 1033-1036.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280431900002">Total Synthesis and Structural Revision of Biyouyanagin B</a>.  Nicolaou, KC; Sanchini, S; Wu, TR; Sarlah, D.  CHEMISTRY-A EUROPEAN JOURNAL, 2010; 16(26): 7678-7682.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280244700013">Microwave Assisted One-pot Synthesis of Novel alpha-Aminophosphonates and Their Biological Activity</a>.  Rao, AJ; Rao, PV; Rao, VK; Mohan, C; Raju, CN; Reddy, CS.  BULLETIN OF THE KOREAN CHEMICAL SOCIETY, 2010; 31(7): 1863-1868.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280654100003">Amino Acid Derivatives, Part 4: Synthesis and Anti-HIV Activity of New Naphthalene Derivatives</a>.  Hamad, NS; Al-Haidery, NH; Al-Masoudi, IA; Sabri, M; Sabri, L; Al-Masoudi, NA.  ARCHIV DER PHARMAZIE, 2010; 343(7):  397-403.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280583400023">Dendrimers as Potential Inhibitors of the Dimerization of the Capsid Protein of HIV-1.</a>  Domenech, R; Abian, O; Bocanegra, R; Correa, JA; Sousa-Herves, A; Riguera, R; Mateu, MG; Fernandez-Megia, E; Velazquez-Campoy, A; Neira, JL.  BIOMACROMOLECULES, 2010; 11(8): 2069-2078.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280613000003">Synthesis and Biological Evaluation of a New Series of 2-{[4-(3,4-Dichlorophenyl)-1,2,3-thiadiazol-5-yl]sulfanyl}acetanilides as HIV-1 Inhibitors</a>.  Zhan, P; Liu, XY; Li, ZY; Fang, ZJ; Pannecouque, C; De Clereq, E. CHEMISTRY &amp; BIODIVERSITY, 2010; 7(7): 1717-1727.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280557700004">Synthesis and anti-HIV activity evaluation of novel N&#39;-arylidene-2-[1-(naphthalen-1-yl)-1H-tetrazol-5-ylthio]acetohydrazide</a>.  Zhan, P; Liu, HB; Liu, XY; Wang, Y; Pannecouque, C; Witvrouw, M; De Clercq, E.  MEDICINAL CHEMISTRY RESEARCH, 2010; 19(7) 652-663.</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280636400007">Synthesis and Biological Activity of 2-Hydroxy-N(5-methylene-4-oxo-2-aryl-thiazolidin-3-yl)-benzamide</a>.  Patel, HS; Patel, SJ. PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS, 2010; 185(8): 1632-1639.</p>

    <p>21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280445700003">Purification and Characterization of a Laccase from the Edible Wild Mushroom Tricholoma mongolicum</a>.  Li, MA; Zhang, GQ; Wang, HX; Ng, T.  JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY, 2010; 20(7): 1069-1076.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
