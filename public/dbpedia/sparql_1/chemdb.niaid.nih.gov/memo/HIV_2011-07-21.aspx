

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-07-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yy0m8Mq5I0hA3lW0KqR/cf89UW6l0jXt22VMO25gS07SYazChCyZZC9qC8bifw8RLE9d26WBpIwqsZT4KwRakoNn2uUsYZTCcm0sdYryPVKqlhEE0CpkLjC5Ddg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="28F2F531" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 8, 2011- July 21, 2011</h1>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763726">The New and Less Toxic Protease Inhibitor Saquinavir-NO Maintains anti-HIV-1 Properties In vitro Indistinguishable from Those of the Parental Compound Saquinavir.</a> Canducci, F., E.R. Ceresola, D. Saita, Y. Al-Abed, G. Garotta, M. Clementi, and F. Nicoletti. Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[21763726].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21762475">Human-Phosphate-Binding-Protein Inhibits HIV-1 Gene Transcription and Replication.</a> Cherrier, T., M. Elias, A. Jeudy, G. Gotthard, V. Le Douce, H. Hallay, P. Masson, A. Janossy, E. Candolfi, O. Rohr, E. Chabriere, and C. Schwartz. Virology Journal, 2011. 8(1): p. 352; PMID[21762475].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568335">Synthesis, Activity, and Structural Analysis of Novel alpha-Hydroxytropolone Inhibitors of Human Immunodeficiency Virus Reverse Transcriptase-Associated Ribonuclease H.</a> Chung, S., D.M. Himmel, J.K. Jiang, K. Wojtak, J.D. Bauman, J.W. Rausch, J.A. Wilson, J.A. Beutler, C.J. Thomas, E. Arnold, and S.F. Le Grice, Journal of Medicinal Chemistry. 2011. 54(13): p. 4462-4473; PMID[21568335].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21767569">4-[1-(4-Fluorobenzyl)-4-hydroxy-1H-indol-3-yl]-2-hydroxy-4-oxobut-2-enoic acid as a Prototype to Develop Dual Inhibitors of HIV-1 Integration Process.</a> De Luca, L., R. Gitto, F. Christ, S. Ferro, S. De Grazia, F. Morreale, Z. Debyser, and A. Chimirri. Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[21767569].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763149">Structural Modifications of Quinolone-3-carboxylic acids with anti-HIV Activity.</a> He, Q.Q., S.X. Gu, J. Liu, H.Q. Wu, X. Zhang, L.M. Yang, Y.T. Zheng, and F.E. Chen. Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21763149].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21766363">Synthesis of the Anti-HIV Agent (-)-Hyperolactone C by Using Oxonium Ylide Formation-Rearrangement.</a> Hodgson, D.M. and S. Man. Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21766363].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21765914">Novel PI3K/Akt Inhibitors Screened by the Cytoprotective Function of Human Immunodeficiency Virus Type 1 Tat.</a> Kim, Y., J.A. Hollenbaugh, D.H. Kim, and B. Kim. Plos One, 2011. 6(7): p. e21781; PMID[21765914].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21688836">Synthesis of Aminoglycoside-3&#39;-Conjugates of 2&#39;-O-Methyl Oligoribonucleotides and Their Invasion to a (19)F labeled HIV-1 TAR Model.</a> Kiviniemi, A. and P. Virta, Bioconjugate. Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21688836].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21762100">Antiviral Activity of Substituted Salicylanilides - A Review.</a> Kratky, M. and J. Vinsova. Mini Reviews in Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21762100].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21749165">Targeting HIV Entry through Interaction with Envelope Glycoprotein 120 (gp120): Synthesis and Antiviral Evaluation of 1,3,5-Triazines with Aromatic Amino Acids.</a> Lozano, V., L. Aguado, B. Hoorelbeke, M. Renders, M.J. Camarasa, D. Schols, J. Balzarini, A. San-Felix, and M.J. Perez-Perez. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21749165].
    <br />
    <b>[PubMed]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292032700035">Synthesis and Antimicrobial Activity of Some Novel 5-Alkyl-6-Substituted Uracils and Related Derivatives.</a> Al-Turkistani, A.A., O.A. Al-Deeb, N.R. El-Brollosy, and A.A. El-Emam. Molecules. 2011. 16(6): p. 4764-4774; ISI[000292032700035].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292381900035">Partially Glycosylated Dendrimers Block MD-2 and Prevent TLR4-MD-2-LPS Complex Mediated Cytokine Responses.</a> Barata, T.S., I. Teo, S. Brocchini, M. Zloh, and S. Shaunak. Plos Computational Biology, 2011. 7(6); ISI[000292381900035].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292186400042">Quantitative Structure-Activity Relationship Study of Phloroglucinol-terpene Adducts as Anti-leishmanial Agents.</a> Bharate, S.B. and I.P. Singh. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(14): p. 4310-4315; ISI[000292186400042].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292301200008">Imidazo[1,2-a]pyridin-3-amines as Potential HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Bode, M.L., D. Gravestock, S.S. Moleele, C.W. van der Westhuyzen, S.C. Pelly, P.A. Steenkamp, H.C. Hoppe, T. Khan, and L.A. Nkabinde. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4227-4237; ISI[000292301200008].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292290600002">3,4,5-Trisubstituted-1,2,4-4H-triazoles as WT and Y188L Mutant HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors: Docking-ased CoMFA and CoMSIA Analyses.</a> Cichero, E., L. Buffa, and P. Fossa. Journal of Molecular Modeling, 2011. 17(7): p. 1537-1550; ISI[000292290600002].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982800893">Synthesis of Cyclopropyl Peptidomimetics as Potential BACE and HIV Protease Inhibitors.</a> Dunlap, N., N. Reddy, J. Taylor, and A.L. Pathiranage. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982800893].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291912500014">Synergistic In vitro anti-HIV Type 1 Activity of Tenofovir with Carbohydrate-Binding Agents (CBAs).</a> Ferir, G., K. Vermeire, D. Huskens, J. Balzarini, E.J.M. Van Damme, J.C. Kehr, E. Dittmann, M.D. Swanson, D.M. Markovitz, and D. Schols. Antiviral Research, 2011. 90(3): p. 200-204; ISI[000291912500014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291948800005">Synthesis, Antibacterial and Potential anti-HIV Activity of Some Novel Imidazole Analogs.</a> Ganguly, S., V.V. Vithlani, A.K. Kesharwani, R. Kuhu, L. Baskar, P. Mitramazumder, A. Sharon, and A. Dev. Acta Pharmaceutica, 2011. 61(2): p. 187-201; ISI[000291948800005].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292113700085">Synthesis and Enantioselective Hydrogenation of Seven-embered Cyclic Imines: Substituted Dibenzo[b,f][1,4]oxazepines.</a> Gao, K., C.B. Yu, W. Li, Y.G. Zhou, and X.M. Zhang. Chemical Communications, 2011. 47(27): p. 7845-7847; ISI[000292113700085].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982802364">Rational Engineering of Antiviral Lectins Targeting HIV.</a> Green, D.F. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982802364].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292301200007">Synthesis and Biological Evaluation of Naphthyl phenyl ethers (NPEs) as Novel Nonnucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Gu, S.X., X. Zhang, Q.Q. He, L.M. Yang, X.D. Ma, Y.T. Zheng, S.Q. Yang, and F.E. Chen. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4220-4226; ISI[000292301200007].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">22<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291763400054">. HIV</a> <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291763400054">Inhibitory Cyclic Depsipeptides from Marine Sponges: New Structural Features, Biological Activity Profiles, and Stereochemical Challenges.</a> Gustafson, K.R. Biopolymers, 2011. 96(4): p. 422-422; ISI[000291763400054].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291763400449">Novel Macrocyclic Molecules as Entry Inhibitors of HIV-1.</a> Joubran, S., J. Phanos, A. Swed, M. Kotler, A. Hofmann, and C. Gilon. Biopolymers, 2011. 96(4): p. 520-520; ISI[000291763400449].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982806030">Discovery of BMS-663068, an HIV Attachment Inhibitor for the Treatment of HIV-1.</a> Kadow, J.F., Y. Ueda, T.P. Connolly, T. Wang, C.P. Chen, K.S. Yeung, J. Bender, Z. Yang, J.L. Zhu, J. Mattiskella, A. Regueiro-Ren, Z.W. Yin, Z.X. Zhang, M. Farkas, X.J. Yang, H. Wong, D. Smith, K.S. Raghaven, Y. Pendri, A. Staab, N. Soundararajan, N. Meanwell, M. Zheng, D.D. Parker, S. Adams, H.T. Ho, G. Yamanaka, B. Nowicka-Sans, B. Eggers, B. McAuliffe, H. Fang, L. Fan, N. Zhou, Y.F. Gong, R.J. Colonno, P.F. Lin, J. Brown, D.M. Grasela, C. Chen, and R.E. Nettles. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982806030].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982806213">New Inhibitors for an Old Target: Pyrrolidines and Piperidines - Promising New Scaffolds to Block HIV Protease.</a> Klee, N., K. Linde, I. Lindemann, A. Heine, G. Klebe, and W.E. Diederich. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982806213].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292290600012">Understanding the Structure-Activity Relationship of Betulinic acid Derivatives as anti-HIV-1 Agents by Using 3D-QSAR and Docking.</a> Lan, P., W.N. Chen, Z.J. Huang, P.H. Sun, and W.M. Chen. Journal of Molecular Modeling, 2011. 17(7): p. 1643-1659; ISI[000292290600012].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291982800375">Peptide Based Inhibitor of HIV1 Entry.</a> Luna, A., J.T. Ngo, and D.A. Tirrell. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982800375].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291958200002">Development of Modified Nucleosides that have Supremely High anti-HIV activity and Low Toxicity and Prevent the Emergence of Resistant HIV Mutants.</a> Ohrui, H. Proceedings of the Japan Academy Series B-Physical and Biological Sciences, 2011. 87(3): p. 53-65; ISI[000291958200002].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292301200019.">Design, Synthesis and Biological Evaluation of 2 &#39;-Deoxy-2 &#39;,2 &#39;-difluoro-5-halouridine phosphoramidate ProTides.</a> Quintiliani, M., L. Persoons, N. Solaroli, A. Karlsson, G. Andrei, R. Snoeck, J. Balzarini, and C. McGuigan. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4338-4345; ISI[000292301200019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292215300021.">Synthesis and Biological Activities of Novel s-Triazine Bridged Dinucleoside Analogs.</a> Shen, F.J., X.L. Li, X.Y. Zhang, B. Zhanbin, Q.M. Yin, H. Chen, and J.C. Zhang. Chinese Journal of Chemistry, 2011. 29(6): p. 1205-1210; ISI[000292215300021].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291912500013">Virucidal Activity of the Dendrimer Microbicide SPL7013 against HIV-1.</a> Telwatte, S., K. Moore, A. Johnson, D. Tyssen, J. Sterjovski, M. Aldunate, P.R. Gorry, P.A. Ramsland, G.R. Lewis, J.R.A. Paull, S. Sonza, and G. Tachedjian. Antiviral Research, 2011. 90(3): p. 195-199; ISI[000291912500013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292301200022">Synthesis and Biological Evaluation of Novel 5-Alkyl-2-arylthio-6-((3,4-dihydroquinolin-1(2H)-yl)methyl)pyrimidin-4(3 H)-ones as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Zhang, J., P. Zhan, J.D. Wu, Z.Y. Li, Y. Jiang, W.Y. Ge, C. Pannecouque, E. De Clercq, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry, 2011. 19(14): p. 4366-4376; ISI[000292301200022].
    <br />
    <b>[WOS]</b>. HIV_0708-072111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
