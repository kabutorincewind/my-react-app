

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-130.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wgHWdIuXneRoljG5hAcpu2HcZ9cnMmm4b0aHrzMuM8lD1H5COng2J6ejUV2t/wXh4Xzgq5tcaxWERcu3gni/GfqfhO2Q91BHvEiK+Nk7RDRJUAjKxI7dYmPpju8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1AC4F60D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-130-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59228   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          A Small-Molecule Probe for Hepatitis C Virus Replication that Blocks Protein Folding</p>

    <p>          Rakic, B, Clarke, J, Tremblay, TL, Taylor, J, Schreiber, K, Nelson, KM, Abrams, SR, and Pezacki, JP</p>

    <p>          Chem Biol <b>2006</b>.  13(10): 1051-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052609&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17052609&amp;dopt=abstract</a> </p><br />

    <p>2.     59229   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Hepatitis E virus</p>

    <p>          Panda, SK, Thakral, D, and Rehman, S</p>

    <p>          Rev Med Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17051624&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17051624&amp;dopt=abstract</a> </p><br />

    <p>3.     59230   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Antiviral drugs prophylaxis and treatment of influenza</p>

    <p>          Anon</p>

    <p>          Med Lett Drugs Ther <b>2006</b>.  48(1246): 87-88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17051136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17051136&amp;dopt=abstract</a> </p><br />

    <p>4.     59231   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Characterization of an influenza A H5N2 reassortant as a candidate for live-attenuated and inactivated vaccines against highly pathogenic H5N1 viruses with pandemic potential</p>

    <p>          Desheva, JA, Lu, XH, Rekstin, AR, Rudenko, LG, Swayne, DE, Cox, NJ, Katz, JM, and Klimov, AI</p>

    <p>          Vaccine <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17050041&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17050041&amp;dopt=abstract</a> </p><br />

    <p>5.     59232   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Isothiazoles as active-site inhibitors of HCV NS5B polymerase</p>

    <p>          Yan, S, Appleby, T, Gunic, E, Shim, JH, Tasu, T, Kim, H, Rong, F, Chen, H, Hamatake, R, Wu, JZ, Hong, Z, and Yao, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049853&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049853&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59233   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Novel thiazolones as HCV NS5B polymerase allosteric inhibitors: Further designs, SAR, and X-ray complex structure</p>

    <p>          Yan, S, Larson, G, Wu, JZ, Appleby, T, Ding, Y, Hamatake, R, Hong, Z, and Yao, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049849&amp;dopt=abstract</a> </p><br />

    <p>7.     59234   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Stable inhibition of hepatitis B virus expression and replication in HepG2.2.15 cells by RNA interference based on retrovirus delivery</p>

    <p>          Jia, F, Zhang, YZ, and Liu, CM</p>

    <p>          J Biotechnol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049658&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049658&amp;dopt=abstract</a> </p><br />

    <p>8.     59235   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Stereoselective Synthesis of 2&#39;-C-Methyl-cyclopropyl-Fused Carbanucleosides as Potential Anti-HCV Agents</p>

    <p>          Lee, JA, Kim, HO, Tosh, DK, Moon, HR, Kim, S, and Jeong, LS</p>

    <p>          Org Lett <b>2006</b>.  8(22): 5081-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17048848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17048848&amp;dopt=abstract</a> </p><br />

    <p>9.     59236   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Binding interaction of quercetin-3-beta-galactoside and its synthetic derivatives with SARS-CoV 3CL(pro): Structure-activity relationship studies reveal salient pharmacophore features</p>

    <p>          Chen, L, Li, J, Luo, C, Liu, H, Xu, W, Chen, G, Liew, OW, Zhu, W, Puah, CM, Shen, X, and Jiang, H</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046271&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046271&amp;dopt=abstract</a> </p><br />

    <p>10.   59237   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of novel iso-d-2&#39;,3&#39;-dideoxy-3&#39;-fluorothianucleoside derivatives</p>

    <p>          Kim, KR, Moon, HR, Park, AY, Chun, MW, and Jeong, LS</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046264&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046264&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59238   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Clinical evaluation (Phase I) of a human monoclonal antibody against hepatitis C virus: Safety and antiviral activity</p>

    <p>          Galun, Eithan, Terrault, Norah A, Eren, Rachel, Zauberman, Arie, Nussbaum, Ofer, Terkieltaub, Dov, Zohar, Meirav, Buchnik, Rachel, Ackerman, Zvi, and Safadi, Rifaat</p>

    <p>          Journal of Hepatology <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4M57P2H-1/2/ff30f9553f691a1ba55b1cfbce303130">http://www.sciencedirect.com/science/article/B6W7C-4M57P2H-1/2/ff30f9553f691a1ba55b1cfbce303130</a> </p><br />

    <p>12.   59239   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Acyclic nucleoside phosphonates: Past, present and future Bridging chemistry to HIV, HBV, HCV, HPV, adeno-, herpes-, and poxvirus infections: The phosphonate bridge</p>

    <p>          De Clercq, E</p>

    <p>          Biochem Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17045247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17045247&amp;dopt=abstract</a> </p><br />

    <p>13.   59240   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Multi-Drug Resistance Conferred by Novel DNA Polymerase Mutations in Human Cytomegalovirus Isolates</p>

    <p>          Scott, GM, Weinberg, A, Rawlinson, WD, and Chou, S</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17043128&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17043128&amp;dopt=abstract</a> </p><br />

    <p>14.   59241   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synthesis of Tricarbonyl Rhenium and Technetium complexes of a 5&#39;-carboxamide 5-Ethyl-2&#39;-Deoxyuridine for Selective Inhibition of Herpes Simplex Virus Thymidine Kinase 1</p>

    <p>          Desbouis, D, Schubiger, PA, and Schibli, R</p>

    <p>          Journal of Organometallic Chemistry <b>2006</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGW-4M3R2CW-4/2/463a55d997aa7e30eae91e83a58d7f49">http://www.sciencedirect.com/science/article/B6TGW-4M3R2CW-4/2/463a55d997aa7e30eae91e83a58d7f49</a> </p><br />

    <p>15.   59242   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Short communication inhibitory activity of 4-[(1,2-dihydro-2-oxo-3H-indol-3-ylidene)amino]-N-(4,6-dimethylpyrimidin-2 -yl) benzenesulphonamide and its derivatives against orthopoxvirus replication in vitro</p>

    <p>          Selvam, P, Murugesh, N, Chandramohan, M, Keith, KA, and Kern, ER</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(2): 107-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042332&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042332&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59243   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Design and synthesis of novel bis(L-amino acid) ester prodrugs of 9-[2-(phosphonomethoxy)ethyl]adenine (PMEA) with improved anti-HBV activity</p>

    <p>          Fu, Xiaozhong, Jiang, Saihong, Li, Chuan, Xin, Jian, Yang, Yushe, and Ji, Ruyun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M3H7HF-8/2/ab00665c9c62e12957e6c2622a031bbd">http://www.sciencedirect.com/science/article/B6TF9-4M3H7HF-8/2/ab00665c9c62e12957e6c2622a031bbd</a> </p><br />

    <p>17.   59244   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Anti-herpes simplex virus activities of two novel disulphated cyclitols</p>

    <p>          Ekblad, M, Bergstrom, T, Banwell, MG, Bonnet, M, Renner, J, Ferro, V, and Trybala, E</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(2): 97-106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042331&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042331&amp;dopt=abstract</a> </p><br />

    <p>18.   59245   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synergistic combination effect of cidofovir and idoxuridine on vaccinia virus replication</p>

    <p>          Remichkova, M, Petrov, N, and Galabov, AS</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(2): 53-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042327&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042327&amp;dopt=abstract</a> </p><br />

    <p>19.   59246   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Inhibition and escape of SARS-CoV treated with antisense morpholino oligomers</p>

    <p>          Neuman, BW, Stein, DA, Kroeker, AD, Moulton, HM, Bestwick, RK, Iversen, PL, and Buchmeier, MJ</p>

    <p>          Adv Exp Med Biol <b>2006</b>.  581: 567-71</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17037599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17037599&amp;dopt=abstract</a> </p><br />

    <p>20.   59247   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synergistic inhibition of SARS-coronavirus replication by type I and type II IFN</p>

    <p>          Mossel, EC, Sainz, B Jr, Garry, RF, and Peters, CJ</p>

    <p>          Adv Exp Med Biol <b>2006</b>.  581: 503-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17037585&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17037585&amp;dopt=abstract</a> </p><br />

    <p>21.   59248   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          In-vitro and in-vivo efficacy of influenza neuraminidase inhibitors</p>

    <p>          Mendel, DB and Roberts, NA</p>

    <p>          Curr Opin Infect Dis <b>1998</b>.  11(6): 727-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035750&amp;dopt=abstract</a> </p><br />

    <p>22.   59249   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of dihydrofuran-fused perhydrophenanthrenes as a new anti-influenza agent having novel structural characteristic</p>

    <p>          Matsuya, Y, Sasaki, K, Ochiai, H, and Nemoto, H</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035034&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035034&amp;dopt=abstract</a> </p><br />

    <p>23.   59250   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro-Anti-hepatitis B Virus Activities of Several Ethyl 5-Hydroxy-1H-indole-3-carboxylates</p>

    <p>          ZHAO, Chun-shen, ZHAO, Yan-fang, CHAI, Hui-fang, and GONG, Ping</p>

    <p>          Chemical Research in Chinese Universities <b>2006</b>.  22(5): 577-583</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B82XH-4M3CHK6-8/2/c7b23f932147710284b73c9cd4647d99">http://www.sciencedirect.com/science/article/B82XH-4M3CHK6-8/2/c7b23f932147710284b73c9cd4647d99</a> </p><br />

    <p>24.   59251   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Human papillomaviruses target the double-stranded RNA protein kinase pathway</p>

    <p>          Hebner, CM, Wilson, R, Rader, J, Bidder, M, and Laimins, LA</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 11): 3183-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030851&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030851&amp;dopt=abstract</a> </p><br />

    <p>25.   59252   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Rapid Decline of Viral RNA in Hepatitis C Patients Treated With VX-950: A Phase Ib, Placebo-Controlled, Randomized Study</p>

    <p>          Reesink, HW, Zeuzem, S, Weegink, CJ, Forestier, N, van, Vliet A, van, de Wetering de Rooij J, McNair, L, Purdy, S, Kauffman, R, Alam, J, and Jansen, PL</p>

    <p>          Gastroenterology <b>2006</b>.  131(4): 997-1002</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030169&amp;dopt=abstract</a> </p><br />

    <p>26.   59253   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus production by Boehmeria nivea root extract in HepG2 2.2.15 cells</p>

    <p>          Huang, KL, Lai, YK, Lin, CC, and Chang, JM</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(35): 5721-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17007029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17007029&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>27.   59254   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Small Molecule and Biologic Inhibitors of Hepatitis C Virus: a Symbiotic Approach</p>

    <p>          Del Vecchio, A. and Sarisky, R.</p>

    <p>          Mini-Reviews in Medicinal Chemistry <b>2006</b>.  6(11): 1263-1268</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240918000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240918000009</a> </p><br />

    <p>28.   59255   DMID-LS-130; PUBMED-DMID-10/23/2006</p>

    <p class="memofmt1-2">          SARS-CoV nucleocapsid protein binds to hUbc9, a ubiquitin conjugating enzyme of the sumoylation system</p>

    <p>          Fan, Z, Zhuo, Y, Tan, X, Zhou, Z, Yuan, J, Qiang, B, Yan, J, Peng, X, and Gao, GF</p>

    <p>          J Med Virol <b>2006</b>.  78(11): 1365-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16998888&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16998888&amp;dopt=abstract</a> </p><br />

    <p>29.   59256   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Evaluation of Replication Capacity and Cross-Resistance of Hbv Drug Resistant Mutants</p>

    <p>          Villet, S. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300069">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300069</a> </p><br />

    <p>30.   59257   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Combination Therapy With Antiviral Drugs and Immunomodulation Against Chronic Hepatitis B Virus Infection: Evaluation in the Woodchuck Model</p>

    <p>          Lu, M. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S30-S31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300090">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300090</a> </p><br />

    <p>31.   59258   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Alpha-Glucosidase Inhibitors Prevent the Assembly and Induce a Reduction of Hcv Infectivity</p>

    <p>          Chapel, C. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S110</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300332">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300332</a> </p><br />

    <p>32.   59259   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Hbv Specific Acyclic Pyrimidine Nucleosides: Synthesis and Anti-Hbv Activity of 5-and/or 6-Substituted Analogs of 1-[(2-Hydroxyethoxy)Methyl] Uracils</p>

    <p>          Kumar, R. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36: S170</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300518">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300518</a> </p><br />

    <p>33.   59260   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          An Efficient Synthesis of 3-Benzyl-2h-Chromenes as Potential Antipicornavirus Agents</p>

    <p>          Desideri, N.</p>

    <p>          Letters in Organic Chemistry <b>2006</b>.  3(7): 546-548</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240673100011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240673100011</a> </p><br />

    <p>34.   59261   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Synthesis of[no-break space]stilbene derivatives with inhibition of[no-break space]SARS coronavirus replication</p>

    <p>          Li, Yue-Qing, Li, Ze-Lin, Zhao, Wei-Jie, Wen, Rui-Xing, Meng, Qing-Wei, and Zeng, Yi</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(9): 1084-1089</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4KH47JY-1/2/61d4fff63eaa241bdf91ceb7c74f5fa5">http://www.sciencedirect.com/science/article/B6VKY-4KH47JY-1/2/61d4fff63eaa241bdf91ceb7c74f5fa5</a> </p><br />

    <p>35.   59262   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Entecavir: a Review of Its Use in Chronic Hepatitis B</p>

    <p>          Robinson, D., Scott, L., and Plosker, G.</p>

    <p>          Drugs <b>2006</b>.  66 (12): 1605-1622</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240778500009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240778500009</a> </p><br />

    <p>36.   59263   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Design and synthesis of 7H-pyrrolo[2,3-d]pyrimidines as focal adhesion kinase inhibitors. Part 2</p>

    <p>          Choi, Ha-Soon, Wang, Zhicheng, Richmond, Wendy, He, Xiaohui, Yang, Kunyong, Jiang, Tao, Karanewsky, Donald, Gu, Xiang-ju, Zhou, Vicki, and Liu, Yi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(10): 2689-2692</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4JF8H86-3/2/47400976504f9ba99e43470b815481f3">http://www.sciencedirect.com/science/article/B6TF9-4JF8H86-3/2/47400976504f9ba99e43470b815481f3</a> </p><br />

    <p>37.   59264   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Inhibition of the Replication of Hepatitis B Virus in Vitro by Emodin</p>

    <p>          Dang, S. <i>et al.</i></p>

    <p>          Medical Science Monitor <b>2006</b>.  12(9): BR302-BR306</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240641400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240641400003</a> </p><br />

    <p>38.   59265   DMID-LS-130; EMBASE-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Design and synthesis of 7H-pyrrolo[2,3-d]pyrimidines as focal adhesion kinase inhibitors. Part 1</p>

    <p>          Choi, Ha-Soon, Wang, Zhicheng, Richmond, Wendy, He, Xiaohui, Yang, Kunyong, Jiang, Tao, Sim, Taebo, Karanewsky, Donald, Gu, Xiang-ju, and Zhou, Vicki</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(8): 2173-2176</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J6169M-9/2/e420c472a2fe6a06573857ea9d2b0bb2">http://www.sciencedirect.com/science/article/B6TF9-4J6169M-9/2/e420c472a2fe6a06573857ea9d2b0bb2</a> </p><br />

    <p>39.   59266   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Mutations Conferring Zanamivir Resistance in Human Influenza Virus N2 Neuraminidases Compromise Virus Fitness and Are Not Stably Maintained in Vitro</p>

    <p>          Zurcher, T. <i>et al.</i></p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  58(4): 723-732</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240587400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240587400003</a> </p><br />
    <br clear="all">

    <p>40.   59267   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Syntheses of Triazole-Modified Zanamivir Analogues Via Click Chemistry and Anti-Aiv Activities</p>

    <p>          Li, J. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(19): 5009-5013</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615400005</a> </p><br />

    <p>41.   59268   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Acidform Inactivates Herpes Simplex Virus and Prevents Genital Herpes in a Mouse Model: Optimal Candidate for Microbicide Combinations</p>

    <p>          Tuyama, A. <i>et al.</i></p>

    <p>          Journal of Infectious Diseases <b>2006</b>.  194(6): 795-803</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240316600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240316600011</a> </p><br />

    <p>42.   59269   DMID-LS-130; WOS-DMID-10/23/2006</p>

    <p class="memofmt1-2">          Aryl Furano Pyrimidines: the Most Potent and Selective Anti-Vzv Agents Reported to Date</p>

    <p>          Mcguigan, C. and Balzarini, J.</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 149-153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
