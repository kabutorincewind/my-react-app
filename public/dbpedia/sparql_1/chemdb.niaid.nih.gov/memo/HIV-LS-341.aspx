

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-341.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="W/u0U7gw9ndAMbKpkvokfoJNCYHPGbmPJ0U64Vgkdox0AaotsSMr+U6b+znCSqaLUrm7C+dT4i+FL+K8xuKgqNszhBU88UA6TzH8uk/dEuBoVEDgiPvPuaA/rbc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0BB18E57" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-341-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48432   HIV-LS-341; PUBMED-HIV-2/6/2006</p>

    <p class="memofmt1-2">          Probabilistic Neural Network Model for the In Silico Evaluation of Anti-HIV Activity and Mechanism of Action</p>

    <p>          Vilar, S, Santana, L, and Uriarte, E</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 1118-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451076&amp;dopt=abstract</a> </p><br />

    <p>2.     48433   HIV-LS-341; PUBMED-HIV-2/6/2006</p>

    <p class="memofmt1-2">          Structure-Based Design, Synthesis, and Biological Evaluation of Novel Inhibitors of Human Cyclophilin A</p>

    <p>          Guichou, JF, Viaud, J, Mettling, C, Subra, G, Lin, YL, and Chavanieu, A</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 900-910</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451056&amp;dopt=abstract</a> </p><br />

    <p>3.     48434   HIV-LS-341; PUBMED-HIV-2/6/2006</p>

    <p class="memofmt1-2">          CD4-Specific Transgenic Expression of Human Cyclin T1 Markedly Increases Human Immunodeficiency Virus Type 1 (HIV-1) Production by CD4+ T Lymphocytes and Myeloid Cells in Mice Transgenic for a Provirus Encoding a Monocyte-Tropic HIV-1 Isolate</p>

    <p>          Sun, J, Soos, T, Kewalramani, VN, Osiecki, K, Zheng, JH, Falkin, L, Santambrogio, L, Littman, DR, and Goldstein, H</p>

    <p>          J Virol <b>2006</b>.  80(4): 1850-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439541&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439541&amp;dopt=abstract</a> </p><br />

    <p>4.     48435   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Kalata B8, a novel antiviral circular protein, exhibits conformational flexibility in the cystine knot motif</p>

    <p>          Daly, Norelle L, Clark, Richard J, Plan, Manuel R, and Craik, David J</p>

    <p>          Biochemical Journal <b>2006</b>.  393(3): 619-626</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48436   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Quinoline derivatives and other compounds, pharmaceutical compositions, and methods for inhibiting HIV infectivity</p>

    <p>          Li, Limin</p>

    <p>          PATENT:  WO <b>2006004608</b>  ISSUE DATE:  20060112</p>

    <p>          APPLICATION: 2005  PP: 30 pp.</p>

    <p>          ASSIGNEE:  (Functional Genetics, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     48437   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Uracil DNA glycosylase is dispensable for human immunodeficiency virus type 1 replication and does not contribute to the antiviral effects of the cytidine deaminase Apobec3G</p>

    <p>          Kaiser, Shari M and Emerman, Michael</p>

    <p>          Journal of Virology <b>2006</b>.  80(2): 875-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     48438   HIV-LS-341; PUBMED-HIV-2/6/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of beta-D-3&#39;-azido-2&#39;,3&#39;-unsaturated nucleosides and beta-D-3&#39;-azido-3&#39;-deoxyribofuranosylnucleosides</p>

    <p>          Gadthula, S, Chu, CK, and Schinazi, RF</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(10-12): 1707-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438043&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438043&amp;dopt=abstract</a> </p><br />

    <p>8.     48439   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Anti-retroviral moronic acid derivatives</p>

    <p>          Lee, Kuo-Hsiung, Chang, Fang-Rong, Sakurai, Yojiro, and Chen, Chin Ho</p>

    <p>          PATENT:  WO <b>2006001964</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (The University of North Carolina At Chapel Hill, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48440   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Compounds for immunopotentiation</p>

    <p>          Valiante, Nicholas and Xu, Feng</p>

    <p>          PATENT:  WO <b>2006002422</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 178 pp.</p>

    <p>          ASSIGNEE:  (Chiron Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48441   HIV-LS-341; PUBMED-HIV-2/6/2006</p>

    <p class="memofmt1-2">          In Vitro Antiretroviral Activity and In Vitro Toxicity Profile of SPD754, a New Deoxycytidine Nucleoside Reverse Transcriptase Inhibitor for Treatment of Human Immunodeficiency Virus Infection</p>

    <p>          Gu, Z, Allard, B, de, Muys JM, Lippens, J, Rando, RF, Nguyen-Ba, N, Ren, C, McKenna, P, Taylor, DL, and Bethell, RC</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(2): 625-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436719&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436719&amp;dopt=abstract</a> </p><br />

    <p>11.   48442   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Methods and compositions for enhancement of immunity by in vivo depletion of immunosuppressive cell activity</p>

    <p>          Vieweg, Johannes W</p>

    <p>          PATENT:  US <b>2006002932</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005-13011  PP: 40 pp.</p>

    <p>          ASSIGNEE:  (Duke University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   48443   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          Sigma-2 receptor agonists and their use in the treatment of HIV or other immunodeficiency virus infection</p>

    <p>          Bowen, Wayne D, Crawford, Keith W, and Hildreth, James E</p>

    <p>          PATENT:  US <b>2006004036</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005-50725  PP: 27 pp., Cont.-in-part of Appl. No. PCT/US04-000739.</p>

    <p>          ASSIGNEE:  (The United States of America, Per Secy. Dhhs Nih USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48444   HIV-LS-341; SCIFINDER-HIV-1/31/2006</p>

    <p class="memofmt1-2">          An inhibitor of c-Jun N-terminal kinases (CEP-11004) counteracts the anti-HIV-1 action of trichosanthin</p>

    <p>          Ouyang, Dong-Yun, Chan, Herman, Wang, Yuan-Yuan, Huang, Hai, Tam, Siu-Cheung, and Zheng, Yong-Tang</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  339(1): 25-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48445   HIV-LS-341; WOS-HIV-1/29/2006</p>

    <p class="memofmt1-2">          Identification of peptides from human pathogens able to cross-activate an HIV-1-gag-specific CD4(+) T cell clone</p>

    <p>          Venturini, S, Allicotti, G, Zhao, YD, Simon, R, Burton, DR, Pinilla, C, and Poignard, P</p>

    <p>          EUROPEAN JOURNAL OF IMMUNOLOGY <b>2006</b>.  36(1): 27-36, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234617500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234617500004</a> </p><br />

    <p>15.   48446   HIV-LS-341; WOS-HIV-1/29/2006</p>

    <p class="memofmt1-2">          Efficient intervention of growth and infiltration of primary adult T-cell leukemia cells by an HIV protease inhibitor, ritonavir</p>

    <p>          Dewan, MZ, Uchihara, J, Terashima, K, Honda, M, Sata, T, Ito, M, Fujii, N, Uozumi, K, Tsukasaki, K, Tomonaga, M, Kubuki, Y, Okayama, A, Toi, M, Mori, N, and Yamamoto, N</p>

    <p>          BLOOD <b>2006</b>.  107(2): 716-724, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234549300050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234549300050</a> </p><br />

    <p>16.   48447   HIV-LS-341; WOS-HIV-1/29/2006</p>

    <p class="memofmt1-2">          Smilaxin, a novel protein with immunostimulatory, antiproliferative, and HIV-1-reverse transcriptase inhibitory activities from fresh Smilax glabra rhizomes</p>

    <p>          Chu, KT and Ng, TB</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2006</b>.  340(1): 118-124, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234640600018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234640600018</a> </p><br />

    <p>17.   48448   HIV-LS-341; WOS-HIV-1/29/2006</p>

    <p class="memofmt1-2">          Hypertriglyceridemia and hypercholesterolemia in human immunodeficiency virus-1-infected children treated with protease inhibitors</p>

    <p>          Santos, FS, Rangel, LGG, Saucedo, GP, Rosales, GV, and Novales, MGM</p>

    <p>          ARCHIVES OF MEDICAL RESEARCH <b>2006</b>.  37(1): 129-132, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234609100020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234609100020</a> </p><br />
    <br clear="all">

    <p>18.   48449   HIV-LS-341; WOS-HIV-1/29/2006</p>

    <p class="memofmt1-2">          Inhibition of drug-resistant HIV-1 by RNA interference</p>

    <p>          Huelsmann, PM, Rauch, P, Allers, K, John, MJ, and Metzner, KJ</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  69(1): 1-8, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234525900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234525900001</a> </p><br />

    <p>19.   48450   HIV-LS-341; WOS-HIV-2/5/2006</p>

    <p class="memofmt1-2">          Synthesis of tetrahydrobenzazepinesulfonamides and their rearrangement to diarylsulfones</p>

    <p>          Ren, H, Zanger, M, and McKee, JR</p>

    <p>          SYNTHETIC COMMUNICATIONS <b>2006</b>.  36(3): 355-363, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234819100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234819100013</a> </p><br />

    <p>20.   48451   HIV-LS-341; WOS-HIV-2/5/2006</p>

    <p><b>          APOBEC3A and APOBEC3B are potent inhibitors of LTR-retrotransposon function in human cells</b> </p>

    <p>          Bogerd, HP, Wiegand, HL, Doehle, BP, Lueders, KK, and Cullen, BR</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2006</b>.  34(1): 89-95, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234782300016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234782300016</a> </p><br />

    <p>21.   48452   HIV-LS-341; WOS-HIV-2/5/2006</p>

    <p class="memofmt1-2">          A model predictive control based scheduling method for HIV therapy</p>

    <p>          Zurakowski, R and Teel, AR</p>

    <p>          JOURNAL OF THEORETICAL BIOLOGY <b>2006</b>.  238(2): 368-382, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234770700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234770700012</a> </p><br />

    <p>22.   48453   HIV-LS-341; WOS-HIV-2/5/2006</p>

    <p class="memofmt1-2">          Variations in plasmacytoid dendritic cell (PDC) and myeloid dendritic cell (MDC) levels in HIV-infected subjects on and off antiretroviral therapy</p>

    <p>          Schmidt, B, Fujimura, SH, Martin, JN, and Levy, JA</p>

    <p>          JOURNAL OF CLINICAL IMMUNOLOGY <b>2006</b>.  26(1): 55-64, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234730000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234730000007</a> </p><br />

    <p>23.   48454   HIV-LS-341; WOS-HIV-2/5/2006</p>

    <p class="memofmt1-2">          HIV-1 protease inhibitors and cytomegalovirus vMIA induce mitochondrial fragmentation without triggering apoptosis</p>

    <p>          Roumier, T, Szabadkai, G, Simoni, AM, Perfettini, JL, Paulau, AL, Castedo, M, Metivier, D, Badley, A, Rizzuto, R, and Kroemer, G</p>

    <p>          CELL DEATH AND DIFFERENTIATION <b>2006</b>.  13(2): 348-351, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234678800017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234678800017</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
