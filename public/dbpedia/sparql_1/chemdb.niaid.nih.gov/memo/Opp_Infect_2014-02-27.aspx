

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-02-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CbKuZfsyHTKYFjsIYtNyHitSaMoi21Kq3B/NkA/9uFz6CmQ+L8TN3qo9tT9p0XrrdNzfsm9vrMTGA1hBx7CrC+/tSooaasfV6f7wGQzHhyjitnqhtdOC/rmhm2o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="11D2C00C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: February 14 - February 27, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24534840">Antimicrobial Activity of the Marine Alkaloids, Clathrodin and Oroidin, and their Synthetic Analogues.</a> Zidar, N., S. Montalvao, Z. Hodnik, D.A. Nawrot, A. Zula, J. Ilas, D. Kikelj, P. Tammela, and L.P. Masic. Marine Drugs, 2014. 12(2): p. 940-963. PMID[24534840].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23948674">Avocado Roots Treated with Salicylic acid Produce Phenol-2,4-bis (1,1-dimethylethyl), a Compound with Antifungal Activity.</a> Rangel-Sanchez, G., E. Castro-Mercado, and E. Garcia-Pineda.Journal of Plant Physiology, 2014. 171(3-4): p. 189-198. PMID[23948674].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24468413">Synthesis and Antifungal Activity of ASP9726, a Novel Echinocandin with Potent Aspergillus Hyphal Growth Inhibition.</a> Morikawa, H., M. Tomishima, N. Kayakiri, T. Araki, D. Barrett, S. Akamatsu, S. Matsumoto, S. Uchida, T. Nakai, S. Takeda, and K. Maki. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(4): p. 1172-1175. PMID[24468413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24496121">Catalytic Regioselective Synthesis of Pyrazole Based Pyrido[2,3-d]pyrimidine-diones and their Biological Evaluation.</a> Satasia, S.P., P.N. Kalaria, and D.K. Raval. Organic &amp; Biomolecular Chemistry, 2014. 12(11): p. 1751-1758. PMID[24496121].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24231338">In Vitro and in Silico Antimalarial Activity of 2-(2-Hydrazinyl)thiazole Derivatives.</a>Makam, P., P.K. Thakur, and T. Kannan. European Journal of Pharmaceutical Sciences, 2014. 52: p. 138-145. PMID[24231338].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24552985">Synthesis, Biological Evaluation and Structure-Activity Relationships of New Quinoxaline Derivatives as anti-Plasmodium falciparum Agents.</a>Gil, A., A. Pabon, S. Galiano, A. Burguete, S. Perez-Silanes, E. Deharo, A. Monge, and I. Aldana. Molecules, 2014. 19(2): p. 2166-2180. PMID[24552985].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24468411">Synthesis of Halogenated 4-Quinolones and Evaluation of Their Antiplasmodial Activity.</a>Vandekerckhove, S., T. Desmet, H.G. Tran, C. de Kock, P.J. Smith, K. Chibale, and M. D&#39;Hooghe. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(4): p. 1214-1217. PMID[24468411].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24472146">Antimycobacterial Evaluation of Novel Hybrid Arylidene thiazolidine-2,4-diones.</a>Ponnuchamy, S., S. Kanchithalaivan, R. Ranjith Kumar, M. Ashraf Ali, and T. Soo Choon. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(4): p. 1089-1093. PMID[24472146].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24475925">Structure and Inhibition of Tuberculosinol Synthase and Decaprenyl diphosphate Synthase from Mycobacterium tuberculosis.</a> Chan, H.C., X. Feng, T.P. Ko, C.H. Huang, Y. Hu, Y. Zheng, S. Bogue, C. Nakano, T. Hoshino, L. Zhang, P. Lv, W. Liu, D.C. Crick, P.H. Liang, A.H. Wang, E. Oldfield, and R.T. Guo. Journal of the American Chemical Society, 2014. 136(7): p. 2892-2896. PMID[24475925]. <b>[PubMed]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24461562">Synthesis of a C-phosphonate Mimic of Maltose-1-phosphate and Inhibition Studies on Mycobacterium tuberculosis GLGE.</a> Veleti, S.K., J.J. Lindenberger, D.R. Ronning, and S.J. Sucheck. Bioorganic &amp; Medicinal Chemistry, 2014. 22(4): p. 1404-1411. PMID[24461562].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p> 

    <p class="plaintext">11<a href="http://www.ncbi.nlm.nih.gov/pubmed/24566322">. Synthetic Fosmidomycin Analogues with Altered Chelating Moieties Do Not Inhibit 1-Deoxy-d-xylulose 5-phosphate Reductoisomerase or Plasmodium falciparum Growth in Vitro.</a> Chofor, R., M.D. Risseeuw, J. Pouyez, C. Johny, J. Wouters, C.S. Dowd, R.D. Couch, and S. Van Calenbergh. Molecules, 2014. 19(2): p. 2571-2587. PMID[24566322].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24440298">Virtual Screening Reveals Allosteric Inhibitors of the Toxoplasma gondii Thymidylate Synthase-dihydrofolate Reductase.</a> Sharma, H., M.J. Landau, T.J. Sullivan, V.P. Kumar, M.K. Dahlgren, W.L. Jorgensen, and K.S. Anderson. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(4): p. 1232-1235. PMID[24440298].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0214-022714.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330311500084">An in Vitro Study of the Antimicrobial Effects of Indigo naturalis Prepared from Strobilanthes formosanus Moore.</a>Chiang, Y.R., A. Li, Y.L. Leu, J.Y. Fang, and Y.K. Lin. Molecules, 2013. 18(11): p. 14381-14396. ISI[000330311500084].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329769300008">Antibacterial Potential of Some Medicinal Plants of the Cordillera Region, Philippines.</a> Gutierrezi, R.M., R. Baculi, N. Pastor, T. Puma-at, and T. Balangcod. Indian Journal of Traditional Knowledge, 2013. 12(4): p. 630-637. ISI[000329769300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330055300001">Antifungal Susceptibility and Growth Inhibitory Response of Oral Candida Species to Brucea javanica Linn. Extract.</a> Nordin, M.A.F., W. Harun, and F.A. Razak. BMC Complementary and Alternative Medicine, 2013. 13: p. 342. ISI[000330055300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330099500017">Discovery of Inhibitors of Bacillus anthracis Primase DNAG.</a>Biswas, T., K.D. Green, S. Garneau-Tsodikova, and O.V. Tsodikov. Biochemistry, 2013. 52(39): p. 6905-6910. ISI[000330099500017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329862500191">Gemini Alkyldeoxy-d-glucitolammonium Salts as Modern Surfactants and Microbiocides: Synthesis, Antimicrobial and Surface Activity, Biodegradation.</a>Brycki, B. and A. Szulc. Plos One, 2014. 9(1): p. e84936. ISI[000329862500191].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330129900020">In Vitro Effect of Antifungal Fractions from the Plants Baccharis glutinosa and Jacquinia macrocarpa on Chitin and beta-1,3-Glucan Hydrolysis of Maize Phytopathogenic Fungi and on the Fungal beta-1,3-Glucanase and Chitinase Activities.</a> Buitimea-Cantua, G.V., E.C. Rosas-Burgos, F.J. Cinco-Moroyoqui, A. Burgos-Hernandez, M. Plascencia-Jatomea, M.O. Cortez-Rocha, and J.C. Galvez-Ruiz. Journal of Food Safety, 2013. 33(4): p. 526-535. ISI[000330129900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329581100058">The Natural Diyne-furan fatty acid EV-086 is an Inhibitor of Fungal Delta-9 fatty acid Desaturation with Efficacy in a Model of Skin Dermatophytosis.</a>Knechtle, P., M. Diefenbacher, K.B.V. Greve, F. Brianza, C. Folly, H. Heider, M.A. Lone, L.S. Long, J.P. Meyer, P. Roussel, M.A. Ghannoum, R. Schneiter, and A.S. Sorensen. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 455-466. ISI[000329581100058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0214-022714.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
