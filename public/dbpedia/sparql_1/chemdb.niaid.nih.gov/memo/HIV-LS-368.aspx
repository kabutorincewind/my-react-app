

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-368.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fbdWmDLNZBBs4N+fnKNr8WZMFKx0mO20Y4q7zN477noxeWCkcuQGrK/zVgAIa7/rr78W9NQ4U/D/wii6Ti1SVGXoWnEIWkL9WnDzkKWKfbjF92Ekccil8sM/NdQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D54A31F5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-368--MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60171   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Target Recognition by Catechols and beta-Ketoenols: Potential Contribution of Hydrogen Bonding and Mn/Mg Chelation to HIV-1 Integrase Inhibition</p>

    <p>          Tchertanov, L and Mouscadet, JF</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17302399&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17302399&amp;dopt=abstract</a> </p><br />

    <p>2.     60172   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Evaluation of the HIV-1 reverse transcriptase inhibitory properties of extracts from some medicinal plants in Kenya</p>

    <p>          Rukunga, GM, Kofi-Tsekpo, MW, Kurokawa, M, Kageyama, S, Mungai, GM, Muli, JM, Tolo, FM, Kibaya, RM, Muthaura, CN, Kanyara, JN, Tukei, PM, and Shiraki, K</p>

    <p>          Afr J Health Sci <b>2002</b>.  9(1-2): 81-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298148&amp;dopt=abstract</a> </p><br />

    <p>3.     60173   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Newly Synthesized APOBEC3G Is Incorporated into HIV Virions, Inhibited by HIV RNA, and Subsequently Activated by RNase H</p>

    <p>          Soros, VB, Yonemoto, W, and Greene, WC</p>

    <p>          PLoS Pathog <b>2007</b>.  3(2): e15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17291161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17291161&amp;dopt=abstract</a> </p><br />

    <p>4.     60174   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Characterization of novel nonnucleoside reverse transcriptase (RT) inhibitor resistance mutations at residues 132 and 135 in the 51kDa subunit of HIV-1 RT</p>

    <p>          Nissley, DV, Radzio, J, Ambrose, Z, Sheen, CW, Hamamouch, N, Moore, KL, Tachedjian, G, and Sluis-Cremer, N</p>

    <p>          Biochem J <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17286555&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17286555&amp;dopt=abstract</a> </p><br />

    <p>5.     60175   HIV-LS-368; EMBASE-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Spirodiketopiperazine-based CCR5 antagonists: Lead optimization from biologically active metabolite</p>

    <p>          Nishizawa, Rena, Nishiyama, Toshihiko, Hisaichi, Katsuya, Matsunaga, Naoki, Minamoto, Chiaki, Habashita, Hiromu, Takaoka, Yoshikazu, Toda, Masaaki, Shibayama, Shiro, Tada, Hideaki, Sagawa, Kenji, Fukushima, Daikichi, Maeda, Kenji, and Mitsuya, Hiroaki</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(3): 727-731</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M7YK0D-2/2/a9e216a52cb272d18c810ebf7f223afb">http://www.sciencedirect.com/science/article/B6TF9-4M7YK0D-2/2/a9e216a52cb272d18c810ebf7f223afb</a> </p><br />

    <p>6.     60176   HIV-LS-368; EMBASE-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Regioselective synthesis and anti-HIV activity of the novel 2- and 4-substituted pyrazolo[4,5-e][1,2,4]thiadiazines</p>

    <p>          Liu, Xin Yong, Yan, Ren Zhang, Chen, Nian Gen, and Xu, Wen Fang</p>

    <p>          Chinese Chemical Letters <b>2007</b>.  18(2): 137-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B8G3X-4MWJ6HC-8/2/22853ef25d75f4bff7aff6b27515791f">http://www.sciencedirect.com/science/article/B8G3X-4MWJ6HC-8/2/22853ef25d75f4bff7aff6b27515791f</a> </p><br />

    <p>7.     60177   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of two glycerolipidic prodrugs of didanosine for direct lymphatic delivery against HIV</p>

    <p>          Lalanne, M, Paci, A, Andrieux, K, Dereuddre-Bosquet, N, Clayette, P, Deroussent, A, Re, M, Vassal, G, Couvreur, P, and Desmaele, D</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276686&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276686&amp;dopt=abstract</a> </p><br />

    <p>8.     60178   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Discovery of small-molecule HIV-1 fusion and integrase inhibitors oleuropein and hydroxytyrosol: Part I. Integrase inhibition</p>

    <p>          Lee-Huang, S, Huang, PL, Zhang, D, Lee, JW, Bao, J, Sun, Y, Chang, YT, Zhang, J, and Huang, PL </p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  354(4): 872-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17275783&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17275783&amp;dopt=abstract</a> </p><br />

    <p>9.     60179   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Discovery of Non-Nucleoside Inhibitors of HIV-1 Reverse Transcriptase Competing with the Nucleotide Substrate</p>

    <p>          Maga, G, Radi, M, Zanoli, S, Manetti, F, Cancio, R, Hubscher, U, Spadari, S, Falciani, C, Terrazas, M, Vilarrasa, J, and Botta, M</p>

    <p>          Angew Chem Int Ed Engl <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17274083&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17274083&amp;dopt=abstract</a> </p><br />

    <p>10.   60180   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Design of second generation HIV-1 integrase inhibitors</p>

    <p>          Deng, J, Dayam, R, Al-Mawsawi, LQ, and Neamati, N</p>

    <p>          Curr Pharm Des  <b>2007</b>.  13(2): 129-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17269923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17269923&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60181   HIV-LS-368; EMBASE-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Quantitative structure-activity relationship studies of HIV-1 integrase inhibition. 1. GETAWAY descriptors</p>

    <p>          Saiz-Urra, Liane, Gonzalez, Maykel Perez, Fall, Yagamare, and Gomez, Generosa</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(1): 64-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4M2WNT6-1/2/e471bb0973750f95408d6ab3a743ede4">http://www.sciencedirect.com/science/article/B6VKY-4M2WNT6-1/2/e471bb0973750f95408d6ab3a743ede4</a> </p><br />

    <p>12.   60182   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type-1 reverse transcriptase by a novel peptide derived from the viral integrase</p>

    <p>          Oz, Gleenberg I, Herschhorn, A, Goldgur, Y, and Hizi, A</p>

    <p>          Arch Biochem Biophys <b>2007</b>.  458(2): 202-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257575&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257575&amp;dopt=abstract</a> </p><br />

    <p>13.   60183   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Novel resistance mechanism of HIV-1 To peptide fusion inhibitors</p>

    <p>          Gupta, N, Vassell, R, Wang, W, He, Y, and Weiss, CD</p>

    <p>          Retrovirology <b>2006</b>.  3 Suppl 1: S86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254216&amp;dopt=abstract</a> </p><br />

    <p>14.   60184   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Antiretroviral activity of aminothiols, WR2721 and WR1065</p>

    <p>          Poirier, MC, Shearer, GM, Hardy, AW, Olivero, OA, Walker, DM, and Walker, VE</p>

    <p>          Retrovirology <b>2006</b>.  3 Suppl 1: P78</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254118&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17254118&amp;dopt=abstract</a> </p><br />

    <p>15.   60185   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Impact of V2 mutations for escape from a potent neutralizing anti-V3 monoclonal antibody during in vitro selection of a primary HIV-1 isolate</p>

    <p>          Shibata, J, Yoshimura, K, Honda, A, Koito, A, Murakami, T, and Matsushita, S</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251298&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251298&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60186   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Novel HIV integrase inhibitors with anti-HIV activity: insights into integrase inhibition from docking studies</p>

    <p>          Cox, AG and Nair, V</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(6): 343-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249248&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249248&amp;dopt=abstract</a> </p><br />

    <p>17.   60187   HIV-LS-368; PUBMED-HIV-2/20/2007</p>

    <p class="memofmt1-2">          The benzamide derivative N-[1-(7-tert-Butyl-1H-indol-3-ylmethyl)-2-(4-cyclopropanecarbonyl-3-methyl -piperazin-1-yl)-2-oxo-ethyl]-4-nitro-benzamide (SP-10) reduces HIV-1 infectivity in vitro by modifying actin dynamics</p>

    <p>          Xu, J, Lecanu, L, Tan, M, Yao, W, Greeson, J, and Papadopoulos, V</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(6): 331-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17249247&amp;dopt=abstract</a> </p><br />

    <p>18.   60188   HIV-LS-368; EMBASE-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis of a series of novel 2&#39;,3&#39;-dideoxy-6&#39;,6&#39;-difluoro-3&#39;-thionucleosides</p>

    <p>          Yue, Xuyi, Wu, Yun-Yun, and Qing, Feng-Ling</p>

    <p>          Tetrahedron <b>2007</b>.  63(7): 1560-1567</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4MNHXY0-4/2/38067b0ac78b738f030a4edebddcb5b2">http://www.sciencedirect.com/science/article/B6THR-4MNHXY0-4/2/38067b0ac78b738f030a4edebddcb5b2</a> </p><br />

    <p>19.   60189   HIV-LS-368; EMBASE-HIV-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-HIV Studies of 2-Adamantyl-Substituted Thiazolidin-4-ones</p>

    <p>          Balzarini, Jan, Orzeszko, Barbara, Maurin, Jan K, and Orzeszko, Andrzej</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  1199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MVVSJF-3/2/89853149879474d616aa88691126c2af">http://www.sciencedirect.com/science/article/B6VKY-4MVVSJF-3/2/89853149879474d616aa88691126c2af</a> </p><br />

    <p>20.   60190   HIV-LS-368; WOS-HIV-2/12/2007</p>

    <p class="memofmt1-2">          Retinoic acid inhibits HIV-1-induced podocyte proliferation through the cAMP pathway</p>

    <p>          He, JCJ, Lu, TC, Fleet, M, Sunamoto, M, Husain, M, Fang, W, Neves, S, Chen, Y, Shankland, S, Iyengar, R, and Klotman, PE</p>

    <p>          JOURNAL OF THE AMERICAN SOCIETY OF NEPHROLOGY <b>2007</b>.  18(1): 93-102, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243537800013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243537800013</a> </p><br />

    <p>21.   60191   HIV-LS-368; WOS-HIV-2/12/2007</p>

    <p class="memofmt1-2">          Structure-activity relationships of cyclic peptide-based chemokine receptor CXCR4 antagonists: Disclosing the importance of side-chain and backbone functionalities</p>

    <p>          Ueda, S, Oishi, S, Wang, ZX, Araki, T, Tamamura, H, Cluzeau, J, Ohno, H, Kusano, S, Nakashima, H, Trent, JO, Peiper, SC, and Fujii, N</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  50(2): 192-198, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243535600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243535600004</a> </p><br />

    <p>22.   60192   HIV-LS-368; WOS-HIV-2/12/2007</p>

    <p class="memofmt1-2">          Quinoline antimalarials as investigational drugs for HIV-1/AIDS: In vitro effects on HIV-1 replication, HIV-1 response to antiretroviral drugs, and intracellular antiretroviral drug concentrations</p>

    <p>          Savarino, A, Lucia, MB, ter Heine, R, Rastrelli, E, Rutella, S, Majori, G, Huitema, A, Boelaert, JR, and Cauda, R</p>

    <p>          DRUG DEVELOPMENT RESEARCH <b>2006</b>.  67(10): 806-817, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243490000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243490000005</a> </p><br />

    <p>23.   60193   HIV-LS-368; WOS-HIV-2/12/2007</p>

    <p class="memofmt1-2">          Structure-guided peptidomimetic design leads to nanomolar beta-hairpin inhibitors of the Tat-TAR interaction of bovine immunodeficiency Virus</p>

    <p>          Athanassiou, Z, Patora, K, Dias, RLA, Moehle, K, Robinson, JA, and Varani, G</p>

    <p>          BIOCHEMISTRY <b>2007</b>.  46(3): 741-751, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243483100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243483100012</a> </p><br />

    <p>24.   60194   HIV-LS-368; WOS-HIV-2/12/2007</p>

    <p class="memofmt1-2">          Potent knock down of HIV-1 replication by targeting HIV-1 Tat/Rev RNA sequences synergistically with catalytic RNA and DNA</p>

    <p>          Sood, V, Unwalla, H, Gupta, N, Chakraborti, S, and Banerjea, AC</p>

    <p>          AIDS <b>2007</b>.  21 (1): 31-40, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243418900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243418900004</a> </p><br />

    <p>25.   60195   HIV-LS-368; WOS-HIV-2/18/2007</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV, and antifungal activity of new benzensulfonamides bearing the 2,5-disubstituted-1,3,4-oxadiazole moiety</p>

    <p>          Zareef, M, Iqbal, R, Al-Masoudi, NA, Zaidi, JH, Arfan, M, and Shahzad, SA</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2007</b>.  182(2): 281-298, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243672900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243672900005</a> </p><br />

    <p>26.   60196   HIV-LS-368; WOS-HIV-2/18/2007</p>

    <p class="memofmt1-2">          Fragment based approach for the investigation of HIV-1 integrase inhibition</p>

    <p>          Polanskia, J, Niedbala, H, Musiola, R, Podeszwa, B, Tabak, D, Palka, A, Mencel, A, Mouscadet, JF, and Le Bret, M</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2007</b>.  4(2): 99-105, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243855800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243855800004</a> </p><br />

    <p>27.   60197   HIV-LS-368; WOS-HIV-2/18/2007</p>

    <p class="memofmt1-2">          The promise of CCR5 antagonists as new therapies for HIV-1</p>

    <p>          Repik, A, Richards, KH, and Clapham, PR</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2007</b>.  8(2): 130-139, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800005</a> </p><br />
    <br clear="all">

    <p>28.   60198   HIV-LS-368; WOS-HIV-2/18/2007</p>

    <p class="memofmt1-2">          CCR5 receptor antagonists: Discovery and SAR study of guanylhydrazone derivatives</p>

    <p>          Wei, RG, Arnaiz, DO, Chou, YL, Davey, D, Dunning, L, Lee, W, Lu, SF, Onuffer, J, Ye, B, and Phillips, G</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2007</b>.  17(1): 231-234, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243630500045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243630500045</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
