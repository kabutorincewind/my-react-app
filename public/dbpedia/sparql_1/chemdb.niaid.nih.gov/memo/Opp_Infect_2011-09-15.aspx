

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MJ+2QaLa5Rh8JgihqcXJqf5mPbkll0+HGXt8eUFTQYJu+W34luQOQl2Qd5bZgqFAA5fDYgEE/10NOOuwLqzDcLKIvYwcgv/Tz2CK+ZzLZhBChCBPHIcscZuOeGU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1C98CD1E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">September 2 - September 15, 2011</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

     <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21905650">Synthesis and Antifungal Activity of Natural Product-Based 6-Alkyl-2,3,4,5-tetrahydropyridines.</a>Dai, L., M.R. Jacob, S.I. Khan, I.A. Khan, A.M. Clark, and X.C. Li, Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21905650].</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
     <p> </p>

     <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21903824">Small Molecule Inhibitors of Biofilm Formation in Laboratory and Clinical Isolates of Candida albicans.</a> Grald, A., P. Yargosz, S. Case, K. Shea, and D.I. Johnson, Journal of Medical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21903824].</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>    
     <p> </p>
     
     <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21893402">In Vitro Antifungal, Anti-elastase and Anti-keratinase Activity of Essential Oils of Cinnamomum-, Syzygium- and Cymbopogon-species against Aspergillus fumigatus and Trichophyton rubrum.</a> Khan, M.S. and I. Ahmad, Phytomedicine, 2011. <b>[Epub ahead of print]</b>; PMID[21893402].</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
     <p> </p>
     
     <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21911344">Sodium butyrate Inhibits Pathogenic Yeast Growth and Enhances the Functions of Macrophages.</a> Nguyen, L.N., L.C. Lopes, R.J. Cordero, and J.D. Nosanchuk, The Journal of Antimicrobial Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21911344].</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
     <p> </p>

     <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21900519">Triazole and Echinocandin Wild-Type MIC Distributions with Epidemiological Cutoff Values for Six Uncommon Species of Candida.</a> Pfaller, M.A., M. Castanheira, D.J. Diekema, S.A. Messer, and R.N. Jones, Journal of Clinical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21900519]</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
     <p> </p>

     <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21911564">Synergistic Interaction of the Triple Combination of Amphotericin B, Ciprofloxacin and Polymorphonuclear Neutrophils against Aspergillus fumigatus.</a> Stergiopoulou, T., J. Meletiadis, T. Seiin, P. Papaioannidou, T.J. Walsh, and E. Roilides, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21911564].</p>
     <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>


    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21898832">Identification and Characterization of Inhibitors of the Aminoglycoside Resistance Acetyltransferase Eis from Mycobacterium tuberculosis.</a> Green, K.D., W. Chen, and S. Garneau-Tsodikova, ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21898832].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>
     
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21895982">Synthesis and Biological Evaluation of Some Novel 1,4-Dihydropyridines as Potential Anti-tubercular Agents.</a> Trivedi, A., D. Dodiya, B. Dholariya, V. Kataria, V. Bhuva, and V. Shah, Chemical Biology and Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21895982].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21905228">Synthesis and Antimalarial Activities of a Diverse Set of Ariazole-Containing Furamidine Analogues.</a> Berger, O., A. Kaniti, C.T. Ba, H. Vial, S.A. Ward, G.A. Biagini, P.G. Bray, and P.M. O&#39;Neill, ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21905228].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21910466">Synthesis and Evaluation of 7-Substituted 4-aminoquinoline Analogs for Antimalarial Activity.</a> Hwang, J.Y., T. Kawasuji, D.J. Lowes, J.A. Clark, M. Connelly, F. Zhu, W.A. Guiguemde, M. Sigal, E.B. Wilson, J.L. Derisi, and R.K. Guy, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21910466].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>


    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21782916">In Vitro Antitrypanosomal and Antileishmanial Activity of Plants Used in Benin in Traditional Medicine and Bio-guided Fractionation of the Most Active Extract.</a> Bero, J., V. Hannaert, G. Chataigne, M.F. Herent, and J. Quetin-Leclercq, Journal of Ethnopharmacology, 2011. 137(2): p. 998-1002; PMID[21782916].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21913331">Identification of Inhibitors of the Leishmania cdc-Related Protein Kinase CRK3.</a>  Cleghorn, L.A., A. Woodland, I.T. Collie, L.S. Torrie, N. Norcross, T. Luksch, C. Mpamhanga, R.G. Walker, J.C. Mottram, R. Brenk, J.A. Frearson, I.H. Gilbert, and P.G. Wyatt, ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21913331].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21907525">Synthesis, Cytotoxicity, and in Vitro Antileishmanial Activity of Mono-t-butyloxycarbonyl-protected diamines.</a> Pinheiro, A.C., M.N. Rocha, P.M. Nogueira, T.C. Nogueira, L.F. Jasmim, M.V. de Souza, and R.P. Soares, Diagnostic Microbiology and Infectious Disease, 2011. <b>[Epub ahead of print]</b>; PMID[21907525].</p>
    <p class="plaintext"><b>[Pubmed]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800027">Discovery of Novel Antitubercular 3a,4-Dihydro-3H-indeno 1,2-c pyrazole-2-carboxamide/carbothioamide Analogues.</a> Ahsan, M.J., J.G. Samy, S. Soni, N. Jain, L. Kumar, L.K. Sharma, H. Yadav, L. Saini, R.G. Kalyansing, N.S. Devenda, R. Prasad, and C.B. Jain, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5259-5261; ISI[000294051800027].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800057">Substitution of the Phosphonic acid and Hydroxamic acid Functionalities of the DXR Inhibitor FR900098: An Attempt to Improve the Activity against Mycobacterium tuberculosis.</a> Andaloussi, M., M. Lindh, C. Bjorkelid, S. Suresh, A. Wieckowska, H. Iyer, A. Karlen, and M. Larhed, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5403-5407; ISI[000294051800057].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800047">Identification of SQ609 as a Lead Compound from a Library of Dipiperidines.</a> Bogatcheva, E., C. Hanrahan, B. Nikonenko, G. de los Santos, V. Reddy, P. Chen, F. Barbosa, L. Einck, C. Nacy, and M. Protopopova, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5353-5357; ISI[000294051800047].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294012800010">Antifungal Activity of Some Cyclooxygenase Inhibitors on Candida albicans: PGE2-dependent Mechanism.</a> de Quadros, A.U., D. Bini, P.A.T. Pereira, E.G. Moroni, and M.C. Monteiro, Folia Microbiologica, 2011. 56(4): p. 349-352; ISI[000294012800010].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293883400067">Light-induced Antifungal Activity of TiO(2) Nanoparticles/ZnO Nanowires.</a> Haghighi, N., Y. Abdi, and F. Haghighi, Applied Surface Science, 2011. 257(23): p. 10096-10100; ISI[000293883400067].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293363000085">Phenoxide Bridged Tetranuclear Co(II), Ni(II), Cu(II) and Zn(II) Complexes: Electrochemical, Magnetic and Antimicrobial Studies.</a> Kamath, A., N.V. Kulkarni, P.P. Netalkar, and V.K. Revankar, Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2011. 79(5): p. 1418-1424; ISI[000293363000085].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293471700012">Morpholino pyrimidinyl acetamides: Design, Green Chemical One-pot Synthesis, and in Vitro Microbiological Evaluation</a>. Kanagarajan, V. and M. Gopalakrishnan, Pharmaceutical Chemistry Journal, 2011. 45(3): p. 170-177; ISI[000293471700012].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294034100002">Effects of Interferon-gamma and Granulocyte Colony-stimulating Factor on Antifungal Activity of Human Polymorphonuclear Neutrophils against Candida albicans Grown as Biofilms or Planktonic Cells.</a> Katragkou, A., M. Simitsopoulou, A. Chatzimoschou, E. Georgiadou, T.J. Walsh, and E. Roilides, Cytokine, 2011. 55(3): p. 330-334; ISI[000294034100002].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294274700020">An Investigation of the Antimicrobial Impact of Drug Combinations against Mycobacterium tuberculosis Strains.</a> Limoncu, M.H., S. Ermertcan, B. Erac, and H. Tasli, Turkish Journal of Medical Sciences, 2011. 41(4): p. 719-724; ISI[000294274700020].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294002400015">Tylosema esculentum Extractives and Their Bioactivity.</a> Mazimba, O., R.R.T. Majinda, C. Modibedi, I.B. Masesane, A. Cencic, and W. Chingwaru, Bioorganic &amp; Medicinal Chemistry, 2011. 19(17): p. 5225-5230; ISI[000294002400015].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293363000041">Co(II), Ni(II) and Cu(II) Complexes with Coumarin-8-yl Schiff-bases: Spectroscopic, in Vitro Antimicrobial, DNA Cleavage and Fluorescence Studies.</a> Patil, S.A., S.N. Unki, A.D. Kulkarni, V.H. Naik, and P.S. Badami, Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2011. 79(5): p. 1128-1136; ISI[000293363000041].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293363000027">Spectral, Structural Elucidation and Coordination Abilities of Co(II) and Mn(II) Coordination Entities of 2,6,11,15-Tetraoxa-9,17-diaza-1,7,1 0,16-(1,2)-tetrabenzenacyclooctadecaphan-8,17-diene.</a> Rajiv, K. and J. Rajni, Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2011. 79(5): p. 1042-1049; ISI[000293363000027].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293929600020">DNA Interaction, Antimicrobial, Electrochemical and Spectroscopic Studies of Metal(II) Complexes with Tridentate heterocyclic Schiff base Derived from 2 &#39;-Methylacetoacetanilide.</a> Raman, N., K. Pothiraj, and T. Baskaran, Journal of Molecular Structure, 2011. 1000(1-3): p. 135-144; ISI[000293929600020].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800094">Synthesis and Biological Evaluation of Substituted 4-Arylthiazol-2-amino Derivatives as Potent Growth Inhibitors of Replicating Mycobacterium tuberculosis H(37)R(V).</a> Roy, K.K., S. Singh, S.K. Sharma, R. Srivastava, V. Chaturvedi, and A.K. Saxena, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5589-5593; ISI[000294051800094].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800070">Design and Synthesis of Spiro Indole-thiazolidine Spiro Indole-pyrans as Antimicrobial Agents.</a> Sakhuja, R., S.S. Panda, L. Khanna, S. Khurana, and S.C. Jain, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5465-5469; ISI[000294051800070].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293469100001">Antimicrobial and Antioxidant Activities of Plants from Northeast of Mexico.</a> Salazar-Aranda, R., L.A. Perez-Lopez, J. Lopez-Arroyo, B.A. Alanis-Garza, and N.W. de Torres, Evidence-Based Complementary and Alternative Medicine, 2011. <b>[Epub ahead of print]</b>; p. 1-6; ISI[000293469100001].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294077800012">Preliminary Studies for a New Antibiotic from the Marine Mollusk Melo melo (Lightfoot, 1786).</a> Sivasubramanian, K., S. Ravichandran, and M. Kumaresan, Asian Pacific Journal of Tropical Medicine, 2011. 4(4): p. 310-314; ISI[000294077800012].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800003">Synthesis of Various 3-Nitropropionamides as Mycobacterium tuberculosis Isocitrate Lyase Inhibitor.</a> Sriram, D., P. Yogeeswari, S. Methuku, D.R.K. Vyas, P. Senthilkumar, M. Alvala, and V.U. Jeankumar, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5149-5154; ISI[000294051800003].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800009">Synthesis and Biological Evaluation of Some Novel N-Aryl-1,4-dihydropyridines as Potential Antitubercular Agents.</a> Trivedi, A.R., D.K. Dodiya, B.H. Dholariya, V.B. Kataria, V.R. Bhuva, and V.H. Shah, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5181-5183; ISI[000294051800009].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0902-091511.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
