

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NguoyjAHZ/xOVyH4ewGFz8BhTcQCrTP7rRhHLdEBDJ58nOn6lcjTN9Pq8q0MsGnxKrNbmh7Sd800aAyDb1n3zGgvhKLx0d93tobjSZMh0Od803t2CmlqI9L0/iE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5B61B60C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  August 5, 2011 - August 18, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21783371">Synthesis, gp120 Binding and anti-HIV Activity of Fatty acid esters of 1,1-Linked disaccharides.</a> Bachan, S., J. Fantini, A. Joshi, H. Garg, and D.R. Mootoo, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 4803-4811; PMID[21783371].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21840903">Stacking of G-quadruplexes: NMR Structure of a G-rich Oligonucleotide with Potential anti-HIV and Anticancer Activity.</a> Do, N.Q., K.W. Lim, M.H. Teo, B. Heddi, and A.T. Phan, Nucleic Acids Research, 2011. <b>[Epub ahead of print]</b>; PMID[21840903].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21778063">Design of HIV-1 Integrase Inhibitors Targeting the Catalytic Domain as well as its Interaction with LEDGF/p75: A Scaffold Hopping Approach Using salicylate and Catechol Groups.</a>  Fan, X., F.H. Zhang, R.I. Al-Safi, L.F. Zeng, Y. Shabaik, B. Debnath, T.W. Sanchez, S. Odde, N. Neamati, and Y.Q. Long, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 4935-4952; PMID[21778063].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763149">Structural Modifications of Quinolone-3-carboxylic acids with anti-HIV Activity.</a> He, Q.Q., S.X. Gu, J. Liu, H.Q. Wu, X. Zhang, L.M. Yang, Y.T. Zheng, and F.E. Chen, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 5039-5045; PMID[21763149].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21827952">Structural Basis of the anti-HIV Activity of the Cyanobacterial Oscillatoria Agardhii Agglutinin.</a> Koharudin, L.M. and A.M. Gronenborn, Structure, 2011. 19(8): p. 1170-1181; PMID[21827952].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21749165">Targeting HIV Entry through Interaction with Envelope Glycoprotein 120 (gp120): Synthesis and Antiviral Evaluation of 1,3,5-Triazines with Aromatic Amino Acids.</a> Lozano, V., L. Aguado, B. Hoorelbeke, M. Renders, M.J. Camarasa, D. Schols, J. Balzarini, A. San-Felix, and M.J. Perez-Perez, Journal of Medicinal Chemistry, 2011. 54(15): p. 5335-5348; PMID[21749165].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21788138">Synthesis and Biological Evaluation of (+/-)-Benzhydrol Derivatives as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Ma, X.D., X. Zhang, S.Q. Yang, H.F. Dai, L.M. Yang, S.X. Gu, Y.T. Zheng, Q.Q. He, and F.E. Chen, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 4704-4709; PMID[21788138].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21827337">Inhibition of Hepatitis B Virus and Human Immunodeficiency Virus (HIV-1) Replication by Warscewiczia coccinea (Vahl) Kl. (Rubiaceae) Ethanol Extract.</a>Quintero, A., R. Fabbro, M. Maillo, M. Barrios, M.B. Milano, A. Fernandez, B. Williams, F. Michelangeli, H.R. Rangel, and F.H. Pujol, Natural Product Research, 2011; PMID[21827337].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21834513">Aryl H-phosphonates 17. (N-Aryl)phosphoramidates of Pyrimidine Nucleoside Analogues- Synthesis, Selected Properties and anti-HIV Activity.</a>Romanowska, J., M. Sobkowski, A. Szymanska-Michalak, K. Kolodziej, A. Dabrowska, A. Lipniacki, A. Piasek, Z.M. Pietrusiewicz, M. Figlerowicz, A. Guranowski, J. Boryski, J. Stawinski, and A. Kraszewski, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21834513].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0805-081811.</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293222500003">Synthesis and Biological Activity of New Mixed HIV-PR Inhibitors Conjugated to Bifunctional High-Molecular Weight Poly(Ethylene Glycol).</a> Benedetti, F., F. Berti, G.M. Bonora, P. Campaner, and S. Drioli, Letters in Organic Chemistry, 2011. 8(6): p. 380-384; ISI[000293222500003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292964700022">Quasi-biomimetic Ring Contraction Promoted by a Cysteine-based Nucleophile: Total Synthesis of Sch-642305, some Analogs and Their Putative anti-HIV Activities.</a> Dermenci, A., P.S. Selig, R.A. Domaoal, K.A. Spasov, K.S. Anderson, and S.J. Miller, Chemical Science, 2011. 2(8): p. 1568-1572; ISI[000292964700022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293020000029">Identification of a Methylated Oligoribonucleotide as a Potent Inhibitor of HIV-1 Reverse Transcription Complex.</a> Grigorov, B., A. Bocquin, C. Gabus, S. Avilov, Y. Mely, A. Agopian, G. Divita, M. Gottikh, M. Witvrouw, and J.L. Darlix, Nucleic Acids Research, 2011. 39(13): p. 5586-5596; ISI[000293020000029].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293133100009">Synthesis of 3 &#39;-S-Phosphonomethyl-Modified Nucleoside Phosphonates with a 3 &#39;-Deoxy-3 &#39;-thio-alpha-L-threosyl Sugar Moiety.</a> Huang, Q.Y. and P. Herdewijn, European Journal of Organic Chemistry, 2011(19): p. 3450-3457; ISI[000293133100009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293037400003">A Convenient Synthesis of Novel Thiazolidin-4-one-linked Pseudodisaccharides by Tandem Staudinger/aza-Wittig/cyclization and Their Biological Evaluation.</a> Li, X.L., Q.M. Yin, L.L. Jiao, Z.B. Qin, J.N. Feng, H. Chen, J.C. Zhang, and M. Meng, Carbohydrate Research, 2011. 346(3): p. 401-409; ISI[000293037400003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293230300023">Microwave-assisted Synthesis of Dinucleoside Analogues Containing a Thiazolidin-4-one Linkage via One-pot Tandem Staudinger/aza-Wittig/cyclization.</a> Shen, F.J., X.L. Li, X.Y. Zhang, Q.M. Yin, Z.B. Qin, H. Chen, J.C. Zhang, and Z.P. Ma, Organic &amp; Biomolecular Chemistry, 2011. 9(16): p. 5766-5772; ISI[000293230300023].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000292982500005">A Critical Subset Model Provides a Conceptual Basis for the High Antiviral Activity of Major HIV Drugs.</a> Shen, L., S.A. Rabi, A.R. Sedaghat, L. Shan, J. Lai, S.F. Xing, and R.F. Siliciano, Science Translational Medicine, 2011. 3(91); ISI[000292982500005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293220900008">Computer-Aided Design, Synthesis, and Biological Activity Evaluation of Potent Fusion Inhibitors Targeting HIV-1 gp41.</a> Tan, J.J., B. Zhang, X.J. Cong, L.F. Yang, B. Liu, R. Kong, Z.Y. Kui, C.X. Wang, and L.M. Hu, Medicinal Chemistry, 2011. 7(4): p. 309-316; ISI[000293220900008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0805-0818.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
