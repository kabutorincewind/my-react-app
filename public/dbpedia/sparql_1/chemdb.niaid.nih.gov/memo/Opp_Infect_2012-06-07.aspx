

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-06-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="UVL90kvKBAjHVvPJvjwR0We6qsFny8jIJzojBBuxsAjWiViRaMnXt8lH5qdsm8XX7kOXKqWJH1FnGi2ymp0zuCIBO8hqKqzsdqAjr1/VSlt40LZCuZjoTKkBv8I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0B47754D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  May 25 - June 7, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22484288">Antimicrobial/Cytolytic Peptides from the Venom of the North African Scorpion, Androctonus amoreuxi: Biochemical and Functional Characterization of Natural Peptides and a Single Site-substituted Analog.</a> Almaaytah, A., M. Zhou, L. Wang, T. Chen, B. Walker, and C. Shaw. Peptides, 2012. 35(2): p. 291-299; PMID[22484288].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22647252">Biological Activities and Volatile Constituents of Daucus muricatus L. From Algeria.</a> Bendiabdellah, A., M.E. Dib, N. Djabou, H. Allali, B. Tabti, J. Costa, and A. Muselli. Chemistry Central Journal, 2012. 6(1): p. 48; PMID[22647252].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22574670">Bahamaolides A and B, Antifungal Polyene Polyol Macrolides from the Marine Actinomycete Streptomyces sp.</a> Kim, D.G., K. Moon, S.H. Kim, S.H. Park, S. Park, S.K. Lee, K.B. Oh, J. Shin, and D.C. Oh. Journal of Natural Products, 2012. 75(5): p. 959-967; PMID[22574670].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22455581">Deciphering the Antimicrobial Activity of Phenanthroline Chelators.</a> McCann, M., A. Kellett, K. Kavanagh, M. Devereux, and A.L. Santos. Current Medicinal Chemistry, 2012. 19(17): p. 2703-2714; PMID[22455581].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22497805">The Hymenochirins: A Family of Host-Defense Peptides from the Congo Dwarf Clawed Frog Hymenochirus boettgeri (Pipidae).</a> Mechkarska, M., M. Prajeep, L. Coquet, J. Leprince, T. Jouenne, H. Vaudry, J.D. King, and J.M. Conlon. Peptides, 2012. 35(2): p. 269-275; PMID[22497805].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22469914">Preparation and Antimicrobial Activity of Some Carboxymethyl Chitosan Acyl Thiourea Derivatives.</a> Mohamed, N.A. and N.A. Abd El-Ghany. International Journal of Biological Macromolecules, 2012. 50(5): p. 1280-1285; PMID[22469914].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22560587">An Efficient One-pot Synthesis, Structure, Antimicrobial and Antioxidant Investigations of Some Novel Quinolyldibenzo[B,E][1,4]diazepinones.</a> Parmar, N.J., H.A. Barad, B.R. Pansuriya, S.B. Teraiya, V.K. Gupta, and R. Kant. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(11): p. 3816-3821; PMID[22560587].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22560472">Synthesis and Antimicrobial Activities of Structurally Novel S,S&#39;-Bis(heterosubstituted) Disulfides.</a> Ramaraju, P., D. Gergeres, E. Turos, S. Dickey, D.V. Lim, J. Thomas, and B. Anderson. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(11): p. 3623-3631; PMID[22560472].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p>   
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22429912">Cobalt, Nickel, Copper and Zinc Complexes with 1,3-Diphenyl-1H-pyrazole-4-carboxaldehyde Schiff Bases: Antimicrobial, Spectroscopic, Thermal and Fluorescence Studies.</a> Singh, K., Y. Kumar, P. Puri, M. Kumar, and C. Sharma. European Journal of Medicinal Chemistry, 2012. 52: p. 313-321; PMID[22429912].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22483088">Synthesis, Cytotoxicity and Apoptosis of Cyclotriphosphazene Compounds as Anti-cancer Agents.</a> Yildirim, T., K. Bilgin, G.Y. Ciftci, E.T. Ecik, E. Senkuytu, Y. Uludag, L. Tomak, and A. Kilic. European Journal of Medicinal Chemistry, 2012. 52: p. 213-220; PMID[22483088].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p>
    <p> </p>
    
    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22560837">Syntheses and Biological Studies of Novel Spiropiperazinyl Oxazolidinone Antibacterial Agents Using a Spirocyclic Diene Derived Acylnitroso Diels-Alder Reaction.</a> Ji, C., W. Lin, G.C. Moraski, J.A. Thanassi, M.J. Pucci, S.G. Franzblau, U. Mollmann, and M.J. Miller. Bioorganic &amp; Medicinal Chemistry, 2012. 20(11): p. 3422-3428; PMID[22560837].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22503208">Synthesis, Structure-Activity Relationship and in Vitro Anti-mycobacterial Evaluation of 13-N-Octylberberine Derivatives.</a> Liu, Y.X., C.L. Xiao, Y.X. Wang, Y.H. Li, Y.H. Yang, Y.B. Li, C.W. Bi, L.M. Gao, J.D. Jiang, and D.Q. Song. European Journal of Medicinal Chemistry, 2012. 52: p. 151-158; PMID[22503208].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22572576">Synthesis and Antimycobacterial Activity of Prodrugs of Sulfur Dioxide (So(2)).</a> Malwal, S.R., D. Sriram, P. Yogeeswari, and H. Chakrapani. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(11): p. 3603-3606; PMID[22572576].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22483635">Chemical Synthesis and Biological Evaluation of Triazole Derivatives as Inhibitors of InhA and Antituberculosis Agents.</a> Menendez, C., A. Chollet, F. Rodriguez, C. Inard, M.R. Pasca, C. Lherbet, and M. Baltas. European Journal of Medicinal Chemistry, 2012. 52: p. 275-283; PMID[22483635]. <b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22405030">Potential Selective Inhibitors against Rv0183 of Mycobacterium tuberculosis Targeting Host Lipid Metabolism.</a> Saravanan, P., V.K. Dubey, and S. Patra. Chemical biology &amp; drug design, 2012. 79(6): p. 1056-1062; PMID[22405030].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p>
    <p> </p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22483965">Amino Acid, Dipeptide and Pseudodipeptide Conjugates of Ring-substituted 8-aminoquinolines: Synthesis and Evaluation of Anti-infective, beta-Haematin Inhibition and Cytotoxic Activities.</a> Kaur, K., M. Jain, S.I. Khan, M.R. Jacob, B.L. Tekwani, S. Singh, P.P. Singh, and R. Jain. European Journal of Medicinal Chemistry, 2012. 52: p. 230-241; PMID[22483965].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22459876">2-Aminopyrimidine based 4-Aminoquinoline Anti-plasmodial Agents. Synthesis, Biological Activity, Structure-Activity Relationship and Mode of Action Studies.</a> Singh, K., H. Kaur, K. Chibale, J. Balzarini, S. Little, and P.V. Bharatam. European Journal of Medicinal Chemistry, 2012. 52: p. 82-97; PMID[22459876].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22450967">The Aminopeptidase Inhibitor CHR-2863 Is an Orally Bioavailable Inhibitor of Murine Malaria.</a> Skinner-Adams, T.S., C.L. Peatey, K. Anderson, K.R. Trenholme, D. Krige, C.L. Brown, C. Stack, D.M. Nsangou, R.T. Mathews, K. Thivierge, J.P. Dalton, and D.L. Gardiner. Antimicrobial Agents and Chemotherapy, 2012. 56(6): p. 3244-3249; PMID[22450967].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p>
    <p> </p>
    
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22179265">In Vitro Effects of Aqueous Extracts of Astragalus membranaceus and Scutellaria baicalensis Georgi on Toxoplasma bondii.</a> Yang, X., B. Huang, J. Chen, S. Huang, H. Zheng, Z.R. Lun, J. Shen, Y. Wang, and F. Lu. Parasitology Research, 2012. 110(6): p. 2221-2227; PMID[22179265].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0525-060712.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303813500001">Preliminary Studies on Luffa cylindrica: Comparative Phytochemical and Antimicrobial Screening of the Fresh and Dried Aerial Parts.</a> Aboh, M.I., S.E. Okhale, and K. Ibrahim. African Journal of Microbiology Research, 2012. 6(13): p. 3088-3091; ISI[000303813500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303765800019">Studies on Antimicrobial Activity of Ethanolic Extract of Maize Silk.</a> Feng, X., L. Wang, M.L. Tao, Q. Zhou, and Z.H. Zhong. African Journal of Microbiology Research, 2012. 6(2): p. 335-338; ISI[000303765800019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303456900009">Synthesis and in Vitro Antimicrobial Evaluation of Penta-substituted Pyridine Derivatives Bearing the Quinoline Nucleus.</a> Makawana, J.A., M.P. Patel, and R.G. Patel. Medicinal Chemistry Research, 2012. 21(5): p. 616-623; ISI[000303456900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303775000021">Silver(I) Complexes of 9-Anthracenecarboxylic Acid and Imidazoles: Synthesis, Structure and Antimicrobial Activity.</a> McCann, M., R. Curran, M. Ben-Shoshan, V. McKee, A.A. Tahir, M. Devereux, K. Kavanagh, B.S. Creaven, and A. Kellett. Dalton Transactions, 2012. 41(21): p. 6516-6527; ISI[000303775000021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0525-060712.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303857100067">Peptides of the Constant Region of Antibodies Display Fungicidal Activity.</a> Polonelli, L., T. Ciociola, W. Magliani, P.P. Zanello, T. D&#39;Adda, S. Galati, F. De Bernardis, S. Arancia, E. Gabrielli, E. Pericolini, A. Vecchiarelli, D.C. Arruda, M.R. Pinto, L.R. Travassos, T.A. Pertinhez, A. Spisni, and S. Conti. PLOS One, 2012. 7(3); ISI[000303857100067].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0525-060712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
