

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-76.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="K6lN6Q+1X8wfvvIfAhbhYE3g98NT3SzkLEyQF/V6jKOhgx4CpeJmikzJRIybDAv4mahXQOXwOBtb/opU7QD9cgcVv7h/nsv5TJxjPkQYxIfGJKCaKlgvb4nI21Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A50B6ADC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-76-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6409   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          (E)-5-(2-bromovinyl)-2&#39;-deoxyuridine (BVDU)</p>

    <p>          De Clercq, ED</p>

    <p>          Med Res Rev <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15389733&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15389733&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>2.         6410   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Crimean-Congo hemorrhagic fever</p>

    <p>          Whitehouse, Chris A</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4D97KGD-1/2/7905d3187b27bce1fb2b3932793c6f45">http://www.sciencedirect.com/science/article/B6T2H-4D97KGD-1/2/7905d3187b27bce1fb2b3932793c6f45</a> </p><br />

    <p>3.         6411   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Synthesis of Some New 4-Amino-1,2,4-Triazole Derivatives as Potential Anti-Hiv and Anti-Hbv</p>

    <p>          El-Barbary, A. <i>et al.</i></p>

    <p>          Phosphorus Sulfur and Silicon and the Related Elements <b>2004</b>.  179(8): 1497-1508</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         6412   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          A 7-deaza-adenosine analog is a potent and selective inhibitor of hepatitis C virus replication with excellent pharmacokinetic properties</p>

    <p>          Olsen, DB, Eldrup, AB, Bartholomew, L, Bhat, B, Bosserman, MR, Ceccacci, A, Colwell, LF, Fay, JF, Flores, OA, Getty, KL, Grobler, JA, LaFemina, RL, Markel, EJ, Migliaccio, G, Prhavc, M, Stahlhut, MW, Tomassini, JE, MacCoss, M, Hazuda, DJ, and Carroll, SS </p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3944-53</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388457&amp;dopt=abstract</a> </p><br />

    <p>5.         6413   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Inhibition of human cytomegalovirus replication by benzimidazole nucleosides involves three distinct mechanisms</p>

    <p>          Evers, DL, Komazin, G, Ptak, RG, Shin, D, Emmer, BT, Townsend, LB, and Drach, JC</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3918-27</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388453&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388453&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         6414   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Combinations of Adefovir with Nucleoside Analogs Produce Additive Antiviral Effects against Hepatitis B Virus In Vitro</p>

    <p>          Delaney, WE 4th, Yang, H, Miller, MD, Gibbs, CS, and Xiong, S</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3702-10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388423&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388423&amp;dopt=abstract</a> </p><br />

    <p>7.         6415   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Chemical Constituents of Glycosmis arborea: Three New Carbazole Alkaloids and Their Biological Activity(1)</p>

    <p>          Ito, C, Itoigawa, M, Sato, A, Hasan, CM, Rashid, MA, Tokuda, H, Mukainaka, T, Nishino, H, and Furukawa, H</p>

    <p>          J Nat Prod <b>2004</b>.  67(9): 1488-1491</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15387647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15387647&amp;dopt=abstract</a> </p><br />

    <p>8.         6416   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          HCV NS5B polymerase-bound conformation of a soluble sulfonamide inhibitor by 2D transferred NOESY</p>

    <p>          Yannopoulos, Constantin G, Xu, Ping, Ni, Feng, Chan, Laval, Pereira, Oswy Z, Reddy, TJagadeeswar, Das, Sanjoy K, Poisson, Carl, Nguyen-Ba, Nghe, and Turcotte, Nathalie</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4D8MJ22-5/2/39f1e069e56bb83774cccb223875cc0c">http://www.sciencedirect.com/science/article/B6TF9-4D8MJ22-5/2/39f1e069e56bb83774cccb223875cc0c</a> </p><br />

    <p>9.         6417   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Simian varicella: a model for human varicella-zoster virus infections</p>

    <p>          Gray, WL</p>

    <p>          Rev Med Virol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386593&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386593&amp;dopt=abstract</a> </p><br />

    <p>10.       6418   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          The design of a potent inhibitor of the hepatitis C virus NS3 protease: BILN 2061-From the NMR tube to the clinic</p>

    <p>          Tsantrizos, YS </p>

    <p>          Biopolymers <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386268&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386268&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       6419   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Antiherpetic activities of sulfated polysaccharides from green algae</p>

    <p>          Lee, JB, Hayashi, K, Maeda, M, and Hayashi, T</p>

    <p>          Planta Med <b>2004</b>.  70(9): 813-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386190&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386190&amp;dopt=abstract</a> </p><br />

    <p>12.       6420   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Ebola virus: new insights into disease aetiopathology and possible therapeutic interventions</p>

    <p>          Geisbert, TW and Hensley, LE</p>

    <p>          Expert Rev Mol Med <b>2004</b>.  2004: 1-24</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383160&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383160&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>13.       6421   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          A new strategy for studying in vitro the drug susceptibility of clinical isolates of human hepatitis B virus</p>

    <p>          Durantel, D, Carrouee-Durantel, S, Werle-Lapostolle, B, Brunelle, MN, Pichoud, C, Trepo, C, and Zoulim, F</p>

    <p>          Hepatology <b>2004</b>.  40(4): 855</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15382118&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15382118&amp;dopt=abstract</a> </p><br />

    <p>14.       6422   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Cellular entry of the SARS coronavirus</p>

    <p>          Hofmann, H and Pohlmann, S</p>

    <p>          Trends Microbiol <b>2004</b>.  12(10): 466-72</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15381196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15381196&amp;dopt=abstract</a> </p><br />

    <p>15.       6423   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Biaryl guanidine inhibitors of in vitro HCV-IRES activity</p>

    <p>          Jefferson, EA, Seth, PP, Robinson, DE, Winter, DK, Miyaji, A, Osgood, SA, Swayze, EE, and Risen, LM</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(20): 5139-43</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380215&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380215&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       6424   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Active site inhibitors of HCV NS5B polymerase. The development and pharmacophore of 2-thienyl-5,6-dihydroxypyrimidine-4-carboxylic acid</p>

    <p>          Stansfield, I, Avolio, S, Colarusso, S, Gennari, N, Narjes, F, Pacini, B, Ponzi, S, and Harper, S</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(20): 5085-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380204&amp;dopt=abstract</a> </p><br />

    <p>17.       6425   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Identification of novel small-molecule inhibitors of severe acute respiratory syndrome-associated coronavirus by chemical genetics</p>

    <p>          Kao, RY, Tsui, WH, Lee, TS, Tanner, JA, Watt, RM, Huang, JD, Hu, L, Chen, G, Chen, Z, Zhang, L, He, T, Chan, KH, Tse, H, To, AP, Ng, LW, Wong, BC, Tsoi, HW, Yang, D, Ho, DD, and Yuen, KY</p>

    <p>          Chem Biol <b>2004</b>.  11(9): 1293-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380189&amp;dopt=abstract</a> </p><br />

    <p>18.       6426   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Cyclin-dependent kinase inhibitor indirubin-3&#39;-oxime selectively inhibits human papillomavirus type 16 E7-induced numerical centrosome anomalies</p>

    <p>          Duensing, S, Duensing, A, Lee, DC, Edwards, KM, Piboonniyom, SO, Manuel, E, Skaltsounis, L, Meijer, L, and Munger, K</p>

    <p>          Oncogene <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15378001&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15378001&amp;dopt=abstract</a> </p><br />

    <p>19.       6427   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Complete Inhibition of Hepatitis B Virus Gene Expression in Vivo With Short Hairpin Rna Expressed From a Novel Double-Stranded, Bi-Cistronic Adeno-Associated Virus Pseudotype 8 Vector</p>

    <p>          Grimm, D. <i>et al.</i></p>

    <p>          Molecular Therapy <b>2004</b>.  9: S141-S142</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       6428   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Antivirals and antiviral strategies</p>

    <p>          De Clercq, E</p>

    <p>          Nat Rev Microbiol <b>2004</b>.  2(9): 704-20</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15372081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15372081&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       6429   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          NISN statement on antiviral resistance in influenza viruses</p>

    <p>          Anon</p>

    <p>          Wkly Epidemiol Rec <b>2004</b>.  79(33): 306-308</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369045&amp;dopt=abstract</a> </p><br />

    <p>22.       6430   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Binding mode determination of benzimidazole inhibitors of the hepatitis C virus RNA polymerase by a structure and dynamics strategy</p>

    <p>          LaPlante, SR, Jakalian, A, Aubry, N, Bousquet, Y, Ferland, JM, Gillard, J, Lefebvre, S, Poirier, M, Tsantrizos, YS, Kukolj, G, and Beaulieu, PL</p>

    <p>          Angew Chem Int Ed Engl <b>2004</b>.  43(33): 4306-11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15368379&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15368379&amp;dopt=abstract</a> </p><br />

    <p>23.       6431   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          N- t-Boc-amino acid esters of isomannide Potential inhibitors of serine proteases</p>

    <p>          Muri, EM, Gomes, M Jr, Costa, JS, Alencar, FL, Sales, A Jr, Bastos, ML, Hernandez-Valdes, R, Albuquerque, MG, Da, Cunha EF, Alencastro, RB, Williamson, JS, and Antunes, OA</p>

    <p>          Amino Acids <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365909&amp;dopt=abstract</a> </p><br />

    <p>24.       6432   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Evaluation of metal-conjugated compounds as inhibitors of 3CL protease of SARS-CoV</p>

    <p>          Hsu, JT, Kuo, CJ, Hsieh, HP, Wang, YC, Huang, KK, Lin, CP, Huang, PF, Chen, X, and Liang, PH</p>

    <p>          FEBS Lett <b>2004</b>.  574(1-3): 116-20</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358550&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358550&amp;dopt=abstract</a> </p><br />

    <p>25.       6433   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          New Non-Nucleoside Inhibitors of Hepatitis C Virus Rna-Dependent Rna Polymerase</p>

    <p>          Ivanov, A. <i>et al.</i></p>

    <p>          Biochemistry-Moscow <b>2004</b>.  69(7): 782-788</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       6434   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Molecular biology of severe acute respiratory syndrome coronavirus</p>

    <p>          Ziebuhr, J</p>

    <p>          Curr Opin Microbiol <b>2004</b>.  7(4): 412-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358261&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358261&amp;dopt=abstract</a> </p><br />

    <p>27.       6435   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          SAR and pharmacokinetic studies on phenethylamide inhibitors of the hepatitis C virus NS3/NS4A serine protease</p>

    <p>          Malancona, S, Colarusso, S, Ontoria, JM, Marchetti, A, Poma, M, Stansfield, I, Laufer, R, Di, Marco A, Taliani, M, Verdirame, M, Gonzalez-Paz, O, Matassa, VG, and Narjes, F</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(17): 4575-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357995&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357995&amp;dopt=abstract</a> </p><br />

    <p>28.       6436   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Practical synthesis of a potent hepatitis C virus RNA replication inhibitor</p>

    <p>          Bio, MM, Xu, F, Waters, M, Williams, JM, Savary, KA, Cowden, CJ, Yang, C, Buck, E, Song, ZJ, Tschaen, DM, Volante, RP, Reamer, RA, and Grabowski, EJ</p>

    <p>          J Org Chem <b>2004</b>.  69(19): 6257-66</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357584&amp;dopt=abstract</a> </p><br />

    <p>29.       6437   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Potent Inhibitors of the Hepatitis C Virus NS3 Protease: Design and Synthesis of Macrocyclic Substrate-Based beta-Strand Mimics</p>

    <p>          Goudreau, N, Brochu, C, Cameron, DR, Duceppe, JS, Faucher, AM, Ferland, JM, Grand-Maitre, C, Poirier, M, Simoneau, B, and Tsantrizos, YS</p>

    <p>          J Org Chem <b>2004</b>.  69(19): 6185-201</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357576&amp;dopt=abstract</a> </p><br />

    <p>30.       6438   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Inactivation of the coronavirus that induces severe acute respiratory syndrome, SARS-CoV</p>

    <p>          Darnell, ME, Subbarao, K, Feinstone, SM, and Taylor, DR</p>

    <p>          J Virol Methods <b>2004</b>.  121(1): 85-91</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15350737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15350737&amp;dopt=abstract</a> </p><br />

    <p>31.       6439   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Progress and development of small molecule HCV antivirals</p>

    <p>          Ni, ZJ and Wagman, AS</p>

    <p>          Curr Opin Drug Discov Devel <b>2004</b>.  7(4): 446-59</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338954&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>32.       6440   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          A derivative of bovine herpesvirus 1 (BoHV-1) UL3.5 lacking the last forty amino acids inhibits replication of BoHV-1</p>

    <p>          Lam, N and Letchworth, G</p>

    <p>          Arch Virol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15338323&amp;dopt=abstract</a> </p><br />

    <p>33.       6441   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Discovery of Anti-Sars Coronavirus Drug Based on Molecular Docking and Database Screening</p>

    <p>          Chen, H. <i>et al.</i></p>

    <p>          Chinese Journal of Chemistry <b>2004</b>.  22(8): 882-887</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       6442   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Hepatitis C treatment update</p>

    <p>          Pearlman, BL</p>

    <p>          Am J Med <b>2004</b>.  117(5): 344-52</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15336584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15336584&amp;dopt=abstract</a> </p><br />

    <p>35.       6443   DMID-LS-76; PUBMED-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Therapeutic potential of nucleoside/nucleotide analogues against poxvirus infections</p>

    <p>          De Clercq, E and Neyts, J</p>

    <p>          Rev Med Virol <b>2004</b>.  14(5): 289-300</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15334537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15334537&amp;dopt=abstract</a> </p><br />

    <p>36.       6444   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Further Advances in Immuno-Gene Therapy Against Human Papillomavirus by Adeno-Associated Virus Gene Transfer Into Dendritic Cells</p>

    <p>          Liu, Y. <i>et al.</i></p>

    <p>          Molecular Therapy <b>2004</b>.  9: S222</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>37.       6445   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Discovery and development of BVDU (brivudin) as a therapeutic for the treatment of herpes zoster</p>

    <p>          De Clercq, E</p>

    <p>          Biochemical Pharmacology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4DBKJJ8-5/2/c57f23daabf3ffb8207daa0824f213a5">http://www.sciencedirect.com/science/article/B6T4P-4DBKJJ8-5/2/c57f23daabf3ffb8207daa0824f213a5</a> </p><br />
    <br clear="all">

    <p>38.       6446   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Azaanalogues of ebselen as antimicrobial and antiviral agents: synthesis and properties</p>

    <p>          Wojtowicz, H, Kloc, K, Maliszewska, I, Mlochowski, J, Pietka, M, and Piasecki, E</p>

    <p>          Il Farmaco <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4D990H2-1/2/eaef8acf1d6416a3eb236e99e69994c3">http://www.sciencedirect.com/science/article/B6VJ8-4D990H2-1/2/eaef8acf1d6416a3eb236e99e69994c3</a> </p><br />

    <p>39.       6447   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Inhibition of Sars by Peptides of the Coronavirus Spike Protein</p>

    <p>          Suriawinata, A.</p>

    <p>          Laboratory Investigation <b>2004</b>.  84(9): 1083</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>40.       6448   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Inhibition of Respiratory Syncytial Virus by Rhoa-Derived Peptides: Implications for the Development of Improved Antiviral Agents Targeting Heparin-Binding Viruses</p>

    <p>          Budge, P. and  Graham, B.</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2004</b>.  54(2): 299-302</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>41.       6449   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Targeting HSV amplicon vectors</p>

    <p>          Grandi, Paola, Spear, Matthew, Breakefield, Xandra O, and Wang, Samuel</p>

    <p>          Methods <b>2004</b>.  33(2): 179-186</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WN5-4BP99C6-7/2/c8b96134b34d063c032b5e19ba89467e">http://www.sciencedirect.com/science/article/B6WN5-4BP99C6-7/2/c8b96134b34d063c032b5e19ba89467e</a> </p><br />

    <p>42.       6450   DMID-LS-76; WOS-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of Furobenzopyrones</p>

    <p>          Pandey, V. <i>et al.</i></p>

    <p>          Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry <b>2004</b>.  43(8): 1770-1773</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>43.       6451   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Opportunities for early therapy of respiratory syncytial virus (RSV) infection: what happens before hospitalization</p>

    <p>          DeVincenzo, John P, Aitken, Jody B, and Harrison, Lisa G</p>

    <p>          Antiviral Research <b>2004</b>.  62(1): 47-51</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4BHCG0Y-1/2/cc1b29eeaf4f404d59de981819d973b8">http://www.sciencedirect.com/science/article/B6T2H-4BHCG0Y-1/2/cc1b29eeaf4f404d59de981819d973b8</a> </p><br />

    <p>44.       6452   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Synthesis and antienteroviral activity of a series of novel, oxime ether-containing pyridyl imidazolidinones</p>

    <p>          Chern, Jyh-Haur, Lee, Chung-Chi, Chang, Chih-Shiang, Lee, Yen-Chun, Tai, Chia-Liang, Lin, Ying-Ting, Shia, Kak-Shan, Lee, Ching-Yin, and Shih, Shin-Ru</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(20): 5051-5056</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4D634S0-2/2/b699f0e07aabe3608a7f04183ea19210">http://www.sciencedirect.com/science/article/B6TF9-4D634S0-2/2/b699f0e07aabe3608a7f04183ea19210</a> </p><br />

    <p>45.       6453   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Characterization of SARS-CoV main protease and identification of biologically active small molecule inhibitors using a continuous fluorescence-based assay</p>

    <p>          Kao, Richard Y, To, Amanda PC, Ng, Louisa WY, Tsui, Wayne HW, Lee, Terri SW, Tsoi, Hoi-Wah, and Yuen, Kwok-Yung</p>

    <p>          FEBS Letters <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4DD885G-5/2/0ad4a4b9d2bc13748dec97f31c9f1c84">http://www.sciencedirect.com/science/article/B6T36-4DD885G-5/2/0ad4a4b9d2bc13748dec97f31c9f1c84</a> </p><br />

    <p>46.       6454   DMID-LS-76; EMBASE-DMID-9/27/2004</p>

    <p class="memofmt1-2">          Characterization of trans- and cis-cleavage activity of the SARS coronavirus 3CLpro protease: basis for the in vitro screening of anti-SARS drugs</p>

    <p>          Lin, Cheng-Wen, Tsai, Chang-Hai, Tsai, Fuu-Jen, Chen, Pei-Jer, Lai, Chien-Chen, Wan, Lei, Chiu, Hua-Hao, and Lin, Kuan-Hsun</p>

    <p>          FEBS Letters <b>2004</b>.  574(1-3): 131-137</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4D4T75M-8/2/be963e9d8ababbe4a441a34573820a95">http://www.sciencedirect.com/science/article/B6T36-4D4T75M-8/2/be963e9d8ababbe4a441a34573820a95</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
