

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-07-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bwCooagCYlzGJWw9OxVTWL7xfF6ZsqaKJLwOBdX8BMFl2gd5UEyenHnn+7sCDCAh8EWVuje57l7Wn3CRgM9xkHUheOWg5+/VJNZSGqhIAvOqj+ViQ9pQnNeKqwc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="10E44BFA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 21 - July 4, 2013</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23683596">Discovery of N-(4&#39;-(Indol-2-yl)phenyl)sulfonamides as Novel Inhibitors of HCV Replication.</a> Chen, G., H. Ren, A. Turpoff, A. Arefolov, R. Wilde, J. Takasugi, A. Khan, N. Almstead, Z. Gu, T. Komatsu, C. Freund, J. Breslin, J. Colacino, J. Hedrick, M. Weetall, and G.M. Karp. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(13): p. 3942-3946; PMID[23683596].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23806690">Valine, the Branched-chain Amino acid, Suppresses Hepatitis C Virus RNA Replication but Promotes Infectious Particle Formation.</a> Ishida, H., T. Kato, K. Takehana, T. Tatsumi, A. Hosui, T. Nawa, T. Kodama, S. Shimizu, H. Hikita, N. Hiramatsu, T. Kanto, N. Hayashi, and T. Takehara. Biochemical and Biophysical Research Communications, 2013. <b>[Epub ahead of print]</b>; PMID[23806690].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23322441">Combination Therapies with NS5A, NS3 and NS5B Inhibitors on Different Genotypes of Hepatitis C Virus in Human Hepatocyte Chimeric Mice.</a> Shi, N., N. Hiraga, M. Imamura, C.N. Hayes, Y. Zhang, K. Kosaka, A. Okazaki, E. Murakami, M. Tsuge, H. Abe, H. Aikata, S. Takahashi, H. Ochi, C. Tateno-Mukaidani, K. Yoshizato, H. Matsui, A. Kanai, T. Inaba, F. McPhee, M. Gao, and K. Chayama. Gut, 2013. 62(7): p. 1055-1061; PMID[23322441].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23683597">Discovery of Novel HCV Inhibitors: Synthesis and biological Activity of 6-(Indol-2-yl)pyridine-3-sulfonamides Targeting Hepatitis C Virus NS4B.</a> Zhang, X., N. Zhang, G. Chen, A. Turpoff, H. Ren, J. Takasugi, C. Morrill, J. Zhu, C. Li, W. Lennox, S. Paget, Y. Liu, N. Almstead, F. George Njoroge, Z. Gu, T. Komatsu, V. Clausen, C. Espiritu, J. Graci, J. Colacino, F. Lahser, N. Risher, M. Weetall, A. Nomeir, and G.M. Karp. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(13): p. 3947-53; PMID[23683597]. <b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23813140">Pegylated Interferon-alpha2b and Ribavirin Decrease Claudin-1 and E-Cadherin Expression in HepG2 and Huh-7.5 Cells.</a> Rendon-Huerta, E.P., A. Torres-Martinez, C. Charles-Nino, A.M. Rivas-Estilla, A. Paez, T.I. Fortoul, and L.F. Montano. Annals of Hepatology, 2013. 12(4): p. 616-25; PMID[23813140].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23673225">Semisynthesis, Cytotoxicity, Antiviral Activity, and Drug Interaction Liability of 7-O-Methylated Analogues of Flavonolignans from Milk Thistle.</a> Althagafy, H.S., T.N. Graf, A.A. Sy-Cordero, B.T. Gufford, M.F. Paine, J. Wagoner, S.J. Polyak, M.P. Croatt, and N.H. Oberlies. Bioorganic &amp; Medicinal Chemistry, 2013. 21(13): p. 3919-3926; PMID[23673225].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23624267">Synthesis and Evaluation of a New Phosphorylated Ribavirin Prodrug.</a> Dong, S.D., C.C. Lin, and M. Schroeder. Antiviral Research, 2013. 99(1): p. 18-26; PMID[23624267].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23768434">Development of Flavonoid-based Inverse Agonists of the Key Signaling Receptor US28 of Human Cytomegalovirus.</a> Kralj, A., M.T. Nguyen, N. Tschammer, N. Ocampo, Q. Gesiotto, M.R. Heinrich, and O.t. Phanstiel. Journal of Medicinal Chemistry, 2013. 56(12): p. 5019-5032; PMID[23768434]. <b>[PubMed]</b> OV_0621-070413.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23673218">Artemisinin-derived Dimer Phosphate Esters as Potent Anti-cytomegalovirus (anti-CMV) and Anti-cancer Agents: A Structure-Activity Study.</a> Mott, B.T., R. He, X. Chen, J.M. Fox, C.I. Civin, R. Arav-Boger, and G.H. Posner. Bioorganic &amp; Medicinal Chemistry, 2013. 21(13): p. 3702-3707; PMID[23673218].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0621-070413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
