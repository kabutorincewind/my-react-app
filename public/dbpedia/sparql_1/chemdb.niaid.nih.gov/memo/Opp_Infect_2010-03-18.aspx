

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-03-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="y3YwSz+oEbBCxrP7ZFIWWmidFl+8bGOwArw/m29ZJ08k1s1kEFdLrGtnaQRLVtauRttatyUrGZqyuEzInHmB40Qf4vhHNp5qrPCoLcDSKdZTixuLioWThenTXG4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B1E8C9B6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">March 4 - March 18, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p> </p>

    <p class="plaintext">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20209378">Antifungal activity of the naphthoquinone beta-lapachone against disseminated infection with Cryptococcus neoformans var. neoformans in dexamethasone-immunosuppressed Swiss mice.</a> Medeiros, C.S., N.T. Pontes-Filho, C.A. Camara, J.V. Lima-Filho, P.C. Oliveira, S.A. Lemos, A.F. Leal, J.O. Brandao, and R.P. Neves. Braz J Med Biol Res. <b>[Epub ahead of print]</b>; PMID[20209378].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20176480">Synthesis, antibacterial and antifungal activities of some carbazole derivatives.</a>Zhang, F.F., L.L. Gan, and C.H. Zhou. Bioorg Med Chem Lett. 20(6): p. 1881-4; PMID[20176480].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20205198">Synthesis and Antimycobacterial Activity of Azetidine-, Quinazoline-, and Triazolo-thiadiazole-containing Pyrazines.</a> Bonde, C.G., A. Peepliwal, and N.J. Gaikwad. Arch Pharm (Weinheim). <b>[Epub ahead of print]</b>; PMID[20205198].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p> </p>

    <p class="plaintext">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20218656">Sesquiterpenes from Oplopanax horridus.</a> Inui, T., Y. Wang, D. Nikolic, D.C. Smith, S.G. Franzblau, and G.F. Pauli. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20218656].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p> </p>

    <p class="plaintext">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20231398">In vitro activity of a new Isothiazoloquinolone (ACH-702) against Mycobacterium tuberculosis and other mycobacteria.</a> Molina-Torres, C.A., J. Ocampo-Candiani, A. Rendon, M.J. Pucci, and L. Vera-Cabrera. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20231398].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p> </p>

    <p class="plaintext">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20209576">Discovery and Syntheses of &quot;Superbug Challengers&quot;-Platensimycin and Platencin.</a> Palanichamy, K. and K.P. Kaliappan. Chem Asian J. <b>[Epub ahead of print]</b>; PMID[20209576].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0304-031810.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274581600030&amp;SID=4E3cnElMpe4cdj8CJfI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">SYNTHESIS, CHARACTERIZATION AND ANTIMICROBIAL ACTIVITY OF MIXED LIGAND COMPLEXES OF DIOXOURANIUM(VI) WITH ISATIN-3-PHENYL HYDRAZONE.</a> ABHILASHA, A . ASIAN JOURNAL OF CHEMISTRY. 22(2): p. 1017-1021; ISI[000274581600030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0304-031810.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000274581800051&amp;SID=4E3cnElMpe4cdj8CJfI&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis, Antibacterial and Antifungal Activities of New Bis-5(6)-nitrobenzimidazoles.</a>Kucukbay, H., R. Durmaz, N. Sireci, and S. Gunal. Asian Journal of Chemistry. 22(4): p. 2816-2824; ISI[000274581800051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0304-031810.</p>

    <p> </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
