

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-290.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3p6DALDg3BDNHs0uSM5z1veuaqXX73Ws71UFmOpHpV6+tHUloqJransLThTgKSS9XXlzCpV6Xcq0MVgMXaV+1QnMrXfA8NpbJO0gzXG2SE/gcodbQYAp1cYJWJ0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2EEE406C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-290-MEMO</p>

<p>    1.    42913   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Prevention of lethal murine candidiasis using HP (2-20), an antimicrobial peptide derived from the N-terminus of Helicobacter pylori ribosomal protein L1</p>

<p>           Ribeiro, Patricia Damasceno and Medina-Acosta, Enrique</p>

<p>           Peptides 2004. 24(11): 1807-1814</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T0M-4B8KF2R-8/2/806e8dfe0e8f2a0a9fb0b28ce032a0d9">http://www.sciencedirect.com/science/article/B6T0M-4B8KF2R-8/2/806e8dfe0e8f2a0a9fb0b28ce032a0d9</a> </p><br />

<p>    2.    42914   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Short-course treatment regimen to identify potential antituberculous agents in a murine model of tuberculosis</p>

<p>           Shoen, CM, DeStefano, MS, Sklaney, MR, Monica, BJ, Slee, AM, and Cynamon, MH</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14973154&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14973154&amp;dopt=abstract</a> </p><br />

<p>    3.    42915   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of in vitro antimycobacterial activity of some 5-(5-nitro-2-thienyl)-2-(piperazinyl, piperidinyl and morpholinyl)-1,3,4-thiadiazole derivatives</p>

<p>           Foroumadi, A, Soltani, F, Rezaee, MA, and Moshafi, MH</p>

<p>           Boll Chim Farm 2003. 142(9): 416-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971311&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971311&amp;dopt=abstract</a> </p><br />

<p>    4.    42916   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Synthesis and in vitro antimycobacterial activity of novel 3-(1H-pyrrol-1-yl)-2-oxazolidinone analogues of PNU-100480</p>

<p>           Sbardella, Gianluca, Mai, Antonello, Artico, Marino, Loddo, Roberta, Setzu, Maria Grazia, and La Colla, Paolo</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BN0FXJ-3/2/faa988498d81f97afcef0a2e019458ee">http://www.sciencedirect.com/science/article/B6TF9-4BN0FXJ-3/2/faa988498d81f97afcef0a2e019458ee</a> </p><br />

<p>    5.    42917   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Inhibition of hepatitis C virus-transfected cholangiocarcinoma by antisense oligodeoxynucleotide in nude mice</p>

<p>           Liu, XF, Zhou, XT, and Zou, SQ</p>

<p>           Hepatobiliary Pancreat Dis Int 2004.  3(1): 115-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14969852&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14969852&amp;dopt=abstract</a> </p><br />

<p>    6.    42918   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Anti-retroviral therapy with protease inhibitors decreases virulence enzyme expression in vivo by Candida albicans without selection of avirulent fungus strains or decreasing their anti-mycotic susceptibility</p>

<p>           De Bernardis, Flavia, Tacconelli, Evelina, Mondello, Francesca, Cataldo, Adriana, Arancia, Silvia, Cauda, Roberto, and Cassone, Antonio</p>

<p>           FEMS Immunology and Medical Microbiology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2T-4BG3SG0-1/2/65155d3b3500747f3421d8242b426bdf">http://www.sciencedirect.com/science/article/B6T2T-4BG3SG0-1/2/65155d3b3500747f3421d8242b426bdf</a> </p><br />

<p>    7.    42919   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Mutant prevention concentration: comparison of fluoroquinolones and linezolid with Mycobacterium tuberculosis</p>

<p>           Rodriguez, JC, Cebrian, L, Lopez, M, Ruiz, M, Jimenez, I, and Royo, G</p>

<p>           J Antimicrob Chemother 2004. 53(3): 441-444</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963069&amp;dopt=abstract</a> </p><br />

<p>    8.    42920   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis Rv2118c codes for a single-component homotetrameric m1A58 tRNA methyltransferase</p>

<p>           Varshney, U, Ramesh, V, Madabushi, A, Gaur, R, Subramanya, HS, and RajBhandary, UL</p>

<p>           Nucleic Acids Res 2004. 32(3): 1018-27</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14960715&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14960715&amp;dopt=abstract</a> </p><br />

<p>    9.    42921   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           A two-component signal transduction system with a PAS domain-containing sensor is required for virulence of Mycobacterium tuberculosis in mice</p>

<p>           Rickman, Lisa, Saldanha, Jose W, Hunt, Debbie M, Hoar, Dominic N, Colston, MJoseph, Millar, Jonathan BA, and Buxton, Roger S</p>

<p>           Biochemical and Biophysical Research Communications 2004. 314(1): 259-267</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4BBH97W-B/2/02a55f87d711e712c6e1dd3a067dce70">http://www.sciencedirect.com/science/article/B6WBK-4BBH97W-B/2/02a55f87d711e712c6e1dd3a067dce70</a> </p><br />

<p>  10.    42922   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           In vitro resistance studies of hepatitis C virus serine protease inhibitors, VX-950 and BILN 2061: Structural analysis indicates different resistance mechanisms</p>

<p>           Lin, C, Lin, K, Luong, YP, Rao, BG, Wei, YY, Brennan, DL, Fulghum, JR, Hsiao, HM, Ma, S, Maxwell, JP, Cottrell, KM, Perni, RB, Gates, CA, and Kwong, AD</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766754&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766754&amp;dopt=abstract</a> </p><br />

<p>  11.    42923   OI-LS-290; PUBMED-OI-2/25/2004</p>

<p class="memofmt1-2">           Testing antivirals against hepatitis delta virus: farnesyl transferase inhibitors</p>

<p>           Bordier, BB and Glenn, JS</p>

<p>           Methods Mol Med 2004. 96: 539-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762290&amp;dopt=abstract</a> </p><br />

<p>  12.    42924   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Synthesis and antimicrobial properties of imidazolium and pyrrolidinonium salts</p>

<p>           Demberelnyamba, D, Kim, Ki-Sub, Choi, Sukjeong, Park, Seung-Yeob, Lee, Huen, Kim, Chang-Jin, and Yoo, Ick-Dong</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(5): 853-857</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-2/2/a02f433005005f29b2b51c48efdf7021">http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-2/2/a02f433005005f29b2b51c48efdf7021</a> </p><br />

<p>  13.    42925   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Synthesis of stavudine amino acid ester prodrugs with broad-spectrum chemotherapeutic properties for the effective treatment of HIV/AIDS</p>

<p>           Sriram, Dharmarajan, Yogeeswari, Perumal, Srichakravarthy, Narasimharaghavan, and Bal, Tanushree Ratan</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(5): 1085-1087</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-3/2/84ad99cdf358730a40170d19ff2cf3d0">http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-3/2/84ad99cdf358730a40170d19ff2cf3d0</a> </p><br />

<p>  14.    42926   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Synthesis and structure-activity relationships of new antimicrobial active multisubstituted benzazole derivatives</p>

<p>           Yildiz-Oren, Ilkay, Yalcin, Ismail, Aki-Sener, Esin, and Ucarturk, Nejat</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4BS0JJP-1/2/c8fa98f765b7be74f72c749f85a3bea9">http://www.sciencedirect.com/science/article/B6VKY-4BS0JJP-1/2/c8fa98f765b7be74f72c749f85a3bea9</a> </p><br />

<p>  15.    42927   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Programmes and principles in treatment of multidrug-resistant tuberculosis</p>

<p>           Mukherjee, Joia S, Rich, Michael L, Socci, Adrienne R, Keith Joseph, J, Alcantara Viru, Felix, Shin, Sonya S, Furin, Jennifer J, Becerra, Mercedes C, Barry, Donna J, and Yong Kim, Jim</p>

<p>           The Lancet 2004. 363(9407): 474-481</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1B-4BMSWPG-X/2/ccdc0c94878f822b52424f1596c1c511">http://www.sciencedirect.com/science/article/B6T1B-4BMSWPG-X/2/ccdc0c94878f822b52424f1596c1c511</a> </p><br />

<p>  16.    42928   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Induction of persistent in vivo resistance to Mycobacterium avium infection in BALB/c mice injected with interleukin-18-secreting fibroblasts</p>

<p>           Chung, SW, Choi, SH, and Kim, TS</p>

<p>           VACCINE 2004. 22(3-4): 398-406</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188301600014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188301600014</a> </p><br />

<p>  17.    42929   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Evaluation of four DNA extraction methods for the detection of Mycobacterium avium subsp. paratuberculosis by polymerase chain reaction</p>

<p>           Chui, Linda W, King, Robin, Lu, Patricia, Manninen, Ken, and Sim, Jeong</p>

<p>           Diagnostic Microbiology and Infectious Disease 2004. 48(1): 39-45</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T60-49Y401M-4/2/f4758c37ca6fcd33d2fd17e04970dec7">http://www.sciencedirect.com/science/article/B6T60-49Y401M-4/2/f4758c37ca6fcd33d2fd17e04970dec7</a> </p><br />

<p>  18.    42930   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Iron, mycobacteria and tuberculosis</p>

<p>           Ratledge, C</p>

<p>           TUBERCULOSIS 2004. 84(1-2): 110-130</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188378600015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188378600015</a> </p><br />

<p>  19.    42931   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Viral and cellular kinases are potential antiviral targets and have a central role in varicella zoster virus pathogenesis</p>

<p>           Moffat, Jennifer F, McMichael, Michelle A, Leisenfelder, Stacey A, and Taylor, Shannon L</p>

<p>           Biochimica et Biophysica Acta (BBA) - Proteins &amp; Proteomics 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B73DJ-4B5B6D0-2/2/75d73a9da0f40b968cceac2243fb7de6">http://www.sciencedirect.com/science/article/B73DJ-4B5B6D0-2/2/75d73a9da0f40b968cceac2243fb7de6</a> </p><br />

<p>  20.    42932   OI-LS-290; EMBASE-OI-2/25/2004</p>

<p class="memofmt1-2">           Effects of pharmacological cyclin-dependent kinase inhibitors on viral transcription and replication</p>

<p>           Schang, Luis M</p>

<p>           Biochimica et Biophysica Acta (BBA) - Proteins &amp; Proteomics 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B73DJ-4B5R93M-4/2/cb66b65a8841fe76aef5d782a611b2fd">http://www.sciencedirect.com/science/article/B73DJ-4B5R93M-4/2/cb66b65a8841fe76aef5d782a611b2fd</a> </p><br />

<p>  21.    42933   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           An amino acid substitution in the Babesia bovis dihydrofolate reductase-thymidylate synthase gene is correlated to cross-resistance against pyrimethamine and WR99210</p>

<p>           Gaffar, FR, Wilschut, K, Franssen, FFJ, and de, Vries E</p>

<p>           MOL BIOCHEM PARASIT 2004. 133(2):  209-219</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188385100007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188385100007</a> </p><br />

<p>  22.    42934   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Bioinformatic insight into the unity and diversity of cytochromes p450</p>

<p>           Lisitsa, A, Archakov, A, Lewi, R, and Janssen, P</p>

<p>           METHOD FIND EXP CLIN 2003. 25(9): 733-745</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188419700008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188419700008</a> </p><br />

<p>  23.    42935   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           &quot;Cocktail&quot; approaches and strategies in drug development: Valuable tool or flawed science?</p>

<p>           Zhou, HH, Tong, Z, and McLeod, JF</p>

<p>           J CLIN PHARMACOL 2004. 44(2): 120-134</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188369900002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188369900002</a> </p><br />

<p>  24.    42936   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Screening of new antifungal compounds in a collection of chemical products</p>

<p>           Lemriss, S, Marquet, B, Ginestet, H, Lefeuvre, L, Fassouane, A, and Boiron, P</p>

<p>           J MYCOL MED 2003. 13(4): 189-192</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188584200006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188584200006</a> </p><br />

<p>  25.    42937   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Pfizer - Triazole antifungal approved for treatment of esophageal candidiasis</p>

<p>           [Anon]</p>

<p>           FORMULARY 2004. 39(1): 7-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188466000003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188466000003</a> </p><br />

<p>  26.    42938   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Antimicrobial activity of aqueous extracts and of berberine isolated from Berberis heterophylla</p>

<p>           Freile, ML, Giannini, F, Pucci, G, Sturniolo, A, Rodero, L, Pucci, O, Balzareti, V, and Enriz, RD</p>

<p>           FITOTERAPIA 2003. 74(7-8): 702-705</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188466100016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188466100016</a> </p><br />

<p>  27.    42939   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Regulation of DNA topology in mycobacteria</p>

<p>           Nagaraja, V</p>

<p>           CURR SCI INDIA 2004. 86(1): 135-140</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188395500030">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188395500030</a> </p><br />

<p>  28.    42940   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Drug discovery for tuberculosis: Bottlenecks and path forward</p>

<p>           Balganesh, TS, Balasubramanian, V, and Kumar, SA</p>

<p>           CURR SCI INDIA 2004. 86(1): 167-176</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188395500034">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188395500034</a> </p><br />

<p>  29.    42941   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           The place of G-CSF in pre-emptive therapy for cytomegalo virus (CMV): Cost implications.</p>

<p>           Coombes, JI, Davies, MM, Wrigley, M, Mutton, K, Guiver, M, Foulkes, B, Chang, J, Cavet, J, and Chopra, R</p>

<p>           BLOOD 2003. 102(11): 448B</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000186537101809">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000186537101809</a> </p><br />

<p>  30.    42942   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Structure of Toxoplasma gondii LDH1: Active-site differences from human lactate dehydrogenases and the structural basis for efficient APAD(+) use</p>

<p>           Kavanagh, KL, Elling, RA, and Wilson, DK</p>

<p>           BIOCHEMISTRY-US 2004. 43(4): 879-889</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188504800006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188504800006</a> </p><br />

<p>  31.    42943   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           HCV core, NS3, NS5A and NS5B proteins modulate cell proliferation independently from p53 expression in hepatocarcinoma cell lines</p>

<p>           Siavoshian, S, Abraham, JD, Kieny, MP, and Schuster, C</p>

<p>           ARCH VIROL 2004. 149(2): 323-336</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188428100008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188428100008</a> </p><br />

<p>  32.    42944   OI-LS-290; WOS-OI-2/15/2004</p>

<p class="memofmt1-2">           Moxifloxacin-containing regimen greatly reduces time to culture conversion in murine tuberculosis</p>

<p>           Nuermberger, EL, Yoshimatsu, T, Tyagi, S, O&#39;Brien, RJ, Vernon, AN, Chaisson, RE, Bishai, WR, and Grosset, JH</p>

<p>           AM J RESP CRIT CARE 2004. 169(3):  421-426</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188417800020">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188417800020</a> </p><br />

<p>  33.    42945   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Effect of arthropod extracts on the multiplication of Toxoplasma gondii in mouse peritoneal macrophages</p>

<p>           Carmona, MC, Corrales, MH, Bermudez, OMG, Somarribas, AJ, Tamayo, G, Appel, AS, Nielsen, V, and Hurtado, P</p>

<p>           REV BIOL TROP  2003. 51(2): 317-320</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188223200003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188223200003</a> </p><br />

<p>  34.    42946   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Prospects for antiviral ribozymes and deoxyribozymes</p>

<p>           Peracchi, A</p>

<p>           REV MED VIROL  2004. 14(1): 47-64</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188501000005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188501000005</a> </p><br />

<p>  35.    42947   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Effect of water extract of Turkish propolis on tuberculosis infection in guinea-pigs</p>

<p>           Yildirim, Z, Hacievliyagil, S, Kutlu, NO, Aydin, NE, Kurkcuoglu, M, Iraz, M, and Durmaz, R</p>

<p>           PHARMACOL RES  2004. 49(3): 287-292</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188698800013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188698800013</a> </p><br />

<p>  36.    42948   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis phagosome maturation arrest: mycobacterial phosphatidylinositol analog phosphatidylinositol mannoside stimulates early endosomal fusion</p>

<p>           Vergne, I, Fratti, RA, Hill, PJ, Chua, J, Belisle, J, and Deretic, V</p>

<p>           MOL BIOL CELL  2004. 15(2): 751-760</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188718900033">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188718900033</a> </p><br />

<p>  37.    42949   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Dramatic effects of 2-bromo-5,6-dichloro-1-beta-D-ribofuranosyl benzimidazole riboside on the genome structure, packaging, and egress of guinea pig cytomegalovirus</p>

<p>           Nixon, DE and McVoy, MA</p>

<p>           J VIROL 2004. 78(4): 1623-1635</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900003</a> </p><br />

<p>  38.    42950   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Perforin and gamma interferon-mediated control of coronavirus central nervous system infection by CD8 T cells in the absence of CD4 T cells</p>

<p>           Bergmann, CC, Parra, B, Hinton, DR, Ramakrishna, C, Dowdell, KC, and Stohlman, SA</p>

<p>           J VIROL 2004. 78(4): 1739-1750</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900014</a> </p><br />

<p>  39.    42951   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Design of liposomal aerosols for improved delivery of rifampicin to alveolar macrophages</p>

<p>           Vyas, SP, Kannan, ME, Jain, S, Mishra, V, and Singh, P</p>

<p>           INT J PHARM 2004. 269(1): 37-49</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188509200005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188509200005</a> </p><br />

<p>  40.    42952   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Similarity approach to QSAR - Application to antimycobacterial benzoxazines</p>

<p>           Gallegos, A, Carbo-Dorca, R, Ponec, R, and Waisser, K</p>

<p>           INT J PHARM 2004. 269(1): 51-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188509200006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188509200006</a> </p><br />

<p>  41.    42953   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Biophysical characterization of hepatitis C virus core protein: implications for interactions within the virus and host</p>

<p>           Kunkel, M and Watowich, SJ</p>

<p>           FEBS LETT 2004. 557(1-3): 174-180</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188648100032">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188648100032</a> </p><br />

<p>  42.    42954   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Enantioselectivity of inhibition of cytochrome P450 3A4 (CYP3A4) by ketoconazole: Testosterone and methadone as substrates</p>

<p>           Dilmaghanian, S, Gerber, JG, Filler, SG, Sanchez, A, and Gal, J</p>

<p>           CHIRALITY 2004. 16(2): 79-85</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188504300003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188504300003</a> </p><br />

<p>  43.    42955   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Discovery of thiophene-2-carboxylic acids as potent inhibitors of HCVNS5B polymerase and HCV subgenomic RNA replication. Part 1: Sulfonamides</p>

<p>           Chan, L, Das, SK, Reddy, TJ, Poisson, C, Proulx, M, Pereira, O, Courchesne, M, Roy, C, Wang, WY, Siddiqui, A, Yannopoulos, CG, Nguyen-Ba, N, Labrecque, D, Bethell, R, Hamel, M, Courtemanche-Asselin, P, L&#39;Heureux, L, David, M, Nicolas, O, Brunette, S, Bilimoria, D, and Bedard, J</p>

<p>           BIOORG MED CHEM LETT 2004. 14(3): 793-796</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188611500047">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188611500047</a> </p><br />

<p>  44.    42956   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Discovery of thiophene-2-carboxylic acids as potent inhibitors of HCVNS5B polymerase and HCV subgenomic RNA replication. Part 2: Tertiary amides</p>

<p>           Chan, L, Pereira, O, Reddy, TJ, Das, SK, Poisson, C, Courchesne, M, Proulx, M, Siddiqui, A, Yannopoulos, CG, Nguyen-Ba, N, Roy, C, Nasturica, D, Moinet, C, Bethell, R, Hamel, M, L&#39;Heureux, L, David, M, Nicolas, O, Courtemanche-Asselin, P, Brunette, S, Bilimoria, D, and Bedard, J</p>

<p>           BIOORG MED CHEM LETT 2004. 14(3): 797-800</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188611500048">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188611500048</a> </p><br />

<p>  45.    42957   OI-LS-290; WOS-OI-2/22/2004</p>

<p class="memofmt1-2">           Three-dimensional models of wild-type and mutated forms of cytochrome P450 14 alpha-sterol demethylases from Aspergillus fumigatus and Candida albicans provide insights into posaconazole binding</p>

<p>           Xiao, L, Madison, V, Chau, AS, Loebenberg, D, Palermo, RE, and McNicholas, PM</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(2): 568-574</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200030">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200030</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
