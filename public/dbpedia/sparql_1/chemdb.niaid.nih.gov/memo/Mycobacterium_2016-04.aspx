

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XevAaDecEZCNQET4ISHfKczlt9Mv/YKNJZWE6h2acigDi5zNzIWk0Yv1ZGDOcXDejp9PgUX4xJokzOfccY7EUq4uTXtrJ/o+ld++LBMmReGPH+NhEH1bFIprHNg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="15814945" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: April, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26883697">Tigecycline Potentiates Clarithromycin Activity against Mycobacterium avium in Vitro.</a> Bax, H.I., I.A. Bakker-Woudenberg, M.T. Ten Kate, A. Verbon, and J.E. de Steenwinkel. Antimicrobial Agents Chemotherapy, 2016. 60(4): p. 2577-2579. PMID[26883697]. PMCID[PMC4808172].<b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">2.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26951892">Novel Nicotine Analogues with Potential Anti-mycobacterial Activity</a>. Gandhi, P.T., T.N. Athmaram, and G.R. Arunkumar. Bioorganic &amp; Medicinal Chemistry, 2016. 24(8): p. 1637-1647. PMID[26951892]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26934341">N-Benzyl-4-((heteroaryl)methyl)benzamides: A New Class of Direct NADH-dependent 2-trans Enoyl-Acyl Carrier Protein Reductase (InhA) Inhibitors with Antitubercular Activity</a>. Guardia, A., G. Gulten, R. Fernandez, J. Gomez, F. Wang, M. Convery, D. Blanco, M. Martinez, E. Perez-Herran, M. Alonso, F. Ortega, J. Rullas, D. Calvo, L. Mata, R. Young, J.C. Sacchettini, A. Mendoza-Losana, M. Remuinan, L. Ballell Pages, and J. Castro-Pichel. ChemMedChem, 2016. 11(7): p. 687-701. PMID[26934341].<b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27095514">Synthesis, Biological Evaluation and Structure-Activity Relationship of 2-Styrylquinazolones as Anti-tubercular Agents.</a> Jadhavar, P.S., T.M. Dhameliya, M.D. Vaja, D. Kumar, J.P. Sridevi, P. Yogeeswari, D. Sriram, and A.K. Chakraborti. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(11): p. 2663-2669. PMID[27095514]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">5.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26974691">Antituberculosis Activity of a Naturally Occurring Flavonoid, Isorhamnetin.</a> Jnawali, H.N., D. Jeon, M.C. Jeong, E. Lee, B. Jin, S. Ryoo, J. Yoo, I.D. Jung, S.J. Lee, Y.M. Park, and Y. Kim. Journal of Natural Products, 2016. 79(4): p. 961-969. PMID[26974691]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26867795">Antimycobacterial Activity of Methanolic Plant Extract of Artemisia capillaris Containing Ursolic acid and Hydroquinone against Mycobacterium tuberculosis</a>. Jyoti, M.A., K.W. Nam, W.S. Jang, Y.H. Kim, S.K. Kim, B.E. Lee, and H.Y. Song. Journal of Infection and Chemotherapy, 2016. 22(4): p. 200-208. PMID[26867795]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26900659">Novel Morpholinoquinoline Nucleus Clubbed with Pyrazoline Scaffolds: Synthesis, Antibacterial, Antitubercular and Antimalarial Activities.</a> Karad, S.C., V.B. Purohit, P. Thakor, V.R. Thakkar, and D.K. Raval. European Journal of Medicinal Chemistry, 2016. 112: p. 270-279. PMID[26900659]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">8.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27002486">Fragment-based Approaches to the Development of Mycobacterium Tuberculosis CYP121 Inhibitors.</a> Kavanagh, M.E., A.G. Coyne, K.J. McLean, G.G. James, C.W. Levy, L.B. Marino, L.P. de Carvalho, D.S. Chan, S.A. Hudson, S. Surade, D. Leys, A.W. Munro, and C. Abell. Journal of Medicinal Chemistry, 2016. 59(7): p. 3272-3302. PMID[27002486]. PMCID[PMC4835159].<b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">9.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26602814">Combination of Anti-tuberculosis Drugs with Vitamin C or NAC against Different Staphylococcus aureus and Mycobacterium tuberculosis Strains.</a> Khameneh, B., B.S. Fazly Bazzaz, A. Amani, J. Rostami, and N. Vahdati-Mashhadian. Microbial Pathogenesis, 2016. 93: p. 83-87. PMID[26602814]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27107949">Synthesis and Antimycobacterial Activity of N-(2-Aminopurin-6-yl) and N-(Purin-6-yl) Amino acids and Dipeptides</a>. Krasnov, V.P., A.Y. Vigorov, V.V. Musiyak, I.A. Nizova, D.A. Gruzdev, T.V. Matveeva, G.L. Levit, M.A. Kravchenko, S.N. Skornyakov, O.B. Bekker, V.N. Danilenko, and V.N. Charushin. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(11): p. 2645-2648. PMID[27107949]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/26951749">Design, Syntheses, and Anti-tuberculosis Activities of Conjugates of Piperazino-1,3-benzothiazin-4-ones (pBTZs) with 2,7-Dimethylimidazo [1,2-a]pyridine-3-carboxylic acids and 7-Phenylacetyl cephalosporins.</a> Majewski, M.W., R. Tiwari, P.A. Miller, S. Cho, S.G. Franzblau, and M.J. Miller. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(8): p. 2068-2071. PMID[26951749]. PMCID[PMC4824297].<b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27115026">In Vitro Antimycobacterial and Cytotoxic Data on Medicinal Plants Used to Treat Tuberculosis.</a> Nguta, J.M., R. Appiah-Opong, A.K. Nyarko, D. Yeboah-Manu, P.G. Addo, I.D. Otchere, and A. Kissi-Twum. Data in Brief, 2016. 7: p. 1124-1130. PMID[27115026]. PMCID[PMC4833128].<b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/26900657">New Direct Inhibitors of InhA with Antimycobacterial Activity Based on a Tetrahydropyran Scaffold.</a> Pajk, S., M. Zivec, R. Sink, I. Sosic, M. Neu, C.W. Chung, M. Martinez-Hoyos, E. Perez-Herran, D. Alvarez-Gomez, E. Alvarez-Ruiz, A. Mendoza-Losana, J. Castro-Pichel, D. Barros, L. Ballell-Pages, R.J. Young, M.A. Convery, L. Encinas, and S. Gobec. European Journal of Medicinal Chemistry, 2016. 112: p. 252-257. PMID[26900657]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26902758">Combined Bioinformatic and Rational Design Approach to Develop Antimicrobial Peptides against Mycobacterium tuberculosis.</a> Pearson, C.S., Z. Kloos, B. Murray, E. Tabe, M. Gupta, J.H. Kwak, P. Karande, K.A. McDonough, and G. Belfort. Antimicrobial Agents and Chemotherapy, 2016. 60(5): p. 2757-2764. PMID[26902758]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27101894">Click-based Synthesis and Antitubercular Evaluation of Novel Dibenzo[b,d]thiophene-1,2,3-triazoles with Piperidine, Piperazine, Morpholine and Thiomorpholine Appendages.</a> Pulipati, L., P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(11): p. 2649-2654. PMID[27101894]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27089314">In Vitro Activity of Selected West African Medicinal Plants against Mycobacterium ulcerans Disease.</a> Tsouh Fokou, P.V., A.A. Kissi-Twum, D. Yeboah-Manu, R. Appiah-Opong, P. Addo, L.R. Tchokouaha Yamthe, A. Ngoutane Mfopa, F. Fekam Boyom, and A.K. Nyarko. Molecules, 2016. 21(4): E445. PMID[27089314]. <b><br />
    [PubMed]</b>. TB_04_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160421&amp;CC=WO&amp;NR=2016061561A1&amp;KC=A1">Formulations Having Anti-inflammatory Activity and Antimicrobial Activity against Gram-positive Bacteria.</a> James-Meyer, L.S. and G.C. Coles. Patent. 2016. 2015-US56111 2016061561: 43pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160421&amp;CC=WO&amp;NR=2016059391A1&amp;KC=A1">Polymer Composed of Repeat Units Having a Biologically Active Molecule Attached Thereto via a pH-sensitive Bond.</a> Ouberai, M.M. and M. Welland. Patent. 2016. 2015-GB53007 2016059391: 114pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160428&amp;CC=WO&amp;NR=2016063298A1&amp;KC=A1">Substituted 1,2,3-Triazol-1-yl-methyl-2,3-dihydro-2-methyl-6-nitroimidazo[2,1-b]oxazoles as Anti-mycobacterial Agents and a Process for the Preparation Thereof.</a> Yempalla, K.R., G. Munagala, S. Singh, S. Sharma, I.A. Khan, R.A. Vishwakarma, and P.P. Singh. Patent. 2016. 2015-IN50111 2016063298: 35pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_04_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160428&amp;CC=WO&amp;NR=2016061772A1&amp;KC=A1">Nargenicin Compounds and Uses Thereof as Antibacterial Agents.</a> Young, K., D.B. Olsen, S.B. Singh, J. Su, R.R. Wilkening, J.M. Apgar, D. Meng, D. Parker, M. Mandal, L. Yang, R.E. Painter, Q. Dang, and T. Suzuki. Patent. 2016. 2014-CN89204 2016061772: 167pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_04_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
