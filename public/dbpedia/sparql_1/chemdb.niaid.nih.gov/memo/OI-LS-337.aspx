

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-337.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xqOFsk2+esZ7AkV7bPSS5dn+VCtv76GM3GuwhTuCtt3xUrDQmT+Jr9zlaTzs3Zzlrv7j7i5DVE9oPNe6BCiuvNHGtc/hzACCYQxFKTUDxIXpTznU/8W19Gd3MgQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E78DE50" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-337-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48243   OI-LS-337; PUBMED-OI-12/12/2005</p>

    <p class="memofmt1-2">          1-Deoxy-D-Xylulose 5-Phosphate Reductoisomerase (IspC) from Mycobacterium tuberculosis: towards Understanding Mycobacterial Resistance to Fosmidomycin</p>

    <p>          Dhiman, RK, Schaeffer, ML, Bailey, AM, Testa, CA, Scherman, H, and Crick, DC</p>

    <p>          J Bacteriol <b>2005</b>.  187(24): 8395-402</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321944&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16321944&amp;dopt=abstract</a> </p><br />

    <p>2.     48244   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Cloning and expression of isocitrate lyase, a key enzyme of the glyoxylate cycle, of Candida albicans for development of antifungal drugs</p>

    <p>          Shin, Dong-Sun, Kim, Sanghee, Yang, Hyeong-Cheol, and Oh, Ki-Bong</p>

    <p>          Journal of Microbiology and Biotechnology <b>2005</b>.  15(3): 652-655</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     48245   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          An update on antifungal targets and mechanisms of resistance in Candida albicans</p>

    <p>          Akins, Robert</p>

    <p>          Medical Mycology <b>2005</b>.  43(4): 285-318</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48246   OI-LS-337; WOS-OI-12/1/2005</p>

    <p class="memofmt1-2">          Recent advances in discovery and development of promising therapeutics against hepatitis C virus NS5B RNA-dependent RNA polymerase</p>

    <p>          Wu, JZ, Yao, NH, Walker, M, and Hong, Z</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY <b>2005</b>.  5(12): 1103-1112, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233099600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233099600006</a> </p><br />

    <p>5.     48247   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of phthalimido(2-aryl-3-isonicotinamido-4-oxo-1,3-thiazolidine-5-yl) ethanoates</p>

    <p>          Sharma, Ranjana, Ahmed, M, Sharma, Kanika, and Talesara, GL</p>

    <p>          Indian Journal of Pharmaceutical Sciences <b>2005</b>.  67(4): 462-466</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     48248   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Tin(IV) complexes of pyrrolidinedithiocarbamate: synthesis, characterisation and antifungal activity</p>

    <p>          Menezes, DC, Vieira, FT, de Lima, GM, Porto, AO, Cortes, ME, Ardisson, JD, and Albrecht-Schmitt, TE</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(12): 1277-1282</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     48249   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Metal complexes of 2-benzoylpyridine-derived thiosemicarbazones: structural, electrochemical and biological studies</p>

    <p>          Costa, Ricardo FF, Rebolledo, Anayive P, Matencio, Tulio, Calado, Hallen DR, Ardisson, Jose D, Cortes, Maria E, Rodrigues, Bernardo L, and Beraldo, Heloisa</p>

    <p>          Journal of Coordination Chemistry <b>2005</b>.  58(15): 1307-1319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     48250   OI-LS-337; WOS-OI-12/1/2005</p>

    <p class="memofmt1-2">          Facile synthesis of active antitubercular, cytotoxic and antibacterial agents: a Michael addition approach</p>

    <p>          Chande, MS, Verma, RS, Barve, PA, Khanwelkar, RR, Vaidya, RB, and Ajaikumar, KB</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  40(11): 1143-1148, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233184600010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233184600010</a> </p><br />

    <p>9.     48251   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Compound IKD-8344, a selective growth inhibitor against the mycelial form of Candida albicans, isolated from Streptomyces sp. A6792</p>

    <p>          Hwang, Eui Il, Yun, Bong Sik, Yeo, Woon Hyung, Lee, Sang Han, Moon, Jae Sun, Kim, Young Kook, Lim, Se Jin, and Kim, Sung Uk</p>

    <p>          Journal of Microbiology and Biotechnology <b>2005</b>.  15(4): 909-912</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48252   OI-LS-337; WOS-OI-12/1/2005</p>

    <p class="memofmt1-2">          Organophosphorus chemistry: Therapeutic intervention in mechanisms of viral and cellular replication</p>

    <p>          Wardle, NJ, Bligh, SWA, and Hudson, HR</p>

    <p>          CURRENT ORGANIC CHEMISTRY <b>2005</b>.  9(18): 1803-1828, 26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233104300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233104300002</a> </p><br />

    <p>11.   48253   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Pharmaceuticals and antifungal agents containing water-soluble triazoles</p>

    <p>          Uchida, Takuya, Mikojima, Yoshiko, Konoso, Toshiyuki, and Shibayama, Takahiro</p>

    <p>          PATENT:  JP <b>2005263636</b>  ISSUE DATE:  20050929</p>

    <p>          APPLICATION: 2004-8458  PP: 270 pp.</p>

    <p>          ASSIGNEE:  (Sankyo Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48254   OI-LS-337; WOS-OI-12/1/2005</p>

    <p class="memofmt1-2">          Treatment of tuberculosis: present status and future prospects</p>

    <p>          Onyebujoh, P, Zumla, A, Ribeiro, I, Rustomjee, R, Mwaba, P, Gomes, M, and Grange, JM</p>

    <p>          BULLETIN OF THE WORLD HEALTH ORGANIZATION <b>2005</b>.  83(11): 857-865, 9</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233185100015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233185100015</a> </p><br />

    <p>13.   48255   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          A murine model of dual infection with cytomegalovirus and Pneumocystis carinii: Effects of virus-induced immunomodulation on disease progression</p>

    <p>          Qureshi, Mahboob H, Garvy, Beth A, Pomeroy, Claire, Inayat, Mohammed S, and Oakley, Oliver R</p>

    <p>          Virus Research  <b>2005</b>.  114(1-2): 35-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48256   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Synthesis and evaluation of demethoxyviridin derivatives as potential antimicrobials</p>

    <p>          Kiran, I, Ilhan, S, Akar, T, Tur, L, and Erol, E</p>

    <p>          ZEITSCHRIFT FUR NATURFORSCHUNG C-A JOURNAL OF BIOSCIENCES <b>2005</b>.  60(9-10): 686-692, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233276000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233276000005</a> </p><br />
    <br clear="all">

    <p>15.   48257   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Mycobacterial resistance to reactive oxygen and nitrogen intermediates: Recent views and progress in M. Tuberculosis</p>

    <p>          Rhee, Kyu Y</p>

    <p>          Mycobacterium <b>2005</b>.: 321-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48258   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Update on antimicrobial agents: new indications of older agents</p>

    <p>          Pasquale, Timothy R and Tan, James S</p>

    <p>          Expert Opinion on Pharmacotherapy <b>2005</b>.  6(10): 1681-1691</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   48259   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Antitubercular sterols from Thalia multiflora Horkel ex Koernicke</p>

    <p>          Gutierrez-Lugo, MT, Wang, YH, Franzblau, SG, Suarez, E, and Timmermann, BN</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2005</b>.  19(10): 876-880, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233341200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233341200008</a> </p><br />

    <p>18.   48260   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Use of GeneDecipher software for predicting protein coding sequences useful as drug targets, for development of antimicrobial agents</p>

    <p>          Brahmachari, Samir Kumar, Dash, Debasis, Sharma, Ramakant, and Maheshwari, Jitendra Kumar</p>

    <p>          PATENT:  US <b>2005136480</b>  ISSUE DATE:  20050623</p>

    <p>          APPLICATION: 2004-34519  PP: 210 pp., Cont.-in-part of U.S. Ser. No. 727,989.</p>

    <p>          ASSIGNEE:  (India)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48261   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          New fungal metabolite geranylgeranyltransferase inhibitors with antifungal activity</p>

    <p>          Singh, SB, Kelly, R, Guan, ZQ, Polishook, JD, Dombrowski, AW, Collado, J, Gonzalez, A, Pelaez, F, Register, E, Kelly, TM, Bonfiglio, C, and Williamson, JM</p>

    <p>          NATURAL PRODUCT RESEARCH <b>2005</b>.  19(8): 739-747, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233410700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233410700003</a> </p><br />

    <p>20.   48262   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Detection of anti-hepatitis C virus effects of interferon and ribavirin by a sensitive replicon system</p>

    <p>          Kato, T, Date, T, Miyamoto, M, Sugiyama, M, Tanaka, Y, Orito, E, Ohno, T, Sugihara, K, Hasegawa, I, Fujiwara, K, Ito, K, Ozasa, A, Mizokami, M, and Wakita, T</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2005</b>.  43(11): 5679-5684, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233312200042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233312200042</a> </p><br />

    <p>21.   48263   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Phenyl(quinazolinyl)amines as UL 97-kinase inhibitors for the treatment of human cytomegaloviral and other herpesviral infections</p>

    <p>          Herget, Thomas </p>

    <p>          PATENT:  WO <b>2005040125</b>  ISSUE DATE:  20050506</p>

    <p>          APPLICATION: 2004  PP: 56 pp.</p>

    <p>          ASSIGNEE:  (Axxima Pharmaceuticals A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>22.   48264   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Conditional depletion of KasA, a key enzyme of mycolic acid biosynthesis, leads to mycobacterial cell lysis</p>

    <p>          Bhatt, A, Kremer, L, Dai, AZ, Sacchettini, JC, and Jacobs, WR</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(22): 7596-7606, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233400200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233400200007</a> </p><br />

    <p>23.   48265   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          QSAR analysis of 2,4-diaminopyrido[2,3-d]pyrimidines and 2,4-diaminopyrrolo[2,3-d]pyrimidines as dihydrofolate reductase inhibitors</p>

    <p>          Jain, P, Soni, LK, Gupta, AK, and Kashkedikar, SG</p>

    <p>          INDIAN JOURNAL OF BIOCHEMISTRY &amp; BIOPHYSICS <b>2005</b>.  42(5): 315-320, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233316300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233316300007</a> </p><br />

    <p>24.   48266   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of imidazol-4-yl-N&#39;-phenylureas as antiviral agents</p>

    <p>          Zimmermann, Holger, Brueckner, David, Heimbach, Dirk, Hendrix, Martin, Henninger, Kerstin, Hewlett, Guy, Rosentreter, Ulrich, Keldenich, Joerg, Lang, Dieter, and Radtke, Martin</p>

    <p>          PATENT:  WO <b>2005092865</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005  PP: 124 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   48267   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Screening of antiviral drugs, and pharmaceutical compositions containing 2-thioxothiazolidinone derivatives</p>

    <p>          Gregor, Paul, Harris, Nicholas, and Zhuk, Regina</p>

    <p>          PATENT:  WO <b>2005089067</b>  ISSUE DATE:  20050929</p>

    <p>          APPLICATION: 2005  PP: 52 pp.</p>

    <p>          ASSIGNEE:  (Rimonyx Pharmaceuticals Ltd., Israel</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   48268   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of 2&#39;-C-methyl nucleoside derivatives and their uses for the treatment of hepatitis C viral infection</p>

    <p>          Reddy, KRaja and Erion, Mark D</p>

    <p>          PATENT:  US <b>2005182252</b>  ISSUE DATE:  20050818</p>

    <p>          APPLICATION: 2004-51247  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   48269   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Novel potent allosteric inhibitors of HCV NS5B polymerase</p>

    <p>          Brochu, Christian, Coulombe, Rene, Gillard, James R, Jolicoeur, Eric, Kohlbauer, Peter, Kuhn, Peter, Poupart, Marc-Andre, Lefebvre, Sylvain, McKercher, Ginette, Kukolj, George, and Beaulieu, Pierre L</p>

    <p>          Abstracts of Papers, 230th ACS National Meeting, Washington, DC, United States, Aug. 28-Sept. 1, 2005 <b>2005</b>.: MEDI-015</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>28.   48270   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of (1R,2S,5S)-N-[(1S)-3-amino-1-(cyclobutylmethyl)-2,3-dioxopropyl]-3-[(2S)-2-[[[(1,1-dimethylethyl)amino]carbonyl]amino]-3,3-dimethyl-1-oxobutyl]-6,6-dimethyl-3-azabicyclo[3.1.0]hexane-2-carboxamide as inhibitor of hepatitis C virus NS3/NS4a serine protease</p>

    <p>          Njoroge, FGeorge and Venkatraman, Srikanth</p>

    <p>          PATENT:  US <b>20050249702</b>  ISSUE DATE: 20051110</p>

    <p>          APPLICATION: 2005-55897  PP: 14 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   48271   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of N-acyldihydropyrroles as hepatitis C virus (HCV) inhibitors</p>

    <p>          Haigh, David and Slater, Martin John</p>

    <p>          PATENT:  WO <b>2005103045</b>  ISSUE DATE:  20051103</p>

    <p>          APPLICATION: 2005  PP: 45 pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   48272   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          In Vitro Studies of Cross-resistance Mutations against Two Hepatitis C Virus Serine Protease Inhibitors, VX-950 and BILN 2061</p>

    <p>          Lin, Chao, Gates, Cynthia A, Rao, BGovinda, Brennan, Debra L, Fulghum, John R, Luong, Yu-Ping, Frantz, JDaniel, Lin, Kai, Ma, Sue, Wei, Yun-Yi, Perni, Robert B, and Kwong, Ann D</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(44): 36784-36791</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   48273   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Demonstration of the efficacy of small molecule HCV inhibitors in the KMT mouse model of hepatitis C virus (HCV) infection</p>

    <p>          Kneteman, N</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 532A-533A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301398">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301398</a> </p><br />

    <p>32.   48274   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Identification of a novel small molecule hepatitis C virus replication inhibitor that targets host sphingolipid biosynthesis</p>

    <p>          Sakamoto, H, Okamoto, K, Aoki, M, Kato, H, Katsume, A, Ohta, A, Tsukuda, T, Shimma, N, Aoki, Y, Arisawa, M, Kohara, M, and Sudoh, M</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 535A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301404">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301404</a> </p><br />

    <p>33.   48275   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Heterocyclic anti-viral compounds comprising metabolizable moieties and their uses as inhibitors of hepatitis C virus replication and/or proliferation for treatment of hepatitis C infection</p>

    <p>          Singh, Rajinder, Goff, Dane, Kolluri, Rao SS, Darwish, Ihab S, Partridge, John, Cooper, Robin, Lu, Henry H, and Park, Gary</p>

    <p>          PATENT:  WO <b>2005097760</b>  ISSUE DATE:  20051020</p>

    <p>          APPLICATION: 2005  PP: 149 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>34.   48276   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          SCH 503034, a mechanism-based inhibitor of hepatitis C virus (HCV) NS3 protease suppresses polyprotein maturation and enhances the antiviral activity of interferona-211 (INF)</p>

    <p>          Malcolm, BA, Arassappan, A, Bennett, F, Bogen, S, Chase, R, Chen, K, Chen, T, Ingravallo, P, Jao, E, Kong, S, Lahser, F, Liu, R, Liu, YT, Lovey, R, McCormick, J, Njoroge, GF, Saksana, A, Skelton, A, Tong, X, Venkatraman, S, Wright-Minogue, J, Xia, E, and Girijavallabhan, V</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 535A-536A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301405">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301405</a> </p><br />

    <p>35.   48277   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          NIM811 exhibits potent anti-HCV activity in vitro and represents a novel approach for viral hepatitis C therapy</p>

    <p>          Lin, K, Ma, S, Boerner, J, Huang, MM, Ryder, N, Weidmann, B, and Cooreman, MP</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 536A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301407">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301407</a> </p><br />

    <p>36.   48278   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of pyrazole-4-carboxylic acids as HCV RNA polymerase inhibitors for the treatment of viral infections</p>

    <p>          Bravi, Gianpaolo, Corfield, John Andrew, Grimes, Richard Martin, Guidetti, Rossella, Lovegrove, Victoria Lucy Helen, Mordaunt, Jacqueline Elizabeth, Shah, Pritom, and Slater, Martin John</p>

    <p>          PATENT:  WO <b>2005092863</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005  PP: 140 pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   48279   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Immunophenotyping profile of CPG 10101, a new TLR9 agonist antiviral for hepatitis C</p>

    <p>          McHutchinson, JG, Bacon, BR, Gordon, SC, Lawitz, E, Shiffman, M, Afdhal, NH, Jacobson, IM, Muir, A, Efler, SM, Vicari, A, Myette, K, Ahluwalia, N, Murzenok, PP, Lipford, G, Schmalbach, TK, Krieg, AM, and Davis, HL</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 539A-540A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301414">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480301414</a> </p><br />

    <p>38.   48280   OI-LS-337; SCIFINDER-OI-12/5/2005</p>

    <p class="memofmt1-2">          Preparation of novel peptidyl ketoamides with cyclic p4s as inhibitors of NS3 serine protease of hepatitis C virus</p>

    <p>          Chen, Kevin X, Njoroge, FGeorge, Sannigrahi, Mousumi, Nair, Latha G, Yang, Weiying, Vibulbhan, Bancha, Venkatraman, Srikanth, Arasappan, Ashok, Bogen, Stephane L, Bennett, Frank, and Girijavallabhan, Viyyoor M</p>

    <p>          PATENT:  WO <b>2005085242</b>  ISSUE DATE:  20050915</p>

    <p>          APPLICATION: 2005  PP: 282 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   48281   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Two flavonoids extracts from a harb, glycyrrhizae radix, inhibit in-vitro hepatitis C virus replication</p>

    <p>          Sekine, Y, Sakamoto, N, Nakagawa, M, Itsui, Y, Tasaka, M, Chen, CH, and Watanabe, M</p>

    <p>          HEPATOLOGY <b>2005</b>.  42(4): 702A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302336">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232480302336</a> </p><br />

    <p>40.   48282   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          The modeled structure of the RNA dependent RNA polymerase of GBV-C Virus suggests a role for motif E in Flaviviridae RNA polymerases</p>

    <p>          Ferron, F, Bussetta, C, Dutartre, H, and Canard, B</p>

    <p>          BMC BIOINFORMATICS <b>2005</b>.  6: 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233338800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233338800001</a> </p><br />

    <p>41.   48283   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Piperazine propanol derivative as a novel antifungal targeting 1,3-beta-D-glucan synthase</p>

    <p>          Kondoh, O, Inagaki, Y, Fukuda, H, Mizuguchi, E, Ohya, Y, Arisawa, M, Shimma, N, Aoki, Y, Sakaitani, M, and Watanabe, T</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  28(11): 2138-2141, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233348500022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233348500022</a> </p><br />

    <p>42.   48284   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          ChemDB: a public database of small molecules and related chemoinformatics resources</p>

    <p>          Chen, J, Swamidass, SJ, Bruand, J, and Baldi, P</p>

    <p>          BIOINFORMATICS  <b>2005</b>.  21(22): 4133-4139, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233218100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233218100009</a> </p><br />

    <p>43.   48285   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Gene expression profiling analysis of Mycobacterium tuberculosis genes in response to salicylate</p>

    <p>          Denkin, S, Byrne, S, Jie, C, and Zhang, Y</p>

    <p>          ARCHIVES OF MICROBIOLOGY <b>2005</b>.  184(3): 152-157, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233242900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233242900002</a> </p><br />

    <p>44.   48286   OI-LS-337; WOS-OI-12/4/2005</p>

    <p class="memofmt1-2">          Proline-based macrocyclic inhibitors of the hepatitis C virus: Stereoselective synthesis and biological activity</p>

    <p>          Chen, KX, Njoroge, EG, Vibulbhan, B, Prongay, A, Pichardo, J, Madison, V, Buevich, A, and Chan, TM</p>

    <p>          ANGEWANDTE CHEMIE-INTERNATIONAL EDITION <b>2005</b>.  44(43): 7024-7028, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233279800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233279800005</a> </p><br />

    <p>45.   48287   OI-LS-337; WOS-OI-12/11/2005</p>

    <p class="memofmt1-2">          A highly efficient, asymmetric synthesis of benzothiadiazine-substituted tetramic acids: Ppotent inhibitors of hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Fitch, DM, Evans, KA, Chai, DP, and Duffy, KJ</p>

    <p>          ORGANIC LETTERS <b>2005</b>.  7(24): 5521-5524, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233505700044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233505700044</a> </p><br />

    <p>46.   48288   OI-LS-337; WOS-OI-12/11/2005</p>

    <p class="memofmt1-2">          Tetrahydrobenzothiophene inhibitors of hepatitis C virus NS5B polymerase</p>

    <p>          LaPorte, MG, Lessen, TA, Leister, L, Cebzanov, D, Amparo, E, Faust, C, Ortlip, D, Bailey, TR, Nitz, TJ, Chunduru, SK, Young, DC, and Burns, CJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(1): 100-103, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233516200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233516200020</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
