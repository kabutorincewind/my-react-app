

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-354.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ARpDRhBQhwlREvt+3RT8A5Wh1XklWuvlU2aDXHApFNpZ2okTOzB7hYKDr2YIhcVK06CmnmZ6qhpmrLJx3owlYaAXS/vKwX7a1ChforWQXtEebE+4aE1nOmEjh3k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8C20085C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-354-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     49257   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Bleomycin is a Potent Small-Molecule Inhibitor of Hepatitis C Virus Replication</p>

    <p>          Rakic, B, Brulotte, M, Rouleau, Y, Belanger, S, and Pezacki, JP</p>

    <p>          Chembiochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888741&amp;dopt=abstract</a> </p><br />

    <p>2.     49258   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Understanding the action of INH on a highly INH-resistant Mycobacterium tuberculosis strain using Genechips</p>

    <p>          Fu, Li M and Shinnick, Thomas M</p>

    <p>          Tuberculosis <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXK-4KJDWYW-1/2/a7effe9ed0e2dcea2d1ca6b1eb0590db">http://www.sciencedirect.com/science/article/B6WXK-4KJDWYW-1/2/a7effe9ed0e2dcea2d1ca6b1eb0590db</a> </p><br />

    <p>3.     49259   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Antimycobacterial Agents. Novel Diarylpyrrole Derivatives of BM212 Endowed with High Activity toward Mycobacterium tuberculosis and Low Cytotoxicity</p>

    <p>          Biava, M, Porretta, GC, Poce, G, Supino, S, Deidda, D, Pompei, R, Molicotti, P, Manetti, F, and Botta, M</p>

    <p>          J Med Chem <b>2006</b>.  49(16): 4946-4952</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884306&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884306&amp;dopt=abstract</a> </p><br />

    <p>4.     49260   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Brunsvicamides A-C: Sponge-Related Cyanobacterial Peptides with Mycobacteriumtuberculosis Protein Tyrosine Phosphatase Inhibitory Activity</p>

    <p>          Muller, D, Krick, A, Kehraus, S, Mehner, C, Hart, M, Kupper, FC, Saxena, K, Prinz, H, Schwalbe, H, Janning, P, Waldmann, H, and Konig, GM</p>

    <p>          J Med Chem <b>2006</b>.  49(16): 4871-4878</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884299&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884299&amp;dopt=abstract</a> </p><br />

    <p>5.     49261   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Intracellular growth and drug susceptibility of Mycobacterium tuberculosis in macrophages</p>

    <p>          Chanwong, Sakarin, Maneekarn, Niwat, Makonkawkeyoon, Luksana, and Makonkawkeyoon, Sanit</p>

    <p>          Tuberculosis <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXK-4KFV3G2-1/2/614fc58c7859f21d4d88572f27ab7a95">http://www.sciencedirect.com/science/article/B6WXK-4KFV3G2-1/2/614fc58c7859f21d4d88572f27ab7a95</a> </p><br />
    <br clear="all">

    <p>6.     49262   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Drug resistance in Mycobacterium tuberculosis</p>

    <p>          Johnson, R, Streicher, EM, Louw, GE, Warren, RM, van, Helden PD, and Victor, TC</p>

    <p>          Curr Issues Mol Biol <b>2006</b>.  8(2): 97-111</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16878362&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16878362&amp;dopt=abstract</a> </p><br />

    <p>7.     49263   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Entry and intracellular replication of Mycobacterium tuberculosis in cultured human microvascular endothelial cells</p>

    <p>          Mehta, Parmod K, Karls, Russell K, White, Elizabeth H, Ades, Edwin W, and Quinn, Frederick D</p>

    <p>          Microbial Pathogenesis <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WN6-4KGG1NV-1/2/f5946d360a8956e521b6c81ba5cad854">http://www.sciencedirect.com/science/article/B6WN6-4KGG1NV-1/2/f5946d360a8956e521b6c81ba5cad854</a> </p><br />

    <p>8.     49264   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Recruitment of an interferon molecular signaling complex to the mitochondrial membrane: Disruption by hepatitis C virus NS3-4A protease</p>

    <p>          Hiscott, J, Lacoste, J, and Lin, R</p>

    <p>          Biochem Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16876765&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16876765&amp;dopt=abstract</a> </p><br />

    <p>9.     49265   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Serine palmitoyltransferase inhibitor suppresses HCV replication in a mouse model</p>

    <p>          Umehara, Takuya, Sudoh, Masayuki, Yasui, Fumihiko, Matsuda, Chiho, Hayashi, Yukiko, Chayama, Kazuaki, and Kohara, Michinori</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  346(1): 67-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4K1G451-5/2/717388dac90a0b82815d00acf5b891d2">http://www.sciencedirect.com/science/article/B6WBK-4K1G451-5/2/717388dac90a0b82815d00acf5b891d2</a> </p><br />

    <p>10.   49266   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Symmetrical and unsymmetrical analogues of isoxyl; active agents against Mycobacterium tuberculosis</p>

    <p>          Bhowruth, V, Brown, AK, Reynolds, RC, Coxon, GD, Mackay, SP, Minnikin, DE, and Besra, GS</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875817&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875817&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   49267   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Crystal Structure and Activity Studies of the Mycobacterium tuberculosis {beta}-Lactamase Reveal Its Critical Role in Resistance to {beta}-Lactam Antibiotics</p>

    <p>          Wang, F, Cassidy, C, and Sacchettini, JC</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(8): 2762-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870770&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870770&amp;dopt=abstract</a> </p><br />

    <p>12.   49268   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Gatifloxacin derivatives: Synthesis, antimycobacterial activities, and inhibition of Mycobacterium tuberculosis DNA gyrase</p>

    <p>          Sriram, Dharmarajan, Aubry, Alexandra, Yogeeswari, Perumal, and Fisher, LM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(11): 2982-2985</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4JJ2BJM-2/2/27d1f9018ac921fd3763b08fdf29ceb7">http://www.sciencedirect.com/science/article/B6TF9-4JJ2BJM-2/2/27d1f9018ac921fd3763b08fdf29ceb7</a> </p><br />

    <p>13.   49269   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Combination Chemotherapy with the Nitroimidazopyran PA-824 and First-Line Drugs in a Murine Model of Tuberculosis</p>

    <p>          Nuermberger, E, Rosenthal, I, Tyagi, S, Williams, KN, Almeida, D, Peloquin, CA, Bishai, WR, and Grosset, JH</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(8): 2621-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870750&amp;dopt=abstract</a> </p><br />

    <p>14.   49270   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Crystallographic and Pre-steady-state Kinetics Studies on Binding of NADH to Wild-type and Isoniazid-resistant Enoyl-ACP(CoA) Reductase Enzymes from Mycobacterium tuberculosis</p>

    <p>          Oliveira, Jaim S, Pereira, Jose H, Canduri, Fernanda, Rodrigues, Nathalia C, de Souza, Osmar N, de Azevedo, Jr Walter F, Basso, Luiz A, and Santos, Diogenes S</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  359(3): 646-666</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4JSG6FC-1/2/1b662ac95819caeaf7d73a5fb4965c4f">http://www.sciencedirect.com/science/article/B6WK7-4JSG6FC-1/2/1b662ac95819caeaf7d73a5fb4965c4f</a> </p><br />

    <p>15.   49271   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Synthesis, in vitro and in vivo antimycobacterial activities of diclofenac acid hydrazones and amides</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Devakaram, Ruth Vandana</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(9): 3113-3118</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4J2KT9Y-7/2/3eb87cefe86b1239c68810c8ce3f85b9">http://www.sciencedirect.com/science/article/B6TF8-4J2KT9Y-7/2/3eb87cefe86b1239c68810c8ce3f85b9</a> </p><br />
    <br clear="all">

    <p>16.   49272   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Functional characterization of a fatty acyl-CoA-binding protein (ACBP) from the apicomplexan Cryptosporidium parvum</p>

    <p>          Zeng, B, Cai, X, and Zhu, G</p>

    <p>          Microbiology <b>2006</b>.  152(Pt 8): 2355-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849800&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16849800&amp;dopt=abstract</a> </p><br />

    <p>17.   49273   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Enhanced susceptibility of multidrug resistant strains of Mycobacterium tuberculosis to granulysin peptides correlates with a reduced fitness phenotype</p>

    <p>          Toro, Juan Carlos, Hoffner, Sven, Linde, Charlotte, Andersson, Mats, Andersson, Jan, and Grundstrom, Susanna</p>

    <p>          Microbes and Infection <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4K3CXST-1/2/c024fd5a6120d48a8109b81bfea7f6b0">http://www.sciencedirect.com/science/article/B6VPN-4K3CXST-1/2/c024fd5a6120d48a8109b81bfea7f6b0</a> </p><br />

    <p>18.   49274   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Synthesis of pyrazinamide Mannich bases and its antitubercular properties</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Reddy, Sushma Pobba</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(8): 2113-2116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J6X3N2-7/2/b1717482dd482fe09bd8dfa6c1620723">http://www.sciencedirect.com/science/article/B6TF9-4J6X3N2-7/2/b1717482dd482fe09bd8dfa6c1620723</a> </p><br />

    <p>19.   49275   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          l-Nucleoside enantiomers as antivirals drugs: A mini-review</p>

    <p>          Mathe, Christophe and Gosselin, Gilles</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-1/2/7c9e5bd48ebba0d36b3d74cfefe3a1b1">http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-1/2/7c9e5bd48ebba0d36b3d74cfefe3a1b1</a> </p><br />

    <p>20.   49276   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Ileabethoxazole: a novel benzoxazole alkaloid with antimycobacterial activity</p>

    <p>          Rodriguez, Ileana I, Rodriguez, Abimael D, Wang, Yuehong, and Franzblau, Scott G</p>

    <p>          Tetrahedron Letters <b>2006</b>.  47(19): 3229-3232</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4JJGB0S-B/2/736d60a19fe2382b77e00eafa6c30c04">http://www.sciencedirect.com/science/article/B6THS-4JJGB0S-B/2/736d60a19fe2382b77e00eafa6c30c04</a> </p><br />

    <p>21.   49277   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Toxoplasma gondii purine nucleoside phosphorylase: Biochemical characterization, inhibitor profiles and comparison with the plasmodium falciparum ortholog</p>

    <p>          Chaudhary, K, Ting, LM, Kim, K, and Roos, DS</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16829527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16829527&amp;dopt=abstract</a> </p><br />

    <p>22.   49278   OI-LS-354; PUBMED-OI-8/7/2006</p>

    <p class="memofmt1-2">          Avian influenza (H5N1)--the next pandemic?</p>

    <p>          Rivera, SJ</p>

    <p>          Imprint <b>2006</b>.  53(3): 56, 58, 62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821532&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821532&amp;dopt=abstract</a> </p><br />

    <p>23.   49279   OI-LS-354; WOS-OI-7/30/2006</p>

    <p class="memofmt1-2">          Synthesis of the antimycobacterial naphthoquinone, 7-methyljuglone and its dimer, neodiospyrin</p>

    <p>          van der Kooy, F and Meyer, JJM</p>

    <p>          SOUTH AFRICAN JOURNAL OF CHEMISTRY-SUID-AFRIKAANSE TYDSKRIF VIR CHEMIE <b>2006</b>.  59: 60-61, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239036100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239036100006</a> </p><br />

    <p>24.   49280   OI-LS-354; WOS-OI-7/30/2006</p>

    <p class="memofmt1-2">          Nonstructural protein 5B of hepatitis C virus</p>

    <p>          Lee, JH, Nam, IY, and Myung, H</p>

    <p>          MOLECULES AND CELLS <b>2006</b>.  21(3): 330-336, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238843800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238843800002</a> </p><br />

    <p>25.   49281   OI-LS-354; WOS-OI-7/30/2006</p>

    <p class="memofmt1-2">          Substituted pyrazinecarboxamides: Synthesis and biological evaluation</p>

    <p>          Dolezal, M, Palek, L, Vinsova, J, Buchta, V, Jampilek, J, and Kralova, K</p>

    <p>          MOLECULES <b>2006</b>.  11(4): 242-256, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238940100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238940100003</a> </p><br />

    <p>26.   49282   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Beyond interferon and ribavirin: Antiviral therapies for hepatitis C virus</p>

    <p>          Kwong, Ann D, Cowherd, Sarah, and Mueller, Peter</p>

    <p>          Drug Discovery Today: Therapeutic Strategies <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75D9-4KDBM66-1/2/fb3ffcdd0bbbbd02b81f49fe622a6504">http://www.sciencedirect.com/science/article/B75D9-4KDBM66-1/2/fb3ffcdd0bbbbd02b81f49fe622a6504</a> </p><br />

    <p>27.   49283   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Hepatotoxicity of antiretrovirals: Incidence, mechanisms and management: Proceedings of the 1st European Consensus Conference on the Treatment of Chronic Hepatitis B and C in HIV Co-infected Patients</p>

    <p>          Nunez, Marina</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(Supplement 1): S132-S139</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4HNSR78-4/2/022d47488fc89c044da925b95bf8eef0">http://www.sciencedirect.com/science/article/B6W7C-4HNSR78-4/2/022d47488fc89c044da925b95bf8eef0</a> </p><br />
    <br clear="all">

    <p>28.   49284   OI-LS-354; WOS-OI-7/30/2006</p>

    <p class="memofmt1-2">          Model-based drug development: The road to quantitative pharmacology</p>

    <p>          Zhang, LP, Sinha, V, Forgue, ST, Callies, S, Ni, L, Peck, R, and Allerheiligen, SRB</p>

    <p>          JOURNAL OF PHARMACOKINETICS AND PHARMACODYNAMICS <b>2006</b>.  33(3): 369-393, 25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238862600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238862600006</a> </p><br />

    <p>29.   49285   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          78 The HCV NS3 protease inhibitor SCH 503034 in combination with PEG-IFN[alpha]-2b in the treatment of HCV-1 PEG-IFN[alpha]-2b non-responders: Antiviral activity and HCV variant analysis</p>

    <p>          Zeuzem, S, Sarrazin, C, Wagner, F, Rouzier, R, Forestier, N, Gupta, S, Hussain, M, Shah, A, Cutler, D, and Zhang, J</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(Supplement 2): S35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4KCHKWH-2T/2/2f9b986834323c0fa308d59c6d83c51a">http://www.sciencedirect.com/science/article/B6W7C-4KCHKWH-2T/2/2f9b986834323c0fa308d59c6d83c51a</a> </p><br />

    <p>30.   49286   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Search for antibacterial and antifungal agents from selected Indian medicinal plants</p>

    <p>          Kumar, VPrashanth, Chauhan, Neelam S, Padh, Harish, and Rajani, M</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4JK4DBM-1/2/be6e3a8abfc0d9dc30fc89af604399fb">http://www.sciencedirect.com/science/article/B6T8D-4JK4DBM-1/2/be6e3a8abfc0d9dc30fc89af604399fb</a> </p><br />

    <p>31.   49287   OI-LS-354; EMBASE-OI-8/7/2006</p>

    <p class="memofmt1-2">          Microwave-assisted synthesis of antimicrobial dihydropyridines and tetrahydropyrimidin-2-ones: Novel compounds against aspergillosis</p>

    <p>          Chhillar, Anil K, Arya, Pragya, Mukherjee, Chandrani, Kumar, Pankaj, Yadav, Yogesh, Sharma, Ajendra K, Yadav, Vibha, Gupta, Jyotsana, Dabur, Rajesh, and Jha, Hirday N</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(4): 973-981</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H8MNSK-D/2/116a4c3eedde1edf0b37f23a70923f6a">http://www.sciencedirect.com/science/article/B6TF8-4H8MNSK-D/2/116a4c3eedde1edf0b37f23a70923f6a</a> </p><br />

    <p>32.   49288   OI-LS-354; WOS-OI-7/30/2006</p>

    <p class="memofmt1-2">          Crystal structure of Mycobacterium tuberculosis shikimate kinase in complex with shikimic acid and an ATP analogue</p>

    <p>          Gan, JH, Gu, YJ, Li, Y, Yan, HG, and Ji, XH</p>

    <p>          BIOCHEMISTRY <b>2006</b>.  45(28): 8539-8545, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238924600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238924600009</a> </p><br />

    <p>33.   49289   OI-LS-354; WOS-OI-8/6/2006</p>

    <p class="memofmt1-2">          Development of a synthetic process towards a hepatitis C polymerase inhibitor</p>

    <p>          Camp, D, Matthews, CF, Neville, ST, Rouns, M, Scott, RW, and Truong, Y</p>

    <p>          ORGANIC PROCESS RESEARCH &amp; DEVELOPMENT <b>2006</b>.  10(4): 814-821, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239167500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239167500018</a> </p><br />
    <br clear="all">

    <p>34.   49290   OI-LS-354; WOS-OI-8/6/2006</p>

    <p class="memofmt1-2">          Designing better drugs: predicting cytochrome P450 metabolism</p>

    <p>          de Groot, MJ</p>

    <p>          DRUG DISCOVERY TODAY <b>2006</b>.  11(13-14): 601-606, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239081100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239081100005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
