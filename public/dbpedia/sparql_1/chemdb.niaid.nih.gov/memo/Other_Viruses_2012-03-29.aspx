

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-03-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dVLz7cxYJR2LAZdDUa1grUHCPSxyVQ1pebaCeI0TEWxZrdmVxEVUQikM7OyOWSyDXQEcjpvxL2dFjRauG0rmZuZeSWipmHnZLKeNLc8G1phIYitETbAxofP7F0k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A0B9DED1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
     <h1 class="memofmt2-h1">Other Viruses Citation List:  March 16 - March 29, 2012</h1>

    <h2>Hepatitis B virus</h2>

    <p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22452880">Antiviral Activity of FNC, 2&#39;-Deoxy-2&#39;-beta-fluoro-4&#39;-azidocytidine, against Human and Duck HBV Replication.</a> Zheng, L., Q. Wang, X. Yang, X. Guo, L. Chen, L. Tao, L. Dong, Y. Li, H. An, X. Yu, and J. Chang, Antiviral Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22452880].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
 
    <p>2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22417649">Synthesis and Antiviral Activity of 6-Deoxycyclopropavir, a New Prodrug of Cyclopropavir.</a> Li, C., D.C. Quenelle, M.N. Prichard, J.C. Drach, and J. Zemlicka, Bioorganic &amp; Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22417649].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <h2>Hepatitis C Virus</h2>

    <p>3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22446091">Synthesis and Antiviral Activity of a Series of 1&#39;-Substituted 4-aza-7,9-dideazaadenosine C-nucleosides.</a> Cho, A., O.L. Saunders, T. Butler, L. Zhang, J. Xu, J.E. Vela, J.Y. Feng, A.S. Ray, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22446091].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22445328">Discovery of Flavonoid Derivatives as anti-HCV Agents via Pharmacophore Search Combining Molecular Docking Strategy.</a> Liu, M.M., L. Zhou, P.L. He, Y.N. Zhang, J.Y. Zhou, Q. Shen, X.W. Chen, J.P. Zuo, W. Li, and D.Y. Ye, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22445328].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22444190">A 2&#39;-Deoxy-2&#39;-fluoro-2&#39;-C-methyl uridine cyclopentyl carbocyclic Analog and Its Phosphoramidate Prodrug as Inhibitors of HCV NS5B Polymerase.</a> Liu, J., J. Du, P. Wang, D. Nagarathnam, C.L. Espiritu, H. Bao, E. Murakami, P.A. Furman, and M.J. Sofia, Nucleosides, Nucleotides &amp; Nucleic acids, 2012. 31(4): p. 277-285; PMID[22444190].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430955">Genotype and Subtype Profiling of PSI-7977 as a Nucleotide Inhibitor of Hepatitis C Virus.</a> Lam, A.M., C. Espiritu, S. Bansal, H.M. Micolochick Steuer, C. Niu, V. Zennou, M. Keilman, Y. Zhu, S. Lan, M.J. Otto, and P.A. Furman, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22430955].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22425564">Inhibition of Hepatitis C Virus NS5A by Fluoro-olefin Based Gamma-turn Mimetics.</a> Chang, W., R.T. Mosley, S. Bansal, M. Keilman, A.M. Lam, P.A. Furman, M.J. Otto, and M.J. Sofia, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22425564].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22425454">Discovery of Novel P3-oxo Inhibitor of Hepatitis C Virus NS3/4A Serine Protease.</a> Duan, M., W. Kazmierski, R. Crosby, M. Gartland, J. Ji, M. Tallant, A. Wang, R. Hamatake, L. Wright, M. Wu, Y.K. Zhang, C.Z. Ding, X. Li, Y. Liu, S. Zhang, Y. Zhou, J.J. Plattner, and S.J. Baker, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22425454].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22424979">Synthesis and SAR Studies of Novel Heteroaryl Fused Tetracyclic indole-diamide Compounds: Potent Allosteric Inhibitors of the Hepatitis C Virus NS5B Polymerase.</a> Ding, M., F. He, T.W. Hudyma, X. Zheng, M.A. Poss, J.F. Kadow, B.R. Beno, K.L. Rigat, Y.K. Wang, R.A. Fridell, J.A. Lemm, D. Qiu, M. Liu, S. Voss, L.A. Pelosi, S.B. Roberts, M. Gao, J. Knipe, and R.G. Gentles, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22424979].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <h2>HSV-1</h2>

    <p>10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22444197">Synthesis, Molecular Conformation and Activity against Herpes Simplex Virus of (E)-5-(2-Bromovinyl)-2&#39;-deoxycytidine Analogs.</a> Zoghaib, W.M., S. Mannala, V.S. Gupta, and G. Tourigny, Nucleosides, Nucleotides &amp; Nucleic acids, 2012. 31(4): p. 364-376; PMID[22444197].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22423636">Biological Activities of Purified Marennine, the Blue Pigment Responsible for the Greening of Oysters.</a> Gastineau, R., J.B. Pouvreau, C. Hellio, M. Morancais, J. Fleurence, P. Gaudin, N. Bourgougnon, and J.L. Mouget, Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22423636].
    <br />
    <b>[PubMed]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p>12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300782600024">Catalytic Metallodrugs Targeting HCV IRES RNA.</a> Bradford, S. and J.A. Cowan, Chemical Communications, 2012. 48(25): p. 3118-3120; ISI[000300782600024].
    <br />
    <b>[WOS]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300623300004">Pharmacodynamic Analysis of a Serine Protease Inhibitor, MK-4519, against Hepatitis C Virus Using a Novel in Vitro Pharmacodynamic System.</a> Brown, A.N., J.J. McSharry, J.R. Adams, R. Kulawy, R.J.O. Barnard, W. Newhard, A. Corbin, D.J. Hazuda, A. Louie, and G.L. Drusano, Antimicrobial Agents and Chemotherapy, 2012. 56(3): p. 1170-1181; ISI[000300623300004].
    <br />
    <b>[WOS]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300632900003">Hepatitis C Virus Replication-Specific Inhibition of MicroRNA Activity with Self-cleavable Allosteric Ribozyme.</a> Lee, C.H., J.H. Kim, H.W. Kim, H. Myung, and S.W. Lee, Nucleic Acid Therapeutics, 2012. 22(1): p. 17-29; ISI[000300632900003].
    <br />
    <b>[WOS]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300817000008">Lignans from the Heartwood of Streblus asper and Their Inhibiting Activities to Hepatitis B Virus.</a> Li, L.Q., J. Li, Y. Huang, Q. Wu, S.P. Deng, X.J. Su, R.Y. Yang, J.G. Huang, Z.Z. Chen, and S. Li, Fitoterapia, 2012. 83(2): p. 303-309; ISI[000300817000008].
    <br />
    <b>[WOS]</b>. OV_0316-032912.</p>
    <p> </p>
    
    <p>16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300524900260">Safety, Pharmacokinetics (pk), and Antiviral Activity of RG7348, a Novel Hepdirect (TM) Liver-targeted Double Prodrug Hepatitis C Virus (HCV) Nucleotide Polmerase Inhibitor.</a> Nieforth, K.A., P.N. Morcos, L. Chang, B. Davies, R. Li, and P.F. Smith, Clinical Pharmacology and Therapeutics, 2012. 91: p. S99-S100; ISI[000300524900260].
    <br />
    <b>[WOS]</b>. OV_0316-032912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
