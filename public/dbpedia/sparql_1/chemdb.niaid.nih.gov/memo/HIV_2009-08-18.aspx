

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/v9knr7XPMSjYs7ApmnbhUg7egDTL1C+I/t5qtw9+kOfPixsL8VEHbd9mepX0lFQolrA+7Jjmz5VEkJB+WHTiAflhVUvCa3LwG9zTpkcLAxD/oduWU3PrrHSTNQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C54776B5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  August 5 - August 18, 2009</h1> 

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19686819?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The phenylpropanoids of Aster flaccidus.</a>  Liu ZL, Liu YQ, Zhao L, Xu J, Tian X.  Fitoterapia. 2009 Aug 14. [Epub ahead of print]  PMID: 19686819</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19685905?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bioactive Nortriterpenoids from Schisandra grandiflora.</a>  Xiao WL, Gong YQ, Wang RR, Weng ZY, Luo X, Li XN, Yang GY, He F, Pu JX, Yang LM, Zheng YT, Lu Y, Sun HD.  J Nat Prod. 2009 Aug 17. [Epub ahead of print]  PMID: 19685905</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19678643?ordinalpos=14&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The Nucleoside Analogue D-carba T Blocks HIV-1 Reverse Transcription.</a>  Boyer PL, Vu BC, Ambrose Z, Julias JG, Warnecke S, Liao C, Meier C, Marquez VE, Hughes SH.  J Med Chem. 2009 Aug 13. [Epub ahead of print]  PMID: 19678643</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19658430?ordinalpos=43&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Catalytic Asymmetric Alkylation of Substituted Isoflavanones.</a>  Nibbs AE, Baize AL, Herter RM, Scheidt KA.  Org Lett. 2009 Aug 7. [Epub ahead of print]  PMID: 19658430</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19643613?ordinalpos=50&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and biological evaluation of imidazole thioacetanilides as novel non-nucleoside HIV-1 reverse transcriptase inhibitors.</a>  Zhan P, Liu X, Zhu J, Fang Z, Li Z, Pannecouque C, Clercq ED.  Bioorg Med Chem. 2009 Aug 15;17(16):5775-81. Epub 2009 Jul 18.  PMID: 19643613</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19625188?ordinalpos=53&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and synthesis of novel nitrogen-containing polyhydroxylated aromatics as HIV-1 integrase inhibitors from caffeic acid phenethyl ester.</a>  Wang P, Liu C, Sanches T, Zhong Y, Liu B, Xiong J, Neamati N, Zhao G.  Bioorg Med Chem Lett. 2009 Aug 15;19(16):4574-8. Epub 2009 Jul 3.  PMID: 19625188</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19620009?ordinalpos=55&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel 1,2,3-thiadiazole derivatives as HIV-1 NNRTIs with improved potency: Synthesis and preliminary SAR studies.</a>  Zhan P, Liu X, Li Z, Fang Z, Li Z, Wang D, Pannecouque C, Clercq ED.  Bioorg Med Chem. 2009 Aug 15;17(16):5920-7. Epub 2009 Jul 7.  PMID: 19620009</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19616956?ordinalpos=56&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis, and structure-activity relationships of 1,3-dihydrobenzimidazol-2-one analogues as anti-HIV agents.</a>  Monforte AM, Logoteta P, Ferro S, Luca LD, Iraci N, Maga G, Clercq ED, Pannecouque C, Chimirri A.  Bioorg Med Chem. 2009 Aug 15;17(16):5962-7. Epub 2009 Jul 3.  PMID: 19616956</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19656383?ordinalpos=47&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of HIV-1 entry by extracts derived from traditional Chinese medicinal herbal plants.</a>  Park IW, Han C, Song X, Green LA, Wang T, Liu Y, Cen C, Song X, Yang B, Chen G, He JJ.  BMC Complement Altern Med. 2009 Aug 5;9(1):29. [Epub ahead of print]  PMID: 19656383</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19643614?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of orthogonally reactive FK506 derivatives via olefin cross metathesis.</a>  Marinec PS, Evans CG, Gibbons GS, Tarnowski MA, Overbeek DL, Gestwicki JE.  Bioorg Med Chem. 2009 Aug 15;17(16):5763-8. Epub 2009 Jul 18.  PMID: 19643614</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268403100026">Crystal structures of HIV-1 nonnucleoside reverse transcriptase inhibitors: N-benzyl-4-methyl-benzimidazoles.</a>  Ziolkowska, NE; Michejda, CJ; Bujacz, GD.  JOURNAL OF MOLECULAR STRUCTURE, 2009; 930(1-3): 157-161. </p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268471200010">SYNTHESIS, EVALUATION AND MOLECULAR MODELING STUDIES OF SOME NOVEL TETRAHYDROISOQUINOLINE DERIVATIVES TARGETED AT THE HIV-1 REVERSE TRANSCRIPTASE.</a>  Ganguly, S; Murugesan, S; Maga, G.  INDIAN JOURNAL OF HETEROCYCLIC CHEMISTRY, 2009; 18(4): 357-360. </p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268255900043">Calycosin 7-O-beta-D-glucopyranoside, an anti-HIV agent from the roots of Astragalus membranaceus var. mongholicus.</a>  Ma, CH; Wang, RR; Tian, RR; Ye, G; Fan, MS; Zheng, YT; Huang, CG.  CHEMISTRY OF NATURAL COMPOUNDS, 2009; 45(2): 282-285. </p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268358800019">Dihydroxy-pyrimidine and N-methylpyrimidone HIV-integrase inhibitors: Improving cell based activity by the quaternarization of a chiral center.</a>  Nizi, E; Orsale, MV; Crescenzi, B; Pescatore, G; Muraglia, E; Alfieri, A; Gardelli, C; Spieser, SAH; Summa, V.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(16): 4617-4621.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268358800061">Structural and theoretical studies of [6-bromo-1-(4-fluorophenylmethyl)-4(1H)-quinolinon-3-yl)]-4-hydroxy-2-oxo-3-butenoic acid as HIV-1 integrase inhibitor.</a>  Vandurm, P; Cauvin, C; Guiguen, A; Georges, B; Le Van, K; Martinelli, V; Cardona, C; Mbemba, G; Mouscadet, JF; Hevesi, L; Van Lint, C; Wouters, J.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(16): 4806-4809. </p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268715000014">Synthesis and Biological Activity of New Glycyrrhizic Acid Conjugates with Amino Acids and Dipeptides.</a>  Baltina, LA; Kondratenko, RM; Baltina, LA; Baschenko, NZ; Pl&#39;yasunova, OA.  RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY, 2009; 35(4): 517-480EM. </p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268622500018">Synthesis and Anti-HIV-1 Evaluation of New Sonogashira-Modified Emivirine (MKC-442) Analogues.</a>  Danel, K; Jorgensen, PT; Pedersen, EB; La Colla, P; Collu, G; Loddo, R.  HELVETICA CHIMICA ACTA, 2009; 92(7): 1385-1403. </p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268547300002">Anti Human Immunodeficiency Virus-1 (HIV-1) Agents 3. Synthesis and in Vitro Anti-HIV-1 Activity of Some N-Arylsulfonylindoles.</a>  Fan, LL; Liu, WQ; Xu, H; Yang, LM; Lv, M; Zheng, YT.  CHEMICAL &amp; PHARMACEUTICAL BULLETIN, 2009; 57(8): 797-800. </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
