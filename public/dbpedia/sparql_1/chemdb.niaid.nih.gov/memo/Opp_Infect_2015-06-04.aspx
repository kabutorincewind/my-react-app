

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-06-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TsubXQFyMkAGYqnjgZZ/nkSYUo8N6d22drdI5izt/tJ4CHQDeYazwn02pIvSEQCH7ZesJzyIhgT4B4Au9unBfdyKB2aZmKAityY4J+LcZMSD5EsICfYemcfJd6w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="900472F8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 22 - June 4, 2015</h1>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25874329">Development of Selective DprE1 Inhibitors: Design, Synthesis, Crystal Structure and Antitubercular Activity of Benzothiazolylpyrimidine-5-Carboxamides.</a> Chikhale, R., S. Menghani, R. Babu, R. Bansode, G. Bhargavi, N. Karodia, M.V. Rajasekharan, A. Paradkar, and P. Khedekar. European Journal of Medicinal Chemistry, 2015. 96: p. 30-46. PMID[25874329].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25824227.">Meropenem-Clavulanic acid Has High in Vitro Activity against Multidrug-resistant Mycobacterium tuberculosis.</a> Davies Forsman, L., C.G. Giske, J. Bruchfeld, T. Schon, P. Jureen, and K. Angeby. Antimicrobial Agents and Chemotherapy, 2015. 59(6): p. 3630-3632. PMID[25824227].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25656411">Tetrahydroisoquinolines Affect the Whole-cell Phenotype of Mycobacterium tuberculosis by Inhibiting the ATP-dependent Mure Ligase.</a> Guzman, J.D., T. Pesnot, D.A. Barrera, H.M. Davies, E. McMahon, D. Evangelopoulos, P.N. Mortazavi, T. Munshi, A. Maitra, E.D. Lamming, R. Angell, M.C. Gershater, J.M. Redmond, D. Needham, J.M. Ward, L.E. Cuca, H.C. Hailes, and S. Bhakta. Antimicrobial Agents and Chemotherapy, 2015. 70(6): p. 1691-1703. PMID[25656411].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25795315">Antimycobacterial Activity of Linezolid against Multidrug-resistant and Extensively Drug-resistant Strains of Mycobacterium tuberculosis in Iran.</a> Kazemian, H., M. Haeili, J. Kardan Yamchi, F. Rezaei, S. Gizaw Feyisa, F. Zahednamazi, P. Mohajeri, S. Zaker Bostanabd, A. Hashemi Shahraki, A.A. Imani Fooladi, and M.M. Feizabadi. International Journal of Antimicrobial Agents, 2015. 45(6): p. 668-670. PMID[25795315].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25599413">Antibiotic Susceptibility Patterns of Mycobacterium tuberculosis Isolates from Guizhou Province of China against 13 Antituberculosis Drugs.</a> Li, N., X. Liao, L. Chen, J. Wang, M. Liu, and H. Zhang. Microbial Drug Resistance, 2015. 21(3): p. 292-296. PMID[25599413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25882678">2-Substituted 6-(het)aryl-7-deazapurine Ribonucleosides: Synthesis, Inhibition of Adenosine Kinases, and Antimycobacterial Activity.</a> Malnuit, V., L.P. Slavetinska, P. Naus, P. Dzubak, M. Hajduch, J. Stolarikova, J. Snasel, I. Pichova, and M. Hocek. ChemMedChem, 2015. 10(6): p. 1079-1093. PMID[25882678].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0522-060415.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000354180200009">Synthesis and Anti-tubercular Activity of 6-(4-Chloro/Methyl-phenyl)-4-arylidene-4,5-dihydropyridazin-3(2H)-one Derivatives against Mycobacterium tuberculosis.</a> Asif, M. and A. Singh. Letters in Drug Design &amp; Discovery, 2015. 12(6): p. 500-504. ISI[000354180200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0522-060415.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000354115400025">Synthesis and Anti-mycobacterial Activity of 2-Chloronicotinaldehydes Based Novel 1H-1,2,3-Triazolylbenzohydrazides.</a> Suman, P., C. Dayakar, K. Rajkumar, B. Yashwanth, P. Yogeeswari, D. Sriram, J.V. Rao, and B.C. Raju. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(11): p. 2390-2394. ISI[000354115400025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0522-060415.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
