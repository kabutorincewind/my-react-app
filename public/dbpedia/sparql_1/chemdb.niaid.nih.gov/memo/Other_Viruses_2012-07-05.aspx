

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-07-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="p9nQm9Vut0k5evYsVIZ96TDhDbq+rrxc/MwXalk7gw53HsSPrPdYxVqoO4X0DJx1xCflM2kek28YfyGGqXaEDDxWxEAqx27hu0JBLDC5Uy8YxySDAHGFFR8/VJY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="47E95909" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 22 - July 5, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22748708">C-6 Aryl Substituted 4-Quinolone-3-carboxylic acids as Inhibitors of Hepatitis C Virus.</a> Chen, Y.L., J. Zacharias, R. Vince, R.J. Geraghty, and Z. Wang, Bioorganic &amp; Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22748708].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22728816">Mycophenolate mofetil Inhibits Hepatitis C Virus Replication in Human Hepatic Cells.</a> Ye, L., J. Li, T. Zhang, X. Wang, Y. Wang, Y. Zhou, J. Liu, H.K. Parekh, and W. Ho, Virus Research, 2012. <b>[Epub ahead of print]</b>; PMID[22728816].</p>

    <p class="plaintext"><b>[PubMed]</b>.  OV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22720059">The HCV Non-Nucleoside Inhibitor Tegobuvir Utilizes a Novel Mechanism of Action to Inhibit NS5B Polymerase Function</a>. Hebner, C.M., B. Han, K.M. Brendza, M. Nash, M. Sulfab, Y. Tian, M. Hung, W. Fung, R.W. Vivian, J. Trenkle, J. Taylor, K. Bjornson, S. Bondy, X. Liu, J. Link, J. Neyts, R. Sakowicz, W. Zhong, H. Tang, and U. Schmitz, Plos One, 2012. 7(6): p. e39163; PMID[22720059].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0622-070512.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22758929">Synthesis and Biological Properties of C-2 Triazolylinosine Derivatives</a>. Lakshman, M.K., A. Kumar, R. Balachandran, B.W. Day, G. Andrei, R. Snoeck, and J. Balzarini, The Journal of Organic Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22758929].</p>

    <p class="plaintext">[PubMed]. OV_0622-070512.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22738985">Identification and Biological Activities of Bryostatins from Japanese Bryozoan</a>. Ueno, S., R.C. Yanagita, K. Murakami, A. Murakami, H. Tokuda, N. Suzuki, T. Fujiwara, and K. Irie, Bioscience, Biotechnology, and Biochemistry, 2012. 76(5): p. 1041-3; PMID[22738985].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0622-070512.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304484600056">Synthesis and Characterization of 2 &#39;-C-Me Branched C-Nucleosides as HCV Polymerase Inhibitors</a>. Cho, A., L.J. Zhang, J. Xu, D. Babusis, T. Butler, R. Lee, O.L. Saunders, T. Wang, J. Parrish, J. Perry, J.Y. Feng, A.S. Ray, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(12): p. 4127-4132; ISI[000304484600056].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304838600015">Polyphenols Content, Antioxidant and Antiviral Activities of Leaf Extracts of Marrubium deserti Growing in Tunisia</a>. Edziri, H., M. Mastouri, M. Aouni, and L. Verschaeve, South African Journal of Botany, 2012. 80: p. 104-109; ISI[000304838600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0622-070512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304484600039">Identification of Myricetin and Scutellarein as Novel Chemical Inhibitors of the SARS Coronavirus Helicase, nsP13</a>. Yu, M.S., J. Lee, J.M. Lee, Y. Kim, Y.W. Chin, J.G. Jee, Y.S. Keum, and Y.J. Jeong, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(12): p. 4049-4054; ISI[000304484600039].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0622-070512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
