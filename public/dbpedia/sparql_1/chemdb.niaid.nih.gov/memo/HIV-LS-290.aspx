

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-290.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="M2lo2ipZhibq2K7VEK+eKqVlSFBt/GlQ5NYDu2TPhemLzO5q0zFbu1zEYT1d/DfZmRNVkRp9WT91cRuvG+xmB1a/ueWLyo1kzi9VXr7ZKkXdhmnJ1buy7YX3Z38=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9BBECB64" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-290-MEMO</b> </p>

<p>    1.    42869   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           The Vif protein of human immunodeficiency virus type 1 is posttranslationally modified by ubiquitin*1, *2</p>

<p>           Dussart, Sylvie, Courcoul, Marianne, Bessou, Gilles, Douaisi, Marc, Duverger, Yohann, Vigne, Robert, and Decroly, Etienne</p>

<p>           Biochemical and Biophysical Research Communications 2004. 315(1): 66-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4BJW268-3/2/cbfd383697ccf6eb7f95236335c7f33e">http://www.sciencedirect.com/science/article/B6WBK-4BJW268-3/2/cbfd383697ccf6eb7f95236335c7f33e</a> </p><br />

<p>    2.    42870   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p><b>           Potent Nonclassical Nucleoside Antiviral Drugs Based on the N,N-Diarylformamidine Concept</b> </p>

<p>           Anastasi, C, Hantz, O, De Clercq, E, Pannecouque, C, Clayette, P, Dereuddre-Bosquet, N, Dormont, D, Gondois-Rey, F, Hirsch, I, and Kraus, JL</p>

<p>           J Med Chem 2004. 47(5): 1183-1192</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971898&amp;dopt=abstract</a> </p><br />

<p>    3.    42871   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Novel arylsulfonamides possessing sub-picomolar HIV protease activities and potent anti-HIV activity against wild-type and drug-resistant viral strains</p>

<p>           Miller, John F, Furfine, Eric S, Hanlon, Mary H, Hazen, Richard J, Ray, John A, Robinson, Laurence, Samano, Vicente, and Spaltenstein, Andrew</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(4): 959-963</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-7/2/b42a0f4892f981972c4f8c5eac036b73">http://www.sciencedirect.com/science/article/B6TF9-4BH63MS-7/2/b42a0f4892f981972c4f8c5eac036b73</a> </p><br />

<p>    4.    42872   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Novel Benzophenones as Non-nucleoside Reverse Transcriptase Inhibitors of HIV-1</p>

<p>           Chan, JH, Freeman, GA, Tidwell, JH, Romines, KR, Schaller, LT, Cowan, JR, Gonzales, SS, Lowell, GS, Andrews, III CW, Reynolds, DJ, St, Clair M, Hazen, RJ, Ferris, RG, Creech, KL, Roberts, GB, Short, SA, Weaver, K, Koszalka, GW, and Boone, LR</p>

<p>           J Med Chem 2004. 47(5): 1175-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971897&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971897&amp;dopt=abstract</a> </p><br />

<p>    5.    42873   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p><b>           Potent and selective inhibition of HIV and SIV by prostratin interacting with viral entry</b> </p>

<p>           Witvrouw, M, Pannecouque, C, Fikkert, V, Hantson, A, Van Remoortel, B, Hezareh, M, De Clercq, E, and Brown, SJ</p>

<p>           Antivir Chem Chemother 2003. 14(6): 321-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968938&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968938&amp;dopt=abstract</a> </p><br />

<p>    6.    42874   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Structure-based Drug Design Targeting an Inactive RNA Conformation: Exploiting the Flexibility of HIV-1 TAR RNA*1</p>

<p>           Murchie, Alastair IH, Davis, Ben, Isel, Catherine, Afshar, Mohammad, Drysdale, Martin J, Bower, Justin, Potter, Andrew J, Starkey, Ian D, Swarbrick, Terry M, and Mirza, Shabana</p>

<p>           Journal of Molecular Biology 2004.  336(3): 625-638</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WK7-4BBH5CC-1/2/428750903befa86793fafe47bb5b19f1">http://www.sciencedirect.com/science/article/B6WK7-4BBH5CC-1/2/428750903befa86793fafe47bb5b19f1</a> </p><br />

<p>    7.    42875   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Mode of Action for Linear Peptide Inhibitors of HIV-1 gp120 Interactions</p>

<p>           Biorn, AC, Cocklin, S, Madani, N, Si, Z, Ivanovic, T, Samanen, J, Van Ryk, DI, Pantophlet, R, Burton, DR, Freire, E, Sodroski, J, and Chaiken, IM</p>

<p>           Biochemistry 2004. 43(7): 1928-1938</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14967033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14967033&amp;dopt=abstract</a> </p><br />

<p>    8.    42876   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           A possible improvement for structure-based drug design illustrated by the discovery of a Tat HIV-1 inhibitor</p>

<p>           Montembault, Mickael, Vo-Thanh, Giang, Deyine, Abdallah, Fargeas, Valerie, Villieras, Monique, Adjou, Ane, Dubreuil, Didier, Esquieu, Didier, Gregoire, Catherine, and Opi, Sandrine</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BMC7GM-9/2/816108622c6a7f4707ac0883a7ebf279">http://www.sciencedirect.com/science/article/B6TF9-4BMC7GM-9/2/816108622c6a7f4707ac0883a7ebf279</a> </p><br />

<p>    9.    42877   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Quantitative real-time PCR on Lightcycler(R) for the detection of human immunodeficiency virus type 2 (HIV-2)</p>

<p>           Ruelle, Jean, Mukadi, Benoit Kabamba, Schutten, Martin, and Goubau, Patrick</p>

<p>           Journal of Virological Methods 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T96-4BM8KFH-3/2/ab3223f676c4021e7f6ad4c538c26438">http://www.sciencedirect.com/science/article/B6T96-4BM8KFH-3/2/ab3223f676c4021e7f6ad4c538c26438</a> </p><br />

<p>  10.    42878   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Analysis of protease inhibitor combinations in vitro: activity of lopinavir, amprenavir and tipranavir against HIV type 1 wild-type and drug-resistant isolates</p>

<p>           Bulgheroni, E, Citterio, P, Croce, F, Lo, Cicero M, Vigano, O, Soster, F, Chou, TC, Galli, M, and Rusconi, S</p>

<p>           J Antimicrob Chemother 2004. 53(3): 464-468</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963061&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963061&amp;dopt=abstract</a> </p><br />

<p>  11.    42879   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Potent antiviral activity of amprenavir in primary macrophages infected by human immunodeficiency virus</p>

<p>           Aquaro, Stefano, Guenci, Tania, Santo, Fabiola Di, Francesconi, Mauro, Calio, Raffaele, and Perno, Carlo Federico</p>

<p>           Antiviral Research 2004. 61(2): 133-137</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-49WPBVK-1/2/3c415a8acfbb4615543d15d5cdcf77f0">http://www.sciencedirect.com/science/article/B6T2H-49WPBVK-1/2/3c415a8acfbb4615543d15d5cdcf77f0</a> </p><br />

<p>  12.    42880   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Design and synthesis of a template-assembled oligomannose cluster as an epitope mimic for human HIV-neutralizing antibody 2G12</p>

<p>           Li, H and Wang, LX</p>

<p>           Org Biomol Chem 2004. 2(4): 483-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14770226&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14770226&amp;dopt=abstract</a>  </p><br />

<p>  13.    42881   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Tannin inhibits HIV-1 entry by targeting gp41</p>

<p>           Lu, L, Liu, SW, Jiang, SB, and Wu, SG</p>

<p>           Acta Pharmacol Sin 2004. 25(2): 213-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14769212&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14769212&amp;dopt=abstract</a> </p><br />

<p>  14.    42882   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Inhibition of HIV-1 Replication in Monocyte-Derived Macrophages by Mycobacterium tuberculosis</p>

<p>           Goletti, D, Carrara, S, Vincenti, D, Giacomini, E, Fattorini, L, Garbuglia, AR, Capobianchi, MR, Alonzi, T, Fimia, GM, Federico, M, Poli, G, and Coccia, E</p>

<p>           J Infect Dis 2004. 189(4): 624-33</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767815&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767815&amp;dopt=abstract</a> </p><br />

<p>  15.    42883   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           HIV-1 entry inhibitor entrances</p>

<p>           Kuhmann, Shawn E and Moore, John P</p>

<p>           Trends in Pharmacological Sciences 2004. 25(3): 117-120</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1K-4BM8YTY-2/2/fd1b82557c0c74c1eb1df42015ef37dd">http://www.sciencedirect.com/science/article/B6T1K-4BM8YTY-2/2/fd1b82557c0c74c1eb1df42015ef37dd</a> </p><br />

<p>  16.    42884   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           HIV-1 Vpr Induces Defects in Mitosis, Cytokinesis, Nuclear Structure, and Centrosomes</p>

<p>           Chang, F, Re, F, Sebastian, S, Sazer, S, and Luban, J</p>

<p>           Mol Biol Cell  2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767062&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767062&amp;dopt=abstract</a> </p><br />

<p>  17.    42885   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Efficacy of two modalities of triple HIV therapy: probable superiority of indinavir</p>

<p>           Eiros Bouza, JM, Ortega, M, de Lejarazu, ROrtiz, Blanco, R, Bachiller, P, and de Luis, DA</p>

<p>           International Journal of Antimicrobial Agents 2004. 23(3): 303-305</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4BN0J57-8/2/31734c25d03a4f294818c6839d34821e">http://www.sciencedirect.com/science/article/B6T7H-4BN0J57-8/2/31734c25d03a4f294818c6839d34821e</a> </p><br />

<p>  18.    42886   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Binding of hybrid molecules containing pyrrolo [2,1-c][1,4]benzodiazepine (PBD) and oligopyrrole carriers to the human immunodeficiency type 1 virus TAR-RNA</p>

<p>           Mischiati, Carlo, Finotti, Alessia, Sereni, Alessia, Boschetti, Sindi, Baraldi, Pier Giovanni, Romagnoli, Romeo, Feriotto, Giordana, Jeang, Kuan-Teh, Bianchi, Nicoletta, Borgatti, Monica, and Gambari, Roberto</p>

<p>           Biochemical Pharmacology 2004. 67(3): 401-410</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T4P-4BRCNCP-3/2/f9cf7272f21dd4b21f3558568e53d075">http://www.sciencedirect.com/science/article/B6T4P-4BRCNCP-3/2/f9cf7272f21dd4b21f3558568e53d075</a> </p><br />

<p>  19.    42887   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Actinohivin, a novel anti-human immunodeficiency virus protein from an actinomycete, inhibits viral entry to cells by binding high-mannose type sugar chains of gp120</p>

<p>           Chiba, Harumi, Inokoshi, Junji, Nakashima, Hideki, Omura, Satoshi, and Tanaka, Haruo</p>

<p>           Biochemical and Biophysical Research Communications 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4BS0D6C-B/2/4b869d3db670d50f79ee72fcee4347b5">http://www.sciencedirect.com/science/article/B6WBK-4BS0D6C-B/2/4b869d3db670d50f79ee72fcee4347b5</a> </p><br />

<p>  20.    42888   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Antagonists of human CCR5 receptor containing 4-(pyrazolyl)piperidine side chains. Part 1: Discovery and SAR study of 4-pyrazolylpiperidine side chains</p>

<p>           Shen, Dong-Ming, Shu, Min, Mills, Sander G, Chapman, Kevin T, Malkowitz, Lorraine, Springer, Martin S, Gould, Sandra L, DeMartino, Julie A, Siciliano, Salvatore J, and Kwei, Gloria Y</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(4): 935-939</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-2/2/8e11bd5ea7118aeb5b61cb8a1546d145">http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-2/2/8e11bd5ea7118aeb5b61cb8a1546d145</a> </p><br />

<p>  21.    42889   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Antagonists of human CCR5 receptor containing 4-(pyrazolyl)piperidine side chains. Part 2: Discovery of potent, selective, and orally bioavailable compounds</p>

<p>           Shen, Dong-Ming, Shu, Min, Willoughby, Christopher A, Shah, Shrenik, Lynch, Christopher L, Hale, Jeffrey J, Mills, Sander G, Chapman, Kevin T, Malkowitz, Lorraine, and Springer, Martin S</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(4): 941-945</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-3/2/c3814d4f4f8b25072fabc7b3f549fc42">http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-3/2/c3814d4f4f8b25072fabc7b3f549fc42</a> </p><br />

<p>  22.    42890   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Antagonists of human CCR5 receptor containing 4-(pyrazolyl)piperidine side chains. Part 3: SAR studies on the benzylpyrazole segment</p>

<p>           Shu, Min, Loebach, Jennifer L, Parker, Kerry A, Mills, Sander G, Chapman, Kevin T, Shen, Dong-Ming, Malkowitz, Lorraine, Springer, Martin S, Gould, Sandra L, and DeMartino, Julie A</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(4): 947-952</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-4/2/49ba8e9c3f15fbaa56a46b1cf53abd32">http://www.sciencedirect.com/science/article/B6TF9-4BF0CYW-4/2/49ba8e9c3f15fbaa56a46b1cf53abd32</a> </p><br />

<p>  23.    42891   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Comparative study of non nucleoside inhibitors with HIV-1 reverse transcriptase based on 3D-QSAR and docking</p>

<p>           Chen, HF, Yao, XJ, Li, Q, Yuan, SG, Panaye, A, Doucet, JP, and Fan, BT</p>

<p>           SAR QSAR Environ Res 2003. 14(5-6): 455-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14758988&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14758988&amp;dopt=abstract</a> </p><br />

<p>  24.    42892   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Novel nevirapine-like inhibitors with improved activity against NNRTI-resistant HIV: 8-heteroarylthiomethyldipyridodiazepinone derivatives</p>

<p>           Yoakim, C, Bonneau, PR, Deziel, R, Doyon, L, Duan, J, Guse, I, Landry, S, Malenfant, E, Naud, J, and Ogilvie, WW</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(3): 739-742</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4B7Y52H-7/2/dc6a254e7dbae1f2829f68da29fb547c">http://www.sciencedirect.com/science/article/B6TF9-4B7Y52H-7/2/dc6a254e7dbae1f2829f68da29fb547c</a> </p><br />

<p>  25.    42893   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Resistance to HIV-1 entry inhibitors</p>

<p>           Olson, WC and Maddon, PJ</p>

<p>           Curr Drug Targets Infect Disord 2003. 3(4): 283-94</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754430&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754430&amp;dopt=abstract</a>  </p><br />

<p>  26.    42894   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Phenyl phosphoramidate derivatives of stavudine as anti-HIV agents with potent and selective in-vitro antiviral activity against Adenovirus</p>

<p>           Uckun, Fatih M, Pendergrass, Sharon, Qazi, Sanjive, Samuel, P, and Venkatachalam, TK</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4BMTN9N-3/2/0e5189b149fe3ff633c503bb474c0f3a">http://www.sciencedirect.com/science/article/B6VKY-4BMTN9N-3/2/0e5189b149fe3ff633c503bb474c0f3a</a> </p><br />

<p>  27.    42895   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           The Vif protein of human immunodeficiency virus type 1 (HIV-1): enigmas and solutions</p>

<p>           Baraz, L and  Kotler, M</p>

<p>           Curr Med Chem 2004. 11(2): 221-231</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754418&amp;dopt=abstract</a> </p><br />

<p>  28.    42896   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           HIV-1 induces complement factor C3 synthesis in astrocytes and neurons by modulation of promoter activity</p>

<p>           Bruder, Cornelia, Hagleitner, Magdalena, Darlington, Gretchen, Mohsenipour, Iradj, Wurzner, Reinhard, Hollmuller, Isolde, Stoiber, Heribert, Lass-Florl, Cornelia, Dierich, Manfred P, and Speth, Cornelia</p>

<p>           Molecular Immunology 2004. 40(13):  949-961</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T9R-4B7HGX7-2/2/0127ef95606aae415c57c38ed0f79582">http://www.sciencedirect.com/science/article/B6T9R-4B7HGX7-2/2/0127ef95606aae415c57c38ed0f79582</a> </p><br />

<p>  29.    42897   HIV-LS-290; PUBMED-HIV-2/25/2004</p>

<p class="memofmt1-2">           Different resistance mutations can be detected simultaneously in the blood and the lung of HIV-1 infected individuals on antiretroviral therapy</p>

<p>           White, NC, Israel-Biet, D, Coker, RJ, Mitchell, DM, Weber, JN, and Clarke, JR</p>

<p>           J Med Virol 2004. 72(3): 352-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748057&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14748057&amp;dopt=abstract</a> </p><br />

<p>  30.    42898   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           A novel approach to develop anti-HIV drugs: adapting non-nucleoside anticancer chemotherapeutics</p>

<p>           Sadaie, MReza, Mayner, Ronald, and Doniger, Jay</p>

<p>           Antiviral Research 2004. 61(1): 1-18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-49Y4064-2/2/c1bb4665d0932990e6fe9361be47c36d">http://www.sciencedirect.com/science/article/B6T2H-49Y4064-2/2/c1bb4665d0932990e6fe9361be47c36d</a> </p><br />

<p>  31.    42899   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Anti-HIV-1 activity of pyrryl aryl sulfone (PAS) derivatives: synthesis and SAR studies of novel esters and amides at the position 2 of the pyrrole nucleus</p>

<p>           Silvestri, Romano, Artico, Marino, La Regina, Giuseppe, De Martino, Gabriella, La Colla, Massimiliano, Loddo, Roberta, and La Colla, Paolo</p>

<p>           Il Farmaco 2004. 59(3): 201-210</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJ8-4BP3TT5-3/2/42429d250ad0e320cd5e2456025b9ca5">http://www.sciencedirect.com/science/article/B6VJ8-4BP3TT5-3/2/42429d250ad0e320cd5e2456025b9ca5</a>  </p><br />

<p>  32.    42900   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Synthesis of - and -apio nucleoside analogues with 2&#39;-hydroxyl group as potential anti-HIV agents</p>

<p>           Jin, Dong Zhe, Kwon, Sung Hee, Moon, Hyung Ryong, Gunaga, Prashantha, Kim, Hea Ok, Kim, Dae-Kee, Chun, Moon Woo, and Jeong, Lak Shin</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(5): 1101-1109</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-10/2/ada87e9f276bc9879cacf2b4bc22977d">http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-10/2/ada87e9f276bc9879cacf2b4bc22977d</a> </p><br />

<p>  33.    42901   HIV-LS-290; EMBASE-HIV-2/25/2004</p>

<p class="memofmt1-2">           Synthesis of 6-arylvinyl analogues of the HIV drugs SJ-3366 and Emivirine</p>

<p>           Wamberg, Michael, Pedersen, Erik B, El-Brollosy, Nasser R, and Nielsen, Claus</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(5): 1141-1149</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-14/2/ec7640aec635bfd17806847ebb53a063">http://www.sciencedirect.com/science/article/B6TF8-4BRJ47V-14/2/ec7640aec635bfd17806847ebb53a063</a> </p><br />

<p>  34.    42902   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           CXCR4-tropic HIV-1 suppresses replication of CCR5-tropic HIV-1 in human lymphoid tissue by selective induction of CC-chemokines</p>

<p>           Ito, Y, Grivel, JC, Chen, S, Kiselyeva, Y, Reichelderfer, P, and Margolis, L</p>

<p>           J INFECT DIS 2004. 189(3): 506-514</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188467900019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188467900019</a> </p><br />

<p>  35.    42903   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           Structural investigation of 3,5-disubstituted isoxazoles by H-1-nuclear magnetic resonance</p>

<p>           Sechi, M, Sannia, L, Orecchioni, M, Carta, F, Paglietti, G, and Neamati, N</p>

<p>           J HETEROCYCLIC CHEM 2003. 40(6): 1097-1102</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188322600021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188322600021</a> </p><br />

<p>  36.    42904   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           Enumeration of latently infected CD4(+) T cells from HIV-1-infected patients using an HIV-1 antigen ELISPOT assay</p>

<p>           Fondere, JM, Planas, JF, Huguet, MF, Baillat, V, Bolos, F, Reynes, J, and Vendrell, JP</p>

<p>           J CLIN VIROL 2004. 29(1): 33-38</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188289000006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188289000006</a> </p><br />

<p>  37.    42905   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           A highly flexible route to 1,2,3,4,5,6-hexahydro-5-hydroxypyrimidin-2-one as potential HIV protease inhibitors</p>

<p>           Enders, D, Wortmann, L, Raabe, G, and Ducker, B</p>

<p>           HETEROCYCLES 2004. 62: 559-581</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188256600048">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188256600048</a> </p><br />

<p>  38.    42906   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           An improved microtiter assay for evaluating anti-HIV-I neutralizing antibodies from sera or plasma</p>

<p>           Zhang, CY, Chen, YY, and Ben, KL</p>

<p>           BMC INFECT DIS 2003. 3: art. no.-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188425300001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188425300001</a> </p><br />

<p>  39.    42907   HIV-LS-290; WOS-HIV-2/15/2004</p>

<p class="memofmt1-2">           Synthesis, anti-HIV-1 activity study of Keggin type heteropoly blues containing glycine</p>

<p>           Liu, SX, Wang, EB, Zhai, HJ, Han, ZB, Zeng, Y, and Li, ZL</p>

<p>           ACTA CHIM SINICA 2004. 62(2): 170-175</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188418600012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188418600012</a>  </p><br />

<p>  40.    42908   HIV-LS-290; WOS-HIV-2/22/2004</p>

<p class="memofmt1-2">           Chimeric human immunodeficiency virus type 1 (HIV-1) virions containing HIV-2 or Simian immunodeficiency virus Nef are resistant to cyclosporine treatment</p>

<p>           Khan, M, Jin, LL, Huang, MB, Miles, L, Bond, VC, and Powell, MD</p>

<p>           J VIROL 2004. 78(4): 1843-1850</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188662900023</a> </p><br />

<p>  41.    42909   HIV-LS-290; WOS-HIV-2/22/2004</p>

<p class="memofmt1-2">           3D-QSAR studies on chromone derivatives as HIV-1 protease inhibitors</p>

<p>           Ungwitayatorn, H, Samee, W, and Pimthon, J</p>

<p>           J MOL STRUCT 2004. 689(1-2): 99-106</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188589200012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188589200012</a> </p><br />

<p>  42.    42910   HIV-LS-290; WOS-HIV-2/22/2004</p>

<p class="memofmt1-2">           Differential anti-HIV activity of distinct hydroalcoholic Hypericum perforatum preparations.</p>

<p>           Willbrand, CT, Hodgson, A, Clanton, DJ, Ho, RJ, and Wenner, CA</p>

<p>           J INVEST MED 2004. 52(1): S140</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188254600371">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188254600371</a> </p><br />

<p>  43.    42911   HIV-LS-290; WOS-HIV-2/22/2004</p>

<p class="memofmt1-2">           Treatment of advanced human immunodeficiency virus type 1 disease with the viral entry inhibitor PRO 542</p>

<p>           Jacobson, JM, Israel, RJ, Lowy, I, Ostrow, NA, Vassilatos, LS, Barish, M, Tran, DNH, Sullivan, BM, Ketas, TJ, O&#39;Neill, TJ, Nagashima, KA, Huang, W, Petropoulos, CJ, Moore, JP, Maddon, PJ, and Olson, WC</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(2): 423-429</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200009</a> </p><br />

<p>  44.    42912   HIV-LS-290; WOS-HIV-2/22/2004</p>

<p><b>           Natural variation of drug susceptibility in wild-type human immunodeficiency virus type 1</b> </p>

<p>           Parkin, NT, Hellmann, NS, Whitcomb, JM, Kiss, L, Chappey, C, and Petropoulos, CJ</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(2): 437-443</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188592200011</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
