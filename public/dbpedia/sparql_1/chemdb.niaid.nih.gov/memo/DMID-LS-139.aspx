

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-139.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Pwwk12Y1TNVCj2LhY0+aQD9uiLCz4E3bEaoqXP0K5M92W/YRFy8ACwAhfk5VTGL65FawPqzSao/suWo3jxTIk6UvEbsQaqpH/pEE7kSxkTz1TXVQ0ULS5Kz9B90=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="42EA91AE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMDI-LS-139-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60255   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Pyrrolidine derivatives as immunomodulators and antiviral agents</p>

    <p>          Nash, Robert James, Carroll, Miles William, Watson, Alison Ann, Fleet, George William John, and Horne, Graeme</p>

    <p>          PATENT:  WO <b>2007010266</b>  ISSUE DATE:  20070125</p>

    <p>          APPLICATION: 2006  PP: 72pp.</p>

    <p>          ASSIGNEE:  (MNL Pharma Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60256   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Dysoxylins A-D, Tetranortriterpenoids with Potent Anti-RSV Activity from Dysoxylum gaudichaudianum</p>

    <p>          Chen, JL, Kernan, MR, Jolad, SD, Stoddart, CA, Bogan, M, and Cooper, R</p>

    <p>          J Nat Prod <b>2007</b>.  70(2): 312-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315968&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315968&amp;dopt=abstract</a> </p><br />

    <p>3.     60257   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Synthetic single strand deoxynucleosides for antiviral uses</p>

    <p>          Wang, Li-Ying, Bao, Mu-Sheng, and Yu, Yong-Li</p>

    <p>          PATENT:  WO <b>2007012285</b>  ISSUE DATE:  20070201</p>

    <p>          APPLICATION: 2006  PP: 62pp.</p>

    <p>          ASSIGNEE:  (Changchun Huapu Biotechnology Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60258   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Molecular diagnostics of hepatitis C virus infection: a systematic review</p>

    <p>          Scott, JD and Gretch, DR</p>

    <p>          JAMA <b>2007</b>.  297(7): 724-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17312292&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17312292&amp;dopt=abstract</a> </p><br />

    <p>5.     60259   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Proteases essential for human influenza virus entry into cells and their inhibitors as potential therapeutic agents</p>

    <p>          Kido, H, Okumura, Y, Yamada, H, Le, TQ, and Yano, M</p>

    <p>          Curr Pharm Des  <b>2007</b>.  13(3): 403-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17311557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17311557&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60260   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p><b>          Human Rhinovirus 3C Protease as a Potential Target for the Development of Antiviral Agents</b> </p>

    <p>          Wanga, QM and Chen, SH</p>

    <p>          Curr Protein Pept Sci <b>20067</b>.   8(1): 19-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17305557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17305557&amp;dopt=abstract</a> </p><br />

    <p>7.     60261   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Inhibitory effect of cinnamaldehyde, derived from Cinnamomi cortex, on the growth of influenza A/PR/8 virus in vitro and in vivo</p>

    <p>          Hayashi, K, Imanishi, N, Kashiwayama, Y, Kawano, A, Terasawa, K, Shimada, Y, and Ochiai, H</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303260&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303260&amp;dopt=abstract</a> </p><br />

    <p>8.     60262   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Efficient Synthesis of (S)-2-(Cyclopentyloxycarbonyl)-Amino-8-Nonenoic Acid: Key Building Block for Biln 2061, an Hcvns3 Protease Inhibitor</p>

    <p>          Wang, X. <i>et al.</i></p>

    <p>          Organic Process Research &amp; Development <b>2007</b>.  11(1): 60-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243560000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243560000009</a> </p><br />

    <p>9.     60263   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of macrocyclic inhibitors of hepatitis C virus</p>

    <p>          Raboisson, Pierre Jean-Marie Bernard, De Kock, Herman Augustinus, Hu, Lili, Simmen, Kenneth Alan, Lindquist, Karin Charlotta, Lindstroem, Mats Stefan, Belfrage, Anna Karin Gertrud Linnea, Waehling, Horst Juergen, Nilsson, Karl Magnus, Samuelsson, Bengt Bertil, Rosenquist, Aasa Annica Kristina, Sahlberg, Sven Crister, Wallberg, Hans Kristian, Kahnberg, Pia Cecilia, and Classon, Bjoern Olof</p>

    <p>          PATENT:  WO <b>2007014927</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 158pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire. and Medivir AB)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60264   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of N-prolyl-1-aminocyclopropanecarboxylic acid peptides as antiviral agents</p>

    <p>          Casarez, Anthony, Chaudhary, Kleem, Kim, Choung U, McMurtrie, Darren, and Sheng, Xiaoning C</p>

    <p>          PATENT:  WO <b>2007011658</b>  ISSUE DATE:  20070125</p>

    <p>          APPLICATION: 2006  PP: 190pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   60265   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Structure-activity relationship studies on anti-HCV activity of ring-expanded (&#39;fat&#39;) nucleobase analogues containing the imidazo[4,5-e][1,3]diazepine-4,8-dione ring system</p>

    <p>          Zhang, P, Zhang, N, Korba, BE, and Hosmane, RS</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17300935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17300935&amp;dopt=abstract</a> </p><br />

    <p>12.   60266   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Valopicitabine Dihydrochloride: a Specific Polymerase Inhibitor of Hepatitis C Virus</p>

    <p>          Toniutto, P. <i>et al.</i></p>

    <p>          Current Opinion in Investigational Drugs <b>2007</b>.  8(2): 150-158</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800007</a> </p><br />

    <p>13.   60267   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Inhibitors of serine proteases</p>

    <p>          Lyons, Steve and Perni, Robert B</p>

    <p>          PATENT:  WO <b>2007016589</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 86pp.</p>

    <p>          ASSIGNEE:  (Vertex Pharmaceuticals Incorporated, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60268   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Antiviral deoxyribozymes targeting hepatitis C core antigen RNA</p>

    <p>          Alfieri, Carolina, Trepanier, Janie, Tanner, Jerome E, and Momparler, Richard</p>

    <p>          PATENT:  WO <b>2007014469</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 100pp.</p>

    <p>          ASSIGNEE:  (Chu Sainte-Justine, Le Centre Hospitalier Universitaire Mere-Enfant Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60269   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Protocatechuic aldehyde inhibits hepatitis B virus replication both in vitro and in vivo</p>

    <p>          Zhou, Z, Zhang, Y, Ding, XR, Chen, SH, Yang, J, Wang, XJ, Jia, GL, Chen, HS, Bo, XC, and Wang, SQ</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298850&amp;dopt=abstract</a> </p><br />

    <p>16.   60270   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of macrocyclic compounds as HCV NS3 protease inhibitors</p>

    <p>          Holloway, MKatharine, Liverton, Nigel J, Ludmerer, Steven W, Mccauley, John A, Olsen, David B, Rudd, Michael T, Vacca, Joseph P, and Mcintyre, Charles J</p>

    <p>          PATENT:  US <b>2007027071</b>  ISSUE DATE:  20070201</p>

    <p>          APPLICATION: 2006-26173  PP: 142pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   60271   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Efficacy of oseltamivir therapy in ferrets inoculated with different clades of H5N1 influenza virus</p>

    <p>          Govorkova, EA, Ilyushina, NA, Boltz, DA, Douglas, A, Yilmaz, N, and Webster, RG</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296744&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296744&amp;dopt=abstract</a> </p><br />

    <p>18.   60272   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p><b>          Preparation of N-(piperazinylcarbonylazolyl)-N&#39;-phenylureas as anti-cytomegalovirus agents</b> </p>

    <p>          Zimmermann, Holger, Brueckner, David, Henninger, Kerstin, Hendrix, Martin, and Radtke, Martin</p>

    <p>          PATENT:  DE <b>102005033103</b>  ISSUE DATE: 20070125</p>

    <p>          APPLICATION: 2005  PP: 31pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60273   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Natural Products and Chronic Hepatitis C Virus</p>

    <p>          Azzam, H. <i>et al.</i></p>

    <p>          Liver International <b>2007</b>.  27(1): 17-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243534600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243534600004</a> </p><br />

    <p>20.   60274   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of L-nucleosides as antiviral agents</p>

    <p>          Yuan, Jiandong, Zhang, Kai, and Ye, Xinjian</p>

    <p>          PATENT:  CN <b>1891710</b>  ISSUE DATE: 20070110</p>

    <p>          APPLICATION: 1004  PP: 30pp.</p>

    <p>          ASSIGNEE:  (Brightgene Bio-Medical (Suzhou) Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60275   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          The novel C-5 aryl, alkenyl, and alkynyl substituted uracil derivatives of L-ascorbic acid: Synthesis, cytostatic, and antiviral activity evaluations</p>

    <p>          Gazivoda, Tatjana, Raic-Malic, Silvana, Marjanovic, Marko, Kralj, Marijeta, Pavelic, Kresimir, Balzarini, Jan, De Clercq, Erik, and Mintas, Mladen</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 749-758</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60276   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          The Kinetics of Hepatitis C Virus</p>

    <p>          Herrmann, E. and Zeuzem, S.</p>

    <p>          European Journal of Gastroenterology &amp; Hepatology <b>2006</b> .  18(4): 339-342</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243484200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243484200006</a> </p><br />

    <p>23.   60277   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Impact of Human Immune Deficiency Virus Infection on Hepatitis C Virus Infection and Replication</p>

    <p>          Parodi, C. <i>et al.</i></p>

    <p>          Current Hiv Research <b>2007</b>.  5(1): 55-67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243501800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243501800005</a> </p><br />
    <br clear="all">

    <p>24.   60278   DMID-LS-139; PUBMED-DMID-2/26/2007</p>

    <p class="memofmt1-2">          A dengue Fever viremia model in mice shows reduction in viral replication and suppression of the inflammatory response after treatment with antiviral drugs</p>

    <p>          Schul, W, Liu, W, Xu, HY, Flamand, M, and Vasudevan, SG</p>

    <p>          J Infect Dis <b>2007</b>.  195(5): 665-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17262707&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17262707&amp;dopt=abstract</a> </p><br />

    <p>25.   60279   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Recent patents on treatment of severe acute respiratory syndrome (SARS)</p>

    <p>          Zhai, Shumei, Liu, Wei, and Yan, Bing</p>

    <p>          Recent Patents on Anti-Infective Drug Discovery <b>2007</b>.  2(1 ): 1-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   60280   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          A mouse-adapted SARS-coronavirus causes disease and mortality in BALB/c mice</p>

    <p>          Roberts, Anjeanette, Deming, Damon, Paddock, Christopher D, Cheng, Aaron, Yount, Boyd, Vogel, Leatrice, Herman, Brian D, Sheahan, Tim, Heise, Mark, Genrich, Gillian L, Zaki, Sherif R, Baric, Ralph, and Subbarao, Kanta</p>

    <p>          PLoS Pathogens  <b>2007</b>.  3(1): 23-37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   60281   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of prolyl tripeptides as hepatitis C virus inhibitors</p>

    <p>          Hewawasam, Piyasena, Ding, Min, Sun, Li-Qiang, and Scola, Paul Michael</p>

    <p>          PATENT:  US <b>2007010455</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006-22784  PP: 86pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   60282   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Antiviral Activity of Cyclopropavir and Ether Lipid Esters of Cidofovir Against Hhv6 and Other Human Herpesviruses</p>

    <p>          Prichard, M.</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  37: S112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243845000077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243845000077</a> </p><br />

    <p>29.   60283   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Antiviral phosphoramidates</p>

    <p>          Klumpp, Klaus, Martin, Joseph Armstrong, Mcguigan, Christopher, and Smith, David Bernard</p>

    <p>          PATENT:  WO <b>2007020193</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche A.-G., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   60284   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p><b>          Application of multi-substituted acyl thiouracil derivant in preparation of antiviral drug</b> </p>

    <p>          Zhou, Pei, Feng, Meiqing, Sun, Chuanwen, Huang, Hai, and Zhou, Wei</p>

    <p>          PATENT:  CN <b>1903203</b>  ISSUE DATE: 20070131</p>

    <p>          APPLICATION: 1002-8153  PP: 11pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   60285   DMID-LS-139; SCIFINDER-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Pyrrolo[2,3-b]pyridine derivatives as protein kinase inhibitors and their preparation, pharmaceutical compositions and use in the treatment of diseases</p>

    <p>          Ibrahim, Prahbha N, Artis, Dean R, Bremer, Ryan, Mamo, Shumeye, Nespi, Marika, Zhang, Chao, Zhang, Jiazhong, Zhu, Yong-Liang, Tsai, James, Hirth, Klaus-Peter, Bollag, Gideon, Spevak, Wayne, Cho, Hanna, Gillette, Samuel J, Wu, Guoxiam, Zhu, Hongyao, and Shi, Shenghua</p>

    <p>          PATENT:  WO <b>2007002325</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 291 pp.</p>

    <p>          ASSIGNEE:  (Plexxikon, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   60286   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Preventing and Treating Influenza</p>

    <p>          Baum, S. and Carey, J.</p>

    <p>          Infections in Medicine <b>2007</b>.  24(1): 13-+</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243830700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243830700006</a> </p><br />

    <p>33.   60287   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p><b>          Human Rhinovirus 3c Protease as a Potential Target for the Development of Antiviral Agents</b> </p>

    <p>          Wang, Q. and Chen, S.</p>

    <p>          Current Protein &amp; Peptide Science <b>2007</b>.  8(1): 19-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243632700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243632700003</a> </p><br />

    <p>34.   60288   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Anti-Enterovirus Activity and Structure-Activity Relationship of a Series of 2,6-Dihalophenyl-Substituted 1h,3h-Thiazolo[3,4-a]Benzimidazoles</p>

    <p>          De Palma, A. <i>et al.</i></p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  353(3): 628-632</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243570000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243570000017</a> </p><br />

    <p>35.   60289   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Human Influenza a (H5n1): a Brief Review and Recommendations for Travelers</p>

    <p>          Hurtado, T.</p>

    <p>          Wilderness &amp; Environmental Medicine <b>2006</b>.  17(4): 276-281</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243309400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243309400009</a> </p><br />

    <p>36.   60290   DMID-LS-139; WOS-DMID-2/26/2007</p>

    <p class="memofmt1-2">          Long-Term Inhibition of Hepatitis B Virus in Transgenic Mice by Double-Stranded Adeno-Associated Virus 8-Delivered Short Hairpin Rna</p>

    <p>          Chen, C. <i>et al.</i></p>

    <p>          Gene Therapy <b>2007</b>.  14(1): 11-19</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243255300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243255300002</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
