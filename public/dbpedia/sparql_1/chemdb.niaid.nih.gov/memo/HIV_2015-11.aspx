

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZZjQe+FJsUQFkkCGFgSjFtEnsCc96dsCObqn/Kt7dm9DOStZ8WLUWeavEC0V/7pY7QvZ/yhZuhxYo0IT/sMtlYi1Vf/QGOS44J25TF7Xf7rL3QGXBS5ECVWGOfU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7F5B80D2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: November, 2015</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26443549">Antiviral Activity of Benzotriazole Derivatives. 5-[4-(Benzotriazol-2-yl)phenoxy]-2,2-dimethylpentanoic acids Potently and Selectively Inhibit Coxsackie Virus B5.</a> Loddo, R., F. Novelli, A. Sparatore, B. Tasso, M. Tonelli, V. Boido, F. Sparatore, G. Collu, I. Delogu, G. Giliberti, and P. La Colla. Bioorganic &amp; Medicinal Chemistry, 2015. 23(21): p. 7024-7034. PMID[26443549].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25613403">Antiviral Properties from Plants of the Mediterranean Flora.</a> Sanna, G., P. Farci, B. Busonera, G. Murgia, P. La Colla, and G. Giliberti. Natural Product Research, 2015. 29(22): p. 2065-2070. PMID[25613403].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26177889">Core-structure-inspired Asymmetric Addition Reactions: Enantioselective Synthesis of Dihydrobenzoxazinone- and Dihydroquinazolinone-based anti-HIV Agents.</a> Li, S. and J.A. Ma. Chemical Society Reviews, 2015. 44(21): p. 7439-7448. PMID[26177889].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26404185">Curcumin and Its Analogues: A Potential Natural Compound against HIV Infection and AIDS.</a> Prasad, S. and A.K. Tyagi. Food &amp; Function, 2015. 6(11): p. 3412-3419. PMID[26404185].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26166629">Discovery and Crystallography of Bicyclic Arylaminoazines as Potent Inhibitors of HIV-1 Reverse Transcriptase.</a> Lee, W.G., K.M. Frey, R. Gallardo-Macias, K.A. Spasov, A.H. Chan, K.S. Anderson, and W.L. Jorgensen. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(21): p. 4824-4827. PMID[26166629]. PMCID[PMC4607639].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25935642">Discovery of Novel N-aryl Piperazine CXCR4 Antagonists.</a> Zhao, H., A.R. Prosser, D.C. Liotta, and L.J. Wilson. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(21): p. 4950-4955. PMID[25935642].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26545586">Identification and Characterization of a New Type of Inhibitor against the Human Immunodeficiency Virus Type-1 Nucleocapsid Protein.</a> Kim, M.J., S.H. Kim, J.A. Park, K.L. Yu, S.I. Jang, B.S. Kim, E.S. Lee, and J.C. You. Retrovirology, 2015. 12: p. 90. PMID[26545586]. PMCID[PMC4636002].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26338723">Metal Complexes of Pyridine-fused Macrocyclic Polyamines Targeting the Chemokine Receptor CXCR4.</a> Hamal, S., T. D&#39;Huys, W.F. Rowley, K. Vermeire, S. Aquaro, B.J. Frost, D. Schols, and T.W. Bell. Organic &amp; Biomolecular Chemistry, 2015. 13(42): p. 10517-10526. PMID[26338723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26488492">A Novel Electrochemiluminescence Immunosensor for the Analysis of HIV-1 p24 Antigen Based on P-Rgo@Au@Ru-Sio2 Composite.</a> Zhou, L., J. Huang, B. Yu, Y. Liu, and T. You. ACS Applied Materials &amp; Interfaces, 2015. 7(44): p. 24438-24445. PMID[26488492].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26562066">Stelleralides D-J and anti-HIV Daphnane Diterpenes from Stellera chamaejasme.</a> Yan, M., Y. Lu, C.H. Chen, Y. Zhao, K.H. Lee, and D.F. Chen. Journal of Natural Products, 2015. 78(11): p. 2712-2718. PMID[26562066].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26096678">Structure-based Design, Synthesis, X-ray Studies, and Biological Evaluation of Novel HIV-1 Protease Inhibitors Containing Isophthalamide-derived P2-ligands.</a> Ghosh, A.K., J. Takayama, L.A. Kassekert, J.R. Ella-Menye, S. Yashchuk, J. Agniswamy, Y.F. Wang, M. Aoki, M. Amano, I.T. Weber, and H. Mitsuya. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(21): p. 4903-4909. PMID[26096678]. PMCID[PMC4607586].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26517310">Structure-enhanced Methods in the Development of Non-nucleoside Inhibitors Targeting HIV Reverse Transcriptase Variants.</a> Frey, K.M. Future Microbiology, 2015. 10: p. 1767-1772. PMID[26517310].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26451771">Synthesis of Dihydropyrimidine-alpha,gamma-diketobutanoic acid Derivatives Targeting HIV Integrase.</a> Sari, O., V. Roy, M. Metifiot, C. Marchand, Y. Pommier, S. Bourg, P. Bonnet, R.F. Schinazi, and L.A. Agrofoglio. European Journal of Medicinal Chemistry, 2015. 104: p. 127-138. PMID[26451771]. PMCID[PMC4629473].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26456550">Synthesis of Piscidinol a Derivatives and Their Ability to Inhibit HIV-1 Protease.</a> Wei, Y., C.M. Ma, T.B. Jiang, J. Du, X. Zhou, G.Q. Liu, and M. Hattori. Journal of Asian Natural Products Research, 2015. 17(11): p. 1079-1090. PMID[26456550].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26031778">Synthesis, Biological Evaluation and Molecular Modeling Studies of New 2,3-Diheteroaryl thiazolidin-4-ones as NNRTIs.</a> Debnath, U., S. Verma, P. Singh, K. Rawat, S.K. Gupta, R.K. Tripathi, H.H. Siddiqui, S.B. Katti, and Y.S. Prabhakar. Chemical Biology &amp; Drug Design, 2015. 86(5): p. 1285-1291. PMID[26031778].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_11_2015</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26268341">6,7-Dihydroxyisoindolin-1-one and 7,8-dihydroxy-3,4-dihydroisoquinolin-1(2H)-one Based HIV-1 Integrase Inhibitors.</a> Zhao, X.Z., M. Metifiot, S.J. Smith, K. Maddali, C. Marchand, S.H. Hughes, Y. Pommier, and T.R. Burke. Current Topics in Medicinal Chemistry, 2015. 16(4): p. 435-440. PMID[26268341].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <p class="plaintext"><br />
    17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26423643">BMS-663068, a Safe and Effective HIV-1 Attachment Inhibitor.</a> Ballana, E. and J.A. Este. Lancet HIV, 2015. 2(10): p. E404-E405. PMID[26423643].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26390175">Discovery and Optimization of Novel, Selective Histone Methyltransferase Set7 Inhibitors by Pharmacophore- and Docking-based Virtual Screening.</a> Meng, F.W., S.F. Cheng, H. Ding, S. Liu, Y. Liu, K.K. Zhu, S.J. Chen, J.Y. Lu, Y.Q. Xie, L.J. Li, R.F. Liu, Z. Shi, Y. Zhou, Y.C. Liu, M.Y. Zheng, H.L. Jiang, W.C. Lu, H. Liu, and C. Luo. Journal of Medicinal Chemistry, 2015. 58(20): p. 8166-8181. PMID[26390175].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26192705">DNA Triplex-based Complexes Display anti-HIV-1-Cell Fusion Activity.</a> Xu, L., T. Zhang, X.Y. Xu, H.H. Chong, W.Q. Lai, X.F. Jiang, C. Wang, Y.X. He, and K.L. Liu. Nucleic Acid Therapeutics, 2015. 25(4): p. 219-225. PMID[26192705]. PMCID[PMC4507350].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26447651">Fluorescent DNA-protected Silver Nanoclusters for Ligand-HIV RNA Interaction Assay.</a> Qi, L., Y. Huo, H. Wang, J. Zhang, F.Q. Dang, and Z.Q. Zhang. Analytical Chemistry, 2015. 87(21): p. 11078-11083. PMID[26447651].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26431196">Ligand-based Virtual Screening in a Search for Novel anti-HIV-1 Chemotypes.</a> Kurczyk, A., D. Warszycki, R. Musiol, R. Kafel, A.J. Bojarski, and J. Polanski. Journal of Chemical Information and Modeling, 2015. 55(10): p. 2168-2177. PMID[26431196].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26467263">Synthesis and Biological Evaluation of 9-Deazaadenine 5&#39;-deoxy-6&#39;,6&#39;-difluoro-carbocyclic C-nucleoside Phosphonic acid Derivatives.</a> Kim, S. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2015. 34(10): p. 708-728. PMID[26467263].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26383895">Versatile Synthesis of Oxime-containing Acyclic Nucleoside Phosphonates - Synthetic Solutions and Antiviral Activity.</a> Solyev, P.N., M.V. Jasko, A.A. Kleymenova, M.K. Kukhanova, and S.N. Kochetkov. Organic &amp; Biomolecular Chemistry, 2015. 13(44): p. 10946-10956. PMID[26383895].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_11_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151029&amp;CC=WO&amp;NR=2015162192A1&amp;KC=A1">Use of a HIV Derived Accessory Protein Tat for the Reactivation of Latent HIV Infection.</a> Boden, D. Patent. 2015. 2015-EP58747 2015162192: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151105&amp;CC=WO&amp;NR=2015164956A1&amp;KC=A1">Preparation of Benzisothiazole Derivative Compounds as Therapeutics.</a> Grierson, D.S., P.K. Cheung, B. Chabot, P.R. Harrigan, and A.W. Cochrane. Patent. 2015. 2015-CA291 2015164956: 109pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151119&amp;CC=WO&amp;NR=2015174511A1&amp;KC=A1">Preparation of Condensed Tricyclic Heterocyclic Derivatives Having HIV Replication-inhibiting Effect.</a> Kawasuji, T., D. Taniyama, S. Sugiyama, and Y. Tamura. Patent. 2015. 2015-JP63972 2015174511: 235pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151015&amp;CC=WO&amp;NR=2015157483A1&amp;KC=A1">Preparation of Triterpenoids with HIV Maturation Inhibitory Activity, Substituted in Position 3 by a Non-aromatic Ring Carrying a Haloalkyl Substituent.</a> Sit, S.-Y., Y. Chen, J. Chen, J. Swidorski, B.L. Venables, N. Sin, N.A. Meanwell, A. Regueiro-Ren, R.A. Hartz, L. Xu, and Z. Liu. Patent. 2015. 2015-US25029 2015157483: 348pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2015.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151112&amp;CC=WO&amp;NR=2015171995A1&amp;KC=A1">Small Molecule Inhibitors of HIV-1 Entry and Methods of Use Thereof.</a> Sodroski, J., A. Herschhorn, and C. Gu. Patent. 2015. 2015-US29846 2015171995: 256pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_11_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
