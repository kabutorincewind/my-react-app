

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-11-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sNk2RhR3k4grKByb1R3lxR4MEnBG3IWXkLmIzP39JGbtoNdSEywUT67+7Oh/UUIMkz7gcVJh8MO21TuGvojFWXV17cXlDWTF/sxm2jYIspbY/P6cWxFq2DoNGPo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FE0F11C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: October 26 - November 8, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23117889">Isolation, Characterization and Antifungal Activity of Proteinase Inhibitors from Capsicum chinense Jacq. Seeds.</a> Dias, G.B., V.M. Gomes, U.Z. Pereira, S.F. Ribeiro, A.O. Carvalho, R. Rodrigues, O.L. Machado, K.V. Fernandes, A.T. Ferreira, J. Perales, and M. Da Cunha. The Protein Journal, 2012. <b>[Epub ahead of print]</b>. PMID[23117889].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23107822">Biological Activities of Pseudevernia furfuracea (L.) Zopf Extracts and Isolation of the Active Compounds.</a> Guvenc, A., E.K. Akkol, I. Suntar, H. Keles, S. Yildiz, and I. Calis. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23107822].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23130342">Antifungal Susceptibility to Amphotericin B, Fluconazole, Voriconazole, and Flucytosine in Candida Bloodstream Isolates from 15 Tertiary Hospitals in Korea.</a> Jung, S.I., J.H. Shin, H.J. Choi, M.Y. Ju, S.H. Kim, W.G. Lee, Y.J. Park, and K. Lee. Annals of Laboratory Medicine, 2012. 32(6): p. 426-428. PMID[23130342].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/223118472">1-Alkyl-(N,N-dimethylamino)pyridinium bromides: Inhibitory Effect on Virulence Factors of Candida albicans and on the Growth of Bacterial Pathogens.</a> Muthuraman, S., R. Radhakrishnan, V. Perumal, and I. Andivelu. Journal of Medical Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[23118472].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2312979">Efficacy and Pharmacodynamics of Voriconazole Combined with Anidulafungin in Azole-resistant Invasive Aspergillosis.</a> Seyedmousavi, S., R.J. Bruggemann, W.J. Melchers, A.J. Rijs, P.E. Verweij, and J.W. Mouton. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23129729].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23107821">Toxicity, Antimicrobial and Anthelmintic Activities of Vernonia guineensis Benth. (Asteraceae) Crude Extracts.</a> Toyang, N.J., E.N. Ateh, J. Keiser, M. Vargas, H. Bach, P. Tane, L.B. Sondengam, H. Davis, J. Bryant, and R. Verpoorte. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23107821].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23102555">Antifungal Effect of Ophthalmic Preservatives Phenylmercuric nitrate and Benzalkonium chloride on Ocular Pathogenic Filamentous Fungi.</a> Xu, Y., Y. He, X. Li, C. Gao, L. Zhou, S. Sun, and G. Pang. Diagnostic Microbiology and Infectious Disease, 2012. <b>[Epub ahead of print]</b>. PMID[23102555]. <b>[PubMed]</b>. OI_1026-110812.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129730">Improving Existing Tools for Mycobacterium xenopi Treatment: Assessment of Drug Combinations and Characterization of Mouse Models of Infection and Chemotherapy.</a> Andrejak, C., D.V. Almeida, S. Tyagi, P.J. Converse, N.C. Ammerman, and J.H. Grosset. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23129730].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2278281">Macrolide Treatment for Mycobacterium abscessus and Mycobacterium massiliense Infection and Inducible Resistance.</a> Choi, G.E., S.J. Shin, C.J. Won, K.N. Min, T. Oh, M.Y. Hahn, K. Lee, S.H. Lee, C.L. Daley, S. Kim, B.H. Jeong, K. Jeon, and W.J. Koh. American Journal of Respiratory and Critical Care Medicine, 2012. 186(9): p. 917-925. PMID[22878281].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22975264">Identification and Validation of a Novel Lead Compound Targeting 4-Diphosphocytidyl-2-C-methylerythritol Synthetase (Ispd) of Mycobacteria.</a> Gao, P., Y. Yang, C. Xiao, Y. Liu, M. Gan, Y. Guan, X. Hao, J. Meng, S. Zhou, X. Chen, and J. Cui. European Journal of Pharmacology, 2012. 694(1-3): p. 45-52. PMID[22975264].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23093034">Antimicrobial Peptides from Arachnid venoms and Their Microbicidal Activity in the Presence of Commercial Antibiotics.</a> Garcia, F., E. Villegas, G.P. Espino-Solis, A. Rodriguez, J.F. Paniagua-Solis, G. Sandoval-Lopez, L.D. Possani, and G. Corzo. The Journal of Antibiotics, 2012. <b>[Epub ahead of print]</b>. PMID[23093034].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23114617">Antimycobacterial Assessment of Salicylanilide benzoates Including Multidrug-resistant Tuberculosis Strains.</a> Kratky, M., J. Vinsova, and J. Stolarikova. Molecules, 2012. 17(11): p. 12812-12820. PMID[23114617].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23108268">Synthesis and Evaluation of M. Tuberculosis Salicylate Synthase (Mbti) Inhibitors Designed to Probe Plasticity in the Active Site.</a> Manos-Turvey, A., K.M. Cergol, N.K. Salam, E.M. Bulloch, G. Chi, A. Pang, W.J. Britton, N.P. West, E.N. Baker, J.S. Lott, and R.J. Payne. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23108268].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23106287">Increment of Antimycobaterial Activity on Lichexanthone Derivatives.</a> Micheletti, A.C., N.K. Honda, F.R. Pavan, C.Q. Leite, M.D. Matos, R.T. Perdomo, D. Bogo, G.B. Alcantara, and A. Beatriz. Medicinal Chemistry (Shariqah (United Arab Emirates)), 2012. <b>[Epub ahead of print]</b>. PMID[23106287].</p>
    <p class="plaintext"> <b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23000294">1,3-Azoles from Ortho-naphthoquinones: Synthesis of Aryl Substituted Imidazoles and Oxazoles and Their Potent Activity against Mycobacterium tuberculosis.</a> Moura, K.C., P.F. Carneiro, C. Pinto Mdo, J.A. da Silva, V.R. Malta, C.A. de Simone, G.G. Dias, G.A. Jardim, J. Cantos, T.S. Coelho, P.E. da Silva, and E.N. da Silva, Jr. Bioorganic &amp; Medicinal Chemistry, 2012. 20(21): p. 6482-6488. PMID[23000294].</p>
    <p class="plaintext"> <b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23108921">In Vitro and in Vivo Antiplasmodial Activity of the Root Extracts of Brucea mollis Wall. ex Kurz.</a> Prakash, A., S.K. Sharma, P.K. Mohapatra, K. Bhattacharjee, K. Gogoi, P. Gogoi, J. Mahanta, and D.R. Bhattacharyya. Parasitology Research, 2012. <b>[Epub ahead of print]</b>. PMID[23108921].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22982011">Evaluation of the Antimycobacterium Activity of the Constituents from Ocimum basilicum against Mycobacterium tuberculosis.</a> Siddiqui, B.S., H.A. Bhatti, S. Begum, and S. Perwaiz. Journal of Ethnopharmacology, 2012. 144(1): p. 220-222. PMID[22982011].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22889240">6-Hydrogen-8-methylquinolones Active against Replicating and Non-Replicating Mycobacterium tuberculosis.</a> Tabarrini, O., S. Sabatini, S. Massari, M. Pieroni, S.G. Franzblau, and V. Cecchetti. Chemical Biology &amp; Drug Design, 2012. 80(5): p. 781-786. PMID[22889240].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23127648">New Antiplasmodial Alkaloids from Stephania rotunda.</a> Baghdikian, B., V. Mahiou-Leddet, S. Bory, S.S. Bun, A. Dumetre, F. Mabrouki, S. Hutter, N. Azas, and E. Ollivier. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[23127648].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23116403">1,4-Naphthoquinones and Others NADPH-Dependent Glutathione Reductase-Catalyzed Redox Cyclers as Antimalarial Agents.</a> Belorgey, D., D.A. Lanfranchi, and E. Davioud-Charvet. Current Pharmaceutical Design, 2012. <b>[Epub ahead of print]</b>. PMID[23116403].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23102258">Incorporation of a 3-(2,2,2-Trifluoroethyl)-gamma-hydroxy-gamma-lactam Motif in the Side Chain of 4-Aminoquinolines. Syntheses and Antimalarial Activities.</a> Cornut, D., H. Lemoine, O. Kanishchev, E. Okada, F. Albrieux, A.H. Beavogui, A.L. Bienvenu, S. Picot, J.P. Bouillon, and M. Medebielle. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23102258].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23129047">4(1H)-Quinolones with Liver Stage Activity against Plasmodium berghei.</a> Lacrue, A.N., F. Saenz, R.M. Cross, K.O. Udenze, A. Monastyrskyi, S. Stein, T.S. Mutka, R. Manetsch, and D.E. Kyle. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23129047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23040894">Pyrido[1,2-a]pyrimidin-4-ones as Antiplasmodial Falcipain-2 Inhibitors.</a> Mane, U.R., H. Li, J. Huang, R.C. Gupta, S.S. Nadkarni, R. Giridhar, P.P. Naik, and M.R. Yadav. Bioorganic &amp; Medicinal Chemistry, 2012. 20(21): p. 6296-6304. PMID[23040894].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">24.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/23110858">Toxoplasma gondii: Effects of Exogenous Nitric Oxide on Egress of Tachyzoites from Infected Macrophages.</a> Ji, Y.S., X.M. Sun, X.Y. Liu, and X. Suo. Experimental Parasitology, 2012. [Epub ahead of print]. PMID[23110858].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1026-110812.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308621800010">Facile Synthesis of Novel Fluorine Containing Pyrazole Based Thiazole Derivatives and Evaluation of Antimicrobial Activity.</a> Desai, N.C., V.V. Joshi, K.M. Rajpara, H.V. Vaghani, and H.M. Satodiya. Journal of Fluorine Chemistry, 2012. 142: p. 67-78. ISI[000308621800010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030924500012">17-Allylamino-17-(demethoxy)geldanamycin (17-AAG) Is a Potent and Effective Inhibitor of Human Cytomegalovirus Replication in Primary Fibroblast Cells.</a> Evers, D.L., C.F. Chao, Z.G. Zhang, and E.S. Huang. Archives of Virology, 2012. 157(10): p. 1971-1974. ISI[000309234500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307405500028">Antimicrobial Activity of a Biosurfactant Produced by Bacillus licheniformis Strain M104 Grown on Whey.</a> Gomaa, E.Z. African Journal of Microbiology Research, 2012. 6(20): p. 4396-4403. ISI[000307405500028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308399500018">Application of Fragment Screening and Merging to the Discovery of Inhibitors of the Mycobacterium tuberculosis Cytochrome P450 Cyp121.</a> Hudson, S.A., K.J. McLean, S. Surade, Y.Q. Yang, D. Leys, A. Ciulli, A.W. Munro, and C. Abell. Angewandte Chemie-International Edition, 2012. 51(37): p. 9311-9316. ISI[000308399500018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003083830900004">Microwave Synthesis and Spectral, Thermal and Antimicrobial Activities of Some Novel Transition Metal Complexes with Tridentate Schiff Base Ligands.</a> Jain, R.K. and A.P. Mishra. Journal of the Serbian Chemical Society, 2012. 77(8): p. 1013-1029. ISI[000308830900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307773700012">Leishmanicidal and Anticandidal Activity of Constituents of Indian Edible Mushroom Astraeus hygrometricus.</a> Lai, T.K., G. Biswas, S. Chatterjee, A. Dutta, C. Pal, J. Banerji, N. Bhuvanesh, J.H. Reibenspies, and K. Acharya. Chemistry &amp; Biodiversity, 2012. 9(8): p. 1517-1524. ISI[000307773700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309272700012">Synthesis, Structure Optimization and Antifungal Screening of Novel Tetrazole Ring Bearing Acyl-Hydrazones.</a> Malik, M.A. and S.A. Al-Thabaiti. International Journal of Molecular Sciences, 2012. 13(9): p. 10880-10898. ISI[000309272700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p>    
    <p> </p>
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030894800014">Anti-candidal Activity of Astragalus verus in the in Vitro and in Vivo Guinea Pig Models of Cutaneous and Systemic Candidiasis.</a> Mikaeili, A., I. Karimi, T. Shamspur, B. Gholamine, M. Modaresi, and A. Khanlari. Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2012. 22(5): p. 1035-1043. ISI[000308948000014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030891600016">Indole, a Bacterial Signaling Molecule, Exhibits Inhibitory Activity against Growth, Dimorphism and Biofilm Formation in Candida albicans.</a> Raut, J.S., R.B. Shinde, and M.S. Karuppayil. African Journal of Microbiology Research, 2012. 6(30): p. 6005-6012. ISI[000308916000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
    <p> </p>
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308820300011">Synthesis of Eperezolid-Like Molecules and Evaluation of Their Antimicrobial Activities.</a> Yolal, M., S. Basoglu, H. Bektas, S. Demirci, S. Alpay-Karaoglu, and A. Demirbas. Russian Journal of Bioorganic Chemistry, 2012. 38(5): p. 539-549. ISI[000308820300011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1026-110812.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
