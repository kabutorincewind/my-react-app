

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-06-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7XXwkGwTEA+uvQgpzI2vJr7bfyG0x8pS9I5w+jvEYxfGuXQRj2GW306C23O/l2+J6fDhiZe3TroLyMS2dvrFrMROSHeeYqsjPcwG76fWA7AUgGQkFD+5hzBCaGg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="84BC2C82" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">June 11 - June 24, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p />

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20564282">A Modular Approach to Synthetic RNA Binders of the Hepatitis C Virus Internal Ribosome Entry Site.</a></p>

    <p class="NoSpacing">Carnevali M, Parsons J, Wyles DL, Hermann T. Chembiochem. 2010 Jun 16. <b>[Epub ahead of print]</b>; PMID: 20564282 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20541424">Improved P2 phenylglycine-based hepatitis C virus NS3 protease inhibitors with alkenylic prime-side substituents.</a> Lampa A, Ehrenberg AE, Gustafsson SS, Vema A, Kerblom E, Lindeberg G, Karlén A, Danielson UH, Sandström A. Bioorg Med Chem. 2010 May 20. <b>[Epub ahead of print]</b>; PMID: 20541424 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20541405">Synthesis and SAR of potent inhibitors of the Hepatitis C virus NS3/4A protease: Exploration of P2 quinazoline substituents.</a> Nilsson M, Belfrage AK, Lindström S, Wähling H, Lindquist C, Ayesa S, Kahnberg P, Pelcman M, Benkestock K, Agback T, Vrang L, Terelius Y, Wikström K, Hamelink E, Rydergård C, Edlund M, Eneroth A, Raboisson P, Lin TI, de Kock H, Wigerinck P, Simmen K, Samuelsson B, Rosenquist S. Bioorg Med Chem Lett. 2010 May 21. <b>[Epub ahead of print]</b>; PMID: 20541405 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20536420">Advances in the Development of Macrocyclic Inhibitors of Hepatitis C Virus NS3-4A Protease.</a></p>

    <p class="NoSpacing">Avolio S, Summa V. Curr Top Med Chem. 2010 Jun 11. <b>[Epub ahead of print]</b>; PMID: 20536420 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275161600144">3D-QSAR KNN MFA Study on Thiouracil Derivatives as Hepatitis C Virus Inhibitors.</a> Patil, VM, et al. Medicinal Chemistry Research. 19:2010. P. s98.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278207900017">Antiviral Dugs New Oral HCV Drug Shows Promise.</a> Tse, Man Tsuey. Nature Reviews Drug Discovery. 9(6):2010. P. 432.</p>

    <p />

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278208200021">Synthesis and Evaluation of Novel Alpha-Amino Cyclic Boronates as Inhibitors of HCV NS3 Protease.</a> Li, Xianfeng, et al. Bioorganic &amp; Medicinal Chemistry Letters. 20(12):2010. P. 3550-6.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
