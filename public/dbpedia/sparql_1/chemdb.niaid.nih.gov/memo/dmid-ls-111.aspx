

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-111.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OVR4J2MbNg2xYnWidCNX1aFiESAv7/gBWmPmIEYqHPU/4Iq+t9/tSvD0aZIJACyF0fcWRRucmVnEew74nuEiv+HP52JIXtgsMUi+SrF9KdNYlL6vAoS4bFwuZbg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1B701254" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-111-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58160   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preclinical Pharmacokinetics and Metabolism of a Potent Non-Nucleoside Inhibitor of the Hepatitis C Virus Ns5b Polymerase</p>

    <p>          Giuliano, C. <i>et al.</i></p>

    <p>          Xenobiotica <b>2005</b>.  35(10-11): 1035-1054</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234427600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234427600008</a> </p><br />

    <p>2.     58161   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effective Inhibition of HBV Replication in Vivo by Anti-HBx Short Hairpin RNAs</p>

    <p>          Carmona, Sergio, Ely, Abdullah, Crowther, Carol, Moolla, Naazneen, Salazar, Felix H, Marion, Patricia L, Ferry, Nicolas, Weinberg, Marc S, and Arbuthnot, Patrick</p>

    <p>          Molecular Therapy <b>2006</b>.  13(2): 411-421</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     58162   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Role for Indoleamine 2,3-Dioxygenase in Vivo in Vaginal Infection by West Nile Virus</p>

    <p>          Wu, W., Bao, S., and King, N.</p>

    <p>          Tissue Antigens <b>2005</b>.  66(5): 464</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542700334">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542700334</a> </p><br />

    <p>4.     58163   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel 4&#39;-hydroxymethyl branched apiosyl nucleosides</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(1): 109-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440989&amp;dopt=abstract</a> </p><br />

    <p>5.     58164   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral screening of some thieno[2,3-d]pyrimidine nucleosides</p>

    <p>          Rashad, AE and Ali, MA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(1): 17-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440982&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16440982&amp;dopt=abstract</a> </p><br />

    <p>6.     58165   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Heterocyclic rimantadine analogues with antiviral activity</p>

    <p>          Zoidis, G, Fytas, C, Papanastasiou, I, Foscolos, GB, Fytas, G, Padalko, E, De Clercq, E, Naesens, L, Neyts, J, and Kolocouris, N</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439137&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     58166   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel imidazole nucleoside containing a diaminodihydro-S-triazine as a substituent: inhibitory activity against the West Nile virus NTPase/helicase</p>

    <p>          Ujjinamatada, RK, Agasimundin, YS, Zhang, P, Hosmane, RS, Schuessler, R, Borowski, P, Kalicharran, K, and Fattom, A</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(10-12): 1775-1788</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438047&amp;dopt=abstract</a> </p><br />

    <p>8.     58167   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-viral uses of borinic acid complexes</p>

    <p>          Bellinger-Kawahara, Carolyn, Maples, Kirk R., and Plattner, Jacob J</p>

    <p>          PATENT:  US <b>2006019927</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005-21887  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     58168   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel yeast cell-based assay to screen for inhibitors of human cytomegalovirus protease in a high-throughput format</p>

    <p>          Cottier, V, Barberis, A, and Luthi, U</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(2): 565-571</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436711&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436711&amp;dopt=abstract</a> </p><br />

    <p>10.   58169   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          ent-Epiafzelechin-(4{alpha}-&gt;8)-epiafzelechin extracted from Cassia javanica inhibits herpes simplex virus type 2 replication</p>

    <p>          Cheng, HY, Yang, CM, Lin, TC, Shieh, DE, and Lin, CC</p>

    <p>          J Med Microbiol <b>2006</b>.  55(Pt 2): 201-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16434713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16434713&amp;dopt=abstract</a> </p><br />

    <p>11.   58170   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Simple Synthesis and Anti-Hiv Activity of Novel Cyclopentene Phosphonate Nucleosides</p>

    <p>          Kim, A. and Hong, J.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2006</b>.  25(1): 1-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200001</a> </p><br />

    <p>12.   58171   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral applications of RNAi for coronavirus</p>

    <p>          Wu, CJ and Chan, YL</p>

    <p>          Expert Opin Investig Drugs <b>2006</b>.  15(2): 89-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16433589&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16433589&amp;dopt=abstract</a> </p><br />

    <p>13.   58172   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase inhibitor susceptibility network position statement: antiviral resistance in influenza A/H5N1 viruses</p>

    <p>          Hayden, F, Klimov, A, Tashiro, M, Hay, A, Monto, A, McKimm-Breschkin, J, Macken, C, Hampson, A, Webster, RG, Amyard, M, and Zambon, M</p>

    <p>          Antivir Ther <b>2005</b>.  10(8): 873-877</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16430192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16430192&amp;dopt=abstract</a> </p><br />

    <p>14.   58173   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral property and mechanisms of a sulphated polysaccharide from the brown alga Sargassum patens against Herpes simplex virus type 1</p>

    <p>          Zhu, W, Chiu, LC, Ooi, VE, Chan, PK, and Ang, PO Jr</p>

    <p>          Phytomedicine <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16427262&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16427262&amp;dopt=abstract</a> </p><br />

    <p>15.   58174   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral prodrugs - the development of successful prodrug strategies for antiviral chemotherapy</p>

    <p>          De Clercq, Erik and Field, Hugh J</p>

    <p>          British Journal of Pharmacology <b>2006</b>.  147(1): 1-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58175   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High levels of adamantane resistance among influenza A (H3N2) viruses and interim guidelines for use of antiviral agents--United States, 2005-06 influenza season</p>

    <p>          Anon</p>

    <p>          MMWR Morb Mortal Wkly Rep <b>2006</b>.  55(2): 44-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16424859&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16424859&amp;dopt=abstract</a> </p><br />

    <p>17.   58176   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An siRNA-based microbicide protects mice from lethal herpes simplex virus 2 infection</p>

    <p>          Palliser, Deborah, Chowdhury, Dipanjan, Wang, Qing-Yin, Lee, Sandra J, Bronson, Roderick T, Knipe, David M, and Lieberman, Judy</p>

    <p>          Nature (London, United Kingdom) <b>2006</b>.  439(7072): 89-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   58177   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Primary cytomegalovirus disease after five years of antiviral prophylaxis</p>

    <p>          Kijpittayarit, S, Deziel, P, Eid, AJ, and Razonable, RR</p>

    <p>          Transplantation <b>2006</b>.  81(1): 137-138</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421494&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421494&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   58178   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological activity of isoxazolidinyl polycyclic aromatic hydrocarbons: potential DNA intercalators</p>

    <p>          Rescifina, A,  Chiacchio, MA, Corsaro, A, De Clercq, E, Iannazzo, D, Mastino, A, Piperno, A, Romeo, G, Romeo, R, and Valveri, V</p>

    <p>          J Med Chem <b>2006</b>.  49(2): 709-715</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420056&amp;dopt=abstract</a> </p><br />

    <p>20.   58179   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent 7-hydroxy-1,2,3,4-tetrahydroisoquinoline-3-carboxylic acid-based macrocyclic inhibitors of hepatitis C virus NS3 protease</p>

    <p>          Chen, KX, Njoroge, FG, Pichardo, J, Prongay, A, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          J Med Chem <b>2006</b>.  49(2): 567-574</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420042&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420042&amp;dopt=abstract</a> </p><br />

    <p>21.   58180   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cyanoacetic Acid Hydrazones of 3-(and 4-)Acetylpyridine and Some Derived Ring Systems as Potential Antitumor and Anti-HCV Agents</p>

    <p>          El-Hawash, SA, Abdel, Wahab AE, and El-Demellawy, MA</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.  339(1): 14-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16411172&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16411172&amp;dopt=abstract</a> </p><br />

    <p>22.   58181   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Imidazole and thiazole derivatives as antiviral agents</p>

    <p>          Conte, Immacolata, Hernando, Jose Ignacio Martin, Malancona, Savina, Ontoria Ontoria, Jesus Maria, and Stansfield, Ian</p>

    <p>          PATENT:  WO <b>2006008556</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   58182   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of a combination of cyclosporin and pegylated interferon for treating hepatitis C (HVC)</p>

    <p>          Cornu-Artis, Catherine, Vachon, Guylaine, Uryuhara, Yoko, Asakawa, Kazuo, Mertes, Reinhild Elisabeth, and Yoshiba, Shinsyou</p>

    <p>          PATENT:  WO <b>2006005610</b>  ISSUE DATE:  20060119</p>

    <p>          APPLICATION: 2005  PP: 13 pp.</p>

    <p>          ASSIGNEE:  (Novartis A.-G., Switz. and Novartis Pharma G.m.b.H.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>24.   58183   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Construction of influenza virus siRNA expression vectors and their inhibitory effects on multiplication of influenza virus</p>

    <p>          Li, YC, Kong, LH, Cheng, BZ, and Li, KS</p>

    <p>          Avian Dis <b>2005</b>.  49(4): 562-573</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16405000&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16405000&amp;dopt=abstract</a> </p><br />

    <p>25.   58184   DMID-LS-111; SCIFINDER-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of aza nucleoside analogs and their use as antiviral agents and inhibiting viral RNA polymerases</p>

    <p>          Babu, Yarlagadda S., Chand, Pooran, Ghosh, Ajit K., Kotian, Pravin L., and Kumar, Satish V</p>

    <p>          PATENT:  WO <b>2006002231</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 75 pp.</p>

    <p>          ASSIGNEE:  (Biocryst Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   58185   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Aminopyrimidinimino isatin analogues: design of novel non- nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum chemotherapeutic properties</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          J Pharm Pharm Sci <b>2005</b>.  8(3): 565-577</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16401403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16401403&amp;dopt=abstract</a> </p><br />

    <p>27.   58186   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase Inhibitors and Their Potential Use in the Avian Influenza Pandemic</p>

    <p>          Reina, J.</p>

    <p>          Medicina Clinica <b>2005</b>.  125(20): 780-783</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234428800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234428800006</a> </p><br />

    <p>28.   58187   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of clarithromycin on rhinovirus-16 infection in A549 cells</p>

    <p>          Jang, YJ, Kwon, HJ, and Lee, BJ</p>

    <p>          Eur Respir J <b>2006</b>.  27(1): 12-19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387930&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>29.   58188   DMID-LS-111; PUBMED-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Depeptidization efforts on P(3)-P(2)(&#39;) alpha-ketoamide inhibitors of HCV NS3-4A serine protease: Effect on HCV replicon activity</p>

    <p>          Bogen, SL, Ruan, S, Liu, R, Agrawal, S, Pichardo, J, Prongay, A, Baroudy, B, Saksena, AK, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387495&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387495&amp;dopt=abstract</a> </p><br />

    <p>30.   58189   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Bortezomib (Velcade (Tm)) and the Bcl-2 Inhibitor Ha14-1 Synergistically Stimulate Apoptosis in Ebv-Transformed Lymphocytes: a Potential Approach to Treatment of Ebv-Associated Lymphoproliferative Disorders.</p>

    <p>          Srimatkandada, P., Loomis, R., and Lacy, J.</p>

    <p>          Blood <b>2005</b>.  106(11): 677A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233426004255">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233426004255</a> </p><br />

    <p>31.   58190   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Response in Pandemic Influenza Viruses</p>

    <p>          Garcia-Sastre, A.</p>

    <p>          Emerging Infectious Diseases <b>2006</b>.  12(1): 44-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700009</a> </p><br />

    <p>32.   58191   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pandemic Influenza Threat and Preparedness</p>

    <p>          Fauci, A.</p>

    <p>          Emerging Infectious Diseases <b>2006</b>.  12(1): 73-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700014</a> </p><br />

    <p>33.   58192   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Economics of Neuraminidase Inhibitor Stockpiling for Pandemic Influenza, Singapore</p>

    <p>          Lee, V. <i>et al.</i></p>

    <p>          Emerging Infectious Diseases <b>2006</b>.  12(1): 95-102</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234419700018</a> </p><br />

    <p>34.   58193   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Evaluation of Some Polyhalogenated Indole C-Nucleosides</p>

    <p>          Chen, J. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1417-1437</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900003</a> </p><br />
    <br clear="all">

    <p>35.   58194   DMID-LS-111; WOS-DMID-1/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ribavarin Should Be Tested in Clinical Trials in Combination With Other Antiviral Agents for Severe Acute Respiratory Syndrome</p>

    <p>          Chu, C. and Chan, K.</p>

    <p>          Chest <b>2005</b>.  128(6): 4050</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234235900043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234235900043</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
