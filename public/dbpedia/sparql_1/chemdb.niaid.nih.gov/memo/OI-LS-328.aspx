

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-328.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9CyMB9riOPL+kW1M3Nr6yt7CBPkUttYrkQn/Egxgy+tJABVQ9iksnPCjga4YMJOFH6/1GCimHA3ooCTmblhgjSG38S3L1WCLDSj6FGwoaKS0IPAzME/5HcixvBk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B1D418EB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-328-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47744   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Antifungal Agents. 11. N-Substituted Derivatives of 1-[(Aryl)(4-aryl-1H-pyrrol-3-yl)methyl]-1H-imidazole: Synthesis, Anti-Candida Activity, and QSAR Studies</p>

    <p>          Di Santo, R, Tafi, A, Costi, R, Botta, M, Artico, M, Corelli, F, Forte, M, Caporuscio, F, Angiolella, L, and Palamara, AT</p>

    <p>          J Med Chem <b>2005</b>.  48(16): 5140-5153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078834&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078834&amp;dopt=abstract</a> </p><br />

    <p>2.     47745   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Design and Synthesis of Depeptidized Macrocyclic Inhibitors of Hepatitis C NS3-4A Protease Using Structure-Based Drug Design</p>

    <p>          Venkatraman, S, Njoroge, FG, Girijavallabhan, VM, Madison, VS, Yao, NH, Prongay, AJ, Butkiewicz, N, and Pichardo, J</p>

    <p>          J Med Chem <b>2005</b>.  48(16): 5088-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078825&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078825&amp;dopt=abstract</a> </p><br />

    <p>3.     47746   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Deletion of the Mycobacterium tuberculosis pknH Gene Confers a Higher Bacillary Load during the Chronic Phase of Infection in BALB/c Mice</p>

    <p>          Papavinasasundaram, KG, Chan, B, Chung, JH, Colston, MJ, Davis, EO, and Av-Gay, Y</p>

    <p>          J Bacteriol <b>2005</b>.  187(16): 5751-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16077122&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16077122&amp;dopt=abstract</a> </p><br />

    <p>4.     47747   OI-LS-328; EMBASE-OI-8/8/2005</p>

    <p class="memofmt1-2">          Synthesis of some novel benzoxazole derivatives as anticancer, anti-HIV-1 and antimicrobial agents</p>

    <p>          Rida, Samia M, Ashour, Fawzia A, El-Hawash, Soad AM, ElSemary, Mona M, Badr, Mona H, and Shalaby, Manal A</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4GP7NX0-1/2/c946fba54969a9980668844fcf2244d8">http://www.sciencedirect.com/science/article/B6VKY-4GP7NX0-1/2/c946fba54969a9980668844fcf2244d8</a> </p><br />

    <p>5.     47748   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Triazine Inhibits Toxoplasma gondii tachyzoites in vitro and in vivo</p>

    <p>          Mui, EJ, Jacobus, D, Milhous, WK, Schiehser, G, Hsu, H, Roberts, CW, Kirisits, MJ, and McLeod, R</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(8): 3463-3467</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048961&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048961&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47749   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Correspondence of in vitro and in vivo fluconazole dose-response curves for Cryptococcus neoformans</p>

    <p>          Larsen, RA, Bauer, M, Thomas, AM, Sanchez, A, Citron, D, Rathbun, M, and Harrison, TS</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(8): 3297-3301</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048939&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048939&amp;dopt=abstract</a> </p><br />

    <p>7.     47750   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Molecular analysis of cross-resistance to capreomycin, kanamycin, amikacin, and viomycin in Mycobacterium tuberculosis</p>

    <p>          Maus, CE, Plikaytis, BB, and Shinnick, TM</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(8): 3192-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048924&amp;dopt=abstract</a> </p><br />

    <p>8.     47751   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Antiviral drug discovery and development: Where chemistry meets with biomedicine</p>

    <p>          De Clercq, E</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16046240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16046240&amp;dopt=abstract</a> </p><br />

    <p>9.     47752   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Inhibition of 7,8-diaminopelargonic acid aminotransferase by amiclenomycin and analogues</p>

    <p>          Mann, S, Marquet, A, and Ploux, O</p>

    <p>          Biochem Soc Trans <b>2005</b>.  33(Pt 4): 802-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16042602&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16042602&amp;dopt=abstract</a> </p><br />

    <p>10.   47753   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Isoprenoid biosynthetic pathways as anti-infective drug targets</p>

    <p>          Rohdich, F, Bacher, A, and Eisenreich, W</p>

    <p>          Biochem Soc Trans <b>2005</b>.  33(Pt 4): 785-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16042599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16042599&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   47754   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial evaluation of various 7-substituted ciprofloxacin derivatives</p>

    <p>          Sriram, D, Yogeeswari, P, Basha, JS, Radha, DR, and Nagaraja, V</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16039859&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16039859&amp;dopt=abstract</a> </p><br />

    <p>12.   47755   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          CD43 Is Required for Optimal Growth Inhibition of Mycobacterium tuberculosis in Macrophages and in Mice</p>

    <p>          Randhawa, AK, Ziltener, HJ, Merzaban, JS, and Stokes, RW</p>

    <p>          J Immunol <b>2005</b>.  175(3): 1805-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16034122&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16034122&amp;dopt=abstract</a> </p><br />

    <p>13.   47756   OI-LS-328; PUBMED-OI-8/8/2005</p>

    <p class="memofmt1-2">          Molecular virology of the hepatitis C virus: implication for novel therapies</p>

    <p>          Glenn, JS</p>

    <p>          Clin Liver Dis  <b>2005</b>.  9(3): 353-69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023970&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16023970&amp;dopt=abstract</a> </p><br />

    <p>14.   47757   OI-LS-328; WOS-OI-7/31/2005</p>

    <p class="memofmt1-2">          TB and phenothiazines</p>

    <p>          Amaral, L</p>

    <p>          SCIENTIST <b>2005</b>.  19(14): 8-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230473100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230473100002</a> </p><br />

    <p>15.   47758   OI-LS-328; WOS-OI-7/31/2005</p>

    <p class="memofmt1-2">          Identification of phthiodiolone ketoreductase, an enzyme required for production of mycobacterial diacyl phthiocerol virulence factors</p>

    <p>          Onwueme, KC, Vos, CJ, Zurita, J, Soll, CE, and Quadri, LEN</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(14): 4760-4766, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230350500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230350500008</a> </p><br />

    <p>16.   47759   OI-LS-328; EMBASE-OI-8/8/2005</p>

    <p class="memofmt1-2">          Antiviral therapy of congenital cytomegalovirus infection</p>

    <p>          Schleiss, Mark R</p>

    <p>          Seminars in Pediatric Infectious Diseases <b>2005</b>.  16(1): 50-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75KN-4FC3661-B/2/3fa0a2dcb2527e05b5dff5b1606f613d">http://www.sciencedirect.com/science/article/B75KN-4FC3661-B/2/3fa0a2dcb2527e05b5dff5b1606f613d</a> </p><br />
    <br clear="all">

    <p>17.   47760   OI-LS-328; WOS-OI-7/31/2005</p>

    <p class="memofmt1-2">          Initial results of a phase 1B, multiple-dose study of VX-950, a hepatitis C virus protease inhibitor</p>

    <p>          Reesink, HW, Zeuzem, S, van Vliet, A, McNair, L, Purdy, S, Chu, HM, and Jansen, PL</p>

    <p>          GASTROENTEROLOGY <b>2005</b>.  128(4): A697-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305474">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305474</a> </p><br />

    <p>18.   47761   OI-LS-328; WOS-OI-7/31/2005</p>

    <p class="memofmt1-2">          Winnowing out drug candidates</p>

    <p>          Yarnell, A</p>

    <p>          CHEMICAL &amp; ENGINEERING NEWS <b>2005</b>.  83(28): 28-30, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230420000046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230420000046</a> </p><br />

    <p>19.   47762   OI-LS-328; WOS-OI-8/7/2005</p>

    <p class="memofmt1-2">          Complete replication of hepatitis C virus in cell culture</p>

    <p>          Lindenbach, BD, Evans, MJ, Syder, AJ, Wolk, B, Tellinghuisen, TL, Liu, CC, Maruyama, T, Hynes, RO, Burton, DR, McKeating, JA, and Rice, CM</p>

    <p>          SCIENCE <b>2005</b>.  309(5734): 623-626, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230735200052">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230735200052</a> </p><br />

    <p>20.   47763   OI-LS-328; WOS-OI-8/7/2005</p>

    <p class="memofmt1-2">          Characterization of intracellular activity of antitubercular constituents from the roots of Euclea natalensis</p>

    <p>          Lall, N, Meyer, JJM, Wang, Y, Bapela, NB, van, Rensburg CEJ, Fourie, B, and Franzblau, SG</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2005</b>.  43(4): 353-357, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230647400013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230647400013</a> </p><br />

    <p>21.   47764   OI-LS-328; WOS-OI-8/7/2005</p>

    <p class="memofmt1-2">          Amino acid ester prodrugs of 2-bromo-5,6-dichloro-1-(beta-D-ribofuranosyl) benzimidazole enhance metabolic stability in vitro and in vivo</p>

    <p>          Lorenzi, PL, Landowski, CP, Song, XQ, Borysko, KZ, Breitenbach, JM, Kim, JS, Hilfinger, JM, Townsend, LB, Drach, JC, and Amidon, GL</p>

    <p>          JOURNAL OF PHARMACOLOGY AND EXPERIMENTAL THERAPEUTICS <b>2005</b>.  314(2): 883-890, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230550300049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230550300049</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
