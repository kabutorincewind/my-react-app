

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-09-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="I5vcQBMcYOnI82iyXe06IzozDJ59y7SpNNuyCEkeFVr3DxeNOCodSy/6EftEmwyG+zGPkfoeeNWYAJ7Lv4w61yM81TMMTmLBd/JC8pZlNv/9r1tjazO4gbTxjJg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="92917E8C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  August 19 - September 1, 2009</h1> 

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19721069?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">LFA-1 antagonists as agents limiting HIV-1 infection/transmission and potentiating the effect of the fusion inhibitor T-20.</a>  Tardif MR, Gilbert C, Thibault S, Fortin JF, Tremblay MJ.  Antimicrob Agents Chemother. 2009 Aug 31. [Epub ahead of print]  PMID: 19721069</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19721067?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of Envelope Mediated CD4+ T Cell Depletion by HIV Attachment Inhibitors.</a>  Alexander L, Zhang S, McAuliffe B, Connors D, Zhou N, Wang T, Agler M, Kadow J, Lin PF.  Antimicrob Agents Chemother. 2009 Aug 31. [Epub ahead of print]  PMID: 19721067</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19721061?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The phtalocyanine prototype derivative Alcian Blue: the first synthetic agent with selective anti-human immunodeficiency virus activity due to its gp120 glycan-binding potential.</a>  François KO, Pannecouque C, Auwerx J, Lozano V, Pérez-Pérez MJ, Schols D, Balzarini J.  Antimicrob Agents Chemother. 2009 Aug 31. [Epub ahead of print]  PMID: 19721061</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19704131?ordinalpos=29&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Impact of Novel HIV-1 Reverse Transcriptase Mutations, P119S and T165A on 4&#39;-ethynylthymidine Analog Resistance Profile.</a>  Yang G, Paintsil E, Dutschman GE, Grill SP, Wang CJ, Wang J, Tanaka H, Hamasaki T, Baba M, Cheng YC.  Antimicrob Agents Chemother. 2009 Aug 24. [Epub ahead of print]  PMID: 19704131</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19704127?ordinalpos=30&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">HIV-1 recombinant reverse transcriptase enzymes containing the G190A and Y181C resistance mutations remain sensitive to etravirine.</a>  Xu H, Quan Y, Brenner BG, Bar-Magen T, Oliveira M, Schader SM, Wainberg MA.  Antimicrob Agents Chemother. 2009 Aug 24. [Epub ahead of print]  PMID: 19704127</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19699239?ordinalpos=38&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Sustained and specific in vitro inhibition of HIV-1 replication by a protease inhibitor encapsulated in gp120-targeted liposomes.</a>  Clayton R, Ohagen A, Nicol F, Del Vecchio AM, Jonckers TH, Goethals O, Van Loock M, Michiels L, Grigsby J, Xu Z, Zhang YP, Gutshall LL, Cunningham M, Jiang H, Bola S, Sarisky RT, Hertogs K.  Antiviral Res. 2009 Aug 20. [Epub ahead of print]  PMID: 19699239</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19664921?ordinalpos=59&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibitors of HIV-1 attachment. Part 4: A study of the effect of piperazine substitution patterns on antiviral potency in the context of indole-based derivatives.</a>  Wang T, Kadow JF, Zhang Z, Yin Z, Gao Q, Wu D, Parker DD, Yang Z, Zadjura L, Robinson BA, Gong YF, Blair WS, Shi PY, Yamanaka G, Lin PF, Meanwell NA.  Bioorg Med Chem Lett. 2009 Sep 1;19(17):5140-5. Epub 2009 Jul 18.  PMID: 19664921</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19660957?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and anti-HIV activity of novel 2&#39;,3&#39;-dideoxy-3&#39;-thiacytidine prodrugs.</a>  Ravetti S, Gualdesi MS, Trinchero-Hernández JS, Turk G, Briñón MC.  Bioorg Med Chem. 2009 Sep 1;17(17):6407-13. Epub 2009 Jul 18.  PMID: 19660957</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19647440?ordinalpos=64&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">1,2,3-Selenadiazole thioacetanilides: synthesis and anti-HIV activity evaluation.</a>  Zhan P, Liu X, Fang Z, Pannecouque C, De Clercq E.  Bioorg Med Chem. 2009 Sep 1;17(17):6374-9. Epub 2009 Jul 18.  PMID: 19647440</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19640718?ordinalpos=66&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Amine substituted N-(1H-benzimidazol-2ylmethyl)-5,6,7,8-tetrahydro-8-quinolinamines as CXCR4 antagonists with potent activity against HIV-1.</a>  Gudmundsson KS, Sebahar PR, Richardson LD, Miller JF, Turner EM, Catalano JG, Spaltenstein A, Lawrence W, Thomson M, Jenkinson S.  Bioorg Med Chem Lett. 2009 Sep 1;19(17):5048-52. Epub 2009 Jul 10.  PMID: 19640718</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19632112?ordinalpos=67&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibitors of HIV-1 attachment. Part 3: A preliminary survey of the effect of structural variation of the benzamide moiety on antiviral activity.</a>  Meanwell NA, Wallace OB, Wang H, Deshpande M, Pearce BC, Trehan A, Yeung KS, Qiu Z, Wright JJ, Robinson BA, Gong YF, Wang HG, Blair WS, Shi PY, Lin PF.  Bioorg Med Chem Lett. 2009 Sep 1;19(17):5136-9. Epub 2009 Jul 10.  PMID: 19632112</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19631528?ordinalpos=68&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Substituted tetrahydroquinolines as potent allosteric inhibitors of reverse transcriptase and its key mutants.</a>  Su DS, Lim JJ, Tinney E, Wan BL, Young MB, Anderson KD, Rudd D, Munshi V, Bahnck C, Felock PJ, Lu M, Lai MT, Touch S, Moyer G, Distefano DJ, Flynn JA, Liang Y, Sanchez R, Prasad S, Yan Y, Perlow-Poehnelt R, Torrent M, Miller M, Vacca JP, Williams TM, Anthony NJ.  Bioorg Med Chem Lett. 2009 Sep 1;19(17):5119-23. Epub 2009 Jul 10.  PMID: 19631528</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19324473?ordinalpos=137&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, antiviral and anticancer activity of some novel thioureas derived from N-(4-nitro-2-phenoxyphenyl)-methanesulfonamide.</a>  Karaku&#351; S, Güniz Küçükgüzel S, Küçükgüzel I, De Clercq E, Pannecouque C, Andrei G, Snoeck R, Sahin F, Bayrak OF.  Eur J Med Chem. 2009 Sep;44(9):3591-5. Epub 2009 Mar 9.  PMID: 19324473</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000268975300020">Phosphonate terminated PPH dendrimers: influence of pendant alkyl chains on the in vitro anti-HIV-1 properties.</a>  Perez-Anes, A; Spataro, G; Coppel, Y; Moog, C; Blanzat, M; Turrin, CO; Caminade, AM; Rico-Lattes, I; Majoral, JP.  ORGANIC &amp; BIOMOLECULAR CHEMISTRY, 2009; 7(17): 3941-3948. </p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269042700023">Identification and Biological Evaluation of a Series of 1H-Benzo[de]isoquinoline-1,3(2H)-diones as Hepatitis C Virus NS5B Polymerase Inhibitors.</a>  Ontoria, JM; Rydberg, EH; Di Marco, S; Tomei, L; Attenni, B; Malancona, S; Hernando, JIM; Gennari, N; Koch, U; Narjes, F; Rowley, M; Summa, V; Carroll, SS; Olsen, DB; De Francesco, R; Altamura, S; Migliaccio, G; Carfi, A.  JOURNAL OF MEDICINAL CHEMISTRY, 2009: 52(16): 5217-5227. </p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269058200004">Toward the Design of Mutation-Resistant Enzyme Inhibitors: Further Evaluation of the Substrate Envelope Hypothesis.</a>  Kairys, V; Gilson, MK; Lather, V; Schiffer, CA; Fernandes, MX.  CHEMICAL BIOLOGY &amp; DRUG DESIGN, 2009; 74(3): 234-245. </p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269029100028">1,2,3-Selenadiazole thioacetanilides: Synthesis and anti-HIV activity evaluation.</a>  Zhan, P; Liu, XY; Fang, ZJ; Pannecouque, C; De Clercq, E.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(17): 6374-6379. </p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269029100032">Synthesis and anti-HIV activity of novel 2 &#39;,3 &#39;-dideoxy-3&#39;-thiacytidine prodrugs.</a>  Ravetti, S; Gualdesi, MS; Trinchero-Hernandez, JS; Turk, G; Brinon, MC.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(17): 6407-6413.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269018200074">A New Lignan from Fruit of Schisandra wilsoniana ACSmith and its Anti-HIV activity.</a>  Fan, YP; Duan, LP; Dong, XH; Dai, Y; Li, GP.  ASIAN JOURNAL OF CHEMISTRY, 2009; 21(7): 5488-5492.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
