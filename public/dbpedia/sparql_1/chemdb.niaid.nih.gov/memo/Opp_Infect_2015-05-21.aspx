

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-05-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EMtuGjbDDyzaS2bEWAczewrQwUDgBay/y9BsvLv4sTIuD/x9h957z2d4c6vF0S2rSU7fRI37GmJ6layQ5cAz29LlSdKgTj3fYRwCzLXFdJV3ujjXG6tP5Ma5qwU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0301F59A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 8 - May 21, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25985354">Microwave-assisted Synthesis of Novel Pyrazolo[3,4-g][1,8]naphthyridin-5-amine with Potential Antifungal and Antitumor Activity.</a> Acosta, P., E. Butassi, B. Insuasty, A. Ortiz, R. Abonia, S.A. Zacchino, and J. Quiroga. Molecules, 2015. 20(5): p. 8499-8520. PMID[25985354].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25965506">Activity of Novel Synthetic Peptides against Candida albicans.</a> Lum, K.Y., S.T. Tay, C.F. Le, V.S. Lee, N.H. Sabri, R.D. Velayuthan, H. Hassan, and S.D. Sekaran. Scientific Reports, 2015. 5: p. 9657. PMID[25965506].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25875695">1,5-Benzodiazepine Derivatives as Potential Antimicrobial Agents: Design, Synthesis, Biological Evaluation, and Structure-Activity Relationships.</a> Wang, L.Z., X.Q. Li, and Y.S. An. Organic &amp; Biomolecular Chemistry, 2015. 13(19): p. 5497-5509. PMID[25875695].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25882524">New Indolizines with Phenanthroline Skeleton: Synthesis, Structure, Antimycobacterial and Anticancer Evaluation.</a> Danac, R., C.M. Al Matarneh, S. Shova, T. Daniloaia, M. Balan, and I.I. Mangalagiu. Bioorganic &amp; Medicinal Chemistry, 2015. 23(10): p. 2318-2327. PMID[25882524].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25988611">Design, Synthesis and Antitubercular Activity of Certain Nicotinic acid Hydrazides.</a> Eldehna, W.M., M. Fares, M.M. Abdel-Aziz, and H.A. Abdel-Aziz. Molecules, 2015. 20(5): p. 8800-8815. PMID[25988611].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25714942">Antimalarial NADPH-consuming Redox-cyclers as Superior Glucose-6-Phosphate Dehydrogenase Deficiency Copycats.</a> Bielitza, M., D. Belorgey, K. Ehrhardt, L. Johann, D.A. Lanfranchi, V. Gallo, E. Schwarzer, F. Mohring, E. Jortzik, D.L. Williams, K. Becker, P. Arese, M. Elhabiri, and E. Davioud-Charvet. Antioxidants &amp; Redox Signaling, 2015. 22(15): p. 1337-1351. PMID[25714942].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25881827">Synthesis and Antimalarial Evaluation of Prodrugs of Novel Fosmidomycin Analogues.</a> Faisca Phillips, A.M., F. Nogueira, F. Murtinheira, and M.T. Barros. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(10): p. 2112-2116. PMID[25881827].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25721804">Antiplasmodial Activity of Medicinal Plants from Chhotanagpur Plateau, Jharkhand, India.</a> Singh, N., N.K. Kaushik, D. Mohanakrishnan, S.K. Tiwari, and D. Sahal. Journal of Ethnopharmacology, 2015. 165: p. 152-162. PMID[25721804].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25951139">Discovery of Novel Liver-stage Antimalarials through Quantum Similarity.</a> Sullivan, D.J., Y. Liu, B.T. Mott, N. Kaludov, and M.N. Martinov. Plos One, 2015. 10(5): p. e0125593. PMID[25951139].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0508-052115.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352801500019">Simple One Step Synthesis of Nonionic Dithiol Surfactants and Their Self-assembling with Silver Nanoparticles: Characterization, Surface Properties, Biological Activity.</a>  Abd-Elaal, A.A., S.M. Tawfik, and S.M. Shaban. Applied Surface Science, 2015. 342: p. 144-153. ISI[000352801500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0508-052115.</p><br />

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000353404500037"></a></p>

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000353404500037">11. Cleavage Specificity of Mycobacterium tuberculosis ClpP1P2 Protease and Identification of Novel Peptide Substrates and Boronate Inhibitors with Anti-bacterial Activity.</a> Akopian, T., O. Kandror, C. Tsu, J.H. Lai, W.G. Wu, Y.X. Liu, P. Zhao, A. Park, L. Wolf, L.R. Dick, E.J. Rubin, W. Bachovchin, and A.L. Goldberg. Journal of Biological Chemistry, 2015. 290(17): p. 11008-11020. ISI[000353404500037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp">Synthesis, Crystal Structure, Theoretical Calculations, and Electrochemical and Biological Studies of Polymeric (N,N,N&#39;,N&#39;-Tetramethylethylenediamine)bis(thiocyanato-kappa N)copper(II), Cu(tmeda)(NCS)(2) n.</a> Espinosa, A., M. Sohail, M. Habib, K. Naveed, M. Saleem, H.U. Rehman, I. Hussain, A. Munawar, and S. Ahmad. Polyhedron, 2015. 90: p. 252-257. ISI[000352663900033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0508-052115.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000353072900009">Identification of Potential Mycobacterium tuberculosis Topoisomerase I Inhibitors: A Study against Active, Dormant and Resistant Tuberculosis.</a> Sridevi, J.P., P. Suryadevara, R. Janupally, J. Sridhar, V. Soni, H.S. Anantaraju, P. Yogeeswari, and D. Sriram. European  Journal of Pharmaceutical Sciences, 2015. 72: p. 81-92. ISI[000353072900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0508-052115.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
