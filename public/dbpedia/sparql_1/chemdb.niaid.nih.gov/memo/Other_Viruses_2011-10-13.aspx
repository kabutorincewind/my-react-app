

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XOU5eq67dqZOHFr8nLSii9elGjVymc0mt1sQ1KRjGWBxwRz99PcEo2cqdyTaRJFujKVlWrvQHyrdp35xLjzQyxbjH3IKrzgp6i/pirZlzVDzZFkT69diyJLT1ug=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="17295948" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">September 30 - October 13, 2011</h1>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21983447">Synthesis of Purine Modified 2&#39;-C-Methyl Nucleosides as Potential anti-HCV Agents</a>. Zhang, H.W., L. Zhou, S.J. Coats, T.R. McBrayer, P.M. Tharnish, L. Bondada, M. Detorio, S.A. Amichai, M.D. Johns, T. Whitaker, and R.F. Schinazi, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21983447].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0930-101311.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21978675">Discovery of Highly Potent Small Molecule Hepatitis C Virus Entry Inhibitors.</a> Mittapalli, G.K., A. Jackson, F. Zhao, H. Lee, S. Chow, J. McKelvy, F. Wong-Staal, and J.E. Macdonald, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21978675].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0930-101311.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21976647">ISG56 and IFITM1 Proteins Inhibit Hepatitis C Virus Replication.</a> Raychoudhuri, A., S. Shrivastava, R. Steele, H. Kim, R. Ray, and R.B. Ray, Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21976647].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0930-101311.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21957306">HCV Nucleotide Inhibitors PSI-352938 and PSI-353661 Exhibit a Novel Mechanism of Resistance Requiring Multiple Mutations within Replicon RNA.</a> Lam, A.M., C. Espiritu, S. Bansal, H.M. Micolochick Steuer, V. Zennou, M.J. Otto, and P.A. Furman, Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21957306].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0930-101311.</p> 

    <p> </p>

    <p class="memofmt2-2">Epstein Barr Virus</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21967288">Synthesis and Biological Activity of the Nucleoside Analogs Based on Polyfluoroalkyl-substituted 1,2,3-triazoles.</a> Kanishchev, O.S., G.P. Gudz, Y.G. Shermolovich, N.V. Nesterova, S.D. Zagorodnya, and A.V. Golovan, Nucleosides, Nucleotides &amp; Nucleic acids, 2011. 30(10): p. 768-783; PMID[21967288].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0930-101311.</p>

    <p> </p> 

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p> 

    <p class="NoSpacing">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294936800011">Inhibitory Effects of Tricin Derivative from Sasa albo-marginata on Replication of Human Cytomegalovirus.</a> Akuzawa, K., R. Yamada, Z. Li, Y. Li, H. Sadanari, K. Matsubara, K. Watanabe, M. Koketsu, Y. Tuchida, and T. Murayama, Antiviral Research, 2011. 91(3): p. 296-303; ISI[000294936800011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0930-101311.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294720600031">The Synthesis of Novel Heteroaryl-fused 7,8,9,10-tetrahydro-6H-azepino 1,2-a indoles, 4-oxo-2,3-dihydro-1H- 1,4 diazepino 1,7-a indoles and 1,2,4,5-tetrahydro- 1,4 oxazepino 4,5-a indoles.</a> Ding, M., F. He, M.A. Poss, K.L. Rigat, Y.K. Wang, S.B. Roberts, D.K. Qiu, R.A. Fridell, M. Gao, and R.G. Gentles, Effective inhibitors of HCV NS5B polymerase. Organic &amp; Biomolecular Chemistry, 2011. 9(19): p. 6654-6662; ISI[000294720600031].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0930-101311.</p>
    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
