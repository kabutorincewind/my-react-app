

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-09-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sENol6VyrVZc1C7KkhP0njak/YQRehquh4LoUucvXf6NEcEb17MJaegCSgmg0433WtKnhwXDcrqEKGml5GBGClOi12ABrc63vCrJahvrWq+VyYaLd0C/lG46uWs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="17DCB365" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:</h1>

    <h1 class="memofmt2-h1">August 20 - September 2, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20538384">Synthesis, structure-activity relationship and antiviral activity of 3&#39;-N,N-dimethylamino-2&#39;,3&#39;-dideoxythymidine and its prodrugs.</a> Singh RK, Yadav D, Rai D, Kumari G, Pannecouque C, De Clercq E. Eur J Med Chem. 2010 Sep;45(9):3787-93. Epub 2010 May 20.PMID: 20538384 [PubMed - in process]</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20580677">Rev-derived peptides inhibit HIV-1 replication by antagonism of Rev and a co-receptor, CXCR4.</a> Shimane K, Kodama EN, Nakase I, Futaki S, Sakurai Y, Sakagami Y, Li X, Hattori T, Sarafianos SG, Matsuoka M. Int J Biochem Cell Biol. 2010 Sep;42(9):1482-8. Epub 2010 May 16.PMID: 20580677</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20626408">Structure-based in silico design of a high-affinity dipeptide inhibitor for novel protein drug target Shikimate kinase of Mycobacterium tuberculosis.</a> Kumar M, Verma S, Sharma S, Srinivasan A, Singh TP, Kaur P. Chem Biol Drug Des. 2010 Sep 1;76(3):277-84. Epub 2010 Jul 5.PMID: 20626408</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20598780">Tricyclononene carboxamide derivatives as novel anti-HIV-1 agents.</a> Dong MX, Zhang J, Peng XQ, Lu H, Yun LH, Jiang S, Dai QY. Eur J Med Chem. 2010 Sep;45(9):4096-103. Epub 2010 Jun 9.PMID: 20598780</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20639524">Ibalizumab: an anti-CD4 monoclonal antibody for the treatment of HIV-1 infection.</a> Bruno CJ, Jacobson JM. J Antimicrob Chemother. 2010 Sep;65(9):1839-41. Epub 2010 Jul 17.PMID: 20639524</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20723214">Specific eradication of HIV-1 from infected cultured cells.</a> Levin A, Hayouka Z, Friedler A, Loyter A. AIDS Res Ther. 2010 Aug 19;7(1):31. [Epub ahead of print]PMID: 20723214</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20718496">Effect of synthetic peptides belonging to E2 envelope protein of GB virus C on human immunodeficiency virus type 1 infection.</a>Herrera E, Tenckhoff S, Gómara MJ, Galatola R, Bleda MJ, Gil C, Ercilla G, Gatell JM, Tillmann HL, Haro I. J Med Chem. 2010 Aug 26;53(16):6054-63.PMID: 20718496</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20707594">Novel integrase inhibitors for HIV.</a> Prada N, Markowitz M. Expert Opin Investig Drugs. 2010 Sep;19(9):1087-98.PMID: 20707594</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20706961">Synthesis of novel acyclic nucleoside analogues with anti-retroviral activity.</a> Paju A, Päri M, Selyutina A, Zusinaite E, Merits A, Pehk T, Siirde K, Müürisepp AM, Kailas T, Lopp M. Nucleosides Nucleotides Nucleic Acids. 2010 Sep;29(9):707-20.PMID: 20706961</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20691339">Synthesis of substituted thieno[2,3-d]pyrimidine-2,4-dithiones and their S-glycoside analogues as potential antiviral and antibacterial agents.</a> Hafez HN, Hussein HA, El-Gazzar AR. Eur J Med Chem. 2010 Sep;45(9):4026-34. Epub 2010 Jun 2.PMID: 20691339</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20685126">Antiviral agents 2. Synthesis of trimeric naphthoquinone analogues of conocurvone and their antiviral evaluation against HIV.</a> Crosby IT, Bourke DG, Jones ED, de Bruyn PJ, Rhodes D, Vandegraaff N, Cox S, Coates JA, Robertson AD. Bioorg Med Chem. 2010 Sep 1;18(17):6442-50. Epub 2010 Aug 3.PMID: 20685126</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20673723">Conjugates of betulin derivatives with AZT as potent anti-HIV agents.</a> Xiong J, Kashiwada Y, Chen CH, Qian K, Morris-Natschke SL, Lee KH, Takaishi Y. Bioorg Med Chem. 2010 Sep 1;18(17):6451-69. Epub 2010 Jul 7.PMID: 20673723</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20731670">Synthesis, Biological Evaluation and Molecular Modeling Studies of N-aryl-2-arylthioacetamides as Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Xiaohe Z, Yu Q, Hong Y, Xiuqing S, Rugang Z. Chem Biol Drug Des. 2010 Aug 20. [Epub ahead of print]PMID: 20731670</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20803523">Phlorotannins from Ecklonia cava (Phaeophyceae): Biological activities and potential health benefits.</a> Wijesekara I, Yoon NY, Kim SK. Biofactors. 2010 Aug 27. [Epub ahead of print]PMID: 20803523</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20800626">Evaluation of the effect of pyrimethamine; an anti-malarial drug; on HIV-1 replication.</a>Oguariri RM, Adelsberger JW, Baseler MW, Imamichi T. Virus Res. 2010 Aug 25. [Epub ahead of print]PMID: 20800626</p>

    <p class="title">16.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20799144">AIDS 2010 - XVIII International AIDS Conference.</a>Desimmie BA. IDrugs. 2010 Sep;13(9):622-5.PMID: 20799144</p>

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20739163">A laccase with antiproliferative activity against tumor cells from an edible mushroom, white common Agrocybe cylindracea.</a> Hu DD, Zhang RY, Zhang GQ, Wang HX, Ng TB. Phytomedicine. 2010 Aug 23. [Epub ahead of print]PMID: 20739163</p>

    <p class="title">18.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20739003">Partial selective inhibition of HIV-1 reverse transcriptase and human DNA polymerases gamma and beta by thiated 3&#39;-fluorothymidine analogue 5&#39;-triphosphates.</a> Wi&#324;ska P, Miazga A, Pozna&#324;ski J, Kulikowski T. Antiviral Res. 2010 Aug 22. [Epub ahead of print]PMID: 20739003</p>

    <p class="title">19.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20685117">Discovery of potent HIV integrase inhibitors active against raltegravir resistant viruses.</a> Le G, Vandegraaff N, Rhodes DI, Jones ED, Coates JA, Lu L, Li X, Yu C, Feng X, Deadman JJ. Bioorg Med Chem Lett. 2010 Sep 1;20(17):5013-8. Epub 2010 Jul 16.PMID: 20685117</p>

    <p class="title">20.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20674358">Studies on the structure-activity relationship of 1,3,3,4-tetra-substituted pyrrolidine embodied CCR5 receptor antagonists. Part 2: Discovery of highly potent anti-HIV agents.</a> Li B, Jones ED, Zhou E, Chen L, Baylis DC, Yu S, Wang M, He X, Coates JA, Rhodes DI, Pei G, Deadman JJ, Xie X, Ma D. Bioorg Med Chem Lett. 2010 Sep 1;20(17):5334-6. Epub 2010 May 19.PMID: 20674358</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="ListParagraphCxSpFirst">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600028">Studies of HIV-1 Integrase Inhibitory Activity of Wrightia tinctoria</a>.  Selvam, P; Maddali, K; Pommier, Y.   ANTIVIRAL RESEARCH, 2010; 86(1): A28.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600039">Small Molecules Targeting Protein-Protein Interactions: A Promising  Anti-HIV Strategy</a>.  Chimirri, A; De Luca, L; Ferro, S; Gitto, R; Monforte, AM; Agnello, S; Barreca, ML; Christ, F; Debyser, Z.  ANTIVIRAL RESEARCH, 2010; 86(1): A32</p>
    <p>

    <p class="ListParagraphCxSpMiddle">23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600062">Pro-drugs of Strand Transfer Inhibitors of HIV-1 Integrase: Inhibition Data, Structure-Activity Analysis and Anti-HIV Activity</a>. Nair, V; Seo, B; Nishonov, M; Okello, M; Mishra, S. ANTIVIRAL RESEARCH, 2010; 86(1):  A40.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600078">Studies of HIV lntegrase Inhibitory Activity of Morinda citrifolia L  Noni Fruit Extracts</a>. Selvam, P; Maddali, K; Marchand, C; Pommier, Y.  ANTIVIRAL RESEARCH, 2010; 86(1): A45-46.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">25.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600105">Screening and Rational Design of Low Molecular Weight HIV Fusion Inhibitors.</a>  Gochin, M; Zhou, GY; Wu, D; Whitby, L; Boger, D; Chung, T.  ANTIVIRAL RESEARCH, 2010; 86(1):  A55.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">26.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600119">New Nucleoside and bis-Nucleoside-Phosphonate Conjugates: Design,  Stability, and Anti-HIV Evaluation.</a>  Kukhanova, M; Jasko, M; Yanvarev, D; Karpenko, I; Shipitzyn, A; Khandazhinskaya, A. ANTIVIRAL RESEARCH, 2010; 86(1):  A60.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">27.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280691700019">Design and evaluation of antiretroviral peptides corresponding to the  C-terminal heptad repeat region (C-HR) of human immunodeficiency virus type 1 envelope glycoprotein gp41.</a>  Soonthornsata, B; Tian, YS; Utachee, P; Sapsutthipas, S; Isarangkura-na-ayuthaya, P; Auwanit, W; Takagi, T; Ikuta; Sawanpanyalert, P; Kawashita, N; Kameoka, M.  VIROLOGY, 2010; 405(1):  157-164.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">28.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278329600065">PRO 2000, a broadly active anti-HIV sulfonated compound, inhibits viral  entry by multiple mechanisms</a>. Huskens, D; Profy, AT; Vermeire, K; Schols, D.  RETROVIROLOGY, 2010; 7(S1): 20-20.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">29.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278329600201">Allosteric regulation by non peptidic, low molecular weight compounds of CCR5 coupling to g-proteins and interaction with Gp120-consequences on inhibition of R5 HIV-1 infection</a><span class="memofmt2-3">.</span>    Rueda, P; Garcia-Perez, J; Staropoli, I; Kellenberger, E; Alcami, J; Arenzana-Seisdedos, F; Lagane, B.  RETROVIROLOGY, 2010; 7(S1): 63-64.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">30.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280862200022">Palmitic Acid Analogs Exhibit Nanomolar Binding Affinity for the HIV-1 CD4 Receptor and Nanomolar Inhibition of gp120-to-CD4 Fusion.</a>  Paskaleva, EE; Xue, J; Lee, DYW; Shekhtman, A; Canki, M.  PLOS ONE, 2010; 5(8).</p>
    <p>

    <p class="ListParagraphCxSpMiddle">31.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278019801532">Membrane-Anchored and Secreted C-Peptides as HIV Entry Inhibitors.</a>  Kimpel, J; Egerer, L; Kahle, J; Brauer, F; Hartmann, M; Hermann, FG; Newrzela, S; von Laer, D.  MOLECULAR THERAPY, 2009; 17(S1): 366.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">32.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280702100019">HIV-1 integrase inhibition by dimeric bisbenzimidazoles with different spacer structures</a>.  Korolev, SP; Tashlitsky, VN; Smolov, MA; Gromyko, AV; Zhuze, AL; Agapkina, YY; Gottikh, MB.  MOLECULAR BIOLOGY,  2010; 44(4): 633-641.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">33.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280682400044">Novel Recombinant Engineered gp41 N-terminal Heptad Repeat Trimers and Their Potential as Anti-HIV-1 Therapeutics or Microbicides</a>.  Chen, X; Lu, L; Qi, Z; Lu, H; Wang, J; Yu, XX; Chen, YH; Jiang, SB.  JOURNAL OF BIOLOGICAL CHEMISTRY, 2010; 285(33):  25506-25515.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">34.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280890000020">Lignin-like Activity of Lentinus edodes Mycelia Extract (LEM).</a>  Kawano, M; Sakagami, H; Satoh, K; Shioda, S; Kanamoto, T; Terakubu, S; Nakashima, H; Makino, T.  IN VIVO, 2010; 24(4):  543-551.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">35.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277953700274">Developing a Topical HIV Microbicide: CD4-targeting Aptamers Inhibit HIV Infection in Primary Cells in vitro and in Polarized Human Cervical Explants</a>.  Wheeler, A; Lieberman, J; Trifonova, R; Basar, E; Dykxhoorn, D.  CLINICAL IMMUNOLOGY, 2010; 135(S):  S96-97.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">36.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280664100026">Anti-HIV and antiplasmodial activity of original flavonoid derivatives</a>.  Casano, G; Dumetre, A; Pannecouque, C; Hutter, S; Azas, N; Robin, M. BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(16): 6012-6023.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">37.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280664100035">An anti-proliferative curcuminoid from structure-activity relationship studies on 3,5-bis(benzylidene)-4-piperidones.</a>  Lagisetty, P; Vilekar, P; Sahoo, K; Anant, S; Awasthi, V.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(16): 6109-6120.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">38.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600138">Rational Drug Design-Screening and Synthesis of Potential Deoxyhypusine Synthase Inhibitors Targeting HIV-1 Replication</a>. Schroeder, M; Kolodzik, A; Windshuegel, B; Krepstakies, M; Priyadarshini, P; Rarey, M; Hauber, J; Meier, C.  ANTIVIRAL RESEARCH, 2010; 86(1): A67.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">39.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280966900003">Anti-HIV, anti-tubercular and mutagenic activities of borrelidin.</a>  Bhikshapathi, DVRN; Krishna, DR; Kishan, V. INDIAN JOURNAL OF BIOTECHNOLOGY,  2010; 9(3):  265-270.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">40.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280930900010">Synthesis and biological activities of 2-methyl-4-N-2&#39;-cyanoethyl-N-substituted benzaldehydes and their derivatives</a>.  Dhingra, S; Srivastava, AK; Dhingra, V.  JOURNAL OF SAUDI CHEMICAL SOCIETY, 2010; 14(2): 213-215.</p>
    <p>

    <p class="ListParagraphCxSpMiddle">41.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281091700006">A new chitin-binding lectin from rhizome of Setcreasea purpurea with antifungal, antiviral and apoptosis-inducing activities</a>.  Yao, Q; Wu, C;  Luo, P; Xiang, XC; Liu, JJ; Mou, L; Bao, JK.  PROCESS BIOCHEMISTRY, 2010; 45(9):  1477-1485.</p>
    <p>

    <p class="ListParagraphCxSpLast">42.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000278565100334">Novel allosteric inhibitors of HIV-1 integrase on the base of multimodified oligonucleotides</a>.  Zatsepin, T; Korolev, S; Smolov, M; Gottikh, M; Agapkina, J.  FEBS JOURNAL, 2010; 277(S1):  94-94.</p>
    <p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
