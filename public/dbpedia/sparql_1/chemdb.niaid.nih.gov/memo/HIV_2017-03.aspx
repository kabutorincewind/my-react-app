

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tUxsXgCZsi1o0Q8REWNwticOxKJbFiRFMWURfEzOa0AsA5ICd1pbas1JRvj/NaCyJU0SfuFYQafYh/yTzQZKgel5uUocCTKHnN/f4hW/9pI+2251imLaTRFsfwY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D693AEC5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: March, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28166799">Toll-like Receptor 3 Activation Selectively Reverses HIV Latency in Microglial Cells.</a> Alvarez-Carbonell, D., Y. Garcia-Mesa, S. Milne, B. Das, C. Dobrowolski, R. Rojas, and J. Karn. Retrovirology, 2017. 14(9): 25pp. PMID[28166799]. PMCID[PMC5294768].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28050723">Computational Identification of Novel Entry Inhibitor Scaffolds Mimicking Primary Receptor CD4 of HIV-1 gp120.</a> Andrianov, A.M., I.A. Kashyn, and A.V. Tuzikov. Journal of Molecular Modeling, 2017. 23(18). PMID[28050723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28122580">Identification of Small Molecule Modulators of HIV-1 Tat and Rev Protein Accumulation.</a> Balachandran, A., R. Wong, P. Stoilov, S. Pan, B. Blencowe, P. Cheung, P.R. Harrigan, and A. Cochrane. Retrovirology, 2017. 14(7): 21pp. PMID[28122580]. PMCID[PMC5267425].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28096489">Induced Packaging of Cellular MicroRNAs into HIV-1 Virions Can Inhibit Infectivity.</a> Bogerd, H.P., E.M. Kennedy, A.W. Whisnant, and B.R. Cullen. mBio, 2017. 8(1): e02125-16. PMID[28096489].</p>

    <p class="plaintext">PMCID[PMC5241401].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27552154">MZC Gel Inhibits SHIV-RT and HSV-2 in Macaque Vaginal Mucosa and SHIV-RT in Rectal Mucosa.</a> Calenda, G., G. Villegas, P. Barnable, C. Litterst, K. Levendosky, A. Gettie, M.L. Cooney, J. Blanchard, J.A. Fernandez-Romero, T.M. Zydowsky, and N. Teleshova. Journal of Acquired Immune Deficiency Syndromes (1999), 2017. 74(3): p. e67-e74. PMID[27552154]. PMCID[PMC5303173].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28167548">Ga(III) Nanoparticles Inhibit Growth of Both Mycobacterium Tuberculosis and HIV and Release of Interleukin-6 (IL-6) and IL-8 in Coinfected Macrophages.</a> Choi, S.R., B.E. Britigan, and P. Narayanasamy. Antimicrobial Agents and Chemotherapy, 2017. 61(4): e02505-16. PMID[28167548].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27868445">Acetone and Methanol Fruit Extracts of Terminalia paniculata Inhibit HIV-1 Infection in Vitro.</a>Durge, A., P. Jadaun, A. Wadhwani, A.A. Chinchansure, M. Said, H.V. Thulasiram, S.P. Joshi, and S.S. Kulkarni. Natural Product Research, 2017. 31(12): p. 1468-1471. PMID[27868445].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28252110">Insights into the Activity of Maturation Inhibitor PF-46396 on HIV-1 Clade C.</a> Ghimire, D., U. Timilsina, T.P. Srivastava, and R. Gaur. Scientific Reports, 2017. 7(43711): 10pp. PMID[28252110]. PMCID[PMC5333120].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28134625">Autophagy Facilitates Macrophage Depots of Sustained-release Nanoformulated Antiretroviral Drugs.</a>Gnanadhas, D.P., P.K. Dash, B. Sillman, A.N. Bade, Z. Lin, D.L. Palandri, N. Gautam, Y. Alnouti, H.A. Gelbard, J. McMillan, R.L. Mosley, B. Edagwa, H.E. Gendelman, and S. Gorantla. The Journal of Clinical Investigation, 2017. 127(3): p. 857-873. PMID[28134625]. PMCID[PMC5330738].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28002064">Antibody Therapy Paired with Antiretroviral Therapy Suppresses Viral Load Long Term in SIV-macaques, Even after Treatment Stops.</a> Harper, K.N. AIDS, 2017. 31(4): p. N7-N8. PMID[28002064].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394325800023">Mangrove-mediated Green Synthesis of Silver Nanoparticles with High HIV-1 Reverse Transcriptase Inhibitory Potential.</a> Kumar, S.D., G. Singaravelu, S. Ajithkumar, K. Murugan, M. Nicoletti, and G. Benelli. Journal of Cluster Science, 2017. 28(1): p. 359-367. ISI[000394325800023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28176813">Combinational CRISPR/Cas9 Gene-editing Approach Can Halt HIV Replication and Prevent Viral Escape.</a> Lebbink, R.J., D.C.M. de Jong, F. Wolters, E.M. Kruse, P.M. van Ham, E.J.H.J. Wiertz, and M. Nijhuis. Scientific Reports, 2017. 7(41968). 10pp. PMID[28176813]. PMCID[PMC5296774].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28017762">Structure-Activity Relationship Studies on a Trp Dendrimer with Dual Activities against HIV and Enterovirus A71. Modifications on the Amino acid.</a>Martinez-Gualda, B., L. Sun, E. Rivero-Buceta, A. Flores, E. Quesada, J. Balzarini, S. Noppen, S. Liekens, D. Schols, J. Neyts, P. Leyssen, C. Mirabelli, M.J. Camarasa, and A. San-Felix. Antiviral Research, 2017. 139: p. 32-40. PMID[28017762].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27981421">Identification of Binding Mode and Prospective Structural Features of Novel Nef Protein Inhibitors as Potential anti-HIV Drugs.</a> Moonsamy, S., S. Bhakat, M. Ramesh, and M.E. Soliman. Cell Biochemistry and Biophysics, 2017. 75(1): p. 49-64. PMID[27981421].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28246360">Multiple Histone Lysine Methyltransferases Are Required for the Establishment and Maintenance of HIV-1 Latency.</a> Nguyen, K., B. Das, C. Dobrowolski, and J. Karn. mBio, 2017. 8(1): e00133-17. PMID[28246360]. PMCID[PMC5347344].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28289286">Early Antibody Therapy Can Induce Long-lasting Immunity to SHIV.</a> Nishimura, Y., R. Gautam, T.W. Chun, R. Sadjadpour, K.E. Foulds, M. Shingai, F. Klein, A. Gazumyan, J. Golijanin, M. Donaldson, O.K. Donau, R.J. Plishka, A. Buckler-White, M.S. Seaman, J.D. Lifson, R.A. Koup, A.S. Fauci, M.C. Nussenzweig, and M.A. Martin. Nature, 2017. 543(7646): p. 559-563. PMID[28289286].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28251988">Administration of Nucleoside-modified mRNA Encoding Broadly Neutralizing Antibody Protects Humanized Mice from HIV-1 Challenge.</a> Pardi, N., A.J. Secreto, X. Shan, F. Debonera, J. Glover, Y. Yi, H. Muramatsu, H. Ni, B.L. Mui, Y.K. Tam, F. Shaheen, R.G. Collman, K. Kariko, G.A. Danet-Desnoyers, T.D. Madden, M.J. Hope, and D. Weissman. Nature Communications, 2017. 8: p. 14630. PMID[28251988]. PMCID[PMC5337964].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395050500004">Macrocyclic Peptidomimetics Prepared by Ring-closing Metathesis and Azide-alkyne Cycloaddition.</a>Pehere, A.D., X. Zhang, and A.D. Abell. Australian Journal of Chemistry, 2017. 70(2): p. 138-151. ISI[000395050500004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28251988">Reassessing APOBEC3G Inhibition by HIV-1 Vif-Derived Peptides.</a> Richards, C.M., M. Li, A.L. Perkins, A. Rathore, D.A. Harki, and R.S. Harris. Journal of Molecular Biology, 2017. 429(1): p. 88-96. PMID[27887868]. PMCID[PMC5186364].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28288662">A Mutant Tat Protein Inhibits Infection of Human Cells by Strains from Diverse HIV-1 Subtypes.</a>Rustanti, L., H. Jin, M. Lor, M.H. Lin, D.J. Rawle, and D. Harrich. Virology Journal, 2017. 14(1): p. 52. PMID[28288662]. PMCID[PMC5348743].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28314269">Quantitative Structure-Cytotoxicity Relationship of Chalcones.</a> Sakagami, H., Y. Masuda, M. Tomomura, S. Yokose, Y. Uesawa, N. Ikezoe, D. Asahara, K. Takao, T. Kanamoto, S. Terakubo, H. Kagaya, H. Nakashima, and Y. Sugita. Anticancer Research, 2017. 37(3): p. 1091-1098. PMID[28314269].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395050500004">Novel Dioxolan Derivatives of Indole as HIV-1 Integrase Strand Transfer Inhibitors Active against RAL Resistant Mutant Virus.</a> Singh, R., P. Yadav, Urvashi, and V. Tandon. ChemistrySelect, 2016. 1(17): p. 5471-5478. ISI[000395425400012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000394093700154">Evaluation of Combination Long-acting Nanoformulated Antiretroviral Therapy in Early HIV-1 Infected Hupbl Nod.Cg-Prkdcscid Il2rgtm1wjl/Szj Mice.</a> Su, H., P. Dash, M. Arainga, B. Edagwa, J. McMillan, L. Poluektova, S. Gorantla, and H. Gendelman. Journal of Neurovirology, 2016. 22: p. S74-S75. ISI[000394093700154].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27795416">Creating an Artificial Tail Anchor as a Novel Strategy to Enhance the Potency of Peptide-based HIV Fusion Inhibitors.</a> Su, S., Y. Zhu, S. Ye, Q. Qi, S. Xia, Z. Ma, F. Yu, Q. Wang, R. Zhang, S. Jiang, and L. Lu. Journal of Virology, 2017. 91(1): e01445-16. PMID[27795416]. PMCID[PMC5165219].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28272311">Synthesis and Biological Activities of Ethyl 2-(2-Pyridylacetate) Derivatives Containing Thiourea, 1,2,4-Triazole, Thiadiazole and Oxadiazole Moieties.</a> Szulczyk, D., P. Tomaszewski, M. Jozwiak, A.E. Koziol, T. Lis, D. Collu, F. Iuliano, and M. Struga. Molecules, 2017. 22(409): 16pp. PMID[28272311].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28182989">6-Cyclohexylmethyl-3-hydroxypyrimidine-2,4-dione as an Inhibitor Scaffold of HIV Reverase Transcriptase: Impacts of the 3-OH on Inhibiting RNase H and Polymerase.</a> Tang, J., K.A. Kirby, A.D. Huber, M.C. Casey, J. Ji, D.J. Wilson, S.G. Sarafianos, and Z. Wang. European Journal of Medicinal Chemistry, 2017. 128: p. 168-179. PMID[28182989]. PMCID[PMC5384110].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27932583">Inhibition of HIV-1 Gag-membrane Interactions by Specific RNAs.</a> Todd, G.C., A. Duchon, J. Inlora, E.D. Olson, K. Musier-Forsyth, and A. Ono. RNA, 2017. 23(3): p. 395-405. PMID[27932583]. PMCID[PMC5311501].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28179531">Toll-like Receptor 7 Agonist GS-9620 Induces HIV Expression and HIV-specific Immunity in Cells from HIV-infected Individuals on Suppressive Antiretroviral Therapy.</a> Tsai, A., A. Irrinki, J. Kaur, T. Cihlar, G. Kukolj, D.D. Sloan, and J.P. Murry. Journal of Virology, 2017. 91(8):e02166-16 . PMID[28179531].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28169081">Conjugates of Phosphorylated Zalcitabine and Lamivudine with SiO2 Nanoparticles: Synthesis by CuAAC Click Chemistry and Preliminary Assessment of anti-HIV and Antiproliferative Activity.</a> Vasilyeva, S.V., A.A. Shtil, A.S. Petrova, S.M. Balakhnin, P.Y. Achigecheva, D.A. Stetsenko, and V.N. Silnikov. Bioorganic &amp; Medicinal Chemistry, 2017. 25(5): p. 1696-1702. PMID[28169081].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28321215">IFN-lambda Inhibits Drug-resistant HIV Infection of Macrophages.</a> Wang, X., H. Wang, M.Q. Liu, J.L. Li, R.H. Zhou, Y. Zhou, Y.Z. Wang, W. Zhou, and W.Z. Ho. Frontiers in Immunology, 2017. 8(210): 8pp. PMID[28321215]. PMCID[PMC5337814].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27986599">Development of a Novel Formulation That Improves Preclinical Bioavailability of Tenofovir Disoproxil Fumarate.</a> Watkins, M.E., S. Wring, R. Randolph, S. Park, K. Powell, L. Lutz, M. Nowakowski, R. Ramabhadran, and P.L. Domanico. Journal of Pharmaceutical Sciences, 2017. 106(3): p. 906-919. PMID[27986599]. PMCID[PMC5320394].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28117586">Griseofulvin Derivative and Indole Alkaloids from Penicillium griseofulvum CPCC 400528.</a> Zhang, D., L. Zhao, L. Wang, X. Fang, J. Zhao, X. Wang, L. Li, H. Liu, Y. Wei, X. You, S. Cen, and L. Yu. Journal of Natural Products, 2017. 80(2): p. 371-376. PMID[28117586].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28254696">Discovery of Uracil-bearing DAPYs Derivatives as Novel HIV-1 NNRTIS via Crystallographic Overlay-based Molecular Hybridization.</a> Zhang, H., Y. Tian, D. Kang, Z. Huo, Z. Zhou, H. Liu, E. De Clercq, C. Pannecouque, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2017. 130: p. 209-222. PMID[28254696].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_03_2017.</p>

    <br />

    <h2><a name="_Toc479675802">Patent Citations</a></h2>

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170302&amp;CC=WO&amp;NR=2017035127A1&amp;KC=A1">Substituted Phenylpyrrolecarboxamides with Therapeutic Activity in HIV Infection.</a> Debnath, A.K., F. Curreli, P.D. Kwong, and Y.D. Kwon. Patent. 2017. 2016-US48151 2017035127: 99pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025917A1&amp;KC=A1">5-(N-Benzyltetrahydroisoquinolin-6-yl)pyridin-3-ylacetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation.</a> Eastman, K.J., J.F. Kadow, K.E. Parcella, B.N. Naidu, T. Wang, Z. Yin, and Z. Zhang. Patent. 2017. 2016-IB54832 2017025917: 121pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170330&amp;CC=WO&amp;NR=2017053216A2&amp;KC=A2">Preparation of 4&#39;-Substituted Nucleosides and Nucleotide Triphosphates as Reverse Transcriptase Inhibitors.</a> Girijavallabhan, V.M., M. McLughlin, E. Cleator, J. Kong, A.W. Gibson, D.R. Lieberman, A. Dieguez Vazquez, S.P. Keen, M.J. Williams, J.C. Moore, E.M. Milczek, F. Peng, K.M. Belyk, and Z.J. Song. Patent. 2017. 2016-US52409 2017053216: 70pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">37. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025913A1&amp;KC=A1">Imidazopyridine Macrocycles as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation.</a> Kadow, J.F., B.N. Naidu, M. Patel, K. Peese, and Z. Wang. Patent. 2017. 2016-IB54827 2017025913: 111pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">38. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170223&amp;CC=WO&amp;NR=2017029631A1&amp;KC=A1">Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation.</a> Kadow, J.F., B.N. Naidu, and Y. Tu. Patent. 2017. 2016-IB54944 2017029631: 133pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">39. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025916A1&amp;KC=A1">5-(N-Aryltetrahydroisoquinolin-6-yl)pyridin-3-ylacetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation.</a> Kadow, J.F., B.N. Naidu, T. Wang, Z. Yin, and Z. Zhang. Patent. 2017. 2016-IB54831 2017025916: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">40. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025914A1&amp;KC=A1">5-(N-Substituted tetrahydroisoquinolin-6-yl)pyridin-3-ylacetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation.</a> Kadow, J.F., B.N. Naidu, T. Wang, Z. Yin, and Z. Zhang. Patent. 2017. 2016-IB54829 2017025914: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>

    <br />

    <p class="plaintext">41. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025901A1&amp;KC=A1">Preparation of Novel C28-Analogues with C3-Modifications of Triterpene Derivatives as HIV Inhibitors.</a> Reddy, B.P., K.R. Reddy, G.L.D. Krupadanam, A.P. Reddy, and M. Venkati. Patent. 2017. 2016-IB54804 2017025901: 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_03_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
