

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6sUUw28uPgWGqR8SR2aOdoiZg3bfFK4328zp5a07t0fpzLt9UPoLb2VOw2lLl5+i9U2ZIApmmAZslWw2vz6pctMgvi8vO+ikgn1rwTQ6t2tMj+j/2rF4ft0jBSE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F643A1B3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: October, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27721488">CD4-mimetic Sulfopeptide Conjugates Display Sub-nanomolar anti-HIV-1 Activity and Protect Macaques against a SHIV162P3 Vaginal Challenge.</a> Arien, K.K., F. Baleux, D. Desjardins, F. Porrot, Y.M. Coic, J. Michiels, K. Bouchemal, D. Bonnaffe, T. Bruel, O. Schwartz, R. Le Grand, G. Vanham, N. Dereuddre-Bosquet, and H. Lortat-Jacob. Scientific Reports, 2016. 6(34829): 13pp. PMID[27721488]. PMCID[PMC5056392].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">2. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27240277">Enfuvirtide-peg Conjugate: A Potent HIV Fusion Inhibitor with Improved Pharmacokinetic Properties.</a> Cheng, S., Y. Wang, Z. Zhang, X. Lv, G.F. Gao, Y. Shao, L. Ma, and X. Li. European Journal of Medicinal Chemistry, 2016. 121: p. 232-237. PMID[27240277].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">3. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27788205">A Cinnamon-derived Procyanidin Compound Displays anti-HIV-1 Activity by Blocking Heparan Sulfate- and Co-receptor-binding Sites on gp120 and Reverses T Cell Exhaustion via Impeding Tim-3 and PD-1 Upregulation.</a> Connell, B.J., S.Y. Chang, E. Prakash, R. Yousfi, V. Mohan, W. Posch, D. Wilflingseder, C. Moog, E.N. Kodama, P. Clayette, and H. Lortat-Jacob. Plos One, 2016. 11(10): p. e0165386. PMID[27788205]. PMCID[PMC5082894].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">4. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27253715">An Evolved RNA Recognition Motif That Suppresses HIV-1 Tat/Tar-dependent Transcription.</a> Crawford, D.W., B.D. Blakeley, P.H. Chen, C. Sherpa, S.F.J. Le Grice, I.A. Laird-Offringa, and B.R. McNaughton. ACS Chemical Biology, 2016. 11(8): p. 2206-2215. PMID[27253715].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">5. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27568924">Preclinical Evaluation of a Mercaptobenzamide and Its Prodrug for NCp7-Targeted Inhibition of Human Immunodeficiency Virus.</a> Hartman, T.L., L. Yang, A.N. Helfrick, M. Hassink, N.I. Shank, K. George Rosenker, M.T. Scerba, M. Saha, E. Hughes, A.Q. Wang, X. Xu, P. Gupta, R.W. Buckheit, Jr., and D.H. Appella. Antiviral Research, 2016. 134: p. 216-225. PMID[27568924].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">6. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27475281">Sulfotyrosine Dipeptide: Synthesis and Evaluation as HIV-Entry Inhibitor.</a> Ju, T., D. Hu, S.H. Xiang, and J. Guo. Bioorganic Chemistry, 2016. 68: p. 105-111. PMID[27475281].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">7. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27631702">Identification of Interferon-stimulated Genes with Antiretroviral Activity.</a> Kane, M., T.M. Zang, S.J. Rihn, F.W. Zhang, T. Kueck, M. Alim, J. Schoggins, C.M. Rice, S.J. Wilson, and P.D. Bieniasz. Cell Host &amp; Microbe, 2016. 20(3): p. 392-405. PMID[27631702]. PMCID[PMC5026698].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">8. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27776523">In Vitro anti-HIV and Antioxidant Activity of Hoodia gordonii (Apocynaceae), a Commercial Plant Product.</a> Kapewangolo, P., M. Knott, R.E. Shithigona, S.L. Uusiku, and M. Kandawa-Schulz. BMC Complementary and Alternative Medicine, 2016. 16(1): p. 411. PMID[27776523]. PMCID[PMC5078957].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000383294500021">Sceletium tortuosum Demonstrates in Vitro anti-HIV and Free Radical Scavenging Activity.</a> Kapewangolo, P., T. Tawha, T. Nawinda, M. Knott, and R. Hans. South African Journal of Botany, 2016. 106: p. 140-143. ISI[000383294500021].
    <br />
    <b>[WOS]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">10. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27734899">Soybean-derived Bowman-Birk Inhibitor (BBI) Inhibits HIV Replication in Macrophages.</a> Ma, T.C., R.H. Zhou, X. Wang, J.L. Li, M. Sang, L. Zhou, K. Zhuang, W. Hou, D.Y. Guo, and W.Z. Ho. Scientific Reports, 2016. 6(34752): 10pp. PMID[27734899]. PMCID[PMC5062087].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">11. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27415449">Investigational Protease Inhibitors as Antiretroviral Therapies.</a> Midde, N.M., B.J. Patters, P. Rao, T.J. Cory, and S. Kumar. Expert Opinion on Investigational Drugs, 2016. 25(10): p. 1189-1200. PMID[27415449].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384256800009">Synthetic alpha-Hydroxytropolones as Inhibitors of HIV Reverse Transcriptase Ribonuclease H Activity.</a> Murelli, R.P., M.P. D&#39;Erasmo, D.R. Hirsch, C. Meck, T. Masaoka, J.A. Wilson, B.F. Zhang, R.K. Pal, E. Gallicchio, J.A. Beutlere, and S.F.J. Le Grice. MedChemComm, 2016. 7(9): p. 1783-1788. ISI[000384256800009].
    <br />
    <b>[WOS]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">13. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27556447">Article Inhibitory Effect of 2,3,5,6-Tetrafluoro-4-[4-(aryl)-1H-1,2,3-triazol-1-yl]benzenesulfonamide Derivatives on HIV Reverse Transcriptase Associated RNase H Activities.</a> Pala, N., F. Esposito, D. Rogolino, M. Carcelli, V. Sanna, M. Palomba, L. Naesens, A. Corona, N. Grandi, E. Tramontano, and M. Sechi. International Journal of Molecular Sciences, 2016. 17(8): E1371. PMID[27556447]. PMCID[PMC5000766].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">14. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27568085">Indole-based Allosteric Inhibitors of HIV-1 Integrase.</a> Patel, P.A., N. Kvaratskhelia, Y. Mansour, J. Antwi, L. Feng, P. Koneru, M.J. Kobe, N. Jena, G. Shi, M.S. Mohamed, C. Li, J.J. Kessl, and J.R. Fuchs. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(19): p. 4748-4752. PMID[27568085]. PMCID[PMC5018460].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">15. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27603759">Asymmetric Total Syntheses of Aetheramides and Their Stereoisomers: Stereochemical Assignment of Aetheramides.</a> Qi, N., S.R. Allu, Z.L. Wang, Q. Liu, J. Guo, and Y. He. Organic Letters, 2016. 18(18): p. 4718-4721. PMID[27603759].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000383786900002">X-ray Crystal and DFT Study of a Potent anti-HIV-1 Agent: 2-(5,5-Dioxido-3-phenylpyrazolo[4,3-c][1,2]benzothiazin-4(2H)-yl)-N&#39;-(3-nitrophenyl)methylidene acetohydrazide.</a> Saif, M.J., M. Ahmad, and N. Idrees. Journal of Theoretical &amp; Computational Chemistry, 2016. 15(1650038): 11pp. ISI[000383786900002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">17. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27371922">Novel (2,6-Difluorophenyl)(2-(phenylamino)pyrimidin-4-yl)methanones with Restricted Conformation as Potent Non-Nucleoside Reverse Transcriptase Inhibitors against HIV-1.</a> Simon, P., O. Baszczynski, D. Saman, G. Stepan, E. Hu, E.B. Lansdon, P. Jansa, and Z. Janeba. European Journal of Medicinal Chemistry, 2016. 122: p. 185-195. PMID[27371922].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000384023400008">A Divergent Approach for the Synthesis of D- and L-4&#39;-Ethynyl dioxolane Nucleosides with Potent anti-HIV Activity.</a> Singh, S., V. Gajulapati, M. Kim, J.I. Goo, J.K. Lee, K. Lee, C.K. Lee, L.S. Jeong, and Y. Choi. Synthesis, 2016. 48(18): p. 3050-3056. ISI[000384023400008].
    <br />
    <b>[WOS]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">19. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27634404">A Triazinone Derivative Inhibits HIV-1 Replication by Interfering with Reverse Transcriptase Activity.</a> Urano, E., K. Miyauchi, Y. Kojima, M. Hamatake, S.D. Ablan, S. Fudo, E.O. Freed, T. Hoshino, and J. Komano. ChemMedChem, 2016. 11(20): p. 2320-2326. PMID[27634404].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">20. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27575477">Quassinoids: Viral Protein R Inhibitors from Picrasma javanica Bark Collected in Myanmar for HIV Infection.</a> Win, N.N., T. Ito, Y.Y. Win, H. Ngwe, T. Kodama, I. Abe, and H. Morita. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(19): p. 4620-4624. PMID[27575477].
    <br />
    <b>[PubMed]</b>. HIV_10_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">21. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160922&amp;CC=WO&amp;NR=2016147099A2&amp;KC=A2">Preparation of Novel C-3 Triterpenones with C-28 Amide Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, G.L.D. Krupadanam, P.R. Adulla, E.R. Bammidi, B.R. Kasireddy, and S. Neela. Patent. 2016. 2016-IB51424 2016147099: 86pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">22. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161013&amp;CC=WO&amp;NR=2016162405A1&amp;KC=A1">Pyrone Derivatives for Use as Antiviral Agents.</a> Brack-Werner, R., M. Helfer, M. Roesner, M. Schneider, U. Protzer, C. Hertweck, and M. Werneburg. Patent. 2016. 2016-EP57598 2016162405: 109pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">23. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161006&amp;CC=WO&amp;NR=2016161382A1&amp;KC=A1">Preparation of Polycyclic-carbamoylpyridone Compounds and Their Pharmaceutical Use.</a> Cai, Z.R., H. Jin, S.E. Lazerwith, and H.-J. Pyun. Patent. 2016. 2016-US25740 2016161382: 120pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_10_2016.</p>
    
    <br />
 
    <p class="plaintext">24. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161006&amp;CC=WO&amp;NR=2016156888A1&amp;KC=A1">Preparation of a Coumarin Derivative as an Antiviral Agent, Pharmaceutical Composition Thereof and Its Use.</a> Trkovnik, M., M. Cacic, and J. Rizvani. Patent. 2016. 2016-HR5 2016156888: 60pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_10_2016.</p>

    <br />

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160929&amp;CC=WO&amp;NR=2016154527A1&amp;KC=A1">Phosphate-substituted Quinolizine Derivatives Useful as HIV Integrase Inhibitors and Their Preparation.</a> Yu, T., S.T. Waddell, J.A. McCauley, T.H. Graham, H. Li, I. Raheem, and J.A. Grobler. Patent. 2016. 2016-US24193 2016154527: 127pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_10_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
