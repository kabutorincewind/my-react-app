

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-02-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="t7WBltMyS63IL3Tz28hMuyg6BVgIivoayp9Vd1zoAPCcQ6+cNJAtNMjddW1Rj5HHQERJ1wHEVEKEwjlwIFG5ZBr31AOaFinB7wG6h+rCqlYRKiKrwhUZqbVHgt8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7F339A1F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: February 1 - February 14, 2013</h1>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23405147">Tricyclic Benzo[cd]azulenes Selectively Inhibit Activities of PIM Kinases and Restrict Growth of Epstein-Barr Virus-transformed Cells</a><i>.</i> Kiriazis, A., R.L. Vahakoski, N.M. Santio, R. Arnaudova, S.K. Eerola, E.M. Rainio, I.B. Aumuller, J. Yli-Kauhaluoma, and P.J. Koskinen. Plos One, 2013. 8(2): p. e55409; PMID[23405147].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23266293">Ezetimibe Blocks Hepatitis B Virus Infection after Virus Uptake into Hepatocytes</a><i>.</i> Lucifora, J., K. Esser, and U. Protzer. Antiviral Research, 2013. 97(2): p. 195-197; PMID[23266293].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23397077">Controllable Inhibition of Hepatitis B Virus Replication by a DR1-targeting Short Hairpin RNA (shRNA) Expressed from a DOX-inducible Lentiviral Vector</a><i>.</i> Wang, W., H. Peng, J. Li, X. Zhao, F. Zhao, and K. Hu. Virus Genes, 2013.  <b>[Epub ahead of print]</b>; PMID[23397077].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23183437">Preclinical Characterization of GS-9669, a Thumb Site II Inhibitor of the Hepatitis C Virus NS5B Polymerase</a><i>.</i> Fenaux, M., S. Eng, S.A. Leavitt, Y.J. Lee, E.M. Mabery, Y. Tian, D. Byun, E. Canales, M.O. Clarke, E. Doerffler, S.E. Lazerwith, W. Lew, Q. Liu, M. Mertzman, P. Morganelli, L. Xu, H. Ye, J. Zhang, M. Matles, B.P. Murray, J. Mwangi, J. Zhang, A. Hashash, S.H. Krawczyk, A.M. Bidgood, T.C. Appleby, and W.J. Watkins. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 804-810; PMID[23183437].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23320525">Myriberine A, a New Alkaloid with an Unprecedented Heteropentacyclic Skeleton from Myrioneuron faberi</a><i>.</i> Huang, S.D., Y. Zhang, M.M. Cao, Y.T. Di, G.H. Tang, Z.G. Peng, J.D. Jiang, H.P. He, and X.J. Hao. Organic Letters, 2013. 15(3): p. 590-593; PMID[23320525].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23165461">Resistance Studies of a Dithiazol Analogue, DBPR110, as a Potential Hepatitis C Virus NS5A Inhibitor in Replicon Systems</a><i>.</i> Lin, H.M., J.C. Wang, H.S. Hu, P.S. Wu, W.H. Wang, S.Y. Wu, C.C. Yang, T.K. Yeh, T.A. Hsu, W.T. Jiaang, Y.S. Chao, J.H. Chern, and A. Yueh. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 723-733; PMID[23165461].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23175359">Screening and Rational Design of Hepatitis C Virus Entry Inhibitory Peptides Derived from GB Virus A NS5A</a><i>.</i> Liu, X., Y. Huang, M. Cheng, L. Pan, Y. Si, G. Li, Y. Niu, L. Zhao, J. Zhao, X. Li, Y. Chen, and W. Yang. Journal of Virology, 2013. 87(3): p. 1649-1657; PMID[23175359].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23273521">HCV NS5A Replication Complex Inhibitors. Part 3: Discovery of Potent Analogs with Distinct Core Topologies</a><i>.</i> Lopez, O.D., V.N. Nguyen, D.R. St Laurent, M. Belema, M.H. Serrano-Wu, J.T. Goodrich, F. Yang, Y. Qiu, A.S. Ripka, P.T. Nower, L. Valera, M. Liu, D.R. O&#39;Boyle, 2nd, J.H. Sun, R.A. Fridell, J.A. Lemm, M. Gao, A.C. Good, N.A. Meanwell, and L.B. Snyder. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(3): p. 779-784; PMID[23273521].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23377187">Novel Anilinocoumarin Derivatives as Agents against Hepatitis C Virus by the Induction of IFN-Mediated Antiviral Responses</a><i>.</i> Peng, H.K., W.C. Chen, J.C. Lee, S.Y. Yang, C.C. Tzeng, Y.T. Lin, and S.C. Yang. Organic &amp; Biomolecular Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23377187].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23177287">HCV NS5A Inhibitors in Development</a><i>.</i> Suk-Fong Lok, A. Clinics in Liver Disease, 2013. 17(1): p. 111-121; PMID[23177287].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22996292">IFITM1 Is a Tight Junction Protein That Inhibits Hepatitis C Virus Entry</a><i>.</i> Wilkins, C., J. Woodward, D.T. Lau, A. Barnes, M. Joyce, N. McFarlane, J.A. McKeating, D.L. Tyrrell, and M. Gale, Jr. Hepatology, 2013. 57(2): p. 461-469; PMID[22996292].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23392633">Antiherpetic Potential of 6-Bromoindirubin-3&#39;-acetoxime (BIO-acetoxime) in Human Oral Epithelial Cells</a><i>.</i> Hsu, M.J. and S.L. Hung. Archives of Virology, 2013.  <b>[Epub ahead of print]</b>; PMID[23392633].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23341013">A Cationic Peptide, TAT-CD0, Inhibits Herpes Simplex Virus Type 1 Ocular Infection in Vivo</a><i>.</i> Jose, G.G., I.V. Larsen, J. Gauger, E. Carballo, R. Stern, R. Brummel, and C.R. Brandt. Investigative Ophthalmology &amp; Visual Science, 2013. 54(2): p. 1070-1079; PMID[23341013].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23386436">A Novel Selective LSD1/KDM1A Inhibitor Epigenetically Blocks Herpes Simplex Virus Lytic Replication and Reactivation from Latency</a><i>.</i> Liang, Y., D. Quenelle, J.L. Vogel, C. Mascaro, A. Ortega, and T.M. Kristie. mBio, 2013. 4(1); PMID[23386436].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <h2>SIV</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23387294">Effect of Combination Antiretroviral Therapy on Chinese Rhesus Macaques of SIV Infection</a><i>.</i> Ling, B., L.B. Rogers, A.M. Johnson, M. Piatak, J. Lifson, and R. Veazey. AIDS Research and Human Retroviruses, 2013.  <b>[Epub ahead of print]</b>; PMID[23387294].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0201-021413.</p>

    <h2> </h2>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000312416200005.">Hepatitis C Virus Protease Inhibitors</a><i>.</i> Abdel-Magid, A.F. ACS Medicinal Chemistry Letters, 2012. 3(9): p. 699-700; ISI[000312416200005].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000312692100006">Inhibition on Hepatitis B Virus in Vitro of Lectin from Musca domestica Pupa Via the Activation of NF-Kappa B</a><i>.</i> Cao, X.H., M.H. Zhou, S.X. Wang, C.L. Wang, L.H. Hou, Y.Q. Luo, and L.Y. Chen. Virus Research, 2012. 170(1-2): p. 53-58; ISI[000312692100006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000313095100013">Hydrazonoyl Halides as Precursors for Synthesis of Novel Bioactive Thiazole and Formazan Derivatives</a><i>.</i> Farghaly, T.A. and E.M.H. Abbas. Journal of Chemical Research, 2012(11): p. 660-664; ISI[000313095100013].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000312745100025">Discovery of New Scaffolds for Rational Design of HCV NS5B Polymerase Inhibitors</a><i>.</i> Golub, A.G., K.R. Gurukumar, A. Basu, V.G. Bdzhola, Y. Bilokin, S.M. Yarmoluk, J.C. Lee, T.T. Talele, D.B. Nichols, and N. Kaushik-Basu. European Journal of Medicinal Chemistry, 2012. 58: p. 258-264; ISI[000312745100025].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000313362800019">Chemo-enzymatic Synthesis and Biological Evaluation of 5,6-Disubstituted Benzimidazole Ribo- and 2 &#39;-Deoxyribonucleosides</a><i>.</i> Konstantinova, I.D., O.M. Selezneva, I.V. Fateev, T.A. Balashova, S.K. Kotovskaya, Z.M. Baskakova, V.N. Charushin, A.V. Baranovsky, A.I. Miroshnikov, J. Balzarini, and I.A. Mikhailopulo. Synthesis-Stuttgart, 2013. 45(2): p. 272-280; ISI[000313362800019].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000313492600004">RNA Aptamer-mediated Interference of HCV Replication by Targeting the CRE-5BSl3.2 Domain</a><i>.</i> Marton, S., C. Romero-Lopez, and A. Berzal-Herranz. Journal of Viral Hepatitis, 2013. 20(2): p. 103-112; ISI[000313492600004].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0201-021413.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
