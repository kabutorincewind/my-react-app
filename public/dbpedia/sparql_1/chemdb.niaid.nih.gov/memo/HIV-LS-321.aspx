

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-321.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yWx/UzLz+KyIIZlkTWL1Xo2HrqyWBb3eFX3JHf3tKZODarLGmsyx/xG56N506rV2AuWsFrbgiPrCtMZP1OC9GE5C/nj8vSi72wE84elzKmygwkLoZnAaBnFR5RY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A93A3D34" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-321-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45215   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Discovery of Small-Molecule Human Immunodeficiency Virus Type 1 Entry Inhibitors That Target the gp120-Binding Domain of CD4</p>

    <p>          Yang, QE, Stephen, AG, Adelsberger, JW , Roberts, PE, Zhu, W, Currens, MJ, Feng, Y, Crise, BJ, Gorelick, RJ, Rein, AR, Fisher, RJ, Shoemaker, RH, and Sei, S</p>

    <p>          J Virol <b>2005</b> .  79(10): 6122-6133</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857997&amp;dopt=abstract</a> </p><br />

    <p>2.     45216   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Anti-HIV Protease Activity from Rosa Family Plant Extracts and Rosamultin from Rosa rugosa</p>

    <p>          Park, JC, Kim, SC, Choi, MR, Song, SH, Yoo, EJ, Kim, SH,  Miyashiro, H, and Hattori, M</p>

    <p>          J Med Food <b>2005</b>.  8(1): 107-109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857219&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857219&amp;dopt=abstract</a> </p><br />

    <p>3.     45217   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Targeting tn5 transposase identifies human immunodeficiency virus type 1 inhibitors</p>

    <p>          Ason, B, Knauss, DJ, Balke, AM, Merkel, G, Skalka, AM, and Reznikoff, WS</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(5): 2035-2043</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855529&amp;dopt=abstract</a> </p><br />

    <p>4.     45218   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of 2-aminobenzimidazoles for the treatment of HIV/AIDS</p>

    <p>          Schohe-Loop, Rudolf, Paessens, Arnold, Bauser, Marcus, Koebberling, Johannes, Dittmer, Frank, Henninger, Kerstin, Lang, Dieter, and Paulsen, Daniela</p>

    <p>          PATENT:  WO <b>2005019186</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2004  PP: 59 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     45219   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Novel human immunodeficiency virus type 1 protease mutations potentially involved in resistance to protease inhibitors</p>

    <p>          Svicher, V, Ceccherini-Silberstein, F, Erba, F, Santoro, M, Gori, C, Bellocchi, MC, Giannella, S, Trotta, MP, Monforte, A, Antinori, A, and Perno, CF</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(5): 2015-2025</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855527&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     45220   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Mechanism of Anti-Human Immunodeficiency Virus Activity of {beta}-D-6-Cyclopropylamino-2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-Dideoxyguanosine</p>

    <p>          Ray, AS, Hernandez-Santiago, BI, Mathew, JS, Murakami, E, Bozeman, C, Xie, MY, Dutschman, GE, Gullen, E, Yang, Z, Hurwitz, S, Cheng, YC, Chu, CK, McClure, H, Schinazi, RF, and Anderson, KS</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(5): 1994-2001</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855524&amp;dopt=abstract</a> </p><br />

    <p>7.     45221   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of 5-membered heterocycles, in particular pyrazoles and imidazoles, as p38 kinase inhibitors</p>

    <p>          Fryszman, Olga M, Lang, Hengyuan, Lan, Jiong, Chang, Edcon, and Fang, Yunfeng</p>

    <p>          PATENT:  WO <b>2005009973</b>  ISSUE DATE:  20050203</p>

    <p>          APPLICATION: 2004  PP: 197 pp.</p>

    <p>          ASSIGNEE:  (Triad Therapeuctics, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     45222   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Evaluation of selected South African medicinal plants for inhibitory properties against human immunodeficiency virus type 1 reverse transcriptase and integrase</p>

    <p>          Bessong, PO, Obi, CL, Andreola, ML, Rojas, LB, Pouysegu, L, Igumbor, E, Meyer, JJ, Quideau, S, and Litvak, S</p>

    <p>          J Ethnopharmacol <b>2005</b>.  99(1): 83-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848024&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848024&amp;dopt=abstract</a> </p><br />

    <p>9.     45223   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Limited development and progression of resistance of HIV-1 to the nucleoside analogue reverse transcriptase inhibitor lamivudine in human primary macrophages</p>

    <p>          Aquaro, S, Svicher, V, Ceccherini-Silberstein, F, Cenci, A, Marcuccilli, F, Giannella, S, Marcon, L, Calio, R, Balzarini, J, and Perno, CF</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845785&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845785&amp;dopt=abstract</a> </p><br />

    <p>10.   45224   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by caffeine and caffeine-related methylxanthines</p>

    <p>          Nunnari, G, Argyris, E, Fang, J, Mehlman, KE, Pomerantz, RJ, and Daniel, R</p>

    <p>          Virology <b>2005</b>.  335(2): 177-184</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15840517&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15840517&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45225   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Oximinoarylsulfonamides as potent HIV protease inhibitors</p>

    <p>          Yeung, CM, Klein, LL, Flentge, CA, Randolph, JT, Zhao, C, Sun, M, Dekhtyar, T, Stoll, VS, and Kempf, DJ</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(9): 2275-2278</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15837308&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15837308&amp;dopt=abstract</a> </p><br />

    <p>12.   45226   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Suppression of HIV-1 infection by a small molecule inhibitor of the ATM kinase</p>

    <p>          Lau, A, Swinbank, KM, Ahmed, PS, Taylor, DL, Jackson, SP, Smith, GC, and O&#39;connor, MJ</p>

    <p>          Nat Cell Biol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15834407&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15834407&amp;dopt=abstract</a> </p><br />

    <p>13.   45227   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 gene expression by retroviral vector-mediated small-guide RNAs that direct specific RNA cleavage by tRNase ZL</p>

    <p>          Habu, Yuichiro, Miyano-Kurosaki, Naoko, Kitano, Michiko, Endo, Yumihiko, Yukita, Masakazu, Ohira, Shigeru, Takaku, Hiroaki, Nashimoto, Masayuki, and Takaku, Hiroshi</p>

    <p>          Nucleic Acids Research <b>2005</b>.  33(1): 235-243</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   45228   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          THE AMINO ACID N136 IN HIV-1 REVERSE TRANSCRIPTASE (RT) MAINTAINS EFFICIENT ASSOCIATION OF BOTH RT SUBUNITS AND ENABLES THE RATIONAL DESIGN OF NOVEL RT INHIBITORS</p>

    <p>          Balzarini, J,  Auxerx, J, Rodriguez-Barrios, F, Chedad, A, Farkas, V, Ceccherini-Silberstein, F, Garcia-Aparicio, C, Velazquez, S, De Clercq, E, Perno, CF, Camarasa, MJ, and Gago, F</p>

    <p>          Mol Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833734&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833734&amp;dopt=abstract</a> </p><br />

    <p>15.   45229   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Essential regions for antiviral activities of actinohivin, a sugar-binding anti-human immunodeficiency virus protein from an actinomycete</p>

    <p>          Takahashi, Atsushi, Inokoshi, Junji, Chiba, Harumi, Omura, Satoshi, and Tanaka, Haruo</p>

    <p>          Archives of Biochemistry and Biophysics <b>2005</b>.  437(2): 233-240</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   45230   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Dihydropyrimidinyl and other heterocyclic compound dipeptidyl peptidase IV (DPPIV) inhibitors</p>

    <p>          Cao, Sheldon X, Feng, Jun, Gwaltney, Stephen L, Kaldor, Stephen W, Stafford, Jeffrey A, Wallace, Michael B, Xiao, Xiao-Yi, and Zhang, Zhiyuan</p>

    <p>          PATENT:  WO <b>2005026148</b>  ISSUE DATE:  20050324</p>

    <p>          APPLICATION: 2004  PP: 161 pp.</p>

    <p>          ASSIGNEE:  (Syrrx, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   45231   HIV-LS-321; PUBMED-HIV-5/2/2005</p>

    <p class="memofmt1-2">          Blockade of chemokine-induced signalling inhibits CCR5-dependent HIV infection in vitro without blocking gp120/CCR5 interaction</p>

    <p>          Grainger, DJ and Lever, AM</p>

    <p>          Retrovirology <b>2005</b>.  2(1): 23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15807900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15807900&amp;dopt=abstract</a> </p><br />

    <p>18.   45232   HIV-LS-321; WOS-HIV-4/24/2005</p>

    <p class="memofmt1-2">          4D-QSAR models of HOE/BAY-793 analogues as HIV-1 protease inhihitors</p>

    <p>          da Cunha, EFF, Albuquerque, MG, Antunes, OAC, and de Alencastro, RB</p>

    <p>          QSAR &amp; COMBINATORIAL SCIENCE <b>2005</b>.  24(2): 240-253, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228251600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228251600006</a> </p><br />

    <p>19.   45233   HIV-LS-321; WOS-HIV-4/24/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 2 &#39;,3 &#39;-didehydro-2 &#39;, 3 &#39;-dideoxy-9-deazaguanosine, a monophosphate prodrug and two analogues, 2 &#39;,3 &#39;-dideoxy-9-deazaguanosine and 2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy-9-deazainosine</p>

    <p>          Liu, MC, Luo, MZ, Mozdziesz, DE, Dutschman, GE, Gullen, EA, Cheng, YC, and Sartorelli, AC</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(2): 135-145, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228077200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228077200005</a> </p><br />

    <p>20.   45234   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Embrey, Mark W, Wai, John S, Funk, Timothy W, Homnick, Carl F, Perlow, Debbie S, Young, Steven D, Vacca, Joseph P, Hazuda, Daria J, Felock, Peter J, Stillmock, Kara A, Witmer, Marc V, Moyer, Gregory, Schleif, William A, Gabryelski, Lori J, Jin, Lixia, Chen, I-Wu, Ellis, Joan D, Wong, Bradley K, Lin, Jiunn H, Leonard, Yvonne M, Tsou, Nancy N, and Zhuang, Linghang</p>

    <p>          &lt;10 Journal Title&gt; <b>2005</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   45235   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Polypeptide multimers comprising coiled coil structure and a-helical structure for use as antiviral, anti-HIV and anti-SIV agents</p>

    <p>          Weiss, Carol D, He, Yong, Vassell, Russell, and De, Rosny Eve</p>

    <p>          PATENT:  WO <b>2005018666</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2003  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (The Government of the United States of America, as Represented by the Department of Health and Human Services USA and De Rosny, Eve</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   45236   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of phenanthridine derivatives as anti-viral agents</p>

    <p>          Tor, Yitzhak and Luedtke, Nathan</p>

    <p>          PATENT:  WO <b>2005016343</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   45237   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Ex Vivo Modeling of the Effects of Mycophenolic Acid on HIV Infection: Considerations for Antiviral Therapy</p>

    <p>          Kaur, Rupinderjeet, Klichko, Vladimir, and Margolis, David</p>

    <p>          AIDS Research and Human Retroviruses <b>2005</b>.  21(2): 116-124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   45238   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          A preparation of antiviral and antibacterial derivatives of dithioxopyranodipyrimidine and their 10-aza-analogs</p>

    <p>          Tets, VV, Krasnov, KA, and Ashkinazi, RI</p>

    <p>          PATENT:  RU <b>2246496</b>  ISSUE DATE: 20050220</p>

    <p>          APPLICATION: 2003-62807  PP: No pp. given</p>

    <p>          ASSIGNEE:  (Russia)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   45239   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of nucleobase phosphonate analogs for antiviral treatment and as retroviral reverse transcriptase inhibitors</p>

    <p>          Krawczyk, Steven H</p>

    <p>          PATENT:  WO <b>2005012324</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 140 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   45240   HIV-LS-321; SCIFINDER-HIV-4/25/2005</p>

    <p class="memofmt1-2">          Trimeric Membrane-anchored gp41 Inhibits HIV Membrane Fusion</p>

    <p>          Lenz, Oliver, Dittmar, Matthias T, Wagner, Andreas, Ferko, Boris, Vorauer-Uhl, Karola, Stiegler, Gabriela, and Weissenhorn, Winfried</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(6): 4095-4101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   45241   HIV-LS-321; WOS-HIV-4/24/2005</p>

    <p class="memofmt1-2">          Amino acid derivatives, part 2: Synthesis, antiviral, and antitumor activity of simple protected amino acids functionalized at N-terminus with naphthalene side chain</p>

    <p>          Ali, IAI, Al-Masoudi, IA, Saeed, B, Al-Masoudi, NA, and La Colla, P</p>

    <p>          HETEROATOM CHEMISTRY <b>2005</b>.  16(2): 148-155, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228057400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228057400008</a> </p><br />

    <p>28.   45242   HIV-LS-321; WOS-HIV-4/24/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of some no vel isatin derivatives</p>

    <p>          Selvam, P, Rajasekaran, A, Dharamsi, A, Ahmed, KL, Musthafa, SM, Poornima, K, Pournami, KA, Murugesh, N, Chandramohan, M, and De, Clercq E</p>

    <p>          ASIAN JOURNAL OF CHEMISTRY <b>2005</b>.  17(1): 443-448, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228191800064">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228191800064</a> </p><br />

    <p>29.   45243   HIV-LS-321; WOS-HIV-4/24/2005</p>

    <p class="memofmt1-2">          Antiviral activity of serum from the American alligator (Alligator mississippiensis)</p>

    <p>          Merchant, ME, Pallansch, M, Paulman, RL, Wells, JB, Nalca, A, and Ptak, R</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  66(1): 35-38, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800006</a> </p><br />

    <p>30.   45244   HIV-LS-321; WOS-HIV-5/1/2005</p>

    <p class="memofmt1-2">          Synthesis of 1-[2-hydroxy thoxy)methyl]-6-(5,6,7,8-tetrahydronaphthylmethyl-1) thymine as novel inhibitor against drug-resistant HIV mutants</p>

    <p>          Meng, G, Kuang, YY, Ji, L, and Chen, FE</p>

    <p>          SYNTHETIC COMMUNICATIONS <b>2005</b>.  35(8): 1095-1102, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228451200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228451200012</a> </p><br />

    <p>31.   45245   HIV-LS-321; WOS-HIV-5/1/2005</p>

    <p class="memofmt1-2">          Marked depletion of glycosylation sites in HIV-1 gp120 under selection pressure by the mannose-specific plant lectins of Hippeastrum hybrid and Galanthus nivalis</p>

    <p>          Balzarini, J,  Van Laethem, K, Hatse, S , Froeyen, M, Van Damme, E, Bolmstedt, A, Peumans, W, De Clercq, E, and Schols, D</p>

    <p>          MOLECULAR PHARMACOLOGY <b>2005</b>.  67(5): 1556-1565, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228387900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228387900022</a> </p><br />

    <p>32.   45246   HIV-LS-321; WOS-HIV-5/1/2005</p>

    <p class="memofmt1-2">          ATP gamma S disrupts human immunodeficiency virus type 1 virion core integrity</p>

    <p>          Gurer, C, Hoglund, A, Hoglund, S, and Luban, J</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(9): 5557-5567, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228433100033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228433100033</a> </p><br />

    <p>33.   45247   HIV-LS-321; WOS-HIV-5/1/2005</p>

    <p class="memofmt1-2">          Macrophage inflammatory protein 1 alpha inhibits postentry steps of human immunodeficiency virus type 1 infection via suppression of intracellular cyclic AMP</p>

    <p>          Amella, CA, Sherry, B, Shepp, DH, and Schmidtmayerova, H</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(9): 5625-5631, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228433100040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228433100040</a> </p><br />

    <p>34.   45248   HIV-LS-321; WOS-HIV-5/1/2005</p>

    <p class="memofmt1-2">          Some reactions of 3-chloroisoindolium salts with nucleophiles: access to isoindole derivatives and ellipticine analogues as potential antiviral agents</p>

    <p>          Hamed, AA</p>

    <p>          JOURNAL OF CHEMICAL RESEARCH-S <b>2005</b>.(1): 54-58, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228312800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228312800016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
