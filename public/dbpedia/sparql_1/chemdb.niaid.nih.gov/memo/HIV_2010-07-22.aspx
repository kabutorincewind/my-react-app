

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-07-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JC5Zi1ng1RJbp2PUJDcWEub6KBhh/87DowxWzscjKdBs2+jJFO38BhUhfkVz0RAafTRFPnoI6nNuo/1f3eHvzWzAiotIB6klr4nDqqt74W4gR1StlC30SEn8y8M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CB922801" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  July 9 - July 22, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20643915">Effectiveness and Safety of Tenofovir Gel, an Antiretroviral Microbicide, for the Prevention of HIV Infection in Women.</a> Karim QA, Karim SS, Frohlich JA, Grobler AC, Baxter C, Mansoor LE, Kharsany AB, Sibeko S, Mlisana KP, Omar Z, Gengiah TN, Maarschalk S, Arulappan N, Mlotshwa M, Morris L, Taylor D; on behalf of the CAPRISA 004 Trial Group. Science. 2010 Jul 19. [Epub ahead of print]PMID: 20643915</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20625151">Phosphorothioate 2&#39; deoxyribose oligomers as microbicides that inhibit human immunodeficiency virus type 1 (HIV-1) infection and block Toll-like receptor (TLR)7/9-triggering by HIV-1.</a> Fraietta JA, Mueller YM, Do DH, Holmes VM, Howett MK, Lewis MG, Boesteanu AC, Alkan SS, Katsikis PD. Antimicrob Agents Chemother. 2010 Jul 12. [Epub ahead of print]PMID: 20625151</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20624919">Identification of two APOBEC3F splice variants displaying HIV-1 antiviral activity and contrasting sensitivity to Vif.</a> Lassen KG, Wissing S, Lobritz MA, Santiago M, Greene WC. J Biol Chem. 2010 Jul 12. [Epub ahead of print]PMID: 20624919</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20610719">Identification of Novel Mutations Responsible for Resistance to MK-2048, a Second-Generation HIV-1 Integrase inhibitor.</a> Bar-Magen T, Sloan RD, Donahue DA, Kuhl BD, Zabeida A, Xu H, Oliveira M, Hazuda DJ, Wainberg MA. J Virol. 2010 Jul 7. [Epub ahead of print]PMID: 20610719</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20610712">Exploiting Drug Repositioning for the Discovery of a Novel HIV Combination Therapy.</a> Clouser CL, Patterson SE, Mansky LM. J Virol. 2010 Jul 7. [Epub ahead of print]PMID: 20610712</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20598556">Hybrid diarylbenzopyrimidine non-nucleoside reverse transcriptase inhibitors as promising new leads for improved anti-HIV-1 chemotherapy.</a> Zeng ZS, He QQ, Liang YH, Feng XQ, Chen FE, De Clercq E, Balzarini J, Pannecouque C. Bioorg Med Chem. 2010 Jul 15;18(14):5039-47. Epub 2010 Jun 4.PMID: 20598556</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20597164">Full-length HIV-1 Gag determines protease inhibitor.</a> Gupta RK, Kohli A, McCormick AL, Towers GJ, Pillay D, Parry CM. AIDS. 2010 Jul 17;24(11):1651-5.PMID: 20597164</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20542430">Synthesis and anti-HIV activity of 2&#39;-deoxy-2&#39;-fluoro-4&#39;-C-ethynyl nucleoside analogs.</a> Wang Q, Li Y, Song C, Qian K, Chen CH, Lee KH, Chang J.  Bioorg Med Chem Lett. 2010 Jul 15;20(14):4053-6. Epub 2010 May 25.PMID: 20542430</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20542438">Discovery of orally available spirodiketopiperazine-based CCR5 antagonists.</a> Nishizawa R, Nishiyama T, Hisaichi K, Hirai K, Habashita H, Takaoka Y, Tada H, Sagawa K, Shibayama S, Maeda K, Mitsuya H, Nakai H, Fukushima D, Toda M. Bioorg Med Chem. 2010 Jul 15;18(14):5208-23. Epub 2010 May 25.PMID: 20542438</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20547452">Epsilon substituted lysinol derivatives as HIV-1 protease inhibitors.</a> Jones KL, Holloway MK, Su HP, Carroll SS, Burlein C, Touch S, DiStefano DJ, Sanchez RI, Williams TM, Vacca JP, Coburn CA. Bioorg Med Chem Lett. 2010 Jul 15;20(14):4065-8. Epub 2010 May 25.PMID: 20547452</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20541944">New 2-arylnaphthalenediols and triol inhibitors of HIV-1 integrase-Discovery of a new polyhydroxylated antiviral agent.</a> Maurin C, Lion C, Bailly F, Touati N, Vezin H, Mbemba G, Mouscadet JF, Debyser Z, Witvrouw M, Cotelle P. Bioorg Med Chem. 2010 Jul 15;18(14):5194-201. Epub 2010 May 26.PMID: 20541944</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20538456">Discovery of piperidin-4-yl-aminopyrimidines as HIV-1 reverse transcriptase inhibitors. N-benzyl derivatives with broad potency against resistant mutant viruses.</a> Kertesz DJ, Brotherton-Pleiss C, Yang M, Wang Z, Lin X, Qiu Z, Hirschfeld DR, Gleason S, Mirzadegan T, Dunten PW, Harris SF, Villaseñor AG, Hang JQ, Heilek GM, Klumpp K. Bioorg Med Chem Lett. 2010 Jul 15;20(14):4215-8. Epub 2010 May 16.PMID: 20538456</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20406803">An intrabody based on a llama single-domain antibody targeting the N-terminal alpha-helical multimerization domain of HIV-1 rev prevents viral production.</a> Vercruysse T, Pardon E, Vanstreels E, Steyaert J, Daelemans D. J Biol Chem. 2010 Jul 9;285(28):21768-80. Epub 2010 Apr 20.PMID: 20406803</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="title">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279207300011">Design, Synthesis and Structure-activity Studies of Rhodanine Derivatives as HIV-1 Integrase Inhibitors.</a>  Ramkumar, K; Yarovenko, VN; Nikitina, AS; Zavarzin, IV; Krayushkin, MM; Kovalenko, LV; Esqueda, A; Odde, S; Neamati, N.  MOLECULES, 2010; 15(6): 3958-3992.</p>

    <p class="title">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279510400001"> Maleic anhydride-modified chicken ovalbumin as an effective and inexpensive anti-HIV microbicide candidate for prevention of HIV sexual transmission.</a>  Li, L; Qiao, PY; Yang, J; Lu, L; Tan, SY; Lu, H; Zhang, XJ; Chen, X; Wu, SG; Jiang, SB; Liu, SW. RETROVIROLOGY, 2010; 7: 37.</p>

    <p class="title">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279381200071">SYNTHESIS OF [C-14]- AND [C-13(6)]-LABELED POTENT HIV NON-NUCLEOSIDE REVERSE TRANSCRIPTASE INHIBITOR.</a>  Latli, B; Hrapchak, M; Busacca, CA; Krishnamurthy, D; Senanayake, CH. JOURNAL OF LABELLED COMPOUNDS &amp; RADIOPHARMACEUTICALS, 2010; 53(5-6): 446-448.</p>

    <p class="title">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279213500019">Anti-human Immunodeficiency Virus-1 Constituents of the Bark of Poncirus trifoliate.</a>  Feng, T; Wang, RR; Cai, XH; Zheng, YT; Luo, XD. CHEMICAL &amp; PHARMACEUTICAL BULLETIN, 2010; 58(7): 971-975.</p>

    <p class="title">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279213500020">Anti Human Immunodeficiency Virus Type 1 (HIV-1) Agents 4. Discovery of 5,5 &#39;-(p-Phenylenebisazo)-8-hydroxyquinoline Sulfonates as New HIV-1 Inhibitors in Vitro.</a>  Zeng, XW; Huang, N; Xu, H; Yang, WB; Yang, LM; Qu, HA; Zheng, YT. CHEMICAL &amp; PHARMACEUTICAL BULLETIN, 2010; 58(7): 976-979.</p>

    <p class="title">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279454900010">The specific character of the reaction of derivatives of 2-thioxo-2,3-dihydropyrimidin-4(1H)-one with iodomethane and alkyl chloromethyl sulfides.</a>  Novakov, IA; Orlinson, BS; Nawrozkij, MB; Mai, A; Artico, M; Rotili, D; Eremiychuk, AS; Gordeeva, EA; Brunilina, LL; Este, JA. CHEMISTRY OF HETEROCYCLIC COMPOUNDS, 2010; 46(2): 200-205.</p>

    <p class="title">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279454900010">Studies on the structure-activity relationship of 1,3,3,4-tetra-substituted pyrrolidine embodied CCR5 receptor antagonists. Part 1: Tuning the N-substituents.</a>  Ben, L; Jones, ED; Zhou, EK; Li, C; Baylis, DC; Yu, SH; Wang, MA; He, X; Coates, JAV; Rhodes, DI; Pei, G; Deadman, JJ; Xie, X; Ma, DW.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(14): 4012-4014.</p>

    <p class="title">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279030900002">Mutremdamide A and Koshikamides C-H, Peptide Inhibitors of HIV-1 Entry from Different Thennella Species.</a>  Plaza, A; Bifulco, G; Masullo, M; Lloyd, JR; Keffer, JL; Colin, PL; Hooper, JNA; Bell, LJ; Bewley, CA.  JOURNAL OF ORGANIC CHEMISTRY, 2010; 75(13): 4344-4355.</p>

    <p class="title">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279011000008">[d4U]-Spacer-[HI-236] double-drug inhibitors of HIV-1 reverse-transcriptase.</a> Younis, Y; Hunter, R; Muhanji, CI; Hale, I; Singh, R; Bailey, CM; Sullivan, TJ; Anderson, KS. BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(13): 4661-4673.</p>

    <p class="title">23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279011000008">COMPUTATIONAL DESIGN OF NORBORNANE-BASED HIV-1 PROTEASE INHIBITORS.</a>  Zhang, DW;   Yu, LZ; Huang, PL; Lee-Huang, S; Zhang, JZH.  JOURNAL OF THEORETICAL &amp; COMPUTATIONAL CHEMISTRY, 2010; 9(2): 471-485.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
