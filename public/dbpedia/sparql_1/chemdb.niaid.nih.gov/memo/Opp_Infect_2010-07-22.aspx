

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-07-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eQtjhJ1M1GdWwr4Mv2Wg9sLJr/zOqKNMm06ZfzxfchEoEmyzRJNfiDXfMdVBAAR/8cPtAe2FwR2dAH7Xn+NC6qA5H7TQ18cnHxnk24zKad/w2V5gIbj5AIHePUc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3FD7AB5F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">July 9 - July 22, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20634332">In vitro synergy of eugenol and methyl eugenol with fluconazole against clinical Candida isolates.</a> Ahmad, A., A. Khan, L.A. Khan, and N. Manzoor. J Med Microbiol. <b>[Epub ahead of print]</b>; PMID[20634332].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20488435">Synthesis and antimicrobial activities of two novel amino sugars derived from chloraloses.</a> Yenil, N., E. Ay, K. Ay, M. Oskay, and J. Maddaluno. Carbohydr Res. 345(11): p. 1617-21; PMID[20488435].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0709-072210.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20644889">Design and synthesis of nonpeptidic, small molecule inhibitors for the Mycobacterium tuberculosis protein tyrosine phosphatase PtpB.</a> Rawls, K.A., C. Grundner, and J.A. Ellman. Org Biomol Chem. <b>[Epub ahead of print]</b>; PMID[20644889].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20558071">Novel antimicrobial peptides that exhibit activity against select agents and other drug resistant bacteria.</a> Venugopal, D., D. Klapper, A.H. Srouji, J.B. Bhonsle, R. Borschel, A. Mueller, A.L. Russell, B.C. Williams, and R.P. Hicks. Bioorg Med Chem. 18(14): p. 5137-47; PMID[20558071].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20629533">Pharmacokinetics and Whole-Blood Bactericidal Activity against Mycobacterium tuberculosis of Single Doses of PNU-100480 in Healthy Volunteers.</a> Wallis, R.S., W.M. Jakubiec, V. Kumar, A.M. Silvia, D. Paige, D. Dimitrova, X. Li, L. Ladutko, S. Campbell, G. Friedland, M. Mitton-Fry, and P.F. Miller. J Infect Dis. <b>[Epub ahead of print]</b>; PMID[20629533].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0709-072210.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279258800004">Synthesis and biological activity of 2-hydroxynicotinoyl-serine-butyl esters related to antibiotic UK-3A.</a> Arsianti, A., M. Hanafi, E. Saepudin, T. Morimoto, and K. Kakiuchi. Bioorganic &amp; Medicinal Chemistry Letters. 20(14): p. 4018-4020; ISI[000279258800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279258800041">In vitro antituberculosis activities of the constituents isolated from Haloxylon salicornicum.</a> Bibi, N., S.A.K. Tanoli, S. Farheen, N. Afza, S. Siddiqi, Y. Zhang, S.U. Kazmi, and A. Malik. Bioorganic &amp; Medicinal Chemistry Letters. 20(14): p. 4173-4176; ISI[000279258800041].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279207300033">Bisbenzamidines as Antifungal Agents. Are Both Amidine Functions Required to Observe an Anti-Pneumocystis carinii Activity?</a> Laurent, J., D. Stanicki, T.L. Huang, E. Dei-Cas, M. Pottier, E.M. Aliouat, and J.J.V. Eynde. Molecules. 15(6): p. 4283-4293; ISI[000279207300033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279304900008">The photodynamic activity of a novel porphyrin derivative bearing a fluconazole structure in different media and against Candida albicans.</a> Mora, S.J., M.P. Cormick, M.E. Milanesio, and E.N. Durantini. Dyes and Pigments. 87(3): p. 234-240; ISI[000279304900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279035300007">Synthesis and biological activity of new cycloalkylthiophene-Schiff bases and their Cr(III) and Zn(II) complexes.</a> Altundas, A., N. Sari, N. Colak, and H. Ogutcu. Medicinal Chemistry Research. 19(6): p. 576-588; ISI[000279035300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279126700009">anti-Tuberculosis natural products: synthesis and biological evaluation of pyridoacridine alkaloids related to ascididemin.</a> Appleton, D.R., A.N. Pearce, and B.R. Copp. Tetrahedron. 66: p. 4977-4986; ISI[000279126700009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279126800038">An Efficient Synthesis of 3,4-Dihydro-3-substituted-2H-naphtho[2,1-e][1,3]oxazine Derivatives Catalyzed by Zirconyl(IV) Chloride and Evaluation of its Biological Activities.</a> Kategaonkar, A.H., S.S. Sonar, R.U. Pokalwar, B.B. Shingate, and M.S. Shingare. Bulletin of the Korean Chemical Society. 31(6): p. 1657-1660; ISI[000279126800038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279199800011">A Promising Broad Spectrum Antimicrobial Red Fluorescent Protein Present in Silkworm Excreta.</a> Matti, K.M., C.J. Savanurmath, and S.B. Hinchigeri. Biological &amp; Pharmaceutical Bulletin. 33(7): p. 1143-1147; ISI[000279199800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000279035300004">Iodine (III)-mediated synthesis of some 2-aryl/hetarylbenzoxazoles as antibacterial/antifungal agents.</a> Kumar, R., R.R. Nair, S.S. Dhiman, J. Sharma, and O. Prakash. Medicinal Chemistry Research. 19(6): p. 541-550; ISI[000279035300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0709-072210.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
