

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-08-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="l5nHP2YI45QwLUmhAgSRQfm8mb5n2aWn8IjY+o2KVBFhIfGWiC31Zv6oMs7E28qvGL/iK9fGv3mEr1yUl3YJSG9oiH2FsuxZfRKpT/a/cQhbVvHB/3JOv2Qp6ho=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FC376571" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 16 - August 29, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23979732">The in Vitro Cross-resistance Profile of the Nucleoside Reverse Transcriptase Inhibitor (NRTI) BMS-986001 against Known NRTI Resistance Mutations.</a> Li, Z., B. Terry, W. Olds, T. Protack, C. Deminie, B. Minassian, B. Nowicka-Sans, Y. Sun, I. Dicker, C. Hwang, M. Lataillade, G.J. Hanna, and M. Krystal. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23979732].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23811046">Inhibition of HIV-1 Enzymes, Antioxidant and Anti-inflammatory Activities of Plectranthus barbatus.</a> Kapewangolo, P., A.A. Hussein, and D. Meyer. Journal of Ethnopharmacology, 2013. 149(1): p. 184-190. PMID[23811046].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23959304">In Vitro and in Vivo Activities of AIC292, a Novel HIV-1 Nonnucleoside Reverse Transcriptase Inhibitor.</a> Wildum, S., D. Paulsen, K. Thede, H. Ruebsamen-Schaeff, and H. Zimmermann. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23959304].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23962745">Evaluating the Potential of IL-27 as a Novel Therapeutic Agent in HIV-1 Infection.</a> Swaminathan, S., L. Dai, H.C. Lane, and T. Imamichi. Cytokine Growth Factor Reviews, 2013. <b>[Epub ahead of print]</b>. PMID[23962745].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23957390">Discovery of HIV-1 Integrase Inhibitors: Pharmacophore Mapping, Virtual Screening, Molecular Docking, Synthesis and Biological Evaluation.</a> Bhatt, H., P. Patel, and C. Pannecouque. Chemical Biology and Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23957390].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0816-082913.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322053600014">Chemical Constituents from the Branches and Leaves of Pyrus pashia Buch.-Ham. Ex D. Don.</a> Zhao, M., L. Cai, J.M. He, T.P. Yin, Y.C. Sui, M.T. Luo, and Z.T. Ding. Chinese Journal of Organic Chemistry, 2013. 33(6): p. 1284-1290. ISI[000322053600014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320755400152">Epitope Mapping of M36, a Human Antibody Domain with Potent and Broad HIV-1 Inhibitory Activity.</a> Wan, C., J.P. Sun, W.Z. Chen, X.H. Yuan, H.H. Chong, P. Prabakaran, D.S. Dimitrov, and Y.X. He. PloS One, 2013. 8(6). ISI[000320755400152].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321758200001">Peptide and Protein-based Inhibitors of HIV-1 Co-receptors.</a> von Recum, H.A. and J.K. Pokorski. Experimental Biology and Medicine, 2013. 238(5): p. 442-449. ISI[000321758200001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320673200013">Syntheses and in Vitro Biological Screening of 1-Aryl-10H-[1,2,4]triazolo[3&#39;,4&#39;:3,4][1,2,4]triazino[5,6-b]indoles.</a> Upadhyay, K., A. Manvar, R. Loddo, P. La Colla, V. Virsodiya, J. Trivedi, R. Chaniyara, and A. Shah. Medicinal Chemistry Research, 2013. 22(8): p. 3675-3686. ISI[000320673200013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322130500001">Pseudomonas DING Proteins as Human Transcriptional Regulators and HIV-1 Antagonists.</a> Suh, A., V. Le Douce, O. Rohr, C. Schwartz, and K. Scott. Virology Journal, 2013. 10. ISI[000322130500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320827400002">A One Pot Microwave Assisted Synthesis of 3-Acyl-2,4-dihydroxyquinoline Followed by Synthesis of 7-Methyldibenzo[c,f][2,7]naphthyridin-6(5H)-ones Via Three Routes.</a> Pitchai, P., C. Uvarani, R.M. Gengan, and P.S. Mohan. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(6): p. 776-786. ISI[000320827400002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322529300010">Comparative in Vitro Functional Analysis of Synthetic Defensins and Their Corresponding Peptide Variants against HIV-1NL4.3, E. coli, S. aureus and P. aeruginosa.</a> Mohan, T., D. Mitra, and D.N. Rao. International Journal of Peptide Research and Therapeutics, 2013. 19(3): p. 245-255. ISI[000322529300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321980000001">Anti-HIV-1 Activity, Protease Inhibition and Safety Profile of Extracts Prepared from Rhus parviflora.</a> Modi, M., Nutan, B. Pancholi, S. Kulshrestha, A.K.S. Rawat, S. Malhotra, and S.K. Gupta. Bmc Complementary and Alternative Medicine, 2013. 13. ISI[000321980000001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321761800070">A Modified Zinc acetate Gel, a Potential Nonantiretroviral Microbicide, Is Safe and Effective against Simian-Human Immunodeficiency Virus and Herpes Simplex Virus 2 Infection in Vivo.</a> Kenney, J., A. Rodriguez, L. Kizima, S. Seidor, R. Menon, N. Jean-Pierre, P. Pugach, K. Levendosky, N. Derby, A. Gettie, J. Blanchard, M. Piatak, J.D. Lifson, G. Paglini, T.M. Zydowsky, M. Robbiani, and J.A.F. Romero. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 4001-4009. ISI[000321761800070].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320673200038">Novel PCU Cage Diol Peptides as Potential Targets against Wild-type CSA HIV-1 Protease: Synthesis, Biological Screening and Molecular Modeling Studies.</a> Karpoormath, R., Y. Sayed, T. Govender, H.G. Kruger, M.E.S. Soliman, and G.E.M. Maguire. Medicinal Chemistry Research, 2013. 22(8): p. 3918-3933. ISI[000320673200038].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321883800010">4-Substituted 2-Hydroxyisoquinoline-1,3(2H,4H)-diones as a Novel Class of HIV-1 Integrase Inhibitors.</a> Billamboz, M., V. Suchaud, F. Bailly, C. Lion, J. Demeulemeester, C. Calmels, M.L. Andreola, F. Christ, Z. Debyser, and P. Cotelle. ACS Medicinal Chemistry Letters, 2013. 4(7): p. 41-46. ISI[000321883800010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0816-082913.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
