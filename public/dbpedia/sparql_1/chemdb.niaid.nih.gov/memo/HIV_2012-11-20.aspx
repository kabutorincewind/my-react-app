

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-11-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="G4X1od2uA4pli8PUAun2GjFXFr+/+WxUF0+F2f2ou+Zl1bPMRKFOajmzbF1XFXDapQOX43Dj5xF0+8Q3mn2liptseS6UgnuB3ntEY5jUXfGc08+9l+C0muuye3Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ACC9C02C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 9 - November 20, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23151033">Design of a Novel Cyclotide-cased CXCR4 Antagonist with anti-Human Inmmunodeficiency Virus (HIV)-1 Activity.</a> Aboye, T.L., H. Ha, S. Majumder, F. Christ, Z. Debyser, A. Shekhtamn, N. Neamati, and J.A. Camarero. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print];</b> PMID[23151033]. <b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23152485">In Vitro Effects of the CCR5 Inhibitor Maraviroc on Human T Cell Function.</a> Arberas, H., A.C. Guardo, M.E. Bargallo, M.J. Maleno, M. Calvo, J.L. Blanco, F. Garcia, J.M. Gatell, and M. Plana. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print];</b> PMID[23152485].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147734">A Rationally Engineered anti-HIV Peptide Fusion Inhibitor with Greatly Reduced Immunogenicity.</a> Brauer, F., K. Schmidt, R.C. Zahn, C. Richter, H.H. Radeke, J.E. Schmitz, D. von Laer, and L. Egerer. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print];</b> PMID[23147734].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23153325">Impact of Sustained RNAi-Mediated Suppression of Cellular Cofactor Tat-SF1 on HIV-1 Replication in Cd4+ T Cells.</a> Green, V.A., P. Arbuthnot, and M.S. Weinberg. Virology Journal, 2012. 9(1): p. 272. PMID[23153325].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23152526">Trimeric GPI-Anchored HCDR3 of Broadly Neutralizing Antibody PG16 Is a Potent HIV-1 Entry Inhibitor.</a> Liu, L., W. Wang, L. Yang, H. Ren, J.T. Kimata, and P. Zhou. Journal of Virology, 2012. <b>[Epub ahead of print];</b> PMID[23152526].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23150992">Effect of Early Anti-retroviral Therapy on the Pathogenic Changes in Mucosal Tissues of SIV Infected Rhesus macaques.</a> Malzahn, J., C. Shen, L. Caruso, P. Ghosh, S. Ramachandra Sankapal, S. Barratt-Boyes, P. Gupta, and Y. Chen. Virology Journal, 2012. 9(1): p. 269. PMID[23150992].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23137340">Rational Design of Potent Non-nucleoside Inhibitors of HIV Reverse Transcriptase.</a> Peat, A.J., P.Y. Chong, D.M. Garrido, H. Zhang, E. Stewart, R. Nolte, L. Wang, R.G. Ferris, M. Edelstein, K.L. Weaver, A. Mathis, M. Youngman, and P. Sebahar. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print];</b> PMID[23137340].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23146561">A Phenyl-thiadiazolylidene-amine Derivative Ejects Zinc from Retroviral Nucleocapsid Zinc Fingers and Inactivates HIV Virions.</a> Vercruysse, T., B. Basta, W. Dehaen, N. Humbert, J. Balzarini, F. Debaene, S. Sanglier-Cianferani, C. Pannecouque, Y. Mely, and D. Daelemans. Retrovirology, 2012. 9(1): p. 95. PMID[23146561].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_1109-112012.</p>

    <p> </p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309472100293">Anti-HIV and Immune Modulating Activities of IND02.</a> Biedma, M.E., B. Connell, S. Schmidt, H. Lortat-Jacob, C. Moog, and E. Prakash. Retrovirology, 2012. 9(Suppl 2): p. P220.  ISI[000309472100293].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309654200059">The M-T Hook Structure Is Critical for Design of HIV-1 Fusion Inhibitors.</a> Chong, H.H., X. Yao, J.P. Sun, Z.L. Qiu, M. Zhang, S. Waltersperger, M.T. Wang, S. Cui, and Y.X. He. Journal of Biological Chemistry, 2012. 287(41): p. 34558-34568. ISI[000309654200059].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309472100030">A Short Segment in the HIV-1 gp120 V1/V2 Region Is a Major Determinant of Neutralization Resistance to PG9-Like Antibodies.</a> Doria-Rose, N.A., I. Georgiev, R.P. Staupe, S. O&#39;Dell, G. Chuang, J. Gorman, J.S. McLellan, M. Pancera, M. Bonsignori, B.F. Haynes, D.R. Burton, W.C. Koff, P.D. Kwong, and J.R. Mascola. Retrovirology, 2012. 9(Suppl 2): p. O29. ISI[000309472100030].</p>

    <p class="plaintext"><b>[</b><b>WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309472100268">Anti-gp41 Antibodies Inhibit Infection and Transcytosis of HIV-1 Infectious Molecular Clones Expressing Transmitted/Founder Envelopes.</a> Jain, S., C. Ochsenbauer, J.C. Kappes, and K.L. Rosenthal. Retrovirology, 2012. 9(Suppl 2): p. P195. ISI[000309472100268].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309472100295">Inhibition of HIV-1 Subtype C by 2 &#39; F-RNA Aptamers Isolated against Enveloped Pseudovirus.</a> London, G.G., M.M. Madiga, E.E. Gray, D.D. Sok, L.L. Morris, D.D. Burton, and M.M. Khati. Retrovirology, 2012. 9(Suppl 2):P222. ISI[000309472100295].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309472100133">Neutralizing anti-HIV Antibodies Develop in a Humanized Mouse Model of HIV-1 Infection.</a> Seung, E., A. Dugast, T. Dudek, H. Mattoo, V. Vrbanac, T. Tivey, T. Murooka, A. Cariappa, A.D. Luster, S. Pillai, and A.M. Tager. Retrovirology, 2012. 9(Suppl 2): p. P60. ISI[000309472100133].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309777600001">A Laccase with Antiproliferative and HIV-I Reverse Transcriptase Inhibitory Activities from the Mycorrhizal Fungus Agaricus placomyces.</a> Sun, J., Q.J. Chen, Q.Q. Cao, Y.Y. Wu, L.J. Xu, M.J. Zhu, T.B. Ng, H.X. Wang, and G.Q. Zhang. Journal of Biomedicine and Biotechnology, 2012. 2012: 736472. ISI[000309777600001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309816500037">A Role for microRNA-155 Modulation in the anti-HIV-1 Effects of Toll-like Receptor 3 Stimulation in Macrophages.</a> Swaminathan, G., F. Rossi, L.J. Sierra, A. Gupta, S. Navas-Martin, and J. Martin-Garcia. Plos Pathogens, 2012. 8(9): e1002937. ISI[000309816500037].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309639500010">Purification and Characterization of anti-HIV-1 Protein from Canna indica L. Leaves.</a> Thepouyporn, A., C. Yoosook, W. Chuakul, K. Thirapanmethee, C. Napaswad, and C. Wiwat. Southeast Asian Journal of Tropical Medicine and Public Health, 2012. 43(5): p. 1153-1160. ISI[000309639500010].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309662500006">Methamphetamine Activates Nuclear Factor Kappa-light-chain-enhancer of Activated B Cells (NF-kappa B) and Induces Human Immunodeficiency Virus (HIV) Transcription in Human Microglial Cells.</a> Wires, E.S., D. Alvarez, C. Dobrowolski, Y. Wang, M. Morales, J. Karn, and B.K. Harvey. Journal of Neurovirology, 2012. 18(5): p. 400-410. ISI[000309662500006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000309629500011">New Aryl-1,3-thiazole-4-carbohydrazides, Their 1,3,4-oxadiazole-2-thione, 1,2,4-triazole, Isatin-3-ylidene and Carboxamide Derivatives. Synthesis and anti-HIV Activity.</a> Zia, M., T. Akhtar, S. Hameed, and N.A. Al-Masoudi. Zeitschrift Fur Naturforschung Section B-A. Journal of Chemical Sciences, 2012. 67(7): p. 747-758. ISI[000309629500011].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_1109-112012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
