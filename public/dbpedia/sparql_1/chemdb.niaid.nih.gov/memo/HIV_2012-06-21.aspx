

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-06-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RbvH+LDYFgpWmwYlyTW+tQ19/MYTs9Ztw+9HIepLXpQzE1vHmwTvuthos7UJe4LLc1+SDAuYf+IhFLRX71KqXG2Rk4PKSj4inj+zb9pGEHfxDbbXXRzHsNuTyM4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0844DF08" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 8 - June 21, 2012</h1>

    <p class="memofmt2-2">PubMed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22703590">Synthesis and Biological Evaluation of Tricyclic Guanidine Analogues of Batzelladine K for Antimalarial, Antileishmanial, Antibacterial, Antifungal and anti-HIV Activities.</a> Ahmed, N., K.G. Brahmbhatt, S.I. Khan, M. Jacob, B.L. Tekwani, S. Sabde, D. Mitra, I.P. Singh, I.A. Khan, and K.K. Bhutani, Chemical Biology and Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22703590].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22618567">Frequent Emergence of N348I in HIV-1 Subtype C Reverse Transcriptase with Failure of Initial Therapy Reduces Susceptibility to Reverse-transcriptase Inhibitors.</a> Brehm, J.H., D.L. Koontz, C.L. Wallis, K.A. Shutt, I. Sanne, R. Wood, J.A. McIntyre, W.S. Stevens, N. Sluis-Cremer, and J.W. Mellors, Clinical Infectious Disease, 2012. <b>[Epub ahead of print]</b>; PMID[22618567].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22591854">Design, Synthesis, anti-HIV Evaluation and Molecular Modeling of Piperidine-linked Amino-triazine Derivatives as Potent Non-nucleoside Reverse Transcriptase Inhibitors.</a> Chen, X., P. Zhan, X. Liu, Z. Cheng, C. Meng, S. Shao, C. Pannecouque, and E.D. Clercq, Bioorganic &amp; Medicinal Chemistry, 2012. 20(12): p. 3856-3864; PMID[22591854].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22511760">Discovery of Critical Residues for Viral Entry and Inhibition through Structural Insight of HIV-1 Fusion Inhibitor CP621-652.</a> Chong, H., X. Yao, Z. Qiu, B. Qin, R. Han, S. Waltersperger, M. Wang, S. Cui, and Y. He, Journal of Biological Chemistry, 2012. 287(24): p. 20281-20289; PMID[22511760].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22698070">A Peptide Nucleic Acid-aminosugar Conjugate Targeting Transactivation Response Element of HIV-1 RNA Genome Shows a High Bioavailability in Human Cells and Strongly Inhibits Tat-mediated Transactivation of HIV-1 Transcription.</a> Das, I., J. Desire, D.K. Manvar, I. Baussanne, V.N. Pandey, and J.L. Decout, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22698070].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22687513">Bifunctional CD4-DC-SIGN Fusion Proteins Demonstrate Enhanced Avidity to gp120 and Inhibit HIV-1 Infection and Dissemination.</a> Du, T., K. Hu, J. Yang, J. Jin, C. Li, D. Stieh, G.E. Griffin, R.J. Shattock, and Q. Hu, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22687513].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704066">Miltefosine Represses HIV-1 Replication in Human Dendritic Cell/T-cell Cocultures Partially by Inducing Secretion of Type-I Interferon.</a><i>.</i> Garg, R. and M.J. Tremblay, Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22704066].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22465592">Ethnopharmacology in Overdrive: The Remarkable anti-HIV Activity of Artemisia annua.</a> Lubbe, A., I. Seibert, T. Klimkait, and F. van der Kooy, Journal of Ethnopharmacology, 2012. 141(3): p. 854-859; PMID[22465592].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22713337">Response of SIV to the Novel Nucleoside Reverse Transcriptase Inhibitor 4&#39; -Ethynyl-2-fluoro-2&#39; -deoxyadenosine (EFdA) in Vitro and in Vivo.</a> Murphey-Corb, M., P. Rajakumar, H. Michael, J. Nyaundi, P.J. Didier, A.B. Reeve, H. Mitsuya, S.G. Sarafianos, and M.A. Parniak, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22713337].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22579418">Pharmacophore-based Small Molecule CXCR4 Ligands.</a> Narumi, T., T. Tanaka, C. Hashimoto, W. Nomura, H. Aikawa, A. Sohma, K. Itotani, M. Kawamata, T. Murakami, N. Yamamoto, and H. Tamamura, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(12): p. 4169-4172; PMID[22579418].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22708897">Design, Synthesis, Biological and Structural Evaluations of Novel HIV-1 Protease Inhibitors to Combat Drug Resistance.</a> Parai, M.K., D.J. Huggins, H. Cao, M.N. Nalam, A. Ali, C.A. Schiffer, B. Tidor, and T.M. Rana, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22708897].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22696642">Antiretroviral Agents Effectively Block HIV Replication after Cell to Cell Transfer.</a> Permanyer, M., E. Ballana, A. Ruiz, R. Badia, E. Riveira-Munoz, E. Gonzalo, B. Clotet, and J.A. Este, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22696642].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22698778">Inhibition of HIV-1 Tat-induced Transactivation and Apoptosis by the Divalent Metal Chelators, Fusaric Acid and Picolinic Acid-implications for HIV-1 Dementia.</a> Ramautar, A., M. Mabandla, J. Blackburn, and W.M. Daniels, Neuroscience Research, 2012. <b>[Epub ahead of print]</b>; PMID[22698778].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607675">Development of a Series of 3-Hydroxyquinolin-2(1H)-ones as Selective Inhibitors of HIV-1 Reverse Transcriptase Associated RNase H Activity.</a> Suchaud, V., F. Bailly, C. Lion, E. Tramontano, F. Esposito, A. Corona, F. Christ, Z. Debyser, and P. Cotelle, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(12): p. 3988-3992; PMID[22607675].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22535962">New Class of HIV-1 Integrase (IN) Inhibitors with a Dual Mode of Action.</a> Tsiang, M., G.S. Jones, A. Niedziela-Majka, E. Kan, E.B. Lansdon, W. Huang, M. Hung, D. Samuel, N. Novikov, Y. Xu, M. Mitchell, H. Guo, K. Babaoglu, X. Liu, R. Geleziunas, and R. Sakowicz, Journal of Biological Chemistry, 2012. 287(25): p. 21189-21203; PMID[22535962].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22621689">Dual Inhibitors for Aspartic Proteases HIV-1 PR and Renin: Advancements in AIDS-hypertension-diabetes Linkage Via Molecular Dynamics, Inhibition Assays, and Binding Free Energy Calculations.</a> Tzoupis, H., G. Leonis, G. Megariotis, C.T. Supuran, T. Mavromoustakos, and M.G. Papadopoulos, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22621689].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22697794">Antibody against Integrin LFA-1 Inhibits HIV-1 Infection in Primary Cells through Caspase 8-mediated Apoptosis.</a> Walker, T.N., L.M. Cimakasky, E.M. Coleman, M.N. Madison, and J.E. Hildreth, AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22697794].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22709537">Efficient Inhibition of HIV-1 Replication by an Artificial Polycistronic miRNA Construct.</a> Zhang, T., T. Cheng, L. Wei, Y. Cai, A.E. Yeo, J. Han, Y.A. Yuan, J. Zhang, and N. Xia, Virology Journal, 2012. 9(1): p. 118; PMID[22709537].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302451300030">Anti-AIDS Agents 88. anti-HIV Conjugates of Betulin and Betulinic Acid with AZT Prepared Via Click Chemistry.</a> Bori, I.D., H.Y. Hung, K.D. Qian, C.H. Chen, S.L. Morris-Natschke, and K.H. Lee, Tetrahedron Letters, 2012. 53(15): p. 1987-1989; ISI[000302451300030].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304291900006">Synthesis and Biological Evaluation of Piperidine-substituted Triazine Derivatives as HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Chen, X.W., P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, and X.Y. Liu, European Journal of Medicinal Chemistry, 2012. 51: p. 60-66; ISI[000304291900006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304338800018">Design, Synthesis, and Antiviral Activity of Entry Inhibitors That Target the CD4-binding Site of HIV-1.</a> Curreli, F., S. Choudhury, I. Pyatkin, V.P. Zagorodnikov, A.K. Bulay, A. Altieri, Y. Do Kwon, P.D. Kwong, and A.K. Debnath, Journal of Medicinal Chemistry, 2012. 55(10): p. 4764-4775; ISI[000304338800018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304117700005">Polymeric Nanoparticles Affect the Intracellular Delivery, Antiretroviral Activity and Cytotoxicity of the Microbicide Drug Candidate Dapivirine.</a> das Neves, J., J. Michiels, K.K. Arien, G. Vanham, M. Amiji, M.F. Bahia, and B. Sarmento, Pharmaceutical Research, 2012. 29(6): p. 1468-1484; ISI[000304117700005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304244000040">The Ledgins: First-in-class Allosteric HIV-1 Integrase Inhibitors.</a> Desimmie, B., Tropical Medicine &amp; International Health, 2012. 17: p. 14-14; ISI[000304244000040].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304003200069">Peptides from Second Extracellular Loop of C-C Chemokine Receptor Type 5 (CCR5) Inhibit Diverse Strains of HIV-1.</a> Dogo-Isonagie, C., S. Lam, E. Gustchina, P. Acharya, Y.P. Yang, S. Shahzad-ul-Hussan, G.M. Clore, P.D. Kwong, and C.A. Bewley, Journal of Biological Chemistry, 2012. 287(18): p. 15076-15086; ISI[000304003200069].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304487800087">Pharmacologic Inducers of the Macrophage Antioxidant Response Inhibit HIV Infection and Neurotoxin Production.</a> Gill, A., S. Cross, P. Vance, and D. Kolson, Journal of Neurovirology, 2012. 18: p. 41-42; ISI[000304487800087].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304030900040">CCR5 Antagonist TD-0680 Uses a Novel Mechanism for Enhanced Potency against HIV-1 Entry, Cell-mediated Infection, and a Resistant Variant.</a> Kang, Y.X., Z.W. Wu, T.C.K. Lau, X.F. Lu, L. Liu, A.K.L. Cheung, Z.W. Tan, J. Ng, J.G. Liang, H.B. Wang, S.K. Li, B.J. Zheng, B. Li, L. Chen, and Z.W. Chen, Journal of Biological Chemistry, 2012. 287(20): p. 16499-16509; ISI[000304030900040].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302276700008">Design and Synthesis of Novel 1 &#39;,3 &#39;-Dioxolane 5 &#39;-deoxyphosphonic Acid Purine Analogues as Potent Antiviral Agents.</a> Kim, E., L.J. Liu, W. Lee, and J.H. Hong, Nucleosides Nucleotides &amp; Nucleic Acids, 2012. 31(1-3): p. 85-96; ISI[000302276700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304487800115">Vpr as a Regulator of NF-kappa B in HIV-1: Selective Inhibition of the TNF-alpha Pathway.</a> Kogan, M., S. Deshmane, B. Sawaya, K. Khalili, and J. Rappaport, Journal of Neurovirology, 2012. 18: p. 55-56; ISI[000304487800115].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303785900026">Structure-based Design, Synthesis, and Characterization of Dual Hotspot Small-molecule HIV-1 Entry Inhibitors.</a> LaLonde, J.M., Y.D. Kwon, D.M. Jones, A.W. Sun, J.R. Courter, T. Soeta, T. Kobayashi, A.M. Princiotto, X.L. Wu, A. Schon, E. Freire, P.D. Kwong, J.R. Mascola, J. Sodroski, N. Madani, and A.B. Smith, Journal of Medicinal Chemistry, 2012. 55(9): p. 4382-4396; ISI[000303785900026].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304449300013">Cycloartane Triterpenoids from Cassia occidentalis.</a> Li, S.F., Y.T. Di, R.H. Luo, Y.T. Zheng, Y.H. Wang, X. Fang, Y. Zhang, L. Li, H.P. He, S.L. Li, and X.J. Hao, Planta Medica, 2012. 78(8): p. 821-827; ISI[000304449300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304230000014">Synthesis, Drug Release and anti-HIV Activity of a Series of Pegylated Zidovudine Conjugates.</a> Li, W.J., J.D. Wu, P. Zhan, Y. Chang, C. Pannecouque, E. De Clercq, and X.Y. Liu, International Journal of Biological Macromolecules, 2012. 50(4): p. 974-980; ISI[000304230000014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302559000002">Therapeutic Potential of HIV Protease-activable CASP3.</a> Miyauchi, K., E. Urano, M. Takizawa, R. Ichikawa, and J. Komano, Scientific Reports, 2012. 2; ISI[000302559000002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302073100001">A Synthetic C34 Trimer of HIV-1 gp41 Shows Significant Increase in Inhibition Potency (Vol 7, Pg 205, 2012).</a> Nomura, W., C. Hashimoto, A. Ohya, K. Miyauchi, E. Urano, T. Tanaka, T. Narumi, T. Nakahara, J.A. Komano, N. Yamamoto, and H. Tamamura, ChemMedChem, 2012. 7(4): p. 546-546; ISI[000302073100001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304185400002">Intramolecular Regulation of the Sequence-specific mRNA Interferase Activity of MazF Fused to a MazE Fragment with a Linker Cleavable by Specific Proteases.</a> Park, J.H., Y. Yamaguchi, and M. Inouye, Applied and Environmental Microbiology, 2012. 78(11): p. 3794-3799; ISI[000304185400002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302475000001">Cleavage of Pyrene-stabilized RNA Bulge Loops by trans-(+/-)-Cyclohexane-1,2-diamine.</a> Patel, S., J. Rana, J. Roy, and H.D. Huang, Chemistry Central Journal, 2012. 6; ISI[000302475000001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302591100058">2-(Alkyl/Aryl)amino-6-benzylpyrimidin-4(3H)-ones as Inhibitors of Wild-type and Mutant HIV-1: Enantioselectivity Studies.</a> Rotili, D., A. Samuele, D. Tarantino, R. Ragno, I. Musmuca, F. Ballante, G. Botta, L. Morera, M. Pierini, R. Cirilli, M.B. Nawrozkij, E. Gonzalez, B. Clotet, M. Artico, J.A. Este, G. Maga, and A. Mai, Journal of Medicinal Chemistry, 2012. 55(7): p. 3558-3562; ISI[000302591100058].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304084700008">Synthesis and anti-HIV Activity of Triazolo-fused 3&#39;,5&#39;-cyclic Nucleoside Analogues Derived from an Intramolecular Huisgen 1,3-Dipolar Cycloaddition.</a> Sun, J.B., X.Y. Liu, H.M. Li, R.H. Duan, and J.C. Wu, Helvetica Chimica Acta, 2012. 95(5): p. 772-779; ISI[000304084700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304487800231">A Novel High-throughput Screening Assay to Identify Inhibitors of HIV-1 gp120 Protein Interaction with DC-SIGN.</a> Tran, T., R. El Baz, A. Cuconati, J. Arthos, P. Jain, and Z. Khan, Journal of Neurovirology, 2012. 18: p. 112-113; ISI[000304487800231].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302682300001">A Laccase with HIV-1 Reverse Transcriptase Inhibitory Activity from the Broth of Mycelial Culture of the Mushroom Lentinus tigrinus.</a> Xu, L.J., H.X. Wang, and T. Ng, Journal of Biomedicine and Biotechnology, 2012. <b>[Epub ahead of print]</b>; ISI[000302682300001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0608-062112.</p>

    <h2>Latent HIV Reactivation</h2>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22696646">Selected Drugs with Reported Secondarycell-differentiating Capacity Prime Latent HIV-1 Infection for Reactivation.</a> Shishido, T., F. Wolschendorf, A. Duverger, F. Wagner, J. Kappes, J. Jones, and O. Kutsch, J Virol, 2012. <b>[Epub ahead of print]</b>; PMID[22696646].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0608-062112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
