

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-05-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fkBEP/kM6Sq7Tlfu388sOf8faOiv5Gq5Hc6ZMfMXP8x/O6LyPqiOKASqPdFgBnZudOD50IegzpQRKwHiqKgwTxs1qIkt44TtbRtSfRUlbzDbAqpXFaySy9ZR1wE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="84DB9634" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 27 - May 10, 2012</h1>

    <h2>Hepatitis B virus</h2>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/22558448">A Novel Inhibitor of Human La Protein with anti-HBV Activity Discovered by Structure-based Virtual Screening and in Vitro Evaluation.</a> Tang, J., Z.M. Huang, Y.Y. Chen, Z.H. Zhang, G.L. Liu, and J. Zhang, PloS One, 2012. 7(4): p. e36363; PMID[22558448].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22555152">Novel 1,2,4-Triazole and Imidazole Derivatives of L-Ascorbic and Imino-ascorbic acid: Synthesis, anti-HCV and Antitumor Activity Evaluations.</a> Wittine, K., M. Stipkovic Babic, D. Makuc, J. Plavec, S. Kraljevic Pavelic, M. Sedic, K. Pavelic, P. Leyssen, J. Neyts, J. Balzarini, and M. Mintas, Bioorganic &amp; Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22555152].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22545104">Novel Small-molecule Inhibitors of Hepatitis C Virus Entry Block Viral Spread and Promote Viral Clearance in Cell Culture.</a> Coburn, G.A., D.N. Fisch, S.M. Moorji, J.M. de Muys, J.D. Murga, D. Paul, K.P. Provoncha, Y. Rotshteyn, A.Q. Han, D. Qian, P.J. Maddon, and W.C. Olson, PloS One, 2012. 7(4): p. e35351; PMID[22545104].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22543048">Tegobuvir (GS-9190) Potency against HCV Chimeric Replicons Derived from Consensus NS5B Sequences from Genotypes 2b, 3a, 4a, 5a, and 6a.</a> Wong, K.A., S. Xu, R. Martin, M.D. Miller, and H. Mo, Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22543048].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22542193">Finger-loop Inhibitors of the HCV NS5b Polymerase. Part 1: Discovery and Optimization of Novel 1,6- and 2,6-Macrocyclic Indole Series.</a> McGowan, D., S. Vendeville, T.I. Lin, A. Tahri, L. Hu, M.D. Cummings, K. Amssoms, J.M. Berke, M. Canard, E. Cleiren, P. Dehertogh, S. Last, E. Fransen, E. Van Der Helm, I. Van den Steen, L. Vijgen, M.C. Rouan, G. Fanning, O. Nyanguile, K. Van Emelen, K. Simmen, and P. Raboisson, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22542193].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22542020">Identification of Aryl dihydrouracil Derivatives as Palm Initiation Site Inhibitors of HCV NS5B Polymerase.</a> Liu, Y., B.H. Lim, W.W. Jiang, C.A. Flentge, D.K. Hutchinson, D.L. Madigan, J.T. Randolph, R. Wagner, C.J. Maring, W.M. Kati, and A. Molla, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22542020].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <h2>FIV</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22551322">Efficacy and Adverse Effects of the Antiviral Compound Plerixafor in Feline Immunodeficiency Virus-infected Cats.</a>. Hartmann, K., C. Stengel, D. Klein, H. Egberink, and J. Balzarini, Journal of Veterinary Internal Medicine / American College of Veterinary Internal Medicine, 2012. 26(3): p. 483-90; PMID[22551322].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22552453">In Vitro Study of the Effect of a Probiotic Bacterium Lactobacillus rhamnosus against Herpes Simplex Virus Type 1.</a> Khani, S., M. Motamedifar, H. Golmoghaddam, H.M. Hosseini, and Z. Hashemizadeh, The Brazilian Journal of Infectious Diseases: An Official Publication of the Brazilian Society of Infectious Diseases, 2012. 16(2): p. 129-35; PMID[22552453].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22550533">HSV-1 miR-H6 Inhibits HSV-1 Replication and IL-6 Expression in Human Corneal Epithelial Cells In Vitro.</a> Duan, F., J. Liao, Q. Huang, Y. Nie, and K. Wu, Clinical &amp; Developmental immunology, 2012. <b>[Epub ahead of print]</b>: p. 192791; PMID[22550533].
    <br />
    <b>[PubMed]</b>. OV_0427-051012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302593200008">Synthesis of Novel Thienonorbornylpurine Derivatives</a>. Hrebabecky, H., M. Dejmek, M. Sala, H. Mertlikova-Kaiserova, M. Dracinsky, P. Leyssen, J. Neyts, and R. Nencka, Tetrahedron, 2012. 68(15): p. 3195-3204; PMID[WOS:000302593200008].
    <br />
    <b>[WOS]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301767000001">Nucleoside, Nucleotide, and Non-nucleoside Inhibitors of Hepatitis C Virus NS5B RNA-Dependent RNA-Polymerase.</a> Sofia, M.J., W.S. Chang, P.A. Furman, R.T. Mosley, and B.S. Ross, Journal of Medicinal Chemistry, 2012. 55(6): p. 2481-2531; PMID[WOS:000301767000001].
    <br />
    <b>[WOS]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302288800198">Azithromycin Inhibits Hepatitis C Virus Replication in Hepatocytes.</a> Ye, L., M. Guo, J.L. Li, X. Wang, Y.Z. Wang, Y. Zhou, and W.Z. Ho, Journal of Neuroimmune Pharmacology, 2012. 7: p. S80-S80; PMID[WOS:000302288800198].
    <br />
    <b>[WOS]</b>. OV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301884800009">Antioxidant and Antiviral Activities of Lipophilic Epigallocatechin Gallate (EGCG) Derivatives.</a> Zhong, Y., C.M. Ma, and F. Shahidi, Journal of Functional Foods, 2012. 4(1): p. 87-93; PMID[WOS:000301884800009].
    <br />
    <b>[WOS]</b>. OV_0427-051012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
