

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-09-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ej8SDyxMsx2D/0I36TFy9VIZuZ+JkK9oMlEJqexK25eLopmSgk0RuTRYaZ3ebdbotE+IAnA/hFobOgzDFa57XytcL6XT1qijqZ0UdO6qW0XW+Jf/uY62HbTHpQw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B5D6AE34" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 31 - September 13, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22687516">Amino acid-derived 1,2-Benzisothiazolinone Derivatives as Novel Small-molecule Antifungal Inhibitors: Identification of Potential Genetic Targets.</a> Alex, D., F. Gay-Andrieu, J. May, L. Thampi, D. Dou, A. Mooney, W. Groutas, and R. Calderone. Antimicrobial Agents and Chemotherapy, 2012. 56(9): p. 4630-4639. PMID[22687516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22963878">Antibacterial and Antifungal Activities of Polyketide Metabolite from Marine Streptomyces Sp. Ap-123 and Its Cytotoxic Effect.</a> Arasu, M.V., V. Duraipandiyan, and S. Ignacimuthu. Chemosphere, 2012. <b>[Epub ahead of print]</b>. PMID[22963878].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22807132">Synthesis of Short Cationic Antimicrobial Peptidomimetics Containing Arginine Analogues.</a> Baldassarre, L., F. Pinnen, C. Cornacchia, E. Fornasari, L. Cellini, M. Baffoni, and I. Cacciatore. Journal of Peptide Science, 2012. 18(9): p. 567-578. PMID[22807132].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22759702">An Investigation of the Antimicrobial and Anti-inflammatory Activities of Crocodile Oil.</a> Buthelezi, S., C. Southway, U. Govinden, J. Bodenstein, and K. du Toit. Journal of Ethnopharmacology, 2012. 143(1): p. 325-330. PMID[22759702].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22294341">Essential Oil of Juniperus communis Subsp. Alpina (Suter) Celak Needles: Chemical Composition, Antifungal Activity and Cytotoxicity.</a> Cabral, C., V. Francisco, C. Cavaleiro, M.J. Goncalves, M.T. Cruz, F. Sales, M.T. Batista, and L. Salgueiro. Phytotherapy Research, 2012. 26(9): p. 1352-1357. PMID[22294341].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22892340">Synthesis and Antifungal Activity of Derivatives of 2- and 3-Benzofurancarboxylic acids.</a> Hejchman, E., K. Ostrowska, D. Maciejewska, J. Kossakowski, and W. Courchesne. The Journal of Pharmacology and Experimental Therapeutics, 2012. <b>[Epub ahead of print]</b>. PMID[22892340].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22945026">Fumigant Antifungal Activity of Myrtaceae Essential Oils and Constituents from Leptospermum petersonii against Three Aspergillus Species.</a> Kim, E. and I.K. Park. Molecules, 2012. 17(9): p. 10459-10469. PMID[22945026].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22963079">Brevianamides with Antitubercular Potential from a Marine-derived Isolate of Aspergillus versicolor.</a> Song, F., X. Liu, H. Guo, B. Ren, C. Chen, A.M. Piggott, K. Yu, H. Gao, Q. Wang, M. Liu, H. Dai, L. Zhang, and R.J. Capon. Organic Letters, 2012. <b>[Epub ahead of print]</b>. PMID[22963079].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22941481">Antimicrobial Aflatoxins from the Marine-derived Fungus Aspergillus flavus 092008.</a> Wang, H., Z. Lu, H.J. Qu, P. Liu, C. Miao, T. Zhu, J. Li, K. Hong, and W. Zhu. Archives of Pharmacal Research, 2012. 35(8): p. 1387-1392. PMID[22941481].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22960551">An Antimicrobial Evaluation of Plants Used for the Treatment of Respiratory Infections in Rural Maputaland,</a> Kwazulu-Natal, South Africa. York, T., S.F. van Vuuren, and H. de Wet. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>. PMID[22960551].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p>
    <p> </p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22952939">Activity of Trifluoperazine against Replicating, Non-replicating and Drug Resistant M. tuberculosis.</a> Advani, M.J., I. Siddiqui, P. Sharma, and H. Reddy. Plos One, 2012. 7(8): p. e44245. PMID[22952939]. <b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22957940">Astraodoric acid A-D: New Lanostane Triterpenes from Edible Mushroom Astraeus odoratus and Their Anti Mycobacterium tuberculosis H37ra and Cytotoxic Activity.</a> Arpha, K., C. Phosri, N. Suwannasai, W. Mongkolthanaruk, and S. Sodngam. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22957940].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22945019">Synthesis and Antitubercular Activity Evaluation of Novel Unsymmetrical Cyclohexane-1,2-diamine Derivatives.</a> Beena, S. Joshi, N. Kumar, S. Kidwai, R. Singh, and D.S. Rawat. Archiv der Pharmazie, 2012. <b>[Epub ahead of print]</b>. PMID[22945019].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22943408">Management of Difficult Multidrug-resistant Tuberculosis and Extensively Drug-resistant Tuberculosis: Update 2012.</a> Chang, K.C. and W.W. Yew. Respirology, 2012. <b>[Epub ahead of print]</b>. PMID[22943408].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22942435">Cyanovirin-N Inhibits Mannose-dependent Mycobacterium-C-type Lectin Interactions but Does Not Protect against Murine tuberculosis.</a> Driessen, N.N., H.I. Boshoff, J.J. Maaskant, S.A. Gilissen, S. Vink, A.M. van der Sar, C.M. Vandenbroucke-Grauls, C.A. Bewley, B.J. Appelmelk, and J. Geurtsen. Journal of Immunology, 2012. <b>[Epub ahead of print]</b>. PMID[22942435].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22765970">Salicylanilide Derivatives Block Mycobacterium tuberculosis through Inhibition of Isocitrate Lyase and Methionine Aminopeptidase.</a> Kratky, M., J. Vinsova, E. Novotna, J. Mandikova, V. Wsol, F. Trejtnar, V. Ulmann, J. Stolarikova, S. Fernandes, S. Bhat, and J.O. Liu. Tuberculosis, 2012. 92(5): p. 434-439. PMID[22765970].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22294548">Investigating the Effectiveness of St John&#39;s Wort Herb as an Antimicrobial Agent against Mycobacteria.</a> Mortensen, T., S. Shen, F. Shen, M.K. Walsh, R.C. Sims, and C.D. Miller. Phytotherapy Research, 2012. 26(9): p. 1327-1333. PMID[22294548].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22943459">Synthesis and Biological Evaluation of Coumarin Derivatives as Inhibitors of Mycobacterium bovis (Bcg).</a> Rezayan, A.H., P. Azarang, S. Sardari, and A. Sarvary. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>. PMID[22943459].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22832312">Green Synthesis and Anti-infective Activities of Fluorinated Pyrazoline Derivatives.</a> Shelke, S.N., G.R. Mhaske, V.D. Bonifacio, and M.B. Gawande. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5727-5730. PMID[22832312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22952596">Dominant Incidence of Multidrug and Extensively Drug-resistant Specific Mycobacterium tuberculosis Clones in Osaka Prefecture, Japan.</a> Tamaru, A., C. Nakajima, T. Wada, Y. Wang, M. Inoue, R. Kawahara, R. Maekura, Y. Ozeki, H. Ogura, K. Kobayashi, Y. Suzuki, and S. Matsumoto. Plos One, 2012. 7(8): p. e42505. PMID[22952596].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p>
    <p> </p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22950983">Selective and Potent Urea Inhibitors of Cryptosporidium parvum Inosine 5&#39;-Monophosphate Dehydrogenase.</a> Gorla, S.K., M. Kavitha, M. Zhang, X. Liu, L. Sharling, D.R. Gollapalli, B. Striepen, L. Hedstrom, and G.D. Cuny. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22950983].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0831-091312.</p>

    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306752700062">Enzymatic Reduction of 9-Methoxytariacuripyrone by Saccharomyces cerevisiae and Its Antimycobacterial Activity.</a> Alvarez-Fitz, P., L. Alvarez, S. Marquina, J. Luna-Herrera, and V.M. Navarro-Garcia. Molecules, 2012. 17(7): p. 8464-8470. ISI[000306752700062].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0831-091312.</p> 
    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306953200011">Anti-Microbial Activity of Leucas clarkei.</a> Das, S.N., V.J. Patro, and S.C. Dinda. Bangladesh Journal of Pharmacology, 2012. 7(2): p. 135-139. ISI[000306953200011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0831-091312.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306461800012">Calcofluor White Combination Antifungal Treatments for Trichophyton rubrum and Candida albicans.</a> Kingsbury, J.M., J. Heitman, and S.R. Pinnell. Plos One, 2012. 7(7): p. e39405. ISI[000306461800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0831-091312.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
