

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-99.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CWpsPbiM3z9zeXSgw6K3EniK822YiaTFVVIHJzF+jbQsC9LG58q8NaQ3OkXTPGkHuyY6BJMXTydPhhuQAxQ/nWrpgMFd2yP5zN3UlGN0Cc54D6sdShOvua8/B+E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E8042315" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-99-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57775   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Small interfering RNA effectively inhibits protein expression and negative strand RNA synthesis from a full-length hepatitis C virus clone</p>

    <p>          Prabhu, Ramesh, Vittal, Padmaja, Yin, Qinyan, Flemington, Erik, Garry, Robert, Robichaux, William H, and Dash, Srikanta</p>

    <p>          Journal of Medical Virology <b>2005</b>.  76(4): 511-519</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     57776   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro studies of cross-resistance mutations against two hepatitis C virus serine protease inhibitors, VX-950 and BILN 2061</p>

    <p>          Lin, C, Gates, CA, Rao, BG, Brennan, DL, Fulghum, JF, Luong, YP, Frantz, JD, Lin, K, Ma, S, Wei, YY, Perni, RB, and Kwong, AD</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087668&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087668&amp;dopt=abstract</a> </p><br />

    <p>3.     57777   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleotide analogs as novel anti-hepatitis B virus agents</p>

    <p>          Iyer, RP, Padmanabhan, S, Zhang, G, Morrey, JD, and Korba, BE</p>

    <p>          Curr Opin Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087397&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087397&amp;dopt=abstract</a> </p><br />

    <p>4.     57778   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel inhibitors of the hepatitis C virus NS3 proteinase</p>

    <p>          Bennett, F, Liu, YT, Saksena, AK, Arasappan, A, Butkiewicz, N, Dasmahapatra, B, Pichardo, JS, Njoroge, FG, Patel, NM, Huang, Y, and Yang, X</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087334&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087334&amp;dopt=abstract</a> </p><br />

    <p>5.     57779   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of cathepsin L prevent severe acute respiratory syndrome coronavirus entry</p>

    <p>          Simmons, G, Gosalia, DN, Rennekamp, AJ, Reeves, JD, Diamond, SL, and Bates, P</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081529&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     57780   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          2-5A antisense treatment of respiratory syncytial virus</p>

    <p>          Leaman, DW</p>

    <p>          Curr Opin Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081320&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081320&amp;dopt=abstract</a> </p><br />

    <p>7.     57781   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Strategies for containing an emerging influenza pandemic in Southeast Asia</p>

    <p>          Ferguson, NM, Cummings, DA, Cauchemez, S, Fraser, C, Riley, S, Meeyai, A, Iamsirithaworn, S, and Burke, DS</p>

    <p>          Nature <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079797&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079797&amp;dopt=abstract</a> </p><br />

    <p>8.     57782   DMID-LS-99; WOS-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Safety, Pharmacodynamic (Pd) and Pharmacokinetic (Pk) Profiles of Cpg 10101 (Actilon-Tm), a Novel Tlr9 Agonist: Comparison in Normal Volunteers and Hcv Infected Individuals</p>

    <p>          Bacon, B. <i>et al.</i></p>

    <p>          Gastroenterology <b>2005</b>.  128(4): A696</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305472">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228619305472</a> </p><br />

    <p>9.     57783   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Containing Pandemic Influenza at the Source</p>

    <p>          Longini, Jr IM, Nizam, A, Xu, S, Ungchusak, K, Hanshaoworakul, W, Cummings, DA, and Halloran, EM</p>

    <p>          Science <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079251&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079251&amp;dopt=abstract</a> </p><br />

    <p>10.   57784   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and synthesis of depeptidized macrocyclic inhibitors of hepatitis C NS3-4A protease using structure-based drug design</p>

    <p>          Venkatraman, S, Njoroge, FG, Girijavallabhan, VM, Madison, VS, Yao, NH, Prongay, AJ, Butkiewicz, N, and Pichardo, J</p>

    <p>          J Med Chem <b>2005</b>.  48(16): 5088-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078825&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16078825&amp;dopt=abstract</a> </p><br />

    <p>11.   57785   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Carbocyclic N-3 isonucleosides as SAHase inhibitors for antiviral chemotherapeutics</p>

    <p>          Bakke, Brian A, Mosley, Sylvester L, Sadler, Joshua M, Sunkara, Narsesh K, and Seley, Katherine L</p>

    <p>          Abstracts of Papers, 230th ACS National Meeting, Washington, DC, United States, Aug. 28-Sept. 1, 2005 <b>2005</b>.: MEDI-166</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   57786   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Utilization of the embryonated egg for in vivo evaluation of the anti-influenza virus activity of neuraminidase inhibitors</p>

    <p>          Sauerbrei, A, Haertl, A, Brandstaedt, A, Schmidtke, M, and Wutzler, P</p>

    <p>          Med Microbiol Immunol (Berl) <b>2005</b>.: 1-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059699&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059699&amp;dopt=abstract</a> </p><br />

    <p>13.   57787   DMID-LS-99; WOS-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Phase 2 Study to Assess Antiviral Response, Safety, and Pharmacokinetics of Albuferon(Tm) in Ifn-Oc Naive Subjects With Genotype 1 Chronic Hepatitis C</p>

    <p>          Bain, V. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000019</a> </p><br />

    <p>14.   57788   DMID-LS-99; WOS-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Telbivudine (Ldt) Preferentially Inhibits Second (Plus) Strand Hbv Dna Synthesis</p>

    <p>          Seifer, M. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2005</b>.  42: 151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000413">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000413</a> </p><br />

    <p>15.   57789   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral drug discovery and development: Where chemistry meets with biomedicine</p>

    <p>          De Clercq, E</p>

    <p>          Antiviral Res <b>2005</b>.  67(2): 56-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16046240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16046240&amp;dopt=abstract</a> </p><br />

    <p>16.   57790   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Perspective: Marburg and Ebola - Arming ourselves against the deadly filoviruses</p>

    <p>          Peters, CJ</p>

    <p>          New England Journal of Medicine <b>2005</b>.  352(25): 2571-2573</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   57791   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nitric oxide radical suppresses replication of wild-type dengue 2 viruses in vitro</p>

    <p>          Charnsilpa, W, Takhampunya, R, Endy, TP, Mammen, MP Jr, Libraty, DH, and Ubol, S</p>

    <p>          J Med Virol <b>2005</b>.  77(1): 89-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16032750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16032750&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   57792   DMID-LS-99; PUBMED-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p><b>          Chondroitin sulfate characterized by the E disaccharide unit is a potent inhibitor of herpes simplex virus infectivity and provides the virus binding sites on gro2c cells</b> </p>

    <p>          Bergefall, K, Trybala, E, Johansson, M, Uyama, T, Naito, S, Yamada, S, Kitagawa, H, Sugahara, K, and Bergstrom, T</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027159&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027159&amp;dopt=abstract</a> </p><br />

    <p>19.   57793   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of imidazo[4,5-c]pyridine derivatives as antiviral agents</p>

    <p>          Puerstinger, Gerhard, Bondy, Steven S, Dowdy, Eric Davis, Kim, Choung U, Oare, David A, Neyts, Johan, and Zia, Vahid</p>

    <p>          PATENT:  WO <b>2005063744</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004  PP: 265 pp.</p>

    <p>          ASSIGNEE:  (K. U. Leuven Research &amp; Development, Belg. and Gilead Sciences, Inc.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   57794   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral agents and methods of use</p>

    <p>          Striker, Robert T, Elfarra, Adnan A, Gunnarsdottir, Sjofn, and Hoover, Spencer W</p>

    <p>          PATENT:  US <b>2005119284</b>  ISSUE DATE:  20050602</p>

    <p>          APPLICATION: 2004-56321  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (Wisconsin Alumni Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   57795   DMID-LS-99; WOS-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amino Acid Ester Prodrugs of 2-Bromo-5,6-Dichloro-1-(Beta-D-Ribofuranosyl) Benzimidazole Enhance Metabolic Stability in Vitro and in Vivo</p>

    <p>          Lorenzi, P. <i>et al.</i></p>

    <p>          Journal of Pharmacology and Experimental Therapeutics <b>2005</b>.  314(2): 883-890</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230550300049</span></p>

    <p>22.   57796   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virus-encoded chemokine receptors - putative novel antiviral drug targets</p>

    <p>          Rosenkilde, Mette M</p>

    <p>          Neuropharmacology <b>2005</b>.  48(1): 1-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   57797   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of novel test compounds for antiviral chemotherapy of severe acute respiratory syndrome (SARS)</p>

    <p>          Kesel, Andreas J</p>

    <p>          Current Medicinal Chemistry <b>2005</b>.  12(18): 2095-2162</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>24.   57798   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of peptides as protease inhibitors</p>

    <p>          Yang, Syaulan, Wu, Jen-Dar, Su, Feng-Yih, Kuo, Chun-Wei, Chen, Wen-Chang, Hsu, Ming-Chu, Xiang, Yibin, Wang, Ching-Cheng, and Liao, Shao-Ying</p>

    <p>          PATENT:  US <b>2005143320</b>  ISSUE DATE:  20050630</p>

    <p>          APPLICATION: 2005  PP: 46 pp., Cont.-in-part of U.S. Ser. No. 24,929.</p>

    <p>          ASSIGNEE:  (Taigen Biotechnology, Taiwan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   57799   DMID-LS-99; SCIFINDER-DMID-8/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A preparation of mesylate salt of indole derivative, useful as antiviral agent</p>

    <p>          Glushkov, RG, Maksimov, VA, Mart&#39;yanov, VA, Khamitov, RA, and Shuster, AM</p>

    <p>          PATENT:  RU <b>2255086</b>  ISSUE DATE: 20050627</p>

    <p>          APPLICATION: 2004-46336  PP: No pp. given</p>

    <p>          ASSIGNEE:  (Zakrytoe Aktsionernoe Obshchestvo \&quot;Masterklon\&quot;, Russia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
