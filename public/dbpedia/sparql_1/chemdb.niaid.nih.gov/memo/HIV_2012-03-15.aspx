

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-03-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MZSSJW4GhRzuxcVONfMH3Ur9Y+e7LYRTVfvhQeVVvlGZyLb7Sj9m3PM2nKp20BrH/rxqVU47fTJIeGCukhFZLUdeBz3iMrbjPKkQkGPIDCJRF1An7uW57JoAdrE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52CEE252" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 2 - March 15, 2012</h1>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>
    
    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22404139">HIV-1 Protease with 20 Mutations Exhibits Extreme Resistance to Clinical Inhibitors Through Coordinated Structural Rearrangements.</a> Agniswamy, J., C.H. Shen, A. Aniana, J.M. Louis, J.M. Sayer, and I.T. Weber, Biochemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22404139].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22404160">Targeting Frameshifting in the Human Immunodeficiency Virus.</a> Brakier-Gingras, L., J. Charbonneau, and S.E. Butcher, Expert Opinion on Therapeutic Targets, 2012. <b>[Epub ahead of print]</b>; PMID[22404160].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22407016">Diverse HIV Viruses are Targeted by a Conformationally Dynamic Antiviral.</a> Caines, M.E., K. Bichel, A.J. Price, W.A. McEwan, G.J. Towers, B.J. Willett, S.M. Freund, and L.C. James, Nature Structural &amp; Molecular Biology, 2012. <b>[Epub ahead of print]</b>; PMID[22407016].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22401672">Potent Antiviral HIV-1 Protease Inhibitor GRL-02031 Adapts to the Structures of Drug Resistant Mutants with Its P1&#39;-Pyrrolidinone Ring.</a> Chang, Y.C., X. Yu, Y. Zhang, Y. Tie, Y.F. Wang, S. Yashchuk, A.K. Ghosh, R.W. Harrison, and I.T. Weber, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22401672].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22403678">Biophysical Property and Broad anti-HIV Activity of Albuvirtide, a 3-Maleimimidopropionic acid-modified Peptide Fusion Inhibitor.</a> Chong, H., X. Yao, C. Zhang, L. Cai, S. Cui, Y. Wang, and Y. He, Plos One, 2012. 7(3): p. e32599; PMID[22403678].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22403408">Peptides from the Second Extracellular Loop of the C-C Chemokine Receptor Type 5 (CCR5) Inhibit Diverse Strains of HIV-1.</a> Dogo-Isonagie, C., S. Lam, E. Gustchina, P. Acharya, Y. Yang, S. Shahzad-Ul-Hussan, G.M. Clore, P.D. Kwong, and C.A. Bewley, The Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22403408].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22404213">Identification of Cellular Proteins Required for Replication of Human Immunodeficiency Virus Type 1.</a> Dziuba, N., M. Ferguson, W.A. O&#39;Brien, A. Sanchez, A.J. Prussia, N. McDonald, B.M. Friedrich, G. Li, M.W. Shaw, J. Sheng, T.W. Hodge, D.H. Rubin, and J.L. Murray, AIDS Research and Human Retroviruses, 2012. <b>[Epub ahead of print]</b>; PMID[22404213].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22364812">Substituent Effects on P2-Cyclopentyltetrahydrofuranyl urethanes: Design, Synthesis, and X-ray Studies of Potent HIV-1 Protease Inhibitors.</a> Ghosh, A.K., B.D. Chapsal, M. Steffey, J. Agniswamy, Y.F. Wang, M. Amano, I.T. Weber, and H. Mitsuya, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(6): p. 2308-2311; PMID[22364812].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22403253">Engagement of NKp30 on Vdelta1 T-cells Induces the Production of CCL3, CCL4 and CCL5 and Suppresses HIV-1 Replication.</a> Hudspeth, K., M. Fogli, D.V. Correia, J. Mikulak, A. Roberto, S. Della Bella, B. Silva-Santos, and D. Mavilio, Blood, 2012. <b>[Epub ahead of print]</b>; PMID[22403253].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22407670">HIV Protease Inhibitors Modulate Ca(2+) Homeostasis and Potentiate Alcoholic Stress and Injury in Mice and Primary Mouse and Human Hepatocytes.</a> Kao, E., M. Shinohara, M. Feng, M.Y. Lau, and C. Ji, Hepatology, 2012. <b>[Epub ahead of print]</b>; PMID[22407670].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22401805">Novel anti-HIV-1 Activity Produced by Conjugating Unsulfated Dextran with Polyl-lysine.</a> Nakamura, K., T. Ohtsuki, H. Mori, H. Hoshino, A. Hoque, A. Oue, F. Kanou, H. Sakagami, K.I. Tanamoto, H. Ushijima, N. Kawasaki, H. Akiyama, and H. Ogawa, Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[22401805].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22395700">Quantifying the Activity of anti-HIV Treatment in Silico.</a> Ribeiro, R.M., Nature Medicine, 2012. 18(3): p. 355-356; PMID[22395700].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22396600">Transmembrane Domain Membrane Proximal External Region but Not Surface Unit-directed Broadly Neutralizing HIV-1 Antibodies Can Restrict Dendritic Cell-mediated HIV-1 Trans-infection.</a> Sagar, M., H. Akiyama, B. Etemad, N. Ramirez, I. Freitas, and S. Gummuluru, Journal of Infectious Diseases, 2012. <b>[Epub ahead of print]</b>; PMID[22396600].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22405323">New and Novel Intrinsic Host Repressive Factors against HIV-1: PAF1 Complex, HERC5 and Others.</a> Tyagi, M. and F. Kashanchi, Retrovirology, 2012. 9(1): p. 19; PMID[22405323].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22376008">Synthesis, X-ray Analysis, and Biological Evaluation of a New Class of Stereopure Lactam-based HIV-1 Protease Inhibitors.</a> Wu, X., P. Ohrngren, A.A. Joshi, A. Trejos, M. Persson, R.K. Arvela, H. Wallberg, L. Vrang, A. Rosenquist, B.B. Samuelsson, J. Unge, and M. Larhed, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22376008].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22414543">Catechins Containing a Galloyl Moiety as Potential anti-HIV-1 Compounds.</a> Zhao, Y., F. Jiang, P. Liu, W. Chen, and K. Yi, Drug Discovery Today, 2012. <b>[Epub ahead of print]</b>; PMID[22414543].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22391529">L-selectin and P-selectin Are Novel Biomarkers of Cervicovaginal Inflammation for Preclinical Mucosal Safety Assessment of anti-HIV-1 Microbicide.</a> Zhong, M., B. He, J. Yang, R. Bao, Y. Zhang, D. Zhou, Y. Chen, L. Li, C. Han, Y. Yang, Y. Sun, Y. Cao, Y. Li, W. Shi, S. Jiang, X. Zhang, and H. Yan, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22391529].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22235115">Trimeric, Coiled-coil Extension on Peptide Fusion Inhibitor of HIV-1 Influences Selection of Resistance Pathways.</a> Zhuang, M., W. Wang, C.J. De Feo, R. Vassell, and C.D. Weiss, Journal of Biological Chemistry, 2012. 287(11): p. 8297-8309; PMID[22235115].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22258251">Combination of Biological Screening in a Cellular Model of Viral Latency and Virtual Screening Identifies Novel Compounds that Reactivate HIV-1.</a> Gallastegui, E., B. Marshall, D. Vidal, G. Sanchez-Duffhues, J.A. Collado, C. Alvarez-Fernandez, N. Luque, J.M. Terme, J.M. Gatell, S. Sanchez-Palomino, E. Munoz, J. Mestres, E. Verdin, and A. Jordan, Journal of Virology, 2012. 86(7): p. 3795-3808; PMID[22258251].
    <br />
    <b>[PubMed]</b>. HIV_0302-031512.</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300409000008">Combination of Antiretroviral Drugs as Microbicides</a>. Balzarini, J. and D. Schols, Current HIV Research, 2012. 10(1): p. 53-60; PMID[WOS:000300409000008].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300074600004">Denaturation of HIV-1 Protease (PR) Monomer by Acetic acid: Mechanistic and Trajectory Insights from Molecular Dynamics Simulations and NMR</a>. Borkar, A., M. Rout, and R. Hosur, Journal of Biomolecular Structure &amp; Dynamics, 2012. 29(5): p. 893-903; PMID[WOS:000300074600004].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300029000011">Thymidine- and AZT-linked 5-(1,3-Dioxoalkyl)tetrazoles and 4-(1,3-Dioxoalkyl)-1,2,3-triazoles.</a> Bosch, L., O. Delelis, F. Subra, E. Deprez, M. Witvrow, and J. Vilarrasa, Tetrahedron Letters, 2012. 53(5): p. 514-518; PMID[WOS:000300029000011].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300364700014">Synthesis of 1,5-Benzothiazepine Derivatives Bearing 2-Phenoxy-quinoline Moiety via 1,3-Diplolar Cycloaddition Reaction.</a> Dong, Z.Q., F.M. Liu, F. Xu, and Z.L. Yuan, Molecular Diversity, 2011. 15(4): p. 963-970; PMID[WOS:000300364700014].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299909300014">Phenols with anti-HIV Activity from Daphne acutiloba.</a> Huang, S.Z., X. Zhang, X. Li, H. Jiang, Q. Ma, P. Wang, Y. Liu, J. Hu, Y. Zheng, J. Zhou, and Y. Zhao, Planta Medica, 2012. 78(2): p. 182-185; PMID[WOS:000299909300014].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300339700024">Evaluation of Competing Process Concepts for the Production of Pure Enantiomers.</a> Kaspereit, M., S. Swernath, and A. Kienle, Organic Process Research &amp; Development, 2012. 16(2): p. 353-363; PMID[WOS:000300339700024].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299859600009">A QSAR Study on a Series of N-Methyl pyrimidones Acting as HIV Integrase Inhibitors.</a> Kaushik, S., S. Gupta, P. Sharma, and Z. Anwer, Indian Journal of Biochemistry &amp; Biophysics, 2011. 48(6): p. 427-434; PMID[WOS:000299859600009].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300407400005">Recent Advances in the Development of Small-molecule Compounds Targeting HIV-1 gp41 as Membrane Fusion Inhibitors.</a> Kawashita, N., Y.S. Tian, U.C. de Silva, K. Okamoto, and T. Takagi, Mini-Reviews in Organic Chemistry, 2012. 9(1): p. 20-26; PMID[WOS:000300407400005].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300201300037">Three New Arylnaphthalene Lignans from Schisandra propinqua var. sinensis.</a> Li, X.N., C. Lei, L.M. Yang, H.M. Li, S.X. Huang, X. Du, J.X. Pu, W.L. Xiao, Y.T. Zheng, and H.D. Sun, Fitoterapia, 2012. 83(1): p. 249-252; PMID[WOS:000300201300037].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300322700008">Involvement of K(v)1.3 and p38 MAPK Signaling in HIV-1 Glycoprotein 120-induced Microglia Neurotoxicity.</a> Liu, J., C. Xu, L. Chen, P. Xu, and H. Xiong, Cell Death &amp; Disease, 2012. 3; PMID[WOS:000300322700008].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299949500016">Quinoxalinone Inhibitors of the Lectin DC-SIGN.</a> Mangold, S.L., L. Prost, and L. Kiessling, Chemical Science, 2012. 3(3): p. 772-777; PMID[WOS:000299949500016].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300228300025">HIV-1 Subtype and Virological Response to Antiretroviral Therapy: Acquired Drug Resistance.</a> Soares, E.A., A.F. Santos, and M.A. Soares, Clinical Infectious Diseases, 2012. 54(5): p. 738-738; PMID[WOS:000300228300025].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300284100001">Dihydro-alkoxyl-benzyl-oxopyrimidine Derivatives (DABOs) As Non-nucleoside Reverse Transcriptase Inhibitors: An Update Review (2001-2011).</a> Yang, S.Q., F.E. Chen, and E. De Clercq, Current Medicinal Chemistry, 2012. 19(2): p. 152-162; PMID[WOS:000300284100001].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>

    <p class="memofmt2-2">Patent citations</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120111&amp;CC=CN&amp;NR=102311380A&amp;KC=A">Piperidine-4-Carboxamide Derivatives as Ccr5 Antagonists and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of HIV Infection and Autoimmune Diseases.</a> Liu, T., X. Xie, Y. Hu, Z. Weng, and W. Wei. Patent. 2012. 2010-10275734 102311380: 17pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120119&amp;CC=WO&amp;NR=2012006680A1&amp;KC=A1.">7,9-Nitrogen radical-4-oxo-4h-Pyrido[1,2-B]pyrimidine-2-carboxylic acid Benzyl amides as Antiviral Agents and Their Preparation.</a> Rhodes, D.I., J.J. Deadman, G.T. Le, N.A. Van De Graff, L. Lu, X. Li, X. Feng, and C. Yu. Patent. 2012. 2011-AU894 2012006680: 118pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120209&amp;CC=WO&amp;NR=2012019003A1&amp;KC=A1">Indolyl- and Azaindolyloxoacetylpiperazines and Related Compounds and Their Preparation and Use for the Treatment of HIV Infection.</a>. Wang, T., Z. Zhang, Z. Yin, J.F. Kadow, and N.A. Meanwell. Patent. 2012. 2011-US46589 2012019003: 221pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0217-030112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
