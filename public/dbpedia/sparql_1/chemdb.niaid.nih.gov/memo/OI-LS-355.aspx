

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-355.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yrMHOc9aUpqMXm/maGOtahj7Q71L9kc2WmYNW8HB1Q7PfZaZ1YDhkboRbyHpaE52fBzWXbYgw9ffY3c8GbgD2bFDqdZ6LfL4pcYZp+Nze8j0WcRMNlmjqFQEW3E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="154064F6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-355-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58762   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Antimicrobial peptides in the airway</p>

    <p>          Laube, DM, Yim, S, Ryan, LK, Kisich, KO, and Diamond, G</p>

    <p>          Curr Top Microbiol Immunol <b>2006</b>.  306: 153-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16909921&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16909921&amp;dopt=abstract</a> </p><br />

    <p>2.     58763   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Transfer of a point mutation in Mycobacterium tuberculosis inhA resolves the target of isoniazid</p>

    <p>          Vilcheze, C, Wang, F, Arai, M, Hazbon, MH, Colangeli, R, Kremer, L, Weisbrod, TR, Alland, D, Sacchettini, JC, and Jacobs, WR Jr</p>

    <p>          Nat Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16906155&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16906155&amp;dopt=abstract</a> </p><br />

    <p>3.     58764   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Cytostatic and antiviral 6-arylpurine ribonucleosides. Part 7: Synthesis and evaluation of 6-substituted purine l-ribonucleosides</p>

    <p>          Hocek, M, Silhar, P, Shih, IH, Mabery, E, and Mackman, R</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905315&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905315&amp;dopt=abstract</a> </p><br />

    <p>4.     58765   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Novel targets for tuberculosis drug discovery</p>

    <p>          Mdluli, K and Spigelman, M</p>

    <p>          Curr Opin Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904376&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904376&amp;dopt=abstract</a> </p><br />

    <p>5.     58766   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Antiviral Effect of Artemisinin from Artemisia annua against a Model Member of the Flaviviridae Family, the Bovine Viral Diarrhoea Virus (BVDV)</p>

    <p>          Romero, MR, Serrano, MA, Vallejo, M, Efferth, T, Alvarez, M, and Marin, JJ</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16902856&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16902856&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58767   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Inhibition of hcv replication in HCV replicon by shRNAs</p>

    <p>          Hamazaki, H, Takahashi, H, Shimotohno, K, Miyano-Kurosaki, N, and Takaku, H</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(7): 801-805</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16898418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16898418&amp;dopt=abstract</a> </p><br />

    <p>7.     58768   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Oral nanoparticle-based antituberculosis drug delivery to the brain in an experimental model</p>

    <p>          Pandey, Rajesh and Khuller, GK</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(6): 1146-1152</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     58769   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          NADP+ Expels both the Co-factor and a Substrate Analog from the Mycobacterium tuberculosis ThyX Active Site: Opportunities for Anti-bacterial Drug Design</p>

    <p>          Sampathkumar, Parthasarathy, Turley, Stewart, Sibley, Carol Hopkins, and Hol, Wim GJ</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  360(1): 1-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     58770   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          On an aspect of calculated molecular descriptors in QSAR studies of quinolone antibacterials</p>

    <p>          Ghosh, P, Thanadath, M, and Bagchi, MC</p>

    <p>          Mol Divers <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16896544&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16896544&amp;dopt=abstract</a> </p><br />

    <p>10.   58771   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Selective intracellular accumulation of the major metabolite issued from the activation of the prodrug ethionamide in mycobacteria</p>

    <p>          Hanoulle, X, Wieruszeski, JM, Rousselot-Pailley, P, Landrieu, I, Locht, C, Lippens, G, and Baulard, AR</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895935&amp;dopt=abstract</a> </p><br />

    <p>11.   58772   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of quinoline derivatives and their use as mycobacterial inhibitors</p>

    <p>          Koul, Anil and Andries, Koenraad Jozef Lodewijk Marcel</p>

    <p>          PATENT:  CA <b>2529265</b>  ISSUE DATE: 20060624</p>

    <p>          APPLICATION: 2005  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (Janssen Pharmaceutica N.V., Belg.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   58773   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Ligand-Based Virtual Screening, Parallel Solution-Phase and Microwave-Assisted Synthesis as Tools to Identify and Synthesize New Inhibitors of Mycobacterium tuberculosis</p>

    <p>          Manetti, F, Magnani, M, Castagnolo, D, Passalacqua, L, Botta, M, Corelli, F, Saddi, M, Deidda, D, and De, Logu A</p>

    <p>          ChemMedChem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892466&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892466&amp;dopt=abstract</a> </p><br />

    <p>13.   58774   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Kinetic modeling of tricarboxylic acid cycle and glyoxylate bypass in Mycobacterium tuberculosis, and its application to assessing drug targets</p>

    <p>          Singh, VK and Ghosh, I</p>

    <p>          Theor Biol Med Model <b>2006</b>.  3(1): 27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16887020&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16887020&amp;dopt=abstract</a> </p><br />

    <p>14.   58775   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Functional and Structural Characterization of a Thiol Peroxidase from Mycobacterium tuberculosis</p>

    <p>          Rho, BS, Hung, LW, Holton, JM, Vigil, D, Kim, SI, Park, MS, Terwilliger, TC, and Pedelacq, JD</p>

    <p>          J Mol Biol <b>2006</b>.  361(5): 850-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884737&amp;dopt=abstract</a> </p><br />

    <p>15.   58776   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Discovery of new antitubercular oxazolyl thiosemicarbazones</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, Thirumurugan, Rathinasababathy, and Pavana, Roheet Kumar</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(12): 3448-3450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58777   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis and Mycobacterium avium Inhibit IFN- gamma -Induced Gene Expression by TLR2-Dependent and Independent Pathways</p>

    <p>          Lafuse, WP, Alvarez, GR, Curry, HM, and Zwilling, BS</p>

    <p>          J Interferon Cytokine Res <b>2006</b>.  26(8): 548-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16881865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16881865&amp;dopt=abstract</a> </p><br />

    <p>17.   58778   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Joshi, Bipin C, Veres, Tracey, Kubin, Martin, and Trischman, Jacqueline A</p>

    <p>          &lt;10 Journal Title&gt; <b>2006</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   58779   OI-LS-355; PUBMED-OI-8/21/2006</p>

    <p class="memofmt1-2">          Prospects for advancing tuberculosis control efforts through novel therapies</p>

    <p>          Salomon, JA, Lloyd-Smith, JO, Getz, WM, Resch, S, Sanchez, MS, Porco, TC, and Borgdorff, MW</p>

    <p>          PLoS Med <b>2006</b>.  3(8): e273</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16866578&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16866578&amp;dopt=abstract</a> </p><br />

    <p>19.   58780   OI-LS-355; WOS-OI-8/13/2006</p>

    <p class="memofmt1-2">          Bioactive steroidal saponins from Smilax medica</p>

    <p>          Sautour, M, Miyarrioto, T, and Lacaille-Dubois, MA</p>

    <p>          PLANTA MEDICA <b>2006</b>.  72(7): 667-670, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239243100019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239243100019</a> </p><br />

    <p>20.   58781   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity of picolinic acid against extracellular and intracellular Mycobacterium avium complex and its combined activity with clarithromycin, rifampicin and fluoroquinolones</p>

    <p>          Cai, Shanshan, Sato, Katsumasa, Shimizu, Toshiaki, Yamabe, Seiko, Hiraki, Miho, Sano, Chiaki, and Tomioka, Haruaki</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(1): 85-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   58782   OI-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis and biological activity of 4-thiazolidinones, thiosemicarbazides derived from diflunisal hydrazide</p>

    <p>          Kuecuekguezel, Gueniz, Kocatepe, Ayla, De Clercq, Erik, Sahin, Fikrettin, and Guelluece, Medine</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(3): 353-359</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   58783   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          1-N-(arylmaleamoyl)-3,5-bis(phenylmethylene)-4-piperidones: A novel class of antimycobacterial agents</p>

    <p>          Jha, A and Dimmock, JR</p>

    <p>          Pharmazie <b>2006</b>.  61(6): 562-563</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   58784   OI-LS-355; WOS-OI-8/13/2006</p>

    <p class="memofmt1-2">          Benzimidazole derivatives bearing substituted biphenyls as hepatitis C virus NS5B RNA-dependent RNA polymerase inhibitors: Structure-activity relationship studies and identification of a potent and highly selective inhibitor JTK-109</p>

    <p>          Hirashima, S, Suzuki, T, Ishida, T, Noji, S, Yata, S, Ando, I, Komatsu, M, Ikeda, S, and Hashimoto, H</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(15): 4721-4736, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239141500032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239141500032</a> </p><br />

    <p>24.   58785   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Antimycobacterial activities of pyrethrins and their Diels-Alder products</p>

    <p>          Andrews, Melanie Kadine and Rugutt, Joseph K</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: CHED-996</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>25.   58786   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of 1,2,3-trisubstituted-1,4-dihydrobenzo[g]quinoxaline-5,10-diones and related compounds as antifungal and antibacterial agents</p>

    <p>          Tandon, Vishnu K, Yadav, Dharmendra B, Maurya, Hardesh K, Chaturvedi, Ashok K, and Shukla, Praveen K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(17): 6120-6126</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   58787   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Foundations of antibiotic resistance in bacterial physiology: the mycobacterial paradigm</p>

    <p>          Nguyen, L and Thompson, CJ</p>

    <p>          TRENDS IN MICROBIOLOGY <b>2006</b>.  14(7): 304-312, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239369500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239369500006</a> </p><br />

    <p>27.   58788   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Prioritizing genomic drug targets in pathogens: Application to Mycobacterium tuberculosis</p>

    <p>          Hasan, S, Daugelat, S, Rao, PSS, and Schreiber, M</p>

    <p>          PLOS COMPUTATIONAL BIOLOGY <b>2006</b>.  2(6): 539-550, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239494000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239494000007</a> </p><br />

    <p>28.   58789   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-C-methylcytidine and 2&#39;-C-methyluridine derivatives modified in the 3&#39;-position as potential antiviral agents</p>

    <p>          Pierra, Claire, Amador, Agnes, Badaroux, Eric, Storer, Richard, and Gosselin, Gilles</p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(7): 991-1010</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   58790   OI-LS-355; SCIFINDER-OI-8/14/2006</p>

    <p class="memofmt1-2">          Valopicitabine: anti-hepatitis C virus drug RNA-directed RNA polymerase (NS5B) inhibitor</p>

    <p>          Sorbera, LA, Castaner, J, and Leeson, PA</p>

    <p>          Drugs of the Future <b>2006</b>.  31(4): 320-324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   58791   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Dual role of isocitrate lyase 1 in the glyoxylate and methylcitrate cycles in Mycobacterium tuberculosis</p>

    <p>          Gould, TA, de Langemheen, HV, Munoz-Elias, EJ, McKinney, JD, and Sacchettini, JC</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2006</b>.  61(4): 940-947, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336100009</a> </p><br />

    <p>31.   58792   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Phytochemistry and antifungal properties of the newly discovered tree Pleodendron costaricense</p>

    <p>          Amiguet, VT, Petit, P, Ta, CA, Nunez, R, Sanchez-Vindas, P, Alvarez, LP, Smith, ML, Arnason, JT, and Durst, T</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(7): 1005-1009, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336000005</a> </p><br />
    <br clear="all">

    <p>32.   58793   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Towards establishing a method to screen for inhibitors of essential genes in mycobacteria: evaluation of the acetamidase promoter</p>

    <p>          Raghunand, TR, Bishai, WR, and Chen, P</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2006</b>.  28(1): 36-41, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239417500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239417500006</a> </p><br />

    <p>33.   58794   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Azole antifungals as novel chemotherapeutic agents against murine tuberculosis</p>

    <p>          Ahmad, Z, Sharma, S, and Khuller, GK</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2006</b>.  261(2): 181-186, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239333800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239333800004</a> </p><br />

    <p>34.   58795   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Neoplaether, a new cytotoxic and antifungal endophyte metabolite from Neoplaconema napellum IFB-E016</p>

    <p>          Wang, FW, Ye, YH, Chen, JR, Wang, XT, Zhu, HL, Song, YC, and Tan, RX</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2006</b>.  261(2): 218-223, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239333800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239333800009</a> </p><br />

    <p>35.   58796   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus entry: An intriguing challenge for drug discovery</p>

    <p>          Meanwell, NA</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2006</b>.  7(8): 727-732, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239425000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239425000006</a> </p><br />

    <p>36.   58797   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Drug evaluation: BAL-8557 - a novel broad-spectrum triazole antifungal</p>

    <p>          Odds, FC</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2006</b>.  7(8): 766-772, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239425000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239425000011</a> </p><br />

    <p>37.   58798   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Isoniazid is not a lead compound for its pyridyl ring derivatives, isonicotinoyl amides, hydrazides, and hydrazones: A critical review</p>

    <p>          Scior, T and Garces-Eisele, SJ</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2006</b>.  13(18): 2205-2219, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239258200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239258200011</a> </p><br />

    <p>38.   58799   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Synthesis of 13-(substituted benzyl) berberine and berberrubine derivatives as antifungal agents</p>

    <p>          Park, KD, Lee, JH, Kim, SH, Kang, TH, Moon, JS, and Kim, SU</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(15): 3913-3916, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239291900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239291900006</a> </p><br />
    <br clear="all">

    <p>39.   58800   OI-LS-355; WOS-OI-8/20/2006</p>

    <p class="memofmt1-2">          Cloning, expression, purification, crystallization and preliminary X-ray diffraction analysis of DapC (Rv0858c) from Mycobacterium tuberculosis</p>

    <p>          Weyand, S, Kefala, G, and Weiss, MS</p>

    <p>          ACTA CRYSTALLOGRAPHICA SECTION F-STRUCTURAL BIOLOGY AND CRYSTALLIZATION COMMUNICATIONS <b>2006</b>.  62 : 794-797, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239357900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239357900022</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
