

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-04-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RuFAabm2HKRGXXpJ6o/mZHT+c+KsSeSnvhtrznDq1/OUNUW74FoT9GHCMn6xVPZQ2AcITv+PK/Tvoj0y6l6fClLUMP49+imsd63Tz7Lj3uMSf4LD3AeQBFUWDFo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3995A972" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 13 - April 26, 2012</h1>

    <h2>Hepatitis B virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22520261">Synthesis, Biological Evaluation and Structure-Activity Relationships of Glycyrrhetinic acid Derivatives as Novel anti-Hepatitis B Virus Agents.</a> Wang, L.J., C.A. Geng, Y.B. Ma, X.Y. Huang, J. Luo, H. Chen, X.M. Zhang, and J.J. Chen, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22520261].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22532686">Isoflavone Agonists of IRF-3 Dependent Signaling have Antiviral Activity against RNA Viruses.</a> Bedard, K.M., M.L. Wang, S.C. Proll, Y.M. Loo, M.G. Katze, M. Gale, Jr., and S.P. Iadonato, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22532686].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22530910">Synthesis, in Vitro and in Silico NS5B Polymerase Inhibitory Activity of Benzimidazole Derivatives.</a> Patil, V.M., K.R. Gurukumar, M. Chudayeu, S.P. Gupta, S. Samanta, N. Masand, and N. Kaushik-Basu, Medicinal Chemistry (Shariqah (United Arab Emirates)), 2012. <b>[Epub ahead of print]</b>; PMID[22530910].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22513121">5-Acetyl-2-arylbenzimidazoles as Antiviral Agents. Part 4.</a> Vitale, G., P. Corona, M. Loriga, A. Carta, G. Paglietti, G. Giliberti, G. Sanna, P. Farci, M.E. Marongiu, and P. La Colla, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22513121].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508787">In Vitro and ex Vivo Delivery of Short Hairpin RNAs for Control of Hepatitis C Viral Transcript Expression.</a> Lonze, B.E., H.T. Holzer, M.K. Knabel, J.E. Locke, G.A. Dicamillo, S.S. Karhadkar, R.A. Montgomery, Z. Sun, D.S. Warren, and A.M. Cameron, Archives of Surgery (Chicago, Ill. : 1960), 2012. 147(4): p. 384-387; PMID[22508787].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22507961">Synthesis and Biological Evaluation of New Potent and Selective HCV NS5A Inhibitors.</a> Shi, J., L. Zhou, F. Amblard, D.R. Bobeck, H. Zhang, P. Liu, L. Bondada, T.R. McBrayer, P.M. Tharnish, T. Whitaker, S.J. Coats, and R.F. Schinazi, Bioorganic and Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22507961].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22497816">Anti-hepatitis C Virus Activity of 3-Hydroxy Caruilignan C from Swietenia macrophylla Stems.</a> Wu, S.F., C.K. Lin, Y.S. Chuang, F.R. Chang, C.K. Tseng, Y.C. Wu, and J.C. Lee, Journal of Viral Hepatitis, 2012. 19(5): p. 364-370; PMID[22497816].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22522395">Anti-tumour Promoting Activity and Antioxidant Properties of Girinimbine Isolated from the Stem Bark of Murraya koenigii S.</a> Kok, Y.Y., L.Y. Mooi, K. Ahmad, M.A. Sukari, N. Mat, M. Rahmani, and A.M. Ali, Molecules (Basel, Switzerland), 2012. 17(4): p. 4651-4660; PMID[22522395].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <h2>HSV-1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22528269">Diastereoselective Multicomponent Synthesis and anti-HSV-1 Evaluation of Dihydrofuran-fused Derivatives.</a> Scala, A., M. Cordaro, F. Risitano, I. Colao, A. Venuti, M.T. Sciortino, P. Primerano, and G. Grassi, Molecular Diversity, 2012. <b>[Epub ahead of print]</b>; PMID[22528269].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22498878">The Anti-herpetic Activity of Trichosanthin via the Nuclear Factor-kappaB and p53 Pathways.</a> He, D., Y. Zheng, and S. Tam, Life Sciences, 2012. <b>[Epub ahead of print]</b>; PMID[22498878].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>

    <h2>HSV-2</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22531715">Identification of OASL d, a Splice Variant of Human OASL, with Antiviral Activity.</a> Guo, X., X. Li, Y. Xu, T. Sun, G. Yang, Z. Wu, and E. Li, The International Journal of Biochemistry &amp; Cell Biology, 2012. <b>[Epub ahead of print]</b>; PMID[22531715].
    <br />
    <b>[PubMed]</b>. OV_0413-042612.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300817600005">Antihepatitis B Virus Constituents of Solanum erianthum</a>. Chou, S.C., T.J. Huang, E.H. Lin, C.H. Huang, and C.H. Chou, Natural Product Communications, 2012. 7(2): p. 153-156; ISI[000300817600005].
    <br />
    <b>[WOS]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301759200008">Reducing Agents Affect Inhibitory Activities of Compounds: Results from Multiple Drug Targets.</a> Lee, H., J. Torres, L. Truong, R. Chaudhuri, A. Mittal, and M.E. Johnson, Analytical Biochemistry, 2012. 423(1): p. 46-53; ISI[000301759200008].
    <br />
    <b>[WOS]</b>. OV_0413-042612.</p>
    <p> </p>
    
    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301846100053">Discovery of GS-9451: An Acid Inhibitor of the Hepatitis C Virus NS3/4A Protease.</a> Sheng, X.C., T. Appleby, T. Butler, R.B. Cai, X.W. Chen, A.S. Cho, M.O. Clarke, J. Cottell, W.E. Delaney, E. Doerffler, J. Link, M.Z. Ji, R. Pakdaman, H.J. Pyun, Q.Y. Wu, J. Xu, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2629-2634; ISI[000301846100053].
    <br />
    <b>[WOS]</b>. OV_0413-042612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
