

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-02-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JMwBbSN7X4ahFBodb3irHFx+PF9uohFubTNHUTtvlCT/aNnCSfNZ5ODlHvow+uCWb5nO94evron5TbofD56Vx1XKWQW+eMyprxbbAt7atp1MBgrsXXMxL5vIpdw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="03B9AAD8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 1 - February 14, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22917277">Emtricitabine Prodrugs with Improved anti-HIV Activity and Cellular Uptake.</a> Agarwal, H.K., B.S. Chhikara, S. Bhavaraju, D. Mandal, G.F. Doncel, and K. Parang. Molecular Pharmaceutics, 2013. 10(2): p. 467-476. PMID[22917277].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23017010">Comparison of the Molecular Dynamics and Calculated Binding Free Energies for Nine FDA-approved HIV-1 PR Drugs against Subtype B and C-SA HIV PR.</a> Ahmed, S.M., H.G. Kruger, T. Govender, G.E. Maguire, Y. Sayed, M.A. Ibrahim, P. Naicker, and M.E. Soliman. Chemical Biology &amp; Drug Design, 2013. 81(2): p. 208-218. PMID[23017010].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23403426">GRL-0519, a Novel Oxatricyclic-ligand-containing Nonpeptidic HIV-1 Protease Inhibitor (PI), Potently Suppresses the Replication of a Wide Spectrum of Multi-PI-resistant HIV-1 Variants in Vitro.</a> Amano, M., Y. Tojo, P.M. Salcedo-Gomez, J.R. Campbell, D. Das, M. Aoki, C.X. Xu, K.V. Rao, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23403426].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22961335">Covalent Fusion Inhibitors Targeting HIV-1 gp41 Deep Pocket.</a> Bai, Y., H. Xue, K. Wang, L. Cai, J. Qiu, S. Bi, L. Lai, M. Cheng, S. Liu, and K. Liu. Amino Acids, 2013. 44(2): p. 701-713. PMID[22961335].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147734">A Rationally Engineered anti-HIV Peptide Fusion Inhibitor with Greatly Reduced Immunogenicity.</a> Brauer, F., K. Schmidt, R.C. Zahn, C. Richter, H.H. Radeke, J.E. Schmitz, D. von Laer, and L. Egerer. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 679-688. PMID[23147734].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23379607">New Betulinic Acid Derivatives for Bevirimat-resistant Human Immunodeficiency Virus Type-1.</a> Dang, Z., P. Ho, L. Zhu, K. Qian, K.H. Lee, L. Huang, and C.H. Chen. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23379607].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22924614">Antiviral Activity of Retrocyclin RC-101, a Candidate Microbicide against Cell-associated HIV-1.</a> Gupta, P., C. Lackman-Smith, B. Snyder, D. Ratner, L.C. Rohan, D. Patton, B. Ramratnam, and A.M. Cole. Aids Research and Human Retroviruses, 2013. 29(2): p. 391-396. PMID[22924614].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23403432">Efficient Single Tobamoviral Vector-based Bioproduction of Broadly Neutralizing anti-HIV-1 Monoclonal Antibody VRC01 in Nicotiana Plants and Its Utility in Combination Microbicides.</a> Hamorsky, K.T., T.W. Grooms-Williams, A.S. Husk, L.J. Bennett, K.E. Palmer, and N. Matoba. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23403432].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>
    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23368966">Antiviral Phenolic Compounds from Arundina gramnifolia.</a> Hu, Q.F., B. Zhou, J.M. Huang, X.M. Gao, L.D. Shu, G.Y. Yang, and C.T. Che. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23368966].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23378232">Design, Synthesis and Biological Evaluation of Indolizine Derivatives as HIV-1 VIF-ElonginC Interaction Inhibitors.</a> Huang, W., T. Zuo, H. Jin, Z. Liu, Z. Yang, X. Yu, L. Zhang, and L. Zhang. Molecular Diversity, 2013. <b>[Epub ahead of print]</b>. PMID[23378232].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23239505">Non-natural Peptide Triazole Antagonists of HIV-1 Envelope gp120.</a> Kamanna, K., R. Aneja, C. Duffy, P. Kubinski, D. Rodrigo Moreira, L.D. Bailey, K. McFadden, A. Schon, A. Holmes, F. Tuzer, M. Contarino, E. Freire, and I.M. Chaiken. ChemMedChem, 2013. 8(2): p. 322-328. PMID[23239505].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22845664">CXCR4-tropic, but Not CCR5-tropic, Human Immunodeficiency Virus Infection Is Inhibited by the Lipid Raft-associated Factors, Acyclic Retinoid Analogs, and Cholera Toxin B Subunit.</a> Kamiyama, H., K. Kakoki, S. Shigematsu, M. Izumida, Y. Yashima, Y. Tanaka, H. Hayashi, T. Matsuyama, H. Sato, N. Yamamoto, T. Sano, Y. Shidoji, and Y. Kubo. Aids Research and Human Retroviruses, 2013. 29(2): p. 279-288. PMID[22845664].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23369356">Biological Profile of the Less Lipophilic and Synthetically More Accessible Bryostatin 7 Closely Resembles That of Bryostatin 1.</a> Kedei, N., N.E. Lewin, T. Geczy, J. Selezneva, D.C. Braun, J. Chen, M.A. Herrmann, M.R. Heldman, L. Lim, P. Mannan, S.H. Garfield, Y.B. Poudel, T.J. Cummins, A. Rudra, P.M. Blumberg, and G.E. Keck. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23369356].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23290051">Inhibition of HIV-1 Replication by a Tricyclic Coumarin GUT-70 in Acutely and Chronically Infected Cells.</a> Kudo, E., M. Taura, K. Matsuda, M. Shimamoto, R. Kariya, H. Goto, S. Hattori, S. Kimura, and S. Okada. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(3): p. 606-609. PMID[23290051].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23387294">Effect of Combination Antiretroviral Therapy on Chinese Rhesus Macaques of SIV Infection.</a> Ling, B., L.B. Rogers, A.M. Johnson, M. Piatak, J. Lifson, and R. Veazey. Aids Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23387294].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23152526">Trimeric Glycosylphosphatidylinositol-anchored HCDR3 of Broadly Neutralizing Antibody PG16 Is a Potent HIV-1 Entry Inhibitor.</a> Liu, L., W. Wang, L. Yang, H. Ren, J.T. Kimata, and P. Zhou. Journal of Virology, 2013. 87(3): p. 1899-1905. PMID[23152526].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23165744">Purification and Characterization of a Novel Antifungal Protein with Antiproliferation and anti-HIV-1 Reverse Transcriptase Activities from Peganum harmala Seeds.</a> Ma, X., D. Liu, H. Tang, Y. Wang, T. Wu, Y. Li, J. Yang, J. Yang, S. Sun, and F. Zhang. Acta Biochimica et Biophysica Sinica, 2013. 45(2): p. 87-94. PMID[23165744].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23384676">Complete and Repeatable Inactivation of HIV-1 Viral Particles in Suspension Using a Photo-labeled Non-nucleoside Reverse Transcriptase Inhibitor.</a> Marin-Muller, C., A. Rios, D. Anderson, E. Siwak, and Q. Yao. Journal of Virological Methods, 2013. <b>[Epub ahead of print]</b>. PMID[23384676].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22380777">Synthesis and HIV-1 RT Inhibitory Action of Novel (4/6-substituted Benzo[d]thiazol -2-yl)thiazolidin-4-ones. Divergence from the Non-competitive Inhibition Mechanism.</a> Pitta, E., A. Geronikaki, S. Surmava, P. Eleftheriou, V.P. Mehta, and E.V. Van der Eycken. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(1): p. 113-122. PMID[22380777].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23132334">In Vitro Investigations into the Roles of Drug Transporters and Metabolizing Enzymes in the Disposition and Drug Interactions of Dolutegravir, a HIV Integrase Inhibitor.</a> Reese, M.J., P.M. Savina, G.T. Generaux, H. Tracey, J.E. Humphreys, E. Kanaoka, L.O. Webster, K.A. Harmon, J.D. Clarke, and J.W. Polli. Drug Metabolism and Disposition: The Biological Fate of Chemicals, 2013. 41(2): p. 353-361. PMID[23132334].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23374053">Inhibitors of Human Immunodeficiency Virus Type 1 (HIV-1) Attachment 13. Synthesis and Profiling of a Novel Amminium Prodrug of the HIV-1 Attachment Inhibitor BMS-585248.</a> Regueiro-Ren, A., J. Simmermacher-Mayer, M. Sinz, K.A. Johnson, X.S. Huang, S. Jenkins, D. Parker, S. Rahematpura, M. Zheng, N.A. Meanwell, and J.F. Kadow. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23374053].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23280502">Some Hydrazones of 2-Aroylamino-3-methylbutanohydrazide: Synthesis, Molecular Modeling Studies, and Identification as Stereoselective Inhibitors of HIV-1.</a> Tatar, E., I. Kucukguzel, D. Daelemans, T.T. Talele, N. Kaushik-Basu, E. De Clercq, and C. Pannecouque. Archiv der Pharmazie, 2013. 346(2): p. 140-153. PMID[23280502].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23011758">HIV-1 ENV gp120 Structural Determinants for Peptide Triazole Dual Receptor Site Antagonism.</a> Tuzer, F., N. Madani, K. Kamanna, I. Zentner, J. Lalonde, A. Holmes, E. Upton, S. Rajagopal, K. McFadden, M. Contarino, J. Sodroski, and I. Chaiken. Proteins, 2013. 81(2): p. 271-290. PMID[23011758].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23385524">Potent Inhibition of HIV-1 Reverse Transcriptase and Replication by Nonpseudoknot, &quot;UCAA-motif&quot; RNA Aptamers.</a> Whatley, A.S., M.A. Ditzler, M.J. Lange, E. Biondi, A.W. Sawyer, J.L. Chang, J.D. Franken, and D.H. Burke. Molecular Therapy. Nucleic Acids, 2013. 2: p. e71. PMID[23385524].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23385314">In Vitro Analysis and Quantitative Prediction of Efavirenz Inhibition of Eight Cytochrome P450 (CYP) Enzymes: Major Effects on CYPs 2B6, 2C8, 2C9 and 2C19.</a> Xu, C. and Z. Desta. Drug Metabolism and Pharmacokinetics, 2013. <b>[Epub ahead of print]</b>. PMID[23385314].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23403120">Antiviral Peptide Nanocomplexes as a Potential Therapeutic Modality for HIV/HCV Co-infection.</a> Zhang, J., A. Mulvenon, E. Makarov, J. Wagoner, J. Knibbe, J.O. Kim, N. Osna, T.K. Bronich, and L.Y. Poluektova. Biomaterials, 2013. <b>[Epub ahead of print]</b>. PMID[23403120].
    <br />
    <b>[PubMed]</b>. HIV_0201-021413.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313065200006">1,2,4-Triazine Chemistry Part III: Synthetic Strategies to Functionalized Bridgehead Nitrogen Heteroannulated 1,2,4-Triazine Systems and Their Regiospecific and Pharmacological Properties.</a> Abdel-Rahman, R.M., M.S.T. Makki, T.E. Ali, and M.A. Ibrahim. Current Organic Synthesis, 2013. 10(1): p. 136-160. ISI[000313065200006].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313197200067">Analysis of Small Molecule Ligands Targeting the HIV-1 Matrix protein-RNA Binding Site.</a> Alfadhli, A., H. McNett, J. Eccles, S. Tsagli, C. Noviello, R. Sloan, C.S. Lopez, D.H. Peyton, and E. Barklis. Journal of Biological Chemistry, 2013. 288(1): p. 666-676. ISI[000313197200067].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313036800002">Positional Adaptability in the Design of Mutation-resistant Nonnucleoside HIV-1 Reverse Transcriptase Inhibitors: A Supramolecular Perspective.</a> Bruccoleri, A. Aids Research and Human Retroviruses, 2013. 29(1): p. 4-12. ISI[000313036800002].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312829100075">Anti-HIV-1 Activity of Elafin Depends on Its Nuclear Localization and Altered Innate Immune Activation in Female Genital Epithelial Cells.</a> Drannik, A.G., K. Nag, X.D. Yao, B.M. Henrick, T.B. Ball, F.A. Plummer, C. Wachihi, J. Kimani, and K.L. Rosenthal. Plos One, 2012. 7(12). ISI[000312829100075].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313036800013">Amniotic Fluid Exhibits an Innate Inhibitory Activity against HIV Type 1 Replication in Vitro.</a> Farzin, A., P. Boyer, B. Ank, K. Nielsen-Saines, and Y. Bryson. Aids Research and Human Retroviruses, 2013. 29(1): p. 77-83. ISI[000313036800013].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311869600001">Crystal Structures of HIV-1 Reverse Transcriptase with Picomolar Inhibitors Reveal Key Interactions for Drug Design.</a> Frey, K.M., M. Bollini, A.C. Mislak, J.A. Cisneros, R. Gallardo-Macias, W.L. Jorgensen, and K.S. Anderson. Journal of the American Chemical Society, 2012. 134(48): p. 19501-19503. ISI[000311869600001].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312942500006">Inhibitors of HIV-1 Integrase-human LEDGF/p75 Interaction Identified from Natural Products Via Virtual Screening.</a> Hu, G.P., X. Li, Y.Z. Li, X.Q. Sun, G.X. Liu, W.H. Li, J. Huang, X. Shen, and Y. Tang. Chinese Journal of Chemistry, 2012. 30(12): p. 2752-2758. ISI[000312942500006].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313154400018">Synthesis and Conformation of Novel 4 &#39;-Fluorinated 5 &#39;-deoxythreosyl phosphonic acid Nucleosides as Antiviral Agents.</a> Kang, L., E. Kim, E.J. Choi, J.C. Yoo, W. Lee, and J.H. Hong. Bulletin of the Korean Chemical Society, 2012. 33(12): p. 4007-4014. ISI[000313154400018].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312621500057">Isolation and Biological Activities of Furanoflavones from the Roots of Codonopsis cordifolioidea.</a> Leng, H.Q., G.Y. Yang, Y.K. Chen, Y.D. Guo, and Z.Y. Chen. Asian Journal of Chemistry, 2013. 25(4): p. 2024-2026. ISI[000312621500057].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313322900014">A One-pot Transition-metal-free Tandem Process to 1,4-Benzodiazepine Scaffolds.</a> Li, Y.Q., C.J. Zhan, B.C. Yang, X.Q. Cao, and C. Ma. Synthesis-Stuttgart, 2013. 45(1): p. 111-117. ISI[000313322900014].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312621500023">Phenolic Compounds from Clinopodium urticifolium and Their Antivirus Activities.</a> Pan, C., J. Huang, Z.R. Gao, W. Feng, G.Y. Yang, Y.G. Chen, and Z. Li. Asian Journal of Chemistry, 2013. 25(4): p. 1860-1862. ISI[000312621500023].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312620300059">Microwave Assisted, One Pot Synthesis of Alkyl-3-amino-2-arylimino-1,3-thiazolan-4-ones-5-ylidene acetate.</a> Pourshamsian, K., N. Montazeri, S. Ahamadi, and T. Noghani. Asian Journal of Chemistry, 2013. 25(1): p. 306-308. ISI[000312620300059].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313071500004">Benzophenone Derivatives of Pyrimidines as Effective Non-nucleoside Inhibitors of Wild-type and Drug-resistant HIV-1 Reverse Transcriptase.</a> Prokofjeva, M.M., V.T. Valuev-Elliston, A.V. Ivanov, S.N. Kochetkov, M.S. Novikov, and V.S. Prassolov. Doklady Biochemistry and Biophysics, 2012. 447(1): p. 280-281. ISI[000313071500004].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313414800036">HRP2 Determines the Efficiency and Specificity of HIV-1 Integration in LEDGF/p75 Knockout Cells but Does Not Contribute to the Antiviral Activity of a Potent LEDGF/p75-binding Site Integrase Inhibitor.</a> Wang, H., K.A. Jurado, X.L. Wu, M.C. Shun, X. Li, A.L. Ferris, S.J. Smith, P.A. Patel, J.R. Fuchs, P. Cherepanov, M. Kvaratskhelia, S.H. Hughes, and A. Engelman. Nucleic Acids Research, 2012. 40(22): p. 11518-11530. ISI[000313414800036].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313155800051">The Effect of Graphene Oxide on Conformation Change, Aggregation and Cytotoxicity of HIV-1 Regulatory Protein (Vpr).</a> Zhang, M., X.B. Mao, C.X. Wang, W.F. Zeng, C.L. Zhang, Z.J. Li, Y. Fang, Y.L. Yang, W. Liang, and C. Wang. Biomaterials, 2013. 34(4): p. 1383-1390. ISI[000313155800051].
    <br />
    <b>[WOS]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130110&amp;CC=WO&amp;NR=2013006792A1&amp;KC=A1">Preparation of Amino acid Amides as Antiviral Agents.</a> Chen, P., D. Zhou, S. Shao, and Z.-W. Cai. Patent. 2013. 2012-US45761 2013006792: 92pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130117&amp;CC=US&amp;NR=2013018049A1&amp;KC=A1">Preparation of Azaindole Compounds and Methods for Treating HIV.</a> de La Rosa, M.A., B.A. Johns, E.J. Velthuisen, J. Weatherhead, and V. Samano. Patent. 2013. 2012-547199 20130018049: 167pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">44. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130117&amp;CC=WO&amp;NR=2013009844A2&amp;KC=A2">Processes for Preparing C-3 Substituted Bicyclooctane Based HIV Protease Inhibitors.</a> Ghosh, A.K., B.D. Chapsal, and H. Mitsuya. Patent. 2013. 2012-US46216 2013009844: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0201-021413.</p>

    <br />

    <p class="plaintext">45. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130103&amp;CC=WO&amp;NR=2013002357A1&amp;KC=A1">Preparation of Pyridinones and Related Compounds as HIV Replication Inhibitors.</a> Hattori, K., N. Kurihara, T. Iwaki, T. Inoue, T. Akiyama, and Y. Hasegawa. Patent. 2013. 2012-JP66642 2013002357: 367pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0201-021413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
