

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-144.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+CVUYm50YsX6fcY4NwwcEMS6f7Pb3Wr39qL4ZmLEp6Cvz/aGeIWB+n8J+QXA8k1Zh2XsaOJDQ0wxQESD/Cpr9B+PfU4+gsLYyVBgBnLEGXVoI5C37CI6Ko/INhM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2682AE74" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-144-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60785   DMID-LS-144; EMBASE-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Efficacy of Oral CMX-001 Therapy Against Human Herpes Virus-6 Infections in SCID-hu Mice</p>

    <p>          Quenelle, Debra, Prichard, Mark, Daily, Shannon, Collins, Deborah, Rice, Terri, Painter, George, Robertson, Alice, and Kern, Earl</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A70-4068</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3R/2/f080554fbc09bd89ac5bd6f61a0848ee">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3R/2/f080554fbc09bd89ac5bd6f61a0848ee</a> </p><br />

    <p>2.     60786   DMID-LS-144; EMBASE-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Imidazo[1,2-a]pyridines with potent activity against herpesviruses</p>

    <p>          Gudmundsson, Kristjan S and Johns, Brian A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(10): 2735-2739</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N5TN59-G/2/a475568333fce8852dfb0f4afaba49c4">http://www.sciencedirect.com/science/article/B6TF9-4N5TN59-G/2/a475568333fce8852dfb0f4afaba49c4</a> </p><br />

    <p>3.     60787   DMID-LS-144; EMBASE-DMID-5/7/2007</p>

    <p><b>          Pyrazolo[1,5-a]pyridine antiherpetics: Effects of the C3 substituent on antiviral activity</b> </p>

    <p>          Johns, Brian A, Gudmundsson, Kristjan S, and Allen, Scott H</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(10): 2858-2862</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N4J2Y0-3/2/2c1dcc2ae42eedb75bf9e6a4bc61f469">http://www.sciencedirect.com/science/article/B6TF9-4N4J2Y0-3/2/2c1dcc2ae42eedb75bf9e6a4bc61f469</a> </p><br />

    <p>4.     60788   DMID-LS-144; EMBASE-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Synthesis, antiviral and antitumor activity of 2-substituted-5-amidino-benzimidazoles</p>

    <p>          Starcevic, Kristina, Kralj, Marijeta, Ester, Katja, Sabol, Ivan, Grce, Magdalena, Pavelic, Kresimir, and Karminski-Zamola, Grace</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NK4G35-5/2/55ff6e019c62766096cda2f006f68cf5">http://www.sciencedirect.com/science/article/B6TF8-4NK4G35-5/2/55ff6e019c62766096cda2f006f68cf5</a> </p><br />

    <p>5.     60789   DMID-LS-144; EMBASE-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Avian influenza A (H5N1) infection: targets and strategies for chemotherapeutic intervention</p>

    <p>          De Clercq, Erik and Neyts, Johan</p>

    <p>          Trends in Pharmacological Sciences <b>2007</b>.  In Press, Corrected Proof: 2862</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1K-4NN1T9W-1/2/15898dd2f890f173ceeb5e8b7c09863a">http://www.sciencedirect.com/science/article/B6T1K-4NN1T9W-1/2/15898dd2f890f173ceeb5e8b7c09863a</a> </p><br />

    <p>6.     60790   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          HBV and HCV co-infections in HIV positive patients in the &quot;HAART era&quot;: new challenges</p>

    <p>          Laufer, NL, Quarleri, JF, Bouzas, MB, Perez, HM, Salomon, H, and Cahn, PE</p>

    <p>          MEDICINA-BUENOS AIRES <b>2007</b>.  67(1): 82-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245192400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245192400016</a> </p><br />
    <br clear="all">

    <p>7.     60791   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Characterization of full-length hepatitis C virus genotype 4 sequences</p>

    <p>          Timm, J, Neukamm, M, Kuntzen, T, Kim, AY, Chung, RT, Brander, C, Lauer, GM, Walker, BD, and Allen, TM</p>

    <p>          JOURNAL OF VIRAL HEPATITIS <b>2007</b>.  14(5): 330-337</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245614200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245614200006</a> </p><br />

    <p>8.     60792   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Inhibition of subgenomic hepatitis C virus RNA replication in HeLa cells</p>

    <p>          Kanda, T, Yokosuka, O, Mikata, R, Zhang, KY, Tanaka, M, Tada, M, Fukai, K, Imazeki, F, and Saisho, H</p>

    <p>          HEPATO-GASTROENTEROLOGY <b>2007</b>.  54(73): 32-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245252200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245252200008</a> </p><br />

    <p>9.     60793   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Phylogeny and evolution of papillomaviruses based on the E1 and E2 proteins</p>

    <p>          Bravo, IG and Alonso, A</p>

    <p>          VIRUS GENES <b>2007</b>.  34(3): 249-262</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245177300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245177300003</a> </p><br />

    <p>10.   60794   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Understanding avian influenza</p>

    <p>          Palmer-Holtry, K</p>

    <p>          VETERINARY TECHNICIAN <b>2007</b>.  28(3): 152-+</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245566400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245566400002</a> </p><br />

    <p>11.   60795   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          N-(3,3a,4,4a,5,5a,6,6a-octahydro-1,3-dioxo-4,6-ethenocycloprop[f]isoindo l-2-(1H)-yl)carboxamides: Identification of novel orthopoxvirus egress inhibitors</p>

    <p>          Bailey, TR, Rippin, SR, Opsitnick, E, Burns, CJ, Pevear, DC, Collett, MS, Rhodes, G, Tohan, S, Huggins, JW, Baker, RO, Kern, ER, Keith, KA, Dai, DC, Yang, G, Hruby, D, and Jordan, R</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  50(7): 1442-1444</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245259000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245259000002</a> </p><br />

    <p>12.   60796   DMID-LS-144; WOS-DMID-5/7/2007</p>

    <p class="memofmt1-2">          IFN mimetic as a therapeutic for lethal vaccinia virus infection: Possible effects on innate and adaptive immune responses</p>

    <p>          Ahmed, CM, Martin, JP, and Johnson, HM</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2007</b>.  178(7): 4576-4583</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245197300066">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245197300066</a> </p><br />
    <br clear="all">

    <p>13.   60797   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Some alpha-[6-(1&#39;-Carbamoylalkylthio)-1 H-Pyrazolo[3,4-D]Pyrimidin-4-yl]Thioalkylcarboxamide Acyclonucleosides</p>

    <p>          Moukha-Chafiq, O, Taha, ML, Mouna, A, Lazrek, HB, Vasseur, JJ, and De, Clercq E</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(4): 335-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17479430&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17479430&amp;dopt=abstract</a> </p><br />

    <p>14.   60798   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Different pH requirements are associated with divergent inhibitory effects of chloroquine on human and avian influenza A viruses</p>

    <p>          Di Trani, L, Savarino, A, Campitelli, L, Norelli, S, Puzelli, S, D&#39;Ostilio, D, Vignolo, E, Donatelli, I, and Cassone, A</p>

    <p>          Virol J <b>2007</b> .  4(1): 39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17477867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17477867&amp;dopt=abstract</a> </p><br />

    <p>15.   60799   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Telbivudine: a new option for the treatment of chronic hepatitis B</p>

    <p>          Ruiz-Sancho, A, Sheldon, J, and Soriano, V</p>

    <p>          Expert Opin Biol Ther <b>2007</b>.  7(5): 751-761</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17477811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17477811&amp;dopt=abstract</a> </p><br />

    <p>16.   60800   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p><b>          Isolation, identification and function of a novel anti-HSV-1 protein from Grifola frondosa</b> </p>

    <p>          Gu, CQ, Li, JW, Chao, F, Jin, M, Wang, XW, and Shen, ZQ</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475344&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475344&amp;dopt=abstract</a> </p><br />

    <p>17.   60801   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Non-Nucleoside Inhibitor of Measles Virus RNA-Dependent RNA Polymerase Complex Activity</p>

    <p>          White, LK, Yoon, JJ, Lee, JK, Sun, A, Du, Y, Fu, H, Snyder, JP, and Plemper, RK</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17470652&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17470652&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   60802   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p><b>          Functional determinants of NS2B for activation of Japanese encephalitis virus NS3 protease</b> </p>

    <p>          Lin, CW, Huang, HD, Shiu, SY, Chen, WJ, Tsai, MH, Huang, SH, Wan, L, and Lin, YJ</p>

    <p>          Virus Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17467838&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17467838&amp;dopt=abstract</a> </p><br />

    <p>19.   60803   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Synthesis of Pyrazolo[4&#39;,3&#39;:5,6]pyrano[2,3-d]pyrimidine Derivatives for Antiviral Evaluation</p>

    <p>          Shamroukh, AH, Zaki, ME, Morsy, EM, Abdel-Motti, FM, and Abdel-Megeid, FM</p>

    <p>          Arch Pharm (Weinheim) <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17464958&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17464958&amp;dopt=abstract</a> </p><br />

    <p>20.   60804   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Purification of 3 monomeric monocot mannose-binding lectins and their evaluation for antipoxviral activity: potential applications in multiple viral diseases caused by enveloped viruses</p>

    <p>          Kaur, A, Kamboj, SS, Singh, J, Singh, R, Abrahams, M, Kotwal, GJ, and Saxena, AK</p>

    <p>          Biochem Cell Biol <b>2007</b>.  85(1): 88-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17464348&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17464348&amp;dopt=abstract</a> </p><br />

    <p>21.   60805   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Entecavir: A new nucleoside analogue for the treatment of chronic hepatitis B</p>

    <p>          Rivkin, A</p>

    <p>          Drugs Today (Barc) <b>2007</b>.  43(4): 201-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17460784&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17460784&amp;dopt=abstract</a> </p><br />

    <p>22.   60806   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Pandemic (avian) influenza</p>

    <p>          Rajagopal, S and Treanor, J</p>

    <p>          Semin Respir Crit Care Med <b>2007</b>.  28(2): 159-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458770&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458770&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   60807   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Influenza: evolving strategies in treatment and prevention</p>

    <p>          Lynch, JP 3rd and Walsh, EE</p>

    <p>          Semin Respir Crit Care Med <b>2007</b>.  28(2): 144-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458769&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458769&amp;dopt=abstract</a> </p><br />

    <p>24.   60808   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          The chloroxoquinolinic derivative 6-chloro-1,4-dihydro-4-oxo-1-(beta-D-ribofuranosyl) quinoline-3-carboxylic acid inhibits HSV-1 adsorption by impairing its adsorption on HVEM</p>

    <p>          Souza, TM, De, Souza MC, Ferreira, VF, Canuto, CV, Marques, IP, Fontes, CF, and Frugulhetti, IC</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17458622&amp;dopt=abstract</a> </p><br />

    <p>25.   60809   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Racemic Synthesis and Antiviral Evaluation of 4&#39;(alpha)-Hydroxymethyl and 6&#39;(alpha)-Methyl Substituted Apiosyl Nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(3): 291-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454738&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17454738&amp;dopt=abstract</a> </p><br />

    <p>26.   60810   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          New therapeutic approaches for influenza A H5N1 infected humans</p>

    <p>          Ward, PA</p>

    <p>          Crit Care Med <b>2007</b>.  35(5): 1437-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446748&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446748&amp;dopt=abstract</a> </p><br />

    <p>27.   60811   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Sho-Saiko-To (Xiao-Chai-Hu-Tang) and Crude Saikosaponins Inhibit Hepatitis B Virus in a Stable HBV-Producing Cell Line</p>

    <p>          Chang, JS, Wang, KC, Liu, HW, Chen, MC, Chiang, LC, and Lin, CC</p>

    <p>          Am J Chin Med <b>2007</b>.  35(2): 341-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17436373&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17436373&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   60812   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          The efficacy of viral capsid inhibitors in human enterovirus infection and associated diseases</p>

    <p>          Li, C, Wang, H, Shih, SR, Chen, TC, and Li, ML</p>

    <p>          Curr Med Chem <b>2007</b>.  14(8): 847-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430140&amp;dopt=abstract</a> </p><br />

    <p>29.   60813   DMID-LS-144; PUBMED-DMID-5/7/2007</p>

    <p class="memofmt1-2">          Inhibition of severe acute respiratory syndrome-associated coronavirus infection by equine neutralizing antibody in golden Syrian hamsters</p>

    <p>          Zhao, G, Ni, B, Jiang, H, Luo, D, Pacal, M, Zhou, L, Zhang, L, Xing, L, Zhang, L, Jia, Z, Lin, Z, Wang, L, Li, J, Liang, Y, Shi, X, Zhao, T, Zhou, L, Wu, Y, and Wang, X</p>

    <p>          Viral Immunol <b>2007</b>.  20(1): 197-205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425434&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17425434&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
