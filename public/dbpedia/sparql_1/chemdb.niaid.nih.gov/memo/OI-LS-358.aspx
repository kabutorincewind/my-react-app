

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-358.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vyRtwWC/A1P7KldhSIyyT82HTaLdX5FQWLDv70rBJMaLi+I5d6KRD5js+TRPzq5uy6qzj0znqLinGmKzZNwDlH5GctDOlluOQ2/KJS58IOX11E+oAsckijmc7/I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EF0BB9DE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-358-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59074   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Construction of a eukaryotic expression system for granulysin and its protective effect in mice infected with Mycobacterium tuberculosis</p>

    <p>          Liu, B, Liu, S, Qu, X, and Liu, J</p>

    <p>          J Med Microbiol <b>2006</b>.  55(Pt 10): 1389-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005788&amp;dopt=abstract</a> </p><br />

    <p>2.     59075   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Design, synthesis, biochemical evaluation and antimycobacterial action of phosphonate inhibitors of antigen 85C, a crucial enzyme involved in biosynthesis of the mycobacterial cell wall</p>

    <p>          Gobec, Stanislav, Plantan, Ivan, Mravljak, Janez, Svajger, Urban, Wilson, Rosalind A, Besra, Gurdyal S, Soares, Sousana L, Appelberg, Rui, and Kikelj, Danijel</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4M0J48C-3/2/37bdcfa11408c67ae4f741c11ef4d3cb">http://www.sciencedirect.com/science/article/B6VKY-4M0J48C-3/2/37bdcfa11408c67ae4f741c11ef4d3cb</a> </p><br />

    <p>3.     59076   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Novel non-classical C9-methyl-5-substituted-2,4-diaminopyrrolo[2,3-d]pyrimidines as potential inhibitors of dihydrofolate reductase and as anti-opportunistic agents</p>

    <p>          Gangjee, Aleem, Yang, Jie, and Queener, Sherry F</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-5/2/48723d446bf53848558bbec32b8956c2">http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-5/2/48723d446bf53848558bbec32b8956c2</a> </p><br />

    <p>4.     59077   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Expression and characterization of alpha-(1,4)-glucan branching enzyme Rv1326c of Mycobacterium tuberculosis H37Rv</p>

    <p>          Garg, SK, Alam, MS, Kishan, KV, and Agrawal, P</p>

    <p>          Protein Expr Purif <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17005418&amp;dopt=abstract</a> </p><br />

    <p>5.     59078   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of substituted phenylmethyl- and pyridylmethyl amines</p>

    <p>          Tripathi, RP, Saxena, Nisha, Tiwari, VK, Verma, SS, Chaturvedi, Vinita, Manju, YK, Srivastva, AK, Gaikwad, A, and Sinha, S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-8/2/86742df2be75e5c836bd3e5fe4a895f6">http://www.sciencedirect.com/science/article/B6TF8-4M0J4DK-8/2/86742df2be75e5c836bd3e5fe4a895f6</a> </p><br />
    <br clear="all">

    <p>6.     59079   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          9-{[3-Fluoro-2-(hydroxymethyl)cyclopropylidene]methyl}adenines and -guanines. Synthesis and Antiviral Activity of All Stereoisomers(1)</p>

    <p>          Zhou, S, Kern, ER, Gullen, E, Cheng, YC, Drach, JC, Tamiya, S, Mitsuya, H, and Zemlicka, J</p>

    <p>          J Med Chem <b>2006</b>.  49(20): 6120-6128</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004726&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004726&amp;dopt=abstract</a> </p><br />

    <p>7.     59080   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Pyrazolidine-3,5-diones and 5-Hydroxy-1H-pyrazol-3(2H)-ones, Inhibitors of UDP-N-acetylenolpyruvyl Glucosamine Reductase</p>

    <p>          Gilbert, AM, Failli, A, Shumsky, J, Yang, Y, Severin, A, Singh, G, Hu, W, Keeney, D, Petersen, PJ, and Katz, AH</p>

    <p>          J Med Chem <b>2006</b>.  49(20): 6027-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004716&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17004716&amp;dopt=abstract</a> </p><br />

    <p>8.     59081   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          In vitro activity of[no-break space]terbinafine against[no-break space]Indian clinical isolates of[no-break space]Candida[no-break space]albicans and[no-break space]non-albicans using a[no-break space]macrodilution method</p>

    <p>          Garg, S, Naidu, J, Singh, SM, Nawange, SR, Jharia, N, and Saxena, M</p>

    <p>          Journal de Mycologie Medicale/Journal of Medical Mycology <b>2006</b>.  16(3): 119-125</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7RN8-4KXVD8H-3/2/07ae72d52fe05a93e83f4265ded6c7d0">http://www.sciencedirect.com/science/article/B7RN8-4KXVD8H-3/2/07ae72d52fe05a93e83f4265ded6c7d0</a> </p><br />

    <p>9.     59082   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Evaluation of the flora of Northern Mexico for in vitro antimicrobial and antituberculosis activity</p>

    <p>          Molina-Salinas, GM, Perez-Lopez, A, Becerril-Montes, P, Salazar-Aranda, R, Said-Fernandez, S, and Torres, NW</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000069&amp;dopt=abstract</a> </p><br />

    <p>10.   59083   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Design, synthesis and activity against Toxoplasma gondii, Plasmodium spp., and Mycobacterium tuberculosis of new 6-fluoroquinolones</p>

    <p>          Anquetin, G, Greiner, J, Mahmoudi, N, Santillana-Hayat, M, Gozalbes, R, Farhati, K, Derouin, F, Aubry, A, Cambau, E, and Vierling, P</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000032&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17000032&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59084   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Control of human cytomegalovirus gene expression by differential histone modifications during lytic and latent infection of a monocytic cell line</p>

    <p>          Ioudinkova, E, Arcangeletti, MC, Rynditch, A, De, Conto F, Motta, F, Covan, S, Pinardi, F, Razin, SV, and Chezzi, C</p>

    <p>          Gene <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989963&amp;dopt=abstract</a> </p><br />

    <p>12.   59085   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Comparison of lanosterol-14alpha-demethylase (CYP51) of human and Candida albicans for inhibition by different antifungal azoles</p>

    <p>          Trosken, ER, Adamska, M, Arand, M, Zarn, JA, Patten, C, Volkel, W, and Lutz, WK</p>

    <p>          Toxicology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989930&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16989930&amp;dopt=abstract</a> </p><br />

    <p>13.   59086   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Antiviral activity of CHO-SS cell-derived human omega interferon and other human interferons against HCV RNA replicons and related viruses</p>

    <p>          Buckwold, VE, Wei, J, Huang, Z, Huang, C, Nalca, A, Wells, J, Russell, J, Collins, B, Ptak, R, Lang, W, Scribner, C, Blanchett, D, Alessi, T, and Langecker, P</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16987555&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16987555&amp;dopt=abstract</a> </p><br />

    <p>14.   59087   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          A randomized trial of pegylated-interferon-alpha2a plus ribavirin with or without amantadine in the re-treatment of patients with chronic hepatitis C not responding to standard interferon and ribavirin</p>

    <p>          Ciancio, A, Picciotto, A, Giordanino, C, Smedile, A, Tabone, M, Manca, A, Marenco, G, Garbagnoli, P, Andreoni, M, Cariti, G, Calleri, G, Sartori, M, Cusumano, S, Grasso, A, Rizzi, R, Gallo, M, Basso, M, Anselmo, M, Percario, G, Ciccone, G, Rizzetto, M, and Saracco, G</p>

    <p>          Aliment Pharmacol Ther <b>2006</b>.  24(7): 1079-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984502&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984502&amp;dopt=abstract</a> </p><br />

    <p>15.   59088   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          7,8-Diaminoperlargonic acid aminotransferase from Mycobacterium tuberculosis, a potential therapeutic target</p>

    <p>          Mann, S and Ploux, O</p>

    <p>          FEBS J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984394&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984394&amp;dopt=abstract</a> </p><br />

    <p>16.   59089   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Structural and thermodynamic insights into the binding mode of five novel inhibitors of lumazine synthase from Mycobacterium tuberculosis</p>

    <p>          Morgunova, E, Illarionov, B, Sambaiah, T, Haase, I, Bacher, A, Cushman, M, Fischer, M, and Ladenstein, R</p>

    <p>          FEBS J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984393&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16984393&amp;dopt=abstract</a> </p><br />

    <p>17.   59090   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Breaking down the wall: Fractionation of mycobacteria</p>

    <p>          Rezwan, Mandana, Laneelle, Marie-Antoinette, Sander, Peter, and Daffe, Mamadou</p>

    <p>          Journal of Microbiological Methods <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T30-4KCXJ7F-1/2/e210a073f6024095adc5a8cf382e57f5">http://www.sciencedirect.com/science/article/B6T30-4KCXJ7F-1/2/e210a073f6024095adc5a8cf382e57f5</a> </p><br />

    <p>18.   59091   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Nanomolar cationic dendrimeric sulfadiazine as potential antitoxoplasmic agent</p>

    <p>          Prieto, MJ, Bacigalupe, D, Pardini, O, Amalvy, JI, Venturini, C, Morilla, MJ, and Romero, EL</p>

    <p>          International Journal of Pharmaceutics <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7W-4K9KY3T-5/2/5295b9c999f873e28dd732463629ff8c">http://www.sciencedirect.com/science/article/B6T7W-4K9KY3T-5/2/5295b9c999f873e28dd732463629ff8c</a> </p><br />

    <p>19.   59092   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Diagnosis and Treatment of Tuberculosis</p>

    <p>          Cohen, Shannon Munro</p>

    <p>          The Journal for Nurse Practitioners <b>2006</b>.  2(6): 390-396</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XMP-4K674WV-C/2/738f91424621e4ff7dcac80448c01269">http://www.sciencedirect.com/science/article/B7XMP-4K674WV-C/2/738f91424621e4ff7dcac80448c01269</a> </p><br />

    <p>20.   59093   OI-LS-358; PUBMED-OI-10/2/2006</p>

    <p class="memofmt1-2">          Determination of the Bound Conformation of a Competitive Nanomolar Inhibitor of Mycobacterium tuberculosis Type II Dehydroquinase by NMR Spectroscopy</p>

    <p>          Prazeres, VF, Sanchez-Sixto, C, Castedo, L, Canales, A, Canada, FJ, Jimenez-Barbero, J, Lamb, H, Hawkins, AR, and Gonzalez-Bello, C</p>

    <p>          ChemMedChem <b>2006</b>.  1(9): 990-996</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16952136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16952136&amp;dopt=abstract</a> </p><br />

    <p>21.   59094   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Protective efficacy induced by Mycobacterium bovis bacille Calmette-Guerin can be augmented in an antigen independent manner by use of non-coding plasmid DNA</p>

    <p>          Hogarth, Philip J, Logan, Karen E, Ferraz, Jose Candido, Hewinson, R, Glyn, and Chambers, Mark A</p>

    <p>          Vaccine <b>2006</b>.  24(1): 95-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4HJYYMP-5/2/6e57d68329753e4a93feca13be63c7d1">http://www.sciencedirect.com/science/article/B6TD4-4HJYYMP-5/2/6e57d68329753e4a93feca13be63c7d1</a> </p><br />

    <p>22.   59095   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          Antibacterial, antifungal and antitubercular activity of (the roots of) Pelargonium reniforme (CURT) and Pelargonium sidoides (DC) (Geraniaceae) root extracts</p>

    <p>          Mativandlela, SPN, Lall, N, and Meyer, JJM</p>

    <p>          SOUTH AFRICAN JOURNAL OF BOTANY <b>2006</b>.  72(2): 232-237, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800008</a> </p><br />

    <p>23.   59096   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          Mycothione reductase inhibition by anti-mycobacterial naphthoquinones isolated from Euclea natalensis: Possible mode(s) of biological activity</p>

    <p>          Hamilton, CJ and Lall, N</p>

    <p>          SOUTH AFRICAN JOURNAL OF BOTANY <b>2006</b>.  72(2): 322-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240298800065</a> </p><br />

    <p>24.   59097   OI-LS-358; EMBASE-OI-10/2/2007</p>

    <p class="memofmt1-2">          Search for antibacterial and antifungal agents from selected Indian medicinal plants</p>

    <p>          Kumar, V, Prashanth, Chauhan, Neelam S, Padh, Harish, and Rajani, M</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  107(2): 182-188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4JK4DBM-1/2/be6e3a8abfc0d9dc30fc89af604399fb">http://www.sciencedirect.com/science/article/B6T8D-4JK4DBM-1/2/be6e3a8abfc0d9dc30fc89af604399fb</a> </p><br />

    <p>25.   59098   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          New heterocyclic hydrazones in the search for antitubercular agents: Synthesis and in vitro evaluations</p>

    <p>          Bijev, A</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(7): 506-512, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240304300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240304300010</a> </p><br />

    <p>26.   59099   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          The structure of MbtI from Mycobacterium tuberculosis, the first enzyme in the biosynthesis of the siderophore mycobactin, reveals it to be a salicylate synthase</p>

    <p>          Harrison, AJ, Yu, MM, Gardenborg, T, Middleditch, M, Ramsay, RJ, Baker, EN, and Lott, JS</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2006</b>.  188(17): 6081-6091, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240250200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240250200007</a> </p><br />

    <p>27.   59100   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          Peptidyl fluoro-ketones as proteolytic enzyme inhibitors</p>

    <p>          Sani, M, Sinisi, R, and Viani, F</p>

    <p>          CURRENT TOPICS IN MEDICINAL CHEMISTRY <b>2006</b>.  6(14): 1545-1566, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240204400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240204400009</a> </p><br />
    <br clear="all">

    <p>28.   59101   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          In vitro activity and in vivo efficacy of icofungipen (PLD-118), a novel oral antifungal agent, against the pathogenic yeast Candida albicans</p>

    <p>          Hasenoehrl, A, Galic, T, Ergovic, G, Marsic, N, Skerlev, M, Mittendorf, J, Geschke, U, Schmidt, A, and Schoenfeld, W</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(9): 3011-3018, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240297000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240297000017</a> </p><br />

    <p>29.   59102   OI-LS-358; WOS-OI-9/24/2006</p>

    <p class="memofmt1-2">          In vitro activities of DA-7157 and DA-7218 against Mycobacterium tuberculosis and Nocardia brasiliensis</p>

    <p>          Vera-Cabrera, L, Gonzalez, E, Rendon, A, Ocampo-Candiani, J, Welsh, O, Velazquez-Moreno, VM, Choi, SH, and Molina-Torres, C</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(9): 3170-3172, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240297000042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240297000042</a> </p><br />

    <p>30.   59103   OI-LS-358; WOS-OI-10/1/2006</p>

    <p class="memofmt1-2">          A new, broad-spectrum azole antifungal: posaconazole mechanisms of action and resistance, spectrum of activity</p>

    <p>          Hof, H</p>

    <p>          MYCOSES <b>2006</b>.  49: 2-6, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240404600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240404600002</a> </p><br />

    <p>31.   59104   OI-LS-358; WOS-OI-10/1/2006</p>

    <p class="memofmt1-2">          Inactivation of Rv2525c, a substrate of the twin arginine translocation (Tat) system of Mycobacterium tuberculosis, increases beta-lactam susceptibility and virulence</p>

    <p>          Saint-Joanis, B, Demangel, C, Jackson, M, Brodin, P, Marsollier, L, Boshoff, H, and Cole, ST</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2006</b>.  188(18): 6669-6679, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240475900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240475900026</a> </p><br />

    <p>32.   59105   OI-LS-358; WOS-OI-10/1/2006</p>

    <p class="memofmt1-2">          Blueberry leaf inhibits hepatitis C virus RNA replication</p>

    <p>          Akamatsu, E, Kai, T, Hirabaru, H, Yukizaki, C, Sakai, M, Uto, H, Tsubouchi, H, and Kunitake, H </p>

    <p>          HORTSCIENCE <b>2006</b>.  41(4): 1082-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239045700626">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239045700626</a> </p><br />

    <p>33.   59106   OI-LS-358; WOS-OI-10/1/2006</p>

    <p class="memofmt1-2">          The hepatitis C virus and immune evasion: nonstructural 3/4A transgenic mice are resistant to lethal tumour necrosis factor alpha mediated liver disease</p>

    <p>          Frelin, L, Brenndorfer, ED, Ahlen, G, Weiland, M, Hultgren, C, Alheim, M, Glaumann, H, Rozell, B, Milich, DR, Bode, JG, and Sallberg, M</p>

    <p>          GUT <b>2006</b>.  55 (10): 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240437600022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240437600022</a> </p><br />
    <br clear="all">

    <p>34.   59107   OI-LS-358; WOS-OI-10/1/2006</p>

    <p class="memofmt1-2">          Aspects of successful drug discovery and development</p>

    <p>          Pauwels, R</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  71(2-3): 77-89, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240381200002</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
