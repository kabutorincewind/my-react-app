

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-297.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="shtf4YRMdk6rmGXMgNzRTGNu2PmHoNuC4Q+gnhdrYrj1dpbWLQs4rbZxo0zAfhCq0UjRqf2HluGoIHFCZNLXjxQ5PDi9nQ0Xvba3g9f+mW6qzcXEc8MNkjaWICI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="00AB04CB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-297-MEMO</b> </p>

<p>    1.    43493   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Crystallization and preliminary X-ray analysis of alpha-isopropylmalate synthase from Mycobacterium tuberculosis</p>

<p>           Koon, N, Squire, CJ, and Baker, EN</p>

<p>           Acta Crystallogr D Biol Crystallogr 2004. 60(Pt 6): 1167-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159590&amp;dopt=abstract</a> </p><br />

<p>    2.    43494   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           The SRS superfamily of Toxoplasma surface proteins</p>

<p>           Jung, Calvin, Lee, Cleo Y-F, and Grigg, Michael E</p>

<p>           International Journal for Parasitology 2004. 34(3): 285-296</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    43495   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Crystal structure of LeuA from Mycobacterium tuberculosis, a key enzyme in leucine biosynthesis</p>

<p>           Koon, N, Squire, CJ, and Baker, EN</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(22): 8295-8300</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159544&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159544&amp;dopt=abstract</a> </p><br />

<p>    4.    43496   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Inhibition of hepatitis C virus NS3-mediated cell transformation by recombinant intracellular antibodies</p>

<p>           Zemel, R, Berdichevsky, Y, Bachmatov, L, Benhar, I, and Tur-Kaspa, R</p>

<p>           J Hepatol 2004. 40(6): 1000-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15158342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15158342&amp;dopt=abstract</a> </p><br />

<p>    5.    43497   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Effect of antiretroviral protease inhibitors alone, and in combination with paromomycin, on the excystation, invation and in vitro development of Cryptosporidium parvum. Reply</p>

<p>           Petry, Franz, Hommer, Vera, and Eichholz, Jutta</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(2): 403</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    6.    43498   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Phosphorothioate di- and trinucleotides as a novel class of anti-hepatitis B virus agents</p>

<p>           Iyer, RP, Jin, Y, Roland, A, Morrey, JD, Mounir, S, and Korba, B</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2199-205</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155222&amp;dopt=abstract</a> </p><br />

<p>    7.    43499   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Polyamine metabolism in a member of the phylum Microspora (Encephalitozoon cuniculi): effects of polyamine analogues</p>

<p>           Bacchi, CJ, Rattendi, D, Faciane, E, Yarlett, N, Weiss, LM, Frydman, B, Woster, P, Wei, B, Marton, LJ, and Wittner, M</p>

<p>           Microbiology 2004. 150(Pt 5): 1215-24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133083&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133083&amp;dopt=abstract</a> </p><br />

<p>    8.    43500   OI-LS-297; PUBMED-OI-6/02/2004</p>

<p class="memofmt1-2">           Construction, molecular modeling, and simulation of Mycobacterium tuberculosis cell walls</p>

<p>           Hong, X and Hopfinger, AJ</p>

<p>           Biomacromolecules 2004. 5(3): 1052-65</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15132700&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15132700&amp;dopt=abstract</a> </p><br />

<p>    9.    43501   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Identification of a new microsporidian parasite related to Vittaforma corneae in HIV-positive and HIV-negative patients from Portugal</p>

<p>           Sulaiman, Irshad M, Matos, Olga, Lobo, Maria L, and Xiao, Lihua</p>

<p>           Journal of Eukaryotic Microbiology 2003. 50(Suppl.): 586-590</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  10.    43502   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p><b>           Intravenous rifalazil formulation and methods of use thereof</b>((Activbiotics, Inc. USA)</p>

<p>           Michaelis, Arthur F, Sayada, Chalom, and Cabana, Bernard E</p>

<p>           PATENT: WO 2003101445 A1;  ISSUE DATE: 20031211</p>

<p>           APPLICATION: 2003; PP: 71 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  11.    43503   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Citrafungins A and B, two new fungal metabolite inhibitors of GGTase I with antifungal activity</p>

<p>           Singh, Sheo B, Zink, Deborah L, Doss, George A, Polishook, Jon D, Ruby, Carolyn, Register, Elizabeth, Kelly, Theresa M, Bonfiglio, Cynthia, Williamson, Joanne M, and Kelly, Rosemarie</p>

<p>           Organic Letters 2004. 6(3): 337-340</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    43504   OI-LS-297; WOS-OI-5/23/2004</p>

<p class="memofmt1-2">           Encephalitozoon cuniculi infection in rabbits</p>

<p>           Harcourt-Brown, FM</p>

<p>           SEMIN AVIAN EXOT PET 2004. 13(2): 86-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221045500005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221045500005</a> </p><br />

<p>  13.    43505   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Antifungal Cyclopentenediones from Piper coruscans</p>

<p>           Li, Xing-Cong, Ferreira, Daneel, Jacob, Melissa R, Zhang, Qifeng, Khan, Shabana I, ElSohly, Hala N, Nagle, Dale G, Smillie, Troy J, Khan, Ikhlas A, Walker, Larry A, and Clark, Alice M</p>

<p>           Journal of the American Chemical Society 2004. 126(22): 6872-6873</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  14.    43506   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p><b>           Antifungal medicaments comprising arylamidine derivatives</b>((Bayer Cropscience SA, Fr.)</p>

<p>           Vors, Jean-Pierre, O&#39;Neill, Elizabeth, Labourdette, Gilbert, Mansfield, Gilian, Pillmoor, John, and Barchietto, Thierry</p>

<p>           PATENT: EP 1413301 A1;  ISSUE DATE: 20040428</p>

<p>           APPLICATION: 2002-28530; PP: 19 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    43507   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           HP(2 - 9)-magainin 2(1 - 12), a synthetic hybrid peptide, exerts its antifungal effect on Candida albicans by damaging the plasma membrane</p>

<p>           Park, Yoonkyung, Lee, Dong Gun, and Hahm, Kyung-Soo</p>

<p>           Journal of Peptide Science 2004. 10(4): 204-209</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43508   OI-LS-297; WOS-OI-5/23/2004</p>

<p class="memofmt1-2">           The antimicrobial properties of new synthetic porphyrins</p>

<p>           Philippova, TO, Galkin, BN, Zinchenko, OY, Rusakova, MY, Ivanitsa, VA, Zhilina, ZI, Vodzinskii, SV, and Ishkov, YV</p>

<p>           J PORPHYR PHTHALOCYA 2003. 7(11-12): 755-760</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221042100005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221042100005</a> </p><br />

<p>  17.    43509   OI-LS-297; WOS-OI-5/23/2004</p>

<p class="memofmt1-2">           Pharmaceutical evaluation of early development candidates &quot;the 100 mg-approach&quot;</p>

<p>           Balbach, S and Korn, C</p>

<p>           INT J PHARM 2004. 275(1-2): 1-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221062400001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221062400001</a> </p><br />

<p>  18.    43510   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Broad-spectrum antifungal metabolites produced by the soil bacterium Serratia plymuthica A 153</p>

<p>           Levenfors, Jolanta J, Hedman, Rikard, Thaning, Christian, Gerhardson, Berndt, and Welch, Christopher J</p>

<p>           Soil Biology &amp; Biochemistry 2004.  36(4): 677-685</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  19.    43511   OI-LS-297; WOS-OI-5/23/2004</p>

<p class="memofmt1-2">           Mycobacteria inhibit nitric oxide synthase recruitment to phagosomes during macrophage infection</p>

<p>           Miller, BH, Fratti, RA, Poschet, JF, Timmins, GS, Master, SS, Burgos, M, Marletta, MA, and Deretic, V</p>

<p>           INFECT IMMUN 2004. 72(5): 2872-2878</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221120100049">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221120100049</a> </p><br />

<p>  20.    43512   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Antifungal steroid saponins from Dioscorea cayenensis</p>

<p>           Sautour, M, Mitaine-Offer, A-C, Miyamoto, T, Dongmo, A, and Lacaille-Dubois, M-A</p>

<p>           Planta Medica  2004. 70(1): 90-92</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    43513   OI-LS-297; WOS-OI-5/23/2004</p>

<p class="memofmt1-2">           Identification of novel inhibitors of the SARS coronavirus main protease 3CL(pro)</p>

<p>           Bacha, U, Barrila, J, Velazquez-Campoy, A, Leavitt, SA, and Freire, E</p>

<p>           BIOCHEMISTRY-US 2004. 43(17): 4906-4912</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221106100004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221106100004</a> </p><br />

<p>  22.    43514   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Synthesis of 2,4-Diamino-6-[2&#39;-O-(w-carboxyalkyl)oxydibenz[b,f]azepin-5-yl]methylpteridines as Potent and Selective Inhibitors of Pneumocystis carinii, Toxoplasma gondii, and Mycobacterium avium Dihydrofolate Reductase</p>

<p>           Rosowsky, Andre, Fu, Hongning, Chan, David CM, and Queener, Sherry F</p>

<p>           Journal of Medicinal Chemistry 2004.  47(10): 2475-2485</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43515   OI-LS-297; WOS-OI-5/30/2004</p>

<p class="memofmt1-2">           The Mycobacterium tuberculosis Rv2358-furB operon is induced by zinc</p>

<p>           Milano, A, Branzoni, M, Canneva, F, Profumo, A, and Riccardi, G</p>

<p>           RES MICROBIOL  2004. 155(3): 192-200</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221205000009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221205000009</a> </p><br />

<p>  24.    43516   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Synthesis, Biological Activity and Molecular Modeling of 6-Benzylthioinosine Analogs as Subversive Substrates of Toxoplasma gondii Adenosine Kinase</p>

<p>           Yadav, Vikas, Chu, Chung K, Rais, Reem H, Al Safarjalani, Omar N, Guarcello, Vincenzo, Naguib, Fardos NM, and El Kouni, Mahmoud H</p>

<p>           Journal of Medicinal Chemistry 2004.  47(8): 1987-1996</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    43517   OI-LS-297; WOS-OI-5/30/2004</p>

<p class="memofmt1-2">           Non-peptidic small-molecule inhibitors of the single-chain hepatitis C virus NS3 protease/NS4A cofactor complex discovered by structure-based NMR screening</p>

<p>           Wyss, DF, Arasappan, A, Senior, MM, Wang, YS, Beyer, BM, Njoroge, FG, and McCoy, MA</p>

<p>           J MED CHEM 2004. 47(10): 2486-2498</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221183800014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221183800014</a> </p><br />

<p>  26.    43518   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Activities of benzimidazole D- and L-ribonucleosides in animal models of cytomegalovirus infections</p>

<p>           Kern, Earl R, Hartline, Caroll B, Rybak, Rachel J, Drach, John C, Townsend, Leroy B, Biron, Karen K, and Bidanset, Deborah J</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(5): 1749-1755</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    43519   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           RNase P ribozyme as an antiviral agent against human cytomegalovirus</p>

<p>           Trang, Phong and Liu, Fenyong</p>

<p>           Methods in Molecular Biology (Totowa, NJ, United States) 2004. 252(Ribozymes and siRNA Protocols): 437-450</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    43520   OI-LS-297; WOS-OI-5/30/2004</p>

<p class="memofmt1-2">           Effect of listeriolysin O-loaded erythrocytes on Mycobacterium avium replication within macrophages</p>

<p>           Rossi, L, Brandi, G, Malatesta, M, Serafini, S, Pierige, F, Celeste, AG, Schiavano, GF, Gazzanelli, G, and Magnani, M</p>

<p>           J ANTIMICROB CHEMOTH 2004. 53(5): 863-866</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221143800030">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221143800030</a> </p><br />

<p>  29.    43521   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Thiadiazolyl quinazolones as potential antiviral and antihypertensive agents</p>

<p>           Pandey, VK, Tusi, Sarah, Tusi, Zehra, Raghubir, R, Dixit, M, Joshi, MN, and Bajpai, SK</p>

<p>           Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry 2004. 43B(1): 180-183</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    43522   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p><b>           Preparation of cyanothiazolylpyrrolidinecarboxylates and related compounds as antivirals</b>((Glaxo Group Limited, UK)</p>

<p>           Burton, George, Goodland, Helen Susanne, Haigh, David, Kiesow, Terence John, Ku, Thomas W, and Slater, Martin John</p>

<p>           PATENT: WO 2004009543 A2;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 27 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    43523   OI-LS-297; WOS-OI-5/30/2004</p>

<p class="memofmt1-2">           Synthesis and anti-HCMV activity of 1-acyl-beta-lactams and 1-acylazetidines derived from phenylalanine</p>

<p>           Gerona-Navarro, G, de, Vega MJP, Garcia-Lopez, MT, Andrei, G, Snoeck, R, Balzarini, J, De, Clercq E, and Gonzalez-Muniz, R</p>

<p>           BIOORG MED CHEM LETT 2004. 14(9): 2253-2256</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221160200048">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221160200048</a> </p><br />

<p>  32.    43524   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Roscovitine, a cyclin-dependent kinase inhibitor, prevents replication of varicella-zoster virus</p>

<p>           Taylor, Shannon L, Kinchington, Paul R, Brooks, Andrew, and Moffat, Jennifer F</p>

<p>           Journal of Virology 2004. 78(6): 2853-2862</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    43525   OI-LS-297; WOS-OI-5/30/2004</p>

<p class="memofmt1-2">           Effects of maribavir and selected indolocarbazoles on Epstein-Barr virus protein kinase BGLF4 and on viral lytic replication</p>

<p>           Gershburg, E, Hong, K, and Pagano, JS</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(5): 1900-1903</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221227900066">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221227900066</a> </p><br />

<p>  34.    43526   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p><b>           Preparation of 2,3-dihydro-6-nitroimidazo[2,1-b]oxazoles as antibacterial agents</b>((Otsuka Pharmaceutical Co., Ltd. Japan)</p>

<p>           Tsubouchi, Hidetsugu, Sasaki, Hirofumi, Kuroda, Hideaki, Itotani, Motohiro, Hasegawa, Takeshi, Haraguchi, Yoshikazu, Kuroda, Takeshi, Matsuzaki, Takayuki, Tai, Kuninori, Komatsu, Makoto, Matsumoto, Makoto, Hashizume, Hiroyuki, Tomishige, Tatsuo, Seike, Yuji, Kawasaki, Masanori, Sumida, Takumi, and Miyamura, Shin</p>

<p>           PATENT: WO 2004033463 A1;  ISSUE DATE: 20040422</p>

<p>           APPLICATION: 2003; PP: 1084 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  35.    43527   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Bauhinoxepins A and B: New antimycobacterial dibenzo[b,f]oxepins from Bauhinia saccocalyx</p>

<p>           Kittakoop, Prasat, Nopichai, Sombat, Thongon, Nuntawan, Charoenchai, Panarat, and Thebtaranonth, Yodhathai</p>

<p>           Helvetica Chimica Acta 2004. 87(1): 175-179</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    43528   OI-LS-297; SCIFINDER-OI-5/25/2004</p>

<p class="memofmt1-2">           Synthesis of b-enantiomers of N4-hydroxy-3&#39;-deoxypyrimidine nucleosides and their evaluation against bovine viral diarrhoea virus and hepatitis C virus in cell culture</p>

<p>           Hollecker, Laurent, Choo, Hyunah, Chong, Youhoon, Chu, Chung K, Lostia, Stefania, McBrayer, Tamara R, Stuyver, Lieven J, Mason, JChristian, Du, Jinfa, Rachakonda, Suguna, Shi, Junxing, Schinazi, Raymond F, and Watanabe, Kyochi A</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 2004. 15(1): 43-55</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
