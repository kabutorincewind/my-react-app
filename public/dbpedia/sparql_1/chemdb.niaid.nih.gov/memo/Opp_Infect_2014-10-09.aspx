

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-10-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="O/6ZFWGTG+1hssKKhKz0qK2kOsw0KLxhSIWsWH+dRJD8PLG4rw5rR6UtD9fA/Br+v5EXs00XBSXgdSBwRtLWDiKqadBgKMGO3pXV6cY8GvZMWHTaE93+2YOGBAU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4FDFFDEE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 26 - October 9, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24955933">Antifungal Activity and Pore-forming Mechanism of Astacidin 1 against Candida albicans.</a> Choi, H. and D.G. Lee. Biochimie, 2014. 105: p. 58-63. PMID[24955933].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25219900">Synthesis and Biological Evaluation of a New Series of N-Acyldiamines as Potential Antibacterial and Antifungal Agents.</a> Ferreira Bda, S., A.M. de Almeida, T.C. Nascimento, P.P. de Castro, V.L. Silva, C.G. Diniz, and M. Le Hyaric. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(19): p. 4626-4629. PMID[25219900].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25108373">Eugenia calycina Cambess Extracts and their Fractions: Their Antimicrobial Activity and the Identification of Major Polar Compounds Using Electrospray Ionization FT-ICR Mass Spectrometry.</a> Ferreira, F.P., S.R. Morais, M.T. Bara, E.C. Conceicao, J.R. Paula, T.C. Carvalho, B.G. Vaz, H.B. Costa, W. Romao, and M.H. Rezende. Journal of Pharmaceutical and Biomedical Analysis, 2014. 99: p. 89-96. PMID[25108373].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25268720">Discovery of New Imidazole Derivatives Containing the 2,4-Dienone Motif with Broad-spectrum Antifungal and Antibacterial Activity.</a> Liu, C., C. Shi, F. Mao, Y. Xu, J. Liu, B. Wei, J. Zhu, M. Xiang, and J. Li. Molecules, 2014. 19(10): p. 15653-15672. PMID[25268720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25256694">In Vitro Antimicrobial Activity and HPTLC Analysis of Hydroalcoholic Seed Extract of Nymphaea nouchali Burm. f.</a> Parimala, M. and F.G. Shoba. BMC Complementary and Alternative Medicine, 2014. 14: p. 361. PMID[25256694].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24851813">Phenylpropanoids of Plant Origin as Inhibitors of Biofilm Formation by Candida albicans.</a> Raut, J.S., R.B. Shinde, N.M. Chauhan, and S.M. Karuppayil. Journal of Microbiology and Biotechnology, 2014. 24(9): p. 1216-1225. PMID[24851813].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25193230">Synthesis and Antimicrobial Activity of Novel 5-Aminoimidazole-4-carboxamidrazones.</a> Ribeiro, A.I., C. Gabriel, F. Cerqueira, M. Maia, E. Pinto, J.C. Sousa, R. Medeiros, M.F. Proenca, and A.M. Dias. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(19): p. 4699-4702. PMID[25193230].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25129868">Sequential Synthesis of Amino-1,4-Naphthoquinone-appended Triazoles and Triazole-chromene Hybrids and their Antimycobacterial Evaluation.</a> Devi Bala, B., S. Muthusaravanan, T.S. Choon, M. Ashraf Ali, and S. Perumal. European Journal of Medicinal Chemistry, 2014. 85: p. 737-746. PMID[25129868].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24839955">Antimycobacterial Activity of Pichia pastoris-derived Mature Bovine Neutrophil beta-Defensins 5.</a> Kang, J., D. Zhao, Y. Lyu, L. Tian, X. Yin, L. Yang, K. Teng, and X. Zhou. European Journal of Clinical Microbiology &amp; Infectious Diseases, 2014. 33(10): p. 1823-1834. PMID[24839955].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25089809">10. Synthesis, Biological Evaluation and 3D-QSAR Study of Hydrazide, Semicarbazide and Thiosemicarbazide Derivatives of 4-(Adamantan-1-yl)quinoline as Anti-tuberculosis Agents.</a> Patel, S.R., R. Gangwal, A.T. Sangamwar, and R. Jain. European Journal of Clinical Microbiology &amp; Infectious Diseases, 2014. 85: p. 255-267. PMID[25089809].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25092705">Structure, Activity, and Inhibition of the Carboxyltransferase Beta-subunit of Acetyl Coenzyme a Carboxylase (AccD6) from Mycobacterium tuberculosis.</a> Reddy, M.C., A. Breda, J.B. Bruning, M. Sherekar, S. Valluru, C. Thurman, H. Ehrenfeld, and J.C. Sacchettini. Antimicrobial Agents and Chemotherapy, 2014. 58(10): p. 6122-6132. PMID[25092705].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24720445">Synthesis and Evaluation of Antimalarial Properties of Novel 4-Aminoquinoline Hybrid Compounds.</a> Fisher, G.M., R.P. Tanpure, A. Douchez, K.T. Andrews, and S.A. Poulsen. Chemical Biology &amp; Drug Design, 2014. 84(4): p. 462-472. PMID[24720445].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25128801">The Potential of Quinoline Derivatives for the Treatment of Toxoplasma gondii Infection.</a> Kadri, D., A.K. Crater, H. Lee, V.R. Solomon, and S. Ananvoranich. Experimental Parasitology, 2014. 145: p. 135-144. PMID[25128801].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25058875">In Vitro Antitrypanosomal and Antiplasmodial Activities of Crude Extracts and Essential Oils of Ocimum gratissimum Linn from Benin and Influence of Vegetative Stage.</a> Kpadonou Kpoviessi, B.G., S.D. Kpoviessi, E. Yayi Ladekan, F. Gbaguidi, M. Frederich, M. Moudachirou, J. Quetin-Leclercq, G.C. Accrombessi, and J. Bero. Journal of Ethnopharmacology, 2014. 155(3): p. 1417-1423. PMID[25058875].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25226494">Identification of Potent and Selective Non-covalent Inhibitors of the Plasmodium falciparum Proteasome.</a> Li, H., C. Tsu, C. Blackburn, G. Li, P. Hales, L. Dick, and M. Bogyo. Journal of the American Chemical Society, 2014. 136(39): p. 13562-13565. PMID[25226494].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25276063">Antimalarial Activity of Malaysian Plectranthus amboinicus against Plasmodium berghei.</a> Ramli, N., P.O. Ahamed, H.M. Elhady, and M. Taher. Pharmacognosy Research, 2014. 6(4): p. 280-284. PMID[25276063].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25089811">In Vitro Antiplasmodial and Cytotoxic Activities of Asymmetrical Pyridinium Derivatives.</a> Rubio-Ruiz, B., V.M. Castillo-Acosta, G. Perez-Moreno, A. Espinosa, D. Gonzalez-Pacanowska, L.M. Ruiz-Perez, A. Entrena, and A. Conejo-Garcia. European Journal of Medicinal Chemistry, 2014. 85: p. 289-292. PMID[25089811].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25184829">In Vitro and in Vivo Antimalarial Activity of Amphiphilic Naphthothiazolium Salts with Amine-bearing Side Chains.</a> Ulrich, P., G.R. Gipson, M.A. Clark, A. Tripathi, D.J. Sullivan, Jr., and C. Cerami. The American Journal of Tropical Medicine and Hygiene, 2014. 91(4): p. 824-832. PMID[25184829].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25026331">Antiplasmodial Activity of Extracts of 25 Cyanobacterial Species from Coastal Regions of Tamil Nadu.</a> Veerabadhran, M., N. Manivel, D. Mohanakrishnan, D. Sahal, and S. Muthuraman. Pharmaceutical Biology, 2014. 52(10): p. 1291-1301. PMID[25026331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0926-100914.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341518200023">Chemical Composition and Antimicrobial Activity of the Essential Oil from Leaves of Algerian melissa Officinalis L.</a> Abdellatif, F., H. Boudjella, A. Zitouni, and A. Hassani. EXCLI Journal, 2014. 13: p. 772-781. ISI[000341518200023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341293300012">Synthesis and Antifungal Activity of Substituted Salicylaldehyde Hydrazones, Hydrazides and Sulfohydrazides.</a> Backes, G.L., D.M. Neumann, and B.S. Jursic. Bioorganic &amp; Medicinal Chemistry, 2014. 22(17): p. 4629-4636. ISI[000341293300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341584300012">Composition and Antimicrobial Activity of the Essential Oil from Algerian Warionia saharae Benth. &amp; Hook.</a> Gherib, M., F.A. Bekkara, C. Bekhechi, A. Bighelli, J. Casanova, and F. Tomi. Journal of Essential Oil Research, 2014. 26(5): p. 385-391. ISI[000341584300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340998200020">Chemical Composition Antioxidant and Antimicrobial Activities of the Essential Oils of Matricaria aurea Loefl. Growing in Tunisia.</a> Kheder, F.B.H., M.A. Mahjoub, F. Zaghrouni, S. Kwaja, A.N. Helal, and Z. Mighri. Journal of Essential Oil Bearing Plants, 2014. 17(3): p. 493-505. ISI[000340998200020]. <b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341572300003">Synthesis, Characterization, and Antimicrobial Properties of Two Cu(II) Complexes Derived from a Benzimidazole Ligand.</a> Kose, M. Journal of Coordination Chemistry, 2014. 67(14): p. 2377-2392. ISI[000341572300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341091300002">Novel S-Triazine Accommodated 5-Benzylidino-4-Thiazolidinones: Synthesis and in Vitro Biological Evaluations.</a> Rana, A.M., P. Trivedi, K.R. Desai, and S. Jauhari. Medicinal Chemistry Research, 2014. 23(10): p. 4320-4336. ISI[000341091300002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341439300008">Characterization and Biological Activity of Pefloxacin-Imidazole Mixed Ligands Complexes.</a> Soayed, A.A., H.M. Refaat, and D.A.N. El-Din. Inorganica Chimica Acta, 2014. 421: p. 59-66. ISI[000341439300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341063000009">Synthesis, Surface Properties and Antimicrobial Activity of Some Germanium Nonionic Surfactants.</a> Zaki, M.F. and S.M. Tawfik. Journal of Oleo Science, 2014. 63(9): p. 921-931. ISI[000341063000009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0926-100914.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
