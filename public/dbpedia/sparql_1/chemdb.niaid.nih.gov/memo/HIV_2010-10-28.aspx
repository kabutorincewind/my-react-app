

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-10-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Wyk+WLRBvPKlJcJtaIC0p5vyQqtXds9MZeFa9REC3KRSIqfVn4fM21ua3WCImftukwUD+wPJgS5RfNrvMBVmBhfdXtRwbO+oAWUoK08vO6gIEMl8fW8Vc6NTSuo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A55464C1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 15 - October 28, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20965725">Synthesis, antiviral and contraceptive activities of nucleoside-sodium cellulose sulfate acetate and succinate conjugates.</a> Agarwal, H.K., A. Kumar, G.F. Doncel, and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2010. <b>[EPub Ahead of Print]</b>;  PMID[20965725].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20958050">Structure-Based Design, Synthesis, and Structure-Activity Relationship Studies of HIV-1 Protease Inhibitors Incorporating Phenyloxazolidinones.</a> Ali, A., G.S. Reddy, M.N. Nalam, S.G. Anjum, H. Cao, C.A. Schiffer, and T.M. Rana,. Journal of Medicinal Chemistry, 2010. <b>[Epub Ahead of Print]</b>; PMID[20958050].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20977258">Design, Synthesis, and Biological Evaluation of Novel Hybrid Dicaffeoyltartaric/Diketo Acid and Tetrazole-Substituted l-Chicoric Acid Analogue Inhibitors of Human Immunodeficiency Virus Type 1 Integrase.</a> Crosby, D.C., X. Lei, C.G. Gibbs, B.R. McDougall, W.E. Robinson, and M.G. Reinecke. Journal of Medicinal Chemistry, 2010. <b>[EPub Ahead of Print]</b>; PMID[20977258].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20956357">Iron Chelators of the DpT and BpT Series Inhibit HIV-1 Transcription: Identification of Novel Cellular Targets - Iron, CDK2 and CDK9.</a> Debebe, Z., T. Ammosova, D. Breuer, D. Lovejoy, D. Kalinowski, P. Karla, K. Kumar, M. Jerebtsova, P. Ray, F. Kashanchi, V. Gordeuk, D.R. Richardson, and S. Nekhai. Molecular Pharmacology, 2010. <b>[EPub Ahead of Print]</b>;  PMID[20956357].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20599774">Inhibition of HIV-1 by non-nucleoside reverse transcriptase inhibitors via an induced fit mechanism-Importance of slow dissociation and relaxation rates for antiviral efficacy.</a> Elinder, M., P. Selhorst, G. Vanham, B. Oberg, L. Vrang, and U.H. Danielson. Biochemical Pharmacology, 2010. 80(8): p. 1133-1140; PMID[20599774].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20955169">Isolation of a New Trypsin Inhibitor from the Faba bean (Vicia faba cv. Giza 843) with Potential Medicinal Applications.</a> Fang, E.F., A. Abd Elazeem Hassanien, J.H. Wong, C.S. Bah, S.S. Soliman, and T.B. Ng. Protein &amp; Peptide Letters, 2010. <b>[EPub Ahead of Print]</b>; PMID[20955169].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20832304">Validated predictive QSAR modeling of N-aryl-oxazolidinone-5-carboxamides for anti-HIV protease activity.</a> Halder, A.K. and T. Jha. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(20): p. 6082-6087; PMID[20832304].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20953192">Small-molecule inactivation of HIV-1 NCp7 by repetitive intracellular acyl transfer.</a> Jenkins, L.M., D.E. Ott, R. Hayashi, L.V. Coren, D. Wang, Q. Xu, M.L. Schito, J.K. Inman, D.H. Appella, and E. Appella. Nature Chemical Biology, 2010. <b>[EPub Ahead of Print]</b>;  PMID[20953192].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20950436">Selective killing of human immunodeficiency virus infected cells by non-nucleoside reverse transcriptase inhibitor-induced activation of HIV protease.</a> Jochmans, D., M. Anders, I. Keuleers, L. Smeulders, H.G. Kraeusslich, G. Kraus, and B. Mueller. Retrovirology, 2010. 7(1): p. 89; PMID[20950436].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20956603">A Potent Strategy to Inhibit HIV-1 by Binding Both gp120 and gp41.</a> Kagiampakis, I., A. Gharibi, M.K. Mankowski, B.A. Snyder, R.G. Ptak, K. Alatas, and P.J. Liwang. Antimicrobial Agents and Chemotherapy, 2010. <b>[EPub Ahead of Print]</b>; PMID[20956603].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20961847">Novel fold and carbohydrate specificity of the potent anti-HIV cyanobacterial lectin from oscillatoria agardhii.</a> Koharudin, L.M., W. Furey, and A.M. Gronenborn. The Journal of Biological Chemistry, 2010. <b>[EPub Ahead of Print]</b>; PMID[20961847].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20972507">Tetra-end-linked oligonucleotides forming DNA G-quadruplexes: a new class of aptamers showing anti-HIV activity.</a> Oliviero, G., J. Amato, N. Borbone, S. D&#39;Errico, A. Galeone, L. Mayol, S. Haider, O. Olubiyi, B. Hoorelbeke, J. Balzarini, and G. Piccialli. Chemical Communications (Cambridge), 2010. <b>[EPub Ahead of Print]</b>; PMID[20972507].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20964394">Synthesis of Conformationally Locked l-Deoxythreosyl Phosphonate Nucleosides Built on a Bicyclo[3.1.0]hexane Template.</a> Saneyoshi, H., J.R. Deschamps, and V.E. Marquez. Journal of Organic Chemistry, 2010. <b>[EPub Ahead of Print]</b>;  PMID[20964394].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20976350">Recent advances in anti-HIV natural products.</a> Singh, I.P. and H.S. Bodiwala. Natural Product Reports, 2010. <b>[EPub Ahead of Print]</b>; PMID[20976350].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20823220">Rigid amphipathic fusion inhibitors, small molecule antiviral compounds against enveloped viruses.</a> St Vincent, M.R., C.C. Colpitts, A.V. Ustinov, M. Muqadas, M.A. Joyce, N.L. Barsby, R.F. Epand, R.M. Epand, S.A. Khramyshev, O.A. Valueva, V.A. Korshun, D.L.J. Tyrrell, and L.M. Schang. Proceedings of the National Academy of the Sciences USA, 2010. 107(40): p. 17339-17344. PMID[20823220].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20829038">Exploration of piperidine-4-yl-aminopyrimidines as HIV-1 reverse transcriptase inhibitors. N-Phenyl derivatives with broad potency against resistant mutant viruses.</a> Tang, G., D.J. Kertesz, M. Yang, X. Lin, Z. Wang, W. Li, Z. Qiu, J. Chen, J. Mei, L. Chen, T. Mirzadegan, S.F. Harris, A.G. Villasenor, J. Fretland, W.L. Fitch, J.Q. Hang, G. Heilek, and K. Klumpp. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(20): p. 6020-6023; PMID[20829038].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20973495">Synthesis of N-Terminally Linked Protein and Peptide Dimers by Native Chemical Ligation.</a> Xiao, J., B.S. Hamilton, and T.J. Tolbert. Bioconjugate Chemistry, 2010. <b>[EPub Ahead of Print]</b>; PMID[20973495].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20691099">In vitro evaluation of marine-microorganism extracts for anti-viral activity.</a> Yasuhara-Bell, J., Y.B. Yang, R. Barlow, H. Trapido-Rosenthal, and Y.A. Lu.  Virology Journal, 2010. 7: p. 182. PMID[20691099].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20962083">Small Molecule Inhibition of Human Immunodeficiency Virus Type 1 Infection by Virus Capsid Destabilization.</a> Shi, J.Z., J; Shah, VB; Aiken, C; Whitby, K. The Journal of Virology., 2010. <b>[Epub ahead of print]</b>. PMID[20962083].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="NoSpacing memofmt2-2"> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281483700032">Inhibition of HIV-1 Release by Cell Permeable Peptides.</a> Daniels, S.I., D.A. Davis, and R. Yarchoan.  Journal of Acquired Immune Deficiency Syndromes, 2009. 51: p. 53-53. ISI[000281483700032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282066500580">Dammarenolic acid, a secodammarane triterpenoid from Aglaia sp shows potent anti-retroviral activity in vitro.</a> Esimone, C., G. Eck, C. Nworu, K. Uberla, and P. Proksch. Planta Medica, 2010. 76(12): p. 1309-1309. ISI[000282066500580].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281483700114">Potent Orally Bioavailable HIV-1 Fusion Inhibitors Alter Env Conformation and Expose Conserved Neutralization Epitopes.</a> Finnegan, C., V. Dettmer, M. Bramah-Lawani, T. Nitz, P. Bullock, I. Burimski, M. Reddick, C. Matallana, C. Beaubien, D. Stanley, J. Pettitt, G. Allaway, and K. Salzwedel.  Journal of Acquired Immune Deficiency Syndromes, 2009. 51: p. 138-138. ISI[000281483700114].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281483700102">Design, Synthesis, Anti-HIV and Cytotoxicity of Novel Heterocyclic Compounds.</a> Selvam, P.  Journal of Acquired Immune Deficiency Syndromes, 2009. 51: p. 126-126. ISI[000281483700102].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281783500013">Total Synthesis of (+/-)-Batzelladine K: A Biomimetic Approach.</a> Ahmed, N., K.G. Brahmbhatt, I.P. Singh, and K.K. Bhutani. Synthesis-Stuttgart, 2010. (15): p. 2567-2570. ISI[000281783500013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282479700018">Amino acid derivatives. Part 5. Synthesis and anti-HIV activity of new sebacoyl precursor derived thioureido-amino acid and phthalimide derivatives.</a> Al-Masoudi, N.A., N. Al-Haidery, N.T. Faili, and C. Pannecouque. Arkivoc, 2010: (xi). p. 185-195. ISI[000282479700018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281610800330">Synthesis of a PEG conjugated HIV Gp41 MPER fragment: a new Gp41 helix bundle mimic.</a> D&#39;Ursi, M., M. Grimaldi, A. Mastrogiacomo, G. Sabatino, F. Rizzolo, S. Giannecchini, and P. Rovero. 2010. 16(S1): p. 133-134. ISI[000281610800330].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281937200006">Carbosilane Dendrimers to Transfect Human Astrocytes with Small Interfering RNA Targeting Human Immunodeficiency Virus.</a> Jimenez, J.L., M.I. Clemente, N.D. Weber, J. Sanchez, P. Ortega, F.J. de la Mata, R. Gomez, D. Garcia, L.A. Lopez-Fernandez, and M.A. Munoz-Fernandez. Biodrugs, 2010. 24(5): p. 331-343. ISI[000281937200006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282369500004">Synthesis and anti-HIV-1 evaluation of phosphonates which mimic the 5 &#39;-monophosphate of 4 &#39;-branched 2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy nucleosides.</a> Kubota, Y., N. Ishizaki, K. Haraguchi, T. Hamasaki, M. Baba, and H. Tanaka. Bioorganic &amp; Medicinal Chemistry, 2010. 18(20): p. 7186-7192.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810. ISI[000282369500004].</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282533000008">Acaconin, a chitinase-like antifungal protein with cytotoxic and anti-HIV-1 reverse transcriptase activities from Acacia confusa seeds.</a> Lam, S.K. and T.B. Ng. Acta Biochimica Polonica, 2010. 57(3): p. 299-230. ISI[000282533000008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281610800336">Modified Cyclic Peptides as Inhibitors of HIV-1 Integrase Activity.</a> Northfield, E., S. Headey, M. Scanlon, P. Thompson, and D. Chalmers. Journal of Peptide Science, 2010. 16(S1): p. 130-130.</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282676700003">Selection of Potent Non-Toxic Inhibitory Sequences from a Randomized HIV-1 Specific Lentiviral Short Hairpin RNA Library.</a> Pongratz, C., B. Yazdanpanah, H. Kashkar, M.J. Lehmann, H.G. Krausslich, and M. Kronke. PLOS One, 2010. 5(10): e13172. ISI[000282676700003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281483700102">Inhibition of HIV replication and integrase activity by isatin derivatives.</a> Selvam, P.  Journal of Acquired Immune Deficiency Syndromes, 2009. 51: p. 127-127. ISI[000281483700102].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282369500006">Anti-AIDS agents 84. Synthesis and anti-human immunodeficiency virus (HIV) activity of 2 &#39;-monomethyl-4-methyl- and 1 &#39;-thia-4-methyl-(3 &#39; R,4 &#39; R)-3 &#39;,4 &#39;-di-O-(S)-camphanoyl-(+)-cis-khellactone (DCK) analogs.</a> Xu, S.Q., X. Yan, Y. Chen, P. Xia, K.D. Qian, D.L. Yu, Y. Xia, Z.Y. Yang, S.L. Morris-Natschke, and K.H. Lee. Bioorganic &amp; Medicinal Chemistry, 2010. 18(20): p. 7203-7211. ISI[000282369500006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282479700022">Organocatalytic asymmetric conjugate addition of cyclic 1,3-dicarbonyl compounds to beta,gamma-unsaturated alpha-ketoesters.</a> Wang, J.J., J.H. Lao, Z.P. Hu, R.J. Lu, S.Z. Nie, Q.S. Du, and M. Yan. Arkivoc, 2010: p. 229-243. ISI[000282479700022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282418400355">Inhibitory effects of heme arginate on HIV-1 Growth.</a> Shankaran, P. and Z. Melkova. Cytokine, 2010. 52(1-2): p. 92-92. ISI[000282418400355].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1015-102810.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
