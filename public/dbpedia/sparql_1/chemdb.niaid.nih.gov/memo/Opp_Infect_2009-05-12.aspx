

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3GNlj3jPnUBzfMM559kPyn5FEAHxeIUZ5HOijsVmgwdk0Eq5oMbsXmnJzusL0qhSbW2a+zF+uHCpj4COj2X45SQK2FWzUdxdO2231apTaaEGs9TAgPZ9bo3Mctg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="05B5112E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 29, 2009 - May 12, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19433362?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Bulky 1,4-benzoxazine derivatives with antifungal activity.</a></p>

    <p class="NoSpacing">Fringuelli R, Giacchè N, Milanese L, Cenci E, Macchiarulo A, Vecchiarelli A, Costantino G, Schiaffella F.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 May 3. [Epub ahead of print]   </p>

    <p class="NoSpacing">PMID: 19433362 [PubMed - as supplied by publisher]</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19429621?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">GEMFIBROZIL (LOPIDTM) INHIBITS LEGIONELLA PNEUMOPHILA AND MYCOBACTERIUM TUBERCULOSIS ENOYL-COA REDUCTASES AND BLOCKS INTRACELLULAR GROWTH OF THESE BACTERIA IN MACROPHAGES.</a></p>

    <p class="NoSpacing">Reich-Slotky R, Kabbash CA, Della-Latta P, Blanchard JS, Feinmark SJ, Freeman S, Kaplan G, Shuman HA, Silverstein SC.</p>

    <p class="NoSpacing">J Bacteriol. 2009 May 8. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19429621 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19427791?ordinalpos=12&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro anti Mycobacterium tuberculosis activity of a series of phthalimide derivatives.</a></p>

    <p class="NoSpacing">Santos JL, Yamasaki PR, Chin CM, Takashi CH, Pavan FR, Leite CQ.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 May 3. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19427791 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19412174?ordinalpos=32&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthetic EthR inhibitors boost antituberculous activity of ethionamide.</a></p>

    <p class="NoSpacing">Willand N, Dirié B, Carette X, Bifani P, Singhal A, Desroses M, Leroux F, Willery E, Mathys V, Déprez-Poulain R, Delcroix G, Frénois F, Aumercier M, Locht C, Villeret V, Déprez B, Baulard AR.</p>

    <p class="NoSpacing">Nat Med. 2009 May;15(5):537-44. Epub 2009 May 3.     </p>

    <p class="NoSpacing">PMID: 19412174 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19319854?ordinalpos=86&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Peptide conjugates of therapeutically used antitubercular isoniazid-design, synthesis and antimycobacterial effect.</a></p>

    <p class="NoSpacing">Horváti K, Mezo G, Szabó N, Hudecz F, Bosze S.</p>

    <p class="NoSpacing">J Pept Sci. 2009 May;15(5):385-91.                   </p>

    <p class="NoSpacing">PMID: 19319854 [PubMed - in process]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19299584?ordinalpos=91&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Benzothiazinones kill Mycobacterium tuberculosis by blocking arabinan synthesis.</a></p>

    <p class="NoSpacing">Makarov V, Manina G, Mikusova K, Möllmann U, Ryabova O, Saint-Joanis B, Dhar N, Pasca MR, Buroni S, Lucarelli AP, Milano A, De Rossi E, Belanova M, Bobovska A, Dianiskova P, Kordulakova J, Sala C, Fullam E, Schneider P, McKinney JD, Brodin P, Christophe T, Waddell S, Butcher P, Albrethsen J, Rosenkrands I, Brosch R, Nandi V, Bharath S, Gaonkar S, Shandil RK, Balasubramanian V, Balganesh T, Tyagi S, Grosset J, Riccardi G, Cole ST.</p>

    <p class="NoSpacing">Science. 2009 May 8;324(5928):801-4. Epub 2009 Mar 19.                     </p>

    <p class="NoSpacing">PMID: 19299584 [PubMed - in process]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19237201?ordinalpos=110&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Organotin meclofenamic complexes: Synthesis, crystal structures and antiproliferative activity of the first complexes of meclofenamic acid - novel anti-tuberculosis agents.</a></p>

    <p class="NoSpacing">Kovala-Demertzi D, Dokorou V, Primikiri A, Vargas R, Silvestru C, Russo U, Demertzis MA.</p>

    <p class="NoSpacing">J Inorg Biochem. 2009 May;103(5):738-44. Epub 2009 Jan 27.</p>

    <p class="NoSpacing">PMID: 19237201 [PubMed - in process]</p>

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18662840?ordinalpos=129&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A macrolactone from benzo[a]phenazine with potent activity against Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Silva RS, Pinto Mdo C, Goulart MO, de Souza Filho JD, Neves I Jr, Lourenço MC, Pinto AV.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 May;44(5):2334-7. Epub 2008 Jun 26.</p>

    <p class="NoSpacing">PMID: 18662840 [PubMed - in process]            </p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">9. Horvati, K., et al.</p>

    <p class="NoSpacing">Peptide conjugates of therapeutically used antitubercular isoniazid - design, synthesis and antimycobacterial effect</p>

    <p class="NoSpacing">JOURNAL OF PEPTIDE SCIENCE   2009 (May)  15: 385 - 391         </p>  

    <p class="ListParagraph">10. Klimesova, V., et al.</p>

    <p class="NoSpacing">Preparation and in vitro evaluation of benzylsulfanyl benzoxazole derivatives as potential antituberculosis agents</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (May) 44: 2286 - 2293                       </p> 

    <p class="ListParagraph">11. Silva, RSF., et al.</p>

    <p class="NoSpacing">A macrolactone from benzo[a]phenazine with potent activity against Mycobacterium tuberculosis</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (May) 44: 2334 - 2337                       </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
