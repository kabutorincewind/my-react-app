

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-03-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Kf0BivEjgA+CKRyabmymK0d/5VTNpJtyibpXzooUOCzS8jm1SIWBTNQZLicXjI/gZ11IdCixOzTfiEhlpO5NgzIXxMaT7Nq4r1DRaFxIzoGH97lYFeoL0jI1lCE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="38A89A55" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 17 - March 1, 2012</h1>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22352809">Synthesis and anti-HIV Activities of Glutamate and Peptide Conjugates of Nucleoside Reverse Transcriptase Inhibitors.</a> Agarwal, H.K., B.S. Chhikara, M. Quiterio, G.F. Doncel, and K. Parang, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22352809].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22353003">Emerging Drugs for the Treatment of Human Immunodeficiency Virus.</a> Bhopale, G.M., Recent Patents on Antiinfective Drug Discovery, 2012. <b>[Epub ahead of print]</b>; PMID[22353003].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22196934">Daphnane-type Diterpene Esters with Cytotoxic and anti-HIV-1 Activities from Daphne acutiloba Rehd.</a> Huang, S.Z., X.J. Zhang, X.Y. Li, L.M. Kong, H.Z. Jiang, Q.Y. Ma, Y.Q. Liu, J.M. Hu, Y.T. Zheng, Y. Li, J. Zhou, and Y.X. Zhao, Phytochemistry, 2012. 75: p. 99-107; PMID[22196934].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22344296">A Quantitative Basis for Antiretroviral Therapy for HIV-1 Infection.</a> Jilek, B.L., M. Zarr, M.E. Sampah, S.A. Rabi, C.K. Bullen, J. Lai, L. Shen, and R.F. Siliciano, Nature Medicine, 2012. <b>[Epub ahead of print]</b>; PMID[22344296].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22356441">Inhibitors of Human Immunodeficiency Virus Type 1 (HIV-1) Attachment 6. Preclinical and Human Pharmacokinetic Profiling of BMS-663749, a Phosphonooxymethyl Prodrug of the HIV-1 Attachment Inhibitor 2-(4-Benzoyl-1-piperazinyl)-1-(4,7-dimethoxy-1H-pyrrolo[2,3-c]pyridin-3-yl)-2-oxo ethanone (BMS-488043).</a> Kadow, J.F., Y. Ueda, N.A. Meanwell, T.P. Connolly, T. Wang, C.P. Chen, K.S. Yeung, J. Zhu, J.A. Bender, Z. Yang, D. Parker, P.F. Lin, R.J. Colonno, M. Mathew, D. Morgan, M. Zheng, C. Chien, and D. Grasela, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22356441].</p>
    <p><b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>
    
    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22305583">Discovery of 3,4-Dihydropyrimidin-2(1H)-ones with Inhibitory Activity against HIV-1 Replication.</a> Kim, J., C. Park, T. Ok, W. So, M. Jo, M. Seo, Y. Kim, J.H. Sohn, Y. Park, M.K. Ju, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, P. Sommer, and Z. No, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(5): p. 2119-2124; PMID[22305583].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>

    <p class="plaintext"> </p>

    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22190694">Complex Drug Interactions of the HIV Protease Inhibitors 3: Effect of Simultaneous or Staggered Dosing of Digoxin and Ritonavir, Nelfinavir, Rifampin, or Bupropion.</a> Kirby, B.J., A.C. Collier, E.D. Kharasch, D. Whittington, K.E. Thummel, and J.D. Unadkat, Drug Metabolism and Disposition, 2012. 40(3): p. 610-616; PMID[22190694].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>

    <p class="plaintext"> </p>

    <p>8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22339087">Synthesis, 2D-NMR and Molecular Modelling Studies of Pentacycloundecane lactam-peptides and Peptoids as Potential HIV-1 Wild Type C-SA Protease Inhibitors.</a> Makatini, M.M., K. Petzold, C.N. Alves, P.I. Arvidsson, B. Honarparvar, P. Govender, T. Govender, H.G. Kruger, Y. Sayed, Jeronimolameira, G.E. Maguire, and M.E. Soliman, Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22339087].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p class="plaintext"> </p>
    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22351671">Suppression of Pre Adipocyte Differentiation and Promotion of Adipocyte Death by anti-HIV Drugs.</a> Manente, L., A. Lucariello, C. Costanzo, R. Viglietti, G. Parrella, R. Parrella, M. Gargiulo, D.E.L. A, A. Chirianni, and V. Esposito, In Vivo, 2012. 26(2): p. 287-291; PMID[22351671].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22348230">Antiviral Activity of Recombinant Ankyrin Targeted to the Capsid Domain of HIV-1 Gag Polyprotein.</a> Nangola, S., A. Urvoas, M. Valerio-Lepiniec, W. Khamaikawin, S. Sakkhachornphop, S.S. Hong, P. Boulanger, P. Minard, and C. Tayapiwatana, Retrovirology, 2012. 9(1): p. 17; PMID[22348230].</p>
    <p><b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>
    
    <p>11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22300661">Discovery of the First Small Molecule Inhibitor of Human DDX3 Specifically Designed to Target the RNA Binding Site: Towards the Next Generation HIV-1 Inhibitors.</a> Radi, M., F. Falchi, A. Garbelli, A. Samuele, V. Bernardo, S. Paolucci, F. Baldanti, S. Schenone, F. Manetti, G. Maga, and M. Botta, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(5): p. 2094-2098; PMID[22300661].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p class="plaintext"> </p>

    <p>12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22351667">Comparative Study of Biological Activity of Three Commercial Products of Sasa senanensis Rehder Leaf Extract.</a> Sakagami, H., S. Iwamoto, T. Matsuta, K. Satoh, C. Shimada, T. Kanamoto, S. Terakubo, H. Nakashima, Y. Morita, A. Ohkubo, T. Tsuda, K. Sunaga, M. Kitajima, H. Oizumi, and T. Oizumi, In Vivo, 2012. 26(2): p. 259-264; PMID[22351667].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p>13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238126">Critical Differences in HIV-1 and HIV-2 Protease Specificity for Clinical Inhibitors.</a> Tie, Y., Y.F. Wang, P.I. Boross, T.Y. Chiu, A.K. Ghosh, J. Tozser, J.M. Louis, R.W. Harrison, and I.T. Weber, Protein Science, 2012. 21(3): p. 339-350; PMID[22238126].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22358223">Phosphoramidate Prodrugs of (-)-beta-D-(2R,4R)-Dioxolane-thymine (DOT) as Potent anti-HIV Agents.</a> Wang, P., S. Rachakonda, V. Zennou, M. Keilman, C. Niu, D. Bao, B.S. Ross, P.A. Furman, M.J. Otto, and M.J. Sofia, Antiviral Chemistry &amp; Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22358223].</p>

    <p><b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22306123">Synthesis and Biological Evaluation of Novel Amprenavir-based P1-substituted Bi-aryl Derivatives as Ultra-potent HIV-1 Protease Inhibitors.</a> Yan, J., N. Huang, S. Li, L.M. Yang, W. Xing, Y.T. Zheng, and Y. Hu, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(5): p. 1976-1979; PMID[22306123].
    <br />
    <b>[PubMed]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>
    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299656000026">Dolutegravir-A Promising Antiretroviral in Development.</a> Boyd, M., Lancet Infectious Diseases, 2012. 12(2): p. 90-91; ISI[000299656000026].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299643100005">Plant Defensins and Defensin-Like Peptides - Biological Activities and Biotechnological Applications.</a> Carvalho, A.D. and V.M. Gomes, Current Pharmaceutical Design, 2011. 17(38): p. 4270-4293; ISI[000299643100005].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299238700015">Practical Convergent Laboratory-scale Synthesis of a CCR5 Receptor Antagonist.</a> Crawford, J.B., G. Chen, B. Carpenter, T. Wilson, J. Ji, R.T. Skerlj, and G.J. Bridger, Organic Process Research &amp; Development, 2012. 16(1): p. 109-116; ISI[000299238700015].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299566600008">Discovery of 5,6-Dihydro-indolo[1,2-a]quinoxaline Derivatives as New HIV-1 Inhibitors in Vitro.</a> Fan, L.L., N. Huang, R.G. Yang, S.Z. He, L.M. Yang, H. Xu, and Y.T. Zheng, Letters in Drug Design &amp; Discovery, 2012. 9(1): p. 44-47; ISI[000299566600008].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297951300021">Targeting Zinc Finger Domains with Small Molecules: Solution Structure and Binding Studies of the RanBP2-Type Zinc Finger of RBM5.</a> Farina, B., R. Fattorusso, and M. Pellecchia, ChemBioChem, 2011. 12(18): p. 2837-2845; ISI[000297951300021].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299642200006">Amphipathic Properties of HIV-1 gp41 Fusion Inhibitors.</a> Gochin, M. and G.Y. Zhou, Current Topics in Medicinal Chemistry, 2011. 11(24): p. 3022-3032; ISI[000299642200006].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299128000023">Interactions Between Buprenorphine and the Protease Inhibitors Darunavir-Ritonavir and Fosamprenavir-Ritonavir.</a> Gruber, V.A., P.M. Rainey, D.E. Moody, G.D. Morse, Q. Ma, S. Prathikanti, P.A. Pade, A.A.H. Alvanzo, and E.F. McCance-Katz, Clinical Infectious Diseases, 2012. 54(3): p. 414-423; ISI[000299128000023].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299378304759">HIV Attachment Inhibitors with Measurable Aqueous Solubility, Significant Bioavailability, and High Potency.</a> Johnson, B.L., D.J. Carini, A. Regueiro-Ren, S. Rahematpura, L.G. Hamann, J.F. Kadow, M. Zheng, D.D. Parker, B.D. Nowicka-Sans, S.R. Zhang, P.F. Lin, and N.A. Meanwell, Abstracts of Papers of the American Chemical Society, 2011. 242; ISI[000299378304759].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299645400004">In Vitro anti-HIV-1 Activity of the Aqueous Extract of Asterina pectinifera</a>. Karadeniz, F., M.Z. Karagozlu, C.S. Kong, and S.K. Kim, Current HIV Research, 2011. 9(2): p. 95-102; ISI[000299645400004].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299577500004">A QSAR Study on Some Series of HIV-1 Integrase Inhibitors.</a> Kaushik, S., S.P. Gupta, P.K. Sharma, and Z. Anwar, Medicinal Chemistry, 2011. 7(6): p. 553-560; ISI[000299577500004].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299769900017">A Curious Conformational Property of 2-Amino-4-thiazolyl-methoxyimino Polymers Exhibiting Activity against HIV-1 Reverse Transcriptase.</a> Kill, K.A., A.C. Smith, T. Mizdalo, E.H. Al-Mahrouq, Nidhi, and D.B. Boyd, Structural Chemistry, 2012. 23(1): p. 137-145; ISI[000299769900017].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299516300005">Synthesis and HIV-1 Reverse Transcriptase Inhibition Activity of 1,4-Naphthoquinone Derivatives.</a> Mahapatra, A., T.E. Tshikalange, J.J.M. Meyer, and N. Lall, Chemistry of Natural Compounds, 2012. 47(6): p. 883-887; ISI[000299516300005].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299638000011">Targeting Protein-Protein and Protein-Nucleic Acid Interactions for anti-HIV Therapy.</a> Mori, M., F. Manetti, and M. Botta, Current Pharmaceutical Design, 2011. 17(33): p. 3713-3728; ISI[000299638000011].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299677400010">Defensins and Other Biocidal Proteins from Bean Seeds with Medicinal Activities.</a> Ng, T.B., J.H. Wong, and E.F. Fang, Current Medicinal Chemistry, 2011. 18(36): p. 5644-5654; ISI[000299677400010].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299378304760">Design, Synthesis and SAR Study of Bridged Tricyclic pyrimidinone as HIV Integrase Inhibitors.</a> Patel, M., B.N. Naidu, I. Dicker, H. Hingley, Z.Y. Lin, B. Terry, T. Protack, N.A. Meanwell, M. Krystal, and M.A. Walker, Abstracts of Papers of the American Chemical Society, 2011. 242; ISI[000299378304760].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299378304761">Preclinical Drug Metabolism and Pharmacokinetic Evaluation of Novel HMDCK Prodrugs as Potent anti-HIV NNRTIs.</a> Qian, K.D., L. Xie, and K.H. Lee, Abstracts of Papers of the American Chemical Society, 2011. 242; ISI[000299378304761].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>
    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299672100004">Investigation of Structure-Activity Relationship Between Chemical Structure and CCR5 Anti HIV-1 Activity in a Class of 1-[N-(Methyl)-N- (phenylsulfonyl) amino]-2-(phenyl)-4-[4-(substituted)piperidin-1-yl]butane Derivatives: The Electronic-Topological Approach.</a> Saracoglu, M., S.G. Kandemirli, M.A. Basaran, H. Sayiner, and F. Kandemirli, Current HIV Research, 2011. 9(5): p. 300-312; ISI[000299672100004].
    <br />
    <b>[WOS]</b>. HIV_0217-030112.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299378304625">Towards the Synthesis of Novel Boronates as Potential HIV-1 Protease Inhibitors.</a> Schreiber, J.D., A.L. Faulkner, L.L. Holmberg, J.J. Jennings, M.D. Frank, J.L. Nye, D.R. Clayton, and L. Fabry-Asztalos, Abstracts of Papers of the American Chemical Society, 2011. 242; ISI[000299378304625].</p>

    <p><b>[WOS]</b>. HIV_0217-030112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
