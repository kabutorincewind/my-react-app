

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-324.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yuEXse5+G7Ii9TJQ3sQ2WBWtNUmXhKEPO17wntdu+WyVT3si+rcYoBJ/CPIs8lJOdX5DcGlSX+Rec3CMT0u93ZPG+B3m/M8OZAOGkLnw8d5sN9McvJHmlteSYFA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AD2F5F59" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-324-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45412   OI-LS-324; PUBMED-OI-6/13/2005</p>

    <p class="memofmt1-2">          Expression and purification of a functionally active recombinant GDP-mannosyltransferase (PimA) from Mycobacterium tuberculosis H37Rv</p>

    <p>          Gu, X, Chen, M, Wang, Q, Zhang, M, Wang, B, and Wang, H</p>

    <p>          Protein Expr Purif <b>2005</b>.  42(1): 47-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15939292&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15939292&amp;dopt=abstract</a> </p><br />

    <p>2.     45413   OI-LS-324; PUBMED-OI-6/13/2005</p>

    <p class="memofmt1-2">          Antitubercular Activity and Inhibitory Effect on Epstein-Barr Virus Activation of Sterols and Polyisoprenepolyols from an Edible Mushroom, Hypsizigus marmoreus</p>

    <p>          Akihisa, T, Franzblau, SG, Tokuda, H, Tagata, M, Ukiya, M, Matsuzawa, T, Metori, K, Kimura, Y, Suzuki, T, and Yasukawa, K</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(6): 1117-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930759&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15930759&amp;dopt=abstract</a> </p><br />

    <p>3.     45414   OI-LS-324; PUBMED-OI-6/13/2005</p>

    <p class="memofmt1-2">          The effects of ponazuril on development of apicomplexans in vitro</p>

    <p>          Mitchell, SM, Zajac, AM, Davis, WL, Kennedy, TJ, and Lindsay, DS</p>

    <p>          J Eukaryot Microbiol <b>2005</b>.  52(3): 231-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15926999&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15926999&amp;dopt=abstract</a> </p><br />

    <p>4.     45415   OI-LS-324; PUBMED-OI-6/13/2005</p>

    <p class="memofmt1-2">          Solid lipid particle-based inhalable sustained drug delivery system against experimental tuberculosis</p>

    <p>          Pandey, R and Khuller, GK</p>

    <p>          Tuberculosis (Edinb) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15922668&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15922668&amp;dopt=abstract</a> </p><br />

    <p>5.     45416   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of some new 2-(2-(p-chlorophenyl) benzimidazol-1-yl methyl)-5-substituted amino-[1,3,4]-thiadiazoles</p>

    <p>          Kilcigil, GA, Kus, C, Altanlar, N, and Ozbey, S</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2005</b>.  29(2): 153-162, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100006</a> </p><br />
    <br clear="all">

    <p>6.     45417   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Synthesis of some novel optically active isocoumarin and 3,4-dihydroisocoumarin containing L-valine and L-leucine moieties</p>

    <p>          Zamani, K, Faghihi, K, and Ebrahimi, S</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2005</b>.  29(2): 171-175, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100008</a> </p><br />

    <p>7.     45418   OI-LS-324; EMBASE-OI-6/13/2005</p>

    <p class="memofmt1-2">          The 4&#39;,4&#39;-difluoro analog of 5&#39;-noraristeromycin: A new structural prototype for possible antiviral drug development toward orthopoxvirus and cytomegalovirus</p>

    <p>          Roy, Atanu, Schneller, Stewart W, Keith, Kathy A, Hartline, Caroll B, and Kern, Earl R</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(14): 4443-4449</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G7DY0X-2/2/0cc1862b6da480861f34c90f1c09bbc6">http://www.sciencedirect.com/science/article/B6TF8-4G7DY0X-2/2/0cc1862b6da480861f34c90f1c09bbc6</a> </p><br />

    <p>8.     45419   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activities of some new flavonyl pro-drug esters of ampicillin</p>

    <p>          Unlusoy, MC, Altanlar, N, and Ertan, R</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2005</b>.  29(2): 187-191, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100010</a> </p><br />

    <p>9.     45420   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro antimicrobial and cytotoxicity activities of 2-[(2-nitro-1-phenylalkyl)thio]benzoic acid derivatives</p>

    <p>          Gokce, M, Utku, S, Bercin, E, Ozcelik, B, Karaoglu, T, and Noyanalpan, N</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2005</b>.  29(2): 207-217, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229088100013</a> </p><br />

    <p>10.   45421   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          An unbiased cell morphology-based screen for new, biologically active small molecules</p>

    <p>          Tanaka, M, Bateman, R, Rauh, D, Vaisberg, E, Ramachandani, S, Zhang, C, Hansen, KC, Burlingame, AL, Trautman, JK, Shokat, KM, and Adams, CL</p>

    <p>          PLOS BIOLOGY <b>2005</b>.  3(5): 764-776, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229125400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229125400007</a> </p><br />

    <p>11.   45422   OI-LS-324; EMBASE-OI-6/13/2005</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus translation and subgenomic replication by siRNAs directed against highly conserved HCV sequence and cellular HCV cofactors</p>

    <p>          Korf, Mortimer, Jarczak, Dominik, Beger, Carmela, Manns, Michael P, and Kru[#x308]ger, Martin</p>

    <p>          Journal of Hepatology <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4G5BDYT-1/2/b2c599fb03f8012a9bebe6b84d38c2ec">http://www.sciencedirect.com/science/article/B6W7C-4G5BDYT-1/2/b2c599fb03f8012a9bebe6b84d38c2ec</a> </p><br />
    <br clear="all">

    <p>12.   45423   OI-LS-324; EMBASE-OI-6/13/2005</p>

    <p class="memofmt1-2">          Mizoribine inhibits hepatitis C virus RNA replication: Effect of combination with interferon-[alpha]</p>

    <p>          Naka, Kazuhito, Ikeda, Masanori, Abe, Ken-ichi, Dansako, Hiromichi, and Kato, Nobuyuki</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  330(3): 871-879</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4FTXXH3-D/2/62eda24fff55e4ce0a6872283c7612f1">http://www.sciencedirect.com/science/article/B6WBK-4FTXXH3-D/2/62eda24fff55e4ce0a6872283c7612f1</a> </p><br />

    <p>13.   45424   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Identification of FBL2 as a geranylgeranylated required for hepatitis C cellular protein virus RNA replication</p>

    <p>          Wang, CF, Gale, M, Keller, BC, Huang, H, Brown, MS, Goldstein, JL, and Ye, J</p>

    <p>          MOLECULAR CELL  <b>2005</b>.  18(4): 425-434, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229271700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229271700006</a> </p><br />

    <p>14.   45425   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Targeting IdeR as a potential therapy against mycobacterium tuberculosis infections</p>

    <p>          Monfeli, RR, Chou, J, Wisedchaisri, G, Oram, D, Holmes, RK, Hol, WGJ, and Beeson, C</p>

    <p>          FASEB JOURNAL <b>2005</b>.  19(4): A545-A546, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610703596">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227610703596</a> </p><br />

    <p>15.   45426   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Inhibitors of HCVNS5B polymerase. Part 1: Evaluation of the southern region of (2Z)-2-(benzoylamino)-3-(5-phenyl-2-furyl)acrylic acid</p>

    <p>          Pfefferkorn, JA, Greene, ML, Nugent, RA, Gross, RJ, Mitchell, MA, Finzel, BC, Harris, MS, Wells, PA, Shelly, JA, Anstadt, RA, Kilkuskie, RE, Kopta, LA, and Schwende, FJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(10): 2481-2486, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500010</a> </p><br />

    <p>16.   45427   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          In vitro advanced antimycobacterial screening of isoniazid-related hydrazones, hydrazides and cyanoboranes: Part 14</p>

    <p>          Maccari, R, Ottana, R, and Vigorita, MG</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(10): 2509-2513, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500015</a> </p><br />

    <p>17.   45428   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 5-arylamino-4,7-dioxobenzo[b]thiophenes</p>

    <p>          Ryu, CK, Lee, SK, Han, JY, Jung, OJ, Lee, JY, and Jeong, SH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(10): 2617-2620, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229160500037</a> </p><br />

    <p>18.   45429   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          6-Benzylthioinosine analogues as subversive substrate of Toxoplasma gondii adenosine kinase: Activities and selective toxicities</p>

    <p>          Rais, RH, Al, Safarjalani ON, Yadav, V, Guarcello, V, Kirk, M, Chu, CK, Naguib, FNM, and el, Kouni MH</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2005</b>.  69(10): 1409-1419, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229197500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229197500002</a> </p><br />

    <p>19.   45430   OI-LS-324; WOS-OI-6/5/2005</p>

    <p class="memofmt1-2">          Efficacy of two peroxygen-based disinfectants for inactivation of Cryptosporidium parvum ocysts</p>

    <p>          Quilez, J, Sanchez-Acedo, C, Avendano, C, del, Cacho E, and Lopez-Bernad, F</p>

    <p>          APPLIED AND ENVIRONMENTAL MICROBIOLOGY <b>2005</b>.  71(5): 2479-2483, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229105300037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229105300037</a> </p><br />

    <p>20.   45431   OI-LS-324; WOS-OI-6/12/2005</p>

    <p class="memofmt1-2">          Identifying the stable polymorph early in the drug discovery-development process</p>

    <p>          Miller, JM, Collman, BM, Greene, LR, Grant, DJW, and Blackburn, AC</p>

    <p>          PHARMACEUTICAL DEVELOPMENT AND TECHNOLOGY <b>2005</b>.  10(2): 291-297, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229326600014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229326600014</a> </p><br />

    <p>21.   45432   OI-LS-324; WOS-OI-6/12/2005</p>

    <p class="memofmt1-2">          Nucleoside diphosphate kinase from Mycobacterium tuberculosis cleaves single strand DNA within the human c-myc promoter in an enzyme-catalyzed reaction</p>

    <p>          Kumar, P, Verma, A, Saini, AK, Chopra, P, Chakraborti, PK, Singh, Y, and Chowdhury, S</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2005</b>.  33(8): 2707-2714, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229113100045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229113100045</a> </p><br />

    <p>22.   45433   OI-LS-324; WOS-OI-6/12/2005</p>

    <p class="memofmt1-2">          Cidofovir peptide conjugates as prodrugs</p>

    <p>          McKenna, CE, Kashemirov, BA, Eriksson, U, Amidon, GL, Kish, PE, Mitchell, S, Kim, JS, and Hilfinger, JM</p>

    <p>          JOURNAL OF ORGANOMETALLIC CHEMISTRY <b>2005</b>.  690(10): 2673-2678, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229392800036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229392800036</a> </p><br />

    <p>23.   45434   OI-LS-324; WOS-OI-6/12/2005</p>

    <p class="memofmt1-2">          In vitro activity of Fluoroquinolones against Mycobacterium tuberculosis</p>

    <p>          Sulochana, S, Rahman, F, and Paramasivan, CN</p>

    <p>          JOURNAL OF CHEMOTHERAPY <b>2005</b>.  17(2): 169-173, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229303900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229303900006</a> </p><br />

    <p>24.   45435   OI-LS-324; WOS-OI-6/12/2005</p>

    <p class="memofmt1-2">          Prediction methods and databases within chemoinformatics: emphasis on drugs and drug candidates</p>

    <p>          Jonsdottir, SO, Jorgensen, FS, and Brunak, S</p>

    <p>          BIOINFORMATICS  <b>2005</b>.  21(10): 2145-2160, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229285600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229285600001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
