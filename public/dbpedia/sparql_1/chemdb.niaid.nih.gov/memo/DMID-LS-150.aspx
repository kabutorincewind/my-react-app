

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-150.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5mm8Sy7M0kStnqGuSW/vicTR3VBvpnYMxYMn4lK+i1wbBnzT0blmiHMNL21REV8WnMP8TAyiOJiYqtdRe328ysD/gvJq8DzQMNDN7E3toLcYecQTh7hHSdHxGkI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="89F4FBF1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-150-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61381   DMID-LS-150; EMBASE-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Hepatitis viruses in the XXI century: Replicative and Evolutionary Aspects of Hepatitis Viruses</p>

    <p>          Cristina, Juan and Bartenschlager, Ralf</p>

    <p>          Virus Research  <b>2007</b>.  127(2): 129-130</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4P2J30B-2/2/9247a098aff73580e924a0c820572f5f">http://www.sciencedirect.com/science/article/B6T32-4P2J30B-2/2/9247a098aff73580e924a0c820572f5f</a> </p><br />

    <p>2.     61382   DMID-LS-150; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Complete Genomes of Hepatitis C Virus (Hcv) Subtypes 6c, 5l, 6o, 6p and 6q: Completion of a Full Panel of Genomes for Hcv Genotype 6</p>

    <p>          Lu, L. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2007</b>.  88: 1519-1525</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246532300016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246532300016</a> </p><br />

    <p>3.     61383   DMID-LS-150; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Durability of Response to Peginterferon Alfa-2a (40 Kd) (Pegasys (R)) in Asian Patients With Hbeag-Positive Chronic Hepatitis B: 12 Month Follow-up Data From a Large, Randomized Study</p>

    <p>          Lau, G. <i>et al.</i></p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A88 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400050</a> </p><br />

    <p>4.     61384   DMID-LS-150; EMBASE-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Ribavirin and mycophenolic acid markedly potentiate the anti-hepatitis B virus activity of entecavir</p>

    <p>          Ying, Chunxiao, Colonno, Richard, De Clercq, Erik, and Neyts, Johan</p>

    <p>          Antiviral Research <b>2007</b>.  73(3): 192-196</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4M7CR5C-1/2/758f8107d1b1c04a9d030be4f22be357">http://www.sciencedirect.com/science/article/B6T2H-4M7CR5C-1/2/758f8107d1b1c04a9d030be4f22be357</a> </p><br />

    <p>5.     61385   DMID-LS-150; EMBASE-DMID-7/30/2007</p>

    <p class="memofmt1-2">          An overview of the epidemiology of avian influenza: 4th International Veterinary Vaccines and Diagnostics Conference, Oslo, 25-29 June 2006, 4th International Veterinary Vaccines and Diagnostics Conference</p>

    <p>          Alexander, Dennis J</p>

    <p>          Vaccine <b>2007</b>.  25(30): 5637-5644</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4M9HX09-4/2/899f8c43f7bd63b80d0079a808b8950d">http://www.sciencedirect.com/science/article/B6TD4-4M9HX09-4/2/899f8c43f7bd63b80d0079a808b8950d</a> </p><br />

    <p>6.     61386   DMID-LS-150; EMBASE-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Control and prevention of avian influenza in an evolving scenario: 4th International Veterinary Vaccines and Diagnostics Conference, Oslo, 25-29 June 2006, 4th International Veterinary Vaccines and Diagnostics Conference</p>

    <p>          Capua, Ilaria and Marangon, Stefano</p>

    <p>          Vaccine <b>2007</b>.  25(30): 5645-5652</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4M9HX09-2/2/93992312c609c52ed45d33c2c647def1">http://www.sciencedirect.com/science/article/B6TD4-4M9HX09-2/2/93992312c609c52ed45d33c2c647def1</a> </p><br />

    <p>7.     61387   DMID-LS-150; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Inhibition of Duck Hepatitis B Virus Infection in Vitro by Oxymatrine Using Primary Duck Hepatocytes Culture System</p>

    <p>          Xu, B., Zhou, S., and Qu, D.</p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400268">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400268</a> </p><br />

    <p>8.     61388   DMID-LS-150; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          The Effects of Apobec 3g on Hbv Replication</p>

    <p>          Mohammed, E. <i>et al.</i></p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A218</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400492">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400492</a> </p><br />

    <p>9.     61389   DMID-LS-150; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Characterization of Two Novel Cutaneous Human Papillomaviruses, Hpv93 and Hpv96</p>

    <p>          Vasiljevic, N. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2007</b>.  88: 1479-1483</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246532300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246532300011</a> </p><br />

    <p>10.   61390   DMID-LS-150; WOS-DMID-7/27/2007</p>

    <p class="memofmt1-2">          Characterization of Hepatitis C Virus Subgenomic Replicon Resistance to Cyclosporine in Vitro</p>

    <p>          Robida, J. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(11): 5829-5840</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246866300037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246866300037</a> </p><br />

    <p>11.   61391   DMID-LS-150; WOS-DMID-7/27/2007</p>

    <p class="memofmt1-2">          Evaluation of a Diverse Set of Potential P-1 Carboxylic Acid Bioisosteres in Hepatitis C Virus Ns3 Protease Inhibitors</p>

    <p>          Ronn, R. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(12): 4057-4068</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246870400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246870400010</a> </p><br />

    <p>12.   61392   DMID-LS-150; WOS-DMID-7/27/2007</p>

    <p class="memofmt1-2">          Simultaneous Stereoselective 4-Amination With Cyclic Secondary Amines and 2-O-Deacetylation of Peracetylated Sialic Acid Derivatives</p>

    <p>          Ye, D. <i>et al.</i></p>

    <p>          Tetrahedron Letters <b>2007</b>.  48(23): 4023-4027</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246796000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246796000019</a> </p><br />

    <p>13.   61393   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Induction of antiviral cytidine deaminases does not explain the inhibition of hepatitis B virus replication by interferons</p>

    <p>          Jost, S, Turelli, P, Mangeat, B, Protzer, U, and Trono, D</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17652382&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17652382&amp;dopt=abstract</a> </p><br />

    <p>14.   61394   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          An in vitro evaluation of extracts from some medicinal plants in Kenya against herpes simplex virus</p>

    <p>          Kofi-Tsekpo, MW, Rukunga, GM, Kurokawa, M, Kageyama, S, Mungai, GM, Muli, JM, Tolo, FM, Kibaya, RM, Muthaura, CN, Kanyara, JN, Tukei, PM, and Shiraki, K</p>

    <p>          Afr J Health Sci <b>2001</b>.  8(1-2): 61-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17650049&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17650049&amp;dopt=abstract</a> </p><br />

    <p>15.   61395   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Bismuth Complexes Inhibit the SARS Coronavirus</p>

    <p>          Yang, N, Tanner, JA, Zheng, BJ, Watt, RM, He, ML, Lu, LY, Jiang, JQ, Shum, KT, Lin, YP, Wong, KL, Lin, MC, Kung, HF, Sun, H, and Huang, JD</p>

    <p>          Angew Chem Int Ed Engl <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17645269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17645269&amp;dopt=abstract</a> </p><br />

    <p>16.   61396   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Non-nucleoside inhibitors of the measles virus RNA-dependent RNA polymerase complex activity: Synthesis and in vitro evaluation</p>

    <p>          Sun, A, Chandrakumar, N, Yoon, JJ, Plemper, RK, and Snyder, JP</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17643302&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17643302&amp;dopt=abstract</a> </p><br />

    <p>17.   61397   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Targeting the glycans of glycoproteins: a novel paradigm for antiviral therapy</p>

    <p>          Balzarini, J</p>

    <p>          Nat Rev Microbiol <b>2007</b>.  5(8): 583-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17632570&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17632570&amp;dopt=abstract</a> </p><br />

    <p>18.   61398   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Recent progress in henipavirus research</p>

    <p>          Halpin, K and Mungall, BA</p>

    <p>          Comp Immunol Microbiol Infect Dis <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629946&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   61399   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Pharmacokinetic enhancement of the hepatitis C virus protease inhibitors VX-950 and SCH 503034 by co-dosing with ritonavir</p>

    <p>          Kempf, DJ, Klein, C, Chen, HJ, Klein, LL, Yeung, C, Randolph, JT, Lau, YY, Chovan, LE, Guan, Z, Hernandez, L, Turner, TM, Dandliker, PJ, and Marsh, KC</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(3): 163-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626600&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626600&amp;dopt=abstract</a> </p><br />

    <p>20.   61400   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          N-acetylcysteine synergizes with oseltamivir in protecting mice from lethal influenza infection</p>

    <p>          Garozzo, A, Tempera, G, Ungheri, D, Timpanaro, R, and Castro, A</p>

    <p>          Int J Immunopathol Pharmacol <b>2007</b>.  20(2): 349-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624247&amp;dopt=abstract</a> </p><br />

    <p>21.   61401   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          The cysteine protease inhibitors cystatins inhibit herpes simplex virus type 1-induced apoptosis and virus yield in HEp-2 cells</p>

    <p>          Peri, P, Hukkanen, V, Nuutila, K, Saukko, P, Abrahamson, M, and Vuorinen, T</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 8): 2101-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622610&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622610&amp;dopt=abstract</a> </p><br />

    <p>22.   61402   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Application of ring-closing metathesis for the synthesis of macrocyclic peptidomimetics as inhibitors of HCV NS3 protease</p>

    <p>          Velazquez, F, Venkatraman, S, Wu, W, Blackman, M, Prongay, A, Girijavallabhan, V, Shih, NY, and Njoroge, FG</p>

    <p>          Org Lett <b>2007</b>.  9(16): 3061-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17608487&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17608487&amp;dopt=abstract</a> </p><br />

    <p>23.   61403   DMID-LS-150; PUBMED-DMID-7/30/2007</p>

    <p class="memofmt1-2">          Synthetic organic chemistry based on small ring compounds</p>

    <p>          Nemoto, H</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2007</b>.  55(7): 961-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17603183&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17603183&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
