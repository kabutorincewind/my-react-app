

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-03-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LG8rS5PRkxMheVAKeJjwQiLROhz6w5ObCltrHGB2gWrVGuElkXJcWwxfJVoji6OuL+xArQNtmP1Ied0ieHdENkU8C/cApM+rmuPw2HnIAVI9PI53uCIUQeAh5+E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="65A57D6F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 28 - March 13, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24586176">Highly Active Antiretroviral Therapies are Effective against HIV-1 Cell-to-cell Transmission.</a> Agosto, L.M., P. Zhong, J. Munro, and W. Mothes. Plos Pathogens, 2014. 10(2): p. e1003982. PMID[24586176].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24594934">Long-acting Integrase Inhibitor Protects Macaques from Intrarectal Simian/Human Immunodeficiency Virus.</a> Andrews, C.D., W.R. Spreen, H. Mohri, L. Moss, S. Ford, A. Gettie, K. Russell-Lodrigue, R.P. Bohm, C. Cheng-Mayer, Z. Hong, M. Markowitz, and D.D. Ho. Science, 2014. 343(6175): p. 1151-1154. PMID[24594934].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24246953">Engineering a Switch-on Peptide to Ricin A Chain for Increasing Its Specificity Towards HIV-infected Cells.</a> Au, K.Y., R.R. Wang, Y.T. Wong, K.B. Wong, Y.T. Zheng, and P.C. Shaw. Biochimica Biophysica Acta, 2014. 1840(3): p. 958-963. PMID[24246953].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24165098">Biological Activity of Sporolides A and B from Salinispora tropica: In Silico Target Prediction Using Ligand-based Pharmacophore Mapping and in Vitro Activity Validation on HIV-1 Reverse Transcriptase.</a>Dineshkumar, K., V. Aparna, K.Z. Madhuri, and W. Hopper. Chemical Biology &amp; Drug Design, 2014. 83(3): p. 350-361. PMID[24165098].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24568567">Cytotoxic and HIV-1 Enzyme Inhibitory Activities of Red Sea Marine Organisms.</a>Ellithey, M.S., N. Lall, A.A. Hussein, and D. Meyer. BMC Complementary and Alternative Medicine, 2014. 14(1): p. 77. PMID[24568567].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24371077">GS-8374, a Prototype Phosphonate-containing Inhibitor of HIV-1 Protease, Effectively Inhibits Protease Mutants with Amino acid Insertions.</a>Grantz Saskova, K., M. Kozisek, K. Stray, D. de Jong, P. Rezacova, J. Brynda, N.M. van Maarseveen, M. Nijhuis, T. Cihlar, and J. Konvalinka. Journal of Virology, 2014. 88(6): p. 3586-3590. PMID[24371077]. .</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24379202">In Vitro Characterization of MK-1439, a Novel HIV-1 Nonnucleoside Reverse Transcriptase Inhibitor.</a> Lai, M.T., M. Feng, J.P. Falgueyret, P. Tawa, M. Witmer, D. Distefano, Y. Li, J. Burch, N. Sachs, M. Lu, E. Cauchon, L.C. Campeau, J. Grobler, Y. Yan, Y. Ducharme, B. Cote, E. Asante-Appiah, D.J. Hazuda, and M.D. Miller. Antimicrobial Agents and Chemotherapy, 2014. 58(3): p. 1652-1663. PMID[24379202].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24480359">Semi-synthesis of Acylated triterpenes from Olive-oil Industry Wastes for the Development of Anticancer and anti-HIV Agents.</a> Parra, A., S. Martin-Fonseca, F. Rivas, F.J. Reyes-Zurita, M. Medina-O&#39;Donnell, A. Martinez, A. Garcia-Granados, J.A. Lupianez, and F. Albericio. European Journal of Medicinal Chemistry, 2014. 74: p. 278-301. PMID[24480359].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24390333">A Double-mimetic Peptide Efficiently Neutralizes HIV-1 by Bridging the CD4- and Coreceptor-binding Sites of gp120.</a> Quinlan, B.D., V.R. Joshi, M.R. Gardner, K.H. Ebrahimi, and M. Farzan. Journal of Virology, 2014. 88(6): p. 3353-3358. PMID[24390333].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24594845">R88-APOBEC3Gm Inhibits the Replication of Both Drug-resistant Strains of HIV-1 and Viruses Produced from Latently Infected Cells.</a> Wang, X., Z. Ao, K. Danappa Jayappa, B. Shi, G. Kobinger, and X. Yao. Molecular therapy. Nucleic acids, 2014. 3: p. e151. PMID[24594845].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0228-031314.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330768100022">Synthesis of Novel Pyrazolobenzothiazine 5,5-dioxide Derivatives as Potent anti-HIV-1 Agents.</a> Ahmad, M., S. Aslam, M.H. Bukhari, C. Montero, M. Detorio, M. Parvez, and R.F. Schinazi. Medicinal Chemistry Research, 2014. 23(3): p. 1309-1319. ISI[000330768100022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330215300497">Improvement of the HIV Fusion Inhibitor C34 Activity by Membrane Anchoring and Enhanced Exposure.</a> Augusto, M.T., A. Hollmann, M. Castanho, A. Pessi, and N.C. Santos. European Biophysics Journal with Biophysics Letters, 2013. 42: p. S165-S165. ISI[000330215300497].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330163300002">On the Reaction Products of 4-Substituted 3-Pyridinesulfonamides with Some Benzenesulfonyl chloride Derivatives.</a>Brzozowski, Z., J. Slawinski, and K. Szafranski. Journal of Heterocyclic Chemistry, 2014. 51(1): p. 11-17. ISI[000330163300002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330507300065">Linker-extended Native Cyanovirin-N Facilitates PEGylation and Potently Inhibits HIV-1 by Targeting the Glycan Ligand.</a>Chen, J., D.E. Huang, W. Chen, C.W. Guo, B. Wei, C.C. Wu, Z. Peng, J. Fan, Z.B. Hou, Y.S. Fang, Y.F. Wang, K. Kitazato, G.Y. Yu, C.B. Zou, C.W. Qian, and S. Xiong. PloS One, 2014. 9(1): p. e86455. ISI[000330507300065].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330570000008">Discovery of a Small Molecule Agonist of Phosphatidylinositol 3-Kinase P110 Alpha That Reactivates Latent HIV-1.</a> Doyon, G., M.D. Sobolewski, K. Huber, D. McMahon, J.W. Mellors, and N. Sluis-Cremer. PloS One, 2014. 9(1): p. e84964. ISI[000330570000008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330554400067">The Synthesis, Antiviral, Cytostatic and Cytotoxic Evaluation of a New Series of Acyclonucleotide Analogues with a 1,2,3-Triazole Linker.</a>Glowacka, I.E., J. Balzarini, and A.E. Wroblewski. European Journal of Medicinal Chemistry, 2013. 70: p. 703-722. ISI[000330554400067].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330535400062">The CD8-Derived Chemokine XCL1/Lymphotactin Is a Conformation-dependent, Broad-spectrum Inhibitor of HIV-1.</a> Guzzo, C., J. Fox, Y. Lin, H.Y. Miao, R. Cimbro, B.F. Volkman, A.S. Fauci, and P. Lusso. Plos Pathogens, 2013. 9(12): p. e1003852. ISI[000330535400062]. .</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330852900002">Dynamins are Forever: MxB Inhibits HIV-1.</a>Haller, O. Cell Host &amp; Microbe, 2013. 14(4): p. 371-373. ISI[000330852900002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330265700002">Dithiocarbamates as Precursors in Organic Chemistry; Synthesis and Uses.</a>Hassan, E.A. and S.E. Zayed. Phosphorus Sulfur and Silicon and the Related Elements, 2014. 189(3): p. 300-323. ISI[000330265700002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330215300340">New HIV Fusion Inhibitors LJ001 and JL103 Act by Modifying Viral Lipid Membrane Properties.</a> Hollmann, A., F. Vigant, M. Castanho, B. Lee, and N.C. Santos. European Biophysics Journal with Biophysics Letters, 2013. 42: p. S123-S123. ISI[000330215300340].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329575900011">Cytolytic Nanoparticles Attenuate HIV-1 Infectivity.</a>Hood, J.L., A.P. Jallouk, N. Campbell, L. Ratner, and S.A. Wickline. Antiviral Therapy, 2013. 18(1): p. 95-103. ISI[000329575900011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330294400051">A New Dibenzocyclooctadiene Lignan from Stems of Schisandra lancifolia and Its Bioactivities.</a> Li, Y.K., X.X. Wu, M. Li, G. Du, Q.F. Hu, and X.M. Gao. Asian Journal of Chemistry, 2014. 26(1): p. 224-226. ISI[000330294400051].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330458000065">HIV-1 Variants with Mutations in the gp41 Pocket Region are Resistant to HIV Fusion Inhibitors with Pocket-binding Domain, but Sensitive to T20.</a>Lua, L., P. Tong, X.W. Yu, C.G. Pan, P. Zou, Y.H. Chen, and S.B. Jiang. JAIDS-Journal of Acquired Immune Deficiency Syndromes, 2013. 62: p. 65-65. ISI[000330458000065].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330554400028">Design, Synthesis and Biological Evaluation of Phosphorodiamidate Prodrugs of Antiviral and Anticancer Nucleosides.</a>McGuigan, C., C. Bourdin, M. Derudas, N. Hamon, K. Hinsinger, S. Kandil, K. Madela, S. Meneghesso, F. Pertusati, M. Serpi, M. Slusarczyk, S. Chamberlain, A. Kolykhalov, J. Vernachio, C. Vanpouille, A. Introini, L. Margolis, and J. Balzarini. European Journal of Medicinal Chemistry, 2013. 70: p. 326-340. ISI[000330554400028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330521400001">Molecular Docking Studies of Marine Diterpenes as Inhibitors of Wild-type and Mutants HIV-1 Reverse Transcriptase.</a>Miceli, L.A., V.L. Teixeira, H.C. Castro, C.R. Rodrigues, J.F.R. Mello, M.G. Albuquerque, L.M. Cabral, M.A. de Brito, and A.M.T. de Souza. Marine Drugs, 2013. 11(11): p. 4127-4143. ISI[000330521400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330713200017">Synthesis of A-pentacyclic Triterpene alpha,beta-Alkenenitriles.</a>Pereslavtseva, A.V., I.A. Tolmacheva, P.A. Slepukhin, O.S. El&#39;tsov, Kucherov, II, V.F. Eremin, and V.V. Grishko. Chemistry of Natural Compounds, 2014. 49(6): p. 1059-1066. ISI[000330713200017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330521300020">Fucoidans as Potential Inhibitors of HIV-1.</a>Prokofjeva, M.M., T.I. Imbs, N.M. Shevchenko, P.V. Spirin, S. Horn, B. Fehse, T.N. Zvyagintseva, and V.S. Prassolov. Marine Drugs, 2013. 11(8): p. 3000-3014. ISI[000330521300020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330487600007">Iminophosphoranes in Heterocyclic Synthesis: Synthesis of Pyrazolo[1,5-a]pyrimidines, Imidazo[1,2-b]pyrazole and Pyrazolo[1,5-b][1,2,4]triazine Derivatives via Intermolecular Aza-Wittig Reactions.</a>Raslan, M., M. Khalil, and S.M. Sayed. Heterocycles, 2013. 87(12): p. 2567-2576. ISI[000330487600007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330713200016">Synthesis and Antiviral Activity of C-3(C-28)-substituted 2,3-Seco-triterpenoids.</a> Tolmacheva, I.A., E.V. Igosheva, O.V. Savinova, E.I. Boreko, and V.V. Grishko. Chemistry of Natural Compounds, 2014. 49(6): p. 1050-1058. ISI[000330713200016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330796700050">Virus Inhibition Induced by Polyvalent Nanoparticles of Different Sizes.</a> Vonnemann, J., C. Sieben, C. Wolff, K. Ludwig, C. Bottcher, A. Herrmann, and R. Haag. Nanoscale, 2014. 6(4): p. 2353-2360. ISI[000330796700050].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000330785400027">A Novel Ribonuclease with Antiproliferative Activity toward Leukemia and Lymphoma Cells and HIV-1 Reverse Transcriptase Inhibitory Activity from the Mushroom, Hohenbuehelia serotina.</a> Zhang, R., L.Y. Zhao, H.X. Wang, and T.B. Ng. International Journal of Molecular Medicine, 2014. 33(1): p. 209-214. ISI[000330785400027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0228-031314.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
