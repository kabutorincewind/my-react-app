

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-11-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dy9f5XHDt6xfs8ygGuDPcjwxH6+zVdVsT8zNwrKsQTBInj21UaErOd+rbmz4NgMcOrSd+4WlHxQlAPg6IGvv/Et3EDZpop68co2xdepMevhtaEHllEmPDCMYD9s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="84021D2B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: November 8 - November 21, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24225656">The Acid-catalysed Synthesis of 7-Azaindoles from 3-Alkynyl-2-aminopyridines and Their Antimicrobial Activity</a><i>.</i> Leboho, T.C., S.F. van Vuuren, J.P. Michael, and C.B. de Koning. Organic &amp; Biomolecular Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24225656].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24221134">Anti-Candida Activity of 1-18 Fragment of the Frog Skin Peptide Esculentin-1B: In Vitro and in Vivo Studies in a Caenorhabditis elegans Infection Model</a><i>.</i> Luca, V., M. Olivi, A. Di Grazia, C. Palleschi, D. Uccelletti, and M.L. Mangoni. Cellular and Molecular Life Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[24221134].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24253293">Antifungal Activity of Plant Defensin AFP1 in Brassica juncea involves the Recognition of the Methyl Residue in Glucosylceramide of Target Pathogen Candida albicans</a><i>.</i> Oguro, Y., H. Yamazaki, M. Takagi, and H. Takaku. Current Genetics, 2013. <b>[Epub ahead of print]</b>. PMID[24253293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24196133">Antimicrobial Efficacy and Wound Healing Property of a Topical Ointment Containing Nitric oxide-loaded Zeolite</a><i>.</i> Neidrauer, M., U.K. Ercan, A. Bhattacharya, J. Samuels, J. Sedlak, R. Trikha, K.A. Barbee, M.S. Weingarten, and S.G. Joshi. Journal of Medical Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[24196133].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24195531">C. albicans Growth, Transition, Biofilm Formation, and Gene Expression Modulation by Antimicrobial Decapeptide KSL-W</a><i>.</i> Theberge, S., A. Semlali, A. Alamri, K.P. Leung, and M. Rouabhia. BMC Microbiology, 2013. 13(1): p. 246. PMID[24195531].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24215268">Canine Antimicrobial Peptides are Effective against Resistant Bacteria and Yeasts</a><i>.</i> Santoro, D. and C.W. Maddox. Veterinary Dermatology, 2013. <b>[Epub ahead of print]</b>. PMID[24215268].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24219354">Effect of Antimicrobial Agents Incorporated into Resilient Denture Relines on the C. albicans Biofilm</a><i>.</i> Bueno, M.G., V.M. Urban, G.S. Barberio, W.J. Silva, V.C. Porto, L. de Rezende Pinto, and K.H. Neppelenbroek. Oral Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[24219354].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24222512">Synthesis and Antimicrobial Activity of alpha-Aminoboronic-containing Peptidomimetics</a><i>.</i> Gozhina, O.V., J.S. Svendsen, and T. Lejon. Journal of Peptide Science, 2013. <b>[Epub ahead of print]</b>. PMID[24222512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br />  

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211430">Synthesis and Characterization of Some Novel Antimicrobial Thiosemicarbazone O-carboxymethyl Chitosan Derivatives</a><i>.</i> Mohamed, N.A., R.R. Mohamed, and R.S. Seoudi. International Journal of Biological Macromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[24211430].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24218046">Synthesis, Characterization, and Antimicrobial Activity of Silver(I) and Copper(II) Complexes of Phosphate Derivatives of Pyridine and Benzimidazole</a><i>.</i> Kalinowska-Lis, U., E.M. Szewczyk, L. Checinska, J.M. Wojciechowski, W.M. Wolf, and J. Ochocki. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[24218046].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24217700">An in Vitro Study of Sequential Fluconazole/Caspofungin Treatment against Candida albicans Biofilms</a><i>.</i> Sarkar, S., P. Uppuluri, C.G. Pierce, and J.L. Lopez-Ribot. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24217700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24217328">3D-QSAR and Cell Wall Permeability of Antitubercular Nitroimidazoles against Mycobacterium tuberculosis.</a> Lee, S.H., M. Choi, P. Kim, and P.K. Myung. Molecules, 2013. 18(11): p. 13870-13885. PMID[24217328].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24228904">Antimicrobial Polycarbonates: Investigating the Impact of Balancing Charge and Hydrophobicity Using a Same-centered Polymer Approach.</a> Engler, A.C., J.P. Tan, Z.Y. Ong, D.J. Coady, V.W. Ng, Y.Y. Yang, and J.L. Hedrick. Biomacromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[24228904].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24215368">Azaindoles: Non-covalent DprE1 Inhibitors from Scaffold Morphing Efforts Kill Mycobacterium tuberculosis and are Efficacious in Vivo.</a> Shirude, P.S., R. Shandil, C. Sadler, M. Naik, V. Hosagrahara, S. Hameed, V. Shinde, C. Bathula, V. Humnabadkar, N. Kumar, J. Reddy, V. Panduga, S. Sharma, A. Ambady, N. Hegde, J. Whiteaker, R.E. McLaughlin, H. Gardner, P. Madhavapeddi, V. Ramachandran, P. Kaur, A. Narayan, S. Guptha, D. Awasthy, C. Narayan, J. Mahadevaswamy, V. Kg, V. Ahuja, A. Srivastava, P. Kr, S. Bharath, R. Kale, M. Ramaiah, N.R. Choudhury, V. Sambandamurthy, S.M. Solapure, P.S. Iyer, S. Narayanan, and M. Chatterji. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24215368].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24240974">Binding of Pyrazole-based Inhibitors to Mycobacterium tuberculosis Pantothenate Synthetase: Docking and MM-GB(PB)SA Analysis</a><i>.</i> Ntie-Kang, F., S. Kannan, K. Wichapong, L.C. Owono Owono, W. Sippl, and E. Megnassan. Molecular BioSystems, 2013. <b>[Epub ahead of print]</b>. PMID[24240974].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245764">DprE1 - from the Discovery to the Promising Tuberculosis Drug Target.</a> Mikusova, K., V. Makarov, and J. Neres. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245764].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211081">Effect of Stereochemistry, Chain Length and Sequence Pattern on Antimicrobial Properties of Short Synthetic beta-Sheet Forming Peptide Amphiphiles.</a> Ong, Z.Y., J. Cheng, Y. Huang, K. Xu, Z. Ji, W. Fan, and Y.Y. Yang. Biomaterials, 2013. <b>[Epub ahead of print]</b>. PMID[24211081].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24244263">A High-throughput Screen against Pantothenate Synthetase (PanC) Identifies 3-Biphenyl-4-cyanopyrrole-2-carboxylic acids as a New Class of Inhibitor with Activity against Mycobacterium tuberculosis.</a> Kumar, A., A. Casey, J. Odingo, E.A. Kesicki, G. Abrahams, M. Vieth, T. Masquelin, V. Mizrahi, P.A. Hipskind, D.R. Sherman, and T. Parish. Plos One, 2013. 8(11): p. e72786. PMID[24244263].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24243718">Identification of the Target Protein of Agelasine D, a Marine Sponge Diterpene Alkaloid, as an Anti-Dormant Mycobacterial Substance.</a> Arai, M., Y. Yamano, A. Setiawan, and M. Kobayashi. ChemBioChem: A European Journal of Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24243718].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24158720">Inhibition Studies on Mycobacterium tuberculosis N-Acetylglucosamine-1-phosphate uridyltransferase (GlmU).</a> Tran, A.T., D. Wen, N.P. West, E.N. Baker, W.J. Britton, and R.J. Payne. Organic &amp; Biomolecular Chemistry, 2013. 11(46): p. 8113-8126. PMID[24158720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245756">New Approaches to Target the Mycolic Acid Biosynthesis Pathway for the Development of Tuberculosis Therapeutics.</a> North, E.J., M. Jackson, and R.E. Lee. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245756].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245765">New Approaches to Tuberculosis - Novel Drugs Based on Drug Targets Related to Toll-like Receptors in Macrophages.</a> Tomioka, H. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245765].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245758">New Tuberculostatic Agents Targeting Nucleic acid Biosynthesis: Drug Design Using QSAR Approaches.</a> Bueno, R.V., R.C. Braga, N.D. Segretti, E.I. Ferreir, G.H. Trossini, and C.H. Andrade. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245758].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24195491">Plakinamine M, a Steroidal Alkaloid from the Marine Sponge Corticium sp.</a> Lu, Z., M. Koch, M.K. Harper, T.K. Matainaho, L.R. Barrows, R.M. Van Wagoner, and C.M. Ireland. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[24195491].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24218370">Plants: A Source for New Antimycobacterial Drugs.</a> Santhosh, R.S. and B. Suriyanarayanan. Planta Medica, 2013. <b>[Epub ahead of print]</b>. PMID[24218370].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24243647">Potent Anti-mycobacterial Activity of the Pyridoxal isonicotinoyl hydrazone Analogue, 2-Pyridylcarboxaldehyde isonicotinoyl hydrazone: A Lipophilic Transport Vehicle for Isonicotinic acid hydrazide.</a> Ellis, S., D.S. Kalinowski, L. Leotta, M.L. Huang, P. Jelfs, V. Sintchenko, D.R. Richardson, and J.A. Triccas. Molecular Pharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[24243647].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245762">QSAR Based Design of New Antitubercular Compounds: Improved Isoniazi Derivatives against Multidrug-resistant TB.</a> Martins, F., C. Ventura, S. Santos, and M. Viveiros. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245762].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24095091">Rational Drug Design Based Synthesis of Novel Arylquinolines as Anti-tuberculosis Agents.</a> Jain, P.P., M.S. Degani, A. Raju, M. Ray, and M.G. Rajan. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6097-6105. PMID[24095091].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24220110">Structure and Activity of Lobophorins from a Turrid Mollusk-associated Streptomyces sp.</a> Lin, Z., M. Koch, C.D. Pond, G. Mabeza, R.A. Seronay, G.P. Concepcion, L.R. Barrows, B.M. Olivera, and E.W. Schmidt. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[24220110].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24236726">Structure-and-mechanism-based Design and Discovery of Type II Mycobacterium tuberculosis Dehydroquinate dehydratase Inhibitors.</a> Yao, Y. and L. Ze-Sheng. Current Topics in Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24236726].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24222512">Synthesis and Antimicrobial Activity of alpha-Aminoboronic-containing Peptidomimetics.</a> Gozhina, O.V., J.S. Svendsen, and T. Lejon. Journal of Peptide Science, 2013. <b>[Epub ahead of print]</b>. PMID[24222512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24103427">Synthesis and Antimycobacterial Activity of Isoniazid Derivatives from Renewable Fatty acids.</a> Rodrigues, M.O., J.B. Cantos, C.R. D&#39;Oca, K.L. Soares, T.S. Coelho, L.A. Piovesan, D. Russowsky, P.A. da Silva, and M.G. D&#39;Oca. Bioorganic &amp; Medicinal Chemistry, 2013. 21(22): p. 6910-6914. PMID[24103427].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24103299">Synthesis and Structure-Activity Relationships of Phenyl-Substituted Coumarins with Anti-tubercular Activity That Target FadD32.</a> Kawate, T., N. Iwase, M. Shimizu, S.A. Stanley, S. Wellington, E. Kazyanskaya, and D.T. Hung. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6052-6059. PMID[24103299].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24245757">Targeting Type VII/ESX Secretion Systems for Development of Novel Antimycobacterial Drugs<i>.</i></a> Bottai, D., A. Serafini, A. Cascioferro, R. Brosch, and R. Manganelli. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>. PMID[24245757].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24138557">Two Rare-class Tricyclic diterpenes with Antitubercular Activity from the Caribbean Sponge Svenzea flava. Application of Vibrational Circular Dichroism Spectroscopy for Determining Absolute Configuration<i>.</i></a> Aviles, E., A.D. Rodriguez, and J. Vicente. The Journal of Organic Chemistry, 2013. 78(22): p. 11294-11301. PMID[24138557].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24084158">Antimalarial Activity of Compounds Comprising a Primary Benzene Sulfonamide Fragment</a><i>.</i> Andrews, K.T., G.M. Fisher, S.D. Sumanadasa, T. Skinner-Adams, J. Moeker, M. Lopez, and S.A. Poulsen.  Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6114-6117. PMID[24084158].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24237770">Fast in Vitro Methods to Determine the Speed of Action and the Stage-specificity of Anti-malarials in Plasmodium falciparum</a><i>.</i> Le Manach, C., C. Scheurer, S. Sax, S. Schleiferbock, D. Gonzalez Cabrera, Y. Younis, T. Paquet, L. Street, P. Smith, X. Ding, D. Waterson, M.J. Witty, D. Leroy, K. Chibale, and S. Wittlin. Malaria Journal, 2013. 12(1): p. 424. PMID[24237770].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24206914">Identification of Inhibitors of Plasmodium falciparum Gametocyte Development</a><i>.</i> Duffy, S. and V.M. Avery. Malaria Journal, 2013. 12(1): p. 408. PMID[24206914].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24231338">In Vitro and in Silico Antimalarial Activity of 2-(2-Hydrazinyl)thiazole Derivatives</a><i>.</i> Makam, P., P.K. Thakur, and T. Kannan. European Journal of Pharmaceutical Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[24231338].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24255594">In Vitro and in Vivo Characterization of the Antimalarial Lead Compound SSJ-183 in Plasmodium Models</a><i>.</i> Schleiferbock, S., C. Scheurer, M. Ihara, I. Itoh, I. Bathurst, J.N. Burrows, P. Fantauzzi, J. Lotharius, S.A. Charman, J. Morizzi, D.M. Shackleford, K.L. White, R. Brun, and S. Wittlin. Drug Design, Development and Therapy, 2013. 7: p. 1377-1384. PMID[24255594].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1108-112113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325780000017">Antimicrobial Effect of Para-alkoxyphenylcarbamic acid esters Containing Substituted N-Phenylpiperazine Moiety.</a> Malik, I., M. Bukovsky, F. Andriamainty, and J. Galisinova. Brazilian Journal of Microbiology, 2013. 44(2): p. 457-463. ISI[000325780000017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326046200008">Antimicrobial Ergosteroids and Pyrrole Derivatives from Halotolerant Aspergillus flocculosus PT05-1 Cultured in a Hypersaline Medium.</a> Zheng, J.K., Y. Wang, J.F. Wang, P.P. Liu, J. Li, and W.M. Zhu. Extremophiles, 2013. 17(6): p. 963-971. ISI[000326046200008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326011500007">Assay Development for Identifying Inhibitors of the Mycobacterial FadD32 Activity.</a> Galandrin, S., V. Guillet, R.S. Rane, M. Leger, N. Radha, N. Eynard, K. Das, T.S. Balganesh, L. Mourey, M. Daffe, and H. Marrakchi. Journal of Biomolecular Screening, 2013. 18(5): p. 576-587. ISI[000326011500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325747400001">Characterization, Antifungal Activity, and Cell Immobilization of a Chitinase from Serratia marcescens MO-1.</a> Okay, S., M. Ozdal, and E.B. Kurbanoglu. Turkish Journal of Biology, 2013. 37(6): p. 639-644. ISI[000325747400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325096400006">Chemical Composition and Antimicrobial Activities of Essential Oil from Artemisia integrifolia.</a> Zhu, L., Y.J. Tian, and Y.C. Yin. Asian Journal of Chemistry, 2013. 25(14): p. 7679-7682. ISI[000325096400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325882000001">Essential Oil of Common Sage (Salvia officinalis L.) from Jordan: Assessment of Safety in Mammalian Cells and Its Antifungal and Anti-inflammatory Potential.</a> Abu-Darwish, M.S., C. Cabral, I.V. Ferreira, M.J. Goncalves, C. Cavaleiro, M.T. Cruz, T.H. Al-bdour, and L. Salgueiro. BioMed Research International, 2013. 538940: pp. 9. ISI[000325882000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325780000023">In Vitro Activity of Disinfectants against Aspergillus spp.</a> Mattei, A.S., I.M. Madrid, R. Santin, L.F.D. Schuch, and M.C.A. Meireles. Brazilian Journal of Microbiology, 2013. 44(2): p. 481-484. ISI[000325780000023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325236600009">An in Vitro Study on the Anti-adherence Effect of Brucea javanica and Piper Betle Extracts towards Oral Candida.</a> Nordin, M.A.F., W. Harun, and F.A. Razak. Archives of Oral Biology, 2013. 58(10): p. 1335-1342. ISI[000325236600009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325760300015">Inhibitory Effect of Oxygenated Cholestan-3[beta]-ol Derivatives on the Growth of Mycobacterium tuberculosis.</a> Schmidt, A.W., T.A. Choi, G. Theumer, S.G. Franzblau, and H.J. Knolker. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6111-6113. ISI[000325760300015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325236600011">Interactions between Hyaluronic acid, Lysozyme, and the Glucose Oxidase-mediated Lactoperoxidase System in Enzymatic and Candidacidal Activities.</a> Cho, M.A., Y.Y. Kim, J.Y. Chang, and H.S. Kho. Archives of Oral Biology, 2013. 58(10): p. 1349-1356. ISI[000325236600011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325825700013">Microwave-assisted Preparation and Antimicrobial Activity of O-Alkylamino benzofurancarboxylates.</a> Ostrowska, K., E. Hejchman, I. Wolska, H. Kruszewska, and D. Maciejewska. Monatshefte fur Chemie, 2013. 144(11): p. 1679-1689. ISI[000325825700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325774400008">Radiation Synthesis, Characterisation and Antimicrobial Application of Novel Copolymeric Silver/Poly(2-Hydroxyethyl methacrylate/itaconic acid) Nanocomposite Hydrogels.</a> Micic, M., T.V. Milic, M. Mitric, B. Jokic, and E. Suljovrujic. Suljovrujic. Polymer Bulletin, 2013. 70(12): p. 3347-3357. ISI[000325774400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325736300037">Sydowiols A-C: Mycobacterium tuberculosis Protein Tyrosine phosphatase Inhibitors from an East China Sea Marine-derived Fungus, Aspergillus sydowii.</a> Liu, X.R., F.H. Song, L. Ma, C.X. Chen, X. Xiao, B.A. Ren, X.T. Liu, H.Q. Dai, A.M. Piggott, Y. Av-Gay, L.X. Zhang, and R.J. Capon. Tetrahedron Letters, 2013. 54(45): p. 6081-6083. ISI[000325736300037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325418800008">Synthesis and Antifungal Activity of the Novel Triazole Derivatives Containing 1,2,3-Triazole Fragment.</a> Yu, S.C., N. Wang, X.Y. Chai, B.G. Wang, H. Cui, Q.J. Zhao, Y. Zou, Q.Y. Sun, Q.G. Meng, and Q.Y. Wu. Archives of Pharmacal Research, 2013. 36(10): p. 1215-1222. ISI[000325418800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325827000047">Synthesis and Antimicrobial Evaluation of Novel Substituted Pyrimidine Scaffold.</a> Ghodasara, H.B., A.R. Trivedi, V.B. Kataria, B.G. Patel, and V.H. Shah. Medicinal Chemistry Research, 2013. 22(12): p. 6121-6128. ISI[000325827000047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325760300021">Synthesis of Azide Derivative and Discovery of Glyoxalase Pathway Inhibitor against Pathogenic Bacteria.</a> Edagwa, B., Y.R. Wang, and P. Narayanasamy. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6138-6140. ISI[000325760300021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325924800038">Synthesis of Some New Dihydropyrimidines by Iodine as a Catalyst at Ambient Temperature and Evaluation of Their Biological Activity.</a> Zalavadiya, P.D., R.M. Ghetiya, B.L. Dodiya, P.B. Vekariya, and H.S. Joshi. Journal of Heterocyclic Chemistry, 2013. 50(4): p. 973-978. ISI[000325924800038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325878800001">Synthesis, Antimicrobial, and Antioxidant Activities of N-[(5&#39;-Substituted-2&#39;-phenyl-1H-indol-3&#39;-yl)methylene]-5H-dibenzo[b,f]azepine-5-carbohydrazide Derivatives.</a> Saundane, A.R., V.T. Katkar, and A.V. Vaijinath. Journal of Chemistry, 2013. 530135: pp. 9. ISI[000325878800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">59. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325483000044">Synthesis, Characterization and in Vitro Biological Evaluation of Some Novel 1,3,5-Triazine-Schiff Base Conjugates as Potential Antimycobacterial Agents.</a> Avupati, V.R., R.P. Yejella, V.R. Parala, K.N. Killari, V.M.R. Papasani, P. Cheepurupalli, V.R. Gavalapu, and B. Boddeda. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5968-5970. ISI[000325483000044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">60. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325827000028">Synthesis, Evaluation of Antimicrobial Activity, and Molecular Modeling of Novel 2-((4-(2H-Benzo[d][1,2,3]triazol-2-yl)piperidin-1-yl)methyl)-5-substituted Phenyl-1,3,4-oxadiazoles.</a> Vankadari, S.R., D. Mandala, J. Pochampalli, P. Tigulla, A. Valeru, and R. Thampu. Medicinal Chemistry Research, 2013. 22(12): p. 5912-5919. ISI[000325827000028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">61. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325827000022">Thermal, Kinetic, Spectroscopic Studies and Anti-microbial, Anti-tuberculosis, Anti-oxidant Properties of Clioquinol and Benzo-coumarin Derivatives Mixed Complexes with Copper Ion.</a> Dholariya, H.R., K.S. Patel, J.C. Patel, A.K. Patel, and K.D. Patel. Medicinal Chemistry Research, 2013. 22(12): p. 5848-5860. ISI[000325827000022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br /> 

    <p class="plaintext">62. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325760300012">Unusual Transformation of Substituted-3-formylchromones to Pyrimidine Analogues: Synthesis and Antimicrobial Activities of 5-(O-Hydroxyaroyl)pyrimidines.</a> Raj, T., N. Singh, and M.P.S. Ishar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6093-6096. ISI[000325760300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1108-112113.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
