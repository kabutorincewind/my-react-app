

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-05-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bT7V9ErJdy5SYSqspOlfQTJzjjbJPjzWcbO+ik1eljJRwHSdVCtkltSLBca5cj04yTVW1zlnEmbMUdsvjY0wuVJmQMm55U0ZmI+j2NmZJqaaxYI+Dv7x3JU+sP0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0743B20E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">April 30- May 13, 2010</p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20441162">Action of a Pentacyclic Triterpenoid, Maslinic Acid, against Toxoplasma gondii.</a> De Pablos, L.M., G. Gonzalez, R. Rodrigues, A. Garcia Granados, A. Parra, and A. Osuna. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20441162].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20451604">Antimicrobial activity of phenolic acids against commensal, probiotic and pathogenic bacteria.</a> Cueva, C., M.V. Moreno-Arribas, P.J. Martin-Alvarez, G. Bills, M.F. Vicente, A. Basilio, C.L. Rivas, T. Requena, J.M. Rodriguez, and B. Bartolome. Res Microbiol. <b>[Epub ahead of print]</b>; PMID[20451604].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20106559">Synthesis and the selective antifungal activity of 5,6,7,8-tetrahydroimidazo[1,2-a]pyridine derivatives.</a> Ozdemir, A., G. Turan-Zitouni, Z. Asim Kaplancikli, G. Iscan, S. Khan, and F. Demirci. Eur J Med Chem. 45(5): p. 2080-4; PMID[20106559].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20362444">Structure-based rational design, synthesis and antifungal activity of oxime-containing azole derivatives.</a> Xu, Y., C. Sheng, W. Wang, X. Che, Y. Cao, G. Dong, S. Wang, H. Ji, Z. Miao, J. Yao, and W. Zhang. Bioorg Med Chem Lett. 20(9): p. 2942-5; PMID[20362444].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20332195">Wild-type MIC distributions of four fluoroquinolones active against Mycobacterium tuberculosis in relation to current critical concentrations and available pharmacokinetic and pharmacodynamic data.</a> Angeby, K.A., P. Jureen, C.G. Giske, E. Chryssanthou, E. Sturegard, M. Nordvall, A.G. Johansson, J. Werngren, G. Kahlmeter, S.E. Hoffner, and T. Schon. J Antimicrob Chemother. 65(5): p. 946-52; PMID[20332195].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20149461">Pt(II) and Ag(I) complexes with acesulfame: crystal structure and a study of their antitumoral, antimicrobial and antiviral activities.</a> Cavicchioli, M., A.C. Massabni, T.A. Heinrich, C.M. Costa-Neto, E.P. Abrao, B.A. Fonseca, E.E. Castellano, P.P. Corbi, W.R. Lustri, and C.Q. Leite. J Inorg Biochem. 104(5): p. 533-40; PMID[20149461].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19827032">Activity of Scottish plant, lichen and fungal endophyte extracts against Mycobacterium aurum and Mycobacterium tuberculosis.</a> Gordien, A.Y., A.I. Gray, K. Ingleby, S.G. Franzblau, and V. Seidel. Phytother Res. 24(5): p. 692-8; PMID[19827032].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20447452">Inhibitory properties of selected South African medicinal plants against Mycobacterium tuberculosis.</a> Green, E., S. Amidou, L.C. Obi, P.O. Bessong, and R.N. Ndip. J Ethnopharmacol. <b>[Epub ahead of print]</b>; PMID[20447452].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20443682">Synthesis and antitubercular activity of heterocycle substituted diphenyl ether derivatives.</a> Kini, S.G., A. Bhat, Z. Pan, and F.E. Dayan. J Enzyme Inhib Med Chem. <b>[Epub ahead of print]</b>; PMID[20443682].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19950052">Bioactive diterpenes from the aerial parts of Anisochilus harmandii.</a> Lekphrom, R., S. Kanokmedhakul, and K. Kanokmedhakul. Planta Med. 76(7): p. 726-8; PMID[19950052].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20200152">A slow, tight binding inhibitor of InhA, the enoyl-acyl carrier protein reductase from Mycobacterium tuberculosis.</a> Luckner, S.R., N. Liu, C.W. am Ende, P.J. Tonge, and C. Kisker. J Biol Chem. 285(19): p. 14330-7; PMID[20200152].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20456370">Inhibition of Mycobacterial Growth by Plumbagin Derivatives.</a> Mathew, R., A.K. Kruthiventi, J.V. Prasad, S.P. Kumar, G. Srinu, and D. Chatterji. Chem Biol Drug Des. <b>[Epub ahead of print]</b>; PMID[20456370].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20116900">Structure-activity relationship of new anti-tuberculosis agents derived from oxazoline and oxazole benzyl esters.</a> Moraski, G.C., M. Chang, A. Villegas-Estrada, S.G. Franzblau, U. Mollmann, and M.J. Miller. Eur J Med Chem. 45(5): p. 1703-16; PMID[20116900].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20363637">The anti-cancer, anti-inflammatory and tuberculostatic activities of a series of 6,7-substituted-5,8-quinolinequinones.</a> Mulchin, B.J., C.G. Newton, J.W. Baty, C.H. Grasso, W.J. Martin, M.C. Walton, E.M. Dangerfield, C.H. Plunkett, M.V. Berridge, J.L. Harper, M.S. Timmer, and B.L. Stocker. Bioorg Med Chem. 18(9): p. 3238-51; PMID[20363637].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20149497">Synthesis and evaluation of SQ109 analogues as potential anti-tuberculosis candidates.</a> Onajole, O.K., P. Govender, P.D. van Helden, H.G. Kruger, G.E. Maguire, I. Wiid, and T. Govender. Eur J Med Chem. 45(5): p. 2075-9; PMID[20149497].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20163897">Thiosemicarbazones, semicarbazones, dithiocarbazates and hydrazide/hydrazones: anti-Mycobacterium tuberculosis activity and cytotoxicity.</a> Pavan, F.R., S.M.P.I. da, S.R. Leite, V.M. Deflon, A.A. Batista, D.N. Sato, S.G. Franzblau, and C.Q. Leite. Eur J Med Chem. 45(5): p. 1898-905; PMID[20163897].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20149496">Synthesis of some novel 2-substituted-5-[isopropylthiazole] clubbed 1,2,4-triazole and 1,3,4-oxadiazoles as potential antimicrobial and antitubercular agents.</a> Suresh Kumar, G.V., Y. Rajendraprasad, B.P. Mallikarjuna, S.M. Chandrashekar, and C. Kistayya. Eur J Med Chem. 45(5): p. 2063-74; PMID[20149496].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20149489">Synthesis and antituberculosis activity of some N-pyridyl-N&#39;-thiazolylhydrazine derivatives.</a> Turan-Zitouni, G., Z.A. Kaplancikli, and A. Ozdemir. Eur J Med Chem. 45(5): p. 2085-8; PMID[20149489].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20401395">Conformationally-constrained indeno[2,1-c]quinolines--a new class of anti-mycobacterial agents.</a> Upadhayaya, R.S., S.V. Lahore, A.Y. Sayyed, S.S. Dixit, P.D. Shinde, and J. Chattopadhyaya. Org Biomol Chem. 8(9): p. 2180-97; PMID[20401395].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20137835">Novel quinoline and naphthalene derivatives as potent antimycobacterial agents.</a> Upadhayaya, R.S., J.K. Vandavasi, R.A. Kardile, S.V. Lahore, S.S. Dixit, H.S. Deokar, P.D. Shinde, M.P. Sarmah, and J. Chattopadhyaya. Eur J Med Chem. 45(5): p. 1854-67; PMID[20137835].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20363634">Design, synthesis, biological evaluation and computational investigation of novel inhibitors of dihydrofolate reductase of opportunistic pathogens.</a> Bag, S., N.R. Tawari, M.S. Degani, and S.F. Queener. Bioorg Med Chem. 18(9): p. 3187-97; PMID[20363634].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20435781">Killing of non-replicating Mycobacterium tuberculosis by 8-hydroxyquinoline.</a> Darby, C.M. and C.F. Nathan. J Antimicrob Chemother. <b>[Epub ahead of print]</b>; PMID[20435781].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20405928">Function-oriented synthesis of simplified caprazamycins: discovery of oxazolidine-containing uridine derivatives as antibacterial agents against drug-resistant bacteria.</a> Ii, K., S. Ichikawa, B. Al-Dabbagh, A. Bouhss, and A. Matsuda. J Med Chem. 53(9): p. 3793-813; PMID[20405928].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20423460">Novel cathelicidin-derived antimicrobial peptides from Equus asinus.</a> Lu, Z., Y. Wang, L. Zhai, Q. Che, H. Wang, S. Du, D. Wang, F. Feng, J. Liu, R. Lai, and H. Yu. FEBS J. 277(10): p. 2329-39; PMID[20423460].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0430-051310.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">25. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276866900006&amp;SID=1F7N8Pnd8kBj3mj8dna&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis and biological evaluation of biguanide and dihydrotriazine derivatives as potential inhibitors of dihydrofolate reductase of opportunistic microorganisms.</a> Bag, S., N.R. Tawari, S.F. Queener, and M.S. Degani. Journal of Enzyme Inhibition and Medicinal Chemistry. 25(3): p. 331-339; ISI[000276866900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276939500029&amp;SID=1F7N8Pnd8kBj3mj8dna&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Utiliztion of the Suzuki Coupling to Enhance the Antituberculosis Activity of Aryloxazoles.</a> Moraski, G.C., S.G. Franzblau, and M.J. Miller. Heterocycles. 80(2): p. 977-988; ISI[000276939500029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277019100002&amp;SID=1F7N8Pnd8kBj3mj8dna&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">A Facile Synthesis and Discovery of Highly Functionalized Tetrahydropyridines and Pyridines as Antimycobacterial Agents.</a> Raju, S.K., M.R. Stephen, P. Subbu, B. Debjani, Y. Perumal, and S. Dharmarajan. Chemical &amp; Pharmaceutical Bulletin. 58(5): p. 602-610; ISI[000277019100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276695200047&amp;SID=3DdPIm38g5%40LOnmn5b6&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis of some novel 2-substituted-5-[isopropylthiazole] clubbed 1,2,4-triazole and 1,3,4-oxadiazoles as potential antimicrobial and antitubercular agents.</a> Kumar, G.V.S., Y. Rajendraprasad, B.P. Mallikarjuna, S.M. Chandrashekar, and C. Kistayya. European Journal of Medicinal Chemistry. 45(5): p. 2063-2074; ISI[000276695200047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0430-051310.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000276740900012&amp;SID=3DdPIm38g5%40LOnmn5b6&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Characteristics of antiviral drugs, excluding antiretroviral agents: Update 2009 (Fluconazole, caspofungin, voriconazole in combination with amphotericin B).</a> Tuset, M., E. Lopez-Sune, C. Cervera, A. Moreno, J.M. Miro, A. Kalkanci, M. Dizbay, N. Sari, B. Yalcin, I. Fidan, D. Arman, and S. Kustimur. Enfermedades Infecciosas y Microbiologia Clinica. 5(2): p. 194-197; ISI[000276740900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0430-051310.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
