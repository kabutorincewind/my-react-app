

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-10-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d1Pv+MAedb7bAEK/4RktTZaWk7DY2kx6jCGBF3JGxy8BTB8LAtWL3zz3RiXYVUS51MuxwJCCFdaTJT/MzB7VPTtvZjHX0eSevfNna6LDJ1nwQzRnld3e1zgz1Ws=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DEF0A051" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 28 - October 11, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23031791">Inhibition of HCV by the Serpin Antithrombin III</a><i>.</i> Asmal, M., M. Seaman, W. Lin, R.T. Chung, N.L. Letvin, and R. Geiben-Lynn. Virology Journal, 2012. 9(1): p. 226; PMID[23031791].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22885176">Antiviral Activity against the Hepatitis C Virus (HCV) of 1-Indanone Thiosemicarbazones and Their Inclusion Complexes with Hydroxypropyl-beta-cyclodextrin</a><i>.</i> Glisoni, R.J., M.L. Cuestas, V.L. Mathet, J.R. Oubina, A.G. Moglioni, and A. Sosnik. European Journal of Pharmaceutical, 2012. 47(3): p. 596-603; PMID[22885176].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22734835">The Function and Evolution of the Restriction Factor Viperin in Primates Was Not Driven by Lentiviruses</a><i>.</i> Lim, E.S., L.I. Wu, H.S. Malik, and M. Emerman. Retrovirology, 2012. 9:55; PMID[22734835].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23026306">Identification and Evaluation of Anti Hepatitis C Virus Phytochemicals from Eclipta alba</a><i>.</i> Manvar, D., M. Mishra, S. Kumar, and V.N. Pandey. Journal of Ethnopharmacology, 2012.  <b>[Epub ahead of print]</b>; PMID[23026306].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22959243">HCV NS5A Replication Complex Inhibitors. Part 2: Investigation of Stilbene prolinamides</a><i>.</i> St Laurent, D.R., M. Belema, M. Gao, J. Goodrich, R. Kakarla, J.O. Knipe, J.A. Lemm, M. Liu, O.D. Lopez, V.N. Nguyen, P.T. Nower, D. O&#39;Boyle, 2nd, Y. Qiu, J.L. Romine, M.H. Serrano-Wu, J.H. Sun, L. Valera, F. Yang, X. Yang, N.A. Meanwell, and L.B. Snyder. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(19): p. 6063-6066; PMID[22959243].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22318977">Bioactivity-Guided Fractionation of Phyllanthus orbicularis and Identification of the Principal Anti HSV-2 Compounds</a><i>.</i> Alvarez, A.L., K.P. Dalton, I. Nicieza, Y. Dineiro, A. Picinelli, S. Melon, A. Roque, B. Suarez, and F. Parra. Phytotherapy Research, 2012. 26(10): p. 1513-1520; PMID[22318977]. <b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22921079">Structure Based Molecular Design, Synthesis and Biological Evaluation of alpha-Pyrone Analogs as anti-HSV Agent</a><i>.</i> Karampuri, S., P. Bag, S. Yasmin, D.K. Chouhan, C. Bal, D. Mitra, D. Chattopadhyay, and A. Sharon. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(19): p. 6261-6266; PMID[22921079].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22944333">Synthesis and Biological Evaluation of Novel Homochiral Carbocyclic Nucleosides from 1-Amino-2-indanols</a><i>.</i> Ugliarolo, E.A., D. Gagey, B. Lantano, G.Y. Moltrasio, R.H. Campos, L.V. Cavallaro, and A.G. Moglioni. Bioorganic &amp; Medicinal Chemistry, 2012. 20(19): p. 5986-5991; PMID[22944333].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22954898">Phosphoramidate Derivatives of Acyclovir: Synthesis and Antiviral Activity in HIV-1 and HSV-1 Models in Vitro</a><i>.</i> Zakirova, N.F., A.V. Shipitsyn, M.V. Jasko, M.M. Prokofjeva, V.L. Andronova, G.A. Galegov, V.S. Prassolov, and S.N. Kochetkov. Bioorganic &amp; Medicinal Chemistry, 2012. 20(19): p. 5802-5809; PMID[22954898].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22711259">17-Allylamino-17-(demethoxy)geldanamycin (17-AAG) Is a Potent and Effective Inhibitor of Human Cytomegalovirus Replication in Primary Fibroblast Cells</a><i>.</i> Evers, D.L., C.F. Chao, Z. Zhang, and E.S. Huang. Archives of Virology, 2012. 157(10): p. 1971-1974; PMID[22711259].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22683667">Anti-Cytomegalovirus Effects of Tricin Are Dependent on CXCL11</a><i>.</i> Murayama, T., Y. Li, T. Takahashi, R. Yamada, K. Matsubara, Y. Tuchida, Z. Li, and H. Sadanari. Microbes and infection, 2012. 14(12): p. 1086-1092; PMID[22683667].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0928-101112.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=3FCLKO6JNPoBp9J94A3&amp;page=1&amp;doc=1">Targeting Ribosome Assembly on the HCV RNA Using a Small RNA Molecule</a><i>.</i> Bhat, P., S.V. Gnanasundram, P. Mani, P.S. Ray, D.P. Sarkar, and S. Das. RNA Biology, 2012. 9(8): p. 1110-1119; ISI[000308590400009].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=3FCLKO6JNPoBp9J94A3&amp;page=1&amp;doc=1">Selective anti-HCV Activity of 6,7-bis-O-Arylmethyl-5,6,7-trihydroxychromone Derivatives</a><i>.</i> Yoon, H., M.K. Kim, H. Mok, and Y. Chong. Bulletin of the Korean Chemical Society, 2012. 33(8): p. 2803-2805; ISI[000308115300066].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0928-101112.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
