

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-07-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="x1PJxpIETVJYnD7Amdg3gJwQ1gbIqo6KAUsmyhBzNkvCa+WoQXjMLxOm742PDIpLGwnhaMT270yqEgZXX/wHNFxojb8PygMWG0uGZdIUpl4TurHnmj87FKrOHOs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0A37862A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">June 24 - July 7, 2011</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21644248">Two Antimicrobial and Nematicidal Peptides Derived from Sequences Encoded Picea sitchensis.</a> Liu, R., L. Mu, H. Liu, L. Wei, T. Yan, M. Chen, K. Zhang, J. Li, D. You, and R. Lai. Journal of Peptide Science, 2011. <b>[Epub ahead of print]</b>; PMID[21644248].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21712761">Synthesis and Antibacterial Activities of Novel Imidazo[2,1-b]-1,3,4-thiadiazoles.</a> Atta, K.F., O.O. Farahat, A.Z. Ahmed, and M.G. Marei, Molecules, 2011. 16(7): p. 5496-5506; PMID[21712761].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21711055">High-throughput Screening-based Identification and Structure-activity Relationship-characterization Defined (S)-2-(1-Aminoisobutyl)-1-(3-chlorobenzyl) benzimidazole as a Highly Antimycotic Agent, Non-toxic to Cell Lines.</a> Bauer, J., S. Kinast, A. Burger-Kentischer, D. Finkelmeier, G. Kleymann, W.A. Rayyan, K. Schroppel, A. Singh, G. Jung, K.H. Wiesmuller, S. Rupp, and H. Eickhoff,  Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21711055].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21707253">Terpenoid Composition and Antifungal Activity of Three Commercially Important Essential Oils Against Aspergillus flavus and Aspergillus niger.</a> Bisht, D., A. Pal, C.S. Chanotiya, D. Mishra, and K.N. Pandey, Natural Products Research, 2011. <b>[Epub ahead of print]</b>; PMID[21707253].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21531485">Design, Synthesis and Molecular Docking Studies of Novel Triazole as Antifungal Agent.</a> Chai, X., J. Zhang, Y. Cao, Y. Zou, Q. Wu, D. Zhang, Y. Jiang, and Q. Sun, European Journal of Medicinal Chemistry, 2011. 46(7): p. 3167-3176; PMID[21531485].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p class="ListParagraph"> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21555164">Total Synthesis of Racemic and (R) and (S)-4-Methoxyalkanoic acids and their Antifungal Activity.</a> Das, B., D.B. Shinde, B.S. Kanth, A. Kamle, and C.G. Kumar, European Journal of Medicinal Chemistry, 2011. 46(7): p. 3124-3129; PMID[21555164].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p class="NoSpacing">           </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20002309">Silver Enhances the In vitro Antifungal Activity of the Saponin, CAY-1.</a> De Lucca, A.J., S. Boue, T. Sien, T.E. Cleveland, and T.J. Walsh, Mycoses, 2011. 54(4): p. e1-9; PMID[20002309].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21713569">Antifungal Activity of Some Cyclooxygenase Inhibitors on Candida albicans: PGE2-dependent Mechanism.</a> de Quadros, A.U., D. Bini, P.A. Pereira, E.G. Moroni, and M.C. Monteiro, Folia Microbiologica (Praha), 2011. <b>[Epub ahead of print]</b>; PMID[21713569].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21621411">Practical Synthesis, Anticonvulsant, and Antimicrobial Activity of N-Allyl and N-Propargyl di(indolyl)indolin-2-ones.</a> Praveen, C., A. Ayyanar, and P.T. Perumal, Bioorganic Medicinal Chemistry Letters, 2011. 21(13): p. 4072-4077; PMID[21621411].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21708228">Spice Oil Cinnamaldehyde Exhibits Potent Anticandidal Activity Against Fluconazole Resistant Clinical Isolates.</a> Shreaz, S., R. Bhatia, N. Khan, N. Manzoor, S. Muralidhar, and L.A. Khan, Fitoterapia, 2011. <b>[Epub ahead of print]</b>; PMID[21708228].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21543140">Synthesis and Antimicrobial Activities of Hexahydroimidazo[1,5-a]pyridinium bromides with Varying Benzyl Substituents.</a> Turkmen, H., N. Ceyhan, N. Ulku Karabay Yavasoglu, G. Ozdemir, and B. Cetinkaya, European Journal of Medicinal Chemistry, 2011. 46(7): p. 2895-2900; PMID[21543140].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21420761">Design, Synthesis and Antifungal Activities of Novel 1,2,4-Triazole Derivatives</a>. Xu, J., Y. Cao, J. Zhang, S. Yu, Y. Zou, X. Chai, Q. Wu, D. Zhang, Y. Jiang, and Q. Sun, European Journal of Medicinal Chemistry, 2011. 46(7): p. 3142-3148; PMID[21420761].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576450">In Vitro Interaction between Fluconazole and Triclosan against Clinical Isolates of Fluconazole-Resistant Candida albicans Determined by Different Methods.</a> Yu, L., G. Ling, X. Deng, J. Jin, Q. Jin, and N. Guo, Antimicrobial Agents and Chemotherapy, 2011. 55(7): p. 3609-3612; PMID[21576450].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20337938">Evaluation of anti-Candida Potential of Geranium Oil Constituents against Clinical Isolates of Candida albicans Differentially Sensitive to Fluconazole: Inhibition of Growth, Dimorphism and Sensitization.</a> Zore, G.B., A.D. Thakre, V. Rathod, and S.M. Karuppayil, Mycoses, 2011. 54(4): p. e99-e109; PMID[20337938].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>  

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21699150">Facile Diversity-Oriented Synthesis and Antitubercular Evaluation of Novel Aryl and Heteroaryl Tethered Pyridines and Dihydro-6H-quinolin-5-ones Derived via Variants of the Bohlmann-Rahtz Reaction.</a> Kantevari, S., S.R. Patpi, D. Addla, S.R. Putapatri, B. Sridhar, P. Yogeeswari, and D. Sriram, ACS Combinatorial Science, 2011. <b>[Epub ahead of print]</b>; PMID[21699150].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723649">Synthesis and Antitubercular Evaluation of Novel Dibenzo[b,d]furan and 9-Methyl-9H-carbazole Derived Hexahydro-2H-pyrano[3,2-c]quinolines via Povarov Reaction.</a> Kantevari, S., T. Yempala, G. Surineni, B. Sridhar, P. Yogeeswari, and D. Sriram, European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21723649].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21459133">Ethyl p-Methoxycinnamate Isolated from a Traditional anti-Tuberculosis Medicinal Herb Inhibits Drug Resistant Strains of Mycobacterium tuberculosis In vitro.</a> Lakshmanan, D., J. Werngren, L. Jose, K.P. Suja, M.S. Nair, R.L. Varma, S. Mundayoor, S. Hoffner, and R.A. Kumar, Fitoterapia, 2011. 82(5): p. 757-761; PMID[21459133].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.  OI_0624-070711.</p>

    <p class="ListParagraph"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21696174">Structure-guided Lead Optimization of Triazolopyrimidine-ring Substituents Identifies Potent Plasmodium falciparum Dihydroorotate dehydrogenase Inhibitors with Clinical Candidate Potential.</a> Coteron, J.M., M. Marco, J. Esquivias, X. Deng, K.L. White, J. White, M. Koltun, F. El Mazouni, S. Kokkonda, K. Katneni, R. Bhamidipati, D. Shackleford, I. Barturen, S. Ferrer, M.B. Jimenez-Diaz, F.J. Gamo, E.J. Goldsmith, B. Charman, I. Bathurst, D. Floyd, D. Matthews, J.N. Burrows, P.K. Rathod, S.A. Charman, and M.A. Phillips, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21696174].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21565148">Synthesis, Characterization and Antimalarial Activity of New Chromium Arene-quinoline Half Sandwich Complexes.</a> Glans, L., D. Taylor, C. de Kock, P.J. Smith, M. Haukka, J.R. Moss, and E. Nordlander, J Inorganic Biochemistry, 2011. 105(7): p. 985-990; PMID[21565148].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21705805">PG12, a Phospholipid Analog with Potent Antimalarial Activity Inhibits Plasmodium falciparum CTP:Phosphocholine cytidylyltransferase Activity.</a> Gonzalez-Bulnes, P., A.M. Bobenchik, Y. Augagneur, R. Cerdan, H. Vial, A. Llebaria, and C. Ben Mamoun, Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21705805].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21530018">Synthesis and In vitro Antimalarial Activity of Tetraoxane-amine/amide Conjugates.</a> Kumar, N., S.I. Khan, H. Atheaya, R. Mamgain, and D.S. Rawat, European Journal of Medicinal Chemistry, 2011. 46(7): p. 2816-2827; PMID[21530018].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21641879">Isolation and Identification of Antiplasmodial N-Alkylamides from Spilanthes acmella Flowers using Centrifugal Partition Chromatography and ESI-IT-TOF-MS.</a> Mbeunkui, F., M.H. Grace, C. Lategan, P.J. Smith, I. Raskin, and M.A. Lila, Journal of chromatography. B, Analytical technologies in the biomedical and life sciences, 2011. 879(21): p. 1886-1892; PMID[21641879].</p>

    <p class="NoSpacing">[Pubmed]. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21464242">In vivo and In vitro Antimalarial Properties of Azithromycin-chloroquine Combinations that Include the Resistance Reversal Agent Amlodipine.</a> Pereira, M.R., P.P. Henrich, A.B. Sidhu, D. Johnson, J. Hardink, J. Van Deusen, J. Lin, K. Gore, C. O&#39;Brien, M. Wele, A. Djimde, R. Chandra, and D.A. Fidock, Antimicrobial Agents and Chemotherapy, 2011. 55(7): p. 3115-3124; PMID[21464242].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21188600">Seaweeds as a Source of Lead Compounds for the Development of New Antiplasmodial Drugs from South East Coast of India.</a> Ravikumar, S., S. Jacob Inbaneson, and P. Suganthi, Parasitololgy Research, 2011. 109(1): p. 47-52; PMID[21188600].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21714976">Cananginones A-I, Linear Acetogenins From the Stem Bark of Cananga latifolia.</a> Wongsa, N., S. Kanokmedhakul, and K. Kanokmedhakul, Phytochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21714976].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p class="memofmt2-2">ANTILEISHMANIA COMPOUNDS</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21243506">Anti-Leishmanial and anti-Trypanosomal Potential of Polygodial Isolated from Stem Barks of Drimys brasiliensis Miers (Winteraceae).</a> Correa, D.S., A.G. Tempone, J.Q. Reimao, N.N. Taniwaki, P. Romoff, O.A. Favero, P. Sartorelli, M.C. Mecchi, and J.H. Lago, Parasitology Research, 2011. 109(1): p. 231-236; PMID[21243506].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21555166">Synthesis and Evaluation of Monoamidoxime derivatives: Toward New Antileishmanial Compounds.</a> Paloque, L., A. Bouhlel, C. Curti, A. Dumetre, P. Verhaeghe, N. Azas, and P. Vanelle, European Journal of Medicinal Chemistry, 2011. 46(7): p. 2984-2991; PMID[21555166].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21457471">Structure-activity Relationship Investigations of Leishmanicidal N-Benzylcytisine Derivatives.</a> Turabekova, M.A., V.I. Vinogradova, K.A. Werbovetz, J. Capers, B.F. Rasulev, M.G. Levkovich, S.B. Rakhimov, and N.D. Abdullaev, Chemical Biology and Drug Design, 2011. 78(1): p. 183-189; PMID[21457471].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0624-070711.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph"> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291870700008">Synthesis and Antimicrobial Activity of Methoxy azachalcones and N-Alkyl Substituted Methoxy azachalconium bromides.</a> Albay, C., N. Kahriman, N.Y. Iskender, S.A. Karaoglu, and N. Yayli, Turkish Journal of Chemistry, 2011. 35(3): p. 441-454; ISI[000291870700008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291237600020">Antifungal, Acetylcholinesterase Inhibition, Antioxidant and Phytochemical Properties of Three Barleria Species.</a> Amoo, S.O., A.R. Ndhlala, J.F. Finnie, and J. Van Staden, South African Journal of Botany, 2011. 77(2): p. 435-445; ISI[000291237600020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291640700002">Antifungal Activities of the Essential Oil of Five Species of Juniperus from Argentina.</a> Dambolena, J.S., J.M. Meriles, A.G. Lopez, M.N. Gallucci, S.B. Gonzalez, P.E. Guerra, A. Bruno, and M.P. Zunino, Boletin Latinoamericano Y Del Caribe De Plantas Medicinales Y Aromaticas, 2011. 10(2); ISI[000291640700002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291832500022">Synthesis, Spectral, Thermal and Magnetic Studies of Mn(II), Ni(II) and Cu(II) Complexes with some Benzopyran-4-one Schiff Bases.</a> El-Ansary, A.L., H.M. Abdel-Fattah, and N.S. Abdel-Kader, Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2011. 79(3): p. 522-528; ISI[000291832500022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291823500019">A New Approach for the Synthesis of Bioactive Heteroaryl Thiazolidine-2,4-diones.</a> Ibrahim, M.A., M.A.M. Abdel-Hamed, and N.M. El-Gohary, Journal of the Brazillian Chemical Society, 2011. 22(6): p. 1130-U184; ISI[000291823500019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291610600006">Expanding the Pleuromutilin Class of Antibiotics by de Novo Chemical Synthesis.</a> Lotesta, S.D., J.J. Liu, E.V. Yates, I. Krieger, J.C. Sacchettini, J.S. Freundlich, and E.J. Sorensen. Chemical Science, 2011. 2(7): p. 1258-1261; ISI[000291610600006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291487200015">Effects of Tropical Citrus Essential Oils on Growth, Aflatoxin Production, and Ultrastructure Alterations of Aspergillus flavus and Aspergillus parasiticus.</a> Rammanee, K. and T. Hongpattarakere, Food and Bioprocess Technology, 2011. 4(6): p. 1050-1059; ISI[000291487200015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000291614000024">Identification, Isolation and Optimization of Antifungal Metabolites from the Steptomyces Malachitofuscus ctf9.</a> Sajid, I., K.A. Shaaban, and S. Hasnain, Brazilian Journal of Microbiology, 2011. 42(2): p. 592-604; ISI[000291614000024].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0624-070711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
