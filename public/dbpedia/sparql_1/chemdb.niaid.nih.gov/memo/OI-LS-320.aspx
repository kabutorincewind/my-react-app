

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-320.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KWQN1k3L22dVqSIZ6BEDdWt1du0iHZr75Z3iAnmyzAQKg4+gQyos02sXBLT2O8j/MJp+sQxdLgOVGddXtK34ElzJl0xZeoZnSSbk5bmCsOCH2kjXHH4ftwdpvM4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="668466C6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-320-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45185   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Advances in anti-viral therapeutics</p>

    <p>          Klebl, BM</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(3): 343-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15833066&amp;dopt=abstract</a> </p><br />

    <p>2.     45186   OI-LS-320; EMBASE-OI-4/19/2005</p>

    <p class="memofmt1-2">          A Pyrosequencing assay for rapid recognition of SNPs in Mycobacterium tuberculosis embB306 region</p>

    <p>          Isola, Daniela, Pardini, Manuela, Varaine, Francis, Niemann, Stefan, Rusch-Gerdes, Sabine, Fattorini, Lanfranco, Orefici, Graziella, Meacci, Francesca, Trappetti, Claudia, and Rinaldo Oggioni, Marco</p>

    <p>          Journal of Microbiological Methods <b>2005</b>.  62(1): 113-120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T30-4FNW4FX-1/2/343db1ce71677867d319b57ec1a5c0e6">http://www.sciencedirect.com/science/article/B6T30-4FNW4FX-1/2/343db1ce71677867d319b57ec1a5c0e6</a> </p><br />

    <p>3.     45187   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Activity of Adenosine 5&#39;-Phosphonate Analogues as Chain Terminators against Hepatitis C Virus</p>

    <p>          Koh, YH, Shim, JH, Wu, JZ, Zhong, W, Hong, Z, and Girardet, JL</p>

    <p>          J Med Chem <b>2005</b>.  48(8): 2867-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828825&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828825&amp;dopt=abstract</a> </p><br />

    <p>4.     45188   OI-LS-320; EMBASE-OI-4/19/2005</p>

    <p class="memofmt1-2">          Racemic and optically active 2-methoxy-4-oxatetradecanoic acids: novel synthetic fatty acids with selective antifungal properties</p>

    <p>          Carballeira, Nestor M, O&#39;Neill, Rosann, and Parang, Keykavous</p>

    <p>          Chemistry and Physics of Lipids <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2N-4G05H57-1/2/757b77e3b585c8e7e3ba2085f49fca01">http://www.sciencedirect.com/science/article/B6T2N-4G05H57-1/2/757b77e3b585c8e7e3ba2085f49fca01</a> </p><br />

    <p>5.     45189   OI-LS-320; EMBASE-OI-4/19/2005</p>

    <p class="memofmt1-2">          Antimycobacterial activity of new 3-substituted 5-(pyridin-4-yl)-3H-1,3,4-oxadiazol-2-one and 2-thione derivatives. Preliminary molecular modeling investigations</p>

    <p>          Mamolo, Maria Grazia, Zampieri, Daniele, Vio, Luciano, Fermeglia, Maurizio, Ferrone, Marco, Pricl, Sabrina, Scialino, Giuditta, and Banfi, Elena</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FX1085-2/2/c8a39d3d6f750552d530e7f4e831d27d">http://www.sciencedirect.com/science/article/B6TF8-4FX1085-2/2/c8a39d3d6f750552d530e7f4e831d27d</a> </p><br />
    <br clear="all">

    <p>6.     45190   OI-LS-320; EMBASE-OI-4/19/2005</p>

    <p class="memofmt1-2">          Synthesis of 5-deoxy-5-phospho-d-ribonohydroxamic acid: a new competitive and selective inhibitor of type B ribose-5-phosphate isomerase from Mycobacterium tuberculosis</p>

    <p>          Burgos, Emmanuel, Roos, Annette K, Mowbray, Sherry L, and Salmon, Laurent</p>

    <p>          Tetrahedron Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4FWFVTR-D/2/a1ed9b2107d35cf76b3627658f53edb7">http://www.sciencedirect.com/science/article/B6THS-4FWFVTR-D/2/a1ed9b2107d35cf76b3627658f53edb7</a> </p><br />

    <p>7.     45191   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Rational design of dual-functional aptamers that inhibit the protease and helicase activities of HCV NS3</p>

    <p>          Umehara, T, Fukuda, K, Nishikawa, F, Kohara, M, Hasegawa, T, and Nishikawa, S</p>

    <p>          J Biochem (Tokyo) <b>2005</b>.  137(3): 339-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809335&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809335&amp;dopt=abstract</a> </p><br />

    <p>8.     45192   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Search of chemical scaffolds for novel antituberculosis agents</p>

    <p>          Garcia-Garcia, A, Galvez, J, de Julian-Ortiz, JV, Garcia-Domenech, R, Munoz, C, Guna, R, and Borras, R</p>

    <p>          J Biomol Screen <b>2005</b>.  10(3): 206-214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809316&amp;dopt=abstract</a> </p><br />

    <p>9.     45193   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity studies on a trypsin-chymotrypsin protease inhibitor obtained from potato</p>

    <p>          Kim, JY, Park, SC, Kim, MH, Lim, HT, Park, Y, and Hahm, KS</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  330(3): 921-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809084&amp;dopt=abstract</a> </p><br />

    <p>10.   45194   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          LEA3D: a computer-aided ligand design for structure-based drug design</p>

    <p>          Douguet, D, Munier-Lehmann, H, Labesse, G, and Pochet, S</p>

    <p>          J Med Chem <b>2005</b>.  48(7): 2457-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801836&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801836&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45195   OI-LS-320; EMBASE-OI-4/19/2005</p>

    <p class="memofmt1-2">          Antifungal sesquiterpene from the root of Vernonanthura tweedieana</p>

    <p>          Portillo, Aida, Vila, Roser, Freixa, Blanca, Ferro, Esteban, Parella, Teodor, Casanova, Joseph, and Canigueral, Salvador</p>

    <p>          Journal of Ethnopharmacology <b>2005</b>.  97(1): 49-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4F1JP5M-3/2/00692396c873cc13676fa77263bcef44">http://www.sciencedirect.com/science/article/B6T8D-4F1JP5M-3/2/00692396c873cc13676fa77263bcef44</a> </p><br />

    <p>12.   45196   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p class="memofmt1-2">          Activity of Hoechst 33258 against Pneumocystis carinii f. sp. muris, Candida albicans, and Candida dubliniensis</p>

    <p>          Disney, MD, Stephenson, R, Wright, TW, Haidaris, CG, Turner, DH, and Gigliotti, F</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(4): 1326-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793106&amp;dopt=abstract</a> </p><br />

    <p>13.   45197   OI-LS-320; PUBMED-OI-4/19/2005</p>

    <p><b>          Good oral absorption prediction on non-nucleoside benzothiadiazine dioxide human cytomegalovirus inhibitors using combined chromatographic and neuronal network techniques</b> </p>

    <p>          Gil, C, Dorronsoro, I, Castro, A, and Martinez, A</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(7): 1919-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15780633&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15780633&amp;dopt=abstract</a> </p><br />

    <p>14.   45198   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Screening and characterization of aptamers of hepatitis C virus NS3 helicase</p>

    <p>          Zhan, LS, Zhuo, HL, Wang, HZ, Peng, JC, and Wang, QL</p>

    <p>          PROGRESS IN BIOCHEMISTRY AND BIOPHYSICS <b>2005</b>.  32(3): 245-250, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227789000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227789000010</a> </p><br />

    <p>15.   45199   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Antitubercular constituents from the roots of Engelhardia roxburghiana</p>

    <p>          Lin, WY, Peng, CF, Tsai, IL, Chen, JJ, Cheng, MJ, and Chen, IS</p>

    <p>          PLANTA MEDICA <b>2005</b>.  71(2): 171-175, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227726500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227726500013</a> </p><br />

    <p>16.   45200   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Cytotoxic and antifungal isoprenylated xanthones and flavonoids from Cudrania fruticosa</p>

    <p>          Wang, YH, Hou, AJ, Zhu, GF, Chen, DF, and Sun, HD</p>

    <p>          PLANTA MEDICA <b>2005</b>.  71(3): 273-274, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227912500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227912500014</a> </p><br />
    <br clear="all">

    <p>17.   45201   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Enhancement of antimycobacterial activity of macrophages by stabilization of inner mitochondrial membrane potential</p>

    <p>          Gan, HX, He, XB, Duan, L, Mirabile-Levens, E, Kornfeld, H, and Remold, HG</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  191(8): 1292-1300, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227804500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227804500014</a> </p><br />

    <p>18.   45202   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          N glycolylation of the nucleotide precursors of peptidoglycan biosynthesis of Mycobacterium spp. is altered by drug treatment</p>

    <p>          Mahapatra, S, Scherman, H, Brennan, PJ, and Crick, DC</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(7): 2341-2347, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227745800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227745800015</a> </p><br />

    <p>19.   45203   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Synthesis, characterization and evaluation of antimicrobial activity of Mannich bases of some 2-[(4-carbethoxymethylthiazol-2-yl)imino]-4-thi zolidinones</p>

    <p>          Altintas, H, Ates, O, Kocabalkanli, A, Birteksoz, S, and Otuk, G</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2005</b>.  44 (3): 585-590, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227833600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227833600009</a> </p><br />

    <p>20.   45204   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          Efficient replication of a full-length hepatitis C virus genome, strain O, in cell culture, and development of a luciferase reporter system</p>

    <p>          Ikeda, M, Abe, K, Dansako, H, Nakamura, T, Naka, K, and Kato, N</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2005</b>.  329(4): 1350-1359, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227886300026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227886300026</a> </p><br />

    <p>21.   45205   OI-LS-320; WOS-OI-4/10/2005</p>

    <p class="memofmt1-2">          In vitro construction of effective M1GS ribozymes targeting HCMV UL54 RNA segments</p>

    <p>          Su, YZ, Li, HJ, Li, YQ, Chen, HJ, Tang, DS, Zhang, X, Jiang, H, and Zhou, TH</p>

    <p>          ACTA BIOCHIMICA ET BIOPHYSICA SINICA <b>2005</b>.  37(3): 210-214, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227814100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227814100010</a> </p><br />

    <p>22.   45206   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Clinical effectiveness and cost-effectiveness of antiviral combination therapy with peginterferon alfa-2B and ribavirin for chronic hepatitis C administered according to the new genotype-specific guidelines</p>

    <p>          Siebert, U, Sroczynski, G, Aidelsburger, P, Rossol, S, Waseem, J, Manns, MP, McHutchison, JG, and Wong, JB</p>

    <p>          VALUE IN HEALTH <b>2004</b>.  7(6): 759-760, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225020700391">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225020700391</a> </p><br />
    <br clear="all">

    <p>23.   45207   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          In vitro antimycotic activity of some plant extracts towards yeast and yeast-like strains</p>

    <p>          Turchetti, B, Pinelli, P, Buzzini, P, Romani, A, Heimler, D, Franconi, F, and Martini, A</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2005</b>.  19(1): 44-49, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228049900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228049900006</a> </p><br />

    <p>24.   45208   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Efficacy of micafungin against deep-seated candidiasis in cyclophosphamide-induced immunosuppressed mice</p>

    <p>          Ninomiya, M, Mikamo, H, Tanaka, K, Watanabe, K, and Tamaya, T</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2005</b>.  55(4): 587-590, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228076300034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228076300034</a> </p><br />

    <p>25.   45209   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Analysis of the 5 &#39; end structure of HCV subgenomic RNA replicated in a Huh7 cell line</p>

    <p>          Takahashi, H, Yamaji, M, Hosaka, M, Kishine, H, Hijikata, M, and Shimotohno, K</p>

    <p>          INTERVIROLOGY <b>2005</b>.  48(2-3): 104-111, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228022200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228022200005</a> </p><br />

    <p>26.   45210   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Effectiveness of spiramycin in murine models of acute and chronic toxoplasmosis</p>

    <p>          Grujic, J, Djurkovic-Djakovic, O, Nikolic, A, Klun, I, and Bobic, B</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2005</b>.  25(3): 226-230, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227997400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227997400008</a> </p><br />

    <p>27.   45211   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Second-generation azole antifungal agents</p>

    <p>          Kale, P and Johnson, LB</p>

    <p>          DRUGS OF TODAY  <b>2005</b>.  41(2): 91-105, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227933700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227933700002</a> </p><br />

    <p>28.   45212   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Computer prediction of drug resistance mutations in proteins</p>

    <p>          Cao, ZW, Han, LY, Zheng, CJ, Ji, ZL, Chen, X, Lin, HH, and Chen, YZ</p>

    <p>          DRUG DISCOVERY TODAY <b>2005</b>.  10(7): 521-529, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227936000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227936000013</a> </p><br />

    <p>29.   45213   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Development of a cell-based high-throughput specificity screen using a hepatitis C virus-bovine viral diarrhea virus dual replicon assay</p>

    <p>          O&#39;Boyle, DR, Nower, PT, Lemm, JA, Valera, L, Sun, JH, Rigat, K, Colonno, R, and Gao, M</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(4): 1346-1353, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500014</a> </p><br />
    <br clear="all">

    <p>30.   45214   OI-LS-320; WOS-OI-4/17/2005</p>

    <p class="memofmt1-2">          Different rifampin sensitivities of Escherichia coli and Mycobacterium tuberculosis RNA polymerases are not explained by the difference in the beta-subunit rifampin regions I and II</p>

    <p>          Zenkin, N, Kulbachnskiy, A, Bass, I, and Nikiforov, V</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(4): 1587-1590, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500050</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
