

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-296.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8ewbSsUGBe+2WTBP60S50KzbvFXynnYamnQNLoi5R+U6jA6WeycnxnE6+ntqEMF6T0AUgQBpVAuMpHxeAsxoHlI5yl4JUYDZliaA+rorUqCrVXpqYPeqZx65q5Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BDE0E062" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-296-MEMO</b> </p>

<p>    1.    43421   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Synthesis of new fluoroquinolones and evaluation of their in vitro activity on Toxoplasma gondii and Plasmodium spp.</p>

<p>           Anquetin, G, Rouquayrol, M, Mahmoudi, N, Santillana-Hayat, M, Gozalbes, R, Greiner, J, Farhati, K, Derouin, F, Guedj, R, and Vierling, P</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(11): 2773-2776</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4C76C06-2/2/3f495f5098ef5e012d311ad9030c3de2">http://www.sciencedirect.com/science/article/B6TF9-4C76C06-2/2/3f495f5098ef5e012d311ad9030c3de2</a> </p><br />

<p>    2.    43422   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           A Novel Class of Phosphonate Nucleosides. 9-[(1-Phosphonomethoxycyclopropyl)methyl]guanine as a Potent and Selective Anti-HBV Agent</p>

<p>           Choi, JR, Cho, DG, Roh, KY, Hwang, JT, Ahn, S, Jang, HS, Cho, WY, Kim, KW, Cho, YG, Kim, J, and Kim, YZ</p>

<p>           J Med Chem 2004. 47(11): 2864-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139764&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139764&amp;dopt=abstract</a> </p><br />

<p>    3.    43423   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           A microplate indicator-based method for determining the susceptibility of multidrug-resistant Mycobacterium tuberculosis to antimicrobial agents</p>

<p>           Morcillo, N, Di, Giulio B, Testani, B, Pontino, M, Chirico, C, and Dolmann, A</p>

<p>           Int J Tuberc Lung Dis 2004. 8(2): 253-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139456&amp;dopt=abstract</a> </p><br />

<p>    4.    43424   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Cost Effectiveness of Peginterferon alpha-2a Plus Ribavirin versus Interferon alpha-2b Plus Ribavirin as Initial Therapy for Treatment-Naive Chronic Hepatitis C</p>

<p>           Arguedas, M</p>

<p>           Pharmacoeconomics 2004. 22(7): 477-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15137884&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15137884&amp;dopt=abstract</a> </p><br />

<p>    5.    43425   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Development of a scintillation proximity assay for the Mycobacterium tuberculosis KasA and KasB enzymes involved in mycolic acid biosynthesis</p>

<p>           Schaeffer, Merrill L, Carson, Jeffrey D, Kallender, Howard, and Lonsdale, JTJohn T</p>

<p>           Tuberculosis 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WXK-4CB08V1-1/2/9538bc63a2774edb592168b04cbc553d">http://www.sciencedirect.com/science/article/B6WXK-4CB08V1-1/2/9538bc63a2774edb592168b04cbc553d</a> </p><br />

<p>    6.    43426   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           A pncA polymorphism to differentiate between Mycobacterium bovis and Mycobacterium tuberculosis</p>

<p>           Barouni, AS, Augusto, CJ, Lopes, MT, Zanini, MS, and Salas, CE</p>

<p>           Mol Cell Probes 2004. 18(3): 167-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15135450&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15135450&amp;dopt=abstract</a> </p><br />

<p>    7.    43427   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Phosphonate inhibitors of antigen 85C, a crucial enzyme involved in the biosynthesis of the Mycobacterium tuberculosis cell wall</p>

<p>           Gobec, Stanislav, Plantan, Ivan, Mravljak, Janez, Wilson, Rosalind A, Besra, Gurdyal S, and Kikelj, Danijel</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CBV260-4/2/33c37309ec021da3efd490e6c69e56c3">http://www.sciencedirect.com/science/article/B6TF9-4CBV260-4/2/33c37309ec021da3efd490e6c69e56c3</a> </p><br />

<p>    8.    43428   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           In vitro activity of protegrin-1 and beta-defensin-1, alone and in combination with isoniazid, against Mycobacterium tuberculosis</p>

<p>           Fattorini, Lanfranco, Gennaro, Renato, Zanetti, Margherita, Tan, Dejiang, Brunori, Lara, Giannoni, Federico, Pardini, Manuela, and Orefici, Graziella</p>

<p>           Peptides 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T0M-4CBW8FD-3/2/4d829492157cb3169f6c89990986c6fb">http://www.sciencedirect.com/science/article/B6T0M-4CBW8FD-3/2/4d829492157cb3169f6c89990986c6fb</a> </p><br />

<p>    9.    43429   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Molecular Modeling and Simulation of Mycobacterium tuberculosis Cell Wall Permeability</p>

<p>           Hong, X and Hopfinger, AJ</p>

<p>           Biomacromolecules 2004. 5(3): 1066-77</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15132701&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15132701&amp;dopt=abstract</a> </p><br />

<p>  10.    43430   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           New single-isomer compounds on the horizon</p>

<p>           Gal, J</p>

<p>           CNS Spectr 2002. 7(4 Suppl): 45-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15131493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15131493&amp;dopt=abstract</a> </p><br />

<p>  11.    43431   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Subcutaneous nanoparticle-based antitubercular chemotherapy in an experimental model</p>

<p>           Pandey, R and Khuller, GK</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128731&amp;dopt=abstract</a> </p><br />

<p>  12.    43432   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Report from the 11th conference on retroviruses and opportunistic infections. Microbicides</p>

<p>           Zuger, A</p>

<p>           AIDS Clin Care 2004. 16(4): 35</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124589&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124589&amp;dopt=abstract</a> </p><br />

<p>  13.    43433   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Report from the 11th conference on retroviruses and opportunistic infections. Hepatitis</p>

<p>           Albrecht, H</p>

<p>           AIDS Clin Care 2004. 16(4): 32-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124586&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124586&amp;dopt=abstract</a> </p><br />

<p>  14.    43434   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Report from the 11th conference on retroviruses and opportunistic infections. New drugs</p>

<p>           Sax, PE</p>

<p>           AIDS Clin Care 2004. 16(4): 29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124584&amp;dopt=abstract</a> </p><br />

<p>  15.    43435   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Selective anti-Toxoplasma gondii activities of azasterols</p>

<p>           Dantas-Leite, Lucas, Urbina, Julio A, de Souza, Wanderley, and Vommaro, Rossiane C</p>

<p>           International Journal of Antimicrobial Agents 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4C838FH-1/2/03c766ec0118bea3810e342b216f02b4">http://www.sciencedirect.com/science/article/B6T7H-4C838FH-1/2/03c766ec0118bea3810e342b216f02b4</a> </p><br />

<p>  16.    43436   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           A small-molecule approach to studying invasive mechanisms of Toxoplasma gondii</p>

<p>           Carey, KL, Westwood, NJ, Mitchison, TJ, and Ward, GE</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(19): 7433-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15123807&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15123807&amp;dopt=abstract</a> </p><br />

<p>  17.    43437   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Synthesis and study of antibacterial and antifungal activities of novel 1-[2-(benzoxazol-2-yl)ethoxy]- 2,6-diarylpiperidin-4-ones</p>

<p>           Ramalingan, C, Balasubramanian, S, Kabilan, S, and Vasudevan, M</p>

<p>           European Journal of Medicinal Chemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VKY-4C2NPGV-2/2/abda157383071e8d389592ab723e08d0">http://www.sciencedirect.com/science/article/B6VKY-4C2NPGV-2/2/abda157383071e8d389592ab723e08d0</a> </p><br />

<p>  18.    43438   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Proteomic approach identifies HSP27 as an interacting partner of the hepatitis C virus NS5A protein</p>

<p>           Choi, YW, Tan, YJ, Lim, SG, Hong, W, and Goh, PY</p>

<p>           Biochem Biophys Res Commun 2004. 318(2):  514-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120631&amp;dopt=abstract</a> </p><br />

<p>  19.    43439   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           New 6-nitroquinolones: synthesis and antimicrobial activities</p>

<p>           Sbardella, Gianluca, Mai, Antonello, Artico, Marino, Setzu, Maria Giovanna, Poni, Graziella, and La Colla, Paolo</p>

<p>           Il Farmaco 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJ8-4C2R61S-4/2/377755492dd33ebd730a012bfa9b0ad8">http://www.sciencedirect.com/science/article/B6VJ8-4C2R61S-4/2/377755492dd33ebd730a012bfa9b0ad8</a> </p><br />

<p>  20.    43440   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Significant accumulations of cathepsin B and prolylendopeptidase in inflammatory focus of delayed-type hypersensitivity induced by Mycobacterium tuberculosis in mice*1</p>

<p>           Kakegawa, Hisao, Matano, Yuko, Inubushi, Tomoko, and Katunuma, Nobuhiko</p>

<p>           Biochemical and Biophysical Research Communications 2004. 316(1): 78-84</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4BS0D6C-4/2/b31aac65e72ef1ec2d1e5ae0bd716993">http://www.sciencedirect.com/science/article/B6WBK-4BS0D6C-4/2/b31aac65e72ef1ec2d1e5ae0bd716993</a> </p><br />

<p>  21.    43441   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Pegasys-Copegus combination treats both HIV and hepatitis C</p>

<p>           Anon</p>

<p>           AIDS Read 2004. 14(4): 164</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15116695&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15116695&amp;dopt=abstract</a> </p><br />

<p>  22.    43442   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Peptide-based inhibitors of the hepatitis C virus NS3 protease: structure-activity relationship at the C-terminal position</p>

<p>           Rancourt, J, Cameron, DR, Gorys, V, Lamarre, D, Poirier, M, Thibeault, D, and Llinas-Brunet, M</p>

<p>           J Med Chem 2004. 47(10): 2511-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115394&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115394&amp;dopt=abstract</a> </p><br />

<p>  23.    43443   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Developing RNase P ribozymes for gene-targeting and antiviral therapy</p>

<p>           Trang, P, Kim, K, and Liu, F</p>

<p>           Cell Microbiol 2004. 6(6): 499-508</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104592&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104592&amp;dopt=abstract</a> </p><br />

<p>  24.    43444   OI-LS-296; PUBMED-OI-5/18/2004</p>

<p class="memofmt1-2">           Antiviral flavonoids from the seeds of Aesculus chinensis</p>

<p>           Wei, F, Ma, SC, Ma, LY, But, PP, Lin, RC, and Khan, IA</p>

<p>           J Nat Prod 2004. 67(4): 650-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104496&amp;dopt=abstract</a> </p><br />

<p>  25.    43445   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Efficacy of ganciclovir and cidofovir against human cytomegalovirus replication in SCID mice implanted with human retinal tissue</p>

<p>           Bidanset, Deborah J, Rybak, Rachel J, Hartline, Caroll B, and Kern, Earl R</p>

<p>           Antiviral Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4C47FW1-1/2/88769d5f00948ba0108712fb59e6d538">http://www.sciencedirect.com/science/article/B6T2H-4C47FW1-1/2/88769d5f00948ba0108712fb59e6d538</a> </p><br />

<p>  26.    43446   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           In vitro-in vivo antifungal evaluation and structure-activity relationships of 3H-1,2-dithiole-3-thione derivatives</p>

<p>           Giannini, Fernando A, Aimar, Mario L, Sortino, Maximiliano, Gomez, Roxana, Sturniollo, Alejandro, Juarez, Americo, Zacchino, Susana, de Rossi, Rita H, and Enriz, Ricardo D</p>

<p>           Il Farmaco 2004. 59(4): 245-254</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJ8-4BSVJMY-1/2/164cd7ffc637af703b28efb4e533d0b9">http://www.sciencedirect.com/science/article/B6VJ8-4BSVJMY-1/2/164cd7ffc637af703b28efb4e533d0b9</a> </p><br />

<p>  27.    43447   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Combined action of micafungin, a new echinocandin, and human phagocytes for antifungal activity against Aspergillus fumigatus</p>

<p>           Choi, Jung-Hyun, Brummer, Elmer, and Stevens, David A</p>

<p>           Microbes and Infection 2004. 6(4): 383-389</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VPN-4BRSDJP-2/2/3146bc9a0bfe2254065eebd0f0857715">http://www.sciencedirect.com/science/article/B6VPN-4BRSDJP-2/2/3146bc9a0bfe2254065eebd0f0857715</a> </p><br />

<p>  28.    43448   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           The UL97 protein kinase of human cytomegalovirus and homologues in other herpesviruses: impact on virus and host</p>

<p>           Michel, Detlef and Mertens, Thomas</p>

<p>           Biochimica et Biophysica Acta (BBA) - Proteins &amp; Proteomics 2004. 1697(1-2): 169-180</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B73DJ-4B5J89S-4/2/fb1c47c2b71b846c2e5e8ebeb4eb8308">http://www.sciencedirect.com/science/article/B73DJ-4B5J89S-4/2/fb1c47c2b71b846c2e5e8ebeb4eb8308</a> </p><br />

<p>  29.    43449   OI-LS-296; WOS-OI-5/09/2004</p>

<p class="memofmt1-2">           Lethal mutagens: broad-spectrum antivirals with limited potential for development of resistance?</p>

<p>           Freistadt, MS, Meades, GD, and Cameron, CE</p>

<p>           DRUG RESIST UPDATE 2004. 7(1): 19-24</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220829300004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220829300004</a> </p><br />

<p>  30.    43450   OI-LS-296; WOS-OI-5/09/2004</p>

<p class="memofmt1-2">           Methylation of acridin-9-ylthioureas. Structure, fluorescence and biological properties of products</p>

<p>           Bernat, J, Balentova, E, Kristian, P, Imrich, J, Sedlak, E, Danihel, I, Bohm, S, Pronayova, N, Pihlaja, K, and Klikad, KD</p>

<p>           COLLECT CZECH CHEM C 2004. 69(4): 833-849</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220854200008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220854200008</a> </p><br />

<p>  31.    43451   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           Synthesis and antiviral evaluation of benzimidazoles, quinoxalines and indoles from dehydroabietic acid</p>

<p>           Fonseca, Tatiana, Gigante, Barbara, Marques, MMatilde, Gilchrist, Thomas L, and De Clercq, Erik</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(1): 103-112</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4B4YTBX-8/2/b4d4fe8176ec5394d56c0b70fdb49254">http://www.sciencedirect.com/science/article/B6TF8-4B4YTBX-8/2/b4d4fe8176ec5394d56c0b70fdb49254</a> </p><br />

<p>  32.    43452   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           28 Inhibition of hepatitis C virus subgenomic replication by constitutive transport element (CTE)-linked ribozymes directed against the HCV internal ribosomal entry site (IRES) and the 3&#39; untranslated region (UTR)</p>

<p>           Jarczak, D, Korf, M, Manns, MP, and Krueger, M</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 10-11</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-11/2/2ceae737d9b61dfc3f06d09ef63224d1">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-11/2/2ceae737d9b61dfc3f06d09ef63224d1</a> </p><br />

<p>  33.    43453   OI-LS-296; EMBASE-OI-5/18/2004</p>

<p class="memofmt1-2">           First clinical results for a novel antiviral treatment for hepatitis C: A phase I/II dose escalation trial assessing tolerance, pharmacokinetics, and antiviral activity of NM283</p>

<p>           Godofsky, E,  Afdhal, N, Rustgi, V, Shick, L, Duncan, L, Zhou, XJ, Chao, G, Fang, C, Fielman, B, Myers, M, and Brown, NA</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 35</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-3F/2/686934c31e32e9f6b1a426c8533fb3e3">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-3F/2/686934c31e32e9f6b1a426c8533fb3e3</a> </p><br />

<p>  34.    43454   OI-LS-296; WOS-OI-5/09/2004</p>

<p class="memofmt1-2">           Biomimetic chemical catalysts in the oxidative activation of drugs</p>

<p>           Bernadou, J and Meunier, B</p>

<p>           ADV SYNTH CATAL 2004. 346(2-3): 171-184</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220763400008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220763400008</a> </p><br />

<p>  35.    43455   OI-LS-296; WOS-OI-5/16/2004</p>

<p class="memofmt1-2">           Nitric oxide inhibition after Toxoplasma gondii infection of chicken macrophage cell lines</p>

<p>           Guillermo, LVC and DaMatta, RA</p>

<p>           POULTRY SCI 2004. 83(5): 776-782</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221017300010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221017300010</a> </p><br />

<p>  36.    43456   OI-LS-296; WOS-OI-5/16/2004</p>

<p class="memofmt1-2">           Clinical features, diagnosis, and management of multiple drug-resistant tuberculosis since 2002</p>

<p>           Drobniewski, F, Balabanova, Y, and Coker, R</p>

<p>           CURR OPIN PULM MED 2004. 10(3): 211-217</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220972400010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220972400010</a> </p><br />

<p>  37.    43457   OI-LS-296; WOS-OI-5/16/2004</p>

<p class="memofmt1-2">           Use of a high-density DNA probe array for detecting mutations involved in rifampicin resistance in Mycobacterium tuberculosis</p>

<p>           Sougakoff, W, Rodrigue, M, Truffot-Pernot, C, Renard, M, Durin, N, Szpytma, M, Vachon, R, Troesch, A, and Jarlier, V</p>

<p>           CLIN MICROBIOL INFEC 2004. 10(4): 289-294</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220899300003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220899300003</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
