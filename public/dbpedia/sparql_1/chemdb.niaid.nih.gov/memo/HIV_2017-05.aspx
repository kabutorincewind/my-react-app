

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dU9z/oENMXnZozrlTXCSKdCUCuY1Uv1t+U0J62/+G6ry+g1qZ/bmydWwTagGoZfYiyLenG3FpeJKsbHy7GIUdDzAmm4GjnoNxNaJDVqaWMgjj3zTcHZSiqqJBVY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="70CA5D11" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: May, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28351588">Synthesis and anti-HIV Activities of Unsymmetrical Long Chain Dicarboxylate esters of Dinucleoside Reverse Transcriptase Inhibitors.</a> Agarwal, H.K., B.S. Chhikara, G.F. Doncel, and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(9): p. 1934-1937. PMID[28351588].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28390972">Targeting Cell Surface HIV-1 Env Protein to Suppress Infectious Virus Formation.</a> Bastian, A.R., C.G. Ang, K. Kamanna, F. Shaheen, Y.H. Huang, K. McFadden, C. Duffy, L.D. Bailey, R.V.K. Sundaram, and I. Chaiken. Virus Research, 2017. 235: p. 33-36. PMID[28390972].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28356533">A Lipopeptide HIV-1/2 Fusion Inhibitor with Highly Potent in Vitro, ex Vivo, and in Vivo Antiviral Activity.</a> Chong, H., J. Xue, S. Xiong, Z. Cong, X. Ding, Y. Zhu, Z. Liu, T. Chen, Y. Feng, L. He, Y. Guo, Q. Wei, Y. Zhou, C. Qin, and Y. He. Journal of Virology, 2017. 91(11): e00288-17. PMID[28356533].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28489061">Microbial Natural Product Alternariol 5-O-Methyl ether Inhibits HIV-1 Integration by Blocking Nuclear Import of the Pre-integration Complex.</a> Ding, J., J. Zhao, Z. Yang, L. Ma, Z. Mi, Y. Wu, J. Guo, J. Zhou, X. Li, Y. Guo, Z. Peng, T. Wei, H. Yu, L. Zhang, M. Ge, and S. Cen. Viruses, 2017. 9(5): 14pp. PMID[28489061].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28418652">Design and Development of Highly Potent HIV-1 Protease Inhibitors with a Crown-like Oxotricyclic Core as the P2-ligand to Combat Multidrug-resistant HIV Variants.</a> Ghosh, A.K., K.V. Rao, P.R. Nyalapatla, H.L. Osswald, C.D. Martyr, M. Aoki, H. Hayashi, J. Agniswamy, Y.F. Wang, H. Bulut, D. Das, I.T. Weber, and H. Mitsuya. Journal of Medicinal Chemistry, 2017. 60(10): p. 4267-4278. PMID[28418652].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28177967">Neutralizing and Targeting Properties of a New Set of &#945;4&#946;7-Specific Antibodies Are Influenced by Their Isotype.</a> Girard, A., K. Jelicic, D. Van Ryk, N. Rochereau, C. Cicala, J. Arthos, B. Noailly, C. Genin, B. Verrier, S. Laurant, D. Razanajaoana-Doll, J.J. Pin, and S. Paul. Journal of Acquired Immune Deficiency Syndrome, 2017. 75(1): p. 118-127. PMID[28177967].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28542329">Novel Agoshrna Molecules for Silencing of the CCR5 Co-receptor for HIV-1 Infection.</a> Herrera-Carrillo, E. and B. Berkhout. Plos One, 2017. 12(5): p. e0177935. PMID[28542329].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28481112">Structure-based Optimization of Thiophene[3,2-d]pyrimidine Derivatives as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors with Improved Potency against Resistance-associated Variants.</a> Kang, D., Z. Fang, B. Huang, X. Lu, H. Zhang, H. Xu, Z. Huo, Z. Zhou, Z. Yu, Q. Meng, G. Wu, X. Ding, Y. Tian, D. Daelemans, E. De Clercq, C. Pannecouque, P. Zhan, and X. Liu. Journal of Medicinal Chemistry, 2017. 60(10): p. 4424-4443. PMID[28481112].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399743200005">Oligonucleotide Inhibitors of HIV-1 Integrase Efficiently Inhibit HIV-1 Reverse Transcriptase.</a> Korolev, S.P., T.S. Zatsepin, and M.B. Gottikh. Russian Journal of Bioorganic Chemistry, 2017. 43(2): p. 135-139. ISI[000399743200005].
    <br />
    <b>[WOS]</b>. HIV_05_2017.</p>

    <p>10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28138910">Spray-dried Thiolated Chitosan-coated Sodium Alginate Multilayer Microparticles for Vaginal HIV Microbicide Delivery.</a> Meng, J., V. Agrahari, M.J. Ezoulin, S.S. Purohit, T. Zhang, A. Molteni, D. Dim, N.A. Oyler, and B.C. Youan. The AAPS Journal, 2017. 19(3): p. 692-702. PMID[28138910].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400465400151">In Vitro Antigonococcal, Anti-inflammatory and HIV-1 Reverse Transcriptase Activities of the Herbal Mixture Used for the Treatment of Sexually Transmitted Diseases.</a> Mophuting, B.C., M.J. Bapela, and T.E. Tshikalange. South African Journal of Botany, 2017. 109: p. 354-355. ISI[000400465400151].
    <br />
    <b>[WOS]</b>. HIV_05_2017.</p>

    <p>12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400465400167">Pharmaco-synergistic Potential of Moringa oleifera Lam. With Antiretroviral Therapies in Managing HIV-1 Infections.</a> Ndhlala, A.R., K. Cele, P.W. Mashela, C.P. Du Plooy, and H.A. Abdelgadir. South African Journal of Botany, 2017. 109: p. 359-359. ISI[000400465400167].
    <br />
    <b>[WOS]</b>. HIV_05_2017.</p>

    <p>13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28234686">The Second-generation Maturation Inhibitor GSK3532795 Maintains Potent Activity toward HIV Protease Inhibitor-resistant Clinical Isolates.</a> Ray, N., T. Li, Z. Lin, T. Protack, P.M. van Ham, C. Hwang, M. Krystal, M. Nijhuis, M. Lataillade, and I. Dicker. Journal of Acquired Immune Deficiency Syndrome, 2017. 75(1): p. 52-60. PMID[28234686]. PMCID[PMC5389583].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28395128">Probing Mercaptobenzamides as HIV Inactivators via Nucleocapsid Protein 7.</a> Saha, M., M.T. Scerba, N.I. Shank, T.L. Hartman, C.A. Buchholz, R.W. Buckheit, Jr., S.R. Durell, and D.H. Appella. ChemMedChem, 2017. 12(10): p. 714-721. PMID[28395128].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28285916">Discovery and Optimization of 2-Pyridinone aminal Integrase Strand Transfer Inhibitors for the Treatment of HIV.</a> Schreier, J.D., M.W. Embrey, I.T. Raheem, G. Barbe, L.C. Campeau, D. Dubost, J. McCabe Dunn, J. Grobler, T.J. Hartingh, D.J. Hazuda, D. Klein, M.D. Miller, K.P. Moore, N. Nguyen, N. Pajkovic, D.A. Powell, V. Rada, J.M. Sanders, J. Sisko, T.G. Steele, J. Wai, A. Walji, M. Xu, and P.J. Coleman. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(9): p. 2038-2046. PMID[28285916].
    <br />
    <b>[PubMed]</b>. HIV_05_2017</p>

    <p>16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399787300008.">Trimethyl chitosan improves anti-HIV Effects of Atripla as a New Nano-formulated Drug.</a> Shohani, S., M. Mondanizadeh, A. Abdoli, B. Khansarinejad, M. Salimi-Asl, M.S. Ardestani, M. Ghanbari, M.S. Haj, and R. Zabihollahi. Current HIV Research, 2017. 15(1): p. 56-65. ISI[000399787300008].
    <br />
    <b>[WOS]</b>. HIV_05_2017.</p>

    <p>17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400465400226">Metabolomic Analysis on anti-HIV Activity of Selected Helichrysum Species.</a> Yazdi, S.E., G. Prinsloo, H.M. Heyman, and J.J.M. Meyer. South African Journal of Botany, 2017. 109: p. 376-376. ISI[000400465400226].
    <br />
    <b>[WOS]</b>. HIV_05_2017.</p>

    <h2>Patent Citations</h2>

    <p>18. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170420&amp;CC=WO&amp;NR=2017064628A1&amp;KC=A1">Preparation of C-3 Novel Triterpenones with C-28 Urea Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, D.K. Gazula Levi, P.R. Adulla, and B.R. Kasireddy. Patent. 2017. 2016-IB56099 2017064628: 84pp.
    <br />
    <b>[Patent]</b>. HIV_05_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
