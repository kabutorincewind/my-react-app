

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-293.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uMyg8/X75M6FRtzvrU0GmBa8+rFOE3HuLwEErzbCEt1Eq8FVMwUpOOBXhTDtePKVyBOEIshYrEBBnltA0skGo3e6BpTtpWwDyEYTi8LruyWar48bXpKvJpXU4AE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="79A49E3D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-293-MEMO</b> </p>

<p>    1.    43118   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Preparation and antiviral properties of new acyclic, achiral nucleoside analogues: 1- or 9-[3-hydroxy-2-(hydroxymethyl)prop-1-enyl]nucleobases and 1- or 9-[2,3-dihydroxy-2-(hydroxymethyl)propyl]nucleobases</p>

<p>           Boesen, T, Madsen, C, Sejer, Pedersen D, Nielsen, BM, Petersen, AB, Petersen, MA , Munck, M, Henriksen, U, Nielsen, C, and Dahl, O</p>

<p>           Org Biomol Chem 2004. 2(8): 1245-1254</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15064804&amp;dopt=abstract</a> </p><br />

<p>    2.    43119   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Activity of the isolated HIV RNase H domain and specific inhibition by N-hydroxyimides</p>

<p>           Hang, JQ, Rajendran, S, Yang, Y, Li, Y, Wong, Kai In P, Overton, H, Parkes, KE, Cammack, N, Martin, JA, and Klumpp, K</p>

<p>           Biochem Biophys Res Commun 2004. 317(2):  321-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15063760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15063760&amp;dopt=abstract</a> </p><br />

<p>    3.    43120   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Human immunodeficiency virus type 1 inhibitory activity of Mentha longifolia</p>

<p>           Amzazi, S, Ghoulami, S, Bakri, Y, Il, Idrissi A, Fkih-Tetouani, S, and Benjouad, A</p>

<p>           Therapie 2003. 58(6): 531-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15058498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15058498&amp;dopt=abstract</a> </p><br />

<p>    4.    43121   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Inhibition of HIV-1 Reverse Transcriptase and Protease by Phlorotannins from the Brown Alga Ecklonia cava</p>

<p>           Ahn, MJ, Yoon, KD, Min, SY, Lee, JS, Kim, JH, Kim, TG, Kim, SH, Kim, NG, Huh, H, and Kim, J</p>

<p>           Biol Pharm Bull 2004. 27(4): 544-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056863&amp;dopt=abstract</a> </p><br />

<p>    5.    43122   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Small-molecule inhibitors of HIV-1 entry block receptor-induced conformational changes in the viral envelope glycoproteins</p>

<p>           Si, Z, Madani, N, Cox, JM, Chruma, JJ, Klein, JC, Schon, A, Phan, N, Wang, L, Biorn, AC, Cocklin, S, Chaiken, I, Freire, E, Smith, AB 3rd, and Sodroski, JG</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(14): 5036-5041</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15051887&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15051887&amp;dopt=abstract</a> </p><br />

<p>    6.    43123   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Relationship between antiviral activity and host toxicity: comparison of the incorporation efficiencies of 2&#39;,3&#39;-dideoxy-5-fluoro-3&#39;-thiacytidine-triphosphate analogs by human immunodeficiency virus type 1 reverse transcriptase and human mitochondrial DNA polymerase</p>

<p>           Feng, JY, Murakami, E, Zorca, SM, Johnson, AA, Johnson, KA, Schinazi, RF, Furman, PA, and Anderson, KS</p>

<p>           Antimicrob Agents Chemother 2004.  48(4): 1300-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047533&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15047533&amp;dopt=abstract</a> </p><br />

<p>    7.    43124   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           HIV-1 protease molecular dynamics of a wild-type and of the V82F/I84V mutant: possible contributions to drug resistance and a potential new target site for drugs</p>

<p>           Perryman, AL, Lin, JH, and McCammon, JA</p>

<p>           Protein Sci 2004. 13(4): 1108-23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15044738&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15044738&amp;dopt=abstract</a> </p><br />

<p>    8.    43125   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Six-week randomized controlled trial to compare the tolerabilities, pharmacokinetics, and antiviral activities of GW433908 and amprenavir in human immunodeficiency virus type 1-infected patients</p>

<p>           Wood, Robin, Arasteh, Keikawus, Stellbrink, Hans-juergen, Teofilo, Eugenio, Raffi, Francois, Pollard, Richard B, Eron, Joseph, Yeo, Jane, Millard, Judith, Wire, Mary Beth, and Naderer, Odin J</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(1): 116-123</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    9.    43126   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Nucleosides. IX. Synthesis of purine N(3),5&#39;-cyclonucleosides and N(3),5&#39;-cyclo-2&#39;,3&#39;-seconucleosides via Mitsunobu reaction as TIBO-like derivatives</p>

<p>           Chen, GS, Chen, CS, Chien, TC, Yeh, JY, Kuo, CC, Talekar, RS, and Chern, JW</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 347-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043159&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043159&amp;dopt=abstract</a> </p><br />

<p>  10.    43127   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Heterocyclic compound having hiv integrase inhibitory activity</b>((Shionogi &amp; Co., Ltd. Japan)</p>

<p>           Murai, Hitoshi, Endo, Takeshi, Kurose, Noriyuki, Taishi, Teruhiko, and Yoshida, Hiroshi</p>

<p>           PATENT: WO 2004024693 A1;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 396 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  11.    43128   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Dihydroxypyridopyrazine-1,6-dione compounds useful as HIV integrase inhibitors</b>((Merck &amp; Co., Inc. USA)</p>

<p>           Wai, John S, Kim, Boyoung, Fisher, Thorsten E, Zhuang, Linghang, Williams, Peter D, Lyle, Terry A, Langford, HMarie, and Robinson, Kyle A</p>

<p>           PATENT: WO 2004024078 A2;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 137 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  12.    43129   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           A new approach for the synthesis of novel 5-substituted isodeoxyuridine analogs</p>

<p>           Guenther, S and Nair, V</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 183-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043146&amp;dopt=abstract</a> </p><br />

<p>  13.    43130   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Nucleosides. LXV. Synthesis of new pteridine-N8-nucleosides</p>

<p>           Matysiak, S, Waldscheck, B, and Pfleiderer, W</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 51-66</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043136&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043136&amp;dopt=abstract</a> </p><br />

<p>  14.    43131   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Synthesis and antiviral activity of novel fluorinated 2&#39;,3&#39;-dideoxynucleosides</p>

<p>           Kumar, P, Ohkura, K, Balzarini, J, De Clercq, E, Seki, K, and Wiebe, LI</p>

<p>           Nucleosides Nucleotides Nucleic Acids 2004. 23(1-2): 7-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043133&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043133&amp;dopt=abstract</a> </p><br />

<p>  15.    43132   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Understanding the integrase inhibitory activity of azido containing HIV-1 integrase inhibitors</p>

<p>           Karki, Rajeshri G and Nicklaus, Marc C</p>

<p>           CONFERENCE: Abstracts of Papers, 227th ACS National Meeting, Anaheim, CA, United States, March 28-April 1, 2004 2004: MEDI-017</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43133   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Bis-Ketol Nucleoside Triesters as Prodrugs of the Antiviral Nucleoside Triphosphate Analogues of 3&#39;-Deoxythymidine and 3&#39;-Deoxy-2&#39;,3&#39;-didehydrothymidine</p>

<p>           Calvo, Kim C, Wang, Xiaohong, and Koser, Gerald F</p>

<p>           Nucleosides, Nucleotides &amp; Nucleic Acids 2004. 23(3): 637-646</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    43134   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Susceptibility of HIV-2, SIV and SHIV to various anti-HIV-1 compounds: implications for treatment and postexposure prophylaxis</p>

<p>           Witvrouw, M, Pannecouque, C, Switzer, WM, Folks, TM, De Clercq, E, and Heneine, W</p>

<p>           Antivir Ther 2004. 9(1): 57-65</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040537&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040537&amp;dopt=abstract</a> </p><br />

<p>  18.    43135   HIV-LS-293; PUBMED-HIV-4/7/2003</p>

<p class="memofmt1-2">           Antiretroviral drug resistance in non-subtype B HIV-1, HIV-2 and SIV</p>

<p>           Parkin, NT and Schapiro, JM</p>

<p>           Antivir Ther 2004. 9(1): 3-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040531&amp;dopt=abstract</a> </p><br />

<p>  19.    43136   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Synthesis and antimicrobial activity of some novel 6-bromo-2-methyl/phenyl-3-(sulphonamido) quinazolin-4(3H)-ones</p>

<p>           Selvam, P, Vanitha, K, Chandramohan, M, and De Clerco, E</p>

<p>           Indian Journal of Pharmaceutical Sciences 2004. 66(1): 82-86</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    43137   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Phosphatidylinositol 3,5-biphosphate inhibitors as antiviral agents</b>((Piramed Limited, UK)</p>

<p>           Parker, Peter and Waterfield, Michael</p>

<p>           PATENT: WO 2004017950 A2;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2003; PP: 56 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    43138   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Use of inhibitors of acid sphingomyelinase and of acid sphingomyelinase reaction products for the prophylaxis and treatment of infectious diseases</b>((Gulbins, Erich Germany)</p>

<p>           PATENT: DE 10239531 A1;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2002; PP: 10 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    43139   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Non-peptidic protease inhibitors (NPPIs): Tipranavir</p>

<p>           Mayers, Douglas, Curry, Kevin, Kohlbrenner, Veronika, and McCallister, Scott</p>

<p>           Advances in Antiviral Drug Design 2004. 4: 79-98</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43140   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Broad spectrum substituted oxindole sulfonamide HIV protease inhibitors</b>((Tibotec Pharmaceuticals Ltd., Ire.)</p>

<p>           Tahri, Abdellah and Wigerinck, Piet Tom Bert Paul</p>

<p>           PATENT: WO 2004016619 A1;  ISSUE DATE: 20040226</p>

<p>           APPLICATION: 2003; PP: 53 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    43141   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           Human immunodeficiency virus type 1 escapes from RNA interference-mediated inhibition</p>

<p>           Das, Atze T, Brummelkamp, Thijn R, Westerhout, Ellen M, Vink, Monique, Madiredjo, Mandy, Bernards, Rene, and Berkhout, Ben</p>

<p>           Journal of Virology 2004. 78(5): 2601-2605</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    43142   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Broad-spectrum 2-aminobenzothiazole sulfonamide HIV protease inhibitors</b>((Tibotec Pharmaceuticals Ltd., Ire.)</p>

<p>           Surleraux, Dominique Louis Nestor Ghislain, Wigerinck, Piet Tom Bert Paul, and Getman, Daniel P</p>

<p>           PATENT: WO 2004014371 A1;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2003; PP: 44 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    43143   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p class="memofmt1-2">           In vitro activity of stampidine against primary clinical human immunodeficiency virus isolates</p>

<p>           Uckun, Fatih M, Pendergrass, Sharon, Qazi, Sanjive, and Venkatachalam, Taracad K</p>

<p>           Arzneimittel Forschung 2004. 54(1): 69-77</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    43144   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Preparation of PEGylated T1249 polypeptide conjugates as antiviral agents</b>((F. Hoffmann-La Roche AG, Switz.)</p>

<p>           Bailon, Pascal Sebastian and Won, Chee-Youb</p>

<p>           PATENT: WO 2004013165 A1;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2003; PP: 61 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    43145   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Antiviral pyrazines and pharmaceutical compositions containing them</b>((Toyama Chemical Co., Ltd. Japan)</p>

<p>           Egawa, Hiroyuki, Sugita, Atsushi, and Furuta, Yosuke</p>

<p>           PATENT: JP 2004043371 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2002-7517; PP: 41 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    43146   HIV-LS-293; SCIFINDER-HIV-3/30/2004</p>

<p><b>           Preparation of N-(4-piperidinyl) amide compounds as chemokine receptor CCR5 modulators</b>((AstraZeneca AB, Swed.)</p>

<p>           Cumming, John and Winter, Jon</p>

<p>           PATENT: WO 2004018425 A1;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2003; PP: 43 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    43147   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Quality assurance program for clinical measurement of antiretrovirals: AIDS Clinical Trials Group proficiency testing program for pediatric and adult pharmacology laboratories</p>

<p>           Holland, DT, DiFrancesco, R, Stone, J, Hamzeh, F, Connor, JD, and Morse, GD</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(3): 824-831</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189351700018">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189351700018</a> </p><br />

<p>  31.    43148   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           3 &#39; R,4 &#39; R-Di-(O)-(-)-camphanoyl-2 &#39;,2 &#39;-dimethyl-dihydropyrano [2,3-F]-chromone (DCP): A novel class of potent anti-HIV agents.</p>

<p>           Yu, DL, Brossi, A, and Lee, KH</p>

<p>           ABSTR PAP AM CHEM S 2003. 225: U179</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918000961">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918000961</a> </p><br />

<p>  32.    43149   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Synthesis of enantiomerically pure D-FDOC and its anti-HIV activity.</p>

<p>           Mao, SL, Liotta, DC, Bouygues, M, Welch, CJ, Biba, M, and Schinazi, RF</p>

<p>           ABSTR PAP AM CHEM S 2003. 225: U231</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001244">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001244</a> </p><br />

<p>  33.    43150   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Synthesis of modified nucleoside analogues as anti-HIV agents.</p>

<p>           Reddy, VD</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U210-U211</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062400912">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062400912</a> </p><br />

<p>  34.    43151   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           How a modification (8-aza-3-deaza-2 &#39;-deoxyguanosine) influences the quadruplex structure of Hotoda&#39;s 6-mer TGGGAG with 5 &#39;- and 3 &#39;-end modifications</p>

<p>           Jaksa, S, Kralj, B, Pannecouque, C, Balzarini, J, De, Clercq E, and Kobe, J</p>

<p>           NUCLEOS NUCLEOT NUCL 2004. 23(1-2): 77-88</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100008</a> </p><br />

<p>  35.    43152   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Second generation of cycloSal-pronucleotides with esterase-cleavable sites: The &quot;lock-in&quot;-concept</p>

<p>           Meier, C, Ruppel, MFH, Vukadinovic, D, and Balzarini, J</p>

<p>           NUCLEOS NUCLEOT NUCL 2004. 23(1-2): 89-115</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100009</a> </p><br />

<p>  36.    43153   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of a novel synthetic phosphocholine lipid-AZT conjugate that double-targets wild-type and drug resistant variants of HIV</p>

<p>           Kucera, LS, Morris-Natschke, SL, Ishaq, KS, Hes, J, Iyer, N, Furman, PA, and Fleming, RA</p>

<p>           NUCLEOS NUCLEOT NUCL 2004. 23(1-2): 385-399</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100032">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220148100032</a> </p><br />

<p>  37.    43154   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           Retinoid-dependent restriction of human immunodeficiency virus type 1 replication in monocytes/macrophages</p>

<p>           Hanley, TM, Kiefer, HLB, Schnitzler, AC, Marcello, JE, and Viglianti, GA</p>

<p>           J VIROL 2004. 78(6): 2819-2830</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220043100017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220043100017</a> </p><br />

<p>  38.    43155   HIV-LS-293; WOS-HIV-3/28/2004</p>

<p class="memofmt1-2">           New 2,4-diamino-5-(2 &#39;,5 &#39;-substituted benzyl)pyrimidines as potential drugs against opportunistic infections of AIDS and other immune disorders. Synthesis and species-dependent antifolate activity</p>

<p>           Rosowsky, A, Forsch, RA, Sibley, CH, Inderlied, CB, and Queener, SF</p>

<p>           J MED CHEM 2004. 47(6): 1475-1486</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220038700018">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220038700018</a> </p><br />

<p>  39.    43156   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           Synthesis of AZT/d4T boranophosphates as anti-HIV prodrug candidates</p>

<p>           Lin, CX, Fu, H, Tu, GZ, and Zhao, YF</p>

<p>           SYNTHESIS-STUTTGART 2004(4): 509-516</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191300007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191300007</a> </p><br />

<p>  40.    43157   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           HIV protease inhibitor part 1: Use of Evans&#39; oxazolidinone in intermolecular Diels-Alder reaction en route to 3,4-substituted cyclohexanones</p>

<p>           Crackett, P, Demont, E, Eatherton, A, Frampton, CS, Gilbert, J, Kahn, I, Redshaw, S, and Watson, W</p>

<p>           SYNLETT 2004(4): 679-683</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191700021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191700021</a> </p><br />

<p>  41.    43158   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           HIV protease inhibitors part 2: [3+2] cycloaddition, isomerization; and ring expansion en route to 4,5-substituted cyclohexenones</p>

<p>           Demont, E, Eatherton, A, Frampton, CS, Kahn, I, and Redshaw, S</p>

<p>           SYNLETT 2004(4): 684-687</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191700022">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220191700022</a> </p><br />

<p>  42.    43159   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           Novel peptide-based HIV-1 immunotherapy</p>

<p>           Sommerfelt, MA, Nyhus, J, and Sorensen, B</p>

<p>           EXPERT OPIN BIOL TH 2004. 4(3): 349-361</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220193400008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220193400008</a> </p><br />

<p>  43.    43160   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           Novel phosphonate nucleosides as antiviral agents</p>

<p>           Hwang, JT and Choi, JR</p>

<p>           DRUG FUTURE 2004. 29(2): 163-177</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220229600007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220229600007</a> </p><br />

<p>  44.    43161   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           Co-receptor antagonists as HIV-1 entry inhibitors</p>

<p>           Shaheen, F and Collman, RG</p>

<p>           CURR OPIN INFECT DIS 2004. 17(1): 7-16</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220248300002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220248300002</a> </p><br />

<p>  45.    43162   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           Synthesis and in vitro evaluation of S-acyl-3-thiopropyl prodrugs of Foscarnet</p>

<p>           Gagnard, V, Leydet, A, Morere, A, Montero, JL, Lefebvre, I, Gosselin, G, Pannecouque, C, and De, Clercq E</p>

<p>           BIOORGAN MED CHEM 2004. 12(6): 1393-1402</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220237800014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220237800014</a> </p><br />

<p>  46.    43163   HIV-LS-293; WOS-HIV-4/4/2004</p>

<p class="memofmt1-2">           HIV-2 protease sequences of subtypes A and B harbor multiple mutations associated with protease inhibitor resistance in HIV-1</p>

<p>           Pieniazek, D, Rayfield, M, Hu, DJ, Nkengasong, JN, Soriano, V, Heneine, W, Zeh, C, Agwale, SM, Wambebe, C, Odama, L, Wiktor, SZ, and Kalish, ML</p>

<p>           AIDS 2004. 18(3): 495-502</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220233000016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220233000016</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
