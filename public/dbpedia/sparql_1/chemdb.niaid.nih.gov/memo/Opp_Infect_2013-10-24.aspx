

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-10-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NrkQH01gZH1xKUdy5wrFHB2n6Z0Mv1FOP8+0+uYEjrbxIrJlNIKekL34jQqxGXND11ntVtcyt/ve+rbeI2zckJikFJEtze1wIHlCa6Uj1CmCv9BxiywCGYa9J2A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7B481378" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 27 - October 24, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126380">Antimicrobial and Selected in Vitro Enzyme Inhibitory Effects of Leaf Extracts, Flavonols and Indole Alkaloids Isolated from Croton menyharthii.</a> Aderogba, M.A., A.R. Ndhlala, K.R. Rengasamy, and J. Van Staden. Molecules, 2013. 18(10): p. 12633-12644. PMID[24126380].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24084023">In Vitro Anti-candida Activity of Certain New 3-(1H-Imidazol-1-yl)propan-1-one oxime esters.</a> Attia, M.I., A.S. Zakaria, M.S. Almutairi, and S.W. Ghoneim. Molecules, 2013. 18(10): p. 12208-12221. PMID[24084023].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23711519">Antifungal Synergy of Theaflavin and Epicatechin Combinations against Candida albicans.</a> Betts, J.W., D.W. Wareham, S.J. Haswell, and S.M. Kelly. Journal of Microbiology and Biotechnology, 2013. 23(9): p. 1322-1326. PMID[23711519].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23911425">New Compounds, Chemical Composition, Antifungal Activity and Cytotoxicity of the Essential Oil from Myrtus nivellei Batt. &amp; Trab., an Endemic Species of Central Sahara.</a> Bouzabata, A., O. Bazzali, C. Cabral, M.J. Goncalves, M.T. Cruz, A. Bighelli, C. Cavaleiro, J. Casanova, L. Salgueiro, and F. Tomi. Journal of Ethnopharmacology, 2013. 149(3): p. 613-620. PMID[23911425].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145540">Anidulafungin vs Caspofungin in a Mouse Model of Candidiasis Caused by Anidulafungin-susceptible C. parapsilosis Isolates with Different Degrees of Caspofungin Susceptibility.</a> Dimopoulou, D., G. Hamilos, M. Tzardi, R.E. Lewis, G. Samonis, and D.P. Kontoyiannis. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24145540].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856768">Amphotericin B- and Voriconazole-Echinocandin Combinations against Aspergillus spp.: Effect of Serum on Inhibitory and Fungicidal Interactions.</a> Elefanti, A., J.W. Mouton, P.E. Verweij, A. Tsakris, L. Zerva, and J. Meletiadis. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4656-4663. PMID[23856768].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23973821">Synthesis and Antimicrobial Activity of Novel 5-(1-Adamantyl)-2-aminomethyl-4-substituted-1,2,4-triazoline-3-thiones.</a> El-Emam, A.A., A.M. Al-Tamimi, M.A. Al-Omar, K.A. Alrashood, and E.E. Habib. European Journal of Medicinal Chemistry, 2013. 68: p. 96-102. PMID[23973821].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23877676">Candida tropicalis Antifungal Cross-resistance Is Related to Different Azole Target (ERG11P) Modifications.</a> Forastiero, A., A.C. Mesa-Arango, A. Alastruey-Izquierdo, L. Alcazar-Fuoli, L. Bernal-Martinez, T. Pelaez, J.F. Lopez, J.O. Grimalt, A. Gomez-Lopez, I. Cuesta, O. Zaragoza, and E. Mellado. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4769-4781. PMID[23877676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145546">Novel Antifungal Drug Discovery Based on Targeting Pathways Regulating the Fungal-conserved UPC2 Transcription Factor.</a> Gallo-Ebert, C., M. Donigan, I.L. Stroke, R.N. Swanson, M.T. Manners, J. Francisco, G. Toner, D. Gallagher, C.Y. Huang, S.E. Gygax, M. Webb, and J.T. Nickels, Jr. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24145546].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23816874">Antifungal Property of Hibicuslide C and Its Membrane-active Mechanism in Candida albicans.</a> Hwang, J.H., Q. Jin, E.R. Woo, and D.G. Lee. Biochimie, 2013. 95(10): p. 1917-1922. PMID[23816874].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24078467">Anti-Candida albicans Biofilm Effect of Novel Heterocyclic Compounds.</a> Kagan, S., A. Jabbour, E. Sionov, A.A. Alquntar, D. Steinberg, M. Srebnik, R. Nir-Paz, A. Weiss, and I. Polacheck. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24078467].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24077721">Effects of Bovine Lactoferrin to Oral Candida albicans and Candida glabrata Isolates Recovered from the Saliva in Elderly People.</a> Komatsu, A., T. Satoh, H. Wakabayashi, and F. Ikeda. Odontology, 2013. <b>[Epub ahead of print]</b>. PMID[24077721].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23973825">Demonic Axe-like Conjugated Alkynes in Combating Microbes.</a> Komsani, J.R., S. Koppireddi, S. Avula, P.K. Koochana, and R. Yadla. European Journal of Medicinal Chemistry, 2013. 68: p. 132-138. PMID[23973825].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24051242">Chiral N-Benzyl-N-methyl-1-(naphthalen-1-yl)ethanamines and Their in Vitro Antifungal Activity against Cryptococcus neoformans, Trichophyton mentagrophytes and Trichophyton rubrum.</a> Krane Thvedt, T.H., K. Kaasa, E. Sundby, C. Charnock, and B.H. Hoff. European Journal of Medicinal Chemistry, 2013. 68: p. 482-496. PMID[24051242].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099509">Antifungal Dimeric Chalcone Derivative Kamalachalcone E from Mallotus philippinensis.</a> Kulkarni, R.R., S.G. Tupe, S.P. Gample, M.G. Chandgude, D. Sarkar, M.V. Deshpande, and S.P. Joshi. Natural Product Research, 2013. <b>[Epub ahead of print]</b>. PMID[24099509].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24090835">Block Copolymer Mixtures as Antimicrobial Hydrogels for Biofilm Eradication.</a> Lee, A.L., V.W. Ng, W. Wang, J.L. Hedrick, and Y.Y. Yang. Biomaterials, 2013. 34(38): p. 10278-10286. PMID[24090835].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23981323">Antifungal Activity of Dental Resins Containing Amphotericin B-conjugated Nanoparticles.</a> Lino, M.M., C.S. Paulo, A.C. Vale, M.F. Vaz, and L.S. Ferreira. Dental Materials, 2013. 29(10): p. e252-262. PMID[23981323].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099527">Guanylated Polymethacrylates: A Class of Potent Antimicrobial Polymers with Low Haemolytic Activity.</a> Locock, K.E., T.D. Michl, J.D. Valentin, K. Vasilev, J.D. Hayball, Y. Qu, A. Traven, H.J. Griesser, L. Meagher, and M. Haussler. Biomacromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[24099527].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23910360">Photochemical and Antimicrobial Properties of Silver Nanoparticle-encapsulated Chitosan Functionalized with Photoactive Groups.</a> Mathew, T.V. and S. Kuriakose. Materials Science &amp; Engineering. C Materials for Biological Applications, 2013. 33(7): p. 4409-4415. PMID[23910360].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24065288">Purification and Characterization of an Antifungal Chitinase from Citrobacter freundii Str.</a> Nov. Haritd11. Meruvu, H. and S.R. Donthireddy. Applied Biochemistry and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[24065288].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24061160">Bioimaging, Antibacterial and Antifungal Properties of Imidazole-pyridine fluorophores: Synthesis, Characterization and Solvatochromism.</a> Nagarajan, N., G. Vanitha, D.A. Ananth, A. Rameshkumar, T. Sivasudha, and R. Renganathan. Journal of Photochemistry and Photobiology B, 2013. 127: p. 212-222. PMID[24061160].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23880127">Plants Traditionally Used Individually and in Combination to Treat Sexually Transmitted Infections in Northern Maputaland, South Africa: Antimicrobial Activity and Cytotoxicity.</a> Naidoo, D., S.F. van Vuuren, R.L. van Zyl, and H. de Wet. Journal of Ethnopharmacology, 2013. 149(3): p. 656-667. PMID[23880127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24141744">The Antimicrobial Effects of Citrus limonum and Citrus aurantium Essential Oils on Multi-species Biofilms.</a> Oliveira, S.A., J.R. Zambrana, F.B. Iorio, C.A. Pereira, and A.O. Jorge. Brazilian Oral Research, 2013. 0: p. 0. PMID[24141744].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24112802">Antimicrobial Activity and SAR Study of New Gemini Imidazolium-based Chlorides.</a> Palkowski, L., J. Blaszczynski, A. Skrzypczak, J. Blaszczak, K. Kozakowska, J. Wroblewska, S. Kozuszko, E. Gospodarek, J. Krysinski, and R. Slowinski. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[24112802].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24129274">Structural Studies and Investigation on the Activity of Imidazole-derived Thiosemicarbazones and Hydrazones against Crop-related Fungi.</a> Reis, D.C., A.A. Despaigne, J.G. Silva, N.F. Silva, C.F. Vilela, I.C. Mendes, J.A. Takahashi, and H. Beraldo. Molecules, 2013. 18(10): p. 12645-12662. PMID[24129274].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23193085">Chemical and Biological Characterization of Novel Essential Oils from Eremophila bignoniiflora (F. Muell) (Myoporaceae): A Traditional Aboriginal Australian Bush Medicine.</a> Sadgrove, N.J., M. Hitchcock, K. Watson, and G.L. Jones. Phytotherapy Research, 2013. 27(10): p. 1508-1516. PMID[23193085].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24125484">2-Hydroxyisocaproic acid Is Fungicidal for Candida and Aspergillus Species.</a> Sakko, M., C. Moore, L. Novak-Frazer, V. Rautemaa, T. Sorsa, P. Hietala, A. Jarvinen, P. Bowyer, L. Tjaderhane, and R. Rautemaa. Mycoses, 2013. <b>[Epub ahead of print]</b>. PMID[24125484].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23973864">Antimicrobial Activity and Mechanism of Action of a Novel Cationic alpha-Helical Octadecapeptide Derived from Heat Shock Protein 70 of Rice.</a> Taniguchi, M., A. Ikeda, S. Nakamichi, Y. Ishiyama, E. Saitoh, T. Kato, A. Ochiai, and T. Tanaka. Peptides, 2013. 48: p. 147-155. PMID[23973864].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24078108">Drosera peltata Smith var. lunata (Buch.-Ham.) C. B. Clarke as a Feasible Source of Plumbagin: Phytochemical Analysis and Antifungal Activity Assay.</a> Tian, J., Y. Chen, B. Ma, J. He, J. Tong, and Y. Wang. World journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[24078108].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24090423">Identification and Structure-Activity Relationship Study of Carvacrol Derivatives as Mycobacterium tuberculosis Chorismate Mutase Inhibitors.</a> Alokam, R., V.U. Jeankumar, J.P. Sridevi, S.S. Matikonda, S. Peddi, M. Alvala, P. Yogeeswari, and D. Sriram. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24090423].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24138557">Two Rare-class Tricyclic Diterpenes with Anti-tubercular Activity from the Caribbean Sponge Svenzea flava. Application of Vibrational Circular Dichroism Spectroscopy for Determining Absolute Configuration.</a> Aviles, E., A.D. Rodriguez, and J. Vicente. The Journal of Organic Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24138557].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24129589">Design, Synthesis and Characterization of Novel Inhibitors against Mycobacterial beta-Ketoacyl CoA Reductase FabG4.</a> Banerjee, D.R., D. Dutta, B. Saha, S. Bhattacharyya, K. Senapati, A.K. Das, and A. Basak. Organic &amp; Biomolecular Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24129589].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24111908">Fluorinated Compounds against Mycobacterium tuberculosis.</a> Boechat, N. and M.M. Bastos. Current Topics in Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24111908].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23946523">In Vitro Activity of Amikacin against Isolates of Mycobacterium avium Complex with Proposed MIC Breakpoints and Finding of a 16S rRNA Gene Mutation in Treated Isolates.</a> Brown-Elliott, B.A., E. Iakhiaeva, D.E. Griffith, G.L. Woods, J.E. Stout, C.R. Wolfe, C.Y. Turenne, and R.J. Wallace, Jr. Journal of Clinical Microbiology, 2013. 51(10): p. 3389-3394. PMID[23946523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24130033">New Pterosin Sesquiterpenes and Antitubercular Constituents from Pteris ensiformis.</a> Chen, J.J., T.C. Wang, C.K. Yang, H.R. Liao, P.J. Sung, I.S. Chen, M.J. Cheng, C.F. Peng, and J.F. Chen. Chemistry &amp; Biodiversity, 2013. 10(10): p. 1903-1908. PMID[24130033].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24014472">Syntheses and Anti-tubercular Activity of Beta-substituted and alpha,beta-disubstituted Peptidyl beta-Aminoboronates and Boronic acids.</a> Gorovoy, A.S., O. Gozhina, J.S. Svendsen, G.V. Tetz, A. Domorad, V.V. Tetz, and T. Lejon. Journal of Peptide Science, 2013. 19(10): p. 613-618. PMID[24014472].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126586">Efflux Inhibition with Verapamil Potentiates Bedaquiline in Mycobacterium tuberculosis.</a> Gupta, S., K.A. Cohen, K. Winglee, M. Maiga, B. Diarra, and W.R. Bishai. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24126586].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126580">Optimization of Pyrrolamides as Mycobacterial GyrB ATPase Inhibitors: Structure Activity Relationship and in Vivo Efficacy in the Mouse Model of Tuberculosis.</a> Hameed, S.P., S. Solapure, K. Mukherjee, V. Nandi, D. Waterson, R. Shandil, M. Balganesh, V.K. Sambandamurthy, A.K. Raichurkar, A. Deshpande, A. Ghosh, D. Awasthy, G. Shanbhag, G. Sheikh, H. McMiken, J. Puttur, J. Reddy, J. Werngren, J. Read, M. Kumar, M. R, M. Chinnapattu, P. Madhavapeddi, P. Manjrekar, R. Basu, S. Gaonkar, S. Sharma, S. Hoffner, V. Humnabadkar, V. Subbulakshmi, and V. Panduga. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24126580].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24087924">Chlorinated Coumarins from the Polypore Mushroom Fomitopsis officinalis and Their Activity against Mycobacterium tuberculosis.</a> Hwang, C.H., B.U. Jaki, L.L. Klein, D.C. Lankin, J.B. McAlpine, J.G. Napolitano, N.A. Fryling, S.G. Franzblau, S.H. Cho, P.E. Stamets, Y. Wang, and G.F. Pauli. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[24087924].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23986448">Inhibition of the First Step in Synthesis of the Mycobacterial Cell Wall Core, Catalyzed by the GlcNAc-1-Phosphate Transferase WecA, by the Novel Caprazamycin Derivative CPZEN-45.</a> Ishizaki, Y., C. Hayashi, K. Inoue, M. Igarashi, Y. Takahashi, V. Pujari, D.C. Crick, P.J. Brennan, and A. Nomoto. The Journal of Biological Chemistry, 2013. 288(42): p. 30309-30319. PMID[23986448].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24098949">Ursolic and Oleanolic acids as Antimicrobial and Immunomodulatory Compounds for Tuberculosis Treatment.</a> Jimenez-Arellanes, A., J. Luna-Herrera, J. Cornejo-Garrido, S. Lopez-Garcia, M.E. Castro-Mussot, M. Meckes-Fischer, D. Mata-Espinoza, B. Marquina, J.L. Torres, and R.H. Pando. BMC Complementary and Alternative medicine, 2013. 13(1): p. 258. PMID[24098949].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">42. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24088190">Thiazolopyridine ureas as Novel Antitubercular Agents Acting through Inhibition of DNA Gyrase B.</a> Kale, M.G., A. Raichurkar, S.H. P, D. Waterson, D. McKinney, M.R. Manjunatha, U. Kranthi, K. Koushik, L.K. Jena, V. Shinde, S. Rudrapatna, S. Barde, V. Humnabadkar, P. Madhavapeddi, H. Basavarajappa, A. Ghosh, V. Ramya, S. Guptha, S. Sharma, P. Vachaspati, K.N. Kumar, J. Giridhar, J. Reddy, V. Panduga, S. Ganguly, V. Ahuja, S. Gaonkar, C.N. Kumar, D. Ogg, J.A. Tucker, P.A. Boriack-Sjodin, S.M. de Sousa, V.K. Sambandamurthy, and S.R. Ghorpade. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24088190].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">43. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24090347">Design, Synthesis and Biological Evaluation of Indole-2-carboxamides: A Promising Class of Anti-tuberculosis Agents.</a> Kondreddi, R.R., J. Jiricek, S.P. Rao, S.B. Lakshminarayana, L.R. Camacho, R. Rao, M. Herve, P. Bifani, N.L. Ma, K. Kuhen, A. Goh, A.K. Chatterjee, T. Dick, T.T. Diagana, U.H. Manjunatha, and P.W. Smith. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24090347].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">44. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24117130">Antimycobacterial Labdane Diterpenes from Leucas stelligera.</a> Kulkarni, R.R., K. Shurpali, V.G. Puranik, D. Sarkar, and S.P. Joshi. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[24117130].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">45. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24098102">Target Prediction for an Open Access Set of Compounds Active against Mycobacterium tuberculosis.</a> Martinez-Jimenez, F., G. Papadatos, L. Yang, I.M. Wallace, V. Kumar, U. Pieper, A. Sali, J.R. Brown, J.P. Overington, and M.A. Marti-Renom. PLoS Computational Biology, 2013. 9(10): p. e1003253. PMID[24098102].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">46. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24140915">Design, Chemical Synthesis of 3-(9H-Fluoren-9-yl)pyrrolidine-2,5-dione Derivatives and Biological Activity against Enoyl-ACP Reductase (InhA) and Mycobacterium tuberculosis.</a> Matviiuk, T., F. Rodriguez, N. Saffon, S. Mallet-Ladeira, M. Gorichko, A.L. de Jesus Lopes Ribeiro, M.R. Pasca, C. Lherbet, Z. Voitenko, and M. Baltas. European Journal of Medicinal Chemistry, 2013. 70C: p. 37-48. PMID[24140915].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">47. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145521">Therapeutic Efficacy of SQ641-NE against Mycobacterium tuberculosis.</a> Nikonenko, B., V.M. Reddy, E. Bogatcheva, M. Protopopova, L. Einck, and C.A. Nacy. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24145521].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">48. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24133190">Statin Therapy Reduces Mycobacterium tuberculosis Infection in Human Macrophages and in Mice by Enhancing Autophagy and Phagosome Maturation.</a> Parihar, S.P., R. Guler, R. Khutlang, D.M. Lang, R. Hurdayal, M.M. Mhlanga, H. Suzuki, A.D. Marais, and F. Brombacher. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[24133190].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">49. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24139881">IQG-607 Abrogates the Synthesis of Mycolic acids and Displays Intracellular Activity against Mycobacterium tuberculosis in Infected Macrophages.</a> Rodrigues-Junior, V.S., A.A. Dos Santos Junior, A.D. Villela, J.M. Belardinelli, H.R. Morbidoni, L.A. Basso, M.M. Campos, and D.S. Santos. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[24139881].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">50. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126578">New 2-Thiopyridines as Potential Candidates for Killing Both Actively Growing and Dormant Mycobacterium tuberculosis.</a> Salina, E., O. Ryabova, A. Kaprelyants, and V. Makarov. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24126578].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">51. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24107081">Methyl-thiazoles: A Novel Mode of Inhibition with the Potential to Develop Novel Inhibitors Targeting InhA in Mycobacterium tuberculosis.</a> Shirude, P.S., P. Madhavapeddi, M. Naik, K. Murugan, V. Shinde, R. N, J. Bhat, A. Kumar, S. Hameed, G.A. Holdgate, G. Davies, H. McMiken, N. Hegde, A. Ambady, J. Venkatraman, M. Panda, B. Bandodkar, V. Sambandamurthy, and J.A. Read. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24107081].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br />    

    <p class="plaintext">52. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24083570">Bactericidal Activity of PA-824 against Mycobacterium tuberculosis under Anaerobic Conditions and Computational Analysis of Its Novel Analogues against Mutant Ddn Receptor.</a> Somasundaram, S., R.S. Anand, P. Venkatesan, and C.N. Paramasivan. BMC Microbiology, 2013. 13(1): p. 218. PMID[24083570].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">53. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23953191">Design, Synthesis and Antitubercular Evaluation of Novel 2-Substituted-3H-benzofuro benzofurans via Palladium-Copper Catalysed Sonagashira Coupling Reaction.</a> Yempala, T., J.P. Sridevi, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(19): p. 5393-5396. PMID[23953191].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">54. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24098010">Para-aminosalicylic acid Is a Prodrug Targeting Dihydrofolate Reductase in Mycobacterium tuberculosis.</a> Zheng, J., E.J. Rubin, P. Bifani, V. Mathys, V. Lim, M. Au, J. Jang, J. Nam, T. Dick, J.R. Walker, K. Pethe, and L.R. Camacho. The Journal of Biological Chemistry, 2013. 288(40): p. 28951. PMID[24098010].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">55. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24063369">Design, Synthesis, and Biological and Crystallographic Evaluation of Novel Inhibitors of Plasmodium falciparum Enoyl-ACP-Reductase (PFFABI).</a> Belluti, F., R. Perozzo, L. Lauciello, F. Colizzi, D. Kostrewa, A. Bisi, S. Gobbi, A. Rampa, M.L. Bolognesi, M. Recanatini, R. Brun, L. Scapozza, and A. Cavalli. Journal of Medicinal Chemistry, 2013. 56(19): p. 7516-7526. PMID[24063369].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">56. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24139941">3,5-bis(Benzylidene)-4-piperidones and Related N-Acyl Analogs: A Novel Cluster of Antimalarials Targeting the Liver Stage of Plasmodium falciparum.</a> Das, U., R.S. Singh, J. Alcorn, M.R. Hickman, R.J. Sciotti, S.E. Leed, P.J. Lee, N. Roncal, and J.R. Dimmock. Bioorganic &amp; Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24139941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">57. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24032556">Thiaplakortones A-D: Antimalarial Thiazine Alkaloids from the Australian Marine Sponge Plakortis lita.</a> Davis, R.A., S. Duffy, S. Fletcher, V.M. Avery, and R.J. Quinn. The Journal of Organic Chemistry, 2013. 78(19): p. 9608-9613. PMID[24032556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">58. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23906782">Both Plants Sebastiania chamaelea from Niger and Chrozophora senegalensis from Senegal Used in African Traditional Medicine in Malaria Treatment Share a Same Active Principle.</a> Garcia-Alvarez, M.C., I. Moussa, P. Njomnang Soh, R. Nongonierma, A. Abdoulaye, M.L. Nicolau-Travers, A. Fabre, J. Wdzieczak-Bakala, A. Ahond, C. Poupat, K. Ikhiri, and F. Benoit-Vical. Journal of Ethnopharmacology, 2013. 149(3): p. 676-684. PMID[23906782].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">59. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23811202">Membrane Active Chelators as Novel Anti-african Trypanosome and Anti-malarial Drugs.</a> Grab, D.J., E. Nenortas, R.P. Bakshi, O.V. Nikolskaia, J.E. Friedman, and T.A. Shapiro. Parasitology International, 2013. 62(5): p. 461-463. PMID[23811202].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br />  

    <p class="plaintext">60. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23985373">Evaluation of Substituted Phenalenone Analogues as Antiplasmodial Agents.</a> Gutierrez, D., N. Flores, T. Abad-Grillo, and G. McNaughton-Smith. Experimental Parasitology, 2013. 135(2): p. 456-458. PMID[23985373].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">61. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24064863">Enantioselective Synthesis of Encecaline-derived Potent Antimalarial Agents.</a> Harel, D., D. Schepmann, R. Brun, T.J. Schmidt, and B. Wunsch. Organic &amp; Biomolecular Chemistry, 2013. 11(42): p. 7342-7349. PMID[24064863].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">62. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23968432">Natural Product Derived Antiprotozoal Agents: Synthesis, Biological Evaluation, and Structure-activity Relationships of Novel Chromene and Chromane Derivatives.</a> Harel, D., D. Schepmann, H. Prinz, R. Brun, T.J. Schmidt, and B. Wunsch. Journal of Medicinal Chemistry, 2013. 56(18): p. 7442-7448. PMID[23968432].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">63. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24032981">IspC as Target for Antiinfective Drug Discovery: Synthesis, Enantiomeric Separation, and Structural Biology of Fosmidomycin thia isosters.</a> Kunfermann, A., C. Lienau, B. Illarionov, J. Held, T. Grawert, C.T. Behrendt, P. Werner, S. Hahn, W. Eisenreich, U. Riederer, B. Mordmuller, A. Bacher, M. Fischer, M. Groll, and T. Kurz. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24032981].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">64. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836419">Redefining an Epitope of a Malaria Vaccine Candidate, with Antibodies against the N-Terminal MSA-2 Antigen of Plasmodium Harboring Non-natural Peptide Bonds.</a> Lozano, J.M., Y.A. Guerrero, M.P. Alba, L.P. Lesmes, J.O. Escobar, and M.E. Patarroyo. Amino Acids, 2013. 45(4): p. 913-935. PMID[23836419].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">65. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24107123">Drug Repositioning as a Route to Anti-malarial Drug Discovery: Preliminary Investigation of the in Vitro Anti-malarial Efficacy of Emetine Dihydrochloride Hydrate.</a> Matthews, H., M. Usman-Idris, F. Khan, M. Read, and N. Nirmalan. Malaria Journal, 2013. 12(1): p. 359. PMID[24107123].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">66. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23954323">Antimalarial and Safety Evaluation of Pluchea lanceolata (DC.) Oliv. &amp; Hiern: in-Vitro and in-Vivo Study.</a> Mohanty, S., P. Srivastava, A.K. Maurya, H.S. Cheema, K. Shanker, S. Dhawan, M.P. Darokar, and D.U. Bawankule. Journal of Ethnopharmacology, 2013. 149(3): p. 797-802. PMID[23954323].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">67. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24146207">Assessment of in Vivo Antimalarial Activities of Some Selected Medicinal Plants from Turkey.</a> Ozbilgin, A., C. Durmuskahya, H. Kayalar, and I. Ostan. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[24146207].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">68. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24120516">A New Protoberberine Alkaloid from Meconopsis simplicifolia (D. Don) Walpers with Potent Antimalarial Activity against a Multidrug Resistant Plasmodium falciparum Strain.</a> Phurpa, W., P.A. Keller, S.G. Pyne, W. Lie, A.C. Willis, R. Rattanajak, and S. Kamchonwongpaisan. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[24120516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br />   

    <p class="plaintext">69. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24020770">Structural Optimization of Quinolon-4(1H)-imines as Dual-stage Antimalarials: Toward Increased Potency and Metabolic Stability.</a> Ressurreicao, A.S., D. Goncalves, A.R. Sitoe, I.S. Albuquerque, J. Gut, A. Gois, L.M. Goncalves, M.R. Bronze, T. Hanscheid, G.A. Biagini, P.J. Rosenthal, M. Prudencio, P. O&#39;Neill, M.M. Mota, F. Lopes, and R. Moreira. Journal of Medicinal Chemistry, 2013. 56(19): p. 7679-7690. PMID[24020770].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">70. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24075342">Antiplasmodial, Cytotoxic Activities and Characterization of a New Naturally Occurring Quinone methide pentacyclic triterpenoid Derivative Isolated from Salacia leptoclada Tul. (Celastraceae) Originated from Madagascar.</a> Ruphin, F.P., R. Baholy, A. Emmanue, R. Amelie, M.T. Martin, and N. Koto-Te-Nyiwa. Asian Pacific Journal of Tropical Biomedicine, 2013. 3(10): p. 780-784. PMID[24075342].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">71. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24098696">A Purine Analog Synergizes with Chloroquine (CQ) by Targeting Plasmodium falciparum HSP90 (PFHSP90).</a> Shahinas, D., A. Folefoc, T. Taldone, G. Chiosis, I. Crandall, and D.R. Pillai. Plos One, 2013. 8(9): p. e75446. PMID[24098696].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">72. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24108397">Anti-plasmodial Activity of Some Zulu Medicinal Plants and of Some Triterpenes Isolated from Them.</a> Simelane, M.B., A. Shonhai, F.O. Shode, P. Smith, M. Singh, and A.R. Opoku. Molecules, 2013. 18(10): p. 12313-12323. PMID[24108397].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">73. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24073986">Novel Selective and Potent Inhibitors of Malaria Parasite Dihydroorotate Dehydrogenase: Discovery and Optimization of Dihydrothiophenone Derivatives.</a> Xu, M., J. Zhu, Y. Diao, H. Zhou, X. Ren, D. Sun, J. Huang, D. Han, Z. Zhao, L. Zhu, Y. Xu, and H. Li. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24073986].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">74. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099149">Structure-Activity-Relationship Studies around the 2-Amino Group and Pyridine Core of Antimalarial 3,5-Diarylaminopyridines Lead to a Novel Series of Pyrazine Analogues with Oral in Vivo Activity.</a> Younis, Y., F. Douelle, D. Gonzalez Cabrera, C. Le Manach, A.T. Nchinda, T. Paquet, L.J. Street, K.L. White, K.M. Zabiulla, J.T. Joseph, S. Bashyam, D. Waterson, M.J. Witty, S. Wittlin, S.A. Charman, and K. Chibale. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24099149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">75. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23878324">A Novel Calcium-dependent Protein Kinase Inhibitor as a Lead Compound for Treating Cryptosporidiosis.</a> Castellanos-Gonzalez, A., A.C. White, Jr., K.K. Ojo, R.S. Vidadala, Z. Zhang, M.C. Reid, A.M. Fox, K.R. Keyloun, K. Rivas, A. Irani, S.M. Dann, E. Fan, D.J. Maly, and W.C. Van Voorhis. The Journal of Infectious Diseases, 2013. 208(8): p. 1342-1348. PMID[23878324].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">76. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23927969">Substituted Pyrrolo[2,3-d]pyrimidines as Cryptosporidium hominis Thymidylate Synthase Inhibitors.</a> Kumar, V.P., K.M. Frey, Y. Wang, H.K. Jain, A. Gangjee, and K.S. Anderson. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(19): p. 5426-5428. PMID[23927969].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">77. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24020843">Synthetic Chondramide A Analogues Stabilize Filamentous Actin and Block Invasion by Toxoplasma gondii.</a> Ma, C.I., K. Diraviyam, M.E. Maier, D. Sept, and L.D. Sibley. Journal of Natural Products, 2013. 76(9): p. 1565-1572. PMID[24020843].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">78. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856773">Improved Efficacy of Fosmidomycin against Plasmodium and Mycobacterium Species by Combination with the Cell-penetrating Peptide Octaarginine.</a> Sparr, C., N. Purkayastha, B. Kolesinska, M. Gengenbacher, B. Amulic, K. Matuschewski, D. Seebach, and F. Kamena. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4689-4698. PMID[23856773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1011-102413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">79. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323665600015">New Carbodithioate Derivatives: Synthesis, Characterization, and in Vitro Antibacterial, Antifungal, Antitubercular, and Antimalarial Activity.</a> Akhaja, T.N. and J.P. Raval. Medicinal Chemistry Research, 2013. 22(10): p. 4700-4707. ISI[000323665600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">80. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400072">Biological Evaluation of Bisbenzaldehydes against Four Mycobacterium Species.</a> Cappoen, D., D. Forge, F. Vercammen, V. Mathys, M. Kiass, V. Roupie, R. Anthonissen, L. Verschaeve, J.J.V. Eynde, and K. Huygen. European Journal of Medicinal Chemistry, 2013. 63: p. 731-738. ISI[000322855400072].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">81. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323865200009">Antifungal Mechanism of Essential Oil from Anethum graveolens Seeds against Candida albicans.</a> Chen, Y.X., H. Zeng, J. Tian, X.Q. Ban, B.X. Ma, and Y.W. Wang. Journal of Medical Microbiology, 2013. 62: p. 1175-1183. ISI[000323865200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">82. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323665600038">Preparation and Antimicrobial Activity Evaluation of Some New Bi- and Triheterocyclic Azoles.</a> Demirci, S., S. Basoglu, A. Bozdereci, and N. Demirbas. Medicinal Chemistry Research, 2013. 22(10): p. 4930-4945. ISI[000323665600038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">83. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323665600048">Synthesis and Antimicrobial Screening of 5-(Benzylidene)-3-phenylthiazolidin-4-one Derivatives Incorporating Thiazole Ring.</a> Desai, N.C., K.M. Rajpara, and V.V. Joshi. Medicinal Chemistry Research, 2013. 22(10): p. 5044-5055. ISI[000323665600048].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">84. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322940700006">A New Hybrid Approach and in Vitro Antimicrobial Evaluation of Novel 4(3H)-Quinazolinones and Thiazolidinone Motifs.</a> Desai, N.C., H.V. Vaghani, and P.N. Shihora. Journal of Fluorine Chemistry, 2013. 153: p. 39-47. ISI[000322940700006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">85. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400044">Efficient Synthesis of New (R)-2-Amino-1-butanol Derived Ureas, Thioureas and Acylthioureas and in Vitro Evaluation of Their Antimycobacterial Activity.</a> Dobrikov, G.M., V. Valcheva, Y. Nikolova, I. Ugrinova, E. Pasheva, and V. Dimitrov. European Journal of Medicinal Chemistry, 2013. 63: p. 468-473. ISI[000322855400044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">86. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400066">Synthesis and Identification of beta-Aryloxyquinoline Based Diversely Fluorine Substituted N-Aryl Quinolone Derivatives as a New Class of Antimicrobial, Antituberculosis and Antioxidant Agents.</a> Kathrotiya, H.G. and M.P. Patel. European Journal of Medicinal Chemistry, 2013. 63: p. 675-684. ISI[000322855400066].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">87. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324455300016">Medicinal Plants from Open-air Markets in the State of Rio De Janeiro, Brazil as a Potential Source of New Antimycobacterial Agents.</a> Leitao, F., S.G. Leitao, M.Z. de Almeida, J. Cantos, T. Coelho, and P.E.A. da Silva. Journal of Ethnopharmacology, 2013. 149(2): p. 513-521. ISI[000324455300016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">88. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324244300004">Design of 1-(Furan-2-yl)-N-(5-substituted Phenyl-1, 3, 4-thiadiazol-2-yl) methanimine Derivatives as Enoyl-ACP Reductase Inhibitors: Synthesis, Molecular Docking Studies and Anti-tubercular Activity.</a> Mathew, B., G.E. Mathew, G. Sonia, A. Kumar, N.P. Charles, and P. Kumar. Bangladesh Journal of Pharmacology, 2013. 8(3): p. 242-248. ISI[000324244300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">89. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323851300815">Synthesis of Substrate, Development of Assay, and Discovery of Inhibitor against Mycobacteria.</a> Narayanasamy, P., B. Edagwa, and D. Crick. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000323851300815].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">90. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323665600020">Synthesis of a New Series of 2-(2-Oxo-2H-chromen-3-yl)-5H-chromeno[4,3-b]pyridin-5-ones by Two Facile Methods and Evaluation of Their Antimicrobial Activity.</a> Patel, A.A., H.B. Lad, K.R. Pandya, C.V. Patel, and D.I. Brahmbhatt. Medicinal Chemistry Research, 2013. 22(10): p. 4745-4754. ISI[000323665600020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">91. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323665600017">Synthesis and Evaluation of Antibacterial and Antifungal Activities of New (Z)-3-Bromo-4-(1,3-diaryl-1H-pyrazol-4-yl)but-3-en-2-ones and 4-(3-Methyl-1-phenyl-1H-pyrazol-5-yl)-1,3-diaryl-1H-pyrazoles.</a> Pundeer, R., Sushma, V. Kiran, C. Sharma, K.R. Aneja, and O. Prakash. Medicinal Chemistry Research, 2013. 22(10): p. 4715-4726. ISI[000323665600017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">92. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323801800039">Design and Synthesis of Positional Isomers of 5 and 6-Bromo-1-(phenyl)sulfonyl-2-(4-nitrophenoxy)methyl-1H-benzimidazole S as Possible Antimicrobial and Antitubercular Agents.</a> Ranjith, P.K., P. Rajeesh, K.R. Haridas, N.K. Susanta, T.N.G. Row, R. Rishikesan, and N.S. Kumari. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(18): p. 5228-5234. ISI[000323801800039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br /> 

    <p class="plaintext">93. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324210400013">A Facile Eco-friendly One-pot Five-component Synthesis of Novel 1,2,3-Triazole-linked Pentasubstituted 1,4-Dihydropyridines and Their Biological and Photophysical Studies.</a> Singh, H., J. Sindhu, J.M. Khurana, C. Sharma, and K.R. Aneja. Australian Journal of Chemistry, 2013. 66(9): p. 1088-1096. ISI[000324210400013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1011-102413.</p><br />

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
