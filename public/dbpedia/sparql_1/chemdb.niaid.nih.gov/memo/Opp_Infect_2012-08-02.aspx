

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-08-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="GIFRekFkm3/N0RaxwrAd369ZAHW4LN+Y+FVE7O6QWkQ7K3M9LxuvTqfAv4O6pDKFC79NQHpz3hETM0Lo9LDPZ5BAs83Q2OgRhAIMzC/XBcEKnt7wKJ1Cj+TBJfM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="946C5C39" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  July 20 - Aug 2, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. S<a href="http://www.ncbi.nlm.nih.gov/pubmed/22829337">ynthesis and Antimicrobial Activities of a Novel Series of Heterocyclic Alpha-Aminophosphonates.</a> Abdel-Megeed, M.F., B.E. Badr, M.M. Azaam, and G.A. El-Hiti. Archiv der Pharmazie, 2012. <b>[Epub ahead of print]</b>. PMID[22829337].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22828966">Antibacterial, Antifungal and Antileishmanial Activities of Indolone-N-Oxide Derivatives.</a> Ibrahim, H., A. Furiga, E. Najahi, C. Pigasse Henocq, J.P. Nallet, C. Roques, A. Aubouy, M. Sauvain, P. Constant, M. Daffe, and F. Nepveu. The Journal of Antibiotics, 2012. <b>[Epub ahead of print]</b>. PMID[22828966]. [<b>PubMed</b>]. OI_0720-080212.</p> 
    
    <p> </p>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22833742">Enhancement of Antimycotic Activity of Amphotericin B by Targeting the Oxidative Stress Response of <i>Candida</i> and <i>Cryptococcus</i> with Natural Dihydroxybenzaldehydes.</a> Kim, J.H., N.C. Faria, M.D. Martins, K.L. Chan, and B.C. Campbell. Frontiers in Microbiology, 2012. 3: p. 261. PMID[22833742]. [<b>PubMed</b>]. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22839690">Biological Activities of Extracts from <i>Chenopodium ambrosioides</i> lineu and <i>Kielmeyera neglecta</i> saddi.</a> Sousa, Z.L., F.F. de Oliveira, A.O. da Conceicao, L.A. Silva, M.H. Rossi, J.S. Santos, and J.L. Andrioli. Annals of Clinical Microbiology and Antimicrobials, 2012. 11(1): p. 20. PMID[22839690]. [<b>PubMed</b>]. OI_0720-080212.</p>
    
    <p> </p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22828481">14-Day Bactericidal Activity of Pa-824, Bedaquiline, Pyrazinamide, and Moxifloxacin Combinations: A Randomised Trial.</a> Diacon, A.H., R. Dawson, F. von Groote-Bidlingmaier, G. Symons, A. Venter, P.R. Donald, C. van Niekerk, D. Everitt, H. Winter, P. Becker, C.M. Mendel, and M.K. Spigelman. Lancet, 2012. <b>[Epub ahead of print]</b>. PMID[22828481].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22814612">Exploring Anti-Tb Leads from Natural Products Library Originated from Marine Microbes and Medicinal Plants.</a> Liu, X., C. Chen, W. He, P. Huang, M. Liu, Q. Wang, H. Guo, K. Bolla, Y. Lu, F. Song, H. Dai, and L. Zhang. Antonie van Leeuwenhoek, 2012. <b>[Epub ahead of print]</b>. PMID[22814612].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0720-080212.</p> 
    
    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22825123">Systematic Analysis of Pyrazinamide-Resistant Spontaneous Mutants and Clinical Isolates of <i>Mycobacterium tuberculosis</i>.</a> Stoffels, K., V. Mathys, M. Fauville-Dufaux, R. Wintjens, and P. Bifani. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22825123].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22825115">The Combination of Sulfamethoxazole, Trimethoprim, and Isoniazid or Rifampin Is Bactericidal and Prevents the Emergence of Drug Resistance in <i>Mycobacterium tuberculosis</i>.</a> Vilcheze, C. and W.R. Jacobs, Jr. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[22825115]. [<b>PubMed</b>]. OI_0720-080212.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22813531">Discovery of a <i>Plasmodium falciparum</i> Glucose-6-Phosphate Dehydrogenase 6-Phosphogluconolactonase Inhibitor (R,Z)-N-((1-Ethylpyrrolidin-2-Yl)Methyl)-2-(2-Fluorobenzylidene)-3-Oxo-3,4-Dihydr O-2h-Benzo[B][1,4]Thiazine-6-Carboxamide (Ml276) That Reduces Parasite Growth in Vitro.</a> Preuss, J., P. Maloney, S. Peddibhotla, M.P. Hedrick, P. Hershberger, P. Gosalia, M. Milewski, Y.L. Li, E. Sugarman, B. Hood, E. Suyama, K. Nguyen, S. Vasile, E. Sergienko, A. Mangravita-Novo, M. Vicchiarelli, D. McAnally, L.H. Smith, G.P. Roth, J. Diwan, T.D. Chung, E. Jortzik, S. Rahlfs, K. Becker, A.B. Pinkerton, and L. Bode. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[22813531].</p>

    <p class="plaintext">[<b>PubMed</b>]. OI_0720-080212.</p>

    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305797700002">Synthesis, Characterization and Biological Studies of 2-(4-Nitrophenylaminocarbonyl)Benzoic Acid and Its Complexes with Cr(Iii), Co(Ii), Ni(Ii), Cu(Ii) and Zn(Ii).</a> Ashraf, M.A., M.J. Maah, and I. Yusuf. Iranian Journal of Chemistry &amp; Chemical Engineering-International English Edition, 2012. 31(1): p. 9-14. ISI[000305797700002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305800400008">Biological Activities of Alpha-Pinene and Beta-Pinene Enantiomers.</a> da Silva, A.C.R., P.M. Lopes, M.M.B. de Azevedo, D.C.M. Costa, C.S. Alviano, and D.S. Alviano. Molecules, 2012. 17(6): p. 6305-6316. ISI[000305800400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900005">A Clubbed Quinazolinone and 4-Thiazolidinone as Potential Antimicrobial Agents.</a> Desai, N.C., A.M. Dodiya, and P.N. Shihora. Medicinal Chemistry Research, 2012. 21(8): p. 1577-1586. ISI[000305559900005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900003">Microwave-Assisted Synthesis and Antimicrobial Activity of Some Imidazo 2,1-B 1,3,4 Thiadiazole Derivatives.</a> Dhepe, S., S. Kumar, R. Vinayakumar, S.A. Ramareddy, and S.S. Karki. Medicinal Chemistry Research, 2012. 21(8): p. 1550-1556. ISI[000305559900003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900036">Antioxidant Potential and Antimicrobial Screening of Some Novel Imidazole Derivatives: Greenway Efficient One Pot Synthesis.</a> Jayabharathi, J., V. Thanikachalam, N. Rajendraprasath, K. Saravanan, and M.V. Perumal. Medicinal Chemistry Research, 2012. 21(8): p. 1850-1860. ISI[000305559900036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900046">Synthesis, Antimycobacterial, Antiviral, Antimicrobial Activities, and QSAR Studies of Isonicotinic Acid-1-(Substituted Phenyl)-Ethylidene/Cycloheptylidene Hydrazides.</a> Judge, V., B. Narasimhan, M. Ahuja, D. Sriram, P. Yogeeswari, E. De Clercq, C. Pannecouque, and J. Balzarini. Medicinal Chemistry Research, 2012. 21(8): p. 1935-1952. ISI[000305559900046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>
    
    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305892100157">High-Throughput Screening and Sensitized Bacteria Identify an <i>M. tuberculosis</i> Dihydrofolate Reductase Inhibitor with Whole Cell Activity.</a> Kumar, A., M. Zhang, L.Y. Zhu, R.L.P. Liao, C. Mutai, S. Hafsat, D.R. Sherman, and M.W. Wang. PLoS One, 2012. 7(6). ISI[000305892100157].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900032">Synthesis, Characterization and Antimicrobial Activities of Some Novel Bis-Chalcones.</a> Mobinikhaledi, A., M. Kalhor, and H. Jamalifar. Medicinal Chemistry Research, 2012. 21(8): p. 1811-1816. ISI[000305559900032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900057">Pyridoquinolones Containing Azetidinones: Synthesis and Their Biological Evaluation.</a> Patel, N.B. and K.K. Pathak. Medicinal Chemistry Research, 2012. 21(8): p. 2044-2055. ISI[000305559900057].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305777900017">Studies on Antimicrobial Efficacy of Medicinal Tuberous Shrub <i>Talinum cuneifolium</i>.</a> Savithramma, N., S. Ankanna, M.L. Rao, and J. Saradvathi. Journal of Environmental Biology, 2012. 33(4): p. 775-780. ISI[000305777900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305595200013">Synthesis of a Novel Class of Some Biquinoline Pyridine Hybrids Via One-Pot, Three-Component Reaction and Their Antimicrobial Activity.</a> Shah, N.M., M.P. Patel, and R.G. Patel. Journal of Chemical Sciences, 2012. 124(3): p. 669-677. ISI[000305595200013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900053">QSAR Studies of 7-Methyljuglone Derivatives as Antitubercular Agents</a>. Sharma, R., D. Panigrahi, and G.P. Mishra. Medicinal Chemistry Research, 2012. 21(8): p. 2006-2011. ISI[000305559900053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900020">Synthesis, Spectroscopic, Thermal and Antimicrobial Studies of Co(Ii), Ni(Ii), Cu(Ii) and Zn(Ii) Complexes with Schiff Base Derived from 4-Amino-3-Mercapto-6-Methyl-5-Oxo-1,2,4-Triazine.</a> Singh, K., Y. Kumar, P. Puri, C. Sharma, and K.R. Aneja. Medicinal Chemistry Research, 2012. 21(8): p. 1708-1716. ISI[000305559900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900025">Synthesis, Characterization and Biological Activity of Some New Carbostyril Bearing 1h-Pyrazole Moiety.</a> Thumar, N.J. and M.P. Patel. Medicinal Chemistry Research, 2012. 21(8): p. 1751-1761. ISI[000305559900025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305559900040">Synthesis and anti-Tubercular Evaluation of Some Novel Pyrazolo 3,4-D Pyrimidine Derivatives.</a> Trivedi, A.R., B.H. Dholariya, C.P. Vakhariya, D.K. Dodiya, H.K. Ram, V.B. Kataria, A.B. Siddiqui, and V.H. Shah. Medicinal Chemistry Research, 2012. 21(8): p. 1887-1891. ISI[000305559900040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306093300004">Improved Synthesis and <i>in vitro</i> Study of Antimicrobial Activity of Alpha,Beta-Unsaturated and Alpha-Bromo Carboxylic Acids.</a> Vitnik, V.D., M.T. Milenkovic, S.P. Dilber, Z.J. Vitnik, and I.O. Juranic. Journal of the Serbian Chemical Society, 2012. 77(6): p. 741-750. ISI [000306093300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305863100031">Design, Synthesis and Structure-Activity Relationships of New Triazole Derivatives Containing N-Substituted Phenoxypropylamino Side Chains.</a> Wang, S.Z., G. Jin, W.Y. Wang, L.J. Zhu, Y.Q. Zhang, G.Q. Dong, Y. Liu, C.L. Zhuang, Z.Y. Miao, J.Z. Yao, W.N. Zhang, and C.Q. Sheng. European Journal of Medicinal Chemistry, 2012. 53: p. 292-299. ISI[000305863100031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p> 

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305739100017">Two Novel Non-Conventional Seed Oil Extracts with Antioxidant and Antimicrobial Activities.</a></p>

    <p class="plaintext">Zoue, L.T., M.E. Bedikou, J.T. Gonnety, B.M. Faulet, and S.L. Niamke. Tropical Journal of Pharmaceutical Research, 2012. 11(3): p. 469-475. ISI[000305739100017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0720-080212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
