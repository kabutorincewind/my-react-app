

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-10-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vMrVnEFceVet3V3lKoDq+mVUr6KOP9ctLNH3QCieLIQCdF032uiUgyztKVbmH9L9v9AlPvgK1LdtXKN/EtVEvPYQiC8yinbeYKc1lOiosiQBQbhabHHM0XUGnUM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="05FD52E2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 26 - October 9, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25150089">Design and Discovery of 5-Hydroxy-6-oxo-1,6-dihydropyrimidine-4-carboxamide Inhibitors of HIV-1 Integrase.</a> Zhang, D., B. Debnath, S. Yu, T.W. Sanchez, F. Christ, Y. Liu, Z. Debyser, N. Neamati, and G. Zhao. Bioorganic &amp; Medicinal Chemistry, 2014. 22(19): p. 5446-5453. PMID[25150089].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25072874">2,4,5-Trisubstituted Thiazole Derivatives: A Novel and Potent Class of Non-nucleoside Inhibitors of Wild Type and Mutant HIV-1 Reverse Transcriptase.</a> Xu, Z., M. Ba, H. Zhou, Y. Cao, C. Tang, Y. Yang, R. He, Y. Liang, X. Zhang, Z. Li, L. Zhu, Y. Guo, and C. Guo. European Journal of Medicinal Chemistry, 2014. 85: p. 27-42. PMID[25072874].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25272020">Adenosine Deaminase Acting on RNA-1 (ADAR1) Inhibits HIV-1 Replication in Human Alveolar Macrophages.</a> Weiden, M.D., S. Hoshino, D.N. Levy, Y. Li, R. Kumar, S.A. Burke, R. Dawson, C.E. Hioe, W. Borkowsky, W.N. Rom, and Y. Hoshino. PloS One, 2014. 9(10): p. e108476. PMID[25272020].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25146692">Effects of Enzyme Inducers Efavirenz and Tipranavir/Ritonavir on the Pharmacokinetics of the HIV Integrase Inhibitor Dolutegravir.</a> Song, I., J. Borland, S. Chen, P. Guta, Y. Lou, D. Wilfret, T. Wajima, P. Savina, A. Peppercorn, S. Castellino, D. Wagner, L. Hosking, M. Mosteller, J.P. Rubio, and S.C. Piscitelli. European Journal of Clinical Pharmacology, 2014. 70(10): p. 1173-1179. PMID[25146692].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24674646">Synthesis and anti-HIV Activity of 4-(Naphthalen-1-yl)-1,2,5-thiadiazol-3-hydroxyl Derivatives.</a> Rai, D., W. Chen, P. Zhan, H. Liu, Y. Tian, X. Liang, E. De Clercq, C. Pannecouque, J. Balzarini, and X. Liu. Chemical Biology &amp; Drug Design, 2014. 84(4): p. 420-430. PMID[24674646].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25150090">Arylazolyl(azinyl)thioacetanilides. Part 16: Structure-based Bioisosterism Design, Synthesis and Biological Evaluation of Novel Pyrimidinylthioacetanilides as Potent HIV-1 Inhibitors.</a> Li, X., X. Lu, W. Chen, H. Liu, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(19): p. 5290-5297. PMID[25150090].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25092710">The Combined anti-HIV-1 Activities of Emtricitabine and Tenofovir Plus the Integrase Inhibitor Elvitegravir or Raltegravir Show High Levels of Synergy in Vitro.</a> Kulkarni, R., R. Hluhanich, D.M. McColl, M.D. Miller, and K.L. White. Antimicrobial Agents and Chemotherapy, 2014. 58(10): p. 6145-6150. PMID[25092710].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25222521">Synthesis and Antiretroviral Activity of Novel 4&#39;-Fluoro-5&#39;-deoxyphosphonic acid Carbocyclic Nucleoside Analogs.</a> Kim, E., S. Kim, and J.H. Hong. Nucleosides, Nucleotides &amp; Nucleic Acids, 2014. 33(10): p. 678-695. PMID[25222521].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25174000">A Broad HIV-1 Inhibitor Blocks Envelope Glycoprotein Transitions Critical for Entry.</a> Herschhorn, A., C. Gu, N. Espy, J. Richard, A. Finzi, and J.G. Sodroski. Nature Chemical Biology, 2014. 10(10): p. 845-852. PMID[25174000].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24970741">Broad anti-HIV Activity of the Oscillatoria agardhii Agglutinin Homologue Lectin Family.</a> Ferir, G., D. Huskens, S. Noppen, L.M. Koharudin, A.M. Gronenborn, and D. Schols. The Journal of Antimicrobial Chemotherapy, 2014. 69(10): p. 2746-2758. PMID[24970741].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25092689">Identification of Highly Conserved Residues Involved in Inhibition of HIV-1 RNase H Function by Diketo acid Derivatives.</a> Corona, A., F.S. Di Leva, S. Thierry, L. Pescatori, G. Cuzzucoli Crucitti, F. Subra, O. Delelis, F. Esposito, G. Rigogliuso, R. Costi, S. Cosconati, E. Novellino, R. Di Santo, and E. Tramontano. Antimicrobial Agents and Chemotherapy, 2014. 58(10): p. 6101-6110. PMID[25092689].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0926-100914.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341464500041">Identification of Constrained Peptidomimetic Chemotypes as HIV Protease Inhibitors.</a> Calugi, C., A. Guarna, and A. Trabocchi. European Journal of Medicinal Chemistry, 2014. 84: p. 444-453. ISI[000341464500041].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000341743000005">Synthesis and Biological Evaluation of N-substituted Polycyclic Imides Derivatives.</a> Bielenica, A., M. Struga, B. Miroslaw, A.E. Koziol, J. Kossakowski, G. Sanna, C.P. La, and G. Giliberti. Acta Poloniae Pharmaceutica, 2014. 70(5): p. 809-822. ISI[000341743000005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0926-100914.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140828&amp;CC=WO&amp;NR=2014128198A1&amp;KC=A1">Preparation of Bilaterally Substituted Tricyclic Compounds Useful in the Treatment of HIV-1 Infection.</a> Gallego Sala, J., S. Fustero Lardies, J. Alcami Pertejo, L. Gonzalez Bulnes, I. Ibanez Sanchez, S. Catalan Munoz, S. Prado Martin, A. Cantero Camacho, and P. Barrio Fernandez. Patent. 2014. 2014-EP53294 2014128198: 80pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140828&amp;CC=WO&amp;NR=2014128213A1&amp;KC=A1">Compounds for Use in Inhibiting HIV Capsid Assembly.</a> Kraeusslich, H.-G., J. Sticht, M. Pavova, V. Lux, J. Konvalinka, M. Kotora, K. Grantz Saskova, M. Kozisek, O. Stepanek, K. Parkan, and A. Machara. Patent. 2014. 2014-EP53324 2014128213: 85pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140828&amp;CC=WO&amp;NR=2014128206A1&amp;KC=A1">Compounds for Use in Inhibiting HIV Capsid Assembly.</a> Kraeusslich, H.-G., J. Sticht, M. Wildova, V. Lux, J. Konvalinka, M. Kotora, K. Grantz Saskova, M. Kozisek, O. Stepanek, K. Parkan, and A. Machara. Patent. 2014. 2014-EP53303 2014128206: 110pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140910&amp;CC=CN&amp;NR=104031117A&amp;KC=A">Application of Compounds from American National Cancer Institute in Resisting Human Immunodeficiency Virus Type 1 Protease Activity.</a> Lin, J., W. Huang, Z. Hong, D. Li, Y. Wei, Z. Chen, and J. Li. Patent. 2014. 2014-10168474 104031117: 9pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140827&amp;CC=CN&amp;NR=104003986A&amp;KC=A">Preparation of Pyridine Bicyclic Compounds for Treating Retroviral Integrase Mediated Disease.</a> Long, Y., F. Zhang, S. Huang, and Y. Zheng. Patent. 2014. 2013-10057501 104003986: 54pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140904&amp;CC=US&amp;NR=2014249162A1&amp;KC=A1">Preparation of Pyrrolopyridine Derivatives Useful as Antiviral Agents.</a> Son, J.C., B.J. Kim, J.H. Kim, I.Y. Lee, C.S. Yun, S.H. Lee, and C.K. Lee. Patent. 2014. 2014-14278955</p>

    <p class="plaintext">20140249162: 100pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0926-100914.</p>

    <br />  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
