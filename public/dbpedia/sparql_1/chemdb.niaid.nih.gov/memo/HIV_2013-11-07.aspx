

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-11-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PVG/MFwPympvZ0rxVIh9yw6mtUkS9/QUC4/K9ilGB0yOGNul4XqHUc79lDb1fTJBV7Fooo2gikTdpfH5A5ZAnSAZShxfsnuXeDGKcPpXP5EdZO30OamtpGZDmbM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="08AF36FA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: October 25 - November 7, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24156302">S100A9 Protein Is a Novel Ligand for the CD85j Receptor and Its Interaction Is Implicated in the Control of HIV-1 Replication by NK Cells.</a>Arnold, V., J.S. Cummings, U.Y. Moreno-Nieves, C. Didier, A. Gilbert, F. Barre-Sinoussi, and D. Scott-Algara. Retrovirology, 2013. 10(1): p. 122. PMID[24156302].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24119448">Design, Synthesis and Biological Evaluation of N2,N4-Disubstituted-1,1,3-trioxo-2H,4H-pyrrolo[1,2-b][1,2,4,6]thiatriazine Derivatives as HIV-1 NNRTIs.</a>Chen, W., P. Zhan, E. De Clercq, C. Pannecouque, J. Balzarini, X. Jiang, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(22): p. 7091-7100. PMID[24119448].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24124919">6-(1-Benzyl-1H-pyrrol-2-yl)-2,4-dioxo-5-hexenoic acids as Dual Inhibitors of Recombinant HIV-1 Integrase and Ribonuclease H, Synthesized by a Parallel Synthesis Approach.</a> Costi, R., M. Metifiot, F. Esposito, G. Cuzzucoli Crucitti, L. Pescatori, A. Messore, L. Scipione, S. Tortorella, L. Zinzula, E. Novellino, Y. Pommier, E. Tramontano, C. Marchand, and R. Di Santo. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24124919].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24165098">Biological Activity of Sporolide A and B from Salinispora tropica: in Silico Target Prediction Using Ligand Based Pharmacophore Mapping and in Vitro Activity Validation on HIV-1 Reverse Transcriptase.</a>Dineshkumar, K., V. Aparna, K.Z. Madhuri, and W. Hopper. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[24165098].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24119449">CXCR4-derived Synthetic Peptides Inducing anti-HIV-1 Antibodies.</a>Hashimoto, C., W. Nomura, T. Narumi, M. Fujino, T. Nakahara, N. Yamamoto, T. Murakami, and H. Tamamura. Bioorganic &amp; Medicinal Chemistry, 2013. 21(22): p. 6878-6885. PMID[24119449].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23673219">Small Molecule Fusion Inhibitors: Design, Synthesis and Biological Evaluation of (Z)-3-(5-(3-Benzyl-4-oxo-2-thioxothiazolidinylidene)methyl)-N-(3-carboxy-4-hydroxy)phenyl-2,5-dimethylpyrroles and Related Derivatives Targeting HIV-1 gp41.</a> He, X.Y., L. Lu, J. Qiu, P. Zou, F. Yu, X.K. Jiang, L. Li, S. Jiang, S. Liu, and L. Xie. Bioorganic &amp; Medicinal Chemistry, 2013. 21(23): p. 7539-7548. PMID[23673219].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24060906">Terpenoids and Their anti-HIV-1 Activities from Excoecaria acerifolia.</a>Huang, S.Z., X. Zhang, Q.Y. Ma, Y.T. Zheng, F.Q. Xu, H. Peng, H.F. Dai, J. Zhou, and Y.X. Zhao. Fitoterapia, 2013. 91: p. 224-230. PMID[24060906].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24160253">Design and Synthesis of P1-P3 Macrocyclic Tertiary Alcohol Comprising HIV-1 Protease Inhibitors.</a> Joshi, A., J.B. Veron, J. Unge, A. Rosenquist, H. Wallberg, B. Samuelsson, A. Hallberg, and M. Larhed. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24160253].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23711095">3-Hydroxyphthalic anhydride-modified Human Serum Albumin as a Microbicide Candidate against HIV Type 1 Entry by Targeting Both Viral Envelope Glycoprotein gp120 and Cellular Receptor CD4.</a> Li, M., J. Duan, J. Qiu, F. Yu, X. Che, S. Jiang, and L. Li. AIDS Research and Human Retroviruses, 2013. 29(11): p. 1455-1464. PMID[23711095].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23072552">Arylazolylthioacetanilide. Part 11: Design, Synthesis and Biological Evaluation of 1,2,4-Triazole thioacetanilide Derivatives as Novel Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Li, Z., Y. Cao, P. Zhan, C. Pannecouque, J. Balzarini, E.D. Clercq, Y. Shen, and X. Liu. Medicinal Chemistry, 2013. 9(7): p. 968-973. PMID[23072552].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23871041">A Sorghum Xylanase Inhibitor-like Protein with Highly Potent Antifungal, Antitumor and HIV-1 Reverse Transcriptase Inhibitory Activities.</a> Lin, P., J.H. Wong, T.B. Ng, V.S. Ho, and L. Xia. Food Chemistry, 2013. 141(3): p. 2916-2922. PMID[23871041].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24156545">90K, an Interferon-stimulated Gene Product, Reduces the Infectivity of HIV-1.</a>Lodermeyer, V., K. Suhr, N. Schrott, C. Kolbe, C.M. Sturzel, D. Krnavek, J. Munch, C. Dietz, T. Waldmann, F. Kirchhoff, and C. Goffinet. Retrovirology, 2013. 10(1): p. 111. PMID[24156545].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24156270">Proteasome Inhibitors Act as Bifunctional Antagonists of Human Immunodeficiency Virus Type 1 Latency and Replication.</a> Miller, L.K., Y. Kobayashi, C.C. Chen, T.A. Russnak, Y. Ron, and J.P. Dougherty. Retrovirology, 2013. 10(1): p. 120. PMID[24156270].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24157474">Corrigendum: Externally Controlled on-demand Release of anti-HIV Drug Using Magneto-electric Nanoparticles as Carriers.</a> Nair, M., R. Guduru, P. Liang, J. Hong, V. Sagar, and S. Khizroev. Nature Communications, 2013. 4: p. 2729. PMID[24157474].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24120088">5,6-Dihydro-5-aza-2&#39;-deoxycytidine Potentiates the anti-HIV-1 Activity of Ribonucleotide Reductase Inhibitors.</a> Rawson, J.M., R.H. Heineman, L.B. Beach, J.L. Martin, E.K. Schnettler, M.J. Dapp, S.E. Patterson, and L.M. Mansky. Bioorganic &amp; Medicinal Chemistry, 2013. 21(22): p. 7222-7228. PMID[24120088].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24091080">Synthesis, Docking, and Biological Studies of Phenanthrene beta-diketo acids as Novel HIV-1 Integrase Inhibitors.</a> Sharma, H., T.W. Sanchez, N. Neamati, M. Detorio, R.F. Schinazi, X. Cheng, and J.K. Buolamwini. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6146-6151. PMID[24091080].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24102161">Clicking 3&#39;-Azidothymidine into Novel Potent Inhibitors of Human Immunodeficiency Virus.</a> Sirivolu, V.R., S.K. Vernekar, T. Ilina, N.S. Myshakina, M.A. Parniak, and Z. Wang. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24102161].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24102412">Minimalist Hybrid Ligand/Receptor-based Pharmacophore Model for CXCR4 Applied to a Small-library of Marine Natural Products Led to the Identification of Phidianidine a as a New CXCR4 Ligand Exhibiting Antagonist Activity.</a>Vitale, R.M., M. Gatti, M. Carbone, F. Barbieri, V. Felicita, M. Gavagnin, T. Florio, and P. Amodeo. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24102412].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23945945">Characterization of Novel Inhibitors of HIV-1 Replication That Function via Alteration of Viral RNA Processing and Rev Function.</a> Wong, R.W., A. Balachandran, M. Haaland, P. Stoilov, and A. Cochrane. Nucleic Acids Research, 2013. 41(20): p. 9471-9483. PMID[23945945].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24156557">New Insights into Inhibition of Human Immunodeficiency Virus Type 1 Replication through Mutant tRNALys3.</a> Wu, C., V.R. Nerurkar, and Y. Lu. Retrovirology, 2013. 10(1): p. 112. PMID[24156557].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24055077">Towards New C6-rigid S-DABO HIV-1 Reverse Transcriptase Inhibitors: Synthesis, Biological Investigation and Molecular Modeling Studies.</a>Wu, H.Q., Z.H. Yan, W.X. Chen, Q.Q. He, F.E. Chen, E. De Clercq, J. Balzarini, D. Daelemans, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6477-6483. PMID[24055077].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24094817">Modular Assembly of Dimeric HIV Fusion Inhibitor Peptides with Enhanced Antiviral Potency.</a> Xiao, J. and T.J. Tolbert. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6046-6051. PMID[24094817].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24084160">Design and Synthesis of Novel Pyrimidone Analogues as HIV-1 Integrase Inhibitors.</a>Yu, S., T.W. Sanchez, Y. Liu, Y. Yin, N. Neamati, and G. Zhao. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6134-6137. PMID[24084160].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1025-110713.</p>

    <br />

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324868700008">Lamivudine-pegylated Chitosan: A Novel Effective Nanosized Antiretroviral Agent.</a> Aghasadeghi, M.R., H. Heidari, S.M. Sadat, S. Irani, S. Amini, S.D. Siadat, M.E. Fazlhashemy, R. Zabihollahi, S.M. Atyabi, S.B. Momen, M.S. Khosraavy, M. Davari, T. Darvishmohammadi, M. Doroudian, and M.S. Ardestani. Current HIV Research, 2013. 11(4): p. 309-320. ISI[000324868700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317957100017">Triflic acid Mediated Fries Rearrangement of 3-Dienyl-2-azetidinones: Facile Synthesis of 3-(But-2-enylidene)quinolin-4(3H)-ones.</a> Anand, A., V. Mehra, and V. Kumar. Synlett, 2013. 24(7): p. 865-867. ISI[000317957100017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325076400042">Identification of Novel Inhibitors of HIV-1 Integrase Using Pharmacophore-based Virtual Screening Combined with Molecular Docking Strategies.</a> Ardakani, A. and J.B. Ghasemi. Medicinal Chemistry Research, 2013. 22(11): p. 5545-5556. ISI[000325076400042].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324915100004">Biological Properties of Water-soluble Phosphorhydrazone Dendrimers.</a>Caminade, A.M., C.O. Turrin, and J.P. Majoral. Brazilian Journal of Pharmaceutical Sciences, 2013. 49: p. 33-44. ISI[000324915100004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324777300004">Inhibition of HIV-1 Reverse Transcriptase-catalyzed Synthesis by Intercalated DNA Benzo[a]pyrene 7,8-dihydrodiol-9,10-epoxide Adducts.</a> Chary, P., W.A. Beard, S.H. Wilson, and S. Lloyd. PloS One, 2013. 8(9): p. e72131. ISI[000324777300004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324480300019">Chimeric Cyanovirin-MPER Recombinantly Engineered Proteins Cause Cell-free Virolysis of HIV-1.</a> Contarino, M., A.R. Bastian, R.V.K. Sundaram, K. McFadden, C. Duffy, V. Gangupomu, M. Baker, C. Abrams, and I. Chaiken. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4743-4750. ISI[000324480300019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325119600019">A Three Step Continuous Flow Synthesis of the Biaryl Unit of the HIV Protease Inhibitor Atazanavir.</a> Dalla-Vechia, L., B. Reichart, T. Glasnov, L.S.M. Miranda, C.O. Kappe, and R. de Souza. Organic &amp; Biomolecular Chemistry, 2013. 11(39): p. 6806-6813. ISI[000325119600019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325237000027">Production of Oleanolic acid glycosides by Hairy Root Established Cultures of Calendula officinalis L.</a> Dlugosz, M., E. Wiktorowska, A. Wisniewska, and C. Paczkowski. Acta Biochimica Polonica, 2013. 60(3): p. 467-473. ISI[000325237000027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325115200018">Synthesis of New Imidazo[4,5-e][1,3]thiazolo-[3,2-b][1,2,4]triazine Derivatives.</a>Gazieva, G.A., S.A. Serkov, N.V. Sigai, N.N. Kostikova, Y.V. Nelyubina, E.A. Shishkova, and A.N. Kravchenko. Chemistry of Heterocyclic Compounds, 2013. 49(7): p. 1097-1101. ISI[000325115200018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324695900105">In Vitro and ex Vivo Evaluations on Transdermal Delivery of the HIV Inhibitor IQP-0410.</a> Ham, A.S., W. Lustig, L. Yang, A. Boczar, K.W. Buckheit, and R.W. Buckheit. PloS One, 2013. 8(9): p. e75306. ISI[000324695900105].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325395600061">HIV-1 Suppression and Durable Control by Combining Single Broadly Neutralizing Antibodies and Antiretroviral Drugs in Humanized Mice.</a>Horwitz, J.A., A. Halper-Stromberg, H. Mouquet, A.D. Gitlin, A. Tretiakova, T.R. Eisenreich, M. Malbec, S. Gravemann, E. Billerbeck, M. Dorner, H. Buning, O. Schwartz, E. Knops, R. Kaiser, M.S. Seaman, J.M. Wilson, C.M. Rice, A. Ploss, P.J. Bjorkman, F. Klein, and M.C. Nussenzweig. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(41): p. 16538-16543. ISI[000325395600061].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324797000009">Synthesis and NMR Elucidation of Pentacycloundecane-derived Hydroxy acid Peptides as Potential anti-HIV-1 Agents.</a> Karpoormath, R., F. Albericio, T. Govender, G.E.M. Maguire, and H.G. Kruger. Structural Chemistry, 2013. 24(5): p. 1461-1471. ISI[000324797000009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324303602425">Synthesis of Electron-rich Cyclotriazadisulfonamide (CADA) Analogs as anti-HIV and Human CD4 Receptor Down-modulating Agents.</a>Khan, C., N.C. Pflug, D. Schols, K. Vermeire, and T.W. Bell. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000324303602425].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324643100004">Identification of Two HIV Inhibitors That Also Inhibit Human RNaseH2.</a>Kim, J., J. Yoon, M. Ju, Y. Lee, T.H. Kim, J. Kim, P. Sommer, Z. No, J. Cechetto, and S.J. Han. Molecules and Cells, 2013. 36(3): p. 212-218. ISI[000324643100004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325087800049">Flavonoid Compounds from Arundina graminifolia.</a>Li, Y.K., L.Y. Yang, L.D. Shu, Y.Q. Shen, Q.F. Hu, and Z.Y. Xia. Asian Journal of Chemistry, 2013. 25(9): p. 4922-4924. ISI[000325087800049].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323854900031">Anti-HIV-1 Activity and Structure-activity-relationship Study of a Fucosylated glycosaminoglycan from an Echinoderm by Targeting the Conserved CD4 Induced Epitope.</a> Lian, W., M.Y. Wu, N. Huang, N. Gao, C. Xiao, Z. Li, Z.G. Zhang, Y.T. Zheng, W.L. Peng, and J.H. Zhao. Biochimica et Biophysica Acta-General Subjects, 2013. 1830(10): p. 4681-4691. ISI[000323854900031].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324721100026">Biotransformation of Oleanolic and Maslin acids by Rhizomucor miehei.</a>Martinez, A., F. Rivas, A. Perojil, A. Parra, A. Garcia-Granados, and A. Fernandez-Vivas. Phytochemistry, 2013. 94: p. 229-237. ISI[000324721100026].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325164500064">Synthesis and Evaluation of Coumarin Derivatives as Potential Dual-action HIV-1 Protease and Reverse Transcriptase Inhibitors.</a>Olomola, T.O., R. Klein, N. Mautsa, Y. Sayed, and P.T. Kaye. Bioorganic &amp; Medicinal Chemistry, 2013. 21(21): p. 6856-6856. ISI[000325164500064].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325276400011">Pharmacokinetic Interactions between 20(S)-Ginsenoside Rh2 and the HIV Protease Inhibitor Ritonavir in Vitro and in Vivo.</a>Shi, J., B. Cao, W.B. Zha, X.L. Wu, L.S. Liu, W.J. Xiao, R.R. Gu, R.B. Sun, X.Y. Yu, T. Zheng, M.J. Li, X.W. Wang, J. Zhou, Y. Mao, C. Ge, T. Ma, W.J. Xia, J.Y. Aa, G.J. Wang, and C.X. Liu. Acta Pharmacologica Sinica, 2013. 34(10): p. 1349-1358. ISI[000325276400011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325089600117">An Efficient Synthesis of Dichotomine A via Cyclization of L-Tryptophan Derivative.</a> Shi, X.X., D. He, S.B. Li, X. Lei, and H. Yang. Asian Journal of Chemistry, 2013. 25(11): p. 6387-6390. ISI[000325089600117].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324303602426">Towards the Synthesis of Novel 1,3-Azaborines as Potential HIV-1 Protease Inhibitors.</a> Sigurjonsson, K., M.D. Frank, J.D. Schreiber, J.J. Jennings, A.L. Faulkner, and L. Fabry-Asztalos. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000324303602426].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325272100060">Phenyl Hydrazone Bearing Pyrazole and Pyrimidine Scaffolds: Design and Discovery of a Novel Class of Non-Nucleoside Reverse Transcriptase Inhibitors (NNTRIs) against HIV-1 and Their Antibacterial Properties.</a>Singh, U.P., H.R. Bhat, A. Verma, M.K. Kumawat, R. Kaur, S.K. Gupta, and R.K. Singh. RSC Advances, 2013. 3(38): p. 17335-17348. ISI[000325272100060].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324928100009">Design, Synthesis and Biological Evaluation of TAR and cTAR Binders as HIV-1 Nucleocapsid Inhibitors.</a> Sosic, A., F. Frecentese, E. Perissutti, L. Sinigaglia, V. Santagada, G. Caliendo, E. Magli, A. Ciano, G. Zagotto, C. Parolin, and B. Gatto. MedChemComm, 2013. 4(10): p. 1388-1393. ISI[000324928100009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">47. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324303602421">Discovery of Novel HIV Integrase Inhibitors Part 1: Molecular Design and SAR of Azabicyclic carbamoyl pyridone Inhibitors.</a>Taoda, Y., B.A. Johns, T. Akiyama, H. Yoshida, T. Taishi, T. Kawasuji, H. Murai, T. Yoshinaga, A. Sato, and T. Fujiwara. Abstracts of Papers of the American Chemical Society, 2013. 245. ISI[000324303602421].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">48. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324965300010">Is the Conformational Flexibility of Piperazine Derivatives Important to Inhibit HIV-1 Replication?</a> Teixeira, C., N. Serradji, S. Amroune, K. Storck, C. Rogez-Kreuz, P. Clayette, F. Barbault, and F. Maurel. Journal of Molecular Graphics &amp; Modelling, 2013. 44: p. 91-103. ISI[000324965300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">49. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324921500004">Design, Synthesis, and Biological Evaluation of Novel 3,5-Disubstituted-1,2,6-thiadiazine-1,1-dione Derivatives as HIV-1 NNRTIs.</a>Tian, Y., D. Rai, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, H.Q. Liu, and X.Y. Liu. Chemical Biology &amp; Drug Design, 2013. 82(4): p. 384-393. ISI[000324921500004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">50. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324977900001">Synthesis and Biological Evaluation of Acyclic Phosphonic acid Nucleoside Derivatives.</a> Wainwright, P., A. Maddaford, X.R. Zhang, H. Billington, D. Leese, R. Glen, D.C. Pryde, D.S. Middleton, P.T. Stephenson, and S. Sutton. Nucleosides Nucleotides &amp; Nucleic Acids, 2013. 32(9): p. 477-492. ISI[000324977900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">51. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324910200009">The Antiviral Lectin Cyanovirin-N: Probing Multivalency and Glycan Recognition through Experimental and Computational Approaches.</a>Woodrum, B.W., J.D. Maxwell, A. Bolia, S.B. Ozkan, and G. Ghirlanda. Biochemical Society Transactions, 2013. 41: p. 1170-1176. ISI[000324910200009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">52. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325089600059">A New 4-Hydroxyisoflavanone from the Root of Oriental Tobacco and Their Antivirus Activities.</a> Wu, X.X., Y. Xu, H.Q. Leng, G.Y. Yang, Y.K. Chen, Q.F. Hu, and M.M. Miao. Asian Journal of Chemistry, 2013. 25(11): p. 6133-6135. ISI[000325089600059].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">53. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324480300045">DNA Duplexes with Hydrophobic Modifications Inhibit Fusion between HIV-1 and Cell Membranes.</a> Xu, L., L.F. Cai, X.L. Chen, X.F. Jiang, H.H. Chong, B.H. Zheng, K. Wang, J.L. He, W. Chen, T. Zhang, M.S. Cheng, Y.X. He, and K.L. Liu. Antimicrobial Agents and Chemotherapy, 2013. 57(10): p. 4963-4970. ISI[000324480300045].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1025-110713.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">54. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130926&amp;CC=WO&amp;NR=2013139727A1&amp;KC=A1">Triazines with Suitable Spacers for Treatment and/or Prevention of HIV Infections and Their Preparation.</a> Heeres, J., P. Van Der Veken, J. Joossens, V. Muthusamy, K. Augustyns, K.K.F. Arieen, G.L.E. Vanham, and P. Lewi. Patent. 2013. 2013-EP55525 2013139727: 40pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">55. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130926&amp;CC=US&amp;NR=2013252909A1&amp;KC=A1">Tannin Inhibitors of HIV.</a> Kraus, G.A. and W. Maury. Patent. 2013. 2013-13788691 20130252909: 28pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">56. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131003&amp;CC=WO&amp;NR=2013148067A1&amp;KC=A1">Preparation of 3,28-Disubstituted betulinic acid Derivatives as anti-HIV Agents.</a> Lee, K.-H., K. Qian, D. Yu, I.D. Bori, C.-H. Chen, and L. Huang. Patent. 2013. 2013-US28668 2013148067: 46pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">57. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131010&amp;CC=WO&amp;NR=2013150532A1&amp;KC=A1">Lipopeptide Conjugates Comprising Sphingolipid and HIV gp41 Derived Peptides for Treatment of Retrovirus Infections.</a> Shai, Y. and A. Ashkenazi. Patent. 2013. 2013-IL50305 2013150532: 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">58. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131024&amp;CC=WO&amp;NR=2013157622A1&amp;KC=A1">HIV Replication Inhibitor.</a> Sugiyama, S., T. Akiyama, and Y. Taoda. Patent. 2013. 2013-JP61575 2013157622: 181pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">59. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130919&amp;CC=WO&amp;NR=2013138436A1&amp;KC=A1">Cyclic Hydrazine Derivatives as HIV Attachment Inhibitors.</a> Wang, T., J.F. Kadow, N.A. Meanwell, and L.G. Hamann. Patent. 2013. 2013-US30772 2013138436: 93pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>

    <br />

    <p class="plaintext">60. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130911&amp;CC=CN&amp;NR=103288747A&amp;KC=A">6-Cyclohexylmethyl-2-aromatic Ring Carbonyl-substituted N-Dabo-Like Compounds, Their Synthetic Method and Application as anti-HIV Agents.</a> Zhang, S., X. Wang, X. Fang, Y. Li, X. Li, and M. Xu. Patent. 2013. 2013-10235303 103288747: 14pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1025-110713.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
