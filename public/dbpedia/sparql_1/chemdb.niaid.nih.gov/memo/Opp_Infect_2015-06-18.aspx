

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-06-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="G3JR2yp2ZqbMHhzLSmxZPZTxgPYVOl1CvsUhyGN7YvLZvFlkX2+gvYNV+i/AuEeM2IHk3eP98bY7WU0k0l4XXlCxvXRkyXJOEotUQ0zJCslnnKh6Yy5Phrr4mJg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8BA4C6B0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: June 5 - June 18, 2015</h1>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18457948">Synthesis and in Vitro Antimycobacterial Activity of B-ring Modified Diaryl ether InhA Inhibitors.</a> am Ende, C.W., S.E. Knudson, N. Liu, J. Childs, T.J. Sullivan, M. Boyne, H. Xu, Y. Gegina, D.L. Knudson, F. Johnson, C.A. Peloquin, R.A. Slayden, and P.J. Tonge. Bioorganic &amp; Medicinal Chemistry Letters, 2008. 18(10): p. 3029-3033. PMID[18457948].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25915781">First Crystal Structures of Mycobacterium Tuberculosis 6-Oxopurine phosphoribosyltransferase: Complexes with GMP and Pyrophosphate and with Acyclic Nucleoside Phosphonates Whose Prodrugs Have Antituberculosis Activity.</a> Eng, W.S., D. Hockova, P. Spacek, Z. Janeba, N.P. West, K. Woods, L.M. Naesens, D.T. Keough, and L.W. Guddat. Journal of Medicinal Chemistry, 2015. 58(11): p. 4822-4838. PMID[25915781].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19130456">Triclosan Derivatives: Towards Potent Inhibitors of Drug-sensitive and Drug-resistant Mycobacterium tuberculosis.</a> Freundlich, J.S., F. Wang, C. Vilcheze, G. Gulten, R. Langley, G.A. Schiehser, D.P. Jacobus, W.R.J. Jr., and J.C. Sacchettini. ChemMedChem, 2009. 4(2)(1860-7187): p. 241-248. PMID[19130456].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25977095">Synthesis, Antimycobacterial Evaluation and Pharmacophore Modeling of Analogues of the Natural Product Formononetin.</a> Mutai, P., E. Pavadai, I. Wiid, A. Ngwane, B. Baker, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(12): p. 2510-2513. PMID[25977095].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26078037">Structure-based Design of Diverse Inhibitors of Mycobacterium tuberculosis N-Acetylglucosamine-1-phosphate uridyltransferase: Combined Molecular Docking, Dynamic Simulation, and Biological Activity.</a> Soni, V., P. Suryadevara, D. Sriram, S. Kumar, V.K. Nandicoori, and P. Yogeeswari. Journal of Molecular Modeling, 2015. 21(7): p. 2704. PMID[26078037].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17163639">High Affinity InhA Inhibitors with Activity against Drug-resistant Strains of Mycobacterium Tuberculosis.</a> Sullivan, T.J., J.J. Truglio, M.E. Boyne, P. Novichenok, X. Zhang, C.F. Stratton, H.J. Li, T. Kaur, A. Amin, F. Johnson, R.A. Slayden, C. Kisker, and P.J. Tonge. ACS Chemical Biology, 2006. 1(1): p. 43-53. PMID[17163639].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25982331">Synthesis, and Structure-activity Relationship for C(4) and/or C(5) Thienyl Substituted Pyrimidines, as a New Family of Antimycobacterial Compounds.</a> Verbitskiy, E.V., E.M. Cheprakova, P.A. Slepukhin, M.A. Kravchenko, S.N. Skornyakov, G.L. Rusinov, O.N. Chupakhin, and V.N. Charushin. European Journal of Medicinal Chemistry, 2015. 97: p. 225-234. PMID[25982331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0605-061815.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000354480700119">Diterpenes Synthesized from the Natural Serrulatane Leubethanol and their in Vitro Activities against Mycobacterium tuberculosis.</a> Escarcena, R., J. Perez-Meseguer, E. del Olmo, B. Alanis-Garza, E. Garza-Gonzalez, R. Salazar-Aranda, and N.W. de Torres. Molecules, 2015. 20(4): p. 7245-7262. ISI[000354480700119].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0605-061815.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
