

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-349.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RBhOcuVt/VzQBbB4TEj91FZoc7c7RDK/arvZw8tEAqR/z4ZLZ8dlM8B+H/F9Np73HUm+LU0kU43hDPSuQWcu/B8KjFxQAVzzYZnxGfckRSgsshu6HbPFJbLbB3k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1C416536" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-349-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48916   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          3-O-(3&#39;,3&#39;-Dimethysuccinyl) Betulinic Acid Inhibits Maturation of the Human Immunodeficiency Virus Type 1 Gag Precursor Assembled In Vitro</p>

    <p>          Sakalian, M, McMurtrey, CP, Deeg, FJ, Maloy, CW, Li, F, Wild, CT, and Salzwedel, K</p>

    <p>          J Virol <b>2006</b>.  80(12): 5716-5722</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731910&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16731910&amp;dopt=abstract</a> </p><br />

    <p>2.     48917   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Inhibition of highly productive HIV-1 infection in T cells, primary human macrophages, microglia, and astrocytes by Sargassum fusiforme</p>

    <p>          Paskaleva, EE, Lin, X, Li, W, Cotter, R, Klein, MT, Roberge, E, Yu, EK, Clark, BW, Veille, JC, Liu, Y, Lee, DY, and Canki, M</p>

    <p>          AIDS Res Ther <b>2006</b>.  3(1): 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16725040&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16725040&amp;dopt=abstract</a> </p><br />

    <p>3.     48918   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          TSAO Derivatives, Inhibitors of HIV-1 Reverse Transcriptase Dimerization: Recent Progress</p>

    <p>          Camarasa, MJ, Velazquez, S, San-Felix, A, Perez-Perez, MJ, Bonache, MC, and De, Castro S</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(15): 1895-907</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724955&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16724955&amp;dopt=abstract</a> </p><br />

    <p>4.     48919   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Parapoxviruses in combination with other antiviral agents for the treatment of HIV/AIDS</p>

    <p>          Paulsen, Daniela, Ruebsamen-Waigmann, Helga, Kureishi, Amar, Hunsmann, Gerhard, Stahl-Hennig, Christiane, Meyerhans, Andreas, Schuetz, Alexandra, and Weber, Olaf</p>

    <p>          PATENT:  WO <b>2006005529</b>  ISSUE DATE:  20060119</p>

    <p>          APPLICATION: 2005  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48920   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Piperazine derivatives useful as ccr5 antagonists</p>

    <p>          Ramanathan, Ragulan, Ghosal, Anima, Miller, Michael W, Chowdhury, Swapan K, and Alton, Kevin B </p>

    <p>          PATENT:  US <b>2006105964</b>  ISSUE DATE:  20060518</p>

    <p>          APPLICATION: 2005-59035  PP: 78 pp., Cont.-in-part of U.S. Ser. No. 668,862.</p>

    <p>          ASSIGNEE:  (Schering Corporation)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     48921   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Novel multi-component nanopharmaceuticals derived from poly(ethylene) glycol, retro-inverso-tat nonapeptide and saquinavir demonstrate combined anti-HIV effects</p>

    <p>          Wan, Li, Zhang, Xiaoping, Gunaseelan, Simi, Pooyan, Shahriar, Debrah, Olivia, Leibowitz, Michael J, Rabson, Arnold B, Stein, Stanley, and Sinko, Patrick J</p>

    <p>          AIDS Research and Therapy <b>2006</b>.  3: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     48922   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Novel pyridine-based metal chelators as antiviral agents</p>

    <p>          Rajagopalan, Raghavan and Babu, John Sam</p>

    <p>          PATENT:  US <b>2006106070</b>  ISSUE DATE:  20060518</p>

    <p>          APPLICATION: 26  PP: 16 pp.</p>

    <p>          ASSIGNEE:  (Bioflexis, LLC USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     48923   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of novel tricyclic HIV-1 integrase inhibitors by modification of its pyridine ring</p>

    <p>          Metobo, SE, Jin, H, Tsiang, M, and Kim, CU</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723226&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723226&amp;dopt=abstract</a> </p><br />

    <p>9.     48924   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and SAR studies of novel and highly active tri-cyclic HIV integrase inhibitors</p>

    <p>          Jin, H, Cai, RZ, Schacherer, L, Jabri, S, Tsiang, M, Fardis, M, Chen, X, Chen, JM, and Kim, CU </p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723225&amp;dopt=abstract</a> </p><br />

    <p>10.   48925   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Effect of substitution on novel tricyclic HIV-1 integrase inhibitors</p>

    <p>          Fardis, M, Jin, H, Jabri, S, Cai, RZ, Mish, M, Tsiang, M, and Kim, CU</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16716589&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16716589&amp;dopt=abstract</a> </p><br />

    <p>11.   48926   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Novel diaminopropane-based metal chelators as antiviral agents</p>

    <p>          Rajagopalan, Raghavan and Babu, John Sam</p>

    <p>          PATENT:  US <b>2006100282</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005-63165  PP: 16 pp.</p>

    <p>          ASSIGNEE:  (Bioflexis, LLC USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   48927   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Branched peptide possessing anti-HIV activity, preparation method, and pharmaceutical composition</p>

    <p>          Fenouillet, Emmanuel and Barbouche, Rym</p>

    <p>          PATENT:  FR <b>2877344</b>  ISSUE DATE: 20060505</p>

    <p>          APPLICATION: 2004-11727  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Centre National De La Recherche Scientifique, Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48928   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Anti-HIV activity of stilbene-related heterocyclic compounds</p>

    <p>          Bedoya, LM, Del, Olmo E, Sancho, R, Barboza, B, Beltran, M, Garcia-Cadenas, AE, Sanchez-Palomino, S, Lopez-Perez, JL, Munoz, E, Feliciano, AS, and Alcami, J</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713260&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713260&amp;dopt=abstract</a> </p><br />

    <p>14.   48929   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          Inhibitors of HIV-1 Tat-Mediated Transactivation</p>

    <p>          Richter, SN and Palu, G</p>

    <p>          Curr Med Chem <b>2006</b>.  13(11): 1305-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16712471&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16712471&amp;dopt=abstract</a> </p><br />

    <p>15.   48930   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Preparation of HIV inhibiting 5-carbo- or heterocyclic substituted pyrimidines</p>

    <p>          Guillemont, Jerome Emile Georges, Heeres, Jan, and Lewi, Paulus Joannes</p>

    <p>          PATENT:  WO <b>2006035068</b>  ISSUE DATE:  20060406</p>

    <p>          APPLICATION: 2005  PP: 74 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48931   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          HIV-1 integrase inhibition of biscoumarin analogues</p>

    <p>          Su, CX, Mouscadet, JF, Chiang, CC, Tsai, HJ, and Hsu, LY</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(5): 682-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16651766&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16651766&amp;dopt=abstract</a> </p><br />

    <p>17.   48932   HIV-LS-349; PUBMED-HIV-5/30/2006</p>

    <p class="memofmt1-2">          HIV-1 integrase inhibition by pyrrole/imidazole-containing polyamides</p>

    <p>          Zakharova, OD, Baranova, S, Parissi, V, Ryabinin, VA, Sinyakov, AN, Litvak, S, Litvak, LT, and Nevinsky, GA</p>

    <p>          J Pept Res <b>2005</b>.  66 Suppl 1: 138-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650070&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650070&amp;dopt=abstract</a> </p><br />

    <p>18.   48933   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p><b>          Synthesis and antiviral activity of L-3&#39;-fluoro-2&#39;, 3&#39;-unsaturated carbocyclic nucleosides</b> </p>

    <p>          Wang, Jianing, Jin, Yunho, Schinazi, Raymond F, and Chu, CK</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-365</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48934   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          MIA-QSAR modelling of anti-HIV-1 activities of some 2-amino-6-arylsulfonylbenzonitriles and their thio and sulfinyl congeners</p>

    <p>          Freitas, Matheus P</p>

    <p>          Organic &amp; Biomolecular Chemistry <b>2006</b>.  4(6): 1154-1159</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   48935   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Structure-based design, synthesis, and biological evaluation of novel inhibitors of human cyclophilin A</p>

    <p>          Guichou, Jean-Francois, Viaud, Julien, Mettling, Clement, Subra, Guy, Lin, Yea-Lih, and Chavanieu, Alain</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(3): 900-910</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   48936   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          The isolation of rubrifloralignan A and its anti-HIV-1 activities</p>

    <p>          Tian, Ren-Rong, Xiao, Wei-Lie, Yang, Liu-Meng, Wang, Rui-Rui, Sun, Han-Dong, Liu, Nai-Fa, and Zheng, Yong-Tang</p>

    <p>          Zhongguo Tianran Yaowu <b>2006</b>.  4(1): 40-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   48937   HIV-LS-349; WOS-HIV-5/21/2006</p>

    <p class="memofmt1-2">          Vanillic acid glycoside and quinic acid derivatives from Gardeniae Fructus</p>

    <p>          Kim, HJ, Kim, EJ, Seo, SH, Shin, CG, Jin, C, and Lee, YS</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(4): 600-603, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237343400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237343400016</a> </p><br />

    <p>23.   48938   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          Ultra-potent P1 modified arylsulfonamide HIV protease inhibitors: The discovery of GW0385</p>

    <p>          Miller, John F, Andrews, CWebster, Brieger, Michael, Furfine, Eric S, Hale, Michael R, Hanlon, Mary H, Hazen, Richard J, Kaldor, Istvan, McLean, Ed W, Reynolds, David, Sammond, Douglas M, Spaltenstein, Andrew, Tung, Roger, Turner, Elizabeth M, Xu, Robert X, and Sherrill, Ronald G</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(7): 1788-1794</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   48939   HIV-LS-349; SCIFINDER-HIV-5/22/2006</p>

    <p class="memofmt1-2">          HIV Integrase Inhibitors with Nucleobase Scaffolds: Discovery of a Highly Potent Anti-HIV Agent</p>

    <p>          Nair, Vasu, Chi, Guochen, Ptak, Roger, and Neamati, Nouri</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(2): 445-447</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
