

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-01-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d8Qql8G9wa1EZ7Zcuy3FqPFOlOAVP/4r4IPxYHs7crvFUlCDbZQYJZXMp7Uk6dpIDL0x4JqG8gEcUFDACF6zcAoJk/UT75zMfkVvLDqhkaaM3YrBEjj+lFhcrF8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0CF4835F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 19 - January 1, 2015</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25407510">Computational Prediction of Anti HIV-1 Peptides and in Vitro Evaluation of Anti HIV-1 Activity of HIV-1 p24-Derived Peptides.</a> Poorinmohammad, N., H. Mohabatkar, M. Behbahani, and D. Biria. Journal of Peptide Science, 2015. 21(1): p. 10-16. PMID[25407510].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25209965">Nucleoside Mono- and Diphosphate Prodrugs of 2&#39;,3&#39;-Dideoxyuridine and 2&#39;,3&#39;-dideoxy-2&#39;,3&#39;-didehydrouridine.</a> Pertenbreiter, F., J. Balzarini, and C. Meier. ChemMedChem, 2015. 10(1): p. 94-106. PMID[25209965].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25349048">Novel Urushiols with Human Immunodeficiency Virus Type 1 Reverse Transcriptase Inhibitory Activity from the Leaves of Rhus verniciflua.</a> Kadokura, K., K. Suruga, T. Tomita, W. Hiruma, M. Yamada, A. Kobayashi, A. Takatsuki, T. Nishio, T. Oku, and Y. Sekino. Journal of Natural Medicines, 2015. 69(1): p. 148-153. PMID[25349048].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25385110">In Vitro Resistance Selection with Doravirine (MK-1439), a Novel Nonnucleoside Reverse Transcriptase Inhibitor with Distinct Mutation Development Pathways.</a> Feng, M., D. Wang, J.A. Grobler, D.J. Hazuda, M.D. Miller, and M.T. Lai. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 590-598. PMID[25385110].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25468035">Synthesis and Biological Evaluation of Pyridinone Analogues as Novel Potent HIV-1 NNRTIs.</a> Cao, Y., Y. Zhang, S. Wu, Q. Yang, X. Sun, J. Zhao, F. Pei, Y. Guo, C. Tian, Z. Zhang, H. Wang, L. Ma, J. Liu, and X. Wang. Bioorganic &amp; Medicinal Chemistry, 2015. 23(1): p. 149-159. PMID[25468035].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25489108">Single-chain Protein Mimetics of the N-Terminal Heptad-repeat Region of gp41 with Potential as anti-HIV-1 Drugs</a>. Crespillo, S., A. Camara-Artigas, S. Casares, B. Morel, E.S. Cobos, P.L. Mateo, N. Mouz, C.E. Martin, M.G. Roger, R. El Habib, B. Su, C. Moog, and F. Conejero-Lara. Proceedings of the NationalAcademy ofSciences, 2014. 111(51): p. 18207-18212. PMID[25489108].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25495797">Diterpenes from a Chinese Collection of the Brown Alga Dictyota plectens.</a> Cheng, S., M. Zhao, Z. Sun, W. Yuan, S. Zhang, Z. Xiang, Y. Cai, J. Dong, K. Huang, and P. Yan. Journal of Natural Products, 2014. 77(12): p. 2685-2693. PMID[25495797].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1219-010115.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344869800017">Anti-HIV Activity of Fucoidans from Three Brown Seaweed Species.</a> Thuy, T.T.T., B.M. Ly, T.T.T. Van, N.V. Quang, H.C. Tu, Y. Zheng, C. Seguin-Devaux, B.L. Mi, and U. Ai. Carbohydrate Polymers, 2015. 115: p. 122-128. ISI[000344869800017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345586900003">Design of a Highly Potent HIV-1 Fusion Inhibitor Targeting the gp41 Pocket.</a> Chong, H.H., Z.L. Qiu, Y. Su, L.L. Yang, and Y.X. He. AIDS, 2015. 29(1): p. 13-21. ISI[000345586900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345550400213.">In Vitro anti-HIV and -TB Activities of Annona muricata and Artemisia afra Extracts.</a> van, d.V.M., M. Pruissen, T. Koekemoer, A. Sowemimo, and S. Govender. Planta Medica, 2014. 80(16): p. 1420-1420. ISI[000345550400213].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">11. Anti<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=00345550400443">-HIV Activity of Flavonoids from Croton sphaerogynus Baill.</a> Santos, K., L. Motta, and C. Furlan. Planta Medica, 2014. 80(16): p. 1495-1495. ISI[00345550400443].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345706500001">Human Semen Contains Exosomes with Potent anti-HIV-1 Activity.</a> Madison, M.N., R.J. Roller, and C.M. Okeoma. Retrovirology, 2014. 11(102): 15pp. ISI[000345706500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345746000001">Antiviral Activity of Compounds Isolated from Marine Sponges.</a> Gomez-Archila, L.G., M.T. Rugeles, and W. Zapata. Revista de Biologia Marina yOceanografia, 2014. 49(3): p. 401-412. ISI[000345746000001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345545600022">Synthesis and Biological Evaluation of Some Pentacyclic Lupane Triterpenoid Esters.</a> Pinzaru, I., C. Trandafirescu, Z. Szabadai, M. Mioc, I. Ledeti, D. Coricovac, S. Ciurlea, R.M. Ghiulai, Z. Crainiceanu, and G. Simu. Revista de Chimie, 2014. 65(7): p. 848-851. ISI[000345545600022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345550400627">Anti-HIV Activity of Extracts from Hyptis radicans (Lamiaceae).</a> Partida, M.D.S., R. Lombello, and C.M. Furlan. Planta Medica, 2014. 80(16): p. 1551-1551. ISI[000345550400627].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345639600002">Bioactive Carbocyclic Nucleoside Analogues - Syntheses and Properties of Entecavir.</a> Campian, M., M. Putala, and R. Sebesta. Current Organic Chemistry, 2014. 18(22): p. 2808-2832. ISI[000345639600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000346406400001">Design, Synthesis, Antiviral Activity, and Pre-formulation Development of Poly-L-arginine-fatty acyl Derivatives of Nucleoside Reverse Transcriptase Inhibitors.</a> Pemmaraju, B.P., S. Malekar, H.K. Agarwal, R.K. Tiwari, D. Oh, G.F. Doncel, D.R. Worthen, and K. Parang. Nucleosides Nucleotides &amp; Nucleic Acids, 2015. 34(1): p. 1-15. ISI[000346406400001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345581300006">Fragment Based Search for Small Molecule Inhibitors of HIV-1 Tat-Tar.</a> Zeiger, M., S. Stark, E. Kalden, B. Ackermann, J. Ferner, U. Scheffer, F. Shoja-Bazargani, V. Erdel, H. Schwalbe, and M.W. Gobel. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(24): p. 5576-5580. ISI[000345581300006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345950800019">Synthesis of Cinnamate ester-AZT Conjugates as Potential Dual-action HIV-1 Integrase and Reverse Transcriptase Inhibitors.</a> Olomola, T.O., R. Klein, and P.T. Kaye. Tetrahedron, 2014. 70(49): p. 9449-9455. ISI[000345950800019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345551000006">Structure-Activity Relationships of a Novel Capsid Targeted Inhibitor of HIV-1 Replication.</a> Kortagere, S., J.P. Xu, M.K. Mankowski, R.G. Ptak, and S. Cocklin. Journal of Chemical Information and Modeling, 2014. 54(11): p. 3080-3090. ISI[000345551000006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345581300049">Design and Synthesis of Substituted Morpholin/piperidin-1-ylcarbamodithioates as Promising Vaginal Microbicides with Spermicidal Potential.</a> Bala, V., S. Jangir, V. Kumar, D. Mandalapu, S. Gupta, L. Kumar, B. Kushwaha, Y.S. Chhonker, A. Krishna, J.P. Maikhuri, P.K. Shukla, R.S. Bhatta, G. Gupta, and V.L. Sharma. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(24): p. 5782-5786. ISI[000345581300049].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345779400027">Synthesis of Substituted Isoindolo[2,1-a]quinoxalin-6-yl-amino and 6-Imino-5-yl thiourea Derivatives.</a> Parrino, B., C. Ciancimino, C. Sarwade, V. Spano, A. Montalbano, P. Barraja, G. Cirrincione, P. Diana, and A. Carbone. Arkivoc, 2014: p. 384-398. ISI[000345779400027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1219-010115.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
