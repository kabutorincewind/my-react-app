

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-05-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="epGXpZYqQYK5g45vamPriQZlxUHXZIfQkqGxVTmZZsZQLDf8yVBUgMjLRDAjGr/9SUNL4+2xq73uVxc2uG7r9B0HwFriJj7DHumtttDcA3cN2QJBu3IulAQwEI4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0920CE89" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">May 14 - May 27, 2010</p> 

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20486856">The Hepatitis C Virus (HCV) NS4B RNA Binding Inhibitor Clemizole Is Highly Synergistic with HCV Protease Inhibitors.</a></p>

    <p class="NoSpacing">Einav S, Sobol HD, Gehrig E, Glenn JS.</p>

    <p class="NoSpacing">J Infect Dis. 2010 May 20. <b>[Epub ahead of print];</b> PMID[20486856].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0514-052710.</p> 
    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20483607">S-Aryltriazole acyclonucleosides: Synthesis and biological evaluation against hepatitis C virus.</a></p>

    <p class="NoSpacing">Liu Y, Xia Y, Li W, Cong M, Maggiani A, Leyssen P, Qu F, Neyts J, Peng L.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Apr 28. <b>[Epub ahead of print]</b>; PMID[20483607].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0514-052710.</p>

    <p /> 

    <p class="memofmt2-2">Epstein Barr Virus</p> 

    <p class="NoSpacing">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20491082">Chemopreventive properties of phytosterols and maslinic acid extracted from Coleus tuberosus in inhibiting the expression of EBV early-antigen in Raji cells.</a></p>

    <p class="NoSpacing">Mooi LY, Wahab NA, Lajis NH, Ali AM.</p>

    <p class="NoSpacing">Chem Biodivers. 2010 May;7(5):1267-75; PMID[20491082].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0514-052710.</p> 

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://apps.isiknowledge.com/InboundService.do?Func=Frame&amp;product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001365&amp;SID=3F7pOI44KE5GpIPLGK1&amp;Init=Yes&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Discovery of a Novel, Potent Flavonoid Inhibitor of HCV RNA-Dependent RNA Polymerase.</a> Ahmed-Belkacem, A., R. Brillet, C. Pallier, C. Wychowski, J.F. Guichou, and J.M. Pawlotsky. Journal of Hepatology. 52: p. S288-S288; ISI[000277018001365].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001366&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Inhibition of HCV NS3 Serine Protease by the Flavanoid Quercetin.</a> Bachmetov, L., R. Zemel, M. Gal-Tanamy, M. Vorobeychik, I. Benhar, and R. Tur-Kaspa. Journal of Hepatology. 52: p. S288-S288; ISI[000277018001366].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001367&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Fluvastatin Provides Syngergistic Anti-HCV Activity When Combined with a Polymerase Inhibito. 2-CMA, In Vitro.</a> Bader, T. and Z. Zhang. Journal of Hepatology. 52: p. S288-S289; ISI[000277018001367].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018000040&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">MK-5172: A Novel HCV NS3/4A Protease Inhibitor with Potent Activity Against Known Resistance Mutants.</a> Carroll, S., J. McCauley, S. Ludmerer, S. Harper, V. Summa, M. Rowley, M. Rudd, P. Coleman, N. Liverton, J. Butcher, C. McIntyre, J. Romano, K. Bush, M. Ferrara, B. Crescenzi, A. Petrocchi, M. Difilippo, C. Burlein, J. Dimuzio, D. Graham, C. McHale, M. Stahlhut, A. Gates, C. Fandozzi, N. Trainor, D. Hazuda, J. Vacca, and D. Olsen. Journal of Hepatology. 52: p. S17-S17; ISI[000277018000040].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001370&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">In Vivo and In Vitro Resistance to Silibinin, a Component of Silymarin and Potent Inhibitor of HCV Replication.</a> Chevaliez, S., A. Ahmed-Belkacem, A. Soulier, K. Zinober, T.M. Scherzer, R. Brillet, P. Ferenci, and J.M. Pawlotsky. Journal of Hepatology. 52: p. S289-S290; ISI[000277018001370].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277264100002&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Identification of a Class of HCV Inhibitors Directed Against the Nonstructural Protein NS4B.</a> Cho, N.J., H. Dvory-Sobol, C. Lee, S.J. Cho, P. Bryson, M. Masek, M. Elazar, C.W. Frank, and J.S. Glenn. Science Translational Medicine. 2(15); ISI[000277264100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018000113&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Discovery of Potent and Selective Inhibitors of Hepatitis C Virus (HCV) Entry with Distinct Mechanisms of Action.</a> Coburn, G.A., A.Q. Han, J.D. Murga, J.M. de Muys, D.N. Fisch, D. Paul, S. Moorji, K.P. Provoncha, Y. Rotshteyn, D. Qian, P.J. Maddon, and W.C. Olson. Journal of Hepatology. 52: p. S50-S50; ISI[000277018000113].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018000034&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Identification and Characterization of PPL-461, a Potent and Selective HCV NS5A Inhibitor with Activity Against All HCV Genotypes.</a> Colonno, R., E. Peng, M. Bencsik, N. Huang, M. Zhong, A. Huq, Q. Huang, J. Williams, and L. Li. Journal of Hepatology. 52: p. S14-S15; ISI[000277018000034].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018000039&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Ritonavir Boosting of Low Dose RG7227/ITMN-191, HCV NS3/4A Protease Inhibitor, Results in Robust Reduction in HCV RNA at Lower Exposures than Provided by Unboosted Regimens.</a> Gane, E., R. Rouzier, C. Stedman, A. Wiercinska-Drapalo, A. Horban, L. Chang, Y. Zhang, P. Sampeur, I. Najera, N. Shulman, P. Smith, and J. Tran. Journal of Hepatology. 52: p. S16-S17; ISI[:000277018000039].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018000035&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Resistance Selection Following 15 Days of Monotherapy with SCY-635 a Non-Immunosuppressive Cyclophilin Inhibitor with Potent Anti-HCV Activity.</a> Hopkins, S., S. Mosier, R. Harris, P. Kowalczyk, and Z. Huang. Journal of Hepatology. 52: p. S15-S15; ISI[000277018000035].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001385&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Combination of TMC435 with Two Novel NS5B Inhibitors Increases Anti HCV Activity and Reuslts in a Higher Genetic Barrier In Vitro.</a> Lenz, O., J.M. Berke, H. de Kock, P. van Remoortere, O. Nyanguile, S. Vendeville, T. Jonckers, P. Raboisson, K. Simmen, G. Fanning, and T.I. Lin. Journal of Hepatology. 52: p. S295-S296; ISI[000277018001385].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001387&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Identification and Preclinical Profile of the Novel HCV N53 Protease Inhibitor BMS-650032.</a> McPhee, F. Journal of Hepatology. 52: p. S296-S296; ISI[000277018001387].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001389&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Naringenin Inhibits the Assembly and Long-Term Production of Infectious Hepatitis C Virus (HCV) Particles Through a PPAR-Mediated Mechanism.</a> Nahmias, Y., S.J. Polyak, and R.T. Chung. Journal of Hepatology. 52: p. S297-S297; ISI[000277018001389].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277246300006&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Modifications of C-2 on the pyrroloquinoline template aimed at the development of potent herpesvirus antivirals with improved aqueous solubility.</a> Nieman, J.A., S.K. Nair, S.E. Heasley, B.L. Schultz, H.M. Zerth, R.A. Nugent, K. Chen, K.J. Stephanski, T.A. Hopkins, M.L. Knechtel, N.L. Oien, J.L. Wieber, and M.W. Wathen. Bioorganic &amp; Medicinal Chemistry Letters. 20(10): p. 3039-3042; ISI[000277246300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277444600005&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Inhibition of hepatitis C virus replication by single-stranded RNA structural mimics.</a> Smolic, R., M. Smolic, J.H. Andorfer, C.H. Wu, R.M. Smith, and G.Y. Wu. World Journal of Gastroenterology. 16(17): p. 2100-2108; ISI[000277444600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277355800036&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Nonsulfated, Cinnamic Acid-Based Lignins are Potent Antagonists of HSV-1 Entry into Cells.</a> Thakkar, J.N., V. Tiwari, and U.R. Desai. Biomacromolecules. 11(5): p. 1412-1416; ISI[000277355800036].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277018001299&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Enhanced In Vitro Antiviral Activity of ANA598 in Combination with Other Anti-HCV Agents Support Combination Treatment.</a> Thompson, P.A., R.E. Showalter, R.A. Patel, and J.R. Appleman. Journal of Hepatology. 52: p. S262-S262; ISI[000277018001299].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277187600013&amp;SID=3F7pOI44KE5GpIPLGK1&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Effect of oxymatrine on the replication cycle of hepatitis B virus in vitro. World Journal of Gastroenterology.</a> Xu, W.S., K.K. Zhao, X.H. Miao, W. Ni, X. Cai, R.Q. Zhang, and J.X. Wang. 16(16): p. 2028-2037; ISI[000277187600013].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0514-052710.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
