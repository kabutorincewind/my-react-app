

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-12-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5Yo38UgsjNwA4M4r9LVTPvN67MYTboSlsFAoe8l9/unwJPOOi0JE7m04t646qbUlbDl8vEzQ8t0q0QurtCeLnTxCY5f3Xk2FUpUOtm4hT72dJLUBiezFbm/L1WM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6C0F66D7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 25, 2011 - December 8, 2011</h1> 

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22044229">Biochemistry and Biophysics of HIV-1 gp41 - Membrane Interactions and Implications for HIV-1 Envelope Protein Mediated Viral-Cell Fusion and Fusion Inhibitor Design.</a> Cai, L., M. Gochin, and K. Liu, Current Topics in Medicinal Chemistry, 2011. 11(24): p. 2959-2984; PMID[22044229].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22146051">The Crystal Structure of Protease Sapp1p from Candida parapsilosis in Complex with the HIV protease Inhibitor Ritonavir.</a> Dostal, J., J. Brynda, O. Hruskova-Heidingsfeldova, P. Pachl, I. Pichova, and P. Rezacova, Journal of Enzyme Inhibition and Medicinal Chemistry 2011. [Epub ahead of print]; PMID[22146051].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22037050">Design, Synthesis and Biological Evaluation of cycloalkyl arylpyrimidines (CAPYs) as HIV-1 NNRTIs.</a> Gu, S.X., S.Q. Yang, Q.Q. He, X.D. Ma, F.E. Chen, H.F. Dai, E.D. Clercq, J. Balzarini, and C. Pannecouque, Bioorganic &amp; Medicinal Chemistry, 2011. 19(23): p. 7093-7099; PMID[22037050].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22036651">Antiviral Interactions of Combinations of Highly Potent 2,4(1H,3H)-Pyrimidinedione Congeners and other anti-HIV Agents.</a> Hartman, T.L., L. Yang, and R.W. Buckheit, Jr., Antiviral Research 2011. 92(3): p. 505-508; PMID[22036651].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21941167">Initial viral decay to assess the relative antiretroviral potency of protease inhibitor-sparing, nonnucleoside reverse transcriptase inhibitor-sparing, and nucleoside reverse transcriptase inhibitor-sparing regimens for first-line therapy of HIV infection.</a> Haubrich, R.H., S.A. Riddler, H. Ribaudo, G. Direnzo, K.L. Klingman, K.W. Garren, D.L. Butcher, J.F. Rooney, D.V. Havlir, and J.W. Mellors, AIDS, 2011. 25(18): p. 2269-2278; PMID[21941167].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22146583">The molecular pharmacology of AMD11070: An orally bioavailable CXCR4 HIV entry inhibitor.</a> Mosi, R.M., V. Anastassova, J. Cox, M.C. Darkes, S.R. Idzan, J. Labrecque, G. Lau, K.L. Nelson, K. Patel, Z. Santucci, R.S. Wong, R.T. Skerlj, G.J. Bridger, D. Huskens, D. Schols, and S.P. Fricker, Biochemical Pharmacology, 2011. [Epub ahead of print]; PMID[22146583].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22033480">Structure-Based Design of a Protein Immunogen that Displays an HIV-1 gp41 Neutralizing Epitope.</a> Stanfield, R.L., J.P. Julien, R. Pejchal, J.S. Gach, M.B. Zwick, and I.A. Wilson, Journal of Molecular Biology, 2011. 414(3): p. 460-476; PMID[22033480].
    <br />
    <b>[Pubmed]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21989976">Bifunctional Cinchona Alkaloid Thiourea Catalyzed Highly Efficient, Enantioselective Aza-Henry Reaction of Cyclic Trifluoromethyl Ketimines: Synthesis of Anti-HIV Drug DPC 083.</a> Xie, H., Y. Zhang, S. Zhang, X. Chen, and W. Wang, Angewandte Chemie, 2011. 50(49): p. 11773-11776; PMID[21989976]. <b>[Pubmed]</b>. HIV_1125-120811.</p>


    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296938000014">Synthesis and anti-HIV Activity of New Benzimidazole, Benzothiazole and Carbohyrazide Derivatives of the anti-Inflammatory Drug Indomethacin.</a> Al-Masoudi, N.A., N.N.A. Jafar, L.J. Abbas, S.J. Baqir, and C. Pannecouque, Zeitschrift Fur Naturforschung Section B-a Journal of Chemical Sciences, 2011. 66(9): p. 953-960; ISI[000296938000014].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296756000008">Chemically modified tetracycline-3 (CMT-3): A novel inhibitor of the serine proteinase, elastase.</a> Gu, Y., H.M. Lee, S.R. Simon, and L.M. Golub, Pharmacological Research, 2011. 64(6): p. 595-601; ISI[000296756000008].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296515200040">Picomolar Dichotomous Activity of Gnidimacrin Against HIV-1.</a>Huang, L.H., P Yu, J Zhu, L Lee, KH Chen, CH, Plos One, 2011. 6(10); ISI[000296515200040].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296477900017">QSAR modeling of the inhibition of reverse transcriptase enzyme with benzimidazolone analogs.</a> Kumar, S., V. Singh, and M. Tiwari, Medicinal Chemistry Research, 2011. 20(9): p. 1530-1541; ISI[000296477900017].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296477900019">Quantitative structure activity relationship (QSAR) studies of a series of 2,5-disubstituted-1,3,4-oxadiazole derivatives as biologically potential compounds.</a> Pal, T., R.R. Somani, P.Y. Shirodkar, and U.V. Bhanushali, Medicinal Chemistry Research, 2011. 20(9): p. 1550-1555; ISI[000296477900019].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296717600034.">Synthesis and antiviral activity of 2 &#39;-deoxy-2 &#39;-fluoro-2 &#39;-C-methyl-7-deazapurine nucleosides, their phosphoramidate prodrugs and 5 &#39;-triphosphates.</a> Shi, J.X., L.H. Zhou, H.W. Zhang, T.R. McBrayer, M.A. Detorio, M. Johns, L. Bassit, M.H. Powdrill, T. Whitaker, S.J. Coats, M. Gotte, and R.F. Schinazi, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(23): p. 7094-7098; ISI[000296717600034] <b>[WOS]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296477900020">Modeling of novel HIV-1 protease inhibitors incorporating N-Aryl-oxazolidinone-5-carboxamides as P2 ligands using quantum chemical and topological finger print descriptors.</a> Singh, S.S., S Shukla, P, Medicinal Chemistry Research, 2011. 20(9): p. 1556-1565; ISI[000296477900020].
    <br />
    <b>[WOS]</b>. HIV_1125-120811.</p>

    <p class="memofmt2-2">Patent Citations</p>

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111102&amp;CC=CN&amp;NR=102229547A&amp;KC=A">Preparation of Benzenesulfonamide Derivatives as anti-HIV Agents.</a> Chen, F., S. Gu, X. Ma, Q. He, Y. Zheng, X. Zhang, and L. Yang. <b>Patent</b>. 2011-10099552 102229547, 11pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111019&amp;CC=CN&amp;NR=102219717A&amp;KC=A">Preparation of Carboxamide Derivatives Useful in the Treatment of AIDS and Cancer.</a> Chen, F., X. Ma, S. Gu, Q. He, Y. Zheng, X. Zhang, and L. Yang. <b>Patent</b>. 2011-10099575 102219717, 12pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111110&amp;CC=WO&amp;NR=2011139637A1&amp;KC=A1">Small-Molecule Modulators of HIV-1 Capsid Stability and Methods Thereof.</a> Cocklin, S., S. Kortagere, and A.B. Smith, III. <b>Patent</b>. 2011-US33789 2011139637, 121pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111027&amp;CC=WO&amp;NR=2011133929A2&amp;KC=A2">Reducing Transmission of Sexually Transmitted Infections Using a Semen-derived Enhancer of Viral Infection (Sevi)-Binding Agent.</a> Dewhurst, S., B. Nilsson, J. Olsen, and J. Yang. <b>Patent</b>. 2011-US33659 2011133929, 74pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111103&amp;CC=US&amp;NR=2011269676A1&amp;KC=A1">Characterization of Bifunctional Polypeptides for Inactivating HIV and Blocking HIV Entry.</a> Jiang, S., C. Pan, and L. Lu. <b>Patent</b>. 2011-100031</p>

    <p class="plaintext">20110269676, 55pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111027&amp;CC=US&amp;NR=2011263485A1&amp;KC=A1">Bifunctional Griffithsin Analogs Comprising gp120-binding Proteins and gp41-binding Proteins, for use in Prevention of HIV Infection.</a> Liwang, P.J. and I. Kagiampakis. <b>Patent</b>. 2010-957261 20110263485, 44pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111012&amp;CC=CN&amp;NR=102212083A&amp;KC=A">Preparation of Menthyl 2&#39;,3&#39;-didehydro-3&#39;-deoxythymidine 5&#39;-hydrogenphosphonate for Treating AIDS.</a> Wu, Z., S. Du, M. Ao, Z. Cai, and Y. Zhao. <b>Patent</b>. 2011-10085220 102212083, 7pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20080605&amp;CC=US&amp;NR=2008132516A1&amp;KC=A1">Indole, Azaindole and Related Heterocyclic N-Substituted piperazine Derivatives and Their Preparation and use for the Treatment of HIV Infection.</a> Yeung, K.-S., M.E. Farkas, J.F. Kadow, N.A. Meanwell, M. Taylor, D. Johnston, T.S. Coulter, and J.J.K. Wright. <b>Patent</b>. 2008-28189</p>

    <p class="plaintext">8039486, 118pp , Cont -in-part of U S Ser No 129,673.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110818&amp;CC=WO&amp;NR=2011100308A1&amp;KC=A1">Preparation of Betulin Derivatives for Treatment of HIV-1.</a> Gao, D., N. Han, Z. Jin, F. Ning, J. Tang, Y. Wu, and H. Yang. <b>Patent</b>. 2011-US24174 2011100308, 52pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110915&amp;CC=US&amp;NR=2011224242A1&amp;KC=A1">Preparation of Styrylquinolines and Their Therapeutic uses as Integrase Inhibitors and for the Treatment and Prevention of HIV.</a> Giethlen, B., M. Michaut, C. Monneret, E. Soma, L. Thibault, and C.G. Wermuth. <b>Patent</b>. 2011-55703 20110224242, 18pp , Cont -in-part of U S Ser No 269,241.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110909&amp;CC=WO&amp;NR=2011107572A1&amp;KC=A1">Ferrocenyl Flavonoids.</a> Hillard, E., G. Chabot, J.-P. Monserrat, G. Jaouen, K.N. Tiwari, F. De Montigny, and N. Neamati. <b>Patent</b>. 2011-EP53249 2011107572, 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011106445A1&amp;KC=A1">Preparation of Oxazinonaphthyridine Derivatives for use as anti-HIV Agents.</a> Jin, H., C.U. Kim, and B.W. Phillips,. <b>Patent</b>. 2011-US25948</p>

    <p class="plaintext">2011106445, 56pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011106703A2&amp;KC=A2">CXCR4 Receptor Allosteric Modulators.</a> Looby, R.J. and B. Tchernychev. <b>Patent</b>. 2011-US26322 2011106703, 142pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011105590A1&amp;KC=A1">Preparation of 1,3,4,8-Tetrahydro-2h-pyrido[1,2-a]pyrazine Derivatives and Their use as HIV Integrase Inhibitors.</a> Miyazaki, S., Y. Bessho, K. Adachi, S. Kawashita, H. Isoshima, K. Oshita, and S. Fukuda. <b>Patent</b>. 2011-JP54404 2011105590, 278pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110825&amp;CC=WO&amp;NR=2011100838A1&amp;KC=A1">1-Phenyl-1,5-dihydrobenzo[B][1,4]diazepine-2,4-dione Derivatives as HIV Replication Inhibitors and Their Preparation and use for the Treatment of HIV Infection.</a> Simoneau, B., P. Deroy, L. Fader, A.-M. Faucher, A. Gagnon, C. Grand-Maitre, S. Kawai, S. Landry, J.-F. Mercier, and J. Rancourt. <b>Patent</b>. 2011-CA50071 2011100838, 122pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111006&amp;CC=WO&amp;NR=2011120133A1&amp;KC=A1">Preparation of 1-[(5-Oxo-4,5-dihydro-1h-1,2,4-triazol-3-yl)methyl]-pyridin-2(1h)-one Compounds as Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Burch, J., B. Cote, N. Nguyen, C.S. Li, M. St-Onge, and D. Gauvreau. <b>Patent</b>. 2011-CA320 2011120133, 83pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111012&amp;CC=CN&amp;NR=102212032A&amp;KC=A">5-Hydroxyquinolinone Derivative, Its Preparation Method and Application in Preparing the Medicine for Treating and Preventing Acquired Immune Deficiency Syndrome (AIDS).</a> Chen, F., Q. He, H. Wu, S. Gu, X. Ma, Y. Zheng, X. Zhang, and L. Yang. <b>Patent</b>. 2011-10099562 102212032, 14pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111012&amp;CC=CN&amp;NR=102212022A&amp;KC=A">Preparation of Benzhydrol Derivatives as anti-HIV and Antitumor Agents.</a> Chen, F., X. Ma, S. Gu, Q. He, Y. Zheng, X. Zhang, and L. Yang. <b>Patent</b>. 2011-10099577 102212022, 9pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111005&amp;CC=CN&amp;NR=102206177A&amp;KC=A">Preparation of 1-Naphthylbenzophenone Derivatives for use as Antiviral and Anticancer Agents.</a> Chen, F., X. Ma, S. Gu, Q. He, Y. Zheng, X. Zhang, and L. Yang. <b>Patent</b>. 2011-10099574 102206177, 8pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111006&amp;CC=WO&amp;NR=2011120153A1&amp;KC=A1">Pyrimidines and Pyridopyridazine Derivatives and Related Compounds as anti-HIV Agents and Their Preparation and Method for Treatment of HIV.</a> Danter, W., C. Threlfall, S. Guizzetti, and J. Marin. <b>Patent</b>. 2011-CA357 2011120153, 149pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110915&amp;CC=WO&amp;NR=2011113060A2&amp;KC=A2">Antiviral Compounds and Methods of use Thereof.</a> Guenther, R.H. and J.R. Szewczyk. <b>Patent</b>. 2011-US28397 2011113060, 129pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111020&amp;CC=WO&amp;NR=2011129095A1&amp;KC=A1">Pyridone Derivative Having Integrase Inhibitory Activity.</a> Hattori, K. and K. Tomita. <b>Patent</b>. 2011-JP2139 2011129095, 111pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111020&amp;CC=WO&amp;NR=2011130557A2&amp;KC=A2">Nucleobase-bound Phosphonates with Reduced Toxicity for Treatment of Viral Infections.</a> Hostetler, K.Y., J.R. Beadle, and N. Valiaeva. <b>Patent</b>. 2011-US32558 2011130557, 84pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110921&amp;CC=CN&amp;NR=102190638A&amp;KC=A">Preparation of Heterocycles as HIV Protease Inhibitors.</a> Li, S., Y. Hu, J. Yan, D. Bai, and L. Zhou. <b>Patent</b>. 2010-10125641 102190638, 27pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111020&amp;CC=WO&amp;NR=2011127624A1&amp;KC=A1">Preparation of anti-HIV Polypeptides.</a> Liu, K., S. Jiang, Q. Jia, W. Shi, Y. Bai, S. Feng, X. Kang, C. Yang, S. Zhang, and B. Zheng. <b>Patent</b>. 2010-CN488 2011127624, 22pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111020&amp;CC=WO&amp;NR=2011130145A1&amp;KC=A1">Banana Lectins and uses Thereof.</a> Markovitz, D., M. Swanson, I. Goldstein, and H. Winter. <b>Patent</b>. 2011-US31895 2011130145, 42pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20111006&amp;CC=WO&amp;NR=2011121105A1&amp;KC=A1">Preparation of Macrocyclic Pyrazinopyrrolopyridazine dione Derivatives as HIV Replication Inhibitors.</a> Thuring, J.W.J. and J.F. Bonfanti. <b>Patent</b>. 2011-EP55077 2011121105, 123pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=19931125&amp;CC=WO&amp;NR=9323390A1&amp;KC=A1">6-Pyridyl Substituted Pyrimidine Derivitives and Their use as Antiviral Agents.</a> Chu, S.H., Y.C. Cheng, B.C. Pan. <b>Patent</b>. 1993-WO9323390, 32pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://onlinelibrary.wiley.com/doi/10.1002/jhet.5570310130/abstract">Synthesis and anti-HIV-1 Activities of 6-Arylthio and 6-Arylselenoacyclonucleosides.</a> Bai-Chuan, P., Z.H. Chen, G. Piras, G.E.Deutschman, E. Rowe, S.H. Chu. Journal of Heterocyclic Chemistry, 1994. 31(1): p. 177-185.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1125-120811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
