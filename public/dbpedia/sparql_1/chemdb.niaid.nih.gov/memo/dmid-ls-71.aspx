

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-71.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6LHtoVWH+GxerBH/VSAOJRWmAIuR9yMyaN9acGXWO1NXmvnLkvTM9Jra58tOhDhmply7mAUC4rZTi2QZqUkEYpTRO7PcMp4XfU4PT3+E4dkLvDZW9CYU1lJom8U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="048AAE00" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-71-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    6191   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           P4 cap modified tetrapeptidyl alpha-ketoamides as potent HCV NS3 protease inhibitors</p>

<p>           Sun, DX, Liu, L, Heinz, B, Kolykhalov, A, Lamar, J, Johnson, RB, Wang, QM, Yip, Y, and Chen, SH</p>

<p>           Bioorg Med Chem Lett 2004. 14(16):  4333-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261297&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261297&amp;dopt=abstract</a> </p><br />

<p>     2.    6192   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Identification of [(naphthalene-1-carbonyl)-amino]-acetic acid derivatives as nonnucleoside inhibitors of HCV NS5B RNA dependent RNA polymerase</p>

<p>           Gopalsamy, A, Lim, K, Ellingboe, JW, Krishnamurthy, G, Orlowski, M, Feld, B, Van, Zeijl M, and Howe, AY</p>

<p>           Bioorg Med Chem Lett 2004. 14(16):  4221-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261274&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261274&amp;dopt=abstract</a> </p><br />

<p>     3.    6193   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Thiourea inhibitors of herpesviruses. Part 3: Inhibitors of varicella zoster virus</p>

<p>           Di Grandi, MJ, Curran, KJ, Feigelson, G, Prashad, A, Ross, AA, Visalli, R, Fairhurst, J, Feld, B, and Bloom, JD</p>

<p>           Bioorg Med Chem Lett 2004. 14(16): 4157-4160</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261261&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261261&amp;dopt=abstract</a> </p><br />

<p>     4.    6194   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           In vitro susceptibility of lamivudine-resistant hepatitis B virus to adefovir and tenofovir</p>

<p>           Lada, O, Benhamou, Y, Cahour, A, Katlama, C, Poynard, T, and Thibault, V</p>

<p>           Antivir Ther 2004. 9(3): 353-63</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15259898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15259898&amp;dopt=abstract</a> </p><br />

<p>     5.    6195   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Experimental models for hepatitis C virus (HCV): New opportunities for combating hepatitis C</p>

<p>           Trujillo-Murillo, KC, Garza-Rodriguez, ML, Martinez-Rodriguez, HG, Barrera-Saldana, HA, Bosques-Padilla, F, Ramos-Jimenez, J, and Rivas-Estilla, AM</p>

<p>           Ann Hepatol 2004. 3(2): 54-62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15257247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15257247&amp;dopt=abstract</a> </p><br />

<p>     6.    6196   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Virus Inactivation by Nucleic Acid Extraction Reagents</b></p>

<p>           Blow, JA, Dohm, DJ, Negley, DL, and Mores, CN</p>

<p>           Journal of Virological Methods 2004.  119(2): 195-198</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    7.     6197   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Hantaviruses and hantavirus infection</p>

<p>           Dekonenko, AE and Tkachenko, EA</p>

<p>           Voprosy Virusologii 2004. 49(3): 40-44</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     8.    6198   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Genetic antiviral program against hepatitis C virus</p>

<p>           Sivov, IG, Samokhvalov, EI, and Knyazhev, VA</p>

<p>           Dokl Biochem Biophys 2003. 392: 288-91</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15255205&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15255205&amp;dopt=abstract</a> </p><br />

<p>     9.    6199   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Inhibition of infection and replication of human herpesvirus 8 in microvascular endothelial cells by alpha interferon and phosphonoformic Acid</p>

<p>           Krug, LT, Pozharskaya, VP, Yu, Y, Inoue, N, and Offermann, MK</p>

<p>           J Virol 2004. 78(15): 8359-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254208&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254208&amp;dopt=abstract</a> </p><br />

<p>   10.    6200   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           The emergence in Japan of Sathuperi virus, a tropical Simbu serogroup virus of the genus Orthobunyavirus</p>

<p>           Yanase, T, Fukutomi, T, Yoshida, K, Kato, T, Ohashi, S, Yamakawa, M, and Tsuda, T</p>

<p>           Archives of Virology 2004. 149(5):  1007-1013</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   11.    6201   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Role of nucleolin in human parainfluenza virus type 3 infection of human lung epithelial cells</p>

<p>           Bose, S, Basu, M, and Banerjee, AK</p>

<p>           J Virol 2004. 78(15): 8146-58</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254186&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254186&amp;dopt=abstract</a> </p><br />

<p>   12.    6202   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Discovery of antivirals against smallpox</p>

<p>           Harrison, SC, Alberts, B, Ehrenfeld, E, Enquist, L, Fineberg, H, McKnight, SL, Moss, B, O&#39;Donnell, M, Ploegh, H, Schmid, SL, Walter, KP, and Theriot, J</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249657&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249657&amp;dopt=abstract</a> </p><br />

<p>   13.    6203   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Biotechnology and Drug Discovery: From Bench to Bedside</b></p>

<p>           Avidor, Y, Mabjeesh, NJ, and Matzkin, H</p>

<p>           Southern Medical Journal 2003. 96(12): 1174-1186</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  14.     6204   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Potent and selective inhibition of SARS coronavirus replication by aurintricarboxylic acid</p>

<p>           He, R, Adonov, A, Traykova-Adonova, M, Cao, J, Cutts, T, Grudesky, E, Deschambaul, Y, Berry, J, Drebot, M, and Li, X</p>

<p>           Biochem Biophys Res Commun 2004. 320(4):  1199-203</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249217&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15249217&amp;dopt=abstract</a> </p><br />

<p>   15.    6205   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Mutations in the Human Cytomegalovirus Ul27 Gene That Confer Resistance to Maribavir</b></p>

<p>           Chou, SW, Marousek, GI, Senters, AE, Davis, MG, and Biron, KK</p>

<p>           Journal of Virology 2004. 78(13): 7124-7130</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   16.    6206   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p><b>           Glycopeptide antibiotic derivatives, their preparation and their use as antiviral agents</b>((K.U. Leuven Research and Development, Belg.)</p>

<p>           Balzarini, Jan, Preobrazhenskaya, Maria, and De Clercq, Erik</p>

<p>           PATENT: WO 2004019970 A2;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 91 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    6207   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p><b>           Pyrimidinium-guanidiniminoethylphenyl-guanidinoalkylamines and guanidiniminoethylphenyl-guanidinoalkylamines as pharmaceutically active new substances for the treatment of viral infections and other conditions</b>((Mondobiotech SA, Switz.)</p>

<p>           Bevec, Dorian </p>

<p>           PATENT: EP 1413300 A1;  ISSUE DATE: 20040428</p>

<p>           APPLICATION: 2002-24004; PP: 28 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   18.    6208   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Reporter substrates for assessing the activity of the hepatitis C virus NS3-4A serine protease in living cells</p>

<p>           Pacini, L, Bartholomew, L, Vitelli, A, and Migliaccio, G</p>

<p>           Anal Biochem 2004. 331(1): 46-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245996&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245996&amp;dopt=abstract</a> </p><br />

<p>   19.    6209   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Oral Activity of Ether Lipid Ester Prodrugs of Cidofovir against Experimental Human Cytomegalovirus Infection</p>

<p>           Bidanset, DJ, Beadle, JR, Wan, WB, Hostetler, KY, and Kern, ER</p>

<p>           J Infect Dis 2004. 190(3): 499-503</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15243923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15243923&amp;dopt=abstract</a> </p><br />

<p>   20.    6210   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Recent advances in herpes simplex virus antiviral therapies</p>

<p>           Firestine, Steven M</p>

<p>           Expert Opinion on Therapeutic Patents 2004. 14(8): 1139-1151</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   21.    6211   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Novel azapeptide inhibitors of hepatitis C virus serine protease</p>

<p>           Bailey, MD, Halmos, T, Goudreau, N, Lescop, E, and Llinas-Brunet, M</p>

<p>           J Med Chem 2004. 47(15): 3788-99</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239657&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15239657&amp;dopt=abstract</a> </p><br />

<p>   22.    6212   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           The Synthesis and Antiviral Activity of Glycyrrhizic Acid Conjugates with a-D-Glucosamine and Some Glycosylamines</p>

<p>           Kondratenko, RM, Baltina, LA, Mustafina, SR, Vasil&#39;eva, EV, Pompei, R, Deidda, D, Plyasunova, OA, Pokrovskii, AG, and Tolstikov, GA</p>

<p>           Russian Journal of Bioorganic Chemistry (Translation of Bioorganicheskaya Khimiya) 2004. 30(3): 275-282</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    6213   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Synthesis and Antiviral Activities of New Pyrazolo[4,3-c]quinolin-3-ones and Their Ribonucleoside Derivatives</p>

<p>           de Oliveira, Mara RP, Alves, Thatyana R, Pinto, Angelo C, Pereira, Helena de S, Leao-Ferreira, Luiz R, Moussatche, Nissin, Frugulhetti, Izabel Christina de PP, Ferreira, Vitor F, and de Souza, Maria Cecilia BV</p>

<p>           Nucleosides, Nucleotides &amp; Nucleic Acids 2004. 23(5): 735-748</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    6214   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Syntheses of novel modified acyclic purine and pyrimidine nucleosides as potential substrates of herpes simplex virus type-1 thymidine kinase for monitoring gene expression</p>

<p>           Grote, Michaela, Noll, Steffi, Noll, Bernhard, Johannsen, Bernd, and Kraus, Werner</p>

<p>           Canadian Journal of Chemistry 2004.  82(4): 513-523</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   25.    6215   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>New Treatment of Chronic Hepatitis B</b></p>

<p>           Lok, ASF</p>

<p>           Seminars in Liver Disease 2004. 24: 77-82</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   26.    6216   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Synthesis and evaluation of novel 1,4-naphthoquinone derivatives as antiviral, antifungal and anticancer agents</p>

<p>           Tandon, Vishnu K, Singh, Ravindra V, and Yadav, Dharmendra B</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(11): 2901-2904</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   27.    6217   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Peginterferon Alfa-2b Plus Ribavirin for Treatment of Chronic Hepatitis C in Previously Untreated Patients Infected With Hcv Genotypes 2 or 3</b></p>

<p>           Zeuzem, S, Hultcrantz, R, Bourliere, M, Goeser, T, Marcellin, P, Sanchez-Tapias, J, Sarrazin, C, Harvey, J, Brass, C, and Albrecht, J</p>

<p>           Journal of Hepatology 2004. 40(6): 993-999</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.     6218   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           In vitro efficacy of ganciclovir, cidofovir, penciclovir, foscarnet, idoxuridine, and acyclovir against feline herpesvirus type-1</p>

<p>           Maggs, David J and Clarke, Heather E</p>

<p>           American Journal of Veterinary Research 2004. 65(4): 399-403</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   29.    6219   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Interferon-Alpha for Helpatitis C: Antiviral or Immunotherapy?</b></p>

<p>           Diepolder, HM</p>

<p>           Journal of Hepatology 2004. 40(6): 1030-1031</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   30.    6220   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Treatment of acute hepatitis C with interferon alpha-2b: early initiation of treatment is the most effective predictive factor of sustained viral response</p>

<p>           Delwaide, J, Bourgeois, N, Gerard, C, De, Maeght S, Mokaddem, F, Wain, E, Bastens, B, Fevery, J, Gehenot, M, Le, Moine O, Martinet, JP, Robaeys, G, Servais, B, Van, Gossum M, and Van, Vlierberghe H</p>

<p>           Aliment Pharmacol Ther 2004. 20(1): 15-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225166&amp;dopt=abstract</a> </p><br />

<p>   31.    6221   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Neuraminidase inhibitors for treatment of influenza</p>

<p>           Ebell, MH</p>

<p>           Am Fam Physician 2004. 69(12): 2824</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15222647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15222647&amp;dopt=abstract</a> </p><br />

<p>   32.    6222   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Novel phosphonate nucleosides as antiviral agents</p>

<p>           Hwang, Jae-Taeg and Choi, Jong-Ryoo</p>

<p>           Drugs of the Future 2004. 29(2): 163-177</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   33.    6223   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Direct Fluorometric Measurement of Hepatitis C Virus Helicase Activity</b></p>

<p>           Boguszewska-Chachulska, AM, Krawczyk, M, Stankiewicz, A, Gozdek, A, Haenni, AL, and Strokovskaya, L</p>

<p>           Febs Letters 2004. 567(2-3): 253-258</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   34.    6224   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Activity of acyclovir, ciprofloxacin and organotin polymers derived from acyclovir and ciprofloxacin against Herpes simplex virus (HSV-1) and Varicella Zoster virus (VZV)</p>

<p>           Roner, Michael R, Carraher, Charles E Jr, Zhao, Anna, Roehr, Joanne L, Bassett, Kelly D, and Siegmann-Louda, Deborah W</p>

<p>           Polymeric Materials Science and Engineering 2004. 90: 515-518</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  35.     6225   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Characterization of a unique group-specific protein (U122) of the severe acute respiratory syndrome coronavirus</p>

<p>           Fielding, BC, Tan, YJ, Shuo, S, Tan, TH, Ooi, EE, Lim, SG, Hong, W, and Goh, PY</p>

<p>           J Virol 2004. 78(14): 7311-8</p>

<p>           <!--HYPERLNK:--> http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220404&amp;dopt=abstract</p>

<p>   36.    6226   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Poxvirus protein N1L targets the I-kappa B kinase complex, inhibits signaling to NF-kappa B by the tumor necrosis factor superfamily of receptors, and inhibits NF-kappa B and IRF3 signaling by toll-like receptors</p>

<p>           DiPerna, G, Stack, J, Bowie, AG, Boyd, A, Kotwal, G, Zhang, Z, Arvikar, S, Latz, E, Fitzgerald, KA, and Marshall, WL</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215253&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15215253&amp;dopt=abstract</a> </p><br />

<p>   37.    6227   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Neutralizing innate host defenses to control viral translation in HSV-1 infected cells</p>

<p>           Mohr, Ian</p>

<p>           International Reviews of Immunology 2004. 23(1-2): 199-220</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    6228   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>The Monoethyl Ester of Meconic Acid Is an Active Site Inhibitor of Hcvns5b Rna-Dependent Rna Polymerase</b></p>

<p>           Pace, P, Nizi, E, Pacini, B, Pesci, S, Matassa, V, De Francesco, R, Altamura, S, and Summa, V</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(12): 3257-3261</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   39.    6229   DMID-LS-71; PUBMED-DMID-7/21/2004</p>

<p class="memofmt1-3">           Differential activation of interferon regulatory factors-3 and -7 by non-cytopathogenic and cytopathogenic bovine viral diarrhoea virus</p>

<p>           Baigent, SJ, Goodbourn, S, and McCauley, JW</p>

<p>           Vet Immunol Immunopathol 2004. 100(3-4): 135-44</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15207451&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15207451&amp;dopt=abstract</a> </p><br />

<p>   40.    6230   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p><b>           Preparation of 2,2&#39;-(phenylmethylene)bis[4-[[(5-methyl-1H-tetrazol-1-yl)imino]methyl]phenol] compounds, pharmaceutical compositions, and methods for treating or preventing pneumovirus infection and associated diseases</b> ((Viropharma Incorporated, USA)</p>

<p>           Rys, David J, Nitz, Theodore J, Gaboury, Janet A, Burns, Christopher J, Pevear, Daniel C, Lessen, Thomas A, and Herbertz, Torsten</p>

<p>           PATENT: WO 2004014317 A2;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2003; PP: 95 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.     6231   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Herpesvirus Protease Inhibition by Dimer Disruption</b></p>

<p>           Shimba, N, Nomura, AM, Marnett, AB, and Craik, CS</p>

<p>           Journal of Virology 2004. 78(12): 6657-6665</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   42.    6232   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Antiviral prophylaxis of smallpox</p>

<p>           Bray, Mike and Roy, Chad J</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 54(1): 1-5</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   43.    6233   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Inhibition of virus production in JC virus-infected cells by postinfection RNA interference</p>

<p>           Orba, Yasuko, Sawa, Hirofumi, Iwata, Hiroshi, Tanaka, Shinya, and Nagashima, Kazuo</p>

<p>           Journal of Virology 2004. 78(13):  7270-7273</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   44.    6234   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Rapid Identification of Coronavirus Replicase Inhibitors Using a Selectable Replicon Rna</b></p>

<p>           Hertzig, T, Scandella, E, Schelle, B, Ziebuhr, J, Siddell, SG, Ludewig, B, and Thiel, V</p>

<p>           Journal of General Virology 2004. 85: 1717-1725</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   45.    6235   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Antiretroviral therapy and HIV/hepatitis B virus coinfection</p>

<p>           Benhamou, Yves</p>

<p>           Clinical Infectious Diseases 2004.  38(Suppl. 2): S98-S103</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   46.    6236   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>The Role of Ribavirin in the Combination Therapy of Hepatitis C Virus Infection</b></p>

<p>           Picardi, A, Gentilucci, UV, Zardi, EM , D&#39;avola, D, Amoroso, A, and Afeltra, A</p>

<p>           Current Pharmaceutical Design 2004. 10(17): 2081-2092</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   47.    6237   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>The Impact of Antiviral Treatments on the Course of Chronic Hepatitis C: an Evidence-Based Approach</b></p>

<p>           Camma, C, Di Bona, D, and Craxj, A</p>

<p>           Current Pharmaceutical Design 2004. 10(17): 2123-2130</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   48.    6238   DMID-LS-71; SCIFINDER-DMID-7/21/2004</p>

<p class="memofmt1-3">           Inhibitors of hepatitis C virus NS3.4A protease 2. Warhead SAR and optimization</p>

<p>           Perni, Robert B, Pitlik, Janos, Britt, Shawn D, Court, John J, Courtney, Lawrence F, Deininger, David D, Farmer, Luc J, Gates, Cynthia A, Harbeson, Scott L, Levin, Rhonda B, Lin, Chao, Lin, Kai, Moon, Young-Choon, Luong, Yu-Ping, O&#39;Malley, Ethan T, Rao, BGovinda, Thomson, John A, Tung, Roger D, Van Drie, John H, and Wei, Yunyi</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(6): 1441-1446</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  49.     6239   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Suppression of Sars-Cov Entry by Peptides Corresponding to Heptad Regions on Spike Glycoprotein</b></p>

<p>           Yuan, KH, Yi, L, Chen, J, Qu, XX, Qing, TT, Rao, X, Jiang, PF, Hu, JH, Xiong, ZK, Nie, YC, Shi, XL,  Wang, W, Ling, C, Yin, XL, Fan, KQ, Lai, LH,  Ding, MX, and Deng, HK</p>

<p>           Biochemical and Biophysical Research Communications 2004. 319(3): 746-752</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   50.    6240   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Treatment of Cutaneous Human Papilloma Virus, Poxvirus and Herpes Simple Virus Infections With Topical Cidofovir in Hiv Positive Patients</b></p>

<p>           Toutous-Trellu, L, Hirschel, B, Piguet, V, Schiffer, V, Saurat, JH, and Pechere, M</p>

<p>           Annales De Dermatologie Et De Venereologie 2004. 131(5): 445-449</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   51.    6241   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Molecular Virology of Hepatitis B Virus</b></p>

<p>           Locarnini, S</p>

<p>           Seminars in Liver Disease 2004. 24: 3-10</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   52.    6242   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Inhibition of Influenza Virus Production in Virus-Infected Mice by Rna Interference</b></p>

<p>           Ge, Q, Filip, L, Bai, AL, Nguyen, T, Eisen, HN, and Chen, J</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2004. 101(23): 8676-8681</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   53.    6243   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Coxsackievirus Replication and the Cell Cycle: a Potential Regulatory Mechanism for Viral Persistence/Latency</b></p>

<p>           Feuer, R, Mena, I, Pagarigan, RR, Hassett, DE, and Whitton, JL</p>

<p>           Medical Microbiology and Immunology 2004. 193(2-3): 83-90</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   54.    6244   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Tackling the Next Influenza Pandemic - &quot;Ring&quot; Prophylaxis of Close Contacts With Antivirals May Be an Effective Strategy</b></p>

<p>           Balicer, RD,  Huerta, M, and Grotto, I</p>

<p>           British Medical Journal 2004. 328(7453): 1391-1392</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   55.    6245   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Biochemical and Functional Analysis of Smallpox Growth Factor (Spgf) and Anti-Spgf Monoclonal Antibodies</b></p>

<p>           Kim, M, Yang, HL, Kim, SK, Reche, PA, Tirabassi, RS, Hussey, RE, Chishti, Y, Rheinwald, JG, Morehead, TJ, Zech, T, Damon, IK, Welsh, RM, and Reinherz, EL</p>

<p>           Journal of Biological Chemistry 2004. 279(24): 25838-25848</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   56.    6246   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Efficacy of Cidofovir in a Murine Model of Disseminated Progressive Vaccinia</b></p>

<p>           Neyts, J, Leyssen, P, Verbeken, E, and De Clercq, E</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(6): 2267-2273</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  57.     6247   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Infectious Diseases - Part Ii: New Agents, Resistances, and Treatment Strategies</b></p>

<p>           Salzberger, B, Franzen, C, and Gluck, T</p>

<p>           Medizinische Klinik 2004. 99(5): 251-260</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   58.    6248   DMID-LS-71; WOS-DMID-7/21/2004</p>

<p>           <b>Systematic Review: Surveillance Systems for Early Detection of Bioterrorism-Related Diseases</b></p>

<p>           Bravata, DM,  Mcdonald, KM, Smith, WM, Rydzak, C, Szeto, H, Buckeridge, DL, Haberland, C, and Owens, DK</p>

<p>           Annals of Internal Medicine 2004. 140(11): 910-922</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
