

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-11-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NeBYLAQyOVXX1XxDiqQ4YLuV+LsSaUDKtz6E3bDSM+eRC2q9XJM9K8f3pN3rogOdGewL0g5WivpYURt6QfPX1z4v4WGcqFLAHByTGJshminwNSWnBy1OPJW5SsU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="839E1DF8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: November 8 - November 21, 2013</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24159919">Conformation-based Restrictions and Scaffold Replacements in the Design of Hepatitis C Virus Polymerase Inhibitors: Discovery of Deleobuvir (BI 207127)<i>.</i></a> Laplante, S.R., M. Bos, C. Brochu, C. Chabot, R. Coulombe, J.R. Gillard, A. Jakalian, M. Poirier, J. Rancourt, T. Stammers, B. Thavonekham, P.L. Beaulieu, G. Kukolj, and Y.S. Tsantrizos. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24159919].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24144444">Ligand Bioactive Conformation Plays a Critical Role in the Design of Drugs That Target the Hepatitis C Virus NS3 Protease<i>.</i></a> Laplante, S.R., H. Nar, C.T. Lemke, A. Jakalian, N. Aubry, and S.H. Kawai. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24144444].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24224729">Imidazopyridazine Hepatitis C Virus Polymerase Inhibitors. Structure-Activity Relationship Studies and the Discovery of a Novel, Traceless Prodrug Mechanism<i>.</i></a> Leivers, M.R., J.F. Miller, S.A. Chan, R. Lauchli, S. Liehr, W. Mo, T. Ton, E.M. Turner, M. Youngman, J.G. Falls, S. Long, A. Mathis, and J.T. Walker. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24224729].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24251365">Total Synthesis and anti-Hepatitis C Virus Activity of MA026<i>.</i></a> Shimura, S., M. Ishima, S. Nakajima, T. Fujii, N. Himeno, K. Ikeda, J. Izaguirre-Carbonell, H. Murata, T. Takeuchi, S. Kamisuki, T. Suzuki, K. Kuramochi, K. Watashi, S. Kobayashi, and F. Sugawara. Journal of the American Chemical Society, 2013. <b>[Epub ahead of print]</b>; PMID[24251365].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <h2>Kaposi</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24250827">Combination of Arsenic and Interferon-alpha Inhibits Expression of KSHV Latent Transcripts and Synergistically Improves Survival of Mice with Primary Effusion Lymphomas<i>.</i></a> El Hajj, H., J. Ali, A. Ghantous, D. Hodroj, A. Daher, K. Zibara, C. Journo, Z. Otrock, G. Zaatari, R. Mahieux, M. El Sabban, A. Bazarbachi, and R. Abou Merhi. Plos One, 2013. 8(11): p. e79474; PMID[24250827].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <h2>SIV</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24227834">Equine Tetherin Blocks Retrovirus Release and Its Activity Is Antagonized by Equine Infectious Anemia Virus Envelope Protein<i>.</i></a> Yin, X., Z. Hu, Q. Gu, X. Wu, Y.H. Zheng, P. Wei, and X. Wang. Journal of Virology, 2013. <b>[Epub ahead of print]</b>; PMID[24227834].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24217696">Evaluation of PD 404,182 as an anti-HIV and anti-HSV Microbicide<i>.</i></a> Chamoun-Emanuelli, A.M., M. Bobardt, B. Moncla, M.K. Mankowski, R.G. Ptak, P. Gallay, and Z. Chen. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[24217696].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1108-112113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326121400003">MicroRNA-specific Argonaute 2 Protein Inhibitors<i>.</i></a> Schmidt, M.F., O. Korb, and C. Abell. ACS Chemical Biology, 2013. 8(10): p. 2122-2126; ISI[000326121400003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1108-112113.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326114500031">Estimation of Efficiency of Solvent-detergent Method for Virus Inactivation in the Technology of Immunoglobulin Production on the Model of Duck Hepatitis B Virus<i>.</i></a> Zubkova, N.V., V.V. Anastasiev, K.K. Kyuregyan, M.I. Mikhailov, A.K. Lobastova, and I.V. Krasil&#39;nikov. Bulletin of Experimental Biology and Medicine, 2013. 155(6): p. 821-824; ISI[000326114500031].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1108-112113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
