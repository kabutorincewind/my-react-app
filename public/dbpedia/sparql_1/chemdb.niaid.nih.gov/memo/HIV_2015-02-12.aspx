

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-02-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="P8+drxk35UakebkpwcsKHjMBIvTaA1/EOMU+dXrY6YljxjRqEWWlHNPq20G3Qc7ggYkxxPcKKfq1rKCHkp+A6LWb7Hs0O600Rq+keH7RPYFmKI78mDT3mj7aJHw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6BF57E89" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 30 - February 12, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24523258">A Novel Ribonuclease with HIV-1 Reverse Transcriptase Inhibitory Activity Purified from the Fungus Ramaria formosa.</a> Zhang, R., G. Tian, Y. Zhao, L. Zhao, H. Wang, Z. Gong, and T.B. Ng. Journal of Basic Microbiology, 2015. 55(2): p. 269-275. PMID[24523258]. <b><br />
    [PubMed].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25537532">Synthesis and Biological Evaluation of DAPY-DPEs Hybrids as Non-nucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Wu, H.Q., J. Yao, Q.Q. He, W.X. Chen, F.E. Chen, C. Pannecouque, E. De Clercq, and D. Daelemans. Bioorganic &amp; Medicinal Chemistry, 2015. 23(3): p. 624-631. PMID[25537532]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25615910">An Efficient and Highly Diastereoselective Synthesis of GSK1265744, a Potent HIV Integrase Inhibitor.</a> Wang, H., M.D. Kowalski, A.S. Lakdawala, F.G. Vogt, and L. Wu. Organic Letters, 2015. 17(3): p. 564-567. PMID[25615910]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25529736">Synthesis and Evaluation of C2-Carbon-linked Heterocyclic-5-hydroxy-6-oxo-dihydropyrimidine-4-carboxamides as HIV-1 Integrase Inhibitors.</a> Naidu, B.N., M.E. Sorenson, M. Patel, Y. Ueda, J. Banville, F. Beaulieu, S. Bollini, I.B. Dicker, H. Higley, Z. Lin, L. Pajor, D.D. Parker, B.J. Terry, M. Zheng, A. Martel, N.A. Meanwell, M. Krystal, and M.A. Walker. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 717-720. PMID[25529736]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25485732">In Vivo Evaluation of Different Formulation Strategies for Sustained Release Injectables of a Poorly Soluble HIV Protease Inhibitor.</a> Meeus, J., D.J. Scurr, K. Amssoms, K. Wuyts, P. Annaert, M.C. Davies, C.J. Roberts, and G. Van den Mooter. Journal of Controlled Release, 2015. 199: p. 1-9. PMID[25485732]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25621706">Synthesis of Novel 2&#39;,3&#39;-Difluorinated 5&#39;-Deoxythreosyl phosphonic acid Nucleosides as Antiviral Agents.</a> Kim, E., S. Kim, and J.H. Hong. Nucleosides, Nucleotides &amp; Nucleic Acids, 2015. 34(2): p. 130-147. PMID[25621706]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25639621">Enhancement and Induction of HIV-1 Infection through an Assembled Peptide Derived from the CD4 Binding Site of gp120.</a> Gross, A., K. Rodel, B. Kneidl, N. Donhauser, M. Mossl, E. Lump, J. Munch, B. Schmidt, and J. Eichler. ChemBioChem, 2015. 16(3): p. 446-454. PMID[25639621]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25260821">Novel in Vitro Inhibitory Functions of Potato Tuber Proteinaceous Inhibitors.</a> Fischer, M., M. Kuckenberg, R. Kastilan, J. Muth, and C. Gebhardt. Molecular Genetics and Genomics, 2015. 290(1): p. 387-398. PMID[25260821]. <b><br />
    [PubMed].</b> HIV_0130-021215.
    <br />
    <br /></p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25420933">Design, Synthesis, and Biological Evaluation of Unconventional Aminopyrimidine, Aminopurine, and Amino-1,3,5-triazine methyloxynucleosides.</a> Fernandez-Cureses, G., S. de Castro, M.L. Jimeno, J. Balzarini, and M.J. Camarasa. ChemMedChem, 2015. 10(2): p. 321-335. PMID[25420933]. <b><br />
    [PubMed].</b> HIV_0130-021215.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774403062">Anti-HIV Activity of Vaginal Epithelial Cells and Vaginal Secretions.</a> Patel, M.V., M. Ghosh, R.M. Rossoll, J.V. Fahey, and C.R. Wira. AIDS Research and Human Retroviruses, 2014. 30: p. A237-A237. ISI[000344774403062].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0130-021215.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347405700019">Structural Modification of Diarylpyrimidine Derivatives as HIV-1 Reverse Transcriptase Inhibitors.</a> Gu, S.X., Y.Y. Zhu, F.E. Chen, C.E. De, J. Balzarini, and C. Pannecouque. Medicinal Chemistry Research, 2015. 24(1): p. 220-225. ISI[000347405700019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0130-021215.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774400104">HBAHP-25 Inhibits HIV-1 Entry into Cells and Neutralizes gp120 Induced Inflammation.</a> Bashir, T., M. Patgaonkar, S. Kumar, and K.V.R. Reddy. AIDS Research and Human Retroviruses, 2014. 30: p. A49-A49. ISI[000344774400104].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0130-021215.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347405700038">Design and Synthesis of N-Hydroxytriazole-4-carboxamides as HIV Integrase Inhibitors.</a> Bai, Y.X., L.J. Li, X.L. Wang, C.C. Zeng, and L.M. Hu. Medicinal Chemistry Research, 2015. 24(1): p. 423-429. ISI[000347405700038].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0130-021215.</p> 

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774402161">Glycolysis Inhibitors as Potential anti-HIV Compounds.</a> Songok, E., B. Nzau, M. Wainberg, F. Plummer, and S. Mpoke. AIDS Research and Human Retroviruses, 2014. 30: p. A206-A207. ISI[000344774402161].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774401171">Broad-spectrum anti-HIV-1 Activity of Anionic Carbosilane Dendrimers and Synergy in Combination with Maraviroc and Tenofovir as Topical Microbicide.</a> Sepulveda-Crespo, D., M.J. Serramia, J. Sanchez-Rodriguez, R. Lorente, R. Gomez, F.J. de la Mata, J.L. Jimenez, and M.A. Munoz-Fernandez. AIDS Research and Human Retroviruses, 2014. 30: p. A144-A145. ISI[000344774401171].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774402156">Antiviral Action of Sulfonate Anionic Carbosilane Dendrimer as a Topical Microbicide against HIV Infection.</a> Sepulveda-Crepo, D., J. Sanchez-Rodriguez, M.J. Serramia, A. Lopez, E. Alonso, R. Gomez, F.J. de la Mata, J.L. Jimenez, and M.A. Munoz-Fernandez. AIDS Research and Human Retroviruses, 2014. 30: p. A205-A205. ISI[000344774402156].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347469000005">2&#39;,3&#39;-Dialdehyde of ATP, ADP, and Adenosine Inhibit HIV-1 Reverse Transcriptase and HIV-1 Replication.</a> Schachter, J., A.L.C. Valadao, R.S. Aguiar, V. Barreto-de-Souza, A.D. Rossi, P.R. Arantes, H. Verli, P.G. Quintana, N. Heise, A. Tanuri, D.C. Bou-Habib, and P.M. Persechini. Current HIV Research, 2014. 12(5): p. 347-358. ISI[000347469000005].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774402157">Identification of a Novel Acylguanidine-based Inhibitor of HIV-1 Replication.</a> Mwimanzi, P.M., I. Tietjen, A. Shahid, S.C. Miller, D. Fedida, Z. Brummer, and M. Brockman. AIDS Research and Human Retroviruses, 2014. 30: p. A205-A205. ISI[000344774402157].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344774404014">Reactivation of Latent HIV-1 by Bryostatin-1 in the Presence of Antiretroviral Drugs.</a> Martinez-Bonet, M., M.I. Clemente, S. Alvarez, L. Diaz, D. Garcia-Alonso, A. Lopez, S. Moreno, E. Munoz, and M. Munoz-Fernandez. AIDS Research and Human Retroviruses, 2014. 30: p. A285-A285. ISI[000344774404014].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000346646704112">Inhibiting HIV-1 with Chimeric tRNA-miRNA Mimics.</a> Gao, E. and K. Haushalter. FASEB Journal, 2014. 28(1). ISI[000346646704112].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347669400008">Synthesis of Cholesterol Conjugated Non-native Derived Heptad Repeat Sequence Peptides as Potent HIV-1 Fusion Inhibitors.</a> Zhang, S., W.G. Shi, C. Wang, L.F. Cai, B.H. Zheng, K. Wang, S.L. Feng, Q.Y. Jia, and K.L. Liu. Chemical Journal of Chinese Universities-Chinese, 2014. 35(12): p. 2542-2546. ISI[000347669400008].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000348275600001">In Vitro Reactivation of Latent HIV-1 by Cytostatic bis (Thiosemicarbazonate) gold(III) Complexes.</a> Fonteh, P. and D. Meyer. BMC Infectious Diseases, 2014. 14. ISI[000348275600001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347583300079">Synthesis of Novel p-tert-Butylcalix[4]arene Schiff Bases and Their Complexes with C60, Potential HIV-protease Inhibitors.</a> Khadra, K.A., S. Mizyed, D. Marji, S.F. Haddad, M. Ashram, and A. Foudeh. Spectrochimica Acta Part A-Molecular and Biomolecular Spectroscopy, 2015. 136: p. 1869-1874. ISI[000347583300079].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0130-021215.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
