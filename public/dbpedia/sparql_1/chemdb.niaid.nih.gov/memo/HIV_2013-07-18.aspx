

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-07-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mdPyriR3wkjIUo2ey4TTKueQKAowobs0RZfNPXj1OfqNso2FmQs/oOtFjl8OZ28NCfT3g1dJ3XhYhARa+brMBlmbmPbO2Xgh97GTheaY3kHrt59WYqaVKZz+sRA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2A894DCB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 5 - July 18, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23843638">Heavy Chain-only IgG2b-llama Antibody Effects Near-pan HIV-1 Neutralization by Recognizing a CD4-Induced Epitope That Includes Elements of Both CD4- and Co-receptor-binding Sites.</a> Acharya, P., T. Luongo, I. Georgiev, J. Matz, S.D. Schmidt, M.K. Louder, P. Kessler, Y. Yang, K. McKee, S. O&#39;Dell, L. Chen, D. Baty, P. Chames, L. Martin, J.R. Mascola, and P.D. Kwong. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23843638].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23744077">Anti-HIV Host Factor SAMHD1 Regulates Viral Sensitivity to Nucleoside Reverse Transcriptase Inhibitors Via Modulation of Cellular Deoxyribonucleoside Triphosphate (dNTP) Levels.</a> Amie, S.M., M.B. Daly, E. Noble, R.F. Schinazi, R.A. Bambara, and B. Kim. The Journal of Biological Chemistry, 2013. 288(28): p. 20683-20691. PMID[23744077].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23835833">Synthesis and anti-HIV-1 Screening of Novel N&#39;-(1-(Aryl)ethylidene)-2-(5,5-dioxido-3-phenylbenzo[E]pyrazolo[4,3-C][1,2]thiazi N-4(1h)-yl)acetohydrazides.</a>Aslam, S., M. Ahmad, M. Zia-Ur-Rehman, C. Montero, M. Detorio, M. Parvez, and R.F. Schinazi. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23835833].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23852261">Transformation of Althaea Officinalis L. By Agrobacterium rhizogenes for the Production of Transgenic Roots Expressing the anti-HIV Microbicide Cyanovirin-N.</a> Drake, P.M., L. de Moraes Madeira, T.H. Szeto, and J.K. Ma. Transgenic Research, 2013. <b>[Epub ahead of print]</b>. PMID[23852261].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23839820">Dibenzocyclooctane Lignans from the Stems of Schisandra wilsoniana.</a> Ma, W.H., Y. Lu, and D.F. Chen. Planta Medica, 2013. <b>[Epub ahead of print]</b>. PMID[23839820].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23828482">Synthesis and Antiviral Activity of New Substituted Methyl [2-(Arylmethylene-hydrazino)-4-oxo-thiazolidin-5-ylidene]acetates.</a> Saeed, A., N.A. Al-Masoudi, and M. Latif. Archiv der Pharmazie, 2013. <b>[Epub ahead of print]</b>. PMID[23828482].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23841536">Preformulation Studies of EFdA, a Novel Nucleoside Reverse Transcriptase Inhibitor for HIV Prevention.</a> Zhang, W., M.A. Parniak, H. Mitsuya, S.G. Sarafianos, P.W. Graebing, and L.C. Rohan. Drug Development and Industrial Pharmacy, 2013. <b>[Epub ahead of print]</b>. PMID[23841536].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23769827">Molecular Dynamics Study of Carbon Nanotube as a Potential Dual-functional Inhibitor of HIV-1 Integrase.</a> Zhang, Z., B. Wang, B. Wan, L. Yu, and Q. Huang. Biochemical and Biophysical Research Communications, 2013. 436(4): p. 650-654. PMID[23769827].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23165001">Inhibition of HIV-1 Integrase Dimerization and Activity with Crosslinked Interfacial Peptides.</a> Zhao, L. and J. Chmielewski. Bioorganic &amp; Medicinal Chemistry, 2013. 21(14): p. 4041-4044. PMID[23165001].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0705-0718.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319446900039">Curcumin and Its Carbocyclic Analogs: Structure-activity in Relation to Antioxidant and Selected Biological Properties.</a> Bhullar, K.S., A. Jha, D. Youssef, and H.P.V. Rupasinghe. Molecules, 2013. 18(5): p. 5389-5404. ISI[000319446900039].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319398600006">Enantioselective Michael Addition of 4-Hydroxycoumarins to Beta,Gamma-unsaturated Alpha-oxophosphonates Catalysed by a Bifunctional Squaramide Bearing a Structurally Rigid 9,10-Ethylene-9,10-dihydroanthracene Skeleton.</a> Chang, X.F., Q.H. Wang, Y.M. Wang, H.B. Song, Z.H. Zhou, and C.C. Tang. European Journal of Organic Chemistry, 2013(11): p. 2164-2171. ISI[000319398600006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319508600057">Evidence in Support of RNA-mediated Inhibition of Phosphatidylserine-dependent HIV-1 Gag Membrane Binding in Cells.</a> Chukkapalli, V., J. Inlora, G.C. Todd, and A. Ono. Journal of Virology, 2013. 87(12): p. 7155-7159. ISI[000319508600057].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319253500034">Inhibition of HIV-1 Transcription and Replication by a Newly Identified Cyclin T1 Splice Variant.</a> Gao, G.Z., X.Y. Wu, J.Q. Zhou, M.F. He, J.J. He, and D.Y. Guo. Journal of Biological Chemistry, 2013. 288(20): p. 14297-14309. ISI[000319253500034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319307900008">Scaffold-hopping Strategy toward Calanolides with Nitrogen-containing Heterocycles</a>. Guo, X.Y. and G. Liu. Chinese Chemical Letters, 2013. 24(4): p. 295-298. ISI[000319307900008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319437800008">Induced Degradation of Tat by Nucleocapsid (NC) Via the Proteasome Pathway and Its Effect on HIV Transcription.</a> Hong, H.W., S.W. Lee, and H. Myung. Viruses, 2013. 5(4): p. 1143-1152. ISI[000319437800008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.
    <br />
    <br /></p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319858400455">Multi-subtype Targeting, Escape-reducing (MuSTER) anti-HIV ShRNA Gene Therapy.</a> Jung, U. and J.J. Rossi. Molecular Therapy, 2013. 21: p. S176-S176. ISI[000319858400455].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319649800001">Importance of Polar Solvation and Configurational Entropy for Design of Antiretroviral Drugs Targeting HIV-1 Protease.</a> Kar, P., R. Lipowsky, and V. Knecht. Journal of Physical Chemistry B, 2013. 117(19): p. 5793-5805. ISI[000319649800001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319883503398">Inhibition of Endosome Tubulation with Phospholipase A2 Antagonists Prevents Transport of HIV-1 Across Mucosal Vaginal Epithelial Cells.</a> Kinlock, B., Y.D. Wang, and B.D. Liu. FASEB Journal, 2013. 27. ISI[000319883503398].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319109800110">The Effect of Chemical Chaperones on the Assembly and Stability of HIV-1 Capsid Protein</a>. Lampel, A., Y. Bram, M. Levy-Sakin, E. Bacharach, and E. Gazit. PloS one, 2013. 8(4): p. e60867. ISI[000319109800110].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319611300001">Two New 3,4;9,10-Seco-cycloartane Type Triterpenoids from Illicium difengpi and Their Anti-inflammatory Activities.</a> Li, C.T., F.M. Xi, J.L. Mi, Z.J. Wu, and W.S. Chen. Evidence-Based Complementary and Alternative Medicine, 2013. ISI[000319611300001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319858400189">Whole Cell-SELEX Derived anti-CD7 Aptamers for the Delivery of anti-CCR5 Sirna: A Prohylactic Approach to HIV Therapy.</a> Millroy, L., D. Lazar, M. Khati, and M.S. Weinberg. Molecular Therapy, 2013. 21: p. S74-S74. ISI[000319858400189].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320116200033">Thermodynamic Studies of a Series of Homologous HIV-1 TAR RNA Ligands Reveal That Loose Binders Are Stronger Tat Competitors Than Tight Ones.</a> Pascale, L., S. Azoulay, A. Di Giorgio, L. Zenacker, M. Gaysinski, P. Clayette, and N. Patino. Nucleic Acids Research, 2013. 41(11): p. 5851-5863. ISI[000320116200033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319272100035">Identification and Characterization of a Novel HIV-1 Nucleotide-competing Reverse Transcriptase Inhibitor Series.</a> Rajotte, D., S. Tremblay, A. Pelletier, P. Salois, L. Bourgon, R. Coulombe, S. Mason, L. Lamorte, C.F. Sturino, and R. Bethella. Antimicrobial Agents and Chemotherapy, 2013. 57(6): p. 2712-2718. ISI[000319272100035].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320312000147">Poly(ADP-ribose) Polymerase-1 (PARP) Inhibition Decreases HIV-1 Replication in Primary Human Monocyte-Derived Macrophages (MDM).</a> Rom, S., N.L. Reichenbach, and Y. Persidsky. Journal of Neuroimmune Pharmacology, 2013. 8(2): p. 431-432. ISI[000320312000147].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319883502050">Inhibition of HIV-1 Transcription by a Tunable Chimeric tRNA(ser)-nucloelar Localizing Trans-activation Response Element (TAR) Decoy.</a> Stevens, C., L. Scherer, J. Rossi, and K. Haushalter. Faseb Journal, 2013. 27. ISI[000319883502050].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-0718.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319307900003">Neamine-heterocycle Conjugates as Potential anti-HIV Agents.</a> Yan, R.B., Y.F. Wu, J. Liu, X.L. Zhang, and X.S. Ye. Chinese Chemical Letters, 2013. 24(4): p. 273-278. ISI[000319307900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0705-071813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
