

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-100.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CMxSkaatv5hhx7rvMC33vfFTNhWnLIyRsboqzGbx/NI0TDMOF/tyIw6eooFX9KLfSPs0wp+1qu7i2cb7LLeDy6U6LK+FzlChwiFMtrU+8vQvOymiAPzw2BUuG7U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3F695814" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-100-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57800   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          VX-950 (Vertex/Mitsubishi)</p>

    <p>          Summa, V</p>

    <p>          Curr Opin Investig Drugs <b>2005</b>.  6(8): 831-837</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16121690&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16121690&amp;dopt=abstract</a> </p><br />

    <p>2.     57801   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methylene blue photoinactivation abolishes West Nile virus infectivity in vivo</p>

    <p>          Papin, JF, Floyd, RA, and Dittmer, DP</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118025&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118025&amp;dopt=abstract</a> </p><br />

    <p>3.     57802   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A flavonoid from medicinal plants blocks hepatitis B virus-e antigen secretion in HBV-infected hepatocytes</p>

    <p>          Shin, MS, Kang, EH, and Lee, YI</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118024&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16118024&amp;dopt=abstract</a> </p><br />

    <p>4.     57803   DMID-LS-100; WOS-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Drugs - Breakthrough for Hcv Research</p>

    <p>          Jones, S.</p>

    <p>          Nature Reviews Drug Discovery <b>2005</b>.  4(8): 629</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230957400013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230957400013</a> </p><br />

    <p>5.     57804   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Using siRNA in prophylactic and therapeutic regimens against SARS coronavirus in Rhesus macaque</p>

    <p>          Li, BJ, Tang, Q, Cheng, D, Qin, C, Xie, FY, Wei, Q, Xu, J, Liu, Y, Zheng, BJ, Woodle, MC, Zhong, N, and Lu, PY</p>

    <p>          Nat Med <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16116432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16116432&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     57805   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chloroquine is a potent inhibitor of SARS coronavirus infection and spread</p>

    <p>          Vincent, MJ, Bergeron, E, Benjannet, S, Erickson, BR, Rollin, PE, Ksiazek, TG, Seidah, NG, and Nichol, ST</p>

    <p>          Virol J <b>2005</b>.  2(1): 69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115318&amp;dopt=abstract</a> </p><br />

    <p>7.     57806   DMID-LS-100; EMBASE-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity and mode of action of caffeoylquinic acids from Schefflera heptaphylla (L.) Frodin</p>

    <p>          Li, Yaolan, But, Paul PH, and Ooi, Vincent EC</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GSC0Y3-1/2/feab712cc7d322e4d66b1be2c6fada0b">http://www.sciencedirect.com/science/article/B6T2H-4GSC0Y3-1/2/feab712cc7d322e4d66b1be2c6fada0b</a> </p><br />

    <p>8.     57807   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus NS3-4A serine protease inhibitors: Use of a P(2)-P(1) cyclopropyl alanine combination for improved potency</p>

    <p>          Bogen, S, Saksena, AK, Arasappan, A, Gu, H, Njoroge, FG, Girijavallabhan, V, Pichardo, J, Butkiewicz, N, Prongay, A, and Madison, V</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112862&amp;dopt=abstract</a> </p><br />

    <p>9.     57808   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological activity of macrocyclic inhibitors of hepatitis C virus (HCV) NS3 protease</p>

    <p>          Chen, KX, Njoroge, FG, Prongay, A, Pichardo, J, Madison, V, and Girijavallabhan, V</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112859&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112859&amp;dopt=abstract</a> </p><br />

    <p>10.   57809   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mechanism of action of interferon and ribavirin in treatment of hepatitis C</p>

    <p>          Feld, JJ and Hoofnagle, JH</p>

    <p>          Nature <b>2005</b>.  436(7053): 967-972</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107837&amp;dopt=abstract</a> </p><br />

    <p>11.   57810   DMID-LS-100; WOS-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High Throughput Screening Assay for Negative Single Stranded Rna Virus Polymerase Inhibitors</p>

    <p>          Pelet, T. <i>et al.</i></p>

    <p>          Journal of Virological Methods <b>2005</b>.  128(1-2): 29-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231055200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231055200005</a> </p><br />

    <p>12.   57811   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro and in Vivo Enhancement of Ganciclovir-Mediated Bystander Cytotoxicity with Gemcitabine</p>

    <p>          Boucher, PD and Shewach, DS</p>

    <p>          Mol Ther <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107324&amp;dopt=abstract</a> </p><br />

    <p>13.   57812   DMID-LS-100; WOS-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Murine Cytomegalovirus Resistant to Antivirals Has Genetic Correlates With Human Cytomegalovirus</p>

    <p>          Scott, G. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2005</b>.  86: 2141-2151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300003</a> </p><br />

    <p>14.   57813   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Activity of 2&#39;-Deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine, a Potent Inhibitor of Hepatitis C Virus Replication</p>

    <p>          Clark, JL, Hollecker, L, Mason, JC, Stuyver, LJ, Tharnish, PM, Lostia, S, McBrayer, TR, Schinazi, RF, Watanabe, KA, Otto, MJ, Furman, PA, Stec, WJ, Patterson, SE, and Pankiewicz, KW</p>

    <p>          J Med Chem <b>2005</b>.  48(17): 5504-5508</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107149&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107149&amp;dopt=abstract</a> </p><br />

    <p>15.   57814   DMID-LS-100; WOS-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antivirals for Influenza</p>

    <p>          Leophonte, P.</p>

    <p>          Bulletin De L Academie Nationale De Medecine <b>2005</b>.  189(2): 341-355</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230924200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230924200021</a> </p><br />

    <p>16.   57815   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Zanamivir (glaxo wellcome)</p>

    <p>          Oxford, JS</p>

    <p>          IDrugs <b>2000</b>.  3(4): 447-459</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16100701&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16100701&amp;dopt=abstract</a> </p><br />

    <p>17.   57816   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Transcriptional regulation by human papillomaviruses</p>

    <p>          McCance, DJ</p>

    <p>          Curr Opin Genet Dev <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16099158&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16099158&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   57817   DMID-LS-100; WOS-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Novel Test Compounds for Antiviral Chemotherapy of Severe Acute Respiratory Syndrome (Sars)</p>

    <p>          Kesel, A.</p>

    <p>          Current Medicinal Chemistry <b>2005</b>.  12(18): 2095-2162</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16101496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16101496&amp;dopt=abstract</a> </p><br />

    <p>19.   57818   DMID-LS-100; EMBASE-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amino substituted derivatives of 5&#39;-amino-5&#39;-deoxy-5&#39;-noraristeromycin</p>

    <p>          Yang, Minmin and Schneller, Stewart W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(3): 877-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DTKHG9-1/2/6aa36a0434e5ae5e5ecf340a4e7134c9">http://www.sciencedirect.com/science/article/B6TF8-4DTKHG9-1/2/6aa36a0434e5ae5e5ecf340a4e7134c9</a> </p><br />

    <p>20.   57819   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus NS3-4A serine protease inhibitors: SAR of P(2)(&#39;) moiety with improved potency</p>

    <p>          Arasappan, A,  Njoroge, FG, Chan, TY, Bennett, F, Bogen, SL, Chen, K, Gu, H, Hong, L, Jao, E, Liu, YT, Lovey, RG, Parekh, T, Pike, RE , Pinto, P, Santhanam, B, Venkatraman, S, Vaccaro, H, Wang, H, Yang, X, Zhu, Z, McKittrick, B, Saksena, AK, Girijavallabhan, V, Pichardo, J, Butkiewicz, N,  Ingram, R, Malcolm, B, Prongay, A, Yao, N, Marten, B, Madison, V, Kemp, S, Levy, O,  Lim-Wilby, M, Tamura, S, and Ganguly, AK</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(19): 4180-4184</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087332&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087332&amp;dopt=abstract</a> </p><br />

    <p>21.   57820   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prevention and control of influenza. Recommendations of the Advisory Committee on Immunization Practices (ACIP)</p>

    <p>          Harper, SA, Fukuda, K, Uyeki, TM, Cox, NJ, and Bridges, CB</p>

    <p>          MMWR Recomm Rep <b>2005</b>.  54(RR-8): 1-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16086456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16086456&amp;dopt=abstract</a> </p><br />

    <p>22.   57821   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Optimization and validation of a multiplexed luminex assay to quantify antibodies to neutralizing epitopes on human papillomaviruses 6, 11, 16, and 18</p>

    <p>          Dias, D, Van Doren, J, Schlottmann, S, Kelly, S, Puchalski, D, Ruiz, W, Boerckel, P, Kessler, J, Antonello, JM, Green, T, Brown, M, Smith, J, Chirmule, N, Barr, E, Jansen, KU, and Esser, MT</p>

    <p>          Clin Diagn Lab Immunol <b>2005</b>.  12(8): 959-969</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16085914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16085914&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   57822   DMID-LS-100; EMBASE-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral efficacy of VP14637 against respiratory syncytial virus in vitro and in cotton rats following delivery by small droplet aerosol</p>

    <p>          Wyde, Philip R, Laquerre, Sylvie, Chetty, Srikrishna N, Gilbert, Brian E, Nitz, Theodore J, and Pevear, Daniel C</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-6/2/2b971307ef7749b6efd782a90b0d3e33">http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-6/2/2b971307ef7749b6efd782a90b0d3e33</a> </p><br />

    <p>24.   57823   DMID-LS-100; EMBASE-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of catechins in green tea on influenza virus</p>

    <p>          Song, Jae-Min, Lee, Kwang-Hee, and Seong, Baik-Lin</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-4/2/7bb0a7dc4277e077469eee85eb2a0f8a">http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-4/2/7bb0a7dc4277e077469eee85eb2a0f8a</a> </p><br />

    <p>25.   57824   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Enhanced expression of an alpha2,6-linked sialic acid on MDCK cells improves isolation of human influenza viruses and evaluation of their sensitivity to a neuraminidase inhibitor</p>

    <p>          Hatakeyama, S, Sakai-Tagawa, Y, Kiso, M, Goto, H, Kawakami, C, Mitamura, K, Sugaya, N, Suzuki, Y, and Kawaoka, Y</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(8): 4139-4146</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081961&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081961&amp;dopt=abstract</a> </p><br />

    <p>26.   57825   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Emtricitabine (emory university/glaxo wellcome/triangle pharmaceuticals)</p>

    <p>          Daneshtalab, M</p>

    <p>          IDrugs <b>2000</b>.  3(8): 940-952</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059811&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059811&amp;dopt=abstract</a> </p><br />

    <p>27.   57826   DMID-LS-100; PUBMED-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dual regulatory effect of plant extracts of Forsythia suspense on RANTES and MCP-1 secretion in influenza A virus-infected human bronchial epithelial cells</p>

    <p>          Ko, HC, Wei, BL, and Chiou, WF</p>

    <p>          J Ethnopharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054313&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054313&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   57827   DMID-LS-100; EMBASE-DMID-8/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a homogeneous screening assay for automated detection of antiviral agents active against severe acute respiratory syndrome-associated coronavirus</p>

    <p>          Ivens, Tania, Eynde, Christel Van den, Acker, Koen Van, Nijs, Erik, Dams, Gery, Bettens, Eva, Ohagen, Asa, Pauwels, Rudi, and Hertogs, Kurt</p>

    <p>          Journal of Virological Methods <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4GD4S72-4/2/56455a68f8bf81bc1baf55478457e2dc">http://www.sciencedirect.com/science/article/B6T96-4GD4S72-4/2/56455a68f8bf81bc1baf55478457e2dc</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
