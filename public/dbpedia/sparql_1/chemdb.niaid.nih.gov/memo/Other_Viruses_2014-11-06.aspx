

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-11-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="HR1YUUAnjIqYxmk+PH2o3tWABHP7mxdss526LTzZQUc8cL7mJtNY7MrOOXJ+E/xz2darsCYMD2pIc2UM1OzbBRmFKtyGkNS82pYoX5h7+4mKxWHG0fb2TtGhisY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="60177512" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: October 24 - November 6, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1<a href="http://www.ncbi.nlm.nih.gov/pubmed/25150190">. Anti-viral Effect of a Compound Isolated from Liriope platyphylla Against Hepatitis B Virus in Vitro</a>. Huang, T.J., Y.C. Tsai, S.Y. Chiang, G.J. Wang, Y.C. Kuo, Y.C. Chang, Y.Y. Wu, and Y.C. Wu. Virus Research, 2014. 192: p. 16-24. PMID[25150190].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24338503">In Vitro Evaluation of 9-(2-Phosphonylmethoxyethyl)adenine Ester Analogues, a Series of anti-HBV Structures with Improved Plasma Stability and Liver Release.</a> Liao, S., S.Y. Fan, Q. Liu, C.K. Li, J. Chen, J.L. Li, Z.W. Zhang, Z.Q. Zhang, B.H. Zhong, and J.W. Xie. Archives of Pharmacology Research, 2014. 37(11): p. 1416-1425. PMID[24338503].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25174447">An Aptamer Targets HBV Core Protein and Suppresses HBV Replication in HepG2.2.15 Cells.</a> Zhang, Z., J. Zhang, X. Pei, Q. Zhang, B. Lu, X. Zhang, and J. Liu. International Journal of Molecular Medicine, 2014. 34(5): p. 1423-1429. PMID[25174447].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25357246">Identification of a Novel Drug Lead That Inhibits HCV Infection and Cell-to-cell Transmission by Targeting the HCV E2 Glycoprotein.</a> Al Olaby, R.R., L. Cocquerel, A. Zemla, L. Saas, J. Dubuisson, J. Vielmetter, J. Marcotrigiano, A.G. Khan, F.V. Catalan, A.L. Perryman, J.S. Freundlich, S. Forli, S. Levy, R. Balhorn, and H.M. Azzazy. PLoS One, 2014. 9(10): p. e111333. PMID[25357246].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25043937">Breadth of Neutralization and Synergy of Clinically Relevant Human Monoclonal Antibodies Against HCV Genotypes 1a, 1b, 2a, 2b, 2c, and 3a.</a> Carlsen, T.H., J. Pedersen, J.C. Prentoe, E. Giang, Z.Y. Keck, L.S. Mikkelsen, M. Law, S.K. Foung, and J. Bukh. Hepatology, 2014. 60(5): p. 1551-1562. PMID[25043937].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25155588">Clinical and in vitro Resistance to GS-9669, a Thumb Site II Nonnucleoside Inhibitor of the Hepatitis C Virus NS5B Polymerase.</a> Dvory-Sobol, H., C. Voitenleitner, E. Mabery, T. Skurnac, E.J. Lawitz, J. McHutchison, E.S. Svarovskaia, W. Delaney, M.D. Miller, and H. Mo. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6599-6606. PMID[25155588].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25310383">Potent Nonimmunosuppressive Cyclophilin Inhibitors with Improved Pharmaceutical Properties and Decreased Transporter Inhibition.</a> Fu, J., M. Tjandra, C. Becker, D. Bednarczyk, M. Capparelli, R. Elling, I. Hanna, R. Fujimoto, M. Furegati, S. Karur, T. Kasprzyk, M. Knapp, K. Leung, X. Li, P. Lu, W. Mergo, C. Miault, S. Ng, D. Parker, Y. Peng, S. Roggo, A. Rivkin, R.L. Simmons, M. Wang, B. Wiedmann, A.H. Weiss, L. Xiao, L. Xie, W. Xu, A. Yifru, S. Yang, B. Zhou, and Z.K. Sweeney. Journal of Medicinal Chemistry, 2014. 57(20): p. 8503-8516. PMID[25310383].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25254429">Design of Novel Rho Kinase Inhibitors Using Energy Based Pharmacophore Modeling, Shape-based Screening, in Silico Virtual Screening, and Biological Evaluation.</a> Mishra, R.K., R. Alokam, S.M. Singhal, G. Srivathsav, D. Sriram, N. Kaushik-Basu, D. Manvar, and P. Yogeeswari. Journal of Chemical Information and Modeling, 2014. 54(10): p. 2876-2886. PMID[25254429].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25053565">Endocannabinoid CB1 Antagonists Inhibit Hepatitis C Virus Production, Providing a Novel Class of Antiviral Host-targeting Agents.</a> Shahidi, M., E.S. Tay, S.A. Read, M. Ramezani-Moghadam, K. Chayama, J. George, and M.W. Douglas. The Journal of General Virology, 2014. 95(Pt 11): p. 2468-2479. PMID[25053565].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24797654">Successful Anti-scavenger Receptor Class B Type I (SR-BI) Monoclonal Antibody Therapy in Humanized Mice after Challenge with HCV Variants with in Vitro Resistance to SR-BI-targeting Agents.</a> Vercauteren, K., N. Van Den Eede, A.A. Mesalam, S. Belouzard, M.T. Catanese, D. Bankwitz, F. Wong-Staal, R. Cortese, J. Dubuisson, C.M. Rice, T. Pietschmann, G. Leroux-Roels, A. Nicosia, and P. Meuleman. Hepatology, 2014. 60(5): p. 1508-1518. PMID[24797654].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25358999">Antiviral Activity of Basidiomycete Mycelia against Influenza Type A (Serotype H1N1) and Herpes Simplex Virus Type 2 in Cell Culture.</a> Krupodorova, T., S. Rybalko, and V. Barshteyn. Virologica Sinica, 2014. 29(5): p. 284-290. PMID[25358999].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25087911">Antiherpes Simplex Virus Type 2 Activity of the Antimicrobial Peptide Subtilosin.</a> Quintana, V.M., N.I. Torres, M.B. Wachsman, P.J. Sinko, V. Castilla, and M. Chikindas. Journal of Applied Microbiology, 2014. 117(5): p. 1253-1259. PMID[25087911].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25354024">Short Communication: A Repeated Simian Human Immunodeficiency Virus Reverse Transcriptase/Herpes Simplex Virus Type 2 Cochallenge Macaque Model for the Evaluation of Microbicides.</a> Kenney, J., N. Derby, M. Aravantinou, K. Kleinbeck, I. Frank, A. Gettie, B. Grasperge, J. Blanchard, M. Piatak, Jr., J.D. Lifson, T.M. Zydowsky, and M. Robbiani. AIDS Research and Human Retroviruses, 2014. 30(11): p. 1117-1124. PMID[25354024].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1024-110614.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342033600028">A Neutralizing Anti-gH/gL Monoclonal Antibody is Protective in the Guinea Pig Model of Congenital CMV Infection.</a> Auerbach, M.R., D.H. Yan, R. Vij, J.A. Hongo, G. Nakamura, J.M. Vernes, Y.G. Meng, S. Lein, P. Chan, J. Ross, R. Carano, R. Deng, N. Lewin-Koh, M. Xu, and B. Feierbach. PLoS Pathogens, 2014. 10(4): p. e1004060. ISI[000342033600028].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1024-110614.</p><br />    

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342871800041">Syntheses of Nucleosides with a 1&#39;,2&#39;-Beta-lactam Moiety as Potential Inhibitors of Hepatitis C Virus NS5B Polymerase.</a> Dang, Q., Z.B. Zhang, Y.F. Bai, R.J. Sun, J. Yin, T.Q. Chen, S. Bogen, V. Girijavallabhan, D.B. Olsen, and P.T. Meinke. Tetrahedron Letters, 2014. 55(40): p. 5576-5579. ISI[000342871800041].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1024-110614.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343126700007">Novel Indole Derivatives as Hepatitis C Virus NS5B Polymerase Inhibitors: Pharmacophore Modeling and 3D QSAR Studies.</a> Varun, G., M. Lokesh, M. Sandeep, S. Shahbazi, and G.D. Reddy. Bangladesh Journal of Pharmacology, 2014. 9(3): p. 290-297. ISI[000343126700007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1024-110614.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
