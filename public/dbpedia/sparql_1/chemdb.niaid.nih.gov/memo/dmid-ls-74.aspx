

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-74.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LZRtAh/bo7wQmCMOQkDIc8CXFhnON1mnZuPjl+jcVsWag3EvPxgotd+/3/uYFuMIrqBACKESVMe308HKQgiAIDMBNb1wAoDvc8Gb/H5XEK0ZYwpFVQWsNROnjfk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CB70092D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-74-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6340   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          2-Substituted benzoxazinone analogues as anti-human coronavirus (anti-HCoV) and ICAM-1 expression inhibition agents</p>

    <p>          Hsieh, PW, Chang, FR, Chang, CH, Cheng, PW, Chiang, LC, Zeng, FL, Lin, KH, and Wu, YC</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(18): 4751-4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15324901&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15324901&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>2.         6341   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Isolation of Specific Against Ns3 Helicase and High-Affinity Rna Aptamers Domain of Hepatitis C Virus</p>

    <p>          Hwang, B. <i>et al.</i></p>

    <p>          Rna-a Publication of the Rna Society <b>2004</b>.  10(8): 1277-1290</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>3.         6342   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          The flavivirus-conserved penta-nucleotide in the 3&#39; stem-loop of the West Nile virus genome requires a specific sequence and structure for RNA synthesis, but not for viral translation</p>

    <p>          Tilgner, Mark, Deas, Tia S, and Shi, Pei-Yong</p>

    <p>          Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4D6360P-1/2/8a0f2ba8ab85c31505789165121dbe3b">http://www.sciencedirect.com/science/article/B6WXR-4D6360P-1/2/8a0f2ba8ab85c31505789165121dbe3b</a> </p><br />

    <p>4.         6343   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Structure and Function of the 3 &#39; Terminal Six Nucleotides of the West Nile Virus Genome in Viral Replication</p>

    <p>          Tilgner, M. and Shi, P.</p>

    <p>          Journal of Virology <b>2004</b>.  78(15): 8159-8171</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>5.         6344   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Inhibitors of 3C cysteine proteinases from Picornaviridae</p>

    <p>          Lall, MS, Jain, RP, and Vederas, JC</p>

    <p>          Curr Top Med Chem <b>2004</b>.  4(12): 1239-53</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320724&amp;dopt=abstract</a> </p><br />

    <p>6.         6345   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Binding of the antiviral drug WIN51711 to the sabin strain of type 3 poliovirus: structural comparison with drug binding in rhinovirus 14</p>

    <p>          Hiremath, CN</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>1995</b>.  51(Pt 4): 473-89</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299834&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299834&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.         6346   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          The crystal structure of the RNA-dependent RNA polymerase from human rhinovirus: a dual function target for common cold antiviral therapy</p>

    <p>          Love, RA, Maegley, KA, Yu, X, Ferre, RA, Lingardo, LK, Diehl, W, Parge, HE, Dragovich, PS, and Fuhrman, SA</p>

    <p>          Structure (Camb) <b>2004</b>.  12(8): 1533-44</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296746&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296746&amp;dopt=abstract</a> </p><br />

    <p>8.         6347   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Anti-HSV activity of lactoferrin and lactoferricin is dependent on the presence of heparan sulphate at the cell surface</p>

    <p>          Andersen, JH, Jenssen, H, Sandvik, K, and Gutteberg, TJ</p>

    <p>          J Med Virol <b>2004</b>.  74(2): 262-71</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15332275&amp;dopt=abstract</a> </p><br />

    <p>9.         6348   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Valganciclovir: oral prevention and treatment of cytomegalovirus in the immunocompromised host</p>

    <p>          Freeman, RB</p>

    <p>          Expert Opin Pharmacother <b>2004</b>.  5(9): 2007-16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15330737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15330737&amp;dopt=abstract</a> </p><br />

    <p>10.       6349   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Structural Basis for Coronavirus-Mediated Membrane Fusion - Crystal Structure of Mouse Hepatitis Virus Spike Protein Fusion Core</p>

    <p>          Xu, Y. <i>et al.</i></p>

    <p>          Journal of Biological Chemistry <b> 2004</b>.  279(29): 30514-30522</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>11.       6350   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Non-Nucleoside Inhibitors of the Hcv Polymerase</p>

    <p>          Sarisky, R.</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2004</b>.  54(1): 14-16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>12.       6351   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Antiviral property and mode of action of a sulphated polysaccharide from Sargassum patens against herpes simplex virus type 2</p>

    <p>          Zhu, W, Chiu, LC, Ooi, VE, Chan, PK, and Ang, PO Jr</p>

    <p>          Int J Antimicrob Agents <b>2004</b>.  24(3): 279-83</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15325432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15325432&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.       6352   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Epstein-Barr virus and human herpesvirus type 8 infections of the central nervous system</p>

    <p>          Volpi, A</p>

    <p>          Herpes <b>2004</b>.  11 Suppl 2: 120A-127A</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319099&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319099&amp;dopt=abstract</a> </p><br />

    <p>14.       6353   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Prevention of herpes simplex virus type 2 transmission with antiviral therapy</p>

    <p>          Corey, L and Ashley, R</p>

    <p>          Herpes <b>2004</b>.  11 Suppl 3: 170A-4A</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319087&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15319087&amp;dopt=abstract</a> </p><br />

    <p>15.       6354   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Biophysical and mutational analysis of the putative bZIP domain of Epstein-Barr virus EBNA 3C</p>

    <p>          West, MJ, Webb, HM, Sinclair, AJ, and Woolfson, DN</p>

    <p>          J Virol <b>2004</b>.  78(17): 9431-45</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308737&amp;dopt=abstract</a> </p><br />

    <p>16.       6355   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Glycan Arrays Lead to the Discovery of Autoimmunogenic Activity of Sars-Cov</p>

    <p>          Wang, D. and Lu, J.</p>

    <p>          Physiological Genomics <b>2004</b>.  18(2): 245-248</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       6356   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Roscovitine inhibits activation of promoters in herpes simplex virus type 1 genomes independently of promoter-specific factors</p>

    <p>          Diwan, P, Lacasse, JJ, and Schang, LM</p>

    <p>          J Virol <b>2004</b>.  78(17): 9352-65</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308730&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15308730&amp;dopt=abstract</a> </p><br />

    <p>18.       6357   DMID-LS-74; PUBMED-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Antiviral activity of medicinal plants of Nilgiris</p>

    <p>          Vijayan, P, Raghu, C, Ashok, G, Dhanaraj, SA, and Suresh, B</p>

    <p>          Indian J Med Res <b>2004</b>.  120(1): 24-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15299228&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.       6358   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Interleukin-28 and 29 (Il-28 and Il-29): New Cytokines With Anti-Viral Activities</p>

    <p>          Kempuraj, D. <i>et al.</i></p>

    <p>          International Journal of Immunopathology and Pharmacology <b>2004</b>.  17(2): 103-106</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       6359   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Recent Advances in the Discovery and Development of Anti-Influenza Drugs</p>

    <p>          Klumpp, K.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2004</b>.  14(8): 1153-1168</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>21.       6360   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Recent Development in Human Papillomavirus Vaccines</p>

    <p>          Stern, P.</p>

    <p>          Expert Opinion on Investigational Drugs <b>2004</b>.  13(8): 959-971</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>22.       6361   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Correlation of Redox Potentials and Inhibitory Effects on Epstein-Barr Virus Activation of 2-Azaanthraquinones</p>

    <p>          Koyama, J. <i>et al.</i></p>

    <p>          Cancer Letters <b>2004</b>.  212(1): 1-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>23.       6362   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Anti-Hhv-8/Kshv Antibodies in Infected Individuals Inhibit Infection in Vitro</p>

    <p>          Dialyna, I. <i>et al.</i></p>

    <p>          Aids <b>2004</b>.  18(9): 1263-1270</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       6363   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Development of a Safe Neutralization Assay for Sars-Cov and Characterization of S-Glycoprotein</p>

    <p>          Han, D. <i>et al.</i></p>

    <p>          Virology <b>2004</b>.  326(1): 140-149</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       6364   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          A Phosphorothioate Antisense Oligodeoxynucleotide Specifically Inhibits Coxsackievirus B3 Replication in Cardiomyocytes and Mouse Hearts</p>

    <p>          Yuan, J. <i>et al.</i></p>

    <p>          Laboratory Investigation <b>2004</b>.  84(6): 703-714</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       6365   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Antiviral Treatment of Genital Herpes</p>

    <p>          Apoola, A. and Radcliffe, K.</p>

    <p>          International Journal of Std &amp; Aids <b>2004</b>.  15(7): 429-433</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>27.       6366   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Current status of antiviral therapy for juvenile-onset recurrent respiratory papillomatosis</p>

    <p>          Kimberlin, David W</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4D5875R-1/2/45f11717a5b40b29ce73250193341dec">http://www.sciencedirect.com/science/article/B6T2H-4D5875R-1/2/45f11717a5b40b29ce73250193341dec</a> </p><br />

    <p>28.       6367   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Insights Into the Viral G Protein-Coupled Receptor Encoded by Human Herpesvirus Type 8 (Hhv-8)</p>

    <p>          Couty, J. and  Gershengorn, M.</p>

    <p>          Biology of the Cell <b>2004</b>.  96(5): 349-354</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>29.       6368   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Peptide Inhibitors of Virus-Cell Fusion: Enfuvirtide as a Case Study in Clinical Discovery and Development</p>

    <p>          Cooper, D. and Lange, J.</p>

    <p>          Lancet Infectious Diseases <b>2004</b>.  4(7): 426-436</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>30.       6369   DMID-LS-74; WOS-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Antiviral Activity Against Human Immunodeficiency Virus-1 in Vitro by Myristoylated-Peptide From Heliothis Vireseens</p>

    <p>          Ourth, D.</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  320(1): 190-196</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       6370   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          A phase II, double-masked, randomized, placebo-controlled evaluation of a human monoclonal anti-Cytomegalovirus antibody (MSL-109) in combination with standard therapy versus standard therapy alone in the treatment of AIDS patients with Cytomegalovirus retinitis</p>

    <p>          Borucki, Michael J, Spritzler, John, Asmuth, David M, Gnann, John, Hirsch, Martin S, Nokta, Mostafa, Aweeka, Francesca, Nadler, Paul I, Sattler, Fred, and Alston, Beverly</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4D5P0H2-1/2/f60119a78e5b51749a9fab7f84a9e68b">http://www.sciencedirect.com/science/article/B6T2H-4D5P0H2-1/2/f60119a78e5b51749a9fab7f84a9e68b</a> </p><br />

    <p>32.       6371   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of 1&#39;-carbon-substituted 4&#39;-thiothymidines</p>

    <p>          Haraguchi, Kazuhiro, Takahashi, Haruhiko, Tanaka, Hiromichi, Hayakawa, Hiroyuki, Ashida, Noriyuki, Nitanda, Takao, and Baba, Masanori</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4D5P21H-1/2/2f6f9d923c5b183d572e3ce245aa30cc">http://www.sciencedirect.com/science/article/B6TF8-4D5P21H-1/2/2f6f9d923c5b183d572e3ce245aa30cc</a> </p><br />
    <br clear="all">

    <p>33.       6372   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Synthesis, antitumor and antiviral properties of some 1,2,4-triazole derivatives</p>

    <p>          Al-Soud, Yaseen A, Al-Dweri, Mohammad N, and Al-Masoudi, Najim A</p>

    <p>          Il Farmaco <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4D58GNN-3/2/29aa1e01c93683101c8a4da69b8fd470">http://www.sciencedirect.com/science/article/B6VJ8-4D58GNN-3/2/29aa1e01c93683101c8a4da69b8fd470</a> </p><br />

    <p>34.       6373   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Broad specificity of human phosphoglycerate kinase for antiviral nucleoside analogs</p>

    <p>          Gallois-Montbrun, Sarah, Faraj, Abdesslem, Seclaman, Edward, Sommadossi, Jean-Pierre, Deville-Bonne, Dominique, and Veron, Michel</p>

    <p>          Biochemical Pharmacology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4D4R5KM-1/2/5b1e51eadd35beb0d502279ad78439f4">http://www.sciencedirect.com/science/article/B6T4P-4D4R5KM-1/2/5b1e51eadd35beb0d502279ad78439f4</a> </p><br />
    <br clear="all">

    <p>35.       6374   DMID-LS-74; EMBASE-DMID-9/1/2004</p>

    <p class="memofmt1-2">          Antivirals against DNA viruses (hepatitis B and the herpes viruses)</p>

    <p>          Hewlett, Guy, Hallenberger, Sabine, and Rubsamen-Waigmann, Helga</p>

    <p>          Current Opinion in Pharmacology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>HYPERLINK:</p><p><a href="http://www.sciencedirect.com/science/article/B6W7F-4D39Y8J-6/2/972798847dc2f357f6ac4556bdc626ac">http://www.sciencedirect.com/science/article/B6W7F-4D39Y8J-6/2/972798847dc2f357f6ac4556bdc626ac</a></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
