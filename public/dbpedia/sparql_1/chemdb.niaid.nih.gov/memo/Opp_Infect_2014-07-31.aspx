

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-07-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kYAPrfhWbmNmNVkwOgqna/IFKZZFhaJDzk2SdvRxaO69GNpy98+OJgIUu/LF18xkivYYCUK3GBGCzptWF08xvrRWK4Iaj9Ae/9GI+FunzFyPqsrnrk/PgoTfjHE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F2A50B93" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 18 - July 31, 2014</h1>

    <h2>ANTIFUNGAL (cryptococcus, aspergillus, candida, histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25064350">Discovery of Diethyl 2,5-diaminothiophene-3,4-dicarboxylate Derivatives as Potent Anticancer and Antimicrobial Agents and Screening of Anti-diabetic Activity: Synthesis and in Vitro Biological Evaluation. Part 1.</a> Bozorov, K., H.R. Ma, J.Y. Zhao, H.Q. Zhao, H. Chen, K. Bobakulov, X.L. Xin, B. Elmuradov, K. Shakhidoyatov, and H.A. Aisa. European Journal of Medicinal Chemistry, 2014. 84C: p. 739-745. PMID[25064350].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25037059">Assessment of in Vivo Antimicrobial Activity of the Carbene Silver(I) Acetate Derivative Sbc3 Using Galleria mellonella Larvae.</a> Browne, N., F. Hackenberg, W. Streciwilk, M. Tacke, and K. Kavanagh. Biometals, 2014. 27(4): p. 745-752. PMID[25037059].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25026191">Antifungal Terpenoids from Hyalis argentea var. Latisquama.</a> Fernandez, L.R., E. Butassi, L. Svetaz, S.A. Zacchino, J.A. Palermo, and M. Sanchez. Journal of Natural Products, 2014. 77(7): p. 1579-1585. PMID[25026191].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25032708">Screening and Characterisation of Antimicrobial Properties of Semisynthetic Betulin Derivatives.</a> Haque, S., D.A. Nawrot, S. Alakurtti, L. Ghemtio, J. Yli-Kauhaluoma, and P. Tammela. PloS One, 2014. 9(7): p. e102696. PMID[25032708].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25014026">Antifungal Activity of Resveratrol Derivatives against Candida Species.</a> Houille, B., N. Papon, L. Boudesocque, E. Bourdeaud, S. Besseau, V. Courdavault, C. Enguehard-Gueiffier, G. Delanoue, L. Guerin, J.P. Bouchara, M. Clastre, N. Giglioli-Guivarc&#39;h, J. Guillard, and A. Lanoue. Journal of Natural Products, 2014. 77(7): p. 1658-1662. PMID[25014026].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24934573">Design, Synthesis and Antifungal Activity of Novel Triazole Derivatives Containing Substituted 1,2,3-Triazole-piperdine Side Chains.</a> Jiang, Z., J. Gu, C. Wang, S. Wang, N. Liu, Y. Jiang, G. Dong, Y. Wang, Y. Liu, J. Yao, Z. Miao, W. Zhang, and C. Sheng. European Journal of Medicinal Chemistry, 2014. 82: p. 490-497. PMID[24934573].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24837702">Hydrophobicity and Helicity Regulate the Antifungal Activity of 14-Helical Beta-peptides.</a> Lee, M.R., N. Raman, S.H. Gellman, D.M. Lynn, and S.P. Palecek. ACS Chemical Biology, 2014. 9(7): p. 1613-1621. PMID[24837702].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25058485">Synergistic Antifungal Effect of Glabridin and Fluconazole.</a> Liu, W., L.P. Li, J.D. Zhang, Q. Li, H. Shen, S.M. Chen, L.J. He, L. Yan, G.T. Xu, M.M. An, and Y.Y. Jiang. Plos One, 2014. 9(7): p. e103442. PMID[25058485].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25055158">Chemical Composition and Antifungal Activity of Supercritical Extract and Essential Oil of Tanacetum vulgare Growing Wild in Lithuania.</a> Piras, A., D. Falconieri, E. Bagdonaite, A. Maxia, M.J. Goncalves, C. Cavaleiro, L. Salgueiro, and S. Porcedda. Natural Product Research, 2014: p. 1-4. PMID[25055158].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24929343">Microwave-assisted Synthesis of C-8 Aryl and Heteroaryl Inosines and Determination of Their Inhibitory Activities against Plasmodium falciparum Purine Nucleoside Phosphorylase.</a> Gigante, A., E.M. Priego, P. Sanchez-Carrasco, L.M. Ruiz-Perez, J. Vande Voorde, M.J. Camarasa, J. Balzarini, D. Gonzalez-Pacanowska, and M.J. Perez-Perez. European Journal of Medicinal Chemistry, 2014. 82: p. 459-465. PMID[24929343].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24904967">Discovery of HDAC Inhibitors with Potent Activity against Multiple Malaria Parasite Life Cycle Stages.</a> Hansen, F.K., S.D. Sumanadasa, K. Stenzel, S. Duffy, S. Meister, L. Marek, R. Schmetter, K. Kuna, A. Hamacher, B. Mordmuller, M.U. Kassack, E.A. Winzeler, V.M. Avery, K.T. Andrews, and T. Kurz. European Journal of Medicinal Chemistry, 2014. 82: p. 204-213. PMID[24904967].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24992637">Triterpene Lactones from Cultures of Ganoderma Sp. KM01.</a> Lakornwong, W., K. Kanokmedhakul, S. Kanokmedhakul, P. Kongsaeree, S. Prabpai, P. Sibounnavong, and K. Soytong. Journal of Natural Products, 2014. 77(7): p. 1545-1553. PMID[24992637].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25062007">Beta-amino-alcohol Tethered 4-Aminoquinoline-Isatin Conjugates: Synthesis and Antimalarial Evaluation.</a> Nisha, J. Gut, P.J. Rosenthal, and V. Kumar. European Journal of Medicinal Chemistry, 2014. 84C: p. 566-573. PMID[25062007].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24927053">1-Substituted-5-[(3,5-dinitrobenzyl)sulfanyl]-1H-tetrazoles and Their Isosteric Analogs: A New Class of Selective Antitubercular Agents Active against Drug-susceptible and Multidrug-resistant Mycobacteria.</a> Karabanovich, G., J. Roh, T. Smutny, J. Nemecek, P. Vicherek, J. Stolarikova, M. Vejsova, I. Dufkova, K. Vavrova, P. Pavek, V. Klimesova, and A. Hrabalek. European Journal of Medicinal Chemistry, 2014. 82: p. 324-340. PMID[24927053].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25062011">Design, Synthesis and Antimycobacterial Evaluation of 1-(4-(2-Substitutedthiazol-4-yl)phenethyl)-4-(3-(4-substitutedpiperazin-1-yl)alky l)piperazine Hybrid Analogues.</a> Nagesh, H.N., A. Suresh, S.D. Sairam, D. Sriram, P. Yogeeswari, and K.V. Chandra Sekhar. European Journal of Medicinal Chemistry, 2014. 84C: p. 605-613. PMID[25062011].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0718-073114.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338058400001">Synthesis of New [1,2,4]Triazolo[3,4-b][1,3,4]thiadiazines and Study of Their Anti-candidal and Cytotoxic Activities.</a> Bhat, M.A., A.A. Khan, S. Khan, and A. Al-Dhfyan. Journal of Chemistry, 2014 . 897141: 7 pp. ISI[000338058400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337965700052">Small Molecule-directed Immunotherapy against Recurrent Infection by Mycobacterium tuberculosis.</a> Bhattacharya, D., V.P. Dwivedi, M. Maiga, M. Maiga, L. Van Kaer, W.R. Bishai, and G. Das. Journal of Biological Chemistry, 2014. 289(23): p. 16508-16515. ISI[000337965700052].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337908500013">Silver Nanoparticle Production by the Fungus Fusarium oxysporum: Nanoparticle Characterisation and Analysis of Antifungal Activity against Pathogenic Yeasts.</a> Ishida, K., T.F. Cipriano, G.M. Rocha, G. Weissmueller, F. Gomes, K. Miranda, and S. Rozental. Memorias do Instituto Oswaldo Cruz, 2014. 109(2): p. 220-228. ISI[000337908500013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337771500012">Antibacterial and Antifungal Activities of Brown Alga Zonaria tournefortii (JV Lamouroux).</a> Ismail, A., K.B.H. Salah, M. Ahmed, M. Mastouri, A. Bouraoui, and M. Aouni. Allelopathy Journal, 2014. 34(1): p. 143-153. ISI[000337771500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337940500004">Rational Design of InhA Inhibitors in the Class of Diphenyl ether Derivatives as Potential Anti-tubercular Agents Using Molecular Dynamics Simulations.</a> Kamsri, P., N. Koohatammakun, A. Srisupan, P. Meewong, A. Punkvang, P. Saparpakorn, S. Hannongbua, P. Wolschann, S. Prueksaaroon, U. Leartsakulpanich, and P. Pungpo. SAR and QSAR in Environmental Research, 2014. 25(6): p. 473-488. ISI[000337940500004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338276300035">Biochemical Characterization of Quinolinic acid phosphoribosyltransferase from Mycobacterium tuberculosis H37Rv and Inhibition of Its Activity by Pyrazinamide.</a> Kim, H., K. Shibayama, E. Rimbara, and S. Mori. PLoS One, 2014. 9(6): p. e100062. ISI[000338276300035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338095300036">Chemical Composition and Antimicrobial Activity of the Essential Oil from Allium hookeri Consumed in Xishuangbanna, Southwest China.</a> Li, R., Y.F. Wang, Q. Sun, and H.B. Hu. Natural Product Communications, 2014. 9(6): p. 863-864. ISI[000338095300036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337775100180">Some New Cadmium Complexes: Antibacterial/Antifungal Activity and Thermal Behavior.</a> Montazerozohori, M., S. Zahedi, M. Nasr-Esfahani, and A. Naghiha. Journal of Industrial and Engineering Chemistry, 2014. 20(4): p. 2463-2470. ISI[000337775100180].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337919500035">Antifungal Drug Susceptibility and Phylogenetic Diversity among Cryptococcus Isolates from Dogs and Cats in North America.</a>. Singer, L.M., W. Meyer, C. Firacative, G.R.T. Ill, E. Samitz, and J.E. Sykes. Journal of Clinical Microbiology, 2014. 52(6): p. 2061-2070. ISI[000337919500035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337946800003">Synthesis and Characterization of Chloralose-derived Thiosemicarbazones and Semicarbazones and Investigation of Their Antimicrobial Properties.</a> Yetgin, C.E., M. Oskay, and K. Ay. Journal of Carbohydrate Chemistry, 2014. 33(5): p. 238-251. ISI[000337946800003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337989800005">Synthesis, Spectral Characterization, Molecular Modeling and Antimicrobial Activity Studies on 2-Aminopyridine-Cyclodiphosph(V)azane Derivative and Its Homo-binuclear Zinc(II) Complexes.</a> Alaghaz, A.N.M.A. Journal of Molecular Structure, 2014. 1068: p. 27-42. ISI[000337989800005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337862300053">Biopesticides from Plants: Calceolaria integrifolia S.L.</a> Cespedes, C.L., J.R. Salazar, A. Ariza-Castolo, L. Yamaguchi, J.G. Avila, P. Aqueveque, I. Kubo, and J. Alarcon. Environmental Research, 2014. 132: p. 391-406. ISI[000337862300053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337878000015">Antifungal Activity of Flavonoids from Heteropterys byrsonimifolia and a Commercial Source against Aspergillus ochraceus: In Silico Interactions of These Compounds with a Protein Kinase.</a> Santos Junior, H.M., V.A.C. Campos, D.S. Alves, A.J. Cavalheiro, L.P. Souza, D.M.S. Botelho, S.M. Chalfoun, and D.F. Oliveira. Crop Protection, 2014. 62: p. 107-114. ISI[000337878000015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337853900016">Antifungal Activity of Selected Essential Oils and Biocide Benzalkonium chloride against the Fungi Isolated from Cultural Heritage Objects.</a> Stupar, M., M.L. Grbic, A. Dzamic, N. Unkovic, M. Ristic, A. Jelikic, and J. Vukojevic. South African Journal of Botany, 2014. 93: p. 118-124. ISI[000337853900016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337589100005">Bovine Pancreatic Trypsin Inhibitor Is a New Antifungal Peptide That Inhibits Cellular Magnesium Uptake.</a> Bleackley, M.R., B.M. Hayes, K. Parisi, T. Saiyed, A. Traven, I.D. Potter, N.L. van der Weerden, and M.A. Anderson. Molecular microbiology, 2014. 92(6): p. 1188-1197. ISI[000337589100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337692800007">Anti-tuberculosis Evaluation and Conformational Study of N-Acylhydrazones Containing the Thiophene Nucleus.</a> Cardoso, L.N.F., M.L.F. Bispo, C.R. Kaiser, J.L. Wardell, S.M.S.V. Wardell, M.C.S. Lourenco, F.A.F.M. Bezerra, R.P.P. Soares, M.N. Rocha, and M.V.N. de Souza. Archiv der Pharmazie, 2014. 347(6): p. 432-448. ISI[000337692800007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337560000028">Synthesis and Antimicrobial Activity Evaluation of Novel Oxadiazino/Thiadiazino-Indole and Oxadiazole/Thiadiazole Derivatives of 2-Oxo-2H-benzopyran.</a> Deokar, H., J. Chaskar, and A. Chaskar. Journal of Heterocyclic Chemistry, 2014. 51(3): p. 719-725. ISI[000337560000028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337358700007">Synthesis of Novel Imidazo[1,2-a]pyridines and Evaluation of Their Antifungal Activities.</a> Goktas, F., N. Cesur, D. Satana, and M. Uzun. Turkish Journal of Chemistry, 2014. 38(4): p. 581-591. ISI[000337358700007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337720100024">High-resolution Screening Combined with HPLC-HRMS-SPE-NMR for Identification of Fungal Plasma Membrane H+-ATPase Inhibitors from Plants.</a> Kongstad, K.T., S.G. Wubshet, A. Johannesen, L. Kjellerup, A.-M.L. Winther, A.K. Jager, and D. Staerk. Journal of Agricultural and Food Chemistry, 2014. 62(24): p. 5595-5602. ISI[000337720100024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337560000029">Synthesis of New (Pyrazol-3-yl)-1,3,4-oxadiazole Derivatives by Unexpected Aromatization During Oxidative Cyclization of 4,5-Dihydro-1H-pyrazole-3-carbohydrazones and Their Biological Activities.</a> Prathap, K.J., M. Himaja, S.V. Mali, and D. Munirajasekhar. Journal of Heterocyclic Chemistry, 2014. 51(3): p. 726-732. ISI[000337560000029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337293000005">Thionin-like Peptides from Capsicum annuum Fruits with High Activity against Human Pathogenic Bacteria and Yeasts.</a> Taveira, G.B., L.S. Mathias, O.V. da Motta, O.L.T. Machado, R. Rodrigues, A.O. Carvalho, A. Teixeira-Ferreira, J. Perales, I.M. Vasconcelos, and V.M. Gomes. Biopolymers, 2014. 102(1): p. 30-39. ISI[000337293000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337805200001">Coordination Behaviour of N &#39;(1), N &#39;(4)-bis((1,5-Dimethyl-3-oxo-2-phenyl-2,3-dihydro-1H-pyrazol-4-yl) methylene)succinohydrazide toward Transition Metal Ions and Their Antimicrobial Activities.</a> El Saied, F.A., M.M.E. Shakdofa, A.S. El Tabl, and M.M.A. Elzaher. Main Group Chemistry, 2014. 13(2): p. 87-103. ISI[000337805200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337727600005">An Efficient and Convenient Approach for the Synthesis of Novel Pyrazolo[1,2-a]triazole-triones and Evaluation of Their Antimicrobial Activities.</a> Saluja, P., J.M. Khurana, C. Sharma, and K.R. Aneja. Australian Journal of Chemistry, 2014. 67(6): p. 867-874. ISI[000337727600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0718-073114.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
