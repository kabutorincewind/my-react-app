

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-371.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PRH4UkqonYMaEKHSku7o9JfQB+bW7nqtBDh36ZSetz4aG+tqVmvzebzdPTsS3zJiU4ckSISiu0sN9PIbI3Pmbkb4WnypnzhbnN96qIRL2f5mK3hJyQAUHQdXVz0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C0EF7CEB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-371-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60563   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p class="memofmt1-2">          Structural evidence for regulation and specificity of flaviviral proteases and evolution of the Flaviviridae fold</p>

    <p>          Aleshin, AE, Shiryaev, SA, Strongin, AY, and Liddington, RC</p>

    <p>          Protein Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17400917&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17400917&amp;dopt=abstract</a> </p><br />

    <p>2.     60564   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          EthA, a common activator of thiocarbamide-containing drugs acting on different mycobacterial targets</p>

    <p>          Dover, Lynn G, Alahari, Anuradha, Gratraud, Paul, Gomes, Jessica M, Bhowruth, Veemal, Reynolds, Robert C, Besra, Gurdyal S, and Kremer, Laurent</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(3): 1055-1063</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60565   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p><b>          Quinoline alkaloids from Lunasia amara inhibit Mycobacterium tuberculosis H(37)Rv in vitro</b> </p>

    <p>          Aguinaldo, AM, Dalangin-Mallari, VM, Macabeo, AP, Byrne, LT, Abe, F, Yamauchi, T, and Franzblau, SG</p>

    <p>          Int J Antimicrob Agents <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17399957&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17399957&amp;dopt=abstract</a> </p><br />

    <p>4.     60566   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of azole derivatives for treatment of tuberculosis and leishmaniasis</p>

    <p>          Boechat, Nubia, Costa, Marilia dos Santos, Lourenco, Maria Cristina da Silva, Neves, Ivan Jr, Genestra, Marcelo da Silva, and Ferreira, Vitor Francisco</p>

    <p>          PATENT:  WO <b>2007019660</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006  PP: 83pp.</p>

    <p>          ASSIGNEE:  (Fundacao Oswaldo Cruz - Fiocruz, Brazil</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60567   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Novel benzothiazolyl urea and thiourea derivatives with potential cytotoxic and antimicrobial activities</p>

    <p>          Abdel-Rahman Hamdy M and Morsy Mohamed A</p>

    <p>          J Enzyme Inhib Med Chem <b>2007</b>.  22(1): 57-64.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     60568   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Key issues in multidrug-resistant tuberculosis</p>

    <p>          Chakrabarti, Biswajit and Davies, Peter DO</p>

    <p>          Future Microbiology <b>2007</b>.  2(1): 51-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     60569   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Antitubercular activity of mushrooms (Basidiomycetes) and their metabolites</p>

    <p>          Zjawiony, Jordan K</p>

    <p>          Natural Product Communications <b>2007</b>.  2(3): 315-318</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     60570   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Synthetic and biological studies of a key enzyme inhibitor in the detoxification cycle of Mycobacterium tuberculosis</p>

    <p>          Lovely, Carl J and Schmid, Lesley A</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: ORGN-174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60571   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of 4-(5-substituted-1,3,4-oxadiazol-2-yl)pyridines</p>

    <p>          Navarrete-Vazquez, Gabriel, Molina-Salinas, Gloria, Duarte-Fajardo, Zetel Vahi, Vargas-Villarreal, Javier, Gonzalez-Salazar, Francisco, Tlahuext, Hugo, Said-Fernandez, Salvador, and Estrada-Soto, Samuel</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-048</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60572   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          In vitro activities of cloxyquin (5-chloroquinolin-8-ol) against Mycobacterium tuberculosis</p>

    <p>          Hongmanee, Poonpilas, Rukseree, Kamolchanok, Buabut, Benjamas, Somsri, Boontiwa, and Palittapongarnpim, Prasit</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(3): 1105-1106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60573   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Evaluation of rifampicin and isoniazid susceptibility testing of Mycobacterium tuberculosis by a mycobacteriophage D29-based assay</p>

    <p>          Chauca Jose A, Palomino Juan-Carlos, and Guerra Humberto</p>

    <p>          J Med Microbiol <b>2007</b>.  56(Pt 3): 360-4.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   60574   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          N5-substituted benzo[2,3]azepino[4,5-b]indol-6-ones and their preparation, pharmaceutical compositions and use for treating tropical diseases</p>

    <p>          Stuhlmann, Friedrich, Jaeger, Timo, Flohe, Leopold, and Schinzer, Dieter</p>

    <p>          PATENT:  EP <b>1757607</b>  ISSUE DATE: 20070228</p>

    <p>          APPLICATION: 2005-18385  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Molisa G.m.b.H., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60575   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p class="memofmt1-2">          Mycobacterial shikimate pathway enzymes as targets for drug design</p>

    <p>          Ducati, RG, Basso, LA, and Santos, DS</p>

    <p>          Curr Drug Targets <b>2007</b>.  8(3): 423-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348835&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17348835&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>14.   60576   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p class="memofmt1-2">          Development of modern InhA inhibitors to combat drug resistant strains of Mycobacterium tuberculosis</p>

    <p>          Tonge, PJ, Kisker, C, and Slayden, RA</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(5): 489-98</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346194&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346194&amp;dopt=abstract</a> </p><br />

    <p>15.   60577   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p class="memofmt1-2">          New tuberculosis drugs in development</p>

    <p>          Laughon, BE</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(5): 463-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346192&amp;dopt=abstract</a> </p><br />

    <p>16.   60578   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activities of modified fluconazole derivatives</p>

    <p>          Nam, Nguyen H, Sardari, Soroush, and Parang, Keykavous</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-060</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60579   OI-LS-371; PUBMED-OI-4/3/2007</p>

    <p class="memofmt1-2">          Specific targeted antiviral therapy for hepatitis C</p>

    <p>          Sulkowski, MS</p>

    <p>          Curr Gastroenterol Rep <b>2007</b>.  9(1): 5-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335672&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17335672&amp;dopt=abstract</a> </p><br />

    <p>18.   60580   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of bicyclo heterocyclic compounds as antifungal agents</p>

    <p>          Kawakami, Katsuhiro, Kanai, Kazuo, Horiuchi, Takao, Takeshita, Hiroshi, Kobayashi, Syozo, Sugimoto, Yuichi, Achiwa, Issei, and Kuroyanagi, Junichi</p>

    <p>          PATENT:  WO <b>2007020936</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006  PP: 418pp.</p>

    <p>          ASSIGNEE:  (Daiichi Pharmaceutical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60581   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of pyridine derivatives substituted at C-2 and C-6 positions</p>

    <p>          Vieira De Almeida, Mauro, Vinicius de Nora Souza, Marcus, Barbosa, Nadia Rezende, Silva, Frederico Pittella, Amarante, Giovanni Wilson, and Cardoso, Silvia Helena</p>

    <p>          Letters in Drug Design &amp; Discovery <b>2007</b>.  4(2): 149-153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>20.   60582   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some 2-(benzo[d]oxazol/benzo[d]imidazol-2-ylthio) N-(9H-fluoren-9-yl)acetamide derivatives</p>

    <p>          Turan-Zitouni, G, Kaplancikli, ZA, Ozdemir, A, Revial, G, and Guven, K</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2007</b>.  182(3): 639-646, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244456300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244456300015</a> </p><br />

    <p>21.   60583   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of novel heterocycles</p>

    <p>          Yar, MS, Siddiqui, AA, and Ali, MA</p>

    <p>          JOURNAL OF THE SERBIAN CHEMICAL SOCIETY <b>2007</b>.  72(1): 5-11, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244433700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244433700002</a> </p><br />

    <p>22.   60584   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Bioactive diterpenes from Orthosiphon labiatus and Salvia africana-lutea</p>

    <p>          Hussein, AA, Meyer, JJM, Jimeno, ML, and Rodriguez, B</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2007</b>.  70(2): 293-295, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244390900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244390900026</a> </p><br />

    <p>23.   60585   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Indeno[1,2-b]pyrazines and related derivatives as cysteine protease inhibitors, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          Guedat, Philippe, Boissy, Guillaume, Borg-Capra, Catherine, Colland, Frederic, Daviet, Laurent, Formstecher, Etienne, Jacq, Xavier, Rain, Jean-Christophe, Delansorne, Remi, Vallese, Stefania, and Colombo, Matteo</p>

    <p>          PATENT:  EP <b>1749822</b>  ISSUE DATE: 20070207</p>

    <p>          APPLICATION: 2005-29539  PP: 54pp.</p>

    <p>          ASSIGNEE:  (Hybrigenics S.A., Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   60586   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Optimization of Novel Acyl Pyrrolidine Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase Leading to a Development Candidate</p>

    <p>          Slater, Martin J, Amphlett, Elizabeth M, Andrews, David M, Bravi, Gianpaolo, Burton, George, Cheasty, Anne G, Corfield, John A, Ellis, Malcolm R, Fenwick, Rebecca H, Fernandes, Stephanie, Guidetti, Rossella, Haigh, David, Hartley, CDavid, Howes, Peter D, Jackson, Deborah L, Jarvest, Richard L, Lovegrove, Victoria LH, Medhurst, Katrina J, Parry, Nigel R, Price, Helen, Shah, Pritom, Singh, Onkar MP, Stocker, Richard, Thommes, Pia, Wilkinson, Claire, and Wonacott, Alan</p>

    <p>          Journal of Medicinal Chemistry <b>2007</b>.  50(5): 897-900</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   60587   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Molecular modeling databases: A new way in the search of protein targets for drug development</p>

    <p>          da Silveira, NJ, Bonalumi, CE, Arcuri, HA, and de Azevedo, WF</p>

    <p>          CURRENT BIOINFORMATICS <b>2007</b>.  2(1): 1-10, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244444500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244444500001</a> </p><br />
    <br clear="all">

    <p>26.   60588   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral activities of novel 5-phosphono-pent-2-en-1-yl nucleosides and their alkoxyalkyl phosphonoesters</p>

    <p>          Choo, Hyunah, Beadle, James R, Kern, Earl R, Prichard, Mark N, Keith, Kathy A, Hartline, Caroll B, Trahan, Julissa, Aldern, Kathy A, Korba, Brent E, and Hostetler, Karl Y</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(2): 611-615</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   60589   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Synthesis and selection of de novo proteins that bind and impede cellular functions of an essential mycobacterial protein</p>

    <p>          Rao, A, Ram, G, Saini, AK, Vohra, R, Kumar, K, Singh, Y, and Ranganathan, A</p>

    <p>          APPLIED AND ENVIRONMENTAL MICROBIOLOGY <b>2007</b>.  73(4): 1320-1331, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244443700032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244443700032</a> </p><br />

    <p>28.   60590   OI-LS-371; SCIFINDER-OI-3/26/2007</p>

    <p class="memofmt1-2">          Anidulafungin: a new addition to the antifungal armamentarium</p>

    <p>          Mohr, John and Ostrosky-Zeichner, Luis</p>

    <p>          Therapy <b>2007</b>.  4(2): 125-132</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   60591   OI-LS-371; WOS-OI-3/25/2007</p>

    <p class="memofmt1-2">          Structure-function relationships and conformational properties of alpha-MSH(6-13) analogues with candidacidal activity</p>

    <p>          Carotenuto, A, Saviello, MR, Auriemma, L, Campiglia, P, Catania, A, Novellino, E, and Grieco, P</p>

    <p>          CHEMICAL BIOLOGY &amp; DRUG DESIGN <b>2007</b>.  69(1): 68-74, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244343200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244343200010</a> </p><br />

    <p>30.   60592   OI-LS-371; WOS-OI-4/2/2007</p>

    <p class="memofmt1-2">          Cathepsin Cs are key for the intracellular survival of the protozoan parasite, Toxoplasma gondii</p>

    <p>          Que, XC, Engel, JC, Ferguson, D, Wunderlich, A, Tomavo, S, and Reed, SL</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2007</b>.  282(7): 4994-5003, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244482000082">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244482000082</a> </p><br />

    <p>31.   60593   OI-LS-371; WOS-OI-4/2/2007</p>

    <p class="memofmt1-2">          What new knowledge did we gain through the International Journal of Tuberculosis and Lung Disease in 2006?</p>

    <p>          Beyers, N, Pierard, C, Enarson, DA, and Chan-Yeung, M</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2007</b>.  11(3): 237-243, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244918500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244918500001</a> </p><br />

    <p>32.   60594   OI-LS-371; WOS-OI-4/2/2007</p>

    <p class="memofmt1-2">          Immunological protection against Mycobacterium tuberculosis infection</p>

    <p>          Yoshikai, Y</p>

    <p>          CRITICAL REVIEWS IN IMMUNOLOGY <b>2006</b>.  26(6): 515-526, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244674800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244674800004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
