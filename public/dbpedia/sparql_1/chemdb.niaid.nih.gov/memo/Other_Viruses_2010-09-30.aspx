

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-09-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yMtiWH0+SrCtbAItuBnxV7gw/DkEAXldNdS72ugzQFavZSwi1Qto4Hujl5MbEIAHC+YonUvoL47II6kZ7e9XSFb9LEaDpPRtoWNS8rGpKRuhDo8ZAX4APNS3NsI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4AD233F1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">September 17 - September 30, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20869253">Synthesis and in vitro antiviral activities of 3&#39;-fluoro (or chloro) and 2&#39;,3&#39;-difluoro 2&#39;,3&#39;-dideoxynucleoside analogs against hepatitis B and C viruses.</a> Srivastav, N.C., N. Shakya, M. Mak, C. Liang, D.L. Tyrrell, B. Agrawal, and R. Kumar. Bioorganic &amp; Medicinal Chemistry. <b>[Epub ahead of print]</b>; PMID[20869253].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0917-093010.</p>  

    <p class="memofmt2-2">Hepatitis B Virus</p> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20863701">Antiviral activity of 2,3&#39;-anhydro and related pyrimidine nucleosides against hepatitis B virus.</a> Srivastav, N.C., M. Mak, B. Agrawal, D.L. Tyrrell, and R. Kumar. Bioorganic &amp;  Medicinal Chemistry Letters. <b>[Epub ahead of print]</b>; PMID[20863701].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0917-093010.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20857959">Antiviral Activity of Various 1-(2&#39;-Deoxy-beta-d-lyxofuranosyl), 1-(2&#39;-Fluoro-beta-d-xylofuranosyl), 1-(3&#39;-Fluoro-beta-d-arabinofuranosyl), and 2&#39;-Fluoro-2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxyribose Pyrimidine Nucleoside Analogues against Duck Hepatitis B Virus (DHBV) and Human Hepatitis B Virus (HBV) Replication.</a> Srivastav, N.C., N. Shakya, M. Mak, B. Agrawal, D.L. Tyrrell, and R. Kumar. Journal of Medicinal Chemistry. <b>[Epub ahead of print]</b>; PMID[20857959].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0917-093010.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20845949">Trapping of Hepatitis B Virus capsid assembly intermediates by phenylpropenamide assembly accelerators.</a> Katen, S.P., S.R. Chirapu, M.G. Finn, and A. Zlotnick. ACS Chemical Biology. <b>[Epub ahead of print]</b>; PMID[20845949].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0917-093010.</p>

    <p> </p>

    <p class="memofmt2-2">Herpes</p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20860547">Current and New Cytomegalovirus Antivirals and Novel Animal Model Strategies.</a> McGregor, A. Inflammation and Allergy Drug Targets. <b>[Epub ahead of print]</b>; PMID[20860547].
    <br>
    <b>[Pubmed]</b>. OV_0917-093010.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281423000007">Small Molecular Compounds that Inhibit Hepatitis C Virus Replication Through Destabilizing Heat Shock Cognate 70 Messenger RNA.</a> Peng, Z.G., B. Fan, N.N. Du, Y.P. Wang, L.M. Gao, Y.H. Li, F. Liu, X.F. You, Y.X. Han, Z.Y. Zhao, S. Cen, J.R. Li, D.Q. Song, and J.D. Jiang. Hepatology. 52(3): p. 845-853; ISI[000281423000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0917-093010.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
