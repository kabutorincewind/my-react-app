

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-313.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7sZCkXiDUqGSDc4lIMmfuSO+TnO4ThZlVIjPSCzORpPEIIp8H2SgFh7VkB+YlNS6KAPQjjy0saIfLHcIdFwrjl+/HpzcJaKiYK/12kXXPfRx4awkWy78URSudlY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E163D43D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-313-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44622   HIV-LS-313; PUBMED-HIV-1/11/2004</p>

    <p class="memofmt1-2">          Synthesis, cellular uptake and HIV-1 Tat-dependent trans-activation inhibition activity of oligonucleotide analogues disulphide-conjugated to cell-penetrating peptides</p>

    <p>          Turner, JJ, Arzumanov, AA, and Gait, MJ</p>

    <p>          Nucleic Acids Res <b>2005</b>.  33(1): 27-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640444&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640444&amp;dopt=abstract</a> </p><br />

    <p>2.         44623   HIV-LS-313; PUBMED-HIV-1/11/2004</p>

    <p class="memofmt1-2">          Different from the HIV fusion inhibitor C34, the anti-HIV drug fuzeon (T-20) inhibits HIV-1 entry by targeting multiple sites in gp41 and gp120</p>

    <p>          Liu, S, Lu, H, Xu, Y, Wu, S, and Jiang, S</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640162&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640162&amp;dopt=abstract</a> </p><br />

    <p>3.         44624   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of a Novel Synthetic Phosphocholine Lipid-AZT Conjugate That Double-Targets Wild-Type and Drug Resistant Variants of HIV</p>

    <p>          Kucera, Louis S, Morris-Natschke, Susan L, Ishaq, Khalid S, Hes, Jan, Iyer, Nathan, Furman, Phillip A, and Fleming, Ronald A</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2004</b>.  23(1 &amp; 2): 385-399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.         44625   HIV-LS-313; PUBMED-HIV-1/11/2004</p>

    <p class="memofmt1-2">          Mass spectrometric analysis of the HIV-1 integrase:Pyridoxal-5&#39;-phosphate complex reveals a new binding site for a nucleotide inhibitor</p>

    <p>          Williams, KL, Zhang, Y, Shkriabai, N, Karki, RG, Nicklaus, MC, Kotrikadze, N, Hess, S, Le, Grice SF, Craigie, R, Pathak, VK, and Kvaratskhelia, M</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615720&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15615720&amp;dopt=abstract</a> </p><br />

    <p>5.         44626   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Nucleoside analog inhibitors of reverse transcriptase for use as anti-AIDS drugs</p>

    <p>          Huang, Raven H and Liu, Xianjun</p>

    <p>          PATENT:  US <b>2004235869</b>  ISSUE DATE:  20041125</p>

    <p>          APPLICATION: 2004-62515  PP: 22 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.         44627   HIV-LS-313; PUBMED-HIV-1/11/2004</p>

    <p class="memofmt1-2">          Functional Analysis of Vif Protein Shows Less Restriction of Human Immunodeficiency Virus Type 2 by APOBEC3G</p>

    <p>          Ribeiro, AC, Maia, E Silva A, Santa-Marta, M, Pombo, A, Moniz-Pereira, J, Goncalves, J, and Barahona, I</p>

    <p>          J Virol <b>2005</b>.  79(2): 823-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613310&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15613310&amp;dopt=abstract</a> </p><br />

    <p>7.         44628   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          CCR5 antagonists as anti-HIV-1 agents. Part 3: Synthesis and biological evaluation of piperidine-4-carboxamide derivatives</p>

    <p>          Imamura, Shinichi, Nishikawa, Youichi, Ichikawa, Takashi, Hattori, Taeko, Matsushita, Yoshihiro, Hashiguchi, Shohei, Kanzaki, Naoyuki, Iizawa, Yuji, Baba, Masanori, and Sugihara, Yoshihiro</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(2): 397-416</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.         44629   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Structurally diverse HIV-1 integrase inhibitors: past, present and perspective</p>

    <p>          Jiang, Xiao-Hua and Long, Ya-Qiu</p>

    <p>          Youji Huaxue <b>2004</b>.  24(11): 1380-1388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.         44630   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Preparation of phosphonate prodrugs of antiviral compounds</p>

    <p>          Boojamara, Constantine G, Cannizzaro, Carina E, Chen, James M, Chen, Xiaowu, Cho, Aesop, Chong, Lee S, Fardis, Maria, Jin, Haolun, Hirschmann, Ralph F, Huang, Alan X, Kim, Choung U, Kirschberg, Thorsten A, Lee, Christopher P, Lee, William A, Mackman, Richard L, Markevitch, David Y, Oare, David, Prasad, Vidya K, Pyun, Hyung-Jung, Ray, Adrian S, Sherlock, Rosemarie, Swaminathan, Sundaramoorthi, Watkins, Will, and Zhang, Jennifer R</p>

    <p>          PATENT:  WO <b>2004096286</b>  ISSUE DATE:  20041111</p>

    <p>          APPLICATION: 2004  PP: 856 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       44631   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Antiviral pharmaceutical combination of lamivudine, stavudine and efavirenz, or derivatives thereof</p>

    <p>          Hamied, Yusuf Khwaja and Malhotra, Geena</p>

    <p>          PATENT:  GB <b>2400553</b>  ISSUE DATE: 20041020</p>

    <p>          APPLICATION: 2003-8604  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Cipla Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       44632   HIV-LS-313; PUBMED-HIV-1/11/2004</p>

    <p class="memofmt1-2">          Pharmacokinetic and pharmacodynamic characteristics of emtricitabine support its once daily dosing for the treatment of HIV infection</p>

    <p>          Wang, LH, Begley, J, St, Claire RL 3rd, Harris, J, Wakeford, C, and Rousseau, FS</p>

    <p>          AIDS Res Hum Retroviruses <b>2004</b>.  20(11): 1173-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588339&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588339&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.       44633   HIV-LS-313; WOS-HIV-1/2/2005</p>

    <p class="memofmt1-2">          Selection and characterization of peptides specifically binding to HIV-1 psi (psi) RNA</p>

    <p>          Park, MY, Kwon, J, Lee, SY, You, JC, and Myung, HJ</p>

    <p>          VIRUS RESEARCH  <b>2004</b>.  106(1): 77-81, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225328400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225328400009</a> </p><br />

    <p>13.       44634   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Chu, CK, Wang, Jianing, Jin, Yunho, and Schinazi, Raymond F</p>

    <p>          &lt;10 Journal Title&gt; <b>2004</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       44635   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Novel 8-substituted dipyridodiazepinone inhibitors with broad-spectrum of activity against NNRTI-resistant HIV-1</p>

    <p>          Yoakim, Christiane, Bonneau, Pierre R, Deziel, Robert, Doyon, Louise, Duan, Jianmin, Guse, Ingrid, Hache, Bruno, Landry, Serge, Malenfant, Eric, Naud, Julie, Ogilvie, William W, O&#39;Meara, Jeff A, Plante, Raymond, Thavonekham, Bounkham, Simoneau, Bruno, Boes, Michael, and Cordingley, Michael G <b>2004</b>.: MEDI-129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.       44636   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis of 3&#39;-fluoro-4&#39;-ethynyl-2&#39;,3&#39;-unsaturated D- and L- nucleosides</p>

    <p>          Chen, Xin, Zhou, Wen, Schinazi, RF, and Chu, CK <b>2004</b>.: CARB-027</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.       44637   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Attempt to reduce cytotoxicity by synthesizing the l-enantiomer of 4&#39;-C-ethynyl-2&#39;-deoxypurine nucleosides as antiviral agents against HIV and HBV</p>

    <p>          Kitano, Kenji, Kohgo, Satoru, Yamada, Kohei, Sakata, Shinji, Ashida, Noriyuki, Hayakawa, Hiroyuki, Nameki, Daisuke, Kodama, Eiichi, Matsuoka, Masao, Mitsuya, Hiroaki, and Ohrui, Hiroshi</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2004</b>.  15(3): 161-167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.       44638   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Preparation of N-cyclohexylglycines as HIV protease inhibitors</p>

    <p>          Carini, David J</p>

    <p>          PATENT:  WO <b>2004043911</b>  ISSUE DATE:  20040527</p>

    <p>          APPLICATION: 2003  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.       44639   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          HIV-1 integrase: A target for new AIDS chemotherapeutics</p>

    <p>          Anthony, Neville J</p>

    <p>          Current Topics in Medicinal Chemistry (Sharjah, United Arab Emirates) <b>2004</b>.  4(9): 979-990</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.       44640   HIV-LS-313; WOS-HIV-1/2/2005</p>

    <p class="memofmt1-2">          RON receptor tyrosine kinase, a negative regulator of inflammation, inhibits HIV-1 transcription in monocytes/macrophages and is decreased in brain tissue from patients with AIDS</p>

    <p>          Lee, ES, Kalantari, P, Tsutsui, S, Klatt, A, Holden, J, Correll, PH, Power, C, and Henderson, AJ</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2004</b>.  173(11): 6864-6872, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225307500045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225307500045</a> </p><br />

    <p>20.       44641   HIV-LS-313; WOS-HIV-1/2/2005</p>

    <p class="memofmt1-2">          The caveolin-1 binding domain of HIV-1 glycoprotein gp41 is an efficient B cell epitope vaccine candidate against virus infection</p>

    <p>          Hovanessian, AG, Briand, JP, Said, EA, Svab, J, Ferris, S, Dali, H, Muller, S, Desgranges, C, and Krust, B</p>

    <p>          IMMUNITY <b>2004</b>.  21(5): 617-627, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225365000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225365000004</a> </p><br />

    <p>21.       44642   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 reverse transcriptase and protease by phlorotannins from the brown alga Ecklonia cava</p>

    <p>          Ahn, Mi-Jeong, Yoon, Kee-Dong, Min, So-Young, Lee, Ji Suk, Kim, Jeong Ha, Kim, Tae Gyun, Kim, Seung Hee, Kim, Nam-Gil, Huh, Hoon, and Kim, Jinwoong</p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2004</b>.  27(4): 544-547</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.       44643   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Stibonic acid compounds and diphenyl compounds for inhibiting viral replication</p>

    <p>          Shoemaker, Robert H, Currens, Michael, Rein, Alan, Feng, Ya-Xiong, Fisher, Robert, Stephen, Andrew, Worthy, Karen, Sei, Shizuko, Crise, Bruce, and Henderson, Louis E</p>

    <p>          PATENT:  WO <b>2004032869</b>  ISSUE DATE:  20040422</p>

    <p>          APPLICATION: 2003  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (United States Dept. of Health and Human Services, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.       44644   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Relationship between antiviral activity and host toxicity: comparison of the incorporation efficiencies of 2&#39;,3&#39;-dideoxy-5-fluoro-3&#39;-thiacytidine-triphosphate analogs by human immunodeficiency virus type 1 reverse transcriptase and human mitochondrial DNA polymerase</p>

    <p>          Feng, Joy Y, Murakami, Eisuke, Zorca, Suzana M, Johnson, Allison A, Johnson, Kenneth A, Schinazi, Raymond F, Furman, Phillip A, and Anderson, Karen S</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(4): 1300-1306</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.       44645   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          aminocarbonyl-phosphonates d4t inhibitors of human immunodeficiency virus</p>

    <p>          Pokrovskii, Andrei Georgievich, Pronyaeva, Tatyana Rudolfovna, Fedyuk, Nina Vladimirovna, Kukhanova, Marina Konstantinovna, Shirokova, Elena Anatolevna, Yasko, Maksim Vladimirovich, Khandazhinskaya, Anastasia Lvovna, and Yanvarev, Dmitry Vasilevich</p>

    <p>          PATENT:  WO <b>2004111071</b>  ISSUE DATE:  20041223</p>

    <p>          APPLICATION: 2003  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (Gosudarstvenny Nauchny Tsentr Virusologii Biotekhnologii &#39;Vektor&#39;, Russia and Institut Molekulyarnoy Biologii im. V. A. Engelgardta RAN)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.       44646   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel 2&#39;,3&#39;,4&#39;-triply branched carbocyclic nucleosides as potential antiviral agents</p>

    <p>          Ko, Ok Hyun and Hong, Joon Hee</p>

    <p>          Archiv der Pharmazie (Weinheim, Germany) <b>2004</b>.  337(11): 579-586</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>26.       44647   HIV-LS-313; SCIFINDER-HIV-1/5/2005</p>

    <p class="memofmt1-2">          Compounds and preparations having antiviral effect</p>

    <p>          Mori, Masao, Saito, Haruo, Nemoto, Hideo, Yamamoto, Naoki, and Hattori, Masao</p>

    <p>          PATENT:  WO <b>2004103360</b>  ISSUE DATE:  20041202</p>

    <p>          APPLICATION: 2003  PP: 39 pp.</p>

    <p>          ASSIGNEE:  (Lead Chemical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.       44648   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          New and efficient approach to aryl phosphoramidate derivatives of AZT/d4T as anti-HIV prodrugs</p>

    <p>          Li, XS, Fu, H, Jiang, YY, Zhao, YF, and Liu, JY</p>

    <p>          SYNLETT <b>2004</b>.(14): 2600-2602, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225425700034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225425700034</a> </p><br />

    <p>28.       44649   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Medicinal chemistry applied to a synthetic protein: Development of highly potent HIV entry inhibitors</p>

    <p>          Hartley, O, Gaertner, H, Wilken, J, Thompson, D, Fish, R, Ramos, A, Pastore, C, Dufour, B, Cerini, F, Melotti, A, Heveker, N, Picard, L, Alizon, M, Mosier, D, Kent, S, and Offord, R</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2004</b>.  101(47): 16460-16465, 6</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225347400017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225347400017</a> </p><br />

    <p>29.       44650   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Anti-cholinergic, cytotoxic and anti-HIV-1 activities of sesquiterpenes and a flavonoid glycoside from the aerial parts of Polygonum viscosum</p>

    <p>          Datta, BK, Datta, SK, Khan, TH, Kundu, JK, Rashid, MA, Nahar, L, and Sarker, SD</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2004</b>.  42(1): 18-23, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225492600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225492600004</a> </p><br />

    <p>30.       44651   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Thiosugar nucleosides. Synthesis and biological activity of 1,3,4-thiadiazole, thiazoline and thiourea derivatives of 5-thio-D-glucose</p>

    <p>          Al-Masoudi, NA, Al-Soud, YA, and Al-Masoudi, WA</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2004</b>.  23(11): 1739-1749, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225516000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225516000004</a> </p><br />

    <p>31.       44652   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          A 3D similarity method for scaffold hopping from the known drugs or natural ligands to new chemotypes</p>

    <p>          Jenkins, JL, Glick, M, and Davies, JW</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  47(25): 6144-6159, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409400007</a> </p><br />
    <br clear="all">

    <p>32.       44653   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Docking-based CoMFA and CoMSIA studies of non-nucleoside reverse transcriptase inhibitors of the pyridinone derivative type</p>

    <p>          Medina-Franco, JL, Rodriguez-Morales, S, Juarez-Gordiano, C, Hernandez-Campos, A, and Castillo, R</p>

    <p>          JOURNAL OF COMPUTER-AIDED MOLECULAR DESIGN <b>2004</b>.  18(5): 345-360, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225572900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225572900004</a> </p><br />

    <p>33.       44654   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Phosphorylation of a novel SOCS-box regulates assembly of the HIV-1 Vif-Cul5 complex that promotes APOBEC3G degradation</p>

    <p>          Mehle, A, Goncalves, J, Santa-Marta, M, McPike, M, and Gabuzda, D</p>

    <p>          GENES &amp; DEVELOPMENT <b>2004</b>.  18(23): 2861-2866, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225550100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225550100003</a> </p><br />

    <p>34.       44655   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          HIV and the CCR5-Delta 32 resistance allele</p>

    <p>          de Silva, E and Stumpf, MPH</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2004</b>.  241(1): 1-12, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225522600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225522600001</a> </p><br />

    <p>35.       44656   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Anti-HIV drug design and therapy</p>

    <p>          Ross, TM</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(32): 4003-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225447900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225447900001</a> </p><br />

    <p>36.       44657   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Acquisition of multi-PI (protease inhibitor) resistance in HIV-1 in vivo and in vitro</p>

    <p>          Yusa, K and Harada, S</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(32): 4055-4064, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225447900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225447900004</a> </p><br />

    <p>37.       44658   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Anti-AIDS agents. Part 56: Synthesis and anti-HIV activity of 7-thia-di-O-(-)-camphanoyl-(+)-cis-khellactone (7-thia-DCK) analogs</p>

    <p>          Chen, Y, Zhang, Q, Zhang, B, Xia, P, Xia, Y, Yang, ZY, Kilgore, N, Wild, C, Morris-Natschke, SL, and Lee, KH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(24): 6383-6387, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200005</a> </p><br />

    <p>38.       44659   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          RC-101, a retrocyclin-1 analogue with enhanced activity against primary HIV type 1 isolates</p>

    <p>          Owen, SM, Rudolpil, DL, Wang, W, Cole, AM, Waring, AJ, Lal, RB, and Lehrer, RI</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2004</b>.  20(11): 1157-1165, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400003</a> </p><br />
    <br clear="all">

    <p>39.       44660   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Neonatal natural killer cells produce chemokines and suppress HIV replication in vitro</p>

    <p>          Bernstein, HB, Kinter, AL, Jackson, R, and Fauci, AS</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2004</b>.  20(11): 1189-1195, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400007</a> </p><br />

    <p>40.       44661   HIV-LS-313; WOS-HIV-1/9/2005</p>

    <p class="memofmt1-2">          Restricted entry of R5 HIV type 1 strains into eosinophilic cells</p>

    <p>          Taylor, RJ, Schols, D, and Wooley, DP</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2004</b>.  20(11): 1244-1253, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225576400012</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
