

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-07-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Gp6Iqv2agH5msBaPz1ILjTXdkuP0VEZRbhRVUZQaoWTI15GAMPGZQXylf/G03iA1Nx/MXqWZyDBSkOgiPfDLY62kIkvnUj9k74t20aOpkuqzQjfH8r+sSDUGxr4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BCB7B0CA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 3 - July 16, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26033383">Antiviral Activity of Synthetic Aminopyrrolic Carbohydrate Binding Agents: Targeting the Glycans of Viral Gp120 to Inhibit HIV Entry.</a> Francesconi, O., C. Nativi, G. Gabrielli, I. De Simone, S. Noppen, J. Balzarini, S. Liekens, and S. Roelens. Chemistry, 2015. 21(28): p. 10089-10093. PMID[26033383]. <b>[PubMed]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26107245">Structure-based Design of Potent HIV-1 Protease Inhibitors with Modified P1-Biphenyl Ligands: Synthesis, Biological Evaluation, and Enzyme-inhibitor X-Ray Structural Studies.</a> Ghosh, A.K., X. Yu, H.L. Osswald, J. Agniswamy, Y.F. Wang, M. Amano, I.T. Weber, and H. Mitsuya. Journal of Medicinal Chemistry, 2015. 58(13): p. 5334-5343. PMID[26107245].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25896701">Histone Deacetylase Inhibitor Romidepsin Inhibits de Novo HIV-1 Infections.</a> Jonsson, K.L., M. Tolstrup, J. Vad-Nielsen, K. Kjaer, A. Laustsen, M.N. Andersen, T.A. Rasmussen, O.S. Sogaard, L. Ostergaard, P.W. Denton, and M.R. Jakobsen. Antimicrobial Agents and Chemotherapy, 2015. 59(7): p. 3984-3994. PMID[25896701].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26071860">Aryl H-phosphonates 18. Synthesis, Properties, and Biological Activity of 2&#39;,3&#39;-Dideoxynucleoside (N-heteroaryl)phosphoramidates of Increased Lipophilicity.</a> Kolodziej, K., J. Romanowska, J. Stawinski, J. Boryski, A. Dabrowska, A. Lipniacki, A. Piasek, A. Kraszewski, and M. Sobkowski. European Journal of Medicinal Chemistry, 2015. 100: p. 77-88. PMID[26071860].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26096782">ZNF10 Inhibits HIV-1 LTR Activity through Interaction with Nf-kappaB and Sp1 Binding Motifs.</a> Nishitsuji, H., L. Sawada, R. Sugiyama, and H. Takaku. FEBS Letters, 2015. 589(15): p. 2019-2025. PMID[26096782].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0703-071615.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356051200024">A Study of HIV-1 FP Inhibition by GBV-C Peptides using Lipid Nano-assemblies.</a> Ortiza, A., O.</p>

    <p class="plaintext">Domenech, M. Munoz-Juncosa, J. Prat, I. Haro, V. Girona, M.A. Alsina, and M. Pujol. Colloids and</p>

    <p class="plaintext">Surfaces A-Physicochemical and Engineering Aspects, 2015. 480: p. 184-190. ISI[000356051200024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356757200016">Stellettapeptins A and B, HIV-inhibitory Cyclic Depsipeptides from the Marine Sponge Stelletta Sp.</a></p>

    <p class="plaintext">Shin, H.J., M.A. Rashid, L.K. Canner, H.R. Bokesch, J.A. Wilson, J.B. McMahon, and K.R. Gustafson.</p>

    <p class="plaintext">Tetrahedron Letters, 2015. 56(28): p. 4215-4219. ISI[000356757200016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356311500012">Synthesis and Molecular Docking Analysis of Imidazol-5-one Derivatives as anti-HIV NNRTIs.</a> Dube,</p>

    <p class="plaintext">P.N., S.N. Mokale, S.I. Shaikh, Y. Patil, B. Yadav, P. Deshmukh, and S. Sabde. Pharmaceutical</p>

    <p class="plaintext">Chemistry Journal, 2015. 49(2): p. 125-131. ISI[000356311500012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357156600001">AMD3100/CXCR4 Inhibitor.</a> De Clercq, E. Frontiers in Immunology, 2015. 6(276): 3pp.</p>

    <p class="plaintext">ISI[000357156600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357093400003">Antiviral Activity of Cystatin C against HIV.</a> Luthra, K. Indian Journal of Medical Research, 2015</p>

    <p class="plaintext">141: p. 383-384. ISI[000357093400003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356544800085">Enantioselective Synthesis of Dioxatriquinane Structural Motifs for HIV-1 Protease Inhibitors using a</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356544800085">Cascade Radical Cyclization.</a>Ghosh, A.K., C.X. Xu, and H.L. Osswald. Tetrahedron Letters, 2015.</p>

    <p class="plaintext">56(23): p. 3314-3317. ISI[000356544800085].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000357093400009">Evaluation of Cystatin C Activities against HIV.</a> Vernekar, V., S. Velhal, and A. Bandivdekar. Indian</p>

    <p class="plaintext">Journal of Medical Research, 2015. 141: p. 423-430. ISI[000357093400009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000356544800155">Synthesis of Carbocyclic Nucleoside Analogs with Five-membered Heterocyclic Nucleobases.</a> Cho,</p>

    <p class="plaintext">J.H., S.J. Coats, and R.F. Schinazi. Tetrahedron Letters, 2015. 56(23): p. 3587-3590.</p>

    <p class="plaintext">ISI[000356544800155].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0703-071615.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150521&amp;CC=US&amp;NR=2015141415A1&amp;KC=A1">Methionine aminopeptidase Inhibitors for Treating Infectious Diseases.</a> Olaleye, O.A., S.F. John, A.C. Isichei, J.O. Liu, R. Maldonado, and J. Endsley. Patent. 2015. 2014-14536024 20150141415: 47pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0703-071615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
