

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-01-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="C+V/nP7j7iVq8RUYkLvljIBgVPSdAmx0qug9RIZuW+j1ZK5VAK5TrKUJdw3B/cxGE8YgmsSwKLmhj/yWdajFuCN9jR1gSfl6SYJEqUNZahOPsl+WClE9pNOSMDE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="97EA7174" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 2 - January 15, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24470309">Synthesis and Biological Activity of Piperazine Derivatives of Phenothiazine.</a> Amani, A.M. Drug Research, 2015. 65(1): p. 5-8. PMID[24470309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25294047">Imidazolium Salts as Antifungal Agents: Strong Antibiofilm Activity against Multidrug-resistant Candida tropicalis Isolates.</a> Bergamo, V.Z., R.K. Donato, D.F. Dalla Lana, K.J. Donato, G.G. Ortega, H.S. Schrekker, and A.M. Fuentefria. Letters in Applied Microbiology, 2015. 60(1): p. 66-71. PMID[25294047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25367916">Artemisinins, New Miconazole Potentiators Resulting in Increased Activity against Candida albicans Biofilms.</a> De Cremer, K., E. Lanckacker, T.L. Cools, M. Bax, K. De Brucker, P. Cos, B.P. Cammue, and K. Thevissen. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 421-426. PMID[25367916].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25385095">Contribution of Clinically Derived Mutations in ERG11 to Azole Resistance in Candida albicans.</a> Flowers, S.A., B. Colon, S.G. Whaley, M.A. Schuler, and P.D. Rogers. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 450-460. PMID[25385095].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25560012">Novel Synthetic Anti-fungal Tripeptide Effective against Candida krusei.</a> Gill, K., S. Kumar, I. Xess, and S. Dey. Indian Journal of Medical Microbiology, 2015. 33(1): p. 110-116. PMID[25560012].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25460601">Biodirected Synthesis of Miconazole-conjugated Bacterial Silver Nanoparticles and their Application as Antifungal Agents and Drug Delivery Vehicles.</a> Kumar, C.G. and Y. Poornachandra. Colloids and Surfaces B: Biointerfaces, 2015. 125: p. 110-119. PMID[25460601].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25214231">Inhibition by Toxic Compounds in the Hemicellulosic Hydrolysates on the Activity of Xylose Reductase from Candida tropicalis.</a> Rafiqul, I.S., A.M. Sakinah, and A.W. Zularisam. Biotechnology Letters, 2015. 37(1): p. 191-196. PMID[25214231].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25488140">Isavuconazole, a Broad-spectrum Triazole for the Treatment of Systemic Fungal Diseases.</a> Seyedmousavi, S., P.E. Verweij, and J.W. Mouton. Expert Review of Anti-infective Therapy, 2015. 13(1): p. 9-27. PMID[25488140].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br />    

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25348290">Possible Mechanism of Antifungal Phenazine-1-carboxamide from Pseudomonas Sp. against Dimorphic Fungi Benjaminiella poitrasii and Human Pathogen Candida albicans.</a> Tupe, S.G., R.R. Kulkarni, F. Shirazi, D.G. Sant, S.P. Joshi, and M.V. Deshpande. Journal of Applied Microbiology, 2015. 118(1): p. 39-48. PMID[25348290].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25301884">Azole, Polyene and Echinocandin MIC Distributions for Wild-type, TR34/L98H and TR46/Y121f/T289A Aspergillus fumigatus Isolates in the Netherlands.</a> van Ingen, J., H.A. van der Lee, T.A. Rijs, J. Zoll, T. Leenstra, W.J. Melchers, and P.E. Verweij. Journal of Antimicrobial Chemotherapy, 2015. 70(1): p. 178-181. PMID[25301884].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25331706">The Investigational Agent E1210 Is Effective in Treatment of Experimental Invasive Candidiasis Caused by Resistant Candida albicans.</a> Wiederhold, N.P., L.K. Najvar, A.W. Fothergill, D.I. McCarthy, R. Bocanegra, M. Olivo, W.R. Kirkpatrick, M.P. Everson, F.P. Duncanson, and T.F. Patterson. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 690-692. PMID[25331706].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25462220">Antimycobacterial Activity of Natural Products and Synthetic Agents: Pyrrolodiquinolines and Vermelhotin as Anti-tubercular Leads against Clinical Multidrug Resistant Isolates of Mycobacterium tuberculosis.</a> Ganihigama, D.U., S. Sureram, S. Sangher, P. Hongmanee, T. Aree, C. Mahidol, S. Ruchirawat, and P. Kittakoop. European Journal of Medicinal Chemistry, 2015. 89: p. 1-12. PMID[25462220].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25462221">Synthesis and Evaluation of in Vitro Antimycobacterial Activity of Novel 1H-Benzo[d]imidazole Derivatives and Analogues.</a> Gobis, K., H. Foks, M. Serocki, E. Augustynowicz-Kopec, and A. Napiorkowska. European Journal of Medicinal Chemistry, 2015. 89: p. 13-20. PMID[25462221].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25331694">Verapamil Increases the Bactericidal Activity of Bedaquiline against Mycobacterium tuberculosis in a Mouse Model.</a> Gupta, S., S. Tyagi, and W.R. Bishai. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 673-676. PMID[25331694].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25234229">Design and Structural Analysis of Aromatic Inhibitors of Type II Dehydroquinase from Mycobacterium tuberculosis.</a> Howard, N.I., M.V. Dias, F. Peyrot, L. Chen, M.F. Schmidt, T.L. Blundell, and C. Abell. ChemMedChem, 2015. 10(1): p. 116-133. PMID[25234229].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25462270">Synthesis, Molecular Docking and Anti-mycobacterial Evaluation of New Imidazo[1,2-a]pyridine-2-carboxamide Derivatives.</a> Jose, G., T.H. Suresha Kumara, G. Nagendrappa, H.B. Sowmya, D. Sriram, P. Yogeeswari, J.P. Sridevi, T.N. Guru Row, A.A. Hosamani, P.S. Sujan Ganapathy, N. Chandrika, and L.V. Narendra. European Journal of Medicinal Chemistry, 2015. 89: p. 616-627. PMID[25462270].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25568071">Direct Inhibitors of InhA are Active against Mycobacterium tuberculosis.</a> Manjunatha, U.H., S.R. SP, R.R. Kondreddi, C.G. Noble, L.R. Camacho, B.H. Tan, S.H. Ng, P.S. Ng, N.L. Ma, S.B. Lakshminarayana, M. Herve, S.W. Barnes, W. Yu, K. Kuhen, F. Blasco, D. Beer, J.R. Walker, P.J. Tonge, R. Glynne, P.W. Smith, and T.T. Diagana. Science Translational Medicine, 2015. 7(269): p. 269ra263. PMID[25568071].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25552740">Low Minimal Inhibitory Concentrations of Linezolid against Multidrug-resistant Tuberculosis Strains.</a> Sotgiu, G., R. Centis, L. D&#39;Ambrosio, P. Castiglia, and G.B. Migliori. European Respiratory Journal, 2015. 45(1): p. 287-289. PMID[25552740].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25331696">In Vitro and in Vivo Activities of the Nitroimidazole TBA-354 against Mycobacterium tuberculosis.</a> Upton, A.M., S. Cho, T.J. Yang, Y. Kim, Y. Wang, Y. Lu, B. Wang, J. Xu, K. Mdluli, Z. Ma, and S.G. Franzblau. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 136-144. PMID[25331696].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25459737">In Vitro Synergistic Activity of Clofazimine and Other Antituberculous Drugs against Multidrug-resistant Mycobacterium tuberculosis Isolates.</a> Zhang, Z., T. Li, G. Qu, Y. Pang, and Y. Zhao. International Journal of Antimicrobial Agents, 2015. 45(1): p. 71-75. PMID[25459737].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25438883">Synthesis and Antimycobacterial Evaluation of 5-Alkylamino-N-phenylpyrazine-2-carboxamides.</a> Zitko, J., B. Servusova, A. Janoutova, P. Paterova, J. Mandikova, V. Garaj, M. Vejsova, J. Marek, and M. Dolezal. Bioorganic &amp; Medicinal Chemistry, 2015. 23(1): p. 174-183. PMID[25438883].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24934654">Identification of Inhibitors of Plasmodium falciparum RuvB1 Helicase Using Biochemical Assays.</a> Ahmad, M., M. Tarique, F. Afrin, N. Tuteja, and R. Tuteja. Protoplasma, 2015. 252(1): p. 117-125. PMID[24934654].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25497962">Clofazimine Analogs with Antileishmanial and Antiplasmodial Activity.</a> Barteselli, A., M. Casagrande, N. Basilico, S. Parapini, C.M. Rusconi, M. Tonelli, V. Boido, D. Taramelli, F. Sparatore, and A. Sparatore. Bioorganic &amp; Medicinal Chemistry, 2015. 23(1): p. 55-65. PMID[25497962].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25331702">JPC-2997, a New Aminomethylphenol with High in Vitro and in Vivo Antimalarial Activities against Blood Stages of Plasmodium.</a> Birrell, G.W., M. Chavchich, A.L. Ager, H.M. Shieh, G.D. Heffernan, W. Zhao, P.E. Krasucki, K.W. Saionz, J. Terpinski, G.A. Schiehser, L.R. Jacobus, G.D. Shanks, D.P. Jacobus, and M.D. Edstein. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 170-177. PMID[25331702].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25313223">Antimalarial Activity of Granzyme B and its Targeted Delivery by a Granzyme B-single-chain fv Fusion Protein.</a> Kapelski, S., M. de Almeida, R. Fischer, S. Barth, and R. Fendel. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 669-672. PMID[25313223].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25313210">In Vitro Activity of Waladin Benzimidazoles against Different Life Cycle Stages of Plasmodium Parasites.</a> Lentz, C.S., J.M. Sattler, M. Fendler, S. Gottwalt, V.S. Halls, S. Strassel, S. Arriens, J.S. Hannam, S. Specht, M. Famulok, A.K. Mueller, A. Hoerauf, and K.M. Pfarr. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 654-658. PMID[25313210].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25515750">Choline Kinase Active Site Provides Features for Designing Versatile Inhibitors.</a> Serran-Aguilera, L., R. Nuti, L.C. Lopez-Cara, P. Rios-Marco, M.P. Carrasco, C. Marco, A. Entrena, A. Macchiarulo, and R. Hurtado-Guerrero. Current Topics in Medicinal Chemistry, 2015. 14(23): p. 2684-2693. PMID[25515750].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25150147">Methylene Blue Inhibits the Asexual Development of Vivax Malaria Parasites from a Region of Increasing Chloroquine Resistance.</a> Suwanarusk, R., B. Russell, A. Ong, K. Sriprawat, C.S. Chu, A. PyaePhyo, B. Malleret, F. Nosten, and L. Renia. Journal of Antimicrobial Chemotherapy, 2015. 70(1): p. 124-129. PMID[25150147].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25367906">A Chemical Rescue Screen Identifies a Plasmodium falciparum Apicoplast Inhibitor Targeting MEP Isoprenoid Precursor Biosynthesis.</a> Wu, W., Z. Herrera, D. Ebert, K. Baska, S.H. Cho, J.L. DeRisi, and E. Yeh. Antimicrobial Agents and Chemotherapy, 2015. 59(1): p. 356-364. PMID[25367906].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25462254">A Small-molecule Cell-based Screen Led to the Identification of Biphenylimidazoazines with Highly Potent and Broad-spectrum Anti-apicomplexan Activity.</a> Moine, E., C. Denevault-Sabourin, F. Debierre-Grockiego, L. Silpa, O. Gorgette, J.C. Barale, P. Jacquiet, F. Brossier, A. Gueiffier, I. Dimier-Poisson, and C. Enguehard-Gueiffier. European Journal of Medicinal Chemistry, 2015. 89: p. 386-400. PMID[25462254].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0102-011515.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345917200013">Development of a Novel Resin with Antimicrobial Properties for Dental Application.</a> de Castro, D.T., R.D. Holtz, O.L. Alves, E. Watanabe, M.L.D. Valente, C.H.L. da Silva, and A.C. dos Reis. Journal of Applied Oral Science, 2014. 22(5): p. 442-449. ISI[000345917200013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345631200004">Synthesis, Characterization and Antimicrobial Screening of Quinoline Based Quinazolinone-4-Thiazolidinone Heterocycles.</a> Desai, N.C. and A.M. Dodiya. Arabian Journal of Chemistry, 2014. 7(6): p. 906-913. ISI[000345631200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345734400001">The 15-Lipoxygenase Inhibitory, Antioxidant, Antimycobacterial Activity and Cytotoxicity of Fourteen Ethnomedicinally used African Spices and Culinary Herbs.</a> Dzoyem, J.P., V. Kuete, L.J. McGaw, and J.N. Eloff. Journal of Ethnopharmacology, 2014. 156: p. 1-8. ISI[000345734400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346063000006">Chemical Composition and Antimicrobial Activity of Nine Essential Oils Obtained by Steam Distillation of Plants from the Souss-Massa Region (Morocco).</a> El Asbahani, A., A. Jilale, S.N. Voisin, E.A. Addi, H. Casabianca, A. El Mousadik, D.J. Hartmann, and F.N.R. Renaud. Journal of Essential Oil Research, 2015. 27(1): p. 34-44. ISI[000346063000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345734400029">Isolation and Characterization of Antimicrobial Compounds from Terminalia phanerophlebia Engl. &amp; Diels Leaf Extracts.</a> Madikizela, B., M.A. Aderogba, J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology, 2014. 156: p. 228-234. ISI[000345734400029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345946300022">Antibacterial Activity of Some New Hydrazide Derivatives with Potential Biological Action.</a> Mocanu, A.M., C. Luca, S.I. Dunca, and G. Ciobanu. Revista de Chimie, 2014. 65(11): p. 1363-1368. ISI[000345946300022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346081600011">Synthesis and Antimicrobial Activity Evaluation of Novel 4-Thiazolidinones Containing a Pyrone Moiety.</a> Nechak, R., S.A. Bouzroura, Y. Benmalek, L. Salhi, S.P. Martini, V. Morizur, E. Dunach, and B.N. Kolli. Synthetic Communications, 2015. 45(2): p. 262-272. ISI[000346081600011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345923100003">Chemical Composition of Essential Oils of Eugenia caryophylla and Mentha Sp Cf piperita and their in Vitro Antifungal Activities on Six Human Pathogenic Fungi.</a> Nyegue, M.A., F.M.C. Ndoye-Foe, S.R. Essama, T.C. Hockmeni, F.X. Etoa, and C. Menut. African Journal of Traditional Complementary and Alternative Medicines, 2014. 11(6): p. 40-46. ISI[000345923100003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346432800001">In-vitro Antimicrobial, Antibiofilm, Cytotoxic, Antifeedant and Larvicidal Properties of Novel Quinone Isolated from Aegle marmelos (Linn.) Correa.</a> Rejiniemon, T.S., M.V. Arasu, V. Duraipandiyan, K. Ponmurugan, N.A. Al-Dhabi, S. Arokiyaraj, P. Agastian, and K.C. Choi. Annals of Clinical Microbiology and Antimicrobials, 2014. 13(48): p. 9. ISI[000346432800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343761100015">Synthesis of New 2-(N&#39;-Allylidene-hydrazino) quinazolinones and 2-(4,5-Dihydropyrazolyl)-quinazolinones and their Antimicrobial and Antifungal Activity Screening.</a> Saygili, N., M. Ekizoglu, and C. Erdogdu. Asian Journal of Chemistry, 2014. 26(11): p. 3197-3203. ISI[000343761100015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345660300036">Design and Synthesis of Novel Carbazolo-thiazoles as Potential Anti-mycobacterial Agents Using a Molecular Hybridization Approach.</a> Shaikh, M.S., M.B. Palkar, H.M. Patel, R.A. Rane, W.S. Alwan, M.M. Shaikh, I.M. Shaikh, G.A. Hampannavar, and R. Karpoormath. RSC Advances, 2014. 4(107): p. 62308-62320. ISI[000345660300036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0102-011515.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
