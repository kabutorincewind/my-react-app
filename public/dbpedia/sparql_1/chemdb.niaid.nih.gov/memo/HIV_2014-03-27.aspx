

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-03-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NSa8PneGnQQibZHi0Jp/IeRVrT899osgjxBzfqTxhjK3jmtazPBdtAGSLAPq/QF0sNt2u2gK48bIjJVMe22IIQdFWDdH86al5EBlSYLCfSCBHqQMlZoKpNG2P0w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6A846683" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 14 - March 27, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24651404">Activation of HIV-1 from Latent Infection via Synergy of RUNX1 Inhibitor Ro5-3335 and SAHA.</a> Klase, Z., V.S. Yedavalli, L. Houzet, M. Perkins, F. Maldarelli, J. Brenchley, K. Strebel, P. Liu, and K.T. Jeang. Plos Pathogens, 2014. 10(3): p. e1003997. PMID[24651404].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24622515">Postexposure Protection of Macaques from Vaginal SHIV Infection by Topical Integrase Inhibitors.</a> Dobard, C., S. Sharma, U.M. Parikh, R. West, A. Taylor, A. Martin, C.P. Pau, D.L. Hanson, J. Lipscomb, J. Smith, F. Novembre, D. Hazuda, J.G. Garcia-Lerma, and W. Heneine. Science Translational Medicine, 2014. 6(227): p. 227ra35. PMID[24622515].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24581546">Discovery of 2-Pyridone Derivatives as Potent HIV-1 NNRTIs Using Molecular Hybridization Based on Crystallographic Overlays.</a> Chen, W., P. Zhan, D. Rai, E. De Clercq, C. Pannecouque, J. Balzarini, Z. Zhou, H. Liu, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(6): p. 1863-1872. PMID[24581546].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0314-032714.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331340500004">An Extracellular Laccase with Antiproliferative Activity from the Sanghuang Mushroom Inonotus baumii.</a> Sun, J., Q.J. Chen, M.J. Zhu, H.X. Wang, and G.Q. Zhang. Journal of Molecular Catalysis B-Enzymatic, 2014. 99: p. 20-25. ISI[000331340500004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331880600009">Formulation and Evaluation of Mucoadhesive Microspheres of Tenofovir disoproxil fumarate for Intravaginal Use.</a> Khan, A.B. and R.S. Thakur. Current Drug Delivery, 2014. 11(1): p. 112-122. ISI[000331880600009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000331702100001">Cyclophilin a Promotes HIV-1 Reverse Transcription but Its Effect on Transduction Correlates Best with Its Effect on Nuclear Entry of Viral cDNA.</a>De Iaco, A. and J. Luban. Retrovirology, 2014. 11. ISI[000331702100001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0314-032714.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">7. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140130&amp;CC=WO&amp;NR=2014018449A1&amp;KC=A1">Preparation of Substituted Naphthyridinedione Derivatives as HIV Integrase Inhibitors.</a>Coleman, P.J., M. Embrey, T.J. Hartingh, D. Powell, I.T. Raheem, R.K. Chang, J. Schreier, J. Sisko, T. Steele, and A. Walji. Patent. 2014. 2013-US51494 2014018449: 69pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <p class="plaintext">8. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140212&amp;CC=CN&amp;NR=103570683A&amp;KC=A">Multi-substituted Amine Compounds as HIV Integrase Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of AIDS.</a>Long, Y. and B. Cao. Patent. 2014. 2012-10269204 103570683: 116pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">9. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140213&amp;CC=WO&amp;NR=2014025749A2&amp;KC=A2">Small Molecule Inhibitors of Viral Protein Interactions with Human t-RNA.</a>Vendeix, F.A. and P.F. Agris. Patent. 2014. 2013-US53747 2014025749: 81pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">10. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140213&amp;CC=WO&amp;NR=2014025852A1&amp;KC=A1">Preparation of Tricyclic Alkenylpyrrolopyridine Derivatives and Analogs for Use as HIV Attachment Inhibitors.</a> Wang, T., J.F. Kadow, N.A. Meanwell, Z. Zhang, Z. Yin, E.H. Ruediger, C.A. James, and D.H. Deon. Patent. 2014. 2013-US53901 2014025852: 82pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">11. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140213&amp;CC=WO&amp;NR=2014025854A1&amp;KC=A1">Preparation of Piperidinylamide Derivatives for Use as HIV Attachment Inhibitors.</a>Wang, T., Z. Zhang, J.F. Kadow, N.A. Meanwell, E.H. Ruediger, C.A. James, D.H. Deon, D.J. Carini, and B.L. Johnson. Patent. 2014. 2013-US53903 2014025854: 93pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140213&amp;CC=WO&amp;NR=2014025850A1&amp;KC=A1">Preparation of Tricyclic Amidinylindole Derivatives and Analogs for Use as HIV Attachment Inhibitors.</a> Wang, T., Z. Zhang, Z. Yin, J.F. Kadow, and N.A. Meanwell. Patent. 2014. 2013-US53899 2014025850: 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140123&amp;CC=WO&amp;NR=2014012466A1&amp;KC=A1">Compounds of Diarylanilines and Diarylpyridinamines, Preparation Method and Application.</a>Xie, L., C.-H. Chen, L. Sun, and N. Liu. Patent. 2014. 2013-CN79342 2014012466: 60pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140123&amp;CC=WO&amp;NR=2014012467A1&amp;KC=A1">Preparation of Diarylaniline or Diarylpyridinamine Derivatives as anti-HIV Agents.</a>Xie, L., C.-H. Chen, L. Sun, and N. Liu. Patent. 2014. 2013-CN79345 2014012467: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140220&amp;CC=WO&amp;NR=2014026582A1&amp;KC=A1">Tenofovir Diesters Compounds with Activity for Inhibiting HIV-1 Virus Replication, and the Preparation Method and Pharmaceutical Application Thereof.</a>You, G., H. Liu, and S. Yang. Patent. 2014. 2013-CN81304 2014026582: 47pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0314-032714.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
