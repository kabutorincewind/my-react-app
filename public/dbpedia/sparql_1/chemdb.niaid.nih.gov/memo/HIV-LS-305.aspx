

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-305.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7Jt5I3Me/6g5875MVjzy8s/mYMnTQUVHzkkMtmeTLompluA2vUNa3Ko9GkN7NYZazyY+JSz+0IWWKyufbtOrmr1BVeQPgBQ9+iJ/h8my2C8AoBEsykDfEpdoo48=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="53304D07" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-305-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44052   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides targeting HIV</p>

    <p>          Vaillant, Andrew and Juteau, Jean-marc</p>

    <p>          PATENT:  US <b>20040171568</b>  ISSUE DATE: 20040902</p>

    <p>          APPLICATION: 2003-5739  PP: 84 pp., Cont.-in-part of Appl. No. PCT/IB03-04573.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         44053   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          Guanidine alkaloid analogs as inhibitors of HIV-1 Nef interactions with p53, actin, and p56lck</p>

    <p>          Olszewski, A, Sato, K, Aron, ZD, Cohen, F, Harris, A, McDougall, BR, Robinson, WE Jr, Overman, LE, and Weiss, GA</p>

    <p>          Proc Natl Acad Sci U S A <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15371598&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15371598&amp;dopt=abstract</a> </p><br />

    <p>3.         44054   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Assays for determination of HIV resistance to antiviral drugs</p>

    <p>          Baldanti, Fausto, Paolucci, Stefania, Dossena, Luca, and Gerna, Giuseppe</p>

    <p>          Current Drug Metabolism <b>2004</b>.  5(4): 317-319</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         44055   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          Resistance profiles and adherence at primary virological failure in three different highly active antiretroviral therapy regimens: analysis of failure rates in a randomized study</p>

    <p>          Roge, B, Barfod, T, Kirk, O, Katzenstein, T, Obel, N, Nielsen, H, Pedersen, C, Mathiesen, L, Lundgren, J, and Gerstoft, J</p>

    <p>          HIV Med <b>2004</b>.  5(5): 344-351</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369509&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369509&amp;dopt=abstract</a> </p><br />

    <p>5.         44056   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          A Peptide Nucleic Acid-Neamine Conjugate That Targets and Cleaves HIV-1 TAR RNA Inhibits Viral Replication</p>

    <p>          Riguet, E, Tripathi, S, Chaubey, B, Desire, J, Pandey, VN, and Decout, JL</p>

    <p>          J Med Chem <b>2004</b>.  47(20): 4806-4809</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369382&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15369382&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44057   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Uncharged AZT and D4T Derivatives of Phosphonoformic and Phosphonoacetic Acids as Anti-HIV Pronucleosides</p>

    <p>          Shirokova, Elena A, Jasko, Maxim V, Khandazhinskaya, Anastasiya L, Ivanov, Alexander V, Yanvarev, Dmitry V, Skoblov, Yury S, Mitkevich, Vladimir A, Bocharov, Eduard V, Pronyaeva, Tatyana R, Fedyuk, Nina V, Kukhanova, Marina K, and Pokrovsky, Andrey G</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(14): 3606-3614</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>7.         44058   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          In Vitro Inhibition of Human Immunodeficiency Virus Type-1 (HIV-1) Reverse Transcriptase by Gold(III) Porphyrins</p>

    <p>          Sun, RW, Yu, WY, Sun, H, and Che, CM</p>

    <p>          Chembiochem <b>2004</b>.  5(9): 1293</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15368585&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15368585&amp;dopt=abstract</a> </p><br />

    <p>8.         44059   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          Profile of resistance of human immunodeficiency virus to mannose-specific plant lectins</p>

    <p>          Balzarini, J,  Van Laethem, K, Hatse, S , Vermeire, K, De Clercq, E, Peumans, W, Van Damme, E, Vandamme, AM, Bohlmstedt, A, and Schols, D</p>

    <p>          J Virol <b>2004</b> .  78(19): 10617-10627</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15367629&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15367629&amp;dopt=abstract</a> </p><br />

    <p>9.         44060   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          Resveratrol glucuronides as the metabolites of resveratrol in humans: Characterization, synthesis, and anti-HIV activity</p>

    <p>          Wang, LX, Heredia, A, Song, H, Zhang, Z, Yu, B, Davis, C, and Redfield, R</p>

    <p>          J Pharm Sci <b>2004</b>.  93(10): 2448-57</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15349955&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15349955&amp;dopt=abstract</a> </p><br />

    <p>10.       44061   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          HIV-1 Inhibitory Compounds from Calophyllum brasiliense Leaves</p>

    <p>          Huerta-Reyes, M, Basualdo, Mdel C, Abe, F, Jimenez-Estrada, M, Soler, C, and Reyes-Chilpa, R</p>

    <p>          Biol Pharm Bull <b>2004</b>.  27(9): 1471-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15340243&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15340243&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44062   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Improved antiviral activity of the aryloxymethoxyalaninyl phosphoramidate (APA) prodrug of abacavir (ABC) is due to the formation of markedly increased carbovir 5&#39;-triphosphate metabolite levels</p>

    <p>          Balzarini, Jan, Aquaro, Stefano, Hassan-Abdallah, Alshaimaa, Daluge, Susan M, Perno, Carlo-Federico, and McGuigan, Chris</p>

    <p>          FEBS Letters <b>2004</b>.  573(1-3): 38-44</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>12.       44063   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          P1&#39; oxadiazole protease inhibitors with excellent activity against native and protease inhibitor-resistant HIV-1</p>

    <p>          Kim, RM, Rouse, EA, Chapman, KT, Schleif, WA, Olsen, DB, Stahlhut, M, Rutkowski, CA, Emini, EA, and Tata, JR</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(18): 4651-4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15324882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15324882&amp;dopt=abstract</a> </p><br />

    <p>13.       44064   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          A novel assay to identify entry inhibitors that block binding of HIV-1 gp120 to CCR5</p>

    <p>          Zhao, Q, He, Y, Alespeiti, G, and Debnath, AK</p>

    <p>          Virology <b>2004</b>.  326(2): 299-309</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15321703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15321703&amp;dopt=abstract</a> </p><br />

    <p>14.       44065   HIV-LS-305; PUBMED-HIV-9/20/2004</p>

    <p class="memofmt1-2">          An inducible HIV type 1 gp41 HR-2 peptide-binding site on HIV type 1 envelope gp120</p>

    <p>          Alam, SM, Paleos, CA, Liao, HX, Scearce, R, Robinson, J, and Haynes, BF</p>

    <p>          AIDS Res Hum Retroviruses <b>2004</b>.  20(8): 836-45</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320988&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15320988&amp;dopt=abstract</a> </p><br />

    <p>15.       44066   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Characterization of a subtype D human immunodeficiency virus type 1 isolate that was obtained from an untreated individual and that is highly resistant to nonnucleoside reverse transcriptase inhibitors</p>

    <p>          Gao, Yong, Paxinos, Ellen, Galovich, Justin, Troyer, Ryan, Baird, Heather, Abreha, Measho, Kityo, Cissy, Mugyenyi, Peter, Petropoulos, Christos, and Arts, Eric J</p>

    <p>          Journal of Virology <b>2004</b>.  78(10): 5390-5401</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>16.       44067   HIV-LS-305; SCIFINDER-HIV-9/14/2004</p>

    <p class="memofmt1-2">          Preparation of 2&#39;,3&#39;-dideoxy and 2&#39;,3&#39;-didehydro nucleoside analogs as prodrugs for treating viral infections, most notably HIV</p>

    <p>          Cheng, Yung-chi, Tanaka, Hiromichi, and Baba, Masanori</p>

    <p>          PATENT:  US <b>20040167096</b>  ISSUE DATE: 20040826</p>

    <p>          APPLICATION: 2004-60409  PP: 45 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       44068   HIV-LS-305; WOS-HIV-9/12/2004</p>

    <p class="memofmt1-2">          Regiospecific synthesis of 5-halo-substituted thiophene pyridyl thiourea compounds as non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Venkatachalam, TK and Uckun, FM</p>

    <p>          SYNTHETIC COMMUNICATIONS <b>2004</b>.  34(13): 2451-2461, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207200017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207200017</a> </p><br />

    <p>18.       44069   HIV-LS-305; WOS-HIV-9/12/2004</p>

    <p class="memofmt1-2">          Synthesis of beta-fluorophenethyl halopyridyl thiourea compounds as non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Venkatachalam, TK and Uckun, FM</p>

    <p>          SYNTHETIC COMMUNICATIONS <b>2004</b>.  34(13): 2463-2472, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207200018</a> </p><br />

    <p>19.       44070   HIV-LS-305; WOS-HIV-9/12/2004</p>

    <p class="memofmt1-2">          Noncompetitive allosteric inhibitors of the inflammatory chemokine receptors CXCR1 and CXCR2: Prevention of reperfusion injury</p>

    <p>          Bertini, R, Allegretti, M, Bizzarri, C, Moriconi, A, Locati, M, Zampella, G, Cervellera, MN, Di, Cioccio V, Cesta, MC, Galliera, E, Martinez, FO, Di, Bitondo R, Troiani, G, Sabbatini, V, D&#39;Anniballe, G, Anacardio, R, Cutrin, JC, Cavalieri, B, Mainiero, F, Strippoli, R, Villa, P, Di, Girolamo M, Martin, F, Gentile, M, Santoni, A, Corda, D, Poli, G, Mantovani, A, Ghezzi, P, and Colotta, F</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2004</b>.  101(32): 11791-11796, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223276700048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223276700048</a> </p><br />

    <p>20.       44071   HIV-LS-305; WOS-HIV-9/19/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 multiplication by antisense U7 snRNAs and siRNAs targeting cyclophilin A</p>

    <p>          Liu, SK, Asparuhova, M, Brondani, V, Ziekau, I, Klimkait, T, and Schumperli, D</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2004</b>.  32(12): 3752-3759, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223341900025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223341900025</a> </p><br />

    <p>21.       44072   HIV-LS-305; WOS-HIV-9/19/2004</p>

    <p class="memofmt1-2">          Potent in vivo inhibition of HIV-1 infection in thy/liv-SCID-hu mice after in vivo treatment with a combination of SV40-derived vectors targeting CCR5 expression</p>

    <p>          Patel, M, Cordelier, P, Strayer, DS, and Goldstein, H</p>

    <p>          MOLECULAR THERAPY <b>2004</b>.  9: S141-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222316600371">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222316600371</a> </p><br />

    <p>22.       44073   HIV-LS-305; WOS-HIV-9/19/2004</p>

    <p class="memofmt1-2">          Resistance to enfuvirtide, the first HIV fusion inhibitor</p>

    <p>          Greenberg, ML and Cammack, N</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2004</b>.  54(2): 333-340, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223372100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223372100007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
