

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-08-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7Ac5WRFyUySGDUJ7tiAZ4VETFGzVQIVmb8bfK2KYIw3aKXVhhits4YQV89meJSAwHTLU7I0wL6djSKFmDgbhv7haclza6mBzoWCmFnI7BZRmp6PAER9rwQ5fXFU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EF9C75B6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 19 - August 1, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23694860">Silver (I) Complexes as Precursors to Produce Silver Nanowires: Structure Characterization, Antimicrobial Activity and Cell Viability.</a> El-Gamel, N.E. Dalton Transactions, 2013. 42(27): p. 9884-9892. PMID[23694860].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23877676">Candida tropicalis Antifungal Cross-resistance Is Related to Different Azole Target (Erg11p) Modifications.</a> Forastiero, A., A. Mesa-Arango, A. Alastruey-Izquierdo, L. Alcazar-Fuoli, L. Bernal-Martinez, T. Pelaez, J. Lopez, J. Grimalt, A. Gomez-Lopez, I. Cuesta, O. Zaragoza, and E. Mellado. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23877676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23871413">Activity of Antifungal Combinations against Aspergillus Species Evaluated by Isothermal Microcalorimetry.</a> Furustrand Tafin, U., C. Orasch, and A. Trampuz. Diagnostic Microbiology and Infectious Disease, 2013. <b>[Epub ahead of print]</b>. PMID[23871413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23876294">Mechanism of Action of Novel Synthetic Dodecapeptides against Candida albicans.</a> Maurya, I.K., C.K. Thota, J. Sharma, S.G. Tupe, P. Chaudhary, M.K. Singh, I.S. Thakur, M. Deshpande, R. Prasad, and V.S. Chauhan. Biochimica et Biophysica Acta, 2013. <b>[Epub ahead of print]</b>. PMID[23876294].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23880127">Plants Traditionally Used Individually and in Combination to Treat Sexually Transmitted Infections in Northern Maputaland, South Africa: Antimicrobial Activity and Cytotoxicity.</a> Naidoo, D., S.F. van Vuuren, R.L. van Zyl, and H. de Wet. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23880127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23865534">Use of Isothermal Microcalorimetry to Quantify the Influence of Glucose and Antifungals on the Growth of Candida albicans in Urine.</a> Wernli, L., G. Bonkat, T.C. Gasser, A. Bachmann, and O. Braissant. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23865534].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23879927">A New Metabolic Pathway for Converting Blasticidin S in Aspergillus flavus and Inhibitory Activity of Aflatoxin Production by Blasticidin S Metabolites.</a> Yoshinari, T., S. Sakuda, M. Watanabe, Y. Kamata, T. Ohnishi, and Y. Sugita-Konishi. Journal of Agricultural and Food Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23879927].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23894563">Expression of the ARPC4 Subunit of Human Arp2/3 Severely Affects Mycobacterium tuberculosis Growth and Suppresses Immunogenic Response in Murine Macrophages.</a> Ghosh, A., S. Tousif, D. Bhattacharya, S.K. Samuchiwal, K. Bhalla, M. Tharad, S. Kumar, P. Prakash, P. Kumar, G. Das, and A. Ranganathan. Plos One, 2013. 8(7): p. e69949. PMID[23894563].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23749163">A Novel Combinatorial Biocatalytic Approach for Producing Antibacterial Compounds Effective against Mycobacterium tuberculosis (TB).</a> McClay, K., B. Wan, Y. Wang, S. Cho, J. Yu, B. Santarsiero, S. Mehboob, M. Johnson, S. Franzblau, and R. Steffan. Applied Microbiology and Biotechnology, 2013. 97(16): p. 7151-7163. PMID[23749163].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689720">Cationic Antimicrobial Peptides and Biogenic Silver Nanoparticles Kill Mycobacteria without Eliciting DNA Damage and Cytotoxicity in Mouse Macrophages.</a> Mohanty, S., P. Jena, R. Mehta, R. Pati, B. Banerjee, S. Patil, and A. Sonawane. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3688-3698. PMID[23689720].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23587653">Ivermectin Lacks Antituberculous Activity.</a> Muhammed Ameen, S. and M. Drancourt. Journal of Antimicrobial Chemotherapy, 2013. 68(8): p. 1936-1937. PMID[23587653].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836539">Discovery of an Acyclic Nucleoside Phosphonate That Inhibits Mycobacterium tuberculosis ThyX Based on the Binding Mode of a 5-Alkynyl Substrate Analogue.</a> Parchina, A., M. Froeyen, L. Margamuljana, J. Rozenski, S. De Jonghe, Y. Briers, R. Lavigne, P. Herdewijn, and E. Lescrinier. ChemMedChem, 2013. 8(8): p. 1373-1383. PMID[23836539].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23625031">Synthesis and Biological Evaluation of Cationic Fullerene Quinazolinone Conjugates and Their Binding Mode with Modeled Mycobacterium tuberculosis Hypoxanthine-guanine Phosphoribosyltransferase Enzyme.</a> Patel, M.B., S.P. Kumar, N.N. Valand, Y.T. Jasrai, and S.K. Menon. Journal of Molecular Modeling, 2013. 19(8): p. 3201-3217. PMID[23625031].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23889525">Discovery of New Inhibitors of Mycobacterium tuberculosis InhA Enzyme Using Virtual Screening and a 3D-pharmacophore-based Approach.</a> Pauli, I., R.N. Santos, D.C. Rostirolla, L.K. Martinelli, R.G. Ducati, L.F. Timmers, L.A. Basso, D.S. Santos, R.V. Guido, A.D. Andricopulo, and O. Norberto de Souza. Journal of Chemical Information and Modeling, 2013. <b>[Epub ahead of print]</b>. PMID[23889525].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23664678">Effect of Ribosome-targeting Antibiotics on Streptomycin-resistant Mycobacterium Mutants in the rpsL Gene.</a> Pelchovich, G., A. Zhuravlev, and U. Gophna. International Journal of Antimicrobial Agents, 2013. 42(2): p. 129-132. PMID[23664678].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br />  

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23879381">Structure-guided Design of Novel Thiazolidine Inhibitors of O-Acetyl Serine Sulfhydrylase from Mycobacterium tuberculosis.</a> Poyraz, O., V.U. Jeankumar, S. Saxena, R. Schnell, M. Haraldsson, P. Yogeeswari, D. Sriram, and G. Schneider. J Med Chem, 2013. <b>[Epub ahead of print]</b>. PMID[23879381].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23716047">Two Human Host Defense Ribonucleases against Mycobacteria, the Eosinophil Cationic Protein (RNase 3) and RNase 7.</a> Pulido, D., M. Torrent, D. Andreu, M.V. Nogues, and E. Boix. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3797-3805. PMID[23716047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23770708">Antituberculosis Thiophenes Define a Requirement for Pks13 in Mycolic Acid Biosynthesis.</a> Wilson, R., P. Kumar, V. Parashar, C. Vilcheze, R. Veyron-Churlet, J.S. Freundlich, S.W. Barnes, J.R. Walker, M.J. Szymonifka, E. Marchiano, S. Shenai, R. Colangeli, W.R. Jacobs, Jr., M.B. Neiditch, L. Kremer, and D. Alland. Nature Chemical Biology, 2013. 9(8): p. 499-506. PMID[23770708].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23818420">Concise Synthesis and Antimalarial Activity of All Four Mefloquine Stereoisomers Using a Highly Enantioselective Catalytic Borylative Alkene Isomerization.</a> Ding, J. and D.G. Hall. Angewandte Chemie International ed. in English, 2013. 52(31): p. 8069-8073. PMID[23818420].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23876596">In Vitro and in Vivo Antimalarial Activity and Cytotoxicity of Extracts and Fractions from the Leaves, Root-bark and Stem-Bark of Triclisia gilletii.</a> Kikueta, C.M., O.K. Kambu, A.P. Mbenza, S.T. Mavinga, B.M. Mbamu, P. Cos, L. Maes, S. Apers, L. Pieters, and R.K. Cimanga. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23876596].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23791809">Brine Shrimp Toxicity and Antimalarial Activity of Some Plants Traditionally Used in Treatment of Malaria in Msambweni District of Kenya.</a> Nguta, J.M. and J.M. Mbaria. Journal of Ethnopharmacology, 2013. 148(3): p. 988-992. PMID[23791809].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23873616">Antimalarial, Hematological, and Antioxidant Effects of Methanolic Extract of Terminalia avicennioides in Plasmodium berghei-infected Mice.</a> Omonkhua, A.A., M.C. Cyril-Olutayo, O.M. Akanbi, and O.A. Adebayo. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23873616].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23886650">Evaluation of Novel Lipid Based Formulation of beta-Artemether and Lumefantrine in Murine Malaria Model.</a> Patil, S., S. Suryavanshi, S. Pathak, S. Sharma, and V. Patravale. International Journal of Pharmaceutics, 2013. <b>[Epub ahead of print]</b>. PMID[23886650].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23896123">Enhancement of Protective Immune Response to Recombinant Toxoplasma gondii ROP18 Antigen by Ginsenoside Re.</a> Qu, D., J. Han, and A. Du. Experimental Parasitology, 2013. <b>[Epub ahead of print]</b>. PMID[23896123].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23878324">A Novel Calcium Dependent Protein Kinase Inhibitor as a Lead Compound for Treating Cryptosporidiosis.</a> Castellanos-Gonzalez, A., A.C. White, Jr., K.K. Ojo, R.S. Vidadala, Z. Zhang, M.C. Reid, A.M. Fox, K.R. Keyloun, K. Rivas, A. Irani, S.M. Dann, E. Fan, J. Dustin, and W.C. Van Voorhis. Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23878324].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0719-080113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320561500019">Antimicrobial and Antiviral Activity-guided Fractionation from Scutia buxifolia Reissek Extracts.</a> Boligon, A.A., T.F. Kubica, D.N. Mario, T.F. de Brum, M. Piana, R. Weiblen, L. Lovato, S.H. Alves, R.C.V. Santos, C.F.D. Alves, and M.L. Athayde. Acta Physiologiae Plantarum, 2013. 35(7): p. 2229-2239. ISI[000320561500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320906600004">Synthesis and Evaluation of a New Phosphorylated Ribavirin Prodrug.</a> Dong, S.D., C.C. Lin, and M. Schroeder. Antiviral Research, 2013. 99(1): p. 18-26. ISI[000320906600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320777600025">Antibacterial and Antifungal Activities of Selected Microalgae and Cyanobacteria.</a></p>

    <p class="plaintext">Najdenski, H.M., L.G. Gigova, Iliev, II, P.S. Pilarski, J. Lukavsky, I.V. Tsvetkova, M.S. Ninova, and V.K. Kussovski. International Journal of Food Science and Technology, 2013. 48(7): p. 1533-1540. ISI[000320777600025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320658000001">Antioxidative, Antimicrobial and Cytotoxic Effects of the Phenolics of Leea indica Leaf Extract.</a> Rahman, M.A., T. bin Imran, and S. Islam. Saudi Journal of Biological Sciences, 2013. 20(3): p. 213-225. ISI[000320658000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321003000003">Syntheses and Evaluation of Macrocyclic Engelhardione Analogs as Antitubercular and Antibacterial Agents.</a> Shen, L., M.M. Maddox, S. Adhikari, D.F. Bruhn, M. Kumar, R.E. Lee, J.G. Hurdle, R.E. Lee, and D.Q. Sun. Journal of Antibiotics, 2013. 66(6): p. 319-325. ISI[000321003000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320646200001">Chemical Composition, Antimicrobial Properties and Toxicity Evaluation of the Essential Oil of Cupressus lusitanica Mill. Leaves from Cameroon.</a> Teke, G.N., K.N. Elisee, and K.J. Roger. Bmc Complementary and Alternative Medicine, 2013. 13. ISI[000320646200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0719-080113.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
