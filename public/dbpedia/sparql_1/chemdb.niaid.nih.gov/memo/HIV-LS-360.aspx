

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-360.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Az6tuO01tpHyZFFD+k9DURzPqIcahMANeRNmnxgbLsponW54XpPm65KfCaVZe49d6y+j//kPfs8anb5cPiLAQYC4kCtI6xdnhox//TerhMoWlcNZYyr+AkBwFrg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E4051210" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH  - HIV-LS-360-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59270   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          3&#39;-Carbon-substituted pyrimidine nucleosides having a 2&#39;,3&#39;-dideoxy and 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxy structure: synthesis and antiviral evaluation</p>

    <p>          Kumamoto, H, Onuma, S, Tanaka, H, Dutschman, GE, and Cheng, YC</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(4): 225-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17066900&amp;dopt=abstract</a> </p><br />

    <p>2.     59271   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Novel Antiretroviral Agents in HIV Therapy</p>

    <p>          Reed, C and Daar, ES</p>

    <p>          Curr Infect Dis Rep <b>2006</b>.  8(6): 489-96</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064643&amp;dopt=abstract</a> </p><br />

    <p>3.     59272   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          The Vif accessory protein alters the cell cycle of human immunodeficiency virus type 1 infected cells</p>

    <p>          Wang, J, Shackelford, JM, Casella, CR, Shivers, DK, Rapaport, EL, Liu, B, Yu, XF, and Finkel, TH</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17056089&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17056089&amp;dopt=abstract</a> </p><br />

    <p>4.     59273   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Luman, a New Partner of HIV-1 TMgp41, Interferes with Tat-mediated Transcription of the HIV-1 LTR</p>

    <p>          Blot, G, Lopez-Verges, S, Treand, C, Kubat, NJ, Delcroix-Genete, D, Emiliani, S, Benarous, R, and Berlioz-Torrent, C</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17054986&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17054986&amp;dopt=abstract</a> </p><br />

    <p>5.     59274   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Computational Study of the Interaction between TIBO Inhibitors and Y181 (C181), K101, and Y188 Amino Acids</p>

    <p>          Freitas, RF and Galembeck, SE</p>

    <p>          J Phys Chem B Condens Matter Mater Surf Interfaces Biophys <b>2006</b>.  110(42): 21287-98</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17048958&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17048958&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59275   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Synthesis and optimization of peptidomimetics as HIV entry inhibitors against the receptor protein CD4 using STD NMR and ligand docking</p>

    <p>          Neffe, AT, Bilang, M, and Meyer, B</p>

    <p>          Org Biomol Chem <b>2006</b>.  4(17): 3259-67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17036114&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17036114&amp;dopt=abstract</a> </p><br />

    <p>7.     59276   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Antiviral sulfated polysaccharide from Navicula directa, a diatom collected from deep-sea water in Toyama Bay</p>

    <p>          Lee, JB, Hayashi, K, Hirata, M, Kuroda, E, Suzuki, E, Kubo, Y, and Hayashi, T</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(10): 2135-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015966&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015966&amp;dopt=abstract</a> </p><br />

    <p>8.     59277   HIV-LS-360; PUBMED-HIV-10/30/2006</p>

    <p class="memofmt1-2">          The triphosphate of beta-d-4&#39;-C-ethynyl-2&#39;,3&#39;-dideoxycytidine is the preferred enantiomer substrate for HIV reverse transcriptase</p>

    <p>          Siddiqui, MA and Marquez, VE</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046266&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046266&amp;dopt=abstract</a> </p><br />

    <p>9.     59278   HIV-LS-360; WOS-HIV-10/22/2006</p>

    <p class="memofmt1-2">          Computer-aided molecular design of highly potent HIV-1 RT inhibitors: 3D QSAR and molecular docking studies of efavirenz derivatives</p>

    <p>          Pungpo, P, Saparpakorn, P, Wolschann, P, and Hannongbua, S</p>

    <p>          SAR AND QSAR IN ENVIRONMENTAL RESEARCH <b>2006</b>.  17(4): 353-370, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240953700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240953700001</a> </p><br />

    <p>10.   59279   HIV-LS-360; EMBASE-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 Gene Expression by a Fragment of hnRNP U</p>

    <p>          Valente, Susana T and Goff, Stephen P</p>

    <p>          Molecular Cell  <b>2006</b>.  23(4): 597-605</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WSR-4KNKTTM-G/2/064b99fa114806683e0c13e6f7d0e0c8">http://www.sciencedirect.com/science/article/B6WSR-4KNKTTM-G/2/064b99fa114806683e0c13e6f7d0e0c8</a> </p><br />
    <br clear="all">

    <p>11.   59280   HIV-LS-360; EMBASE-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Amino acid insertions at position 35 of HIV-1 protease interfere with virus replication without modifying antiviral drug susceptibility</p>

    <p>          Paolucci, Stefania, Baldanti, Fausto, Dossena, Luca, and Gerna, Giuseppe</p>

    <p>          Antiviral Research <b>2006</b>.  69(3): 181-185</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4J32H1X-1/2/bf2938402a0d88a2b8ff66d56d5d2fee">http://www.sciencedirect.com/science/article/B6T2H-4J32H1X-1/2/bf2938402a0d88a2b8ff66d56d5d2fee</a> </p><br />

    <p>12.   59281   HIV-LS-360; EMBASE-HIV-10/30/2006</p>

    <p class="memofmt1-2">          A 4D-QSAR study on anti-HIV HEPT analogues</p>

    <p>          Bak, Andrzej and Polanski, Jaroslaw</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(1): 273-279</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H68NJT-D/2/7e00f0545b1bf4150d4323d47845d0f2">http://www.sciencedirect.com/science/article/B6TF8-4H68NJT-D/2/7e00f0545b1bf4150d4323d47845d0f2</a> </p><br />

    <p>13.   59282   HIV-LS-360; WOS-HIV-10/22/2006</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 by RNA interference using long-hairpin RNA</p>

    <p>          Konstantinova, P, de Vries, W, Haasnoot, J, ter Brake, O, de Haan, P, and Berkhout, B</p>

    <p>          GENE THERAPY <b>2006</b>.  13(19): 1403-1413, 11</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240942700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240942700005</a> </p><br />

    <p>14.   59283   HIV-LS-360; WOS-HIV-10/22/2006</p>

    <p class="memofmt1-2">          HIV type 1 inhibition by protein kinase C modulatory compounds</p>

    <p>          Warrilow, D, Gardner, J, Darnell, GA, Suhrbier, A, and Harrich, D</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2006</b>.  22(9): 854-864, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240946700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240946700006</a> </p><br />

    <p>15.   59284   HIV-LS-360; WOS-HIV-10/22/2006</p>

    <p class="memofmt1-2">          A nonneutralizing anti-HIV type 1 antibody turns into a broad neutralizing antibody when expressed on the surface of HIV type 1-susceptible cells. II. Inhibition of HIV type 1 captured and transferred by DC-SIGN</p>

    <p>          Lee, SJ, Arora, R, Bull, LM, Arduino, RC, Garza, L, Allan, J, Kimata, JT, and Zhou, P</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2006</b>.  22(9): 874-883, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240946700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240946700009</a> </p><br />

    <p>16.   59285   HIV-LS-360; WOS-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Synthesis of l-(alkoxymethyl)-5-benzyl-6-methyluracil as potential nonnucleoside HIV-1 RT inhibitors</p>

    <p>          Chen, YL, Guo, Y, Yang, H, Wang, XW, and Liu, JY</p>

    <p>          SYNTHETIC COMMUNICATIONS <b>2006</b>.  36(19): 2913-2920, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241196300019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241196300019</a> </p><br />

    <p>17.   59286   HIV-LS-360; WOS-HIV-10/30/2006</p>

    <p class="memofmt1-2">          In vitro and in vivo anti-retroviral activity of the substance purified from the aqueous extract of Chelidonium majus L</p>

    <p>          Gerencer, M, Turecek, PL, Kistner, O, Mitterer, A, Savidis-Dacho, H, and Barrett, NP</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  72(2): 153-156, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241092900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241092900009</a> </p><br />

    <p>18.   59287   HIV-LS-360; WOS-HIV-10/30/2006</p>

    <p class="memofmt1-2">          Switching from protease inhibitor-based-HAART to a protease inhibitor-sparing regimen is associated with improved specific HIV-immune responses in HIV-infected children</p>

    <p>          Pensieroso, S, Romiti, ML, Palma, P, Castelli-Gattinara, G, Bernardi, S, Freda, E, Rossi, P, and Cancrini, C</p>

    <p>          AIDS <b>2006</b>.  20 (14): 1893-1896, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241071800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241071800015</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
