

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-10-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ozcsPlWru8tRk9HwaeP78jU9DU9hzaV9FVTGqP5b84JKXhjHC+sDaAeKaUa+azeyzjqc0tHF6JR/CKVX+OZDb71nVzz+dKlOy0bBN52rflOeYU/yMYAbu2QuJX4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="724E0BC9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: October 12 - October 25, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/23072552">Arylazolylthioacetanilide. Part 11: Design, Synthesis and Biological Evaluation of 1,2,4-Triazole</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23072552">thioacetanilide Derivatives as Novel Non-nucleoside HIV-1 Reverse transcriptase Inhibitors.</a></p>

    <p class="plaintext">Li, Z., Y. Cao, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, and X. Liu. Medicinal Chemistry</p>

    <p class="plaintext">(Shariqah (United Arab Emirates)), 2012. <b>[Epub ahead of print];</b> PMID[23072552].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23075516">Activities, Crystal Structures and Molecular Dynamics of Dihydro-1H-isoindole Derivatives, Inhibitors of HIV-1 Integrase.</a> Metifiot, M., K. Maddali, B.C. Johnson, S. Hare, S.J. Smith, X.Z. Zhao, C. Marchand, T.R. Burke, S.H. Hughes, P. Cherepanov, and Y. Pommier. ACS Chemical Biology, 2012.</p>

    <p class="plaintext">[<b>Epub ahead of print]</b>; PMID[23075516].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/23072320">Anti-infective Properties of Epigallocatechin-3-gallate (EGCg), a Component of Green Tea.</a></p>

    <p class="plaintext">Steinmann, J., J. Buer, T. Pietschmann, and E. Steinmann. British Journal of Pharmacology, 2012.</p>

    <p class="plaintext"><b>[Epub ahead of print];</b> PMID[23072320].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23084924">Inhibition of HIV-1 Particle Assembly by 2&#39;,3&#39;-Cyclic-nucleotide 3&#39;-phosphodiesterase.</a> Wilson, S.J.,</p>

    <p class="plaintext">J.W. Schoggins, T. Zang, S.B. Kutluay, N. Jouvenet, M.A. Alim, J. Bitzegeio, C.M. Rice, and P.D.</p>

    <p class="plaintext">Bieniasz. Cell Host &amp; Microbe, 2012. 12(4): p. 585-597. PMID[23084924].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22868231">Physicochemical Characterization of GBV-C E1 Peptides as Potential Inhibitors of HIV-1 Fusion</a></p>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/22868231">Peptide: Interaction with Model Membranes.</a>Sanchez-Martin, M.J., A. Cruz, M.A. Busquets, I. Haro,</p>

    <p class="plaintext">M.A. Alsina, and M. Pujol. International Journal of Pharmaceutics, 2012. 436(1-2): p. 593-601.</p>

    <p class="plaintext">PMID[22868231].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="file:///C:/AppData/Local/105/Miltefosine%20Represses%20HIV-1%20Replication%20in%20Human%20Dendritic%20Cell/T-Cell%20Cocultures%20Partially%20by%20Inducing%20Secretion%20of%20Type-I%20Interferon">Miltefosine Represses HIV-1 Replication in Human Dendritic Cell/T-Cell Cocultures Partially by</a></p>

    <p class="plaintext"><a href="file:///C:/AppData/Local/105/Miltefosine%20Represses%20HIV-1%20Replication%20in%20Human%20Dendritic%20Cell/T-Cell%20Cocultures%20Partially%20by%20Inducing%20Secretion%20of%20Type-I%20Interferon">Inducing Secretion of Type-I Interferon.</a>Garg, R. and M.J. Tremblay. Virology, 2012. 432(2): p. 271-276.</p>

    <p class="plaintext">PMID[22704066].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1012-102512.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308042000010">Nitrogen-containing Polyhydroxylated Aromatics as HIV-1 Integrase Inhibitors: Synthesis, Structure-</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308042000010">Activity Relationship Analysis, and Biological Activity.</a></p>

    <p class="plaintext">Yu, S.H., L.N. Zhang, S.F. Yan, P. Wang, T. Sanchez, F. Christ, Z. Debyser, N. Neamati, and G.S. Zhao.</p>

    <p class="plaintext">Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. 27(5): p. 628-640.</p>

    <p class="plaintext">PMID[WOS:000308533100002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308042000010">An Intravaginal Ring That Releases the NNRTI MIV-150 Reduces SHIV Transmission in Macaques.</a></p>

    <p class="plaintext">Singer, R., P. Mawson, N. Derby, A. Rodriguez, L. Kizima, R. Menon, D. Goldman, J. Kenney, M.</p>

    <p class="plaintext">Aravantinou, S. Seidor, A. Gettie, J. Blanchard, M. Piatak, J.D. Lifson, J.A. Fernandez-Romero, M.</p>

    <p class="plaintext">Robbiani, and T.M. Zydowsky. Science Translational Medicine, 2012. 4(150).</p>

    <p class="plaintext">PMID[WOS:000308677100009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308042000010">Transmembrane Protein Aptamers that Inhibit CCR5 Expression and HIV Coreceptor Function.</a></p>

    <p class="plaintext">Scheideman, E.H., S.A. Marlatt, Y.H. Xie, Y.N. Hu, R.E. Sutton, and D. DiMaio. Journal of Virology,</p>

    <p class="plaintext">2012. 86(19): p. 10281-10292. PMID[WOS:000308740700003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308916500003">Zinc Finger Protein Designed to Target 2-long Terminal Repeat Junctions Interferes with Human</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308916500003">Immunodeficiency Virus Integration.</a>Sakkhachornphop, S., C.F. Barbas, R. Keawvichit, K.</p>

    <p class="plaintext">Wongworapat, and C. Tayapiwatana. Human Gene Therapy, 2012. 23(9): p. 932-942.</p>

    <p class="plaintext">PMID[WOS:000308916500003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308916500003">The Role of Crude Saliva and Purified Salivary Mucins in the Inhibition of the Human</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308916500003">Immunodeficiency Virus Type 1.</a>Peacocke, J., Z. Lotz, C. de Beer, P. Roux, and A.S. Mall. Virology</p>

    <p class="plaintext">Journal, 2012. 9: p. 177. PMID[WOS:000308889200001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306941900011">An Analog of the Natural Steroidal Alkaloid Cortistatin A Potently Suppresses Tat-Dependent HIV</a></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306941900011">Transcription.</a> Mousseau, G., M.A. Clementz, W.N. Bakeman, N. Nagarsheth, M. Cameron, J. Shi, P.</p>

    <p class="plaintext">Baran, R. Fromentin, N. Chomont, and S.T. Valente. Cell Host &amp; Microbe, 2012. 12(1): p. 97-108.</p>

    <p class="plaintext">PMID[WOS:000306941900011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306941900011">Stereoselective Synthesis and Antiviral Activity of Methyl-substituted Cyclosal-pronucleotides.</a></p>

    <p class="plaintext">Morales, E.H.R., J. Balzarini, and C. Meier. Journal of Medicinal Chemistry, 2012. 55(16): p. 7245-7252.</p>

    <p class="plaintext">PMID[WOS:000307748800021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308579800069">Biochemical Analysis of Hypermutation by the Deoxycytidine Deaminase APOBEC3A.</a> Love, R.P.,</p>

    <p class="plaintext">H.X. Xu, and L. Chelico. Journal of Biological Chemistry, 2012. 287(36): p. 30812-30822.</p>

    <p class="plaintext">PMID[WOS:000308579800069].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1012-102512.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
