

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-117.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4mBu0926iEs2uoHgZGp4wVI5i5aZePVkR14f6MaiPigPZCvI8L+ESSUrMeHnIsLxB98WVJ460U36+5ruFsVf5GoUndYaGKgJ4IsKJ+eEi81S4vG2lAtTeWCXwm0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6E92A7C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-117-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58346   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Topical antiviral therapeutic and prophylactic treatment of adenoviruses and their associated diseases</p>

    <p>          Gershon, David </p>

    <p>          PATENT:  US <b>2006025398</b>  ISSUE DATE:  20060202</p>

    <p>          APPLICATION: 2005-31545  PP: 14 pp., Cont.-in-part of U.S. Ser. No. 883,406.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58347   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The bananins: new anticorona-RNA-viral agents with unique structural signature</p>

    <p>          Kesel, Andreas J</p>

    <p>          Anti-Infective Agents in Medicinal Chemistry <b>2006</b>.  5(2): 161-174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     58348   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Arenavirus Z protein as an antiviral target: virus inactivation and protein oligomerization by zinc finger-reactive compounds</p>

    <p>          Garcia Cybele C, Djavani Mahmoud, Topisirovic Ivan, Borden Katherine L B, Salvato Maria S, and Damonte Elsa B</p>

    <p>          The Journal of general virology <b>2006</b>.  87(Pt 5): 1217-28.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58349   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral antisense oligonucleotides for treating ss(+)RNA viral infection</p>

    <p>          Iversen, Patrick L and Stein, David A</p>

    <p>          PATENT:  US <b>2006063150</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005-30387  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     58350   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis B virus mutations associated with antiviral therapy</p>

    <p>          Bartholomeusz, A and Locarnini, S</p>

    <p>          J Med Virol <b>2006</b>.  78(S1): S52-S55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16622878&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16622878&amp;dopt=abstract</a> </p><br />

    <p>6.     58351   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of cyclosaligenyl prodrugs of the nucleoside analogue bromovinyldeoxyuridine against herpes viruses</p>

    <p>          Meerbach, A, Meier, C, Sauerbrei, A, Meckel, HM, and Wutzler, P</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621459&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     58352   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro and in vivo evaluation of isatin-beta-thiosemicarbazone and marboran against vaccinia and cowpox virus infections</p>

    <p>          Quenelle, DC, Keith, KA, and Kern, ER</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621041&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621041&amp;dopt=abstract</a> </p><br />

    <p>8.     58353   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of treatment strategies to combat Ebola and Marburg viruses</p>

    <p>          Paragas, Jason and Geisbert, Thomas W</p>

    <p>          Expert Review of Anti-Infective Therapy <b>2006</b>.  4(1): 67-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     58354   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of indole derivatives as viral polymerase inhibitors</p>

    <p>          Beaulieu, Pierre L, Brochu, Christian, Kawai, Stephen, Rancourt, Jean, Stammers, Timothy A, Thavonekham, Bounkham, and Tsantrizos, Youla S</p>

    <p>          PATENT:  WO <b>2006007693</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 116 pp.</p>

    <p>          ASSIGNEE:  (Boehringer Ingelheim International GmbH, Germany and Boehringer Ingelheim Pharma Gmbh &amp; Co. KG)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58355   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ebola virus: unravelling pathogenesis to combat a deadly disease</p>

    <p>          Hoenen, T, Groseth, A, Falzarano, D, and Feldmann, H</p>

    <p>          Trends Mol Med  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616875&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616875&amp;dopt=abstract</a> </p><br />

    <p>11.   58356   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of severe acute respiratory syndrome-associated coronavirus (SARS-CoV) infectivity by peptides analogous to the viral spike protein</p>

    <p>          Sainz, B Jr, Mossel, EC, Gallaher, WR, Wimley, WC, Peters, CJ, Wilson, RB, and Garry, RF</p>

    <p>          Virus Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616792&amp;dopt=abstract</a> </p><br />

    <p>12.   58357   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of nucleoside analogs as antiviral agents for treating flaviviruses, pestiviruses and hepacivirus</p>

    <p>          Sommadossi, Jean-Pierre, Gosselin, Gilles, Storer, Richard, and Egan, James</p>

    <p>          PATENT:  WO <b>2006037028</b>  ISSUE DATE:  20060406</p>

    <p>          APPLICATION: 2005  PP: 73 pp.</p>

    <p>          ASSIGNEE:  (Idenix (Cayman) Limited, Cayman I. and Centre National de la Recherche Scientifique)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   58358   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peroxisome proliferator-activated receptor-gamma agonists inhibit the replication of respiratory syncytial virus (RSV) in human lung epithelial cells</p>

    <p>          Arnold, R and Konig, W</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616290&amp;dopt=abstract</a> </p><br />

    <p>14.   58359   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Imidazo[4,5-d]pyrimidines as antiviral agents and their methods of preparation, pharmaceutical compositions, and use for treatment of Flaviviridae and Picornaviridae, in particular HCV infections</p>

    <p>          Kim, Choung U, Neyts, Johan, Oare, David A, and Puerstinger, Gerhard</p>

    <p>          PATENT:  US <b>2006052602</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005-59679  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   58360   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in flavivirus antiviral drug discovery and vaccine development</p>

    <p>          Ray, Debashish and Shi, Pei-Yong</p>

    <p>          Recent Patents on Anti-Infective Drug Discovery <b>2006</b>.  1(1 ): 45-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58361   LR-14883; DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Characterization and Inhibition of SARS-Coronavirus Main Protease</p>

    <p>          Liang, PH</p>

    <p>          Curr Top Med Chem <b>2006</b>.  6(4): 361-376</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611148&amp;dopt=abstract</a> </p><br />

    <p>17.   58362   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Herpes simplex viruses in antiviral drug discovery</p>

    <p>          Schang, LM</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(11): 1357-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611120&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611120&amp;dopt=abstract</a> </p><br />

    <p>18.   58363   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-BVDV Activity of Acridones As New Potential Antiviral Agents(1)</p>

    <p>          Tabarrini, O, Manfroni, G, Fravolini, A, Cecchetti, V, Sabatini, S, De, Clercq E, Rozenski, J, Canard, B, Dutartre, H, Paeshuyse, J, and Neyts, J</p>

    <p>          J Med Chem <b>2006</b>.  49(8): 2621-2627</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610805&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   58364   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of Chinese medicine jiaweisinisan in hepatitis B virus transgenic mice</p>

    <p>          Chen, XY, Tong, GD, and Xia, F</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(14): 2280-3</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610037&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16610037&amp;dopt=abstract</a> </p><br />

    <p>20.   58365   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral therapy targeting viral polymerase</p>

    <p>          Tsai Ching-Hsiu, Lee Pei-Yu, Stollar Victor, and Li Mei-Ling</p>

    <p>          Current pharmaceutical design <b>2006</b>.  12(11): 1339-55.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   58366   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Natural and synthetic retinoids: structural bases and biological effects of potential clinical relevance for the prevention and treatment of infection-driven tumors</p>

    <p>          Dolcetti, Riccardo, Di Luca, Dario, and de Lera, Angel R</p>

    <p>          Anti-Infective Agents in Medicinal Chemistry <b>2006</b>.  5(1): 85-103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   58367   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vivo antiviral activity: defective interfering virus protects better against virulent Influenza A virus than avirulent virus</p>

    <p>          Dimmock, NJ and Marriott, AC</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 5): 1259-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603528&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603528&amp;dopt=abstract</a> </p><br />

    <p>23.   58368   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel antiviral activity of chemokines</p>

    <p>          Nakayama, T, Shirane, J, Hieshima, K, Shibano, M, Watanabe, M, Jin, Z, Nagakubo, D, Saito, T, Shimomura, Y, and Yoshie, O</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603217&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603217&amp;dopt=abstract</a> </p><br />

    <p>24.   58369   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological properties of 2-oxabicyclo[4.1.0]heptane nucleosides containing uracil, and thymine</p>

    <p>          Corsaro, Antonino, Chiacchio, Ugo, Pistara, Venerando, Borrello, Luisa, Romeo, Giovanni, and Dalpozzo, Renato</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2006</b>.(6): 74-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>25.   58370   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rapid peptide-based screening on the substrate specificity of severe acute respiratory syndrome (SARS) coronavirus 3C-like protease by matrix-assisted laser desorption/ionization time-of-flight mass spectrometry</p>

    <p>          Chu, LH, Choy, WY, Tsai, SN, Rao, Z, and Ngai, SM</p>

    <p>          Protein Sci <b>2006</b>.  15(4): 699-709</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16600962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16600962&amp;dopt=abstract</a> </p><br />

    <p>26.   58371   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and derivatization of angular 3-chloro-3-chlorosulfenyl naphtho[1,2-b]pyran(4H)-4-ones with evaluation of antiviral activity</p>

    <p>          Hegab, Mohamed, Rashad, Aymn, Shamroukh, Ahmed, and Hamza, Ibrahim</p>

    <p>          Journal of Sulfur Chemistry <b>2006</b>.  27(3): 1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   58372   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Conserved receptor-binding domains of lake Victoria marburgvirus and zaire ebolavirus bind a common receptor</p>

    <p>          Kuhn, JH, Radoshitzky, SR, Guth, AC, Warfield, KL, Li, W, Vincent, MJ, Towner, JS, Nichol, ST, Bavari, S, Choe, H, Aman, MJ, and Farzan, M</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595665&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16595665&amp;dopt=abstract</a> </p><br />

    <p>28.   58373   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          4-Arylazo-3,5-diaminopyrazole compounds and their preparation, pharmaceutical compositions, inhibition of cyclin-dependent kinases and treatment of proliferative and viral disorders</p>

    <p>          Cankar, Petr, Frisova, Iveta, Krystof, Vladimir, Lenobel, Rene, Slouka, Jan, Strnad, Miroslav, and Fisher, Peter Martin</p>

    <p>          PATENT:  WO <b>2006024858</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005  PP: 101 pp.</p>

    <p>          ASSIGNEE:  (Institute of Experimental Botany of the Academy of Sciences of the Czech Republic, Czech Rep. and Cyclacel Limited)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   58374   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Edible bird&#39;s nest extract inhibits influenza virus infection</p>

    <p>          Guo, CT, Takahashi, T, Bukawa, W, Takahashi, N, Yagi, H, Kato, K, Hidari, KI, Miyamoto, D, Suzuki, T, and Suzuki, Y</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16581142&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16581142&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.   58375   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tertiary structure prediction of SARS coronavirus helicase</p>

    <p>          Bernini, A, Spiga, O, Venditti, V, Prischi, F, Bracci, L, Huang, J, Tanner, JA, and Niccolai, N</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16579970&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16579970&amp;dopt=abstract</a> </p><br />

    <p>31.   58376   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oseltamivir resistance in influenza A (H5N1) infection</p>

    <p>          Gupta, RK and Nguyen-Van-Tam, JS</p>

    <p>          N Engl J Med <b>2006</b>.  354(13): 1423-4; author reply 1423-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571890&amp;dopt=abstract</a> </p><br />

    <p>32.   58377   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Triaryl pyrazoline compound inhibits flavivirus RNA replication</p>

    <p>          Puig-Basagoiti, F, Tilgner, M, Forshey, BM, Philpott, SM, Espina, NG, Wentworth, DE, Goebel, SJ, Masters, PS, Falgout, B, Ren, P, Ferguson, DM, and Shi, PY</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1320-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569847&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569847&amp;dopt=abstract</a> </p><br />

    <p>33.   58378   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of antiherpetic activity and genotoxic effects of tea catechin derivatives</p>

    <p>          Savi, LA, Barardi, CR, and Simoes, CM</p>

    <p>          J Agric Food Chem <b>2006</b>.  54(7): 2552-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569042&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569042&amp;dopt=abstract</a> </p><br />

    <p>34.   58379   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Lambda Interferon (IFN-{lambda}), a Type III IFN, Is Induced by Viruses and IFNs and Displays Potent Antiviral Activity against Select Virus Infections In Vivo</p>

    <p>          Ank Nina, West Hans, Bartholdy Christina, Eriksson Kristina, Thomsen Allan R, and Paludan Soren R</p>

    <p>          Journal of virology <b>2006</b>.  80(9): 4501-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   58380   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phytochemical screening and antiviral activity of some medicinal plants from the island Soqotra</p>

    <p>          Mothana Ramzi A A, Mentel Renate, Reiss Christiane, and Lindequist Ulrike</p>

    <p>          Phytotherapy research : PTR <b>2006</b>.  20(4): 298-302.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>36.   58381   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiherpetic activity of 5-alkynyl derivatives of 2&#39;-deoxyuridine</p>

    <p>          Andronova V L, Pchelintseva A A, Ustinov A V, Petrunina A L, Korshun V A, Skorobogatyi M V, and Galegov G A</p>

    <p>          Voprosy virusologii <b>2006</b>.  51(1): 34-8.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   58382   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chinese medicinal herbs for influenza: a systematic review</p>

    <p>          Chen, X, Wu, T, and Liu, G</p>

    <p>          J Altern Complement Med <b>2006</b>.  12(2): 171-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566677&amp;dopt=abstract</a> </p><br />

    <p>38.   58383   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-influenza virus activity of peramivir in mice with single intramuscular injection</p>

    <p>          Bantia Shanta, Arnold C Shane, Parker Cynthia D, Upshaw Ramanda, and Chand Pooran</p>

    <p>          Antiviral research <b>2006</b>.  69(1): 39-45.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   58384   DMID-LS-117; PUBMED-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Suppression of mRNA accumulation by the duck hepatitis B virus reverse transcriptase</p>

    <p>          Cao, F and Tavis, JE</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16563457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16563457&amp;dopt=abstract</a> </p><br />

    <p>40.   58385   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Variation in the ability of human influenza A viruses to induce and inhibit the IFN-beta pathway</p>

    <p>          Hayman, A, Comely, S, Lackenby, A, Murphy, S, McCauley, J, Goodbourn, S, and Barclay, W</p>

    <p>          VIROLOGY: Virology <b>2006</b>.  347(1): 52-64, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600005</a> </p><br />

    <p>41.   58386   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Role of sialic acid-containing molecules in paramyxovirus entry into the host cell: A minireview</p>

    <p>          Villar, Enrique and Barroso, Isabel Munoz</p>

    <p>          Glycoconjugate Journal <b>2006</b>.  23(1-2): 5-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   58387   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomaviruses bind a basal extracellular matrix component secreted by keratinocytes which is distinct from a membrane-associated receptor</p>

    <p>          Culp, TD, Budgeon, LR, and Christensen, ND</p>

    <p>          VIROLOGY: Virology <b>2006</b>.  347(1): 147-159, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236531600014</a> </p><br />
    <br clear="all">

    <p>43.   58388   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polyomaviruses and human diseases</p>

    <p>          Ahsan, N and Shah, KV</p>

    <p>          POLYOMAVIRUSES AND HUMAN DISEASES: Adv.Exp.Med.Biol <b>2006</b>.  577: 1-18, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236353100001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236353100001</a> </p><br />

    <p>44.   58389   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacotherapeutic options for the management of human polyomaviruses</p>

    <p>          Roskopf, J, Trofe, J, Stratta, RJ, and Ahsan, N</p>

    <p>          POLYOMAVIRUSES AND HUMAN DISEASES: Adv.Exp.Med.Biol <b>2006</b>.  577: 228-254, 27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236353100017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236353100017</a> </p><br />

    <p>45.   58390   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, antiviral and antibacterial activities of isatin mannich bases</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH: Med. Chem. Res <b>2005</b>.  14(4): 211-228, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236528600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236528600003</a> </p><br />

    <p>46.   58391   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Poxviruses: past, present and future</p>

    <p>          Lefkowitz, EJ, Wang, C, and Upton, C</p>

    <p>          Virus Research  <b>2006</b>.  117(1): 105-118</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   58392   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel and specific inhibitors of a poxvirus type I topoisomerase</p>

    <p>          Bond, Alexis, Reichert, Zachary, and Stivers, James T</p>

    <p>          Molecular Pharmacology <b>2006</b>.  69(2): 547-557</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   58393   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-O-methyl-5-alkynyl and alkenyl substituted uridine derivatives to screen for inhibitors of HCV</p>

    <p>          Ding, VL, Girardet, JL, Hong, Z, Shaw, SZ, and Yao, NH</p>

    <p>          HETEROCYCLES: Heterocycles <b>2006</b>.  68(3): 521-530, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236642800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236642800009</a> </p><br />

    <p>49.   58394   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Diagnosis and treatment of Kawasaki disease and coronavirus infection based on nucleic acid sequences of human coronavirus HCoV-NH</p>

    <p>          Kahn, Jeffrey S and Esper, Frank</p>

    <p>          PATENT:  WO <b>2006020824</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 53 pp.</p>

    <p>          ASSIGNEE:  (Yale University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>50.   58395   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral flavonoid-type C-glycosides from the flowers of Trollius chinensis</p>

    <p>          Cai, SQ, Wang, RF, Yang, XW, Shang, MY, Ma, CM, and Shoyama, Y</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY: Chem. Biodivers <b>2006</b>.  3(3): 343-348, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236523400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236523400009</a> </p><br />

    <p>51.   58396   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Halogenated benzamide derivatives with antiviral activity</p>

    <p>          Rossignol, Jean Francois</p>

    <p>          PATENT:  WO <b>2006031566</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005  PP: 26 pp.</p>

    <p>          ASSIGNEE:  (Romark Laboratories, L. C. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>52.   58397   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Chemical structure and antiviral activity of carrageenans from Meristiella gelidium against herpes simplex and dengue virus</p>

    <p>          de SF-Tischer, PC, Talarico, LB, Noseda, MD, Guimaraes, SMPB, Damonte, EB, and Duarte, MER</p>

    <p>          CARBOHYDRATE POLYMERS: Carbohydr. Polym <b>2006</b>.  63(4): 459-465, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236443700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236443700002</a> </p><br />

    <p>53.   58398   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rabbit viral papillomas and carcinomas: model diseases for human papillomavirus infections and associated carcinogenesis</p>

    <p>          Breitburd, Francoise, Nonnenmacher, Mathieu, Salmon, Jerome, and Orth, Gerard</p>

    <p>          Papillomavirus Research <b>2006</b>.: 399-420</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>54.   58399   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of nucleoside-lipid conjugates as antiviral and antitumor agents</p>

    <p>          Ahmad, Moghis U, Ali, Shoukath M, Khan, Abdul R, and Ahmad, Imran</p>

    <p>          PATENT:  WO <b>2006029081</b>  ISSUE DATE:  20060316</p>

    <p>          APPLICATION: 2005  PP: 72 pp.</p>

    <p>          ASSIGNEE:  (Neopharm, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>55.   58400   DMID-LS-117; SCIFINDER-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Treatment of renal allograft polyoma BK virus infection with leflunomide</p>

    <p>          Josephson Michelle A, Gillen Daniel, Javaid Basit, Kadambi Pradeep, Meehan Shane, Foster Preston, Harland Robert, Thistlethwaite Richard J, Garfinkel Marc, Atwood Walter, Jordan Joslynn, Sadhu Molly, Millis Michael J, and Williams James</p>

    <p>          Transplantation <b>2006</b>.  81(5): 704-10.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>56.   58401   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expression of plasminogen activator inhibitor 2 (PAI-2) in response to rhinovirus infection</p>

    <p>          Dagher, H, Hanson, K, Brockman-Schneider, R, Lee, W, Mosser, A, and Gern, J</p>

    <p>          JOURNAL OF ALLERGY AND CLINICAL IMMUNOLOGY: J. Allergy Clin. Immunol <b>2006</b>.  117(2): S17-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235865300067">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235865300067</a> </p><br />
    <br clear="all">

    <p>57.   58402   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The non-immunosuppressive cyclosporin DEBIO-025 is a potent inhibitor of hepatitis C virus replication in vitro</p>

    <p>          Paeshuyse, J, Kaul, A, De Clercq, E, Rosenwirth, B, Dumont, JM, Scalfaro, P, Bartenschlager, R, and Neyts, J</p>

    <p>          HEPATOLOGY: Hepatology <b>2006</b>.  43(4): 761-770, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236433900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236433900016</a> </p><br />

    <p>58.   58403   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural analysis of a designed inhibitor complexed with the hemagglutinin-neuraminidase of Newcastle disease virus</p>

    <p>          Ryan, C, Zaitsev, V, Tindal, DJ, Dyason, JC, Thomson, RJ, Alymova, I, Portner, A, von Itzstein, M, and Taylor, G</p>

    <p>          GLYCOCONJUGATE JOURNAL: Glycoconjugate J <b>2006</b>.  23(1-2): 135-141, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236501200016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236501200016</a> </p><br />

    <p>59.   58404   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral compounds and one new iridoid glycoside from Cornus officinalis</p>

    <p>          Wang, Y, Li, ZQ, Chen, LR, and Xu, XJ</p>

    <p>          PROGRESS IN NATURAL SCIENCE: Prog. Nat. Sci <b>2006</b>.  16(2): 142-146, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005</a> </p><br />

    <p>60.   58405   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cell culture-grown hepatitis C virus is infectious in vivo and can be recultured in vitro</p>

    <p>          Lindenbach, BD, Meuleman, P, Ploss, A, Vanwolleghem, T, Syder, AJ, McKeating, JA, Lanford, RE, Feinstone, SM, Major, ME, Leroux-Roels, G, and Rice, CM</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA: Proc. Natl. Acad. Sci. U. S. A <b>2006</b>.  103(10): 3805-3809, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236225300055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236225300055</a> </p><br />

    <p>61.   58406   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          TNF inhibitors and infections</p>

    <p>          Orenstein, R and Matteson, EL</p>

    <p>          INFECTIONS IN MEDICINE: Infect. Med <b>2006</b>.  23(3): 99-+, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236189100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236189100004</a> </p><br />

    <p>62.   58407   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Theoretical studies on SARS coronavirus 3CL proteinase and its inhibitor</p>

    <p>          Wang, S, Huang, XR, Gao, XF, Zhao, X, and Sun, CC</p>

    <p>          CHEMICAL JOURNAL OF CHINESE UNIVERSITIES-CHINESE: Chem. J. Chin. Univ.-Chin <b>2006</b>.  27(3): 535-537, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236261100032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236261100032</a> </p><br />
    <br clear="all">

    <p>63.   58408   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and characterization of a hemoglobin-ribavirin conjugate for targeted drug delivery</p>

    <p>          Brookes, S, Biessels, P, Ng, NFL, Woods, C, Bell, DN, and Adamson, G</p>

    <p>          BIOCONJUGATE CHEMISTRY: Bioconjugate Chem <b>2006</b>.  17(2): 530-537, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236226200036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236226200036</a> </p><br />

    <p>64.   58409   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Replication of West Nile virus in equine peripheral blood mononuclear cells</p>

    <p>          Garcia-Tapia, D, Loiacono, CM, and Klelboeker, SB</p>

    <p>          VETERINARY IMMUNOLOGY AND IMMUNOPATHOLOGY: Vet. Immunol. Immunopathol <b>2006</b>.  110(3-4): 229-244, 16 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236154700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236154700004</a> </p><br />

    <p>65.   58410   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          HepDirect prodrugs for targeting nucleotide-based antiviral drugs to the liver</p>

    <p>          Erion, MD, Bullough, DA, Lin, CC, and Hong, Z</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS: Curr. Opin. Investig. Drugs <b>2006</b>.  7(2): 109-117, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800002</a> </p><br />

    <p>66.   58411   DMID-LS-117; WOS-DMID-4/24/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A target on the move: Innate and adaptive immune escape strategies of hepatitis C virus</p>

    <p>          Thimme, R, Lohmann, V, and Weber, F</p>

    <p>          ANTIVIRAL RESEARCH: Antiviral Res <b>2006</b>.  69(3): 129-141, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236084800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236084800001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
