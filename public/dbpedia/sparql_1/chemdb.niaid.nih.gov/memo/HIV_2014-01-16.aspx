

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-01-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qvkZ7qf3Jzak9mui0rmca+lDBX4CQxy5wkEJgpM+vo7R0/E8Ykhahd4V8naTm0mUVkoeZFm6h9hk3l3ztyCxIot931AIbWIDeYoM/6zfFel3I801vpTsRrsiThU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4574FD9B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 3 - January 16, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24388952">ADS-J1 Inhibits HIV-1 Infection and Membrane Fusion by Targeting the Highly Conserved Pocket in the pp41 NHR-trimer.</a> Yu, F., L. Lu, Q. Liu, X. Yu, L. Wang, E. He, P. Zou, L. Du, R.W. Sanders, S. Liu, and S. Jiang. Biochimica et Biophysica Acta, 2014. <b>[Epub ahead of print]</b>. PMID[24388952].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24392742">4-Quinolone Alkaloids from Melochia odorata.</a> Jadulco, R.C., C.D. Pond, R.M. Van Wagoner, M. Koch, O.G. Gideon, T.K. Matainaho, P. Piskaut, and L.R. Barrows. Journal of Natural Products, 2014. <b>[Epub ahead of print]</b>. PMID[24392742].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24391217">Antibodies VRC01 and 10E8 Neutralize HIV-1 with High Breadth and Potency Even with Ig-framework Regions Substantially Reverted to Germline.</a>Georgiev, I.S., R.S. Rudicell, K.O. Saunders, W. Shi, T. Kirys, K. McKee, S. O&#39;Dell, G.Y. Chuang, Z.Y. Yang, G. Ofek, M. Connors, J.R. Mascola, G.J. Nabel, and P.D. Kwong. Journal of Immunology, 2014. <b>[Epub ahead of print]</b>. PMID[24391217].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24403586">The Mitochondrial Translocator Protein (TSPO) Inhibits HIV-1 Envelope Glycoprotein Biosynthesis Via the Endoplasmic Reticulum (ER)-associated Protein Degradation (ERAD) Pathway.</a> Zhou, T., Y. Dang, and Y.H. Zheng. Journal of Virology, 2014. <b>[Epub ahead of print]</b>. PMID[24403586].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24404757">Bioactive Compounds from Vitex leptobotrys.</a> Pan, W., K. Liu, Y. Guan, G.T. Tan, N.V. Hung, N.M. Cuong, D.D. Soejarto, J.M. Pezzuto, H.H. Fong, and H. Zhang. Journal of Natural Products, 2014. <b>[Epub ahead of print]</b>. PMID[24404757].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24401082">Indirubin 3&#39;-monoxime, from a Chinese Traditional Herbal Formula, Suppresses Viremia in Humanized Mice Infected with Multidrug-resistant HIV.</a>Heredia, A., S. Natesan, N.M. Le, S. Medina-Moreno, J.C. Zapata, M. Reitz, J. Bryant, and R.R. Redfield. AIDS Research and Human Retroviruses, 2014. <b>[Epub ahead of print]</b>. PMID[24401082].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0103-011614.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327254700069">Single, Double and Quadruple Alanine Substitutions at Oligomeric Interfaces Identify Hydrophobicity as the Key Determinant of Human Neutrophil Alpha Defensin HNP1 Function.</a> Zhao, L., W.D. Tolbert, B. Ericksen, C.Y. Zhan, X.J. Wu, W.R. Yuan, X. Li, M. Pazgier, and W.Y. Lu. PloS One, 2013. 8(11): p. e78937. ISI[000327254700069].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328231600009">Triflic acid-mediated Rearrangements of 3-Methoxy-8-oxabicyclo[3.2.1]octa-3,6-dien-2-ones: Synthesis of Methoxytropolones and Furans.</a>Williams, Y.D., C. Meck, N. Mohd, and R.P. Murelli. Journal of Organic Chemistry, 2013. 78(23): p. 11707-11713. ISI[000328231600009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327800000007">Some Mechanisms of FLIP Expression in Inhibition of HIV-1 Replication in Jurkat Cells, CD4+T Cells and PBMCs.</a> Tan, J.Y., X. Wang, K. Devadas, J.Q. Zhao, P.H. Zhang, and I. Hewlett. Journal of Cellular Physiology, 2013. 228(12): p. 2305-2313. ISI[000327800000007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328255300002">Synthesis of 2-Methyl-1-(5-methylfuran-2-yl)methyl- and 2-Methyl-1-(5-methylpyrrol-2-yl)methyl-1H-benzimidazoles.</a> Stroganova, T.A., V.M. Red&#39;kin, G.A. Kovalenko, V.K. Vasilin, and G.D. Krapivin. Chemistry of Heterocyclic Compounds, 2013. 49(9): p. 1264-1273. ISI[000328255300002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327906300007">Effect of Sex Steroid Hormones on Replication and Transmission of Major HIV Subtypes.</a> Ragupathy, V., K. Devadas, S.X. Tang, O. Wood, S. Lee, A. Dastyer, X. Wang, A. Dayton, and I. Hewlett. Journal of Steroid Biochemistry and Molecular Biology, 2013. 138: p. 63-71. ISI[000327906300007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328231600014">Proline-catalyzed Asymmetric Synthesis of Syn- and Anti-1,3-diamines.</a>Kumar, P., V. Jha, and R. Gonnade. Journal of Organic Chemistry, 2013. 78(23): p. 11756-11764. ISI[000328231600014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328240500001">Parallel Syntheses of Eight-membered Ring Sultams via Two Cascade Reactions in Water.</a> Ji, T., Y.J. Wang, M. Wang, B. Niu, P. Xie, C.U. Pittman, and A.H. Zhou. ACS Combinatorial Science, 2013. 15(12): p. 595-600. ISI[000328240500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327766200034">A CD4 Mimic as an HIV Entry Inhibitor: Pharmacokinetics.</a>Hashimoto, C., T. Narumi, H. Otsuki, Y. Hirota, H. Arai, K. Yoshimura, S. Harada, N. Ohashi, W. Nomura, T. Miura, T. Igarashi, S. Matsushita, and H. Tamamura. Bioorganic &amp; Medicinal Chemistry, 2013. 21(24): p. 7884-7889. ISI[000327766200034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328182900019">Enantioselective Inhibition of Reverse Transcriptase (RT) of HIV-1 by Non-racemic Indole-based Trifluoropropanoates Developed by Asymmetric Catalysis Using Recyclable Organocatalysts.</a>Han, X., W.J. Ouyang, B. Liu, W. Wang, P. Tien, S.W. Wu, and H.B. Zhou. Organic &amp; Biomolecular Chemistry, 2013. 11(48): p. 8463-8475. ISI[000328182900019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327162900010">Complexes of Neutralizing and Non-neutralizing Affinity Matured Fabs with a Mimetic of the Internal Trimeric Coiled-coil of HIV-1 gp41.</a>Gustchina, E., M. Li, R. Ghirlando, P. Schuck, J.M. Louis, J. Pierson, P. Rao, S. Subramaniam, A. Gustchina, G.M. Clore, and A. Wlodawer. PloS One, 2013. 8(11): p. e78187. ISI[000327162900010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000327921100021">Structural and Antivirial Studies of Dipetalactone and Its Methyl Derivative.</a> Drzewiecka, A., A.E. Koziol, P. Borowski, G. Sanna, G. Giliberti, P. La Colla, T. Zawadowski, and M. Struga. Journal of Molecular Structure, 2013. 1054: p. 150-156. ISI[000327921100021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000328344000012">Towards an Improved anti-HIV Activity of NRTI via Metal-organic Frameworks Nanoparticles.</a> Agostoni, V., T. Chalati, P. Horcajada, H. Willaime, R. Anand, N. Semiramoth, T. Baati, S. Hall, G. Maurin, H. Chacun, K. Bouchemal, C. Martineau, F. Taulelle, P. Couvreur, C. Rogez-Kreuz, P. Clayette, S. Monti, C. Serre, and R. Gref. Advanced Healthcare Materials, 2013. 2(12): p. 1630-1637. ISI[000328344000012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0103-011614.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
