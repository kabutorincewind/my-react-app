

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="csBXWUIFPtL56ONh3u4NQzNk6DlPkkg3zHCBOMvqeFu0DMPqafS6xubqBQK5YkZ/BNG8WK9x0s23kvJHiNGWwOaUG4U8KpaToGAkwC/eDWceubXWRDvNEKRLs7o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F0275E32" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 11 - November 24, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19926964?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">Efficacy and safety of TMC278 in antiretroviral-naive HIV-1 patients: week 96 results of a phase IIb randomized trial.</a>  Pozniak AL, Morales-Ramirez J, Katabira E, Steyn D, Lupo SH, Santoscoy M, Grinsztejn B, Ruxrungtham K, Rimsky LT, Vanveggel S, Boven K; on behalf of the TMC278-C204 study group.  AIDS. 2009 Nov 18. [Epub ahead of print]PMID: 19926964</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19922674?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Hypothesis of snake and insect venoms against Human Immunodeficiency Virus: a review.</a>  Meenakshisundaram R, Sweni S, Thirumalaikolundusubramanian P.  AIDS Res Ther. 2009 Nov 19;6(1):25. [Epub ahead of print]PMID: 19922674</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19919067?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Diketopiperazines from the Cordyceps-Colonizing Fungus Epicoccum nigrum.</a>  Guo H, Sun B, Gao H, Chen X, Liu S, Yao X, Liu X, Che Y.  J Nat Prod. 2009 Nov 17. [Epub ahead of print]PMID: 19919067</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19850483?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Peptide inhibitors of HIV-1 integrase: from mechanistic studies to improved lead compounds.</a>  Maes M, Levin A, Hayouka Z, Shalev DE, Loyter A, Friedler A.  Bioorg Med Chem. 2009 Nov 15;17(22):7635-42. Epub 2009 Oct 4.PMID: 19850483</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19839582?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">An antibody-recruiting small molecule that targets HIV gp120.</a>  Parker CG, Domaoal RA, Anderson KS, Spiegel DA.  J Am Chem Soc. 2009 Nov 18;131(45):16392-4.PMID: 19839582</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19819705?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Synthesis and biological evaluation of novel 2-(substituted phenylaminocarbonylmethylthio)-6-(2,6-dichlorobenzyl)-pyrimidin-4(3H)-ones as potent HIV-1 NNRTIs.</a>  Yu M, Liu X, Li Z, Liu S, Pannecouque C, Clercq ED.  Bioorg Med Chem. 2009 Nov 15;17(22):7749-54. Epub 2009 Sep 24.PMID: 19819705</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19807124?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">Chemical Library Screens Targeting an HIV-1 Accessory Factor/Host Cell Kinase Complex Identify Novel Antiretroviral Compounds.</a>  Emert-Sedlak L, Kodama T, Lerner EC, Dai W, Foster C, Day BW, Lazo JS, Smithgall TE.  ACS Chem Biol. 2009 Nov 20;4(11):939-947.PMID: 19807124</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271452200018">Isolation and Characterization of a Trypsin Inhibitor and a Lectin from Glycine max cv. Large Black Soybean.</a>  Xiu, JY; Tzi, BN.  FOOD SCIENCE AND BIOTECHNOLOGY, 2009; 18(5): 1173-1179.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271521000014">Bioorganic synthesis of a recombinant HIV-1 fusion inhibitor, SC35EK, with an N-terminal pyroglutamate capping group.</a>  Kajiwara, K; Watanabe, K; Tokiwa, R; Kurose, T; Ohno, H; Tsutsumi, H; Hata, Y; Izumi, K; Kodama, E; Matsuoka, M; Oishi, S; Fujii, N.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(23): 7964-7970.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
