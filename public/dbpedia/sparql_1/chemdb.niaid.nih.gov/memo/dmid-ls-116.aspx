

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-116.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mzZgaavc4GNRU+1zgZmxtrgYUvUv1JnsbbRPmXm4WUMjnAG6NDjzBr0JL2q4fm9pnm8hWCbq9svz53YoKUcLl330iE057i77//rpBwibOXdRF9UegZcXQDcxkko=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2C72F5D0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-116-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58308   DMID-LS-116; EMBASE-DMID-4/12/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A pyrimidine-pyrazolone nucleoside chimera with potent in vitro anti-orthopoxvirus activity</p>

    <p>          Fan, Xuesen, Zhang, Xinying, Zhou, Longhu, Keith, Kathy A, Kern, Earl R, and Torrence, Paul F</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4JN2NRM-4/2/ad734ab55baf9d8b258f1998c2dbc564">http://www.sciencedirect.com/science/article/B6TF9-4JN2NRM-4/2/ad734ab55baf9d8b258f1998c2dbc564</a> </p><br />

    <p>2.     58309   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral compounds and one new iridoid glycoside from Cornus officinalis</p>

    <p>          Wang, Y, Li, ZQ, Chen, LR, and Xu, XJ</p>

    <p>          PROGRESS IN NATURAL SCIENCE: Prog. Nat. Sci <b>2006</b>.  16(2): 142-146, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236287000005</a> </p><br />

    <p>3.     58310   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in anti-influenza agents with neuraminidase as target</p>

    <p>          Zhang, H and Xu, WF</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY: Mini-Rev. Med. Chem <b>2006</b>.  6(4): 429-448, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236267700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236267700008</a> </p><br />

    <p>4.     58311   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of hepatitis C virus (HCV) NS5B-nucleolin interaction on HCV replication with HCV subgenomic replicon</p>

    <p>          Shimakami, T, Honda, M, Kusakawa, T, Murata, T, Shimotohno, K, Kaneko, S, and Murakami, S</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2006</b>.  80(7): 3332-3340, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200020</a> </p><br />

    <p>5.     58312   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Brd4 is involved in multiple processes of the bovine papillomavirus type 1 life cycle</p>

    <p>          Ilves, I, Maemets, K, Silla, T, Janikson, K, and Ustav, M</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2006</b>.  80(7): 3660-3665, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200055</a> </p><br />

    <p>6.     58313   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recovery of a neurovirulent human coronavirus OC43 from an infectious cDNA clone</p>

    <p>          St-Jean, JR, Desforges, M, Almazan, F, Jacomy, H, Enjuanes, L, and Talbot, PJ</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2006</b>.  80(7): 3670-3674, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236305200057</a> </p><br />

    <p>7.     58314   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleic-acid-based antiviral agents against positive single-stranded RNA viruses</p>

    <p>          Lim, TW, Yuan, J, Liu, Z, Qiu, DX, Sall, A, and Yang, DC</p>

    <p>          CURRENT OPINION IN MOLECULAR THERAPEUTICS: Curr. Opin. Mol. Ther <b>2006</b>.  8(2): 104-107, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236285300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236285300003</a> </p><br />
    <br clear="all">

    <p>8.     58315   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oligonucleotide analogs as antiviral agents</p>

    <p>          Fabani, MM, Turner, JJ, and Gait, MJ</p>

    <p>          CURRENT OPINION IN MOLECULAR THERAPEUTICS: Curr. Opin. Mol. Ther <b>2006</b>.  8(2): 108-114, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236285300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236285300004</a> </p><br />

    <p>9.     58316   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An efficient, asymmetric solid-phase synthesis of benzothiadiazine-substituted tetramic acids: Potent inhibitors of the hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Evans, KA, Chai, DP, Graybill, TL, Burton, G, Sarisky, RT, Lin-Goerke, J, Johnston, VK, and Rivero, RA</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS: Bioorg. Med. Chem. Lett <b>2006</b>.  16(8): 2205-2208, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000030</a> </p><br />

    <p>10.   58317   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prospects and strategies for the discovery and development of small-molecule inhibitors of six-helix bundle formation in class 1 viral fusion proteins</p>

    <p>          Debnath, AK</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS: Curr. Opin. Investig. Drugs <b>2006</b>.  7(2): 118-127, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800003</a> </p><br />

    <p>11.   58318   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential applications of siRNA in hepatitis C virus therapy</p>

    <p>          Smolic, R, Volarevic, M, Wu, CH, and Wu, GY</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS: Curr. Opin. Investig. Drugs <b>2006</b>.  7(2): 142-146, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800006</a> </p><br />

    <p>12.   58319   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SCV-07 - SciClone pharmaceuticals/verta</p>

    <p>          Aspinall, RJ and Pockros, PJ</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS: Curr. Opin. Investig. Drugs <b>2006</b>.  7(2): 180-185, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800011</a> </p><br />

    <p>13.   58320   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Therapeutic potential of an adenovirus expressing p73 beta, a p53 homologue, against human papilloma virus positive cervical cancer in vitro and in vivo</p>

    <p>          Das, S and Somasundaram, K</p>

    <p>          CANCER BIOLOGY &amp; THERAPY: Cancer Biol. Ther <b>2006</b>.  5(2): 210-217, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236044600022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236044600022</a> </p><br />
    <br clear="all">

    <p>14.   58321   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Leflunomide prevents alveolar fluid clearance inhibition by respiratory syncytial virus</p>

    <p>          Davis, IC, Lazarowski, ER, Hickman-Davis, JM, Fortenberry, JA, Chen, FP, Zhao, XD, Sorscher, E, Graves, LM, Sullender, WM, and Matalon, S</p>

    <p>          AMERICAN JOURNAL OF RESPIRATORY AND CRITICAL CARE MEDICINE: Am. J. Respir. Crit. Care Med <b>2006</b>.  173(6): 673-682, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236182100015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236182100015</a> </p><br />

    <p>15.   58322   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An in vitro fluorescence screen to identify antivirals that disrupt hepatitis B virus capsid assembly</p>

    <p>          Stray, SJ, Johnson, JM, Kopek, BG, and Zlotnick, A</p>

    <p>          NATURE BIOTECHNOLOGY: Nat. Biotechnol <b>2006</b>.  24(3): 358-362, 5</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235868600040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235868600040</a> </p><br />

    <p>16.   58323   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Targeted delivery of ribavirin improves outcome of murine viral fulminant hepatitis via enhanced anti-viral activity</p>

    <p>          Levy, GA, Adamson, G, Phillips, MJ, Scrocchi, LA, Fung, L, Biessels, P, Ng, NF, Ghanekar, A, Rowe, A, Ma, MXZ, Levy, A, Koscik, C, He, W, Gorczynski, R, Brookes, S, Woods, C, McGilvray, ID, and Bell, D</p>

    <p>          HEPATOLOGY: Hepatology <b>2006</b>.  43(3): 581-591, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235911200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235911200026</a> </p><br />

    <p>17.   58324   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Deciphering the biosynthetic codes for the potent anti-SARS-CoV cyclodepsipeptide valinomycin in Streptomyces tsusimaensis ATCC 15141</p>

    <p>          Cheng, YQ</p>

    <p>          CHEMBIOCHEM: ChemBioChem <b>2006</b>.  7(3): 471-477, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235930800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235930800012</a> </p><br />

    <p>18.   58325   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          VP35 knockdown inhibits Ebola virus amplification and protects against lethal infection in mice</p>

    <p>          Enterlein, S, Warfield, KL, Swenson, DL, Stein, DA, Smith, JL, Gamble, CS, Kroeker, AD, Iversen, PL, Bavari, S, and Muhlberger, E</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2006</b>.  50( 3): 984-993, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300023</a> </p><br />

    <p>19.   58326   DMID-LS-116; WOS-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SCH 503034, a mechanism-based inhibitor of hepatitis C virus NS3 protease, suppresses polyprotein maturation and enhances the antiviral activity of alpha interferon in replicon cells</p>

    <p>          Malcolm, BA, Liu, R, Lahser, F, Agrawal, S, Belanger, B, Butkiewicz, N, Chase, R, Gheyas, F, Hart, A, Hesk, D, Ingravallo, P, Jiang, C, Kong, R, Lu, J, Pichardo, J, Prongay, A, Skelton, A, Tong, X, Venkatraman, S, Xia, E, Girijavallabhan, V, and Njoroge, FG</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2006</b>.  50( 3): 1013-1020, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235786300026</a> </p><br />
    <br clear="all">

    <p>20.   58327   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential antivirals and antiviral strategies against SARS coronavirus infections</p>

    <p>          De Clercq, E</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(2): 291-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597209&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597209&amp;dopt=abstract</a> </p><br />

    <p>21.   58328   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Smallpox antiviral drug development: satisfying the animal efficacy rule</p>

    <p>          Jordan, R and Hruby, D</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(2): 277-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597208&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16597208&amp;dopt=abstract</a> </p><br />

    <p>22.   58329   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oligonucleotide-based antiviral strategies</p>

    <p>          Schubert, S and Kurreck, J</p>

    <p>          Handb Exp Pharmacol <b>2006</b>.(173): 261-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16594620&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16594620&amp;dopt=abstract</a> </p><br />

    <p>23.   58330   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Non-Nucleosides: 7- and 1,3-Substituents of New Pyrrolo[2,3-d]pyrimidin-4-ones on Antiviral Activity</p>

    <p>          Hilmy, KM</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.  339(4): 174-181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16586425&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16586425&amp;dopt=abstract</a> </p><br />

    <p>24.   58331   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pandemic influenza: using evidence on vaccines and antivirals for clinical decisions and policy making</p>

    <p>          Granados, A, Goodman, C, and Eklund, L</p>

    <p>          Eur Respir J <b>2006</b>.  27(4): 661-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16585070&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16585070&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>25.   58332   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Unsaturated N-acetyl- D-glucosaminuronic acid glycosides as inhibitors of influenza virus sialidase</p>

    <p>          Mann, MC, Islam, T, Dyason, JC, Florio, P, Trower, CJ, Thomson, RJ, and Itzstein, M</p>

    <p>          Glycoconj J <b>2006</b>.  23(1-2): 127-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16575530&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16575530&amp;dopt=abstract</a> </p><br />

    <p>26.   58333   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The CPSF30 binding site on the NS1A protein of influenza A virus is a potential antiviral target</p>

    <p>          Twu, KY, Noah, DL, Rao, P, Kuo, RL, and Krug, RM</p>

    <p>          J Virol <b>2006</b>.  80(8): 3957-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571812&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571812&amp;dopt=abstract</a> </p><br />

    <p>27.   58334   DMID-LS-116; LR-14931-LWC; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          1,4-benzodiazepines as inhibitors of respiratory syncytial virus</p>

    <p>          Carter, MC, Alber, DG, Baxter, RC, Bithell, SK, Budworth, J, Chubb, A, Cockerill, GS, Dowdell, VC, Henderson, EA, Keegan, SJ, Kelsey, RD, Lockyer, MJ, Stables, JN, Wilson, LJ, and Powell, KL</p>

    <p>          J Med Chem <b>2006</b>.  49(7): 2311-2319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570927&amp;dopt=abstract</a> </p><br />

    <p>28.   58335   DMID-LS-116; PUBMED-DMID-4/10/2006; LR-14642 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ether Lipid Ester Derivatives of Cidofovir Inhibit Polyomavirus BK Replication In Vitro</p>

    <p>          Randhawa, P, Farasati, NA, Shapiro, R, and Hostetler, KY</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1564-1566</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569886&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569886&amp;dopt=abstract</a> </p><br />

    <p>29.   58336   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sialidase fusion protein as a novel broad-spectrum inhibitor of influenza virus infection</p>

    <p>          Malakhov, MP, Aschenbrenner, LM, Smee, DF, Wandersee, MK, Sidwell, RW, Gubareva, LV, Mishin, VP, Hayden, FG, Kim, do H, Ing, A, Campbell, ER, Yu, M, and Fang, F</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1470-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569867&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.   58337   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Activity and Mechanism of Action of N-Methanocarbathymidine against Herpesvirus and Orthopoxvirus Infections</p>

    <p>          Prichard, MN, Keith, KA, Quenelle, DC, and Kern, ER</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1336-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569849&amp;dopt=abstract</a> </p><br />

    <p>31.   58338   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus RNA replication by short hairpin RNA synthesized by T7 RNA polymerase in hepatitis C virus subgenomic replicons</p>

    <p>          Hamazaki, H, Ujino, S, Miyano-Kurosaki, N, Shimotohno, K, and Takaku, H</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  343(3): 988-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566896&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566896&amp;dopt=abstract</a> </p><br />

    <p>32.   58339   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of the anti-hepatitis C virus effects of cyclophilin inhibitors, cyclosporin A, and NIM811</p>

    <p>          Goto, K, Watashi, K, Murata, T, Hishiki, T, Hijikata, M, and Shimotohno, K</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  343(3): 879-884</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16564500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16564500&amp;dopt=abstract</a> </p><br />

    <p>33.   58340   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory efficacy of cyclosal-nucleoside monophosphates of aciclovir and brivudin on DNA synthesis of orthopoxvi ruses</p>

    <p>          Sauerbrei, A, Meier, C, Meerbach, A, and Wutzler, P</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(1): 25-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542003&amp;dopt=abstract</a> </p><br />

    <p>34.   58341   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The effect of a methyl or 2-fluoroethyl substituent at the N-3 position of thymidine, 3&#39;-fluoro-3&#39;-deoxythymidine and 1-beta-D-arabinosylthymine on their antiviral and cytostatic activity in cell culture</p>

    <p>          Balzarini, J,  Celen, S, Karlsson, A, de Groot, T, Verbruggen, A, and Bormans, G</p>

    <p>          Antivir Chem Chemother <b>2006</b>.  17(1): 17-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16542002&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>35.   58342   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of synthetic oligonucleotides as inhibitors of West Nile virus replication</p>

    <p>          Torrence, PF, Gupta, N, Whitney, C, and Morrey, JD</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16540182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16540182&amp;dopt=abstract</a> </p><br />

    <p>36.   58343   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Developments in antiviral drug design, discovery and development in 2004</p>

    <p>          Meanwell, NA, Belema, M, Carini, DJ, D&#39;Andrea, SV, Kadow, JF, Krystal, M, Naidu, BN, Regueiro-Ren, A, Scola, PM, Sit, SY, Walker, MA, Wang, T, and Yeung, KS</p>

    <p>          Curr Drug Targets Infect Disord <b>2005</b>.  5(4): 307-400</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16535860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16535860&amp;dopt=abstract</a> </p><br />

    <p>37.   58344   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural basis for the activation of flaviviral NS3 proteases from dengue and West Nile virus</p>

    <p>          Erbel, P, Schiering, N, D&#39;Arcy, A, Renatus, M, Kroemer, M, Lim, SP, Yin, Z, Keller, TH, Vasudevan, SG, and Hommel, U</p>

    <p>          Nat Struct Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16532006&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16532006&amp;dopt=abstract</a> </p><br />

    <p>38.   58345   DMID-LS-116; PUBMED-DMID-4/10/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Distinct thymidine kinases encoded by cowpox virus and herpes simplex virus contribute significantly to the differential antiviral activity of nucleoside analogs</p>

    <p>          Prichard, MN, Williams, AD, Keith, KA, Harden, EA, and Kern, ER</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16530858&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16530858&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
