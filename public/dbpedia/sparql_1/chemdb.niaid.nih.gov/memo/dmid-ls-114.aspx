

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-114.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="e+l9IJ/3+QSsdXJ1pwSxZi0FmZhLFrKpEneAJth1AXH/Hj7LLpznhqGXZ9ZdvSPUrAr6+qxEX4QwulVO+V3PijZy1vCb4Kvgo16HqV6AFaKYJjvpDn4Tz/Q+GJU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D3B9BA61" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-114-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58256   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          H5N1 influenza virus evolution: a comparison of different epidemics in birds and humans (1997-2004)</p>

    <p>          Campitelli, L, Ciccozzi, M, Salemi, M, Taglia, F, Boros, S, Donatelli, I, and Rezza, G</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 4): 955-60</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16528045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16528045&amp;dopt=abstract</a> </p><br />

    <p>2.     58257   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of {alpha}-glucosidase inhibitors on viral morphogenesis and binding properties of hepatitis C virus-like particles</p>

    <p>          Chapel, C, Garcia, C, Roingeard, P, Zitzmann, N, Dubuisson, J, Dwek, RA, Trepo, C, Zoulim, F, and Durantel, D</p>

    <p>          J Gen Virol <b>2006</b>.  87(Pt 4): 861-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16528036&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16528036&amp;dopt=abstract</a> </p><br />

    <p>3.     58258   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          5-Arylethynyl-2&#39;-deoxyuridines, compounds active against HSV-1</p>

    <p>          Skorobogatyi, MV, Ustinov, AV, Stepanova, IA, Pchelintseva, AA, Petrunina, AL, Andronova, VL, Galegov, GA, Malakhov, AD, and Korshun, VA</p>

    <p>          Org Biomol Chem <b>2006</b>.  4(6): 1091-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16525553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16525553&amp;dopt=abstract</a> </p><br />

    <p>4.     58259   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The use of the hydrodynamic HBV animal model to study HBV biology and anti-viral therapy</p>

    <p>          Ketzinel-Gilad, M, Zauberman, A, Nussbaum, O, Shoshany, Y, Ben-Moshe, O, Pappo, O, Felig, Y, Ilan, E, Wald, H, Dagan, S, and Galun, E</p>

    <p>          Hepatol Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16520091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16520091&amp;dopt=abstract</a> </p><br />

    <p>5.     58260   DMID-LS-114; EMBASE-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virology of hepatitis B and C viruses and antiviral targets: Proceedings of the 1st European Consensus Conference on the Treatment of Chronic Hepatitis B and C in HIV Co-infected Patients</p>

    <p>          Pawlotsky, Jean-Michel</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(Supplement 1): S10-S13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4HM86JR-2/2/512219f0fa4da965a8074e3f9dd992e5">http://www.sciencedirect.com/science/article/B6W7C-4HM86JR-2/2/512219f0fa4da965a8074e3f9dd992e5</a> </p><br />
    <br clear="all">

    <p>6.     58261   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          NFkB negatively regulates interferon-induced gene expression and anti-influenza activity</p>

    <p>          Wei, L, Sandbulte, MR, Thomas, PG, Webby, RJ, Homayouni, R, and Pfeffer, LM</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16517601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16517601&amp;dopt=abstract</a> </p><br />

    <p>7.     58262   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Combination chemotherapy, a potential strategy for reducing the emergence of drug-resistant influenza A variants</p>

    <p>          Ilyushina, NA, Bovin, NV, Webster, RG, and Govorkova, EA</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16516984&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16516984&amp;dopt=abstract</a> </p><br />

    <p>8.     58263   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Entecavir: A new treatment option for chronic hepatitis B</p>

    <p>          Zoulim, F</p>

    <p>          J Clin Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515882&amp;dopt=abstract</a> </p><br />

    <p>9.     58264   DMID-LS-114; EMBASE-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and the biological evaluation of 2-benzenesulfonylmethyl-5-substituted-sulfanyl-[1,3,4]-oxadiazoles as potential anti-hepatitis B virus agents</p>

    <p>          Tan, Theresa May Chin, Chen, Yu, Kong, Kah Hoe, Bai, Jing, Li, Yang, Lim, Seng Gee, Ang, Thiam Hong, and Lam, Yulin</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JF95YJ-1/2/7c1d0b5a2243861352eb5e905cb53269">http://www.sciencedirect.com/science/article/B6T2H-4JF95YJ-1/2/7c1d0b5a2243861352eb5e905cb53269</a> </p><br />

    <p>10.   58265   DMID-LS-114; PUBMED-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          2-(2-Thienyl)-5,6-dihydroxy-4-carboxypyrimidines as Inhibitors of the Hepatitis C Virus NS5B Polymerase: Discovery, SAR, Modeling, and Mutagenesis</p>

    <p>          Koch, U, Attenni, B, Malancona, S, Colarusso, S, Conte, I, Di, Filippo M, Harper, S, Pacini, B, Giomini, C, Thomas, S, Incitti, I, Tomei, L, De, Francesco R, Altamura, S, Matassa, VG, and Narjes, F</p>

    <p>          J Med Chem <b>2006</b>.  49(5): 1693-1705</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509585&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16509585&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58266   DMID-LS-114; EMBASE-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral and antituberculous activity of Helichrysum melanacme constituents</p>

    <p>          Lall, N, Hussein, AA, and Meyer, JJM</p>

    <p>          Fitoterapia <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4JF97B2-1/2/de53525aafbb02316a052d4fbf3f89f9">http://www.sciencedirect.com/science/article/B6VSC-4JF97B2-1/2/de53525aafbb02316a052d4fbf3f89f9</a> </p><br />

    <p>12.   58267   DMID-LS-114; EMBASE-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Distinct thymidine kinases encoded by cowpox virus and herpes simplex virus contribute significantly to the differential antiviral activity of nucleoside analogs</p>

    <p>          Prichard, Mark N, Williams, Angela D, Keith, Kathy A, Harden, Emma A, and Kern, Earl R</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JCBJRF-1/2/8da1215e2419dd808ebab359b0fbadf4">http://www.sciencedirect.com/science/article/B6T2H-4JCBJRF-1/2/8da1215e2419dd808ebab359b0fbadf4</a> </p><br />

    <p>13.   58268   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Clinical Need for New Antiviral Drugs Directed Against Influenza Virus</p>

    <p>          Mccullers, J.</p>

    <p>          Journal of Infectious Diseases <b>2006</b>.  193(6): 751-753</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235536200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235536200001</a> </p><br />

    <p>14.   58269   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Screening of Bicyclic Carbohydrate-Based Compounds: a Novel Type of Antivirals</p>

    <p>          Van Hoof, S. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(6): 1495-1498</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235492800007</a> </p><br />

    <p>15.   58270   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Pyranobenzothiophenes as Inhibitors of Hcv Rna Dependent Rna Polymerase.</p>

    <p>          Gopalsamy, A. <i> et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U166</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600321">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600321</a> </p><br />

    <p>16.   58271   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of Carbazole and Cyclopentaindole Derivatives as Inhibitors of Hcv Rna Dependent Rna Polymerase.</p>

    <p>          Gopalsamy, A. <i> et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600325">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600325</a> </p><br />

    <p>17.   58272   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel C-Terminal Functionalities in Hepatitis C Virus Ns3 Protease Inhibitors.</p>

    <p>          Ronn, R. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U167-U168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600329">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600329</a> </p><br />
    <br clear="all">

    <p>18.   58273   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Toward Smaller Hcv Ns3-4a Protease Inhibitors: 3-Substituted Proline-Based Tripeptide Scaffolds.</p>

    <p>          Perni, R. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  229: U171</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600349">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235066600349</a> </p><br />

    <p>19.   58274   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Resistance in Influenza Viruses - Implications for Management and Pandemic Response</p>

    <p>          Hayden, F.</p>

    <p>          New England Journal of Medicine <b>2006</b>.  354(8): 785-788</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235456200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235456200001</a> </p><br />

    <p>20.   58275   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High Levels of Adamantane Resistance Among Influenza a (H3n2) Viruses and Interim Guidelines for Use of Antiviral Agents - United States, 2005-06 Influenza Season (Reprinted From Mmwr, Vol 55, Pg 44-46, 2006)</p>

    <p>          Bright, R. <i>et al.</i></p>

    <p>          Jama-Journal of the American Medical Association <b>2006</b>.  295(8): 881-882</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235459200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235459200009</a> </p><br />

    <p>21.   58276   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyridine N-Oxide Derivatives Are Inhibitory to the Human Sars and Feline Infectious Peritonitis Coronavirus in Cell Culture</p>

    <p>          Balzarini, J. <i> et al.</i></p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  57(3): 472-481</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235282500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235282500011</a> </p><br />

    <p>22.   58277   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Impact of Naturally Occurring Variants of Hcv Protease on the Binding of Different Classes of Protease Inhibitors</p>

    <p>          Tong, X. <i>et al.</i></p>

    <p>          Biochemistry <b>2006</b>.  45(5): 1353-1361</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235244500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235244500001</a> </p><br />

    <p>23.   58278   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiretroviral Drugs in Severe Acute Respiratory Syndrome</p>

    <p>          Yazdanpanah, Y. and Guery, B.</p>

    <p>          Presse Medicale <b>2006</b>.  35(1): 105-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235078500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235078500005</a> </p><br />

    <p>24.   58279   DMID-LS-114; WOS-DMID-3/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Role of an Amphiphilic Capping Group in Covalent and Non-Covalent Dipeptide Inhibitors of Hcvns3 Serine Protease</p>

    <p>          Colarusso, S. <i> et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2005</b>.  2(2): 113-117</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
