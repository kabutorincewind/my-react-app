

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-08-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="2YSF+JVFhtw8NalyI23Gd4J+iXW5gaa/nwJ87wwoeCzr1RTf9Rl5JO02gp2H2pGaOFVKZYCgfEaCbNVBtSV3QCcexWbZsbn8S88da8rjLznkDDEIdmyYxtM7Xj4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4D7EFC30" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">August 4 - August 19, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20713670">Activity of the histone deacetylase inhibitor FR235222 on Toxoplasma gondii: inhibition of stage conversion of the parasite cyst form and study of new derivative compounds.</a> Maubon, D., A. Bougdour, Y.S. Wong, M.P. Brenier-Pinchart, A. Curt, M.A. Hakimi, and H. Pelloux. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20713670].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20706578">A screening pipeline for antiparasitic agents targeting cryptosporidium inosine monophosphate dehydrogenase.</a> Sharling, L., X. Liu, D.R. Gollapalli, S.K. Maurya, L. Hedstrom, and B. Striepen. PLoS Negl Trop Dis. 4(8); PMID[20706578].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20698542">Identification and Development of Novel Inhibitors of Toxoplasma gondii Enoyl Reductase.</a> Tipparaju, S.K., S.P. Muench, E.J. Mui, S.N. Ruzheinikov, J.Z. Lu, S.L. Hutson, M.J. Kirisits, S.T. Prigge, C.W. Roberts, F.L. Henriquez, A.P. Kozikowski, D.W. Rice, and R.L. McLeod. J Med Chem. <b>[Epub ahead of print]</b>; PMID[20698542].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20711160">Antifungal and post-antifungal effects of chlorhexidine, fluconazole, chitosan and its combinations on candida albicans.</a> Calamari, S.E., M.A. Bojanich, S.R. Barembaum, N. Berdicevski, and A.I. Azcurra. Med Oral Patol Oral Cir Bucal. <b>[Epub ahead of print]</b>; PMID[20711160].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20714310">Seasonal Variation, Chemical Composition, and Analgesic and Antimicrobial Activities of the Essential Oil from Leaves of Tetradenia riparia (Hochst.)</a> Gazim, Z.C., A.C. Amorim, A.M. Hovell, C.M. Rezende, I.A. Nascimento, G.A. Ferreira, and D.A. Cortez. Codd in Southern Brazil. Molecules. 15(8): p. 5509-24; PMID[20714310].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20687790">Activated fly ash catalyzed facile synthesis of novel spiro imidazolidine derivatives as potential antibacterial and antifungal agents.</a> Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. J Enzyme Inhib Med Chem. <b>[Epub ahead of print]</b>; PMID[20687790].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20667743">Novel antifungal agents: triazolopyridines as inhibitors of beta-1,6-glucan synthesis.</a> Kuroyanagi, J., K. Kanai, Y. Sugimoto, T. Fujisawa, C. Morita, T. Suzuki, K. Kawakami, and M. Takemura. Bioorg Med Chem. 18(16): p. 5845-54; PMID[20667743].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20620056">New antifungal peptides. Synthesis, bioassays and initial structure prediction by CD spectroscopy.</a> Olivella, M.S., A.M. Rodriguez, S.A. Zacchino, C. Somlai, B. Penke, V. Farkas, A. Perczel, and R.D. Enriz. Bioorg Med Chem Lett. 20(16): p. 4808-11; PMID[20620056].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20538054">Essential oils from Distichoselinum tenuifolium: chemical composition, cytotoxicity, antifungal and anti-inflammatory properties.</a> Tavares, A.C., M.J. Goncalves, M.T. Cruz, C. Cavaleiro, M.C. Lopes, J. Canhoto, and L.R. Salgueiro. J Ethnopharmacol. 130(3): p. 593-8; PMID[20538054].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20707344">Lycopersicon esculentum Seeds: An Industrial Byproduct as an Antimicrobial Agent.</a> Taveira, M., L.R. Silva, L.A. Vale-Silva, E. Pinto, P. Valentao, F. Ferreres, P. Guedes de Pinho, and P.B. Andrade. J Agric Food Chem. <b>[Epub ahead of print]</b>; PMID[20707344].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20696500">Schiff bases of indoline-2,3-dione (isatin) derivatives and nalidixic acid carbohydrazide, synthesis, antitubercular activity and pharmacophoric model building.</a> Aboul-Fadl, T., F.A. Bin-Jubair, and O. Aboul-Wafa. Eur J Med Chem. <b>[Epub ahead of print]</b>; PMID[20696500].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20691508">Isoxazole analogs of curcuminoids with highly potent multidrug-resistant antimycobacterial activity.</a> Changtam, C., P. Hongmanee, and A. Suksamrarn. Eur J Med Chem. <b>[Epub ahead of print]</b>; PMID[20691508].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20718072">NOC Chemistry for Tuberculosis-Further Investigations on the Structure-Activity Relationships of Antitubercular Isoxazole-3-carboxylic Acid Ester Derivatives.</a> Pieroni, M., A. Lilienkampf, Y. Wang, B. Wan, S. Cho, S.G. Franzblau, and A.P. Kozikowski. ChemMedChem. <b>[Epub ahead of print]</b>; PMID[20718072].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0804-081910.</p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280273900010">Synthesis and Antitubercular Activity of Heteroaromatic Isonicotinoyl and 7-Chloro-4-Quinolinyl Hydrazone Derivatives.</a> Ferreira, M.D., R.S.B. Goncalves, L.N.D. Cardoso, C.R. Kaiser, A.L.P. Candea, M. Henriques, M.C.S. Lourenco, F. Bezerra, and M.V.N. de Souza. Thescientificworldjournal. 10: p. 1347-1355; ISI[000280273900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280183300009">Inhibition Studies of Mycobacterium tuberculosis Salicylate Synthase (Mbtl).</a> Manos-Turvey, A., E.M.M. Bulloch, P.J. Rutledge, E.N. Baker, J.S. Lott, and R.J. Payne. ChemMedChem. 5(7): p. 1067-1079; ISI[000280183300009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280122700007">Synthesis of New 3-(4-Fluoronaphthyl)-5-(aryl)-2-isoxazolines and Their Antimicrobial and Spermicidal Activity.</a> Sharma, S., V.N. Pathak, H. Madan, and A. Sharma. Indian Journal of Heterocyclic Chemistry. 19(4): p. 337-340; ISI[000280122700007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280373800006">Azaphilone and Isocoumarin Derivatives from the Endophytic Fungus Penicillium sclerotiorum PSU-A13.</a> Arunpanichlert, J., V. Rukachaisirikul, Y. Sukpondma, S. Phongpaichit, S. Tewtrakul, N. Rungjindamai, and J. Sakayaroj. Chemical &amp; Pharmaceutical Bulletin. 58(8): p. 1033-1036; ISI[000280373800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280476900003">Efficient &quot;one-pot&quot; synthesis of novel 3-azabicyclic spiro-1&#39;,2&#39;,4&#39;-triazolidin-3&quot;-thiones catalyzed by potassium superoxide and their in vitro antibacterial and antifungal activities.</a> Kanagarajan, V. and M. Gopalakrishnan. European Review for Medical and Pharmacological Sciences. 14(6): p. 513-520; ISI[000280476900003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280340900017">Synthesis and Preliminary Evaluation of the Antimicrobial Activity of Selected 3-Benzofurancarboxylic Acid Derivatives.</a> Kossakowski, J., M. Krawiecka, B. Kuran, J. Stefanska, and I. Wolska. Molecules. 15(7): p. 4737-4749; ISI[000280340900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280340900026">Antifungal Activity of Extracts and Prenylated Coumarins Isolated from Baccharis darwinii Hook &amp; Arn. (Asteraceae).</a> Kurdelas, R.R., B. Lima, A. Tapia, G.E. Feresin, M.G. Sierra, M.V. Rodriguez, S. Zacchino, R.D. Enriz, and M.L. Freile. Molecules. 15(7): p. 4898-4907; ISI[000280340900026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804081910.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280244700039">Synthesis of Novel Halobenzyloxy and Alkoxy 1,2,4-Triazoles and Evaluation for Their Antifungal and Antibacterial Activities.</a> Wan, K. and C.H. Zhou. Bulletin of the Korean Chemical Society. 31(7): p. 2003-2010; ISI[000280244700039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280343400007">Synthesis, antimicrobial and insecticidal activity of some 4H-1,2,4 triazole derivatives.</a> Gautam, N. and O.P. Chourasia. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry. 49(7): p. 956-959; ISI[000280343400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280477000003">Strange formation of N-hydroxy-3,3-dimethyl-2,6-diarylpiperidin-4-one and its oxime derivative - Synthesis, stereochemistry, antibacterial and antifungal activity.</a> Kanagarajan, V., P. Sureshkumar, and M. Gopalakrishnan. European Review for Medical and Pharmacological Sciences. 14(7): p. 597-604; ISI[000280477000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280450700019">2-Substituted 4H-[1,3]thiazolo[3,2-a][1,3,5]triazine-4-thiones: Synthesis, Crystal Structure, and Antifungal Activity.</a> Saeed, S., N. Rashid, P.G. Jones, and U. Yunas. Journal of Heterocyclic Chemistry. 47(4): p. 908-912; ISI[000280450700019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0804-081910.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
