

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-148.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TNYK23h51IHUcUJr+DCzzB1LHka5tIkNWTx211qCsB+YdZEnGEJZOOvPzZIMW9eFBDMA/TsgZfx8PD7aMBD3Odg6t8WvAMNCue9zO6TY6H5ZdoHysUvh60kPrM8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A281C946" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-148-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61163   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Identification of antibacterial and antiviral activities of novel fused 1,2,4-triazine esters</p>

    <p>          Sztanke, Krzysztof, Pasternak, Kazimierz, Rajtar, Barbara, Sztanke, Malgorzata, Majek, Magdalena, and Polz-Dacewicz, Malgorzata</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(16): 5480-5486</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-2/2/10c0c3c65564bce8ebcf89d2b3c817b2">http://www.sciencedirect.com/science/article/B6TF8-4NTJGXV-2/2/10c0c3c65564bce8ebcf89d2b3c817b2</a> </p><br />

    <p>2.     61164   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Combination of Small Interfering Rna and Lamivudine on Inhibition of Human B Virus Replication in Hepg2.2.15 Cells</p>

    <p>          Li, G. <i>et al.</i></p>

    <p>          World Journal of Gastroenterology <b>2007</b>.  13(16): 2324-2327</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246791900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246791900008</a> </p><br />

    <p>3.     61165   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Mutations in West Nile virus nonstructural proteins that facilitate replicon persistence in vitro attenuate virus replication in vitro and in vivo</p>

    <p>          Rossi, Shannan L, Fayzulin, Rafik, Dewsbury, Nathan, Bourne, Nigel, and Mason, Peter W</p>

    <p>          Virology <b>2007</b>.  364(1): 184-195</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NB3897-1/2/26f85a619a7a5771c4e857e835e0a3b1">http://www.sciencedirect.com/science/article/B6WXR-4NB3897-1/2/26f85a619a7a5771c4e857e835e0a3b1</a> </p><br />

    <p>4.     61166   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Benefits and Adverse Effects of Hepatitis C Screening: Early Results of a Screening Program</p>

    <p>          Trepka, M. <i>et al.</i></p>

    <p>          Journal of Public Health Management and Practice <b>2007</b>.  13(3): 263-269</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246248300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246248300006</a> </p><br />

    <p>5.     61167   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          An analogue of AICAR with dual inhibitory activity against WNV and HCV NTPase/helicase: Synthesis and in vitro screening of 4-carbamoyl-5-(4,6-diamino-2,5-dihydro-1,3,5-triazin-2-yl)imidazole-1-[beta]-d-ribofuranoside</p>

    <p>          Ujjinamatada, Ravi K, Baier, Andrea, Borowski, Peter, and Hosmane, Ramachandra S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(8): 2285-2288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MXBF90-9/2/93a2fd7ee457bd5b21a793aba377918e">http://www.sciencedirect.com/science/article/B6TF9-4MXBF90-9/2/93a2fd7ee457bd5b21a793aba377918e</a> </p><br />

    <p>6.     61168   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Yellow Fever Initiative</p>

    <p>          Stephenson, J. </p>

    <p>          Jama-Journal of the American Medical Association <b>2007</b>.  297(23): 2578</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247352600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247352600006</a> </p><br />
    <br clear="all">

    <p>7.     61169   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Flavivirus Molecular Biology in the 21st Century</p>

    <p>          Barrett, A.</p>

    <p>          Human Vaccines  <b>2007</b>.  3(3): 81-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246212700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246212700003</a> </p><br />

    <p>8.     61170   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Exomethylene pyranonucleosides: Efficient synthesis and biological evaluation of 1-(2,3,4-trideoxy-2-methylene-[beta]-d-glycero-hex-3-enopyranosyl)thymine</p>

    <p>          Agelis, George, Tzioumaki, Niki, Botic, Tanja, Cencic, Avrelija, and Komiotis, Dimitri</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(16): 5448-5456</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NTRT26-2/2/314e6961934bfedbf8573c85836636a8">http://www.sciencedirect.com/science/article/B6TF8-4NTRT26-2/2/314e6961934bfedbf8573c85836636a8</a> </p><br />

    <p>9.     61171   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          A Mechanistic View of Enzyme Inhibition and Peptide Hydrolysis in the Active Site of the SARS-CoV 3C-like Peptidase</p>

    <p>          Yin, Jiang, Niu, Chunying, Cherney, Maia M, Zhang, Jianmin, Huitema, Carly, Eltis, Lindsay D, Vederas, John C, and James, Michael NG</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  In Press, Corrected Proof: 398</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4NXHCJC-4/2/621730f696b84b8767d0d51a8d987c65">http://www.sciencedirect.com/science/article/B6WK7-4NXHCJC-4/2/621730f696b84b8767d0d51a8d987c65</a> </p><br />

    <p>10.   61172   DMID-LS-148; EMBASE-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Selective Phosphorylation of Antiviral Drugs by Vaccinia Virus Thymidine Kinase</p>

    <p>          Harden, Emma, Keith, Kathy, Johnson, Mary, McBrayer, Alexis, Luo, Ming, Qiu, Shihong, Chattopadhyay, Debasish, Fan, Xuesen, Torrence, Paul, Kern, Earl, and Prichard, Mark</p>

    <p>          Antiviral Research <b>2007</b>.  74(3): A84-A85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-53/2/df2a45ed658662d26ae5a19288b2e1f0">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-53/2/df2a45ed658662d26ae5a19288b2e1f0</a> </p><br />

    <p>11.   61173   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Viral Load and Crimean-Congo Hemorrhagic Fever</p>

    <p>          Papa, A. <i>et al.</i></p>

    <p>          Emerging Infectious Diseases <b>2007</b>.  13(5): 805-806</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246212400038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246212400038</a> </p><br />

    <p>12.   61174   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Dengue Virus Evolution and Virulence Models</p>

    <p>          Rico-Hesse, R. </p>

    <p>          Clinical Infectious Diseases <b>2007</b>.  44(11): 1462-1466</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246198800012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246198800012</a> </p><br />
    <br clear="all">

    <p>13.   61175   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Cyclic Monophosphate Prodrugs of Base-Modified 2 &#39;-C-Methyl Ribonucleosides as Potent Inhibitors of Hepatitis C Virus Rna Replication</p>

    <p>          Gunic, E. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(9): 2452-2455</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246087900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246087900012</a> </p><br />

    <p>14.   61176   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Pandemic Influenza: Studying the Lessons of History</p>

    <p>          Morse, S.</p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2007</b>.  104(18): 7313-7314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246239400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246239400002</a> </p><br />

    <p>15.   61177   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Dramatic Rescue Relieves Rare Case of Smallpox Infection</p>

    <p>          Marris, E.</p>

    <p>          Nature Medicine <b>2007</b>.  13(5): 517</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246302800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246302800005</a> </p><br />

    <p>16.   61178   DMID-LS-148; WOS-DMID-6/29/2007</p>

    <p class="memofmt1-2">          Pharmacophores and Biological Activities of Severe Acute Respiratory Syndrome Viral Protease Inhibitors</p>

    <p>          Wang, H. and Liang, P.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(5): 533-546</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246222400006</a> </p><br />

    <p>17.   61179   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Antiviral effects of quinine sulfate on HSV-1 HaCat cells infected: Analysis of the molecular mechanisms involved</p>

    <p>          Baroni, A, Paoletti, I, Ruocco, E, Ayala, F, Corrado, F, Wolf, R, Tufano, MA, and Donnarumma, G</p>

    <p>          J Dermatol Sci  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600687&amp;dopt=abstract</a> </p><br />

    <p>18.   61180   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Sensitivity of hepatitis C virus to cyclosporine A depends on nonstructural proteins NS5A and NS5B</p>

    <p>          Fernandes, F, Poole, DS, Hoover, S, Middleton, R, Andrei, AC, Gerstner, J, and Striker, R</p>

    <p>          Hepatology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17600342&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   61181   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Antiviral effects of amantadine and iminosugar derivatives against hepatitis C virus</p>

    <p>          Steinmann, E, Whitfield, T, Kallis, S, Dwek, RA, Zitzmann, N, Pietschmann, T, and Bartenschlager, R</p>

    <p>          Hepatology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17599777&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17599777&amp;dopt=abstract</a> </p><br />

    <p>20.   61182   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Antiviral drug-resistant HBV: Standardization of nomenclature and assays and recommendations for management</p>

    <p>          Lok, AS, Zoulim, F, Locarnini, S, Bartholomeusz, A, Ghany, MG, Pawlotsky, JM, Liaw, YF, Mizokami, M, and Kuiken, C</p>

    <p>          Hepatology <b>2007</b>.  46(1): 254-265</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17596850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17596850&amp;dopt=abstract</a> </p><br />

    <p>21.   61183   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Prediction of the efficacy of antiviral therapy for hepatitis C virus infection by an ultrasensitive RT-PCR assay</p>

    <p>          Kinai, E, Hanabusa, H, and Kato, S</p>

    <p>          J Med Virol <b>2007</b>.  79(8): 1113-1119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17596840&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17596840&amp;dopt=abstract</a> </p><br />

    <p>22.   61184   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Amantadine-oseltamivir combination therapy for H5N1 influenza virus infection in mice</p>

    <p>          Ilyushina, NA, Hoffmann, E, Solomon, R, Webster, RG, and Govorkova, EA</p>

    <p>          Antivir Ther <b>2007</b>.  12(3): 363-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591026&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591026&amp;dopt=abstract</a> </p><br />

    <p>23.   61185   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          In vitro susceptibility of adefovir-associated hepatitis B virus polymerase mutations to other antiviral agents</p>

    <p>          Qi, X, Xiong, S, Yang, H, Miller, M, and Delaney, WE 4th</p>

    <p>          Antivir Ther <b>2007</b>.  12(3): 355-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591025&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591025&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>24.   61186   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          A rationale for using steroids in the treatment of severe cases of H5N1 avian influenza</p>

    <p>          Carter, MJ</p>

    <p>          J Med Microbiol <b>2007</b>.  56(Pt 7): 875-883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17577050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17577050&amp;dopt=abstract</a> </p><br />

    <p>25.   61187   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          RSV604 A NOVEL INHIBITOR OF RESPIRATORY SYNCYTIAL VIRUS REPLICATION</p>

    <p>          Chapman, J, Abbott, E, Alber, DG, Baxter, RC, Bithell, SK, Henderson, EA, Carter, MC, Chambers, P, Chubb, A, Cockerill, GS, Collins, PL, Dowdell, VC, Keegan, SJ, Kelsey, RD, Lockyer, MJ, Luongo, C, Najarro, P, Pickles, RJ, Simmonds, M, Taylor, D, Tyms, S, Wilson, LJ, and Powell, KL</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576833&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576833&amp;dopt=abstract</a> </p><br />

    <p>26.   61188   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Respiratory syncytial virus fusion inhibitors. Part 5: Optimization of benzimidazole substitution patterns towards derivatives with improved activity</p>

    <p>          Wang, XA, Cianci, CW, Yu, KL, Combrink, KD, Thuring, JW, Zhang, Y, Civiello, RL, Kadow, KF, Roach, J, Li, Z, Langley, DR, Krystal, M, and Meanwell, NA</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576060&amp;dopt=abstract</a> </p><br />

    <p>27.   61189   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Anti-hepatitis C virus activity of albinterferon alfa-2b in cell culture</p>

    <p>          Liu, C, Zhu, H, Subramanian, GM, Moore, PA, Xu, Y, and Nelson, DR</p>

    <p>          Hepatol Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17573950&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17573950&amp;dopt=abstract</a> </p><br />

    <p>28.   61190   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          CYSTUS052, a polyphenol-rich plant extract, exerts anti-influenza virus activity in mice</p>

    <p>          Droebner, K, Ehrhardt, C, Poetter, A, Ludwig, S, and Planz, O</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17573133&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17573133&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>29.   61191   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          A polyphenol rich plant extract, CYSTUS052, exerts anti influenza virus activity in cell culture without toxic side effects or the tendency to induce viral resistance</p>

    <p>          Ehrhardt, C, Hrincius, ER, Korte, V, Mazur, I, Droebner, K, Poetter, A, Dreschers, S, Schmolke, M, Planz, O, and Ludwig, S</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17572513&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17572513&amp;dopt=abstract</a> </p><br />

    <p>30.   61192   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Identification of antibacterial and antiviral activities of novel fused 1,2,4-triazine esters</p>

    <p>          Sztanke, K, Pasternak, K, Rajtar, B, Sztanke, M, Majek, M, and Polz-Dacewicz, M</p>

    <p>          Bioorg Med Chem <b>2007</b>.  15(16): 5480-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17572093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17572093&amp;dopt=abstract</a> </p><br />

    <p>31.   61193   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          A novel mannose-binding tuber lectin from Typhonium divaricatum (L.) Decne (family Araceae) with antiviral activity against HSV-II and anti-proliferative effect on human cancer cell lines</p>

    <p>          Luo, Y, Xu, X, Liu, J, Li, J, Sun, Y, Liu, Z, Liu, J, Van, Damme E, Balzarini, J, and Bao, J</p>

    <p>          J Biochem Mol Biol <b>2007</b>.  40(3): 358-67</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562287&amp;dopt=abstract</a> </p><br />

    <p>32.   61194   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Exomethylene pyranonucleosides: Efficient synthesis and biological evaluation of 1-(2,3,4-trideoxy-2-methylene-beta-d-glycero-hex-3-enopyranosyl)thymine</p>

    <p>          Agelis, G, Tzioumaki, N, Botic, T, Cencic, A, and Komiotis, D</p>

    <p>          Bioorg Med Chem <b>2007</b>.  15(16): 5448-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17555969&amp;dopt=abstract</a> </p><br />

    <p>33.   61195   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Semliki Forest virus non-structural protein 2 is involved in the suppression of the type I Interferon response</p>

    <p>          Breakwell, L, Dosenovic, P, Karlsson, Hedestam GB, D&#39;Amato, M, Liljestrom, P, Fazakerley, J, and McInerney, GM</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17553895&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17553895&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   61196   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Treatment of hepatitis B virus-infected cells with alpha-glucosidase inhibitors results in production of virions with altered molecular composition and infectivity</p>

    <p>          Lazar, C, Durantel, D, Macovei, A, Zitzmann, N, Zoulim, F, Dwek, RA, and Branza-Nichita, N</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548120&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548120&amp;dopt=abstract</a> </p><br />

    <p>35.   61197   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Inhibitory potential of Quercus lusitanica extract on dengue virus type 2 replication</p>

    <p>          Muliawan, SY, Kit, LS, Devi, S, Hashim, O, and Yusof, R</p>

    <p>          Southeast Asian J Trop Med Public Health <b>2006</b>.  37 Suppl 3: 132-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17547068&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17547068&amp;dopt=abstract</a> </p><br />

    <p>36.   61198   DMID-LS-148; PUBMED-DMID-7/2/2007</p>

    <p class="memofmt1-2">          Effect of a plant polyphenol-rich extract on the lung protease activities of influenza-virus-infected mice</p>

    <p>          Serkedjieva, J, Toshkova, R, Antonova-Nikolova, S, Stefanova, T, Teodosieva, A, and Ivanova, I </p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(2): 75-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17542152&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
