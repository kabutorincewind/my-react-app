

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-06-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ynr3n9HnQ6FWEKAPFTNQrEpXpslpGcWB0Wh6Ev2ve+MdTfpXd0DONzM3ZR2FIBI65X3fjc4HC+bv6ncro68kqZ4q/eb2cSvE7eyE6rRUNQcwnDuePGKyXxiwApk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E1118970" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 5 - June 18, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25935383">Discovery of Piperidin-4-yl-aminopyrimidine Derivatives as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Wan, Z.Y., J. Yao, Y. Tao, T.Q. Mao, X.L. Wang, Y.P. Lu, H.F. Wang, H. Yin, Y. Wu, F.E. Chen, E. De Clercq, D. Daelemans, and C. Pannecouque. European Journal of Medicinal Chemistry, 2015. 97: p. 1-9. PMID[25935383].
    <br />
    <b>[PubMed]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25306174">In Search of Uracil Derivatives as Bioactive Agents. Uracils and Fused Uracils: Synthesis, Biological Activity and Applications.</a> Palasz, A. and D. Ciez. European Journal of Medicinal Chemistry, 2015. 97: p. 582-611. PMID[25306174].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25939331">Concise and Practical Asymmetric Synthesis of a Challenging Atropisomeric HIV Integrase Inhibitor.</a> Fandrick, K.R., W. Li, Y. Zhang, W. Tang, J. Gao, S. Rodriguez, N.D. Patel, D.C. Reeves, J.P. Wu, S. Sanyal, N. Gonnella, B. Qu, N. Haddad, J.C. Lorenz, K. Sidhu, J. Wang, S. Ma, N. Grinberg, H. Lee, Y. Tsantrizos, M.A. Poupart, C.A. Busacca, N.K. Yee, B.Z. Lu, and C.H. Senanayake. Angewandte Chemie International Edition, 2015. 54(24): p. 7144-7148. PMID[25939331].
    <br />
    <b>[PubMed]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed?term=26075019">4. De-novo Design, Synthesis and Evaluation of Novel 6,7-Dimethoxy-1,2,3,4-Tetrahydroisoquinoline Derivatives as HIV-1 Reverse Transcriptase Inhibitors.</a> Chander, S., P. Ashok, A. Singh, and S. Murugesan. Chemistry Central Journal, 2015. 9: p. 33. PMID[26075019].
    <br />
    <b>[PubMed]</b>. HIV_0605-061815</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355051000016">Anti-HIV Securinega Alkaloid Oligomers from Flueggea virosa.</a> Zhang, H., Y.S. Han, M.A. Wainberg, and J.M. Yue. Tetrahedron, 2015. 71(22): p. 3671-3679. ISI [000355051000016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354657900012">SRSF1 RNA Recognition Motifs are Strong Inhibitors of HIV-1 Replication.</a> Paz, S., M.L. Lu, H. Takata, L. Trautmann, and M. Caputi. Journal of Virology, 2015. 89(12): p. 6275-6286. ISI[000354657900012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355158300017">Antiviral Activity of Flexibilane and Tigliane Diterpenoids from Stillingia lineata.</a> Olivon, F., H. Palenzuela, E. Girard-Valenciennes, J. Neyts, C. Pannecouque, F. Roussi, I. Grondin, P. Leyssen, and M. Litaudon. Journal of Natural Products, 2015. 78(5): p. 1119-1128. ISI[000355158300017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355336700008">Synthesis and in Vitro Antiviral Activities of (Dihydrofuran-2-yl)oxy methyl-phosphonate Nucleosides with 2-Substituted Adenine as Base.</a> Liu, F., Y.J. Liu, R.G. Xu, G.F. Dai, L.X. Zhao, Y.F. Wang, H.M. Liu, F.W. Liu, C. Pannecouque, and P. Herdewijn. Chemistry &amp; Biodiversity, 2015. 12(5): p. 813-822. ISI[000355336700008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355183300025">Synthesis and Structure of Potentially Biologically Active N-(Silylmethyl)tetrahydropyrimidin-2-ones.</a> Lazareva, N.F. and I.M. Lazarev. Russian Chemical Bulletin, 2015. 63(9): p. 2081-2086. ISI[000355183300025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355471100001">Antimycobacterial and HIV-1 Reverse Transcriptase Activity of Julianaceae and Clusiaceae Plant Species from Mexico.</a> Gomez-Cansino, R., C.I. Espitia-Pinzon, M.G. Campos-Lara, S.L. Guzman-Gutierrez, E. Segura-Salinas, G. Echeverria-Valencia, L. Torras-Claveria, X.M. Cuevas-Figueroa, and R. Reyes-Chilpa. Evidence-Based Complementary and Alternative Medicine, 2015. 183036: 8pp. ISI[000355471100001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000354907400012">Swapped-domain Constructs of the Glycoprotein-41 Ectodomain are Potent Inhibitors of HIV Infection.</a> Chu, S.D., H. Kaur, A. Nemati, J.D. Walsh, V. Partida, S.Q. Zhang, and M. Gochin. ACS Chemical Biology, 2015. 10(5): p. 1247-1257. ISI[000354907400012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355158300009">Isolation, Structural Modification, and HIV Inhibition of Pentacyclic Lupane-type Triterpenoids from Cassine xlocarpa and Maytenus cuzcoina.</a> Callies, O., L.M. Bedoya, M. Beltran, A. Munoz, P.O. Calderon, A.A. Osorio, I.A. Jimenez, J. Alcami, and I.L. Bazzocchi. Journal of Natural Products, 2015. 78(5): p. 1045-1055. ISI[000355158300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0605-061815.</p>

    <br />

    <br />

    <br />

    <br />

    <br />

    <br />

    <p class="plaintext">                                      </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
