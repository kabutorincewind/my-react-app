

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-363.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9qOXeWXoqgMolm8UVQJLoscmb9VLUBNPq5t5k4VcWnCBnM4XE/jqsju/03jT2qTDfywMuRchlxjAQkzhZgXWGRnPcaZenYKMYpL0uxXyu+illR8xXzcxDJ2WeRs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FFAE6741" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">&#39;ONLINE DATABASE SEARCH - HIV-LS-363-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59583   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis and properties of G-quartet oligonucleotide-HIV-1 tat peptide conjugate</p>

    <p>          Kumashiro, T, Otake, T, Kawahata, T, Akagi, M, and Urata, H</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2006</b>.(50): 177-178</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150875&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150875&amp;dopt=abstract</a> </p><br />

    <p>2.     59584   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          2&#39;-Deoxy-4&#39;-C-ethynyl-2-fluoroadenosine: A nucleoside reverse transcriptase inhibitor with highly potent activity against all HIV-1 strains, favorable toxic profiles and stability in plasma</p>

    <p>          Ohrui, H, Kohgo, S, Hayakawa, H, Kodama, E, Matsuoka, M, Nakata, T, and Mitsuya, H</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2006</b>.(50): 1-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150787&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150787&amp;dopt=abstract</a> </p><br />

    <p>3.     59585   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Amino disaccharides having an alpha-(1-&gt;4) or a beta-(1-&gt;4) linkage, their synthesis and evaluation as a potential inhibitor for HIV-1 TAR-Tat</p>

    <p>          Iguchi, T, Ishikawa, H, Matumoto, H, Mizuno, M, Goto, K, and Hamasaki, K</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2005</b>.(49): 169-170</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150687&amp;dopt=abstract</a> </p><br />

    <p>4.     59586   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 1-substituted 3-(3,5-dimethylbenzyl)uracil against HIV-1</p>

    <p>          Maruyama, T, Kozai, S, Demizu, Y, Witvrouw, M, Pannecouque, C, Balzarini, J, Snoeck, R, Andrei, G, and De, Clercq E</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2004</b>.(48): 3-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150449&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150449&amp;dopt=abstract</a> </p><br />

    <p>5.     59587   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Discovery of HIV-1 Protease Inhibitors with Picomolar Affinities Incorporating N-Aryl-oxazolidinone-5-carboxamides as Novel P2 Ligands</p>

    <p>          Ali, A, Reddy, GS, Cao, H, Anjum, SG, Nalam, MN, Schiffer, CA, and Rana, TM</p>

    <p>          J Med Chem <b>2006</b>.  49(25): 7342-7356</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149864&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17149864&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59588   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV evaluation of the novel 2-(m-chlorobenzyl)-4-substituted-7-methyl-1, 1, 3-trioxo-pyrazolo[4, 5-e] [1, 2, 4]thiadiazines</p>

    <p>          Yan, RZ, Liu, XY, Xu, WF, Pannecouque, C, Witvrouw, M, and De, Clercq E</p>

    <p>          Arch Pharm Res  <b>2006</b>.  29(11): 957-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17146963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17146963&amp;dopt=abstract</a> </p><br />

    <p>7.     59589   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Anti-HIV Activity of Steric Block Oligonucleotides</p>

    <p>          Ivanova, G, Arzumanov, AA, Turner, JJ, Reigadas, S, Toulme, JJ, Brown, DE, Lever, AM, and Gait, MJ</p>

    <p>          Ann N Y Acad Sci <b>2006</b>.  1082: 103-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145931&amp;dopt=abstract</a> </p><br />

    <p>8.     59590   HIV-LS-363; SCIFINDER-HIV-12/4/2006</p>

    <p class="memofmt1-2">          Design, synthesis and antiviral activity of cyclobutyl phosphonate nucleoside analogs as chain terminators against HIV</p>

    <p>          Li, Yongfeng, Schinazi, Raymond F, and Liotta, Dennis C</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-244</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     59591   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Antiretroviral Activity, Pharmacokinetics, and Tolerability of MK-0518, a Novel Inhibitor of HIV-1 Integrase, Dosed As Monotherapy for 10 Days in Treatment-Naive HIV-1-Infected Individuals</p>

    <p>          Markowitz, M, Morales-Ramirez, JO, Nguyen, BY, Kovacs, CM, Steigbigel, RT, Cooper, DA, Liporace, R, Schwartz, R, Isaacs, R, Gilde, LR, Wenning, L, Zhao, J, and Teppler, H</p>

    <p>          J Acquir Immune Defic Syndr <b>2006</b>.  43(5): 509-515</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17133211&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17133211&amp;dopt=abstract</a> </p><br />

    <p>10.   59592   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by siRNA targeting conserved regions of gag/pol</p>

    <p>          Morris, KV, Chung, CH, Witke, W, and Looney, DJ</p>

    <p>          RNA Biol <b>2005</b>.  2(1): 17-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132935&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59593   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          ANTIVIRAL ACTIVITY OF NOVEL 5-PHOSPHONO-PENT-2-EN-1-YL NUCLEOSIDES AND THEIR ALKOXYALKYL PHOSPHONOESTERS</p>

    <p>          Choo, H, Beadle, JR, Kern, ER, Prichard, MN, Keith, KA, Hartline, CB, Trahan, J, Aldern, KA, Korba, BE, and Hostetler, KY</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17130297&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17130297&amp;dopt=abstract</a> </p><br />

    <p>12.   59594   HIV-LS-363; SCIFINDER-HIV-12/4/2006</p>

    <p class="memofmt1-2">          Hiv integrase inhibitors</p>

    <p>          Vacca, Joseph P, Wai, John S, Payne, Linda S, Isaacs, Richard CA, Han, Wei, Egbertson, Melissa, and Pracitto, Richard</p>

    <p>          PATENT:  WO <b>2006121831</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 122pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   59595   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Interactions between Buprenorphine and Antiretrovirals. II. The Protease Inhibitors Nelfinavir, Lopinavir/Ritonavir, and Ritonavir</p>

    <p>          McCance-Katz, EF, Moody, DE, Smith, PF, Morse, GD, Friedland, G, Pade, P, Baker, J, Alvanzo, A, Jatlow, P, and Rainey, PM</p>

    <p>          Clin Infect Dis <b>2006</b>.  43 Suppl 4: S235-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109310&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109310&amp;dopt=abstract</a> </p><br />

    <p>14.   59596   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Interactions between Buprenorphine and Antiretrovirals. I. The Nonnucleoside Reverse-Transcriptase Inhibitors Efavirenz and Delavirdine</p>

    <p>          McCance-Katz, EF, Moody, DE, Morse, GD, Friedland, G, Pade, P, Baker, J, Alvanzo, A, Smith, P, Ogundele, A, Jatlow, P, and Rainey, PM</p>

    <p>          Clin Infect Dis <b>2006</b>.  43 Suppl 4: S224-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109309&amp;dopt=abstract</a> </p><br />

    <p>15.   59597   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          From dihydroxypyrimidine carboxylic acids to carboxamide HIV-1 integrase inhibitors: SAR around the amide moiety</p>

    <p>          Petrocchi, A, Koch, U, Matassa, VG, Pacini, B, Stillmock, KA, and Summa, V</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107799&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107799&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59598   HIV-LS-363; SCIFINDER-HIV-12/4/2006</p>

    <p class="memofmt1-2">          Polynucleotide and polypeptide sequences of HIV-1 Vif variants with antiviral activity and their therapeutic use</p>

    <p>          Bovolenta, Chiara</p>

    <p>          PATENT:  WO <b>2006111866</b>  ISSUE DATE:  20061026</p>

    <p>          APPLICATION: 2006  PP: 84pp.</p>

    <p>          ASSIGNEE:  (Molmed SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59599   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis and in Vitro Biological Evaluation of Mannose-Containing Prodrugs Derived from Clinically Used HIV-Protease Inhibitors with Improved Transepithelial Transport</p>

    <p>          Roche, D, Greiner, J, Aubertin, AM, and Vierling, P</p>

    <p>          Bioconjug Chem  <b>2006</b>.  17(6): 1568-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105238&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105238&amp;dopt=abstract</a> </p><br />

    <p>18.   59600   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Potent nonnucleoside reverse transcriptase inhibitors target HIV-1 Gag-Pol</p>

    <p>          Figueiredo, A, Moore, KL, Mak, J, Sluis-Cremer, N, de, Bethune MP, and Tachedjian, G</p>

    <p>          PLoS Pathog <b>2006</b>.  2(11): e119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17096588&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17096588&amp;dopt=abstract</a> </p><br />

    <p>19.   59601   HIV-LS-363; PUBMED-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Zinc chelation inhibits HIV Vif activity and liberates antiviral function of the cytidine deaminase APOBEC3G</p>

    <p>          Xiao, Z, Ehrlich, E, Luo, K, Xiong, Y, and Yu, XF</p>

    <p>          FASEB J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17135358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17135358&amp;dopt=abstract</a> </p><br />

    <p>20.   59602   HIV-LS-363; SCIFINDER-HIV-12/4/2006</p>

    <p class="memofmt1-2">          Piperidinylpiperidine derivatives useful as inhibitors of chemokine receptors and their preparation and pharmaceutical compositions</p>

    <p>          Chan, Tze-Ming, Cox, Kathleen, Feng, Wenqing, Miller, Michael W, Weston, Daniel, and Mccombie, Stuart</p>

    <p>          PATENT:  WO <b>2006091534</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 92pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59603   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          Synthesis of N,S-substituted 4-chloro-2-mercapto-5-methylbenzenesulfonamide derivatives as potential anti-HIV agents</p>

    <p>          Brzozowski, Z and Slawinski, J</p>

    <p>          POLISH JOURNAL OF CHEMISTRY <b>2006</b>.  80(11): 1807-1815, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242117200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242117200005</a> </p><br />

    <p>22.   59604   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of nevirapine prodrugs</p>

    <p>          Sriram, D, Yogeeswari, P, and Kshore, MRK</p>

    <p>          PHARMAZIE <b>2006</b>.  61(11): 895-897, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002000001</a> </p><br />

    <p>23.   59605   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          Specific binding of a hexanucleotide to HIV-1 reverse transcriptase: a novel class of bioactive molecules</p>

    <p>          Mescalchin, A, Wunsche, W, Laufer, SD, Grohmann, D, Restle, T, and Sczakiel, G</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2006</b>.  34(19): 5631-5637, 7</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241955200038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241955200038</a> </p><br />

    <p>24.   59606   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          4,5-dihydroxypyrimidine carboxamides and N-alkyl-5-hydroxypyrimidinone carboxamides are potent, selective HIV integrase inhibitors with good pharmacokinetic profiles in preclinical species</p>

    <p>          Summa, V, Petrocchi, A, Matassa, VG, Gardelli, C, Muraglia, E, Rowley, M, Paz, OG, Laufer, R, Monteagudo, E, and Pace, P</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(23): 6646-6649, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241894000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241894000005</a> </p><br />

    <p>25.   59607   HIV-LS-363; SCIFINDER-HIV-12/4/2006</p>

    <p class="memofmt1-2">          Substituted carbamates as HIV protease inhibitors and their preparation, pharmaceutical compositions and use in the treatment of HIV infection, AIDS and AIDS-related conditions</p>

    <p>          Mclean, Ed W and Miller, John Franklin</p>

    <p>          PATENT:  WO <b>2006104646</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2006  PP: 73pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   59608   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          Part 13: Synthesis and biological evaluation of piperazine derivatives with dual anti-PAF and anti-HIV-1 or pure antiretroviral activity</p>

    <p>          Serradji, N, Bensaid, O, Martin, M, Sallem, W, Dereuddre-Bosquet, N, Benmehdi, H, Redeuilh, C, Lamouri, A, Dive, G, Clayette, P, and Heymans, F</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2006</b>.  14(23): 8109-8125, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242063500045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242063500045</a> </p><br />

    <p>27.   59609   HIV-LS-363; WOS-HIV-12/04/2006</p>

    <p class="memofmt1-2">          Resistance profile of a neutralizing anti-HIV monoclonal antibody, KD-247, that shows favourable synergism with anti-CCR5 inhibitors</p>

    <p>          Yoshimura, K, Shibata, J, Kimura, T, Honda, A, Maeda, Y, Koito, A, Murakami, T, Mitsuya, H, and Matsushita, S</p>

    <p>          AIDS <b>2006</b>.  20 (16): 2065-2073, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242000100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242000100008</a> </p><br />
    <br clear="all">

    <p>28.   59610   HIV-LS-363; WOS-HIV-12/11/2006</p>

    <p class="memofmt1-2">          HIV infection of mononuclear cells is calcium-dependent</p>

    <p>          Anzinger, JJ, Mezo, I, Ji, X, Gabali, AM, Thomas, LL, and Spear, GT</p>

    <p>          VIRUS RESEARCH  <b>2006</b>.  122(1-2): 183-188, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242227300022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242227300022</a> </p><br />

    <p>29.   59611   HIV-LS-363; WOS-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Tristetraprolin inhibits HIV-1 production by binding to genomic RNA</p>

    <p>          Maeda, M, Sawa, H, Tobiume, M, Tokunaga, K, Hasegawa, H, Ichinohe, T, Sata, T, Moriyama, M, Hall, WW, Kurata, T, and Takahashi, H</p>

    <p>          MICROBES AND INFECTION <b>2006</b>.  8(11): 2647-2656, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242207100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242207100010</a> </p><br />

    <p>30.   59612   HIV-LS-363; WOS-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Inhibition of tRNA(3)(Lys)-primed reverse transcription by human APOBEC3G during human immunodeficiency virus type 1 replicationv</p>

    <p>          Guo, F, Cen, S, Niu, MJ, Saadatmand, J, and Kleiman, L</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(23): 11710-11722, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242222200028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242222200028</a> </p><br />

    <p>31.   59613   HIV-LS-363; WOS-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Helicobacter pylori VacA toxin inhibits human immunodeficiency virus infection of primary human T cells</p>

    <p>          Oswald-Richter, K, Torres, VJ, Sundrud, MS, VanCompernolle, SE, Cover, TL, and Unutmaz, D</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(23): 11767-11775, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242222200033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242222200033</a> </p><br />

    <p>32.   59614   HIV-LS-363; WOS-HIV-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis of new substituted 4,5-dihydro-3H-spiro[1,5]-benzoxazepine-2,4 &#39;-piperidine and biological properties</p>

    <p>          Laras, Y, Pietrancosta, N, Moret, V, Marc, S, Garino, C, Rolland, A, Monnier, V, and Kraus, JL </p>

    <p>          AUSTRALIAN JOURNAL OF CHEMISTRY <b>2006</b>.  59(11): 812-818, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242115500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242115500006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
