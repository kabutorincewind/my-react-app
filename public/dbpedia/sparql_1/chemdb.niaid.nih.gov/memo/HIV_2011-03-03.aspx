

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CklMYNymaHCi0UhfI5QEkDISJQxw9xtiluDMrZsly4GsXg0OEV7TZEW7Az5Yb0EbrZBczpZRU8hi4SQ972CmtFyc/0c3d+0zGJ2FkJjhKEB1Cf3qR8VGE31d6gg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="00BF9276" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 18, 2011- March 3, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21159874">Inhibition of Early Stages of HIV-1 Assembly by INI1/hSNF5 Transdominant Negative Mutant S6.</a> Cano, J. and G.V. Kalpana. Journal of Virology, 2011. 85(5): p. 2254-2265; PMID[21159874].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21305065">Discovery of novel anti-HIV active G-quadruplex-forming oligonucleotides.</a> Di Fabio, G., J. D&#39;Onofrio, M. Chiapparelli, B. Hoorelbeke, D. Montesarchio, J. Balzarini, and L. De Napoli. Chemical Communications (Cambridge), 2011. 47(8): p. 2363-2365; PMID[21305065].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21146540">Structure of the HIV-1 Full-Length Capsid Protein in a Conformationally Trapped Unassembled State Induced by Small-Molecule Binding.</a> Du, S., L. Betts, R. Yang, H. Shi, J. Concel, J. Ahn, C. Aiken, P. Zhang, and J.I. Yeh. Journal of Molecular Biology, 2011. 406(3): p. 371-386; PMID[21146540]</p>

    <p class="plaintext"> <b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21348941">Alizarine derivatives as new dual inhibitors of the HIV-1 reverse transcriptase(RT)-associated DNA polymerase and Ribonuclease H (RNase H) activities effective also on the RNase H activity of non-nucleoside resistant RTs.</a> Esposito, F., T. Kharlamova, S. Distinto, L. Zinzula, Y.C. Cheng, G. Dutschman, G. Floris, P. Markt, A. Corona, and E. Tramontano. FEBS Journal, 2011.  <b>[Epub ahead of print]</b>; PMID[21348941].</p>

    <p class="plaintext"> <b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21306897">Synthesis of chroman aldehydes that inhibit HIV.</a> Kraus, G.A., J. Mengwasser, W. Maury, and C. Oh. Bioorganic and Medicinal Chemistry Letters, 2011. 21(5): p. 1399-1401; PMID[21306897].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21357691">Vipirinin, a coumarin-based HIV-1 VPR inhibitor, interacts with a hydrophobic region of VPR.</a> Ong, E.B., N. Watanabe, A. Saito, Y. Futamura, K.H. Abd El Galil, A. Koito, N. Najimudin, and H. Osada. Journal of Biological Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21357691].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21345957">HIV-1 escape from the CCR5 antagonist maraviroc associated with an altered and less efficient mechanism of gp120-CCR5 engagement that attenuates macrophage-tropism.</a> Roche, M., M.R. Jakobsen, J. Sterjovski, A. Ellett, F. Posta, B. Lee, B. Jubb, M. Westby, S.R. Lewin, P.A. Ramsland, M.J. Churchill, and P.R. Gorry. Journal of Virology, 2011.  <b>[Epub ahead of print]</b>; PMID[21345957].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21352856">A mammalian two-hybrid system-based assay for small-molecular HIV fusion inhibitors targeting gp41.</a> Shui, X., X. Lu, Y. Gao, C. Liu, F. Ren, Q. Jiang, H. Zhang, B. Zhao, and Z. Zheng. Antiviral Research, 2011.  <b>[Epub ahead of print]</b>; PMID[21352856].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21342055">Small molecule HIV entry inhibitors: Part II. Attachment and fusion inhibitors: 2004 - 2010.</a> Singh, I.P. and S.K. Chauthe. Expert Opinion on Therapeutic Patents, 2011. 21(3): p. 399-416; PMID[21342055].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21295470">Design of novel CXCR4 antagonists that are potent inhibitors of T-tropic (X4) HIV-1 replication.</a> Skerlj, R., G. Bridger, E. McEachern, C. Harwig, C. Smith, A. Kaller, D. Veale, H. Yee, K. Skupinska, R. Wauthy, L. Wang, I. Baird, Y. Zhu, K. Burrage, W. Yang, M. Sartori, D. Huskens, E.D. Clercq, and D. Schols. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(5): p. 1414-1418; PMID[21295470].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21292480">Synthesis and evaluation of 2-phenyl-1,4-butanediamine-based CCR5 antagonists for the treatment of HIV-1.</a> Tallant, M.D., M. Duan, G.A. Freeman, R.G. Ferris, M.P. Edelstein, W.M. Kazmierski, and P.J. Wheelan. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(5): p. 1394-1398; PMID[21292480].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21306567">Synthesis and Antiviral Evaluation of New N-acylhydrazones Containing Glycine Residue.</a> Tian, B., M. He, Z. Tan, S. Tang, I. Hewlett, S. Chen, Y. Jin, and M. Yang. Chem Biol Drug Des, 2011. 77(3): p. 189-198; PMID[21306567].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21272964">Virtual screening studies on HIV-1 reverse transcriptase inhibitors to design potent leads.</a> Vadivelan, S., T.N. Deeksha, S. Arun, P.K. Machiraju, R. Gundla, B.N. Sinha, and S.A. Jagarlapudi. European Journal of Medicinal Chemistry, 2011. 46(3): p. 851-859; PMID[21272964].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21343443">4&#39;C-methyl-2&#39;-deoxyadenosine and 4&#39;C-ethyl-2&#39;-deoxyadeonosine Inhibit HIV-1 Replication.</a> Vu, B.C., P.L. Boyer, M.A. Siddiqui, V.E. Marquez, and S.H. Hughes. Antimicrobial Agents &amp; Chemotherapy, 2011.  <b>[Epub ahead of print]</b>; PMID[21343443].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0218-030311.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285525900020">Synthesis and Bioactivity of N-glycosyl-N &#39;-(4-aryithiazol-2-yl) Aminoguanidines.</a> Chen, H.M., Y.W. Zhao, and L.H. Cao. Journal of the Chinese Chemical Society, 2010. 57(5A): p. 1085-1090; ISI[000285525900020],</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285690000011">In vitro antiviral activity of diterpenes isolated from the Brazilian brown alga Canistrocarpus cervicornis.</a> Vallim, M.A., J.E. Barbosa, D.N. Cavalcanti, J.C. De-Paula, V. da Silva, V.L. Teixeira, and I. Paixao. Journal of Medicinal Plants Research, 2010. 4(22): p. 2379-2382; ISI[000285690000011],</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286221500007">Recent Advances and Future Perspectives of Triazole Analogs as Promising Antiviral Agents.</a> Kharb, R., M.S. Yar, and P.C. Sharma. Mini-Reviews in Medicinal Chemistry, 2011. 11(1): p. 84-96; ISI[000286221500007].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286306400018">Design and Synthesis of Potent HIV-1 Protease Inhibitors Incorporating Hexahydrofuropyranol-Derived High Affinity P-2 Ligands: Structure-Activity Studies and Biological Evaluation.</a> Ghosh, A.K., B.D. Chapsal, A. Baldridge, M.P. Steffey, D.E. Walters, Y. Koh, M. Amano, and H. Mitsuya. Journal of Medicinal Chemistry, 2011. 54(2): p. 622-634; ISI[000286306400018].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">19.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286444200004">Cross-Target View to Feature Selection: Identification of Molecular Interaction Features in Ligand-Target Space.</a>  Niijima, S., H. Yabuuchi, and Y. Okuno. Journal of Chemical Information and Modeling, 2011. 51(1): p. 15-24; ISI[000286444200004].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286485500013">Synthesis, in vitro Antiproliferative and Anti-HIV Activity of New Derivatives of 2-Piperazino-1,3-benzo d thiazoles.</a> Al-Soud, Y.A., H.H. Al-Sa&#39;doni, S.O.W. Saber, R.H.M. Al-Shaneek, N.A. Al-Masoudi, R. Loddo, and P. La Colla. Zeitschrift Fur Naturforschung Section B-a Journal of Chemical Sciences, 2010. 65(11): p. 1372-1380; ISI[000286485500013].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286489300009">Synthesis and Anti-HIV Activity of New 2-Thiolumazine and 2-Thiouracil Metal Complexes.</a> Al-Masoudi, N.A., B.A. Saleh, N.A. Karim, A.Y. Issa, and C. Pannecouque. Heteroatom Chemistry, 2011. 22(1): p. 44-50; ISI[000286489300009].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286498200013">Synthesis and antiviral bioactivities of novel chiral bis-thiourea-type derivatives containing alpha-aminophosphonate moiety.</a> Yang, X.A., B.A. Song, L.H. Jin, X. Wei, S.P. Bhadury, X.Y. Li, S. Yang, and D.Y. Hu. Science China-Chemistry, 2011. 54(1): p. 103-109; ISI[000286498200013].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286499000021">Isolation of optically active nevirapine, a dipyridodiazepinone metabolite from the seeds of Cleome viscosa.</a> Chattopadhyay, S.K., A. Chatterjee, S. Tandon, P.R. Maulik, and R. Kant. Tetrahedron, 2011. 67(2): p. 452-454; ISI[000286499000021].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286506800025">A Concise and Flexible Diastereoselective Approach to Heteroring-Fused Isoindolinones.</a> Ahn, G., M. Lorion, V. Agouridas, A. Couture, E. Deniau, and P. Grandclaudon. Synthesis-Stuttgart, 2011(1): p. 147-153; ISI[000286506800025].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286508400012">The Furan Approach to Oxacycles: New Entry to the Synthesis of Isodideoxynucleosides.</a> Canoa, P., Z. Gandara, M. Perez, R. Gago, G. Gomez, and Y. Fall. Synthesis-Stuttgart, 2011(3): p. 431-436; ISI[000286508400012].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286514000001">Novel Peptides Based on HIV-1 gp120 Sequence with Homology to Chemokines Inhibit HIV Infection in Cell Culture.</a> Chertov, O., N. Zhang, X. Chen, J.J. Oppenheim, J. Lubkowski, C. McGrath, R.C. Sowder, B.J. Crise, A. Malyguine, M.A. Kutzler, A.D. Steele, E.E. Henderson, and T.J. Rogers. PLoS ONE, 2011. 6(1); ISI[000286514000001].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0218-030311.</p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-2">Patent Citations:</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20101202&amp;CC=WO&amp;NR=2010138338A1&amp;KC=A1">Preparation of phenylsulfonylpyrrolidinylaminoethyl diphenylalaninamides as HIV protease inhibitors.</a> Coburn, C.A., M.K. Holloway, and J.P. Vacca. 2010. 2010-US35200 2010138338.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20101222&amp;CC=CN&amp;NR=101921224A&amp;KC=A">1-(3-aminopropyl)piperazine-4-amino amide-like compounds, their preparation and application as anti-AIDs agents.</a> Dai, Q., M. Dong, S. Jiang, and H. Lu. 2010. 2010-10182303</p>

    <p class="plaintext">101921224.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20101221&amp;CC=US&amp;NR=7854946B1&amp;KC=B1">Anti-inflammatory and anti-HIV compositions and methods of use.</a> Hillwig, M.L. 2010. 2008-129391 7854946.</p>

    <p class="plaintext">[<b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20101216&amp;CC=WO&amp;NR=2010143169A2&amp;KC=A2">Preparation of pyridinyl-substituted quinolinylamine derivatives useful for treating AIDS.</a> Tazi, J., F. Mahuteau, and R. Najman. 2010. 2010-IB52651 2010143169.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110106&amp;CC=WO&amp;NR=2011001208A1&amp;KC=A1">The use of an extract of helichrysum, for the treatment of HIV/AIDS.</a> Harding, N.M., N. Moodley, P. Meoni, V.J. Maharaj, S.-W. Van Rooyen, M. Van der Merwe, and J.J.M. Meyer. 2011. 2009-IB52807 2011001208.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110127&amp;CC=WO&amp;NR=2011011652A1&amp;KC=A1">(Heterocyclyl)azabicyclooctane derivatives as CCR5 antagonists and their preparation and use for the treatment of CCR5-mediated diseases.</a> Duan, M., W.M. Kazmierski, and M. Tallant. 2011. 2010-US42992</p>

    <p class="plaintext">2011011652.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110203&amp;CC=WO&amp;NR=2011014561A1&amp;KC=A1">Extracts of medicinal plant and uses thereof.</a> He, J.J., I.-W. Park, G. Chen, C. Han, and X. Song. 2011. 2010-US43542</p>

    <p class="plaintext">2011014561.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110119&amp;CC=CN&amp;NR=101948418A&amp;KC=A">Hydroxyproline derivatives as HIV-1 protease inhibitors and their preparation, pharmaceutical compositions and use in the treatment of HIV infection.</a> Liu, Z. and B. Gao. 2011. 2010-10246801</p>

    <p class="plaintext">101948418.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110203&amp;CC=WO&amp;NR=2011014239A1&amp;KC=A1">Anti-proliferative substituted pyrazolo[3,4-d]pyrimidines derivatives (SPP) to inhibit immune activation, virus replication and tumor growth.</a> Lori, F., D. De Forni, and M.R. Stevens. 2011. 2010-US2091</p>

    <p class="plaintext">2011014239.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110120&amp;CC=WO&amp;NR=2011007230A2&amp;KC=A2">Preparation of lupeol-type triterpene derivatives as antiviral agents.</a> Reddy, B.P., V.M. Sharma, K.R. Reddy, M.M. Reddy, N.Y. Reddy, and L.V.L. Subrahmanyam. 2011. 2010-IB1677</p>

    <p class="plaintext">2011007230.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://v3.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110203&amp;CC=WO&amp;NR=2011012630A1&amp;KC=A1">Substituted bis(aryl)pyrazolecarboxamides having terminal primary amide functionalization for treating retroviral diseases and their preparation.</a> Wildum, S., B. Klenke, H. Schirmer, J. Stiehl, and K.M. Gericke. 2011. 2010-EP60914</p>

    <p class="plaintext">2011012630.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0218-030311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
