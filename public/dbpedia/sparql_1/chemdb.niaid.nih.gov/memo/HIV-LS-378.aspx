

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-378.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="erHTOJqYytq/sCsSilWEdbcd43AGJVATdyIvMkw4UPJf/xxq6KhIuXGvzsGCjid1OeCUlqiYjLxqksb+TzHSWGQWTVC1InAM0qCYhLXlOFz/MLYJyc+wsRqa4DQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2A517574" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-378-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61199   HIV-LS-378; EMBASE-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 transcription by N-aminoimidazole derivatives</p>

    <p>          Stevens, Miguel, Balzarini, Jan, Lagoja, Irene M, Noppen, Bernard, Francois, Katrien, Van Aerschot, Arthur, Herdewijn, Piet, De Clercq, Erik, and Pannecouque, Christophe</p>

    <p>          Virology <b>2007</b>.  365(1): 220-237</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NJX42B-4/2/d099467a1f0a71ac4ca0dd61da705268">http://www.sciencedirect.com/science/article/B6WXR-4NJX42B-4/2/d099467a1f0a71ac4ca0dd61da705268</a> </p><br />

    <p>2.     61200   HIV-LS-378; EMBASE-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Anti-AIDS agents 72. Bioisosteres (7-carbon-DCKs) of the potent anti-HIV lead DCK</p>

    <p>          Wang, Yang, Huang, Shao-Xu, Xia, Peng, Xia, Yi, Yang, Zheng-Yu, Kilgore, Nicole, Morris-Natschke, Susan L, and Lee, Kuo-Hsiung</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(15): 4316-4319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NRK4G0-7/2/150e22b12692b1a3c485bcf00cdb9a96">http://www.sciencedirect.com/science/article/B6TF9-4NRK4G0-7/2/150e22b12692b1a3c485bcf00cdb9a96</a> </p><br />

    <p>3.     61201   HIV-LS-378; WOS-HIV-6/29/2007</p>

    <p class="memofmt1-2">          Structural Descriptors of Gp120 V3 Loop for the Prediction of Hiv-1 Coreceptor Usage</p>

    <p>          Sander, O. <i>et al.</i></p>

    <p>          Plos Computational Biology <b>2007</b>.  3(3): 555-564</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246191000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246191000021</a> </p><br />

    <p>4.     61202   HIV-LS-378; WOS-HIV-6/29/2007</p>

    <p class="memofmt1-2">          New Emivirine (Mkc-442) Analogues Containing a Tetrahydronaphthalene at C-6 and Their Anti-Hiv Activity</p>

    <p>          Therkelsen, F. <i>et al.</i></p>

    <p>          Monatshefte Fur Chemie <b>2007</b>.  138(5): 495-503</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246276000013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246276000013</a> </p><br />

    <p>5.     61203   HIV-LS-378; EMBASE-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Structural Characterization of B and non-B Subtypes of HIV-Protease: Insights into the Natural Susceptibility to Drug Resistance Development</p>

    <p>          Sanches, Mario, Krauchenco, Sandra, Martins, Nadia H, Gustchina, Alla, Wlodawer, Alexander, and Polikarpov, Igor</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  369(4): 1029-1040</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4NBBYYK-1/2/b39fd499810efdfbc6fdb9d6a277d3cc">http://www.sciencedirect.com/science/article/B6WK7-4NBBYYK-1/2/b39fd499810efdfbc6fdb9d6a277d3cc</a> </p><br />

    <p>6.     61204   HIV-LS-378; WOS-HIV-6/29/2007</p>

    <p class="memofmt1-2">          Overcoming Hiv-1 Resistance to Rna Interference</p>

    <p>          Boden, D., Pusch, O., and Ramratnam, B.</p>

    <p>          Frontiers in Bioscience <b>2007</b>.  12: 3104-3116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246157900028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246157900028</a> </p><br />
    <br clear="all">

    <p>7.     61205   HIV-LS-378; WOS-HIV-7/6/2007</p>

    <p class="memofmt1-2">          Novel Nuclear Import of Vpr Promoted by Importin Alpha Is Crucial for Human Immunodeficiency Virus Type 1 Replication in Macrophages</p>

    <p>          Nitahara-Kasahara, Y. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(10): 5284-5293</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246464200036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246464200036</a> </p><br />

    <p>8.     61206   HIV-LS-378; WOS-HIV-7/6/2007</p>

    <p class="memofmt1-2">          Sphingomyelinase Restricts the Lateral Diffusion of Cd4 and Inhibits Human Immunodeficiency Virus Fusion</p>

    <p>          Finnegan, C. <i>et al.</i></p>

    <p>          Journal of Virology <b>2007</b>.  81(10): 5294-5304</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246464200037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246464200037</a> </p><br />

    <p>9.     61207   HIV-LS-378; WOS-HIV-7/6/2007</p>

    <p class="memofmt1-2">          Nitroimidazoies, Part 4: Synthesis and Anti-Hiv Activity of New 5-Alkylsulfanyl and 5-(4 &#39;-Arylsulfonyl)Piperazinyl-4-Nitroimidazole Derivatives</p>

    <p>          Al-Soud, Y. <i>et al.</i></p>

    <p>          Heteroatom Chemistry <b>2007</b>.  18(4): 333-340</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246375600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246375600001</a> </p><br />

    <p>10.   61208   HIV-LS-378; WOS-HIV-7/6/2007</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hiv Activity of Substituted 1 2,4-Triazolo-Thiophene Derivatives</p>

    <p>          Al-Soud, Y.</p>

    <p>          Heteroatom Chemistry <b>2007</b>.  18(4): 443-448</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246375600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246375600019</a> </p><br />

    <p>11.   61209   HIV-LS-378; WOS-HIV-7/6/2007</p>

    <p class="memofmt1-2">          Discovery of Highly Potent and Selective Cxcr4 Inhibitors Using Protein Epitope Mimetics (Pem) Technology</p>

    <p>          Lederer, A. <i>et al.</i></p>

    <p>          Chimia <b>2007</b>.  61 (4): 147-150</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246401500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246401500007</a> </p><br />

    <p>12.   61210   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Replication competent variants of human immunodeficiency virus type-2 lacking the V3 loop exhibit resistance to chemokine receptor antagonists</p>

    <p>          Lin, G, Bertolotti-Ciarlet, A, Haggarty, B, Romano, J, McGeehan, K, Leslie, G, d&#39;Oliviere, AP, Huang, CC, Kwong, PD, Doms, RW, and Hoxie, JA</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17609282&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17609282&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   61211   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          N&#39;-[2-(2-thiophene) ethyl]-N&#39;-[2-(5-bromopyridyl)]thiourea (HI-443), a rationally designed non-nucleoside reverse transcriptase inhibitor compound with potent anti-HIV activity</p>

    <p>          Uckun, FM, Qazi, S, and Venkatachalam, T</p>

    <p>          Arzneimittelforschung <b>2007</b>.  57(5): 278-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17598700&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17598700&amp;dopt=abstract</a> </p><br />

    <p>14.   61212   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Computational screening of inhibitors for HIV-1 integrase using a receptor based pharmacophore model</p>

    <p>          Jaganatharaja, J and Gowthaman, R</p>

    <p>          Bioinformation  <b>2006</b>.  1(4): 112-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597868&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597868&amp;dopt=abstract</a> </p><br />

    <p>15.   61213   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          The Nucleoside Analogs 4&#39;C-Methyl Thymidine and 4&#39;C-Ethyl Thymidine Block DNA Synthesis by Wild-type HIV-1 RT and Excision Proficient NRTI Resistant RT Variants</p>

    <p>          Boyer, PL, Julias, JG, Ambrose, Z, Siddiqui, MA, Marquez, VE, and Hughes, SH</p>

    <p>          J Mol Biol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597154&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17597154&amp;dopt=abstract</a> </p><br />

    <p>16.   61214   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Metabolism and Disposition in Humans of Raltegravir (MK-0518), an Anti-AIDS Drug Targeting the HIV-1 Integrase Enzyme</p>

    <p>          Kassahun, K, McIntosh, I, Cui, D, Hreniuk, D, Merschman, S, Lasseter, K, Azrolan, N, Iwamoto, M, Wagner, J, and Wenning, L</p>

    <p>          Drug Metab Dispos <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591678&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591678&amp;dopt=abstract</a> </p><br />

    <p>17.   61215   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Synergistic inhibition of protease-inhibitor-resistant HIV type 1 by saquinavir in combination with atazanavir or lopinavir</p>

    <p>          Dam, E, Lebel-Binay, S, Rochas, S, Thibaut, L, Faudon, JL, Thomas, CM, Essioux, L, Hill, A, Schutz, M, and Clavel, F</p>

    <p>          Antivir Ther <b>2007</b>.  12(3): 371-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591027&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17591027&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   61216   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Synthesis, cytotoxicity, and antiviral activities of new neolignans related to honokiol and magnolol</p>

    <p>          Amblard, F, Govindarajan, B, Lefkove, B, Rapp, KL, Detorio, M, Arbiser, JL, and Schinazi, RF</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17587572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17587572&amp;dopt=abstract</a> </p><br />

    <p>19.   61217   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Phosphonated Carbocyclic 2&#39;-Oxa-3&#39;-azanucleosides as New Antiretroviral Agents</p>

    <p>          Chiacchio, U, Rescifina, A, Iannazzo, D, Piperno, A, Romeo, R, Borrello, L, Sciortino, MT, Balestrieri, E, Macchi, B, Mastino, A, and Romeo, G</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17580846&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17580846&amp;dopt=abstract</a> </p><br />

    <p>20.   61218   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          In vitro selection and characterization of human immunodeficiency virus type 2 (HIV-2) with decreased susceptibility to lopinavir</p>

    <p>          Masse, S, Lu, X, Dekhtyar, T, Lu, L, Koev, G, Gao, F, Mo, H, Kempf, D, Bernstein, B, Hanna, GJ, and Molla, A</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17576848&amp;dopt=abstract</a> </p><br />

    <p>21.   61219   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of N-acetyl-beta-aryl-1,2-didehydroethylamines as new HIV-1 RT inhibitors in vitro</p>

    <p>          Cheng, P, Jiang, ZY, Wang, RR, Zhang, XM, Wang, Q, Zheng, YT, Zhou, J, and Chen, JJ</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17574419&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17574419&amp;dopt=abstract</a> </p><br />

    <p>22.   61220   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          Differential in vitro inhibitory activity against HIV-1 of alpha-(1-3)- and alpha-(1-6)-D-mannose specific plant lectins : Implication for microbicide development</p>

    <p>          Saidi, H, Nasreddine, N, Jenabian, MA, Lecerf, M, Schols, D, Krief, C, Balzarini, J, and Belec, L</p>

    <p>          J Transl Med <b>2007</b>.  5(1): 28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17565674&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17565674&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   61221   HIV-LS-378; PUBMED-HIV-7/9/2007</p>

    <p class="memofmt1-2">          New 4-[(1-Benzyl-1H-indol-3-yl)carbonyl]-3-hydroxyfuran-2(5H)-ones, beta-Diketo Acid Analogs as HIV-1 Integrase Inhibitors</p>

    <p>          Ferro, S, Barreca, ML, De Luca, L, Rao, A, Monforte, AM, Debyser, Z, Witvrouw, M, and Chimirri, A</p>

    <p>          Arch Pharm (Weinheim) <b>2007</b>.  340(6): 292-298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562561&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562561&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
