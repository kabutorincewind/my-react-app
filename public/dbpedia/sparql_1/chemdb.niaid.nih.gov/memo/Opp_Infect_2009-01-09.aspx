

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-01-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nHWs+BYS65DeKCJxV2kWSzjLzKePyfqji7F4WJ15YWBUw0Pk8NR4Tt5Ky6sTn0WTj1qnXIMFFAvweUDqM1Fjsg+8BSywXp0ugE5NRkXu2yn/euylPtSbSiJnq/k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B837D234" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">December 25, 2008- January 6, 2009</p>

    <h2> Opportunistic protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma]) </h2>

    <p class="ListParagraph">1.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18988681?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Artemisinin derivatives inhibit Toxoplasma gondii in vitro at multiple steps in the lytic cycle.</a></p>

    <p class="NoSpacing">D&#39;Angelo JG, Bordón C, Posner GH, Yolken R, Jones-Brando L.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2009 Jan;63(1):146-50. Epub 2008 Nov 6.</p>

    <p class="NoSpacing">PMID: 18988681 [PubMed - in process]            </p>

    <h2>Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</h2> 

    <p class="ListParagraph">2.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19010674?ordinalpos=98&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">N,N-Dichloroaminosulfonic acids as novel topical antimicrobial agents.</a></p>

    <p class="NoSpacing">Low E, Nair S, Shiau T, Belisle B, Debabov D, Celeri C, Zuck M, Najafi R, Georgopapadakou N, Jain R.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jan 1;19(1):196-8. Epub 2008 Oct 31.</p>

    <p class="NoSpacing">PMID: 19010674 [PubMed - in process]            </p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2> 

    <p class="ListParagraph">3.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19058970?ordinalpos=157&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Selective activity against Mycobacteriumtuberculosis of new quinoxaline 1,4-di-N-oxides.</a></p>

    <p class="NoSpacing">Vicente E, Pérez-Silanes S, Lima LM, Ancizu S, Burguete A, Solano B, Villar R, Aldana I, Monge A.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jan 1;17(1):385-9. Epub 2008 Nov 5.</p>

    <p class="NoSpacing">PMID: 19058970 [PubMed - in process]            </p>  

    <p class="ListParagraph">4.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19032598?ordinalpos=179&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A novel inhibitor of indole-3-glycerol phosphate synthase with activity against multidrug-resistant Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Shen H, Wang F, Zhang Y, Huang Q, Xu S, Hu H, Yue J, Wang H.</p>

    <p class="NoSpacing">FEBS J. 2009 Jan;276(1):144-54. Epub 2008 Nov 20.</p>

    <p class="NoSpacing">PMID: 19032598 [PubMed - in process]            </p>  

    <p class="ListParagraph">5.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/19010684?ordinalpos=206&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro activities of a new antiviral duplex drug linking Zidovudine (AZT) and Foscarnet (PFA) via an octadecylglycerol residue.</a></p>

    <p class="NoSpacing">Schott H, Hamprecht K, Schott S, Schott TC, Schwendener RA.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jan 1;17(1):303-10. Epub 2008 Nov 5.</p>

    <p class="NoSpacing">PMID: 19010684 [PubMed - in process]            </p>  

    <p class="ListParagraph">6.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18726560?ordinalpos=284&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antibacterial activity of two limonoids from Swietenia mahagoni against multiple-drug-resistant (MDR) bacterial strains.</a></p>

    <p class="NoSpacing">Rahman AK, Chowdhury AK, Ali HA, Raihan SZ, Ali MS, Nahar L, Sarker SD.</p>

    <p class="NoSpacing">Nat Med (Tokyo). 2009 Jan;63(1):41-5. Epub 2008 Aug 26.</p>

    <p class="NoSpacing">PMID: 18726560 [PubMed - in process]            </p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">7.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18502542?ordinalpos=308&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antimycobacterial activities of novel 6-nitroquinolone-3-carboxylic acids.</a></p>

    <p class="NoSpacing">Senthilkumar P, Dinakaran M, Yogeeswari P, Sriram D, China A, Nagaraja V.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Jan;44(1):345-58. Epub 2008 Mar 7.</p>

    <p class="NoSpacing">PMID: 18502542 [PubMed - in process]            </p>  

    <p class="ListParagraph">8.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18436347?ordinalpos=309&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and evaluation of in vitro anti-tuberculosis activity of N-substituted glycolamides.</a></p>

    <p class="NoSpacing">Daryaee F, Kobarfard F, Khalaj A, Farnia P.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Jan;44(1):289-95. Epub 2008 Mar 7.</p>

    <p class="NoSpacing">PMID: 18436347 [PubMed - in process]            </p>  

    <p class="ListParagraph">9.         <a href="http://www.ncbi.nlm.nih.gov/pubmed/18343086?ordinalpos=313&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimycobacterial activity of novel N-(substituted)-2-isonicotinoylhydrazinocarbothioamide endowed with high activity towards isoniazid resistant tuberculosis.</a></p>

    <p class="NoSpacing">Sriram D, Yogeeswari P, Yelamanchili Priya D.</p>

    <p class="NoSpacing">Biomed Pharmacother. 2009 Jan;63(1):36-9. Epub 2008 Feb 25.</p>

    <p class="NoSpacing">PMID: 18343086 [PubMed - in process]            </p>  

    <p class="ListParagraph">10.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18078735?ordinalpos=315&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antitubercular activities of novel benzothiazolo naphthyridone carboxylic acid derivatives endowed with high activity toward multi-drug resistant tuberculosis.</a></p>

    <p class="NoSpacing">Dinakaran M, Senthilkumar P, Yogeeswari P, Sriram D.</p>

    <p class="NoSpacing">Biomed Pharmacother. 2009 Jan;63(1):11-8. Epub 2007 Nov 20.</p>

    <p class="NoSpacing">PMID: 18078735 [PubMed - in process]            </p>  

    <p class="ListParagraph">11.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18031974?ordinalpos=316&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimycobacterial activities of novel fluoroquinolones.</a></p>

    <p class="NoSpacing">Senthilkumar P, Dinakaran M, Yogeeswari P, China A, Nagaraja V, Sriram D.</p>

    <p class="NoSpacing">Biomed Pharmacother. 2009 Jan;63(1):27-35. Epub 2007 Oct 29.</p>

    <p class="NoSpacing">PMID: 18031974 [PubMed - in process]            </p>

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">12.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19070499?ordinalpos=100&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antibacterial 5&#39;-O-(N-dipeptidyl)-sulfamoyladenosines.</a></p>

    <p class="NoSpacing">Van de Vijver P, Vondenhoff GH, Denivelle S, Rozenski J, Verhaegen J, Van Aerschot A, Herdewijn P.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jan 1;17(1):260-9. Epub 2008 Nov 27.</p>

    <p class="NoSpacing">PMID: 19070499 [PubMed - in process]            </p> 

    <p class="ListParagraph">13.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18420309?ordinalpos=212&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of new series of 5,6-dihydro-4H-1,2-oxazines via hetero Diels-Alder reaction and evaluation of antimicrobial activity.</a></p>

    <p class="NoSpacing">Manjula MK, Rai KM, Gaonkar SL, Raveesha KA, Satish S.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Jan;44(1):280-8. Epub 2008 Mar 7.</p>

    <p class="NoSpacing">PMID: 18420309 [PubMed - in process]            </p>  

    <p class="ListParagraph">14.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/18396359?ordinalpos=214&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and QSAR modeling of 2-acetyl-2-ethoxycarbonyl-1-[4(4&#39;-arylazo)-phenyl]-N,N-dimethylaminophenyl aziridines as potential antibacterial agents.</a></p>

    <p class="NoSpacing">Sharma P, Kumar A, Upadhyay S, Sahu V, Singh J.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Jan;44(1):251-9. Epub 2008 Feb 29.</p>

    <p class="NoSpacing">PMID: 18396359 [PubMed - in process]            </p>  

    <p class="ListParagraph">15.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19115836?ordinalpos=12&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Isolation, Structure Elucidation, and Biological Activity of Virgineone from Lachnum virgineum Using the Genome-Wide Candida albicans Fitness Test.</a></p>

    <p class="NoSpacing">Ondeyka J, Harris G, Zink D, Basilio A, Vicente F, Bills G, Platas G, Collado J, Gonza&#769;lez A, Cruz MD, Martin J, Kahn JN, Galuska S, Giacobbe R, Abruzzo G, Hickey E, Liberator P, Jiang B, Xu D, Roemer T, Singh SB.</p>

    <p class="NoSpacing">J Nat Prod. 2008 Dec 30. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19115836 [PubMed - as supplied by publisher]    </p>  

    <p class="ListParagraph">16.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19051197?ordinalpos=36&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antifungal and antibacterial activity of the newly synthesized 2-xanthone derivatives.</a></p>

    <p class="NoSpacing">Marona H, Szkaradek N, Karczewska E, Trojanowska D, Budak A, Bober P, Przepiórka W, Cegla M, Szneler E.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Jan;342(1):9-18.</p>

    <p class="NoSpacing">PMID: 19051197 [PubMed - in process]                </p>  

    <p class="ListParagraph">17.        <a href="http://www.ncbi.nlm.nih.gov/pubmed/19010669?ordinalpos=43&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antifungal activity of 1H-pyrrolo[3,2-g]quinoline-4,9-diones and 4,9-dioxo-4,9-dihydro-1H-benzo[f]indoles.</a></p>

    <p class="NoSpacing">Ryu CK, Lee JY, Jeong SH, Nho JH.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jan 1;19(1):146-8. Epub 2008 Nov 5.</p>

    <p class="NoSpacing">PMID: 19010669 [PubMed - in process]            </p>

    <p class="NoSpacing">&nbsp;</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
