

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-09-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yeIqTlSOzSaT6ZFobXgIEmSYGA8FIK7M0Mc3lfHfutQmJ27TNhSDwjdRbNUZKsqT3c63UP7a1hBoVd75IsvBp/hYN6JT/fvDv+H9pHyq8BrU4vPGy4DrWj9JkUA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="82DC5888" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: September 13 - September 26, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24037513">Identification and Structure Elucidation of a Novel Antifungal Compound Produced by Pseudomonas aeruginosa PGPR2 against Macrophomina phaseolina.</a> Illakkiam, D., P. Ponraj, M. Shankar, S. Muthusubramanian, J. Rajendhran, and P. Gunasekaran. Applied Biochemistry and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[24037513].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24022607">In Vitro Effectiveness of Anidulafungin against Candida Sp. Biofilms.</a> Rosato, A., M. Piarulli, B.P. Schiavone, A. Catalano, A. Carocci, A. Carrieri, A. Carone, G. Caggiano, C. Franchini, F. Corbo, and M.T. Montagna. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[24022607].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24055150">Susceptibility Profile of a Brazilian Yeast Stock Collection of Candida Species Isolated from Subjects with Candida-associated Denture Stomatitis with or without Diabetes.</a> Sanita, P.V., E.G. de Oliveira Mima, A.C. Pavarina, J.H. Jorge, A.L. Machado, and C.E. Vergani. Oral Surgery, Oral Medicine, Oral Pathology and Oral Radiology, 2013. <b>[Epub ahead of print]</b>. PMID[24055150].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24030481">Antimicrobial Ergosteroids and Pyrrole Derivatives from Halotolerant Aspergillus flocculosus PT05-1 Cultured in a Hypersaline Medium.</a> Zheng, J., Y. Wang, J. Wang, P. Liu, J. Li, and W. Zhu. Extremophiles, 2013. <b>[Epub ahead of print]</b>. PMID[24030481].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850627">Biphasic Mode of Antibacterial Action of Aminoglycoside Antibiotics-loaded Elastic Hydroxyapatite-glucan Composite.</a> Belcarz, A., A. Zima, and G. Ginalska. International Journal of Pharmaceutics, 2013. 454(1): p. 285-295. PMID[23850627].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24047344">Antibacterial Activity of and Resistance to Small Molecule Inhibitors of the CLPP Peptidase.</a> Compton, C.L., K.R. Schmitz, R.T. Sauer, and J.K. Sello. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24047344].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041897">In Vitro Cross-linking of Peptidoglycan by Mycobacterium tuberculosis L,D-transpeptidases and Inactivation of These Enzymes by Carbapenems.</a> Cordillot, M., V. Dubee, S. Triboulet, L. Dubost, A. Marie, J.E. Hugonnet, M. Arthur, and J.L. Mainardi. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24041897].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041785">IL-22 Inhibits Intracellular Growth of Mycobacterium tuberculosis by Enhancing Calgranulin A Expression.</a> Dhiman, R., S. Venkatasubramanian, P. Paidipally, P.F. Barnes, A. Tvinnereim, and R. Vankayalapati. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[24041785].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24032981">ISPC as Target for Antiinfective Drug Discovery: Synthesis, Enantiomeric Separation and Structural Biology of Fosmidomycin Thia-isosters.</a> Kunfermann, A., C. Lienau, B. Illarionov, J. Held, T. Grawert, C.T. Behrendt, P. Werner, S. Hahn, W. Eisenreich, U. Riederer, B. Mordmuller, A. Bacher, M. Fischer, M. Groll, and T. Kurz. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24032981].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23871806">Medicinal Plants from Open-air Markets in the State of Rio De Janeiro, Brazil as a Potential Source of New Antimycobacterial Agents.</a> Leitao, F., S.G. Leitao, M.Z. de Almeida, J. Cantos, T. Coelho, and P.E. da Silva. Journal of Ethnopharmacology, 2013. 149(2): p. 513-521. PMID[23871806].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24047443">Antimycobacterial Activity of Cyclic Dipeptides Isolated from Bacillus sp. N Strain Associated with Entomopathogenic Nematode.</a> Nishanth Kumar, S. and C. Mohandas. Pharmaceutical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24047443].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23942420">Design and Synthesis of Positional Isomers of 5 and 6-Bromo-1-[(phenyl)sulfonyl]-2-[(4-nitrophenoxy)methyl]-1H-benzimidazoles as Possible Antimicrobial and Antitubercular Agents.</a> Ranjith, P.K., P. Rajeesh, K.R. Haridas, N.K. Susanta, T.N. Guru Row, R. Rishikesan, and N. Suchetha Kumari. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(18): p. 5228-5234. PMID[23942420].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24037308">The DPRE1 Enzyme, One of the Most Vulnerable Targets of Mycobacterium tuberculosis.</a> Riccardi, G., M.R. Pasca, L.R. Chiarelli, G. Manina, A. Mattevi, and C. Binda. Applied Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[24037308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24044050">Structure-activity Studies of Divin: An Inhibitor of Bacterial Cell Division.</a> Zhou, M., Y.J. Eun, I.A. Guzei, and D.B. Weibel. ACS Medicinal Chemistry Letters, 2013. 4(9): p. 880-885. PMID[24044050].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24042110">Protective Humoral Immunity Elicited by a Needle-free Malaria Vaccine Comprised of a Chimeric P. falciparum Circumsporozoite Protein and the TLR5 Agonist Flagellin.</a> Carapau, D., R. Mitchell, A. Nacer, A. Shaw, C. Othoro, U. Frevert, and E. Nardin. Infection and Immunity, 2013. <b>[Epub ahead of print]</b>. PMID[24042110].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24032556">Thiaplakortones A-D, Antimalarial Thiazine Alkaloids from the Australian Marine Sponge Plakortis lita.</a> Davis, R.A., S. Duffy, S. Fletcher, V.M. Avery, and R.J. Quinn. The Journal of Organic Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24032556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br />  

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23933341">Differential Adhesion-inhibitory Patterns of Antibodies Raised against Two Major Variants of the NTS-DBL2X Region of VAR2CSA.</a> Doritchamou, J., P. Bigey, M.A. Nielsen, S. Gnidehou, S. Ezinmegnon, A. Burgain, A. Massougbodji, P. Deloron, A. Salanti, and N.T. Ndam. Vaccine, 2013. 31(41): p. 4516-4522. PMID[23933341].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23916149">Investigation of Acyclic Uridine Amide and 5&#39;-Amido nucleoside Analogues as Potential Inhibitors of the Plasmodium falciparum Dutpase.</a> Hampton, S.E., A. Schipani, C. Bosch-Navarrete, E. Recio, M. Kaiser, P. Kahnberg, D. Gonzalez-Pacanowska, N.G. Johansson, and I.H. Gilbert. Bioorganic &amp; Medicinal Chemistry, 2013. 21(18): p. 5876-5885. PMID[23916149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23876596">In Vitro and in Vivo Antimalarial Activity and Cytotoxicity of Extracts and Fractions from the Leaves, Root-bark and Stem-bark of Triclisia gilletii.</a> Kikueta, C.M., O.K. Kambu, A.P. Mbenza, S.T. Mavinga, B.M. Mbamu, P. Cos, L. Maes, S. Apers, L. Pieters, and R.K. Cimanga. Journal of Ethnopharmacology, 2013. 149(2): p. 438-442. PMID[23876596].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24042109">Functional Comparison of Plasmodium falciparum Transmission-blocking Vaccine Candidates by the Standard Membrane-feeding Assay.</a> Miura, K., E. Takashima, B. Deng, G. Tullo, A. Diouf, S.E. Moretz, D. Nikolaeva, M. Diakite, R.M. Fairhurst, M.P. Fay, C.A. Long, and T. Tsuboi. Infection and Immunity, 2013. <b>[Epub ahead of print]</b>. PMID[24042109].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23916294">Immunogenicity of Single Versus Mixed Allele Vaccines of Plasmodium vivax Duffy Binding Protein Region II.</a> Ntumngia, F.B., J. Schloegel, A.M. McHenry, S.J. Barnes, M.T. George, S. Kennedy, and J.H. Adams. Vaccine, 2013. 31(40): p. 4382-4388. PMID[23916294].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24055215">Screening of Novel Malaria DNA Vaccine Candidates Using Full-length CDNA Library.</a> Shibui, A., S. Nakae, J. Watanabe, Y. Sato, M.E. Tolba, J. Doi, T. Shiibashi, S. Nogami, S. Sugano, and N. Hozumi. Experimental Parasitology, 2013. <b>[Epub ahead of print]</b>. PMID[24055215].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041883">Antiplasmodium Activity and Mechanism of Action of RSM-932A, a Promising Synergistic Inhibitor of Plasmodium falciparum Choline Kinase.</a> Zimmerman, T., C. Moneriz, A. Diez, J.M. Bautista, T.G. Del Pulgar, A. Cebrian, and J.C. Lacal. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24041883].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23913689">Characterization of a Serine Hydrolase Targeted by Acyl-protein Thioesterase Inhibitors in Toxoplasma gondii.</a> Kemp, L.E., M. Rusch, A. Adibekian, H.E. Bullen, A. Graindorge, C. Freymond, M. Rottmann, C. Braun-Breton, S. Baumeister, A.T. Porfetye, I.R. Vetter, C. Hedberg, and D. Soldati-Favre. The Journal of Biological Chemistry, 2013. 288(38): p. 27002-27018. PMID[23913689].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0913-092613.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322832200085">Effect of Schinus terebinthifolius on Candida albicans Growth Kinetics, Cell Wall Formation and Micromorphology.</a> Alves, L.A., I.D. Freires, T.M. Pereira, A. de Souza, E.D. Lima, and R.D. de Castro. Acta Odontologica Scandinavica, 2013. 71(3-4): p. 965-971. ISI[000322832200085].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323532800019">Bronsted Acidic Ionic Liquid Catalysis: An Efficient and Eco-friendly Synthesis of Novel Fused Pyrano pyrimidinones and Their Antimicrobial Activity.</a> Banothu, J., R. Gali, R. Velpula, and R. Bavantula. Journal of Chemical Sciences, 2013. 125(4): p. 843-849. ISI[000323532800019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323536100036">Mycobacterium tuberculosis Shikimate Kinase Inhibitors: Design and Simulation Studies of the Catalytic Turnover.</a> Blanco, B., V. Prado, E. Lence, J.M. Otero, C. Garcia-Doval, M.J. van Raaij, A.L. Llamas-Saiz, H. Lamb, A.R. Hawkins, and C. Gonzalez-Bello. Journal of the American Chemical Society, 2013. 135(33): p. 12366-12376. ISI[000323536100036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323114200017">Whole Cell Screen for Inhibitors of pH Homeostasis in Mycobacterium tuberculosis.</a> Darby, C.M., H.I. Ingolfsson, X.J. Jiang, C. Shen, M.N. Sun, N. Zhao, K. Burns, G. Liu, S. Ehrt, J.D. Warren, O.S. Anderson, S.J. Brickner, and C. Nathan. Plos One, 2013. 8(7): p. e68942. ISI[000323114200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323285500033">Identification of an Aminothiazole with Antifungal Activity against Intracellular Histoplasma capsulatum.</a> Edwards, J.A., M.M. Kemski, and C.A. Rappleye. Antimicrobial Agents and Chemotherapy, 2013. 57(9): p. 4349-4359. ISI[000323285500033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323628500011">Regioselective Synthesis, Characterization and Antimicrobial Evaluation of S-glycosides and S,N-diglycosides of 1,2-Dihydro-5-(1H-indol-2-yl)-1,2,4-triazole-3-thione.</a> El Ashry, E.H., E.H. El Tamany, M.E. Abd El Fattah, A.T.A. Boraei, and H.M. Abd El-Nabi. European Journal of Medicinal Chemistry, 2013. 66: p. 106-113. ISI[000323628500011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323187000033">Synthesis and Structure-Activity Relationships Evaluation of Benzothiazinone Derivatives as Potential Anti-tubercular Agents.</a> Gao, C., T.H. Ye, N.Y. Wang, X.X. Zeng, L.D. Zhang, Y. Xiong, X.Y. You, Y. Xia, Y. Xu, C.T. Peng, W.Q. Zuo, Y.Q. Wei, and L.T. Yu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4919-4922. ISI[000323187000033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323410100002">Selective Peptide Inhibitors of Bifunctional Thymidylate Synthase-dihydrofolate Reductase from Toxoplasma gondii Provide Insights into Domain-domain Communication and Allosteric Regulation.</a> Landau, M.J., H. Sharma, and K.S. Anderson. Protein Science, 2013. 22(9): p. 1161-1173. ISI[000323410100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323097300183">Antifungal Activity of Phlorotannins against Dermatophytes and Yeasts: Approaches to the Mechanism of Action and Influence on Candida albicans Virulence Factor.</a> Lopes, G., E. Pinto, P.B. Andrade, and P. Valentao. Plos One, 2013. 8(8): p. e72203. ISI[000323097300183].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323187000029">Synthesis and Binary QSAR Study of Antitubercular Quinolylhydrazides.</a> Manvar, A., V. Khedkar, J. Patel, V. Vora, N. Dodia, G. Patel, E. Coutinho, and A. Shah. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4896-4902. ISI[000323187000029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323386400009">Transient Expression and Characterization of the Antimicrobial Peptide Protegrin-1 in Nicotiana tabacum for Control of Bacterial and Fungal Mammalian Pathogens.</a> Patino-Rodriguez, O., B. Ortega-Berlanga, Y.Y. Llamas-Gonzalez, M.A. Flores-Valdez, A. Herrera-Diaz, R. Montes-de-Oca-Luna, S.S. Korban, and A.G. Alpuche-Solis. Plant Cell Tissue and Organ Culture, 2013. 115(1): p. 99-106. ISI[000323386400009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323628500051">7-Hydroxy-(E)-3-phenylmethylene-chroman-4-one Analogues as Efflux Pump Inhibitors against Mycobacterium smegmatis mc<sup>2</sup> 155.</a> Roy, S.K., N. Kumari, S. Gupta, S. Pahwa, H. Nandanwar, and S.M. Jachak. European Journal of Medicinal Chemistry, 2013. 66: p. 499-507. ISI[000323628500051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323285500060">Efficacy of Amphotericin B at Suboptimal Dose Combined with Voriconazole in a Murine Model of Aspergillus fumigatus Infection with Poor in Vivo Response to the Azole.</a> Sandoval-Denis, M., F.J. Pastor, J. Capilla, and J. Guarro. Antimicrobial Agents and Chemotherapy, 2013. 57(9): p. 4540-4542. ISI[000323285500060].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323338400029">Inhibitory Effect of Sodium metabisulphite and Chlorine on Growth of Aspergillus spp. And Penicillium spp. Strains Isolated from Marine Shrimp.</a> Santos, Y.F.D., A.P.B. Veloso, R.M. Calvet, M.M.G. Pereira, C.M. Pereyra, A.M. Dalcero, A.M. Torres, and M.C.S. Muratori. Ciencia Rural, 2013. 43(9): p. 1721-1726. ISI[000323338400029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323336800002">Chloroquine Sensitizes Biofilms of Candida albicans to Antifungal Azoles.</a> Shinde, R.B., J.S. Raut, N.M. Chauhan, and S.M. Karuppayil. Brazilian Journal of Infectious Diseases, 2013. 17(4): p. 395-400. ISI[000323336800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323701000005">Purification and Chemical Characterization of Antimicrobial Compounds from a New Soil Isolate Streptomyces rimosus MTCC 10792.</a> Singh, N., V. Rai, and C.K.M. Tripathi. Applied Biochemistry and Microbiology, 2013. 49(5): p. 473-480. ISI[000323701000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323467700007">Activity of Antifungal Combinations against Aspergillus Species Evaluated by Isothermal Microcalorimetry.</a> Tafin, U.F., C. Orasch, and A. Trampuz. Diagnostic Microbiology and Infectious Disease, 2013. 77(1): p. 31-36. ISI[000323467700007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br /> 

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323294600054">42. Synthesis and Biological Activity of Benzo-fused 7-deazaadenosine Analogues. 5- and 6-Substituted 4-amino- or 4-alkylpyrimido[4,5-b]indole ribonucleosides.</a> Tichy, M., R. Pohl, E. Tloust&#39;ova, J. Weber, G. Bahador, Y.J. Lee, and M. Hocek. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 5362-5372. ISI[000323294600054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0913-092613.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
