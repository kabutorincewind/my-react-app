

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-67.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="wfI+RCpP83GAgSxfOiyOrSe8JHLkieWLznRw7qgeRWiO6SR0AJfP+W90da94Na5XT/eSBfXfCq2sSR36trO2pkv1i7QlnGIi8UVjGnKPRv3C2jdEWDx920l3Tpc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F7303C7C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-67-MEMO</b> </p>

<p>    1.    5991   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Combination therapies with L-FMAU for the treatment of hepatitis B virus infection</b>((Triangle Pharmaceuticals, Inc. USA)</p>

<p>           Furman, Phillip A</p>

<p>           PATENT: WO 2004006843 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 76 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    2.    5992   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Methods for treating viral infection using IL-28 and IL-29</b>((Zymogenetics, Inc. USA)</p>

<p>           Klucher, Kevin M, Sivakumar, Pallavur V, Kindsvogel, Wayne R, and Henderson, Katherine E</p>

<p>           PATENT: WO 2004037995 A2;  ISSUE DATE: 20040506</p>

<p>           APPLICATION: 2003; PP: 102 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    5993   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p><b>           ELVIRA HSV, a Yield Reduction Assay for Rapid Herpes Simplex Virus Susceptibility Testing</b> </p>

<p>           Stranska, R, Schuurman, R, Scholl, DR, Jollick, JA, Shaw, CJ, Loef, C, Polman, M, and Van, Loon AM</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2331-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155247&amp;dopt=abstract</a> </p><br />

<p>    4.    5994   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Mutations conferring resistance to a potent hepatitis C virus serine protease inhibitor in vitro</p>

<p>           Lu, L, Pilot-Matias, TJ, Stewart, KD, Randolph, JT, Pithawalla, R, He, W, Huang, PP, Klein, LL, Mo, H, and Molla, A</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2260-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155230&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155230&amp;dopt=abstract</a> </p><br />

<p>    5.    5995   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Phosphorothioate di- and trinucleotides as a novel class of anti-hepatitis B virus agents</p>

<p>           Iyer, RP, Jin, Y, Roland, A, Morrey, JD, Mounir, S, and Korba, B</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2199-205</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155222&amp;dopt=abstract</a> </p><br />

<p>    6.    5996   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           {alpha}-Galactosylceramide and Novel Synthetic Glycolipids Directly Induce the Innate Host Defense Pathway and Have Direct Activity against Hepatitis B and C Viruses</p>

<p>           Mehta, AS, Gu, B, Conyers, B, Ouzounov, S, Wang, L, Moriarty, RM, Dwek, RA, and Block, TM</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2085-2090</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155204&amp;dopt=abstract</a> </p><br />

<p>    7.    5997   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           The regulation of rhinovirus infection in vitro by IL-8, HuIFN-alpha, and TNF-alpha</p>

<p>           Berg, K, Andersen, H, and Owen, TC</p>

<p>           APMIS 2004. 112(3): 172-182</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15153159&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15153159&amp;dopt=abstract</a> </p><br />

<p>    8.    5998   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Antiviral activity of natural and synthetic brassinosteroids</p>

<p>           Wachsman, Monica B, Ramirez, Javier A, Talarico, Laura B, Galagovsky, Lydia R, and Coto, Celia E</p>

<p>           Current Medicinal Chemistry: Anti-Infective Agents 2004 . 3(2): 163-179</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    9.    5999   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           A structural basis for the inhibition of the NS5 dengue virus mRNA 2&#39;-O-methyltransferase domain by Ribavirin 5&#39;-triphosphate</p>

<p>           Benarroch, D, Egloff, MP, Mulard, L, Guerreiro, C, Romette, JL, and Canard, B</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15152003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15152003&amp;dopt=abstract</a> </p><br />

<p>  10.    6000   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Severe acute respiratory syndrome coronavirus (SARS-CoV) infection inhibition using spike protein heptad repeat-derived peptides</p>

<p>           Bosch, BJ, Martina, BE, Van, Der Zee R, Lepault, J, Haijema, BJ, Versluis, C, Heck, AJ, De, Groot R, Osterhaus, AD, and Rottier, PJ</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15150417&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15150417&amp;dopt=abstract</a> </p><br />

<p>  11.    6001   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Aryl phosphate derivatives of d4T with potent anti-viral activity against hemorrhagic fever viruses</b> ((USA))</p>

<p>           Uckun, Fatih M</p>

<p>           PATENT: US 2004077607 A1;  ISSUE DATE: 20040422</p>

<p>           APPLICATION: 2002-19258; PP: 25 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    6002   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           The small RING finger protein Z drives arenavirus budding: Implications for antiviral strategies</p>

<p>           Perez, Mar, Craven, Rebecca C, and de la Torre, Juan C</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2003. 100(22): 12978-12983</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  13.    6003   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Purification and characterization of a new antiviral protein from the leaves of Pandanus amaryllifolius (Pandanaceae)</p>

<p>           Ooi, LS, Sun, SS, and Ooi, VE</p>

<p>           Int J Biochem Cell Biol 2004. 36(8): 1440-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15147723&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15147723&amp;dopt=abstract</a> </p><br />

<p>  14.    6004   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Antiviral Therapies</b></p>

<p>           Hallenberger, S</p>

<p>           Journal of Clinical Virology 2004. 30: S23-S25</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    6005   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           HIV protease inhibitor nelfinavir inhibits replication of SARS-associated coronavirus</p>

<p>           Yamamoto, N, Yang, R, Yoshinaka, Y, Amari, S, Nakano, T, Cinatl, J, Rabenau, H, Doerr, HW, Hunsmann, G, Otaka, A, Tamamura, H, Fujii, N, and Yamamoto, N</p>

<p>           Biochem Biophys Res Commun 2004. 318(3):  719-25</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15144898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15144898&amp;dopt=abstract</a> </p><br />

<p>  16.    6006   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Viral Hepatitis in 2003</b></p>

<p>           Fung, SK and  Lok, ASF</p>

<p>           Current Opinion in Gastroenterology 2004. 20(3): 241-247</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    6007   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           IGFBP-3, a Marker of Cellular Senescence, Is Overexpressed in Human Papillomavirus-Immortalized Cervical Cells and Enhances IGF-1-Induced Mitogenesis</p>

<p>           Baege, AC, Disbrow, GL, and Schlegel, R</p>

<p>           J Virol 2004. 78(11): 5720-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140969&amp;dopt=abstract</a> </p><br />

<p>  18.    6008   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           A Novel Class of Phosphonate Nucleosides. 9-[(1-Phosphonomethoxycyclopropyl)methyl]guanine as a Potent and Selective Anti-HBV Agent</p>

<p>           Choi, JR, Cho, DG, Roh, KY, Hwang, JT, Ahn, S, Jang, HS, Cho, WY, Kim, KW, Cho, YG, Kim, J, and Kim, YZ</p>

<p>           J Med Chem 2004. 47(11): 2864-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139764&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139764&amp;dopt=abstract</a> </p><br />

<p>  19.    6009   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Severe acute respiratory syndrome (SARS): a review</p>

<p>           Vijayanand, P, Wilkins, E, and Woodhead, M</p>

<p>           Clin Med 2004. 4 (2): 152-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139736&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139736&amp;dopt=abstract</a> </p><br />

<p>  20.    6010   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Modulation of innate immunity by filoviruses</p>

<p>           Basler, Christopher F and Palese, Peter</p>

<p>           CONFERENCE: Ebola and Marburg Viruses 2004: 305-349</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  21.    6011   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Methods for detecting, preventing, and treating African hemorrhagic fever using antigenic or inhibitory peptides - analogs of a portion of the fusion (F) glycoprotein (GP) of a Filovirus</b>((Board of Supervisors of Louisiana State University and Agricultural and Mechanical College, USA)</p>

<p>           Gallaher, William R</p>

<p>           PATENT: US 6713069 B1;  ISSUE DATE: 20040330</p>

<p>           APPLICATION: 97-51696; PP: 27 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    6012   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Comparisons of the HBV and HIV polymerase, and antiviral resistance mutations</p>

<p>           Bartholomeusz, A, Tehan, BG, and Chalmers, DK</p>

<p>           Antivir Ther 2004. 9(2): 149-60</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134177&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134177&amp;dopt=abstract</a> </p><br />

<p>  23.    6013   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Epoetin alfa maintains ribavirin dose in HCV-infected patients: a prospective, double-blind, randomized controlled study</p>

<p>           Afdhal, NH, Dieterich, DT, Pockros, PJ, Schiff, ER, Shiffman, ML, Sulkowski, MS , Wright, T, Younossi, Z, Goon, BL, Tang, KL, and Bowers, PJ</p>

<p>           Gastroenterology 2004. 126(5): 1302-1311</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15131791&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15131791&amp;dopt=abstract</a> </p><br />

<p>  24.    6014   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Adenosine 5 &#39;-Triphosphate (Atp(4-)): Aspects of the Coordination Chemistry of a Multitalented Biological Substrate</b></p>

<p>           Sigel, H</p>

<p>           Pure and Applied Chemistry 2004. 76(2): 375-388</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    6015   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Synthesis and Biological Evaluation of Novel Tert-Azido or Tert-Amino Substituted Penciclovir Analogs</b></p>

<p>           Kim, HO, Baek, HW, Moon, HR, Kim, DK, Chun, MW, and Jeong, LS</p>

<p>           Organic &amp; Biomolecular Chemistry  2004. 2(8): 1164-1168</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    6016   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Excision of Incorporated Nucleotide Analogue Chain-terminators can Diminish their Inhibitory Effects on Viral RNA-dependent RNA Polymerases</p>

<p>           D&#39;Abramo, Claudia M, Cellai, Luciano, and Gotte, Matthias</p>

<p>           Journal of Molecular Biology 2004.  337(1): 1-14</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    6017   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Antiviral drugs in current clinical use</p>

<p>           De Clercq, E</p>

<p>           J Clin Virol 2004. 30(2): 115-133</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15125867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15125867&amp;dopt=abstract</a> </p><br />

<p>  28.    6018   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Effects of four antiviral substances on lethal vaccinia virus (IHD strain) respiratory infections in mice</p>

<p>           Smee, DF, Wong, MH, Bailey, KW, Beadle, JR, Hostetler, KY, and Sidwell, RW</p>

<p>           Int J Antimicrob Agents 2004. 23(5): 430-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120719&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120719&amp;dopt=abstract</a> </p><br />

<p>  29.    6019   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Persistent infection and suppression of host response by alphaviruses</p>

<p>           Frolov, I</p>

<p>           Arch Virol Suppl 2004(18): 139-47</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15119769&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15119769&amp;dopt=abstract</a> </p><br />

<p>  30.    6020   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Method for the treatment or prevention of virus infection using polybiguanide-based compounds</b> ((USA))</p>

<p>           Labib, Mohamed E and Stockel, Richard F</p>

<p>           PATENT: US 2004009144 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003-42540; PP: 30 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    6021   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Antiretrovirals, Part 1: Overview, History, and Focus on Protease Inhibitors</b></p>

<p>           Wynn, GH, Zapor, MJ, Smith, BH, Wortmann, G, Oesterheld, JR, Armstrong, SC, and Cozza, KL</p>

<p>           Psychosomatics 2004. 45(3): 262-270</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    6022   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Susceptibility of porcine cytomegalovirus to antiviral drugs</p>

<p>           Fryer, JF, Griffiths, PD, Emery, VC, and Clark, DA</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117919&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117919&amp;dopt=abstract</a> </p><br />

<p>  33.    6023   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Metal Ion Complexes of Antivirally Active Nucleotide Analogues. Conclusions Regarding Their Biological Action</b></p>

<p>           Sigel, H</p>

<p>           Chemical Society Reviews 2004. 33(3): 191-200</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    6024   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Discovery, SAR, and medicinal chemistry of herpesvirus helicase primase inhibitors</p>

<p>           Kleymann, Gerald</p>

<p>           Current Medicinal Chemistry: Anti-Infective Agents 2004 . 3(1): 69-83</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  35.    6025   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p><b>           Preparation of heteroarylethanolamines as antiviral agents</b>((Pharmacia &amp; Upjohn Company, USA and Fleck, Bruce Francis)</p>

<p>           Fleck, Thomas J, Schnute, Mark E, Cudahy, Michele M, Anderson, David J, Judge, Thomas M, Herrington, Paul M, Nair, Sajiv K, Scott, Allen, Perrault, William R, Tanis, Steven P, Nieman, James A, and Collier, Sarah A</p>

<p>           PATENT: WO 2004022567 A1;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 90 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    6026   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Antiviral Chemotherapeutic Agents Against Respiratory Viruses: Where Are We Now and What&#39;s in the Pipeline?</b></p>

<p>           Brooks, MJ, Sasadeusz, JJ, and Tannock, GA</p>

<p>           Current Opinion in Pulmonary Medicine 2004. 10(3): 197-203</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  37.    6027   DMID-LS-67; PUBMED-DMID-5/25/2004</p>

<p class="memofmt1-2">           Identification of Novel Inhibitors of the SARS Coronavirus Main Protease 3CL(pro)</p>

<p>           Bacha, U, Barrila, J, Velazquez-Campoy, A, Leavitt, SA, and Freire, E</p>

<p>           Biochemistry 2004. 43(17): 4906-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109248&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109248&amp;dopt=abstract</a> </p><br />

<p>  38.    6028   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Role of Cytoskeleton Components in Measles Virus Replication</b></p>

<p>           Berghall, H, Wallen, C, Hyypia, T, and Vainionpaa, R</p>

<p>           Archives of Virology 2004. 149(5):  891-901</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  39.    6029   DMID-LS-67; WOS-DMID-5/25/2004</p>

<p>           <b>Comparison of First-Line Antiretroviral Therapy With Regimens Including Nevirapine, Efavirenz, or Both Drugs, Plus Stavudine and Lamivudine: a Randomised Open-Label Trial, the 2nn Study</b></p>

<p>           Van Leth, F,  Phanuphak, P, Ruxrungtham, K, Baraldi, E, Miller, S, Gazzard, B, Cahn, P, Lalloo, UG, Van Der Westhuizen, IP, Malan, DR, Johnson, MA, Santos, BR, Mulcahy, F, Wood, R, Levi, GC, Reboredo, G, Squires, K, Cassetti, I, Petit, D, Raffi, F, Katlama, C, Murphy, RL, Horban, A, Dam, JP, Hassink, E, Van Leeuwen, R, Robinson, P, Wit, FW, and Lange, JMA</p>

<p>           Lancet 2004. 363(9417): 1253-1263</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    6030   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           The structure of the agaran sulfate from Acanthophora spicifera (Rhodomelaceae, Ceramiales) and its antiviral activity. Relation between structure and antiviral activity in agarans</p>

<p>           Duarte, Maria ER, Cauduro, Jean P, Noseda, Diego G, Noseda, Miguel D, Goncalves, Alan G, Pujol, Carlos A, Damonte, Elsa B, and Cerezo, Alberto S</p>

<p>           Carbohydrate Research 2004. 339(2):  335-347</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    6031   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Inhibition of severe acute respiratory syndrome-associated coronavirus (SARSCoV) by calpain inhibitors and b-D-N4-hydroxycytidine</p>

<p>           Barnard, Dale L, Hubbard, Valerie D, Burton, Jared, Smee, Donald F, Morrey, John D, Otto, Michael J, and Sidwell, Robert W</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 2004. 15(1): 15-22</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.    6032   DMID-LS-67; SCIFINDER-DMID-5/25/2004</p>

<p class="memofmt1-2">           Virology of SARS coronavirus</p>

<p>           Kirikae, Teruo</p>

<p>           Iryo 2004. 58(3): 138-142</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
