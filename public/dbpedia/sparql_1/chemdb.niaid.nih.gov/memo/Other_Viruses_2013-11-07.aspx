

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-11-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TUgK5Sabruh1Sz2GQyn9oIKRsM1Llv0M+i58hoMPnM3iV45cALslF2gXNjJRMsqXB2RnF4uPSINeo9OoJbrRa20D475o2SbQiZNdJ/PGhvWznKx7rnloSg9Xl2g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="624B2B7D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: October 25 - November 7, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24164660">Metformin Inhibits Hepatitis B Virus Protein Production and Replication in Human Hepatoma Cells<i>.</i></a> Xun, Y.H., Y.J. Zhang, Q.C. Pan, R.C. Mao, Y.L. Qin, H.Y. Liu, Y.M. Zhang, Y.S. Yu, Z.H. Tang, M.J. Lu, G.Q. Zang, and J.M. Zhang. Journal of Viral Hepatitis, 2013. <b>[Epub ahead of print]</b>; PMID[24164660].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24171666">Synthesis of HCV Replicase Inhibitors: Base Catalyzed Synthesis of Protected alpha-Hydrazinoesters and Selective Aerobic Oxidation with Catalytic Pt/Bi/C for Synthesis of Imidazole-4,5-dicarbaldehyde<i>.</i></a> Bowman, R.K., A.D. Brown, J.H. Cobb, J.F. Eaddy, M.R. Leivers, J.F. Miller, M.B. Mitchell, D.E. Patterson, M.A. Toczko, S. Xie, and M.A. Hatcher. The Journal of Organic Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24171666].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24165192">A Small Molecule Inhibitor of Hepatitis C Viral Infectivity<i>.</i></a> Bush, C.O., M.V. Pokrovskii, R. Saito, P. Morganelli, E. Canales, M.O. Clarke, S.E. Lazerwith, J. Golde, B.G. Reid, K. Babaoglu, N. Pagratis, W. Zhong, W.E.t. Delaney, M.S. Paulson, and R.K. Beran. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[24165192].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23659500">Amiodarone Inhibits the Entry and Assembly Steps of Hepatitis C Virus Life Cycle<i>.</i></a> Cheng, Y.L., K.H. Lan, W.P. Lee, S.H. Tseng, L.R. Hung, H.C. Lin, F.Y. Lee, S.D. Lee, and K.H. Lan. Clinical Science, 2013. 125(9): p. 439-448; PMID[23659500].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24152340">Inhibition of Hepatitis C Virus by the Cyanobacterial Protein MVL: Mechanistic Differences between the High-mannose Specific Lectins MVL, CV-N, and GNA<i>.</i></a> Kachko, A., S. Loesgen, S. Shahzad-Ul-Hussan, W. Tan, I. Zubkova, K. Takeda, F. Wells, S. Rubin, C.A. Bewley, and M.E. Major. Molecular Pharmaceutics, 2013. <b>[Epub ahead of print]</b>; PMID[24152340].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24180242">Phenylboronic acid-modified Nanoparticles: Potential Antiviral Therapeutics<i>.</i></a> Khanal, M., T. Vausselin, A. Barras, O.P. Bande, K. Turcheniuk, M. Benazza, V. Zaitzev, C.M. Teodorescu, R. Boukherroub, A. Siriwardena, J. Dubuisson, and S. Szunerits. ACS Applied Materials &amp; Interfaces, 2013. <b>[Epub ahead of print]</b>; PMID[24180242].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24161679">2-Heteroarylimino-5-arylidene-4-thiazolidinones as a New Class of Non-nucleoside Inhibitors of HCV NS5B Polymerase<i>.</i></a> Kucukguzel, I., G. Satilmis, K.R. Gurukumar, A. Basu, E. Tatar, D.B. Nichols, T.T. Talele, and N. Kaushik-Basu. European Journal of Medicinal Chemistry, 2013. 69: p. 931-941; PMID[24161679].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24177211">Retinoids and Rexinoids Inhibit Hepatitis C Virus Independently of Retinoid Receptor Signaling<i>.</i></a> Murakami, Y., M. Fukasawa, Y. Kaneko, T. Suzuki, T. Wakita, and H. Fukazawa. Microbes and Infection, 2013. <b>[Epub ahead of print]</b>; PMID[24177211].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24025401">Inhibition of Hepatitis C Virus Infection by Polyoxometalates<i>.</i></a> Qi, Y., Y. Xiang, J. Wang, Y. Qi, J. Li, J. Niu, and J. Zhong. Antiviral Research, 2013. 100(2): p. 392-398; PMID[24025401].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23999140">Synthesis and Antiviral Activity of a Novel Class of (5-Oxazolyl)phenyl amines<i>.</i></a> Zhong, Z.J., D.J. Zhang, Z.G. Peng, Y.H. Li, G.Z. Shan, L.M. Zuo, L.T. Wu, S.Y. Li, R.M. Gao, and Z.R. Li. European Journal of Medicinal Chemistry, 2013. 69: p. 32-43; PMID[23999140].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24177676">New Diterpenoids from Soft Coral Sarcophyton ehrenbergi<i>.</i></a> Wang, S.K., M.K. Hsieh, and C.Y. Duh. Marine Drugs, 2013. 11(11): p. 4318-4327; PMID[24177676].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/24172244">12. Synthesis, Characterization, X-Ray Structure and Biological Activities of C-5-Bromo-2-hydroxyphenylcalix[4]-2-methyl Resorcinarene<i>.</i></a> Abosadiya, H.M., S.A. Hasbullah, M.M. Mackeen, S.C. Low, N. Ibrahim, M. Koketsu, and B.M. Yamin. Molecules, 2013. 18(11): p. 13369-13384; PMID[24172244].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24172213">Anti HSV-1 Activity of Halistanol Sulfate and Halistanol Sulfate C Isolated from Brazilian Marine Sponge Petromica citrina (Demospongiae)<i>.</i></a> da Rosa Guimaraes, T., C.G. Quiroz, C.R. Borges, S.Q. de Oliveira, M.T. de Almeida, E.M. Bianco, M.I. Moritz, J.L. Carraro, J.A. Palermo, G. Cabrera, E.P. Schenkel, F.H. Reginatto, and C.M. Simoes. Marine Drugs, 2013. 11(11): p. 4176-4192; PMID[24172213].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24021190">Baseline Sensitivity of HSV-1 and HSV-2 Clinical Isolates and Defined Acyclovir-resistant Strains to the Helicase-Primase Inhibitor Pritelivir<i>.</i></a> Field, H.J., M.L. Huang, E.M. Lay, I. Mickleburgh, H. Zimmermann, and A. Birkmann. Antiviral Research, 2013. 100(2): p. 297-299; PMID[24021190].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24012999">Synthetic Analogues of Bovine Bactenecin Dodecapeptide Reduce Herpes Simplex Virus Type 2 Infectivity in Mice<i>.</i></a> Shestakov, A., H. Jenssen, R.E. Hancock, I. Nordstrom, and K. Eriksson. Antiviral Research, 2013. 100(2): p. 455-459; PMID[24012999].</p>

    <p class="plaintext">[<b>PubMed</b>] OV_1025-110713.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325483000037">Benzohydroxamic acids as Potent and Selective anti-HCV Agents<i>.</i></a> Kozlov, M.V., A.A. Kleymenova, L.I. Romanova, K.A. Konduktorov, O.A. Smirnova, V.S. Prasolov, and S.N. Kochetkov. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(21): p. 5936-5940; ISI[000325483000037].</p>

    <p class="plaintext">[<b>WOS</b>] OV_1025-110713.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325469200024">Formal Synthesis of Hepatitis C Virus NS5B Polymerase Inhibitor<i>.</i></a> Noro, T., K. Okano, and H. Tokuyama. Synlett, 2013. 24(16): p. 2143-2147; ISI[000325469200024].</p>

    <p class="plaintext">[<b>WOS</b>] OV_1025-110713.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
