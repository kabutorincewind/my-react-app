

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-04-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uqcLwPjsYWdTZdckZpVepP0ziWnhUW+jw0H5NyJgAf9/MbqB56a2q0PDnCc0R0Eer9cbEXfi0IUJM+k2Kz80okWaZpWm0JrU4OLYYNkufTMLceHhu+UxRIIeOIc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="13E88AC9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 11 - April 24, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24680058">Structural Modifications of CH(OH)-DAPYs as New HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Yan, Z.H., X.Y. Huang, H.Q. Wu, W.X. Chen, Q.Q. He, F.E. Chen, E. De Clercq, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry, 2014. 22(8): p. 2535-2541. PMID[24680058].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24727459">Amino acid Prodrugs: An Approach to Improve the Absorption of HIV-1 Protease Inhibitor, Lopinavir.</a> Patel, M., N. Mandava, M. Gokulgandhi, D. Pal, and A.K. Mitra. Pharmaceuticals, 2014. 7(4): p. 433-452. PMID[24727459].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24684270">Basic Quinolinonyl diketo acid Derivatives as Inhibitors of HIV Integrase and Their Activity against RNase H Function of Reverse Transcriptase.</a> Costi, R., M. Metifiot, S. Chung, G. Cuzzucoli Crucitti, K. Maddali, L. Pescatori, A. Messore, V.N. Madia, G. Pupo, L. Scipione, S. Tortorella, F.S. Di Leva, S. Cosconati, L. Marinelli, E. Novellino, S.F. Le Grice, A. Corona, Y. Pommier, C. Marchand, and R. Di Santo. Journal of Medicinal Chemistry, 2014. 57(8): p. 3223-3234. PMID[24684270].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24736215">HIV-1 Latency: An Update of Molecular Mechanisms and Therapeutic Strategies.</a> Battistini, A. and M. Sgarbanti. Viruses, 2014. 6(4): p. 1715-1758. PMID[24736215].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0411-042414.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333121600136">Anti-HIV-1 miRNAs Regulate Cocaine Induced Enhancement of HIV-1 Replication in Human Monocyte Derived Macrophages.</a> Swepson, C.B., J. Pandhare, and C. Dash. Journal of Neuroimmune Pharmacology, 2014. 9(1): p. 54-54. ISI[000333121600136].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332534100002">The Use of 2-(1-Alkoxyalkylidene)-1,3-dicarbonyl Compounds in Organic Synthesis.</a> Kudyakova, Y.S., D.N. Bazhin, M.V. Goryaeva, Y.V. Burgart, and V.I. Saloutin. Russian Chemical Reviews, 2014. 83(2): p. 120-142. ISI[000332534100002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000332770000021">Synthesis, Characterization and Pharmacological Investigations of Some Novel Heterocyclic Derivatives Incorporating Pyrene and Sugar Moieties.</a> Khalifa, N.M., M.S. Mohamed, M.E. Zaki, M.A. Al-Omar, and Y.M. Zohny. Research on Chemical Intermediates, 2014. 40(4): p. 1565-1574. ISI[000332770000021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0411-042414.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000333116600001">The Myxobacterial Metabolite Ratjadone A Inhibits HIV Infection by Blocking the Rev/CRM1-mediated Nuclear Export Pathway.</a> Fleta-Soriano, E., J.P. Martinez, B. Hinkelmann, K. Gerth, P. Washausen, J. Diez, R. Frank, F. Sasse, and A. Meyerhans. Microbial Cell Factories, 2014. 13(17): p. 10. ISI[000333116600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0411-042414.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
