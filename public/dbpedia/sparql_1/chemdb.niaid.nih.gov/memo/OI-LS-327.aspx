

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-327.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BR+psqDiVsrfc+g/udVLxeO+BMEGq6Bqps3yE4376Ia4AYCodbz1EgQOnEV6MyhbRvYSjPPl7hlg1ZjEVnPM4kDqlN3K+vF3Mgrlzv8TLyZpuTkIQlz1MBreszw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8FFF6A71" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-327-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47674   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Structure-Based Design, Synthesis, and Biological Evaluation of Inhibitors of Mycobacterium tuberculosis Type II Dehydroquinase</p>

    <p>          Sanchez-Sixto, C, Prazeres, VF, Castedo, L, Lamb, H, Hawkins, AR, and Gonzalez-Bello, C</p>

    <p>          J Med Chem <b>2005</b>.  48(15): 4871-4881</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033267&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033267&amp;dopt=abstract</a> </p><br />

    <p>2.     47675   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Therapeutic strategies for human microsporidia infections</p>

    <p>          Didier, Elizabeth S, Maddry, Joseph A, Brindley, Paul J, Stovall, Mary E, and Didier, Peter J</p>

    <p>          Expert Review of Anti-Infective Therapy <b>2005</b>.  3(3): 419-434</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     47676   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Structure of Mycobacterium tuberculosis glutamine synthetase in complex with a transition-state mimic provides functional insights</p>

    <p>          Krajewski, WW, Jones, TA, and Mowbray, SL</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027359&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16027359&amp;dopt=abstract</a> </p><br />

    <p>4.     47677   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Phylogenetic relationships of methionine aminopeptidase 2 among Encephalitozoon species and genotypes of microsporidia</p>

    <p>          Pandrea, Ivona, Mittleider, Derek, Brindley, Paul J, Didier, Elizabeth S, and Robertson, David L</p>

    <p>          Molecular and Biochemical Parasitology <b>2005</b>.  140(2): 141-152</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     47678   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          A novel two-component system found in Mycobacterium tuberculosis</p>

    <p>          Morth, JP, Gosmann, S, Nowak, E, and Tucker, PA</p>

    <p>          FEBS Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16026786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16026786&amp;dopt=abstract</a> </p><br />

    <p>6.     47679   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Exploring Mycobacterium avium inhibition by macrocyclic compounds</p>

    <p>          David, S, Barros, V, Passos, Guerra K, and Delgado, R</p>

    <p>          Res Microbiol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16024228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16024228&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     47680   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          The unusual mitochondrial compartment of Cryptosporidium parvum</p>

    <p>          Henriquez, Fiona L, Richards, Thomas A, Roberts, Fiona, McLeod, Rima, and Roberts, Craig W</p>

    <p>          Trends in Parasitology <b>2005</b>.  21(2): 68-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     47681   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          New drugs being developed for the treatment of tuberculosis</p>

    <p>          Doggrell, SA</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(7): 917-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16022581&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16022581&amp;dopt=abstract</a> </p><br />

    <p>9.     47682   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p><b>          Methods of treating disease through the administration of a manzamine analog or derivative</b> </p>

    <p>          Hamann, Mark T, Rao, Karumanchi Venkateswara, and Peng, Jiangnan</p>

    <p>          PATENT:  US <b>2005085554</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004-26734  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   47683   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Simple 1,4-benzoquinones with antibacterial activity from stems and leaves of Gunnera perpensa</p>

    <p>          Drewes, SE, Khan, F, van Vuuren, SF, and Viljoen, AM</p>

    <p>          Phytochemistry <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16019043&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16019043&amp;dopt=abstract</a> </p><br />

    <p>11.   47684   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          In vitro and in vivo evaluation of aminopeptides inhibitors as antimicrosporidial therapies</p>

    <p>          Millership, Jason J, Didier, Elizabeth S, Okhuysen, Pablo C, Maddry, Joseph A, Kwong, Cecil D, Chen, Xi, and Snowden, Karen F</p>

    <p>          Journal of Eukaryotic Microbiology <b>2001</b>.(Suppl.): 95S-98S</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   47685   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Production of heptaene antifungal antibiotic by Streptomyces purpeofuscus CM 1261</p>

    <p>          Jain, Praveen Kumar and Jain, PC</p>

    <p>          Indian Journal of Experimental Biology <b>2005</b>.  43(4): 342-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47686   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Synthesis of schiff bases of 2-amino-5-aryl-1,3,4-oxadiazoles and their evaluation for antimicrobial activities</p>

    <p>          Mishra, Pradeep, Rajak, Harish, and Mehta, Archana</p>

    <p>          Journal of General and Applied Microbiology <b>2005</b>.  51(2): 133-141</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>14.   47687   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Achievements and challenges in antiviral drug discovery</p>

    <p>          Littler, E and Oberg, B</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(3): 155-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004079&amp;dopt=abstract</a> </p><br />

    <p>15.   47688   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Potent inhibitors of subgenomic hepatitis C virus RNA replication through optimization of indole-N-acetamide allosteric inhibitors of the viral NS5B polymerase</p>

    <p>          Harper, S, Avolio, S, Pacini, B, Di, Filippo M, Altamura, S, Tomei, L, Paonessa, G, Di, Marco S, Carfi, A, Giuliano, C, Padron, J, Bonelli, F, Migliaccio, G, De, Francesco R, Laufer, R, Rowley, M, and Narjes, F</p>

    <p>          J Med Chem <b>2005</b>.  48(14): 4547-57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999993&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999993&amp;dopt=abstract</a> </p><br />

    <p>16.   47689   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Citridones, new potentiators of antifungal miconazole activity, produced by Penicillium sp. FKI-1938: II. Structure elucidation</p>

    <p>          Fukuda, Takashi, Tomoda, Hiroshi, and Omura, Satoshi</p>

    <p>          Journal of Antibiotics <b>2005</b>.  58(5): 315-321</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   47690   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          Structural characterization and antimicrobial activity of 1,3-bis(2-benzimidazyl)-2-thiapropane ligand and its Pd(II) and Zn(II) halide complexes</p>

    <p>          Agh-Atabay, NM, Dulger, B, and Gucin, F</p>

    <p>          Eur J Med Chem  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992966&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992966&amp;dopt=abstract</a> </p><br />

    <p>18.   47691   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Citridones, new potentiators of antifungal miconazole activity, produced by Penicillium sp. FKI-1938. I. Taxonomy, fermentation, isolation and biological properties</p>

    <p>          Fukuda, Takashi, Yamaguchi, Yuichi, Masuma, Rokuro, Tomoda, Hiroshi, and Omura, Satoshi</p>

    <p>          Journal of Antibiotics <b>2005</b>.  58(5): 309-314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   47692   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Genome-wide expression profiling of the response to azole, polyene, echinocandin, and pyrimidine antifungal agents in Candida albicans</p>

    <p>          Liu, Teresa T, Lee, Robin EB, Barker, Katherine S, Lee, Richard E, Wei, Lai, Homayouni, Ramin, and Rogers, PDavid</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(6): 2226-2236</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>20.   47693   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro antimicrobial and cytotoxicity activities of 2-[(2-nitro-1-phenylalkyl)thio]benzoic acid derivatives</p>

    <p>          Goekce, Mehtap, Utku, Semra, Bercin, Erdogan, Oezcelik, Berrin, Karaoglu, Taner, and Noyanalpan, Ningur</p>

    <p>          Turkish Journal of Chemistry <b>2005</b>.  29(2): 207-217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47694   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Preparation of bis(trialkylammonium)alkane dihalides or analogs thereof as antibacterial, antiviral, parasiticidal, and antifungal agents</p>

    <p>          Widmer, Alfred Werner, Jolliffe, Katrina Anne, Wright, Lesley Catherine, and Sorrell, Tania Christine</p>

    <p>          PATENT:  WO <b>2005047230</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 94 pp.</p>

    <p>          ASSIGNEE:  (The University of Sydney, Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   47695   OI-LS-327; PUBMED-OI-7/25/2005</p>

    <p class="memofmt1-2">          In vitro interactions of micafungin with other antifungal drugs against clinical isolates of four species of Cryptococcus</p>

    <p>          Serena, C, Fernandez-Torres, B, Pastor, FJ, Trilles, L, Lazera, Mdos S, Nolard, N, and Guarro, J</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2994-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980382&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980382&amp;dopt=abstract</a> </p><br />

    <p>23.   47696   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Efficacy, plasma pharmacokinetics, and safety of icofungipen, an inhibitor of Candida isoleucyl-tRNA synthetase, in treatment of experimental disseminated candidiasis in persistently neutropenic rabbits</p>

    <p>          Petraitiene, Ruta, Petraitis, Vidmantas, Kelaher, Amy M, Sarafandi, Alia A, Mickiene, Diana, Groll, Andreas H, Sein, Tin, Bacher, John, and Walsh, Thomas J</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(5): 2084-2092</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   47697   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Oceanalin A, a hybrid alpha,omega-bifunctionalized sphingoid tetrahydroisoquinoline beta-glycoside from the marine sponge Oceanapia sp</p>

    <p>          Makarieva, TN, Denisenko, VA, Dmitrenok, PS, Guzii, AG, Santalova, EA, Stonik, VA, MacMillan, JB, and Molinski, TF</p>

    <p>          ORGANIC LETTERS <b>2005</b>.  7(14): 2897-2900, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230213100024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230213100024</a> </p><br />

    <p>25.   47698   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Sanabria, David J, Gimenez, Lizabeth, Suarez, Christian, Cruz, Clarisa, and Carballeira, Nestor M</p>

    <p>          &lt;10 Journal Title&gt; <b>2005</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>26.   47699   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Design, synthesis, and antifolate activity of new analogues of piritrexim and other diaminopyrimidine dihydrofolate reductase inhibitors with omega-carboxyalkoxy or omega-carboxy-1-alkynyl substitution in the side chain</p>

    <p>          Chan, DCM, Fu, HN, Forsch, RA, Queener, SF, and Rosowsky, A</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(13): 4420-4431, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230121800027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230121800027</a> </p><br />

    <p>27.   47700   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Facile synthesis of 2-(substituted benzylsulfanyl)benzothiazoles and their antimicrobial activity screening</p>

    <p>          Kumar, Ramanatham Vinod, Kumar, Kotha VSRSeshu, and Gopal, Kotarkonda Raja</p>

    <p>          Journal of Heterocyclic Chemistry <b>2005</b>.  42(1): 153-156</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   47701   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Identification and active expression of the Mycobacterium tuberculosis gene encoding 5-phospho-alpha-D-ribose-1-diphosphate: Decaprenyl-phosphate 5-phosphoribosyltransferase, the first enzyme committed to decaprenylphosphoryl-D-arabinose synthesis</p>

    <p>          Huang, HR, Scherman, MS, D&#39;Haeze, W, Vereecke, D, Holsters, M, Crick, DC, and McNeil, MR</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(26): 24539-24543, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230114000031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230114000031</a> </p><br />

    <p>29.   47702   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Structure and mechanism of the alkyl hydroperoxidase AhpC, a key element of the Mycobacterium tuberculosis defense system against oxidative stress</p>

    <p>          Guimaraes, BG, Souchon, H, Honore, N, Saint-Joanis, B, Brosch, R, Shepard, W, Cole, ST, and Alzari, PM</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(27): 25735-25742, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230207900054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230207900054</a> </p><br />

    <p>30.   47703   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Activity of Hoechst 33258 against Pneumocystis carinii f. sp. muris, Candida albicans, and Candida dubliniensis</p>

    <p>          Disney, Matthew D, Stephenson, Ruth, Wright, Terry W, Haidaris, Constantine G, Turner, Douglas H, and Gigliotti, Francis</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(4): 1326-1330</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   47704   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Polycyclic aromatic compounds as antimicrobial and antiviral compounds</p>

    <p>          Banik, Bimal K and Becker, Frederick F</p>

    <p>          PATENT:  WO <b>2005039507</b>  ISSUE DATE:  20050506</p>

    <p>          APPLICATION: 2004  PP: 31 pp.</p>

    <p>          ASSIGNEE:  (Board of Regents, The University of Texas System USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   47705   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Antiviral Activity of Liposomal Preparations of Antibiotic Geliomycin</p>

    <p>          Podol&#39;skaya, SV, Naryshkina, NA, Sorokoumova, GM, Kaplun, AP, Fedorova, NE, Medzhidova, AA, Kuts, AA, and Shvets, VI</p>

    <p>          Bulletin of Experimental Biology and Medicine <b>2005</b>.  139(3 ): 349-351</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   47706   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Isolation of an Antiviral Polysaccharide, Nostoflan, from a Terrestrial Cyanobacterium, Nostoc flagelliforme</p>

    <p>          Kanekiyo, Kenji, Lee, Jung-Bum, Hayashi, Kyoko, Takenaka, Hiroyuki, Hayakawa, Yumiko, Endo, Shunro, and Hayashi, Toshimitsu</p>

    <p>          Journal of Natural Products <b>2005</b>.  68(7): 1037-1041</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   47707   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Preparation of dihydroquinazolines as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Jeske, Mario, Nell, Peter, Nikolic, Susanne, Suessmeier, Frank, Zimmermann, Holger, Grosser, Rolf, Hewlett, Guy, Keldenich, Joerg, Lang, Dieter, and Henninger, Kerstin</p>

    <p>          PATENT:  WO <b>2005047278</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   47708   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Different anti-Candida activities of two human lactoferrin-derived peptides, Lfpep and kaliocin-1</p>

    <p>          Viejo-Diaz, M, Andres, MT, and Fierro, JF</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(7): 2583-2588, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230181800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230181800002</a> </p><br />

    <p>36.   47709   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          A preparation of acridone derivatives, useful as anti-herpes virus agents</p>

    <p>          Bastow, Kenneth F and Lowden, Christopher T</p>

    <p>          PATENT:  US <b>2005049273</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2003-62052  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   47710   OI-LS-327; WOS-OI-7/17/2005</p>

    <p class="memofmt1-2">          Purification and structure elucidation of compounds that inhibit the growth of mycobacteria</p>

    <p>          Veres, TA, Kubin, M, and Trischman, JA</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  229: U412-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177702507">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228177702507</a> </p><br />

    <p>38.   47711   OI-LS-327; SCIFINDER-OI-7/18/2005</p>

    <p class="memofmt1-2">          Indoles and azaindoles as inhibitors of hepatitis C virus polymerase, their preparation, pharmaceutical compositions, and use as antiviral agents</p>

    <p>          Avolio, Salvatore, Harper, Steven, Narjes, Frank, Pacini, Barbara, and Rowley, Michael</p>

    <p>          PATENT:  WO <b>2005034941</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004  PP: 41 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare p Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>39.   47712   OI-LS-327; WOS-OI-7/24/2005</p>

    <p class="memofmt1-2">          A phase 2 study to assess antiviral response, safety, and pharmacokinetics of Albuferon(TM) in IFN-OC naive subjects with genotype 1 chronic hepatitis C</p>

    <p>          Bain, V, Kaita, K, Yoshida, E, Swain, M, Heathcote, J, McHutchison, I, Neumann, A, Bagchi, P, Osborn, B, Cronin, F, Novello, L, Freimuth, W, and Subramanian, M</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  42: 9-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000019</a> </p><br />

    <p>40.   47713   OI-LS-327; WOS-OI-7/24/2005</p>

    <p class="memofmt1-2">          Peptide-aptamers as specific inhibitors of hepatitis C virus NS3 serine protease</p>

    <p>          Benhar, I, Leibenzon, A, Gal-Tanamy, M, Bachmatov, L, Zemel, R, and Tur-Kaspa, R</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  42: 10-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000021</a> </p><br />

    <p>41.   47714   OI-LS-327; WOS-OI-7/24/2005</p>

    <p class="memofmt1-2">          Small interfering RNA(siRNA)-mediated inhibition of hepatitis C virus replication using bicistronic vector in vivo (cell line Huh-7) and in vitro</p>

    <p>          Baltayiannis, G, Filis, S, Tsianos, E, and Karayiannis, P</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  42: 154-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000421">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000421</a> </p><br />

    <p>42.   47715   OI-LS-327; WOS-OI-7/24/2005</p>

    <p class="memofmt1-2">          Additive antiviral effect of artemisinin and ribavirin on an &quot;in vitro&quot; model of hepatitis C infection</p>

    <p>          Romero, MR, Serrano, MA, Vallejo, M, Alvarez, M, Efferth, T, and Marin, JJG</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  42: 166-1</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000459">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229024000459</a> </p><br />

    <p>43.   47716   OI-LS-327; WOS-OI-7/24/2005</p>

    <p class="memofmt1-2">          Biologically active fluorescent farnesol analogs</p>

    <p>          Shchepin, R, Dumitru, R, Nickerson, KW, Lund, M, and Dussault, PH</p>

    <p>          CHEMISTRY &amp; BIOLOGY <b>2005</b>.  12(6): 639-641, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230213600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230213600007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
