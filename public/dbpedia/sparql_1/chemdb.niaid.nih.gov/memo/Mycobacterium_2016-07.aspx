

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="U7x02mCfjZgE1rjM0L7wQuxd73yE3Nt8dnOLFID1ZntOfJBGOp6y4kHObsGPfQrh8B11YorXc9+wVJMBVKdRote76fvrQBkFaUe0e6/w3AjJ04kbPo3e0nQBK7Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F936ACDD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: July, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27193014">Anti-dormant Mycobacterial Activity and Target Molecule of Melophlins, Tetramic acid Derivatives Isolated from a Marine Sponge of Melophlus Sp.</a> Arai, M., Y. Yamano, K. Kamiya, A. Setiawan, and M. Kobayashi. Journal of Natural Medicines, 2016. 70(3): p. 467-475. PMID[27193014]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27241518">Total Synthesis of Cyclomarins A, C and D, Marine Cyclic Peptides with Interesting Anti-tuberculosis and Anti-malaria Activities.</a> Barbie, P. and U. Kazmaier. Organic &amp; Biomolecular Chemistry, 2016. 14(25): p. 6036-6054. PMID[27241518]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27379713">Inhibitors of the Cysteine Synthase CysM with Antibacterial Potency against Dormant Mycobacterium tuberculosis.</a> Brunner, K., S. Maric, R.S. Reshma, H. Almqvist, B. Seashore-Ludlow, A.L. Gustavsson, O. Poyraz, P. Yogeeswari, T. Lundback, M. Vallin, D. Sriram, R. Schnell, and G. Schneider. Journal of Medicinal Chemistry, 2016. 59(14): p. 6848-6859. PMID[27379713]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27270287">Pyrazinamide Is Active against Mycobacterium tuberculosis Cultures at Neutral pH and Low Temperature.</a> den Hertog, A.L., S. Menting, R. Pfeltz, M. Warns, S.H. Siddiqi, and R.M. Anthony. Antimicrobial Agents Chemotherapy, 2016. 60(8): p. 4956-4960. PMID[27270287]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26919556">Acyldepsipeptide Antibiotics Kill Mycobacteria by Preventing the Physiological Functions of the ClpP1P2 Protease.</a> Famulla, K., P. Sass, I. Malik, T. Akopian, O. Kandror, M. Alber, B. Hinzen, H. Ruebsamen-Schaeff, R. Kalscheuer, A.L. Goldberg, and H. Brotz-Oesterhelt. Molecular Microbiology, 2016. 101(2): p. 194-209. PMID[26919556]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27384553">Design and Stereochemical Research (DFT, ECD and Crystal Structure) of Novel Bedaquiline Analogs as Potent Antituberculosis Agents.</a> Geng, Y., L. Li, C. Wu, Y. Chi, Z. Li, W. Xu, and T. Sun. Molecules, 2016. 21(7): E875. PMID[27384553]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27144688">Novel Cephalosporins Selectively Active on Nonreplicating Mycobacterium tuberculosis.</a> Gold, B., R. Smith, Q. Nguyen, J. Roberts, Y. Ling, L. Lopez Quezada, S. Somersan, T. Warrier, D. Little, M. Pingle, D. Zhang, E. Ballinger, M. Zimmerman, V. Dartois, P. Hanson, L.A. Mitscher, P. Porubsky, S. Rogers, F.J. Schoenen, C. Nathan, and J. Aube. Journal of Medicinal Chemistry, 2016. 59(13): p. 6027-6044. PMID[27144688]. PMCID[PMC4947980].<b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27437078">Dehydrozingerone Inspired Styryl hydrazine thiazole Hybrids as Promising Class of Antimycobacterial Agents.</a> Hampannavar, G.A., R. Karpoormath, M.B. Palkar, M.S. Shaikh, and B. Chandrasekaran. ACS Medicinal Chemistry Letters, 2016. 7(7): p. 686-691. PMID[27437078]. PMCID[PMC4948005].<b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27216051">Imidazo[1,2-a]pyridine-3-carboxamides Are Active Antimicrobial Agents against Mycobacterium avium Infection in Vivo.</a> Moraski, G.C., Y. Cheng, S. Cho, J.W. Cramer, A. Godfrey, T. Masquelin, S.G. Franzblau, M.J. Miller, and J. Schorey. Antimicrobial Agents Chemotherapy, 2016. 60(8): p. 5018-5022. PMID[27216051]. PMCID[PMC4958206].<b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27174081">Antimycobacterial Triterpenes from the Canadian Medicinal Plant Sarracenia purpurea.</a> Morrison, S.A., H. Li, D. Webster, J.A. Johnson, and C.A. Gray. Journal of Ethnopharmacology, 2016. 188: p. 200-203. PMID[27174081]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27348630">Searching for New Leads for Tuberculosis: Design, Synthesis, and Biological Evaluation of Novel 2-Quinolin-4-yloxyacetamides.</a> Pitta, E., M.K. Rogacki, O. Balabon, S. Huss, F. Cunningham, E.M. Lopez-Roman, J. Joossens, K. Augustyns, L. Ballell, R.H. Bates, and P. Van der Veken. Journal of Medicinal Chemistry, 2016. 59(14): p. 6709-6728. PMID[27348630]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27184765">Synthesis and Antitubercular Evaluation of Novel Dibenzo[b,d]thiophene Tethered Imidazo[1,2-a]pyridine-3-carboxamides.</a> Pulipati, L., J.P. Sridevi, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(13): p. 3135-3140. PMID[27184765]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27449999">Antimycobacterial Activity of Pyrazinoate Prodrugs in Replicating and Non-replicating Mycobacterium tuberculosis.</a> Segretti, N.D., C.K. Simoes, M.F. Correa, V.M. Felli, M. Miyata, S.H. Cho, S.G. Franzblau, and J.P. Fernandes. Tuberculosis, 2016. 99: p. 11-16. PMID[27449999]. <b><br />
    [PubMed]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000377959900117">In Vitro Interactions between R207910 and Second-line anti-TB Drugs or BTZ043 against Mycobacterium tuberculosis by Microplate Alamar Blue Assay.</a> Xu, Z.Q., W. Peng, K.L. Wan, C.K. Luo, H. Zeng, P.H. Zhang, Z. Liu, Y.P. Zhang, and X.Y. Wang. International Journal of Clinical and Experimental Medicine, 2016. 9(3): p. 6336-6341. ISI[000377959900117].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_07_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">15. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160721&amp;CC=WO&amp;NR=2016112447A1&amp;KC=A1">&#913;-Ketoacyl Isoniazid Compounds, Method for Producing These Compounds, Use of the Compounds for the Treatment of Tuberculosis.</a> Boechat, N., E.C. De Lima, and F.S.C. Branco. Patent. 2016. 2016-BR4 2016112447: 27pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_07_2016.</p>

    <br />

    <p class="plaintext">16. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160707&amp;CC=WO&amp;NR=2016108249A1&amp;KC=A1">Preparation of 1,2,4-Triazole 1,3,4-Oxadiazole, and 1,3,4-Thiadiazole Derivatives and Their Antibacterial Activity.</a> Sarkar, D., R.A. Joshi, A.P. Likhite, R.R. Joshi, and V.M. Khedkar. Patent. 2016. 2015-IN50221 2016108249: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_07_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
