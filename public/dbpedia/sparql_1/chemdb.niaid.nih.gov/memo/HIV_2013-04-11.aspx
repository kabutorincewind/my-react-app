

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-04-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="w02zrfzuny2h72rO/FnIi6Yj/8Ohi+TE45ZXJJrX86mcKVtOJGfkMnXCMuVsMmxN7iLG8a9qwC9bvslvdOHvvuUMJjj5Rm+++RI8uJw+G24Z1QuztozKTi12Z0M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="844A0BF2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 29 - April 11, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23563315">Natural Variation in Fc Glycosylation of HIV-specific Antibodies Impacts Antiviral Activity.</a> Ackerman, M.E., M. Crispin, X. Yu, K. Baruah, A.W. Boesch, D.J. Harvey, A.S. Dugast, E.L. Heizen, A. Ercan, I. Choi, H. Streeck, P.A. Nigrovic, C. Bailey-Kellogg, C. Scanlan, and G. Alter. The Journal of Clinical Investigation, 2013. <b>[Epub ahead of print]</b>. PMID[23563315].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23464580">Inhibition of HIV Type 1 Replication by Human T Lymphotropic Virus Types 1 and 2 Tax Proteins in Vitro.</a> Barrios, C.S., L. Castillo, C.Z. Giam, L. Wu, and M.A. Beilke. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[23464580].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23376165">Characteristics of IFITM, the Newly Identified IFN-inducible anti-HIV-1 Family Proteins.</a> Chutiwitoonchai, N., M. Hiyoshi, Y. Hiyoshi-Yoshidomi, M. Hashimoto, K. Tokunaga, and S. Suzu. Microbes and Infection, 2013. 15(4): p. 280-290. PMID[23376165].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23399723">Synthesis and Biological Evaluation of a New Derivative of Bevirimat That Targets the Gag CA-SP1 Cleavage Site.</a> Coric, P., S. Turcaud, F. Souquet, L. Briant, B. Gay, J. Royer, N. Chazal, and S. Bouaziz. European Journal of Medicinal Chemistry, 2013. 62: p. 453-465. PMID[23399723].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23569210">Inhibition of HIV-1 Transcription and Replication by a Newly Identified Cyclin T1 Splice Variant.</a> Gao, G., X. Wu, J. Zhou, M. He, J.J. He, and D. Guo. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23569210].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23562054">Anti-HIV Efficacy and Biodistribution of Nucleoside Reverse Transcriptase Inhibitors Delivered as Squalenoylated Prodrug Nanoassemblies.</a> Hillaireau, H., N. Dereuddre-Bosquet, R. Skanji, F. Bekkara-Aounallah, J. Caron, S. Lepetre, S. Argote, L. Bauduin, R. Yousfi, C. Rogez-Kreuz, D. Desmaele, B. Rousseau, R. Gref, K. Andrieux, P. Clayette, and P. Couvreur. Biomaterials, 2013. <b>[Epub ahead of print]</b>. PMID[23562054].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23565220">Conjugation of Cholesterol to HIV-1 Fusion Inhibitor C34 Increases Peptide-membrane Interactions Potentiating Its Action.</a> Hollmann, A., P.M. Matos, M.T. Augusto, M.A. Castanho, and N.C. Santos. PloS One, 2013. 8(4): p. e60302. PMID[23565220].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23317497">Recent Progress in Biological Activities and Synthetic Methodologies of Pyrroloquinoxalines.</a> Huang, A. and C. Ma. Mini Reviews in Medicinal Chemistry, 2013. 13(4): p. 607-616. PMID[23317497].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23566519">Discovery of a New HIV-1 Inhibitor Scaffold and Synthesis of Potential Prodrugs of Indazoles.</a> Kim, S.H., B. Markovitz, R. Trovato, B.R. Murphy, H. Austin, A.J. Willardsen, V. Baichwal, S. Morham, and A. Bajji. Bioorganic &amp; Medicinal Chemistry Letters, 2013. <b>[Epub ahead of print]</b>. PMID[23566519].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23550966">Cytotoxic, Antitopoisomerase II-alpha, and anti-HIV-1 Activities of Triterpenoids Isolated from Leaves and Twigs of Gardenia carinata.</a> Kongkum, N., P. Tuchinda, M. Pohmakotr, V. Reutrakul, P. Piyachaturawat, S. Jariyawat, K. Suksen, R. Akkarawongsapat, J. Kasisit, and C. Napaswad. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23550966].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23555255">HIV Restriction by APOBEC3 in Humanized Mice.</a> Krisko, J.F., F. Martinez-Torres, J.L. Foster, and J.V. Garcia. PLoS Pathogens, 2013. 9(3): p. e1003242. PMID[23555255].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23540737">Novel Pyridinone Derivatives as Non-nucleoside Reverse Transcriptase Inhibitors (NNRTIs) with High Potency against NNRTI-resistant HIV-1 Strains.</a> Li, A., Y. Ouyang, Z. Wang, Y. Cao, X. Liu, L. Ran, C. Li, L. Li, L. Zhang, K. Qiao, W. Xu, Y. Huang, Z. Zhang, C. Tian, Z. Liu, S. Jiang, Y. Shao, Y. Du, L. Ma, X. Wang, and J. Liu. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23540737].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23415090">Synthesis and Biological Evaluation of Pyridazine Derivatives as Novel HIV-1 NNRTIs.</a> Li, D., P. Zhan, H. Liu, C. Pannecouque, J. Balzarini, E. De Clercq, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(7): p. 2128-2134. PMID[23415090].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23419738">Efficient Synthesis of 3H,3&#39;H-spiro[Benzofuran-2,1&#39;-isobenzofuran]-3,3&#39;-dione as Novel Skeletons Specifically for Influenza Virus Type B Inhibition.</a> Malpani, Y., R. Achary, S.Y. Kim, H.C. Jeong, P. Kim, S.B. Han, M. Kim, C.K. Lee, J.N. Kim, and Y.S. Jung. European Journal of Medicinal Chemistry, 2013. 62: p. 534-544. PMID[23419738].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23542011">D316 Is Critical for the Enzymatic Activity and HIV-1 Restriction Potential of Human and Rhesus APOBEC3B.</a> McDougle, R.M., J.F. Hultquist, A.C. Stabell, S.L. Sawyer, and R.S. Harris. Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23542011].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23288422">Chimpanzee GB Virus C and GB Virus A E2 Envelope Glycoproteins Contain a Peptide Motif That Inhibits Human Immunodeficiency Virus Type 1 Replication in Human CD4+ T-cells.</a> McLinden, J.H., J.T. Stapleton, D. Klinzman, K.K. Murthy, Q. Chang, T.M. Kaufman, N. Bhattarai, and J. Xiang. The Journal of General Virology, 2013. 94(Pt 4): p. 774-782. PMID[23288422].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23403297">Design and Synthesis of Biotin- or Alkyne-conjugated Photoaffinity Probes for Studying the Target Molecules of PD 404182.</a> Mizuhara, T., S. Oishi, H. Ohno, K. Shimura, M. Matsuoka, and N. Fujii. Bioorganic &amp; Medicinal Chemistry, 2013. 21(7): p. 2079-2087. PMID[23403297].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23442049">Sustained Inhibition of HIV-1 Replication by Conditional Expression of the E. coli-Derived Endoribonuclease MazF in CD4+ T Cells.</a> Okamoto, M., H. Chono, Y. Kawano, N. Saito, H. Tsuda, K. Inoue, I. Kato, J. Mineno, and M. Baba. Human Gene Therapy Methods, 2013. <b>[Epub ahead of print]</b>. PMID[23442049].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23415084">Synthesis and Evaluation of Coumarin Derivatives as Potential Dual-action HIV-1 Protease and Reverse Transcriptase Inhibitors.</a> Olomola, T.O., R. Klein, N. Mautsa, Y. Sayed, and P.T. Kaye. Bioorganic &amp; Medicinal Chemistry, 2013. 21(7): p. 1964-1971. PMID[23415084].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23545531">Identification and Characterization of a Novel HIV-1 Nucleotide Competing RT Inhibitor Series.</a> Rajotte, D., S. Tremblay, A. Pelletier, P. Salois, L. Bourgon, R. Coulombe, S. Mason, L. Lamorte, C. Sturino, and R. Bethell. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23545531].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23566198">Side Chain Requirements for Affinity and Specificity in D5, an HIV-1 Antibody Derived from the VH1-69 Germline Segment.</a> Stewart, A., J.S. Harrison, L.K. Regula, and J.R. Lai. BMC Biochemistry, 2013. 14(1): p. 9. PMID[23566198].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23567268">Expression, Purification and Identification of CtCVNH, a Novel anti-HIV (Human Immunodeficiency Virus) Protein from Ceratopteris thalictroides.</a> Sun, J., Y. Su, and T. Wang. International Journal of Molecular Sciences, 2013. 14(4): p. 7506-7514. PMID[23567268].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23540839">Small Molecule Regulation of Protein Conformation by Binding in the Flap of HIV Protease.</a> Tiefenbrunn, T., S. Forli, M.M. Baksh, M.W. Chang, M. Happer, Y.C. Lin, A.L. Perryman, J.K. Rhee, B.E. Torbett, A.J. Olson, J.H. Elder, M.G. Finn, and C.D. Stout. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23540839].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23434230">Evaluation of HIV-1 Inhibition by Stereoisomers and Analogues of the Sesquiterpenoid Hydroquinone Peyssonol A.</a> Treitler, D.S., Z. Li, M. Krystal, N.A. Meanwell, and S.A. Snyder. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2192-2196. PMID[23434230].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23417900">Selective Targeting of Dendritic Cell-specific Intercellular Adhesion Molecule-3-grabbing Nonintegrin (DC-SIGN) with Mannose-based Glycomimetics: Synthesis and Interaction Studies of Bis(benzylamide) Derivatives of a Pseudomannobioside.</a> Varga, N., I. Sutkeviciute, C. Guzzi, J. McGeagh, I. Petit-Haertlein, S. Gugliotta, J. Weiser, J. Angulo, F. Fieschi, and A. Bernardi. Chemistry, 2013. 19(15): p. 4786-4797. PMID[23417900].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23434419">Improved Guanide Compounds Which Bind the CXCR4 Co-receptor and Inhibit HIV-1 Infection.</a> Wilkinson, R.A., S.H. Pincus, K. Song, J.B. Shepard, A.J. Weaver, Jr., M.E. Labib, and M. Teintze. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2197-2201. PMID[23434419].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23555254">Digoxin Suppresses HIV-1 Replication by Altering Viral RNA Processing.</a> Wong, R.W., A. Balachandran, M.A. Ostrowski, and A. Cochrane. PLoS Pathogens, 2013. 9(3): p. e1003241. PMID[23555254].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23432118">Sensitive and Continuous Screening of Inhibitors of beta-Site Amyloid Precursor Protein Cleaving Enzyme 1 (BACE1) at Single SPR Chips.</a> Yi, X., Y. Hao, N. Xia, J. Wang, M. Quintero, D. Li, and F. Zhou. Analytical Chemistry, 2013. 85(7): p. 3660-3666. PMID[23432118].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23563857">Protostane and Fusidane Triterpenes: A Mini-review.</a> Zhao, M., T. Godecke, J. Gunn, J.A. Duan, and C.T. Che. Molecules, 2013. 18(4): p. 4054-4080. PMID[23563857].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23539429">Investigation of the Pharmacokinetic Interaction between Ritonavir and CMDCK, a New Non-nucleoside Reverse Transcriptase Inhibitor.</a> Zhuang, X.M., G.L. Shen, M. Yuan, and H. Li. Drug Research, 2013. <b>[Epub ahead of print]</b>. PMID[23539429].
    <br />
    <b>[PubMed]</b>. HIV_0329-041113.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315352400022">Compartmentalization and Antiviral Effect of Efavirenz Metabolites in Blood Plasma, Seminal Plasma, and Cerebrospinal Fluid.</a> Avery, L.B., J.L. VanAusdall, C.W. Hendrix, and N.N. Bumpus. Drug Metabolism and Disposition, 2013. 41(2): p. 422-429. ISI[000315352400022].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315607300015">Prostratin Exhibits Both Replication Enhancing and Inhibiting Effects on FIV Infection of Feline CD4(+) T-cells.</a> Chan, C.N., E.L. McMonagle, M.J. Hosie, and B.J. Willett. Virus Research, 2013. 171(1): p. 121-128. ISI[000315607300015].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315400600073">Neglschisandrins E-F: Two New Lignans and Related Cytotoxic Lignans from Schisandra neglecta.</a> Chen, M., X.M. Xu, B. Xu, P.P. Yang, Z.H. Liao, S.L. Morris-Natschke, K.H. Lee, and D.F. Chen. Molecules, 2013. 18(2): p. 2297-2306. ISI[000315400600073].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316036400007">The LEDGF/p75 Integrase Interaction, a Novel Target for anti-HIV Therapy (Vol 435, Pg 102, 2013).</a> Christ, F. and Z. Debyser. Virology, 2013. 438(1): p. 50-50. ISI[000316036400007].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315997000008">IL-27 Inhibits HIV-1 Infection in Human Macrophages by Down-regulating Host Factor SPTBN1 During Monocyte to Macrophage Differentiation.</a> Dai, L., K.B. Lidie, Q. Chen, J.W. Adelsberger, X. Zheng, D.W. Huang, J. Yang, R.A. Lempicki, T. Rehman, R.L. Dewar, Y.M. Wang, R.L. Hornung, K.A. Canizales, S.J. Lockett, H.C. Lane, and T. Imamichi. Journal of Experimental Medicine, 2013. 210(3): p. 517-534. ISI[000315997000008].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315352400029">The Potential of Sutherlandia frutescens for Herb-drug Interaction.</a> Fasinu, P.S., H. Gutmann, H. Schiller, A.D. James, P.J. Bouic, and B. Rosenkranz. Drug Metabolism and Disposition, 2013. 41(2): p. 488-497. ISI[000315352400029].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />
    
    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315820700006">HIV-1 Viral Infectivity Factor (Vif) Alters Processive Single-stranded DNA Scanning of the Retroviral Restriction Factor APOBEC3G.</a> Feng, Y.Q., R.P. Love, and L. Chelico. Journal of Biological Chemistry, 2013. 288(9): p. 6083-6094. ISI[000315820700006].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315325300021">Insights into the Mechanism of Drug Resistance: X-ray Structure Analysis of Multi-drug Resistant HIV-1 Protease Ritonavir Complex.</a> Liu, Z.G., R.S. Yedidi, Y. Wang, T.G. Dewdney, S.J. Reiter, J.S. Brunzelle, I.A. Kovari, and L.C. Kovari. Biochemical and Biophysical Research Communications, 2013. 431(2): p. 232-238. ISI[000315325300021].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315591500008">d(GGGT)(4) and r(GGGU)(4) Are Both HIV-1 Inhibitors and Interleukin-6 Receptor Aptamers.</a> Magbanua, E., T. Zivkovic, B. Hansen, N. Beschorner, C. Meyer, I. Lorenzen, J. Grotzinger, J. Hauber, A.E. Torda, G. Mayer, S. Rose-John, and U. Hahn. RNA Biology, 2013. 10(2): p. 216-227. ISI[000315591500008].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315373000031">Structure of a Glycomimetic Ligand in the Carbohydrate Recognition Domain of C-type Lectin DC-SIGN. Structural Requirements for Selectivity and Ligand Design.</a> Thepaut, M., C. Guzzi, I. Sutkeviciute, S. Sattin, R. Ribeiro-Viana, N. Varga, E. Chabrol, J. Rojo, A. Bernardi, J. Angulo, P.M. Nieto, and F. Fieschi. Journal of the American Chemical Society, 2013. 135(7): p. 2518-2529. ISI[000315373000031].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315414300005">Evaluation of a 13-Hexyl-berberine Hydrochloride Topical Gel Formulation.</a> Wei, H.L., S.Q. Wang, F. Xu, L.F. Xu, J.R. Zheng, and Y. Chen. Drug Development and Industrial Pharmacy, 2013. 39(4): p. 534-539. ISI[000315414300005].
    <br />
    <b>[WOS]</b>. HIV_0329-041113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
