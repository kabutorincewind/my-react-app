

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="I2BQlicgunKUGSpIgQdQRdxXO1ZlhHBgLink0p7QFvm/6can/YXTzh3TEtzO7zhV42vDDFFGcOiBFVGryTAtlliGsozVmO0hMtmc7f4MVnpcocjuYLxc5Ryq4xU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1C2A985F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: November, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29188776">Metal Complexes of Isonicotinylhydrazide and Their Antitubercular Activity.</a> Ali, M., M. Ahmed, S. Hafiz, M. Kamal, M. Mumtaz, M. Hanif, and K.M. Khan. Pakistan Journal of Pharmaceutical Sciences, 2017. 30(6(Supplementary)): p. 2399-2403. PMID[29188776].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414497800044">Synthesis, Crystal Structure and Antimycobacterial Activities of 4-Indolyl-1,4-dihydropyridine Derivatives Possessing Various Ester Groups.</a> Baydar, E., M.G. Gunduz, V.S. Krishna, R. Simsek, D. Sriram, S.O. Yildirim, R.J. Butcher, and C. Safak. Research on Chemical Intermediates, 2017. 43(12): p. 7471-7489. ISI[000414497800044].
    <br />
    <b>[WOS]</b>. TB_11_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28094223">Anti-tuberculosis Activity and Structure-Activity Relationships of Oxygenated Tricyclic Carbazole Alkaloids and Synthetic Derivatives.</a> Borger, C., C. Brutting, K.K. Julich-Gruner, R. Hesse, V.P. Kumar, S.K. Kutz, M. Ronnefahrt, C. Thomas, B. Wan, S.G. Franzblau, and H.J. Knolker. Bioorganic &amp; Medicinal Chemistry, 2017. 25(22): p. 6167-6174. PMID[28094223].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28964936">Natural Isoflavone Biochanin A as a Template for the Design of New and Potent 3-Phenylquinolone Efflux Inhibitors against Mycobacterium avium.</a> Cannalire, R., D. Machado, T. Felicetti, S.S. Costa, S. Massari, G. Manfroni, M.L. Barreca, O. Tabarrini, I. Couto, M. Viveiros, S. Sabatini, and V. Cecchetti. European Journal of Medicinal Chemistry, 2017. 140: p. 321-330. PMID[28964936].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28840414">Bis-spirochromanones as Potent Inhibitors of Mycobacterium tuberculosis: Synthesis and Biological Evaluation.</a> Dongamanti, A., V.K. Aamate, M.G. Devulapally, S. Gundu, S. Balabadra, V. Manga, P. Yogeeswari, D. Sriram, and S. Balasubramanian. Molecular Diversity, 2017. 21(4): p. 999-1010. PMID[28840414].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29189762">Synthesis and in Vitro Antimycobacterial Activity of Novel N-Arylpiperazines Containing an Ethane-1,2-diyl Connecting Chain.</a> Gonec, T., I. Malik, J. Csollei, J. Jampilek, J. Stolarikova, I. Solovic, P. Mikus, S. Keltosova, P. Kollar, J. O&#39;Mahony, and A. Coffey. Molecules, 2017. 22(12): E2100. PMID[29189762].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28893784">Dual Mechanism of Action of 5-Nitro-1,10-phenanthroline against Mycobacterium tuberculosis.</a> Kidwai, S., C.Y. Park, S. Mawatwal, P. Tiwari, M.G. Jung, T.P. Gosain, P. Kumar, D. Alland, S. Kumar, A. Bajaj, Y.K. Hwang, C.S. Song, R. Dhiman, I.Y. Lee, and R. Singh. Antimicrobial Agents and Chemotherapy, 2017. 61(11): e00969-17. PMID[28893784]. PMCID[PMC5655107].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28870005">In Vitro Effect of DFC-2 on Mycolic acid Biosynthesis in Mycobacterium tuberculosis.</a> Kim, S., H. Seo, H.A. Mahmud, M.I. Islam, Y.S. Kim, J. Lyu, K.W. Nam, B.E. Lee, K.I. Lee, and H.Y. Song. Journal of Microbiology and Biotechnology, 2017. 27(11): p. 1932-1941. PMID[28870005].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000413459100031">Synthesis and Antimycobacterial Activity of N-Isonicotinoyl-N&#39;-alkylideneferrocenecarbohydrazides.</a> Kulikov, V.N., R.S. Nikulin, A.N. Rodionov, E.S. Babusenko, V.N. Babin, L.V. Kovalenko, and Y.A. Belousov. Russian Chemical Bulletin, 2017. 66(6): p. 1122-1125. ISI[000413459100031].
    <br />
    <b>[WOS]</b>. TB_11_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29163906">Biosynthesis of Methyl-proline Containing Griselimycins, Natural Products with Anti-tuberculosis Activity.</a> Lukat, P., Y. Katsuyama, S. Wenzel, T. Binz, C. Konig, W. Blankenfeldt, M. Bronstrup, and R. Muller. Chemical Science, 2017. 8(11): p. 7521-7527. PMID[29163906]. PMCID[PMC5676206].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28993106">The Antitubercular Activity of Various Nitro(triazole/imidazole)-based Compounds.</a> Papadopoulou, M.V., W.D. Bloomer, and H.S. Rosenzweig. Bioorganic &amp; Medicinal Chemistry, 2017. 25(21): p. 6039-6048. PMID[28993106].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29107814">Synthesis of Substrate Analogues as Potential Inhibitors for Mycobacterium tuberculosis Enzyme MshC.</a> Patel, K., F. Song, and P.R. Andreana. Carbohydrate Research, 2017. 453-454: p. 10-18. PMID[29107814].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000415333600013">The Synthesis, Biological Evaluation and Structure-Activity Relationship of 2-Phenylaminomethylene-cyclohexane-1,3-diones as Specific Antituberculosis Agents.</a> Rather, M.A., A.M. Lone, B. Teli, Z.S. Bhat, P. Singh, M. Maqbool, B.A. Shairgojray, M.J. Dar, S. Amin, S.K. Yousuf, B.A. Bhat, and Z. Ahmad. MedChemComm, 2017. 8(11): p. 2133-2141. ISI[000415333600013].
    <br />
    <b>[WOS]</b>. TB_11_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000413387300056">Evaluation of Antituberculosis Activity and DFT Study on Dipyrromethane-derived Hydrazone Derivatives.</a> Rawat, P., R.N. Singh, P. Niranjan, A. Ranjan, and N.R.F. Holguin. Journal of Molecular Structure, 2017. 1149: p. 539-548. ISI[000413387300056].
    <br />
    <b>[WOS]</b>. TB_11_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28835350">Development of Water-soluble 3,5-Dinitrophenyl Tetrazole and Oxadiazole Antitubercular Agents.</a> Roh, J., G. Karabanovich, H. Vlckova, A. Carazo, J. Nemecek, P. Sychra, L. Valaskova, O. Pavlis, J. Stolarikova, V. Klimesova, K. Vavrova, P. Pavek, and A. Hrabalek. Bioorganic &amp; Medicinal Chemistry, 2017. 25(20): p. 5468-5476. PMIDI[28835350].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28527650">New 1-Hydroxy-2-thiopyridine Derivatives Active against Both Replicating and Dormant Mycobacterium tuberculosis.</a> Salina, E.G., O. Ryabova, A. Vocat, B. Nikonenko, S.T. Cole, and V. Makarov. Journal of Infection and Chemotherapy, 2017. 23(11): p. 794-797. PMID[28527650].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29151652">Antimicrobial Efficacy of Synthetic Pyranochromenones and (Coumarinyloxy)acetamides.</a> Singh, A.K., S. Prasad, B. Kumar, S. Kumar, A. Anand, S.S. Kamble, S.K. Sharma, and H.K. Gautam. Indian Journal of Microbiology, 2017. 57(4): p. 499-502. PMID[29151652]. PMCID[PMC5671431].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28961864">Biological Evaluation of Diphenyleneiodonium chloride (DPIC) as a Potential Drug Candidate for Treatment of Non-tuberculous Mycobacterial Infections.</a> Singh, A.K., R. Thakare, P. Karaulia, S. Das, I. Soni, M. Pandey, A.K. Pandey, S. Chopra, and A. Dasgupta. Journal of Antimicrobial Chemotherapy, 2017. 72(11): p. 3117-3121. PMID[28961864].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29035081">Biological Evaluation of Novel Curcumin-pyrazole-mannich Derivative Active against Drug-resistant Mycobacterium tuberculosis.</a> Singh, A.K., P. Yadav, P. Karaulia, V.K. Singh, P. Gupta, S.K. Puttrevu, S. Chauhan, R.S. Bhatta, N. Tadigoppula, U.D. Gupta, S. Chopra, and A. Dasgupta. Future Microbiology, 2017. 12: p. 1349-1362. PMID[29035081].
    <br />
    <b>[PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29093335">Novel 6-Phenylnicotinohydrazide Derivatives: Design, Synthesis and Biological Evaluation as a Novel Class of Antitubercular and Antimicrobial Agents.</a> Soliman, D.H., W.M. Eldehna, H.A. Ghabbour, M.M. Kabil, M.M. Abdel-Aziz, and H.A.K. Abdel-Azizh. Biological &amp; Pharmaceutical Bulletin, 2017. 40(11): p. 1883-1893. PMID[29093335]. <b><br />
    [PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414497800040">Novel 2-Methyl-6-arylpyridines Carrying Active Pharmacophore 4,5-Dihydro 2-Pyrazolines: Synthesis, Antidepressant, and Anti-tuberculosis Evaluation.</a> Sowmya, P.V., B. Poojary, B.C. Revanasiddappa, M. Vijayakumar, P. Nikil, and V. Kumar. Research on Chemical Intermediates, 2017. 43(12): p. 7399-7422. ISI[000414497800040]. <b><br />
    [WOS]</b>. TB_11_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28951604">Identification of a Novel Class of Small Compounds with Anti-tuberculosis Activity by in Silico Structure-based Drug Screening.</a> Taira, J., K. Morita, S. Kawashima, T. Umei, H. Baba, T. Maruoka, H. Komatsu, H. Sakamoto, J.C. Sacchettini, and S. Aoki. The Journal of Antibiotics, 2017. 70(11): p. 1057-1064. PMID[28951604]. <b><br />
    [PubMed]</b>. TB_11_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28830789">New Amide and Dioxopiperazine Derivatives from Leaves of Breynia nivosa.</a> Umeokoli, B.O., F.A. Onyegbule, F.B.C. Okoye, H. Wang, R. Kalscheuer, W.E.G. Muller, R. Hartmann, Z. Liu, and P. Proksch. Fitoterapia, 2017. 122: p. 16-19. PMID[28830789]. <b><br />
    [PubMed]</b>. TB_11_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">24. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171102&amp;CC=WO&amp;NR=2017190043A1&amp;KC=A1">Preparation of Cephalosporin-type Compounds for the Treatment of Bacterial and Fungal Infections.</a> Gold, B.S., J. Aube, C.F. Nathan, Q. Nguyen, F.J. Schoenen, and R.A. Smith. Patent. 2017. 2017-US30177 2017190043: 101pp.
    <br />
    <b>[Patent]</b>. TB_11_2017.</p>

    <br />

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171102&amp;CC=WO&amp;NR=2017190034A1&amp;KC=A1">Synthesis of Azasteroids for Treatment of Tuberculosis.</a> Sampson, N., X. Yang, and T. Yuan. Patent. 2017. 2017-US30166 2017190034: 115pp.
    <br />
    <b>[Patent]</b>. TB_11_2017.</p>

    <br />

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171026&amp;CC=WO&amp;NR=2017184947A1&amp;KC=A1">Preparation of Anti-infective 2-Aminothiophenes.</a> Sucheck, S., S. Thanna, and R. Slayden. Patent. 2017. 2017-US28782 2017184947: 131pp.
    <br />
    <b>[Patent]</b>. TB_11_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
