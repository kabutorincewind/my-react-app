

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-10-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AIYBAo9skOxvoeBG3qciwQkkgcSsjYcU5YkPywnSWJ+P3Vi4F3AagAJNZ7L7l9PVhxdPA8DMlfDLBAdnWU+5Xal9g3oS3d1GkIKfSJqHY8miagBnmp/Qk5z1ptU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E570A678" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: October 10 - October 23, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25293523">Antimicrobial Activity, Toxicity and Selectivity Index of Two Biflavonoids and a Flavone Isolated from Podocarpus henkelii (Podocarpaceae) Leaves.</a> Bagla, V.P., L.J. McGaw, E.E. Elgorashi, and J.N. Eloff. BMC Complementary and Alternative Medicine, 2014. 14(383): 6 pp. PMID[25293523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25302492">9-O-Butyl-13-(4-isopropylbenzyl)berberine, KR-72, is a Potent Antifungal Agent that Inhibits the Growth of Cryptococcus neoformans by Regulating Gene Expression.</a> Bang, S., H. Kwon, H.S. Hwang, K.D. Park, S.U. Kim, and Y.S. Bahn. PloS One, 2014. 9(10): p. e109863. PMID[25302492].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25310766">Suvanine Sesterterpenes from a Tropical Sponge Coscinoderma sp. Inhibit Isocitrate Lyase in the Glyoxylate Cycle.</a> Lee, S.H., T.H. Won, H. Kim, C.H. Ahn, J. Shin, and K.B. Oh. Marine Drugs, 2014. 12(10): p. 5148-5159. PMID[25310766].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25329706">Antimicrobial Activity of Peptides Derived from Olive Flounder Lipopolysaccharide Binding Protein/Bactericidal Permeability-increasing Protein (LBP/BPI).</a> Nam, B.H., J.Y. Moon, E.H. Park, Y.O. Kim, D.G. Kim, H.J. Kong, W.J. Kim, Y.J. Jee, C.M. An, N.G. Park, and J.K. Seo. Marine Drugs, 2014. 12(10): p. 5240-5257. PMID[25329706].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25310152">Exploration of Piperidinols as Potential Antitubercular Agents.</a> Abuhammad, A., E. Fullam, S. Bhakta, A.J. Russell, G.M. Morris, P.W. Finn, and E. Sim. Molecules, 2014. 19(10): p. 16274-16290. PMID[25310152].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25324128">Novel Drugs and Drug Combinations for Treating Tuberculosis.</a> Munang, M.L., M.K. O&#39;Shea, and M. Dedicoat. BMJ, 2014. 349: p. g5948. PMID[25324128].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25137149">Synthesis of a Poly-hydroxypyrolidine-based Inhibitor of Mycobacterium tuberculosis GlgE.</a> Veleti, S.K., J.J. Lindenberger, S. Thanna, D.R. Ronning, and S.J. Sucheck. The Journal of Organic Chemistry, 2014. 79(20): p. 9444-9450. PMID[25137149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25315519">Identification of Plant-derived Natural Products as Potential Inhibitors of the Mycobacterium tuberculosis Proteasome.</a> Zheng, Y., X. Jiang, F. Gao, J. Song, J. Sun, L. Wang, X. Sun, Z. Lu, and H. Zhang. BMC Complementary and Alternative Medicine, 2014. 14(1): p. 400. PMID[25315519].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25089658">Potent Antimalarial Activity of Acriflavine in Vitro and in Vivo.</a> Dana, S., D. Prusty, D. Dhayal, M.K. Gupta, A. Dar, S. Sen, P. Mukhopadhyay, T. Adak, and S.K. Dhar. ACS Chemical Biology, 2014. 9(10): p. 2366-2373. PMID[25089658].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25157434">The Antimalarial Drugs Chloroquine and Primaquine Inhibit Pyridoxal Kinase, an Essential Enzyme for Vitamin B6 Production.</a> Kimura, T., R. Shirakawa, N. Yaoita, T. Hayashi, K. Nagano, and H. Horiuchi. FEBS Letters, 2014. 588(20): p. 3673-3676. PMID[25157434].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25195945">Blood Schizontocidal and Gametocytocidal Activity of 3-Hydroxy-N&#39;-arylidenepropanehydrazonamides: A New Class of Antiplasmodial Compounds</a>. Leven, M., J. Held, S. Duffy, S. Tschan, S. Sax, J. Kamber, W. Frank, K. Kuna, D. Geffken, C. Siethoff, S. Barth, V.M. Avery, S. Wittlin, B. Mordmuller, and T. Kurz. Journal of Medicinal Chemistry, 2014. 57(19): p. 7971-7976. PMID[25195945].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25319003">Enantiomerically Pure Amino-alcohol Quinolines: In Vitro Anti-malarial Activity in Combination with Dihydroartemisinin, Cytotoxicity and in Vivo Efficacy in a Plasmodium berghei Mouse Model.</a> Mullie, C., N. Taudon, C. Degrouas, A. Jonet, A. Pascual, P. Agnamey, and P. Sonnet. Malaria Journal, 2014. 13(407): 8 pp. PMID[25319003].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25245989">13. Facile Synthesis of Antimalarial 1,2-Disubstituted 4-quinolones from 1,3-Bisaryl-monothio-1,3-diketones.</a> Vinayaka, A.C., M.P. Sadashiva, X. Wu, S.S. Biryukov, J.A. Stoute, K.S. Rangappa, and D.C. Gowda. Organic &amp; Biomolecular Chemistry, 2014. 12(42): p. 8555-8561. PMID[25245989].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1010-102314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342104900002">Sub-MICs of Carum copticum and Thymus vulgaris Influence Virulence Factors and Biofilm Formation in Candida Spp.</a> Khan, M.S., I. Ahmad, S.S. Cameotra, and F. Botha. BMC Complementary and Alternative Medicine, 2014. 14(337): 14 pp. ISI[000342104900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341631500019">A Simple, Efficient One-pot Synthesis of 2-Isoxazoline Derivatives and their Antimicrobial Activity.</a> Kumar, A., A.U.R. Sankar, and S.H. Kim. Journal of Heterocyclic Chemistry, 2014. 51: p. E146-E151. ISI[000341631500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341631500035">Quinolone Analogs 14: Synthesis of Antimalarial 1-Aryl-3-(4-quinolon-2-yl)-ureas and Related Compounds.</a> Kurasawa, Y., K. Yoshida, N. Yamazaki, K. Sasaki, Y. Zamami, Z. Min, A. Togi, H. Ito, E. Kaji, and H. Fukaya. Journal of Heterocyclic Chemistry, 2014. 51: p. E241-E248. ISI[000341631500035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br />  

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341703800008">Determination of the Antimicrobial Capacity of Green Tea (Camellia sinensis) against the Potentially Pathogenic Microorganisms Escherichia coli, Salmonella enterica, Staphylococcus aureus, Listeria monocytogenes, Candida albicans and Aspergillus niger.</a> Mora, A., J. Parra, J.M. Chaverri, and M.L. Arias. Archivos Latinoamericanos de Nutricion, 2013. 63(3): p. 247-253. ISI[000341703800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342038900010">Potential Anti-tubercular and in Vitro Anti-inflammatory Agents: 9-Substituted 1,8-dioxo-octahydroxanthenes through Cascade/Domino Reaction by Citric Fruit Juices.</a> Napoleon, A.A. and F.R.N. Khan. Medicinal Chemistry Research, 2014. 23(11): p. 4749-4760. ISI[000342038900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341842500043">Synthesis of 5-Phenyl-3-(10H-phenothiazinyl)-delta(2)-cyclohexen-1-ones by Conventional and Microwave-assisted Methods and their Antifungal Activity.</a> Saranya, A.V. and S. Ravi. Research on Chemical Intermediates, 2014. 40(8): p. 3085-3093. ISI[000341842500043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341964100003">Novel Quinazolinone-thiazolidinone Hybrid: Design, Synthesis and in Vitro Antimicrobial and Antituberculosis Studies.</a> Shah, D.R., R.P. Modh, D.D. Desai, and K.H. Chikhalia. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2014. 53(9): p. 1169-1177. ISI[000341964100003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342038900029">In Vitro Antimicrobial Activity of O-Phenylenediamine-tert-butyl-N-1,2,3-triazole Carbamate Analogs.</a> Singh, M.K., M. Gangwar, D. Kumar, R. Tilak, G. Nath, and A. Agarwal. Medicinal Chemistry Research, 2014. 23(11): p. 4962-4976. ISI[000342038900029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342057300008">Chemical Composition and Bioactivity of the Volatile Oil from Leaves and Stems of Eucalyptus cinerea.</a> Soliman, F.M., M.M. Fathy, M.M. Salama, and F.R. Saber. Pharmaceutical Biology, 2014. 52(10): p. 1272-1277. ISI[000342057300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000341854500001">Antimicrobial Nanocapsules: From New Solvent-free Process to in Vitro Efficiency.</a> Steelandt, J., D. Salmon, E. Gilbert, E. Almouazen, F.N.R. Renaud, L. Roussel, M. Haftek, and F. Pirot. International Journal of Nanomedicine, 2014. 9: p. 4467-4474. ISI[000341854500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1010-102314.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
