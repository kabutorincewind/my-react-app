

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-04-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Bx/toiFfuJLe88Q9c3nGvpZFjcgzAa9jCcuUV4jWMiKa4fV3c+PgPlbR+4oWGxs3ladHxF9ax8W73oD/CrT/IvjOmt0aQkBib/lxWxZMT2R4W6ZuG3Mt+tqmFXw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="26236187" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 11 - April 24, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583607">Imidazolylchromanones Containing Non-benzylic oxime ethers: Synthesis and Molecular Modeling Study of New Azole Antifungals Selective against Cryptococcus gattii.</a> Babazadeh-Qazijahani, M., H. Badali, H. Irannejad, M.H. Afsarian, and S. Emami. European Journal of Medicinal Chemistry, 2014. 76: p. 264-273. PMID[24583607].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24631896">Design, Synthesis &amp; Evaluation of Condensed 2H-4-Arylaminopyrimidines as Novel Antifungal Agents.</a> Jain, K.S., V.M. Khedkar, N. Arya, P.V. Rane, P.K. Chaskar, and E.C. Coutinho. European Journal of Medicinal Chemistry, 2014. 77: p. 166-175. PMID[24631896].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583354">Synthesis, Structures, Spectroscopy and Antimicrobial Properties of Complexes of Copper(II) with Salicylaldehyde N-substituted Thiosemicarbazones and 2,2&#39;-Bipyridine or 1,10-Phenanthroline.</a>Lobana, T.S., S. Indoria, A.K. Jassal, H. Kaur, D.S. Arora, and J.P. Jasinski. European Journal of Medicinal Chemistry, 2014. 76: p. 145-154. PMID[24583354].</p>

    <p class="plaintext"><b>[</b><b>PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583356">Bifunctional Ethyl 2-amino-4-methylthiazole-5-carboxylate Derivatives: Synthesis and in Vitro Biological Evaluation as Antimicrobial and Anticancer Agents.</a> Rostom, S.A., H.M. Faidallah, M.F. Radwan, and M.H. Badr. European Journal of Medicinal Chemistry, 2014. 76: p. 170-181. PMID[24583356].</p>

    <p class="plaintext"><b>[</b><b>PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24748772">Triazole Derivatives with Improved in Vitro Antifungal Activity over Azole Drugs.</a> Yu, S., X. Chai, Y. Wang, Y. Cao, J. Zhang, Q. Wu, D. Zhang, Y. Jiang, T. Yan, and Q. Sun. Journal of Drug Design, Development and Therapy, 2014. 8: p. 383-390. PMID[24748772].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24679703">Design, Synthesis and Evaluation of 1,2,3-Triazole-adamantylacetamide Hybrids as Potent Inhibitors of Mycobacterium tuberculosis.</a> Addla, D., A. Jallapally, D. Gurram, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(8): p. 1974-1979. PMID[24679703].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24631899">Anti-mycobacterial Activity of 1,3-Diaryltriazenes.</a>Cappoen, D., J. Vajs, C. Uythethofken, A. Virag, V. Mathys, M. Kocevar, L. Verschaeve, M. Gazvoda, S. Polanc, K. Huygen, and J. Kosmrlj. European Journal of Medicinal Chemistry, 2014. 77: p. 193-203. PMID[24631899].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24681334">2,4-Dialkyl-8,9,10,11-tetrahydrobenzo[g]pyrimido[4,5-c]isoquinoline-1,3,7,12(2H,4 H)-tetraones as New Leads against Mycobacterium tuberculosis.</a> Claes, P., D. Cappoen, C. Uythethofken, J. Jacobs, B. Mertens, V. Mathys, L. Verschaeve, K. Huygen, and N. De Kimpe. European Journal of Medicinal Chemistry, 2014. 77: p. 409-421. PMID[24681334].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24642567">Enantiopure Antituberculosis Candidates Synthesized from (-)-Fenchone.</a> Dobrikov, G.M., V. Valcheva, Y. Nikolova, I. Ugrinova, E. Pasheva, and V. Dimitrov. European Journal of Medicinal Chemistry, 2014. 77: p. 243-247. PMID[24642567].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24736743">A Trisubstituted Benzimidazole Cell Division Inhibitor with Efficacy against Mycobacterium tuberculosis.</a>Knudson, S.E., D. Awasthi, K. Kumar, A. Carreau, L. Goullieux, S. Lagrange, H. Vermet, I. Ojima, and R.A. Slayden. Plos One, 2014. 9(4): p. e93953. PMID[24736743].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24589483">Sulfur Rich 2-Mercaptobenzothiazole and 1,2,3-Triazole Conjugates as Novel Antitubercular Agents.</a>Mir, F., S. Shafi, M.S. Zaman, N.P. Kalia, V.S. Rajput, C. Mulakayala, N. Mulakayala, I.A. Khan, and M.S. Alam. European Journal of Medicinal Chemistry, 2014. 76: p. 274-283. PMID[24589483].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24583350">Development of Antimycobacterial Tetrahydrothieno[2,3-C]pyridine-3-carboxamides and Hexahydrocycloocta[b]thiophene-3-carboxamides: Molecular Modification from Known Antimycobacterial Lead.</a> Nallangi, R., G. Samala, J.P. Sridevi, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2014. 76: p. 110-117. PMID[24583350].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">13.<a href="http://www.ncbi.nlm.nih.gov/pubmed/24722591">Synthesis and Biological Evaluation of Novel 2-Methoxypyridylamino-substituted Riminophenazine Derivatives as Antituberculosis Agents.</a> Zhang, D., Y. Liu, C. Zhang, H. Zhang, B. Wang, J. Xu, L. Fu, D. Yin, C.B. Cooper, Z. Ma, Y. Lu, and H. Huang. Molecules, 2014. 19(4): p. 4380-4394. PMID[24722591].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24684842">Design, Synthesis, and Biological Evaluation of Dihydroartemisinin-fluoroquinolone Conjugates as a Novel Type of Potential Antitubercular Agents.</a> Zhou, F.W., H.S. Lei, L. Fan, L. Jiang, J. Liu, X.M. Peng, X.R. Xu, L. Chen, C.H. Zhou, Y.Y. Zou, C.P. Liu, Z.Q. He, and D.C. Yang. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(8): p. 1912-1917. PMID[24684842].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24650715">Emergence of Pyrido quinoxalines as New Family of Antimalarial Agents.</a> Chandra Shekhar, A., P. Shanthan Rao, B. Narsaiah, A.D. Allanki, and P.S. Sijwali. European Journal of Medicinal Chemistry, 2014. 77: p. 280-287. PMID[24650715].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24602791">Synthesis, in Vitro Antiplasmodial Activity and Cytotoxicity of a Series of Artemisinin-triazine Hybrids and Hybrid-dimers.</a>Cloete, T.T., C. de Kock, P.J. Smith, and D.D. N&#39;Da. European Journal of Medicinal Chemistry, 2014. 76: p. 470-481. PMID[24602791].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24565569">Synthesis of Febrifugine Derivatives and Development of an Effective and Safe Tetrahydroquinazoline-type Antimalarial.</a>Kikuchi, H., S. Horoiwa, R. Kasahara, N. Hariguchi, M. Matsumoto, and Y. Oshima. European Journal of Medicinal Chemistry, 2014. 76: p. 10-19. PMID[24565569].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24731238">Assessment of the Prophylactic Activity and Pharmacokinetic Profile of Oral Tafenoquine Compared to Primaquine for Inhibition of Liver Stage Malaria Infections.</a> Li, Q., M. O&#39;Neil, L. Xie, D. Caridha, Q. Zeng, J. Zhang, B. Pybus, M. Hickman, and V. Melendez. Malaria Journal, 2014. 13: p. 141. PMID[24731238].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24740150">Gellan Sulfate Inhibits Plasmodium falciparum Growth and Invasion of Red Blood Cells in Vitro.</a> Recuenco, F.C., K. Kobayashi, A. Ishiwa, Y. Enomoto-Rogers, N.G. Fundador, T. Sugi, H. Takemae, T. Iwanaga, F. Murakoshi, H. Gong, A. Inomata, T. Horimoto, T. Iwata, and K. Kato. Scientific Reports, 2014. 4: p. 4723. PMID[24740150].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0411-042414.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332937600029">Synthesis and Antimicrobial Activity of Novel 2,4-Disubstituted Thiazoles.</a> An, T.N.M. and K.D. Lee. Asian Journal of Chemistry, 2013. 25(18): p. 10160-10164. ISI[000332937600029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332470000060">Antibacterial Activity of Adamantyl Substituted Cyclohexane Diamine Derivatives against Methicillin Resistant Staphylococcus aureus and Mycobacterium tuberculosis.</a> Beena, D. Kumar, W. Kumbukgolla, S. Jayaweera, M. Bailey, T. Alling, J. Ollinger, T. Parish, and D.S. Rawat. RSC Advances, 2014. 4(23): p. 11962-11966. ISI[000332470000060].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331932400002">Evaluation of Antimicrobial and Cytotoxic Activities of Plant Extracts from Southern Minas Gerais Cerrado.</a>Chavasco, J.M., B. Feliphe, C.D. Cerdeira, F.D. Leandro, L.F.L. Coelho, J.J. da Silva, J.K. Chavasco, and A.L.T. Dias. Revista do Instituto de Medicina Tropical de Sao Paulo, 2014. 56(1): p. 13-20. ISI[000331932400002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332770000036">Synthesis and Antimicrobial Activity of Novel Chalcone Derivatives.</a> Fang, X.W., B.Q. Yang, Z. Cheng, P.F. Zhang, and M.P. Yang. Research on Chemical Intermediates, 2014. 40(4): p. 1715-1725. ISI[000332770000036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332507500003">Determination of Essential Oil Composition, Antimicrobial and Antioxidant Properties of Anise (Pimpinella anisum L.) and Cumin (Cuminum cyminum L.) Seeds.</a> Hasimi, N., V. Tolan, S. Kizil, and E. Kilinc. Tarim Bilimleri Dergisi-Journal of Agricultural Sciences, 2014. 20(1): p. 19-26. ISI[000332507500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332937600097">Synthesis and Evaluation of Some Novel Benzimidazole Derivatives Bearing Thiazolidinone Moiety as Potential Antimicrobial Activity.</a> Kumar, P.S. and S.K. Patro. Asian Journal of Chemistry, 2013. 25(18): p. 10449-10453. ISI[000332937600097].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332785800018">Antimicrobial, Thermoanalytical and Viscometric Studies of Metal Based Schiff Base Polymer.</a> Mughal, M.A., A. Mughal, G.Z. Memon, M.Y. Khuhawar, and N.N. Memon. Journal of the Chemical Society of Pakistan, 2013. 35(6): p. 1535-1542. ISI[000332785800018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332385200062">Antioxidant and Antifungal Activities of Some Agro Wastes and their Phenolic acid Profile.</a> Naseer, R., B. Sultana, H.N. Bhatti, and A. Jamil. Asian Journal of Chemistry, 2014. 26(4): p. 1225-1231. ISI[000332385200062].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332944300015">Photocatalytic Antimicrobial Effect of TiO2 Anatase Thin-film-coated Orthodontic Arch Wires on 3 Oral Pathogens.</a>Ozyildiz, F., A. Uzel, A.S. Hazar, M. Guden, S. Olmez, I. Aras, and I. Karaboz. Turkish Journal of Biology, 2014. 38(2): p. 289-295. ISI[000332944300015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332760700029">Application of Methyl Fatty Hydroxamic acids Based on Jatropha curcas Seed Oil and Their Metal Complexes as Anti Microbial Agents.</a> Rafiee-Moghaddam, R., J. Salimon, M.J. Haron, H. Jahangirian, M.H.S. Ismail, L. Afsah-Hejri, N. Vafaei, and Y. Tayefehchamani. Digest Journal of Nanomaterials and Biostructures, 2014. 9(1): p. 261-271. ISI[000332760700029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332679700001">Synthesis and Antimicrobial Activity of Some New N-(Aryloxoalkyl)-5-arylidene-thiazolidine-2,4-diones.</a> Stana, A., B. Tiperciuc, M. Duma, A. Pirnau, P. Verite, and O. Oniga. Journal of the Serbian Chemical Society, 2014. 79(2): p. 115-123. ISI[000332679700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332637400001">Effects of Mentha suaveolens Essential Oil Alone or in Combination with Other Drugs in Candida albicans.</a>Stringaro, A., E. Vavala, M. Colone, F. Pepi, G. Mignogna, S. Garzoli, S. Cecchetti, R. Ragno, and L. Angiolella. Evidence-Based Complementary and Alternative Medicine, 2014.125904: p. 9.ISI[000332637400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332701400033">All-trans Retinoic acid-triggered Antimicrobial Activity against Mycobacterium tuberculosis is Dependent on NPC2.</a> Wheelwright, M., E.W. Kim, M.S. Inkeles, A. De Leon, M. Pellegrini, S.R. Krutzik, and P.T. Liu. Journal of Immunology, 2014. 192(5): p. 2280-2290. ISI[000332701400033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0411-042414.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
