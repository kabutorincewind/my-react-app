

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9/qB+dGBDRkHxnK2n+STPY6vJxZybx/W6w1OSBGBZDB/iDEkK2nEUwMbvHvURaMicLr5q/n7d5uuu5yOlhTK4SLn9lhYe6f7LF4lfWUdTMsX/oefgwtbLIvtiD0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="59A48DB3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: January, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367540300010">One-pot Synthesis of N-Aryl 1,4-dihydropyridine Derivatives and their Biological Activities.</a> Dhinakaran, I., V. Padmini, and N. Bhuvanesh. Journal of Chemical Sciences, 2015. 127(12): p. 2201-2209. ISI[000367540300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26472515">In vitro anti-HIV-1 Activity of Fucoidan from Sargassum swartzii.</a> Dinesh, S., T. Menon, L.E. Hanna, V. Suresh, M. Sathuvan, and M. Manikannan. International Journal of Biological Macromolecules, 2016. 82: p. 83-88. PMID[26472515].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26608715">Facile Synthesis of the NNRTI Microbicide MC-1220 and Synthesis of Its Phosphoramidate Prodrugs.</a> Loksha, Y.M., E.B. Pedersen, P. La Colla, and R. Loddo. Organic &amp; Biomolecular Chemistry, 2016. 14(3): p. 940-946. PMID[26608715].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367554100074">Soybean-derived Bowman-Birk Inhibitor (BBI) Inhibits HIV Infection of Human Macrophages.</a> Ma, T.C., X. Wang, J.L. Li, L. Zhou, K. Zhuang, J.B. Liu, Y. Zhou, and W.Z. Ho. Journal of Neuroimmune Pharmacology, 2015. 10: p. S84-S85. ISI[000367554100074].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26685880">Specific Inhibitors of HIV Capsid Assembly Binding to the C-terminal Domain of the Capsid Protein: Evaluation of 2-Arylquinazolines as Potential Antiviral Compounds.</a> Machara, A., V. Lux, M. Kozisek, K. Grantz Saskova, O. Stepanek, M. Kotora, K. Parkan, M. Pavova, B. Glass, P. Sehr, J. Lewis, B. Muller, H.G. Krausslich, and J. Konvalinka. Journal of Medicinal Chemistry, 2016. 59(2): p. 545-558. PMID[26685880].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26775541">Synthesis and Evaluation of 3-Hydroxy-3-phenylpropanoate ester-AZT Conjugates as Potential Dual-action HIV-1 Integrase and Reverse Transcriptase Inhibitors.</a> Manyeruke, M.H., T.O. Olomola, S. Majumder, S. Abrahams, M. Isaacs, N. Mautsa, S. Mosebi, D. Mnkandhla, R. Hewer, H.C. Hoppe, R. Klein, and P.T. Kaye. Bioorganic &amp; Medicinal Chemistry, 2015. 23(24): p. 7521-7528. PMID[26775541].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367558100190">Anti-HIV Activity of Hyptis Jacq. (Lamiaceae).</a> Partida, M.D.S., K.P. Santos, B. Loureiro, and C.M. Furlan. Planta Medica, 2015. 81(16): p. 1448-1448. ISI[000367558100190].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26377841">Influence of Structural Parameters on the Self-association Properties of anti-HIV Catanionic Dendrimers.</a> Perez-Anes, A., F. Rodrigues, A.M. Caminade, C. Stefaniu, B. Tiersch, C.O. Turrin, and M. Blanzat. ChemPhysChem, 2015. 16(16): p. 3433-3437. PMID[26377841].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26691389">Nitrogen Positional Scanning in Tetramines Active against HIV-1 as Potential CXCR4 Inhibitors.</a> Puig de la Bellacasa, R., A. Gibert, J.M. Planesas, L. Ros-Blanco, X. Batllori, R. Badia, B. Clotet, J. Este, J. Teixido, and J.I. Borrell. Organic &amp; Biomolecular Chemistry, 2016. 14(4): p. 1455-1472. PMID[26691389].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000366569300044">Anti-HIV-1 Integrase Diterpenoids from Dichrocephala benthamii.</a> Song, B., G. Ding, X.H. Tian, L. Li, C. Zhou, Q.B. Zhang, M.H. Wang, T. Zhang, and Z.M. Zou. Phytochemistry Letters, 2015. 14: p. 249-253. ISI[000366569300044].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26767741">Fullerene-based Inhibitors of HIV-1 Protease.</a> Strom, T.A., S. Durdagi, S.S. Ersoz, R.E. Salmas, C.T. Supuran, and A.R. Barron. Journal of Peptide Science, 2015. 21(12): p. 862-870. PMID[26767741].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26706116">Design, Synthesis and Activity Evaluation of Novel Peptide Fusion Inhibitors Targeting HIV-1 gp41.</a> Tan, J., M. Su, Y. Zeng, and C. Wang. Bioorganic &amp; Medicinal Chemistry, 2016. 24(2): p. 201-206. PMID[26706116].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26676723">A Stereo Configuration-activity Study of 3-Iodo-4-(2-methylcyclohexyloxy)-6-phenethylpyridin-2(2H)-ones as Potency Inhibitors of HIV-1 Variants.</a> Wu, S., Q. Yin, L. Zhao, N. Fan, X. Tang, J. Zhao, T. Sheng, Y. Guo, C. Tian, Z. Zhang, W. Xu, Z. Liu, S. Jiang, L. Ma, J. Liu, and X. Wang. Organic &amp; Biomolecular Chemistry, 2016. 14(4): p. 1413-1420. PMID[26676723].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000367274300082">New Securinega Alkaloids with anti-HIV Activity from Flueggea virosa.</a> Zhang, H., C.R. Zhang, Y.S. Han, M.A. Wainberg, and J.M. Yue. RSC Advances, 2015. 5(129): p. 107045-107053. ISI[000367274300082].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26711143">Aspernigrins with anti-HIV-1 Activities from the Marine-derived Fungus Aspergillus niger SCSIO Jcsw6F30.</a> Zhou, X., W. Fang, S. Tan, X. Lin, T. Xun, B. Yang, S. Liu, and Y. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 361-365. PMID[26711143].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_01_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160120&amp;CC=EP&amp;NR=2975034A1&amp;KC=A1">A Quinoline Derivative for the Treatment of Inflammatory Diseases and AIDS.</a> Patent. 2016. 2014-306166 2975034: 18pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160107&amp;CC=WO&amp;NR=2016001820A1&amp;KC=A1">Novel Betulinic Proline Imidazole Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, P.R. Adulla, D.K. Gazula Levi, E.R. Bammidi, and R.R. Kothakapu. Patent. 2016. 2015-IB54864 2016001820: 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160114&amp;CC=WO&amp;NR=2016007765A1&amp;KC=A1">Modulators of Toll-like Receptors for the Treatment of Human Immunodeficiency Virus (HIV) Infection.</a> Geleziunas, R. and J.E. Hesselgesser. Patent. 2016. 2015-US39776 2016007765: 308pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160128&amp;CC=WO&amp;NR=2016012913A1&amp;KC=A1">Phenyl and Tertbutylacetic acid Substituted Pyridinones Having anti-HIV Effects.</a> Johns, B.A. and E.J. Velthuisen. Patent. 2016. 2015-IB55385 2016012913: 35pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160114&amp;CC=WO&amp;NR=2016005878A1&amp;KC=A1">Preparation of Isoindoline Derivatives for Use in the Treatment of a Viral Infection.</a> Johns, B.A., E.J. Velthuisen, J.G. Weatherhead, L. Suwandi, and D. Temelkoff. Patent. 2016. 2015-IB55095 2016005878: 227pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_01_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
