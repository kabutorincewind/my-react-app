

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-88.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T/JwO4KonX69WVPJSA3oa3GFCX7h4/k9bEnXHuAKTzItohVqdNd2mDZR0/8MaMv9d2oQywT2QTuCsbbekyv9DpbB8CS959QBv5rqy+4HBeHSkVPD0Ql98OEJ2bk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="67DF6455" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-88-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56231   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rapid identification of emerging pathogens: coronavirus</p>

    <p>          Sampath, R</p>

    <p>          Emerg Infect Dis <b>2005</b>.  11(3): 373-379</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757550&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757550&amp;dopt=abstract</a> </p><br />

    <p>2.     56232   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel strategies for prevention and treatment of influenza</p>

    <p>          Kandel, R and Hartshorn, KL</p>

    <p>          Expert Opin Ther Targets <b>2005</b>.  9(1): 1-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757479&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757479&amp;dopt=abstract</a> </p><br />

    <p>3.     56233   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Hepatitis C Virus (Hcv) Internal Ribosome Entry Site (Ires) Domain Iii-Iv-Targeted Aptamer Inhibits Translation by Binding to an Apical Loop of Domain Iiid</p>

    <p>          Kikuchi, K. <i>et al.</i></p>

    <p>          Nucleic Acids Research <b>2005</b>.  33(2): 683-692</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226941000037</a> </p><br />

    <p>4.     56234   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Filling the hole: evidence of a small molecule binding to the fusion core pocket in human respiratory syncytial virus</p>

    <p>          Gao, GF</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(2): 195-197</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757395&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757395&amp;dopt=abstract</a> </p><br />

    <p>5.     56235   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 5&#39;,9-anhydro-3-([beta]-d-ribofuranosyl)xanthine, and 3,5&#39;-anhydro-xanthosine as potential anti-hepatitis C virus agents</p>

    <p>          Chun, Byoung-Kwon, Wang, Peiyuan, Hassan, Abdalla, Du, Jinfa, Tharnish, Phillip M, Stuyver, Lieven J, Otto, Michael J, Schinazi, Raymond F, and Watanabe, Kyoichi A</p>

    <p>          Tetrahedron Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4FNCVT1-4/2/aecea2332f93225dbeda2ae1e4fe02d1">http://www.sciencedirect.com/science/article/B6THS-4FNCVT1-4/2/aecea2332f93225dbeda2ae1e4fe02d1</a> </p><br />
    <br clear="all">

    <p>6.     56236   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Agents and strategies in development for improved management of herpes simplex virus infection and disease</p>

    <p>          Kleymann, G</p>

    <p>          Expert Opin Investig Drugs <b>2005</b>.  14(2): 135-161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757392&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757392&amp;dopt=abstract</a> </p><br />

    <p>7.     56237   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense approaches for inhibiting respiratory syncytial virus</p>

    <p>          Cramer, H</p>

    <p>          Expert Opin Biol Ther <b>2005</b>.  5(2): 207-220</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757382&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757382&amp;dopt=abstract</a> </p><br />

    <p>8.     56238   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of S-Acyl-2-Thioethyl Esters of Modified Nucleoside 5&#39;-Monophosphates as Inhibitors of Hepatitis C Virus Rna Replication</p>

    <p>          Prakash, T. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(4): 1199-1210</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227115500030</a> </p><br />

    <p>9.     56239   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyrazolopyridine antiherpetics: SAR of C2&#39; and C7 amine substituents</p>

    <p>          Johns, BA, Gudmundsson, KS, Turner, EM , Allen, SH, Samano, VA, Ray, JA, Freeman, GA, Boyd, FL Jr, Sexton, CJ, Selleseth, DW, Creech, KL, and Moniri, KR</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(7): 2397-2411</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15755642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15755642&amp;dopt=abstract</a> </p><br />

    <p>10.   56240   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human Cathelicidin (Ll-37/Hcap-18) Demonstrates Direct Antiviral Activity Against Adenovirus and Herpes Simplex Virus in Vitro</p>

    <p>          Gordon, Y. <i>et al.</i></p>

    <p>          Investigative Ophthalmology &amp; Visual Science <b>2004</b>.  45: U810</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338002211">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338002211</a> </p><br />

    <p>11.   56241   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Pyranoindole Derivatives as Hcv Polymerase Inhibitors.</p>

    <p>          Park, K. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U931</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803965">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803965</a> </p><br />
    <br clear="all">

    <p>12.   56242   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The substrate specificity of SARS coronavirus 3C-like proteinase</p>

    <p>          Fan, K, Ma, L, Han, X, Liang, H, Wei, P, Liu, Y, and Lai, L</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  329(3): 934-940</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752746&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15752746&amp;dopt=abstract</a> </p><br />

    <p>13.   56243   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tripeptide Inhibitors of the Hepatitis C Virus Serine Protease.</p>

    <p>          Ghiro, E. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U932</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803966">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803966</a> </p><br />

    <p>14.   56244   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          From genome to antivirals: SARS as a test tube</p>

    <p>          Kliger, Y, Levanon, EY, and Gerber, D</p>

    <p>          Drug Discov Today <b>2005</b>.  10(5): 345-352</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749283&amp;dopt=abstract</a> </p><br />

    <p>15.   56245   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of a critical neutralization determinant of severe acute respiratory syndrome (SARS)-associated coronavirus: importance for designing SARS vaccines</p>

    <p>          He, Y, Zhu, Q, Liu, S, Zhou, Y, Yang, B, Li, J, and Jiang, S</p>

    <p>          Virology <b>2005</b>.  334(1): 74-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749124&amp;dopt=abstract</a> </p><br />

    <p>16.   56246   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Crystal structures of the RNA dependent RNA polymerase genotype 2a of hepatitis C virus reveal two conformations and suggest mechanisms of inhibition by non-nucleoside inhibitors</p>

    <p>          Biswal, BK, Cherney, MM, Wang, M, Chan, L, Yannopoulos, CG, Bilimoria, D, Nicolas, O, Bedard, J, and James, MN</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15746101&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15746101&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   56247   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          QSAR for anti-RNA-virus activity, synthesis, and assay of anti-RSV carbonucleosides given a unified representation of spectral moments, quadratic, and topologic indices</p>

    <p>          Gonzalez-Diaz, H, Cruz-Monteagudo, M, Vina, D, Santana, L, Uriarte, E, and De, Clercq E</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(6): 1651-1657</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745816&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745816&amp;dopt=abstract</a> </p><br />

    <p>18.   56248   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase: synthesis and structure-activity relationships of N-1-heteroalkyl-4-hydroxyquinolon-3-yl-benzothiadiazines</p>

    <p>          Pratt, JK, Donner, P, McDaniel, KF, Maring, CJ, Kati, WM, Mo, H, Middleton, T, Liu, Y, Ng, T, Xie, Q, Zhang, R, Montgomery, D, Molla, A, Kempf, DJ, and Kohlbrenner, W</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(6): 1577-1582</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745800&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745800&amp;dopt=abstract</a> </p><br />

    <p>19.   56249   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Parallel Synthesis of Pteridine Derivatives as Potent Inhibitors for Hepatitis C Virus Ns5b Rna-Dependent Rna Polymerase</p>

    <p>          Ding, Y. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(3): 675-678</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226935700036</a> </p><br />

    <p>20.   56250   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of the anti-cytomegalovirus (CMV) drug maribavir (MB) on the activities of cytochrome p450 (CYP) 1A2, 2C9, 2C19, 2D6, 3A, N-ACETYLTRANSFERASE-2 (NAT-2), and xanthine oxidase (XO) as assessed by the cooperstown 5+1 drug cocktail</p>

    <p>          Ma, JD, Nafziger, AN, Villano, SA, Gaedigk, A, Victory, J, and Bertino, JS</p>

    <p>          Clinical Pharmacology &amp; Therapeutics <b>2005</b>.  77(2): P19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WCN-4FDMRBG-2K/2/2c19d48c06a4524cc168e7bcb15ed5ca">http://www.sciencedirect.com/science/article/B6WCN-4FDMRBG-2K/2/2c19d48c06a4524cc168e7bcb15ed5ca</a> </p><br />

    <p>21.   56251   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of small molecule inhibitors of the hepatitis C virus RNA-dependent RNA polymerase from a pyrrolidine combinatorial mixture</p>

    <p>          Burton, G, Ku, TW, Carr, TJ, Kiesow, T, Sarisky, RT, Lin-Goerke, J, Baker, A, Earnshaw, DL, Hofmann, GA, Keenan, RM, and Dhanak, D</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(6): 1553-1556</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15745795&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   56252   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sialobiology of influenza: molecular mechanism of host range variation of influenza viruses</p>

    <p>          Suzuki, Y</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(3): 399-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15744059&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15744059&amp;dopt=abstract</a> </p><br />

    <p>23.   56253   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel [(biphenyloxy)propyl]isoxazole derivatives for inhibition of human rhinovirus 2 and coxsackievirus B3 replication</p>

    <p>          Makarov, VA, Riabova, OB, Granik, VG, Wutzler, P, and Schmidtke, M</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743897&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743897&amp;dopt=abstract</a> </p><br />

    <p>24.   56254   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Prospects for adenovirus antivirals</p>

    <p>          Kinchington, PR, Romanowski, EG, and Jerold, Gordon Y</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743895&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743895&amp;dopt=abstract</a> </p><br />

    <p>25.   56255   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Phosphonated Carbocyclic 2&#39;-Oxa-3&#39;-aza-nucleosides: Novel Inhibitors of Reverse Transcriptase</p>

    <p>          Chiacchio, U, Balestrieri, E, Macchi, B, Iannazzo, D, Piperno, A, Rescifina, A, Romeo, R, Saglimbeni, M, Sciortino, MT, Valveri, V, Mastino, A, and Romeo, G</p>

    <p>          J Med Chem <b>2005</b>.  48(5): 1389-1394</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743182&amp;dopt=abstract</a> </p><br />

    <p>26.   56256   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p><b>          Development and Preliminary Optimization of Indole-N-Acetamide Inhibitors of Hepatitis C Virus NS5B Polymerase</b> </p>

    <p>          Harper, S, Pacini, B, Avolio, S, Di Filippo, M, Migliaccio, G, Laufer, R, De Francesco, R, Rowley, M, and Narjes, F</p>

    <p>          J Med Chem <b>2005</b>.  48(5): 1314-1317</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743173&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15743173&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>27.   56257   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Current status of anti-SARS agents</p>

    <p>          Shigeta, S and Yamase, T</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(1): 23-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739619&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739619&amp;dopt=abstract</a> </p><br />

    <p>28.   56258   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New antiviral drugs, vaccines and classic public health interventions against SARS coronavirus</p>

    <p>          Oxford, JS, Balasingam, S, Chan, C, Catchpole, A, and Lambkin, R</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(1): 13-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739618&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15739618&amp;dopt=abstract</a> </p><br />

    <p>29.   56259   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Poxvirus tropism</p>

    <p>          McFadden, G</p>

    <p>          Nat Rev Microbiol <b>2005</b>.  3(3): 201-213</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15738948&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15738948&amp;dopt=abstract</a> </p><br />

    <p>30.   56260   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The proton pump inhibitor lansoprazole inhibits rhinovirus infection in cultured human tracheal epithelial cells</p>

    <p>          Sasaki, T, Yamaya, M, Yasuda, H, Inoue, D, Yamada, M, Kubo, H, Nishimura, H, and Sasaki, H</p>

    <p>          Eur J Pharmacol <b>2005</b>.  509(2-3): 201-210</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15733557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15733557&amp;dopt=abstract</a> </p><br />

    <p>31.   56261   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of West Nile Virus Entry by Using a Recombinant Domain Iii From the Envelope Glycoprotein</p>

    <p>          Chu, J. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2005</b>.  86: 405-412</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226738500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226738500018</a> </p><br />

    <p>32.   56262   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Lambda interferon inhibits hepatitis B and C virus replication</p>

    <p>          Robek, MD, Boyd, BS, and Chisari, FV</p>

    <p>          J Virol <b>2005</b> .  79(6): 3851-3854</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15731279&amp;dopt=abstract</a> </p><br />

    <p>33.   56263   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthetic peptides derived from SARS coronavirus S protein with diagnostic and therapeutic potential</p>

    <p>          Lu, Wei, Wu, Xiao-Dong, Shi, Mu De, Yang, Rui Fu, He, You Yu, Bian, Chao, Shi, Tie Liu, Yang, Sheng, Zhu, Xue-Liang, and Jiang, Wei-Hong</p>

    <p>          FEBS Letters <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4FPBK78-3/2/e94a063249e2e49f5a7881490bc6fa33">http://www.sciencedirect.com/science/article/B6T36-4FPBK78-3/2/e94a063249e2e49f5a7881490bc6fa33</a> </p><br />

    <p>34.   56264   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Practical Synthesis of a Potent Hepatitis C Virus Rna Replication Inhibitor</p>

    <p>          Xu, F. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U121</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800588">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800588</a> </p><br />

    <p>35.   56265   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design of recombinant protein-based SARS-CoV entry inhibitors targeting the heptad-repeat regions of the spike protein S2 domain</p>

    <p>          Ni, Ling, Zhu, Jieqing, Zhang, Junjie, Yan, Meng, Gao, George F, and Tien, Po</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4FKXFJM-6/2/bb803259c675ba4ee3726e5a10c92261">http://www.sciencedirect.com/science/article/B6WBK-4FKXFJM-6/2/bb803259c675ba4ee3726e5a10c92261</a> </p><br />

    <p>36.   56266   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interferon alfacon1 is an inhibitor of SARS-corona virus in cell-based models</p>

    <p>          Paragas, Jason, Blatt, Lawrence M, Hartmann, Chris, Huggins, John W, and Endy, Tim P</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4FGX4CT-1/2/69769527ed41cdca776375a3c3a50c03">http://www.sciencedirect.com/science/article/B6T2H-4FGX4CT-1/2/69769527ed41cdca776375a3c3a50c03</a> </p><br />

    <p>37.   56267   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Triterpenoid Saponins From the Roots of Platycodon Grandiflorum</p>

    <p>          He, Z. <i>et al.</i></p>

    <p>          Tetrahedron <b>2005</b>.  61(8): 2211-2215</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227197400027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227197400027</a> </p><br />

    <p>38.   56268   DMID-LS-88; EMBASE-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of mizoribine and ribavirin on the replication of severe acute respiratory syndrome (SARS)-associated coronavirus</p>

    <p>          Saijo, Masayuki, Morikawa, Shigeru, Fukushi, Shuetsu, Mizutani, Tetsuya, Hasegawa, Hideki, Nagata, Noriyo, Iwata, Naoko, and Kurane, Ichiro</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4FHJVX4-1/2/7faf9721e0e27c687e9ad78a9637a9ab">http://www.sciencedirect.com/science/article/B6T2H-4FHJVX4-1/2/7faf9721e0e27c687e9ad78a9637a9ab</a> </p><br />
    <br clear="all">

    <p>39.   56269   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Susceptibilities of several clinical varicella-zoster virus (VZV) isolates and drug-resistant VZV strains to bicyclic furano pyrimidine nucleosides</p>

    <p>          Andrei, G, Sienaert, R, McGuigan, C, De Clercq, E, Balzarini, J, and Snoeck, R</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(3): 1081-1086</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728906&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15728906&amp;dopt=abstract</a> </p><br />

    <p>40.   56270   DMID-LS-88; PUBMED-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of anti-herpetic and antioxidant activities, and cytotoxic and genotoxic effects of synthetic alkyl-esters of gallic acid</p>

    <p>          Savi, LA, Leal, PC, Vieira, TO, Rosso, R, Nunes, RJ, Yunes, RA, Creczynski-Pasa, TB, Barardi, CR, and Simoes, CM</p>

    <p>          Arzneimittelforschung <b>2005</b>.  55(1): 66-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15727165&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15727165&amp;dopt=abstract</a> </p><br />

    <p>41.   56271   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenolic Profile, Antioxidant Property, and Anti-Influenza Viral Activity of Chinese Quince (Pseudocydonia Sinensis Schneid.), Quince (Cydonia Oblonga Mill.), And Apple (Malus Domestica Mill.) Fruits</p>

    <p>          Hamauzu, Y. <i>et al.</i></p>

    <p>          Journal of Agricultural and Food Chemistry <b>2005</b>.  53(4): 928-934</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227079800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227079800015</a> </p><br />

    <p>42.   56272   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Efficacy of Dipeptide Ganciclovir and Acyclovir Prodrugs, Val-Val-Ganciclovir (Vvgcv) and Gly-Val-Acyclovir (Gvacv), Against Hsv-1 Corneal Epithelial Keratitis in the Rabbit</p>

    <p>          Itahashi, M. <i>et al.</i></p>

    <p>          Investigative Ophthalmology &amp; Visual Science <b>2004</b>.  45: U127</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338000090">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338000090</a> </p><br />

    <p>43.   56273   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Inhibitors of Respiratory Syncytial Virus Rna-Dependent Rna Polymerase.</p>

    <p>          Bordeleau, J. <i> et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U931</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803963">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803963</a> </p><br />

    <p>44.   56274   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methylenecyclopropane Analogues of Purine Nucleosides as Agents Against Drug-Resistant Cytomegalovirus.</p>

    <p>          Zemlicka, J. and Drach, J.</p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U937</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803999">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712803999</a> </p><br />
    <br clear="all">

    <p>45.   56275   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of the Measles Virus Cell Entry: Fusion Blockade.</p>

    <p>          Sun, A. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2004</b>.  228: U968</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712804163">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223712804163</a> </p><br />

    <p>46.   56276   DMID-LS-88; WOS-DMID-3/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of a New Furanosic Sialylmimetic as a Potential Influenza Neuraminidase Viral Inhibitor</p>

    <p>          Bianco, A. <i>et al.</i></p>

    <p>          Letters in Organic Chemistry <b>2005</b>.  2(1): 83-88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226712500021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226712500021</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
