

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-12-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7yaK2EZn0pBT5OZIOHwXqDYR7yWibyq7MvlDL4OxN423Ku/9MG6vIJXkmRSdNERSwuyEt10dcEi7tWwiUZKgMeLTvZLyH0rmN88/0FG88inQ/e8EWlj60NRvZHQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="18609A32" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 22 - December 5, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24210372">Anti-HIV Active Daphnane Diterpenoids from Trigonostemon thyrsoideum.</a>Cheng, Y.Y., H. Chen, H.P. He, Y. Zhang, S.F. Li, G.H. Tang, L.L. Guo, W. Yang, F. Zhu, Y.T. Zheng, S.L. Li, and X.J. Hao. Phytochemistry, 2013. 96: p. 360-369. PMID[24210372].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24128815">Ligand Modifications to Reduce the Relative Resistance of Multi-drug Resistant HIV-1 Protease.</a> Dewdney, T.G., Y. Wang, Z. Liu, S.K. Sharma, S.J. Reiter, J.S. Brunzelle, I.A. Kovari, P.M. Woster, and L.C. Kovari. Bioorganic &amp; Medicinal Chemistry, 2013. 21(23): p. 7430-7434. PMID[24128815].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24289305">Structure-based Evaluation of C5 Derivatives in the Catechol Diether Series Targeting HIV-1 Reverse Transcriptase.</a> Frey, K.M., W.T. Gray, K.A. Spasov, M. Bollini, R. Gallardo-Macias, W.L. Jorgensen, and K.S. Anderson. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[24289305].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24117013">A Combination Microbicide Gel Protects Macaques against Vaginal Simian Human Immunodeficiency Virus-Reverse Transcriptase Infection, but Only Partially Reduces Herpes Simplex Virus-2 Infection after a Single High-dose Cochallenge.</a>Hsu, M., M. Aravantinou, R. Menon, S. Seidor, D. Goldman, J. Kenney, N. Derby, A. Gettie, J. Blanchard, M. Piatak, Jr., J.D. Lifson, J.A. Fernandez-Romero, T.M. Zydowsky, and M. Robbiani. AIDS Research and Human Retroviruses, 2013. <b>[Epub ahead of print]</b>. PMID[24117013].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24100493">Effects of Substitutions at the 4&#39; and 2 Positions on the Bioactivity of 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine.</a> Kirby, K.A., E. Michailidis, T.L. Fetterly, M.A. Steinbach, K. Singh, B. Marchand, M.D. Leslie, A.N. Hagedorn, E.N. Kodama, V.E. Marquez, S.H. Hughes, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6254-6264. PMID[24100493].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24261564">Dual Inhibition of HIV-1 Replication by Integrase-LEDGF Allosteric Inhibitors Is Predominant at the Post-integration Stage.</a> Le Rouzic, E., D. Bonnard, S. Chasset, J.M. Bruneau, F. Chevreuil, F. Le Strat, J. Nguyen, R. Beauvoir, C. Amadori, J. Brias, S. Vomscheid, S. Eiler, N. Levy, O. Delelis, E. Deprez, A. Saib, A. Zamborlini, S. Emiliani, M. Ruff, B. Ledoussal, F. Moreau, and R. Benarous. Retrovirology, 2013. 10(1): p. 144. PMID[24261564].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24060408">Isolation and Characterization of Antimicrobial Constituents of Searsia chirindensis L. (Anacardiaceae) Leaf Extracts.</a> Madikizela, B., M.A. Aderogba, and J. Van Staden. Journal of Ethnopharmacology, 2013. 150(2): p. 609-613. PMID[24060408].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24277152">Broadly Neutralizing Antibodies That Inhibit HIV-1 Cell to Cell Transmission.</a>Malbec, M., F. Porrot, R. Rua, J. Horwitz, F. Klein, A. Halper-Stromberg, J.F. Scheid, C. Eden, H. Mouquet, M.C. Nussenzweig, and O. Schwartz. The Journal of Experimental Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[24277152].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24080645">Biochemical Analysis of the Role of G118R-linked Dolutegravir Drug Resistance Substitutions in HIV-1 Integrase.</a> Quashie, P.K., T. Mesplede, Y.S. Han, T. Veres, N. Osman, S. Hassounah, R.D. Sloan, H.T. Xu, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6223-6235. PMID[24080645].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24150998">Computer-aided Design, Synthesis and Validation of 2-Phenylquinazolinone Fragments as CDK9 Inhibitors with anti-HIV-1 Tat-mediated Transcription Activity.</a>Sancineto, L., N. Iraci, S. Massari, V. Attanasio, G. Corazza, M.L. Barreca, S. Sabatini, G. Manfroni, N.R. Avanzi, V. Cecchetti, C. Pannecouque, A. Marcello, and O. Tabarrini. ChemMedChem, 2013. 8(12): p. 1941-1953. PMID[24150998].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24278016">The Effects of Somatic Hypermutation on Neutralization and Binding in the PGT121 Family of Broadly Neutralizing HIV Antibodies.</a> Sok, D., U. Laserson, J. Laserson, Y. Liu, F. Vigneault, J.P. Julien, B. Briney, A. Ramos, K.F. Saye, K. Le, A. Mahan, S. Wang, M. Kardar, G. Yaari, L.M. Walker, B.B. Simen, E.P. St John, P.Y. Chan-Hui, K. Swiderek, S.H. Kleinstein, G. Alter, M.S. Seaman, A.K. Chakraborty, D. Koller, I.A. Wilson, G.M. Church, D.R. Burton, and P. Poignard. Plos Pathogens, 2013. 9(11): p. e1003754. PMID[24278016].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24080659">BMS-986001, an HIV Nucleoside Reverse Transcriptase Inhibitor, Does Not Degrade Mitochondrial DNA in Long-term Primary Cultures of Cells Isolated from Human Kidney, Muscle, and Adipose Tissue.</a> Wang, F. and O.P. Flint. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6205-6212. PMID[24080659].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24282600">DB-02, a C-6-cyclohexylmethyl Substituted Pyrimidinone HIV-1 Reverse Transcriptase Inhibitor with Nanomolar Activity, Displays an Improved Sensitivity against K103N or Y181C Than S-DABOs.</a> Zhang, X.J., L.H. Lu, R.R. Wang, Y.P. Wang, R.H. Luo, C. Cong Lai, L.M. Yang, Y.P. He, and Y.T. Zheng. PloS One, 2013. 8(11): p. e81489. PMID[24282600].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1122-120513.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325919202159">Methylenebysphosphonates as New Class of HIV-1 Integrase Inhibitors.</a>Anisenko, A., J. Agapkina, S. Korolev, D. Yanvarev, S. Kochetkov, and M. Gottikh. FEBS Journal, 2013. 280: p. 365-366. ISI[000325919202159].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500278">Anti-HIV Activity of 5-Hydroxytyrosol, a Microbicidal Candidate.</a> Bedoya, L., M. Beltran, D. Aunon, E. Gomez-Acebo, and J. Alcami. AIDS Research and Human Retroviruses, 2013. 29(11): p. A108-A109. ISI[000326037500278].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325939000017">Synthesis of Novel Series of 6-Chloro-1,1-dioxo-1,4,2-benzodithiazine Derivatives with Potential Biological Activity.</a>Brzozowski, Z. and J. Slawinski. Journal of Heterocyclic Chemistry, 2013. 50(5): p. 1099-1107. ISI[000325939000017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325218700119">Anti-HIV Activity of Human Defensin 5 in Primary CD4+T Cells under Serum-deprived Conditions Is a Consequence of Defensin-mediated Cytotoxicity.</a> Ding, J., C. Tasker, K. Valere, T. Sihvonen, D.B. Descalzi-Montoya, W.Y. Lu, and T.L. Chang. PloS One, 2013. 8(9): p. e76038. ISI[000325218700119].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325919200274">Modified Oligonucleotides as Inhibitors of HIV-1 Enzymes.</a>Gottikh, M., S. Korolev, Y. Agapkina, O. Kondrashina, E. Knyazhanskaya, and V. Valuev-Elliston. FEBS Journal, 2013. 280: p. 85-85. ISI[000325919200274].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325988400056">Human MX2 Is an Interferon-induced Post-entry Inhibitor of HIV-1 Infection.</a> Goujon, C., O. Moncorge, H. Bauby, T. Doyle, C.C. Ward, T. Schaller, S. Hue, W.S. Barclay, R. Schulz, and M.H. Malim. Nature, 2013. 502(7472): p. 559-562. ISI[000325988400056].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326374600027">Small Molecule Fusion Inhibitors: Design, Synthesis and Biological Evaluation of (Z)-3-(5-(3-Benzyl-4-oxo-2-thioxothiazolidinylidene)methyl)-N-(3-carboxy -4-hydroxy)phenyl-2,5-dimethylpyrroles and Related Derivatives Targeting HIV-1 gp41.</a> He, X.Y., L. Lu, J.Y. Qiu, P. Zou, F. Yu, X.K. Jiang, L. Li, S.B. Jiang, S.W. Liu, and L. Xie. Bioorganic &amp; Medicinal Chemistry, 2013. 21(23): p. 7539-7548. ISI[000326374600027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326357800005">Transient Kinetic Analyses of the Ribonuclease H Cleavage Activity of HIV-1 Reverse Transcriptase in Complex with Efavirenz and/or a Beta-yhujaplicinol Analogue.</a> Herman, B.D. and N. Sluis-Cremer. Biochemical Journal, 2013. 455: p. 179-184. ISI[000326357800005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324481600098">Inhibition of HIV-1 Replication by Balsamin, a Ribosome Inactivating Protein of Momordica balsamina.</a> Kaur, I., M. Puri, Z. Ahmed, F.P. Blanchet, B. Mangeat, and V. Piguet. PloS One, 2013. 8(9). ISI[000324481600098].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500507">3-Hydroxyphthalic Anhydride-modified Human Serum Albumin as a Microbicide Candidate against HIV Type 1 Entry by Targeting Both Viral Envelope Glycoprotein gp120 and Cellular Receptor CD4.</a> Li, M.M., J.M. Duan, J.Y. Qiu, F. Yu, X.Y. Che, S.B. Jiang, and L. Li. AIDS Research and Human Retroviruses, 2013. 29(11): p. 1455-1464. ISI[000326037500507].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326089300009">Arylazolylthioacetanilide. Part 11: Design, Synthesis and Biological Evaluation of 1,2,4-Triazole Thioacetanilide Derivatives as Novel Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Li, Z.Y., Y. Cao, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, Y.M. Shen, and X.Y. Liu. Medicinal Chemistry, 2013. 9(7): p. 968-973. ISI[000326089300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500508">Effect of Combination Antiretroviral Therapy on Chinese Rhesus Macaques of Simian Immunodeficiency Virus Infection.</a>Ling, B.H., L. Rogers, A.M. Johnson, M. Piatak, J. Lifson, and R.S. Veazey. AIDS Research and Human Retroviruses, 2013. 29(11): p. 1465-1474. ISI[000326037500508].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500419">A Trimeric HIV-1 gp140-Baff Fusion Construct Enhances Mucosal anti-Trimeric HIV-1 gp140 IgA in Mice.</a> Liu, J., K. Clayton, Y. Li, M. Haaland, J. Schwartz, H. Sivanesan, A. Saiyed, A. Bozorgzad, W. Zhan, F. Yue, J. Rini, and M. Ostrowski. AIDS Research and Human Retroviruses, 2013. 29(11): p. A160-A160. ISI[000326037500419].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326121700007">InCl3 Catalyzed Highly Diastereoselective 3+2 Cycloaddition of 1,2-Cyclopropanated Sugars with Aldehydes: A Straightforward Synthesis of Persubstituted Bis-tetrahydrofurans and Perhydrofuro 2,3-B Pyrans.</a>Ma, X.F., Q. Tang, J. Ke, X.L. Yang, J.C. Zhang, and H.W. Shao. Organic Letters, 2013. 15(20): p. 5170-5173. ISI[000326121700007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325939000010">Effect of Substituents in the Syntheses of Phenyl-substituted Dibenzonaphthyridines.</a> Manoj, M. and K.J.R. Prasad. Journal of Heterocyclic Chemistry, 2013. 50(5): p. 1049-1063. ISI[000325939000010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326244600004">Structure-activity Evaluation of New Uracil-based Non-nucleoside Inhibitors of HIV Reverse Transcriptase.</a> Matyugina, E.S., V.T. Valuev-Elliston, A.N. Geisman, M.S. Novikov, A.O. Chizhov, S.N. Kochetkov, K.L. Seley-Radtke, and A.L. Khandazhinskaya. MedChemComm, 2013. 4(11): p. 1443-1451. ISI[000326244600004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500285">Aptamers That Inhibits Entry of Diverse HIV-1 Subtype C Is Not Cytotoxic.</a> Mufhandu, H.T., W.R. Campos, B. Mayosi, L. Morris, and M. Khati. AIDS Research and Human Retroviruses, 2013. 29(11): p. A111-A111. ISI[000326037500285].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500355">Synthetic Enhanced EP Delivered Ig DNA Vector Drives Biologically Relevant anti-HIV-1 Envelope Responses in Vivo.</a> Muthumani, K., S. Fligai, M. Wise, C. Tingey, J. Yan, K.E. Ugen, and D.B. Weiner. AIDS Research and Human Retroviruses, 2013. 29(11): p. A136-A136. ISI[000326037500355].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326374600014">Design, Synthesis and Biological Evaluation of 3-Benzyloxy-linked Pyrimidinylphenylamine Derivatives as Potent HIV-1 NNRTIs.</a>Rai, D., W.M. Chen, Y. Tian, X.W. Chen, P. Zhan, E. De Clercq, C. Pannecouque, J. Balzarini, and X.Y. Liu. Bioorganic &amp; medicinal chemistry, 2013. 21(23): p. 7398-7405. ISI[000326374600014]. <b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500124">Improving the Potency of Neutralizing anti-HIV-1 Antibodies with a CD4 Mimetic Compound.</a> Ramirez, K.P., T. Kuwata, Y. Maruta, K. Tanaka, M. Alam, K. Rahman, Y. Kawanami, I. Enomoto, H. Tamamura, K. Yoshimura, and S. Matsushita. AIDS Research and Human Retroviruses, 2013. 29(11): p. A51-A51. ISI[000326037500124].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326359800002">Synthesis and Evaluation of the Cytotoxic Activities of Some Isatin Derivatives.</a> Reddy, S.S., R. Pallela, D.M. Kim, M.S. Won, and Y.B. Shim. Chemical &amp; Pharmaceutical Bulletin, 2013. 61(11): p. 1105-1113. ISI[000326359800002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326037500036">Highly Potent Broadly Neutralizing Antibodies Lack Potential to Inhibit HIV-1 Cell-to-cell Transmission.</a> Reh, L., I.A. Abela, P. Rusert, and A. Trkola. AIDS Research and Human Retroviruses, 2013. 29(11): p. A16-A17. ISI[000326037500036].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326465600016">A New Isobenzofuranone from the Mangrove Endophytic Fungus Penicillium Sp (ZH58).</a> Yang, J.X., R.M. Huang, S.X. Qiu, Z.G. She, and Y.C. Lin. Natural Product Research, 2013. 27(20): p. 1902-1905. ISI[000326465600016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325919202157">Pyrophosphate Analogs Suppress Phosphorolytic Activity of Wild-type and AZT-resistant HIV-1 Reverse Transcriptase.</a> Yanvarev, D., N. Usanov, M. Kukhanova, and S. Kochetkov. FEBS Journal, 2013. 280: p. 365-365. ISI[000325919202157].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323879100009">Anti-HIV Effect of Liposomes Bearing CXCR4 Receptor Antagonist N15P.</a> Zhao, Y.X., H.X. Sun, X.Y. Li, X.M. Mo, and G.Z. Zhang. Tropical Journal of Pharmaceutical Research, 2013. 12(4): p. 503-509. ISI[000323879100009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1122-120513.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131031&amp;CC=US&amp;NR=2013289067A1&amp;KC=A1">Preparation of Bicyclic Compounds as HIV-1 Protease Inhibitors for Treating HIV.</a> Ghosh, A.K., B.D. Chapsal, and H. Mitsuya. Patent. 2013. 2013-13929395 20130289067: 82pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131031&amp;CC=WO&amp;NR=2013160810A2&amp;KC=A2">Preparation of Novel Betulinic acid Derivatives as HIV Inhibitors.</a>Panduranga Adulla, R., B. Parthasaradhi Reddy, V. Manohar Sharma, K. Rathnakar Reddy, M. Prem Kumar, K. Bhaskar Reddy, M. Narsingam, M. Venkati, L. Vl Subrahmanyam, and R. Mallikarjun Reddy. Patent. 2013. 2013-IB53120 2013160810: 198pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1122-120513.</p>

    <br />

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20131114&amp;CC=WO&amp;NR=2013169578A1&amp;KC=A1">C-17 Bicyclic amines of Triterpenoids with HIV Maturation Inhibitory Activity and Their Preparation.</a> Sin, N., Z. Liu, J. Swidorski, S.-Y. Sit, J. Chen, Y. Chen, N.A. Meanwell, and A. Regueiro-Ren. Patent. 2013. 2013-US39389 2013169578: 187pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1122-120513.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
