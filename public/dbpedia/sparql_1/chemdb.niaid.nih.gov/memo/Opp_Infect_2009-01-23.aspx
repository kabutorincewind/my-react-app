

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-01-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="UZ2R2kRqAm4SocFu4ABkUxrSs9pQOdtM1GJ7GqcJFmSmoSkb5Q/JZGec2DQ9+7EJih1I1YIromuyhJZttQwx8+8kjHTOaI0+yL2QmFEMrtTDnWp1jm9ULr/uatQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="65A228AA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">January 7, 2009 - January 20, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p>

    <p class="title">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19072692?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Celebesides A-C and theopapuamides B-D, depsipeptides from an Indonesian sponge that inhibit HIV-1 entry.</a></p>
    <p class="plaintext">Plaza A, Bifulco G, Keffer JL, Lloyd JR, Baker HL, Bewley CA.</p>
    <p class="plaintext">J Org Chem. 2009 Jan 16;74(2):504-12.</p>
    <p class="plaintext">PMID: 19072692 [PubMed - in process]            </p>

    <p class="title">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19055412?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Chemical modification and biological evaluation of new semisynthetic derivatives of 28,29-Didehydronystatin A1 (S44HP), a genetically engineered antifungal polyene macrolide antibiotic.</a></p>
    <p class="plaintext">Preobrazhenskaya MN, Olsufyeva EN, Solovieva SE, Tevyashova AN, Reznikova MI, Luzikov YN, Terekhova LP, Trenin AS, Galatenko OA, Treshalin ID, Mirchink EP, Bukhman VM, Sletta H, Zotchev SB.</p>
    <p class="plaintext">J Med Chem. 2009 Jan 8;52(1):189-96.</p>
    <p class="plaintext">PMID: 19055412 [PubMed - in process]            </p>

    <p class="memofmt2-2">TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</p>

    <p class="title">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19137534?ordinalpos=60&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">N-Benzylsalicylthioamides: Highly Active Potential Antituberculotics.</a></p>

    <p class="plaintext">Dole&#382;al R, Waisser K, Petrlíková E, Kune&#353; J, Kubicová L, Machácek M, Kaustová J, Dahse HM.</p>

    <p class="plaintext">Arch Pharm (Weinheim). 2009 Jan 9. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 19137534 [PubMed - as supplied by publisher]    </p> 

    <p class="title">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19137533?ordinalpos=61&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in-vitro Antimycobacterial Evaluation of 1-(Cyclopropyl/2,4-difluorophenyl/tert-butyl)-1,4-dihydro- 8-methyl-6-nitro-4-oxo-7-(substituted secondary amino)quinoline-3-carboxylic acids.</a></p>

    <p class="plaintext">Senthilkumar P, Dinakaran M, Chandraseakaran Y, Yogeeswari P, Sriram D.</p>

    <p class="plaintext">Arch Pharm (Weinheim). 2009 Jan 9. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 19137533 [PubMed - as supplied by publisher]    </p> 

    <p class="title">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19130456?ordinalpos=89&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Triclosan Derivatives: Towards Potent Inhibitors of Drug-Sensitive and Drug-Resistant Mycobacterium tuberculosis.</a></p>

    <p class="plaintext">Freundlich JS, Wang F, Vilchèze C, Gulten G, Langley R, Schiehser GA, Jacobus DP, Jacobs WR Jr, Sacchettini JC.</p>

    <p class="plaintext">ChemMedChem. 2009 Jan 7. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 19130456 [PubMed - as supplied by publisher]    

    <p class="title">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19084396?ordinalpos=116&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and evaluation of a novel series of pseudo-cinnamic derivatives as antituberculosis agents.</a></p>

    <p class="plaintext">Yoya GK, Bedos-Belval F, Constant P, Duran H, Daffé M, Baltas M.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2009 Jan 15;19(2):341-3. Epub 2008 Nov 27.</p>

    <p class="plaintext">PMID: 19084396 [PubMed - in process]            </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
