

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-360.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZCg/DIt7eQOtE5CEkim7XQ3fXG9UyW4QyPw7P+0/2IhXiSgpesSWY4x6KM4vRhP7pbPsPUozN9TJPSSQKrDmscqtZnTSccNkttAuR3cCcyjZvC8oVsdvmulKEkU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E492578" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-360-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59288   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Synthesis and Pharmacokinetics of Valopicitabine (NM283), an Efficient Prodrug of the Potent Anti-HCV Agent 2&#39;-C-Methylcytidine</p>

    <p>          Pierra, C, Amador, A, Benzaria, S, Cretton-Scott, E, D&#39;Amours, M, Mao, J, Mathieu, S, Moussa, A, Bridges, EG, Standring, DN, Sommadossi, JP, Storer, R, and Gosselin, G</p>

    <p>          J Med Chem <b>2006</b>.  49(22): 6614-6620</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064080&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17064080&amp;dopt=abstract</a> </p><br />

    <p>2.     59289   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p><b>          Mycobacterium tuberculosis beta-Ketoacyl-Acyl Carrier Protein (ACP) Reductase: Kinetic and Chemical Mechanisms</b> </p>

    <p>          Silva, RG, de Carvalho, LP, Blanchard, JS, Santos, DS, and Basso, LA</p>

    <p>          Biochemistry <b>2006</b>.  45(43): 13064-13073</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17059223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17059223&amp;dopt=abstract</a> </p><br />

    <p>3.     59290   OI-LS-360; EMBASE-OI-10/30/2006</p>

    <p class="memofmt1-2">          Modification at the C9 position of the marine natural product isoaaptamine and the impact on HIV-1, mycobacterial, and tumor cell activity</p>

    <p>          Gul, Waseem, Hammond, Nicholas L, Yousaf, Muhammad, Bowling, John J, Schinazi, Raymond F, Wirtz, Susan S, de Castro Andrews, Garcia, Cuevas, Carmen, and Hamann, Mark T</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M3BC48-1/2/b265c3734e159aea266fcd18b421ad89">http://www.sciencedirect.com/science/article/B6TF8-4M3BC48-1/2/b265c3734e159aea266fcd18b421ad89</a> </p><br />

    <p>4.     59291   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis Subverts Innate Immunity to Evade Specific Effectors</p>

    <p>          Loeuillet, C, Martinon, F, Perez, C, Munoz, M, Thome, M, and Meylan, PR</p>

    <p>          J Immunol <b>2006</b>.  177(9): 6245-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17056554&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17056554&amp;dopt=abstract</a> </p><br />

    <p>5.     59292   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Biological evaluation of pyrazinamide liposomes for treatment of Mycobacterium tuberculosis</p>

    <p>          El-Ridy, MS, Mostafa, DM, Shehab, A, Nasr, EA, and Abd, El-Alim S</p>

    <p>          Int J Pharm <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049192&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59293   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          National anti-tuberculosis drug resistance survey, 2002, in Myanmar</p>

    <p>          Ti, T, Lwin, T, Mar, TT, Maung, W, Noe, P, Htun, A, Kluge, HH, Wright, A, Aziz, MA, and Paramasivan, CN</p>

    <p>          Int J Tuberc Lung Dis <b>2006</b>.  10(10): 1111-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17044203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17044203&amp;dopt=abstract</a> </p><br />

    <p>7.     59294   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Addressing the threat of tuberculosis caused by extensively drug-resistant Mycobacterium tuberculosis</p>

    <p>          Anon</p>

    <p>          Wkly Epidemiol Rec <b>2006</b>.  81(41): 386-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17042081&amp;dopt=abstract</a> </p><br />

    <p>8.     59295   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Rapid detection of tuberculosis and drug-resistant tuberculosis</p>

    <p>          Iseman, MD and Heifets, LB</p>

    <p>          N Engl J Med <b>2006</b>.  355(15): 1606-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035655&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035655&amp;dopt=abstract</a> </p><br />

    <p>9.     59296   OI-LS-360; EMBASE-OI-10/30/2006</p>

    <p class="memofmt1-2">          Understanding the action of INH on a highly INH-resistant Mycobacterium tuberculosis strain using Genechips</p>

    <p>          Fu, Li M and Shinnick, Thomas M</p>

    <p>          Tuberculosis <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXK-4KJDWYW-1/2/a7effe9ed0e2dcea2d1ca6b1eb0590db">http://www.sciencedirect.com/science/article/B6WXK-4KJDWYW-1/2/a7effe9ed0e2dcea2d1ca6b1eb0590db</a> </p><br />

    <p>10.   59297   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          The SecA2 secretion factor of Mycobacterium tuberculosis promotes growth in macrophages and inhibits the host immune response</p>

    <p>          Kurtz, S, McKinnon, KP, Runge, MS, Ting, JP, and Braunstein, M</p>

    <p>          Infect Immun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030572&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59298   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of beta-ketosulfonamide adenylation inhibitors as potential antitubercular agents</p>

    <p>          Vannada, J, Bennett, EM, Wilson, DJ, Boshoff, HI, Barry, CE 3rd, and Aldrich, CC</p>

    <p>          Org Lett <b>2006</b>.  8(21): 4707-4710</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020283&amp;dopt=abstract</a> </p><br />

    <p>12.   59299   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Development of a novel dicistronic reporter-selectable hepatitis C virus replicon suitable for high throughput inhibitor screening</p>

    <p>          Hao, W, Herlihy, KJ, Zhang, NJ, Fuhrman, SA, Doan, C, Patick, AK, and Duggal, R</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060518&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17060518&amp;dopt=abstract</a> </p><br />

    <p>13.   59300   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          New resistance mechanisms to azole drugs in Aspergillus fumigatus and emergence of antifungal drugs-resistant A. fumigatus atypical strains</p>

    <p>          Mellado, E, Alcazar-Fuoli, L, Garcia-Effron, G, Alastruey-Izquierdo, A, Cuenca-Estrella, M, and Rodriguez-Tudela, JL</p>

    <p>          Med Mycol <b>2006</b>.  44 Suppl: 367-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17050465&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17050465&amp;dopt=abstract</a> </p><br />

    <p>14.   59301   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Antifolates as antimycotics? Connection between the folic acid cycle and the ergosterol biosynthesis pathway in Candida albicans</p>

    <p>          Navarro-Martinez, MD, Cabezas-Herrera, J, and Rodriguez-Lopez, JN</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046206&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17046206&amp;dopt=abstract</a> </p><br />

    <p>15.   59302   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Novel interferon alpha variant with improved inhibitory activity against HCV genotype 1 replication compared to IFN-alpha 2b therapy, in a subgenomic replicon system</p>

    <p>          Escuret, V, Martin, A, Durantel, D, Parent, R, Hantz, O, Trepo, C, Menguy, T, Bottius, E, Dardy, J, Maral, J, Escary, JL, and Zoulim, F</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030563&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17030563&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59303   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Crystal structure of the mycobacterium tuberculosis P450 CYP121-fluconazole complex reveals new azole drug-P450 binding mode</p>

    <p>          Seward, HE, Roujeinikova, A, McLean, KJ, Munro, AW, and Leys, D</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17028183&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17028183&amp;dopt=abstract</a> </p><br />

    <p>17.   59304   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          New antifungal flavonoid glycoside from Vitex negundo</p>

    <p>          Sathiamoorthy, B, Gupta, P, Kumar, M, Chaturvedi, AK, Shukla, PK, and Maurya, R</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17027268&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17027268&amp;dopt=abstract</a> </p><br />

    <p>18.   59305   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Fungicidal activity of five cathelicidin peptides against clinically isolated yeasts</p>

    <p>          Benincasa, M,  Scocchi, M, Pacor, S, Tossi, A, Nobili, D, Basaglia, G, Busetti, M, and Gennaro, R</p>

    <p>          J Antimicrob Chemother <b>2006</b>.  58(5): 950-959</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17023499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17023499&amp;dopt=abstract</a> </p><br />

    <p>19.   59306   OI-LS-360; EMBASE-OI-10/30/2006</p>

    <p class="memofmt1-2">          In vitro antifungal activities of Allium cepa, Allium sativum and ketoconazole against some pathogenic yeasts and dermatophytes</p>

    <p>          Shams-Ghahfarokhi, Masoomeh, Shokoohamiri, Mohammad-Reza, Amirrajab, Nasrin, Moghadasi, Behnaz, Ghajari, Ali, Zeini, Farideh, Sadeghi, Golnar, and Razzaghi-Abyaneh, Mehdi</p>

    <p>          Fitoterapia <b>2006</b>.  77(4): 321-323</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4JXPS3H-4/2/ee4ef5cd413333ef48d9b474902b00d8">http://www.sciencedirect.com/science/article/B6VSC-4JXPS3H-4/2/ee4ef5cd413333ef48d9b474902b00d8</a> </p><br />

    <p>20.   59307   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          Tuberculosis Recurrence and Mortality after Successful Treatment: Impact of Drug Resistance</p>

    <p>          Cox, H, Kebede, Y, Allamuratova, S, Ismailov, G, Davletmuratova, Z, Byrnes, G, Stone, C, Niemann, S, Rusch-Gerdes, S, Blok, L, and Doshetov, D</p>

    <p>          PLoS Med <b>2006</b>.  3(10)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020405&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17020405&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   59308   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          New antifolate inhibitors for Mycobacterium avium</p>

    <p>          Barrow, EW, Suling, WJ, Seitz, LE, Reynolds, RC, and Barrow, WW</p>

    <p>          Med Chem <b>2006</b>.  2(5): 505-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17017990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17017990&amp;dopt=abstract</a> </p><br />

    <p>22.   59309   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          In Vitro Activity of the Novel Oxazolidinones, DA-7867 and DA-7157, against Rapidly and Slowly Growing Mycobacteria</p>

    <p>          Vera-Cabrera, L, Brown-Elliott, BA, Wallace, RJ Jr, Ocampo-Candiani, J, Welsh, O, Choi, SH, and Molina-Torres, CA</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015632&amp;dopt=abstract</a> </p><br />

    <p>23.   59310   OI-LS-360; PUBMED-OI-10/30/2006</p>

    <p class="memofmt1-2">          The thiol-based redox networks of pathogens: Unexploited targets in the search for new drugs</p>

    <p>          Jaeger, T and Flohe, L</p>

    <p>          Biofactors <b>2006</b>.  27(1-4): 109-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17012768&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17012768&amp;dopt=abstract</a> </p><br />

    <p>24.   59311   OI-LS-360; EMBASE-OI-10/30/2006</p>

    <p class="memofmt1-2">          Antibacterial and antifungal activity of traditional medicinal plants used against venereal diseases in South Africa</p>

    <p>          Buwa, LV and van Staden, J</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  103(1): 139-142</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4HG6HBK-1/2/7e376b06a7582ac16d4eccf0a251aeea">http://www.sciencedirect.com/science/article/B6T8D-4HG6HBK-1/2/7e376b06a7582ac16d4eccf0a251aeea</a> </p><br />

    <p>25.   59312   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          In vitro antifungal, and anti-elastase activity of some aliphatic aldehydes from Olea europaea L. fruit</p>

    <p>          Battinelli, L, Daniele, C, Cristiani, M, Bisignano, G, Saija, A, and Mazzanti, G</p>

    <p>          PHYTOMEDICINE <b>2006</b>.  13(8): 558-563, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241021400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241021400004</a> </p><br />

    <p>26.   59313   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          The unique inhibition mechanism of intrabodies against hepatitis C virus serine protease and their additive effect with IFN-alpha</p>

    <p>          Gal-Tanamy, M, Zemel, R, Pupko, O, Bachmatov, L, Yi, M, Denisova, G, Gershoni, JM, Lemon, SM, Benhar, I, and Tur-Kaspa, R</p>

    <p>          JOURNAL OF CLINICAL VIROLOGY <b>2006</b>.  36: S24-S25, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300073">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300073</a> </p><br />

    <p>27.   59314   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus replication by PKR</p>

    <p>          Chang, J, Kato, N, Taniguchi, H, Bayasi, G, Tateishi, K, Jazag, A, Dharel, N, Moriyama, M, Muroyama, R, Shao, R, Kawabe, T, and Omata, M</p>

    <p>          JOURNAL OF CLINICAL VIROLOGY <b>2006</b>.  36: S26-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239067300077</a> </p><br />

    <p>28.   59315   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          The mshA gene encoding the glycosyltransferase of mycothiol biosynthesis is essential in Mycobacterium tuberculosis Erdman</p>

    <p>          Buchmeier, N and Fahey, RC</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2006</b>.  264(1): 74-79, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240919800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240919800011</a> </p><br />

    <p>29.   59316   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          Recombinant single-chain anti-idiotypic antibody: An effective fungal beta-1,3-glucan synthase inhibitor</p>

    <p>          Selvakumar, D, Karim, N, Miyamoto, M, Furuichi, Y, and Komiyama, T</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  29(9): 1848-1853, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240970900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240970900012</a> </p><br />

    <p>30.   59317   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          Antifungal effect of amentoflavone derived from Selaginella tamariscina</p>

    <p>          Jung, HJ, Sung, WS, Yeo, SH, Kim, HS, Lee, IS, Woo, ER, and Lee, DG</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2006</b>.  29(9): 746-751, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240882200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240882200005</a> </p><br />

    <p>31.   59318   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          Novel robust hepatitis C virus mouse efficacy model</p>

    <p>          Zhu, Q, Oei, Y, Mendel, DB, Garrett, EN, Patawaran, MB, Hollenbach, PW, Aukerman, SL, and Weiner, AJ</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(10): 3260-3268, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241021700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241021700005</a> </p><br />

    <p>32.   59319   OI-LS-360; WOS-OI-10/22/2006</p>

    <p class="memofmt1-2">          Application of factor analysis on Mycobacterium tuberculosis transcriptional responses for drug clustering, drug target, and pathway detections</p>

    <p>          Chaijaruwanich, J, Khamphachua, J, Prasitwattanaseree, S, Warit, S, and Palittapongarnpim, P</p>

    <p>          ADVANCED DATA MINING AND APPLICATIONS, PROCEEDINGS <b>2006</b>.  4093: 835-844, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240088200091">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240088200091</a> </p><br />

    <p>33.   59320   OI-LS-360; WOS-OI-10/30/2006</p>

    <p class="memofmt1-2">          Production of infectious hepatitis C virus by well-differentiated, growth-arrested human hepatoma-derived cells</p>

    <p>          Sainz, B and Chisari, FV</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(20): 10253-10257, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241046800034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241046800034</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
