

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-05-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XR3pSnczlXocfazCyQfUkoONURu5tTmIimsEx1EZpFrmuHrIgDi9QaZG4rgKhosaS9mCff+fxQGE2iW6IS80AQ+1mZP1oAolrQCkVeR7HPrt6eh2KAMD34dO0kM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9C84154F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 10 - May 23, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23678171">Novel Inhibitors of SARS-CoV Entry Acting by Three Distinct Mechanisms.</a> Adedeji, A.O., W. Severson, C. Jonsson, K. Singh, S.R. Weiss, and S.G. Sarafianos. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23678171].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23659183">Bifunctional Inhibition of Human Immunodeficiency Virus Type 1 Reverse Transcriptase: Mechanism and Proof-of-concept as a Novel Therapeutic Design Strategy.</a> Bailey, C.M., T.J. Sullivan, P. Iyidogan, J. Tirado-Rives, R. Chung, J. Ruiz-Caro, E. Mohamed, W. Jorgensen, R. Hunter, and K.S. Anderson. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23659183].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23544648">Sulfation and Biological Activities of Konjac Glucomannan.</a> Bo, S., T. Muschin, T. Kanamoto, H. Nakashima, and T. Yoshida. Carbohydrate Polymers, 2013. 94(2): p. 899-903. PMID[23544648].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23669388">Characterization of Antiviral Activity of a Benzamide Derivative AH0109 against HIV-1 Infection.</a> Chen, L., Z. Ao, K. Danappa Jayappa, G. Kobinger, S. Liu, G. Wu, M.A. Wainberg, and X. Yao. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23669388].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23683135">Benzofuran Derivatives: A Patent Review.</a> Dawood, K.M. Expert Opinion on Therapeutic Patents, 2013. <b>[Epub ahead of print]</b>. PMID[23683135].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23496828">Novel Inhibitor Binding Site Discovery on HIV-1 Capsid N-terminal Domain by NMR and X-ray Crystallography.</a> Goudreau, N., C.T. Lemke, A.M. Faucher, C. Grand-Maitre, S. Goulet, J.E. Lacoste, J. Rancourt, E. Malenfant, J.F. Mercier, S. Titolo, and S.W. Mason. ACS Chemical Biology, 2013. 8(5): p. 1074-1082. PMID[23496828].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23672887">Inhibition of HIV Replication in Vitro by Clinical Immunosuppressants and Chemotherapeutic Agents.</a> Hawley, T., M. Spear, J. Guo, and Y. Wu. Cell &amp; Bioscience, 2013. 3(1): p. 22. PMID[23672887].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23691059">Mice Transgenic for CD4-Specific Human CD4, CCR5 and Cyclin T1 Expression: A New Model for Investigating HIV-1 Transmission and Treatment Efficacy.</a> Seay, K., X. Qi, J.H. Zheng, C. Zhang, K. Chen, M. Dutta, K. Deneroff, C. Ochsenbauer, J.C. Kappes, D.R. Littman, and H. Goldstein. PloS One, 2013. 8(5): p. e63537. PMID[23691059].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23689710">HIV-1 Resistance Mechanism to an Electrostatically Constrained Peptide Fusion Inhibitor That Is Active against T-20-resistant Strains.</a> Shimane, K., K. Kawaji, F. Miyamoto, S. Oishi, K. Watanabe, Y. Sakagami, N. Fujii, K. Shimura, M. Matsuoka, M. Kaku, S.G. Sarafianos, and E.N. Kodama. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23689710].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23659687">Design, Synthesis and Biological Evaluation of Novel 3,5-Disubstituted-1,2,6-thiadiazine-1,1-dione Derivatives as HIV-1 NNRTIs.</a> Tian, Y., D. Rai, P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, H. Liu, and X. Liu. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23659687].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23683267">A Combined 3D-QSAR and Docking Studies for the in-Silico Prediction of HIV-protease Inhibitors.</a> Ul-Haq, Z., S. Usmani, H. Shamshad, U. Mahmood, and S.A. Halim. Chemistry Central Journal, 2013. 7(1): p. 88. PMID[23683267].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23688427">A Synthetic Bivalent Ligand of CXCR4 Inhibits HIV Infection.</a> Xu, Y., S. Duggineni, S. Espitia, D.D. Richman, J. An, and Z. Huang. Biochemical and Biophysical Research Communications, 2013. <b>[Epub ahead of print]</b>. PMID[23688427].
    <br />
    <b>[PubMed]</b>. HIV_0510-052313.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317434600001">Nephrotoxicity of Antiretroviral Agents: Is the List Getting Longer?</a> Fine, D.M. and J.E. Gallant. Journal of Infectious Diseases, 2013. 207(9): p. 1349-1351. ISI[000317434600001].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317446800005">Study of Addition of Difluorocarbene on Double Bond of Triterpenes.</a> Biedermann, D., M. Urban, M. Budesinsky, M. Kvasnica, and J. Sarek. Journal of Fluorine Chemistry, 2013. 148: p. 30-35. ISI[000317446800005].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317168200026">New Triterpenoids from the Fruits of Schisandra wilsoniana and Their Biological Activities.</a> Gao, X.M., Y.Q. Li, L.D. Shu, Y.Q. Shen, L.Y. Yang, L.M. Yang, Y.T. Zheng, H.D. Sun, W.L. Xiao, and Q.F. Hu. Bulletin of the Korean Chemical Society, 2013. 34(3): p. 827-830. ISI[000317168200026].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317733400001">Gelsolin Activity Controls Efficient Early HIV-1 Infection.</a> Garcia-Exposito, L., S. Ziglio, J. Barroso-Gonzalez, L. de Armas-Rillo, M.S. Valera, D. Zipeto, J.D. Machado, and A. Valenzuela-Fernandez. Retrovirology, 2013. 10. ISI[000317733400001].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316912800043">Synthesis, Biological Evaluation, and Molecular Modeling Studies of New 1,3,4-Oxadiazole- and 1,3,4-Thiadiazole-substituted 4-oxo-4H-pyrido[1,2-a]pyrimidines as anti-HIV-1 Agents.</a> Hajimahdi, Z., A. Zarghi, R. Zabihollahi, and M.R. Aghasadeghi. Medicinal Chemistry Research, 2013. 22(5): p. 2467-2475. ISI[000316912800043].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317371300017">Mechanism of Resistance to S138A Substituted Enfuvirtide and Its Application to Peptide Design.</a> Izumi, K., K. Kawaji, F. Miyamoto, K. Shimane, K. Shimura, Y. Sakagami, T. Hattori, K. Watanabe, S. Oishi, N. Fujii, M. Matsuoka, M. Kaku, S.G. Sarafianos, and E.N. Kodama. International Journal of Biochemistry &amp; Cell Biology, 2013. 45(4): p. 908-915. ISI[000317371300017].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317383200018">Structural and Functional Role of INI1 and LEDGF in the HIV-1 Preintegration Complex.</a> Maillot, B., N. Levy, S. Eiler, C. Crucifix, F. Granger, L. Richert, P. Didier, J. Godet, K. Pradeau-Aubreton, S. Emiliani, A. Nazabal, P. Lesbats, V. Parissi, Y. Mely, D. Moras, P. Schultz, and M. Ruff. PloS One, 2013. 8(4). ISI[000317383200018].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316686600004">Oligo(2&#39;-O-methylribonucleotides) and Their Derivatives: IV. Conjugates of Oligo(2&#39;-O-methylribonucleotides) with Minor Groove Binders and Intercalators: Synthesis, Properties, and Application.</a> Novopashina, D.S., A.N. Sinyakov, V.A. Ryabinin, L. Perrouault, C. Giovannangeli, A.G. Venyaminova, and A.S. Boutorine. Russian Journal of Bioorganic Chemistry, 2013. 39(2): p. 138-152. ISI[000316686600004].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316808200016">Chemoselective Synthesis of N-substituted alpha-Amino-alpha &#39;-chloro Ketones Via Chloromethylation of Glycine-derived Weinreb Amides.</a> Pace, V., W. Holzer, G. Verniest, A.R. Alcantara, and N. De Kimpe. Advanced Synthesis &amp; Catalysis, 2013. 355(5): p. 919-926. ISI[000316808200016].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317098100004">Histone Deacetylase Inhibitor MC1293 Induces Latent HIV-1 Reactivation by Histone Modification in Vitro Latency Cell Lines.</a> Qu, X.Y., H. Ying, X.H. Wang, C.J. Kong, X. Zhou, P.F. Wang, and H.Z. Zhu. Current HIV Research, 2013. 11(1): p. 24-29. ISI[000317098100004].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317209300009">Piperidin-4-one: The Potential Pharmacophore.</a> Sahu, S.K., B.K. Dubey, A.C. Tripathi, M. Koshy, and S.K. Saraf. Mini-reviews in Medicinal Chemistry, 2013. 13(4): p. 565-583. ISI[000317209300009].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316686600006">Synthesis, Properties, and anti-HIV Activity of New Lipophilic 3&#39;-Azido-3&#39;-deoxythymidine Conjugates Containing Functional Phosphoric Linkages.</a> Shastina, N.S., T.Y. Maltseva, L.N. D&#39;Yakova, O.A. Lobach, M.S. Chataeva, D.N. Nosik, and V.I. Shvetz. Russian Journal of Bioorganic Chemistry, 2013. 39(2): p. 161-169. ISI[000316686600006].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317168200032">Synthesis of Novel 2&#39;-Spirocyclopropanoid 4&#39;-deoxythreosyl phosphonic acid Nucleoside Analogues.</a> Shen, G.H. and J.H. Hong. Bulletin of the Korean Chemical Society, 2013. 34(3): p. 868-874. ISI[000317168200032].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316912500005">Polyoxometalates as Versatile Enzyme Inhibitors.</a> Stephan, H., M. Kubeil, F. Emmerling, and C.E. Muller. European Journal of Inorganic Chemistry, 2013(10-11): p. 1585-1594. ISI[000316912500005].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316686600010">Synthesis and Biological Activity of Mono- and Diamides of 2,3-Secotriterpene acids.</a> Tolmacheva, I.A., E.V. Igosheva, Y.B. Vikharev, V.V. Grishko, O.V. Savinova, E.I. Boreko, and V.F. Eremin. Russian Journal of Bioorganic Chemistry, 2013. 39(2): p. 186-193. ISI[000316686600010].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317743200016">Potential of the Novel Antiretroviral Drug Rilpivirine to Modulate the Expression and Function of Drug Transporters and Drug-metabolising Enzymes in Vitro.</a> Weiss, J. and W.E. Haefeli. International Journal of Antimicrobial Agents, 2013. 41(5): p. 484-487. ISI[000317743200016].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317926100010">Synthesis and Biological Evaluation of Novel Hydroxylphenethyl-S-DACOs as High Active anti-HIV Agents.</a> Wu, D.C., D.M. Zhuang, X.F. Liu, L.H. Lu, H. Wang, C. Li, C.C. Lai, J.Y. Li, and Y.P. He. Letters in Drug Design &amp; Discovery, 2013. 10(3): p. 271-276. ISI[000317926100010].
    <br />
    <b>[WOS]</b>. HIV_0510-052313.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
