

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-341.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7rKhPHj/S3TUaYaslQHZE4thLoistQbBW41SbVQeO6KWg7z6SLjqpzqyLFW9YopPnrQ35yW5EuuEkRdZpgl0KyjjMbkHKboRygJqrlKDUrPuri/t9W5hewtNB/Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9444B253" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-341-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48455   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          CryptoDB: a Cryptosporidium bioinformatics resource update</p>

    <p>          Heiges, Mark, Wang, Haiming, Robinson, Edward, Aurrecoechea, Cristina, Gao, Xin, Kaluskar, Nivedita, Rhodes, Philippa, Wang, Sammy, He, Cong-Zhou, Su, Yanqi, Miller, John, Kraemer, Eileen, and Kissinger, Jessica C</p>

    <p>          Nucleic Acids Research <b>2006</b>.  34(Supplement1): D419-D422</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     48456   OI-LS-341; PUBMED-OI-2/6/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of some 2-substituted 3-formyl- and 3-cyano-5,6-dichloroindole nucleosides</p>

    <p>          Williams, JD, Drach, JC, and Townsend, LB</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(10-12): 1613-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438038&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438038&amp;dopt=abstract</a> </p><br />

    <p>3.     48457   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro activities of ferrocenic aminohydroxynaphthoquinones against Toxoplasma gondii and Plasmodium falciparum</p>

    <p>          Baramee, Apiwat, Coppin, Alexandra, Mortuaire, Marlene, Pelinski, Lydie, Tomavo, Stanislas, and Brocard, Jacques</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(5): 1294-1302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     48458   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Lipid biology of Apicomplexa: perspectives for new drug targets, particularly for Toxoplasma gondii</p>

    <p>          Sonda, Sabrina and Hehl, Adrian B</p>

    <p>          Trends in Parasitology <b>2006</b>.  22(1): 41-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48459   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Preclinical pharmacokinetics and metabolism of a potent non-nucleoside inhibitor of the hepatitis C virus NS5B polymerase</p>

    <p>          Giuliano, C, Fiore, F, Di, Marco A, Velazquez, JP, Bishop, A, Bonelli, F, Gonzalez-Paz, O, Marcucci, I, Harper, S, Narjes, F, Pacini, B, Monteagudo, E, Migliaccio, G, Rowley, M, and Laufer, R</p>

    <p>          XENOBIOTICA <b>2005</b>.  35(10-11): 1035-1054, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234427600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234427600008</a> </p><br />

    <p>6.     48460   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Purification and characterization of the Mycobacterium tuberculosis FabD2, a novel malonyl-CoA:AcpM transacylase of fatty acid synthase</p>

    <p>          Huang, Yi-Shu, Ge, Jing, Zhang, Hong-Mei, Lei, Jian-Qiang, Zhang, Xue-Lian, and Wang, Hong-Hai </p>

    <p>          Protein Expression and Purification <b>2006</b>.  45(2): 393-399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     48461   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Specific molecular mechanisms involved in the antiviral response to murine cytomegalovirus</p>

    <p>          Sumaria, N, Van Dommelen, S, Smyth, M, and Degli-Esposti, M</p>

    <p>          TISSUE ANTIGENS <b>2005</b>.  66(5): 555-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542700580">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233542700580</a> </p><br />

    <p>8.     48462   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          3D-QSAR studies on antitubercular thymidine monophosphate kinase inhibitors based on different alignment methods</p>

    <p>          Aparna, V, Jeevan, J, Ravi, M, Desiraju, GR, and Gopalakrishnan, B</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(4): 1014-1020</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48463   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Developing an Antituberculosis Compounds Database and Data Mining in the Search of a Motif Responsible for the Activity of a Diverse Class of Antituberculosis Agents</p>

    <p>          Prakash, Om and Ghosh, Indira</p>

    <p>          Journal of Chemical Information and Modeling <b>2006</b>.  46(1): 17-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48464   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Identification of a nitroimidazo-oxazine-specific protein involved in PA-824 resistance in Mycobacterium tuberculosis</p>

    <p>          Manjunatha, UH, Boshoff, H, Dowd, CS, Zhang, L, Albert, TJ, Norton, JE, Daniels, L, Dickl, T, Pang, SS, and Barry, CE</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2006</b>.  103(2): 431-436, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234624100033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234624100033</a> </p><br />

    <p>11.   48465   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Novel gyrase mutations in quinolone-resistant and -hypersusceptible clinical isolates of Mycobacterium tuberculosis: Functional analysis of mutant enzymes</p>

    <p>          Aubry, Alexandra, Veziris, Nicolas, Cambau, Emmanuelle, Truffot-Pernot, Chantal, Jarlier, Vincent, and Fisher, LMark</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2006</b>.  50(1): 104-112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48466   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Substituted quinolones</p>

    <p>          Schohe-Loop, Rudolf, Zimmermann, Holger, Henninger, Kerstin, Paulsen, Daniela, Roelle, Thomas, Lang, Dieter, Thede, Kai, Fuerstner, Chantal, Brueckner, David, Koebberling, Johannes, and Bauser, Marcus</p>

    <p>          PATENT:  WO <b>2006008046</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 115 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48467   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Synthesis, antiviral, and antitumor activity of 2-substituted purine methylenecyclopropane analogues of nucleosides</p>

    <p>          Qin, Xinrong, Chen, Xinchao, Wang, Kun, Polin, Lisa, Kern, Earl R, Drach, John C, Gullen, Elizabeth, Cheng, Yung-Chi, and Zemlicka, Jiri</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(4): 1247-1254</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48468   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Anti-viral uses of borinic acid complexes</p>

    <p>          Bellinger-Kawahara, Carolyn, Maples, Kirk R, and Plattner, Jacob J</p>

    <p>          PATENT:  US <b>2006019927</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005-21887  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48469   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Imidazole and thiazole derivatives as antiviral agents</p>

    <p>          Conte, Immacolata, Hernando, Jose Ignacio Martin, Malancona, Savina, Ontoria Ontoria, Jesus Maria, and Stansfield, Ian</p>

    <p>          PATENT:  WO <b>2006008556</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48470   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          The face of future hepatitis C antiviral drug development: Recent biological and virologic advances and their translation to drug development and clinical practice</p>

    <p>          McHutchison, John G, Bartenschlager, Ralf, Patel, Keyur, and Pawlotsky, Jean-Michel</p>

    <p>          Journal of Hepatology <b>2006</b>.  44(2): 411-421</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   48471   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Antiviral prodrugs - the development of successful prodrug strategies for antiviral chemotherapy</p>

    <p>          De Clercq, Erik and Field, Hugh J</p>

    <p>          British Journal of Pharmacology <b>2006</b>.  147(1): 1-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   48472   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Preparation of 5-aza-7-deazadeazapurine and C-branched nucleosides as antiviral agents for treating Flaviviridae</p>

    <p>          Gosselin, Gilles, La Colla, Paolo, Seela, Frank, Storer, Richard, Dukhan, David, and Leroy, Frederic</p>

    <p>          PATENT:  WO <b>2006000922</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 115 pp.</p>

    <p>          ASSIGNEE:  (Idenix (Cayman) Limited, Cayman I.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48473   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Antioxidant and antimycobacterial activities of Tabernaemontana catharinensis extracts obtained by supercritical CO2 + cosolvent</p>

    <p>          Pereira, CG, Leal, PF, Sato, DN, and Meireles, MAA</p>

    <p>          JOURNAL OF MEDICINAL FOOD <b>2005</b>.  8(4): 533-538, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234554400019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234554400019</a> </p><br />

    <p>20.   48474   OI-LS-341; SCIFINDER-OI-1/31/2006</p>

    <p class="memofmt1-2">          Preparation of aza nucleoside analogs and their use as antiviral agents and inhibiting viral RNA polymerases</p>

    <p>          Babu, Yarlagadda S, Chand, Pooran, Ghosh, Ajit K, Kotian, Pravin L, and Kumar, Satish V</p>

    <p>          PATENT:  WO <b>2006002231</b>  ISSUE DATE:  20060105</p>

    <p>          APPLICATION: 2005  PP: 75 pp.</p>

    <p>          ASSIGNEE:  (Biocryst Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   48475   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 6-(alkyn-1-yl)furo[2,3-d]pyrimidin-2(3H)-one base and nucleoside derivatives</p>

    <p>          Robins, MJ, Miranda, K, Rajwanshi, VK, Peterson, MA, Andrei, G, Snoeck, R, De, Clercq E, and Balzarini, J</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(1): 391-398, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234575300040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234575300040</a> </p><br />

    <p>22.   48476   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          Diagnosis and therapy in the field of hepatology - 2005 Hepatitis C: From discovery of the virus to individualized therapy</p>

    <p>          Manns, MP</p>

    <p>          DEUTSCHE MEDIZINISCHE WOCHENSCHRIFT <b>2005</b>.  130: S197-+, 49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234378300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234378300001</a> </p><br />

    <p>23.   48477   OI-LS-341; WOS-OI-1/29/2006</p>

    <p class="memofmt1-2">          A genotype 2b NS5B polymerase with novel substitutions supports replication of a chimeric HCV 1b : 2b replicon containing a genotype 1b NS3-5A background</p>

    <p>          Graham, DJ, Stahlhut, M, Flores, O, Olsen, DB, Hazuda, DJ, LaFemina, RL, and Ludmerer, SW</p>

    <p>          ANTIVIRAL RESEARCH <b>2006</b>.  69(1): 24-30, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234525900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234525900003</a> </p><br />

    <p>24.   48478   OI-LS-341; WOS-OI-2/5/2006</p>

    <p class="memofmt1-2">          Quantitative proteomic analysis of drug-induced changes in mycobacteria</p>

    <p>          Hughes, MA, Silva, JC, Geromanos, SJ, and Townsend, CA</p>

    <p>          JOURNAL OF PROTEOME RESEARCH <b>2006</b>.  5(1): 54-63, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234668300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234668300005</a> </p><br />

    <p>25.   48479   OI-LS-341; WOS-OI-2/5/2006</p>

    <p class="memofmt1-2">          The potential impact of structural genomics on tuberculosis drug discovery</p>

    <p>          Arcus, VL, Lott, JS, Johnston, JM, and Baker, EN</p>

    <p>          DRUG DISCOVERY TODAY <b>2006</b>.  11(1-2): 28-34, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234823500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234823500005</a> </p><br />

    <p>26.   48480   OI-LS-341; WOS-OI-2/5/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel benzimidazol/benzoxazolylethoxypiperidone oximes</p>

    <p>          Balasubramanian, S, Aridoss, G, Parthiban, P, Ramalingan, C, and Kabilan, S</p>

    <p>          BIOLOGICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  29(1): 125-130, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234760200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234760200026</a> </p><br />

    <p>27.   48481   OI-LS-341; WOS-OI-2/5/2006</p>

    <p class="memofmt1-2">          Establishment of stable HeLa cell lines expressing enzymatically active hepatitis C virus RNA polymerase</p>

    <p>          Kong, LB, Ye, LB, Ye, L, Timani, KA, Zheng, Y, Liao, QJ, Li, BZ, and Gao, B</p>

    <p>          ARCHIVES OF VIROLOGY <b>2006</b>.  151(2): 361-367, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234807300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234807300012</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
