

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-03-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="W9Z60k9KOQKYCOlRdHD/VdzjuuoiJRlmdU4XDvBbWSYGpdsuZwvUQ8IgsYMFTA34vTows+vXzjLiBPuTpg6ceazkF5M9QQWXYclwUIjfFjNuifh0ukbj1eUqEHk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="88F73C3E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 1 - March 14, 2013</h1>

    <h2>FIV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23375089">9-[2-(R)-(Phosphonomethoxy)propyl]-2,6-diaminopurine (R)-PMPDAP and Its Prodrugs: Optimized Preparation, Including Identification of By-products Formed, and Antiviral Evaluation in Vitro.</a> Krecmerova, M., P. Jansa, M. Dracinsky, P. Sazelova, V. Kasicka, J. Neyts, J. Auwerx, E. Kiss, N. Goris, G. Stepan, and Z. Janeba. Bioorganic &amp; Medicinal Chemistry, 2013. 21(5): p. 1199-1208; PMID[23375089].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413. </p>

    <h2>HCMV</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23341371">Allium Sativum-derived Allitridin Inhibits TREG Amplification in Cytomegalovirus Infection.</a> Li, Y.N., F. Huang, X.L. Liu, S.N. Shu, Y.J. Huang, H.J. Cheng, and F. Fang. Journal of Medical Virology, 2013. 85(3): p. 493-500; PMID[23341371].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23385212">Synthesis of Hemslecin A Derivatives: A New Class of Hepatitis B Virus Inhibitors.</a> Guo, R.H., C.A. Geng, X.Y. Huang, Y.B. Ma, Q. Zhang, L.J. Wang, X.M. Zhang, R.P. Zhang, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1201-1205; PMID[23385212].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23369536">Synthesis and Biological Evaluation of Nucleoside Analogues Than Contain Silatrane on the Basis of the Structure of Acyclovir (ACV) as Novel Inhibitors of Hepatitis B Virus (HBV).</a> Han, A., L. Li, K. Qing, X. Qi, L. Hou, X. Luo, S. Shi, and F. Ye. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1310-1314; PMID[23369536].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23409936">Structure-based Discovery of Pyrazolobenzothiazine Derivatives as Inhibitors of Hepatitis C Virus Replication.</a> Barreca, M.L., G. Manfroni, P. Leyssen, J. Winquist, N. Kaushik-Basu, J. Paeshuyse, R. Krishnan, N. Iraci, S. Sabatini, O. Tabarrini, A. Basu, U.H. Danielson, J. Neyts, and V. Cecchetti. Journal of Medicinal Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23409936].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23466667">Inhibition of Hepatitis C Viral RNA-dependent RNA Polymerase by alpha-P-Boranophosphate Nucleotides: Exploring a Potential Strategy for Mechanism-based HCV Drug Design.</a> Cheek, M.A., M.L. Sharaf, M.I. Dobrikov, and B.R. Shaw. Antiviral Research, 2013.  <b>[Epub ahead of print]</b>; PMID[23466667].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23254429">Lucidone Suppresses Hepatitis C Virus Replication by NRF2-mediated Heme Oxygenase-1 Induction.</a> Chen, W.C., S.Y. Wang, C.C. Chiu, C.K. Tseng, C.K. Lin, H.C. Wang, and J.C. Lee. Antimicrobial Agents and Chemotherapy, 2013. 57(3): p. 1180-1191; PMID[23254429].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23448131">Sofosbuvir, a Nucleotide Polymerase Inhibitor, for the Treatment of Chronic Hepatitis C Virus Infection.</a> Herbst, D.A., Jr. and K.R. Reddy. Expert Opinion on Investigational Drugs, 2013.  <b>[Epub ahead of print]</b>; PMID[23448131].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p> 

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23213025">Hepatoprotective and Antiviral Functions of Silymarin Components in Hepatitis C Virus Infection.</a> Polyak, S.J., P. Ferenci, and J.M. Pawlotsky. Hepatology, 2013. 57(3): p. 1262-1271; PMID[23213025].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413. </p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23406520">A Highly Convergent and Efficient Synthesis of a Macrocyclic Hepatitis C Virus Protease Inhibitor BI 201302.</a> Wei, X., C. Shu, N. Haddad, X. Zeng, N.D. Patel, Z. Tan, J. Liu, H. Lee, S. Shen, S. Campbell, R.J. Varsolona, C.A. Busacca, A. Hossain, N.K. Yee, and C.H. Senanayake. Organic Letters, 2013. 15(5): p. 1016-1019; PMID[23406520].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23178400">HIV-1 and HSV-1 Virus Activities of Some New Polycyclic Nucleoside Pyrene Candidates.</a> Khalifa, N.M., M.A. Al-Omar, G. Amr Ael, and M.E. Haiba. International Journal of Biological Macromolecules, 2013. 54: p. 51-56; PMID[23178400].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0301-031413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312999400012">New Therapeutic Strategies in HCV: Second-generation Protease Inhibitors.</a> Clark, V.C., J.A. Peter, and D.R. Nelson. Liver International, 2013. 33: p. 80-84; ISI[000312999400012].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313960900002">Bicyclic Furano-, Pyrrolo-, and Thiopheno 2,3-D Derivatives of Pyrimidine Nucleosides: Synthesis and Antiviral Properties.</a> Ivanov, M.A. and L.A. Aleksandrova. Russian Journal of Bioorganic Chemistry, 2013. 39(1): p. 22-39; ISI[000313960900002].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314023600058">Green Tea Phenolic Epicatechins Inhibit Hepatitis C Virus Replication Via Cycloxygenase-2 and Attenuate Virus-induced Inflammation.</a> Lin, Y.T., Y.H. Wu, C.K. Tseng, C.K. Lin, W.C. Chen, Y.C. Hsu, and J.C. Lee. Plos One, 2013. 8(1): p. e54466; ISI[000314023600058].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314288900041">Identification of Type I and Type II Interferon-induced Effectors Controlling Hepatitis C Virus Replication.</a> Metz, P., E. Dazert, A. Ruggieri, J. Mazur, L. Kaderali, A. Kaul, U. Zeuge, M.P. Windisch, M. Trippler, V. Lohmann, M. Binder, M. Frese, and R. Bartenschlager. Hepatology, 2012. 56(6): p. 2082-2093; ISI[000314288900041].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314335100001">Asymmetric Synthesis of Host-directed Inhibitors of Myxoviruses.</a> Moore, T.W., K. Sana, D. Yan, P. Thepchatri, J.M. Ndungu, M.T. Saindane, M.A. Lockwood, M.G. Natchus, D.C. Liotta, R.K. Plemper, J.P. Snyder, and A.M. Sun. Beilstein Journal of Organic Chemistry, 2013. 9: p. 197-203; ISI[000314335100001].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314008700049">Stereoselective Addition of 2-Phenyloxazol-4-yl trifluoromethanesulfonate to N-Sulfinyl imines: Application to the Synthesis of the HCV Protease Inhibitor Boceprevir.</a> Morris, W.J., K.K. Muppalla, C. Cowden, and R.G. Ball. Journal of Organic Chemistry, 2013. 78(2): p. 706-710; ISI[000314008700049].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0301-031413.  </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
