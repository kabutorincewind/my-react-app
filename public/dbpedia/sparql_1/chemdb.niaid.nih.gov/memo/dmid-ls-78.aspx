

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-78.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yJrRD0t8oFSlSUUadfHZ8ZpUs11btcewIw6NpjBd/19hNBrLRNlJHfvNvXY5i9RSdrlEWBiFmC+/Y/HLOwDPt8vNYnRFFTCoqGojslbVJXPbKFOxx5D3eyUN4S4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3AA3EF0F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-78-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6500   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluation of novel non-nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum chemotherapeutic properties</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(22): 5865-73</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498662&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498662&amp;dopt=abstract</a> </p><br />

    <p>2.         6501   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          The synergistic effects of betulin with acyclovir against herpes simplex viruses</p>

    <p>          Gong, Y, Raj, KM, Luscombe, CA, Gadawski, I, Tam, T, Chu, J, Gibson, D, Carlson, R, and Sacks, SL</p>

    <p>          Antiviral Res <b>2004</b>.  64(2): 127-130</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498608&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498608&amp;dopt=abstract</a> </p><br />

    <p>3.         6502   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Viramidine Hydrochloride - Impdh Inhibitor Anti-Hepatitis C Virus Drug</p>

    <p>          Cox, S. and Castaner, J.</p>

    <p>          Drugs of the Future <b>2004</b>.  29(7): 687-692</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         6503   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          A wide range of medium-sized, highly cationic, alpha-helical peptides show antiviral activity against herpes simplex virus</p>

    <p>          Jenssen, H, Andersen, JH, Mantzilas, D, and Gutteberg, TJ</p>

    <p>          Antiviral Res <b>2004</b>.  64(2): 119-26</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498607&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15498607&amp;dopt=abstract</a> </p><br />

    <p>5.         6504   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p><b>          P4 and P1&#39; Optimization of Bicycloproline P2 Bearing Tetrapeptidyl Alpha-Ketoamides as Hcv Protease Inhibitors</b> </p>

    <p>          Yip, Y. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(19): 5007-5011</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>6.         6505   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Novel 2-oxoimidazolidine-4-carboxylic acid derivatives as Hepatitis C virus NS3-4A serine protease inhibitors: synthesis, activity, and X-ray crystal structure of an enzyme inhibitor complex</p>

    <p>          Arasappan, Ashok, Njoroge, FGeorge, Parekh, Tejal N, Yang, Xiaozheng, Pichardo, John, Butkiewicz, Nancy, Prongay, Andrew, Yao, Nanhua, and Girijavallabhan, Viyyoor</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(23): 5751-5755</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DM1VDK-2/2/671270c0395c27c0d570aaa6d7035d71">http://www.sciencedirect.com/science/article/B6TF9-4DM1VDK-2/2/671270c0395c27c0d570aaa6d7035d71</a> </p><br />

    <p>7.         6506   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Intracellular Inhibition of Hepatitis C Virus (Hcv) Internal Ribosomal Entry Site (Ires)-Dependent Translation by Peptide Nucleic Acids (Pnas) and Locked Nucleic Acids (Lnas)</p>

    <p>          Nulf, C. and Corey, D.</p>

    <p>          Nucleic Acids Research <b>2004</b>.  32(13): 3792-3798</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>8.         6507   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          The poxvirus p28 virulence factor Is An E3 Ubiquitin ligase</p>

    <p>          Huang, J, Huang, Q, Zhou, X, Shen, MM, Yen, A, Yu, SX, Dong, G, Qu, K, Huang, P, Anderson, EM, Daniel-Issakani, S, Buller, RM, Payan, DG, and Lu, HH</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496420&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496420&amp;dopt=abstract</a> </p><br />

    <p>9.         6508   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Synthesis of New 2&#39;,3&#39;-Dideoxy-6&#39;,6&#39;-difluoro-3&#39;-thionucleoside from gem-Difluorohomoallyl Alcohol</p>

    <p>          Wu, YY, Zhang, X, Meng, WD, and Qing, FL</p>

    <p>          Org Lett <b>2004</b>.  6(22): 3941-3944</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15496069&amp;dopt=abstract</a> </p><br />

    <p>10.       6509   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          West Nile virus NS3 protease: Site-directed mutagenesis and kinetic studies that identify key enzyme-substrate interactions</p>

    <p>          Chappell, KJ, Nall, TA, Stoermer, MJ, Fang, NX, Tyndall, JD, Fairlie, DP, and Young, PR</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494419&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494419&amp;dopt=abstract</a> </p><br />

    <p>11.       6510   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Strategy for analysis and screening of bioactive compounds in traditional Chinese medicines</p>

    <p>          Huang, Xiaodong, Kong, Liang, Li, Xin, Chen, Xueguo, Guo, Ming, and Zou, Hanfa</p>

    <p>          Journal of Chromatography B <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6X0P-4CYPY2W-4/2/aaaa5fcd804c1360709cc2c3bcf30303">http://www.sciencedirect.com/science/article/B6X0P-4CYPY2W-4/2/aaaa5fcd804c1360709cc2c3bcf30303</a> </p><br />

    <p>12.       6511   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Neuraminidase activity assays for monitoring MDCK cell culture derived influenza virus</p>

    <p>          Nayak, DP and Reichl, U</p>

    <p>          J Virol Methods <b>2004</b>.  122(1): 9-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488615&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15488615&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.       6512   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Solution Structure and Antibody Binding Studies of the Envelope Protein Domain Iii From the New York Strain of West Nile Virus</p>

    <p>          Volk, D. <i>et al.</i></p>

    <p>          Journal of Biological Chemistry <b> 2004</b>.  279(37): 38755-38761</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>14.       6513   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Rodent Models for the Study of Therapy Against Flavivirus Infections</p>

    <p>          Charlier, N. <i>et al.</i></p>

    <p>          Antiviral Research <b>2004</b>.  63(2): 67-77</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.       6514   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Hepatitis C drug discovery: in vitro and in vivo systems and drugs in the pipeline</p>

    <p>          Huang, M and Deshpande, M</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(3): 375-88</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482203&amp;dopt=abstract</a> </p><br />

    <p>16.       6515   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Comparison of Biodegradable Nanoparticles and Multiple Emulsions (Water-in-Oil-in-Water) Containing Influenza Virus Antigen on the in Vivo Immune Response in Rats</p>

    <p>          Bozkir, A., Hayta, G., and Saka, O.</p>

    <p>          Pharmazie <b>2004</b>.  59(9): 723-725</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       6516   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          6-Azapyrimidine-2&#39;-deoxy-4&#39;-thionucleosides: Antiviral Agents against TK+ and TK- HSV and VZV Strains</p>

    <p>          Maslen, HL, Hughes, D, Hursthouse, M, De, Clercq E, Balzarini, J, and Simons, C</p>

    <p>          J Med Chem <b>2004</b>.  47(22): 5482-5491</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481985&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481985&amp;dopt=abstract</a> </p><br />

    <p>18.       6517   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          HCV NS5b RNA-Dependent RNA Polymerase Inhibitors: From alpha,gamma-Diketoacids to 4,5-Dihydroxypyrimidine- or 3-Methyl-5- hydroxypyrimidinonecarboxylic Acids. Design and Synthesis</p>

    <p>          Summa, V, Petrocchi, A, Matassa, VG, Taliani, M, Laufer, R, De, Francesco R, Altamura, S, and Pace, P</p>

    <p>          J Med Chem <b>2004</b>.  47(22): 5336-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481971&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481971&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.       6518   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Antiviral therapy: nucleotide and nucleoside analogs</p>

    <p>          Quan, DJ and Peters, MG</p>

    <p>          Clin Liver Dis  <b>2004</b>.  8(2): 371-85</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481345&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481345&amp;dopt=abstract</a> </p><br />

    <p>20.       6519   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Treatment of hepatitis B with interferon and combination therapy</p>

    <p>          Cooksley, WG</p>

    <p>          Clin Liver Dis  <b>2004</b>.  8(2): 353-70</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481344&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15481344&amp;dopt=abstract</a> </p><br />

    <p>21.       6520   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          A Subcutaneously Injected Uv-Inactivated Sars Coronavirus Vaccine Elicits Systemic Humoral Immunity in Mice</p>

    <p>          Takasuka, N. <i>et al.</i></p>

    <p>          International Immunology <b>2004</b>.  16(10): 1423-1430</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>22.       6521   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Infection of vero cells by BK virus is dependent on caveolae</p>

    <p>          Eash, S, Querbes, W, and Atwood, WJ</p>

    <p>          J Virol <b>2004</b>.  78(21): 11583-90</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479799&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479799&amp;dopt=abstract</a> </p><br />

    <p>23.       6522   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Medical treatment of viral pneumonia including SARS in immunocompetent adult</p>

    <p>          Cheng, VC, Tang, BS, Wu, AK, Chu, CM, and Yuen, KY</p>

    <p>          J Infect <b>2004</b>.  49(4): 262-73</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474623&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474623&amp;dopt=abstract</a> </p><br />

    <p>24.       6523   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          In vitro and in vivo antiherpetic activity of three new synthetic brassinosteroid analogues</p>

    <p>          Michelini, Flavia M, Ramirez, Javier A, Berra, Alejandro, Galagovsky, Lydia R, and Alche, Laura E</p>

    <p>          Steroids <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TC9-4DFNDYD-1/2/8b536f7d96709af3f5bbc11d40015eff">http://www.sciencedirect.com/science/article/B6TC9-4DFNDYD-1/2/8b536f7d96709af3f5bbc11d40015eff</a> </p><br />
    <br clear="all">

    <p>25.       6524   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Identification of a Novel Telomerase Repressor That Interacts With the Human Papillomavirus Type-16 E6/E6-Ap Complex</p>

    <p>          Gewin, L. <i>et al.</i></p>

    <p>          Genes &amp; Development <b>2004</b>.  18(18): 2269-2282</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       6525   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Synthesis and Anti-Influenza Virus Activity of Ethyl 6-Bromo-5-Hydroxyindole-3-Carboxylate Derivatives</p>

    <p>          Zhao, Y., Dong, J., and Gong, P.</p>

    <p>          Chinese Chemical Letters <b>2004</b>.  15(9): 1039-1042</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       6526   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          A new lignan glycoside and phenylethanoid glycosides from Strobilanthes cusia BREMEK</p>

    <p>          Tanaka, T, Ikeda, T, Kaku, M, Zhu, XH, Okawa, M, Yokomizo, K, Uyeda, M, and Nohara, T</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2004</b>.  52(10): 1242-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15467245&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15467245&amp;dopt=abstract</a> </p><br />

    <p>28.       6527   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          From the Great Barrier Reef to a &quot;Cure&quot; for the Flu: tall tales, but true</p>

    <p>          Laver, WG</p>

    <p>          Perspect Biol Med <b>2004</b>.  47(4): 590-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15467180&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15467180&amp;dopt=abstract</a> </p><br />

    <p>29.       6528   DMID-LS-78; WOS-DMID-10/26/2004</p>

    <p class="memofmt1-2">          Development and Pharmacokinetics and Pharmacodynamics of Pegylated Interferon Alfa-2a (40 Kd)</p>

    <p>          Reddy, K.</p>

    <p>          Seminars in Liver Disease <b>2004</b>.  24: 33-38</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>30.       6529   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          HCV NS5B polymerase-bound conformation of a soluble sulfonamide inhibitor by 2D transferred NOESY</p>

    <p>          Yannopoulos, CG, Xu, P, Ni, F, Chan, L, Pereira, OZ, Reddy, TJ, Das, SK, Poisson, C, Nguyen-Ba, N, Turcotte, N, Proulx, M, Halab, L, Wang, W, Bedard, J, Morin, N, Hamel, M, Nicolas, O, Bilimoria, D, L&#39;Heureux, L, Bethell, R, and Dionne, G</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(21): 5333-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15454222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15454222&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>31.       6530   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Structure of polysaccharide from Polygonatum cyrtonema Hua and the antiherpetic activity of its hydrolyzed fragments</p>

    <p>          Liu, F, Liu, Y, Meng, Y, Yang, M, and He, K</p>

    <p>          Antiviral Res <b>2004</b>.  63(3): 183-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451186&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451186&amp;dopt=abstract</a> </p><br />

    <p>32.       6531   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Inhibition of bovine herpesvirus-4 replication in endothelial cells by arsenite</p>

    <p>          Jiang, SJ, Lin, TM, Shi, GY, Eng, HL, Chen, HY, and Wu, HL</p>

    <p>          Antiviral Res <b>2004</b>.  63(3): 167-75</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451184&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451184&amp;dopt=abstract</a> </p><br />

    <p>33.       6532   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Mechanism of viral persistence and resistance to nucleoside and nucleotide analogs in chronic Hepatitis B virus infection</p>

    <p>          Zoulim, F</p>

    <p>          Antiviral Res <b>2004</b>.  64(1): 1-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451174&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451174&amp;dopt=abstract</a> </p><br />

    <p>34.       6533   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Influenza virus neuraminidase inhibitors: generation and comparison of structure-based and common feature pharmacophore hypotheses and their application in virtual screening</p>

    <p>          Steindl, T and Langer, T</p>

    <p>          J Chem Inf Comput Sci <b>2004</b>.  44(5): 1849-56</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15446845&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15446845&amp;dopt=abstract</a> </p><br />

    <p>35.       6534   DMID-LS-78; PUBMED-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          [Seeds against smallpox: Joaquim Vas and the scientific translation of bananeira brava seeds in Goa, India (1894-1930)]</p>

    <p>          Roque, R</p>

    <p>          Hist Cienc Saude Manguinhos <b>2004</b>.  11 Suppl 1: 183-222</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15446280&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15446280&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>36.       6535   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Vaccines for the prevention of diseases caused by potential bioweapons</p>

    <p>          Hassani, Morad, Patel, Mahesh C, and Pirofski, Liise-anne</p>

    <p>          Clinical Immunology <b>2004</b>.  111(1): 1-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WCJ-4BVPRTS-1/2/431fbe4055686e00c18f77309687a343">http://www.sciencedirect.com/science/article/B6WCJ-4BVPRTS-1/2/431fbe4055686e00c18f77309687a343</a> </p><br />

    <p>37.       6536   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          Molecular targets for the rational design of drugs to inhibit SARS coronavirus</p>

    <p>          Brockway, Sarah M and Denison, Mark R</p>

    <p>          Drug Discovery Today: Disease Mechanisms <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75D7-4DFX31X-1/2/9c20fbe9153fae143c15818f460a8d8e">http://www.sciencedirect.com/science/article/B75D7-4DFX31X-1/2/9c20fbe9153fae143c15818f460a8d8e</a> </p><br />

    <p>38.       6537   DMID-LS-78; EMBASE-DMID-10/26/2004 </p>

    <p class="memofmt1-2">          PML-nuclear bodies accumulate DNA in response to polyomavirus BK and simian virus 40 replication</p>

    <p>          Jul-Larsen, Asne, Visted, Therese, Karlsen, Bard Ove, Rinaldo, Christine Hanssen, Bjerkvig, Rolf, Lonning, Per Eystein, and Boe, Stig Ove</p>

    <p>          Experimental Cell Research <b>2004</b>.  298(1): 58-73</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFC-4CB00F2-3/2/e4589c29526ea31fc096eae099e0ff04">http://www.sciencedirect.com/science/article/B6WFC-4CB00F2-3/2/e4589c29526ea31fc096eae099e0ff04</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
