

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-07-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tVFNYSJNxz1+GPLAYmgjywAho36I86BO/hog/JVyXAUTEWmO22XwIfuJk1j2twNlTLkUMSrffgmQwfRVul87uyjFFaJiHfYmtqQulh8Bj+apx7j6EYNNYXg8zuw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="479E89AC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">July 9 - July 22, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20638289">Synthesis and anti-hepatitis C virus activity of novel ethyl 1H-indole-3-carboxylates in vitro.</a> Sellitto G, Faruolo A, de Caprariis P, Altamura S, Paonessa G, Ciliberto G. Bioorg Med Chem. 2010 Jun 22. <b>[Epub ahead of print]</b></p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20637609">Design, synthesis and evaluation of a novel double pro-drug: INX-08189. A new clinical candidate for hepatitis C virus.</a> McGuigan C, Madela K, Aljarah M, Gilles A, Brancale A, Zonta N, Chamberlain S, Vernachio J, Hutchins J, Hall A, Ames B, Gorovits E, Ganguly B, Kolykhalov A, Wang J, Muhammad J, Patti JM, Henson G. Bioorg Med Chem Lett. 2010 Jun 20. <b>[Epub ahead of print]</b></p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20629614">New NS5B polymerase inhibitors for hepatitis C.</a> Legrand-Abravanel F, Nicot F, Izopet J.</p>

    <p class="NoSpacing">Expert Opin Investig Drugs. 2010 Aug;19(8):963-75. PMID: 20629614 [PubMed - in process]</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20627595">Structure-based virtual screening, synthesis and SAR of novel inhibitors of hepatitis C virus NS5B polymerase.</a> Talele TT, Arora P, Kulkarni SS, Patel MR, Singh S, Chudayeu M, Kaushik-Basu N. Bioorg Med Chem. 2010 May 15. <b>[Epub ahead of print]</b></p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20627592">Synthesis and anti-hepatitis C virus (HCV) activity of 3&#39;-C-substituted-methyl pyrimidine and purine nucleosides.</a> Choi WJ, Kim YM, Kim HO, Lee HW, Kim DE, Park KS, Chong Y, Jeong LS. Bioorg Med Chem. 2010 May 31. <b>[Epub ahead of print]</b>. PMID: 20627592 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B Virus</p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20641855">2&#39;-Fluoro-5-([<sup>11</sup>C]-methyl)-1-&#946;-D-arabinofuranosyluracil.</a> Chopra A. Molecular Imaging and Contrast Agent Database [Internet]. Bethesda (MD): National Center for Biotechnology Information (US); 2004-2010. 2008 Jan 2 [updated 2008 Jan 24]. PMID: 20641855 [PubMed]</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20641443">1-(2&#39;-Deoxy-2&#39;-[<sup>18</sup>F]-fluoro-&#946;-D-arabinofuranosyl)thymine.</a> Chopra A. Molecular Imaging and Contrast Agent Database [Internet]. Bethesda (MD): National Center for Biotechnology Information (US); 2004-2010. 2008 Jan 9 [updated 2008 Feb 14]. PMID: 20641443 [PubMed]</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20639110">Design and synthesis of novel benzimidazole derivatives as inhibitors of hepatitis B virus.</a> Luo Y, Yao JP, Yang L, Feng CL, Tang W, Wang GF, Zuo JP, Lu W. Bioorg Med Chem. 2010 Jul 15;18(14):5048-5055. Epub 2010 Jun 1. PMID: 20639110 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="memofmt2-2">HSV-1 Virus</p> 

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20643896">THE VIRUCIDAL EB PEPTIDE PROTECTS HOST CELLS FROM HSV-1 INFECTION IN THE PRESENCE OF SERUM ALBUMIN AND AGGREGATES PROTEINS IN A DETERGENT-LIKE MANNER.</a> Bultmann H, Girdaukas G, Kwon GS, Brandt CR. Antimicrob Agents Chemother. 2010 Jul 19. <b>[Epub ahead of print]</b>. PMID: 20643896 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20643939">Combined agonist-antagonist genome-wide functional screening identifies broadly active antiviral microRNAs.</a> Santhakumar D, Forster T, Laqtom NN, Fragkoudis R, Dickinson P, Abreu-Goodger C, Manakov SA, Choudhury NR, Griffiths SJ, Vermeulen A, Enright AJ, Dutia B, Kohl A, Ghazal P, Buck AH.</p>

    <p class="NoSpacing">Proc Natl Acad Sci U S A. 2010 Jul 19. <b>[Epub ahead of print]</b>. PMID: 20643939 [PubMed - as supplied by publisher]</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20622455">Inhibition of Human Cytomegalovirus Infection by IE86-Specific Short Hairpin RNA-Mediated RNA Interference.</a> Bai Z, Li L, Wang B, Liu Z, Liu H, Jiang G, Wang H, Yan Z, Qian D, Ding S, Song X. Biosci Biotechnol Biochem. 2010 Jul 7. <b>[Epub ahead of print]</b>. PMID: 20622455 [PubMed - as supplied by publisher]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
