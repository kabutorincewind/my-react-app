

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-316.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FpDIdtybTbRyItFULLD9vAwMgrIXeFF5KmATXNnUC8elkl6XaR6cm0yAg0vx5PFO5hE40dJ9LdG1uYSe0P6yEQbfaLKl0kiS+ixFsklAnb44XLhnmX5ZyDaShQ8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8003F622" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-316-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44898   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Crystal Structure of Lumazine Synthase from Mycobacterium tuberculosis as a Target for Rational Drug Design: Binding Mode of a New Class of Purinetrione Inhibitors(,)</p>

    <p>          Morgunova, E, Meining, W, Illarionov, B, Haase, I, Jin, G, Bacher, A, Cushman, M, Fischer, M, and Ladenstein, R</p>

    <p>          Biochemistry <b>2005</b>.  44(8): 2746-2758</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723519&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15723519&amp;dopt=abstract</a> </p><br />

    <p>2.         44899   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Binding of the anti-tubercular drug isoniazid to the arylamine N-acetyltransferase protein from Mycobacterium smegmatis</p>

    <p>          Sandy, J, Holton, S, Fullam, E, Sim, E, and Noble, M</p>

    <p>          Protein Sci <b>2005</b>.  14(3): 775-782</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15722451&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15722451&amp;dopt=abstract</a> </p><br />

    <p>3.         44900   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          DAHP synthase from Mycobacterium tuberculosis H37Rv: cloning, expression, and purification of functional enzyme</p>

    <p>          Rizzi, Caroline, Frazzon, Jeverson, Ely, Fernanda, Weber, Patricia G, da Fonseca, Isabel O, Gallas, Michelle, Oliveira, Jaim S, Mendes, Maria A, de Souza, Bibiana M, and Palma, Mario S</p>

    <p>          Protein Expression and Purification <b>2005</b>.  40(1): 23-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4F00RT8-1/2/d26e8c36daf69be29a6e9168e5c2f605">http://www.sciencedirect.com/science/article/B6WPJ-4F00RT8-1/2/d26e8c36daf69be29a6e9168e5c2f605</a> </p><br />

    <p>4.         44901   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Study of anticandidal activity of carvacrol and eugenol in vitro and in vivo</p>

    <p>          Chami, N, Bennis, S, Chami, F, Aboussekhra, A, and Remmal, A</p>

    <p>          Oral Microbiol Immunol <b>2005</b>.  20(2): 106-111</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15720571&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15720571&amp;dopt=abstract</a> </p><br />

    <p>5.         44902   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Laboratory approaches to the diagnosis of hepatitis C virus infection</p>

    <p>          Jerome, KR and Gretch, DR</p>

    <p>          Minerva Gastroenterol Dietol <b>2004</b>.  50(1): 9-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15719002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15719002&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44903   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Synthesis of isonicotinic acid N&#39;-arylidene-N-[2-oxo-2-(4-aryl-piperazin-1-yl)-ethyl]-hydrazides as antituberculosis agents</p>

    <p>          Sinha, Neelima, Jain, Sanjay, Tilekar, Ajay, Upadhayaya, Ram Shankar, Kishore, Nawal, Jana, Gour Hari, and Arora, Sudershan K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FJ5XJ4-5/2/5b09c2496690cb2960a3f583fd875fd2">http://www.sciencedirect.com/science/article/B6TF9-4FJ5XJ4-5/2/5b09c2496690cb2960a3f583fd875fd2</a> </p><br />

    <p>7.         44904   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Novel [2&#39;,5&#39;-Bis-O-(tert-butyldimethylsilyl)-beta-d-ribofuranosyl]- 3&#39;-spiro-5&#39; &#39;-(4&#39; &#39;-amino-1&#39; &#39;,2&#39; &#39;-oxathiole-2&#39; &#39;,2&#39; &#39;-dioxide) Derivatives with Anti-HIV-1 and Anti-Human-Cytomegalovirus Activity</p>

    <p>          de Castro, S,  Lobaton, E, Perez-Perez, MJ, San-Felix, A, Cordeiro, A, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          J Med Chem <b>2005</b>.  48(4): 1158-1168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15715482&amp;dopt=abstract</a> </p><br />

    <p>8.         44905   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Synthesis of potent and selective inhibitors of Candida albicans N-myristoyltransferase based on the benzothiazole structure</p>

    <p>          Yamazaki, Kazuo, Kaneko, Yasushi, Suwa, Kie, Ebara, Shinji, Nakazawa, Kyoko, and Yasuno, Kazuhiro</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FH0D6M-2/2/af50e1883f94ddf2d2ca1cfe5ee2336e">http://www.sciencedirect.com/science/article/B6TF8-4FH0D6M-2/2/af50e1883f94ddf2d2ca1cfe5ee2336e</a> </p><br />

    <p>9.         44906   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Crystal Structure of a Substrate Complex of Mycobacterium tuberculosis beta-Ketoacyl-acyl Carrier Protein Synthase III (FabH) with Lauroyl-coenzyme A</p>

    <p>          Musayev, F, Sachdeva, S, Neel Scarsdale, J, Reynolds, KA, and Wright, HT</p>

    <p>          J Mol Biol <b>2005</b>.  346(5): 1313-1321</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15713483&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15713483&amp;dopt=abstract</a> </p><br />

    <p>10.       44907   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Interferon Regulatory Factor 3-Independent Double-Stranded RNA-Induced Inhibition of Hepatitis C Virus Replicons in Human Embryonic Kidney 293 Cells</p>

    <p>          Ali, S and Kukolj, G</p>

    <p>          J Virol <b>2005</b> .  79(5): 3174-3178</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15709037&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15709037&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44908   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Antibacterial and antifungal activity of pinosylvin, a constituent of pine</p>

    <p>          Lee, SK, Lee, HJ, Min, HY, Park, EJ, Lee, KM, Ahn, YH, Cho, YJ, and Pyee, JH</p>

    <p>          Fitoterapia <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4FG4V7X-3/2/0daca8f1ad64b0db160dda433f3d03d5">http://www.sciencedirect.com/science/article/B6VSC-4FG4V7X-3/2/0daca8f1ad64b0db160dda433f3d03d5</a> </p><br />

    <p>12.       44909   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Establishment of a subgenomic replicon for bovine viral diarrhea virus in huh-7 cells and modulation of interferon-regulated factor 3-mediated antiviral response</p>

    <p>          Horscroft, N,  Bellows, D, Ansari, I, Lai, VC, Dempsey, S, Liang, D, Donis, R, Zhong, W, and Hong, Z</p>

    <p>          J Virol <b>2005</b> .  79(5): 2788-2796</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708997&amp;dopt=abstract</a> </p><br />

    <p>13.       44910   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activities of glycosylated amino alcohols and amines</p>

    <p>          Katiyar, D, Tiwari, VK, Tewari, N, Verma, SS, Sinha, S, Gaikwad, A, Srivastava, A, Chaturvedi, V, Srivastava, R, Srivastava, BS, and Tripathi, RP</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4FCRFPD-2/2/3467d5e1ce6cf68499fdf8a276931500">http://www.sciencedirect.com/science/article/B6VKY-4FCRFPD-2/2/3467d5e1ce6cf68499fdf8a276931500</a> </p><br />

    <p>14.       44911   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          (1)H and (13)C NMR characterization of pyridinium-type isoniazid-NAD adducts as possible inhibitors of InhA reductase of Mycobacterium tuberculosis</p>

    <p>          Broussy, S, Bernardes-Genisson, V, Coppel, Y, Quemard, A, Bernadou, J, and Meunier, B</p>

    <p>          Org Biomol Chem <b>2005</b>.  3(4): 670-673</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15703806&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15703806&amp;dopt=abstract</a> </p><br />

    <p>15.       44912   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          The OtsAB pathway is essential for trehalose biosynthesis in mycobacterium tuberculosis</p>

    <p>          Murphy, HN, Stewart, GR, Mischenko, VV, Apt, AS, Harris, R, McAlister, MS, Driscoll, PC, Young, DB, and Robertson, BD</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15703182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15703182&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44913   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          A hybrid between the antifungal azole eberconazole and the alkaloid onychine</p>

    <p>          Dombeck, F and Bracher, F</p>

    <p>          Pharmazie <b>2005</b>.  60(1): 5-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15700771&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15700771&amp;dopt=abstract</a> </p><br />

    <p>17.       44914   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Identification of inhibitors for a virally encoded protein kinase by 2 different screening systems: in vitro kinase assay and in-cell activity assay</p>

    <p>          Mett, H, Holscher, K, Degen, H, Esdar, C, De Neumann, BF, Flicke, B, Freudenreich, T, Holzer, G, Schinzel, S, Stamminger, T, Stein-Gerlach, M, Marschall, M, and Herget, T</p>

    <p>          J Biomol Screen <b>2005</b>.  10(1): 36-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15695342&amp;dopt=abstract</a> </p><br />

    <p>18.       44915   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase: synthesis and structure-activity relationships of N-1-heteroalkyl-4-hydroxyquinolon-3-yl-benzothiadiazines</p>

    <p>          Pratt, John K, Donner, Pamela, McDaniel, Keith F, Maring, Clarence J, Kati, Warren M, Mo, Hongmei, Middleton, Tim, Liu, Yaya, Ng, Teresa, and Xie, Qinghua</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FJ5XJ4-7/2/f484d1c313f9acfc8b877c4811b2c86d">http://www.sciencedirect.com/science/article/B6TF9-4FJ5XJ4-7/2/f484d1c313f9acfc8b877c4811b2c86d</a> </p><br />

    <p>19.       44916   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Design and synthesis of a potent macrocyclic 1,6-napthyridine anti-human cytomegalovirus (HCMV) inhibitors</p>

    <p>          Falardeau, Guy, Lachance, Hugo, St-Pierre, Annie, Yannopoulos, Constantin G, Drouin, Marc, Bedard, Jean, and Chan, Laval</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FFGJ50-7/2/9a87fc85f79b22dc244fb6e2c58702be">http://www.sciencedirect.com/science/article/B6TF9-4FFGJ50-7/2/9a87fc85f79b22dc244fb6e2c58702be</a> </p><br />

    <p>20.       44917   OI-LS-316; EMBASE-OI-2/23/2005</p>

    <p class="memofmt1-2">          Effect of the anti-cytomegalovirus (CMV) drug maribavir (MB) on the activities of cytochrome p450 (CYP) 1A2, 2C9, 2C19, 2D6, 3A, N-ACETYLTRANSFERASE-2 (NAT-2), and xanthine oxidase (XO) as assessed by the cooperstown 5+1 drug cocktail</p>

    <p>          Ma, JD, Nafziger, AN, Villano, SA, Gaedigk, A, Victory, J, and Bertino, JS</p>

    <p>          Clinical Pharmacology &amp; Therapeutics <b>2005</b>.  77(2): P19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WCN-4FDMRBG-2K/2/2c19d48c06a4524cc168e7bcb15ed5ca">http://www.sciencedirect.com/science/article/B6WCN-4FDMRBG-2K/2/2c19d48c06a4524cc168e7bcb15ed5ca</a> </p><br />
    <br clear="all">

    <p>21.       44918   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Mutations responsible for Mycobacterium tuberculosis isoniazid resistance in Italy</p>

    <p>          Rindi, L, Bianchi, L, Tortoli, E, Lari, N, Bonanni, D, and Garzelli, C</p>

    <p>          Int J Tuberc Lung Dis <b>2005</b>.  9(1): 94-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15675557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15675557&amp;dopt=abstract</a> </p><br />

    <p>22.       44919   OI-LS-316; PUBMED-OI-2/23/2005</p>

    <p class="memofmt1-2">          Assays for glucosidase inhibitors with potential antiviral activities: secreted alkaline phosphatase as a surrogate marker</p>

    <p>          Norton, PA, Conyers, B, Gong, Q, Steel, LF, Block, TM, and Mehta, AS</p>

    <p>          J Virol Methods <b>2005</b>.  124(1-2): 167-172</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15664065&amp;dopt=abstract</a> </p><br />

    <p>23.       44920   OI-LS-316; WOS-OI-2/13/2005</p>

    <p class="memofmt1-2">          Antimycobacterial constituents from Juniperus procera, Ferula communis and Plumbago zeylanica and their in vitro synergistic activity with isonicotinic acid hydrazide</p>

    <p>          Mossa, JS, El-Feraly, FS, and Muhammad, I</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2004</b>.  18(11): 934-937, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226394300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226394300014</a> </p><br />

    <p>24.       44921   OI-LS-316; WOS-OI-2/13/2005</p>

    <p class="memofmt1-2">          Intranasal application of lentinan enhances bactericidal activity of rat alveolar macrophages against Mycobacterium tuberculosis</p>

    <p>          Markova, N, Michailova, L, Kussovski, V, Jourdanova, M, and Radoucheva, T</p>

    <p>          PHARMAZIE <b>2005</b>.  60(1): 42-48, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226409400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226409400008</a> </p><br />

    <p>25.       44922   OI-LS-316; WOS-OI-2/13/2005</p>

    <p class="memofmt1-2">          Induction of colony-stimulating factors by a 30-kDa secretory protein of Mycobacterium tuberculosis H37Rv</p>

    <p>          Kaur, S, Kaur, H, and Singh, PP</p>

    <p>          EUROPEAN CYTOKINE NETWORK <b>2004</b>.  15(4): 327-338, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226373400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226373400007</a> </p><br />

    <p>26.       44923   OI-LS-316; WOS-OI-2/13/2005</p>

    <p class="memofmt1-2">          Fatty acid synthesis as a target for antimalarial drug discovery</p>

    <p>          Lu, JZQ, Lee, PJ, Waters, NC, and Prigge, ST</p>

    <p>          COMBINATORIAL CHEMISTRY &amp; HIGH THROUGHPUT SCREENING <b>2005</b>.  8(1): 15-26, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226436400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226436400003</a> </p><br />
    <br clear="all">

    <p>27.       44924   OI-LS-316; WOS-OI-2/13/2005</p>

    <p class="memofmt1-2">          Streamlining drug discovery informatics: Accelerating the flow from gene to structure to pre-clinical candidate</p>

    <p>          Artis, DR</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  227: U679-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655602471">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223655602471</a> </p><br />

    <p>28.       44925   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          Metabolizing enzyme toxicology assay chip (MetaChip) for high-throughput microscale toxicity analyses</p>

    <p>          Lee, MY, Park, CB, Dordick, JS, and Clark, DS</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2005</b>.  102(4): 983-987, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226617900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226617900007</a> </p><br />

    <p>29.       44926   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          Bioactive carbohydrates and recently discovered analogues chemotherapeutics</p>

    <p>          Wrodnigg, TM and Sprenger, FKF</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY <b>2004</b>.  4(4): 437-459, 23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226486800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226486800010</a> </p><br />

    <p>30.       44927   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          The predominant mechanism by which ribavirin exerts its antiviral activity in vitro against flaviviruses and paramyxoviruses is mediated by inhibition of IMP dehydrogenase</p>

    <p>          Leyssen, P, Balzarini, J, De Clercq, E , and Neyts, J</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(3): 1943-1947, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226634300061</a> </p><br />

    <p>31.       44928   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          NF-kappa B activation can mediate inhibition of human cytomegalovirus replication</p>

    <p>          Eickhoff, JE and Cotten, M</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2005</b>.  86: 285-295, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226738500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226738500005</a> </p><br />

    <p>32.       44929   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          Bioluminescence imaging of Toxoplasma gondii infection in living mice reveals dramatic differences between strains</p>

    <p>          Saeij, JPJ, Boyle, JP, Grigg, ME, Arrizabalaga, G, and Boothroyd, JC</p>

    <p>          INFECTION AND IMMUNITY <b>2005</b>.  73(2): 695-702, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226731700004</a> </p><br />

    <p>33.       44930   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          Practical synthesis of a potent hepatitis C virus RNA replication inhibitor</p>

    <p>          Xu, F, Waters, M, Bio, MM, Savary, K, Cowden, C, Yang, CH, Reamer, RA, Buck, E, Song, ZGJ, Williams, JM, Tschaen, DM, Volante, RP, Grabowski, EJJ, and Tillyer, R</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U121-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800588">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800588</a> </p><br />

    <p>34.       44931   OI-LS-316; WOS-OI-2/20/2005</p>

    <p class="memofmt1-2">          Synthesis of a proposed inhibitor of human cytomegalovirus (HCMV) using a tandem chain extension-aldol reaction approach</p>

    <p>          Lin, WM, Theberge, CR, and Zercher, CK</p>

    <p>          ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY <b>2004</b>.  228: U133-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800661">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223713800661</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
