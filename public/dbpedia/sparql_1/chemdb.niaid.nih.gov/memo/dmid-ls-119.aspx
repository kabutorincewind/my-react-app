

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-119.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gKxZDlXsLHTVRwuQV1L2O1aDCz/w8WIFTmX9yHwZZUXuMPbE5CpivbE8su+21uVIKXih7xYpPlK45NE0aytbUq4Xhva+59TaQ1AmDqkp9e7tdtZlQqs8B1Yt3Pw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="47F4F13D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-119-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58449   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Treatment or prevention of hemorrhagic viral infections with peptidic immunomodulator compounds</p>

    <p>          Mossel, Eric C, Tuthill, Cynthia W, and Rudolph, Alfred R</p>

    <p>          PATENT:  WO <b>2006047702</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 16 pp.</p>

    <p>          ASSIGNEE:  (Sciclone Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58450   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense antiviral compounds and methods for treating filovirus infection</p>

    <p>          Stein, David A, Iversen, Patrick L, and Bavari, Sina</p>

    <p>          PATENT:  WO <b>2006050414</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     58451   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Combination antiviral compositions comprising castanospermine and use for the treatment and prevention of infections caused by or assocd. with a virus of the Flaviviridae family</p>

    <p>          Dugourd, Dominique</p>

    <p>          PATENT:  WO <b>2006037227</b>  ISSUE DATE:  20060413</p>

    <p>          APPLICATION: 2005  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Migenix Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58452   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of piperazinylquinolonecarboxamides as antivirals</p>

    <p>          Schohe-Loop, Rudolf, Zimmermann, Holger, Henninger, Kerstin, Paulsen, Daniela, Roelle, Thomas, Lang, Dieter, Thede, Kai, Fuerstner, Chantal, Brueckner, David, Koebberling, Johannes, and Bauser, Marcus</p>

    <p>          PATENT:  WO <b>2006008046</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 115 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare AG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     58453   DMID-LS-119; WOS-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-herpes virus activity of Dunbaria bella prain</p>

    <p>          Akanitapichat, P, Wangmaneerat, A, Wilairat, P, and Bastow, KF</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY: J. Ethnopharmacol <b>2006</b>.  105(1-2): 64-68, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237101300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237101300007</a> </p><br />

    <p>6.     58454   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of sulfated cholic acid molecular umbrellas as anti-HIV and anti-HSV agents</p>

    <p>          Regen, Steven and Herold, Betsy C</p>

    <p>          PATENT:  WO <b>2006034369</b>  ISSUE DATE:  20060330</p>

    <p>          APPLICATION: 2005  PP: 32 pp.</p>

    <p>          ASSIGNEE:  (Lehigh University, USA and Mount Sinai School of Medicine of New York University)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     58455   DMID-LS-119; WOS-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and primary antiviral activity evaluation of 3-hydrazono-5-nitro-2-indolinone derivatives</p>

    <p>          Terzioglu, N, Karali, N, Gursoy, A, Pannecouque, C, Leysen, P, Paeshuyse, J, Neyts, J, and De Clercq, E</p>

    <p>          ARKIVOC: Arkivoc <b>2006</b>.: 109-118, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237239800013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237239800013</a> </p><br />

    <p>8.     58456   DMID-LS-119; WOS-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Fighting infection - An ongoing challenge, Part 3-antimycobacterials, antifungal, and antivirals</p>

    <p>          Turkoski, BB</p>

    <p>          ORTHOPAEDIC NURSING: Orthop. Nurs <b>2005</b>.  24(5): 380-388, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237086600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237086600009</a> </p><br />

    <p>9.     58457   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral methods and compositions</p>

    <p>          Coen, Donald M and Loregian, Arianna</p>

    <p>          PATENT:  WO <b>2006019955</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58458   DMID-LS-119; WOS-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Quaternary structure and cleavage specificity of a poxvirus Holliday junction resolvase</p>

    <p>          Garcia, AD, Otero, J, Lebowitz, J, Schuck, P, and Moss, B</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY: J. Biol. Chem <b>2006</b>.  281(17): 11618-11626, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236988100025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236988100025</a> </p><br />

    <p>11.   58459   DMID-LS-119; SCIFINDER-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense antiviral compound and method for treating influenza viral infection</p>

    <p>          Stein, David A, Ge Qing, Chen, Jianzhu, and Iversen, Patrick L</p>

    <p>          PATENT:  WO <b>2006047683</b>  ISSUE DATE: 20060504</p>

    <p>          APPLICATION: 2005  PP: 57 pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   58460   DMID-LS-119; WOS-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication by in vitro synthesized RNA (vol 26, pg 1385, 2005)</p>

    <p>          Yang, YJ, Heo, YS, Kim, JK, Kim, SY, and Ahn, JK</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY: Bull. Korean Chem. Soc <b>2006</b>.  27(3): 450-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236838700027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236838700027</a> </p><br />

    <p>13.   58461   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of DNA replication of human papillomavirus by artificial zinc finger proteins</p>

    <p>          Mino, T, Hatono, T, Matsumoto, N, Mori, T, Mineta, Y, Aoyama, Y, and Sera, T</p>

    <p>          J Virol <b>2006</b>.  80(11): 5405-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16699021&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16699021&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>14.   58462   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral therapy for adenovirus infections</p>

    <p>          Lenaerts, L and Naesens, L</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16698093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16698093&amp;dopt=abstract</a> </p><br />

    <p>15.   58463   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          FK506, a secondary metabolite produced by Streptomyces, presents a novel antiviral activity against Orthopoxvirus infection in cell culture</p>

    <p>          Reis, SA, Moussatche, N, and Damaso, CR</p>

    <p>          J Appl Microbiol <b>2006</b>.  100(6): 1373-80</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16696686&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16696686&amp;dopt=abstract</a> </p><br />

    <p>16.   58464   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An Efficient Method for the Synthesis of Peptide Aldehyde Libraries Employed in the Discovery of Reversible SARS Coronavirus Main Protease (SARS-CoV M(pro)) Inhibitors</p>

    <p>          Al-Gharabli, SI, Shah, ST, Weik, S, Schmidt, MF, Mesters, JR, Kuhn, D, Klebe, G, Hilgenfeld, R, and Rademann, J</p>

    <p>          Chembiochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16688706&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16688706&amp;dopt=abstract</a> </p><br />

    <p>17.   58465   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of respiratory syncytial virus in cultured cells by nucleocapsid gene targeted deoxyribozyme (DNAzyme)</p>

    <p>          Xie, YY, Zhao, XD, Jiang, LP, Liu, HL, Wang, LJ, Fang, P, Shen, KL, Xie, ZD, Wu, YP, and Yang, XQ</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16687180&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16687180&amp;dopt=abstract</a> </p><br />

    <p>18.   58466   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Modelling the anti-herpes simplex virus activity of small cationic peptides using amino acid descriptors</p>

    <p>          Jenssen, H, Gutteberg, TJ, and Lejon, T</p>

    <p>          J Pept Res <b>2005</b>.  66 Suppl 1: 48-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650060&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   58467   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virucidal activity of a GT-rich oligonucleotide against herpes simplex virus mediated by glycoprotein B</p>

    <p>          Shogan, B, Kruse, L, Mulamba, GB, Hu, A, and Coen, DM</p>

    <p>          J Virol <b>2006</b>.  80(10): 4740-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641267&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641267&amp;dopt=abstract</a> </p><br />

    <p>20.   58468   DMID-LS-119; PUBMED-DMID-5/22/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nitazoxanide, a broad-spectrum thiazolide anti-infective agent for the treatment of gastrointestinal infections</p>

    <p>          Hemphill, A, Mueller, J, and Esposito, M</p>

    <p>          Expert Opin Pharmacother <b>2006</b>.  7(7): 953-64</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16634717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16634717&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
