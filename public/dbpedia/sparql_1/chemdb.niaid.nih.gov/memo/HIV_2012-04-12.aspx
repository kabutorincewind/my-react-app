

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-04-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EJgdmtWHLJga5DYB+fFsD5RI4thFN0s7Y1dHGsLZFa/ZCCQ1lIF8eX4XWXNWrfo1gesYfMhWyfZT+V9mXG4ko0Trl3jA7XTxZtmXed8+jKEaaXTYHN/0SH7NIm8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1D1518AF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 30 - April 12, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22076653">Functional Mechanisms of the Cellular Prion Protein (PrP(C)) Associated anti-HIV-1 Properties.</a> Alais, S., R. Soto-Rifo, V. Balter, H. Gruffat, E. Manet, L. Schaeffer, J.L. Darlix, A. Cimarelli, G. Raposo, T. Ohlmann, and P. Leblanc, Cellular and Molecular Life Sciences, 2012. 69(8): p. 1331-1352; PMID[22076653].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22474613">HIV-1 Antiretroviral Drug Therapy.</a> Arts, E.J. and D.J. Hazuda, Cold Spring Harbor Perspectives in Medicine, 2012. 2(4): p. a007161; PMID[22474613].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22271861">Activity of a Novel Combined Antiretroviral Therapy of Gemcitabine and Decitabine in a Mouse Model for HIV-1.</a> Clouser, C.L., C.M. Holtz, M. Mullett, D.L. Crankshaw, J.E. Briggs, M.G. O&#39;Sullivan, S.E. Patterson, and L.M. Mansky, Antimicrobial Agents and Chemotherapy, 2012. 56(4): p. 1942-1948; PMID[22271861].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22345469">Anti-HIV-1 Activity of Elafin Is More Potent than Its Precursor&#39;s, Trappin-2, in Genital Epithelial Cells.</a> Drannik, A.G., K. Nag, X.D. Yao, B.M. Henrick, S. Jain, T.B. Ball, F.A. Plummer, C. Wachihi, J. Kimani, and K.L. Rosenthal, Journal of Virology, 2012. 86(8): p. 4599-4610; PMID[22345469].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227602">Microwave-assisted Synthesis of Small Molecules Targeting the Infectious Diseases Tuberculosis, HIV/AIDS, Malaria and Hepatitis C.</a> Gising, J., L.R. Odell, and M. Larhed, Organic &amp; Biomolecular Chemistry, 2012. 10(14): p. 2713-2729; PMID[22227602].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22462772">Synthesis and anti-HIV Activity of Lupane and Olean-18-ene Derivatives. Absolute Configuration of 19,20-Epoxylupanes by VCD.</a> Gutierrez-Nicolas, F., B. Gordillo-Roman, J.C. Oberti, A. Estevez-Braun, A.G. Ravelo, and P. Joseph-Nathan, Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[22462772].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227864">Development of a Combination Microbicide Gel Formulation Containing IQP-0528 and Tenofovir for the Prevention of HIV Infection.</a> Ham, A.S., S.R. Ugaonkar, L. Shi, K.W. Buckheit, H. Lakougna, U. Nagaraja, G. Gwozdz, L. Goldman, P.F. Kiser, and R.W. Buckheit, Jr., Journal of Pharmaceutical Sciences, 2012. 101(4): p. 1423-1435; PMID[22227864].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22468749">Synthesis and Biological Evaluation of 5-Fluoroquinolone-3-carboxylic acids as Potential HIV-1 Integrase Inhibitors.</a> He, Q.Q., X. Zhang, L.M. Yang, Y.T. Zheng, and F. Chen, Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22468749].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22406118">Short cyclic Peptides Derived from the C-terminal Sequence of Alpha1-Antitrypsin Exhibit Significant anti-HIV-1 activity.</a> Jia, Q., X. Jiang, F. Yu, J. Qiu, X. Kang, L. Cai, L. Li, W. Shi, S. Liu, S. Jiang, and K. Liu, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2393-2395; PMID[22406118].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22374216">A Novel 3,4-Dihydropyrimidin-2(1H)-one: HIV-1 Replication Inhibitors with Improved Metabolic Stability.</a> Kim, J., T. Ok, C. Park, W. So, M. Jo, Y. Kim, M. Seo, D. Lee, S. Jo, Y. Ko, I. Choi, Y. Park, J. Yoon, M.K. Ju, J. Ahn, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, M. Liuzzi, P. Sommer, and Z. No, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2522-2526; PMID[22374216].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22487967">15-Deoxy-delta(12,14) -prostaglandin J(2) Inhibits Human Immunodeficiency Virus-1 TAT-induced Monocyte Chemoattractant Protein-1/CCL2 Production by Blocking the Extracellular Signal-regulated Kinase-1/2 Signaling Pathway Independently of Peroxisome Proliferator-activated Receptor-gamma and Heme oxygenase-1 in Rat Hippocampal Slices.</a> Kim, S.E., E.O. Lee, J.H. Yang, J.H. Kang, Y.H. Suh, and Y.H. Chong, Journal of Neuroscience Research, 2012. <b>[Epub ahead of print]</b>; PMID[22487967].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22301152">The Human Immunodeficiency Virus Type 1 Nef and Vpu Proteins Downregulate the Natural Killer Cell-activating Ligand PVR.</a> Matusali, G., M. Potesta, A. Santoni, C. Cerboni, and M. Doria, Journal of Virology, 2012. 86(8): p. 4496-4504; PMID[22301152].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22489094">Total Synthesis of (-)-13-Oxyingenol and Its Natural Derivative.</a> Ohyoshi, T., S. Funakubo, Y. Miyazawa, K. Niida, I. Hayakawa, and H. Kigoshi, Angewandte Chemie (International ed in English), 2012. <b>[Epub ahead of print]</b>; PMID[22489094].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22466936">Zinc(ii) Complexes of Constrained Antiviral Macrocycles.</a> Ross, A., J.H. Choi, T.M. Hunter, C. Pannecouque, S.A. Moggach, S. Parsons, E. De Clercq, and P.J. Sadler, Dalton Transactions, 2012. <b>[Epub ahead of print]</b>; PMID[22466936].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22292948">Molecular Dynamics of HIV1-Integrase in Complex with 93del - A Structural Perspective on the Mechanism of Inhibition.</a> Sgobba, M., O. Olubiyi, S. Ke, and S. Haider, Journal of Biomolecular Structure &amp; Dynamics, 2012. 29(5): p. 863-877; PMID[22292948].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22252805">Preexposure Prophylaxis with Albumin-conjugated C34 Peptide HIV-1 Fusion Inhibitor in SCID-hu Thy/Liv Mice.</a> Stoddart, C.A., G. Nault, S.A. Galkina, N. Bousquet-Gagnon, D. Bridon, and O. Quraishi, Antimicrobial Agents and Chemotherapy, 2012. 56(4): p. 2162-2165; PMID[22252805].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22406117">Optimization of 2,4-Diarylanilines as Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Sun, L.Q., B. Qin, L. Huang, K. Qian, C.H. Chen, K.H. Lee, and L. Xie, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(7): p. 2376-2379; PMID[22406117].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22462820">Modulation of HIV-1 Gag NC/p1 Cleavage Efficiency Affects Protease Inhibitor Resistance and Viral Replicative Capacity.</a> van Maarseveen, N.M., D. Andersson, M. Lepsik, A. Fun, P.J. Schipper, D. de Jong, C.A. Boucher, and M. Nijhuis, Retrovirology, 2012. 9(1): p. 29; PMID[22462820].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22475552">Characterization of Pyrimidine Nucleoside Phosphorylase of Mycoplasma hyorhinis: Implications for the Clinical Efficacy of Nucleoside Analogues.</a> Vande Voorde, J., F. Gago, K. Vrancken, S. Liekens, and J. Balzarini, The Biochemical Journal, 2012. <b>[Epub ahead of print]</b>; PMID[22475552].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22369862">Hydroxyl May not be Indispensable for Raltegravir: Design, Synthesis and SAR Studies of Raltegravir Derivatives as HIV-1 Inhibitors.</a> Wang, Z., M. Wang, X. Yao, Y. Li, W. Qiao, Y. Geng, Y. Liu, and Q. Wang, European Journal of Medicinal Chemistry, 2012. 50: p. 361-369; PMID[22369862].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22480197">Identification of Small Molecule Inhibitors of the HIV-1 Nucleocapsid - SL3 RNA Complex.</a> Warui, D.M. and A.M. Baranger, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22480197].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22345467">Kinase Control Prevents HIV-1 Reactivation in Spite of High Levels of Induced NF-kappaB Activity.</a> Wolschendorf, F., A. Bosque, T. Shishido, A. Duverger, J. Jones, V. Planelles, and O. Kutsch, Journal of Virology, 2012. 86(8): p. 4548-4558; PMID[22345467].
    <br />
    <b>[PubMed]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298738200007">Synthesis and Biological Activity of Bimorpholine and Its Carbanucleoside.</a> Ausmees, K., A. Selyutina, K. Kutt, K. Lippur, T. Pehk, M. Lopp, E. Zusinaite, A. Merits, and T. Kanger, Nucleosides Nucleotides &amp; Nucleic Acids, 2011. 30(11): p. 897-907; ISI[000298738200007].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299726800002">Synthesis of Dialkyl Phosphorylsuccinates from the Reaction of Thiouracil Derivatives with Dialkyl Acetylenedicarboxylates in the Presence of Trialkylphosphites.</a> Azizian, J., K. Yadollahzadeh, H. Tahermansouri, and A.S. Delbari, Phosphorus Sulfur and Silicon and the Related Elements, 2011. 186(9): p. 1844-1852; ISI[000299726800002].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299727300001">Synthesis, Characteriztion, and Biological Screening of Some Novel Thizaolidin-4-one and alpha-Aminophosphonate Derivatives.</a> Badadhe, P.V., N.M. Chavan, D.S. Ghotekar, P.G. Mandhane, R.S. Joshi, and C.H. Gill, Phosphorus Sulfur and Silicon and the Related Elements, 2011. 186(10): p. 2021-2032; ISI[000299727300001].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301560500017">Liver X Receptor Agonist Inhibits HIV-1 Replication and Prevents HIV-induced Reduction of Plasma HDL in Humanized Mouse Model of HIV Infection.</a> Dubrovsky, L., R. Van Duyne, S. Senina, I. Guendel, T. Pushkarsky, D. Sviridov, F. Kashanchi, and M. Bukrinsky, Biochemical and Biophysical Research Communications, 2012. 419(1): p. 95-98; ISI[000301560500017].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300404900015">Discovery of Dimeric Inhibitors by Extension into the Entrance Channel of HIV-1 Reverse Transcriptase.</a> Ekkati, A.R., M. Bollini, R.A. Domaoal, K.A. Spasov, K.S. Anderson, and W.L. Jorgensen, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(4): p. 1565-1568; ISI[000300404900015].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298931900012">Synthesis of Certain Novel 4H-Pyrano[3,2-h]quinoline Derivatives.</a> El-Agrody, A.M. and A.M. Al-Ghamdi, Arkivoc, 2011: p. 134-146; ISI[000298931900012].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298719900001">Pharmacokinetics of Antiretroviral Drugs in Anatomical Sanctuary Sites: The Fetal Compartment (Placenta and Amniotic Fluid).</a> Else, L.J., S. Taylor, D.J. Back, and S.H. Khoo, Antiviral Therapy, 2011. 16(8): p. 1139-1147; ISI[000298719900001].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000298719900002">Pharmacokinetics of Antiretroviral Drugs in Anatomical Sanctuary Sites: The Male and Female Genital Tract.</a> Else, L.J., S. Taylor, D.J. Back, and S.H. Khoo, Antiviral Therapy, 2011. 16(8): p. 1149-1167; ISI[000298719900002].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299687100006">Inhibition of HIV-1 Reverse Transcriptase Associated Activities by the Hydroalcoholic Extract of Casimiroa edulis Seeds.</a> Esposito, F., L. Zinzula, A. Maxia, E. Tramontano, and C. Sanna, Natural Product Research, 2011. 25(11): p. 1067-1073; ISI[000299687100006].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300446100006">Enhancing Protein Backbone BindinguA Fruitful Concept for Combating Drug-Resistant HIV.</a> Ghosh, A.K., D.D. Anderson, I.T. Weber, and H. Mitsuya, Angewandte Chemie-International Edition, 2012. 51(8): p. 1778-1802; ISI[000300446100006].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300850700008">A Short Hairpin Loop-structured Oligodeoxynucleotide Targeting the Virion-associated RNase H of HIV Inhibits HIV Production in Cell Culture and in huPBL-SCID Mice.</a> Heinrich, J., D. Schols, and K. Moelling, Intervirology, 2012. 55(3): p. 242-246; ISI[000300850700008].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300729900016">Multi-scale Modeling of HIV Infection in Vitro and APOBEC3G-based Anti-Retroviral Therapy.</a> Hosseini, I. and F. Mac Gabhann, Plos Computational Biology, 2012. 8(2); ISI[000300729900016].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300373200006">The Sugar Ring Conformation of 4 &#39;-Ethynyl-2-fluoro-2 &#39;-deoxyadenosine and Its Recognition by the Polymerase Active Site of HIV Reverse Transcriptase.</a> Kirby, K.A., K. Singh, E. Michailidis, B. Marchand, E.N. Kodama, N. Ashida, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos, Cellular and Molecular Biology, 2011. 57(1): p. 40-46; ISI[000300373200006].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300916000026">Schilancitrilactones A-C: Three Unique Nortriterpenoids from Schisandra lancifolia.</a> Luo, X., Y.M. Shi, R.H. Luo, S.H. Luo, X.N. Li, R.R. Wang, S.H. Li, Y.T. Zheng, X. Du, W.L. Xiao, J.X. Pu, and H.D. Sun, Organic Letters, 2012. 14(5): p. 1286-1289; ISI[000300916000026].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301289200008">Synthesis, Characterization and Coordination Chemistry of Substituted beta-Amino dicarbonyls.</a> Meskini, I., M. Daoudi, A. Kerbal, B. Bennani, J. Sheikh, A. Parvez, L. Toupet, and T. Ben Hadda, Journal of Saudi Chemical Society, 2012. 16(2): p. 161-173; ISI[000301289200008].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299726300014">Synthesis, Antibacterial, and Analgesic Activity of Novel 4-Hydroxy-3-(phenylthio)-2H-chromen-2-ones and 4-Hydroxy-3-[imidazol/tetrazolo-2-yl)thio]-2H-chromen-2-ones.</a> Rajesha, G., K.M. Mahadevan, N.D. Satyanarayan, and H.S.B. Naik, Phosphorus Sulfur and Silicon and the Related Elements, 2011. 186(8): p. 1733-1743; ISI[000299726300014].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301591100007">Prediction and in Vitro Evaluation of Selected Protease Inhibitor Antiviral Drugs as Inhibitors of Carboxylesterase 1: A Potential Source of Drug-Drug Interactions.</a> Rhoades, J.A., Y.K. Peterson, H.J. Zhu, D.I. Appel, C.A. Peloquin, and J.S. Markowitz, Pharmaceutical Research, 2012. 29(4): p. 972-982; ISI[000301591100007].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301188200011">Synthesis, Structure, and Luminescent Properties of 2-[2-(9-Anthryl)vinyl]quinolines</a>. Serdyuk, O.V., I.V. Evseenko, G.A. Dushenko, Y.V. Revinskii, and I.E. Mikhailov, Russian Journal of Organic Chemistry, 2012. 48(1): p. 78-82; ISI[000301188200011].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301169400011">Mitigating hERG Inhibition: Design of Orally Bioavailable CCR5 Antagonists as Potent Inhibitors of R5 HIV-1 Replication.</a> Skerlj, R., G. Bridger, Y. Zhou, E. Bourque, E. McEachern, S. Danthi, J. Langille, C. Harwig, D. Veale, B. Carpenter, T. Ba, M. Bey, I. Baird, T. Wilson, M. Metz, R. MacFarland, R. Mosi, V. Bodart, R. Wong, S. Fricker, D. Huskens, and D. Schols, Acs Medicinal Chemistry Letters, 2012. 3(3): p. 216-221; ISI[000301169400011].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300075700011">Quantitative Structure Activity Relationship Studies on a Series of Triazole Derivatives as Non-Nucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Srivastava, A.K., A. Pandey, A. Nath, and V.K. Pathak, Oxidation Communications, 2011. 34(4): p. 843-854; ISI[000300075700011].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>       
    
    <p class="plaintext">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301371500035">The Cellular Antiviral Protein APOBEC3G Interacts with HIV-1 Reverse Transcriptase and Inhibits Its Function during Viral Replication.</a> Wang, X.X., Z.J. Ao, L.Y. Chen, G. Kobinger, J.Y. Peng, and X.J. Yao, Journal of Virology, 2012. 86(7): p. 3777-3786; ISI[000301371500035].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301128300001">Synthesis and Biological Activities of Novel Isoxazoline-linked Pseudodisaccharide Derivatives.</a> Zhang, P.Z., C. Wei, E.K. Wang, W. Wang, M. Liu, Q.M. Yin, H. Chen, K.R. Wang, X.L. Li, and J.C. Zhang, Carbohydrate Research, 2012. 351: p. 7-16; ISI[000301128300001].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000301169800010">Anti-AIDS Agents 85. Design, Synthesis, and Evaluation of 1R,2R-Dicamphanoyl-3,3-dimethyldihydropyrano-[2,3-c]xanthen-7(1H)-one (DCX) Derivatives as Novel anti-HIV agents.</a> Zhou, T., Q. Shi, C.H. Chen, L. Huang, P. Ho, S.L. Morris-Natschke, and K.H. Lee, European Journal of Medicinal Chemistry, 2012. 47: p. 86-96; ISI[000301169800010].
    <br />
    <b>[WOS]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <h2>Patent citations</h2>

    <p class="plaintext">46. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120215&amp;CC=CN&amp;NR=102351931A&amp;KC=A">Preparation of Pyrimidine Nucleoside Derivatives as Antitumor and Antiviral Agents.</a> Chang, J., H. An, X. Yu, and X. Guo. Patent. 2012. 2011-10245782 102351931: 63pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">47. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120322&amp;CC=WO&amp;NR=2012035034A1&amp;KC=A1">Serpin-finger Fusions with Biologically Active Polypeptides for Anchoring with High Affinity and Functional Spatial Orientation.</a> Engh, R., E. Kopetzki, and T. Schlothauer. Patent. 2012. 2011-EP65884 2012035034: 37pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">48. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120308&amp;CC=WO&amp;NR=2012031237A1&amp;KC=A1">Fused Tricyclic ether carbamates for Treating HIV Infected Patients.</a> Ghosh, A.K., C.-X. Xu, H. Mitsuya, and G. Parham. Patent. 2012. 2011-US50393 2012031237: 40pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">49. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120223&amp;CC=WO&amp;NR=2012021964A1&amp;KC=A1">Compounds and Therapeutic Applications Related to Inhibition of Dendritic Cell Immunoreceptor (DCIR) Activity and Signaling Events.</a> Gilbert, C., A. Lambert, and M.J. Tremblay. Patent. 2012. 2011-CA888 2012021964: 95pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">50. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120126&amp;CC=US&amp;NR=2012022048A1&amp;KC=A1">Anti-proliferative Substituted Pyrazolo[3,4-D]pyrimidines to Inhibit Immune Activation, Virus Replication and Tumor Growth.</a> Lori, F., D.D. Forni, and M.R. Stevens. Patent. 2012. 2010-804551 20120022048: 29pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">51. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120315&amp;CC=WO&amp;NR=2012033735A1&amp;KC=A1">Pyrazolo[1,5-a]pyrimidine Derivatives as Inhibitors of Human Immunodeficiency Virus Replication and Their Preparation and Use for the Treatment of HIV Infection.</a> Pendri, A., G. Li, S. Gerritz, D.R. Langley, G.L. Trainor, and N.A. Meanwell. Patent. 2012. 2011-US50503 2012033735: 250pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">52. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120216&amp;CC=WO&amp;NR=2012020108A2&amp;KC=A2">Multimeric Inhibitors of Viral Fusion as Antiviral Agents.</a> Pessi, A. Patent. 2012. 2011-EP63888 2012020108: 165pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
    <p> </p>   
    
    <p class="plaintext">53. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20120208&amp;CC=CN&amp;NR=102344455A&amp;KC=A">Daphnane-Type Diterpene Orthoesters Compounds, Their Pharmaceutical Composition, Preparation Method and Application for Preventing or Treating Aids.</a> Zhou, J., Y. Zheng, S. Huang, Y. Zhao, X. Zhang, J. Hu, R. Wang, Y. Liu, and X. Zhang. Patent. 2012. 2011-10234308 102344455: 14pp.
    <br />
    <b>[Patent]</b>. HIV_0330-041212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
