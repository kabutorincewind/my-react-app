

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-133.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PQ/ijE7tezJRQOC9vechNpCD1CgpxSwqbhvT4TLh+EtAjrB3o7/rAYxAUNJDK9sGmhytraB/Nm3CM7ih3IvMsLh88vdr57plwObR6W/CW1xTE5HUf4+CKi4uzwA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5D9C6334" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-133-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59539   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Proteinase inhibitors from Streptomyces with antiviral activity</p>

    <p>          Serkedjieva, J, Angelova, L, Remichkova, M, and Ivanova, I</p>

    <p>          J Basic Microbiol <b>2006</b>.  46(6): 504-512</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139614&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139614&amp;dopt=abstract</a> </p><br />

    <p>2.     59540   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiviral agents active against influenza A viruses</p>

    <p>          De Clercq, E</p>

    <p>          Nat Rev Drug Discov <b>2006</b>.  5(12): 1015-1025</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139286&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139286&amp;dopt=abstract</a> </p><br />

    <p>3.     59541   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiviral and antimicrobial assessment of some selected flavonoids</p>

    <p>          Ozcelik, B, Orhan, I, and Toker, G</p>

    <p>          Z Naturforsch [C] <b>2006</b>.  61(9-10): 632-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17137105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17137105&amp;dopt=abstract</a> </p><br />

    <p>4.     59542   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Serum amyloid A has antiviral activity against hepatitis C virus by inhibiting virus entry in a cell culture system</p>

    <p>          Lavie, M, Voisset, C, Vu-Dac, N, Zurawski, V, Duverlie, G, Wychowski, C, and Dubuisson, J</p>

    <p>          Hepatology <b>2006</b>.  44(6): 1626-1634</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17133472&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17133472&amp;dopt=abstract</a> </p><br />

    <p>5.     59543   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Pandemic influenza: overview of vaccines and antiviral drugs</p>

    <p>          Cox, MM</p>

    <p>          Yale J Biol Med <b>2005</b>.  78(5): 321-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132338&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132338&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59544   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis, conformation, and bioactivity of novel analogues of the antiviral lipopeptide halovir A</p>

    <p>          Dalla, Bona A, Formaggio, F, Peggion, C, Kaptein, B, Broxterman, QB, Galdiero, S, Galdiero, M, Vitiello, M, Benedetti, E, and Toniolo, C</p>

    <p>          J Pept Sci <b>2006</b>.  12(12): 748-757</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17131285&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17131285&amp;dopt=abstract</a> </p><br />

    <p>7.     59545   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Entecavir: a new nucleoside analog for the treatment of chronic hepatitis B infection</p>

    <p>          Sims, KA and Woodland, AM</p>

    <p>          Pharmacotherapy <b>2006</b>.  26(12): 1745-57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125436&amp;dopt=abstract</a> </p><br />

    <p>8.     59546   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Application of Phosphoramidate ProTide Technology Significantly Improves Antiviral Potency of Carbocyclic Adenosine Derivatives</p>

    <p>          McGuigan, C, Hassan-Abdallah, A, Srinivasan, S, Wang, Y, Siddiqui, A, Daluge, SM, Gudmundsson, KS, Zhou, H, McLean, EW, Peckham, JP, Burnette, TC, Marr, H, Hazen, R, Condreay, LD, Johnson, L, and Balzarini, J</p>

    <p>          J Med Chem <b>2006</b>.  49(24): 7215-7226</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125274&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125274&amp;dopt=abstract</a> </p><br />

    <p>9.     59547   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Discovery of Conformationally Constrained Tetracyclic Compounds as Potent Hepatitis C Virus NS5B RNA Polymerase Inhibitors</p>

    <p>          Ikegashira, K, Oka, T, Hirashima, S, Noji, S, Yamanaka, H, Hara, Y, Adachi, T, Tsuruha, JI, Doi, S, Hase, Y, Noguchi, T, Ando, I, Ogura, N, Ikeda, S, and Hashimoto, H</p>

    <p>          J Med Chem <b>2006</b>.  49(24): 6950-6953</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125247&amp;dopt=abstract</a> </p><br />

    <p>10.   59548   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          The fight against new types of influenza virus</p>

    <p>          Romanova, J</p>

    <p>          Biotechnol J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17124703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17124703&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59549   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Oral 1-O-octadecyl-2-O-benzyl-sn-glycero-3-cidofovir targets the lung and is effective against a lethal respiratory challenge with ectromelia virus in mice</p>

    <p>          Hostetler, KY, Beadle, JR, Trahan, J, Aldern, KA, Owens, G, Schriewer, J, Melman, L, and Buller, RM</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17123638&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17123638&amp;dopt=abstract</a> </p><br />

    <p>12.   59550   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides for the prophylaxis and treatment of viral infections and ocovirus-caused cancers</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  WO <b>2006042418</b>  ISSUE DATE:  20060427</p>

    <p>          APPLICATION: 2005  PP: 166 pp.</p>

    <p>          ASSIGNEE:  (Replicor Inc., Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   59551   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          The acyclic nucleoside phosphonates from inception to clinical use: Historical perspective</p>

    <p>          De Clercq, E</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116336&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116336&amp;dopt=abstract</a> </p><br />

    <p>14.   59552   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antivirals in the management of an influenza pandemic</p>

    <p>          Harrod, ME, Emery, S, and Dwyer, DE</p>

    <p>          Med J Aust <b>2006</b>.  185(10): S58-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17115954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17115954&amp;dopt=abstract</a> </p><br />

    <p>15.   59553   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of quinazoline derivatives as antiviral agents</p>

    <p>          Cockerill, George Stuart, Flack, Stephen Sean, Mathews, Neil, and Salter, James Iain</p>

    <p>          PATENT:  WO <b>2006079833</b>  ISSUE DATE:  20060803</p>

    <p>          APPLICATION: 2006  PP: 27pp.</p>

    <p>          ASSIGNEE:  (Arrow Therapeutics Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59554   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          The in vitro activity of geraniin and 1,3,4,6-tetra-O-galloyl-beta-d-glucose isolated from Phyllanthus urinaria against herpes simplex virus type 1 and type 2 infection</p>

    <p>          Yang, CM, Cheng, HY, Lin, TC, Chiang, LC, and Lin, CC</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113739&amp;dopt=abstract</a> </p><br />

    <p>17.   59555   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV replication cycle by small interference RNAs silencing specific SARS proteins, 7a/7b, 3a/3b and S</p>

    <p>          Akerstrom, S, Mirazimi, A, and Tan, YJ</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112601&amp;dopt=abstract</a> </p><br />

    <p>18.   59556   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          TNF Protects Resistant C57BL/6 mice Against Herpes Simplex Virus Induced Encephalitis Independently of signaling via TNFR1 or TNFR2</p>

    <p>          Lundberg, P, Welander, PV, Edwards, CK , van Rooijen, N, and  Cantin, E</p>

    <p>          J Virol <b>2006</b> .</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108044&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108044&amp;dopt=abstract</a> </p><br />

    <p>19.   59557   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Inhibition of the heavy chain and {beta}2-microglobulin synthesis as a mechanism of MHC class I downregulation during Epstein-Barr virus replication</p>

    <p>          Guerreiro-Cacais, AO, Uzunel, M, Levitskaya, J, and Levitsky, V</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108039&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108039&amp;dopt=abstract</a> </p><br />

    <p>20.   59558   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Tetrahydrocarbazoles useful as inhibitors of hepatitis C and other viruses belonging to Flaviviridae</p>

    <p>          Gudmundsson, Kristjan and Samano, Vicente</p>

    <p>          PATENT:  WO <b>2006118607</b>  ISSUE DATE:  20061109</p>

    <p>          APPLICATION: 2005  PP: 69pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59559   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          QSAR analyses on avian influenza virus neuraminidase inhibitors using CoMFA, CoMSIA, and HQSAR</p>

    <p>          Zheng, M, Yu, K, Liu, H, Luo, X, Chen, K, Zhu, W, and Jiang, H</p>

    <p>          J Comput Aided Mol Des <b>2006</b>.  20(9): 549-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17103017&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17103017&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   59560   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Thiosialoside clusters using carbosilane dendrimer core scaffolds as a new class of influenza neuraminidase inhibitors</p>

    <p>          Sakamoto, JI, Koyama, T, Miyamoto, D, Yingsakmongkon, S, Hidari, KI, Jampangern, W, Suzuki, T, Suzuki, Y, Esumi, Y, Hatano, K, Terunuma, D, and Matsuoka, K</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17095224&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17095224&amp;dopt=abstract</a> </p><br />

    <p>23.   59561   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Role of Antiviral Drugs in Containing Pandemic Influenza. Contribution of Recent Modelling Exercises - Synthesis Prepared by the Invs/Inserm &quot;Epidemiology&quot; Group November 2005</p>

    <p>          Levy-Bruhl, D. </p>

    <p>          Medecine Et Maladies Infectieuses <b>2006</b>.  36(9): 449-453</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241919600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241919600003</a> </p><br />

    <p>24.   59562   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          HCV NS5B Nonnucleoside Inhibitors Specifically Block Single-Stranded Viral RNA Synthesis Catalyzed By HCV Replication Complexes In Vitro</p>

    <p>          Yang, W, Sun, Y, Phadke, A, Deshpande, M, and Huang, M</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088480&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088480&amp;dopt=abstract</a> </p><br />

    <p>25.   59563   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          New therapies for chronic hepatitis B infection</p>

    <p>          Chang, TT, Jia, JD, Omata, M, and Yoon, SK</p>

    <p>          Liver Int <b>2006</b>.  26 Suppl 2: 30-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087767&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17087767&amp;dopt=abstract</a> </p><br />

    <p>26.   59564   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          On the Lower Susceptibility of Oseltamivir to Influenza Neuraminidase Subtype N1 than those in N2 and N9</p>

    <p>          Aruksakunwong, O, Malaisree, M, Decha, P, Sompornpisut, P, Parasuk, V, Pianwanit, S, and Hannongbua, S</p>

    <p>          Biophys J <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085491&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17085491&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>27.   59565   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Retrocyclins: miniature lectins with potent antiviral activity - A Review</p>

    <p>          Lehrer, Robert I</p>

    <p>          Protein-Carbohydrate Interactions in Infectious Diseases <b>2006</b>.  2006: 92-105</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   59566   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis of Organotin Polyamine Ethers Containing Acyclovir and Their Preliminary Anticancer and Antiviral Activity</p>

    <p>          Carraher, C. <i>et al.</i></p>

    <p>          Journal of Inorganic and Organometallic Polymers and Materials <b>2006</b>.  16(3): 249-257</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242059800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242059800007</a> </p><br />

    <p>29.   59567   DMID-LS-133; PUBMED-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Avian influenza pandemic threat and health systems response</p>

    <p>          Bradt, DA and Drummond, CM</p>

    <p>          Emerg Med Australas <b>2006</b>.  18(5-6): 430-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17083631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17083631&amp;dopt=abstract</a> </p><br />

    <p>30.   59568   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Computational Study of the Effects of Mutations A156t, D168v, and D168q on the Binding of Hcv Protease Inhibitors</p>

    <p>          Guo, Z. <i>et al.</i></p>

    <p>          Journal of Chemical Theory and Computation <b>2006</b>.  2(6): 1657-1663</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241992200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241992200020</a> </p><br />

    <p>31.   59569   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of 1-(2&#39;-deoxy-2&#39;-fluoro-3&#39;-O-aminocarboxylic acid-b-L-arabinofuranosyl)-5-methyluracils as antiviral agents</p>

    <p>          Li, Wei</p>

    <p>          PATENT:  CN <b>1840538</b>  ISSUE DATE: 20061004</p>

    <p>          APPLICATION: 1005-9923  PP: 27pp.</p>

    <p>          ASSIGNEE:  (Peop. Rep. China)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   59570   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Prodrug Approaches of Nucleotides and Oligonucleotides</p>

    <p>          Poijarvi-Virta, P. and Lonnberg, H.</p>

    <p>          Current Medicinal Chemistry <b>2006</b>.  13(28): 3441-3465</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002300007</a> </p><br />

    <p>33.   59571   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiviral Properties of Deazaadenine Nucleoside Derivatives</p>

    <p>          Vittori, S. <i>et al.</i></p>

    <p>          Current Medicinal Chemistry <b>2006</b>.  13(29): 3529-3552</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242002600004</a> </p><br />

    <p>34.   59572   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Rig-I-Mediated Antiviral Responses to Single-Stranded Rna Bearing 5 &#39;-Phosphates</p>

    <p>          Pichlmair, A. <i> et al.</i></p>

    <p>          Science <b>2006</b>.  314(5801): 997-1001</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241896000055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241896000055</a> </p><br />

    <p>35.   59573   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Dual functional mRNA targeting, miRNA recruiting, RNA-silencing oligonucleotides for use as anti-viral agents</p>

    <p>          Zamore, Phillip D and Broderick, Jennifer</p>

    <p>          PATENT:  WO <b>2006113431</b>  ISSUE DATE:  20061026</p>

    <p>          APPLICATION: 2006  PP: 71pp.</p>

    <p>          ASSIGNEE:  (University of Massachusetts, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   59574   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          North-2&#39;-deoxy-methanocarbathymidines as antiviral agents against poxviruses</p>

    <p>          Tseng, Christopher K and Marquez, Victor E</p>

    <p>          PATENT:  WO <b>2006128159</b>  ISSUE DATE:  20061130</p>

    <p>          APPLICATION: 2006</p>

    <p>          ASSIGNEE:  (The Goverment of the United States of America, As Represented by the Secretary Department of Health and Human Services USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   59575   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Evaluation of the Influenza Virus-Inhibitory and Antioxidant Activities of Bulgarian and Turkish Medicinal Plants</p>

    <p>          Serkedjieva, J. <i>et al.</i></p>

    <p>          Febs Journal <b>2006</b>.  273: 343</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238914002369">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238914002369</a> </p><br />

    <p>38.   59576   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Cidofovir compounds, combinations, compositions and methods for the treatment of poxvirus infections</p>

    <p>          Almond, Merrick R and Painter, George R</p>

    <p>          PATENT:  WO <b>2006110655</b>  ISSUE DATE:  20061019</p>

    <p>          APPLICATION: 2006  PP: 43pp.</p>

    <p>          ASSIGNEE:  (Chimerix, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   59577   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Advances in Herpes Simplex Virus Antiviral Therapies</p>

    <p>          Mamidyala, S. and Firestine, S.</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2006</b>.  16(11): 1463-1480</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241825700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241825700001</a> </p><br />

    <p>40.   59578   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiherpetic Flavones From the Heartwood of Artocarpus Gomezianus</p>

    <p>          Likhitwitayawuid, K. <i>et al.</i></p>

    <p>          Chemistry &amp; Biodiversity <b>2006</b>.  3(10): 1138-1143</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241773400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241773400005</a> </p><br />

    <p>41.   59579   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antisense antiviral compound and method for treating ssrna viral infection</p>

    <p>          Iversen, Patrick L, Stein, David A, and Weller, Dwight D</p>

    <p>          PATENT:  US <b>2006269911</b>  ISSUE DATE:  20061130</p>

    <p>          APPLICATION: 2006-38815  PP: 64pp., Cont.-in-part of U.S. Ser. No. 226,995.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   59580   DMID-LS-133; SCIFINDER-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of indolylpentenediones, their medical compositions, and their use as antiviral agents</p>

    <p>          Kobayashi, Masanori, Fujishita, Toshio, and Sato, Akihiko</p>

    <p>          PATENT:  JP <b>2006169176</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2004-37750  PP: 12 pp.</p>

    <p>          ASSIGNEE:  (Shionogi and Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   59581   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Immunomodulators as an Antimicrobial Tool</p>

    <p>          Pirofski, L. and Casadevall, A.</p>

    <p>          Current Opinion in Microbiology <b>2006</b>.  9(5): 489-495</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241639400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241639400008</a> </p><br />

    <p>44.   59582   DMID-LS-133; WOS-DMID-12/4/2006</p>

    <p class="memofmt1-2">          Antiviral Management of Seasonal and Pandemic Influenza</p>

    <p>          Hayden, F. and Pavia, A.</p>

    <p>          Journal of Infectious Diseases <b>2006</b>.  194: S119-S126</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241503000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241503000009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
