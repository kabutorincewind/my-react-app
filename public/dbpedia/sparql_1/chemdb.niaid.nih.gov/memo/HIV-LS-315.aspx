

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-315.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ffR3ctcFbXi3fcnXT4Ca34JLXCpdM9Dzkuq1nMmdiePGevFfSAu3xtV38feTkLmLXiaNGbJojPeVlrRMwUCO+uwgansTSyprDo/sKDoZaMAJVZl+9s/bqTZ+nsk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E84C09B1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-315-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44792   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Antiviral Function of APOBEC3G Can Be Dissociated from Cytidine Deaminase Activity</p>

    <p>          Newman, Edmund NC, Holmes, Rebecca K, Craig, Heather M, Klein, Kevin C, Lingappa, Jaisri R, Malim, Michael H, and Sheehy, Ann M</p>

    <p>          Current Biology <b>2005</b>.  15(2): 166-170</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.         44793   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Dissecting the Effects of DNA Polymerase and Ribonuclease H Inhibitor Combinations on HIV-1 Reverse-Transcriptase Activities</p>

    <p>          Shaw-Reid, CA, Feuston, B, Munshi, V, Getty, K, Krueger, J, Hazuda, DJ, Parniak, MA, Miller, MD, and Lewis, D</p>

    <p>          Biochemistry <b>2005</b>.  44(5): 1595-1606</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15683243&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15683243&amp;dopt=abstract</a> </p><br />

    <p>3.         44794   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel acyclic nucleosides in the 5-alkynyl- and 6-alkylfuro[2,3-d]pyrimidine series</p>

    <p>          Amblard, Franck, Aucagne, Vincent, Guenot, Pierre, Schinazi, Raymond F, and Agrofoglio, Luigi A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(4): 1239-1248</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.         44795   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Caffeine inhibits human immunodeficiency virus type 1 transduction of nondividing cells</p>

    <p>          Daniel, R, Marusich, E, Argyris, E, Zhao, RY, Skalka, AM, and Pomerantz, RJ</p>

    <p>          J Virol <b>2005</b>.  79(4): 2058-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681408&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681408&amp;dopt=abstract</a> </p><br />

    <p>5.         44796   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          VCAM-1 Expression on CD8+ Cells Correlates with Enhanced Anti-HIV Suppressing Activity</p>

    <p>          Diaz, Leyla S, Foster, Hillary, Stone, Mars R, Fujimura, Sue, Relman, David A, and Levy, Jay A </p>

    <p>          Journal of Immunology <b>2005</b>.  174(3): 1574-1579</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.         44797   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Mechanism of action and resistant profile of anti-HIV-1 coumarin derivatives</p>

    <p>          Huang, L, Yuan, X, Yu, D, Lee, KH, and Ho, Chen C</p>

    <p>          Virology <b>2005</b>.  332(2): 623-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15680427&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15680427&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.         44798   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Indole, azaindole and related heterocyclic n-substituted piperazine derivatives</p>

    <p>          Yeung, Kap-Sun, Farkas, Michelle, Kadow, John F, Meanwell, Nicholas A, Taylor, Malcolm, Johnston, David, Coulter, Thomas Stephen, and Wright, JJKim</p>

    <p>          PATENT:  WO <b>2005004801</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 226 pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.         44799   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Human Immunodeficiency Virus Protease Inhibitors Accumulate into Cultured Human Adipocytes and Alter Expression of Adipocytokines</p>

    <p>          Vernochet, Cecile, Azoulay, Stephane, Duval, Daniele, Guedj, Roger, Cottrez, Francoise, Vidal, Hubert, Ailhaud, Gerard, and Dani, Christian</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(3): 2238-2243</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.         44800   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Methods for treatment of HIV or malaria using combinations of chloroquine and protease inhibitors</p>

    <p>          Savarino, Andrea</p>

    <p>          PATENT:  US <b>20050009810</b>  ISSUE DATE: 20050113</p>

    <p>          APPLICATION: 2004-62372  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (Italy)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       44801   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Antiviral compounds derived from naturally occurring proteins</p>

    <p>          Pellegrini, Antonio and Engels, Monika</p>

    <p>          Current Medicinal Chemistry: Anti-Infective Agents <b>2005</b>.  4(1): 55-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       44802   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Pharmacokinetics, pharmacodynamics and drug interaction potential of enfuvirtide</p>

    <p>          Patel, IH, Zhang, X, Nieforth, K, Salgo, M, and Buss, N</p>

    <p>          Clin Pharmacokinet <b>2005</b>.  44(2): 175-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15656696&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15656696&amp;dopt=abstract</a> </p><br />

    <p>12.       44803   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Resistance to dual nucleoside reverse-transcriptase inhibitors in children infected with HIV clade A/E</p>

    <p>          Lolekha, R, Sirivichayakul, S, Siangphoe, U, Pancharoen, C, Kaewchana, S, Apateerapong, W, Mahanontharit, A, Chotpitayasunondh, T, Ruxrungtham, K, Phanuphak, P, and Ananworanich, J</p>

    <p>          Clin Infect Dis <b>2005</b>.  40(2): 309-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15655753&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15655753&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.       44804   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          PEM-3-like proteins and encoding nucleic acids and their use in antiviral therapy and drug screening</p>

    <p>          Greener, Tsvika and Ben-Avraham, Danny</p>

    <p>          PATENT:  WO <b>2005001485</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 175 pp.</p>

    <p>          ASSIGNEE:  (Proteologics, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       44805   HIV-LS-315; SCIFINDER-HIV-2/1/2005</p>

    <p class="memofmt1-2">          Compositions for down-regulation of CCR5 expression and reducing HIV entry into T-cells</p>

    <p>          Redfield, Robert R, Amoroso, Anthony, Davis, Charles E, and Heredia, Alonsa</p>

    <p>          PATENT:  WO <b>2005001027</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (University of Maryland Biotechnology Institute, USA</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>15.       44806   HIV-LS-315; PUBMED-HIV-2/9/2005</p>

    <p class="memofmt1-2">          Rationale for maintenance of the M184v resistance mutation in human immunodeficiency virus type 1 reverse transcriptase in treatment experienced patients</p>

    <p>          Turner, D, Brenner, BG, Routy, JP, Petrella, M, and Wainberg, MA</p>

    <p>          New Microbiol <b>2004</b>.  27(2 Suppl 1): 31-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646062&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15646062&amp;dopt=abstract</a> </p><br />

    <p>16.       44807   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 infection by monoclonal antibodies to carbohydrates of Schistosoma mansoni</p>

    <p>          Antonio, M, Mello, G, Mascarenhas, RE, Ferraro, GA, Harn, D, Galvao-Castro, B, and Bou-Habib, DC</p>

    <p>          MEDICAL MICROBIOLOGY AND IMMUNOLOGY <b>2005</b>.  194(1-2): 61-65, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226096200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226096200009</a> </p><br />

    <p>17.       44808   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Enfuvirtide, first fusion inhibitor in the treatment of human immunodeficiency virus infection - Action mechanism and pharmacokinetics</p>

    <p>          Raffi, F</p>

    <p>          MEDECINE ET MALADIES INFECTIEUSES <b>2004</b>.  34: 3-7, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225859300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225859300002</a> </p><br />

    <p>18.       44809   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Antibodies that are cross-reactive for human immunodeficiency virus type 1 clade A and clade B V3 domains are common in patient sera from cameroon, but their neutralization activity is usually restricted by epitope masking</p>

    <p>          Krachmarov, C, Pinter, A, Honnen, WJ, Gorny, MK, Nyambi, PN, Zolla-Pazner, S, and Kayman, SC</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(2): 780-790, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700013</a> </p><br />
    <br clear="all">

    <p>19.       44810   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Anti-human immunodeficiency virus type 1 (HIV-1) antibodies 2F5 and 4E10 require surprisingly few crucial residues in the membrane-proximal external region of glycoprotein gp41 to neutralize HIV-1</p>

    <p>          Zwick, MB, Jensen, R, Church, S, Wang, M, Stiegler, G, Kunert, R, Katinger, H, and Burton, DR</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(2): 1252-1261, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226149700059</a> </p><br />

    <p>20.       44811   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Changes in the vif protein of HIV-1 associated with the development of resistance to inhibitors of viral protease</p>

    <p>          Adekale, MA, Cane, PA, and McCrae, MA</p>

    <p>          JOURNAL OF MEDICAL VIROLOGY <b>2005</b>.  75(2): 195-201, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226009400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226009400003</a> </p><br />

    <p>21.       44812   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Advances in the diagnosis and treatment of acute human immunodeficiency virus type 1 (HIV-1) infection</p>

    <p>          Miro, JM, Sued, O, Plana, M, Pumarola, T, and Gallart, T</p>

    <p>          ENFERMEDADES INFECCIOSAS Y MICROBIOLOGIA CLINICA <b>2004</b>.  22(10): 643-659, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226008500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226008500003</a> </p><br />

    <p>22.       44813   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Taking aim at a moving target: designing drugs to inhibit drug-resistant HIV-1 reverse transcriptases</p>

    <p>          Sarafianos, SG, Das, K, Hughes, SH, and Arnold, E</p>

    <p>          CURRENT OPINION IN STRUCTURAL BIOLOGY <b>2004</b>.  14(6): 716-730, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226045600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226045600012</a> </p><br />

    <p>23.       44814   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Cyclo-oxygenase type 2-dependent prostaglandin E-2 secretion is involved in retrovirus-induced T-cell dysfunction in mice</p>

    <p>          Rahmouni, S, Aandahl, EM, Nayjib, B, Zeddou, M, Giannini, S, Verlaet, M, Greimers, R, Boniver, J, Tasken, K, and Moutschen, M</p>

    <p>          BIOCHEMICAL JOURNAL <b>2004</b>.  384: 469-476, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226084000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226084000003</a> </p><br />

    <p>24.       44815   HIV-LS-315; WOS-HIV-1/30/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV type 1 replication using lentiviral-mediated delivery of mutant tRNA(Lys3) A58U</p>

    <p>          Renda, MJ, Bradel-Tretheway, B, Planelles, V, Bambara, RA, and Dewhurst, S</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2004</b>.  20(12): 1324-1334, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226049200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226049200007</a> </p><br />

    <p>25.       44816   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          HIV-1 incorporates and proteolytically processes human NDR1 and NDR2 serine-threonine kinases</p>

    <p>          Devroe, E, Silver, PA, and Engelman, A</p>

    <p>          VIROLOGY <b>2005</b>.  331(1): 181-189, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226210500017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226210500017</a> </p><br />

    <p>26.       44817   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          New analogues of the antitumor alkaloid girolline: The 4-deazathiogirolline series</p>

    <p>          Nay, B, Schiavi, B, Ahond, A, Poupat, C, and Potier, P</p>

    <p>          SYNTHESIS-STUTTGART <b>2005</b>.(1): 97-101, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226258700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226258700016</a> </p><br />

    <p>27.       44818   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Human immunodeficiency virus - one of nature&#39;s greatest evolutionary machines</p>

    <p>          Martin, D and Williamson, C</p>

    <p>          SOUTH AFRICAN JOURNAL OF SCIENCE <b>2004</b>.  100(9-10): 479-482, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226240500020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226240500020</a> </p><br />

    <p>28.       44819   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Pharmacokinetic and metabolic studies of retrojusticidin B, a potential anti-viral lignan, in rats</p>

    <p>          Wang, CY, Sun, SW, and Lee, SS</p>

    <p>          PLANTA MEDICA <b>2004</b>.  70(12): 1161-1165, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226292900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226292900011</a> </p><br />

    <p>29.       44820   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Changes in the SU can modulate the susceptibility of feline immunodeficiency virus to TM-derived entry inhibitors</p>

    <p>          Giannecchini, S, Pistello, M, Del, Santo B, Rovero, P, Sichi, O, and Bendinelli, M</p>

    <p>          MICROBIOLOGICA  <b>2004</b>.  27(2): 77-84, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226119200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226119200011</a> </p><br />

    <p>30.       44821   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p><b>          A new prospective against HIV infection: Induction of murin CCR5-downregulating antibodies</b> </p>

    <p>          Barassi, C, Marenzi, C, Pastori, C, Longhi, R, Lazzarin, A, and Lopalco, L</p>

    <p>          MICROBIOLOGICA  <b>2004</b>.  27(2): 85-94, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226119200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226119200012</a> </p><br />

    <p>31.       44822   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Tuftsin-AZT conjugate: potential macrophage targeting for AIDS therapy</p>

    <p>          Fridkin, M, Tsubery, H, Tzehoval, E, Vonsover, A, Biondi, L, Filira, F, and Rocchi, R</p>

    <p>          JOURNAL OF PEPTIDE SCIENCE <b>2005</b>.  11(1): 37-44, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226391100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226391100005</a> </p><br />

    <p>32.       44823   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Study on the interactions between anti-HIV-1 active compounds with trans-activation response RNA by affinity capillary electrophoresis</p>

    <p>          Li, D, Zhang, XX, Chang, WB, Wei, L, and Yang, M</p>

    <p>          JOURNAL OF CHROMATOGRAPHY B-ANALYTICAL TECHNOLOGIES IN THE BIOMEDICAL AND LIFE SCIENCES <b>2005</b>.  814(1): 99-104, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226272100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226272100013</a> </p><br />
    <br clear="all">

    <p>33.       44824   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Constituents from the stems of Hibiscus taiwanensis</p>

    <p>          Wu, PL, Wu, TS, He, CX, Su, CH, and Lee, KH</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2005</b>.  53(1): 56-59, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329000012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329000012</a> </p><br />

    <p>34.       44825   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Topochemical model for prediction of anti-HIV activity of HEPT analogs</p>

    <p>          Bajaj, S, Sambi, SS, and Madan, AK</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(2): 467-469, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226344800042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226344800042</a> </p><br />

    <p>35.       44826   HIV-LS-315; WOS-HIV-2/6/2005</p>

    <p class="memofmt1-2">          Anti-HIV benzylisoquinoline alkaloids and flavonoids from the leaves of Nelumbo nucifera, and structure-activity correlations with related alkaloids</p>

    <p>          Kashiwada, Y, Aoshima, A, Ikeshiro, Y, Chen, YP, Furukawa, H, Itoigawa, M, Fujioka, T, Mihashi, K, Cosentino, LM, Morris-Natschke, SL, and Lee, KH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(2): 443-448, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226343800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226343800014</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
