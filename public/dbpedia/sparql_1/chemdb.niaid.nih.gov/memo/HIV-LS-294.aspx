

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-294.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="aqYci1a+fU6FCoLMpV8j4uP9c8rPSB45Cdb5bX9GS6YHqB7/ps3ZenPXFm0UdvfHJztE21/WUsWOOo9df213CZ4aYtXX1NIrdRoCYkN6ci90FBwDlx0fFhbjrGY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="33B53C77" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-294-MEMO</b> </p>

<p>    1.    43214   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Anti-HIV-1 Cycloartanes from Leaves and Twigs of Gardenia thailandica</p>

<p>           Tuchinda, P, Saiai, A, Pohmakotr, M, Yoosook, C, Kasisit, J, Napaswat, C, Santisuk, T, and Reutrakul, V</p>

<p>           Planta Med 2004. 70(4): 366-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15095155&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15095155&amp;dopt=abstract</a> </p><br />

<p>    2.    43215   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Activator of g protein signaling 3; a gatekeeper of cocaine sensitization and drug seeking</p>

<p>           Bowers, MS, McFarland, K, Lake, RW, Peterson, YK, Lapish, CC, Gregory, ML, Lanier, SM, and Kalivas, PW</p>

<p>           Neuron 2004. 42(2): 269-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15091342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15091342&amp;dopt=abstract</a> </p><br />

<p>    3.    43216   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           GW433908/ritonavir once daily in antiretroviral therapy-naive HIV-infected patients: absence of protease resistance at 48 weeks</p>

<p>           MacManus, S, Yates, PJ, Elston, RC, White, S, Richards, N, and Snowden, W</p>

<p>           AIDS 2004. 18(4): 651-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090770&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090770&amp;dopt=abstract</a> </p><br />

<p>    4.    43217   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Simultaneous quantification of the new HIV protease inhibitors atazanavir and tipranavir in human plasma by high-performance liquid chromatography coupled with electrospray ionization tandem mass spectrometry</p>

<p>           Crommentuyn, KM, Rosing, H, Hillebrand, MJ, Huitema, AD, and Beijnen, JH</p>

<p>           J Chromatogr B Analyt Technol Biomed Life Sci 2004. 804(2): 359-367</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081931&amp;dopt=abstract</a> </p><br />

<p>    5.    43218   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           The use of a new in vitro reaction substrate reproducing both U3 and U5 regions of the HIV-1 3&#39;-ends increases the correlation between the in vitro and in vivo effects of the HIV-1 integrase inhibitors</p>

<p>           Tramontano, E, OniDi L, Esposito, F, Badas, R, and La Colla, P</p>

<p>           Biochem Pharmacol 2004. 67(9): 1751-1761</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081874&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081874&amp;dopt=abstract</a> </p><br />

<p>    6.    43219   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           Effect of stereo and regiochemistry towards wild and multidrug resistant HIV-1 virus: viral potency of chiral PETT derivatives*1</p>

<p>           Venkatachalam, Taracad K, Mao, Chen, and Uckun, Fatih M</p>

<p>           Biochemical Pharmacology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T4P-4C1NHC9-1/2/431317f1fe8f400f006357cd2026a2dd">http://www.sciencedirect.com/science/article/B6T4P-4C1NHC9-1/2/431317f1fe8f400f006357cd2026a2dd</a> </p><br />

<p>    7.    43220   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           CCR5 antagonists as anti-HIV-1 agents. Part 2: Synthesis and biological evaluation of N-[3-(4-benzylpiperidin-1-yl)propyl]-N,N(&#39;)-diphenylureas</p>

<p>           Imamura, S, Kurasawa, O, Nara, Y, Ichikawa, T, Nishikawa, Y, Iida, T, Hashiguchi, S, Kanzaki, N, Iizawa, Y, Baba, M, and Sugihara, Y</p>

<p>           Bioorg Med Chem 2004. 12(9): 2295-306</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080927&amp;dopt=abstract</a> </p><br />

<p>    8.    43221   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           [alpha],[alpha]-Trehalose derivatives bearing guanidino groups as inhibitors to HIV-1 Tat-TAR RNA interaction in human cells</p>

<p>           Wang, Min, Xu, Zhidong, Tu, Pengfei, Yu, Xiaolin, Xiao, Sulong, and Yang, Ming</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4C1FBPN-1/2/da8567b8b54131ee0351d2c7846cb57f">http://www.sciencedirect.com/science/article/B6TF9-4C1FBPN-1/2/da8567b8b54131ee0351d2c7846cb57f</a> </p><br />

<p>    9.    43222   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           Molecular mechanism of dioxolane nucleosides against 3TC resistant M184V mutant HIV</p>

<p>           Chong, Youhoon and Chu, Chung K</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4BYWSJK-1/2/4dee203f2c0c94bd07458cbda17a9995">http://www.sciencedirect.com/science/article/B6T2H-4BYWSJK-1/2/4dee203f2c0c94bd07458cbda17a9995</a> </p><br />

<p>  10.    43223   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Suppression of HIV-1 viral replication and cellular pathogenesis by a novel p38/JNK kinase inhibitor</p>

<p>           Muthumani, K, Wadsworth, SA, Dayes, NS, Hwang, DS, Choo, AY, Abeysinghe, HR, Siekierka, JJ, and Weiner, DB</p>

<p>           AIDS 2004. 18(5): 739-48</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15075508&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15075508&amp;dopt=abstract</a> </p><br />

<p>  11.    43224   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           QSAR study on some anti-HIV HEPT analogues using physicochemical and topological parameters</p>

<p>           Gayen, Shovanlal, Debnath, Bikash, Samanta, Soma, and Jha, Tarun</p>

<p>           Bioorganic &amp; Medicinal Chemistry 2004. 12(6): 1493-1503</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BN0DF1-3/2/cd94e2af7fe50155a7f8d151322d3bf5">http://www.sciencedirect.com/science/article/B6TF8-4BN0DF1-3/2/cd94e2af7fe50155a7f8d151322d3bf5</a> </p><br />

<p>  12.    43225   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Concise synthesis of anti-HIV-1 active (+)-inophyllum B and (+)-calanolide A by application of (-)-quinine-catalyzed intramolecular oxo-Michael addition</p>

<p>           Sekino, E, Kumamoto, T, Tanaka, T, Ikeda, T, and Ishikawa, T</p>

<p>           J Org Chem 2004. 69(8): 2760-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074925&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074925&amp;dopt=abstract</a> </p><br />

<p>  13.    43226   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           Clinical testing for HIV-1 drug resistance</p>

<p>           Iafrate, AJohn</p>

<p>           Clinical Microbiology Newsletter 2004. 26(5): 33-37</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T5D-4C237TT-3/2/53cd7721165a6aeb41ac3059ac9e588e">http://www.sciencedirect.com/science/article/B6T5D-4C237TT-3/2/53cd7721165a6aeb41ac3059ac9e588e</a> </p><br />

<p>  14.    43227   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Genotypic Analysis of HIV-1 Drug Resistance at the Limit of Detection: Virus Production without Evolution in Treated Adults with Undetectable HIV Loads</p>

<p>           Kieffer, TL, Finucane, MM, Nettles, RE, Quinn, TC, Broman, KW, Ray, SC, Persaud, D, and Siliciano, RF</p>

<p>           J Infect Dis 2004. 189(8): 1452-65</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073683&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073683&amp;dopt=abstract</a> </p><br />

<p>  15.    43228   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           HIV protease mutations associated with amprenavir resistance during salvage therapy: importance of I54M</p>

<p>           Murphy, MD, Marousek, GI, and Chou, S</p>

<p>           J Clin Virol 2004. 30(1): 62-67</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15072756&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15072756&amp;dopt=abstract</a> </p><br />

<p>  16.    43229   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           An inhibitor of glycosphingolipid metabolism blocks HIV-1 infection of primary T-cells</p>

<p>           Puri, A, Rawat, SS, Lin, HM, Finnegan, CM, Mikovits, J, Ruscetti, FW, and Blumenthal, R</p>

<p>           AIDS 2004. 18(6): 849-58</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15060432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15060432&amp;dopt=abstract</a> </p><br />

<p>  17.    43230   HIV-LS-294; EMBASE-HIV-4/21/2004</p>

<p class="memofmt1-2">           No effect of rosiglitazone for treatment of HIV-1 lipoatrophy: randomised, double-blind, placebo-controlled trial</p>

<p>           Carr, Andrew, Workman, Cassy, Carey, Dianne, Rogers, Gary, Martin, Allison, Baker, David, Wand, Handan, Law, Matthew, Samaras, Katherine, Emery, Sean, and Cooper, Prof David A</p>

<p>           The Lancet 2004. 363(9407): 429-438</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1B-4BMSWPG-B/2/5d0e2a4d62238a6280c064c2494201a2">http://www.sciencedirect.com/science/article/B6T1B-4BMSWPG-B/2/5d0e2a4d62238a6280c064c2494201a2</a> </p><br />

<p>  18.    43231   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           A phenylnorstatine inhibitor binding to HIV-1 protease: geometry, protonation, and subsite-pocket interactions analyzed at atomic resolution</p>

<p>           Brynda, J, Rezacova, P, Fabry, M, Horejsi, M, Stouracova, R, Sedlacek, J, Soucek, M, Hradilek, M, Lepsik, M, and Konvalinka, J</p>

<p>           J Med Chem 2004. 47(8): 2030-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056001&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15056001&amp;dopt=abstract</a> </p><br />

<p>  19.    43232   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Indium-mediated atom-transfer and reductive radical cyclizations of iodoalkynes: synthesis and biological evaluation of HIV-protease inhibitors</p>

<p>           Yanada, R, Koh, Y, Nishimori, N, Matsumura, A, Obika, S, Mitsuya, H, Fujii, N, and Takemoto, Y</p>

<p>           J Org Chem 2004. 69(7): 2417-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15049639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15049639&amp;dopt=abstract</a> </p><br />

<p>  20.    43233   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Pharmacological cyclin-dependent kinase inhibitors as HIV-1 antiviral therapeutics</p>

<p>           De La Fuente, C, Maddukuri, A, Kehn, K, Baylor, SY, Deng, L, Pumfery, A, and Kashanchi, F</p>

<p>           Curr HIV Res 2003. 1(2): 131-52</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043199&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15043199&amp;dopt=abstract</a> </p><br />

<p>  21.    43234   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Antiviral efficacy of abacavir in antiretroviral therapy-experienced adults harbouring HIV-1 with specific patterns of resistance to nucleoside reverse transcriptase inhibitors</p>

<p>           Lanier, ER, Ait-Khaled, M, Scott, J, Stone, C, Melby, T, Sturge, G, St, Clair M, Steel, H, Hetherington, S, Pearce, G, Spreen, W, and Lafon, S</p>

<p>           Antivir Ther 2004. 9(1): 37-45</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040535&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15040535&amp;dopt=abstract</a> </p><br />

<p>  22.    43235   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           Synthesis of furoannelated analogues of Emivirine (MKC-442)</p>

<p>           Wamberg, M, Pedersen, EB, and Nielsen, C</p>

<p>           Arch Pharm (Weinheim) 2004. 337(3):  148-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038059&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038059&amp;dopt=abstract</a> </p><br />

<p>  23.    43236   HIV-LS-294; PUBMED-OI-4/20/2004</p>

<p class="memofmt1-2">           A synthetic peptide bearing the HIV-1 integrase 161-173 amino acid residues mediates active nuclear import and binding to importin alpha: characterization of a functional nuclear localization signal</p>

<p>           Armon-Omer, A, Graessmann, A, and Loyter, A</p>

<p>           J Mol Biol 2004. 336(5): 1117-28</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15037073&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15037073&amp;dopt=abstract</a> </p><br />

<p>  24.    43237   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Mucosal toxicity studies of a gel formulation of native pokeweed antiviral protein</p>

<p>           D&#39;Cruz, OJ, Waurzyniak, B, and Uckun, FM</p>

<p>           TOXICOL PATHOL 2004. 32(2): 212-221</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220328500005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220328500005</a> </p><br />

<p>  25.    43238   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Identification of Staufen in the human immunodeficiency virus type 1 Gag ribonucleoprotein complex and a role in generating infectious viral particles</p>

<p>           Chatel-Chaix, L, Clement, JF, Martel, C, Beriault, V, Gatignol, A, DesGroseillers, L, and Mouland, AJ</p>

<p>           MOL CELL BIOL  2004. 24(7): 2637-2648</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220333800005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220333800005</a> </p><br />

<p>  26.    43239   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Antiviral effects of sulfated exopolysaccharide from the marine microalga Gyrodinium impudicum strain KG03</p>

<p>           Yim, JH, Kim, SJ, Ahn, SH, Lee, CK, Rhie, KT, and Lee, HK</p>

<p>           MAR BIOTECHNOL 2004. 6(1): 17-25</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334100003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334100003</a> </p><br />

<p>  27.    43240   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Azido-containing diketo acid derivatives inhibit human immunodeficiency virus type 1 integrase in vivo and influence the frequency of deletions at two-long-terminal-repeat-circle junctions</p>

<p>           Svarovskaia, ES, Barr, R, Zhang, XC, Pais, GCG, Marchand, C, Pommier, Y, Burke, TR, and Pathak, VK</p>

<p>           J VIROL 2004. 78(7): 3210-3222</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220293600002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220293600002</a> </p><br />

<p>  28.    43241   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Generation and characterization of neutralizing human monoclonal antibodies against human immunodeficiency virus type 1 tat antigen</p>

<p>           Moreau, E, Hoebeke, J, Zagury, D, Muller, S, and Desgranges, C</p>

<p>           J VIROL 2004. 78(7): 3792-3796</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220293600058">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220293600058</a> </p><br />

<p>  29.    43242   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           High level expression of human immunodeficiency virus type-1 Vif inhibits viral infectivity by modulating proteolytic processing of the gag precursor at the p2/nucleocapsid processing site</p>

<p>           Akari, H, Fujita, M, Kao, S, Khan, MA, Shehu-Xhilaga, M, Adachi, A, and Strebel, K</p>

<p>           J BIOL CHEM 2004. 279(13): 12355-12362</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334900043">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220334900043</a> </p><br />

<p>  30.    43243   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           The NEAT study: A 48-week open-label study to compare the antiviral efficacy and safety of GW433908 versus nelfinavir in antiretroviral therapy-naive HIV-1-infected patients</p>

<p>           Rodriguez-French, A, Boghossian, J, Gray, GE, Nadler, JP, Quinones, AR, Sepulveda, GE, Millard, JM, and Wannamaker, PG</p>

<p>           JAIDS-J ACQ IMM DEF 2004. 35(1): 22-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220326500003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220326500003</a> </p><br />

<p>  31.    43244   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Detection and enumeration of circulating HIV-1-specific memory B cells in HIV-1-infected patients</p>

<p>           Fondere, JM, Huguet, MF, Macura-Biegun, A, Baillat, V, Ohayon, V, Reynes, J, and Vendrell, JP </p>

<p>           JAIDS-J ACQ IMM DEF 2004. 35(2): 114-119</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327000002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327000002</a> </p><br />

<p>  32.    43245   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Differences in frequencies of drug resistance-associated mutations in the HIV-1 pol gene of B subtype and BF intersubtype recombinant samples</p>

<p>           Carobene, MG, Rubio, AE, Carrillo, MG, Maligne, GE, Kijak, GH, Quarleri, JF, and Salomon, H</p>

<p>           JAIDS-J ACQ IMM DEF 2004. 35(2): 207-209</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327000018">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327000018</a> </p><br />

<p>  33.    43246   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Anti-HIV effects of chloroquine - Inhibition of viral particle glycosylation and synergism with protease inhibitors</p>

<p>           Savarino, A, Lucia, MB, Rastrelli, E, Rutella, S, Golotta, C, Morra, E, Tamburrini, E, Perno, CF, Boelaert, JR, Sperber, K, and Cauda, R</p>

<p>           JAIDS-J ACQ IMM DEF 2004. 35(3): 223-232</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327600002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220327600002</a> </p><br />

<p>  34.    43247   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Fosamprenavir - A novel protease inhibitor and prodrug of amprenavir</p>

<p>           Ellis, JM, Ross, JW, and Coleman, CI</p>

<p>           FORMULARY 2004. 39(3): 151-+</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220347400006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220347400006</a> </p><br />

<p>  35.    43248   HIV-LS-294; WOS-HIV-4/11/2004</p>

<p class="memofmt1-2">           Characterisation of mutated proteinases derived from HIV-positive patients: Enzyme activity, vitality and inhibition</p>

<p>           Kozisek, M, Prejdova, J, Soucek, M, Machala, L, Stankova, M, Linka, M, Bruckova, M, and Konvalinka, J</p>

<p>           COLLECT CZECH CHEM C 2004. 69(3): 703-714</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220304500015">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220304500015</a> </p><br />

<p>  36.    43249   HIV-LS-294; WOS-HIV-4/18/2004</p>

<p class="memofmt1-2">           HIV - Vaginal microfloras as HIV inhibitors</p>

<p>           Clarkson, S</p>

<p>           NAT REV MICROBIOL 2003. 1(2): 90-91</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220402500007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220402500007</a> </p><br />

<p>  37.    43250   HIV-LS-294; WOS-HIV-4/18/2004</p>

<p class="memofmt1-2">           Quantitative detection of human immunodeficiency virus type 1 (HIV-1) proviral DNA in peripheral blood mononuclear cells by SYBR green real-time PCR technique</p>

<p>           Gibellini, D, Vitone, F, Schiavone, P, Ponti, C, La, Placa M, and Re, MC</p>

<p>           J CLIN VIROL 2004. 29(4): 282-289</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220447700011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220447700011</a> </p><br />

<p>  38.    43251   HIV-LS-294; WOS-HIV-4/18/2004</p>

<p class="memofmt1-2">           Benefits and limitations of testing for resistance to HIV drugs</p>

<p>           Richman, DD</p>

<p>           J ANTIMICROB CHEMOTH 2004. 53(4): 555-557</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220487000001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220487000001</a> </p><br />

<p>  39.    43252   HIV-LS-294; WOS-HIV-4/18/2004</p>

<p class="memofmt1-2">           Enzymology of a carbonyl reduction clearance pathway for the HIV integrase inhibitor, S-1360: role of human liver cytosolic aldo-keto reductases</p>

<p>           Rosemond, MJC, St, John-Williams L, Yamaguchi, T, Fujishita, T, and Walsh, JS</p>

<p>           CHEM-BIOL INTERACT 2004. 147(2): 129-139</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220413200003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220413200003</a> </p><br />

<p>  40.    43253   HIV-LS-294; WOS-HIV-4/18/2004</p>

<p class="memofmt1-2">           Antiviral inhibition of the HIV-1 capsid protein</p>

<p>           Kinde, I, Tang, C, Loeliger, E, and Kyere, S</p>

<p>           BIOPHYS J 2004. 86(1): 307A-308A</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187971201579">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187971201579</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
