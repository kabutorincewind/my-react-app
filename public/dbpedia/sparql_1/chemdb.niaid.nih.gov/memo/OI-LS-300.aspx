

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-300.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="E79v0jN4V3GLOEsGszxbz1CAMVyU1LAUygNvTtgWfuruTZx/9KJyszsT1u7u1eeU8JP6ZmPRNslPcBZu+Bv3ggx+4Nat5xeWblQDIt43yO6eWMZDJkDeYfevkFw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="48D11916" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-300-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43713   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Isolation of specific and high-affinity RNA aptamers against NS3 helicase domain of hepatitis C virus</p>

<p>           Hwang, B, Cho, JS, Yeo, HJ, Kim, JH, Chung, KM, Han, K, Jang, SK, and Lee, SW</p>

<p>           RNA 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247433&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247433&amp;dopt=abstract</a> </p><br />

<p>     2.    43714   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           S-Adenosylmethionine and Pneumocystis</p>

<p>           Merali, Salim and Clarkson, Jr Allen Boykin</p>

<p>           FEMS Microbiology Letters 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2W-4CSXM5D-1/2/8319ba900586c93656e94e281e7bc040">http://www.sciencedirect.com/science/article/B6T2W-4CSXM5D-1/2/8319ba900586c93656e94e281e7bc040</a> </p><br />

<p>     3.    43715   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           The transcriptional responses of M. tuberculosis to inhibitors of metabolism: Novel insights into drug mechanisms of action</p>

<p>           Boshoff, HI, Myers, TG, Copp, BR, McNeil, MR, Wilson, MA, and Barry, CE</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15247240&amp;dopt=abstract</a> </p><br />

<p>     4.    43716   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Evaluation of microplate Alamar blue assay for drug susceptibility testing of Mycobacterium avium complex isolates</p>

<p>           Vanitha, JD and Paramasivan, CN</p>

<p>           Diagn Microbiol Infect Dis 2004. 49(3): 179-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246507&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15246507&amp;dopt=abstract</a> </p><br />

<p>     5.    43717   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Oral Activity of Ether Lipid Ester Prodrugs of Cidofovir against Experimental Human Cytomegalovirus Infection</p>

<p>           Bidanset, DJ, Beadle, JR, Wan, WB, Hostetler, KY, and Kern, ER</p>

<p>           J Infect Dis 2004. 190(3): 499-503</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15243923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15243923&amp;dopt=abstract</a> </p><br />

<p>     6.    43718   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Newer animal models of Aspergillus and Candida infections</p>

<p>           Steinbach, William J and Zaas, Aimee K</p>

<p>           Drug Discovery Today: Disease Models 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B75D8-4CGNRSP-2/2/5214e58a245ae119157f0719496d9327">http://www.sciencedirect.com/science/article/B75D8-4CGNRSP-2/2/5214e58a245ae119157f0719496d9327</a> </p><br />

<p>    7.     43719   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           N-benzoyl-N&#39;-alkylthioureas and their complexes with Ni(II), Co(III) and Pt(II) - crystal structure of 3-benzoyl-1-butyl-1-methyl-thiourea: activity against fungi and yeast</p>

<p>           Campo, Rafael del, Criado, Julio J, Gheorghe, Ruxandra, Gonzalez, Francisco J, Hermosa, MR, Sanz, Francisca, Manzano, Juan L, Monte, Enrique, and Rodriguez-Fernandez, E</p>

<p>           Journal of Inorganic Biochemistry 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TGG-4CCFCSR-2/2/6e6b4a9e52a0ac559e933bf002f57482">http://www.sciencedirect.com/science/article/B6TGG-4CCFCSR-2/2/6e6b4a9e52a0ac559e933bf002f57482</a> </p><br />

<p>     8.    43720   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Crystal Structure of the TetR/CamR Family Repressor Mycobacterium tuberculosis EthR Implicated in Ethionamide Resistance</p>

<p>           Dover, LG, Corsino, PE, Daniels, IR, Cocklin, SL, Tatituri, V, Besra, GS, and Futterer, K</p>

<p>           J Mol Biol 2004. 340(5): 1095-105</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236969&amp;dopt=abstract</a> </p><br />

<p>     9.    43721   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Design, synthesis, and computational affinity prediction of ester soft drugs as inhibitors of dihydrofolate reductase from Pneumocystis carinii</p>

<p>           Graffner-Nordberg, Malin, Kolmodin, Karin, Aqvist, Johan, Queener, Sherry F, and Hallberg, Anders</p>

<p>           European Journal of Pharmaceutical Sciences 2004. 22(1): 43-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T25-4C7J3K2-3/2/1d92240363ce331484517ebd869199a9">http://www.sciencedirect.com/science/article/B6T25-4C7J3K2-3/2/1d92240363ce331484517ebd869199a9</a> </p><br />

<p>   10.    43722   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           A prospective multicentre study of discontinuing prophylaxis for opportunistic infections after effective antiretroviral therapy</p>

<p>           Green, H, Hay, P, Dunn, D, and McCormack, S</p>

<p>           HIV Med 2004. 5 (4): 278-283</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236617&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15236617&amp;dopt=abstract</a> </p><br />

<p>   11.    43723   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Extracellular mycobacterial DNA-binding protein 1 participates in mycobacterium-lung epithelial cell interaction through hyaluronic acid</p>

<p>           Aoki, K, Matsumoto, S, Hirayama, Y, Wada, T, Ozeki, Y, Niki, M, Domenech, P, Umemori, K, Yamamoto, S, Mineda, A, Matsumoto, M, and Kobayashi, K</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15234978&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15234978&amp;dopt=abstract</a> </p><br />

<p>   12.    43724   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Crystal structure of Mycobacterium tuberculosis catalase-peroxidase</p>

<p>           Bertrand, T, Eady, NA, Jones, JN, Nagy, JM, Jamart-Gregoire, B, Raven, EL, and Brown, KA</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231843&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231843&amp;dopt=abstract</a> </p><br />

<p>  13.     43725   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Mechanism of the interferon alpha response against hepatitis C virus replicons</p>

<p>           Guo, JT, Sohn, JA, Zhu, Q, and Seeger, C</p>

<p>           Virology 2004. 325(1): 71-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231387&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15231387&amp;dopt=abstract</a> </p><br />

<p>   14.    43726   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Ganciclovir resistance mutations in UL97 and UL54 genes of Human cytomegalovirus isolates resistant to ganciclovir</p>

<p>           Foulongne, V, Turriere, C, Diafouka, F, Abraham, B, Lastere, S, and Segondy, M</p>

<p>           Acta Virol 2004. 48(1): 51-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15230476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15230476&amp;dopt=abstract</a> </p><br />

<p>   15.    43727   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Antitubercular Constituents of Valeriana laxiflora</p>

<p>           Gu, JQ, Wang, Y, Franzblau, SG, Montenegro, G, Yang, D, and Timmermann, BN</p>

<p>           Planta Med 2004. 70(6): 509-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15229801&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15229801&amp;dopt=abstract</a> </p><br />

<p>   16.    43728   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Antituberculous activity of some aryl semicarbazone derivatives</p>

<p>           Sriram, D, Yogeeswari, P, and Thirumurugan, R</p>

<p>           Bioorg Med Chem Lett 2004. 14(15):  3923-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225698&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225698&amp;dopt=abstract</a> </p><br />

<p>   17.    43729   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           The folate pathway is a target for resistance to the drug para-aminosalicylic acid (PAS) in mycobacteria</p>

<p>           Rengarajan, J, Sassetti, CM, Naroditskaya, V, Sloutsky, A, Bloom, BR, and Rubin, EJ</p>

<p>           Mol Microbiol  2004. 53(1): 275-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225321&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15225321&amp;dopt=abstract</a> </p><br />

<p>   18.    43730   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Gain of virulence caused by loss of a gene in murine cytomegalovirus</p>

<p>           Bubi, I, Wagner, M, Krmpoti, A, Saulig, T, Kim, S, Yokoyama, WM, Jonji, S, and Koszinowski, UH</p>

<p>           J Virol 2004. 78(14): 7536-44</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220428&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15220428&amp;dopt=abstract</a> </p><br />

<p>  19.     43731   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Molecular cloning and biochemical characterization of Candida albicans acyl-CoA:sterol acyltransferase, a potential target of antifungal agents</p>

<p>           Kim, Ki-Young, Shin, Yu-Kyong, Park, Jong-Chul, Kim, Jung-Ho, Yang, Hongyuan, Han, Dong-Min, and Paik, Young-Ki</p>

<p>           Biochemical and Biophysical Research Communications 2004. 319(3): 911-919</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WBK-4CGMXM0-3/2/ec774d76a72d9eb9f2238837df005204">http://www.sciencedirect.com/science/article/B6WBK-4CGMXM0-3/2/ec774d76a72d9eb9f2238837df005204</a> </p><br />

<p>   20.    43732   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Interferon alpha-2b in combination with ribavirin for the treatment of chronic hepatitis C: assessment of virological, biochemical and histological treatment response</p>

<p>           Petrenkiene, V, Gudinaviciene, I, Jonaitis, L, and Kupcinskas, L</p>

<p>           Medicina (Kaunas) 2004. 40(6): 538-46</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15208476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15208476&amp;dopt=abstract</a> </p><br />

<p>   21.    43733   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Rhodanine-3-acetic acid derivatives as inhibitors of fungal protein mannosyl transferase 1 (PMT1)</p>

<p>           Orchard, Michael G, Neuss, Judi C, Galley, Carl MS, Carr, Andrew, Porter, David W, Smith, Phillip, Scopes, David IC, Haydon, David, Vousden, Katherine, and Stubberfield, Colin R</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CN9M5D-1/2/67018a70a670a0cb3043d8494fb83384">http://www.sciencedirect.com/science/article/B6TF9-4CN9M5D-1/2/67018a70a670a0cb3043d8494fb83384</a> </p><br />

<p>   22.    43734   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Novel bisbenzamidines as potential drug candidates for the treatment of Pneumocystis carinii pneumonia</p>

<p>           Vanden Eynde, Jean Jacques, Mayence, Annie, Huang, Tien L, Collins, Margaret S, Rebholz, Sandra, Walzer, Peter D, and Cushion, Melanie T</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CS4FPP-5/2/6d389e0053bdb4f7c6304e7f57a04ca9">http://www.sciencedirect.com/science/article/B6TF9-4CS4FPP-5/2/6d389e0053bdb4f7c6304e7f57a04ca9</a> </p><br />

<p>   23.    43735   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Novel prenyl-linked benzophenone substrate analogues of mycobacterial mannosyltransferases</p>

<p>           Guy, MR, Illarionov, PA, Gurcha, SS, Dover, LG, Gibson, KJ, Smith, PW, Minnikin, DE, and Besra, GS</p>

<p>           Biochem J 2004. Pt</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15202931&amp;dopt=abstract</a> </p><br />

<p>   24.    43736   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Multidrug-resistant tuberculosis in central Asia</p>

<p>           Cox, HS, Orozco, JD, Male, R, Ruesch-Gerdes, S, Falzon, D, Small, I, Doshetov, D, Kebede, Y, and Aziz, M</p>

<p>           Emerg Infect Dis 2004. 10(5): 865-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15200821&amp;dopt=abstract</a> </p><br />

<p>  25.     43737   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Structure-activity relationships for the selectivity of hepatitis C virus NS3 protease inhibitors</p>

<p>           Poliakov, Anton, Johansson, Anja, Akerblom, Eva, Oscarsson, Karin, Samuelsson, Bertil, Hallberg, Anders, and Danielson, UHelena</p>

<p>           Biochimica et Biophysica Acta (BBA) - General Subjects 2004. 1672(1): 51-59</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd">http://www.sciencedirect.com/science/article/B6T1W-4BW9NM9-1/2/280855c3c0525972fa44aa7e6f94c2bd</a> </p><br />

<p>   26.    43738   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Mutations in the human cytomegalovirus UL27 gene that confer resistance to maribavir</p>

<p>           Chou, S, Marousek, GI, Senters, AE, Davis, MG, and Biron, KK</p>

<p>           J Virol 2004. 78(13): 7124-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194788&amp;dopt=abstract</a> </p><br />

<p>   27.    43739   OI-LS-300; PUBMED-OI-7/13/2004</p>

<p class="memofmt1-3">           Toxoplasmosis</p>

<p>           Montoya, JG and Liesenfeld, O</p>

<p>           Lancet 2004. 363(9425): 1965-76</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194258&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194258&amp;dopt=abstract</a> </p><br />

<p>   28.    43740   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           Mycobacterial catalases, peroxidases, and superoxide dismutases and their effects on virulence and isoniazid-susceptibility in mycobacteria - a review</p>

<p>           Bartos, M, Falkinham, JO, and Pavlik, I</p>

<p>           VET MED-CZECH  2004. 49(5): 161-170</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221844700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221844700003</a> </p><br />

<p>   29.    43741   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           In vitro anti-mycobacterial activities of three species of Cola plant extracts (Sterculiaceae)</p>

<p>           Adeniyi, BA, Groves, MJ, and Gangadharam, PRJ</p>

<p>           PHYTOTHER RES  2004. 18(5): 414-418</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221802400014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221802400014</a> </p><br />

<p>   30.    43742   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           Lipoprotein processing is required for virulence of Mycobacterium tuberculosis</p>

<p>           Sander, P, Rezwan, M, Walker, B, Rampini, SK, Kroppenstedt, RM, Ehlers, S, Keller, C, Keeble, JR, Hagemeier, M, Colston, MJ, Springer, B, and Bottger, EC</p>

<p>           MOL MICROBIOL  2004. 52(6): 1543-1552</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221866300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221866300002</a> </p><br />

<p>   31.    43743   OI-LS-300; EMBASE-OI-7/15/2004</p>

<p class="memofmt1-3">           Inhibition of adherence of Mycobacterium avium complex and Mycobacterium tuberculosis to fibronectin on the respiratory mucosa</p>

<p>           Middleton, AM, Chadwick, MV, Nicholson, AG, Dewar, A, Groger, RK, Brown, EJ, Ratliff, TL, and Wilson, R</p>

<p>           Respiratory Medicine 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WWS-4CSG42G-1/2/a86f4677a59ff439327ae9c252508151">http://www.sciencedirect.com/science/article/B6WWS-4CSG42G-1/2/a86f4677a59ff439327ae9c252508151</a> </p><br />

<p>  32.     43744   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           Antifungal cyclopentenediones from Piper coruscans</p>

<p>           Li, XC, Ferreira, D, Jacob, MR, Zhang, QF, Khan, SI, ElSohly, HN, Nagle, DG, Smillie, TJ, Khan, IA, Walker, LA, and Clark, AM</p>

<p>           J AM CHEM SOC  2004. 126(22): 6872-6873</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221828200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221828200018</a> </p><br />

<p>   33.    43745   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           Microcionamides A and B, bioactive peptides from the Philippine sponge Clathria (Thalysias) abietina</p>

<p>           Davis, RA, Mangalindan, GC, Bojo, ZP, Antemano, RR, Rodriguez, NO, Concepcion, GP, Samson, SC, de, Guzman D, Cruz, LJ, Tasdemir, D, Harper, MK, Feng, XD, Carter, GT, and Ireland, CM</p>

<p>           J ORG CHEM 2004. 69(12): 4170-4176</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221867800021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221867800021</a> </p><br />

<p>   34.    43746   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           A new group of potential antituberculotics: Hydrochlorides of piperidinylalkyl esters of alkoxy-substituted phenylcarbamic acids</p>

<p>           Waisser, K, Drazkova, K, Cizmarik, J, and Kaustova, J</p>

<p>           FOLIA MICROBIOL 2004. 49(3): 265-268</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221874600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221874600004</a> </p><br />

<p>   35.    43747   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           A new 2-carbamoyl pteridine that inhibits mycobacterial FtsZ</p>

<p>           Reynolds, RC, Srivastava, S, Ross, LJ, Suling, WJ, and White, EL</p>

<p>           BIOORG MED CHEM LETT 2004. 14(12):  3161-3164</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800031</a> </p><br />

<p>   36.    43748   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           The monoethyl ester of meconic acid is an active site inhibitor of HCVNS5B RNA-dependent RNA polymerase</p>

<p>           Pace, P, Nizi, E, Pacini, B, Pesci, S, Matassa, V, De, Francesco R, Altamura, S, and Summa, V </p>

<p>           BIOORG MED CHEM LETT 2004. 14(12):  3257-3261</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800051">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221776800051</a> </p><br />

<p>   37.    43749   OI-LS-300; WOS-OI-7/4/2004</p>

<p class="memofmt1-3">           Interaction of antimycobacterial drugs with the anti-Mycobacterium avium complex effects of antimicrobial effectors, reactive oxygen intermediates, reactive nitrogen intermediates, and free fatty acids produced by macrophages</p>

<p>           Sano, K, Tomioka, H, Sato, K, Sano, C, Kawauchi, H, Cai, SS, and Shimizu, T</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(6): 2132-2139</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221813900032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221813900032</a> </p><br />

<p>   38.    43750   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Trypanocidal and antifungal activities of p-hydroxyacetophenone derivatives from Calea uniflora (Heliantheae, Asteraceae)</p>

<p>           do Nascimento, AM, Salvador, MJ, Candido, RC, de Albuquerque, S, and de Oliveira, DCR</p>

<p>           J PHARM PHARMACOL 2004. 56(5): 663-669</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221963900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221963900014</a> </p><br />

<p>   39.    43751   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Interferon-alpha for helpatitis C: antiviral or immunotherapy?</p>

<p>           Diepolder, HM </p>

<p>           J HEPATOL 2004. 40(6): 1030-1031</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221979400023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221979400023</a> </p><br />

<p>  40.     43752   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Biochemical requirements for PCBCK1 kinase activity, the Pneumocystis carinii MEKK involved in cell wall integrity</p>

<p>           Vohra, PK, Sanyal, B, and Thomas, CF</p>

<p>           FEMS MICROBIOL LETT 2004. 235(1):  153-156</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221923800021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221923800021</a> </p><br />

<p>   41.    43753   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Novel chimeric spermidine synthase-saccharopine dehydrogenase gene (SPE3-LYS9) in the human pathogen Cryptococcus neoformans</p>

<p>           Kingsbury, JM, Yang, ZG, Ganous, TM, Cox, GM, and McCusker, JH</p>

<p>           EUKARYOT CELL  2004. 3(3): 752-763</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222076000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222076000018</a> </p><br />

<p>   42.    43754   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Synthesis of new 2 &#39;-beta-C-methyl related triciribine analogues as anti-HCV agents</p>

<p>           Smith, KL, Lai, VCH, Prigaro, BJ, Ding, YL, Gunic, E, Girardet, JL, Zhong, WD, Hong, Z, Lang, S, and An, HY</p>

<p>           BIOORG MED CHEM LETT 2004. 14(13):  3517-3520</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221998100028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221998100028</a> </p><br />

<p>   43.    43755   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           QSAR and ADME</p>

<p>           Hansch, C, Leo, A, Mekapati, SB, and Kurup, A</p>

<p>           BIOORGAN MED CHEM 2004. 12(12): 3391-3400</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221937600030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221937600030</a> </p><br />

<p>   44.    43756   OI-LS-300; WOS-OI-7/12/2004</p>

<p class="memofmt1-3">           Characterization of Mycobacterium tuberculosis NAD kinase: Functional analysis of the full-length enzyme by site-directed mutagenesis</p>

<p>           Raffaelli, N, Finaurini, L, Mazzola, F, Pucci, L, Sorci, L, Amici, A, and Magni, G</p>

<p>           BIOCHEMISTRY-US 2004. 43(23): 7610-7617</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221915100036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000221915100036</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
