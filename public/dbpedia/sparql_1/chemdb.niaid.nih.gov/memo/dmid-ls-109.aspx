

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-109.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="12eMwe4VbFAOnkggapLdDdUpFMTBYT5KGfH7DznTRVqB2k5/s9lRTCSwvCM4Ki/IQS4WPHZbZmmD9Df9Sv9ZesFxrd58uPuyFhrp+pT9g3eoaNqqbfitpfJHoDA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E9DB6727" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-109-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58104   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-HIV, Anti-Poxvirus, and Anti-SARS Activity of a Nontoxic, Acidic Plant Extract from the Trifollium Species Secomet-V/anti-Vac Suggests That It Contains a Novel Broad-Spectrum Antiviral</p>

    <p>          Kotwal, GJ, Kaczmarek, JN, Leivers, S, Ghebremariam, YT, Kulkarni, AP, Bauer, G, DE, Beer C, Preiser, W, and Mohamed, AR</p>

    <p>          Ann N Y Acad Sci <b>2005</b>.  1056: 293-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387696&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387696&amp;dopt=abstract</a> </p><br />

    <p>2.     58105   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p><b>          Anti-HBV nucleotide prodrug analogs: Synthesis, bioreversibility, and cytotoxicity studies</b> </p>

    <p>          Padmanabhan, S, Coughlin, JE, Zhang, G, Kirk, CJ, and Iyer, RP</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16387496&amp;dopt=abstract</a> </p><br />

    <p>3.     58106   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent Advances in Antiviral Nucleoside and Nucleotide Therapeutics</p>

    <p>          Simons, C., Wu, Q., and Htar, T.</p>

    <p>          Current Topics in Medicinal Chemistry <b>2005</b>.  5(13): 1191-1203</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233919200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233919200002</a> </p><br />

    <p>4.     58107   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro research of anti-HSV-1 activity in different extracts from Pacific oysters Crassostrea gigas</p>

    <p>          Olicard, C, Didier, Y, Marty, C, Bourgougnon, N, and Renault, T</p>

    <p>          Dis Aquat Organ <b>2005</b>.  67(1-2): 141-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16385820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16385820&amp;dopt=abstract</a> </p><br />

    <p>5.     58108   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rift Valley Fever Virus</p>

    <p>          Flick, R. and Bouloy, M.</p>

    <p>          Current Molecular Medicine <b>2005</b>.  5(8): 827-834</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233918400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233918400009</a> </p><br />

    <p>6.     58109   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Activity of Novel Acyclic Versions of Neplanocin a</p>

    <p>          Wu, Y. and Hong, J.</p>

    <p>          Archiv Der Pharmazie <b>2005</b>.  338(11): 517-521</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400001</a> </p><br />
    <br clear="all">

    <p>7.     58110   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Terpenoid Constituents of Ganoderma pfeifferi</p>

    <p>          Niedermeyer, TH, Lindequist, U, Mentel, R, Gordes, D, Schmidt, E, Thurow, K, and Lalk, M</p>

    <p>          J Nat Prod <b>2005</b>.  68(12): 1728-1731</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378363&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378363&amp;dopt=abstract</a> </p><br />

    <p>8.     58111   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficient Inhibition of Hepatitis B Virus Replication by Hammerhead Ribozymes Delivered by Hepatitis Delta Virus</p>

    <p>          Li, X. <i>et al.</i></p>

    <p>          Virus Research  <b>2005</b>.  114(1-2): 126-132</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233645100015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233645100015</a> </p><br />

    <p>9.     58112   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in the molecular biology of west nile virus</p>

    <p>          Beasley, DW</p>

    <p>          Curr Mol Med <b>2005</b>.  5(8): 835-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375717&amp;dopt=abstract</a> </p><br />

    <p>10.   58113   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of a New Category of Purine-Nucleoside Analogues</p>

    <p>          Li, D. <i>et al.</i></p>

    <p>          Chinese Journal of Chemistry <b>2005</b>.  23(12): 1659-1664</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233708600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233708600016</a> </p><br />

    <p>11.   58114   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The discovery of finger loop inhibitors of the hepatitis C virus NS5B polymerase: Status and prospects for novel HCV therapeutics</p>

    <p>          Beaulieu, PL</p>

    <p>          IDrugs <b>2006</b>.  9(1): 39-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16374732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16374732&amp;dopt=abstract</a> </p><br />

    <p>12.   58115   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral properties of ent-labdene diterpenes of Andrographis paniculata nees, inhibitors of herpes simplex virus type 1</p>

    <p>          Wiart, C, Kumar, K, Yusof, MY, Hamimah, H, Fauzi, ZM, and Sulaiman, M</p>

    <p>          Phytother Res <b>2005</b>.  19(12): 1069-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16372376&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16372376&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   58116   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oseltamivir resistance--disabling our influenza defenses</p>

    <p>          Moscona, A</p>

    <p>          N Engl J Med <b>2005</b>.  353(25): 2633-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371626&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371626&amp;dopt=abstract</a> </p><br />

    <p>14.   58117   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The 2&#39;,5&#39; oligoadenylate synthetase 1b is a potent inhibitor of West Nile virus replication inside infected cells</p>

    <p>          Kajaste-Rudnitski, A, Mashimo, T, Frenkiel, MP, Guenet, JL, Lucas, M, and Despres, P</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371364&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371364&amp;dopt=abstract</a> </p><br />

    <p>15.   58118   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Respiratory syncytial virus fusion inhibitors. Part 3: Water-soluble benzimidazol-2-one derivatives with antiviral activity in vivo</p>

    <p>          Yu, KL, Wang, XA, Civiello, RL, Trehan, AK, Pearce, BC, Yin, Z, Combrink, KD, Gulgeze, HB, Zhang, Y, Kadow, KF, Cianci, CW, Clarke, J, Genovesi, EV, Medina, I, Lamb, L, Wyde, PR, Krystal, M, and Meanwell, NA</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368233&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368233&amp;dopt=abstract</a> </p><br />

    <p>16.   58119   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Candidates for therapy: HBV</p>

    <p>          Dusheiko, G</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368159&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368159&amp;dopt=abstract</a> </p><br />

    <p>17.   58120   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human herpesvirus 6 (HHV-6) U94/REP protein inhibits betaherpesvirus replication</p>

    <p>          Caselli, E, Bracci, A, Galvan, M, Boni, M, Rotola, A, Bergamini, C, Cermelli, C, Dal, Monte P, Gompels, UA, Cassai, E, and Di, Luca D</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16368124&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58121   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Divergent activities of interferon-alpha subtypes against intracellular hepatitis C virus replication</p>

    <p>          Koyama, T, Sakamoto, N, Tanabe, Y, Nakagawa, M, Itsui, Y, Takeda, Y, Kakinuma, S, Sekine, Y, Maekawa, S, Yanai, Y, Kurimoto, M, and Watanabe, M</p>

    <p>          Hepatol Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364683&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364683&amp;dopt=abstract</a> </p><br />

    <p>19.   58122   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          (+)-12alpha-Hydroxysophocarpine, a new quinolizidine alkaloid and related anti-HBV alkaloids from Sophora flavescens</p>

    <p>          Ding, PL, Liao, ZX, Huang, H, Zhou, P, and Chen, DF</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364643&amp;dopt=abstract</a> </p><br />

    <p>20.   58123   DMID-LS-109; SCIFINDER-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, Reactions, and Antiviral Activity of 5&#39;-Acetyl-6&#39;-methyl-2&#39;-thioxo-1&#39;,2&#39;-dihydro-3,4&#39;-bipyridine-3&#39;-carbonitrile</p>

    <p>          Attaby, Fawzy, Ali, M, Elghandour, A, and Ibrahem, Yasser</p>

    <p>          Phosphorus, Sulfur and Silicon and the Related Elements <b>2006</b>.  181(1): 1-14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   58124   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Role of Indoleamine 2,3-Dioxygenase in Antiviral Activity of Interferon-gamma Against Vaccinia Virus</p>

    <p>          Terajima, M and Leporati, AM</p>

    <p>          Viral Immunol <b>2005</b>.  18(4): 722-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359238&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16359238&amp;dopt=abstract</a> </p><br />

    <p>22.   58125   DMID-LS-109; WOS-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hcmv Activity of Novel 5&#39;-Norcarboacyclic Nucleosides</p>

    <p>          Kim, A. and Hong, J.</p>

    <p>          Archiv Der Pharmazie <b>2005</b>.  338(11): 522-527</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233783400002</a> </p><br />

    <p>23.   58126   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mutations conferring resistance to SCH6, a novel hepatitis C virus NS3/4A protease inhibitor: Reduced RNA replication fitness and partial rescue by second-site mutations</p>

    <p>          Yi, M, Tong, X, Skelton, A, Chase, R, Chen, T, Prongay, A, Bogen, SL, Saksena, AK, Njoroge, FG, Veselenak, RL, Pyles, RB, Bourne, N, Malcolm, BA, and Lemon, SM</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352601&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16352601&amp;dopt=abstract</a> </p><br />

    <p>24.   58127   DMID-LS-109; SCIFINDER-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of New 5-Substituted 2&#39;-Deoxyuridine Derivatives</p>

    <p>          Ivanov, AV, Simonyan, AR, Belanov, EF, and Aleksandrova, LA</p>

    <p>          Russian Journal of Bioorganic Chemistry <b>2005</b>.  31(6): 556-562</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   58128   DMID-LS-109; SCIFINDER-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          John Montgomery&#39;s Legacy: Carbocyclic Adenosine Analogues as Sah Hydrolase Inhibitors with Broad-Spectrum Antiviral Activity</p>

    <p>          De Clercq, E</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1395-1415</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   58129   DMID-LS-109; PUBMED-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The anti-malaria drug artesunate inhibits replication of cytomegalovirus in vitro and in vivo</p>

    <p>          Kaptein, SJ, Efferth, T, Leis, M, Rechter, S, Auerochs, S, Kalmer, M, Bruggeman, CA, Vink, C, Stamminger, T, and Marschall, M</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16325931&amp;dopt=abstract</a> </p><br />

    <p>27.   58130   DMID-LS-109; SCIFINDER-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Coccomyxa extracts and polysaccharide fractions as antiviral medicines</p>

    <p>          Hayashi, Toshimitsu, Watanabe, Yasuo, and Washimi, Akira</p>

    <p>          PATENT:  JP <b>2005247757</b>  ISSUE DATE:  20050915</p>

    <p>          APPLICATION: 2004-61033  PP: 7 pp.</p>

    <p>          ASSIGNEE:  (Nikken Sohonsha Corp., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   58131   DMID-LS-109; SCIFINDER-DMID-1/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Papillomavirus Protein Function in Cervical Cancer Cells by Intrabody Targeting</p>

    <p>          Griffin, Heather, Elston, Robert, Jackson, Deborah, Ansell, Keith, Coleman, Michael, Winter, Greg, and Doorbar, John</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  355(3): 360-378</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
