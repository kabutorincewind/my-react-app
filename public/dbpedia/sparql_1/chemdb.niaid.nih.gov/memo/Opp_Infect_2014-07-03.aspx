

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-07-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="phWcaesAw2L6oyaj5WLZvL5WO1sicNT9CN1o1Tn1EaGbyEYoIAQsIoMxREhDe9nBXwuYdrnMbSfO/X17vwzr2eeaRpSQQMnnyQXqKmEwnk4oCjZHVZQn5C1C4DY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8638C91D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: June 20 - July 3, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24472654">Synthesis, Crystal Structure and Potential Antimicrobial Activities of di(4-Sulfamoyl-phenyl-ammonium) sulphate.</a> Essghaier, B., A. Naouar, J. Abdelhak, M.F. Zid, and N. Sadfi-Zouaoui. Microbiological Research, 2014. 169(7-8): p. 504-510. PMID[24472654].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24733474">In Vitro Antifungal Susceptibility of Candida glabrata to Caspofungin and the Presence of FKS Mutations Correlate with Treatment Response in an Immunocompromised Murine Model of Invasive Infection.</a> Fernandez-Silva, F., M. Lackner, J. Capilla, E. Mayayo, D. Sutton, M. Castanheira, A.W. Fothergill, C. Lass-Florl, and J. Guarro. Antimicrobial Agents and Chemotherapy, 2014. 58(7): p. 3646-3649. PMID[24733474].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24856180">Design, Synthesis and Evaluation of Novel Quinazoline-2,4-dione Derivatives as Chitin Synthase Inhibitors and Antifungal Agents.</a> Ji, Q., D. Yang, X. Wang, C. Chen, Q. Deng, Z. Ge, L. Yuan, X. Yang, and F. Liao. Bioorganic &amp; Medicinal Chemistry, 2014. 22(13): p. 3405-3413. PMID[24856180].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24766994">Diarylheptanoids and Phenylphenalenones from Musa itinerans Fruits.</a> Liu, F., Y. Zhang, Q.Y. Sun, F.M. Yang, W. Gu, J. Yang, H.M. Niu, Y.H. Wang, and C.L. Long. Phytochemistry, 2014. 103: p. 171-177. PMID[24766994].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24221134">Anti-candida Activity of 1-18 Fragment of the Frog Skin Peptide Esculentin-1b: In Vitro and in Vivo Studies in a Caenorhabditis elegans Infection Model.</a> Luca, V., M. Olivi, A. Di Grazia, C. Palleschi, D. Uccelletti, and M.L. Mangoni. Cellular and Molecular Life Sciences, 2014. 71(13): p. 2535-2546. PMID[24221134].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24968329">Synthesis, Anti-microbial and Molecular Docking Studies of Quinazolin-4(3H)-one Derivatives.</a> Mabkhot, Y.N., M.S. Al-Har, A. Barakat, F.D. Aldawsari, A. Aldalbahi, and Z. Ul-Haq. Molecules, 2014. 19(7): p. 8725-8739. PMID[24968329].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24926538">Antifungal Amphidinol 18 and its 7-Sulfate Derivative from the Marine Dinoflagellate Amphidinium carterae.</a> Nuzzo, G., A. Cutignano, A. Sardo, and A. Fontana. Journal of Natural Products, 2014. 77(6): p. 1524-1527. PMID[24926538].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24623187">A Sunflower Lectin with Antifungal Properties and Putative Medical Mycology Applications.</a> Regente, M., G.B. Taveira, M. Pinedo, M.M. Elizalde, A.J. Ticchi, M.S. Diz, A.O. Carvalho, L. de la Canal, and V.M. Gomes. Current Microbiology, 2014. 69(1): p. 88-95. PMID[24623187].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br />  

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24857492">Evaluation of the Biological Effects of 5-Cl-8-Oxyquinolinepropoxycalix[4]arene and 8-Oxyquinolinepropoxycalix[4]arene in Vitro and in Vivo.</a> Soares, M.N., Jr., T.M. Gascon, F.L. Fonseca, K.S. Ferreira, and I.A. Bagatin. Materials Science and Engineering: C, 2014. 40: p. 260-266. PMID[24857492].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24752256">Synergistic Activity of the Tyrocidines, Antimicrobial Cyclodecapeptides from Bacillus aneurinolyticus, with Amphotericin B and Caspofungin against Candida albicans Biofilms.</a> Troskie, A.M., M. Rautenbach, N. Delattin, J.A. Vosloo, M. Dathe, B.P. Cammue, and K. Thevissen. Antimicrobial Agents and Chemotherapy, 2014. 58(7): p. 3697-3707. PMID[24752256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24852277">Synthesis of Novel 1,2,3-Triazole Derivatives of Isoniazid and their in Vitro and in Vivo Antimycobacterial Activity Evaluation.</a> Kumar, D., Beena, G. Khare, S. Kidwai, A.K. Tyagi, R. Singh, and D.S. Rawat. European Journal of Medicinal Chemistry, 2014. 81: p. 301-313. PMID[24852277].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24836065">Design, Synthesis and Biological Evaluation of Novel Isoniazid Derivatives with Potent Antitubercular Activity.</a> Martins, F., S. Santos, C. Ventura, R. Elvas-Leitao, L. Santos, S. Vitorino, M. Reis, V. Miranda, H.F. Correia, J. Aires-de-Sousa, V. Kovalishyn, D.A. Latino, J. Ramos, and M. Viveiros. European Journal of Medicinal Chemistry, 2014. 81: p. 119-138. PMID[24836065].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24871036">4-Aminoquinolone piperidine amides: Noncovalent Inhibitors of DprE1 with Long Residence Time and Potent Antimycobacterial Activity.</a> Naik, M., V. Humnabadkar, S.J. Tantry, M. Panda, A. Narayan, S. Guptha, V. Panduga, P. Manjrekar, L.K. Jena, K. Koushik, G. Shanbhag, S. Jatheendranath, M.R. Manjunatha, G. Gorai, C. Bathula, S. Rudrapatna, V. Achar, S. Sharma, A. Ambady, N. Hegde, J. Mahadevaswamy, P. Kaur, V.K. Sambandamurthy, D. Awasthy, C. Narayan, S. Ravishankar, P. Madhavapeddi, J. Reddy, K. Prabhakar, R. Saralaya, M. Chatterji, J. Whiteaker, B. McLaughlin, L.R. Chiarelli, G. Riccardi, M.R. Pasca, C. Binda, J. Neres, N. Dhar, F. Signorino-Gelo, J.D. McKinney, V. Ramachandran, R. Shandil, R. Tommasi, P.S. Iyer, S. Narayanan, V. Hosagrahara, S. Kavanagh, N. Dinesh, and S.R. Ghorpade. Journal of Medicinal Chemistry, 2014. 57(12): p. 5419-5434. PMID[24871036].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835631">Synthesis and Biological Evaluation of Substituted 4,6-Diarylpyrimidines and 3,5-Diphenyl-4,5-dihydro-1H-pyrazoles as Anti-tubercular Agents.</a> Pathak, V., H.K. Maurya, S. Sharma, K.K. Srivastava, and A. Gupta. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(13): p. 2892-2896. PMID[24835631].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24836067">Antimycobacterial Activity of Chiral Aminoalcohols with Camphane Scaffold.</a> Petkova, Z., V. Valcheva, G. Momekov, P. Petrov, V. Dimitrov, I. Doytchinova, G. Stavrakov, and M. Stoyanova. European Journal of Medicinal Chemistry, 2014. 81: p. 150-157. PMID[24836067].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24810013">Biological Evaluation of Secondary Metabolites from the Roots of Myrica adenophora.</a> Ting, Y.C., H.H. Ko, H.C. Wang, C.F. Peng, H.S. Chang, P.C. Hsieh, and I.S. Chen. Phytochemistry, 2014. 103: p. 89-98. PMID[24810013].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24777103">In Vitro Activity of AZD5847 against Geographically Diverse Clinical Isolates of Mycobacterium tuberculosis.</a> Werngren, J., M. Wijkander, N. Perskvist, V. Balasubramanian, V.K. Sambandamurthy, C. Rodrigues, and S. Hoffner. Antimicrobial Agents and Chemotherapy, 2014. 58(7): p. 4222-4223. PMID[24777103].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835198">Design, Synthesis and Antimycobacterial Activity Evaluation of Natural Oridonin Derivatives.</a> Xu, S., D. Li, L. Pei, H. Yao, C. Wang, H. Cai, H. Yao, X. Wu, and J. Xu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(13): p. 2811-2814. PMID[24835198].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24727183">Antiparasitic Activities of Novel Ruthenium/Lapachol Complexes.</a> Barbosa, M.I., R.S. Correa, K.M. de Oliveira, C. Rodrigues, J. Ellena, O.R. Nascimento, V.P. Rocha, F.R. Nonato, T.S. Macedo, J.M. Barbosa-Filho, M.B. Soares, and A.A. Batista. Journal of Inorganic Biochemistry, 2014. 136: p. 33-39. PMID[24727183].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24944812">Antiplasmodial Properties of Kaempferol-3-O-rhamnoside Isolated from the Leaves of Schima wallichii against Chloroquine-resistant Plasmodium falciparum.</a> Barliana, M.I., E.W. Suradji, R. Abdulah, A. Diantini, T. Hatabu, J. Nakajima-Shimada, A. Subarnas, and H. Koyama. Biomedical Reports, 2014. 2(4): p. 579-583. PMID[24944812].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24857776">Synthesis and Antimalarial Activity of Metal Complexes of Cross-bridged Tetraazamacrocyclic Ligands.</a> Hubin, T.J., P.N. Amoyaw, K.D. Roewe, N.C. Simpson, R.D. Maples, T.N. Carder Freeman, A.N. Cain, J.G. Le, S.J. Archibald, S.I. Khan, B.L. Tekwani, and M.O. Khan. Bioorganic &amp; Medicinal Chemistry, 2014. 22(13): p. 3239-3244. PMID[24857776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24963617">In Vitro Antiplasmodial Activity of Benzophenones and Xanthones from Edible Fruits of Garcinia Species.</a> Lyles, J.T., A. Negrin, S.I. Khan, K. He, and E.J. Kennelly. Planta Medica, 2014. 80(8-9): p. 676-681. PMID[24963617].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24895172">Deorphaning Pyrrolopyrazines as Potent Multi-target Antimalarial Agents.</a> Reker, D., M. Seet, M. Pillong, C.P. Koch, P. Schneider, M.C. Witschel, M. Rottmann, C. Freymond, R. Brun, B. Schweizer, B. Illarionov, A. Bacher, M. Fischer, F. Diederich, and G. Schneider. Angewandte Chemie International Edition, 2014. 53(27): p. 7079-7084. PMID[24895172].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24865793">Antiprotozoal Activity and DNA Binding of N-Substituted N-Phenylbenzamide and 1,3-Diphenylurea bisguanidines.</a> Rios Martinez, C.H., L. Lagartera, M. Kaiser, and C. Dardonville. European Journal of Medicinal Chemistry, 2014. 81: p. 481-491. PMID[24865793].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24858543">Design, Synthesis and Biological Evaluation of Novel 4-Alkapolyenylpyrrolo[1,2-a]quinoxalines as Antileishmanial Agents - Part III.</a> Ronga, L., M. Del Favero, A. Cohen, C. Soum, P. Le Pape, S. Savrimoutou, N. Pinaud, C. Mullie, S. Daulouede, P. Vincendeau, N. Farvacques, P. Agnamey, F. Pagniez, S. Hutter, N. Azas, P. Sonnet, and J. Guillon. European Journal of Medicinal Chemistry, 2014. 81: p. 378-393. PMID[24858543].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24914508">Design, Synthesis and Evaluation of New Tricyclic Endoperoxides as Potential Antiplasmodial Agents.</a> Ruiz, J., S. Mallet-Ladeira, M. Maynadier, H. Vial, and C. Andre-Barres. Organic &amp; Biomolecular Chemistry, 2014. 12(28): p. 5212-5221. PMID[24914508].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0620-070314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336730200003">A Novel Antifungal Protein with Lysozyme-like Activity from Seeds of Clitoria ternatea.</a> Ajesh, K. and K. Sreejith. Applied Biochemistry and Biotechnology, 2014. 173(3): p. 682-693. ISI[000336730200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331874900008">Antifungal Activity of Phenolic Compounds Identified in Flowers from North Eastern Portugal against Candida Species.</a> Alves, C.T., I. Ferreira, L. Barros, S. Silva, J. Azeredo, and M. Henriques. Future Microbiology, 2014. 9(2): p. 139-146. ISI[000331874900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335759500004">Synthesis and Anti-tuberculosis Activity of the Marine Natural Product Caulerpin and its Analogues.</a> Chay, C.I.C., R.G. Cansino, C.I.E. Pinzon, R.O. Torres-Ochoa, and R. Martinez. Marine Drugs, 2014. 12(4): p. 1757-1772. ISI[000335759500004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336376200011">Three New Phenylpropanoids from the Roots of Piper taiwanense and their Inhibitory Activities on Platelet Aggregation and Mycobacterium tuberculosis.</a> Chen, S., M.J. Cheng, C.C. Wu, C.F. Peng, H.Y. Huang, H.S. Chang, C.J. Wang, and I.S. Chen. Chemistry &amp; Biodiversity, 2014. 11(5): p. 792-799. ISI[000336376200011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336512700033">Antimicrobial Activity of Ammodaucus leucotrichus Fruit Oil from Algerian Sahara.</a> El-Haci, I.A., C. Bekhechi, F. Atik-Bekkara, W. Mazari, M. Gherib, A. Bighelli, J. Casanova, and F. Tomi. Natural Product Communications, 2014. 9(5): p. 711-712. ISI[000336512700033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336729300012">Synthesis and Evaluation of New Fluorinated Anti-tubercular Compounds.</a> Esfahanizadeh, M., K. Omidi, J. Kauffman, A. Gudarzi, S.S. Zahedani, S. Amidi, and F. Kobarfard. Iranian Journal of Pharmaceutical Research, 2014. 13(1): p. 115-126. ISI[000336729300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335942400005">Chemical Composition, Antibacterial, Antifungal and Antioxidant Activities of Algerian Eryngium tricuspidatum L. Essential Oil.</a> Esmaeili, M.A., A. Sonboli, and M.H. Mirjalili. Natural Product Research, 2014. 28(11): p. 848-852. ISI[000335942400005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336119600022">Antibacterial Activity of Northern Ontario Medicinal Plant Extracts.</a> Hassan, H.M., Z.H. Jiang, C. Asmussen, E. McDonald, and W.S. Qin. Canadian Journal of Plant Science, 2014. 94(2): p. 417-424. ISI[000336119600022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336513800033">Synthesis, Characterization and Antimicrobial Efficiency of Some Zirconyl(II) Complexes Involving O, N-donor Environment of Triazole-based Azodyes.</a> Khedr, A.M., M. Gaber, and K.M. Saad-allah.  Chinese Journal of Inorganic Chemistry, 2014. 30(5): p. 1201-1211. ISI[000336513800033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336147100015">Chemical Constituents and Pharmacological Profile of Gunnera manicata L. Extracts.</a> Mariotti, K.D., R.S. Schuh, J.D. Nunes, S.P. Salamoni, G. Meirelles, F. Barreto, G.L. Von Poser, R.B. Singer, E. Dallegrave, S.T. Van Der Sand, and R.P. Limberger. Brazilian Journal of Pharmaceutical Sciences, 2014. 50(1): p. 147-154. ISI[000336147100015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336512700020">Identification and Modulatory Activity Assessment of 2-Hydroxy-3,4,6-trimethoxyacetophenone Isolated from Croton anisodontus Mull. Arg.(Euphorbiaceae).</a> Oliveira, M.T.A., A.M.R. Teixeira, H.D.M. Coutinho, I.R.A. Menezes, D.M. Sena, H.S. Santos, B.M. de Mesquita, M. Albuquerque, P.N. Bandeira, and R. Braz. Natural Product Communications, 2014. 9(5): p. 665-668. ISI[000336512700020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332414900005">Effects of Ambroxol on Candida albicans Growth and Biofilm Formation.</a> Rene, H.D., M.S.J. Jose, S.N.R. Isela, and C.R. Claudio. Mycoses, 2014. 57(4): p. 228-232. ISI[000332414900005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br />  

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336506300017">Novel Non-neuroleptic Phenothiazines Inhibit Mycobacterium tuberculosis Replication.</a> Salie, S., N.J. Hsu, D. Semenya, A. Jardine, and M. Jacobs. Journal of Antimicrobial Chemotherapy, 2014. 69(6): p. 1551-1558. ISI[000336506300017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0620-070314.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
