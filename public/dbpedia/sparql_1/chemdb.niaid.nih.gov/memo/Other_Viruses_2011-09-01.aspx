

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-09-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pq3whu9BBhsfcYhYEHMsddHc0LKZ935flzolBITfQdEZiDKiLdeSk7ZT7g5NCJ7qOymJ3kZjkGgoehFoRuzmuKcQYJ/30krLLGW+sEGxr3bgQ0FapJvRAZp8eGs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AE80D6B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">August 19 - September 1, 2011</h1>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21872478">Synthesis and Biological Evaluation of Matijing-Su Derivatives as Potent anti-HBV Agents</a>. Qiu, J., B. Xu, Z. Huang, W. Pan, P. Cao, C. Liu, X. Hao, B. Song, and G. Liang, Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21872478].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21870867">Inhibition of the Replication of Hepatitis B Virus in Vitro by Pu-erh Tea Extracts.</a> Pei, S., Y. Zhang, H. Xu, X. Chen, and S. Chen, Journal of Agricultural and Food Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21870867].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0819-090111.</p>

    <p> </p> 

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21856153">Dual Pro-drugs of 2&#39;-C-Methyl guanosine monophosphate as Potent and Selective Inhibitors of Hepatitis C Virus</a>. McGuigan, C., K. Madela, M. Aljarah, A. Gilles, S.K. Battina, C.V. Ramamurty, C. Srinivas Rao, J. Vernachio, J. Hutchins, A. Hall, A. Kolykhalov, G. Henson, and S. Chamberlain, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21856153].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0819-090111.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

     <p> </p>

    <p class="NoSpacing">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293503000025">P2-P1 &#39; Macrocyclization of P2 Phenylglycine Based HCV NS3 Protease Inhibitors using Ring-closing Metathesis</a>. Lampa, A., A.E. Ehrenberg, A. Vema, E. Akerblom, G. Lindeberg, U.H. Danielson, A. Karlen, and A. Sandstrom, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 4917-4927; ISI[000293593000025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293304400018">Ceestatin, a Novel Small Molecule Inhibitor of Hepatitis C Virus Replication, Inhibits 3-Hydroxy-3-Methylglutaryl-Coenzyme A Synthase</a>. Peng, L.F., E.A.K. Schaefer, N. Maloof, A. Skaff, A. Berical, C.A. Belon, J.A. Heck, W.Y. Lin, D.N. Frick, T.M. Allen, H.M. Miziorko, S.L. Schreiber, and R.T. Chung, Journal of Infectious Diseases, 2011. 204(4): p. 609-616; ISI[000293304400018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0819-090111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
