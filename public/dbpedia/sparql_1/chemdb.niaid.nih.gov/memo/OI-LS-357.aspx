

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-357.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BybuoWAD50mUXw7HW80F+FyMPOHXyota2+h/C/avpsKp7M9fpoP7EtNQ36WkUbMSn2xTHY0SkFaXGF/CjGa2QuOA3ooUgT2AK1Ir09/+EnPhusOXkCHTMLbXBc8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9A8CD844" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-357-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58990   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Pyrrolidine carboxamide as novel class inhibitor of enoyl acyl carrier protein reductase from Mycobacterium tuberculosis (InhA)</p>

    <p>          He, Xin, Akram, Alian, Stroud, Robert, and Ortiz de Montellano, Paul R</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-327</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     58991   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Phenolics and antimicrobial activity of traditional stoned table olives &#39;alcaparra&#39;</p>

    <p>          Sousa, A, Ferreira, IC, Calhelha, R, Andrade, PB, Valentao, P, Seabra, R, Estevinho, L, Bento, A, and Pereira, JA</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16971127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16971127&amp;dopt=abstract</a> </p><br />

    <p>3.     58992   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          TEM and AFM studies of the outer membrane of mycobacterium tuberculosis</p>

    <p>          Bossmann, Stefan H, Basel, Matthew, Janik, Katharine E, and Niederweis, Michael</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: COLL-078</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58993   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Synthesis and evaluation of antimicrobial and anticonvulsant activities of some new 3-[2-(5-aryl-1,3,4-oxadiazol-2-yl/4-carbethoxymethylthiazol-2-yl)imino-4-thiazolidinon-5-ylidene]-5-substituted/nonsubstituted 1H-indole-2-ones and investigation of their structure-activity relationships</p>

    <p>          Altuntas, Handan, Ates, Oeznur, Uydes-Dogan, BSoenmez, Alp, FIlkay, Kalei, Deniz, Oezdemir, Osman, Birteksoez, Seher, Oetuek, Guelten, Satana, Dilek, and Uzun, Meltem</p>

    <p>          Arzneimittel Forschung <b>2006</b>.  56(3): 239-248</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     58994   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Phenyl-substituted oxazolidinone derivatives and their preparation, pharmaceutical compositions, and use as antimicrobials</p>

    <p>          Das Biswajit,  Ahmed, Shahadat, Yadav, Ajay Singh, Ghosh, Soma, Gujrati, Arti, Sharma, Pankaj, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2006038100</b>  ISSUE DATE: 20060413</p>

    <p>          APPLICATION: 2005  PP: 145 pp.</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     58995   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Activity of Picolinic Acid in Combination with the Antiprotozoal Drug Quinacrine against Mycobacterium avium Complex</p>

    <p>          Shimizu Toshiaki and Tomioka Haruaki</p>

    <p>          Antimicrobial agents and chemotherapy <b>2006</b>.  50(9): 3186-8.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     58996   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Extensively drug-resistant tuberculosis: 2 years of surveillance in Iran</p>

    <p>          Masjedi, MR, Farnia, P, Sorooch, S, Pooramiri, MV, Mansoori, SD, Zarifi, AZ, Akbarvelayati, A, and Hoffner, S</p>

    <p>          Clin Infect Dis <b>2006</b>.  43(7): 841-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941364&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16941364&amp;dopt=abstract</a> </p><br />

    <p>8.     58997   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Molecular Mechanism of a Thumb Domain Hepatitis C Virus Non-nucleoside RNA-Dependent RNA Polymerase Inhibitor</p>

    <p>          Howe, AY, Cheng, H, Thompson, I, Chunduru, SK, Herrmann, S, O&#39;connell, J, Agarwal, A, and Chopra, R</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940072&amp;dopt=abstract</a> </p><br />

    <p>9.     58998   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Direct inhibition of cytochrome c-induced caspase activation in vitro by Toxoplasma gondii reveals novel mechanisms of interference with host cell apoptosis</p>

    <p>          Keller, Philine, Schaumburg, Frieder, Fischer, Silke F, Haecker, Georg, Gross, Uwe, and Lueder, Carsten GK</p>

    <p>          FEMS Microbiology Letters <b>2006</b>.  258(2): 312-319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58999   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Chronic Hepatitis B and C--current treatment and future therapeutic prospects</p>

    <p>          Muller, C</p>

    <p>          Wien Med Wochenschr <b>2006</b>.  156(13-14): 391-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16937041&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16937041&amp;dopt=abstract</a> </p><br />

    <p>11.   59000   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Prevalence of primary drug resistant Mycobacterium tuberculosis in Mashhad, Iran</p>

    <p>          Namaei, MH, Sadeghian, A, Naderinasab, M, and Ziaee, M</p>

    <p>          Indian J Med Res <b>2006</b>.  124(1): 77-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16926460&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16926460&amp;dopt=abstract</a> </p><br />

    <p>12.   59001   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          In vitro antimicrobial activity studies of thioethoxy- and thiophenoxyhalobenzene derivatives</p>

    <p>          Logoglu, Elif, Arslan, Seza, and Oktemer, Atilla</p>

    <p>          Heterocyclic Communications <b>2006</b>.  12(3-4): 219-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   59002   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Protein kinase C zeta plays an essential role for Mycobacterium tuberculosis-induced extracellular signal-regulated kinase 1/2 activation in monocytes/macrophages via Toll-like receptor 2</p>

    <p>          Yang, CS, Lee, JS, Song, CH, Hur, GM, Lee, SJ, Tanaka, S, Akira, S, Paik, TH, and Jo, EK</p>

    <p>          Cell Microbiol  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16925784&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16925784&amp;dopt=abstract</a> </p><br />

    <p>14.   59003   OI-LS-357; PUBMED-OI-9/18/2006</p>

    <p class="memofmt1-2">          Phosphatidylinositol Mannoside from Mycobacterium tuberculosis Binds {alpha}5beta1 Integrin (VLA-5) on CD4+ T Cells and Induces Adhesion to Fibronectin</p>

    <p>          Rojas, RE, Thomas, JJ, Gehring, AJ, Hill, PJ, Belisle, JT, Harding, CV, and Boom, WH</p>

    <p>          J Immunol <b>2006</b>.  177(5): 2959-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920931&amp;dopt=abstract</a> </p><br />

    <p>15.   59004   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Antifungal susceptibility of epigallocatechin 3-O-gallate on clinical isolates of pathogenic yeasts</p>

    <p>          Park, Bong Joo, Park, Jong-Chul, Taguchi, Hideaki, Fukushima, Kazutaka, Hyon, Suong-Hyu, and Takatori, Kosuke</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  347(2): 401-405</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59005   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Atropurosides A-G, new steroidal saponins from Smilacina atropurpurea</p>

    <p>          Zhang, Ying, Li, Hai-Zhou, Zhang, Ying-Jun, Jacob, Melissa R, Khan, Shabana I, Li, Xing-Cong, and Yang, Chong-Ren</p>

    <p>          Steroids <b>2006</b>.  71(8): 712-719</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59006   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Phytochemistry and Antifungal Properties of the Newly Discovered Tree Pleodendron costaricense</p>

    <p>          Amiguet, Virginie Treyvaud, Petit, Philippe, Ta, Chieu Anh, Nunez, Ronaldo, Sanchez-Vindas, Pablo, Alvarez, Luis Poveda, Smith, Myron L, Arnason, John Thor, and Durst, Tony</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(7): 1005-1009</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59007   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Antimicrobial and antileishmanial activities of some new 2-arylamino-1,3,4-thiadiazolo substituted imidazol-5-ones</p>

    <p>          Sah, Pramilla and Sinha, Rinku</p>

    <p>          International Journal of Chemical Sciences <b>2006</b>.  4(1): 31-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   59008   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Determination of antifungal activities in serum samples from mice treated with different antifungal drugs allows detection of an active metabolite of itraconazole</p>

    <p>          Maki, Katsuyuki, Watabe, Etsuko, Iguchi, Yumi, Nakamura, Hideko, Tomishima, Masaki, Ohki, Hidenori, Yamada, Akira, Matsumoto, Satoru, Ikeda, Fumiaki, Tawara, Shuichi, and Mutoh, Seitaro</p>

    <p>          Microbiology and Immunology <b>2006</b>.  50(4): 281-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   59009   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          A novel virus culture system for hepatitis C virus</p>

    <p>          Kato, Takanobu, Date, Tomoko, Miyamoto, Michiko, and Wakita, Takaji</p>

    <p>          Future Virology <b>2006</b>.  1(4): 519-525</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59010   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Hepatitis C virus particles and method of proliferating the same</p>

    <p>          Miyamura, Tatsuo, Murakami, Kyoko, Suzuki, Tetsuro, Yoshioka, Hiroshi, Mori, Yuichi, and Ohtsubo, Shin-Ya</p>

    <p>          PATENT:  WO <b>2006090933</b>  ISSUE DATE:  20060831</p>

    <p>          APPLICATION: 2006  PP: 77pp.</p>

    <p>          ASSIGNEE:  (Japan as Represented by Director-General of National Institute of Infectious Diseases, Japan and Mebiol Inc.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   59011   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Discovery of VP19744: A pyrano[3,4-b]indole-based inhibitor of HCV NS5B polymerase demonstrating in vivo antiviral activity</p>

    <p>          LaPorte, Matthew G, Jackson, Randy W, Burns, Christopher J, Draper, Tandy L, Gaboury, Janet A, Galie, Kristin, Herbertz, Torsten, Hussey, Alison R, Rippin, Susan R, Benetatos, Christopher A, Chunduru, Srinivas K, Young, Dorothy C, Christiansen, Joel S, Coburn, Glen A, Rizzo, Christopher J, Collett, Marc S, Pevear, Daniel C, and Condon, Stephen M</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-240</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   59012   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Preparation of indolecarboxylic acid derivatives as inhibitors of HCV replication</p>

    <p>          Hudyma, Thomas W, Zheng, Xiaofan, He, Feng, Ding, Min, Bergstrom, Carl P, Hewawasam, Piyasena, Martin, Scott W, and Gentles, Robert G</p>

    <p>          PATENT:  US <b>2006166964</b>  ISSUE DATE:  20060727</p>

    <p>          APPLICATION: 2006-20085  PP: 318 pp., Cont.-in-part of U.S. Ser. No. 181,639.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   59013   OI-LS-357; WOS-OI-9/10/2006</p>

    <p class="memofmt1-2">          A new rapid and simple colorimetric method to detect pyrazinamide resistance in Mycobacterium tuberculosis using nicotinamide</p>

    <p>          Martin, A, Takiff, H, Vandamme, P, Swings, J, Palomino, JC, and Portaels, F</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2006</b>.  58(2): 327-331, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239840700013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239840700013</a> </p><br />

    <p>25.   59014   OI-LS-357; WOS-OI-9/10/2006</p>

    <p class="memofmt1-2">          Disappointing results of combination therapy for HCV?</p>

    <p>          Dudley, T, O&#39;Donnell, K, Haydon, G, and Mutimer, D</p>

    <p>          GUT <b>2006</b>.  55 (9): 1362-1363, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239723000029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239723000029</a> </p><br />
    <br clear="all">

    <p>26.   59015   OI-LS-357; SCIFINDER-OI-9/11/06</p>

    <p class="memofmt1-2">          Antiviral intervention, resistance, and perspectives</p>

    <p>          Michel, Detlef and Mertens, Thomas</p>

    <p>          Cytomegaloviruses <b>2006</b>.  2006: 573-590</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59016   OI-LS-357; WOS-OI-9/10/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis transporter MmpL7 is a potential substrate for kinase PknD</p>

    <p>          Perez, J, Garcia, R, Bach, H, de, Waard JH, Jacobs, WR, Av-Gay, Y, Bubis, J, and Takiff, HE</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2006</b>.  348(1): 6-12, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239796100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239796100002</a> </p><br />

    <p>28.   59017   OI-LS-357; WOS-OI-9/10/2006</p>

    <p class="memofmt1-2">          Phosphate closes the solution structure of the 5-enolpyruvylshikimate-3-phosphate synthase (EPSPS) from Mycobacterium tuberculosis</p>

    <p>          Borges, JC, Pereira, JH, Vasconcelos, IB, dos Santos, GC, Olivieri, JR, Ramos, CHI, Palma, MS, Basso, LA, Santos, DS, and de Azevedo, WF</p>

    <p>          ARCHIVES OF BIOCHEMISTRY AND BIOPHYSICS <b>2006</b>.  452(2): 156-164, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239911400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239911400008</a> </p><br />

    <p>29.   59018   OI-LS-357; WOS-OI-9/17/2006</p>

    <p><b>          Differentiation of tuberculosis strains in a population with mainly Beijing-family strains</b> </p>

    <p>          Nikolayevskyy, V, Gopaul, K, Balabanova, Y, Brown, T, Fedorin, I, and Drobniewski, F</p>

    <p>          EMERGING INFECTIOUS DISEASES <b>2006</b>.  12(9): 1406-1413, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240081400014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240081400014</a> </p><br />

    <p>30.   59019   OI-LS-357; WOS-OI-9/17/2006</p>

    <p class="memofmt1-2">          Hepatitis C virus molecular clones: from cDNA to infectious virus particles in cell culture</p>

    <p>          Bartenschlager, R</p>

    <p>          CURRENT OPINION IN MICROBIOLOGY <b>2006</b>.  9(4): 416-422, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240056400016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240056400016</a> </p><br />

    <p>31.   59020   OI-LS-357; WOS-OI-9/17/2006</p>

    <p class="memofmt1-2">          Finger loop inhibitors of the HCVNS5B polymerase: Discovery and prospects for new HCV therapy</p>

    <p>          Beaulieu, PL</p>

    <p>          CURRENT OPINION IN DRUG DISCOVERY &amp; DEVELOPMENT <b>2006</b>.  9(5 ): 618-626, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240047900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240047900009</a> </p><br />

    <p>32.   59021   OI-LS-357; WOS-OI-9/17/2006</p>

    <p class="memofmt1-2">          Fishing for new antimicrobials</p>

    <p>          Mukhopadhyay, A and Peterson, RT</p>

    <p>          CURRENT OPINION IN CHEMICAL BIOLOGY <b>2006</b>.  10(4): 327-333, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240056600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240056600007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
