

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-12-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yfoJWe8T9zPILySgHopJdwe3kyZDtehC8JlgKj3s3uP8v0YEsyea4RC9j9gwqfPhw4ziet7yW/yUG+Op7hLpgX3ebrHL0EXb7ZLiXkwcJMx9EfjbCnyEfK0OImg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C06ABD6F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: November 23 - December 6, 2012</h1>

    <h2>FIV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23201205">Prostratin Exhibits Both Replication Enhancing and Inhibiting Effects on FIV Infection of Feline CD4(+) T-cells</a><i>.</i> Chan, C.N., E.L. McMonagle, M.J. Hosie, and B.J. Willett. Virus Research, 2012. <b>[Epub ahead of print]</b>; PMID[23201205].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23069315">Randomized Trial of Lamivudine, Adefovir, and the Combination in HBeAg-Positive Chronic Hepatitis B</a><i>.</i> He, Z., J. Wang, K. Liu, H. Huang, Y. Du, Z. Lin, M. Cai, and X. Feng. Clinics and Research in Hepatology and Gastroenterology, 2012. 36(6): p. 592-597; PMID[23069315].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23098744">Anti-Hepatitis B Virus Activities of &#945;-DDB-FNC, a Novel Nucleoside-biphenyldicarboxylate Compound in Cells and Ducks, and Its Anti-Immunological Liver Injury Effect in Mice</a><i>.</i> Yang, Q., X. Zhao, L. Zang, X. Fang, J. Zhao, X. Yang, Q. Wang, L. Zheng, and J. Chang. Antiviral Research, 2012. 96(3): p. 333-339; PMID[23098744].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23092111">Hyaluronic acid-gold Nanoparticle/Interferon &#945; Complex for Targeted Treatment of Hepatitis C Virus Infection</a><i>.</i> Lee, M.Y., J.A. Yang, H.S. Jung, S. Beack, J.E. Choi, W. Hur, H. Koo, K. Kim, S.K. Yoon, and S.K. Hahn. ACS Nano, 2012. 6(11): p. 9522-9531; PMID[23092111].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23085031">Synthesis, Evaluation of anti-HIV-1 and anti-HCV Activity of Novel 2&#39;,3&#39;-Dideoxy-2&#39;,2&#39;-difluoro-4&#39;-azanucleosides</a><i>.</i> Martinez-Montero, S., S. Fernandez, Y.S. Sanghvi, E.A. Theodorakis, M.A. Detorio, T.R. McBrayer, T. Whitaker, R.F. Schinazi, V. Gotor, and M. Ferrero. Bioorganic &amp; Medicinal Chemistry, 2012. 20(23): p. 6885-93; PMID[23085031].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23021993">Development of Potent Macrocyclic Inhibitors of Genotype 3A HCV NS3/4A Protease</a><i>.</i> Rudd, M.T., J.A. McCauley, J.J. Romano, J.W. Butcher, K. Bush, C.J. McIntyre, K.T. Nguyen, K.F. Gilbert, T.A. Lyle, M.K. Holloway, B.L. Wan, J.P. Vacca, V. Summa, S. Harper, M. Rowley, S.S. Carroll, C. Burlein, J.M. Dimuzio, A. Gates, D.J. Graham, Q. Huang, S.W. Ludmerer, S. McClain, C. McHale, M. Stahlhut, C. Fandozzi, A. Taylor, N. Trainor, D.B. Olsen, and N.J. Liverton. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7201-7206; PMID[23021993].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23084906">Development of Macrocyclic Inhibitors of HCV NS3/4A Protease with Cyclic Constrained P2-P4 Linkers</a><i>.</i> Rudd, M.T., C.J. McIntyre, J.J. Romano, J.W. Butcher, M.K. Holloway, K. Bush, K.T. Nguyen, K.F. Gilbert, T.A. Lyle, N.J. Liverton, B.L. Wan, V. Summa, S. Harper, M. Rowley, J.P. Vacca, S.S. Carroll, C. Burlein, J.M. Dimuzio, A. Gates, D.J. Graham, Q. Huang, S.W. Ludmerer, S. McClain, C. McHale, M. Stahlhut, C. Fandozzi, A. Taylor, N. Trainor, D.B. Olsen, and J.A. McCauley. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(23): p. 7207-7213; PMID[23084906].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23192857">Synthetic Lipophilic Antioxidant BO-653 Suppresses HCV Replication</a><i>.</i> Yasui, F., M. Sudoh, M. Arai, and M. Kohara. Journal of Medical Virology, 2012. <b>[Epub ahead of print]</b>; PMID[23192857]. <b>[PubMed]</b> OV_1123-120612.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22948883">Identification of Hepatitis C Virus Inhibitors Targeting Different Aspects of Infection Using a Cell-based Assay</a><i>.</i> Yu, X., B. Sainz, Jr., P.A. Petukhov, and S.L. Uprichard. Antimicrobial Agents and Chemotherapy, 2012. 56(12): p. 6109-6120; PMID[22948883].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1123-120612.</p>
        
    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310095700008">Divergent Antiviral Effects of Bioflavonoids on the Hepatitis C Virus Life Cycle</a><i>.</i> Khachatoorian, R., V. Arumugaswami, S. Raychaudhuri, G.K. Yeh, E.M. Maloney, J. Wang, A. Dasgupta, and S.W. French. Virology, 2012. 433(2): p. 346-355; ISI[000310095700008].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308718200003">Synthesis of 3&#39;,5&#39;-cyclic Phosphate and Thiophosphate esters of 2&#39;-C-Methyl ribonucleosides</a><i>.</i> Leisvuori, A., Z. Ahmed, M. Ora, L. Beigelman, L. Blatt, and H. Lonnberg. Helvetica Chimica Acta, 2012. 95(9): p. 1512-1520; ISI[000308718200003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310532100011">Sesquiterpene Lactones with anti-Hepatitis C Virus Activity Using Molecular Descriptors</a><i>.</i> Rossini, M., M.T. Scotti, M.V. Correia, H.H. Fokoue, L. Scotti, F.J.B. Mendonca, M.S. da Silva, and V.D. Emerenciano. Letters in Drug Design &amp; Discovery, 2012. 9(9): p. 881-890; ISI[000310532100011].  <b>[WOS]</b> OV_1123-120612.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309748303378">PEG-IFNA/Ribavirin/Protease Inhibitor Combination Is Highly Effective in HCV-mixed Cryoglobulinemia vasculitis</a><i>.</i> Saadoun, D., S. Pol, P. Lebray, F. Blanc, G. Pialoux, A. Karras, D. Bazin, E. Plaisier, and P. Cacoub. Arthritis and Rheumatism, 2012. 64(10): p. S708-S708; ISI[000309748303378].  <b>[WOS]</b> OV_1123-120612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
