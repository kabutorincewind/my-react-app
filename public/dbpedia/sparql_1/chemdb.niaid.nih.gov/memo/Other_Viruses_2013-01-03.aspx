

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-01-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="q3EZd8dM6KUmRIuidmnMtvuwrqdQ/K958ctDxKlevYs2h5M2U9Tb6ZTntY/UJNi3hAafYExzH7Stciw/5C1XHNjcOC6GYRwIqpWyqhFOTyYj/CE4nX0djdPzFHI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B692C221" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: December 21 - January 3, 2013</h1>

    <h2>HCMV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23176752">C-3 Halo and 3-Methyl Substituted 5&#39;-nor-3-deazaaristeromycins: Synthesis and Antiviral Properties</a><i>.</i> Liu, C., Q. Chen, M. Yang, and S.W. Schneller. Bioorganic &amp; Medicinal Chemistry, 2013. 21(1): p. 359-64; PMID[23176752]<b>.</b></p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23094860">The Anti-viral Effect of Sorafenib in Hepatitis C-related Hepatocellular Carcinoma</a><i>.</i> Cabrera, R., A.R. Limaye, P. Horne, R. Mills, C. Soldevila-Pico, V. Clark, G. Morelli, R. Firpi, and D.R. Nelson. Alimentary Pharmacology &amp; Therapeutics, 2013. 37(1): p. 91-7; PMID[23094860].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22855387">Thumb Inhibitor Binding Eliminates Functionally Important Dynamics in the Hepatitis C Virus RNA Polymerase</a><i>.</i> Davis, B.C. and I.F. Thorpe. Proteins, 2013. 81(1): p. 40-52; PMID[22855387].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23064222">Boceprevir with Peginterferon alfa-2A-Ribavirin Is Effective for Previously Treated Chronic Hepatitis C Genotype 1 Infection</a><i>.</i> Flamm, S.L., E. Lawitz, I. Jacobson, M. Bourliere, C. Hezode, J.M. Vierling, B.R. Bacon, C. Niederau, M. Sherman, V. Goteti, H.L. Sings, R.O. Barnard, J.A. Howe, L.D. Pedicone, M.H. Burroughs, C.A. Brass, J.K. Albrecht, and F. Poordad. Clinical Gastroenterology and Hepatology, 2013. 11(1): p. 81-87 e4; PMID[23064222].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23274666">Combinations of Interferon Lambda with Direct-acting Antiviral Agents Are Highly Efficient in Suppressing Hepatitis C Virus Replication</a><i>.</i> Friborg, J., S. Levine, C. Chen, A.K. Sheaffer, S. Chaniewski, and F. McPhee. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[23274666].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23274664">Combination Treatment with HCV Protease and NS5A Inhibitors Is Effective against Recombinant Genotype 1A, 2A and 3A Virus</a><i>.</i> Gottwein, J.M., S.B. Jensen, Y.P. Li, L. Ghanem, T.K. Scheel, S.B. Serre, L. Mikkelsen, and J. Bukh. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[23274664].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22910295">Inhibition of the Interaction between NS3 Protease and HCV IRES with a Small Peptide: A Novel Therapeutic Strategy</a><i>.</i> Ray, U., C.L. Roy, A. Kumar, P. Mani, A.P. Joseph, G. Sudha, D.P. Sarkar, N. Srinivasan, and S. Das. Molecular Therapy : the Journal of the American Society of Gene Therapy, 2013. 21(1): p. 57-67; PMID[22910295].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23261844">Synergistic Activity of Amenamevir (ASP2151) with Nucleoside Analogs against Herpes Simplex Virus Types 1 and 2 and Varicella-Zoster Virus</a><i>.</i> Chono, K., K. Katsumata, H. Suzuki, and K. Shiraki. Antiviral Research, 2012. <b>[Epub ahead of print]</b>; PMID[23261844].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1221-010312.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311469500012">Neutralization Activity and Kinetics of Two Broad-Range Human Monoclonal IGG1 Derived from Recombinant FAB Fragments and Directed against Hepatitis C Virus E2 Glycoprotein</a><i>.</i> Diotti, R.A., G.A. Sautto, L. Solforosi, N. Mancini, M. Clementi, and R. Burioni. New Microbiologica, 2012. 35(4): p. 475-479; ISI[000311469500012].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310711301807">A Cell Permeable Hairpin Peptide Inhibits Hepatitis C Viral NS5A-Mediated Translation and Virus Production</a><i>.</i> Khachatoorian, R., V. Arumugaswami, P. Ruchala, E.M. Maloney, S. Raychaudhuri, E. Miao, A. Dasgupta, and S.W. French. Faseb Journal, 2012. 26; ISI[000310711301807].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311027200004">Strong Antiviral Activity of the New L-Hydroxycytidine Derivative, L-HYD4FC, in HBV-Infected Human Chimeric UPA/SCID Mice</a><i>.</i> Volz, T., M. Lutgehetmann, L. Allweiss, M. Warlich, J. Bierwolf, J.M. Pollok, J. Petersen, E. Matthes, and M. Dandri. Antiviral Therapy, 2012. 17(4): p. 623-631; ISI[000311027200004].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1221-010312.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311027200009">Antiviral Activity of FNC, 2 &#39;-Deoxy-2 &#39;-&#946;-fluoro-4 &#39;-azidocytidine, against Human and Duck HBV Replication</a><i>.</i> Zheng, L.Y., Q. Wang, X.R. Yang, X.H. Guo, L. Chen, L. Tao, L.H. Dong, Y.J. Li, H.Y. An, X.J. Yu, Q.D. Wang, and J.B. Chang. Antiviral Therapy, 2012. 17(4): p. 679-687; ISI[000311027200009].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1221-010312.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
