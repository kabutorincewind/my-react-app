

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-08-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KPnyQDJsZSX06DvF3VJnRV+DCHnHcT9pDW/lgKmAmmZ7YRWNWWnbmYCGBF0mlnT3k3bABsCVIjkVhVvICS4t/k6Ft4vHIek9rz4HYLz0j8BeXjF5HH4k471aubs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5045C787" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 17 - August 30, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22922731">Curcumin Inhibits HCV Replication by Induction of Heme Oxygenase-1 and Suppression of AKT.</a> Chen, M.H., M.Y. Lee, J.J. Chuang, Y.Z. Li, S.T. Ning, J.C. Chen, and Y.W. Liu. International Journal of Molecular Medicine, 2012. <b>[Epub ahead of print];</b> PMID[22922731].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0817-083012.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">2<a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22908173">. 2-[4,5-Difluoro-2-(2-fluorobenzoylamino)-benzoylamino]benzoic acid, an Antiviral Compound with Activity against Acyclovir-resistant Isolates of Herpes Simplex Virus Type 1 and 2.</a> Strand, M., M.K. Islam, K. Edlund, C.T. Oberg, A. Allard, T. Bergstrom, Y.F. Mei, M. Elofsson, and G. Wadell. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print];</b> PMID[22908173].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0817-083012.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22922673">Polyethyleneimine is a Potent Mucosal Adjuvant for Viral Glycoprotein Antigens</a><i>.</i> Wegmann, F., K.H. Gartlan, A.M. Harandi, S.A. Brinckmann, M. Coccia, W.R. Hillson, W.L. Kok, S. Cole, L.P. Ho, T. Lambe, M. Puthia, C. Svanborg, E.M. Scherer, G. Krashias, A. Williams, J.N. Blattman, P.D. Greenberg, R.A. Flavell, A.E. Moghaddam, N.C. Sheppard, and Q.J. Sattentau. National Biotechnology, 2012. <b>[Epub ahead of print];</b> PMID[22922673].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0817-083012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">4. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=3B5fbbANJK@5IOJJ3bn&amp;page=1&amp;doc=1">Quercetin: Bioflavonoids as Part of Interferon-free Hepatitis C Therapy?</a> Lu, N., R. Khachatoorian, and S.W. French. Expert Review of Anti-Infective Therapy, 2012. 10(6): p. 619-621; ISI[000306544300002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=3&amp;SID=3B5fbbANJK@5IOJJ3bn&amp;page=1&amp;doc=1">Transporter-targeted Lipid Prodrugs of Cyclic Cidofovir: A Potential Approach for the Treatment of Cytomegalovirus Retinitis.</a> Gokulgandhi, M.R., M. Barot, M. Bagui, D. Pal, and A.K. Mitra. Journal of Pharmaceutical Sciences, 2012. 101(9): p. 3249-3263; ISI[000306734900024].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=2&amp;SID=3B5fbbANJK@5IOJJ3bn&amp;page=1&amp;doc=1">C-6 Aryl substituted 4-quinolone-3-carboxylic acids as Inhibitors of Hepatitis C Virus.</a> Chen, Y.L., J. Zacharias, R. Vince, R.J. Geraghty, and Z.Q. Wang. Bioorganic &amp; Medicinal Chemistry, 2012. 20(15): p. 4790-4800; ISI[000306480200022].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0817-083012.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=3B5fbbANJK@5IOJJ3bn&amp;page=1&amp;doc=1">Pyridofuran Substituted Pyrimidine Derivatives as HCV Replication (Replicase) Inhibitors</a><i>.</i> Bennett, F., H.S. Kezar, V. Girijavallabhan, Y.H. Huang, R. Huelgas, R. Rossman, N.Y. Shih, J.J. Piwinski, M. MacCoss, C.D. Kwong, J.L. Clark, A.T. Fowler, F. Geng, A. Roychowdhury, R.C. Reynolds, J.A. Maddry, S. Ananthan, J.A. Secrist, C. Li, R. Chase, S. Curry, H.C. Huang, X. Tong, F.G. Njoroge, and A. Arasappan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(15): p. 5144-5149; ISI[000306481100046].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0817-083012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
