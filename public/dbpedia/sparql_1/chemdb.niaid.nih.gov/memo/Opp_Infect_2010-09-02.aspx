

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-09-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fAXA49aRgEY/NhBjTSe1p02JsrmGZMVNbSCYR0RmjHdFfd8P3gO/4dt4fWfgA6ZyMe5+p4aFQ0iVt25pVz4qW53V8lQzJtJJGDCIm7m0YPY5bv8x5NFRts/jAUY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BC60E27B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">August 20 - September 2, 2010</p>

    <p class="memofmt2-1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20675138">Synthesis and evaluation of oryzalin analogs against Toxoplasma gondii.</a> Endeshaw, M.M., C. Li, J. de Leon, N. Yao, K. Latibeaudiere, K. Premalatha, N. Morrissette, and K.A. Werbovetz. Bioorg Med Chem Lett. 20(17): p. 5179-83; PMID[20675138].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20541294">Synthesis and evaluation of anti-Toxoplasma gondii and antimicrobial activities of thiosemicarbazides, 4-thiazolidinones and 1,3,4-thiadiazoles.</a> Liesen, A.P., T.M. de Aquino, C.S. Carvalho, V.T. Lima, J.M. de Araujo, J.G. de Lima, A.R. de Faria, E.J. de Melo, A.J. Alves, E.W. Alves, A.Q. Alves, and A.J. Goes. Eur J Med Chem. 45(9): p. 3685-91; PMID[20541294].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20591538">A study of cytotoxicity of novel chlorokojic acid derivatives with their antimicrobial and antiviral activities.</a> Aytemir, M.D. and B. Ozcelik. Eur J Med Chem. 45(9): p. 4089-95; PMID[20591538].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20598399">Synthesis and biological activities of novel amine-derived bis-azoles as potential antibacterial and antifungal agents.</a> Fang, B., C.H. Zhou, and X.C. Rao. Eur J Med Chem. 45(9): p. 4388-98; PMID[20598399].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20681969">The synergy of honokiol and fluconazole against clinical isolates of azole-resistant Candida albicans.</a> Jin, J., N. Guo, J. Zhang, Y. Ding, X. Tang, J. Liang, L. Li, X. Deng, and L. Yu. Lett Appl Microbiol. 51(3): p. 351-7; PMID[20681969].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20732832">Bactericidal activity of the diarylquinoline TMC207 against Mycobacterium tuberculosis outside and within cells.</a> Dhillon, J., K. Andries, P.P. Phillips, and D.A. Mitchison. Tuberculosis (Edinb). <b>[Epub ahead of print]</b>; PMID[20732832].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20667727">Synthesis and antituberculosis activity of new fatty acid amides.</a> D&#39;Oca Cda, R., T. Coelho, T.G. Marinho, C.R. Hack, C. Duarte Rda, P.A. da Silva, and M.G. D&#39;Oca. Bioorg Med Chem Lett. 20(17): p. 5255-7; PMID[20667727].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20739223">The Abyssomicin C family as in vitro inhibitors of Mycobacterium tuberculosis.</a> Freundlich, J.S., M. Lalgondar, J.R. Wei, S. Swanson, E.J. Sorensen, E.J. Rubin, and J.C. Sacchettini. Tuberculosis (Edinb). <b>[Epub ahead of print]</b>; PMID[20739223].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20576327">Design, synthesis, and antitubercular evaluation of novel series of 3-benzofuran-5-aryl-1-pyrazolyl-pyridylmethanone and 3-benzofuran-5-aryl-1-pyrazolylcarbonyl-4-oxo-naphthyridin analogs.</a> Manna, K. and Y.K. Agrawal. Eur J Med Chem. 45(9): p. 3831-9; PMID[20576327].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20718497">Inhibition of mycobacterial replication by pyrimidines possessing various C-5 functionalities and related 2&#39;-deoxynucleoside analogues using in vitro and in vivo models.</a> Srivastav, N.C., D. Rai, C. Tse, B. Agrawal, D.Y. Kunimoto, and R. Kumar. J Med Chem. 53(16): p. 6180-7; PMID[20718497].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p class="plaintext"> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20795743">Eucapsitrione, an Anti-Mycobacterium tuberculosis Anthraquinone Derivative from the Cultured Freshwater Cyanobacterium Eucapsis sp.</a> Sturdy, M., A. Krunic, S. Cho, S. Franzblau, and J. Orjala. J Nat Prod. 73(8): p. 1441-3; PMID[20795743].</p>

    <p><b>[Pubmed]</b>. OI_0820-090210.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p>12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280836000001">Synthesis of novel naphthyl substituted fused indazolonols as potent anticandidal agents.</a> Kanagarajan, V., J. Thanusu, and M. Gopalakrishnan. European Review for Medical and Pharmacological Sciences. 14(8): p. 653-660; ISI[000280836000001].</p>

    <p><b>[WOS]</b>. OI_0820-090210.</p>

    <p> </p>

    <p>13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280550500057">Potency of Anidulafungin Compared to Nine Other Antifungal Agents Tested against Candida spp., Cryptococcus spp., and Aspergillus spp.: Results from the Global SENTRY Antimicrobial Surveillance Program (2008).</a> Messer, S.A., R.N. Jones, G.J. Moet, J.T. Kirby, and M. Castanheira. Journal of Clinical Microbiology. 48(8): p. 2984-2987; ISI[000280550500057].</p>

    <p><b>[WOS]</b>. OI_0820-090210.</p>

    <p> </p>

    <p>14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280659300011">3-Methyladenine blocks Toxoplasma gondii division prior to centrosome replication.</a> Wang, Y.B., A. Karnataki, M. Parsons, L.M. Weiss, and A. Orlofsky. Molecular and Biochemical Parasitology. 173(2): p. 142-153; ISI[000280659300011].</p>

    <p><b>[WOS]</b>. OI_0820-090210.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
