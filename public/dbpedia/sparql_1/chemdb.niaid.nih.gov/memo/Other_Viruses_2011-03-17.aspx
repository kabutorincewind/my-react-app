

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-03-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3i84qQYpr/tD37NHS/wviKXq+wRlpHNFAmaIYRuJTKvKS148AsCrX93BFR5wdxw+F1FIl/uHINA/4Q4qYlRrVvFX/rU6NapUP/T5h/rfAer2mAnSnaqO+LsVL/A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E331991D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">March 4 - March 17, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Kaposi Sarcoma</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402841">The Hiv Protease Inhibitor Nelfinavir Inhibits Kaposi Sarcoma-Associated Herpesvirus Replication in Vitro</a>. Gantt, S., J. Carlsson, M. Ikoma, E. Gachelet, M. Gray, A.P. Geballe, L. Corey, C. Casper, M. Lagunoff, and J. Vieira. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21402841].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21389629">Antiviral and Immunostimulating Effects of Lignin-Carbohydrate-Protein Complexes from Pimpinella Anisum</a>. Lee, J.B., C. Yamagishi, K. Hayashi, and T. Hayashi. Bioscience, Biotechnology and Biochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21389629].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21376429">Broad-Spectrum Antiviral Activity Including Human Immunodeficiency and Hepatitis C Viruses Mediated by a Novel Retinoid Thiosemicarbazone Derivative</a>. Kesel, A.J. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21376429].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Epstein-Barr virus</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21396962">A Reporter System for Epstein-Barr Virus (Ebv) Lytic Replication: Anti-Ebv Activity of the Broad Anti-Herpesviral Drug Artesunate</a>. Auerochs, S. and K.K. Marschall. Journal of Virological Methods, 2011. <b>[Epub ahead of print]</b>; PMID[21396962].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21396440">Cytotoxicity in L929 Fibroblasts and Inhibition of Herpes Simplex Virus Type 1 Kupka by Estuarine Cyanobacteria Extracts</a>. Lopes, V.R., M. Schmidtke, M. Helena Fernandes, R. Martins, and V. Vasconcelos. Toxicology in vitro : an international journal published in association with BIBRA, 2011. <b>[Epub ahead of print]</b>; PMID[21396440].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21391841">Cytotoxicity, Antiviral and Antimicrobial Activities of Alkaloids, Flavonoids, and Phenolic Acids</a>. Ozcelik, B., M. Kartal, and I. Orhan. Pharmaceutical Biology, 2011. <b>[Epub ahead of print]</b>; PMID[21391841].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">7. [<a href="http://www.ncbi.nlm.nih.gov/pubmed/21381336">Antiviral Properties of the Derivatives of Netropsin and Distamycin against Herpes Simplex Viruses Type 1 and Variolovaccine</a>]. Andronova, V.L., S.L. Grokhovsky, G.A. Galegov, P.G. Deryabin, G.V. Gursky, and D.K. Lvov. Voprosy Virusologii, 2010. 55(6): p. 24-27; PMID[21381336].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21384414">Synthesis and in-Vitro Anti-Hepatitis B Virus Activity of Ethyl 6-Bromo-8-Hydroxyimidazo[1,2-a]Pyridine-3-Carboxylates</a>. Chen, D., Y. Liu, S. Zhang, D. Guo, C. Liu, S. Li, and P. Gong. Archiv Der Pharmazie, 2011. 344(3): p. 158-164; PMID[21384414].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21391660">Diosgenin, a Plant-Derived Sapogenin, Exhibits Antiviral Activity in Vitro against Hepatitis C Virus</a>. Wang, Y.J., K.L. Pan, T.C. Hsieh, T.Y. Chang, W.H. Lin, and J.T. Hsu. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21391660].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21375337">Coumarin-Purine Ribofuranoside Conjugates as New Agents against Hepatitis C Virus</a>. Hwu, J.R., S.Y. Lin, S.C. Tsay, E. De Clercq, P. Leyssen, and J. Neyts. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21375337].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286906100045">Synthesis and Anti-Hiv Activity of New 3 &#39;-O-Phosphonomethyl Nucleosides</a>. Cesnek, M. and P. Herdewijn. Heterocycles, 2010. 82(1): p. 663-687; ISI[000286906100045].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287163700016">2 &#39;-Deoxy-2 &#39;-Alpha-C-(Hydroxymethyl)Adenosine as Potential Anti-Hcv Agent</a>. Chavain, N. and P. Herdewijn. European Journal of Organic Chemistry, 2011. 2011(6): p. 1140-1147; ISI[000287163700016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286422500003">Alkylated Porphyrins Have Broad Antiviral Activity against Hepadnaviruses, Flaviviruses, Filoviruses, and Arenaviruses</a>. Guo, H.T., X.B. Pan, R.C. Mao, X.C. Zhang, L.J. Wang, X.Y. Lu, J.H. Chang, J.T. Guo, S. Passic, F.C. Krebs, B. Wigdahl, T.K. Warren, C.J. Retterer, S. Bavari, X.D. Xu, A. Cuconati, and T.M. Block. Antimicrobial Agents and Chemotherapy, 2011. 55(2): p. 478-486; ISI[000286422500003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286899700002">In-Vitro Antiviral Activity of Solanum Nigrum against Hepatitis C Virus</a>. Javed, T., U.A. Ashfaq, S. Riaz, S. Rehman, and S. Riazuddin. Virology Journal, 2011. 8; ISI[000286899700002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287065100013">Pentagalloylglucose Downregulates Cofilin1 and Inhibits Hsv-1 Infection</a>. Pei, Y., Y.F. Xiang, J.N. Chen, C.H. Lu, J. Hao, Q.A. Du, C.C. Lai, C. Qu, S. Li, H.Q. Ju, Z. Ren, Q.Y. Liu, S. Xiong, C.W. Qian, F.L. Zeng, P.Z. Zhang, C.R. Yang, Y.J. Zhang, J. Xu, K. Kitazato, and Y.F. Wang. Antiviral Research, 2011. 89(1): p. 98-108; ISI[000287065100013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287419000017">Synthesis and Sar Studies on Azetidine-Containing Dipeptides as Hcmv Inhibitors</a>. Perez-Faginas, P., M.T. Aranda, M.T. Garcia-Lopez, R. Snoeck, G. Andrei, J. Balzarini, and R. Gonzalez-Muniz. Bioorganic &amp; Medicinal Chemistry, 2011. 19(3): p. 1155-1161; ISI[000287419000017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287635800055">Biyouxanthones a - D, Prenylated Xanthones from Roots of Hypericum Chinense</a>. Tanaka, N., T. Mamemura, S. Abe, K. Imabayashi, Y. Kashiwada, Y. Takaishi, T. Suzuki, Y. Takebe, T. Kubota, and J. Kobayashi. Heterocycles, 2010. 80(1): p. 613-621; ISI[000287635800055].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287288300003">Inhibition of Hbv Replication by Theophylline</a>. Zheng, Z.R., J.Y. Li, J. Sun, T. Song, C.W. Wei, Y.H. Zhang, G.R. Rao, G.M. Chen, D.C. Li, G.M. Yang, B. Han, S. Wei, C. Cao, and H. Zhong. Antiviral Research, 2011. 89(2): p. 149-155; ISI[000287288300003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0304-031711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
