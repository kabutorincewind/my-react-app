

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-141.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="V0yx+pAMar4FH7KwRmcEQkecUBKOu5cJaaxxyDEP5nrvVvhPJEV5DAdy48R5zOC7d2iIkDu6RiJ3WUFQ7IHIlUffuQTea4+FVFLnCOExovKm+6enUkBmxoa17dk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="89E2DA41" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-141-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60478   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          The Histone Deacetylase Inhibitor Fk228 Given Prior to Adenovirus Infection Can Boost Infection in Melanoma Xenograft Model Systems</p>

    <p>          Goldsmith, M. <i> et al.</i></p>

    <p>          Molecular Cancer Therapeutics <b>2007</b>.  6(2): 496-505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244262700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244262700010</a> </p><br />

    <p>2.     60479   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Reduction of the infectivity of hepatitis C virus pseudoparticles by incorporation of misfolded glycoproteins induced by glucosidase inhibitors</p>

    <p>          Chapel, C, Garcia, C, Bartosch, B, Roingeard, P, Zitzmann, N, Cosset, FL, Dubuisson, J, Dwek, RA, Trepo, C, Zoulim, F, and Durantel, D</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 4): 1133-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17374756&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17374756&amp;dopt=abstract</a> </p><br />

    <p>3.     60480   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Chlorpromazine and Apigenin Reduce Adenovirus Replication and Decrease Replication Associated Toxicity</p>

    <p>          Kanerva, A. <i>et al.</i></p>

    <p>          Journal of Gene Medicine <b>2007</b>.  9(1): 3-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244399800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244399800001</a> </p><br />

    <p>4.     60481   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p><b>          Relationship between HBV genotypes and anti-viral therapeutic efficacy of interferon-alpha</b> </p>

    <p>          Ma, JC, Wang, LW, Li, XJ, Liao, YF, Hu, XY, and Gong, ZJ</p>

    <p>          Hepatobiliary Pancreat Dis Int <b>2007</b>.  6(2): 166-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17374576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17374576&amp;dopt=abstract</a> </p><br />

    <p>5.     60482   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          In vitro activity of 2,4-diamino-6-[2-(phosphonomethoxy)ethoxy]-pyrimidine against multidrug-resistant hepatitis B virus (HBV) mutants</p>

    <p>          Brunelle, MN, Lucifora, J, Neyts, J, Villet, S, Holy, A, Trepo, C, and Zoulim, F</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371827&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371827&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60483   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Evolution of Resistant Mutant M414T in HCV Replicon Cells Treated with Polymerase Inhibitor A-782759</p>

    <p>          Lu, L, Mo, H, Pilot-Matias, TJ, and Molla, A</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371824&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371824&amp;dopt=abstract</a> </p><br />

    <p>7.     60484   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Discovery of Novel Antiviral Leads for the Treatment of Hantavirus Infections</p>

    <p>          Chung, D. <i>et al.</i></p>

    <p>          American Journal of Tropical Medicine and Hygiene <b>2006</b> .  75(5): 277</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343901401">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242343901401</a> </p><br />

    <p>8.     60485   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Pyranone, Thiopyranone, and Pyridone Inhibitors of Phosphatidylinositol 3-Kinase Related Kinases. Structure-Activity Relationships for DNA-Dependent Protein Kinase Inhibition, and Identification of the First Potent and Selective Inhibitor of the Ataxia Telangiectasia Mutated Kinase</p>

    <p>          Hollick, JJ, Rigoreau, LJ, Cano-Soumillac, C, Cockcroft, X, Curtin, NJ, Frigerio, M, Golding, BT, Guiard, S, Hardcastle, IR, Hickson, I, Hummersone, MG, Menear, KA, Martin, NM, Matthews, I, Newell, DR, Ord, R, Richardson, CJ, Smith, GC, and Griffin, RJ</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17371003&amp;dopt=abstract</a> </p><br />

    <p>9.     60486   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Production and characterization of monoclonal antibodies against different epitopes of Ebola virus antigens</p>

    <p>          Shahhosseini, S, Das, D, Qiu, X, Feldmann, H, Jones, SM, and Suresh, MR</p>

    <p>          J Virol Methods <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368819&amp;dopt=abstract</a> </p><br />

    <p>10.   60487   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Imidazo[1,2-a]pyridines with potent activity against herpesviruses</p>

    <p>          Gudmundsson, KS and Johns, BA</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368024&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17368024&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60488   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          2-METHYLTHIO-6-NITRO-1,2,4-TRIAZOLO[5,1-C]-1,2,4-TRIAZINE-7(4H)-ONE SODIUM SALT DIHYDRATE POSSESSING ANTIVIRAL ACTIVITY</p>

    <p>          Chupakhin, ON, Rusinov, VL, Ulomskii, EN, Charushin, VN, Petrov, AYu, and Kiselev, OI</p>

    <p>          PATENT:  RU <b>2294936</b>  ISSUE DATE: 20070310</p>

    <p>          APPLICATION: 2005-54714  PP: 12pp.</p>

    <p>          ASSIGNEE:  (Gos. Obraz. Uchrezhd. Vyssh. Prof. Obraz. Ural&#39;skii Gos. Tekhn. Univ. -UPI, Russia and OOO Nauchno-Tekhn. Predpriyatie \&quot;Ligand\&quot;)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   60489   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Application of the Phosphoramidate ProTide Approach to 4&#39;-Azidouridine Confers Sub-micromolar Potency versus Hepatitis C Virus on an Inactive Nucleoside</p>

    <p>          Perrone, P, Luoni, GM, Kelleher, MR, Daverio, F, Angell, A, Mulready, S, Congiatu, C, Rajyaguru, S, Martin, JA, Leveque, V, Pogam, SL, Najera, I, Klumpp, K, Smith, DB, and McGuigan, C</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17367121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17367121&amp;dopt=abstract</a> </p><br />

    <p>13.   60490   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis of novel 2&#39;-methyl carbovir analogues as potent antiviral agents</p>

    <p>          Hong, JH</p>

    <p>          Arch Pharm Res <b>2007</b>.  30(2): 131-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17366731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17366731&amp;dopt=abstract</a> </p><br />

    <p>14.   60491   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of a group of selective inhibitors of Ebola cell entry</p>

    <p>          Yermolina, Maria V, Wang, Jizhen, Wardrop, Duncan J, and Wang, Lijun</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-062</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60492   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          In vitro antiviral activity of Chamaecrista nictitans (Fabaceae) against herpes simplex virus: biological characterization of mechanisms of action</p>

    <p>          Herrero, Uribe L, Chaves, Olarte E, and Tamayo, Castillo G</p>

    <p>          Rev Biol Trop <b>2004</b>.  52(3): 807-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17361573&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17361573&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60493   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral triterpenoids from the medicinal plant Schefflera heptaphylla</p>

    <p>          Li, Y, Jiang, R, Ooi, LS, But, PP, and Ooi, VE</p>

    <p>          Phytother Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17357972&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17357972&amp;dopt=abstract</a> </p><br />

    <p>17.   60494   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of seven-membered sugar ring nucleoside analogs as antiviral prodrug agents</p>

    <p>          Storer, Richard, Gosselin, Gilles, Dukhan, David, Leroy, Frederic, Meillon, Jean-Christophe, and Converd, Thierry</p>

    <p>          PATENT:  WO <b>2007025043</b>  ISSUE DATE:  20070301</p>

    <p>          APPLICATION: 2006  PP: 195pp.</p>

    <p>          ASSIGNEE:  (Idenix Pharmaceuticals, Inc. USA and Centre National de la Recherche Scientifique)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60495   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Ribavirin, Human Convalescent Plasma and Anti-Beta(3) Integrin Antibody Inhibit Infection by Sin Nombre Virus in the Deer Mouse Model</p>

    <p>          Medina, R. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2007</b>.  88: 493-505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243987100018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243987100018</a> </p><br />

    <p>19.   60496   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Superior efficacy of helicase-primase inhibitor BAY 57-1293 for herpes infection and latency in the guinea pig model of human genital herpes disease</p>

    <p>          Baumeister, J, Fischer, R, Eckenberg, P, Henninger, K, Ruebsamen-Waigmann, H, and Kleymann, G</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(1): 35-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354650&amp;dopt=abstract</a> </p><br />

    <p>20.   60497   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Human antibodies and immunoconjugates capable of neutralizing West Nile virus and other flaviviruses, and uses in diagnosis, prophylaxis and therapy</p>

    <p>          Throsby, Mark and De Kruif, Cornelis Adriaan</p>

    <p>          PATENT:  US <b>2007042359</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 51  PP: 43pp., Cont.-in-part of Appl. No. PCT/EP05-052160.</p>

    <p>          ASSIGNEE:  (Crucell Holland B.V., Neth.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60498   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          High frequency of spontaneous helicase-primase inhibitor (BAY 57-1293) drug-resistant variants in certain laboratory isolates of HSV-1</p>

    <p>          Biswas, S, Swift, M, and Field, HJ</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(1): 13-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354648&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17354648&amp;dopt=abstract</a> </p><br />

    <p>22.   60499   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral activity of human beta-defensin 3 against vaccinia virus</p>

    <p>          Howell, MD, Streib, JE, and Leung, DY</p>

    <p>          J Allergy Clin Immunol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353034&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17353034&amp;dopt=abstract</a> </p><br />

    <p>23.   60500   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Influenza neuraminidase antibodies provide partial protection for chickens against high pathogenic avian influenza infection</p>

    <p>          Sylte, MJ, Hubby, B, and Suarez, DL</p>

    <p>          Vaccine <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17350145&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17350145&amp;dopt=abstract</a> </p><br />

    <p>24.   60501   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Interferon alfa (pegylated and non-pegylated) and ribavirin for the treatment of mild chronic hepatitis C: a systematic review and economic evaluation</p>

    <p>          Shepherd, J, Jones, J, Hartwell, D, Davidson, P, Price, A, and Waugh, N</p>

    <p>          Health Technol Assess <b>2007</b>.  11(11): 1-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346498&amp;dopt=abstract</a> </p><br />

    <p>25.   60502   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p><b>          Antiviral effects of antisense morpholino oligomers in murine coronavirus infection models</b> </p>

    <p>          Burrer, R, Neuman, BW, Ting, JP, Stein, DA, Moulton, HM, Iversen, PL, Kuhn, P, and Buchmeier, MJ</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17344287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17344287&amp;dopt=abstract</a> </p><br />

    <p>26.   60503   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Updates on antiviral therapy for chronic hepatitis C</p>

    <p>          Toniutto, P, Fabris, C, Bitetto, D, Fornasiere, E, Rapetti, R, and Pirisi, M</p>

    <p>          Discov Med <b>2007</b>.  7(37): 27-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17343802&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17343802&amp;dopt=abstract</a> </p><br />

    <p>27.   60504   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of Sulforamate derivatives</p>

    <p>          Moriarty, Robert M, Naithani, Rajesh, Surve, Bhushan C, Tiwari, Vaibhav, and Shukla, Deepak</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-064</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   60505   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of Abyssinone II analogs</p>

    <p>          Moriarty, Robert M, Surve, Bhushan C, Naithani, Rajesh, Chandersekera, Susantha N, Tiwari, Vaibhav, and Shukla, Deepak</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-063</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   60506   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Optimizing the synthesis of N-methanocarbathymidine, a potent and selective antiviral agent</p>

    <p>          Marquez, Victor E and Ludek, Olaf R</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-061</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   60507   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Protonated antimicrobial and antiviral compounds and methods for their use</p>

    <p>          Dale, Roderic MK</p>

    <p>          PATENT:  US <b>2007053971</b>  ISSUE DATE:  20070308</p>

    <p>          APPLICATION: 2006-28663  PP: 42pp., Cont.-in-part of U.S. Ser. No. 937,094.</p>

    <p>          ASSIGNEE:  (Oligos Etc., Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   60508   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral compositions containing p-hydroxybenzoic acid and analogues for prevention and treatment of skin virus infections</p>

    <p>          Zhang, Qingmin and Qin, Weihua</p>

    <p>          PATENT:  WO <b>2007014514</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 18pp.</p>

    <p>          ASSIGNEE:  (Shenghua Guangzhou Pharmaceutical Science &amp; Technology Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   60509   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Use of acyl thiouracil derivatives as antiviral drug</p>

    <p>          Zhou, Pei, Feng, Meiqing, Sun, Chuanwen, Huang, Hai, and Zhou, Wei</p>

    <p>          PATENT:  CN <b>1903203</b>  ISSUE DATE: 20070131</p>

    <p>          APPLICATION: 1002-8153  PP: 11pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   60510   DMID-LS-141; PUBMED-DMID-3/26/2007</p>

    <p class="memofmt1-2">          The effects of DNA methylation and histone deacetylase inhibitors on human papillomavirus early gene expression in cervical cancer, an in vitro and clinical study</p>

    <p>          de la Cruz-Hernandez, E, Perez-Cardenas, E, Contreras-Paredes, A, Cantu, D, Mohar, A, Lizano, M, and Duenas-Gonzalez, A</p>

    <p>          Virol J <b>2007</b> .  4: 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17324262&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17324262&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   60511   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Pf-03491390, (Formerly Idn-6556) a Pancaspase Inhibitor, Is Well-Tolerated and Effectively Reduces Raised Aminotransferases (Alt and Ast) in Chronic Active Hepatitis C (Hcv) Patients (Pts)</p>

    <p>          Shiffman, M. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 224A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300096">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300096</a> </p><br />

    <p>35.   60512   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis C Viral (Hcv) Replication by in-Vivo Dimerization of Stat1</p>

    <p>          Li, X. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 307A-308A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300318">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300318</a> </p><br />

    <p>36.   60513   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antisense antiviral compound and method for treating picornavirus infection</p>

    <p>          Stein, David A, Bestwick, Richard K, Iversen, Patrick L, and Weller, Dwight D</p>

    <p>          PATENT:  WO <b>2007030691</b>  ISSUE DATE:  20070315</p>

    <p>          APPLICATION: 2006  PP: 51pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   60514   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of pyridazine derivatives as picornavirus inhibitors</p>

    <p>          Li, Song, Mi, Chunlai, Zheng, Zhibing, Zhao, Guoming, Zhou, Xinbo, and Gong, Zehui</p>

    <p>          PATENT:  CN <b>1887875</b>  ISSUE DATE: 20070103</p>

    <p>          APPLICATION: 1008-1813  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Institute of Pharmacology &amp; Toxicology, Academy of Military Medical Sciences Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   60515   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Tetrahydrocarbazoles as potential therapeutic agents for human papillomavirus infection</p>

    <p>          Sebahar, Paul R, Brown, Kevin W, Gudmundsson, Kristjan S, Harvey, Robert, Richardson, Leah, and Sethna, Phiroze</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-087</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   60516   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Methods for engineering artificial zinc finger proteins for inhibiting DNA replication of human papillomavirus</p>

    <p>          Sera, Takashi</p>

    <p>          PATENT:  WO <b>2007024029</b>  ISSUE DATE:  20070301</p>

    <p>          APPLICATION: 2006  PP: 53pp.</p>

    <p>          ASSIGNEE:  (Kyoto University, Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>40.   60517   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral compositions containing hydroxybenzoic acid ester and analogues for prevention and treatment of virus infections</p>

    <p>          Zhang, Qingmin and Qin, Weihua</p>

    <p>          PATENT:  WO <b>2007014515</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 23pp.</p>

    <p>          ASSIGNEE:  (Shenghua (Guangzhou) Pharmaceutical Science &amp; Technology Co., Ltd. Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   60518   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Docking and 3-D-QSAR analysis of HCV NS5B RNA-dependent RNA polymerase inhibitors based on a common benzothiadiazine scaffold</p>

    <p>          Odde, Srinivas, Sivaprakasam, Prasanna, and Doerksen, Robert J</p>

    <p>          Abstracts of Papers, 233rd ACS National Meeting, Chicago, IL, United States, March 25-29, 2007  <b>2007</b>.: MEDI-085</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   60519   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Tetracyclic indole derivatives as antiviral agents</p>

    <p>          Narjes, Frank and Stansfield, Ian</p>

    <p>          PATENT:  WO <b>2007029029</b>  ISSUE DATE:  20070315</p>

    <p>          APPLICATION: 2006  PP: 29pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   60520   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Quinazoline derivatives as antiviral agents</p>

    <p>          Donghi, Monica, Ferrara, Marco, Koch, Uwe, Narjes, Frank, Ontoria Ontoria, Jesus Maria, and Summa, Vincenzo</p>

    <p>          PATENT:  WO <b>2007028789</b>  ISSUE DATE:  20070315</p>

    <p>          APPLICATION: 2006  PP: 56pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>44.   60521   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of 3&#39;,5&#39;-cyclic nucleoside analogs as antiviral agents for treatment of HCV</p>

    <p>          Gunic, Esmir, Hong, Zhi, and Girardet, Jean-Luc</p>

    <p>          PATENT:  WO <b>2007027248</b>  ISSUE DATE:  20070308</p>

    <p>          APPLICATION: 2006  PP: 158pp.</p>

    <p>          ASSIGNEE:  (Valeant Research &amp; Development, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   60522   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Phenylglycine as a novel P2 scaffold in hepatitis C virus NS3 protease inhibitors</p>

    <p>          Oertqvist, Pernilla, Peterson, Shane D, Aakerblom, Eva, Gossas, Thomas, Sabnis, Yogesh A, Fransson, Rebecca, Lindeberg, Gunnar, Danielson, UHelena, Karlen, Anders, and Sandstroem, Anja</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(3): 1448-1474</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   60523   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Role of Combination Antiviral Therapy in Pandemic Influenza</p>

    <p>          Tsiodras, S., Mooney, J., and Hatzakilis, A.</p>

    <p>          British Medical Journal <b>2007</b>.  334(7588): 293-294</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244654100031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244654100031</a> </p><br />

    <p>47.   60524   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of spiroisoxazoline-based peptidomimetics as inhibitors of serine proteases, particularly HCV NS3-NS4A protease</p>

    <p>          Cottrell, Kevin M, Maxwell, John, Tang, Qing, Grillot, Anne-Laure, Le Tiran, Arnaud, and Perola, Emanuele</p>

    <p>          PATENT:  WO <b>2007025307</b>  ISSUE DATE:  20070301</p>

    <p>          APPLICATION: 2006  PP: 489pp.</p>

    <p>          ASSIGNEE:  (Vertex Pharmaceuticals Incorporated, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   60525   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Platinum(Ii) and Palladium(Ii) Complexes of Pyridine-2-Carbaldehyde Thiosemicarbazone as Alternative Antiherpes Simplex Virus Agents</p>

    <p>          Kovala-Demertzi, D. <i>et al.</i></p>

    <p>          Bioinorganic Chemistry and Applications <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244557200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244557200001</a> </p><br />

    <p>49.   60526   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of amino acid-containing nucleotide phosphoramidates as antiviral agents</p>

    <p>          Klumpp, Klaus, Martin, Joseph Armstrong, Mcguigan, Christopher, and Smith, David Bernard</p>

    <p>          PATENT:  WO <b>2007020193</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006  PP: 145pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche A.-G., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   60527   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of 2&#39;-C-Me nucleoside 5 &#39;-monophosphate and 4&#39;-C-Me nucleoside 5&#39;- monophosphate prodrugs for the treatment of hepatitis C viral infection</p>

    <p>          Erion, Mark D, Reddy, KRaja, Maccoss, Malcolm, and Olsen, David B</p>

    <p>          PATENT:  WO <b>2007022073</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006  PP: 268pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA and Metabasis Therapeutics, Inc.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>51.   60528   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of macrocyclic inhibitors of hepatitis C virus</p>

    <p>          De Kock, Herman Augustinus, Simmen, Kenneth Alan, Joensson, Carl Erik Daniel, Ayesa Alvarez, Susana, Classon, Bjoern Olof, Nilsson, Karl Magnus, Rosenquist, Aasa Annica Kristina, Samuelsson, Bengt Bertil, and Wallberg, Hans Kristian</p>

    <p>          PATENT:  WO <b>2007014923</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 94pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire. and Medivir AB)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>52.   60529   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of macrocyclic carboxylic acids, amides, and acylsulfonamides as inhibitors of HCV replication</p>

    <p>          Seiwert, Scott D, Blatt, Lawrence M, Andrews, Steven W, Martin, Pierre, Schumacher, Andreas, Barnett, Bradley R, Eary, Todd C, Kaus, Robert, Kercher, Timothy, Liu, Weidong, Lyon, Michael, Nichols, Paul, Wang, Bin, Sammakia, Tarek, Kennedy, April, and Jiang, Yutong</p>

    <p>          PATENT:  WO <b>2007015824</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 512pp.</p>

    <p>          ASSIGNEE:  (Intermune, Inc. USA and Array Biopharma Inc.)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>53.   60530   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          6-Membered aryl and heteroaryl derivatives for treating viruses and their preparation and pharmaceutical compositions</p>

    <p>          Botyanszki, Janos, Shi, Dong-Fang, Roberts, Christopher Don, and Schmitz, Franz Ulrich</p>

    <p>          PATENT:  US <b>2007032488</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006-40709  PP: 45pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>54.   60531   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p><b>          Preparation of peptidyl boronic acid derivatives as hepatitis C serine protease inhibitors</b> </p>

    <p>          Campbell, David Alan, Winn, David T, Betancort, Juan Manuel, and Hepperle, Michael E</p>

    <p>          PATENT:  WO <b>2007016476</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 107pp.</p>

    <p>          ASSIGNEE:  (Phenomix Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>55.   60532   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Preparation of macrocyclic peptides as HCV NS3 protease inhibitors</p>

    <p>          Holloway, MKatharine, Liverton, Nigel J, McCauley, John A, Rudd, Michael T, Vacca, Joseph P, Ludmerer, Steven W, and Olsen, David B</p>

    <p>          PATENT:  WO <b>2007016441</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 210pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>56.   60533   DMID-LS-141; SCIFINDER-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antiviral compositions containing telbivudine salts and their hydrates</p>

    <p>          Yang, Xihong</p>

    <p>          PATENT:  CN <b>1903869</b>  ISSUE DATE: 20070131</p>

    <p>          APPLICATION: 1010-1214  PP: 30pp.</p>

    <p>          ASSIGNEE:  (Peop. Rep. China)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>57.   60534   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Factors That Affect in Vitro Measurement of the Susceptibility of Herpes Simplex Virus to Nucleoside Analogues</p>

    <p>          Weinberg, A. <i>et al.</i></p>

    <p>          Journal of Clinical Virology <b>2007</b>.  38(2): 139-145</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244400200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244400200009</a> </p><br />

    <p>58.   60535   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          In Vitro Susceptibility of Hbv Polymerase Encoding Mutations Acquired During Adefovir Dipivoxil Therapy to Other Anti-Hbv Agents</p>

    <p>          Qi, X. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 252A-253A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300172">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300172</a> </p><br />
    <br clear="all">

    <p>59.   60536   DMID-LS-141; WOS-DMID-3/26/2007</p>

    <p class="memofmt1-2">          Antimicrobial Peptides as New Recognition Molecules for Screening Challenging Species</p>

    <p>          Kulagina, N. <i>et al.</i></p>

    <p>          Sensors and Actuators B-Chemical <b>2007</b>.  121(1): 150-157</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244326400018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244326400018</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
