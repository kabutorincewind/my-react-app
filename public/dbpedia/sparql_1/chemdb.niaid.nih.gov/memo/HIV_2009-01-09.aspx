

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-01-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cs1+w6vV0NRCadPxTtgbEgGF5G2Ix6MmQoy2ZzNHGTXHYrP9XRc+cuHhvjJtLVZBRPFrEVnEHpgg0FRW0woLzTGcBepHSw45zKSnqMztP5zhfz92/JomtdCpdNg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FDC266EC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV  Citation List:  December  25 - January 6, 2009</h1>

    <p class="memofmt2-2">Anti*HIV</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19122322?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti Human Immunodeficiency Virus-1 (HIV-1) Agents 1. Discovery of Benzyl Phenyl Ethers as New HIV-1 Inhibitors in Vitro.</a></p>

    <p class="authors">Dai HL, Liu WQ, Xu H, Yang LM, Lv M, Zheng YT.</p>

    <p class="source">Chem Pharm Bull (Tokyo). 2009 Jan;57(1):84-6.</p>

    <p class="pmid">PMID: 19122322 [PubMed - in process]            </p> 

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19118579?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">High-throughput screening using pseudotyped lentiviral particles: A strategy for the identification of HIV-1 inhibitors in a cell-based assay.</a></p>

    <p class="authors">Garcia JM, Gao A, He PL, Choi J, Tang W, Bruzzone R, Schwartz O, Naya H, Nan FJ, Li J, Altmeyer R, Zuo JP.</p>

    <p class="source">Antiviral Res. 2008 Dec 29. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19118579 [PubMed - as supplied by publisher]    </p> 

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19053778?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis, and biological evaluation of N-carboxyphenylpyrrole derivatives as potent HIV fusion inhibitors targeting gp41.</a></p>

    <p class="authors">Liu K, Lu H, Hou L, Qi Z, Teixeira C, Barbault F, Fan BT, Liu S, Jiang S, Xie L.</p>

    <p class="source">J Med Chem. 2008 Dec 25;51(24):7843-54.</p>

    <p class="pmid">PMID: 19053778 [PubMed - in process]            </p> 

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19053755?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Development and preclinical studies of broad-spectrum anti-HIV agent (3&#39;R,4&#39;R)-3-cyanomethyl-4-methyl-3&#39;,4&#39;-di-O-(S)-camphanoyl-(+)-cis-khellactone (3-cyanomethyl-4-methyl-DCK).</a></p>

    <p class="authors">Xie L, Guo HF, Lu H, Zhuang XM, Zhang AM, Wu G, Ruan JX, Zhou T, Yu D, Qian K, Lee KH, Jiang S.</p>

    <p class="source">J Med Chem. 2008 Dec 25;51(24):7689-96.</p>

    <p class="pmid">PMID: 19053755 [PubMed - in process]            </p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19001108?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel nucleotide human immunodeficiency virus reverse transcriptase inhibitor GS-9148 with a low nephrotoxic potential: characterization of renal transport and accumulation.</a></p>

    <p class="authors">Cihlar T, Laflamme G, Fisher R, Carey AC, Vela JE, Mackman R, Ray AS.</p>

    <p class="source">Antimicrob Agents Chemother. 2009 Jan;53(1):150-6. Epub 2008 Nov 10.</p>

    <p class="pmid">PMID: 19001108 [PubMed - in process]            </p> 

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/18486994?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, HIV-RT inhibitory activity and SAR of 1-benzyl-1H-1,2,3-triazole derivatives of carbohydrates.</a></p>

    <p class="authors">da Silva Fde C, de Souza MC, Frugulhetti II, Castro HC, Souza SL, de Souza TM, Rodrigues DQ, Souza AM, Abreu PA, Passamani F, Rodrigues CR, Ferreira VF.</p>

    <p class="source">Eur J Med Chem. 2009 Jan;44(1):373-83. Epub 2008 Mar 16.</p>

    <p class="pmid">PMID: 18486994 [PubMed - in process]            </p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/18394758?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and anti-HIV-1 integrase activities of 3-aroyl-2,3-dihydro-1,1-dioxo-1,4,2-benzodithiazines.</a></p>

    <p class="authors">Brzozowski Z, Saczewski F, S&#322;awi&#324;ski J, Sanchez T, Neamati N.</p>

    <p class="source">Eur J Med Chem. 2009 Jan;44(1):190-6. Epub 2008 Mar 4.</p>

    <p class="pmid">PMID: 18394758 [PubMed - in process]            </p>


    <p class="memofmt2-2">Compound and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19088294?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Cyanovirin-N potently inhibits human immunodeficiency virus type 1 infection in cellular and cervical explant models.</a></p>

    <p class="authors">Buffa V, Stieh D, Mamhood N, Hu Q, Fletcher P, Shattock RJ.</p>

    <p class="source">J Gen Virol. 2009 Jan;90(Pt 1):234-43.</p>

    <p class="pmid">PMID: 19088294 [PubMed - in process]            </p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19076826?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of human immunodeficiency virus (HIV-1) infection in human peripheral blood leucocytes-SCID reconstituted mice by rapamycin.</a></p>

    <p class="authors">Nicoletti F, Lamenta C, Donati S, Spada M, Ranazzi A, Cacopardo B, Mangano K, Belardelli F, Perno C, Aquaro S.</p>

    <p class="source">Clin Exp Immunol. 2009 Jan;155(1):28-34.</p>

    <p class="pmid">PMID: 19076826 [PubMed - in process]            </p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19010684?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro activities of a new antiviral duplex drug linking Zidovudine (AZT) and Foscarnet (PFA) via an octadecylglycerol residue.</a></p>

    <p class="authors">Schott H, Hamprecht K, Schott S, Schott TC, Schwendener RA.</p>

    <p class="source">Bioorg Med Chem. 2009 Jan 1;17(1):303-10. Epub 2008 Nov 5.</p>

    <p class="pmid">PMID: 19010684 [PubMed - in process]            </p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18984007?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Non-nucleoside HIV-1 reverse transcriptase inhibitors di-halo-indolyl aryl sulfones achieve tight binding to drug-resistant mutants by targeting the enzyme-substrate complex.</a></p>

    <p class="authors">Samuele A, Kataropoulou A, Viola M, Zanoli S, La Regina G, Piscitelli F, Silvestri R, Maga G.</p>

    <p class="source">Antiviral Res. 2009 Jan;81(1):47-55. Epub 2008 Nov 5.</p>

    <p class="pmid">PMID: 18984007 [PubMed - in process]            </p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18952602?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A New Generation of Peptide-based Inhibitors Targeting HIV-1 Reverse Transcriptase Conformational Flexibility.</a></p>

    <p class="authors">Agopian A, Gros E, Aldrian-Herrada G, Bosquet N, Clayette P, Divita G.</p>

    <p class="source">J Biol Chem. 2009 Jan 2;284(1):254-64. Epub 2008 Oct 23.</p>

    <p class="pmid">PMID: 18952602 [PubMed - in process]            </p>

    <p class="memofmt2-2">Inhibition and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19114674?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">SC29EK, a peptide fusion inhibitor with enhanced {alpha}-helicity, inhibits replication of human immunodeficiency virus type 1 mutants resistant to enfuvirtide.</a></p>

    <p class="authors">Naito T, Izumi K, Kodama E, Sakagami Y, Kajiwara K, Nishikawa H, Watanabe K, Sarafianos SG, Oishi S, Fujii N, Matsuoka M.</p>

    <p class="source">Antimicrob Agents Chemother. 2008 Dec 29. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19114674 [PubMed - as supplied by publisher]    </p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18850604?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Complete absolute configuration of integramide A, a natural, 16-mer peptide inhibitor of HIV-1 integrase, elucidated by total synthesis.</a></p>

    <p class="authors">De Zotti M, Formaggio F, Kaptein B, Broxterman QB, Felock PJ, Hazuda DJ, Singh SB, Brückner H, Toniolo C.</p>

    <p class="source">Chembiochem. 2009 Jan 5;10(1):87-90. No abstract available.</p>

    <p class="pmid">PMID: 18850604 [PubMed - in process]            </p>

    <p class="memofmt2-2">Citations From The Forwarded &#39;Web of Knowledge&#39; listings</p>

    <p class="title">15.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19007201?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design of Annulated Pyrazoles as Inhibitors of HIV-1 Reverse Transcriptase.</a></p>

    <p class="authors">Sweeney ZK, Harris SF, Arora N, Javanbakht H, Li Y, Fretland J, Davidson JP, Billedeau JR, Gleason SK, Hirschfeld D, Kennedy-Smith JJ, Mirzadegan T, Roetz R, Smith M, Sperry S, Suh JM, Wu J, Tsing S, Villasen&#771;or AG, Paul A, Su G, Heilek G, Hang JQ, Zhou AS, Jernelius JA, Zhang FJ, Klumpp K.</p>

    <p class="source">J Med Chem. 2008 Nov 14. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19007201 [PubMed - as supplied by publisher]    </p>

    <p class="title">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Synthesis and Bioactivity of Dibenzylbutance Ligans and Their Anaologues</a></p>

    <p class="pmid">Xia YM, Bi WH and Cao XP</p>

    <p class="pmid">Chemical Journal of Chinese Universities. 2008 Nov; 29(11): 2178-2182.             </p>

    <p class="title">17.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18855969?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Optimization of 5-aryloxyimidazole non-nucleoside reverse transcriptase inhibitors.</a></p>

    <p class="authors">Jones LH, Allan G, Corbau R, Hay D, Middleton DS, Mowbray CE, Newman SD, Perros M, Randall A, Vuong H, Webster R, Westby M, Williams D.</p>

    <p class="source">ChemMedChem. 2008 Nov;3(11):1756-62.</p>

    <p class="pmid">PMID: 18855969 [PubMed - in process]            </p>

    <p class="pmid">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Synthesis and Anti-HIV-1 Activity of Carbocyclic Versions of Stavudine Analogues using a Ring-Closing Metathesis</a></p>

    <p class="pmid">Liu LJ, Ko OH and Hong JH</p>

    <p class="pmid">Bulletin of the Korean Chemical Society. 2008 Sep; 29(9): 1723-28.        </p>

    <p class="title">19.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18751697?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Marmorin, a new ribosome inactivating protein with antiproliferative and HIV-1 reverse transcriptase inhibitory activities from the mushroom Hypsizigus marmoreus.</a></p>

    <p class="authors">Wong JH, Wang HX, Ng TB.</p>

    <p class="source">Appl Microbiol Biotechnol. 2008 Dec;81(4):669-74. Epub 2008 Aug 27.</p>

    <p class="pmid">PMID: 18751697 [PubMed - in process]            </p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
