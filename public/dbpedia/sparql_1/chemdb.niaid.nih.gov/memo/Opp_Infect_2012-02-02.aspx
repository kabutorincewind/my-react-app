

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-02-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9fji0oI1mEYraoIIl/F63SxKbeJgojHizb5PxZnmHQawZAU9H3OHJa/1obXEC/UGrgBiI4pTJNPdw0woCpj8QqHUo4+2MPIVFcyuVN1LQHpoKOk7XHhj23mW+rc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="200367E4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">January 20 - February 2, 2012</h1>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22273392">Antimycobacterial and Antioxidant Activities of Reserpine and Its Derivatives.</a> Begum, S., S.Q. Naqvi, A. Ahmed, S. Tauseef, and B.S. Siddiqui. Natural Product Research, 2012. <b>[Epub ahead of print]</b>; PMID[22273392].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22078007">Synthesis and Antitubercular Activity of Novel Amino acid Derivatives.</a> Da Costa, C.F., A.C. Pinheiro, M.V. De Almeida, M.C. Lourenco, and M.V. De Souza. Chemical Biology &amp; Drug Design, 2012. 79(2): p. 216-222; PMID[22078007].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22154833">Synthesis and in Vitro Antimycobacterial Activity of Compounds Derived from (R)- and (S)-2-Amino-1-butanol - the Crucial Role of the Configuration.</a> Dobrikov, G.M., V. Valcheva, M. Stoilova-Disheva, G. Momekov, P. Tzvetkova, A. Chimov, and V. Dimitrov. European Journal of Medicinal Chemistry, 2012. 48: p. 45-56; PMID[22154833].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22106218">Phenylethyl Butyrate Enhances the Potency of Second-line Drugs against Clinical Isolates of Mycobacterium tuberculosis.</a> Grau, T., P. Selchow, M. Tigges, R. Burri, M. Gitzinger, E.C. Bottger, M. Fussenegger, and P. Sander. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 1142-1145; PMID[22106218].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21126020">Novel Trisubstituted benzimidazoles, Targeting Mtb Ftsz, as a New Class of Antitubercular Agents.</a> Kumar, K., D. Awasthi, S.Y. Lee, I. Zanardi, B. Ruzsicska, S. Knudson, P.J. Tonge, R.A. Slayden, and I. Ojima. Journal of Medicinal Chemistry, 2011. 54(1): p. 374-381; PMID[21126020].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22079533">An Antibacterial from Hypericum Acmosepalum Inhibits ATP-dependent Mure Ligase from Mycobacterium tuberculosis.</a> Osman, K., D. Evangelopoulos, C. Basavannacharya, A. Gupta, T.D. McHugh, S. Bhakta, and S. Gibbons. International Journal of Antimicrobial Agents, 2012. 39(2): p. 124-129; PMID[22079533].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22101189">The Cathelicidin-like Peptide Derived from Panda Genome Is a Potential Antimicrobial Peptide.</a> Yan, X., J. Zhong, H. Liu, C. Liu, K. Zhang, and R. Lai. Gene, 2012. 492(2): p. 368-374; PMID[22101189].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22266946">Synthesis, Structure and Biological Evaluation of Novel Bicyclic Nitroimidazole Derivatives.</a> Zaprutko, L., J. Zwawiak, E. Augustynowicz-Kopec, Z. Zwolska, E. Bartoszak-Adamska, and W. Nowicki. Archiv Der Pharmazie, 2012. <b>[Epub ahead of print]</b>; PMID[22266946].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22284812">Synthesis and Antimalarial Activity of Dihydroperoxides and Tetraoxanes Conjugated with bis(Benzyl)acetone Derivatives.</a> Franco, L.L., M.V. de Almeida, E.S. LF, P.P. Vieira, A.M. Pohlit, and M.S. Valle. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22284812].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22221750">In Vitro Antiplasmodial Effect of Ethanolic Extracts of Traditional Medicinal Plant Ocimum Species against Plasmodium falciparum.</a> Inbaneson, S.J., R. Sundaram, and P. Suganthi. Asian Pacific Journal of Tropical Medicine, 2012. 5(2): p. 103-106; PMID[22221750].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22249123">Synthesis of Purine N(9)-[2-hydroxy-3-O-(phosphonomethoxy)propyl] Derivatives and Their Side-Chain Modified Analogs as Potential Antimalarial Agents.</a>  Krecmerova, M., M. Dracinsky, D. Hockova, A. Holy, D.T. Keough, and L.W. Guddat. Bioorganic &amp; Medicinal Chemistry, 2012. 20(3): p. 1222-1230; PMID[22249123].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148880">Antimalarial Activity of 9a-N Substituted 15-Membered Azalides with Improved in Vitro and in Vivo Activity over Azithromycin.</a> Peric, M., A. Fajdetic, R. Rupcic, S. Alihodzic, D. Ziher, M. Bukvic Krajacic, K.S. Smith, Z. Ivezic-Schonfeld, J. Padovan, G. Landek, D. Jelic, A. Hutinec, M. Mesic, A. Ager, W.Y. Ellis, W.K. Milhous, C. Ohrt, and R. Spaventi. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22148880].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22275299">Synthesis and Evaluation of 1-Amino-6-halo-beta-carbolines as Antimalarial and Antiprion Agents.</a> Thompson, M.J., J.C. Louth, S.M. Little, M.P. Jackson, Y. Boursereau, B. Chen, and I. Coldham. ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22275299].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22290450">In Vitro Anti-plasmodial Activity of Some Traditionally Used Medicinal Plants against Plasmodium falciparum.</a> Venkatesalu, V., N. Gopalan, C.R. Pillai, V. Singh, M. Chandrasekaran, A. Senthilkumar, and N. Chandramouli. Parasitology Research, 2012. <b>[Epub ahead of print]</b>; PMID[22290450].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22083475">In Vitro and in Vivo Activity of Solithromycin (Cem-101) against Plasmodium Species.</a> Wittlin, S., E. Ekland, J.C. Craft, J. Lotharius, I. Bathurst, D.A. Fidock, and P. Fernandes. Antimicrobial Agents and Chemotherapy, 2012. 56(2): p. 703-707; PMID[22083475].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22243961">Benzoxaborole Antimalarial Agents. Part 2: Discovery of Fluoro-substituted 7-(2-Carboxyethyl)-1,3-dihydro-1-hydroxy-2,1-benzoxaboroles.</a> Zhang, Y.K., J.J. Plattner, Y.R. Freund, E.E. Easom, Y. Zhou, L. Ye, H. Zhou, D. Waterson, F.J. Gamo, L.M. Sanz, M. Ge, Z. Li, L. Li, H. Wang, and H. Cui. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(3): p. 1299-1307; PMID[22243961].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22143090">In Vitro Antileishmanial and Antitrypanosomal Activities of Flavanones from Baccharis retusa DC. (Asteraceae).</a> Grecco Sdos, S., J.Q. Reimao, A.G. Tempone, P. Sartorelli, R.L. Cunha, P. Romoff, M.J. Ferreira, O.A. Favero, and J.H. Lago. Experimental Parasitology, 2012. 130(2): p. 141-145; PMID[22143090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298392900030">Efficacy of Avicennia marina (Forsk.) Vierh. Leaves Extracts Against Some Atmospheric Fungi.</a> Afzal, M., F.S. Mehdi, F.M. Abbasi, H. Ahmad, R. Masood, Inamullah, J. Alam, G. Jan, M. Islam, N. Ul Amin, A. Majid, M. Fiaz, and A.H. Shah. African Journal of Biotechnology, 2011. 10(52): p. 10790-10794; ISI[000298392900030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298563400016">4-Substituted Thioquinolines and Thiazoloquinolines: Potent, Selective, and Tween-80 in Vitro Dependent Families of Antitubercular Agents with Moderate in Vivo Activity.</a> Escribano, J., C. Rivero-Hernandez, H. Rivera, D. Barros, J. Castro-Pichel, E. Perez-Herran, A. Mendoza-Losana, I. Angulo-Barturen, S. Ferrer-Bazaga, E. Jimenez-Navarro, and L. Ballell. ChemMedChem, 2011. 6(12): p. 2252-2263; ISI[000298563400016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0120-020212.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298538600012">Ganoderma Lucidum: A Source for Novel Bioactive Lectin.</a> Girjal, V.U., S. Neelagund, and M. Krishnappa. Protein and Peptide Letters, 2011. 18(11): p. 1150-1157; ISI[000298538600012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0120-020212.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">SciFinder citations</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?CC=WO&amp;NR=2008130669A1&amp;KC=A1&amp;FT=D&amp;ND=10&amp;date=20081030&amp;DB=EPODOC&amp;locale=en_gb">Benzimidazoles and Pharmaceutical Compositions Thereof.</a> Ojima, I. and S. Lee. Patent. 2008-US05084 20080421, 62 pp.</p>

    <p class="plaintext"><b>[Scifinder]</b>. OI_0120-020212.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
