

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-06-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JnROBUC5ov+qk+SbR0Cxlg3BTpscO5uzF4OMtfQupGbxDNFYRuUn7BFwbivsg4/V6tShVHa6hKlqlNHC47hWOZ4RZSgmt/R+Dc4jY+7/jUfbQ4VnlfSKUCFeOgo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4F0184C8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  May 28 - June 10, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20517160">Potent In Vitro Inactivation of Both Free and Cell-Associated CCR5- and CXCR4-Tropic HIV-1 by Common Commercial Soap Bars From South Africa.</a> Jenabian MA, Auvert B, Saïdi H, Lissouba P, Matta M, Bélec L. J Acquir Immune Defic Syndr. 2010 May 28. [Epub ahead of print]PMID: 20517160</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20512467">Anti-HIV-1 activity of lignans from the fruits of Schisandra rubriflora.</a> Xiao WL, Wang RR, Zhao W, Tian RR, Shang SZ, Yang LM, Yang JH, Pu JX, Zheng YT, Sun HD. Arch Pharm Res. 2010 May;33(5):697-701. Epub 2010 May 29.PMID: 20512467</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20507987">Microvirin, a novel {alpha}(1,2)-mannose-specific lectin isolated from microcystis aeruginosa, has comparable anti-HIV-1 activity as cyanovirin-N, but a much higher safety profile.</a> Huskens D, Ferir G, Vermeire K, Kehr JC, Balzarini J, Dittmann E, Schols D. J Biol Chem. 2010 May 27. [Epub ahead of print]PMID: 20507987</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20507280">Identification of broad-based HIV-1 protease inhibitors from combinatorial libraries.</a> Chang MW, Giffin MJ, Muller R, Savage J, Lin YC, Hong S, Jin W, Whitby LR, Elder JH, Boger DL, Torbett BE. Biochem J. 2010 May 27. [Epub ahead of print]PMID: 20507280</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20454539">Solid-Phase Synthesis of 5&#39;-O-beta,gamma-Methylenetriphosphate Derivatives of Nucleosides and Evaluation of Their Inhibitory Activity Against HIV-1 Reverse Transcriptase.</a> Ahmadibeni Y, Dash C, Le Grice SF, Parang K. Tetrahedron Lett. 2010 Jun 2;51(22):3010-3013.PMID: 20454539</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20380622">The disposition and metabolism of GW695634: a non-nucleoside reverse transcriptase inhibitor (NNRTi) for treatment of HIV/AIDS.</a> de Serres M, Moss L, Sigafoos J, Sefler A, Castellino S, Bowers G, Serabjit-Singh C. Xenobiotica. 2010 Jun;40(6):437-45.PMID: 20380622</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20380483">Anti-HIV therapy with AZT prodrugs: AZT phosphonate derivatives, current state and prospects.</a> Khandazhinskaya A, Matyugina E, Shirokova E. Expert Opin Drug Metab Toxicol. 2010 Jun;6(6):701-14.PMID: 20380483</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277659000012">Cytotoxicity of Natural Compounds Isolated from the Seeds of Garcinia afzelii.</a>  Lannang, AM; Louh, GN; Biloa, BM; Komguem, J; Mbazoa, CD; Sondengam, BL; Naesens, L; Pannecouque, C; De Clercq, E; El Ashry, EH.  PLANTA MEDICA, 2010; 76(7):708-712.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277690600012">Synthesis and In Vitro Anti-HIV Activity of Some New Schiff Base Ligands Derived from 5-Amino-4-phenyl-4H-1,2,4-triazole-3-thiol and Their Metal Complexes.</a>  Al-Masoudi, NA; Aziz, NM; Mohammed, AT. PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS, 2009; 184(11): 2891-2901.</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277621100010">Design and Synthesis of Novel Sate Derivatives of Acyclic Isocytosine and 9-Deazaadenine C-Nucleosides.</a>  Liu, LJ; Hong, JH.  NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2010; 29(3): 257-266.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277910800002">Acafusin, a Dimeric Antifungal Protein from Acacia confusa Seeds.</a>  Lam, SK; Ng, TB.  PROTEIN AND PEPTIDE LETTERS, 2010; 17(7): 817-822.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274983700018">SMALL-MOLECULE INHIBITORS OF HIV-1 REPLICATION TARGETING REVERSE TRANSCRIPTASE (RT) DIMERIZATION.</a>  Camarasa, MJ. DRUGS OF THE FUTURE, 2010; 34(SUP A): 34-35.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274983700111">OPTIMIZATION OF A POTENT HIV-1 INTEGRASE STRAND TRANSFER INHIBITOR (INSTI): MOLECULAR MODELING, SYNTHESIS AND ANTI-HIV ACTIVITY.</a>De Grazia, S; Ferro, S; De Luca, L; Barreca, ML; Debyser, Z; Chimirri, A. DRUGS OF THE FUTURE, 2010; 34(SUP A): 140.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274983700121">PYRIDONE DIARYL ETHER NON-NUCLEOSIDE INHIBITORS OF HIV-1 REVERSE TRANSCRIPTASE.</a>  Kennedy-Smith, J; Arora, N; Billedeau, R; Fretland, J; Hang, JL; Heilek, G; Harris, S; Hirschfeld, D; Javanbakht, H; Li, Y; Liang, WL; Roetz, R; Smith, M; Su, GS; Suh, J; Sweeney, Z; Villasenor, A; Wu, J; Yasuda, D; Klumpp, K. DRUGS OF THE FUTURE, 2010; 34(SUP A): 150.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274983700186">DISCOVERY OF NOVEL BICYCLIC PYRIDINE AND PYRAZINE HETEROCYCLIC SYSTEMS WITH ANTI-HIV-1 ACTIVITY.</a>  de Castro, S; Familiar, O; Balzarini, J; Camarasa, MJ; Velazquez, S. DRUGS OF THE FUTURE, 2010; 34(SUP A): 215.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277913400021">Synthesis and anti-HIV activity evaluation of new phenyl ethyl thiourea (PET) derivatives.</a>  Ettari, R; Pinto, A; Micale, N. ARKIVOC, 2009;   Part 14: 227-234.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
