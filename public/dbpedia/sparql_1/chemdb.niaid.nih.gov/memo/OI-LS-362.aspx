

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-362.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qK/JfQxx03gJEgXG7KIKTPMqGvwKkJTgaX1HhEduYHvNegzK10xt31eV4Ll4JGL0TPLWpLONQf2s0QkEJ9tp8FvxbksXLXEc1RaAEieO4WCOiRjRtf3FDh4jaOw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="91C6B2C8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-362-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59507   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Proteome-wide Profiling of Isoniazid Targets in Mycobacterium tuberculosis</p>

    <p>          Argyrou, A, Jin, L, Siconilfi-Baez, L, Angeletti, RH, and Blanchard, JS</p>

    <p>          Biochemistry <b>2006</b>.  45(47): 13947-13953</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17115689&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17115689&amp;dopt=abstract</a> </p><br />

    <p>2.     59508   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          AccD6, a Member of the Fas II Locus, is a Functional Carboxyltransferase Subunit of the Acyl-CoA Carboxylase in Mycobacterium tuberculosis</p>

    <p>          Daniel, J, Oh, TJ, Lee, CM, and Kolattukudy, PE</p>

    <p>          J Bacteriol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17114269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17114269&amp;dopt=abstract</a> </p><br />

    <p>3.     59509   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Phenylglycine as a novel P2 scaffold in hepatitis C virus NS3 protease inhibitors</p>

    <p>          Ortqvist, P, Peterson, SD, Kerblom, E, Gossas, T, Sabnis, YA, Fransson, R, Lindeberg, G, Helena, Danielson U, Karlen, A, and Sandstrom, A</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113777&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113777&amp;dopt=abstract</a> </p><br />

    <p>4.     59510   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Chemotherapy and diagnosis of tuberculosis</p>

    <p>          Saltini, C</p>

    <p>          Respir Med <b>2006</b>.  100(12): 2085-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113007&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113007&amp;dopt=abstract</a> </p><br />

    <p>5.     59511   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          International Standards for Tuberculosis Care</p>

    <p>          Hopewell, Philip C, Pai, Madhukar, Maher, Dermot, Uplekar, Mukund, and Raviglione, Mario C</p>

    <p>          The Lancet Infectious Diseases <b>2006</b>.  6(11): 710-725</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8X-4M5Y9F3-W/2/25079d1393485b5cd383189778e69b4b">http://www.sciencedirect.com/science/article/B6W8X-4M5Y9F3-W/2/25079d1393485b5cd383189778e69b4b</a> </p><br />
    <br clear="all">

    <p>6.     59512   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-tuberculosis activity, and 3D-QSAR study of 4-(adamantan-1-yl)-2-substituted quinolines</p>

    <p>          Nayyar, A, Monga, V, Malde, A, Coutinho, E, and Jain, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17107805&amp;dopt=abstract</a> </p><br />

    <p>7.     59513   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Antimycobacterial activity of econazole against multidrug-resistant strains of Mycobacterium tuberculosis</p>

    <p>          Ahmad, Z, Sharma, S, Khuller, GK, Singh, P, Faujdar, J, and Katoch, VM</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101262&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101262&amp;dopt=abstract</a> </p><br />

    <p>8.     59514   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Cryptosporidium parvum: Identification of a new surface adhesion protein on sporozoite and oocyst by screening of a phage-display cDNA library</p>

    <p>          Yao, L, Yin, J, Zhang, X, Liu, Q, Li, J, Chen, L, Zhao, Y, Gong, P, and Liu, C</p>

    <p>          Exp Parasitol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17097085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17097085&amp;dopt=abstract</a> </p><br />

    <p>9.     59515   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Assessment of resistance in multi drug resistant tuberculosis patients</p>

    <p>          Irfan, S, Hassan, Q, and Hasan, R</p>

    <p>          J Pak Med Assoc <b>2006</b>.  56(9): 397-400</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091751&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091751&amp;dopt=abstract</a> </p><br />

    <p>10.   59516   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Long-term moxifloxacin in complicated tuberculosis patients with adverse reactions or resistance to first line drugs</p>

    <p>          Codecasa, Luigi Ruffo, Ferrara, Giovanni, Ferrarese, Maurizio, Morandi, Maria Antonietta, Penati, Valeria, Lacchini, Carla, Vaccarino, Patrizia, and Migliori, Giovanni Battista</p>

    <p>          Respiratory Medicine <b>2006</b>.  100(9): 1566-1572</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WWS-4J8D8YB-1/2/0ecffba38233d6fda041b37ef5da6298">http://www.sciencedirect.com/science/article/B6WWS-4J8D8YB-1/2/0ecffba38233d6fda041b37ef5da6298</a> </p><br />
    <br clear="all">

    <p>11.   59517   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Sequence resources at the Candida Genome Database</p>

    <p>          Arnaud, MB, Costanzo, MC, Skrzypek, MS, Shah, P, Binkley, G, Lane, C, Miyasato, SR, and Sherlock, G</p>

    <p>          Nucleic Acids Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17090582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17090582&amp;dopt=abstract</a> </p><br />

    <p>12.   59518   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Latent Tuberculosis Infection Treatment and T-cell responses to M. tuberculosis-Specific Antigens</p>

    <p>          Chee, CB, Khinmar, KW, Gan, SH, Barkham, TM, Pushparani, M, and Wang, YT</p>

    <p>          Am J Respir Crit Care Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17082492&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17082492&amp;dopt=abstract</a> </p><br />

    <p>13.   59519   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Design of endoperoxides with anti-Candida activity</p>

    <p>          Avery, TD, Macreadie, PI, Greatrex, BW, Robinson, TV, Taylor, DK, and Macreadie, IG</p>

    <p>          Bioorg Med Chem <b>2007</b>.  15(1): 36-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079152&amp;dopt=abstract</a> </p><br />

    <p>14.   59520   OI-LS-362; PUBMED-OI-11/27/2006</p>

    <p class="memofmt1-2">          Diagnosis and successful treatment of Cryptococcus neoformans variety grubii in a domestic ferret</p>

    <p>          Hanley, CS, MacWilliams, P, Giles, S, and Pare, J</p>

    <p>          Can Vet J <b>2006</b>.  47(10): 1015-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17078253&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17078253&amp;dopt=abstract</a> </p><br />

    <p>15.   59521   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Early Pulmonary Resection for Mycobacterium Avium Complex Lung Disease Treated With Macrolides and Quinolones</p>

    <p>          Watanabe, Masazumi, Hasegawa, Naoki, Ishizaka, Akitoshi, Asakura, Keisuke, Izumi, Yotarao, Eguchi, Keisuke, Kawamura, Masafumi, Horinouchi, Hirohisa, and Kobayashi, Koichi</p>

    <p>          The Annals of Thoracic Surgery <b>2006</b>.  81(6): 2026-2030</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T11-4K221RS-K/2/55b0ab7952fd642f8c0796f6ae3e50a5">http://www.sciencedirect.com/science/article/B6T11-4K221RS-K/2/55b0ab7952fd642f8c0796f6ae3e50a5</a> </p><br />
    <br clear="all">

    <p>16.   59522   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Antimicrobial peptides from diverse families isolated from the skin of the Asian frog, Rana grahami</p>

    <p>          Conlon, JMichael, Al-Ghaferi, Nadia, Abraham, Bency, Jiansheng, Hu, Cosette, Pascal, Leprince, Jerome, Jouenne, Thierry, and Vaudry, Hubert</p>

    <p>          Peptides <b>2006</b>.  27(9): 2111-2117</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T0M-4JRVFK9-9/2/8810dbf10fd3adcf57d277d40aa6c0a6">http://www.sciencedirect.com/science/article/B6T0M-4JRVFK9-9/2/8810dbf10fd3adcf57d277d40aa6c0a6</a> </p><br />

    <p>17.   59523   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Interesting coordination abilities of antiulcer drug famotidine and antimicrobial activity of drug and its cobalt(III) complex</p>

    <p>          Miodragovic, Djenana U, Bogdanovic, Goran A, Miodragovic, Zoran M, Radulovic, Milanka D, Novakovic, Sladjana B, Kaluderovic, Goran N, and Kozlowski, Henryk</p>

    <p>          Journal of Inorganic Biochemistry <b>2006</b>.  100(9): 1568-1574</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGG-4K4G2FD-1/2/0ecf17bc728d42dffc6e8485118eb074">http://www.sciencedirect.com/science/article/B6TGG-4K4G2FD-1/2/0ecf17bc728d42dffc6e8485118eb074</a> </p><br />

    <p>18.   59524   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Synthesis, characterization and antimicrobial activity of a series of substituted coumarin-3-carboxylatosilver(I) complexes</p>

    <p>          Creaven, Bernadette S, Egan, Denise A, Kavanagh, Kevin, McCann, Malachy, Noble, Andy, Thati, Bhumika, and Walsh, Maureen</p>

    <p>          Inorganica Chimica Acta <b>2006</b>.  359(12): 3976-3984</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TG5-4JSMV82-5/2/e047711dd88b98b1bb1459541d08388c">http://www.sciencedirect.com/science/article/B6TG5-4JSMV82-5/2/e047711dd88b98b1bb1459541d08388c</a> </p><br />

    <p>19.   59525   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Molecular targets for detection and immunotherapy in Cryptosporidium parvum</p>

    <p>          Boulter-Bitzer, Jeanine I, Lee, Hung, and Trevors, Jack T</p>

    <p>          Biotechnology Advances <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4X-4KSH9V6-1/2/c1f1f00b1889eb552ee6f121c047c68f">http://www.sciencedirect.com/science/article/B6T4X-4KSH9V6-1/2/c1f1f00b1889eb552ee6f121c047c68f</a> </p><br />

    <p>20.   59526   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Cryptosporidium parvum: The contribution of Th1-inducing pathways to the resolution of infection in mice</p>

    <p>          Ehigiator, Humphrey N, McNair, Nina, and Mead, Jan R</p>

    <p>          Experimental Parasitology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFH-4KPNB25-1/2/e9aaa72b5a4d8dbfe80b5caa8a2a4760">http://www.sciencedirect.com/science/article/B6WFH-4KPNB25-1/2/e9aaa72b5a4d8dbfe80b5caa8a2a4760</a> </p><br />

    <p>21.   59527   OI-LS-362; EMBASE-OI-11/26/2006</p>

    <p class="memofmt1-2">          Valganciclovir</p>

    <p>          Pescovitz, Mark D</p>

    <p>          Transplantation Reviews <b>2006</b>.  20(2): 82-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75B4-4KG2GY4-8/2/47cbc54b7bf39062ba723476da7ae898">http://www.sciencedirect.com/science/article/B75B4-4KG2GY4-8/2/47cbc54b7bf39062ba723476da7ae898</a> </p><br />

    <p>22.   59528   OI-LS-362; WOS-OI-11/19/2006</p>

    <p class="memofmt1-2">          Isonicotinoylhydrazothiazoles and isonicotinoyl-N-4-substituted thiosemicarbazides: Synthesis, characterization, and anti-mycobacterial activity</p>

    <p>          Cardia, MC, Distinto, S, Maccioni, E, Plumitallo, A, Saddi, M, Sanna, ML, and DeLogu, A</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2006</b>.  43(5): 1337-1342, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241541900029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241541900029</a> </p><br />

    <p>23.   59529   OI-LS-362; WOS-OI-11/19/2006</p>

    <p class="memofmt1-2">          Antifungal activity of some Tanzanian plants used traditionally for the treatment of fungal infections</p>

    <p>          Hamza, OJM, van den Bout-van den Beukel, CJP, Matee, MIN, Moshi, MJ, Mikx, FHM, Selemani, HO, Mbwambo, ZH, Van der Ven, AJAM, and Verweij, PE</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2006</b>.  108(1): 124-132, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241573000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241573000018</a> </p><br />

    <p>24.   59530   OI-LS-362; WOS-OI-11/19/2006</p>

    <p class="memofmt1-2">          Early and rapid microscopy-based diagnosis of true treatment failure and MDR-TB</p>

    <p>          Salim, AH, Aung, KJM, Hossain, MA, and Van Deun, A</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2006</b>.  10(11): 1248-1254, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241611800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241611800011</a> </p><br />

    <p>25.   59531   OI-LS-362; WOS-OI-11/19/2006</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluation of non-azole inhibitors of lanosterol 14 alpha-demethylase of fungi</p>

    <p>          Yao, B, Zhou, YJ, Zhu, J, Lu, JG, Li, YW, Cheng, J, Jiang, QF, and Zheng, CH</p>

    <p>          CHINESE CHEMICAL LETTERS <b>2006</b>.  17(9): 1189-1192, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241571600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241571600015</a> </p><br />

    <p>26.   59532   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          New C-3 &#39; hydroxamate-substituted and more lipophilic cyclic hydroxamate cephalosporin derivatives as a potential new generation of selective antimicrobial agents</p>

    <p>          Miller, MJ, Zhao, GY, Vakulenko, S, Franzblau, S, and Mollmann, U</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2006</b>.  4(22): 4178-4185, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241720500021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241720500021</a> </p><br />

    <p>27.   59533   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          Detection of rifampin resistance patterns in Mycobacterium tuberculosis strains isolated in Iran by polymerase chain reaction-single-strand conformation polymorphism and direct sequencing methods</p>

    <p>          Isfahani, BN, Tavakoli, A, Salehi, M, and Tazhibi, M</p>

    <p>          MEMORIAS DO INSTITUTO OSWALDO CRUZ <b>2006</b>.  101(6): 597-602, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241670700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241670700004</a> </p><br />

    <p>28.   59534   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          PD-1 expression in acute hepatitis C virus (HCV) infection is associated with HCV-specific CD8 exhaustion</p>

    <p>          Urbani, S, Amadei, B, Tola, D, Massari, M, Schivazappa, S, Missale, G, and Ferrari, C</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(22): 11398-11403, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241821300050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241821300050</a> </p><br />

    <p>29.   59535   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          Rahnella aquatilis strain AY2000 produces an anti-yeast substance</p>

    <p>          Ryu, EJ, Kim, HW, Kim, BW, Kwon, HJ, and Kim, KH</p>

    <p>          JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY <b>2006</b>.  16(10): 1597-1604, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241682500016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241682500016</a> </p><br />

    <p>30.   59536   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          Inhibition of the replication of hepatitis C virus replicon with nuclease-resistant RNA aptamers</p>

    <p>          Shin, KS, Lim, JH, Kim, JH, Myung, H, and Lee, SW</p>

    <p>          JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY <b>2006</b>.  16(10): 1634-1639, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241682500021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241682500021</a> </p><br />

    <p>31.   59537   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          New type of inhibitors of hepatitis C virus (HCV) helicase activity</p>

    <p>          Stankiewicz-Drogon, A, Kostina, V, Palchykovska, L, Krawczyk, M, Alexeeva, A, Shved, AD, and Boguszewska-Chachulska, AM</p>

    <p>          FEBS JOURNAL <b>2006</b>.  273: 314-1</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238914002255">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238914002255</a> </p><br />

    <p>32.   59538   OI-LS-362; WOS-OI-11/26/2006</p>

    <p class="memofmt1-2">          Synthesis and properties of 2-guanidinopurines</p>

    <p>          Cesnek, M, Masojidkova, M, Holy, A, Solinova, V, Koval, D, and Kasicka, V</p>

    <p>          COLLECTION OF CZECHOSLOVAK CHEMICAL COMMUNICATIONS <b>2006</b>.  71(9): 1303-1319, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241823600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241823600004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
