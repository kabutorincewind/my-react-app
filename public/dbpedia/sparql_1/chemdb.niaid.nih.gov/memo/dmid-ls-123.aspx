

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-123.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bfjRUfuFETmS4waWzC1fZhQrgMxXYeJ4gkDUyB+/9Ltg5y+aIvY19opalCe//UmsUVYtCuCUWE7SCcV8bp6+MhZnSjb/oDivRJIWL2hiwiG5BsdC/26lZfMgJ1E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="737190DD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-123-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58622   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Replication of Hepatitis C Virus (HCV) RNA in Mouse Embryonic Fibroblasts: Protein Kinase R (PKR)-Dependent and PKR-Independent Mechanisms for Controlling HCV RNA Replication and Mediating Interferon Activities</p>

    <p>          Chang, KS, Cai, Z, Zhang, C, Sen, GC, Williams, BR, and Luo, G</p>

    <p>          J Virol <b>2006</b>.  80(15): 7364-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16840317&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16840317&amp;dopt=abstract</a> </p><br />

    <p>2.     58623   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel phenyl branched apiosyl nucleosides</p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Arch Pharm Res  <b>2006</b>.  29(6): 464-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16833012&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16833012&amp;dopt=abstract</a> </p><br />

    <p>3.     58624   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Non-nucleoside Inhibitors Binding to Hepatitis C Virus NS5B Polymerase Reveal a Novel Mechanism of Inhibition</p>

    <p>          Biswal, BK, Wang, M, Cherney, MM, Chan, L, Yannopoulos, CG, Bilimoria, D, Bedard, J, and James, MN</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828488&amp;dopt=abstract</a> </p><br />

    <p>4.     58625   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pivotal role of animal models in the development of new therapies for cytomegalovirus infections</p>

    <p>          Kern, ER</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828175&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16828175&amp;dopt=abstract</a> </p><br />

    <p>5.     58626   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Toward orthopoxvirus countermeasures: a novel heteromorphic nucleoside of unusual structure</p>

    <p>          Fan, X, Zhang, X, Zhou, L, Keith, KA, Prichard, MN, Kern, ER, and Torrence, PF</p>

    <p>          J Med Chem <b>2006</b>.  49(14): 4052-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821766&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16821766&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58627   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New antiviral agents</p>

    <p>          Abdel-Haq, N, Chearskul, P, Al-Tatari, H, and Asmar, B</p>

    <p>          Indian J Pediatr <b>2006</b>.  73(4): 313-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16816493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16816493&amp;dopt=abstract</a> </p><br />

    <p>7.     58628   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antivirals for influenza: Historical perspectives and lessons learned</p>

    <p>          Hayden, FG</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815563&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815563&amp;dopt=abstract</a> </p><br />

    <p>8.     58629   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of a novel series of inhibitors of human cytomegalovirus primase</p>

    <p>          Cushing, TD, Adrian, J, Chen, X, Dimaio, H, Doughan, B, Flygare, J, Liang, L, Mayorga, V, Miao, S, Mellon, H, Peterson, MG, Powers, JP, Spector, F, Stein, C, Wright, M, Xu, D, Ye, Q, and Jaen, J</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16814545&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16814545&amp;dopt=abstract</a> </p><br />

    <p>9.     58630   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sulfonyl semicarbazides, semicarbazides and ureas, pharmaceutical compositions thereof, and methods for treating hemorrhagic fever virus infections, including infections associated with arenaviruses</p>

    <p>          Deng, Yijun, Nitz, Theodore J, Bailey, Thomas R, Zhang, Yanming, and Laquerre, Sylvie</p>

    <p>          PATENT:  WO <b>2006062898</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2005  PP: 127 pp.</p>

    <p>          ASSIGNEE:  (Siga Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   58631   DMID-LS-123; PUBMED-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          l-Nucleoside enantiomers as antivirals drugs: A mini-review</p>

    <p>          Mathe, C and Gosselin, G</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16797735&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16797735&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58632   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense, nuclease-resistant oligonucleotides targeting VP24, VP35, and L genes of filovirus for use as antiviral agents</p>

    <p>          Stein, David A, Iversen, Patrick L, and Bavari, Sina</p>

    <p>          PATENT:  WO <b>2006050414</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   58633   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of ethynyl uridine derivatives as antiviral drugs against Flaviviridae, especially HCV</p>

    <p>          Aucagne, Vincent, Escuret, Vanessa, Zoulim, Fabien, Durantel, David, Trepo, Christian, Agrofoglio, Luigi, Joubert, Nicolas, and Amblard, Franck</p>

    <p>          PATENT:  EP <b>1674104</b>  ISSUE DATE: 20060628</p>

    <p>          APPLICATION: 2004-28527  PP: 29 pp.</p>

    <p>          ASSIGNEE:  (Institut National De La Sante Et De La Recherche Medicale (Inserm), Fr. and Centre National De La Recherche Scientifique (CNRS))</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   58634   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of cyclobutyl nucleosides for use in the treatment of infections including Retroviridae, Hepadnaviridae, or Flaviviridae in animals and humans</p>

    <p>          Liotta, Dennis C, Mao, Shuli, and Hager, Michael</p>

    <p>          PATENT:  WO <b>2006063281</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2005  PP: 138 pp.</p>

    <p>          ASSIGNEE:  (Emory University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   58635   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of 3-phenyl-9H-thioxanthen-9-one derivatives as inhibitors of flavivirus replication</p>

    <p>          Gilbertson, Scott R, Lory, Pedro J, Malmstrom, Robert D, Pang, Yuan-Ping, Russo, Andrew T, and Watowich, Stanley J</p>

    <p>          PATENT:  WO <b>2006060774</b>  ISSUE DATE:  20060608</p>

    <p>          APPLICATION: 2005  PP: 107 pp.</p>

    <p>          ASSIGNEE:  (Board of Regents, The University of Texas System USA and Mayo Foundation for Medical Education and Research)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   58636   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activities of Purified Compounds From Youngia Japonica (L.) Dc (Asteraceae, Compositae)</p>

    <p>          Ooi, L. <i>et al.</i></p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  106(2): 187-191</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238457600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238457600006</a> </p><br />

    <p>16.   58637   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Valopicitabine - Anti-Hepatitis C Virus Drug Rna-Directed Rna Polymerase (Ns5b) Inhibitor</p>

    <p>          Sorbera, L., Castaner, J., and Leeson, P.</p>

    <p>          Drugs of the Future <b>2006</b>.  31(4): 320-324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238516600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238516600005</a> </p><br />
    <br clear="all">

    <p>17.   58638   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza Therapies: Vaccines and Antiviral Drugs</p>

    <p>          Quigley, E.</p>

    <p>          Drug Discovery Today <b>2006</b>.  11(11-12): 478-480</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238523500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238523500002</a> </p><br />

    <p>18.   58639   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p><b>          Synthesis of 6-Amino-, 6-Methyl- and 6-Aryl-2-(Hydroxymethyl) Purine Bases and Nucleosides</b> </p>

    <p>          Silhar, P. <i>et al.</i></p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(6): 788-803</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238419500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238419500004</a> </p><br />

    <p>19.   58640   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiretroviral Therapy 2006: Pharmacology, Applications, and Special Situations</p>

    <p>          Samuel, R., Bettiker, R., and Suh, B.</p>

    <p>          Archives of Pharmacal Research <b>2006</b>.  29(6): 431-458</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238660500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238660500001</a> </p><br />

    <p>20.   58641   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficient Inhibition of Hepatitis B Virus Replication by Small Interfering Rnas Targeted to the Viral X Gene in Mice</p>

    <p>          Shin, D. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  119(2): 146-153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238438700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238438700003</a> </p><br />

    <p>21.   58642   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Protease Inhibitor Specifically Inhibits Growth of Hpv-Infected Keratinocytes</p>

    <p>          Drubin, D. <i>et al.</i></p>

    <p>          Molecular Therapy <b>2006</b>.  13(6): 1142-1148</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238402000015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238402000015</a> </p><br />

    <p>22.   58643   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          N- and 6-O-Sulfated Heparan Sulfates Mediate Internalization of Coxsackievirus B3 Variant Pd Into Cho-K1 Cells</p>

    <p>          Zautner, A. <i>et al.</i></p>

    <p>          Journal of Virology <b>2006</b>.  80(13): 6629-6636</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238380700044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238380700044</a> </p><br />

    <p>23.   58644   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Morpholino antisense drugs for human herpesvirus 8 (HHV8)</p>

    <p>          Zhang, Yan-Jin and Matson, David O</p>

    <p>          PATENT:  WO <b>2006010041</b>  ISSUE DATE:  20060126</p>

    <p>          APPLICATION: 2005  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Eastern Virginia Medical School, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>24.   58645   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and evaluation of acridine- and acridone-based anti-herpes agents with topoisomerase activity</p>

    <p>          Goodell, John R, Madhok, Avni A, Hiasa, Hiroshi, and Ferguson, David M</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(16): 5467-5480</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   58646   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of 5-(Alkyn-1-Yl)-1-(P-Toluenesulfonyl)Uracil Derivatives</p>

    <p>          Janeba, Z. <i>et al.</i></p>

    <p>          Canadian Journal of Chemistry-Revue Canadienne De Chimie <b>2006</b>.  84(4): 580-586</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238285200016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238285200016</a> </p><br />

    <p>26.   58647   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, Characterization, and Biological Activity of Amino Acid Derivatives of the Heteropolytungstophosphoric Acid</p>

    <p>          Kuntic, Vesna, Stanojevic, Maja, Holclajtner-Antunovic, Ivanka, Uskokovic-Markovic, Snezana, Mioc, Ubavka, Todorovic, Marija, Jovanovic, Tanja, and Vukojevic, Vladana</p>

    <p>          Monatshefte fuer Chemie <b>2006</b>.  137(6): 803-810</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   58648   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of the NF-kB pathway by varicella-zoster virus in vitro and in human epidermal cells in vivo</p>

    <p>          Jones, Jeremy O and Arvin, Ann M</p>

    <p>          Journal of Virology <b>2006</b>.  80(11): 5113-5124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   58649   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Thiadiazole pyrimidine derivatives as antiviral agents</p>

    <p>          Xue, Sijia and Sun, Chuanwen</p>

    <p>          PATENT:  CN <b>1771958</b>  ISSUE DATE: 20060517</p>

    <p>          APPLICATION: 1011-27  PP: 10 pp.</p>

    <p>          ASSIGNEE:  (Shanghai Normal University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   58650   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Measles Virus and Subacute Sclerosing Panencephalitis Virus by Rna Interference</p>

    <p>          Otaki, M. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(3): 105-111</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238397000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238397000002</a> </p><br />

    <p>30.   58651   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Combination Chemotherapy, a Potential Strategy for Reducing the Emergence of Drug-Resistant Influenza a Variants</p>

    <p>          Ilyushina, N. <i> et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  70(3): 121-131</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238397000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238397000004</a> </p><br />
    <br clear="all">

    <p>31.   58652   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Method for construction of hepatitis C virus-transgenic mouse model, and its application in antiviral drug screening</p>

    <p>          Zhan, Linsheng, Wang, Quanli, Rao, Lin, and Peng, Jianchun</p>

    <p>          PATENT:  CN <b>1718731</b>  ISSUE DATE: 20060111</p>

    <p>          APPLICATION: 1006-2739  PP: 8 pp.</p>

    <p>          ASSIGNEE:  (Institute of Field Transfusion, Academy of Military Medical Sciences Pla Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   58653   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Baraclude (Tm) (Entecavir), a Potent and Selective Inhibitor of Hbv-Rt: Molecular Mechanism(S) of Resistance in Viruses With Pre-Existing Lamivudine Resistance Substitutions</p>

    <p>          Langley, D. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U1398-U1399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797302821">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797302821</a> </p><br />

    <p>33.   58654   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular Modeling of Covalent Complexes and Structural-Based Design of Irreversible Inhibitors of Rhinovirus 3c Protease</p>

    <p>          Hou, X. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U1399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797302822">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797302822</a> </p><br />

    <p>34.   58655   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Triazoles useful as inhibitors of protein kinases and their preparation, pharmaceutical compositions, and use for treatment of various disorders</p>

    <p>          Davies, Robert J, Forster, Cornelia J, Arnost, Michael J, and Wang, Jian</p>

    <p>          PATENT:  WO <b>2006047256</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 91 pp.</p>

    <p>          ASSIGNEE:  (Vertex Pharmaceuticals Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   58656   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          M-Tyrosine- and Tic-Based Macrocyclic Inhibitors of Hepatitis C Virus (Hcv) Ns3 Protease: Synthesis and Biological Activity</p>

    <p>          Chen, K. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U2602-U2603</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305162">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305162</a> </p><br />

    <p>36.   58657   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Depeptidized P2-P4 Biaryl Macrocyclic Inhibitors of Hepatitis-C Ns3-4a Protease</p>

    <p>          Venkatraman, S. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U2605</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305167">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305167</a> </p><br />
    <br clear="all">

    <p>37.   58658   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Isatin Oximes as Inhibitors of Respiratory Syncytial Virus Fusion</p>

    <p>          Venables, B. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U2609</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305171">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305171</a> </p><br />

    <p>38.   58659   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of Sch 503034, a Selective, Potent Hcvns3 Protease Inhibitor With Oral Bioavailability: Potential Therapeutic Agent for Treating Hepatitis C Viral Infection</p>

    <p>          Njoroge, F. <i>et al.</i></p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U2647-U2648</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305241">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305241</a> </p><br />

    <p>39.   58660   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Lead Optimization of Hepatitis C Viral Polymerase Inhibitors: an Acyl Pyrrolidine Development Candidate</p>

    <p>          Slater, M.</p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U2648</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305242">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797305242</a> </p><br />

    <p>40.   58661   DMID-LS-123; WOS-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Highly Stereoselective Synthesis of Bicyclic-Acetal-Based Macrocyclic Inhibitors of Hepatitis C Virus</p>

    <p>          Chen, K. and Njoroge, F.</p>

    <p>          Abstracts of Papers of the American Chemical Society <b>2005</b>.  230: U3341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797306520">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797306520</a> </p><br />

    <p>41.   58662   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptides for the treatment of viral infections, including herpes virus infections</p>

    <p>          Forssmann, Wolf-Georg, Kirchhoff, Frank, Muench, Jan, and Staendker, Ludger</p>

    <p>          PATENT:  WO <b>2006018431</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 13 pp.</p>

    <p>          ASSIGNEE:  (IPF Pharmaceuticals G.m.b.H., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   58663   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of alkoxyalkyl derivatives of 9-(S)-(3-hydroxy-2-phosphonomethoxypropyl)adenine against cytomegalovirus and orthopoxviruses</p>

    <p>          Beadle James R, Wan William B, Ciesla Stephanie L, Keith Kathy A, Hartline Caroll, Kern Earl R, and Hostetler Karl Y</p>

    <p>          Journal of medicinal chemistry <b>2006</b>.  49(6): 2010-5.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   58664   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sialidase fusion protein as a novel broad-spectrum inhibitor of influenza virus infection</p>

    <p>          Malakhov Michael P, Aschenbrenner Laura M, Smee Donald F, Wandersee Miles K, Sidwell Robert W, Gubareva Larisa V, Mishin Vasiliy P, Hayden Frederick G, Kim Do Hyong, Ing Alice, Campbell Erin R, Yu Mang, and Fang Fang</p>

    <p>          Antimicrobial agents and chemotherapy <b>2006</b>.  50(4): 1470-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.   58665   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recent advances in anti-influenza agents with neuraminidase as target</p>

    <p>          Zhang Jie and Xu Wenfang</p>

    <p>          Mini reviews in medicinal chemistry <b>2006</b>.  6(4): 429-48.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   58666   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Activity and mechanism of action of N-methanocarbathymidine against herpesvirus and orthopoxvirus infections</p>

    <p>          Prichard Mark N, Keith Kathy A, Quenelle Debra C, and Kern Earl R</p>

    <p>          Antimicrobial agents and chemotherapy <b>2006</b>.  50(4): 1336-41.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   58667   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome (SARS): development of diagnostics and antivirals</p>

    <p>          Soerensen, Morten Draeby, Soerensen, Brian, Gonzalez-Dosal, Regina, Melchjorsen, Connie Jenning, Weibel, Jens, Wang, Jing, Jun, Chen Wie, Yang, Huanming, and Kristensen, Peter</p>

    <p>          Annals of the New York Academy of Sciences <b>2006</b>.  1067(Understanding and Modulating Aging): 500-505 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   58668   DMID-LS-123; SCIFINDER-DMID-7/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential antivirals and antiviral strategies against SARS coronavirus infections</p>

    <p>          De Clercq, Erik</p>

    <p>          Expert Review of Anti-Infective Therapy <b>2006</b>.  4(2): 291-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
