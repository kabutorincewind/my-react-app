

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-01-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lrHB+x3zl3E/sT5dcOIb/CmeEL5kvOQrJ9tJF2LICt2835dX4BUEMrWFUV/ovo/ZGHO4K59q1X3V6vPti/nK129rvk//8ZtdJhyX6I1J3C9LMVX2CcSFI3o8xrs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7785496B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 18 - January 31, 2013</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23355441">Discovery of Hepatitis C Virus NS3 Helicase Inhibitors by a Multiplexed, High-throughput Helicase Activity Assay Based on Graphene oxide</a><i>.</i> Jang, H., S.R. Ryoo, Y.K. Kim, S. Yoon, H. Kim, S.W. Han, B.S. Choi, D.E. Kim, and D.H. Min. Angewandte Chemie, 2013.  <b>[Epub ahead of print]</b>; PMID[23355441].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23359228">Psammaplin A Inhibits Hepatitis C Virus NS3 Helicase</a><i>.</i> Salam, K.A., A. Furuta, N. Noda, S. Tsuneda, Y. Sekiguchi, A. Yamashita, K. Moriishi, M. Nakakoshi, M. Tsubuki, H. Tani, J. Tanaka, and N. Akimitsu. Journal of Natural Medicines, 2013.  <b>[Epub ahead of print]</b>; PMID[23359228].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23348596">The Antimalarial Ferroquine is an Inhibitor of Hepatitis C Virus</a><i>.</i> Vausselin, T., N. Calland, S. Belouzard, V. Descamps, F. Douam, F. Helle, C. Francois, D. Lavillette, G. Duverlie, A. Wahid, L. Feneant, L. Cocquerel, Y. Guerardel, C. Wychowski, C. Biot, and J. Dubuisson. Hepatology, 2013.  <b>[Epub ahead of print]</b>; PMID[23348596].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23349632">The Hepatitis B Virus Ribonuclease H is Sensitive to Inhibitors of the Human Immunodeficiency Virus Ribonuclease H and Integrase Enzymes.</a> Tavis, J.E., X. Cheng, Y. Hu, M. Totten, F. Cao, E. Michailidis, R. Aurora, M.J. Meyers, E.J. Jacobsen, M.A. Parniak, and S.G. Sarafianos. PLoS Pathogens, 2013. 9(1): p. e1003125. PMID[23349632].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23345517">Activity and Mechanism of Action of HDVD, a Novel Pyrimidine Nucleoside Derivative with High Selectivity and Potency against Gammaherpesviruses</a><i>.</i> Coen, N., U. Singh, V. Vuyyuru, J.J. Van den Oord, J. Balzarini, S. Duraffour, R. Snoeck, Y.C. Cheng, C.K. Chu, and G. Andrei. Journal of Virology, 2013.  <b>[Epub ahead of print]</b>; PMID[23345517].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23341010">Broad-spectrum Virucidal Activity of (NVC-422) N,N-Dichloro-2,2-dimethyltaurine against Viral Ocular Pathogens in Vitro</a><i>.</i> Jekle, A., S. Abdul Rani, C. Celeri, M. Zuck, P. Xu, L. Wang, K. Najafi, M. Anderson, D. Stroman, and D. Debabov. Investigative Ophthalmology &amp; Visual Science, 2013.  <b>[Epub ahead of print]</b>; PMID[23341010].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23341013">A Cationic Peptide, TAT-Cd0, Inhibits Herpes Simplex Virus Type 1 Ocular Infection in Vivo.</a> Jose, G.G., I.V. Larsen, J. Gauger, E. Carballo, R. Stern, R. Brummel, and C.R. Brandt. Investigative Ophthalmology &amp; Visual Science, 2013. <b>[Epub ahead of print]</b>. PMID[23341013].
    <br />
    <b>[PubMed]</b>. HIV_0118-013113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23361434">Efficacy of Herpes Virus Helicase-primase Inhibitor, ASP2151, for Treating Herpes Simplex Keratitis in Mouse Model</a><i>.</i> Sasaki, S.I., D. Miyazaki, T. Haruki, Y. Yamamoto, M. Kandori, K. Yakura, H. Suzuki, and Y. Inoue. The British Journal of Ophthalmology, 2013.  <b>[Epub ahead of print]</b>; PMID[23361434].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0118-013113.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000312518100004">Protein Kinase Inhibitors That Inhibit Induction of Lytic Program and Replication of Epstein-Barr Virus</a><i>.</i> Goswami, R., S. Gershburg, A. Satorius, and E. Gershburg. Antiviral Research, 2012. 96(3): p. 296-304; ISI[000312518100004].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0118-013113.</p>

    <p class="plaintext"><br />
    10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312573700002">Filibuvir. RNA-directed RNA Polymerase (NS5B) Inhibitor, Treatment of Hepatitis C Virus Infection</a><i>.</i> Kapelusznik, L., E.L. Heil, Z. Temesgen, and R. Talwani. Drugs of the Future, 2012. 37(11): p. 767-775; ISI[000312573700002].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0118-013113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
