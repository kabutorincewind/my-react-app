

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-348.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PpNg72xiCVayyiaCPuIr9b8Wi+WjB3QwmLryDI5P3w0bqTOQHcJO8fAP75qVu3uRrwld5kqAkPEUt6i18qFEusWFBipnIJSe36Cqp38wI43y3QUXrJk16hMH+EA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="057B7644" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-348-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48892   OI-LS-348; EMBASE-OI-5/15/2006</p>

    <p class="memofmt1-2">          &#39;Silver bullets&#39; in antimicrobial chemotherapy: Synthesis, characterisation and biological screening of some new Ag(I)-containing imidazole complexes</p>

    <p>          Rowan, Raymond, Tallon, Theresa, Sheahan, Anita M, Curran, Robert, McCann, Malachy, Kavanagh, Kevin, Devereux, Michael, and McKee, Vickie</p>

    <p>          Polyhedron <b>2006</b>.  25(8): 1771-1778</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TH8-4HWX9X6-3/2/e6b96e02f821c57413086ffffa4a7c1c">http://www.sciencedirect.com/science/article/B6TH8-4HWX9X6-3/2/e6b96e02f821c57413086ffffa4a7c1c</a> </p><br />

    <p>2.     48893   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Kinetic analysis of the effects of monovalent cations and divalent metals on the activity of Mycobacterium tuberculosis alpha-isopropylmalate synthase</p>

    <p>          de Carvalho, LP and Blanchard, JS</p>

    <p>          Arch Biochem Biophys <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16684501&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16684501&amp;dopt=abstract</a> </p><br />

    <p>3.     48894   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Antiparasitic activity of flavonoids and isoflavones against Cryptosporidium parvum and Encephalitozoon intestinalis</p>

    <p>          Mead, J and McNair, N</p>

    <p>          FEMS Microbiol Lett <b>2006</b>.  259(1): 153-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16684116&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16684116&amp;dopt=abstract</a> </p><br />

    <p>4.     48895   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Antifungal effects of aminosulphoxide and disulphide derivatives</p>

    <p>          Wittebolle, V, Lemriss, S, La, Morella G, Errante, J, Boiron, P, Barret, R, and Sarciron, ME</p>

    <p>          Mycoses <b>2006</b>.  49(3): 169-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16681806&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16681806&amp;dopt=abstract</a> </p><br />

    <p>5.     48896   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          In vitro investigations on the mode of action of the hydroxypyridone antimycotics rilopirox and piroctone on Candida albicans</p>

    <p>          Sigle, HC, Schafer-Korting, M, Korting, HC, Hube, B, and Niewerth, M</p>

    <p>          Mycoses <b>2006</b>.  49(3): 159-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16681805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16681805&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48897   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Production of infectious hepatitis C virus particles in three-dimensional cultures of the cell line carrying the genome-length dicistronic viral RNA of genotype 1b</p>

    <p>          Murakami, K, Ishii, K, Ishihara, Y, Yoshizaki, S, Tanaka, K, Gotoh, Y, Aizaki, H, Kohara, M, Yoshioka, H, Mori, Y, Manabe, N, Shoji, I, Sata, T, Bartenschlager, R, Matsuura, Y, Miyamura, T, and Suzuki, T</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16678876&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16678876&amp;dopt=abstract</a> </p><br />

    <p>7.     48898   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Potent inhibitors of the hepatitis C virus NS3 protease: Use of a novel P2 cyclopentane-derived template</p>

    <p>          Johansson, PO, Back, M, Kvarnstrom, I, Jansson, K, Vrang, L, Hamelink, E, Hallberg, A, Rosenquist, S, and Samuelsson, B</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675222&amp;dopt=abstract</a> </p><br />

    <p>8.     48899   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          A coupled spectrophotometric assay for l-cysteine:1-d-myo-inosityl 2-amino-2-deoxy-alpha-d-glucopyranoside ligase and its application for inhibitor screening</p>

    <p>          Newton, GL, Ta, P, Sareen, D, and Fahey, RC</p>

    <p>          Anal Biochem <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16674910&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16674910&amp;dopt=abstract</a> </p><br />

    <p>9.     48900   OI-LS-348; EMBASE-OI-5/15/2006</p>

    <p class="memofmt1-2">          Gatifloxacin derivatives: Synthesis, antimycobacterial activities, and inhibition of Mycobacterium tuberculosis DNA gyrase</p>

    <p>          Sriram, Dharmarajan, Aubry, Alexandra, Yogeeswari, Perumal, and Fisher, LM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(11): 2982-2985</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4JJ2BJM-2/2/27d1f9018ac921fd3763b08fdf29ceb7">http://www.sciencedirect.com/science/article/B6TF9-4JJ2BJM-2/2/27d1f9018ac921fd3763b08fdf29ceb7</a> </p><br />

    <p>10.   48901   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Novel ketoconazole analogues based on the replacement of 2,4-dichlorophenyl group with 1,4-benzothiazine moiety: Design, synthesis, and microbiological evaluation</p>

    <p>          Schiaffella, F, Macchiarulo, A, Milanese, L, Vecchiarelli, A, and Fringuelli, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650767&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650767&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48902   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Antiviral properties of new arylsulfone derivatives with activity against human betaherpesviruses</p>

    <p>          Naesens, L, Stephens, CE, Andrei, G, Loregian, A, De, Bolle L, Snoeck, R, Sowell, JW, and De, Clercq E</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650489&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16650489&amp;dopt=abstract</a> </p><br />

    <p>12.   48903   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV and antitubercular activities of isatin derivatives</p>

    <p>          Sriram, D, Yogeeswari, P, and Meena, K</p>

    <p>          Pharmazie <b>2006</b>.  61(4): 274-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16649536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16649536&amp;dopt=abstract</a> </p><br />

    <p>13.   48904   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Crystallographic and Pre-steady-state Kinetics Studies on Binding of NADH to Wild-type and Isoniazid-resistant Enoyl-ACP(CoA) Reductase Enzymes from Mycobacterium tuberculosis</p>

    <p>          Oliveira, JS, Pereira, JH, Canduri, F, Rodrigues, NC, de Souza, ON, de Azevedo, WF Jr, Basso, LA, and Santos, DS</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16647717&amp;dopt=abstract</a> </p><br />

    <p>14.   48905   OI-LS-348; PUBMED-OI-5/15/2006</p>

    <p class="memofmt1-2">          Photoinactivation of Mycobacteria in vitro and in a new murine model of localized Mycobacterium bovis BCG-induced granulomatous infection</p>

    <p>          O&#39;Riordan, K, Sharlin, DS, Gross, J, Chang, S, Errabelli, D, Akilov, OE, Kosaka, S, Nau, GJ, and Hasan, T</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(5): 1828-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641456&amp;dopt=abstract</a> </p><br />

    <p>15.   48906   OI-LS-348; EMBASE-OI-5/15/2006</p>

    <p class="memofmt1-2">          Synthesis of pyrazinamide Mannich bases and its antitubercular properties</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Reddy, Sushma Pobba</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(8): 2113-2116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J6X3N2-7/2/b1717482dd482fe09bd8dfa6c1620723">http://www.sciencedirect.com/science/article/B6TF9-4J6X3N2-7/2/b1717482dd482fe09bd8dfa6c1620723</a> </p><br />
    <br clear="all">

    <p>16.   48907   OI-LS-348; EMBASE-OI-5/15/2006</p>

    <p class="memofmt1-2">          The importance of Mycobacterium bovis as a zoonosis: 4th International Conference on Mycobacterium bovis</p>

    <p>          Thoen, Charles, LoBue, Philip, and de Kantor, Isabel</p>

    <p>          Veterinary Microbiology <b>2006</b>.  112(2-4): 339-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD6-4J2M6WV-2/2/34bfdb7a4f34c404fb03dc00ba01237c">http://www.sciencedirect.com/science/article/B6TD6-4J2M6WV-2/2/34bfdb7a4f34c404fb03dc00ba01237c</a> </p><br />

    <p>17.   48908   OI-LS-348; EMBASE-OI-5/15/2006</p>

    <p class="memofmt1-2">          Efficacy and safety of linezolid in multidrug resistant tuberculosis (MDR-TB)--a report of ten cases</p>

    <p>          von der Lippe, Bent, Sandven, Per, and Brubakk, Oddbjorn</p>

    <p>          Journal of Infection <b>2006</b>.  52(2): 92-96</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WJT-4G6J879-2/2/b2d473d42aff64894847b5f117c02b73">http://www.sciencedirect.com/science/article/B6WJT-4G6J879-2/2/b2d473d42aff64894847b5f117c02b73</a> </p><br />

    <p>18.   48909   OI-LS-348; WOS-OI-5/7/2006</p>

    <p class="memofmt1-2">          Synthesis of isonicotinic acid vinyloxyethylthiosemicarbazide</p>

    <p>          Ibraev, MK, Takibaeva, AT, Gazaliev, AM, Nurkenov, OA, and Fazylov, SD</p>

    <p>          RUSSIAN JOURNAL OF APPLIED CHEMISTRY <b>2006</b>.  79(2): 327-328, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236803900032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236803900032</a> </p><br />

    <p>19.   48910   OI-LS-348; WOS-OI-5/7/2006</p>

    <p class="memofmt1-2">          Functional shikimate dehydrogenase from Mycobacterium tuberculosis H37Rv: Purification and characterization</p>

    <p>          Fonseca, IO, Magalhaes, MLB, Oliveira, JS, Silva, RG, Mendes, MA, Palma, MS, Santos, DS, and Basso, LA</p>

    <p>          PROTEIN EXPRESSION AND PURIFICATION <b>2006</b>.  46(2): 429-437, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236828100033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236828100033</a> </p><br />

    <p>20.   48911   OI-LS-348; WOS-OI-5/7/2006</p>

    <p class="memofmt1-2">          More highlights from the 2006 conference on retroviruses and opportunistic infections</p>

    <p>          Dobkin, JE</p>

    <p>          INFECTIONS IN MEDICINE <b>2006</b>.  23(4): 181-+, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236920900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236920900011</a> </p><br />

    <p>21.   48912   OI-LS-348; WOS-OI-5/7/2006</p>

    <p class="memofmt1-2">          Niementowski reaction: microwave induced and conventional synthesis of quinazolinones and 3-methyl-1H-5-pyrazolones and their antimicrobial activity</p>

    <p>          Desai, AR and Desai, KR</p>

    <p>          ARKIVOC <b>2005</b>.: 98-108, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236923400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236923400009</a> </p><br />

    <p>22.   48913   OI-LS-348; WOS-OI-5/14/2006</p>

    <p class="memofmt1-2">          Enzymes of UDP-GlcNAc biosynthesis in yeast</p>

    <p>          Milewski, S, Gabriel, L, and Olchowy, J</p>

    <p>          YEAST <b>2006</b>.  23 (1): 1-14, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237061200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237061200001</a> </p><br />

    <p>23.   48914   OI-LS-348; WOS-OI-5/14/2006</p>

    <p class="memofmt1-2">          Fighting infection - An ongoing challenge, Part 3-antimycobacterials, antifungal, and antivirals</p>

    <p>          Turkoski, BB</p>

    <p>          ORTHOPAEDIC NURSING <b>2005</b>.  24(5): 380-388, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237086600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237086600009</a> </p><br />

    <p>24.   48915   OI-LS-348; WOS-OI-5/14/2006</p>

    <p class="memofmt1-2">          Susceptibility studies of piperazinyl-cross-linked fluoroquinolone dimers against test strains of Gram-positive and Gram-negative bacteria</p>

    <p>          Kerns, RJ, Rybak, MJ, and Cheung, CM</p>

    <p>          DIAGNOSTIC MICROBIOLOGY AND INFECTIOUS DISEASE <b>2006</b>.  54(4 ): 305-310, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236975800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236975800010</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
