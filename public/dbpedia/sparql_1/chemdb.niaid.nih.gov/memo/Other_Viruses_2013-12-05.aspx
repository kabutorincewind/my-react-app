

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-12-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XFSWzuUIyuyM6xR0V0GANc1nkm0TX36gz2YEjLFRSPluy5XfsF0hiCt4jvsoGXPz+TEgbcf1b56aCIfWEBxE6LozYdW8l9pxOEHIm6UHvzuy+qeleT4Ryv9fj54=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE48A5C5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: November 22 - December 5, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24295872">Cyclosporin A Inhibits Hepatitis B and Hepatitis D Virus Entry by Cyclophilin-independent Interference with the NTCP Receptor<i>.</i></a> Nkongolo, S., Y. Ni, F.A. Lempp, C. Kaufman, T. Lindner, K. Esser-Nobis, V. Lohmann, W. Mier, S. Mehrle, and S. Urban. Journal of Hepatology, 2013. <b>[Epub ahead of print]</b>; PMID[24295872].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23657805">Antiviral Activity of Bifidobacterium adolescentis SPM0212 against Hepatitis B Virus<i>.</i></a> Lee do, K., J.Y. Kang, H.S. Shin, I.H. Park, and N.J. Ha. Archives of Pharmacal Research, 2013. 36(12): p. 1525-1532; PMID[23657805].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24287992">Astataricusones A-D and Astataricusol A, Five New Anti-HBV Shionane-Type Triterpenes from Aster tataricus L. f<i>.</i></a> Zhou, W.B., G.Z. Zeng, H.M. Xu, W.J. He, and N.H. Tan. Molecules, 2013. 18(12): p. 14585-14596; PMID[24287992].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060868">Adapted J6/JFH1-Based Hepatitis C Virus Recombinants with Genotype-specific NS4A Show Similar Efficacies against Lead Protease Inhibitors, Alpha Interferon, and a Putative NS4A Inhibitor<i>.</i></a> Gottwein, J.M., S.B. Jensen, S.B. Serre, L. Ghanem, T.K. Scheel, T.B. Jensen, H. Krarup, N. Uzcategui, L.S. Mikkelsen, and J. Bukh. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6034-6049; PMID[24060868].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24157306">Differential Effect of P7 Inhibitors on Hepatitis C Virus Cell-to-Cell Transmission<i>.</i></a> Meredith, L.W., N. Zitzmann, and J.A. McKeating. Antiviral Research, 2013. 100(3): p. 636-639; PMID[24157306].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24036073">Humanized-VH/VHH that Inhibit HCV Replication by Interfering with the Virus Helicase Activity<i>.</i></a> Phalaphol, A., K. Thueng-In, J. Thanongsaksrikul, O. Poungpair, K. Bangphoomi, N. Sookrung, P. Srimanote, and W. Chaicumpa. Journal of Virological Methods, 2013. 194(1-2): p. 289-299; PMID[24036073].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24215243">Sofosbuvir (GS-7977), a Pan-genotype, Direct-Acting Antiviral for Hepatitis C Virus Infection<i>.</i></a> Rodriguez-Torres, M. Expert Review of Anti-Infective Therapy, 2013. 11(12): p. 1269-1279; PMID[24215243].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24266880">Structure-Activity Relationship (SAR) Optimization of 6-(Indol-2-yl)pyridine-3-sulfonamides: Identification of Potent, Selective and Orally Bioavailable Small Molecules Targeting Hepatitis C (HCV) NS4B<i>.</i></a> Zhang, N., X. Zhang, J. Zhu, A. Turpoff, G. Chen, C. Morrill, S. Huang, W. Lennox, R. Kakarla, R. Liu, C. Li, H. Ren, N.G. Almstead, S. Venkatraman, F.G. Njoroge, Z. Gu, V. Clausen, J.D. Graci, S.P. Jung, Y. Zheng, J.M. Colacino, F. Lahser, J. Sheedy, A. Mollin, M. Weetall, A. Nomeir, and G.M. Karp. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24266880].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22641235">Inhibition of Epstein-Barr Virus Reactivation in Nasopharyngeal Carcinoma Cells by Dietary Sulforaphane<i>.</i></a> Wu, C.C., H.Y. Chuang, C.Y. Lin, Y.J. Chen, W.H. Tsai, C.Y. Fang, S.Y. Huang, F.Y. Chuang, S.F. Lin, Y. Chang, and J.Y. Chen. Molecular Carcinogenesis, 2013. 52(12): p. 946-958; PMID[22641235].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24067975">Tetherin Restricts Herpes Simplex Virus 1 and Is Antagonized by Glycoprotein M<i>.</i></a> Blondeau, C., A. Pelchen-Matthews, P. Mlcochova, M. Marsh, R.S. Milne, and G.J. Towers. Journal of Virology, 2013. 87(24): p. 13124-13133; PMID[24067975].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24067963">SAMHD1 Restricts Herpes Simplex Virus 1 in Macrophages by Limiting DNA Replication<i>.</i></a> Kim, E.T., T.E. White, A. Brandariz-Nunez, F. Diaz-Griffero, and M.D. Weitzman. Journal of Virology, 2013. 87(23): p. 12949-12956; PMID[24067963].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24302293">A Cellular Response Protein Induced During HSV-1 Infection Inhibits Viral Replication by Interacting with ATF5<i>.</i></a> Wu, L., X. Zhang, Y. Che, Y. Zhang, S. Tang, Y. Liao, R. Na, X. Xiong, L. Liu, and Q. Li. Science China. Life Sciences, 2013. 56(12): p. 1124-1133; PMID[24302293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_1122-120513.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326374200049">Identification of a Series of 1,3,4-Trisubstituted Pyrazoles as Novel Hepatitis C Virus Entry Inhibitors<i>.</i></a> Hwang, J.Y., H.Y. Kim, D.S. Park, J. Choi, S.M. Baek, K. Kim, S. Kim, S. Seong, I. Choi, H.G. Lee, M.P. Windisch, and J. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(23): p. 6467-6473; ISI[000326374200049].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326745800020">Ligand Design, Synthesis and Biological anti-HCV Evaluations for Genotypes 1b and 4a of certain 4-(3-&amp; 4- 3-(3,5-Dibromo-4-hydroxyphenyl)-propylamino phenyl) butyric acids and 3-(3,5-Dibromo-4-hydroxyphenyl)-propylamino-acetamidobenzoic acid Esters<i>.</i></a> Ismail, M.A.H., K.A.M. Abouzid, N.S. Mohamed, and E.M.E. Dokla. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(6): p. 1274-1290; ISI[000326745800020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1122-120513.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326327600007">Signal Transducer and Activator of Transcription 3 Is a Proviral Host Factor for Hepatitis C Virus<i>.</i></a> McCartney, E.M., K.J. Helbig, S.K. Narayana, N.S. Eyre, A.L. Aloia, and M.R. Beard. Hepatology, 2013. 58(5): p. 1558-1568; ISI[000326327600007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1122-120513.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
