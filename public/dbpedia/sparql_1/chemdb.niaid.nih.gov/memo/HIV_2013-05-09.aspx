

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-05-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q2kutrH0tdidCYMSTJo+vH73RVttZTgP6U/ub8ZZC3CXUEZabhCt2VXtM+CK85EKEqWHyQZoNnhHgF9CTH0yI5XhYniFlNyDZJ2xioN1YLJijhG95JSZuEZlPrE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E476A359" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 26 - May 9, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23624649">2&#39;-Deoxythymidine Adducts from the anti-HIV Drug Nevirapine.</a> Antunes, A.M., B. Wolf, M.C. Oliveira, F.A. Beland, and M.M. Marques. Molecules, 2013. 18(5): p. 4955-4971. PMID[23624649].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23645585">Diaryltriazine Non-Nucleoside Reverse Transcriptase Inhibitors Are Potent Candidates for Pre-exposure Prophylaxis in the Prevention of Sexual HIV Transmission.</a> Arien, K.K., M. Venkatraj, J. Michiels, J. Joossens, K. Vereecken, P. Van der Veken, S. Abdellati, V. Cuylaerts, T. Crucitti, L. Heyndrickx, J. Heeres, K. Augustyns, P.J. Lewi, and G. Vanham. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23645585].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23330876">Probing a Chemical Space for Fragmental Topology-activity Landscapes (FRAGTAL): Application for Diketo Acid and Catechol HIV Integrase Inhibitor Offspring Fragments.</a> Bak, A., T. Magdziarz, A. Kurczyk, K. Serafin, and J. Polanski. Combinatorial Chemistry &amp; High Throughput Screening, 2013. 16(4): p. 274-287. PMID[23330876].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23650275">Discovery of Piperidine-linked Pyridine Analogues as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Chen, X., Y. Li, S. Ding, J. Balzarini, C. Pannecouque, E. De Clercq, H. Liu, and X. Liu. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23650275].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23633402">Prophylactic Efficacy of Oral Emtricitabine (FTC) and Tenofovir Disoproxil Fumarate (TDF) Combination against a Tenofovir-resistant SHIV Containing the K65R Mutation in Macaques.</a> Cong, M.E., J. Mitchell, E. Sweeney, S. Bachman, D.L. Hanson, W. Heneine, and J.G. Garcia-Lerma. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23633402].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23612005">Synergistic Activity of Carbosilane Dendrimers in Combination with Maraviroc against HIV in Vitro.</a> Cordoba, E.V., E. Arnaiz, F.J. De La Mata, R. Gomez, M. Leal, M. Pion, and M.A. Munoz-Fernandez. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[23612005].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23566104">Elucidating a Relationship between Conformational Sampling and Drug Resistance in HIV-1 Protease.</a> de Vera, I.M., A.N. Smith, M.C. Dancel, X. Huang, B.M. Dunn, and G.E. Fanucci. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23566104].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23454677">Development and Validation of a Stability Indicating Method for Seven Novel Derivatives of Lamivudine with anti-HIV and anti-HBV Activity in Simulated Gastric and Intestinal Fluids.</a> Gualdesi, M.S., J. Esteve-Romero, M.C. Brinon, and M.A. Raviolo. Journal of Pharmaceutical and Biomedical Analysis, 2013. 78-79: p. 52-56. PMID[23454677].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />
    

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23649937">Cationic Host Defence Peptides: Potential as Antiviral Therapeutics.</a> Gwyer Findlay, E., S.M. Currie, and D.J. Davidson. BioDrugs: Clinical Immunotherapeutics, Biopharmaceuticals and Gene Therapy, 2013. <b>[Epub ahead of print]</b>. PMID[23649937].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23545107">Nucleotide Competing Reverse Transcriptase Inhibitors: Discovery of a Series of Non-basic Benzofurano[3,2-d]pyrimidin-2-one Derived Inhibitors.</a> James, C.A., P. Deroy, M. Duplessis, P.J. Edwards, T. Halmos, J. Minville, L. Morency, S. Morin, B. Simoneau, M. Tremblay, R. Bethell, M. Cordingley, J. Duan, L. Lamorte, A. Pelletier, D. Rajotte, P. Salois, S. Tremblay, and C.F. Sturino. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2781-2786. PMID[23545107].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23616384">Self-delivery Multifunctional anti-HIV Hydrogels for Sustained Release.</a> Li, J., X. Li, Y. Kuang, Y. Gao, X. Du, J. Shi, and B. Xu. Advanced Healthcare Materials, 2013. <b>[Epub ahead of print]</b>. PMID[23616384].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23628626">Daphnane Diterpenoids from the Stems of Trigonostemon lii and Their anti-HIV-1 Activity.</a> Li, S.F., Y. Zhang, N. Huang, Y.T. Zheng, Y.T. Di, S.L. Li, Y.Y. Cheng, H.P. He, and X.J. Hao. Phytochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23628626].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23639006">Anti-HIV and NO Production Inhibition Activities of Epi-aleuritolic acid Derivatives.</a> Liu, J.B., Y. Zhang, B.S. Cui, Y.L. Cao, S.P. Yuan, Y. Guo, Q. Hou, and S. Li. Journal of Asian Natural Products Research, 2013. <b>[Epub ahead of print]</b>. PMID[23639006].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23631411">Exploiting Drug-resistant Enzymes as Tools to Identify Thienopyrimidinone Inhibitors of Human Immunodeficiency Virus Reverse Transcriptase-associated Ribonuclease H.</a> Masaoka, T., S. Chung, P. Caboni, J. Rausch, J.A. Wilson, H. Taskent-Sezgin, J.A. Beutler, G. Tocco, and S.F. Le Grice. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23631411].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23467699">Scaffold Optimization in Discontinuous Epitope Containing Protein Mimics of gp120 Using Smart Libraries.</a> Mulder, G.E., H.L. Quarles van Ufford, J. van Ameijde, A.J. Brouwer, J.A. Kruijtzer, and R.M. Liskamp. Organic &amp; Biomolecular Chemistry, 2013. 11(16): p. 2676-2684. PMID[23467699]. <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23535561">CD4 Mimics as HIV Entry Inhibitors: Lead Optimization Studies of the Aromatic Substituents.</a> Narumi, T., H. Arai, K. Yoshimura, S. Harada, Y. Hirota, N. Ohashi, C. Hashimoto, W. Nomura, S. Matsushita, and H. Tamamura. Bioorganic &amp; Medicinal Chemistry, 2013. 21(9): p. 2518-2526. PMID[23535561].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23403358">Hyptis verticillata Jacq: A Review of Its Traditional Uses, Phytochemistry, Pharmacology and Toxicology.</a> Picking, D., R. Delgoda, I. Boulogne, and S. Mitchell. Journal of Ethnopharmacology, 2013. 147(1): p. 16-41. PMID[23403358].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />
    

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23511023">Identification of Benzofurano[3,2-d]pyrimidin-2-ones, a New Series of HIV-1 Nucleotide-competing Reverse Transcriptase Inhibitors.</a> Tremblay, M., R.C. Bethell, M.G. Cordingley, P. Deroy, J. Duan, M. Duplessis, P.J. Edwards, A.M. Faucher, T. Halmos, C.A. James, C. Kuhn, J.E. Lacoste, L. Lamorte, S.R. Laplante, E. Malenfant, J. Minville, L. Morency, S. Morin, D. Rajotte, P. Salois, B. Simoneau, S. Tremblay, and C.F. Sturino. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(9): p. 2775-2780. PMID[23511023].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23480609">Burkholderia oklahomensis Agglutinin Is a Canonical Two-domain OAA-family Lectin: Structures, Carbohydrate Binding and anti-HIV Activity.</a> Whitley, M.J., W. Furey, S. Kollipara, and A.M. Gronenborn. The FEBS Journal, 2013. 280(9): p. 2056-2067. PMID[23480609].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23524106">A Convenient Synthesis of N-linked Diglycose Derivatives Based on One-pot Tandem Staudinger/aza-Wittig/reduction and Biological Evaluation.</a> Zhang, P., Y. Li, M. Liu, Y. Wang, C. Li, D. Ma, H. Chen, K. Wang, X. Li, and J. Zhang. Carbohydrate Research, 2013. 372: p. 15-22. PMID[23524106].
    <br />
    <b>[PubMed]</b>. HIV_0426-050913.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316401700008">3D-QSAR, CoMFA, and CoMSIA of New Phenyloxazolidinones Derivatives as Potent HIV-1 Protease Inhibitors.</a> Abedi, H., H. Ebrahimzadeh, and J.B. Ghasemi. Structural Chemistry, 2013. 24(2): p. 433-444. ISI<b>[</b>000316401700008].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316430100044">Phenylethanoids from the Flowers of Rosa rugosa and Their Biological Activities.</a> Gao, X.M., L.D. Shu, L.Y. Yang, Y.Q. Shen, Y.J. Zhang, and Q.F. Hu. Bulletin of the Korean Chemical Society, 2013. 34(1): p. 246-248. ISI<b>[</b>000316430100044].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317026600007">Aurones and Isoaurones from the Flowers of Rosa damascena and Their Biological Activities.</a> Gao, X.M., L.Y. Yang, X.Z. Huang, L.D. Shu, Y.Q. Shen, Q.F. Hu, and Z.Y. Chen. Heterocycles, 2013. 87(3): p. 583-589. ISI<b>[</b>000317026600007].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316243600008">Synthesis, Stereochemistry, Structural Classification, and Chemical Reactivity of Natural Pterocarpans.</a> Goel, A., A. Kumar, and A. Raghuvanshi. Chemical Reviews, 2013. 113(3): p. 1614-1640. ISI<b>[</b>000316243600008].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316996700002">Substituted Benzothiazoles: Synthesis and Medicinal Characteristics.</a> Henary, M., S. Paranjpe, and E.A. Owens. Heterocyclic Communications, 2013. 19(2): p. 89-99. ISI<b>[</b>000316996700002].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317039700001">Nelfinavir Augments Proteasome Inhibition by Bortezomib in Myeloma Cells and Overcomes Bortezomib and Carfilzomib Resistance.</a> Kraus, M., J. Bader, H. Overkleeft, and C. Driessen. Blood Cancer Journal, 2013. 3. ISI<b>[</b>000317039700001].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316868900017">5 &#39;-Nor Carbocyclic Nucleosides: Unusual Nonnucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Matyugina, E.S., V.T. Valuev-Elliston, D.A. Babkov, M.S. Novikov, A.V. Ivanov, S.N. Kochetkov, J. Balzarini, K.L. Seley-Radtke, and A.L. Khandazhinskaya. MedChemComm, 2013. 4(4): p. 741-748. ISI<b>[</b>000316868900017].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316868900012">Discovery of an HIV Integrase Inhibitor with an Excellent Resistance Profile.</a> Pryde, D.C., R. Webster, S.L. Butler, E.J. Murray, K. Whitby, C. Pickford, M. Westby, M.J. Palmer, D.J. Bull, H. Vuong, D.C. Blakemore, D. Stead, C. Ashcroft, I. Gardner, C. Bru, W.Y. Cheung, I.O. Roberts, J. Morton, and R.A. Bissell. MedChemComm, 2013. 4(4): p. 709-719. ISI<b>[</b>000316868900012].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316677600024">Computational Study and Peptide Inhibitors Design for the CDK9-cyclin T1 Complex.</a> Randjelovic, J., S. Eric, and V. Savic. Journal of Molecular Modeling, 2013. 19(4): p. 1711-1725. ISI<b>[</b>000316677600024].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316531400006">Nerium oleander Derived Cardiac Glycoside Oleandrin Is a Novel Inhibitor of HIV Infectivity.</a> Singh, S., S. Shenoy, P.N. Nehete, P.Y. Yang, B. Nehete, D. Fontenot, G.J. Yang, R.A. Newman, and K.J. Sastry. Fitoterapia, 2013. 84: p. 32-39. ISI<b>[</b>000316531400006].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316868900016">Small Organic Molecules Targeting PCAF Bromodomain as Potent Inhibitors of HIV-1 Replication.</a> Wang, Q., R.R. Wang, B.Q. Zhang, S. Zhang, Y.T. Zheng, and Z.Y. Wang. MedChemComm, 2013. 4(4): p. 737-740. ISI<b>[</b>000316868900016].
    <br />
    <b>[WOS]</b>. HIV_0426-050913.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130307&amp;CC=WO&amp;NR=2013033059A1&amp;KC=A1">Preparation of Spiro Bicyclic Diamine Derivatives as HIV Attachment Inhibitors.</a> Wang, T., Z. Zhang, D.R. Langley, J.F. Kadow, and N.A. Meanwell. Patent. 2013. 2012-US52603 2013033059: 90pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130321&amp;CC=US&amp;NR=2013072465A1&amp;KC=A1">Novel Betulinic acid Derivatives with Antiviral Activity.</a> Liu, Z., N.A. Meanwell, and A. Regueiro-Ren. Patent. 2013. 2012-623165 20130072465: 26pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130306&amp;CC=CN&amp;NR=102952062A&amp;KC=A">Substituted-benzoheterocycle Derivatives, Preparation, and Application for Preparation of Antiviral or Antineoplastic Drugs.</a> Li, Z., X. Ji, S. Xue, P. Tao, and Z. Wang. Patent. 2013. 2012-10283171 102952062: 88pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130307&amp;CC=WO&amp;NR=2013033003A1&amp;KC=A1">Preparation of 5-(Thiazol-2-yl)-3,4-dihydropyrimidones as HIV Replication Inhibitors Useful in the Treatment of HIV Infections.</a> Heil, M.L., N.D.P. Cosford, N. Pagano, and P. Teriete. Patent. 2013. 2012-US52482 2013033003: 81pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130328&amp;CC=WO&amp;NR=2013043553A1&amp;KC=A1">Preparation of Pyrrolopyridinone Compounds and Methods for Treating HIV.</a> Haydar, S.N., B.A. Johns, and E.J. Velthuisen. Patent. 2013. 2012-US55838 2013043553: 180pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130403&amp;CC=CN&amp;NR=103012099A&amp;KC=A">Fluorenone-like Compound, and Preparation Method and Application Thereof.</a> Gao, X., Q. Hu, Y. Li, G. Li, and X. Huang. Patent. 2013. 2013-10014162 103012099: 17pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">38. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130403&amp;CC=CN&amp;NR=103012118A&amp;KC=A">Lignans Compounds, Their Preparation Method and Application in Preparation of anti-AIDS Drugs.</a> Gao, X. and Q. Hu. Patent. 2013. 2013-10014125 103012118: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130314&amp;CC=WO&amp;NR=2013036846A2&amp;KC=A2">N4 Derivatives of Deoxycytidine Prodrugs as anti-HIV and Antitumor Agents.</a> Fromhold, M. and J. Partridge. Patent. 2013. 2012-US54277 2013036846: 42pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130314&amp;CC=WO&amp;NR=2013036676A1&amp;KC=A1">HIV Inhibitors.</a> Debnath, A.K., F. Curreli, P.D. Kwong, and Y.D. Kwong. Patent. 2013. 2012-US54009 2013036676: 102pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130426&amp;CC=FR&amp;NR=2981650A1&amp;KC=A1">Purine Nucleoside Analog Inhibitors of Retroviral Reverse Transcriptase for the Treatment of a Viral Infection and Mutations Associated with Sensitivity to These Inhibitors.</a> Calvez, V., A.G. Marcelin, C. Soulie, M. Etheve Quelquejeu, and M. Sollogoub. Patent. 2013. 2011-59622 2981650: 21pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130313&amp;CC=EP&amp;NR=2567965A1&amp;KC=A1">Preparation of Derivatives of 3-O-(3&#39;,3&#39;-Dimethylsuccinyl)-betulinic acid for the Treatment of HIV.</a> Bouaziz, S., P. Coric, P. Boulanger, S.-S. Hong, and S. Turcaud. Patent. 2013. 2011-306138 2567965: 32pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>

    <br />

    <p class="plaintext">43. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130418&amp;CC=WO&amp;NR=2013056003A2&amp;KC=A2">Catechol Diethers as Potent anti-HIV Agents.</a> Patent. 2013. 2012-US59886 2013056003: 106pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0426-050913.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
