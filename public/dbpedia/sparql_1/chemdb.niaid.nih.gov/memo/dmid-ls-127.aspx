

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-127.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v+p+Yz2fdY9VTvedBiLAOcxjzKXO4LOO3u+22fbwaChMODTpS+Dwk/QM0x6lK5DO0V0OGXn14lEtYKQbYm3nsBwL3HHzzmHzBmXOfvNYQg43DPy51xRlXghazcI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E36907C3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-127-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58918   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Human cytomegalovirus infection alters the substrate specificities and rapamycin sensitivities of raptor- and rictor-containing complexes</p>

    <p>          Kudchodkar, SB, Yu, Y, Maguire, TG, and Alwine, JC</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16959881&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16959881&amp;dopt=abstract</a> </p><br />

    <p>2.     58919   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Efficient Large-Scale Synthesis of BILN 2061, a Potent HCV Protease Inhibitor, by a Convergent Approach Based on Ring-Closing Metathesis</p>

    <p>          Yee, NK, Farina, V, Houpis, IN, Haddad, N, Frutos, RP, Gallou, F, Wang, XJ, Wei, X, Simpson, RD, Feng, X, Fuchs, V, Xu, Y, Tan, J, Zhang, L, Xu, J, Smith-Keenan, LL, Vitous, J, Ridges, MD, Spinelli, EM, Johnson, M, Donsbach, K, Nicola, T, Brenner, M, Winter, E, Kreye, P, and Samstag, W</p>

    <p>          J Org Chem <b>2006</b>.  71(19): 7133-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16958506&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16958506&amp;dopt=abstract</a> </p><br />

    <p>3.     58920   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          N-halogenated and N,N-dihalogenated amino acids in combination with hypohalous acids for use as antimicrobial agents</p>

    <p>          Najafi, Ramin, Bassiri, Mansour, Wang, Lu, and Khosrovi, Behzad</p>

    <p>          PATENT:  WO <b>2006081392</b>  ISSUE DATE:  20060803</p>

    <p>          APPLICATION: 2006  PP: 72pp.</p>

    <p>          ASSIGNEE:  (Novacal Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58921   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Nitric oxide and peroxynitrite have different antiviral effects against hantavirus replication and free mature virions</p>

    <p>          Klingstrom, J, Akerstrom, S, Hardestam, J, Stoltz, M, Simon, M, Falk, KI, Mirazimi, A, Rottenberg, M, and Lundkvist, A</p>

    <p>          Eur J Immunol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955520&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16955520&amp;dopt=abstract</a> </p><br />

    <p>5.     58922   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Antiviral effect of octyl gallate against DNA and RNA viruses</p>

    <p>          Uozaki, M, Yamasaki, H, Katsuyama, Y, Higuchi, M, Higuti, T, and Koyama, AH</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16950523&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16950523&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58923   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Natural products that reduce rotavirus infectivity identified by a cell-based moderate-throughput screening assay</p>

    <p>          Shaneyfelt, ME, Burke, AD, Graff, JW, Jutila, MA, and Hardy, ME</p>

    <p>          Virol J <b>2006</b>.  3(1): 68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948846&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948846&amp;dopt=abstract</a> </p><br />

    <p>7.     58924   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Molecular modeling studies of peptide drug candidates against SARS</p>

    <p>          Zhang, R, Wei, DQ, Du, QS, and Chou, KC</p>

    <p>          Med Chem <b>2006</b>.  2(3): 309-14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948478&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948478&amp;dopt=abstract</a> </p><br />

    <p>8.     58925   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Antiviral Targets of a Chromene Derivative from Sargassum micracanthum in the Replication of Human Cytomegalovirus</p>

    <p>          Hayashi, K, Mori, J, Saito, H, and Hayashi, T</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(9): 1843-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946496&amp;dopt=abstract</a> </p><br />

    <p>9.     58926   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Synthesis of pyrrolo[2,3-d]pyrimidine nucleoside derivatives as potential anti-HCV agents</p>

    <p>          Varaprasad, CV, Ramasamy, KS, Girardet, JL, Gunic, E, Lai, V, Zhong, W, An, H, and Hong, Z</p>

    <p>          Bioorg Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945403&amp;dopt=abstract</a> </p><br />

    <p>10.   58927   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Inhibition of the replication of hepatitis B virus in vitro by emodin</p>

    <p>          Shuangsuo, D, Zhengguo, Z, Yunru, C, Xin, Z, Baofeng, W, Lichao, Y, and Yan&#39;an, C</p>

    <p>          Med Sci Monit <b>2006</b>.  12(9): BR302-306</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940925&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940925&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58928   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          NIM811, a Cyclophilin Inhibitor, Exhibits Potent In Vitro Activity against Hepatitis C Virus Alone or in Combination with Alpha Interferon</p>

    <p>          Ma, S, Boerner, JE, Tiongyip, C, Weidmann, B, Ryder, NS, Cooreman, MP, and Lin, K</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(9): 2976-82</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940091&amp;dopt=abstract</a> </p><br />

    <p>12.   58929   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Antiviral activity from medicinal mushrooms</p>

    <p>          Stamets, Paul</p>

    <p>          PATENT:  US <b>2006171958</b>  ISSUE DATE:  20060803</p>

    <p>          APPLICATION: 2006-58722  PP: 14pp., Cont.-in-part of U.S. Ser. No. 145,679.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   58930   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Natural variation can significantly alter sensitivity to oseltamivir of Influenza A(H5N1) viruses</p>

    <p>          Rameix-Welti, MA, Agou, F, Buchy, P, Mardy, S, Aubin, JT, Veron, M, van, der Werf S, and Naffakh, N</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940075&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16940075&amp;dopt=abstract</a> </p><br />

    <p>14.   58931   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Synergistic effect of a novel oxymatrine-baicalin combination against hepatitis B virus replication, alpha smooth muscle actin expression and type I collagen synthesis in vitro</p>

    <p>          Cheng, Y, Ping, J, Xu, HD, Fu, HJ, and Zhou, ZH</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(32): 5153-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16937525&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16937525&amp;dopt=abstract</a> </p><br />

    <p>15.   58932   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Structure-based design of a novel thiazolone scaffold as HCV NS5B polymerase allosteric inhibitors</p>

    <p>          Yan, S, Appleby, T, Larson, G, Wu, JZ, Hamatake, R, Hong, Z, and Yao, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16934455&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16934455&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   58933   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Temperature sensitive influenza A virus genome replication results from low thermal stability of polymerase-cRNA complexes</p>

    <p>          Dalton, RM, Mullin, AE, Amorim, MJ, Medcalf, E, Tiley, LS, and Digard, P</p>

    <p>          Virol J <b>2006</b>.  3(1): 58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16934156&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16934156&amp;dopt=abstract</a> </p><br />

    <p>17.   58934   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          CD4 T cell-mediated protection from lethal influenza: perforin and antibody-mediated mechanisms give a one-two punch</p>

    <p>          Brown, DM, Dilzer, AM, Meents, DL, and Swain, SL</p>

    <p>          J Immunol <b>2006</b>.  177(5): 2888-98</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920924&amp;dopt=abstract</a> </p><br />

    <p>18.   58935   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Improving anti-hepatitis C virus therapy</p>

    <p>          Pol, S and Mallet, VO</p>

    <p>          Expert Opin Biol Ther <b>2006</b>.  6(9): 923-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16918259&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16918259&amp;dopt=abstract</a> </p><br />

    <p>19.   58936   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Synthesis of 2-bromo-6-octylamino-1-b-D-ribofuranosylimidazo[4,5-e][1,3]diazepine-4,8-dione: A Ring-expanded nucleoside analog with a therapeutic promise against Flaviviridae</p>

    <p>          Zhang, Peng, Zhang, Ning, and Hosmane, Ramachandra S</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-231</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   58937   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Importance of neuraminidase active-site residues to the neuraminidase inhibitor resistance of influenza viruses</p>

    <p>          Yen, HL, Hoffmann, E, Taylor, G, Scholtissek, C, Monto, AS, Webster, RG, and Govorkova, EA</p>

    <p>          J Virol <b>2006</b>.  80(17): 8787-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16912325&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16912325&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   58938   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          West Nile virus 5&#39;-cap structure is formed by sequential guanine N-7 and ribose 2&#39;-O methylations by nonstructural protein 5</p>

    <p>          Ray, D, Shah, A, Tilgner, M, Guo, Y, Zhao, Y, Dong, H, Deas, TS, Zhou, Y, Li, H, and Shi, PY</p>

    <p>          J Virol <b>2006</b>.  80(17): 8362-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16912287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16912287&amp;dopt=abstract</a> </p><br />

    <p>22.   58939   DMID-LS-127; PUBMED-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Corneal toxicity of cell-penetrating peptides that inhibit Herpes simplex virus entry</p>

    <p>          Akkarawongsa, R, Cullinan, AE, Zinkel, A, Clarin, J, and Brandt, CR</p>

    <p>          J Ocul Pharmacol Ther <b>2006</b>.  22(4): 279-89</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16910869&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16910869&amp;dopt=abstract</a> </p><br />

    <p>23.   58940   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Anticancer and antiviral activities of organotin polyether amines derived from the antiviral acyclovir</p>

    <p>          Roner, Michael R, Carraher, Charles E, Sabir, Theodore S, Shahi, Kim, Roehr, Joanne L, and Bassett, Kelly D</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: PMSE-303</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   58941   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Isoindolylureas a new group of antiviral compounds</p>

    <p>          Verma, Manjusha and Singh, Krishna Nand</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-226</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   58942   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Pyrroloquinolines as human cytomegalovirus polymerase inhibitors: SAR of arylethanolamine side-chain at C-8</p>

    <p>          Nair, Sajiv K, Schultz, Brenda L, Nieman, James A, Heasley, Steven E, Vaillancourt, Valerie A, and Brammer, Carolyn A</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   58943   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Detection of influenza viruses resistant to neuraminidase inhibitors in global surveillance during the first 3 years of their use</p>

    <p>          Monto Arnold S, McKimm-Breschkin Jennifer L, Macken Catherine, Hampson Alan W, Hay Alan, Klimov Alexander, Tashiro Masato, Webster Robert G, Aymard Michelle, Hayden Frederick G, and Zambon Maria</p>

    <p>          Antimicrobial agents and chemotherapy <b>2006</b>.  50(7): 2395-402.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>27.   58944   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Practical Synthesis of 2-(2-Isopropylaminothiazol-4-Yl)-7-Methoxy-1h-Quinolin-4-One: Key Intermediate for the Synthesis of Potent Hcvns3 Protease Inhibitor Biln 2061</p>

    <p>          Frutos, R. <i>et al.</i></p>

    <p>          Synthesis-Stuttgart <b>2006</b>.(15): 2563-2567</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239924200017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239924200017</a> </p><br />

    <p>28.   58945   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          The Structure of the Endoribonuclease Xendou: From Small Nucleolar Rna Processing to Severe Acute Respiratory Syndrome Coronavirus Replication</p>

    <p>          Renzi, F. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2006</b>.  103(33): 12365-12370</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239867500031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239867500031</a> </p><br />

    <p>29.   58946   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Inhibition of Viral Proteases by Zingiberaceae Extracts and Flavones Isolated From Kaempferia Parviflora</p>

    <p>          Sookkongwaree, K. <i>et al.</i></p>

    <p>          Pharmazie <b>2006</b>.  61(8): 717-721</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239907900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239907900016</a> </p><br />

    <p>30.   58947   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          8-Azabicyclo[3.2.1]octane derivatives with an activity on chemokine CCR5 receptors and their preparation, pharmaceutical compositions and use for treatment of inflammatory diseases and infection caused by HIV and genetically related retroviruses</p>

    <p>          Pryde, David Cameron and Stupple, Paul Anthony</p>

    <p>          PATENT:  WO <b>2006067584</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 67 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   58948   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          A 5 &#39; Rna Element Promotes Dengue Virus Rna Synthesis on a Circular Genome</p>

    <p>          Filomatori, C. <i>et al.</i></p>

    <p>          Genes &amp; Development <b>2006</b>.  20(16): 2238-2249</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239790600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239790600008</a> </p><br />

    <p>32.   58949   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Bk Virus Infection in Transplant Recipients: an Overview and Update</p>

    <p>          Randhawa, P. and Brennan, D.</p>

    <p>          American Journal of Transplantation <b>2006</b>.  6(9): 2000-2005</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239617800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239617800003</a> </p><br />
    <br clear="all">

    <p>33.   58950   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Inhibition of feline (FIPV) and human (SARS) coronavirus by semisynthetic derivatives of glycopeptide antibiotics</p>

    <p>          Balzarini, Jan, Keyaerts, Els, Vijgen, Leen, Egberink, Herman, De Clercq, Erik, Van Ranst, Marc, Printsevskaya, Svetlana S, Olsufyeva, Eugenia N, Solovieva, Svetlana E, and Preobrazhenskaya, Maria N</p>

    <p>          Antiviral Research <b>2006</b>.  72(1): 20-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   58951   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Utilization of Ribozymes as Antiviral Agents</p>

    <p>          Benitez-Hess, M. and Alvarez-Salas, L.</p>

    <p>          Letters in Drug Design &amp; Discovery <b>2006</b>.  3(6): 390-404</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400005</a> </p><br />

    <p>35.   58952   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Antiviral Defense: Interferons and Beyond</p>

    <p>          Stetson, D. and Medzhitov, R.</p>

    <p>          Journal of Experimental Medicine <b>2006</b>.  203(8): 1837-1841</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239603600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239603600003</a> </p><br />

    <p>36.   58953   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Antiviral effects of saikosaponins on human coronavirus 229E in vitro</p>

    <p>          Cheng, Pei-Win, Ng, Lean-Teik, Chiang, Lien-Chai, and Lin, Chun-Ching</p>

    <p>          Clinical and Experimental Pharmacology and Physiology <b>2006</b>.  33(7): 612-616</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   58954   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          The Anti-Viral Effects of Interferon-Alpha on Varicella-Zoster Virus (Vzv).</p>

    <p>          Ku, C., Schaap, A., and Arvin, A.</p>

    <p>          Clinical Immunology <b>2006</b>.  119: S181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237924300482">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237924300482</a> </p><br />

    <p>38.   58955   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Alkoxyakl Esters of (S)-9-[3-Hydroxy-2-(Phosphonomethoxy)Propyl] Adenine Are Potent Inhibitors of the Replication of Wild-Type and Drug-Resistant Human Immunodeficiency Virus Type 1 in Vitro</p>

    <p>          Hostetler, K. <i> et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2006</b>.  50(8): 2857-2859</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400040</a> </p><br />

    <p>39.   58956   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Furin inhibitors and a-defensins for the treatment or prevention of papillomavirus infection</p>

    <p>          Day, Patricia M, Richards, Rebecca, Buck, Christopher, Schiller, John T, and Lowy, Douglas R</p>

    <p>          PATENT:  WO <b>2006084131</b>  ISSUE DATE:  20060810</p>

    <p>          APPLICATION: 2006  PP: 94pp.</p>

    <p>          ASSIGNEE:  (The Government of the United States of America, As Represented by the Secretary Department of Health and Human Services USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>40.   58957   DMID-LS-127; SCIFINDER-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Carrageenan is a potent inhibitor of papillomavirus infection</p>

    <p>          Buck, Christopher B, Thompson, Cynthia D, Roberts, Jeffrey N, Muller, Martin, Lowy, Douglas R, and Schiller, John T</p>

    <p>          PLoS Pathogens  <b>2006</b>.  2(7): 671-680</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   58958   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Role of T Cells, Cytokines and Antibody in Dengue Fever and Dengue Haemorrhagic Fever</p>

    <p>          Fink, J., Gu, F., and Vasudevan, S.</p>

    <p>          Reviews in Medical Virology <b>2006</b>.  16(4): 263-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239523800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239523800005</a> </p><br />

    <p>42.   58959   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Decision Analysis in Planning for a Polio Outbreak in the United States</p>

    <p>          Jenkins, P. and Modlin, J.</p>

    <p>          Pediatrics <b>2006</b>.  118(2): 611-618</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239440600022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239440600022</a> </p><br />

    <p>43.   58960   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Synthesis of 2-Bromomethyl-3-Hydroxy-2-Hydroxymethyl-Propyl Pyrimidine and Theophylline Nucleosides Under Microwave Irradiation. Evaluation of Their Activity Against Hepatitis B Virus</p>

    <p>          El Ashry, E. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2006</b>.  25(8): 925-939</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239453200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239453200009</a> </p><br />

    <p>44.   58961   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          A Nutritional Supplement Formula for Influenza a (H5n1) Infection in Humans</p>

    <p>          Friel, H. and Lederman, H.</p>

    <p>          Medical Hypotheses <b>2006</b>.  67(3): 578-587</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239465700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239465700023</a> </p><br />

    <p>45.   58962   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          The Hcvns3 Protease Inhibitor Sch 503034 in Combination With Peg-Ifn Alpha-2b in the Treatment of Hcv-1peg-Ifn Alpha-2b Non-Responders: Antiviral Activity and Hcv Variant Analysis</p>

    <p>          Zeuzem, S. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100079">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100079</a> </p><br />

    <p>46.   58963   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Telbivudine Displays Consistent Antiviral Efficacy Across Patient Subgroups for the Treatment of Chronic Hepatitis B: Results From the Globe Study</p>

    <p>          Thongsawat, S. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100111">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100111</a> </p><br />
    <br clear="all">

    <p>47.   58964   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Inhibitory Effects of 10-23 Dnazyme on the Expression of Hepatitis B Virus</p>

    <p>          Ren, N. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S150</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100396">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100396</a> </p><br />

    <p>48.   58965   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Emergence of Dual-Resistance Hbv Mutations to Adefovir-Dipivoxil and Lamivudine in a Hepatitis B Patient With Combination Therapy for Adefovir Resistance</p>

    <p>          Kwon, S. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S273-S274</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100741">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100741</a> </p><br />

    <p>49.   58966   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Is Combination Antiviral Therapy for Cmv Superior to Monotherapy?</p>

    <p>          Drew, W.</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  35(4): 485-488</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239545300023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239545300023</a> </p><br />

    <p>50.   58967   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Early Viral Response to Cpg 10101, a New Investigational Antiviral Tlr9 Agonist, in Combination With Pegylated Interferon and/or Ribavirin, in Prior Relapse Hcv Responders: Preliminary Report of a Randomized Phase 1 Study</p>

    <p>          Mchutchison, J. <i>et al.</i></p>

    <p>          Gastroenterology <b>2006</b>.  130(4): A784</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961706056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961706056</a> </p><br />

    <p>51.   58968   DMID-LS-127; WOS-DMID-9/11/2006</p>

    <p class="memofmt1-2">          Exogenous Nitric Oxide Inhibits Crimean Congo Hemorrhagic Fever Virus</p>

    <p>          Simon, M. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  120(1-2): 184-190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800020</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
