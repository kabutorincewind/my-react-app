

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-08-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lBaZboqlt/VffQx1vjB3r4f2lvGHI5CokNOM/J1YUWSLFVTxjnRIZCVnWwpqQd99dWSznwRGfrUfDswJrY3iCMI/lkBw52c40J85UlFqX8sS4NWboQpR+YUm43Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DD20C164" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 15 - August 28, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25019997">Inhibition of Hepatitis B Virus Replication by SAMHD1.</a> Chen, Z., M. Zhu, X. Pan, Y. Zhu, H. Yan, T. Jiang, Y. Shen, X. Dong, N. Zheng, J. Lu, S. Ying, and Y. Shen. Biochemical and Biophysical Research Communications, 2014. 450(4): p. 1462-1468. PMID[25019997].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24965018">In Silico Analysis and Experimental Validation of Azelastine Hydrochloride (N4) Targeting Sodium Taurocholate Co-transporting Polypeptide (NTCP) in HBV Therapy.</a> Fu, L.L., J. Liu, Y. Chen, F.T. Wang, X. Wen, H.Q. Liu, M.Y. Wang, L. Ouyang, J. Huang, J.K. Bao, and Y.Q. Wei. Cell Proliferation, 2014. 47(4): p. 326-335. PMID[24965018].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25137139">The CRISPR/Cas9 System Facilitates Clearance of the Intrahepatic HBV Templates in vivo.</a> Lin, S.R., H.C. Yang, Y.T. Kuo, C.J. Liu, T.Y. Yang, K.C. Sung, Y.Y. Lin, H.Y. Wang, C.C. Wang, Y.C. Shen, F.Y. Wu, J.H. Kao, D.S. Chen, and P.J. Chen. Molecular Therapy - Nucleic Acids, 2014. 3: p. e186. PMID[25137139].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24986660">Design and Synthesis of Lactam-thiophene Carboxylic acids as Potent Hepatitis C Virus Polymerase Inhibitors.</a> Barnes-Seeman, D., C. Boiselle, C. Capacci-Daniel, R. Chopra, K. Hoffmaster, C.T. Jones, M. Kato, K. Lin, S. Ma, G. Pan, L. Shu, J. Wang, L. Whiteman, M. Xu, R. Zheng, and J. Fu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(16): p. 3979-3985. PMID[24986660].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0815-082814.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25065492">2-Alkyloxazoles as Potent and Selective PI4KIIIbeta Inhibitors Demonstrating Inhibition of HCV Replication.</a> Keaney, E.P., M. Connolly, M. Dobler, R. Karki, A. Honda, S. Sokup, S. Karur, S. Britt, A. Patnaik, P. Raman, L.G. Hamann, B. Wiedmann, and M.J. LaMarche. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(16): p. 3714-3718. PMID[25065492].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24893207">Suppression of Hepatitis C Virus Replication by Cyclin-dependent Kinase Inhibitors.</a> Munakata, T., M. Inada, Y. Tokunaga, T. Wakita, M. Kohara, and A. Nomoto. Antiviral Research, 2014. 108: p. 79-87. PMID[24893207].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24930829">2-Aminobenzoxazole Ligands of the Hepatitis C Virus Internal Ribosome Entry Site.</a> Rynearson, K.D., B. Charrette, C. Gabriel, J. Moreno, M.A. Boerneke, S.M. Dibrov, and T. Hermann. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3521-3525. PMID[24930829].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25117537">Tannic acid Modified Silver Nanoparticles Show Antiviral Activity in Herpes Simplex Virus Type 2 Infection.</a> Orlowski, P., E. Tomaszewska, M. Gniadek, P. Baska, J. Nowakowska, J. Sokolowska, Z. Nowak, M. Donten, G. Celichowski, J. Grobelny, and M. Krzyzowska. PLoS One, 2014. 9(8): p. e104113. PMID[25117537].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0815-082814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339695700029">Syntheses of 4 &#39;-Spirocyclic phosphono-nucleosides as Potential Inhibitors of Hepatitis C Virus NS5B Polyinerase.</a> Dang, Q., Z.B. Zhang, S.S. He, Y.H. Liu, T.Q. Chen, S. Bogen, V. Girijavallabhan, D.B. Olsen, and P.T. Meinke. Tetrahedron Letters, 2014. 55(31): p. 4407-4409. ISI[000339695700029].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339462800015">Broad-spectrum Allosteric Inhibition of Herpesvirus Proteases.</a> Gable, J.E., G.M. Lee, P. Jaishankar, B.R. Hearn, C.A. Waddling, A.R. Renslo, and C.S. Craik. Biochemistry, 2014. 53(28): p. 4648-4660. ISI[000339462800015].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339951200010">Targeted Delivery of siRNA against Hepatitis B Virus by preS1 Peptide Molecular Ligand.</a> Huang, W.J., X. Li, M. Yi, S.F. Zhu, and W.X. Chen. Hepatology Research, 2014. 44(8): p. 897-906. ISI[000339951200010].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0815-082814.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339653000006">Anti-Hepatitis C Virus Activity of Crude Extract and Fractions of Entada africana in Genotype 1b Replicon Systems.</a> Tietcheu, B.R.G., G. Sass, N.F. Njayou, P. Mkounga, G. Tiegs, and P.F. Moundipa. American Journal of Chinese Medicine, 2014. 42(4): p. 853-868. ISI[000339653000006].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0815-082814.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
