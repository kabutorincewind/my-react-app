

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-06-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8s4l4r8di9Qt9f9wLdkDoEgpyWE8p8+MXMsBdkDhgMf+vJrFmlb6ITINF2rnQoy9/TlUDX9kvG6jhXmB6O2GlVWtkMciqYJ6b24pyAcX7y/++l+BnNXjv9NqYG4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="33E42107" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: June 7 - June 20, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23761155">Tentative Epidemiological Cutoff Values for Fluconazole, Itraconazole, Posaconazole and Voriconazole for Six Candida Species Determined by the Colorimetric Sensititre YeastOne(R) Method.</a> Canton, E., J. Peman, C. Iniguez, D. Hervas, J.L. Lopez-Hontangas, C. Pina-Vaz, J.J. Camarena, I. Campos-Herrero, I. Garcia-Garcia, A.M. Garcia-Tapia, R. Guna, P. Merino, L. Perez Del Molino, C. Rubio, and A. Suarez. Journal of Clinical Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23761155].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23756682">Synergistic Antifungal Activity of KB425796-C in Combination with Micafungin against Aspergillus fumigatus and Its Efficacy in Murine Infection Models.</a> Kai, H., M. Yamashita, I. Nakamura, K. Yoshikawa, K. Nitta, M. Watanabe, N. Inamura, and A. Fujie. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23756682].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23592791">Rationally Designed Transmembrane Peptide Mimics of the Multidrug Transporter Protein Cdr1 Act as Antagonists to Selectively Block Drug Efflux and Chemosensitize Azole-resistant Clinical Isolates of Candida albicans.</a> Maurya, I.K., C.K. Thota, S.D. Verma, J. Sharma, M.K. Rawal, B. Ravikumar, S. Sen, N. Chauhan, A.M. Lynn, V.S. Chauhan, and R. Prasad. The Journal of Biological Chemistry, 2013. 288(23): p. 16775-16787. PMID[23592791].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23756964">Biological Evaluation of Alginate-based Hydrogels, with Antimicrobial Features by Ce(III) Incorporation, as Vehicles for a Bone Substitute.</a> Morais, D.S., M.A. Rodrigues, M.A. Lopes, M.J. Coelho, A.C. Mauricio, R. Gomes, I. Amorim, M.P. Ferraz, J.D. Santos, and C.M. Botelho. Journal of Materials Science: Materials in Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[23756964].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23749313">Preparative Thin Layer Chromatographic Separation Followed by Identification of Antifungal Compound in Cassia laevigata by RP-HPLC and GC-MS.</a> Panigrahi, G., R. Maheshwari, V. Kumar, J. Prakash, S. Kumar, and J. Prabakaran. Journal of the Science of Food and Agriculture, 2013. <b>[Epub ahead of print]</b>. PMID[23749313].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23765096">Activity of Four Antimicrobial Cocktails for Tissue Allograft Decontamination against Bacteria and Candida Spp. of Known Susceptibility at Different Temperatures.</a> Pitt, T.L., K. Tidey, A. Roy, S. Ancliff, R. Lomas, and C.P. McDonald. Cell and Tissue Banking, 2013. <b>[Epub ahead of print]</b>. PMID[23765096].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23744287">Antifungal Activity of the Lipopeptides Produced by Bacillus amyloliquefaciens anti-CA against Candida albicans Isolated from Clinic.</a> Song, B., Y.J. Rong, M.X. Zhao, and Z.M. Chi. Applied Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23744287].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23758765">Chemical Composition, Antimicrobial Properties and Toxicity Evaluation of the Essential Oil of Cupressus lusitanica Mill. Leaves from Cameroon.</a> Teke, G.N., K.N. Elisee, and K.J. Roger. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 130. PMID[23758765].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23624092">Nonlytic Fc-fused IL-7 Synergizes with Mtb32 DNA Vaccine to Enhance Antigen-specific T Cell Responses in a Therapeutic Model of Tuberculosis.</a> Ahn, S.S., B.Y. Jeon, S.J. Park, D.H. Choi, S.H. Ku, S.N. Cho, and Y.C. Sung. Vaccine, 2013. 31(27): p. 2884-2890. PMID[23624092].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23767698">Identification, Biochemical and Structural Evaluation of Species-specific Inhibitors against Type I Methionine Aminopeptidases.</a> Kishor, C., T. Arya, R. Reddi, X. Chen, V. Saddanapu, A.K. Marapaka, R. Gumpena, D. Ma, J.O. Liu, and A. Addlagatta. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23767698].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23749163">A Novel Combinatorial Biocatalytic Approach for Producing Antibacterial Compounds Effective against Mycobacterium tuberculosis (Tb).</a> McClay, K., B. Wan, Y. Wang, S. Cho, J. Yu, B. Santarsiero, S. Mehboob, M. Johnson, S. Franzblau, and R. Steffan. Applied Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23749163].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23761146">Rifampicin Resistance Missed in Automated Liquid Culture System for Mycobacterium tuberculosis with Specific rpoB-mutations.</a> Rigouts, L., M. Gumusboga, W.B. de Rijk, E. Nduwamahoro, C. Uwizeye, B. de Jong, and A. Van Deun. Journal of Clinical Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23761146].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23659859">Synthesis and Antimycobacterial Evaluation of N-Substituted 5-chloropyrazine-2-carboxamides.</a> Servusova, B., J. Vobickova, P. Paterova, V. Kubicek, J. Kunes, M. Dolezal, and J. Zitko. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(12): p. 3589-3591. PMID[23659859].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23776209">Identification of a Small Molecule with Activity against Drug-resistant and Persistent Tuberculosis.</a> Wang, F., D. Sambandan, R. Halder, J. Wang, S.M. Batt, B. Weinrick, I. Ahmad, P. Yang, Y. Zhang, J. Kim, M. Hassani, S. Huszar, C. Trefzer, Z. Ma, T. Kaneko, K.E. Mdluli, S. Franzblau, A.K. Chatterjee, K. Johnson, K. Mikusova, G.S. Besra, K. Futterer, W.R. Jacobs, Jr., and P.G. Schultz. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[23776209].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23770708">Antituberculosis Thiophenes Define a Requirement for Pks13 in Mycolic acid Biosynthesis.</a> Wilson, R., P. Kumar, V. Parashar, C. Vilcheze, R. Veyron-Churlet, J.S. Freundlich, S.W. Barnes, J.R. Walker, M.J. Szymonifka, E. Marchiano, S. Shenai, R. Colangeli, W.R. Jacobs, Jr., M.B. Neiditch, L. Kremer, and D. Alland. Nature Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23770708].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23744519">Fluoroquinolones for Treating Tuberculosis (Presumed Drug-sensitive).</a> Ziganshina, L.E., A.F. Titarenko, and G.R. Davies. Cochrane Database of Systematic Reviews (Online), 2013. 6: p. CD004795. PMID[23744519].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23754276">UV-triggered Affinity-capture Identifies Interactions between the Plasmodium falciparum Multidrug Resistance Protein 1 (PfMDR1) and Antimalarial Agents in Live Parasitized Cells.</a> Brunner, R., C.L. Ng, H. Aissaoui, M.H. Akabas, C. Boss, R. Brun, P.S. Callaghan, O. Corminboef, I.J. Frame, B. Heidmann, A. Le Bihan, P. Jeno, C. Mattheis, S. Moes, I.B. Muller, M. Paguio, P.D. Roepe, R. Siegrist, T. Voss, R.W. Welford, S. Wittlin, D.A. Fidock, D.A. Fidock, and C. Binkert. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23754276].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23749994">Antimalarial Activity of Cupredoxins: The Interaction of Plasmodium Merozoite Surface Protein 119 (MSP119) and Rusticyanin.</a> Cruz-Gallardo, I., I. Diaz-Moreno, A. Diaz-Quintana, A. Donaire, A. Velazquez-Campoy, R.D. Curd, K. Rangachari, B. Birdsall, A. Ramos, A.A. Holder, and M.A. De la Rosa. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23749994].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23758861">Anti-malarial Property of Steroidal Alkaloid Conessine Isolated from the Bark of Holarrhena antidysenterica.</a> Dua, V.K., G. Verma, B. Singh, A. Rajan, U. Bagai, D.D. Agarwal, N. Gupta, S. Kumar, and A. Rastogi. Malaria Journal, 2013. 12(1): p. 194. PMID[23758861].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23659857">Synthesis and Evaluation of Phenoxyoxazaphospholidine, Phenoxyoxazaphosphinane, and Benzodioxaphosphininamine sulfides and Related Compounds as Potential Anti-malarial Agents.</a> Mara, C., E. Dempsey, A. Bell, and J.W. Barlow. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(12): p. 3580-3583. PMID[23659857].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23758769">In Vitro Susceptibility to Quinine and Microsatellite Variations of the Plasmodium falciparum Na+/H+ Exchanger Transporter (Pfnhe-1) Gene in 393 Isolates from Dakar, Senegal.</a> Pascual, A., B. Fall, N. Wurtz, M. Fall, C. Camara, A. Nakoulima, E. Baret, B. Diatta, K.B. Fall, P.S. Mbaye, Y. Dieme, R. Bercion, H. Bogreau, S. Briolant, C. Rogier, B. Wade, and B. Pradines. Malaria Journal, 2013. 12: p. 189. PMID[23758769].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23771405">Comparative Analysis of Anti-toxoplasmic Activity of Antipsychotic Drugs and Valproate.</a> Fond, G., A. Macgregor, R. Tamouza, N. Hamdani, A. Meary, M. Leboyer, and J.F. Dubremetz. European Archives of Psychiatry and Clinical Neuroscience, 2013. <b>[Epub ahead of print]</b>. PMID[23771405].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0607-062013.</p> 

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318292400018">Synthesis, Thermal Characterization, and Antimicrobial Activity of Lanthanum, Cerium, and Thorium Complexes of Amino Acid Schiff Base Ligand.</a> Alghool, S., H.F. Abd El-Halim, M.S. Abd El-Sadek, I.S. Yahia, and L.A. Wahab. Journal of Thermal Analysis and Calorimetry, 2013. 112(2): p. 671-681. ISI[000318292400018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318341400027">Inhibition of Nuclear Factor-kappa B Activation Decreases Survival of Mycobacterium tuberculosis in Human Macrophages.</a> Bai, X.Y., N.E. Feldman, K. Chmura, A.R. Ovrutsky, W.L. Su, L. Griffin, D. Pyeon, M.T. McGibney, M.J. Strand, M. Numata, S. Murakami, L. Gaido, J.R. Honda, W.H. Kinney, R.E. Oberley-Deegan, D.R. Voelker, D.J. Ordway, and E.D. Chan. PloS One, 2013. 8(4): p. e61925. ISI[000318341400027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317681800012">Three Antimycobacterial Metabolites Identified from a Marine-derived Streptomyces Sp MS100061.</a> Chen, C.X., J. Wang, H. Guo, W.Y. Hou, N. Yang, B. Ren, M. Liu, H.Q. Dai, X.T. Liu, F.H. Song, and L.X. Zhang. Applied Microbiology and Biotechnology, 2013. 97(9): p. 3885-3892. ISI[000317681800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318687100012">A Rational Approach to Identify Inhibitors of Mycobacterium tuberculosis Enoyl Acyl Carrier Protein Reductase.</a> Chhabria, M.T., K.B. Parmar, and P.S. Brahmkshatriya. Current Pharmaceutical Design, 2013. 19(21): p. 3878-3883. ISI[000318687100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318750000003">A Small Molecule Inhibitor of Fungal Histone Acetyltransferase Rtt109.</a> da Rosa, J.L., V. Bajaj, J. Spoonamore, and P.D. Kaufman. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(10): p. 2853-2859. ISI[000318750000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318672800008">A Novel Mechanistic Approach to Identify New Antifungal Lead Compounds Based on Amphotericin B Molecular Architecture.</a> Ferdosian, M. and S. Sardari. Tropical Journal of Pharmaceutical Research, 2013. 12(2): p. 181-188. ISI[000318672800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318386000024">The Canadian Medicinal Plant Heracleum maximum Contains Antimycobacterial Diynes and Furanocoumarins.</a> O&#39;Neill, T., J.A. Johnson, D. Webster, and C.A. Gray. Journal of Ethnopharmacology, 2013. 147(1): p. 232-237. ISI[000318386000024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318677300062">ApiAP2 Transcription Factor Restricts Development of the Toxoplasma Tissue Cyst.</a> Radke, J.B., O. Lucas, E.K. De Silva, Y.F. Ma, W.J. Sullivan, L.M. Weiss, M. Llinas, and M.W. White. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(17): p. 6871-6876. ISI[000318677300062].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317907200017">Tetrahydropyrazolo[1,5-a]pyrimidine-3-carboxamide and N-Benzyl-6&#39;,7&#39;-dihydrospiro[piperidine-4,4&#39;-thieno[3,2-c]pyran] Analogues with Bactericidal Efficacy against Mycobacterium tuberculosis Targeting MmpL3.</a> Remuinan, M.J., E. Perez-Herran, J. Rullas, C. Alemparte, M. Martinez-Hoyos, D.J. Dow, J. Afari, N. Mehta, J. Esquivias, E. Jimenez, F. Ortega-Muro, M.T. Fraile-Gabaldon, V.L. Spivey, N.J. Loman, M.J. Pallen, C. Constantinidou, D.J. Minick, M. Cacho, M.J. Rebollo-Lopez, C. Gonzalez, V. Sousa, I. Angulo-Barturen, A. Mendoza-Losana, D. Barros, G.S. Besra, L. Ballell, and N. Cammack. Plos One, 2013. 8(4) : p. e60933. ISI[000317907200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0607-062013.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
