

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-01-30.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KGEmDwqz+cvewCjRGrHgFzWzCI315u9SMf166cGI0RagdjYPesi6xls2on2s1v5W/J70socVbGxnu2TuOxDaQFSHLYmfYnizTpgiNy4vSruYGKTHl1xHx0xrbXk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1AF4CD2F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 17, 2013 - January 30, 2014</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24438177">Anti-infective and Cytotoxic Properties of Bupleurum marginatum<i>.</i></a> Ashour, M.L., M.Z. El-Readi, R. Hamoud, S.Y. Eid, S.H. El Ahmady, E. Nibret, F. Herrmann, M. Youns, A. Tahrani, D. Kaufmann, and M. Wink. Chinese Medicine, 2014. 9(1): p. 4; PMID[24438177].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24432736">A New Inositol Triester from Taraxacum mongolicum<i>.</i></a> Liu, J., N. Zhang, and M. Liu. Natural Product Research, 2014. <b>[Epub ahead of print]</b>. PMID[24432736].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24342612">Evaluation and Identification of Hepatitis B Virus Entry Inhibitors Using HepG2 Cells Overexpressing a Membrane Transporter NTCP<i>.</i></a> Iwamoto, M., K. Watashi, S. Tsukuda, H.H. Aly, M. Fukasawa, A. Fujimoto, R. Suzuki, H. Aizaki, T. Ito, O. Koiwai, H. Kusuhara, and T. Wakita. Biochemical and Biophysical Research Communications, 2014. 443(3): p. 808-813; PMID[24342612].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24467709">Integrated Strategies for Identifying Leads That Target the NS3 Helicase of the Hepatitis C Virus<i>.</i></a> Laplante, S.R., A.K. Padyana, A. Abeywardane, P. Bonneau, M. Cartier, R. Coulombe, A. Jakalian, J. Wildeson-Jones, X. Li, S. Liang, G. McKercher, P. White, Q. Zhang, and S.J. Taylor. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24467709].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24464551">Analysis of the Hepatitis C Viral Kinetics during Administration of Two Nucleotide Analogues: Sofosbuvir (GS-7977) and GS-0938<i>.</i></a> Guedj, J., P.S. Pang, J. Denning, M. Rodriguez-Torres, E. Lawitz, W. Symonds, and A.S. Perelson. Antiviral Therapy, 2014. <b>[Epub ahead of print]</b>. PMID[24464551].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24460140">Multiple e-Pharmacophore Modeling, 3D-QSAR and High-throughput Virtual Screening of Hepatitis C Virus NS5B Polymerase Inhibitors<i>.</i></a> Kaushik-Basu, N., P.J. Theresa, D. Manvar, S. Kondepudi, M.B. Battu, D. Sriram, A. Basu, and P. Yogeeswari. Journal of Chemical Information and Modeling, 2014. <b>[Epub ahead of print]</b>. PMID[24460140].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24451189">Identification and Biochemical Characterization of Halisulfate 3 and Suvanine as Novel Inhibitors of Hepatitis C Virus NS3 Helicase from a Marine Sponge<i>.</i></a> Furuta, A., K.A. Salam, I. Hermawan, N. Akimitsu, J. Tanaka, H. Tani, A. Yamashita, K. Moriishi, M. Nakakoshi, M. Tsubuki, P.W. Peng, Y. Suzuki, N. Yamamoto, Y. Sekiguchi, S. Tsuneda, and N. Noda. Marine Drugs, 2014. 12(1): p. 462-476; PMID[24451189].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24446688">Discovery and Development of Simeprevir (TMC435), a HCV NS3/4A Protease Inhibitor<i>.</i></a> Rosenquist, A., B. Samuelsson, P.O. Johansson, M.D. Cummings, O. Lenz, P. Raboisson, K. Simmen, S. Vendeville, H. de Kock, M. Nilsson, A. Horvath, R. Kalmeijer, G. de la Rosa, and M. Beaumont-Mauviel. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24446688].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24438164">Antiviral Activity of Extracts from Morinda citrifolia Leaves and Chlorophyll Catabolites, Pheophorbide A and Pyropheophorbide A, against Hepatitis C Virus<i>.</i></a> Ratnoglik, S.L., C. Aoki, P. Sudarmono, M. Komoto, L. Deng, I. Shoji, H. Fuchino, N. Kawahara, and H. Hotta. Microbiology and Immunology, 2014. <b>[Epub ahead of print]</b>. PMID[24438164].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24437689">Hepatitis C Virus NS5A Replication Complex Inhibitors. Part 6: Discovery of a Novel and Highly Potent Biarylimidazole Chemotype with Inhibitory Activity toward Genotypes 1a and 1b Replicons<i>.</i></a> Belema, M., V.N. Nguyen, J.L. Romine, D.R. St Laurent, O.D. Lopez, J.T. Goodrich, P.T. Nower, D.R. O&#39;Boyle, 2nd, J.A. Lemm, R.A. Fridell, M. Gao, H. Fang, R.G. Krause, Y.K. Wang, A.J. Oliver, A.C. Good, J.O. Knipe, N.A. Meanwell, and L.B. Snyder. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24437689].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24397620">Synthesis, Cytostatic, Antimicrobial, and anti-HCV Activity of 6-Substituted 7-(Het)aryl-7-deazapurine ribonucleosides<i>.</i></a> Naus, P., O. Caletkova, P. Konecny, P. Dzubak, K. Bogdanova, M. Kolar, J. Vrbkova, L. Slavetinska, E. Tloust&#39;ova, P. Perlikova, M. Hajduch, and M. Hocek. Journal of Medicinal Chemistry, 2014. <b>[Epub ahead of print]</b>. PMID[24397620].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24448066">Inhibition of Epstein-Barr Virus Lytic Cycle by an Ethyl acetate Subfraction Separated from Polygonum cuspidatum Root and Its Major Component, Emodin<i>.</i></a> Yiu, C.Y., S.Y. Chen, T.H. Yang, C.J. Chang, D.B. Yeh, Y.J. Chen, and T.P. Lin. Molecules, 2014. 19(1): p. 1258-1272; PMID[24448066].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24364493">Herpes Simplex Virus Type-1 Attachment Inhibition by Functionalized Graphene oxide<i>.</i></a> Sametband, M., I. Kalt, A. Gedanken, and R. Sarid. ACS Applied Materials &amp; Interfaces, 2014. 6(2): p. 1228-1235; PMID[24364493].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24299842">The System of Fucoidans from the Brown Seaweed Dictyota dichotoma: Chemical Analysis and Antiviral Activity<i>.</i></a> Rabanal, M., N.M. Ponce, D.A. Navarro, R.M. Gomez, and C.A. Stortz. Carbohydrate Polymers, 2014. 101: p. 804-811; PMID[24299842].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0117-013014.</p>
   
    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328529400002">New Inhibitors of Cytomegalovirus DNA Polymerase<i>.</i></a> Abdel-Magid, A.F. ACS Medicinal Chemistry Letters, 2013. 4(12): p. 1129-1130; ISI[000328529400002].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328914700002">Anti-Herpes Virus Activity of Silibinin, the Primary Active Component of Silybum marianum<i>.</i></a> Cardile, A.P. and G.K.N. Mbuy. Journal of Herbal Medicine, 2013. 3(4): p. 132-136; ISI[000328914700002].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328747700012">Synthesis and Biological Evaluation of Biphenyl amides That Modulate the US28 Receptor<i>.</i></a> Kralj, A., E. Kurt, N. Tschammer, and M.R. Heinrich. ChemMedChem, 2014. 9(1): p. 151-168; ISI[000328747700012].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328624400096">Identification of Novel Small Molecules as Inhibitors of Hepatitis C Virus by Structure-based Virtual Screening<i>.</i></a> Li, J., X. Liu, S.S. Li, Y.L. Wang, N.N. Zhou, C. Luo, X.M. Luo, M.Y. Zheng, H.L. Jiang, and K.X. Chen. International Journal of Molecular Sciences, 2013. 14(11): p. 22845-22856; ISI[000328624400096].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328730300050">Artemisinin Analogues as Potent Inhibitors of in Vitro Hepatitis C Virus Replication<i>.</i></a> Obeid, S., J. Alen, V.H. Nguyen, V.C. Pham, P. Meuleman, C. Pannecouque, T.N. Le, J. Neyts, W. Dehaen, and J. Paeshuyse. Plos One, 2013. 8(12): p. e81783 ; ISI[000328730300050].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329331200002">Chemically Engineered Sulfated Glucans from Rice Bran Exert Strong Antiviral Activity at the Stage of Viral Entry<i>.</i></a> Ray, B., C. Hutterer, S.S. Bandyopadhyay, K. Ghosh, U.R. Chatterjee, S. Ray, I. Zeittrager, S. Wagner, and M. Marschall. Journal of Natural Products, 2013. 76(12): p. 2180-2188; ISI[000329331200002].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0117-013014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
