

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-92.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="f95UuZSixpNKodVaRQO8AYqBz4KPRxeRA9BIVlmm1e4Vios8Ar3PKmcXSyKS30JsLe7e9S0gGtVp/269H0gvT5IAV4k39zajQqopjkqlrQlUxH40iBS5kXBqpzw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0E02DB2E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-92-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56383   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Newer aminopyrimidinimino isatin analogues as non-nucleoside HIV-1 reverse transcriptase inhibitors for HIV and other opportunistic infections of AIDS: design, synthesis and biological evaluation</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          Farmaco <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15876436&amp;dopt=abstract</a> </p><br />

    <p>2.     56384   DMID-LS-92; EMBASE-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase. Part 1: Evaluation of the southern region of (2Z)-2-(benzoylamino)-3-(5-phenyl-2-furyl)acrylic acid</p>

    <p>          Pfefferkorn, Jeffrey A, Greene, Meredith L, Nugent, Richard A, Gross, Rebecca J, Mitchell, Mark A, Finzel, Barry C, Harris, Melissa S, Wells, Peter A, Shelly, John A, and Anstadt, Robert A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(10): 2481-2486</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FXV77F-D/2/e39e2958197fedf448929065d03373df">http://www.sciencedirect.com/science/article/B6TF9-4FXV77F-D/2/e39e2958197fedf448929065d03373df</a> </p><br />

    <p>3.     56385   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Competitive Inhibition of the Dengue Virus Ns3 Serine Protease by Synthetic Peptides Representing Polyprotein Cleavage Sites</p>

    <p>          Chanprapaph, S, Saparpakorn, P, Sangma, C, Niyomrattanakit, P, Hannongbua, S, Angsuthanasombat, C, and Katzenmeier, G</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  330(4): 1237-1246</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228566300033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228566300033</a> </p><br />

    <p>4.     56386   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An antibacterial and antiviral peptide produced by Enterococcus mundtii ST4V isolated from soya beans</p>

    <p>          Todorov, SD, Wachsman, MB, Knoetze, H, Meincken, M, and Dicks, LM</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15869868&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15869868&amp;dopt=abstract</a> </p><br />

    <p>5.     56387   DMID-LS-92; EMBASE-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCV NS5B polymerase. Part 2: Evaluation of the northern region of (2Z)-2-benzoylamino-3-(4-phenoxy-phenyl)-acrylic acid</p>

    <p>          Pfefferkorn, Jeffrey A, Nugent, Richard, Gross, Rebecca J, Greene, Meredith, Mitchell, Mark A, Reding, Matthew T, Funk, Lee A, Anderson, Rebecca, Wells, Peter A, and Shelly, John A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4G1WY2F-2/2/da1b7247904714398be821f3ba619baf">http://www.sciencedirect.com/science/article/B6TF9-4G1WY2F-2/2/da1b7247904714398be821f3ba619baf</a> </p><br />
    <br clear="all">

    <p>6.     56388   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and Characterization of Potent Small Molecule Inhibitor of Category a Hemorrhagic Fever New World Arenaviruses</p>

    <p>          Bolken, TC, Laquerre, S, Bailey, T, Kickner, SS, Sperzel, LE, Jones, KF, Waren, TK, Lund, SA, Kirkwood-Watts, DL, King, DS, Shurtleff, AC, Guttieri, MC, and Hruby, DE</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900037</a> </p><br />

    <p>7.     56389   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficacy of two commercial preparations of interferon-alpha on human papillomavirus replication</p>

    <p>          Sen, E, McLaughlin-Drubin, M, and Meyers, C</p>

    <p>          Anticancer Res <b>2005</b>.  25(2A): 1091-1100</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15868951&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15868951&amp;dopt=abstract</a> </p><br />

    <p>8.     56390   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Class of Small Molecule Inhibitors of Hepatitis B Virus Surface Antigen Secretion</p>

    <p>          Cuconati, A, Westby, G, Mehta, A, and Block, T</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900063">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900063</a> </p><br />

    <p>9.     56391   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Activity and therapeutic potential of ORI-1001 antisense oligonucleotide on human papillomavirus replication utilizing a model of dysplastic human epithelium</p>

    <p>          Alam, S, Bromberg-White, J, McLaughlin-Drubin, M, Sen, E, Bodily, JM, and Meyers, C</p>

    <p>          Anticancer Res <b>2005</b>.  25(2A): 765-777</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15868908&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15868908&amp;dopt=abstract</a> </p><br />

    <p>10.   56392   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Determination of the Precise Mode of Action of Nucleotide Analog Inhibitors of the Hcv-Ns5b Polymerase</p>

    <p>          Dutartre, N, Boretto, J, Guillemot, JC , and Canard, B</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900066">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900066</a> </p><br />

    <p>11.   56393   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pre-Clinical Evaluation of Human Omega Interferon: a Potent Anti-Flaviviridae Virus Antiviral Agent</p>

    <p>          Buckwold, VE,  Wei, JY, Russell, J, Nalcal, A, Wells, J, Lang, W, and Langecker, P</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900070">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900070</a> </p><br />
    <br clear="all">

    <p>12.   56394   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An Assay for the Biological Testing of Potential Inhibitors for the Hcv Helicase, Dengue Virus Helicase and Dengue Virus Helicase/Protease Complex (Ns3 Domain)</p>

    <p>          Vlachakis, DP, Berry, C, Jones, G, and Brancale, A</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900074">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900074</a> </p><br />

    <p>13.   56395   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Two residues in the hemagglutinin of A/Fujian/411/02-like influenza viruses are responsible for antigenic drift from A/Panama/2007/99</p>

    <p>          Jin, H, Zhou, H, Liu, H, Chan, W, Adhikary, L, Mahmood, K, Lee, MS, and Kemble, G</p>

    <p>          Virology <b>2005</b>.  336(1): 113-119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15866076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15866076&amp;dopt=abstract</a> </p><br />

    <p>14.   56396   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of West Nile Virus Inhibitors</p>

    <p>          Gu, BH, Mason, P, Wang, LJ, Bourne, N, Rossi, S, Ouzounov, S, Cuconati, A, Mehta, A, and Block, T</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900093">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900093</a> </p><br />

    <p>15.   56397   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro susceptibility of adenovirus to antiviral drugs is species-dependent</p>

    <p>          Morfin, F, Dupuis-Girod, S, Mundweiler, S, Falcon, D, Carrington, D, Sedlacek, P, Bierings, M, Cetkovsky, P, Kroes, AC, van Tol, MJ, and Thouvenot, D</p>

    <p>          Antivir Ther <b>2005</b>.  10(2): 225-229</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865216&amp;dopt=abstract</a> </p><br />

    <p>16.   56398   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Reactions of Guanidine With Vinylogous Ester-Aldehydes: Synthesis and Anti-West Nile Virus Activity of a Novel Imidazole Nucleoside Containing a Diaminodihydro-S-Triazine as a Substituent</p>

    <p>          Ujjinamatada, RK, Agasimundin, YS, Borowski, P, and Hosmane, RS</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A68-A69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900094">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900094</a> </p><br />

    <p>17.   56399   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ribavirin in the treatment of hepatitis C</p>

    <p>          Abonyi, ME and Lakatos, PL</p>

    <p>          Anticancer Res <b>2005</b>.  25(2B): 1315-1320</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15865084&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   56400   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Strategies Against Bunyaviruses Using Antisense Morpholino Oligonucleotides</p>

    <p>          Overby, A, Deflube, L, Walpita, P, Angner, K, Iversen, P, Stein, D, and Flick, R</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A85-A86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900135">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900135</a> </p><br />

    <p>19.   56401   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Suppression of Proliferation of Poliovirus and Porcine Parvovirus by Novel Phenoxazines, 2-Amino-4,4alpha-dihydro-4alpha-7-dimethyl-3H-phenoxazine-3-one and 3-Amino-1,4alpha-dihydro-4alpha-8-dimethyl-2H-phenoxazine-2-one</p>

    <p>          Iwata, A, Yamaguchi, T, Sato, K, Yoshitake, N, and Tomoda, A</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(5): 905-907</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863903&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863903&amp;dopt=abstract</a> </p><br />

    <p>20.   56402   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Some Reactions of 3-Chloroisoindolium Salts With Nucleophiles: Access to Isoindole Derivatives and Ellipticine Analogues as Potential Antiviral Agents</p>

    <p>          Hamed, AA</p>

    <p>          Journal of Chemical Research-S <b>2005</b>.(1): 54-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228312800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228312800016</a> </p><br />

    <p>21.   56403   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          S-adenosyl methionine decarboxylase activity is required for the outcome of herpes simplex virus type 1 infection and represents a new potential therapeutic target</p>

    <p>          Greco, A, Calle, A, Morfin, F, Thouvenot, D, Cayre, M, Kindbeiter, K, Martin, L, Levillain, O, and Diaz, JJ</p>

    <p>          FASEB J <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863396&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15863396&amp;dopt=abstract</a> </p><br />

    <p>22.   56404   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p><b>          Chemokine upregulation in SARS coronavirus infected human monocyte derived dendritic cells</b> </p>

    <p>          Law, HK, Cheung, CY, Ng, HY, Sia, SF, Chan, YO, Luk, W, Nicholls, JM, Peiris, JS, and Lau, YL</p>

    <p>          Blood <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15860669&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15860669&amp;dopt=abstract</a> </p><br />

    <p>23.   56405   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Basic and clinical research in polyomavirus nephropathy</p>

    <p>          Trofe, J, Gordon, J, Roy-Chaudhury, P, Koralnik, I, Atwood, W, Eash, S, Alloway, RR, Khalili, K, Alexander, JW, and Woodle, ES</p>

    <p>          Exp Clin Transplant <b>2004</b>.  2(1): 162-173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15859923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15859923&amp;dopt=abstract</a> </p><br />

    <p>24.   56406   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New antiviral pathway that mediates hepatitis C virus replicon interferon sensitivity through ADAR1</p>

    <p>          Taylor, DR, Puig, M, Darnell, ME, Mihalik, K, and Feinstone, SM</p>

    <p>          J Virol <b>2005</b> .  79(10): 6291-6298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858013&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858013&amp;dopt=abstract</a> </p><br />

    <p>25.   56407   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Serum From the American Alligator (Alligator Mississippiensis)</p>

    <p>          Merchant, ME,  Pallansch, M, Paulman, RL, Wells, JB, Nalca, A, and Ptak, R</p>

    <p>          Antiviral Research <b>2005</b>.  66(1): 35-38</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228237800006</a> </p><br />

    <p>26.   56408   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Replication fitness and NS5B drug sensitivity of diverse hepatitis C virus isolates characterized by using a transient replication assay</p>

    <p>          Ludmerer, SW,  Graham, DJ, Boots, E, Murray, EM, Simcoe, A, Markel, EJ, Grobler, JA, Flores, OA, Olsen, DB, Hazuda, DJ, and LaFemina, RL</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(5): 2059-2069</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855532&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855532&amp;dopt=abstract</a> </p><br />

    <p>27.   56409   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of 2&#39;-substituted nucleosides on hepatitis C virus replication correlates with metabolic properties in replicon cells</p>

    <p>          Tomassini, JE, Getty, K, Stahlhut, MW, Shim, S, Bhat, B, Eldrup, AB, Prakash, TP, Carroll, SS, Flores, O, MacCoss, M, McMasters, DR, Migliaccio, G, and Olsen, DB</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(5): 2050-2058</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855531&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15855531&amp;dopt=abstract</a> </p><br />

    <p>28.   56410   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Efficacy of Ns3-Serine Protease Inhibitor Biln-2061 in Patients With Chronic Genotype 2 and 3 Hepatitis C</p>

    <p>          Reiser, M, Hinrichsen, H, Benhamou, Y, Reesink, HW, Wedemeyer, H, Avendano, C, Riba, N, Yong, CL, Nehmiz, G, and Steinmann, GG</p>

    <p>          Hepatology <b>2005</b>.  41(4): 832-835</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228006000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228006000020</a> </p><br />
    <br clear="all">

    <p>29.   56411   DMID-LS-92; EMBASE-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of glycoporphyrin derivatives and their antiviral activity against herpes simplex virus types 1 and 2</p>

    <p>          Tome, Joao PC, Neves, Maria GPMS, Tome, Augusto C, Cavaleiro, Jose AS, Mendonca, Ana F, Pegado, Ines N, Duarte, Ricardo, and Valdeira, Maria L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G3D83N-2/2/5fe15ccb2c770404f31cae68e7012f0c">http://www.sciencedirect.com/science/article/B6TF8-4G3D83N-2/2/5fe15ccb2c770404f31cae68e7012f0c</a> </p><br />

    <p>30.   56412   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Euphorbia thymifolia suppresses herpes simplex virus-2 infection by directly inactivating virus infectivity</p>

    <p>          Yang, CM, Cheng, HY, Lin, TC, Chiang, LC, and Lin, CC</p>

    <p>          Clin Exp Pharmacol Physiol <b>2005</b>.  32(5-6): 346-349</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15854140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15854140&amp;dopt=abstract</a> </p><br />

    <p>31.   56413   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a humanized monoclonal antibody with therapeutic potential against West Nile virus</p>

    <p>          Oliphant, T, Engle, M, Nybakken, GE, Doane, C, Johnson, S, Huang, L, Gorlatov, S, Mehlhop, E, Marri, A, Chung, KM, Ebel, GD, Kramer, LD, Fremont, DH, and Diamond, MS</p>

    <p>          Nat Med <b>2005</b> .  11(6): 522-530</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15852016&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15852016&amp;dopt=abstract</a> </p><br />

    <p>32.   56414   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication in 2.2.15 cells by expressed shRNA</p>

    <p>          Ren, XR, Zhou, LJ, Luo, GB, Lin, B, and Xu, A</p>

    <p>          J Viral Hepat <b>2005</b>.  12(3): 236-242</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15850463&amp;dopt=abstract</a> </p><br />

    <p>33.   56415   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenotypic Characterisation of Cytomegalovirus Dna Polymerase: a Method to Study Cytomegalovirus Isolates Resistant to Foscarnet</p>

    <p>          Ducancelle, A, Gravisse, J, Alain, S, Fillet, AM, Petit, F, Le Pors, MJS, and Mazeron, MC</p>

    <p>          Journal of Virological Methods <b>2005</b>.  125(2): 145-151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228491500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228491500007</a> </p><br />
    <br clear="all">

    <p>34.   56416   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dynamics of the antiviral activity of N-methanocarbathymidine against herpes simplex virus type 1 in cell culture</p>

    <p>          Huleihel, M, Talishanisky, M, Ford, H, Marquez, VE, Kelley, JA, Johns, DG, and Agbaria, R</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.  25(5): 427-432</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848299&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15848299&amp;dopt=abstract</a> </p><br />

    <p>35.   56417   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ligand Specificity of Human Surfactant Protein D - Expression of a Mutant Trimeric Collectin That Shows Enhanced Interactions With Influenza a Virus</p>

    <p>          Crouch, E, Tu, YZ, Briner, D, Mcdonald, B, Smith, K, Holmskov, U, and Hartshorn, K</p>

    <p>          Journal of Biological Chemistry <b> 2005</b>.  280(17): 17046-17056</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228615500062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228615500062</a> </p><br />

    <p>36.   56418   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virus-inducible reporter genes as a tool for detecting and quantifying influenza A virus replication</p>

    <p>          Lutz, A, Dyall, J, Olivo, PD, and Pekosz, A</p>

    <p>          J Virol Methods <b>2005</b>.  126(1-2):  13-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15847914&amp;dopt=abstract</a> </p><br />

    <p>37.   56419   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Primary Infection of Mice With High Titer Inoculum Respiratory Syncytial Virus: Characterization and Response to Antiviral Therapy</p>

    <p>          Bolger, G, Lapeyre, N, Dansereau, N, Lagace, L, Berry, G, Klosowski, K, Mewhort, T, and Liuzzi, M</p>

    <p>          Canadian Journal of Physiology and Pharmacology <b>2005</b>.  83(2): 198-213</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228450100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228450100010</a> </p><br />

    <p>38.   56420   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Novel Broad-Spectrum Inhibitor of Influenza Virus Infections</p>

    <p>          Malakhov, MP,  Aschenbrenner, LM, Gubareva, LV, Mishin, VP, Hayden, FG, Smee, DE, Wandersee, MK,  Sidwell, RW, Kim, DH, Yu, M, and Fang, F</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900021</a> </p><br />

    <p>39.   56421   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development, Validation and Optimization of a Luminescence-Based High Throughput Screen for Inhibitors of Severe Acute Respiratory Syndrome-Associated Coronavirus</p>

    <p>          Jonsson, CB, Shindo, N, Fletcher, T, Sosa, M, Rowe, T, Hogan, J, Mcdowell, M, Taggert, B, Kushner, N, and Cooley, S</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900023</a> </p><br />
    <br clear="all">

    <p>40.   56422   DMID-LS-92; EMBASE-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the anti-influenza virus activity of cyclopentane derivatives with oseltamivir and zanamivir in vivo</p>

    <p>          Chand, Pooran, Bantia, Shanta, Kotian, Pravin L, El-Kattan, Yahya, Lin, Tsu-Hsing, and Babu, Yarlagadda S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G1GF7N-5/2/e0c695b3aaba6d045b6c4ffbe3ecbe01">http://www.sciencedirect.com/science/article/B6TF8-4G1GF7N-5/2/e0c695b3aaba6d045b6c4ffbe3ecbe01</a> </p><br />

    <p>41.   56423   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition, Escape and Attenuation of Sars Coronavirus Treated With Antisense Morpholino Oligomers</p>

    <p>          Neuman, BW, Stein, DA, Kroeker, AD, Churchill, MJ, Kim, AM, Dawson, P, Moulton, HM, Bestwick, RK, Iversen, PL, and Buchmeier, MJ</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900024</a> </p><br />

    <p>42.   56424   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent and Selective Inhibition of Hepatitis C Virus Replication by the Non-Immunosuppressive Cyclosporin Analogue Debio-025</p>

    <p>          Paeshuyse, J,  Dumont, JM, Rosenwirth, B, De Clercq, E, and Neyts, J</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900029</a> </p><br />

    <p>43.   56425   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interfering with hepatitis C virus IRES activity using RNA molecules identified by a novel in vitro selection method</p>

    <p>          Romero-Lopez, C, Barroso-delJesus, A, Puerta-Fernandez, E, and Berzal-Herranz, A</p>

    <p>          Biol Chem <b>2005</b>.  386(2): 183-190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15843163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15843163&amp;dopt=abstract</a> </p><br />

    <p>44.   56426   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interaction between the HCV NS3 protein and the host TBK1 protein leads to inhibition of cellular antiviral responses</p>

    <p>          Otsuka, M, Kato, N, Moriyama, M, Taniguchi, H, Wang, Y, Dharel, N, Kawabe, T, and Omata, M</p>

    <p>          Hepatology <b>2005</b>.  41(5): 1004-1012</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841462&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841462&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>45.   56427   DMID-LS-92; PUBMED-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Anti-Cytomegalovirus Activity of Kampo (Japanese Herbal) Medicine</p>

    <p>          Murayama, T, Yamaguchi, N, Matsuno, H, and Eizuru, Y</p>

    <p>          Evid Based Complement Alternat Med <b>2004</b>.  1(3): 285-289</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841262&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841262&amp;dopt=abstract</a> </p><br />

    <p>46.   56428   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Influenza a Virus in Cell Culture With Morpholino Oligomers</p>

    <p>          Ge, Q, Stein, D, Kroeker, A, Eisen, H, Iversen, P, and Chen, JZ</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900083">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900083</a> </p><br />

    <p>47.   56429   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Summary of the Activity of Antiviral Agents in a Murine Sars-Associated Coronavirus (Sars-Cov) Replication Model</p>

    <p>          Barnard, DL, Jung, KH, Day, CW, Bailey, KW, Heiner, ML, Wootton, WM, and Sidwell, RW</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900088">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900088</a> </p><br />

    <p>48.   56430   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A New Family of Non-Nucleoside Inhibitors of Human Cytomegalovirus (Hcmv) and Varicella-Zoster Virus (Vzv) Based on the Beta-Keto-Gamma-Sultone Template</p>

    <p>          De Castro, S,  Garcia-Aparicio, C, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900106">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900106</a> </p><br />

    <p>49.   56431   DMID-LS-92; WOS-DMID-5/10/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The in Vitro Inhibition of Cytomegalovirus by Novel Ribonucleotide Reductase Inhibitors Didox and Trimidox</p>

    <p>          Inayat, MS, Gallicchio, VS, Garvy, BA, Elford, HL, and Oakley, OR</p>

    <p>          Antiviral Research <b>2005</b>.  65(3): A74-A75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900107">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900107</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
