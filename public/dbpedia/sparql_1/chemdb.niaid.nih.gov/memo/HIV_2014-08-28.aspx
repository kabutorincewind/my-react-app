

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-08-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xbzfIBOSDi9OjN3AU2l5TdodqwCWRVvHE/x9gRK8D7oaymIH6wHbP4TBgcPyCiZgT+6mmJq6Y978JOX8RpJNf3qWiJ2KWY7QMGWWqlsFBe//TTodXrac5VVWrII=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E86D9349" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 15 - August 28, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25136952">Selective HDAC Inhibition for the Disruption of Latent HIV-1 Infection.</a> Barton, K.M., N.M. Archin, K.S. Keedy, A.S. Espeseth, Y.L. Zhang, J. Gale, F.F. Wagner, E.B. Holson, and D.M. Margolis. PloS One, 2014. 9(8): p. e102684. PMID[25136952].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24997293">Pyridopyrimidinone Inhibitors of HIV-1 RNase H.</a> Velthuisen, E.J., B.A. Johns, P. Gerondelis, Y. Chen, M. Li, K. Mou, W. Zhang, J.W. Seal, K.E. Hightower, S.R. Miranda, K. Brown, and L. Leesnitzer. European Journal of Medicinal Chemistry, 2014. 83: p. 609-616. PMID[24997293].</p>

	<p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25132382">Neutralizing Antibodies Inhibit HIV-1 Infection of Plasmacytoid Dendritic Cells by an Fc&#947;RIIa Independent Mechanism and Do Not Diminish Cytokines Production.</a> Lederle, A., B. Su, V. Holl, J. Penichon, S. Schmidt, T. Decoville, G. Laumond, and C. Moog. Scientific Reports, 2014. 4: p. 5845. PMID[25132382].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24997292">Functionalization, Cyclization and Antiviral Activity of A-Secotriterpenoids.</a> Grishko, V.V., N.V. Galaiko, I.A. Tolmacheva, I.I.  Kucherov, V.F. Eremin, E.I. Boreko, O.V. Savinova, and P.A. Slepukhin. European Journal of Medicinal Chemistry, 2014. 83: p. 601-608. PMID[24997292].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25144636">Azvudine, a Novel Nucleoside Reverse Transcriptase Inhibitor Showed Good Drug Combination Features and Better Inhibition on Drug-resistant Strains Than Lamivudine in vitro.</a> Wang, R.R., Q.H. Yang, R.H. Luo, Y.M. Peng, S.X. Dai, X.J. Zhang, H. Chen, X.Q. Cui, Y.J. Liu, J.F. Huang, J.B. Chang, and Y.T. Zheng. PloS One, 2014. 9(8): p. e105617. PMID[25144636].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25144758">Aqueous Extracts of the Marine Brown Alga Lobophora variegata Inhibit HIV-1 Infection at the Level of Virus Entry into Cells.</a> Kremb, S., M. Helfer, B. Kraus, H. Wolff, C. Wild, M. Schneider, C.R. Voolstra, and R. Brack-Werner. PloS One, 2014. 9(8): p. e103895. PMID[25144758].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24996145">Antiretroviral Activity of Metal-chelating HIV-1 Integrase Inhibitors.</a> Carcelli, M., D. Rogolino, M. Sechi, G. Rispoli, E. Fisicaro, C. Compari, N. Grandi, A. Corona, E. Tramontano, C. Pannecouque, and L. Naesens. European Journal of Medicinal Chemistry, 2014. 83: p. 594-600. PMID[24996145].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25026140">Anti-HIV Activities of Precisely Defined, Semirigid, Carboxylated Alternating Copolymers.</a> Savage, A.M., Y. Li, L.E. Matolyak, G.F. Doncel, S.R. Turner, and R.D. Gandour. Journal of Medicinal Chemistry, 2014. 57(15): p. 6354-6363. PMID[25026140].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0815-082814.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339228200007">Small Molecule Inhibition of SAMHD1 dNTPase by Tetramer Destabilization.</a> Seamon, K.J., E.C. Hansen, A.P. Kadina, B.A. Kashemirov, C.E. McKenna, N.N. Bumpus, and J.T. Stivers. Journal of the AmericanChemical Society, 2014. 136(28): p. 9822-9825. ISI[000339228200007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338991100023">Optimization of the Antiviral Potency and Lipophilicity of Halogenated 2,6-Diarylpyridinamines as a Novel Class of HIV-1 NNRTIS.</a> Wu, Z.Y., N. Liu, B.J. Qin, L. Huang, F. Yu, K.D. Qian, S.L. Morris-Natschke, S.B. Jiang, C.H. Chen, K.H. Lee, and L. Xie. ChemMedChem, 2014. 9(7): p. 1546-1555. ISI[000338991100023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339662900005">Minocycline Attenuates HIV-1 Infection and Suppresses Chronic Immune Activation in Humanized NOD/LTSZ-Scidil-2R-gamma(null) Mice.</a> Singh, M., P. Singh, D. Vaira, M. Amand, S. Rahmouni, and M. Moutschen. Immunology, 2014. 142(4): p. 562-572. ISI[000339662900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339781200252">Anti-HIV Activities in an African Plant Extract.</a> Zheng, Y., M. Mulinge, M. Counson, X. Yang, A. Steinmetz, J.C. Schmit, and C. Devaux. Planta Medica, 2014. 80(10): p. 804-804. ISI[000339781200252].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340074600007">A New Activity for SAMHD1 in HIV Restriction.</a> Yang, Z.Y. and W.C. Greene. Nature Medicine, 2014. 20(8): p. 808-809. ISI[000340074600007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339781200285">The anti-HIV Activity of Flavonoids: Quercetin and Pinocembrin.</a> Murata, R.M., S. Pasetto, and V. Pardi. Planta Medica, 2014. 80(10): p. 811-811. ISI[000339781200285].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339944300010">A Cell-intrinsic Inhibitor of HIV-1 Reverse Transcription in CD4(+) T Cells from Elite Controllers.</a> Leng, J., H.P. Ho, M.J. Buzon, F. Pereyra, B.D. Walker, X.G. Yu, E.J. Chang, and M. Lichterfeld. Cell Host &amp; Microbe, 2014. 15(6): p. 717-728. ISI[000339944300010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000339729700038">Synthesis and Studies of Biological Activity of New 8-{(Adamant-1-yl)alkyl amino}theophylline Derivatives.</a> Valuev-Elliston, V.T., E.N. Savel&#39;ev, A.V. Ivanov, B.S. Orlinson, E.N. Gerasimov, E.K. Zakharova, L.L. Brunilina, S.N. Kochetkov, I.A. Novakov, and M.B. Navrotskii. Russian Chemical Bulletin, 2013. 62(11): p. 2544-2546. ISI[000339729700038].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0815-082814.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140717&amp;CC=WO&amp;NR=2014110296A1&amp;KC=A1">Preparation of Macroheterocyclic Compounds for the Treatment of Viral Infections.</a> Bondy, S.S., C.E. Cannizzaro, C.-H. Chou, Y.E. Hu, J.O. Link, Q. Liu, S.D. Schroeder, W.C. Tse, and J.R. Zhang. Patent. 2014. 2014-US10937 2014110296: 108pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140717&amp;CC=WO&amp;NR=2014110298A1&amp;KC=A1">Preparation of 5-Membered Heteroaryl Compounds Useful in Treatment of Viral Infections.</a> Bondy, S.S., C.E. Cannizzaro, C.-H. Chou, J.O. Link, Q. Liu, S.D. Schroeder, W.C. Tse, and J.R. Zhang. Patent. 2014. 2014-US10939 2014110298: 93pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140717&amp;CC=WO&amp;NR=2014110297A1&amp;KC=A1">Preparation of (Hetero)Arylacetamide Derivatives as Antiretroviral Agents.</a> Brizgys, G., C.-H. Chou, R.L. Halcomb, Y.E. Hu, Q. Liu, J.R. Somoza, W.C. Tse, and J.R. Zhang. Patent. 2014. 2014-US10938 2014110297: 156pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140806&amp;CC=CN&amp;NR=103965163A&amp;KC=A">Quinolone Derivative Containing Pyrimidine Ring, and Preparation Method and Use Thereof for Resisting HIV-1 Infection.</a> Chen, F., Q. He, T. Mao, and Z. Wan. Patent. 2014. 2014-10201093 103965163: 17pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140626&amp;CC=WO&amp;NR=2014099586A1&amp;KC=A1">Pyridinotriazinone Derivatives as HIV Integrase Inhibitors and Their Preparation.</a> Embrey, M.W., T.H. Graham, A. Walji, S.T. Waddell, T. Yu, Y. Zhang, W. Liu, P.J. Coleman, J. Wai, T. Steele, and C. Di Marco. Patent. 2014. 2013-US74590 2014099586: 106pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140626&amp;CC=WO&amp;NR=2014100323A1&amp;KC=A1">Preparation of Polycyclic-carbamoylpyridone Compounds and Their Pharmaceutical Use.</a> Jin, H., S.E. Lazerwith, T.A.T. Martin, E.M. Bacon, J.J. Cottell, Z.R. Cai, H.-J. Pyun, P.A. Morganelli, M. Ji, J.G. Taylor, X. Chen, M.R. Mish, and M.C. Desai. Patent. 2014. 2013-US76367 2014100323: 261pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140703&amp;CC=WO&amp;NR=2014104279A1&amp;KC=A1">Preparation of Substituted Spiropyrido[1,2-a]pyrazine Derivative and Medicinal Use Thereof as HIV Integrase Inhibitors.</a> Miyazaki, S., H. Isoshima, K. Oshita, S. Kawashita, N. Nagahashi, and M. Terashita. Patent. 2014. 2013-JP85059 2014104279: 340pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140703&amp;CC=WO&amp;NR=2014105926A1&amp;KC=A1">Preparation of Novel Betulinic acid proline Derivatives as HIV Inhibitors.</a> Panduranga Reddy, A., B. Parthasaradhi Reddy, K. Rathnakar Reddy, L. Subrahmanyam VI, G.L. David Krupadanam, M. Venkati, N. Sudhakar, and K. Srinivas Reddy. Patent. 2014. 2013-US77751 2014105926: 151pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140814&amp;CC=WO&amp;NR=2014123900A1&amp;KC=A1">Anti-cancer and anti-HIV Compounds.</a> Usui, I., J.N. Beverage, and V.R.R. Macherla. Patent. 2014. 2014-US14668 2014123900: 168pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140702&amp;CC=CN&amp;NR=103893172A&amp;KC=A">Application of Antiviral Compound in Preparing anti-HIV-1 Virus Drugs.</a> Zhang, H., C. Bai, and T. Pan. Patent. 2014. 2014-10120259 103893172: 8pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140618&amp;CC=CN&amp;NR=103864699A&amp;KC=A">Preparation and Application of Non-Nucleoside S-Dabos Pyrimidone Derivatives with Action against HBV Virus, HIV Virus and HCV Virus.</a> Zhang, L., Y. Fu, H. Qin, X. Wang, and J. Liu. Patent. 2014. 2012-10531716 103864699: 30pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0815-082814.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
