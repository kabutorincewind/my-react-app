

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-05-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nfBIqznROrgvZXeHucIhGp47j3Vehtu513QE70qMrMLeDgQsJFtZkS0SFHY9yReLm9BgJfHa4DVWw4F+DCOl2SvuIZJ+/8gjSYnSL+CTDDp5gVHm7BV3rmhQfI8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D4C4BBAC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 25 - May 8, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24325676">Monoclonal Antibodies to Various Epitopes of Hepatitis B Surface Antigen Inhibit Hepatitis B Virus Infection<i>.</i></a> Golsaz Shirazi, F., H. Mohammadi, M.M. Amiri, K. Singethan, Y. Xia, A.A. Bayat, M. Bahadori, H. Rabbani, M. Jeddi-Tehrani, U. Protzer, and F. Shokri. Journal of Gastroenterology and Hepatology, 2014. 29(5): p. 1083-1091; PMID[24325676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24697270">Stereoselective Synthesis of 2&#39;-Fluoro-6&#39;-methylene carbocyclic adenosine via Vince Lactam<i>.</i></a> Singh, U.S., R.C. Mishra, R. Shankar, and C.K. Chu. The Journal of Organic Chemistry, 2014. 79(9): p. 3917-3923; PMID[24697270].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24689657">Key Binding and Susceptibility of NS3/4A Serine Protease Inhibitors against Hepatitis C Virus<i>.</i></a> Meeprasert, A., S. Hannongbua, and T. Rungrotmongkol. Journal of Chemical Information and Modeling, 2014. 54(4): p. 1208-1217; PMID[24689657].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24675178">Discovery of 2-Iminobenzimidazoles as Potent Hepatitis C Virus Inhibitors with a Novel Mechanism of Action<i>.</i></a> Windisch, M.P., S. Jo, H.Y. Kim, S.H. Kim, K. Kim, S. Kong, H. Jeong, S. Ahn, Z. No, and J.Y. Hwang. European Journal of Medicinal Chemistry, 2014. 78: p. 35-42; PMID[24675178].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24654886">New Pyrazolobenzothiazine Derivatives as Hepatitis C Virus NS5B Polymerase Palm Site I Inhibitors<i>.</i></a> Manfroni, G., D. Manvar, M.L. Barreca, N. Kaushik-Basu, P. Leyssen, J. Paeshuyse, R. Cannalire, N. Iraci, A. Basu, M. Chudaev, C. Zamperini, E. Dreassi, S. Sabatini, O. Tabarrini, J. Neyts, and V. Cecchetti. Journal of Medicinal Chemistry, 2014. 57(8): p. 3247-3262; PMID[24654886].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24623716">Synthesis and anti-HCV Entry Activity Studies of beta-Cyclodextrin-pentacyclic triterpene Conjugates<i>.</i></a> Xiao, S., Q. Wang, L. Si, Y. Shi, H. Wang, F. Yu, Y. Zhang, Y. Li, Y. Zheng, C. Zhang, C. Wang, L. Zhang, and D. Zhou. ChemMedChem, 2014. 9(5): p. 1060-1070; PMID[24623716].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24550342">Intracellular Effects of the Hepatitis C Virus Nucleoside Polymerase Inhibitor RO5855 (Mericitabine Parent) and Ribavirin in Combination<i>.</i></a> Ma, H., S. Le Pogam, S. Fletcher, F. Hinojosa-Kirschenbaum, H. Javanbakht, J.M. Yan, W.R. Jiang, N. Inocencio, K. Klumpp, and I. Najera. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2614-2625; PMID[24550342].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24678947">Identification and Characterization of Small Molecule Modulators of the Epstein-Barr Virus-induced Gene 2 (EBI2) Receptor<i>.</i></a> Gessier, F., I. Preuss, H. Yin, M.M. Rosenkilde, S. Laurent, R. Endres, Y.A. Chen, T.H. Marsilje, K. Seuwen, D.G. Nguyen, and A.W. Sailer. Journal of Medicinal Chemistry, 2014. 57(8): p. 3358-3368; PMID[24678947].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24576908">A Dihydro-pyrido-indole Potently Inhibits HSV-1 Infection by Interfering the Viral Immediate Early Transcriptional Events<i>.</i></a> Bag, P., D. Ojha, H. Mukherjee, U.C. Halder, S. Mondal, A. Biswas, A. Sharon, L. Van Kaer, S. Chakrabarty, G. Das, D. Mitra, and D. Chattopadhyay. Antiviral Research, 2014. 105: p. 126-134; PMID[24576908].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24794394">Silencing Herpes Simplex Virus Type 1 Capsid Protein Encoding Genes by siRNA: A Promising Antiviral Therapeutic Approach<i>.</i></a> Jin, F., S. Li, K. Zheng, C. Zhuo, K. Ma, M. Chen, Q. Wang, P. Zhang, J. Fan, Z. Ren, and Y. Wang. Plos One, 2014. 9(5): p. e96623; PMID[24794394].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24574416">Nelfinavir Inhibits Maturation and Export of Herpes Simplex Virus 1<i>.</i></a> Kalu, N.N., P.J. Desai, C.M. Shirley, W. Gibson, P.A. Dennis, and R.F. Ambinder. Journal of Virology, 2014. 88(10): p. 5455-5461; PMID[24574416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24590478">Tranylcypromine Reduces Herpes Simplex Virus 1 Infection in Mice<i>.</i></a> Yao, H.W., P.H. Lin, F.H. Shen, G.C. Perng, Y.Y. Tung, S.M. Hsu, and S.H. Chen. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2807-2815; PMID[24590478].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24608026">Chemical Characterization and Antiherpes Activity of Sulfated Polysaccharides from Lithothamnion muelleri<i>.</i></a> Malagoli, B.G., F.T. Cardozo, J.H. Gomes, V.P. Ferraz, C.M. Simoes, and F.C. Braga. International Journal of Biological Macromolecules, 2014. 66: p. 332-337; PMID[24608026].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0425-050814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333555300014">Stearoyl-CoA Desaturase Inhibition Blocks Formation of Hepatitis C Virus-induced Specialized Membranes<i>.</i></a> Lyn, R.K., R. Singaravelu, S. Kargman, S. O&#39;Hara, H. Chan, R. Oballa, Z. Huang, D.M. Jones, A. Ridsdale, R.S. Russell, A.W. Partridge, and J.P. Pezacki. Scientific Reports, 2014. 4: 4549; ISI[000333555300014].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0425-050814.</p><br />  

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332831100007">Synthesis, Antiviral Evaluation, and Computational Studies of Cyclobutane and Cyclobutene L- Nucleoside Analogues<i>.</i></a> Miralles-Lluma, R., A. Figueras, F. Busque, A. Alvarez-Larena, J. Balzarini, M. Figueredo, J. Font, R. Alibes, and J.D. Marechal. European Journal of Organic Chemistry, 2013. 2013(34): p. 7761-7775; ISI[000332831100007].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0425-050814.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333681600011">Hepatitis B and C: From Molecular Virology to New Antiviral Therapies (Part 2)<i>.</i></a> Thimme, R., M. Heim, T.F. Baumert, M. Nassal, and D. Moradpour. Deutsche Medizinische Wochenschrift, 2014. 139(15): p. 778-782; ISI[000333681600011].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0425-050814.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
