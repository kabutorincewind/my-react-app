

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-05-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AKrQ077WAwVX1Bd2wCOGvKWFO7ru88UMhYM/Jmup7hC//PZFPmZ/kpsSVfz9fZARCdbH8MEAdXlLMtBZXmC7yNHDMoVhThZ1GTt4/znZILMk/xMI2iRqG0VCrdk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C45A6CB7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 26 - May 9, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2362907">Purification, Molecular Cloning, and Antimicrobial Activity of Peptides from the Skin Secretion of the Black-spotted Frog, Rana nigromaculata.</a> Li, A., Y. Zhang, C. Wang, G. Wu, and Z. Wang. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23632907].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/223494219">A Comparative Study of Anti-candida Activity and Phenolic Contents of the Calluses from Lythrum salicaria L. In Different Treatments.</a> Manayi, A., S. Saeidnia, M.A. Faramarzi, N. Samadi, S. Jafari, M. Vazirian, A. Ghaderi, T. Mirnezami, A. Hadjiakhoondi, M.R. Ardekani, and M. Khanavi. Applied Biochemistry and Biotechnology, 2013. 170(1): p. 176-184. PMID[23494219].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23617888">Enhanced Inhibition of Aspergillus niger on Sedge (Lepironia articulate) Treated with Heat-cured Lime Oil.</a> Matan, N., N. Matan, and S. Ketsa. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23617888].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23627396">Antifungal Agents from Pseudallescheria boydii SNB-CN73 Isolated from a Nasutitermes Sp. Termite.</a> Nirma, C., V. Eparvier, and D. Stien. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23627396].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23619574">Antifungal Activity of Ferulago capillaris Essential Oil against Candida, Cryptococcus, Aspergillus and Dermatophyte Species.</a> Pinto, E., K. Hrimpeng, G. Lopes, S. Vaz, M.J. Goncalves, C. Cavaleiro, and L. Salgueiro. European Journal of Clinical Microbiology &amp; Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23619574].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23640383">Synthesis and Antifungal Activity of the Novel Triazole Derivatives Containing 1,2,3-Triazole Fragment.</a> Yu, S., N. Wang, X. Chai, B. Wang, H. Cui, Q. Zhao, Y. Zou, Q. Sun, Q. Meng, and Q. Wu. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23640383].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23633686">Rapid in Vivo Assessment of Drug Efficacy against Mycobacterium tuberculosis Using an Improved Firefly Luciferase.</a> Andreu, N., A. Zelmer, S.L. Sampson, M. Ikeh, G.J. Bancroft, U.E. Schaible, S. Wiles, and B.D. Robertson. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23633686].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23324803">Three Antimycobacterial Metabolites Identified from a Marine-Derived Streptomyces Sp. MS100061.</a> Chen, C., J. Wang, H. Guo, W. Hou, N. Yang, B. Ren, M. Liu, H. Dai, X. Liu, F. Song, and L. Zhang. Applied Microbiology and Biotechnology, 2013. 97(9): p. 3885-3892. PMID[23324803].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23650159">Transition Metal-alpha-amino acid Complexes with Antibiotic Activity against Mycobacterium Spp.</a> Karpin, G., J.S. Merola, and J.O. Falkinham, 3rd. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23650159].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23627296">Syntheses and Antimycobacterial Activities of [(2S,3R)-2-(Amino)-4-(arenesulfonamido)-3-hydroxy-1-phenylbutane Derivatives.</a> Moreth, M., C.R. Gomes, M.C. Lourenco, R.P. Soares, M.N. Rocha, C.R. Kaiser, M.V. de Souza, S.M. Wardell, and J.L. Wardell. Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23627296].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23491715">Drug Testing in Mouse Models of Tuberculosis and Nontuberculous mycobacterial Infections.</a> Nikonenko, B.V. and A.S. Apt. Tuberculosis, 2013. 93(3): p. 285-290. PMID[23491715].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23498915">Design, Synthesis and Anti-tuberculosis Activity of 1-Adamantyl-3-heteroaryl Ureas with Improved in Vitro Pharmacokinetic Properties.</a> North, E.J., M.S. Scherman, D.F. Bruhn, J.S. Scarborough, M.M. Maddox, V. Jones, A. Grzegorzewicz, L. Yang, T. Hess, C. Morisseau, M. Jackson, M.R. McNeil, and R.E. Lee. Bioorganic &amp; Medicinal Chemistry, 2013. 21(9): p. 2587-2599. PMID[23498915].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23643393">Quinolones for Mycobacterial Infections.</a> Rubinstein, E. and Y. Keynan. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23643393].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23645588">A Plasmodium Falciparum Screening Assay for Anti-gametocyte Drugs Based on Parasite Lactate  Dehydrogenase Detection.</a> D&#39;Alessandro, S., F. Silvestrini, K. Dechering, Y. Corbett, S. Parapini, M. Timmerman, L. Galastri, N. Basilico, R. Sauerwein, P. Alano, and D. Taramelli. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23645588].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23629698">Male and Female P. falciparum Mature Gametocytes Show Different Responses to Antimalarial Drugs.</a> Delves, M.J., A. Ruecker, U. Straschil, J. Lelievre, S. Marques, M.J. Lopez-Barragan, E.  Herreros, and R.E. Sinden. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID [23629698].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23501156">Antiprotozoal Activity of Khaya anthotheca, (Welv.) C.D.C. A Plant Used by Chimpanzees for Self-Medication.</a> Obbo, C.J., B. Makanga, D.A. Mulholland, P.H. Coombes, and R. Brun. Journal of Ethnopharmacology, 2013. 147(1): p. 220-223. PMID[23501156].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23652641">Antimalarial Potential of China 30 and Chelidonium 30 in Combination Therapy against Lethal Rodent Malaria Parasite: Plasmodium berghei.</a> Rajan, A. and U. Bagai. Journal of Complementary &amp; Integrative Medicine, 2013. 10(1): p. 1-8. PMID[23652641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23624149">The Effect of Kinase, Actin, Myosin and Dynamin Inhibitors on Host Cell Egress by Toxoplasma gondii.</a> Caldas, L.A., S.H. Seabra, M. Attias, and W. de Souza. Parasitology International, 2013. <b>[Epub ahead of print]</b>. PMID[23624149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23627352">Design, Synthesis and Molecular Modeling of Novel Pyrido[2,3-d]pyrimidine Analogs as Antifolates: Application of Buchwald-Hartwig Aminations of Heterocycles.</a> Gangjee, A., O.A. Namjoshi, S. Raghavan, S.F. Queener, R.L. Kisliuk, and V. Cody. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23627352].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0426-050913.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003317057600009">Novel 2-(2-Benzylidenehydrazinyl)benzo d Thiazole as Potential Antitubercular Agents.</a> Bairwa, V.K. and V.N. Telvekar. Combinatorial Chemistry &amp; High Throughput Screening, 2013. 16(3): p. 244-247. ISI[000317057600009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316642900049">Expression, Characterization and Inhibition of Toxoplasma gondii 1-Deoxy-d-xylulose-5-phosphate Reductoisomerase.</a> Cai, G.B., L.S. Deng, J. Xue, S.N.J. Moreno, B. Striepen, and Y.C. Song. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(7): p. 2158-2161. ISI[000316642900049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316742700012">Structural Basis for the Inhibition of Mycobacterium tuberculosis L,D-Transpeptidase by Meropenem, a Drug Effective against Extensively Drug-resistant Strains.</a> Kim, H.S., J. Kim, H.N. Im, J.Y. Yoon, D.R. An, H.J. Yoon, J.Y. Kim, H.K. Min, S.J. Kim, J.Y. Lee, B.W. Han, and S.W. Suh. Acta Crystallographica Section D-Biological Crystallography, 2013. 69: p. 420-431. ISI[000316742700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316977500007">Bactericidal and Fungicidal Activity of Methanolic Extracts of Heracleum persicum Desf. Ex Fischer against Some Aquatic and Terrestrial Animal Pathogens.</a> Kousha, A. and M. Bayat. International Journal of Pharmacology, 2012. 8(7): p. 652-656. ISI[000316977500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317085400003">Inhibition of Protein Splicing of Mycobacterium tuberculosis RecA Intein by Pd Complexes.</a> Liu, Y., Q. Wu, Y.C. Zheng, and Y.Z. Liu. Chemical Journal of Chinese Universities-Chinese, 2013. 34(2): p. 272-276. ISI[000317085400003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316963100010">Paepalanthus Spp: Antimycobacterial Activity of Extracts, Methoxylated Flavonoids and Naphthopyranone Fractions.</a> Moreira, R.R.D., G.Z. Martins, R. Pietro, D.N. Sato, F.R. Pavan, S.R.A. Leite, W. Vilegas, and C.Q.F. Leite. Revista Brasileira de Farmacognosia, 2013. 23(2): p. 268-272. ISI[000316963100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br />   

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316868900013">Potent Growth Inhibitory Activity of (+/-)-Platencin Towards Multi-drug-resistant and Extensively Drug-resistant Mycobacterium tuberculosis.</a> Moustafa, G.A.I., S. Nojima, Y. Yamano, A. Aono, M. Arai, S. Mitarai, T. Tanaka, and T. Yoshimitsu. MedChemComm, 2013. 4(4): p. 720-723. ISI[000316868900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316790000010">Novel Semisynthetic Antibiotics from Caprazamycins A-G: Caprazene Derivatives and Their Antibacterial Activity.</a> Takahashi, Y., M. Igarashi, T. Miyake, H. Soutome, K. Ishikawa, Y. Komatsuki, Y. Koyama, N. Nakagawa, S. Hattori, K. Inoue, N. Doi, and Y. Akamatsu. Journal of Antibiotics, 2013. 66(3): p. 171-178. ISI[000316790000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0426-050913.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
