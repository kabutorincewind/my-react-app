

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NYtdFPbMj9afzT9hGphBJp1Fsu8q5FxpxUDBmd9jEHEIYTWTRYcY34L+D3uGZ86JO7vIVDiAJKFZdqm0qzvRUl8Ry10XOX82pGqz5g0tom4t8roUUE+R70RQRJI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="79D20077" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: April, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000397563000009">Synthesis and Biological Evaluation of New Dipyridylpteridines, Lumazines, and Related Analogues.</a> Abbas, Z.A.A., N.M.J. Abu-Mejdad, Z.W. Atwan, and N.A. Al-Masoudi. Journal of Heterocyclic Chemistry, 2017. 54(2): p. 895-903. ISI[000397563000009].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395421300019">Sonication-assisted Synthesis of (E)-2-Methyl-but-2-enyl Nucleoside Phosphonate Prodrugs.</a> Bessieres, M., O. Sari, V. Roy, D. Warszycki, A.J. Bojarski, S.P. Nolan, R. Snoeck, G. Andrei, R.F. Schinazi, and L.A. Agrofoglio. ChemistrySelect, 2016. 1(12): p. 3108-3113. ISI[000395421300019].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27800697">Chimeric Peptide-mediated siRNA Transduction to Inhibit HIV-1 Infection.</a> Bivalkar-Mehla, S., R. Mehla, and A. Chauhan. Journal of Drug Targeting, 2017. 25(4): p. 307-319. PMID[27800697].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28122982">Lack of ADCC Breadth of Human Nonneutralizing anti-HIV-1 Antibodies.</a> Bruel, T., F. Guivel-Benhassine, V. Lorin, H. Lortat-Jacob, F. Baleux, K. Bourdic, N. Noel, O. Lambotte, H. Mouquet, and O. Schwartz. Journal of Virology, 2017. 91(8). PMID[28122982]. PMCID[PMC5375671].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">5. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28373864">Chelation Motifs Affecting Metal-dependent Viral Enzymes: N&#39;-Acylhydrazone Ligands as Dual Target Inhibitors of HIV-1 Integrase and Reverse Transcriptase Ribonuclease H Domain.</a> Carcelli, M., D. Rogolino, A. Gatti, N. Pala, A. Corona, A. Caredda, E. Tramontano, C. Pannecouque, L. Naesens, and F. Esposito. Frontiers in Microbiology, 2017. 8(440): 10pp. PMID[28373864]. PMCID[PMC5357622].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28436427">Broad and Potent Cross Clade Neutralizing Antibodies with Multiple Specificities in the Plasma of HIV-1 Subtype C Infected Individuals.</a> Cheedarla, N., K.L. Precilla, H. Babu, K.K.V. Vijayan, M. Ashokkumar, P. Chandrasekaran, N. Kailasam, J.C. Sundaramurthi, S. Swaminathan, V. Buddolla, S.K. Vaniambadi, V.D. Ramanathan, and L.E. Hanna. Scientific Reports, 2017. 7(46557): 13pp. PMID[28436427]. PMCID[PMC5402285].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28266845">Synthesis, Antiviral Potency, in Vitro ADMET, and X-ray Structure of Potent CD4 Mimics as Entry Inhibitors That Target the Phe43 Cavity of HIV-1 gp120.</a> Curreli, F., Y.D. Kwon, D.S. Belov, R.R. Ramesh, A.V. Kurkin, A. Altieri, P.D. Kwong, and A.K. Debnath. Journal of Medicinal Chemistry, 2017. 60(7): p. 3124-3153. PMID[28266845].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000398093000011">Synthesis of beta-L-2&#39;-Fluoro-3&#39;-thiacytidine (F-3TC) Stereoisomers: Toward a New Class of Oxathiolanyl Nucleosides?</a> D&#39;Alonzo, D., M. De Fenza, G. Palumbo, V. Romanucci, A. Zarrelli, G. Di Fabio, and A. Guaragna. Synthesis, 2017. 49(5): p. 998-1008. ISI[000398093000011].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">9. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27737540">Site-specific Polymer Attachment to HR2 Peptide Fusion Inhibitors against HIV-1 Decreases Binding Association Rates and Dissociation Rates Rather Than Binding Affinity.</a> Danial, M., A.N. Stauffer, F.R. Wurm, M.J. Root, and H.A. Klok. Bioconjugate Chemistry, 2017. 28(3): p. 701-712. PMID[27737540]. PMCID[PMC5352488].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">10. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28160944">Dihydropyrimidinone-isatin Hybrids as Novel Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Devale, T.L., J. Parikh, P. Miniyar, P. Sharma, B. Shrivastava, and P. Murumkar. Bioorganic Chemistry, 2017. 70: p. 256-266. PMID[28160944].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395394900071">Inhibition of HIV-1 Protease and Plasmodium falciparum by a Modified Digold chloroquine Derivative.</a> Gama, N., K. Kumar, J. Reader, B. Gordhan, L.M. Birkholtz, B. Kana, J. Darkwa, and D. Meyer. Journal of the International AIDS Society, 2016. 19(P028): p. 37-38. ISI[000395394900071].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">12. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28322598">Screening and Functional Profiling of Small-molecule HIV-1 Entry and Fusion Inhibitors.</a> Giroud, C., Y.H. Du, M. Marin, Q. Min, N.T. Jui, H.A. Fu, and G.B. Melikyan. Assay and Drug Development Technologies, 2017. 15(2): p. 53-63. PMID[28322598]. PMCID[PMC5367911].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">13. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27565689">Lipid Raft-like Liposomes Used for Targeted Delivery of a Chimeric Entry-inhibitor Peptide with anti-HIV-1 Activity.</a> Gomara, M.J., I. Perez-Pomeda, J.M. Gatell, V. Sanchez-Merino, E. Yuste, and I. Haro. Nanomedicine: Nanotechnology, Biology and Medicine, 2017. 13(2): p. 601-609. PMID[27565689].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000398396800119">P-D9 A Novel Small Molecule Inhibitor of HIV-1 Entry.</a> Heredia, A., O. Latinovic, and F. Barbault. Journal of Acquired Immune Deficiency Syndromes, 2016. 71: p. 95-95. ISI[000398396800119].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395394300139">Elimination of HIV-1 Latently Infected Cells by PKC Agonist Gnidimacrin Alone and in Combination with a Histone Deacetylase Inhibitor.</a> Huang, L., W. Lai, L. Zhu, and C.H. Chen. Journal of the International AIDS Society, 2016. 19(THAA0106LB): p. 72. ISI[000395394300139].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395394900065">HIV-1 Combinectin GSK3732394: A Long-acting Inhibitor with Multiple Modes of Action.</a> Krystal, M., D. Wensel, Y.N. Sun, J. Davis, T. McDonagh, Z.F. Li, S.R. Zhang, M. Soars, and M. Cockett. Journal of the International AIDS Society, 2016. 19(P022): p. 34. ISI[000395394900065].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">17. <a href="https://www.ncbi.nlm.nih.gov/labs/articles/27928647">Triple Drug Combination of Zidovudine, Efavirenz and Lamivudine Loaded Lactoferrin Nanoparticles: An Effective Nano First-line Regimen for HIV Therapy.</a> Kumar, P., Y.S. Lakshmi, and A.K. Kondapi. Pharmaceutical Research, 2017. 34(2): p. 257-268. PMID[27928647].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28148796">Reduced Potency and Incomplete Neutralization of Broadly Neutralizing Antibodies against Cell-to-cell Transmission of HIV-1 with Transmitted Founder Envs.</a> Li, H., C. Zony, P. Chen, and B.K. Chen. Journal of Virology, 2017. 91(9): e02425-16. PMID[28148796]. PMCID[PMC5391450].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">19. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28089931">A Novel Polysaccharide with Antioxidant, HIV Protease Inhibiting and HIV Integrase Inhibiting Activities from Fomitiporia punctata (P. Karst.) Murrill (Basidiomycota, Hymenochaetales).</a> Liu, F., Y. Wang, K. Zhang, Y. Wang, R. Zhou, Y. Zeng, Y. Han, and T.B. Ng. International Journal of Biological Macromolecules, 2017. 97: p. 339-347. PMID[28089931].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28314514">Structural Modifications of Diarylpyrimidines (DAPYs) as HIV-1 NNRTIs: Synthesis, anti-HIV Activities and SAR.</a> Lu, H.H., P. Xue, Y.Y. Zhu, X.L. Ju, X.J. Zheng, X. Zhang, T. Xiao, C. Pannecouque, T.T. Li, and S.X. Gu. Bioorganic &amp; Medicinal Chemistry, 2017. 25(8): p. 2491-2497. PMID[28314514].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">21. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28165251">Enantioselective Synthesis of 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine (EFdA) via Enzymatic Desymmetrization.</a> McLaughlin, M., J. Kong, K.M. Belyk, B. Chen, A.W. Gibson, S.P. Keen, D.R. Lieberman, E.M. Milczek, J.C. Moore, D. Murray, F. Peng, J. Qi, R.A. Reamer, Z.J. Song, L.S. Tan, L. Wang, and M.J. Williams. Organic Letters, 2017. 19(4): p. 926-929. PMID[28165251].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000397376600010">Synthesis of Novel HIV-1 Protease Inhibitors via Diastereoselective Henry Reaction with Nitrocyclopropane.</a> Mitchell, M.L., L.H. Xu, Z.E. Newby, and M.C. Desai. Tetrahedron Letters, 2017. 58(12): p. 1123-1126. ISI[000397376600010].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000399167900012">Synthesis of 3-Oxoesters and Functional Derivatives of Pyrimidin-4(3H)-one Based on 1-(2,6-Dihalophenyl)cyclopropan-1-carboxylic acids.</a> Novakov, I.A., A.S. Yablokov, M.B. Navrotskii, A.S. Mkrtchyan, A.A. Vernigora, A.S. Babushkin, V.V. Kachala, and E.A. Ruchko. Russian Journal of General Chemistry, 2017. 87(2): p. 224-230. ISI[000399167900012].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27748043">The Small-molecule 3G11 Inhibits HIV-1 Reverse Transcription.</a> Opp, S., T. Fricke, C. Shepard, D. Kovalskyy, A. Bhattacharya, F. Herkules, D.N. Ivanov, B. Kim, J. Valle-Casuso, and F. Diaz-Griffero. Chemical Biology and Drug Design, 2017. 89(4): p. 608-618. PMID[27748043]. PMCID[PMC5378662].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000397478700011">Synthesis of Peptide-functionalized Poly(bis-sulfone) Copolymers Regulating HIV-1 Entry and Cancer Stem Cell Migration.</a> Riegger, A., C.J. Cheni, O. Zirafi, N. Daiss, D. Mukherji, K. Walter, Y. Tokura, B. Stockle, K. Kremer, F. Kirchhoff, D.Y.W. Ng, P.C. Hermann, J. Munch, and T. Weil. ACS Macro Letters, 2017. 6(3): p. 241-246. ISI[000397478700011].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000395672000003">Quantitative Structure-Activity Relationship and Docking Studies on a Series of Oxadiazole and Triazole Substituted Naphthyridines as HIV-1 Integrase Inhibitors.</a> Shaik, B., V. Agrawal, S.P. Gupta, and U. Menon. Letters in Drug Design &amp; Discovery, 2017. 14(1): p. 10-27. ISI[000395672000003].
    <br />
    <b>[WOS]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">27. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27928057">Modulation of the Splicing Regulatory Function of SRSF10 by a Novel Compound That Impairs HIV-1 Replication.</a> Shkreta, L., M. Blanchette, J. Toutant, E. Wilhelm, B. Bell, B.A. Story, A. Balachandran, A. Cochrane, P.K. Cheung, P.R. Harrigan, D.S. Grierson, and B. Chabot. Nucleic Acids Research, 2017. 45(7): p. 4051-4067. PMID[27928057]. PMCID[PMC5397194].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28254698">Natural Product-inspired Esters and Amides of Ferulic and Caffeic acid as Dual Inhibitors of HIV-1 Reverse Transcriptase</a>. Sonar, V.P., A. Corona, S. Distinto, E. Maccioni, R. Meleddu, B. Fois, C. Floris, N.V. Malpure, S. Alcaro, E. Tramontano, and F. Cottiglia. European Journal of Medicinal Chemistry, 2017. 130: p. 248-260. PMID[28254698].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">29. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28121713">A Novel HIV-1 gp41 Tripartite Model for Rational Design of HIV-1 Fusion Inhibitors with Improved Antiviral Activity.</a> Su, S., Q. Wang, W. Xu, F. Yu, C. Hua, Y. Zhu, S. Jiang, and L. Lu. AIDS, 2017. 31(7): p. 885-894. PMID[28121713].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28236450">Evaluation of 4-Thiazolidinone Derivatives as Potential Reverse Transcriptase Inhibitors against HIV-1 Drug Resistant Strains.</a> Suryawanshi, R., S. Jadhav, N. Makwana, D. Desai, D. Chaturbhuj, A. Sonawani, S. Idicula-Thomas, V. Murugesan, S.B. Katti, S. Tripathy, R. Paranjape, and S. Kulkarni. Bioorganic Chemistry, 2017. 71: p. 211-218. PMID[28236450].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28202766">Inhibition of HIV-1 Maturation via Small-molecule Targeting of the Amino-terminal Domain in the Viral Capsid Protein.</a> Wang, W., J. Zhou, U.D. Halambage, K.A. Jurado, A.V. Jamin, Y. Wang, A.N. Engelman, and C. Aiken. Journal of Virology, 2017. 91(9). PMID[28202766]. PMCID[PMC5391466].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">32. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28100616">The V3 Loop of HIV-1 Env Determines Viral Susceptibility to IFITM3 Impairment of Viral Infectivity.</a> Wang, Y., Q. Pan, S. Ding, Z. Wang, J. Yu, A. Finzi, S.L. Liu, and C. Liang. Journal of Virology, 2017. 91(7). PMID[28100616]. PMCID[PMC5355610].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28304114">Antiviral and Cytostatic Evaluation of 5-(1-Halo-2-sulfonylvinyl)- and 5-(2-Furyl)uracil Nucleosides.</a> Wen, Z., S.H. Suzol, J. Peng, Y. Liang, R. Snoeck, G. Andrei, S. Liekens, and S.F. Wnuk. Archiv der Pharmazie, 2017. 350(3-4): e1700023. PMID[28304114].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">34. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27353167">Isolation and anti-HIV-1 Integrase Activity of Lentzeosides A-F from Extremotolerant Lentzea sp. H45, a Strain Isolated from a High-altitude Atacama Desert Soil.</a> Wichner, D., H. Idris, W.E. Houssen, A.R. McEwan, A.T. Bull, J.A. Asenjo, M. Goodfellow, M. Jaspars, R. Ebel, and M.E. Rateb. Journal of Antibiotics, 2017. 70(4): p. 448-453. PMID[27353167].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28314598">Design and Synthesis of Hybrids of Diarylpyrimidines and Diketo Acids as HIV-1 Inhibitors.</a> Xue, P., H.H. Lu, Y.Y. Zhu, X.L. Ju, C. Pannecouque, X.J. Zheng, G.Y. Liu, X.L. Zhang, and S.X. Gu. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(8): p. 1640-1643. PMID[28314598].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28229940">New Phenylpropanoid and 6H-diBenzo[b,d]pyran-6-one Derivatives from the Stems of Dasymaschalon rostratum.</a> Yu, Z.X., Z.G. Niu, X.B. Li, C.J. Zheng, X.M. Song, G.Y. Chen, X.P. Song, C.R. Han, and S.X. Wu. Fitoterapia, 2017. 118: p. 27-31. PMID[28229940].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28110956">Anti-HIV Diphyllin glycosides from Justicia gendarussa.</a> Zhang, H.J., E. Rumschlag-Booms, Y.F. Guan, K.L. Liu, D.Y. Wang, W.F. Li, V.H. Nguyen, N.M. Cuong, D.D. Soejarto, H.H. Fong, and L. Rong. Phytochemistry, 2017. 136: p. 94-100. PMID[28110956].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28371092">Total Syntheses of Sesterterpenoid Ansellones A and B, and Phorbadione.</a> Zhang, W., H. Yao, J. Yu, Z. Zhang, and R. Tong. Angewandte Chemie, 2017. 56(17): p. 4787-4791. PMID[28371092].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p><br />

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28275190">Identification of SERINC5-001 as the Predominant Spliced Isoform for HIV-1 Restriction.</a> Zhang, X., T. Zhou, J. Yang, Y. Lin, J. Shi, X. Zhang, D.A. Frabutt, X. Zeng, S. Li, P.J. Venta, and Y.H. Zheng. Journal of Virology, 2017. 91(10): e00137-17. PMID[28275190].
    <br />
    <b>[PubMed]</b>. HIV_04_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">40. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170330&amp;CC=WO&amp;NR=2017051355A1&amp;KC=A1">Preparation of Triterpene Compounds with HIV Maturation Inhibitory Activity.</a> Johns, B.A. Patent. 2017. 2016-IB55676 2017051355: 110pp.
    <br />
    <b>[Patent]</b>. HIV_04_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
