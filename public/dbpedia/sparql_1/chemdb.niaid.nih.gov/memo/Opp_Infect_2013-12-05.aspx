

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-12-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="K6zu5Ry25fWsMv6wBqXi3Zpvn2CR0yklQO8mavl/nalVFifw28TOQfs1orZuiJPms/AL17DQCf6fVb5/2O8DN3iyxXTmMUWHAm2VXdnSYJigEtcYeskSG3bDsgE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1788A69E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: November 22 - December 5, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24147825">2&#39;-Deoxyuridine 5&#39;-monophosphate Substrate Displacement in Thymidylate Synthase through 6-Hydroxy-2H-naphtho[1,8-bc]furan-2-one Derivatives</a><i>.</i> Ferrari, S., S. Calo, R. Leone, R. Luciani, L. Costantino, S. Sammak, F. Di Pisa, C. Pozzi, S. Mangani, and M.P. Costi. Journal of Medicinal Chemistry, 2013. 56(22): p. 9356-9360. PMID[24147825].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060869">A Cysteine Protease Inhibitor Rescues Mice from a Lethal Cryptosporidium parvum Infection</a><i>.</i> Ndao, M., M. Nath-Chowdhury, M. Sajid, V. Marcus, S.T. Mashiyama, J. Sakanari, E. Chow, Z. Mackey, K.M. Land, M.P. Jacobson, C. Kalyanaraman, J.H. McKerrow, M.J. Arrowood, and C.R. Caffrey. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6063-6073. PMID[24060869].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24096605">Effects of Dextran Sulfates on the Acute Infection and Growth Stages of Toxoplasma gondii</a><i>.</i> Ishiwa, A., K. Kobayashi, H. Takemae, T. Sugi, H. Gong, F.C. Recuenco, F. Murakoshi, A. Inomata, T. Horimoto, and K. Kato. Parasitology Research, 2013. 112(12): p. 4169-4176. PMID[24096605].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060867">Fluconazole Assists Berberine to Kill Fluconazole-resistant Candida albicans</a><i>.</i> Li, D.D., Y. Xu, D.Z. Zhang, H. Quan, E. Mylonakis, D.D. Hu, M.B. Li, L.X. Zhao, L.H. Zhu, Y. Wang, and Y.Y. Jiang. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6016-6027. PMID[24060867].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24103090">Phenothiazine is a Potent Inhibitor of Prostaglandin E2 Production by Candida albicans Biofilms</a><i>.</i> Ells, R., G. Kemp, J. Albertyn, J.L. Kock, and C.H. Pohl. FEMS Yeast Research, 2013. 13(8): p. 849-855. PMID[24103090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23449869">Small Molecules Inhibit Growth, Viability and Ergosterol Biosynthesis in Candida albicans</a><i>.</i> Rajput, S.B. and S.M. Karuppayil. SpringerPlus, 2013. 2(1): p. 26. PMID[23449869].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24276426">Synthesis and Anti-toxoplasmosis Activity of 4-Arylquinoline-2-carboxylate Derivatives</a><i>.</i> McNulty, J., R. Vemula, C. Bordon, R. Yolken, and L. Jones-Brando. Organic &amp; Biomolecular Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24276426].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24028969">Monoterpenes with Antibacterial Activities from a Cameroonian Medicinal Plant Canthium multiflorum (Rubiaceae)</a><i>.</i> Kouam, S.F., A.W. Ngouonpe, A. Bullach, M. Lamshoft, G.M. Kuigoua, and M. Spiteller. Fitoterapia, 2013. 91: p. 199-204. PMID[24028969].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060877">In Vitro Susceptibility of Mycobacterium tuberculosis to Trimethoprim and Sulfonamides in France.</a> Ameen, S.M. and M. Drancourt. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6370-6371. PMID[24060877].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24251446">Lipoamide Channel-binding Sulfonamides Selectively Inhibit Mycobacterial Lipoamide Dehydrogenase.</a> Bryk, R., N. Arango, C. Maksymiuk, A. Balakrishnan, Y.T. Wu, C.H. Wong, T. Masquelin, P. Hipskind, C.D. Lima, and C. Nathan. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24251446].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24224794">Hytramycins V and I, Anti-Mycobacterium tuberculosis Hexapeptides from a Streptomyces hygroscopicus Strain.</a> Cai, G., J.G. Napolitano, J.B. McAlpine, Y. Wang, B.U. Jaki, J.W. Suh, S.H. Yang, I.A. Lee, S.G. Franzblau, G.F. Pauli, and S. Cho. Journal of Natural Products, 2013. 76(11): p. 2009-2018. PMID[24224794].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041897">In Vitro Cross-linking of Mycobacterium tuberculosis Peptidoglycan by L,D-Transpeptidases and Inactivation of These Enzymes by Carbapenems.</a> Cordillot, M., V. Dubee, S. Triboulet, L. Dubost, A. Marie, J.E. Hugonnet, M. Arthur, and J.L. Mainardi. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 5940-5945. PMID[24041897].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060878">Whole-cell Screening-based Identification of Inhibitors against the Intraphagosomal Survival of Mycobacterium tuberculosis.</a> Khare, G., P. Kumar, and A.K. Tyagi. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6372-6377. PMID[24060878].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23790070">Synthesis and 3D-QSAR Analysis of 2-Chloroquinoline Derivatives as H37 RV MTB Inhibitors.</a> Khunt, R.C., V.M. Khedkar, and E.C. Coutinho. Chemical Biology &amp; Drug Design, 2013. 82(6): p. 669-684. PMID[23790070].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060876">Can Inhibitor-Resistant Substitutions in the Mycobacterium tuberculosis beta-Lactamase BlaC Lead to Clavulanate Resistance?: A Biochemical Rationale for the Use of beta-Lactam-beta-Lactamase Inhibitor Combinations.</a> Kurz, S.G., K.A. Wolff, S. Hazra, C.R. Bethel, A.M. Hujer, K.M. Smith, Y. Xu, L.W. Tremblay, J.S. Blanchard, L. Nguyen, and R.A. Bonomo. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6085-6096. PMID[24060876].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24060870">Gallium Nitrate Is Efficacious in Murine Models of Tuberculosis and Inhibits Key Bacterial Fe-Dependent Enzymes.</a> Olakanmi, O., B. Kesavalu, R. Pasula, M.Y. Abdalla, L.S. Schlesinger, and B.E. Britigan. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 6074-6080. PMID[24060870].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24080479">Activity and Interactions of Levofloxacin, Linezolid, Ethambutol and Amikacin in Three-drug Combinations against Mycobacterium tuberculosis Isolates in a Human Macrophage Model.</a> Rey-Jurado, E., G. Tudo, D. Soy, and J. Gonzalez-Martin. International Journal of Antimicrobial Agents, 2013. 42(6): p. 524-530. PMID[24080479].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24267469">In Vitro Anti-mycobacterial Activity of Nine Medicinal Plants Used by Ethnic Groups in Sonora, Mexico.</a> Robles-Zepeda, R.E., E.W. Coronado-Aceves, C.A. Velazquez-Contreras, E. Ruiz-Bustos, M. Navarro-Navarro, and A. Garibay-Escobar. BMC Complementary and Alternative Medicine, 2013. 13: p. 329. PMID[24267469].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24215368">Azaindoles: Noncovalent DprE1 Inhibitors from Scaffold Morphing Efforts, Kill Mycobacterium tuberculosis and Are Efficacious in Vivo.</a> Shirude, P.S., R. Shandil, C. Sadler, M. Naik, V. Hosagrahara, S. Hameed, V. Shinde, C. Bathula, V. Humnabadkar, N. Kumar, J. Reddy, V. Panduga, S. Sharma, A. Ambady, N. Hegde, J. Whiteaker, R.E. McLaughlin, H. Gardner, P. Madhavapeddi, V. Ramachandran, P. Kaur, A. Narayan, S. Guptha, D. Awasthy, C. Narayan, J. Mahadevaswamy, K. Vishwas, V. Ahuja, A. Srivastava, K. Prabhakar, S. Bharath, R. Kale, M. Ramaiah, N.R. Choudhury, V.K. Sambandamurthy, S. Solapure, P.S. Iyer, S. Narayanan, and M. Chatterji. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24215368].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24028327">Identification and Polymorphism Discovery of the Cathelicidins, Lf-CATHs in Ranid Amphibian (Limnonectes fragilis).</a> Yu, H., S. Cai, J. Gao, S. Zhang, Y. Lu, X. Qiao, H. Yang, and Y. Wang. The FEBS Journal, 2013. 280(23): p. 6022-6032. PMID[24028327].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24285734">Ethambutol Resistance Determined by Broth Dilution Method Better Correlates with embB Mutations in MDR Tuberculosis Isolates.</a> Zhang, Z., Y. Wang, Y. Pang, and K.M. Kam. Journal of Clinical Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[24285734].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24139941">3,5-bis(Benzylidene)-4-piperidones and Related N-acyl Analogs: A Novel Cluster of Antimalarials Targeting the Liver Stage of Plasmodium falciparum</a><i>.</i> Das, U., R.S. Singh, J. Alcorn, M.R. Hickman, R.J. Sciotti, S.E. Leed, P.J. Lee, N. Roncal, and J.R. Dimmock. Bioorganic &amp; Medicinal Chemistry, 2013. 21(23): p. 7250-7256. PMID[24139941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24041883">Antiplasmodial Activity and Mechanism of Action of RSM-932A, a Promising Synergistic Inhibitor of Plasmodium falciparum Choline Kinase</a><i>.</i> Zimmerman, T., C. Moneriz, A. Diez, J.M. Bautista, T. Gomez Del Pulgar, A. Cebrian, and J.C. Lacal. Antimicrobial Agents and Chemotherapy, 2013. 57(12): p. 5878-5888. PMID[24041883].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23992634">A Primaquine-Chloroquine Hybrid with Dual Activity against Plasmodium Liver and Blood Stages</a><i>.</i> Lodige, M., M.D. Lewis, E.S. Paulsen, H.L. Esch, G. Pradel, L. Lehmann, R. Brun, G. Bringmann, and A.K. Mueller. International Journal of Medical Microbiology, 2013. 303(8): p. 539-547. PMID[23992634].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br />   

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24121016">Computational and Experimental Analysis Identified 6-Diazo-5-oxonorleucine as a Potential Agent for Treating Infection by Plasmodium falciparum</a><i>.</i> Plaimas, K., Y. Wang, S.O. Rotimi, G. Olasehinde, S. Fatumo, M. Lanzer, E. Adebiyi, and R. Konig. Infection, Genetics and Evolution, 2013. 20: p. 389-395. PMID[24121016].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24271081">In Vivo Antimalarial Evaluation of MAMA Decoction on Plasmodium berghei in Mice</a><i>.</i> Adepiti, A.O., A.A. Elujoba, and O.O. Bolaji. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[24271081].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24195442">Structure Elucidation and Antimalarial Activity of Apicidin F: An Apicidin-like Compound Produced by Fusarium fujikuroi</a><i>.</i> von Bargen, K.W., E.M. Niehaus, K. Bergander, R. Brun, B. Tudzynski, and H.U. Humpf. Journal of Natural Products, 2013. 76(11): p. 2136-2140. PMID[24195442].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24158489">Synergy of Ferrous Ion on 5-Aminolevulinic acid-mediated Growth Inhibition of Plasmodium falciparum</a><i>.</i> Komatsuya, K., M. Hata, E.O. Balogun, K. Hikosaka, S. Suzuki, K. Takahashi, T. Tanaka, M. Nakajima, S. Ogura, S. Sato, and K. Kita. Journal of Biochemistry, 2013. 154(6): p. 501-504. PMID[24158489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24148834">Synthesis and Evaluation of the Antimalarial, Anticancer, and Caspase 3 Activities of Tetraoxane Dimers</a><i>.</i> Amewu, R.K., J. Chadwick, A. Hussain, S. Panda, R. Rinki, O. Janneh, S.A. Ward, C. Miguel, H. Burrell-Saward, L. Vivas, and P.M. O&#39;Neill. Bioorganic &amp; Medicinal Chemistry, 2013. 21(23): p. 7392-7397. PMID[24148834].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1122-120513.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326367200016">Identification of 1-[4-Benzyloxyphenyl)-but-3-enyl]-1H-azoles as New Class of Antitubercular and Antimicrobial Agents.</a> Anand, N., K.K.G. Ramakrishna, M.P. Gupt, V. Chaturvedi, S. Singh, K.K. Srivastava, P. Sharma, N. Rai, R. Ramachandran, A.K. Dwivedi, V. Gupta, B. Kumar, S. Pandey, P.K. Shukla, S.K. Pandey, J. Lal, and R.P. Tripathi. ACS Medicinal Chemistry Letters, 2013. 4(10): p. 958-963. ISI[000326367200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326064900016">Antifungal Effect of CopA3 Monomer Peptide via Membrane-active Mechanism and Stability to Proteolysis of Enantiomeric D-CopA3.</a> Choi, H., J.S. Hwang, H. Kim, and D.G. Lee. Biochemical and Biophysical Research Communications, 2013. 440(1): p. 94-98. ISI[000326064900016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326296800016">Synthesis and Evaluation of Some Novel Precursors of Oxazolidinone Analogues of Chloroquinoline for Their Antimicrobial and Cytotoxic Potential.</a> Devi, K., Y. Asmat, M. Agrawal, S. Sharma, and J. Dwivedi. Journal of Chemical Sciences, 2013. 125(5): p. 1093-1101. ISI[000326296800016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br />   

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326121400017">Identification of Novel Inhibitors of Nonreplicating Mycobacterium tuberculosis using a Carbon Starvation Model.</a> Grant, S.S., T. Kawate, P.P. Nag, M.R. Silvis, K. Gordon, S.A. Stanley, E. Kazyanskaya, R. Nietupski, A. Golas, M. Fitzgerald, S. Cho, S.G. Franzblau, and D.T. Hung. ACS Chemical Biology, 2013. 8(10): p. 2224-2234. ISI[000326121400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326078300012">Structure-Activity Relationship, Cytotoxicity and Mode of Action of 2-Ester-substituted 1,5-Benzothiazepines as Potent Antifungal Agents.</a> Kang, W., X.Q. Du, L.Z. Wang, L.J. Hu, Y.H. Dong, Y.Q. Bian, and Y. Li. Chinese Journal of Chemistry, 2013. 31(10): p. 1305-1314. ISI[000326078300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326344200006">Synthesis and Characterization of 1-Carboxyphenothiazine Derivatives Bearing Nitrogen Mustard as Promising Class of Antitubercular Agents.</a> Kataria, V.B., M.J. Solanki, A.R. Trivedi, and V.H. Shah. Letters in Drug Design &amp; Discovery, 2013. 10(10): p. 951-956. ISI[000326344200006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326296800005">An Efficient Synthesis of 3&#39;-Indolyl Substituted Pyrido[1,2-a]benzimidazoles as Potential Antimicrobial and Antioxidant Agents.</a> Kathrotiya, H.G. and M.P. Patel. Journal of Chemical Sciences, 2013. 125(5): p. 993-1001. ISI[000326296800005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326340200022">GC-MS Analysis and Antimicrobial Activity of Essential Oil of Pinus roxburghii Sarg. from Northern India.</a> Kaushik, D., P. Kaushik, A. Kumar, A.C. Rana, C. Sharma, and K.R. Aneja. Journal of Essential Oil Bearing Plants, 2013. 16(4): p. 563-567. ISI[000326340200022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326365900008">1H-1,2,3-Triazole-tethered Isatin-ferrocene and Isatin-ferrocenylchalcone Conjugates: Synthesis and in Vitro Antitubercular Evaluation.</a> Kumar, K., S. Carrere-Kremer, L. Kremer, Y. Guerardel, C. Biot, and V. Kumar. Organometallics, 2013. 32(20): p. 110-116. ISI[000326365900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325989600007">Systematic Study of Non-natural Short Cationic Lipopeptides as Novel Broad-Spectrum Antimicrobial Agents.</a> Lohan, S., S.S. Cameotra, and G.S. Bisht. Chemical Biology &amp; Drug Design, 2013. 82(5): p. 557-566. ISI[000325989600007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325887300053">Discovery of Mycobacterium tuberculosis Protein Tyrosine Phosphatase B (PtpB) Inhibitors from Natural Products.</a> Mascarello, A., M. Mori, L.D. Chiaradia-Delatorre, A.C.O. Menegatti, F. Delle Monache, F. Ferrari, R.A. Yunes, R.J. Nunes, H. Terenzi, B. Botta, and M. Botta. Plos One, 2013. 8(10): p. e77081. ISI[000325887300053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326199200005">Reactions of Tin and Triorganotin(IV) Isopropoxides with Thymol Derivative: Synthesis, Characterization and in Vitro Antimicrobial Screening.</a> Matela, G., R. Aman, C. Sharma, and S. Chaudhary. Journal of the Serbian Chemical Society, 2013. 78(9): p. 1323-1333. ISI[000326199200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325909700020">Synthesis and Antimicrobial Activity of Some Novel 2-Aryliden-hydrazone-thiazoles.</a> Nastasa, C., B. Tiperciuc, S. Oniga, A. Pirnau, M. Ionescu, D. Tarlungeanu, M. Palage, P. Verite, and O. Oniga. Farmacia, 2013. 61(5): p. 1027-1036. ISI[000325909700020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326121400023">PIF-Pocket as a Target for C. albicans Pkh Selective Inhibitors.</a> Pastor-Flores, D., J.O. Schulze, A. Bahi, R. Giacometti, J. Ferrer-Dalmau, S. Passeron, M. Engel, E. Suss, A. Casamayor, and R.M. Biondi. ACS Chemical Biology, 2013. 8(10): p. 2283-2292. ISI[000326121400023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326244600011">6-Alkyl-, 6-Aryl- or 6-Hetaryl-7-deazapurine Ribonucleosides as Inhibitors of Human or MTB Adenosine Kinase and Potential Antimycobacterial Agents.</a> Perlikova, P., P. Konecny, P. Naus, J. Snasel, I. Votruba, P. Dzubak, I. Pichova, M. Hajduch, and M. Hocek. MedChemComm, 2013. 4(11): p. 1497-1500. ISI[000326244600011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325909700008">Culture Methods Versus Flow Cytometry for the Comparative Assessment of the Antifungal Activity of Eugenia caryophyllata thunb. (Myrtaceae) Essential Oil.</a> Saviuc, C.M., A.M. Grumezescu, C. Bleotu, A.M. Holban, M.C. Chfiriuc, P. Balaure, G. Predan, and V. Lazar. Farmacia, 2013. 61(5): p. 912-919. ISI[000325909700008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000325919202168">New Nucleoside Inhibitors of M. tuberculosis Growth.</a> Shmalenyuk, E.R., L.N. Chernousova, and L.A. Aleksandrova. FEBS Journal, 2013. 280: p. 368-369. ISI[000325919202168].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326262600004">Novel Inhibitors of Mycobacterium tuberculosis Growth Based on Modified Pyrimidine Nucleosides and Their Analogues.</a> Shmalenyuk, E.R., S.N. Kochetkov, and L.A. Alexandrova. Russian Chemical Reviews, 2013. 82(9): p. 896-915. ISI[000326262600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1122-120513.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
