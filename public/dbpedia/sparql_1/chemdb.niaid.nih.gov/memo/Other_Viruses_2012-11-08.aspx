

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-11-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jSD7dL4Rl2rKkz3NfPUICOCS2s2Xee8ZByX5l2VbVuWHNYLen24tspT7FFQpWoQE1wZQtmVCG/d7er7r2NEOyhXQ33OOUAmicguPm+EK43l9eH/OQnldA/BjhVM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="28F6FE56" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: October 26 - November 8, 2012</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22982394">Hepatoprotective and Antiviral Properties of Isochlorogenic Acid A from Laggera alata against Hepatitis B Virus Infection</a><i>.</i> Hao, B.J., Y.H. Wu, J.G. Wang, S.Q. Hu, D.J. Keil, H.J. Hu, J.D. Lou, and Y. Zhao. Journal of Ethnopharmacology, 2012. 144(1): p. 190-194; PMID[22982394].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1026-110812.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22796893">Hepatitis C Virus Non-structural 5B Protein Interacts with Cyclin A2 and Regulates Viral Propagation</a><i>.</i> Pham, L.V., H.T. Ngo, Y.S. Lim, and S.B. Hwang. Journal of Hepatology, 2012. 57(5): p. 960-966; PMID[22796893].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23063849">Inhibition of Norovirus Replication by the Nucleoside Analogue 2&#39;-C-Methylcytidine</a><i>.</i> Rocha-Pereira, J., D. Jochmans, K. Dallmeier, P. Leyssen, R. Cunha, I. Costa, M.S. Nascimento, and J. Neyts. Biochemical and Biophysical Research Communications, 2012. 427(4): p. 796-800; PMID[23063849].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1026-110812.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22908168">Human Cytomegalovirus Inhibitor AL18 Also Possesses Activity against Influenza A and B Viruses</a><i>.</i> Muratore, G., B. Mercorelli, L. Goracci, G. Cruciani, P. Digard, G. Palu, and A. Loregian. Antimicrobial Agents and Chemotherapy, 2012. 56(11): p. 6009-6013; PMID[22908168].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23122474">Ganciclovir for Cytomegalovirus Retinitis</a><i>.</i> Sandler, S.F. and J.B. Rosenberg. Ophthalmology, 2012. 119(11): p. 2418-2419; PMID[23122474].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1026-110812.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306826300028">Preclinical Characterization of JTK-853, a Novel Nonnucleoside Inhibitor of the Hepatitis C Virus RNA-dependent RNA Polymerase</a><i>.</i> Ando, I., T. Adachi, N. Ogura, Y. Toyonaga, K. Sugimoto, H. Abe, M. Kamada, and T. Noguchi. Antimicrobial Agents and Chemotherapy, 2012. 56(8): p. 4250-4256; ISI[000306826300028].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308833600004">Synthesis of Novel Mannoside Glycolipid Conjugates for Inhibition of HIV-1 Trans-infection</a><i>.</i> Dehuyser, L., E. Schaeffer, O. Chaloin, C.G. Mueller, R. Baati, and A. Wagner. Bioconjugate Chemistry, 2012. 23(9): p. 1731-1739; ISI[000308833600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p> 

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000308807900021">Anti-Hepatitis C Virus Activity and Toxicity of Type III Phosphatidylinositol-4-kinase Beta Inhibitors</a><i>.</i> LaMarche, M.J., J. Borawski, A. Bose, C. Capacci-Daniel, R. Colvin, M. Dennehy, J. Ding, M. Dobler, J. Drumm, L.A. Gaither, J. Gao, X. Jiang, K. Lin, U. McKeever, X. Puyang, P. Raman, S. Thohan, R. Tommasi, K. Wagner, X. Xiong, T. Zabawa, S. Zhu, and B. Wiedmann. Antimicrobial Agents and Chemotherapy, 2012. 56(10): p. 5149-5156; ISI[000308807900021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000306806600130">Silibinin Inhibits HIV-1 Infection by Reducing Cellular Activation and Proliferation</a><i>.</i> McClure, J., E.S. Lovelace, S. Elahi, N.J. Maurice, J. Wagoner, J. Dragavon, J.E. Mittler, Z. Kraft, L. Stamatatos, H. Horton, S.C. De Rosa, R.W. Coombs, and S.J. Polyak. Plos One, 2012. 7(7); ISI[000306806600130].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309105200004">Biosynthesis of anti-HCV Compounds Using Thermophilic Microorganisms</a><i>.</i> Rivero, C.W., E.C. De Benedetti, J.E. Sambeth, M.E. Lozano, and J.A. Trelles. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(19): p. 6059-6062; ISI[000309105200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000307722000019">Hepatitis C RNA Clearance after Treatment with Ezetimibe</a><i>.</i> Soza, A. Liver International, 2012. 32(8): p. 1323-1324; ISI[000307722000019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1026-110812.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
