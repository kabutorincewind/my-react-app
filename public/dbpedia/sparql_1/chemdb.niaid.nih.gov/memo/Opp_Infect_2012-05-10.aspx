

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-05-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QKptA+M6soP36YkVDhwXko7m3XZZRUORISTa2dI/svmRqUX8c6U2L30fZ4pdAhQHP+ajyF2i03jGwWGaHRa1jQX+22ZOD+5KdIYveYCEY/wXnyZJstdkiBnpIjU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A2266D17" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 27 - May 10, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21914003">Aloe Vera Extract Reduces Both Growth and Germ Tube Formation by Candida albicans.</a> Bernardes, I., M.P. Felipe Rodrigues, G.K. Bacelli, E. Munin, L.P. Alves, and M.S. Costa. Mycoses, 2012. 55(3): p. 257-261; PMID[21914003].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22564841">Potent Synergistic Effect of Doxycycline with Fluconazole against Candida albicans is Mediated by Interference with Iron Homeostasis.</a> Fiori, A. and P. Van Dijck. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22564841].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22565489">Comparison of in Vitro and Vivo Efficacy of Caspofungin against Candida parapsilosis, C. orthopsilosis, C. metapsilosis and C. albicans.</a> Foldi, R., R. Kovacs, R. Gesztelyi, G. Kardos, R. Berenyi, B. Juhasz, J. Szilagyi, J. Mozes, and L. Majoros. Mycopathologia, 2012. <b>[Epub ahead of print]</b>; PMID[22565489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22543256">Minimal Inhibitory Concentration (MIC) of Caspofungin and Itraconazole Inhibiting Growth of Candida Strains Calculated from the Linear Regression Equation.</a> Kurnatowska, A., P. Kurnatowski, E. Horwatt-Bozyczko, and A.J. Kurnatowska. Advances in Medical Sciences, 2012. <b>[Epub ahead of print]</b>: p. 1-4; PMID[22543256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22555293">Chemical Composition, Antimicrobial and Antitumor Activities of the Essential Oils and Crude Extracts of Euphorbia macrorrhiza.</a> Lin, J., J. Dou, J. Xu, and H.A. Aisa. Molecules (Basel, Switzerland), 2012. 17(5): p. 5030-5039; PMID[22555293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22128758">In Vitro Fungicidal Photodynamic Effect of Hypericin on Candida Species(Dagger).</a> Rezusta, A., P. Lopez-Chicon, M.P. Paz-Cristobal, M. Alemany-Ribes, D. Royo-Diez, M. Agut, C. Semino, S. Nonell, M.J. Revillo, C. Aspiroz, and Y. Gilaberte. Photochemistry and Photobiology, 2012. 88(3): p. 613-619; PMID[22128758].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227461">Cinnamic Aldehydes Affect Hydrolytic Enzyme Secretion and Morphogenesis in Oral Candida Isolates.</a> Shreaz, S., R. Bhatia, N. Khan, I.K. Maurya, S.I. Ahmad, S. Muralidhar, N. Manzoor, and L.A. Khan. Microbial Pathogenesis, 2012. 52(5): p. 251-258; PMID[22227461].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22535361">Antifungal Effect of 4-Arylthiosemicarbazides against Candida Species. Search for Molecular Basis of Antifungal Activity of Thiosemicarbazide Derivatives.</a> Siwek, A., J. Stefanska, K. Dzitko, and A. Ruszczak. Journal of Molecular Modeling, 2012. <b>[Epub ahead of print]</b>; PMID[22535361].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22360686">Antimicrobial Metabolites from the Paracel Islands Sponge Agelas mauritiana.</a> Yang, F., M.T. Hamann, Y. Zou, M.Y. Zhang, X.B. Gong, J.R. Xiao, W.S. Chen, and H.W. Lin. Journal of Natural Products, 2012. 75(4): p. 774-778; PMID[22360686].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p>
    <p> </p>
    
    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22551300">Increase of Leishmanicidal and Tubercular Activities Using Steroids Linked to Aminoquinoline.</a> Antinarelli, L.M., A.M. Carmo, F.R. Pavan, C.Q. Leite, A.D. Da Silva, E.S. Coimbra, and D.B. Salunke. Organic and Medicinal Chemistry Letters, 2012. 2(1): p. 16; PMID[22551300].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22553981">Synthesis and in Vitro Antibacterial Activity of Novel 3-Azabicyclo[3.3.0]octanyl oxazolidinones.</a> Bhattarai, D., S.H. Lee, S.H. Seo, G. Nam, S.B. Kang, A.N. Pae, E.E. Kim, and G. Keum. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22553981].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22471367">Synthesis, Characterization and in-Vitro Antitubercular Activity of Isoniazid-Gelatin Conjugate.</a> Cassano, R., S. Trombino, T. Ferrarelli, P. Cavalcanti, C. Giraldi, F. Lai, G. Loy, and N. Picci. The Journal of Pharmacy and Pharmacology, 2012. 64(5): p. 712-718; PMID[22471367].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22564831">Activities of Moxifloxacin in Combination with Macrolide against Clinical Isolates of Mycobacterium abscessus and Mycobacterium massiliense.</a> Choi, G.E., K.N. Min, C.J. Won, K. Jeon, S.J. Shin, and W.J. Koh. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22564831].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p>
    <p> </p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22566611">Generation of Quinolone Antimalarials Targeting the Plasmodium falciparum Mitochondrial Respiratory Chain for the Treatment and Prophylaxis of Malaria.</a> Biagini, G.A., N. Fisher, A.E. Shone, M.A. Mubaraki, A. Srivastava, A. Hill, T. Antoine, A.J. Warman, J. Davies, C. Pidathala, R.K. Amewu, S.C. Leung, R. Sharma, P. Gibbons, D.W. Hong, B. Pacorel, A.S. Lawrenson, S. Charoensutthivarakul, L. Taylor, O. Berger, A. Mbekeani, P.A. Stocks, G.L. Nixon, J. Chadwick, J. Hemingway, M.J. Delves, R.E. Sinden, A.M. Zeeman, C.H. Kocken, N.G. Berry, P.M. O&#39;Neill, and S.A. Ward. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22566611].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22566106">HPLC Analysis of Stephania Rotunda Extracts and Correlation with Antiplasmodial Activity.</a> Bory, S., S.S. Bun, B. Baghdikian, A. Dumetre, S. Hutter, F. Mabrouki, H. Bun, R. Elias, N. Azas, and E. Ollivier. Phytotherapy research: PTR, 2012. <b>[Epub ahead of print]</b>; PMID[22566106].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22544438">Evaluation of Bis-Alkylamidoxime O-Alkylsulfonates as Orally Available Antimalarials.</a> Degardin, M., S. Wein, S. Gouni, C. Tran Van Ba, J.F. Duckert, T. Durand, R. Escale, H. Vial, and Y. Vo-Hoang. ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22544438].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22424611">Synthesis and in Vitro Antiplasmodial Activities of Fluoroquinolone Analogs.</a> Dixit, S.K., N. Mishra, M. Sharma, S. Singh, A. Agarwal, S.K. Awasthi, and V.K. Bhasin. European Journal of Medicinal Chemistry, 2012. 51: p. 52-59; PMID[22424611].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22440261">Antiparasitic Activities of Two Sesquiterpenic Lactones Isolated from Acanthospermum hispidum D.C.</a> Ganfon, H., J. Bero, A.T. Tchinda, F. Gbaguidi, J. Gbenou, M. Moudachirou, M. Frederich, and J. Quetin-Leclercq. Journal of Ethnopharmacology, 2012. 141(1): p. 411-417; PMID[22440261]. <b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22464685">Antiplasmodial Activity of Novel Keto-Enamine Chalcone-Chloroquine Based Hybrid Pharmacophores.</a> Sashidhara, K.V., M. Kumar, R.K. Modukuri, R.K. Srivastava, A. Soni, K. Srivastava, S.V. Singh, J.K. Saxena, H.M. Gauniyal, and S.K. Puri. Bioorganic &amp; Medicinal Chemistry, 2012. 20(9): p. 2971-2981; PMID[22464685].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p>
    <p> </p>
    
    <h2>ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22550999">Novel 3-Nitro-1H-1,2,4-triazole-Based Amides and Sulfonamides as Potential Anti-trypanosomal Agents.</a> Papadopoulou, M.V., W.D. Bloomer, H.S. Rosenzweig, E. Chatelain, M. Kaiser, S.R. Wilkinson, C. McKenzie, and J.R. Ioset. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22550999].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22510833">In Vitro Evaluation of New Terpenoid Derivatives against Leishmania infantum and Leishmania braziliensis.</a> Ramirez-Macias, I., C. Marin, R. Chahboun, F. Olmo, I. Messouri, O. Huertas, M.J. Rosales, R. Gutierrez-Sanchez, E. Alvarez-Manzaneda, and M. Sanchez-Moreno. Memorias Do Instituto Oswaldo Cruz, 2012. 107(3): p. 370-376; PMID[22510833].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2251062">Indole Alkaloids from Geissospermum reticulatum.</a> Reina, M., W. Ruiz-Mesia, M. Lopez-Rodriguez, L. Ruiz-Mesia, A. Gonzalez-Coloma, and R. Martinez-Diaz. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[22551062].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22559947">Antiprotozoal Sesquiterpene Pyridine Alkaloids from Maytenus ilicifolia.</a> Santos, V.A., L.O. Regasini, C.R. Nogueira, G.D. Passerini, I. Martinez, V.S. Bolzani, M.A. Graminha, R.M. Cicarelli, and M. Furlan. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[22559947].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22037827">In Vitro Antileishmanial and Antitrypanosomal Activities of Five Medicinal Plants from Burkina Faso.</a> Sawadogo, W.R., G. Le Douaron, A. Maciuk, C. Bories, P.M. Loiseau, B. Figadere, I.P. Guissou, and O.G. Nacoulma. Parasitology Research, 2012. 110(5): p. 1779-1783; PMID[22037827].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22543751">Enhanced Leishmanicidal Activity of Cryptopeptide Chimeras from the Active N1 Domain of Bovine Lactoferrin.</a> Silva, T., M.A. Abengozar, M. Fernandez-Reyes, D. Andreu, K. Nazmi, J.G. Bolscher, M. Bastos, and L. Rivas. Amino Acids, 2012. <b>[Epub ahead of print]</b>; PMID[22543751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0427-051012.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00030262000017">Antimicrobial Antioxidant Daucane Sesquiterpenes from Ferula hermonis Boiss</a>. Ibraheim, Z.Z., W.M. Abdel-Mageed, H.Q. Dai, H. Guo, L.X. Zhang, and M. Jaspars. Phytotherapy Research, 2012. 26(4): p. 579-586; ISI[000302620000017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302437200027">Biofilm Inhibition by Cymbopogon citratus and Syzygium aromaticum Essential Oils in the Strains of Candida albicans.</a> Khan, M.S.A. and I. Ahmad. Journal of Ethnopharmacology, 2012. 140(2): p. 416-423; ISI[000302437200027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302607300008">Synthesis and Antimicrobial Evaluation of New Pyrano 4,3-B pyran and Pyrano 3,2-C chromene Derivatives Bearing a 2-Thiophenoxyquinoline Nucleus.</a> Makawana, J.A., M.P. Patel, and R.G. Patel. Archiv Der Pharmazie, 2012. 345(4): p. 314-322; ISI[000302607300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302471300001">Flourensia cernua: Hexane Extracts a Very Active Mycobactericidal Fraction from an Inactive Leaf Decoction against Pansensitive and Panresistant Mycobacterium tuberculosis.</a> Molina-Salinas, G.M., L.M. Pena-Rodriguez, B.D. Mata-Cardenas, F. Escalante-Erosa, S. Gonzalez-Hernandez, V.M.T. de la Cruz, H.G. Martinez-Rodriguez, and S. Said-Fernandez. Evidence-Based Complementary and Alternative Medicine, 2011; ISI[000302471300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302489200018">Sq109 and Pnu-100480 Interact to Kill Mycobacterium tuberculosis in Vitro.</a> Reddy, V.M., T. Dubuisson, L. Einck, R.S. Wallis, W. Jakubiec, L. Ladukto, S. Campbell, and C.A. Nacy. Journal of Antimicrobial Chemotherapy, 2012. 67(5): p. 1163-1166; ISI[000302489200018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302775800012">Synergistic Effect of Two Combinations of Antituberculous Drugs against Mycobacterium tuberculosis.</a> Rey-Jurado, E., G. Tudo, J.A. Martinez, and J. Gonzalez-Martin. Tuberculosis, 2012. 92(3): p. 260-263; ISI[000302775800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302663500021">Symphyocladins A-G: Bromophenol Adducts from a Chinese Marine Red Alga, Symphyocladia latiuscula.</a> Xu, X.L., A.M. Piggott, L.Y. Yin, R.J. Capon, and F.H. Song. Tetrahedron Letters, 2012. 53(16): p. 2103-2106; ISI[000302663500021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302451100016">Total Synthesis and Structure-Activity Relationships of Caspofungin-Like Macrocyclic Antifungal Lipopeptides.</a> Yao, J.Z., H.M. Liu, T. Zhou, H. Chen, Z.Y. Miao, G.Q. Dong, S.Z. Wang, C.Q. Sheng, and W.N.A. Zhang. Tetrahedron, 2012. 68(14): p. 3074-3085; ISI[000302451100016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p> 
    <p> </p>
    
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302665300011">Antifungal and Antibacterial Activity of 3-Alkylpyridinium Polymeric Analogs of Marine Toxins.</a> Zovko, A., M.V. Gabric, K. Sepcic, F. Pohleven, D. Jaklic, N. Gunde-Cimerman, Z.B. Lu, R. Edrada-Ebel, W.E. Houssen, I. Mancini, A. Defant, M. Jaspars, and T. Turk. International Biodeterioration &amp; Biodegradation, 2012. 68: p. 71-77; ISI[000302665300011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0427-051012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
