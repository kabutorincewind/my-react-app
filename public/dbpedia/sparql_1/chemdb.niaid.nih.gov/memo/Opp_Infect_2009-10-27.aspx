

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-10-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KzPdhRUbBwLIivk+RSDkqBNpNDKOEYajosXM0qTIn/gjSqOgyzYxIM/0wS75YVxBxhyASlx+rb15WVvG5nDrx/aTDPU98Y6xzCMNemCLOyDCpl9OfFElX+8abT8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="301F49B0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 14, 2009 -October 27, 2009</p>

    <p class="ListParagraph">1. Pfaller MA, Diekema DJ, Messer SA, Hollis RJ, Jones RN. In vitro activities of caspofungin compared with those of fluconazole and itraconazole against 3,959 clinical isolates of <i>Candida</i> spp., including 157 fluconazole- resistant isolates.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother 2003;47: 1068-71.</p>

    <p class="NoSpacing">DOI 10.1128/AAC.47.3.1068-1071.2003</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="NoSpacing">&nbsp;</p>

    <p class="NoSpacing">2. Mao, J., H. Yuan, Y. Wang, B. Wan, M. Pieroni, Q. Huang, R.B. van Breemen, A.P. Kozikowski, and S.G. Franzblau, From Serendipity to Rational Antituberculosis Drug Discovery of Mefloquine-Isoxazole Carboxylic Acid Esters. J Med Chem, 2009; PMID[19863050] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19863050?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17">http://www.ncbi.nlm.nih.gov/pubmed/19863050?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17</a></p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">3. Nilsson, M.T., W.W. Krajewski, S. Yellagunda, S. Prabhumurthy, G.N. Chamarahally, C. Siddamadappa, B.R. Srinivasa, S. Yahiaoui, M. Larhed, A. Karlen, T.A. Jones, and S.L. Mowbray, Structural basis for the inhibition of Mycobacterium tuberculosis glutamine synthetase by novel ATP-competitive inhibitors. J Mol Biol, 2009. 393(2): p. 504-13; PMID[19695264] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19695264?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=157">http://www.ncbi.nlm.nih.gov/pubmed/19695264?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=157</a></p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="NoSpacing">4. Pieroni, M., A. Lilienkampf, B. Wan, Y. Wang, S.G. Franzblau, and A.P. Kozikowski, Synthesis, biological evaluation, and structure-activity relationships for 5-[(E)-2-arylethenyl]-3-isoxazolecarboxylic acid alkyl ester derivatives as valuable antitubercular chemotypes. J Med Chem, 2009. 52(20): p. 6287-96; PMID[19757815] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19757815?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=148">http://www.ncbi.nlm.nih.gov/pubmed/19757815?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=148</a></p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19828313?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=37">Synthesis of new sugar derivatives and evaluation of their antibacterial activities against Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Horita Y, Takii T, Chiba T, Kuroishi R, Maeda Y, Kurono Y, Inagaki E, Nishimura K, Yamamoto Y, Abe C, Mori M, Onozaki K.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Nov 15;19(22):6313-6. Epub 2009 Sep 27.</p>

    <p class="NoSpacing">PMID: 19828313 [PubMed - in process]</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19819134?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=41">Synthesis and antitubercular activity of 7-chloro-4-quinolinylhydrazones derivatives.</a></p>

    <p class="NoSpacing">Candéa AL, Ferreira Mde L, Pais KC, Cardoso LN, Kaiser CR, Henriques MG, Lourenço MC, Bezerra FA, de Souza MV.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Nov 15;19(22):6272-4. Epub 2009 Sep 29.</p>

    <p class="NoSpacing">PMID: 19819134 [PubMed - in process]</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19813722?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=42">Phosphorus-nitrogen compounds. 18. Syntheses, stereogenic properties, structural and electrochemical investigations, biological activities, and DNA interactions of new spirocyclic mono- and bisferrocenylphosphazene derivatives.</a></p>

    <p class="NoSpacing">Asmafiliz N, Kiliç Z, Oztürk A, Hökelek T, Koç LY, Açik L, Kisa O, Albay A, Ustünda&#287; Z, Solak AO.</p>

    <p class="NoSpacing">Inorg Chem. 2009 Nov 2;48(21):10102-16.</p>

    <p class="NoSpacing">PMID: 19813722 [PubMed - in process]</p>

    <p class="NoSpacing">&nbsp;</p>

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19807189?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=46">Defining the mode of action of tetramic acid antibacterials derived from Pseudomonas aeruginosa quorum sensing signals.</a></p>

    <p class="NoSpacing">Lowery CA, Park J, Gloeckner C, Meijler MM, Mueller RS, Boshoff HI, Ulrich RL, Barry CE 3rd, Bartlett DH, Kravchenko VV, Kaufmann GF, Janda KD.</p>

    <p class="NoSpacing">J Am Chem Soc. 2009 Oct 14;131(40):14473-9.</p>

    <p class="NoSpacing">PMID: 19807189 [PubMed - in process]</p> 

    <p class="ListParagraph">9. Gupta, R.K., T.S. Thakur, G.R. Desiraju, and J.S. Tyagi, Structure-based design of DevR inhibitor active against nonreplicating Mycobacterium tuberculosis. J Med Chem, 2009. 52(20): p. 6324-34; PMID[19827833] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19827833?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=136">http://www.ncbi.nlm.nih.gov/pubmed/19827833?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=136</a>.</p> 

    <p class="ListParagraph">10. Fyfe, P.K., V.A. Rao, A. Zemla, S. Cameron, and W.N. Hunter, Specificity and Mechanism of Acinetobacter baumanii Nicotinamidase: Implications for Activation of the Front-Line Tuberculosis Drug Pyrazinamide. Angew Chem Int Ed Engl, 2009; PMID[19859929] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19859929?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=25">http://www.ncbi.nlm.nih.gov/pubmed/19859929?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=25</a>.</p> 

    <p class="ListParagraph">11. Connell, N.D. and V. Venketaraman, Control of Mycobacterium Tuberculosis Infection by Glutathione. Recent Pat Antiinfect Drug Discov, 2009; PMID[19832692] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19832692?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=131">http://www.ncbi.nlm.nih.gov/pubmed/19832692?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=131</a>.</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">12. Samota, MK, et al.</p>

    <p class="NoSpacing">Synthesis, Characterization, and Biological Activity of Organophosphates Derived from Substituted Benzoxazole</p>

    <p class="NoSpacing">HETEROATOM CHEMISTRY  2009   20: 309 - 315</p> 

    <p class="ListParagraph">13. Arai, M., et al.</p>

    <p class="NoSpacing">Haliclonacyclamines, Tetracyclic Alkylpiperidine Alkaloids, as Anti-dormant Mycobacterial Substances from a Marine Sponge of Haliclona sp.</p>

    <p class="NoSpacing">CHEMICAL &amp; PHARMACEUTICAL BULLETIN  2009 (Oct.) 57: 1136 - 1138</p> 

    <p class="ListParagraph">14. Balamurugan, K, et al.</p>

    <p class="NoSpacing">A facile domino protocol for the regioselective synthesis and discovery of novel 2-amino-5-arylthieno-[2,3-b]thiophenes as antimycobacterial agents</p>

    <p class="NoSpacing">TETRAHEDRON LETTERS  2009  50: 6191 - 6195</p> 

    <p class="ListParagraph">15. Junior, COR, et al.</p>

    <p class="NoSpacing">Preparation and antitubercular activity of lipophilic diamines and amino alcohols</p>

    <p class="NoSpacing">MEMORIAS DO INSTITUTO OSWALDO CRUZ  2009 (August) 104: 703 - 705</p> 

    <p class="ListParagraph">16. Zhang, XL, et al.</p>

    <p class="NoSpacing">Synthesis and evaluation of (S,S)-N,N &#39;-bis-[3-(2,2 &#39;,6,6 &#39;-tetramethylbenzhydryloxy)-2-hydroxy-propyl]-ethylenediamine (S2824) analogs with anti-tuberculosis activity</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2009 (Nov.) 19: 6074 - 6077</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
