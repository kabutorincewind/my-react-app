

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-336.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6IKX/T0NvtKohxYtUXhCna1vJBhCdiYvcZHy7K4w+u3gOc1CPv/zX4eBjriWiDsuylqVzzYrCdjcrZWIgkUwIMgIujUoswjsRxSXkPVn+IFXDBqW3XsUqh61x0s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52C757C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-336-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48198   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Recent Advances in Antiviral Nucleoside and Nucleotide Therapeutics</p>

    <p>          Simons, C, Wu, Q, and Htar, TT</p>

    <p>          Curr Top Med Chem <b>2005</b>.  5(13): 1191-1203</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305526&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16305526&amp;dopt=abstract</a> </p><br />

    <p>2.     48199   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Antifungal activities and action mechanisms of compounds from Tribulus terrestris L.</p>

    <p>          Zhang, Jun-Dong, Xu, Zheng, Cao, Yong-Bing, Chen, Hai-Sheng, Yan, Lan, An, Mao-Mao, Gao, Ping-Hui, Wang, Yan, Jia, Xin-Ming, and Jiang, Yuan-Ying</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  103(1): 76-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4H80SYP-1/2/9e44a6292515d91ce5a429b6dd166924">http://www.sciencedirect.com/science/article/B6T8D-4H80SYP-1/2/9e44a6292515d91ce5a429b6dd166924</a> </p><br />

    <p>3.     48200   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Bactericidal Activity of Organic Extracts from Flourensia cernua DC against Strains of Mycobacterium tuberculosis</p>

    <p>          Molina-Salinas, Gloria Maria, Ramos-Guerra, Monica Celina, Vargas-Villarreal, Javier, Mata-Cardenas, Benito David, Becerril-Montes, Pola, and Said-Fernandez, Salvador</p>

    <p>          Archives of Medical Research <b>2006</b>.  37(1): 45-49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VNM-4HNBXM4-8/2/34afe29f390bcc1b119ad8444050966a">http://www.sciencedirect.com/science/article/B6VNM-4HNBXM4-8/2/34afe29f390bcc1b119ad8444050966a</a> </p><br />

    <p>4.     48201   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          New perspectives on natural products in TB drug research</p>

    <p>          Pauli, Guido F, Case, Ryan J, Inui, Taichi, Wang, Yuehong, Cho, Sanghyun, Fischer, Nikolaus H, and Franzblau, Scott G</p>

    <p>          Life Sciences <b>2005</b>.  78(5): 485-494</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T99-4HCN3W9-1/2/9adf6e502c9ee16e68f2d0d3116f96ff">http://www.sciencedirect.com/science/article/B6T99-4HCN3W9-1/2/9adf6e502c9ee16e68f2d0d3116f96ff</a> </p><br />

    <p>5.     48202   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Treating HCV with ribavirin analogues and ribavirin-like molecules</p>

    <p>          Gish, RG</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16293677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16293677&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48203   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Posaconazole: a broad-spectrum triazole antifungal</p>

    <p>          Torres, Harrys A, Hachem, Ray Y, Chemaly, Roy F, Kontoyiannis, Dimitrios P, and Raad, Issam I</p>

    <p>          The Lancet Infectious Diseases <b>2005</b>.  5(12): 775-785</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8X-4HMXS16-W/2/0de6024d15223e37a76473795e0de51e">http://www.sciencedirect.com/science/article/B6W8X-4HMXS16-W/2/0de6024d15223e37a76473795e0de51e</a> </p><br />

    <p>7.     48204   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Diagnosis of latent Mycobacterium tuberculosis infection: is the demise of the Mantoux test imminent?</p>

    <p>          Rothel, JS and Andersen, P</p>

    <p>          Expert Rev Anti Infect Ther <b>2005</b>.  3(6): 981-993</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16307510&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16307510&amp;dopt=abstract</a> </p><br />

    <p>8.     48205   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          An Overview of Antifungal Drugs and Their Use for Treatment of Deep and Superficial Mycoses in Animals</p>

    <p>          Hector, Richard F</p>

    <p>          Clinical Techniques in Small Animal Practice <b>2005</b>.  20(4): 240-249</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75BB-4HDNVHP-9/2/5f31adde3ddde3d204e6df8d28677e3d">http://www.sciencedirect.com/science/article/B75BB-4HDNVHP-9/2/5f31adde3ddde3d204e6df8d28677e3d</a> </p><br />

    <p>9.     48206   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Upregulation of Protein Phosphatase 2Ac by Hepatitis C Virus Modulates NS3 Helicase Activity through Inhibition of Protein Arginine Methyltransferase 1</p>

    <p>          Duong, FH, Christen, V, Berke, JM, Penna, SH, Moradpour, D, and Heim, MH</p>

    <p>          J Virol <b>2005</b>.  79(24): 15342-15350</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16306605&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16306605&amp;dopt=abstract</a> </p><br />

    <p>10.   48207   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of picolinic acid against extracellular and intracellular Mycobacterium avium complex and its combined activity with clarithromycin, rifampicin and fluoroquinolones</p>

    <p>          Cai, S, Sato, K, Shimizu, T, Yamabe, S, Hiraki, M, Sano, C, and Tomioka, H</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16303883&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16303883&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48208   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Functional shikimate dehydrogenase from Mycobacterium tuberculosis H37Rv: Purification and characterization</p>

    <p>          Fonseca, Isabel O, Magalhaes, Maria LB, Oliveira, Jaim S, Silva, Rafael G, Mendes, Maria A, Palma, Mario S, Santos, Diogenes S, and Basso, Luiz A</p>

    <p>          Protein Expression and Purification <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4HDX8J6-3/2/ec7121a47a30bac6175be3bf6a6796c9">http://www.sciencedirect.com/science/article/B6WPJ-4HDX8J6-3/2/ec7121a47a30bac6175be3bf6a6796c9</a> </p><br />

    <p>12.   48209   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          The Structure of 3-Deoxy-d-arabino-heptulosonate 7-phosphate Synthase from Mycobacterium tuberculosis Reveals a Common Catalytic Scaffold and Ancestry for Type I and Type II Enzymes</p>

    <p>          Webby, Celia J, Baker, Heather M, Lott, JShaun, Baker, Edward N, and Parker, Emily J</p>

    <p>          Journal of Molecular Biology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4HCN7BD-7/2/fe36788b07882cb37c961a9a8aa8356d">http://www.sciencedirect.com/science/article/B6WK7-4HCN7BD-7/2/fe36788b07882cb37c961a9a8aa8356d</a> </p><br />

    <p>13.   48210   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Hairpin ribozymes in combination with siRNAs against highly conserved hepatitis C virus sequence inhibit RNA replication and protein translation from hepatitis C virus subgenomic replicons</p>

    <p>          Jarczak, D, Korf, M, Beger, C, Manns, MP, and Kruger, M</p>

    <p>          FEBS J <b>2005</b>.  272(22): 5910-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279954&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279954&amp;dopt=abstract</a> </p><br />

    <p>14.   48211   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Microarray-based pncA genotyping of pyrazinamide-resistant strains of Mycobacterium tuberculosis</p>

    <p>          Denkin, S, Volokhov, D, Chizhikov, V, and Zhang, Y</p>

    <p>          J Med Microbiol <b>2005</b>.  54(Pt 12): 1127-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16278424&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16278424&amp;dopt=abstract</a> </p><br />

    <p>15.   48212   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hepatitis B and C virus activities of ring-expanded (&#39;fat&#39;) nucleobase analogues containing the imidazo[4,5-e][1,3]diazepine-4,8-dione ring system</p>

    <p>          Zhang, Peng, Zhang, Ning, Korba, Brent E, and Hosmane, Ramachandra S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(24): 5397-5401</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H877F9-D/2/45edb5c3e503d58bd50709bc8aed9b03">http://www.sciencedirect.com/science/article/B6TF9-4H877F9-D/2/45edb5c3e503d58bd50709bc8aed9b03</a> </p><br />
    <br clear="all">

    <p>16.   48213   OI-LS-336; PUBMED-OI-11/28/2005</p>

    <p class="memofmt1-2">          Combined Effects of ATP on the Therapeutic Efficacy of Antimicrobial Drug Regimens against Mycobacterium avium Complex Infection in Mice and Roles of Cytosolic Phospholipase A2-Dependent Mechanisms in the ATP-Mediated Potentiation of Antimycobacterial Host Resistance</p>

    <p>          Tomioka, H, Sano, C, Sato, K, Ogasawara, K, Akaki, T, Sano, K, Cai, SS, and Shimizu, T</p>

    <p>          J Immunol <b>2005</b>.  175(10): 6741-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16272330&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16272330&amp;dopt=abstract</a> </p><br />

    <p>17.   48214   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p class="memofmt1-2">          Virology of hepatitis B and C viruses and antiviral targets</p>

    <p>          Pawlotsky, Jean-Michel</p>

    <p>          Journal of Hepatology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4HM86JR-2/2/512219f0fa4da965a8074e3f9dd992e5">http://www.sciencedirect.com/science/article/B6W7C-4HM86JR-2/2/512219f0fa4da965a8074e3f9dd992e5</a> </p><br />

    <p>18.   48215   OI-LS-336; EMBASE-OI-11/28/2005</p>

    <p><b>          Limited good and limited vision: multidrug-resistant tuberculosis and global health policy</b> </p>

    <p>          Yong Kim, Jim, Shakow, Aaron, Mate, Kedar, Vanderwarker, Chris, Gupta, Rajesh, and Farmer, Paul</p>

    <p>          Social Science &amp; Medicine <b>2005</b>.  61(4): 847-859</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VBF-4G65CDW-1/2/b25bd4adfb4bceae9eeb36dd7ef31724">http://www.sciencedirect.com/science/article/B6VBF-4G65CDW-1/2/b25bd4adfb4bceae9eeb36dd7ef31724</a> </p><br />

    <p>19.   48216   OI-LS-336; WOS-OI-11/20/2005</p>

    <p class="memofmt1-2">          The use of biodiversity as source of new chemical entities against defined molecular targets for treatment of malaria, tuberculosis, and T-cell mediated diseases - A Review</p>

    <p>          Basso, LA, da Silva, LHP, Fett-Neto, AG, Junior, WFD, Moreira, ID, Palma, MS, Calixto, JB, Astolfi, S, dos Santos, RR, Soares, MBP, and Santos, DS</p>

    <p>          MEMORIAS DO INSTITUTO OSWALDO CRUZ <b>2005</b>.  100(6): 475-506, 32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233085400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233085400001</a> </p><br />

    <p>20.   48217   OI-LS-336; WOS-OI-11/20/2005</p>

    <p class="memofmt1-2">          Selective anti-tubercular purines: Synthesis and chemotherapeutic properties of 6-aryl- and 6-heteroaryl-9-benzylpurines</p>

    <p>          Braendvang, M and Gundersen, LL</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(23): 6360-6373, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232959200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232959200007</a> </p><br />

    <p>21.   48218   OI-LS-336; WOS-OI-11/20/2005</p>

    <p class="memofmt1-2">          New Mycobacterium avium antifolate shows synergistic effect when used in combination with dihydropteroate synthase inhibitors</p>

    <p>          Suling, WJ, Seitz, LE, Reynolds, RC, and Barrow, WW</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(11): 4801-4803, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900059</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
