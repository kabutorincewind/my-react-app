

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-08-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="o2R4gjIL5PlrKpy3uHoJFCWn2FpGw3aBRFS36aRq9Y8FzvskNChiYY0xuo2LKwPnf91EfHYYdmE6t41tconFJIn2Yj1DaMDwB/Sb3HQIHEcR9+wiCR46cC1MqXE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ED2B0ED1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 1 - August 14, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23841536">Preformulation Studies of EFdA, a Novel Nucleoside Reverse Transcriptase Inhibitor for HIV Prevention.</a> Zhang, W., M.A. Parniak, H. Mitsuya, S.G. Sarafianos, P.W. Graebing, and L.C. Rohan. Drug Development and Industrial Pharmacy, 2014. 40(8): p. 1101-1111. PMID[23841536].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">2. Examining <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24643333">Structural Analogs of Elvitegravir as Potential Inhibitors of HIV-1 Integrase.</a> Shah, K., S. Gupta, H. Mishra, P.K. Sharma, and A. Jayaswal. Archives of Virology, 2014. 159(8): p. 2069-2080. PMID[24643333].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25014309">Reactivation of Latent HIV-1 by New Semi-synthetic Ingenol Esters</a>. Pandelo Jose, D., K. Bartholomeeusen, R.D. da Cunha, C.M. Abreu, J. Glinski, T.B. da Costa, A.F. Bacchi Rabay, L.F. Pianowski Filho, L.W. Dudycz, U. Ranga, B.M. Peterlin, L.F. Pianowski, A. Tanuri, and R.S. Aguiar. Virology, 2014. 462-463: p. 328-339. PMID[25014309].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24821255">Pharmacokinetics and Dose-range Finding Toxicity of a Novel anti-HIV Active Integrase Inhibitor.</a> Nair, V., M. Okello, S. Mishra, J. Mirsalis, K. O&#39;Loughlin, and Y. Zhong. Antiviral Research, 2014. 108: p. 25-29. PMID[24821255].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24850787">Design, Synthesis, and Biological Evaluation of 1,3-Diarylpropenones as Dual Inhibitors of HIV-1 Reverse Transcriptase.</a> Meleddu, R., V. Cannas, S. Distinto, G. Sarais, C. Del Vecchio, F. Esposito, G. Bianco, A. Corona, F. Cottiglia, S. Alcaro, C. Parolin, A. Artese, D. Scalise, M. Fresta, A. Arridu, F. Ortuso, E. Maccioni, and E. Tramontano. ChemMedChem, 2014. 9(8): p. 1869-1879. PMID[24850787].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24983400">Ginsenoside Rb1 Eliminates HIV-1 (D3)-Transduced Cytoprotective Human Macrophages by Inhibiting the AKT Pathway.</a> Jeong, J.J., B. Kim, and D.H. Kim. Journal of Medicinal Food, 2014. 17(8): p. 849-854. PMID[24983400].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">7<a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24867973">. SAMHD1 Has Differential Impact on the Efficacies of HIV Nucleoside Reverse Transcriptase Inhibitors.</a> Huber, A.D., E. Michailidis, M.L. Schultz, Y.T. Ong, N. Bloch, M.N. Puray-Chavez, M.D. Leslie, J. Ji, A.D. Lucas, K.A. Kirby, N.R. Landau, and S.G. Sarafianos. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4915-4919. PMID[24867973].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24948204">Evaluation of Atazanavir and Darunavir Interactions with Lipids for Developing pH-responsive anti-HIV Drug Combination Nanoparticles.</a> Duan, J., J.P. Freeling, J. Koehn, C. Shu, and R.J. Ho. Journal of Pharmaceutical Sciences, 2014. 103(8): p. 2520-2529. PMID[24948204].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24935032">In vitro Analysis of the Susceptibility of HIV-1 Subtype A and CRF01_AE Integrases to Raltegravir.</a> Bellecave, P., L. Malato, C. Calmels, S. Reigadas, V. Parissi, M.L. Andreola, and H. Fleury. International Journal of Antimicrobial Agents, 2014. 44(2): p. 168-172. PMID[24935032].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24913159">SAMHD1 Specifically Affects the Antiviral Potency of Thymidine Analog HIV Reverse Transcriptase Inhibitors.</a> Ballana, E., R. Badia, G. Terradas, J. Torres-Torronteras, A. Ruiz, E. Pauls, E. Riveira-Munoz, B. Clotet, R. Marti, and J.A. Este. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4804-4813. PMID[24913159].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24820090">Zinc Finger Endonuclease Targeting PSIP1 Inhibits HIV-1 Integration.</a>  Badia, R., E. Pauls, E. Riveira-Munoz, B. Clotet, J.A. Este, and E. Ballana. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4318-4327. PMID[24820090].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0801-081414.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338645000002">Reactivation of HIV Latency by a Newly Modified Ingenol Derivative via Protein Kinase C Delta-NF-kappa B Signaling.</a> Jiang, G.C., E.A. Mendes, P. Kaiser, S. Sankaran-Walters, Y.Y. Tang, M.G. Weber, G.P. Melcher, G.R. Thompson, A. Tanuri, L.F. Pianowski, J.K. Wong, and S. Dandekar. AIDS, 2014. 28(11): p. 1555-1566. ISI[000338645000002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0801-081414.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338815700010">An Efficient One-pot Synthesis of Some New Substituted 1,2,3,5-Thiatriazolidin-4-one 1,1-dioxides.</a> Balti, M., K. Dridi, and E.M.L. El. Heterocycles, 2014. 89(6): p. 1483-1490. ISI[000338815700010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0801-081414.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
