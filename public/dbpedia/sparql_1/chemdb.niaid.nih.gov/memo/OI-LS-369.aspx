

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-369.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v8SiM6nB3584g1CvzsbyqDoLyEfcCtEsftDjKpV/xmA5D8eUH/kn2oquRwYoF0Cfux/SS87T3e1miCGFhR5VEusH9UwxXJVm/cCC7x5AsRwOfPT9+k4GZAi4gpQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="915094C3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-369--MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60318   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Emerging host cell targets for hepatitis C therapy</p>

    <p>          He, Y, Duan, W, and Tan, SL</p>

    <p>          Drug Discov Today <b>2007</b>.  12(5-6): 209-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331885&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331885&amp;dopt=abstract</a> </p><br />

    <p>2.     60319   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Inhibition of Chitin Synthases and Antifungal Activities by 2&#39;-Benzoyloxycinnamaldehyde from Pleuropterus ciliinervis and Its Derivatives</p>

    <p>          Kang, TH, Hwang, EI, Yun, BS, Park, KD, Kwon, BM, Shin, CS, and Kim, SU</p>

    <p>          Biol Pharm Bull <b>2007</b>.  30(3): 598-602</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329866&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329866&amp;dopt=abstract</a> </p><br />

    <p>3.     60320   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Evaluation of 3-deaza-adenosine analogues as ligands for adenosine kinase and inhibitors of Mycobacterium tuberculosis growth</p>

    <p>          Long, Mary C, Allan, Paula W, Luo, Mei-Zhen, Liu, Mao-Chin, Sartorelli, Alan C, and Parker, William B</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2007</b>.  59(1): 118-121</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60321   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Human Serum Amyloid A Protein Inhibits Hepatitis C Virus Entry into Cells</p>

    <p>          Cai, Z, Cai, L, Jiang, J, Chang, KS, van, der Westhuyzen DR, and Luo, G</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329325&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329325&amp;dopt=abstract</a> </p><br />

    <p>5.     60322   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Studies of Toxoplasma gondii and Plasmodium falciparum enoyl acyl carrier protein reductase and implications for the development of antiparasitic agents</p>

    <p>          Muench, SP, Prigge, ST, McLeod, R, Rafferty, JB, Kirisits, MJ, Roberts, CW, Mui, EJ, and Rice, DW</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>2007</b>.  63(Pt 3): 328-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17327670&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17327670&amp;dopt=abstract</a> </p><br />

    <p>6.     60323   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Mycolic acids for the control of tuberculosis</p>

    <p>          Sekanka, Gianna, Baird, Mark, Innikin, David, and Grooten, Johan</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2007</b>.  17(3): 315-331</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     60324   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Modeling the combination of Amphotericin B, Micafungin, and Nikkomycin Z against Aspergillus fumigatus in vitro using a novel response surface paradigm</p>

    <p>          Brun, YF, Dennis, CG, Greco, WR, Bernacki, RJ, Pera, PJ, Bushey, JJ, Youn, RC, White, DB, and Segal, BH</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17325217&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17325217&amp;dopt=abstract</a> </p><br />

    <p>8.     60325   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus infection</p>

    <p>          Fischler, B</p>

    <p>          Semin Fetal Neonatal Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320495&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320495&amp;dopt=abstract</a> </p><br />

    <p>9.     60326   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Discovery of novel phenoxyacetic acid derivatives as antimycobacterial agents</p>

    <p>          Ali, Mohamed Ashraf and Shaharyar, Mohammad</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(5): 1896-1902</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60327   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          A focused salivary gland infection with attenuated MCMV: An animal model with prevention of pathology associated with systemic MCMV infection</p>

    <p>          Pilgrim, MJ, Kasman, L, Grewal, J, Bruorton, ME, Werner, P, London, L, and London, SD</p>

    <p>          Exp Mol Pathol  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320076&amp;dopt=abstract</a> </p><br />

    <p>11.   60328   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral properties of 4&#39;-substituted ribonucleosides as inhibitors of hepatitis C virus replication: The discovery of R1479</p>

    <p>          Smith, DB, Martin, JA, Klumpp, K, Baker, SJ, Blomgren, PA, Devos, R, Granycome, C, Hang, J, Hobbs, CJ, Jiang, WR, Laxton, C, Pogam, SL, Leveque, V, Ma, H, Maile, G, Merrett, JH, Pichota, A, Sarma, K, Smith, M, Swallow, S, Symons, J, Vesey, D, Najera, I, and Cammack, N</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317178&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317178&amp;dopt=abstract</a> </p><br />

    <p>12.   60329   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis with extensive resistance</p>

    <p>          Labie Dominique</p>

    <p>          Med Sci (Paris) <b>2007</b>.  23(2): 205-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60330   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Neopeltolide, a Macrolide from a Lithistid Sponge of the Family Neopeltidae</p>

    <p>          Wright, AE, Botelho, JC, Guzman, E, Harmody, D, Linley, P, McCarthy, PJ, Pitts, TP, Pomponi, SA, and Reed, JK</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17309301&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17309301&amp;dopt=abstract</a> </p><br />

    <p>14.   60331   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Functional characterization of the acyl-[acyl carrier protein] ligase in the Cryptosporidium parvum giant polyketide synthase</p>

    <p>          Fritzler, Jason M and Zhu, Guan</p>

    <p>          International Journal for Parasitology <b>2007</b>.  37(3-4): 307-316</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60332   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Constructing biodiverse gene fragment libraries and screening of the library for bioactive polypeptide products</p>

    <p>          Watt, Paul Michael, Thomas, Wayne Robert, and Hopkins, Richard</p>

    <p>          PATENT:  US <b>2007031832</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006-22045  PP: 174pp., Cont.-in-part of U.S. Ser. No. 372,003.</p>

    <p>          ASSIGNEE:  (Phylogica Limited, Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   60333   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Fatty acid biosynthesis as a drug target in apicomplexan parasites</p>

    <p>          Goodman, CD and McFadden, GI</p>

    <p>          Current Drug Targets <b>2007</b>.  8(1): 15-30</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60334   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Pseudoceratins A and B, Antifungal Bicyclic Bromotyrosine-Derived Metabolites from the Marine Sponge Pseudoceratina purpurea</p>

    <p>          Jang, Jae-Hyuk, Van Soest, Rob WM, Fusetani, Nobuhiro, and Matsunaga, Shigeki</p>

    <p>          Journal of Organic Chemistry <b>2007</b>.  72(4): 1211-1217</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60335   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Chemical genetics approach to hepatitis C virus replication: cyclophilin as a target for anti-hepatitis C virus strategy</p>

    <p>          Watashi, K and Shimotohno, K</p>

    <p>          Rev Med Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17299803&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17299803&amp;dopt=abstract</a> </p><br />

    <p>19.   60336   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Study on the substituents&#39; effects of a series of synthetic chalcones against the yeast Candida albicans</p>

    <p>          Batovska, D, Parushev, St, Slavova, A, Bankova, V, Tsvetkova, I, Ninova, M, and Najdenski, H</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(1): 87-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   60337   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Synthesis and biological activity studies of Di- and mono-halogenofluoro benzenes</p>

    <p>          Logoglu, Elif, Katircioglu, Hikmet, Tilki, Tahir, and Oktemer, Atilla</p>

    <p>          Asian Journal of Chemistry <b>2007</b>.  19(3): 2029-2035</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60338   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Design of endoperoxides with anti-Candida activity</p>

    <p>          Avery, Thomas D, Macreadie, Peter I, Greatrex, Ben W, Robinson, Tony V, Taylor, Dennis K, and Macreadie, Ian G</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(1): 36-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60339   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Synthesis and characterization of 1-(carbalkoxymethyl)-4-hydroxy-1-methylpiperidinium chlorides</p>

    <p>          Dega-Szafran, Zofia, Dulewicz, Ewa, and Brycki, Bogumil</p>

    <p>          ARKIVOC (Gainesville, FL, United States) <b>2007</b>.(6): 90-102</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   60340   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Inositol-1-phosphate synthetase mRNA as a new target for antisense inhibition of Mycobacterium tuberculosis</p>

    <p>          Li, Y, Chen, Z, Li, X, Zhang, H, Huang, Q, Zhang, Y, and Xu, S</p>

    <p>          J Biotechnol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17275118&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17275118&amp;dopt=abstract</a> </p><br />

    <p>24.   60341   OI-LS-369; PUBMED-OI-3/5/2007</p>

    <p class="memofmt1-2">          Antifungal Chemical Compounds Identified Using a C. elegans Pathogenicity Assay</p>

    <p>          Breger, J, Fuchs, BB, Aperis, G, Moy, TI, Ausubel, FM, and Mylonakis, E</p>

    <p>          PLoS Pathog <b>2007</b>.  3(2): e18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17274686&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17274686&amp;dopt=abstract</a> </p><br />

    <p>25.   60342   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          In Vitro Antifungal Activities of Sulfa Drugs against Clinical Isolates of Aspergillus and Cryptococcus Species</p>

    <p>          Hanafy Ahmed, Uno Jun, Mitani Hiroki, Kang Yingqian, and Mikami Yuzuru</p>

    <p>          Nippon Ishinkin Gakkai Zasshi <b>2007</b>.  48(1): 47-50.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   60343   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Hepatitis c serine protease inhibitors and uses therefor</p>

    <p>          Campbell, David Alan, Winn, David T, Betancort, Juan Manuel, and Hepperle, Michael E</p>

    <p>          PATENT:  WO <b>2007016476</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006  PP: 107pp.</p>

    <p>          ASSIGNEE:  (Phenomix Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>27.   60344   OI-LS-369; WOS-OI-2/26/2007</p>

    <p class="memofmt1-2">          Salicylanilide acetates: Synthesis and antibacterial evaluation</p>

    <p>          Vinsova, J, Imramovsky, A, Buchta, V, Ceckova, M, Dolezal, M, Stand, F, Jampilek, J, and Kaustova, J</p>

    <p>          MOLECULES <b>2007</b>.  12(1): 1-12, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243883000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243883000001</a> </p><br />

    <p>28.   60345   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p><b>          Preparation of diaryl substituted pyrimidine-2,4-(1H)-diones as novel bio-active compounds</b> </p>

    <p>          Tadiparthi, Ravikumar, Sharma, Ganapavarapu Veera Raghava, Narayanan, Sukunath, Parameswaran, Venkatesan, Reddy, Gaddam Om, Rajagopal, Sriram, Natarajan, Manikandan, and Koppolu, Kesavan</p>

    <p>          PATENT:  WO <b>2007007161</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006  PP: 67pp.</p>

    <p>          ASSIGNEE:  (Orchid Research Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   60346   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of 7-(1-piperazinyl)-4(1H)-quinolinones as antiviral agents</p>

    <p>          Schohe-Loop, Rudolf, Zimmermann, Holger, Henninger, Kerstin, Lang, Dieter, Thede, Kai, Fuerstner, Chantal, and Brueckner, David</p>

    <p>          PATENT:  WO <b>2007003308</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 120pp.</p>

    <p>          ASSIGNEE:  (Aicuris Gmbh &amp; Co. KG, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   60347   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Novel therapies for cytomegalovirus disease</p>

    <p>          Steininger, Christoph</p>

    <p>          Recent Patents on Anti-Infective Drug Discovery <b>2007</b>.  2(1 ): 53-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   60348   OI-LS-369; SCIFINDER-OI-2/26/2007</p>

    <p class="memofmt1-2">          Anti fungal screening utilizing polypeptides associated with cell wall synthesis</p>

    <p>          Van den Hondel, Cornelis Antonius Maria Jacobus Johannes</p>

    <p>          PATENT:  WO <b>2007011221</b>  ISSUE DATE:  20070125</p>

    <p>          APPLICATION: 2006  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Universiteit Leiden, Neth.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   60349   OI-LS-369; WOS-OI-2/26/2007</p>

    <p class="memofmt1-2">          The chemokine receptor CXCR3 attenuates the control of chronic Mycobacterium tuberculosis infection in BALB/c mice</p>

    <p>          Chakravarty, SD, Xu, JY, Lu, B, Gerard, C, Flynn, J, and Chan, J</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2007</b>.  178(3): 1723-1735, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243820900057">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243820900057</a> </p><br />

    <p>33.   60350   OI-LS-369; WOS-OI-2/26/2007</p>

    <p class="memofmt1-2">          Hsp90 inhibitors suppress HCV replication in replicon cells and humanized liver mice</p>

    <p>          Nakagawa, S, Umehara, T, Matsuda, C, Kuge, S, Sudoh, M, and Kohara, M</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2007</b>.  353(4): 882-888, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243859600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243859600007</a> </p><br />
    <br clear="all">

    <p>34.   60351   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Future therapies for hepatitis C: where do we go from here?</p>

    <p>          Sigal, S and Jacobson, I</p>

    <p>          NATURE CLINICAL PRACTICE GASTROENTEROLOGY &amp; HEPATOLOGY <b>2007</b>.  4(2): 60-61, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243966600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243966600002</a> </p><br />

    <p>35.   60352   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Evaluation of an intracellular pharmacokinetic in vitro infection model as a tool to assess tuberculosis therapy</p>

    <p>          Cappelletty, DM</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2007</b>.  29(2): 212-216, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244082300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244082300015</a> </p><br />

    <p>36.   60353   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          A mycobacterial gene involved in synthesis of an outer cell envelope lipid is a key factor in prevention of phagosome maturation</p>

    <p>          Robinson, N, Wolke, M, Ernestus, K, and Plum, G</p>

    <p>          INFECTION AND IMMUNITY <b>2007</b>.  75(2): 581-591, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243865500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243865500005</a> </p><br />

    <p>37.   60354   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Current status and future prospects for new therapies for pulmonary tuberculosis</p>

    <p>          de Souza, MVN</p>

    <p>          CURRENT OPINION IN PULMONARY MEDICINE <b>2006</b>.  12(3): 167-171, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243918300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243918300002</a> </p><br />

    <p>38.   60355   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Newer diagnostics for tuberculosis and multi-drug resistant tuberculosis</p>

    <p>          Palomino, JC</p>

    <p>          CURRENT OPINION IN PULMONARY MEDICINE <b>2006</b>.  12(3): 172-178, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243918300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243918300003</a> </p><br />

    <p>39.   60356   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Mannan binding lectin and viral hepatitis</p>

    <p>          Brown, KS, Ryder, SD, Irving, WL, Sim, RB, and Hickling, TP</p>

    <p>          IMMUNOLOGY LETTERS <b>2007</b>.  108(1): 34-44, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244061400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244061400005</a> </p><br />

    <p>40.   60357   OI-LS-369; WOS-OI-3/4/2007</p>

    <p class="memofmt1-2">          Aspirin antagonism in isoniazid treatment of tuberculosis in mice</p>

    <p>          Byrne, ST, Denkin, SM, and Zhang, Y</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(2): 794-795, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243900600061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243900600061</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
