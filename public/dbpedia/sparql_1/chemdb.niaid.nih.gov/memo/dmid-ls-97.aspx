

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-97.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="2q+gjtLxxg5yTmvYUVwy6sS+8EJZKV5+mrEOyjagg3WT5QmiqEYD+Fz8z11UPk33RmmCKhvPEDsIOy3y7Xx2XZy/xMaqmZhmgXz0VSvarrxYTtoGA0zCcEbUmx0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7BC097C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-97-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57722   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Isolation of an Antiviral Polysaccharide, Nostoflan, from a Terrestrial Cyanobacterium, Nostoc flagelliforme</p>

    <p>          Kanekiyo, Kenji, Lee, Jung-Bum, Hayashi, Kyoko, Takenaka, Hiroyuki, Hayakawa, Yumiko, Endo, Shunro, and Hayashi, Toshimitsu</p>

    <p>          Journal of Natural Products <b>2005</b>.  68(7): 1037-1041</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     57723   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ready-for-use injectable solution of 9-((1,3-dihydroxypropan-2-iloxy)methyl)-2-amine-1H-purin-6(9H)-one and a closed system for packing the solution with a process for eliminating alkaline residuals of 9-((1,3-dihydroxypropan-2-iloxy)methyl)-2-amine-1H-purin-6(9H)-one crystals</p>

    <p>          Perillo, Heno, Campos Neto, Jose, and Crucelli, Marcio</p>

    <p>          PATENT:  WO <b>2005051286</b>  ISSUE DATE:  20050609</p>

    <p>          APPLICATION: 2004  PP: 37 pp.</p>

    <p>          ASSIGNEE:  (Halex Istar Industria Farmaceutica Ltda., Brazil and Labogen S/A Quimica Fina E Biotecnologia)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     57724   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effects of orally administered bovine lactoferrin and lactoperoxidase on influenza virus infection in mice</p>

    <p>          Shin, K, Wakabayashi, H, Yamauchi, K, Teraguchi, S, Tamura, Y, Kurokawa, M, and Shiraki, K</p>

    <p>          J Med Microbiol <b>2005</b>.  54(Pt 8): 717-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16014423&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16014423&amp;dopt=abstract</a> </p><br />

    <p>4.     57725   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Specific Targeting of Hepatitis C Virus NS3 RNA Helicase. Discovery of the Potent and Selective Competitive Nucleotide-Mimicking Inhibitor QU663</p>

    <p>          Maga, G, Gemma, S, Fattorusso, C, Locatelli, GA, Butini, S, Persico, M, Kukreja, G, Romano, MP, Chiasserini, L, Savini, L, Novellino, E, Nacci, V, Spadari, S, and Campiani, G </p>

    <p>          Biochemistry <b>2005</b>.  44(28): 9637-9644</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16008349&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16008349&amp;dopt=abstract</a> </p><br />

    <p>5.     57726   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus gene expression and replication by helioxanthin and its derivative</p>

    <p>          Li, Y, Fu, L, Yeo, H, Zhu, JL, Chou, CK, Kou, YH, Yeh, SF, Gullen, E, Austin, D, and Cheng, YC </p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(3): 193-201</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004082&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004082&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     57727   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Characterization of hepatitis B virus inhibition by novel 2&#39;-fluoro-2&#39;,3&#39;-unsaturated beta-D- and L-nucleosides</p>

    <p>          Pai, SB, Pai, RB, Xie, MY, Beker, T, Shi, J, Tharnish, PM, Chu, CK, and Schinazi, RF</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(3): 183-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004081&amp;dopt=abstract</a> </p><br />

    <p>7.     57728   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Achievements and challenges in antiviral drug discovery</p>

    <p>          Littler, E and Oberg, B</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(3): 155-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16004079&amp;dopt=abstract</a> </p><br />

    <p>8.     57729   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Acyclic 3-[(2-Hydroxyethoxy)methyl] Analogues of Antiviral Furo- and Pyrrolo[2,3-d]pyrimidine Nucleosides(1)</p>

    <p>          Janeba, Z, Balzarini, J, Andrei, G, Snoeck, R, De, Clercq E, and Robins, MJ</p>

    <p>          J Med Chem <b>2005</b>.  48(14): 4690-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16000005&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16000005&amp;dopt=abstract</a> </p><br />

    <p>9.     57730   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potent Inhibitors of Subgenomic Hepatitis C Virus RNA Replication through Optimization of Indole-N-Acetamide Allosteric Inhibitors of the Viral NS5B Polymerase</p>

    <p>          Harper, S, Avolio, S, Pacini, B, Di, Filippo M, Altamura, S, Tomei, L, Paonessa, G, Di, Marco S, Carfi, A, Giuliano, C, Padron, J, Bonelli, F, Migliaccio, G, De, Francesco R, Laufer, R, Rowley, M, and Narjes, F</p>

    <p>          J Med Chem <b>2005</b>.  48(14): 4547-57</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999993&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15999993&amp;dopt=abstract</a> </p><br />

    <p>10.   57731   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Novel Topical Antiviral Agent, Pcl-016 (Picolinic Acid), Inhibits Adenovirus Replication in the Ad5/Nzw Rabbit Ocular Model</p>

    <p>          Romanowski, E. <i>et al.</i></p>

    <p>          Investigative Ophthalmology &amp; Visual Science <b>2005</b>.  46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227980401059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227980401059</a> </p><br />
    <br clear="all">

    <p>11.   57732   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interferon prevents formation of replication-competent hepatitis B virus RNA-containing nucleocapsids</p>

    <p>          Wieland, SF, Eustaquio, A, Whitten-Bauer, C, Boyd, B, and Chisari, FV</p>

    <p>          Proc Natl Acad Sci U S A <b>2005</b>.  102(28): 9913-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994231&amp;dopt=abstract</a> </p><br />

    <p>12.   57733   DMID-LS-97; PUBMED-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of the severe acute respiratory syndrome 3CL protease by peptidomimetic alpha,beta-unsaturated esters</p>

    <p>          Shie, JJ, Fang, JM, Kuo, TH, Kuo, CJ, Liang, PH, Huang, HJ, Wu, YT, Jan, JT, Cheng, YS, and Wong, CH</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994085&amp;dopt=abstract</a> </p><br />

    <p>13.   57734   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of dihydroquinazolines as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Jeske, Mario, Nell, Peter, Nikolic, Susanne, Suessmeier, Frank, Zimmermann, Holger, Grosser, Rolf, Hewlett, Guy, Keldenich, Joerg, Lang, Dieter, and Henninger, Kerstin</p>

    <p>          PATENT:  WO <b>2005047278</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   57735   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The &#39;supervirus&#39;? Lessons From Il-4-Expressing Poxviruses</p>

    <p>          Stanford, M. and Mcfadden, G.</p>

    <p>          Trends in Immunology <b>2005</b>.  26(6): 339-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229885200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229885200011</a> </p><br />

    <p>15.   57736   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Docking Versus Pharmacophore Model Generation: a Comparison of High-Throughput the Search of Human Rhinovirus Virtual Screening Strategies for Coat Protein Inhibitors</p>

    <p>          Steindl, T. and Langer, T.</p>

    <p>          Qsar &amp; Combinatorial Science <b>2005</b>.  24(4): 470-479</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229958900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229958900005</a> </p><br />

    <p>16.   57737   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Bicyclic carbohydrate compounds useful in the treatment of infections caused by Herpesviridae</p>

    <p>          Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, Peys, Eric, Van Der Eycken, Johan, Ruttens, Bart, and Blom, Petra</p>

    <p>          PATENT:  US <b>2005090452</b>  ISSUE DATE:  20050428</p>

    <p>          APPLICATION: 69  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (Belg.)</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   57738   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Influenza Virus Activity of Polyoxometalates Containing Amantadine</p>

    <p>          Liu, S. <i>et al.</i></p>

    <p>          Acta Chimica Sinica <b>2005</b>.  63(12): 1069-1074</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229977700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229977700006</a> </p><br />

    <p>18.   57739   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the Antiviral Activity of Acivicin (At125) Against Herpesvirus Infection in Human Lens and G-Glutamyltranspeptidase Positive Conjunctival Cells</p>

    <p>          Langford, M. <i>et al.</i></p>

    <p>          Investigative Ophthalmology &amp; Visual Science <b>2005</b>.  46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227980402891">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227980402891</a> </p><br />

    <p>19.   57740   DMID-LS-97; WOS-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Antiviral Activity of Chamaecrista Nictitans (Fabaceae) Against Herpes Simplex Virus: Biological Characterization of Mechanisms of Action</p>

    <p>          Uribe, L., Olarte, E., and Castillo, G.</p>

    <p>          Revista De Biologia Tropical <b>2004</b>.  52(3): 807-816</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229623700041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229623700041</a> </p><br />

    <p>20.   57741   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Papain-Like Protease 2 (PLP2) from Severe Acute Respiratory Syndrome Coronavirus (SARS-CoV): Expression, Purification, Characterization, and Inhibition</p>

    <p>          Han, Yu-San, Chang, Gu-Gang, Juo, Chiun-Gung, Lee, Hong-Jen, Yeh, Shiou-Hwei, Hsu, John Tsu-An, and Chen, Xin</p>

    <p>          Biochemistry <b>2005</b>.  44(30): 10349-10359</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   57742   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of sialic acid, analogues of sialic acid, and antibodies to sialidases as anti-infectious agents and anti-inflammatory agents</p>

    <p>          Cross, Alan and Stamatos, Nicholas</p>

    <p>          PATENT:  WO <b>2005056047</b>  ISSUE DATE:  20050623</p>

    <p>          APPLICATION: 2004  PP: 51 pp.</p>

    <p>          ASSIGNEE:  (United States Army Medical Research and Materiel Command, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   57743   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SiRNA targeting SARS nsp-1, nsp-9 or spike gene for anti-SARS coronavirus therapy</p>

    <p>          Tang, Quinn T, Lu, Patrick Y, Xie, Frank Y, Liu, Yijia, Xu, Jun, and Woodle, Martin C</p>

    <p>          PATENT:  WO <b>2005019410</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2004  PP: 48 pp.</p>

    <p>          ASSIGNEE:  (Intradigm Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   57744   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomavirus inhibitors and screening system reagentss</p>

    <p>          Meneses, Patricio I, Koehler, Angela N, Wong, Jason C, Howley, Peter M, and Schreiber, Stuart L</p>

    <p>          PATENT:  US <b>2005123902</b>  ISSUE DATE:  20050609</p>

    <p>          APPLICATION: 2004-64975  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   57745   DMID-LS-97; SCIFINDER-DMID-7/18/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Indoles and azaindoles as inhibitors of hepatitis C virus polymerase, their preparation, pharmaceutical compositions, and use as antiviral agents</p>

    <p>          Avolio, Salvatore, Harper, Steven, Narjes, Frank, Pacini, Barbara, and Rowley, Michael</p>

    <p>          PATENT:  WO <b>2005034941</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004  PP: 41 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare p Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
