

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-09-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bNahqOBA8HsVaQ176sEEKAn0Pr36hGjXbLQTUC7qRtXOWCbnY8uuKqKRrKktNJTLlZpr0FdDyhr/hisi4sK+ProH+lHjAkXQnY4aKZm72IJamYxqJxnnFHYb0gs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="70BBE249" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List: September 3 - September 16, 2010</h1>

    <p class="memofmt2-1"></p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20728367">Anti-AIDS agents 79. Design, synthesis, molecular modeling and structure-activity relationships of novel dicamphanoyl 2&#39;,2&#39;-dimethyldihydropyranochromone (DCP) analogs as potent anti-HIV agents.</a> Zhou T, Shi Q, Chen CH, Zhu H, Huang L, Ho P, Lee KH. Bioorganic &amp; Medicinal Chemistry. 2010; 18(18):6678-89. PMID[20728367]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="title">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20832370">The evaluation of catechins that contain a galloyl moiety as potential HIV-1 integrase inhibitors.</a> Jiang F, Chen W, Yi K, Wu Z, Si Y, Han W, Zhao Y. Clinical Immunology. 2010. <b>[Epub ahead of print]</b>; PMID[20832370]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="title">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20799722">Physical Trapping of HIV-1 Synaptic Complex by Different Structural Classes of Integrase Strand Transfer Inhibitors.</a> Pandey KK, Bera S, Vora AC, Grandgenett DP. Biochemistry. 2010. <b>[Epub ahead of print]</b>; PMID[ 20799722]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="title">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20708407">Peptidic HIV integrase inhibitors derived from HIV gene products: structure-activity relationship studies.</a>Suzuki S, Maddali K, Hashimoto C, Urano E, Ohashi N, Tanaka T, Ozaki T, Arai H, Tsutsumi H, Narumi T, Nomura W, Yamamoto N, Pommier Y, Komano JA, Tamamura H. Bioorganic &amp; Medicinal Chemistry. 2010; 18(18):6771-5. 2010. PMID[20708407]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="title">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20709544">Synthesis of polyhydroxylated aromatics having amidation of piperazine nitrogen as HIV-1 integrase inhibitor.</a> Yang L, Xu X, Huang Y, Zhang B, Zeng C, He H, Wang C, Hu L. Bioorganic &amp; Medicinal Chemistry Letters. 2010; 20(18):5469-71. 2010. PMID[20709544]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="title">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20580393">Design and evaluation of antiretroviral peptides corresponding to the C-terminal heptad repeat region (C-HR) of human immunodeficiency virus type 1 envelope glycoprotein gp41.</a> Soonthornsata B, Tian YS, Utachee P, Sapsutthipas S, Isarangkura-na-Ayuthaya P, Auwanit W, Takagi T, Ikuta K, Sawanpanyalert P, Kawashita N, Kameoka M. Virology. 2010; 405(1):157-64. 2010. PMID[20580393]</p>

    <p class="pmid"><b>[Pubmed]</b>. HIV_0903-091610</p>

    <p class="title"></p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="ListParagraph">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281181200004">Schisanartane Nortriterpenoids with Diverse Post-modifications from Schisandra propinqua</a>. Lei, C; Huang, SX; Xiao, WL; Li, XN; Pu, JX; Sun, HD. Journal of Natural Products, 2010. 73(8): 1337-1343.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281247000007">Discovery of potent HIV integrase inhibitors active against raltegravir resistant viruses</a>. Le, GA; Vandegraaff, N; Rhodes, DI; Jones, ED; Coates, JAV; Lu, L; Li, XM; Yu, CJ; Feng, X; Deadman, JJ. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(17): 5013-5018.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281247000011">Development of 2-pyrrolidinyl-N-methyl pyrimidones as potent and orally bioavailable HIV integrase inhibitors</a>. Ferrara, M; Fiore, F; Summa, V; Gardelli, C. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(17): 5031-5034.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281247000077">Studies on the structure-activity relationship of 1,3,3,4-tetra-substituted pyrrolidine embodied CCR5 receptor antagonists. Part 2: Discovery of highly potent anti-HIV agents</a>. Li, B; Jones, ED; Zhou, EK; Chen, L; Baylis, DC; Yu, SH; Wang, MA; He, X; Coates, JAV; Rhodes, DI; Pei, G; Deadman, JJ; Xie, X; Ma, DW. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(17): 5334-5336.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281247000083">Corrigendum to &quot;Novel indole-3-sulfonamides as potent HIV non-nucleoside reverse transcriptase inhibitors (NNRTIs)&quot; (vol , pg 554, 2008)</a>. Zhao, ZJ; Wolkenberg, SE; Sanderson, PEJ; Lu, MQ; Munshi, V; Moyer, G; Feng, MZ; Carella, AV; Ecto, LT; Gabryelski, LJ; Lai, MT; Prasad, SG; Yan, YW; McGaughey, GB; Miller, MD; Lindsley, CW; Hartman, GD; Vacca, JP; Williams, TM. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(17): 5360-5360.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281252700023">Synthesis of polyhydroxylated aromatics having amidation of piperazine nitrogen as HIV-1 integrase inhibitor.</a> Yang, LF; Xu, XM; Huang, YL; Zhang, B; Zeng, CC; He, HQ; Wang, CX; Hu, LM. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(17): 5469-5471.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281203300026">Antiviral agents 2. Synthesis of trimeric naphthoquinone analogues of conocurvone and their antiviral evaluation against HIV</a>. Crosby, IT; Bourke, DG; Jones, ED; de Bruyn, PJ; Rhodes, D; Vandegraaff, N; Cox, S; Coates, JAV; Robertson, AD. Bioorganic &amp; Medicinal Chemistry. 2010;18(17):6442-6450.</p>

    <p class="ListParagraphCxSpMiddle"><b>[WOS]</b>. HIV_0903-091610</p>

    <p class="ListParagraphCxSpMiddle"></p>

    <p class="ListParagraph">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281419200012">Purification and Characterization of a Laccase with Inhibitory Activity Toward HIV-1 Reverse Transcriptase and Tumor Cells from an Edible Mushroom (Pleurotus cornucopiae)</a>. Wong, JH; Ng, TB; Jiang, Y;Liu, F; Sze, SCW; Zhang, KY. Protein and Peptide Letters, 2010; 17(8): 1040-1047.</p>

    <p class="ListParagraphCxSpLast"><b>[WOS]</b>. HIV_0903-091610</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
