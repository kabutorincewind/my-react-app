

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OI3WzaqVy2WoAvRb8j7Z3M6Nu7UZl76PGO55F4q8R9PmC0MTnBO+WHJtgEbcmAaZp4ehVXkYafODZsFN/2QFpuY6G0LcBolNrQTdRKNcpQ6syvKJZD26I035u08=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FE493714" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">August 5 - August 18, 2011</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21824751">Efficacy of Anidulafungin Against Aspergillus niger in Vitro and in Vivo.</a> Calvo, E., F.J. Pastor, E. Mayayo, and J. Guarro, International Journal of Antimicrobial Agents, 2011. <b>[Epub ahead of print]</b>; PMID[21824751].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21811915">Synthesis and Antimicrobial Activity of New 1-[(Tetrazol-5-yl)methyl] indole Derivatives, Their 1,2,4-Triazole thioglycosides and Acyclic Analogs.</a> El-Sayed, W.A., R.E. Abdel Megeid, and H.A. Abbas, Archives of Pharmacal Research, 2011. 34(7): p. 1085-1096; PMID[21811915].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21831103">Substituted 2-Aminothiophenes: Antifungal Activities and Effect on Microsporum gypseum Protein Profile.</a> Fogue, P.S., P.K. Lunga, E.S. Fondjo, J. De Dieu Tamokou, B. Thaddee, J. Tsemeugne, A.T. Tchapi, and J.R. Kuiate, Mycoses, 2011. <b>[Epub ahead of print]</b>; PMID[21831103].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21832971">Synthesis and Biological Activity of New 1,3-Dioxolanes as Potential Antibacterial and Antifungal Compounds.</a> Kucuk, H.B., A. Yusufoglu, E. Mataraci, and S. Dosler, Molecules (Basel, Switzerland), 2011. 16(8): p. 6806-6815; PMID[21832971].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21842426">In Vitro Activities of New Triazole Antifungal Agents, Posaconazole and Voriconazole, Against Oral Candida Isolates from Patients Suffering from Denture Stomatitis.</a> Marcos-Arias, C., E. Eraso, L. Madariaga, A.J. Carrillo-Munoz, and G. Quindos, Mycopathologia, 2011. <b>[Epub ahead of print]</b>; PMID[21842426].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21825291">In Vitro Antifungal Activity of E1210, a Novel Antifungal, Against Clinically Important Yeasts and Moulds.</a> Miyazaki, M., T. Horii, K. Hata, N.A. Watanabe, K. Nakamoto, K. Tanaka, S. Shirotori, N. Murai, S. Inoue, M. Matsukura, S. Abe, K. Yoshimatsu, and M. Asada, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21825291].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21844312">In Vitro Activity of a Novel Broad-spectrum Antifungal, E1210, Tested Against Aspergillus spp. as Determined by CLSI and EUCAST Broth Microdilution Methods.</a> Pfaller, M.A., F. Duncanson, S.A. Messer, G.J. Moet, R.N. Jones, and M. Castanheira, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21844312].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21830763">A Protein from Red Cabbage (Brassica oleracea) Seeds withs Antifungal, Antibacterial and Anticancer Activities.</a> Ye, X., T.B. Ng, Z. Wu, L. Xie, E.F. Fang, J.H. Wong, W. Pan, S.C. Sze, and Y. Zhang, Journal of Agricultural and Food Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21830763].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21776980">ESI-MS Assay of M. tuberculosis Cell Wall Antigen 85 Enzymes Permits Substrate Profiling and Design of a Mechanism-Based Inhibitor.</a> Barry, C.S., K.M. Backus, C.E. Barry, and B.G. Davis, Journal of the American Chemical Society, 2011. <b>[Epub ahead of print]</b>; PMID[21776980].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21844321">Clofazimine Analogs with Efficacy against Experimental Tuberculosis and Reduced Potential for Accumulation.</a> Lu, Y., M. Zheng, B. Wang, L. Fu, W. Zhao, P. Li, J. Xu, H. Zhu, H. Jin, D. Yin, H. Huang, A.M. Upton, and Z. Ma, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21844321].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21843895">Pitipeptolides C-F, Antimycobacterial Cyclodepsipeptides from the Marine Cyanobacterium Lyngbya majuscula from Guam.</a> Montaser, R., V.J. Paul, and H. Luesch, Phytochemistry, 2011.</p>

    <p class="plaintext"><b>[Epub ahead of print]</b>; PMID[21843895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21811925">Antimycobacterial Activity of Fusaric acid from a Mangrove Endophyte and Its Metal Complexes.</a> Pan, J.H., Y. Chen, Y.H. Huang, Y.W. Tao, J. Wang, Y. Li, Y. Peng, T. Dong, X.M. Lai, and Y.C. Lin, Archives of Pharmacal Research, 2011. 34(7): p. 1177-1181; PMID[21811925].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21830090">Antiplasmodial Indole Alkaloids from Leuconotis griffithii.</a> Nugroho, A.E., Y. Hirasawa, W.C. Piow, T. Kaneda, A.H. Hadi, O. Shirota, W. Ekasari, A. Widyawaruyanti, and H. Morita, Journal of Natural Medicines, 2011. <b>[Epub ahead of print]</b>; PMID[21830090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21814840">Antimalarial Drug Interactions of Compounds Isolated from Kigelia africana (Bignoniaceae) and Their Synergism with Artemether, Against the Multidrug-resistant W2mef Plasmodium falciparum Strain.</a> Zofou, D., M. Tene, P. Tane, and V.P. Titanji, Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[21814840].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>
        
    <p class="memofmt2-2">ANTI LEISHMANIA COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21757358">Trimethoxy-chalcone Derivatives Inhibit Growth of Leishmania braziliensis: Synthesis, Biological Evaluation, Molecular Modeling and Structure-Activity Relationship (SAR).</a> Bello, M.L., L.D. Chiaradia, L.R. Dias, L.K. Pacheco, T.R. Stumpf, A. Mascarello, M. Steindel, R.A. Yunes, H.C. Castro, R.J. Nunes, and C.R. Rodrigues, Bioorganic &amp; Medicinal Chemistry, 2011. 19(16): p. 5046-5052; PMID[21757358].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21810308">Leishmanicidal Effect of Spiranthera odoratissima (Rutaceae) and Its Isolated Alkaloid Skimmianine Occurs by a Nitric oxide Dependent Mechanism.</a> Dos Santos, R.A., J. Batista, S.I. Rosa, H.F. Torquato, C.L. Bassi, T.A. Ribeiro, D.E.S. PT, A.M. Bessera, C.J. Fontes, D.A.S. LE, and M.R. Piuvezam, Parasitology, 2011. <b>[Epub ahead of print]</b>; PMID[21810308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21809846">Synthetic Chromanol Derivatives and Their Interaction with Complex III in Mitochondria from Bovine, Yeast, and Leishmania.</a> Monzote, L., W. Stamberg, A. Patel, T. Rosenau, L. Maes, P. Cos, and L. Gille, Chemical Research in Toxicology, 2011. <b>[Epub ahead of print]</b>; PMID[21809846].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>
    
    <p> </p>
    
    <p class="NoSpacing">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293096000024">Antimicrobial and Antihyperlipidemic Activities of Isolated Quercetin from Anabaena aequalis.</a> Abdel-Raouf, N., I.B.M. Ibraheem, S. Abdel-Tawab, and Y.A.G. Naser, Journal of Phycology, 2011. 47(4): p. 955-962. ISI[000293096000024].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293193200063">Spectral and Biological Studies of Transition Metal Complexes of 2-(4-Nitrophenylaminocarbonyl)benzoic Acid.</a> Ashraf, M.A., M.J. Maah, and I. Yusoff, Synthesis, Asian Journal of Chemistry, 2011. 23(7): p. 3117-3120. ISI[000293193200063].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292953500007">Sulfonyl-hydrazones of Cyclic Imides Derivatives as Potent Inhibitors of the Mycobacterium tuberculosis Protein tyrosine phosphatase B (PtpB).</a> de Oliveira, K.N., L.D. Chiaradia, P.G.A. Martins, A. Mascarello, M.N.S. Cordeiro, R.V.C. Guido, A.D. Andricopulo, R.A. Yunes, R.J. Nunes, J. Vernal, and H. Terenzi, MedChemComm, 2011. 2(6): p. 500-504. ISI[000292953500007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293320100055">Antioxidant, Antibacterial, and Antiviral Effects of Lactuca sativa Extracts.</a> Edziri, H.L., M.A. Smach, S. Ammar, M.A. Mahjoub, Z. Mighri, M. Aouni, and M. Mastouri, Industrial Crops and Products, 2011. 34(1): p. 1182-1185. ISI[000293320100055].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293189100017">Antifungal Effect of Lavender Honey Against Candida albicans, Candida krusei and Cryptococcus neoformans.</a> Estevinho, M.L., S.E. Afonso, and X. Feas, Journal of Food Science and Technology-Mysore, 2011. 48(5): p. 640-643. ISI[000293189100017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292864200018">Effects of Metal and the Phytyl Chain on Chlorophyll Derivatives: Physicochemical Evaluation for Photodynamic Inactivation of Microorganisms.</a> Gerola, A.P., A. Santana, P.B. Franca, T.M. Tsubone, H.P.M. de Oliveira, W. Caetano, E. Kimura, and N. Hioka, Photochemistry and Photobiology, 2011. 87(4): p. 884-894. ISI[000292864200018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293158800006">Amphotericin B Induces Trehalose Synthesis and Simultaneously Activates an Antioxidant Enzymatic Response in Candida albicans.</a> Gonzalez-Parraga, P., R. Sanchez-Fresneda, O. Zaragoza, and J.C. Arguelles, Biochimica Et Biophysica Acta-General Subjects, 2011. 1810(8): p. 777-783. ISI[000293158800006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293168000053">Efficient Synthesis of Antifungal Active 9-Substituted-3-aryl-5H, 13aH-quinolino 3,2-f 1,2,4 triazolo 4,3-b 1,2,4 triazepines in Ionic Liquids.</a> Gupta, M., Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(16): p. 4919-4923. ISI[000293168000053].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293151500005">Short- and Medium-chain Fatty acids Exhibit Antimicrobial Activity for Oral Microorganisms.</a> Huang, C.B., Y. Alimova, T.M. Myers, and J.L. Ebersole, Archives of Oral Biology, 2011. 56(7): p. 650-654. ISI[000293151500005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292919800020">Bothrops pirajai Snake Venom L-Amino acid oxidase: In Vitro Effects on Infection of Toxoplasma gondii in Human Foreskin Fibroblasts.</a> Izidoro, L.F.M., L.M. Alves, V.M. Rodrigues, D.A.O. Silva, and J.R. Mineo, Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(3): p. 477-485. ISI[000292919800020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292985000006">Influences of Hyaluronic acid on the Anticandidal Activities of Lysozyme and the Peroxidase System.</a> Kang, J.H., Y.Y. Kim, J.Y. Chang, and H.S. Kho, Oral Diseases, 2011. 17(6): p. 577-583. ISI[000292985000006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293175100027">Identification of Inhibitors against Mycobacterium tuberculosis Thiamin Phosphate Synthase, an Important Target for the Development of Anti-TB Drugs.</a> Khare, G., R. Kar, and A.K. Tyagi, Plos One, 2011. 6(7). ISI[000293175100027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293202900021">Synthesis of Tetrazolo 1,5-a Quinoxaline Based Azetidinones &amp; Thiazolidinones as Potent Antibacterial &amp; Antifungal Agents.</a> Kumar, S., S.A. Khan, O. Alam, R. Azim, A. Khurana, M. Shaquiquzzaman, N. Siddiqui, and W. Ahsan, Bulletin of the Korean Chemical Society, 2011. 32(7): p. 2260-2266. ISI[000293202900021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292864200020">Photodynamic Therapy with Pc 4 Induces Apoptosis of Candida albicans.</a> Lam, M., P.C. Jou, A.A. Lattif, Y. Lee, C.L. Malbasa, P.K. Mukherjee, N.L. Oleinick, M.A. Ghannoum, K.D. Cooper, and E.D. Baron, Photochemistry and Photobiology, 2011. 87(4): p. 904-909. ISI[000292864200020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293019500017">Synthesis and in Vitro Antimicrobial Activity of New 3-(2-Morpholinoquinolin-3-yl) Substituted Acrylonitrile and Propanenitrile Derivatives.</a> Makawana, J.A., M.P. Patel, and R.G. Patel, Chemical Papers, 2011. 65(5): p. 700-706. ISI[000293019500017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293221200013">Synthesis, Characterization of (E)-N&#39;-(Substituted-benzylidene)isonicotino-hydrazide Derivatives as Potent Antitubercular Agents.</a> Malhotra, M., R. Sharma, V. Monga, A. Deep, K. Sahu, and A. Samad, Letters in Drug Design &amp; Discovery, 2011. 8(6): p. 575-579. ISI[000293221200013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293168000013">Diversity Oriented Design of Various Hydrazides and Their in Vitro Evaluation against Mycobacterium tuberculosis H37(Rv) Strains.</a> Manvar, A., A. Bavishi, A. Radadiya, J. Patel, V. Vora, N. Dodia, K. Rawal, and A. Shah, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(16): p. 4728-4731. ISI[000293168000013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293340200061">Antimicrobial Screening of Some Plants of Medicinal Importance.</a> Mehjabeen, M. Ahmad, N. Jahan, M. Zia-ul-Haq, S.M. Alam, A. Wazir, and H. Saeedul, Pakistan Journal of Botany, 2011. 43(3): p. 1773-1775. ISI[000293340200061].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293092400008">Synthesis and Biological Activity of N-{5-(4-Methylphenyl) diazenyl-4-phenyl-1, 3-thiazol-2-yl}benzamide Derivatives.</a> Prajapati, A.K. and V.P. Modi, Quimica Nova, 2011. 34(5): p. 771-774. ISI[000293092400008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293316800010">Antimycobacterial Activity of Spirooxindolo-pyrrolidine, pyrrolizine and pyrrolothiazole hybrids Obtained by a Three-component Regio- and Stereoselective 1,3-Dipolar Cycloaddition.</a> Rajesh, S.M., S. Perumal, J.C. Menendez, P. Yogeeswari, and D. Sriram, Medchemcomm, 2011. 2(7): p. 626-630. ISI[000293316800010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292919800015">Influence of Gamma Radiation on the Antimicrobial Activity of Crude Extracts of Anacardium occidentale Rich in Tannins.</a> Santos, G.H.F., E.B. Silva, B.L. Silva, K. Sena, and C.S.A. Lima, Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 21(3): p. 444-449. ISI[000292919800015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293039800002">Synthesis, Characterisation and Biological Aspects of Iron(iii) and Cobalt(iii) Complexes with Schiff Bases Derived from Substituted Mercaptotriazole.</a> Shalini, N.T. and V.K. Sharma, Revue Roumaine De Chimie, 2011. 56(3): p. 189. ISI[000293039800002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293030600019">A Latex Lectin from Euphorbia trigona is a Potent Inhibitor of Fungal Growth.</a> van Deenen, N., D. Prufer, and C.S. Gronover, Biologia Plantarum, 2011. 55(2): p. 335-339. ISI[000293030600019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292714000006">Composition and in Vitro Antimicrobial Activity of the Essential Oil of Dorema ammoniacum D. Don. Fruit from Iran.</a> Yousefzadi, M., M.H. Mirjalili, N. Alnajar, A. Zeinali, and M. Parsa, Journal of the Serbian Chemical Society, 2011. 76(6): p. 857-863. ISI[000292714000006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>

    <p> </p>

    <p class="NoSpacing">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293042200006">Specific Inhibition of Candida albicans Growth in Vitro by Antibodies from Experimental Candida keratitis Mice.</a> Zhang, H.B., C.K. Jia, H.J. Xi, S.Y. Li, L.L. Yang, and Y.Q. Wang, Experimental Eye Research, 2011. 93(1): p. 50-58. ISI[000293042200006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0805-081811.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
