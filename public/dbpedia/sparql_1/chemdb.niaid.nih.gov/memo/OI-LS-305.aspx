

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-305.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VdzP0h9Vb9OH7sx+POJ8eN8/niFhhfuxwHviSMwthcCFUV/Ev0KXpAtGsdjo4xsfKmgapCh76Og+PrK6NO4ZheJdvc1loQRrzjaj+1zTORm6Cr71BMebHRAtlSg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D5D47EE3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-305-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44074   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Natural and experimental infection of immunocompromised rhesus macaques (Macaca mulatta) with the microsporidian Enterocytozoon bieneusi genotype D</p>

    <p>          Green, Linda C, Didier, Peter J, Bowers, Lisa C, and Didier, Elizabeth S</p>

    <p>          Microbes and Infection <b>2004</b>.  6(11): 996-1002</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         44075   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of 3-hydrazino-quinoxaline derivatives and their cyclic analogues</p>

    <p>          El-Bendary, ER, Goda, FE, Maarouf, AR, and Badria, FA</p>

    <p>          Scientia Pharmaceutica <b>2004</b>.  72(2): 175-185</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>3.         44076   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          The efficiency of the benzothiazole APB, the echinocandin micafungin, and amphotericin B in fluconazole-resistant Candida albicans and Candida dubliniensis</p>

    <p>          Melkusova, S, Bujdakova, H, Vollekova, A, Myoken, Y, and Mikami, Y</p>

    <p>          Pharmazie <b>2004</b>.  59(7): 573-574</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         44077   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Discovery of fungicide micafungin</p>

    <p>          Hashimoto, Seiji</p>

    <p>          Gendai Kagaku <b>2004</b>.  399: 56-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>5.         44078   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          In vitro antifungal activity of membrane-active peptides against dermatophytes and yeasts using broth microdilution method</p>

    <p>          Choi, Suk-Jin, Sung, Jung-Hyun, Kim, Jeong Aee, Lee, Dong-Youn, Lee, Joo-Heung, Lee, Eil-Soo, and Yang, Jun-Mo</p>

    <p>          Journal of Dermatological Science <b>2004</b>.  35(1): 64-67</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>6.         44079   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Chemical Composition and Antimicrobial Activity of the Essential Oils from the Gum of Turkish Pistachio (Pistacia vera L.)</p>

    <p>          Alma, Mehmet Hakki, Nitz, Siegfried, Kollmannsberger, Hubert, Digrak, Metin, Efe, Fatih Tuncay, and Yilmaz, Necmettin</p>

    <p>          Journal of Agricultural and Food Chemistry <b>2004</b>.  52(12): 3911-3914</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>7.         44080   OI-LS-305; PUBMED-OI-9/20/2004</p>

    <p class="memofmt1-2">          Inhibition of the growth of Toxoplasma gondii in immature human dendritic cells is dependent on the expression of TNF-alpha receptor 2</p>

    <p>          Giese, A, Stuhlsatz, S, Daubener, W, and MacKenzie, CR</p>

    <p>          J Immunol <b>2004</b>.  173(5): 3366-74</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15322200&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15322200&amp;dopt=abstract</a> </p><br />

    <p>8.         44081   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Synthesis of BILN 2061, an HCVNS3 protease inhibitor with proven antiviral effect in humans</p>

    <p>          Faucher, AM, Bailey, MD, Beaulieu, PL, Brochu, C, Duceppe, JS, Ferland, JM, Ghiro, E, Gorys, V, Halmos, T, Kawai, SH, Poirier, M, Simoneau, B, Tsantrizos, YS, and Llinas-Brunet, M</p>

    <p>          ORGANIC LETTERS <b>2004</b>.  6(17): 2901-2904, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223296800017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223296800017</a> </p><br />

    <p>9.         44082   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Coronamycins, peptide antibiotics produced by a verticillate Streptomyces sp. (MSU-2110) endophytic on Monstera sp</p>

    <p>          Ezra, David, Castillo, Uvidelio F, Strobel, Gary A, Hess, Wilford M, Porter, Heidi, Jensen, James B, Condron, Margaret AM, Teplow, David B, Sears, Joseph, Maranta, Michelle, Hunter, Michelle, Weber, Barbara, and Yaver, Debbie</p>

    <p>          Microbiology (Reading, United Kingdom) <b>2004</b>.  150(4): 785-793</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>10.       44083   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Indole mannich bases and their antimycobacterial effect</p>

    <p>          Malinka, Wieslaw and Swiatek, Piotr</p>

    <p>          Acta Poloniae Pharmaceutica <b>2004</b>.  61(2): 107-111</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>11.       44084   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Molecular characterization of isoniazid-resistant Mycobacterium tuberculosis clinical strains isolated in the Philippines</p>

    <p>          Herrera, L, Valverde, A, Saiz, P, Saez-Nieto, JA, Portero, JL, and Jimenez, MS</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2004</b>.  23(6): 572-576, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223309000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223309000005</a> </p><br />

    <p>12.       44085   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          In vitro activity of extracts and constituents of Pelagonium against rapidly growing mycobacteria</p>

    <p>          Seidel, V and Taylor, PW</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2004</b>.  23(6): 613-619, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223309000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223309000011</a> </p><br />

    <p>13.       44086   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          New antivirals and antiviral resistance</p>

    <p>          Prober, CG</p>

    <p>          HOT TOPICS IN INFECTION AND IMMUNITY IN CHILDREN <b>2004</b>.  549: 9-12, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222937600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222937600003</a> </p><br />

    <p>14.       44087   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Outcome study on hepatitis C therapy with mistletoe (Viscum album L.; Abnobaviscum) and solanum lycopersicum</p>

    <p>          Matthes, H, Schad, F, Matthes, B, Biesenthal-Matthes, S, and Schenk, G</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A665-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203335">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203335</a> </p><br />
    <br clear="all">

    <p>15.       44088   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Hepatitis C combination therapy with viramidine and peginterferon alfa-2a reduces potential for ribavirin-related hemolytic anemia</p>

    <p>          Gish, R, Nelson, D, Arora, S, Braeckman, R, and Yu, G</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A681-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203410">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203410</a> </p><br />

    <p>16.       44089   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          A phase I/II dose escalation trial assessing tolerance, pharmacokinetics, and antiviral activity of NM283, a novel antiviral treatment for hepatitis C</p>

    <p>          Godofsky, E, Afdhal, N, Rustgi, V, Shick, L, Duncan, L, Zhou, XJ, Chao, G, Fang, C, Fielman, B, Myers, M, and Brown, N</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A681-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203411">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203411</a> </p><br />

    <p>17.       44090   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides targeting HSV and CMV</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  US <b>20040162254</b>  ISSUE DATE: 20040819</p>

    <p>          APPLICATION: 2003-5737  PP: 84 pp., Cont.-in-part of WO 2004 24,919.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.       44091   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Statistical analysis of combined substitutions in NS5A 2209-2248 of hepatitis C virus 1b infection and standard or pegylated interferon plus ribavirin therapy response</p>

    <p>          Moriguchi, H and Sato, C</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A719-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203598">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203598</a> </p><br />

    <p>19.       44092   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Mutations of hepatitis C virus NS5B region and the response to interferon plus ribavirin combination treatment</p>

    <p>          Hamano, K, Sakamoto, N, Enomoto, N, Izumi, N, Asahina, Y, Kurosaki, M, Ueda, E, Tanabe, Y, Maekawa, S, and Watanabe, M</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A721-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203606">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203606</a> </p><br />

    <p>20.       44093   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Efficacy of interferon alfa-2b and ribavirin for the treatment of chronic hepatitis C in veterans with psychiatric and substance abuse disorders</p>

    <p>          Sahu, M and Bini, EJ</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A723-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203615">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203615</a> </p><br />
    <br clear="all">

    <p>21.       44094   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          The effectiveness of pegylated-interferon and ribavirin combination therapy for chronic hepatitis C in an urban clinic setting compared to pivotal clinical trials</p>

    <p>          Johnson, TC, Vaseer, S, Iqbal, S, Clain, DJ, Bodenheimer, HC, and Min, AD</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A724-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203619">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203619</a> </p><br />

    <p>22.       44095   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Antiviral properties of pegylated versus non-pegylated interferon alpha-2b against hepatitis C in cell culture</p>

    <p>          Joshi, V, Awad, K, Fermin, C, and Dash, S</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A760-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203799">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203799</a> </p><br />

    <p>23.       44096   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Specific inhibition of hepatitis C virus replication by cyclosporin A</p>

    <p>          Sakamoto, N, Nakagawa, M, Tanabe, Y, Kanazawa, N, Maekawa, S, Koyama, T, Takeda, Y, Enomoto, N, and Watanabe, M</p>

    <p>          GASTROENTEROLOGY <b>2004</b>.  126(4): A764-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203817">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000220890203817</a> </p><br />

    <p>24.       44097   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Unmasking of tuberculosis following antiviral therapy for hepatitis C</p>

    <p>          Nahon, P, Brugiere, O, Marcellin, P, and Valla, D</p>

    <p>          GASTROENTEROLOGIE CLINIQUE ET BIOLOGIQUE <b>2004</b>.  28(6-7): 609-610, 2</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223191800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223191800014</a> </p><br />

    <p>25.       44098   OI-LS-305; SCIFINDER-OI-9/14/2004</p>

    <p class="memofmt1-2">          Anti-infectives compounds and use for treating hepatitis C virus infection associated diseases</p>

    <p>          Chai, Deping, Duffy, Kevin J, Fitch, Duke M, Shaw, Antony N, Tedesco, Rosanna, Wiggall, Kenneth J, and Zimmerman, Michael N</p>

    <p>          PATENT:  WO <b>2004052312</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       44099   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Enzymatic characterization of the full-length and C-terminally truncated hepatitis C virus RNA polymerases: Function of the last 21 amino acids of the C terminus in template binding and RNA synthesis</p>

    <p>          Vo, NV, Tuler, JR, and Lai, MMC</p>

    <p>          BIOCHEMISTRY <b>2004</b>.  43(32): 10579-10591, 13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223278700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223278700029</a> </p><br />
    <br clear="all">

    <p>27.       44100   OI-LS-305; WOS-OI-9/12/2004</p>

    <p class="memofmt1-2">          Phenyl substitution of furamidine markedly potentiates its anti-parasitic activity against Trypanosoma cruzi and Leishmania amazonensis</p>

    <p>          de Souza, EM,  Lansiaux, A, Bailly, C, Wilson, WD, Hu, Q, Boykin, DW, Batista, MM, Araujo-Jorge, TC, and Soeiro, MNC</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2004</b>.  68(4): 593-600, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207800001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223207800001</a> </p><br />

    <p>28.       44101   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          Purification and characterization of hepatitis C virus non-structural protein 5A expressed in Escherichia coli</p>

    <p>          Huang, LY, Sineva, EV, Hargitta, MRS, Sharma, SD, Suthar, M, Raney, KD, and Cameron, CE</p>

    <p>          PROTEIN EXPRESSION AND PURIFICATION <b>2004</b>.  37(1): 144-153, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223413100019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223413100019</a> </p><br />

    <p>29.       44102   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          A unified approach to polyene macrolides: Synthesis of candidin and nystatin polyols</p>

    <p>          Kadota, I, Hu, YQ, Packard, GK, and Rychnovsky, SD</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2004</b>.  101(33): 11992-11995, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223410100016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223410100016</a> </p><br />

    <p>30.       44103   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          Specific residues in the connector loop of the human cytomegalovirus DNA polymerase accessory protein UL44 are crucial for interaction with the UL54 catalytic subunit</p>

    <p>          Loregian, A, Appleton, BA, Hogle, JM, and Coen, DM</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(17): 9084-9092, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223386600013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223386600013</a> </p><br />

    <p>31.       44104   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          The Mycobacterium tuberculosis sigJ gene controls sensitivity of the bacterium to hydrogen peroxide</p>

    <p>          Hu, YM, Kendall, S, Stoker, NG, and Coates, ARM</p>

    <p>          FEMS MICROBIOLOGY LETTERS <b>2004</b>.  237(2): 415-423, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223385700032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223385700032</a> </p><br />

    <p>32.       44105   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          Dynamics of Toxoplasma gondii differentiation</p>

    <p>          Dzierszinski, F, Nishi, M, Ouko, L, and Roos, DS</p>

    <p>          EUKARYOTIC CELL <b>2004</b>.  3(4): 992-1003, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223418000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223418000018</a> </p><br />

    <p>33.       44106   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          Novel bisbenzamidines as potential drug candidates for the treatment of Pneumocystis carinii pneumonia</p>

    <p>          Eynde, JJV, Mayence, A, Huang, TL, Collins, MS, Rebholz, S, Walzer, PD, and Cushion, MT</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(17): 4545-4548, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223382000035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223382000035</a> </p><br />

    <p>34.       44107   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          New non-nucleoside inhibitors of hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Ivanov, AV, Kozlov, MV, Kuzyakin, AO, Kostyuk, DO, Tunitskaya, VL, and Kochetkov, SN</p>

    <p>          BIOCHEMISTRY-MOSCOW <b>2004</b>.  69(7): 782-788, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223441800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223441800011</a> </p><br />

    <p>35.       44108   OI-LS-305; WOS-OI-9/19/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of novel 2 &#39;,4 &#39;- or 3 &#39;,4 &#39;-doubly branched carbocyclic nucleosides as potential antiviral agents</p>

    <p>          Oh, CH and Hong, JH</p>

    <p>          ARCHIV DER PHARMAZIE <b>2004</b>.  337(8): 457-463, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223437900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223437900006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
