

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-372.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iHBV9HnAbHRm1wegp5iRjnePaTD01Mg+ewdnmzxey1/i1cQ8zcRHRlHwBGuit+3hp8vvLYs8BI9A/X14sCu2FCfTVQ0BhZSrkMIRw/HazMEqleawgTzlmGDprWY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FC47E411" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-372-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60672   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          A Beta-1,2 Xylosyltranferase from cryptococcus neoformans defines a new family of glycosyltransferases</p>

    <p>          Klutts, JS, Levery, SB, and Doering, TL</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430900%20&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430900 &amp;dopt=abstract</a> </p><br />

    <p>2.     60673   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Tuberculosis, Drug Resistance, and HIV/AIDS: A Triple Threat</p>

    <p>          Friedland, G</p>

    <p>          Curr Infect Dis Rep <b>2007</b>.  9(3): 252-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430708&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17430708&amp;dopt=abstract</a> </p><br />

    <p>3.     60674   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Structure-based design, synthesis and preliminary evaluation of selective inhibitors of dihydrofolate reductase from Mycobacterium tuberculosis</p>

    <p>          El-Hamamsy, Mervat HRI, Smith, Anthony W, Thompson, Andrew S, and Threadgill, Michael D</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 2375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NFXDF7-6/2/1e6844a7712bd93ef4a75b2e514639cf">http://www.sciencedirect.com/science/article/B6TF8-4NFXDF7-6/2/1e6844a7712bd93ef4a75b2e514639cf</a> </p><br />

    <p>4.     60675   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Synthesis of new S-derivatives of clubbed triazolyl thiazole as anti-Mycobacterium tuberculosis agents</p>

    <p>          Shiradkar, Mahendra Ramesh, Murahari, Kiran Kumar, Gangadasu, Hanimi Reddy, Suresh, Tatikonda, Kalyan, Chakravarthy Akula, Panchal, Dolly, Kaur, Ranjit, Burange, Prashant, Ghogare, Jyoti, Mokale, Vinod, and Raut, Mayuresh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 2375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NF4F13-4/2/5a4c1a0c569441b5cac62dab2770f38b">http://www.sciencedirect.com/science/article/B6TF8-4NF4F13-4/2/5a4c1a0c569441b5cac62dab2770f38b</a> </p><br />

    <p>5.     60676   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Antituberculosis drugs: Ten years of research</p>

    <p>          Janin, Yves L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(7): 2479-2513</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MVN0XP-C/2/3b23fec7ab8ef0780e9edea90ef4563c">http://www.sciencedirect.com/science/article/B6TF8-4MVN0XP-C/2/3b23fec7ab8ef0780e9edea90ef4563c</a> </p><br />
    <br clear="all">

    <p>6.     60677   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          A New Nucleosidyl-peptide Antibiotic, Sansanmycin</p>

    <p>          Xie, Y, Chen, R, Si, S, Sun, C, and Xu, H</p>

    <p>          J Antibiot (Tokyo) <b>2007</b>.  60(2): 158-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420567&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17420567&amp;dopt=abstract</a> </p><br />

    <p>7.     60678   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Antimycobacterial activity of some trees used in South African traditional medicine</p>

    <p>          Eldeen, IMS and Van Staden, J</p>

    <p>          South African Journal of Botany <b>2007</b>.  73(2): 248-251</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7XN9-4M87BC2-1/2/795c21654ba33df6f66094c528d7280b">http://www.sciencedirect.com/science/article/B7XN9-4M87BC2-1/2/795c21654ba33df6f66094c528d7280b</a> </p><br />

    <p>8.     60679   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p><b>          NLCQ-1 and NLCQ-2, two new agents with activity against dormant Mycobacterium tuberculosis</b> </p>

    <p>          Papadopoulou, Maria V, Bloomer, William D, and McNeil, Michael R</p>

    <p>          International Journal of Antimicrobial Agents <b>2007</b>.  In Press, Corrected Proof: 838</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4NBR3TP-1/2/cc5d0cba3b2ba60527b3fcfd537ecbf6">http://www.sciencedirect.com/science/article/B6T7H-4NBR3TP-1/2/cc5d0cba3b2ba60527b3fcfd537ecbf6</a> </p><br />

    <p>9.     60680   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Antiviral Response of HCV Genotype 1 to Consensus Interferon and Ribavirin Versus Pegylated Interferon and Ribavirin</p>

    <p>          Sjogren, MH, Sjogren, R Jr, Lyons, MF, Ryan, M, Santoro, J, Smith, C, Reddy, KR, Bonkovsky, H, Huntley, B, and Faris-Young, S</p>

    <p>          Dig Dis Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406822&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17406822&amp;dopt=abstract</a> </p><br />

    <p>10.   60681   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of symmetrical two-tailed dendritic tricarboxylato amphiphiles</p>

    <p>          Sugandhi, EW, Falkinham, JO 3rd, and Gandour, RD</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17400459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17400459&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60682   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Further studies on hepatitis C virus NS5B RNA-dependent RNA polymerase inhibitors toward improved replicon cell activities: Benzimidazole and structurally related compounds bearing the 2-morpholinophenyl moiety</p>

    <p>          Hirashima, S, Oka, T, Ikegashira, K, Noji, S, Yamanaka, H, Hara, Y, Goto, H, Mizojiri, R, Niwa, Y, Noguchi, T, Ando, I, Ikeda, S, and Hashimoto, H</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17383878&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17383878&amp;dopt=abstract</a> </p><br />

    <p>12.   60683   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Conference urges greater effort to reduce human threat from avian influenza</p>

    <p>          Parry, J</p>

    <p>          BMJ <b>2007</b>.  334(7594): 607</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17379890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17379890&amp;dopt=abstract</a> </p><br />

    <p>13.   60684   OI-LS-372; PUBMED-OI-4/18/2007</p>

    <p class="memofmt1-2">          Eurysterols A and B, Cytotoxic and Antifungal Steroidal Sulfates from a Marine Sponge of the Genus Euryspongia</p>

    <p>          Boonlarppradab, C and Faulkner, DJ</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17378608&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17378608&amp;dopt=abstract</a> </p><br />

    <p>14.   60685   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          3D-QSAR and molecular docking studies on benzothiazole derivatives as Candida albicans N-myristoyltransferase inhibitors</p>

    <p>          Sheng, Chunquan, Zhu, Jie, Zhang, Wannian, Zhang, Min, Ji, Haitao, Song, Yunlong, Xu, Hui, Yao, Jianzhong, Miao, Zhenyuan, Zhou, Youjun, Zhu, Ju, and Lu, Jiaguo</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(4): 477-486</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MCDJYG-1/2/673ec975a624f6f41f0f6cca68ff33ca">http://www.sciencedirect.com/science/article/B6VKY-4MCDJYG-1/2/673ec975a624f6f41f0f6cca68ff33ca</a> </p><br />

    <p>15.   60686   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activities of Schiff bases derived from 5-chloro-salicylaldehyde</p>

    <p>          Shi, Lei, Ge, Hui-Ming, Tan, Shu-Hua, Li, Huan-Qiu, Song, Yong-Chun, Zhu, Hai-Liang, and Tan, Ren-Xiang</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(4): 558-564</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MFSNYJ-4/2/24756f26f70c578bc9ee3fc9b63361b6">http://www.sciencedirect.com/science/article/B6VKY-4MFSNYJ-4/2/24756f26f70c578bc9ee3fc9b63361b6</a> </p><br />
    <br clear="all">

    <p>16.   60687   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          The open book of infectious diseases</p>

    <p>          Sassetti, CM and Rubin, EJ</p>

    <p>          NATURE MEDICINE <b>2007</b>.  13(3): 279-280, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700034</a> </p><br />

    <p>17.   60688   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Discovery of novel indazole-linked triazoles as antifungal agents</p>

    <p>          Park, Joon Seok, Yu, Kyung A, Kang, Tae Hee, Kim, Sunghoon, and Suh, Young-Ger</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  258</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NC38R9-1/2/fb3b4ead072c200173eeef883c4d54eb">http://www.sciencedirect.com/science/article/B6TF9-4NC38R9-1/2/fb3b4ead072c200173eeef883c4d54eb</a> </p><br />

    <p>18.   60689   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          Challenges in tuberculosis drug research and development</p>

    <p>          Ginsberg, AM and Spigelman, M</p>

    <p>          NATURE MEDICINE <b>2007</b>.  13(3): 290-294, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244715700039</a> </p><br />

    <p>19.   60690   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Synthesis, structure, and structure-activity relationship analysis of enamines as potential antibacterials</p>

    <p>          Xiao, Zhu-Ping, Xue, Jia-Yu, Tan, Shu-Hua, Li, Huan-Qiu, and Zhu, Hai-Liang</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 258</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NB99CH-2/2/80935868282f86b2d9685038ff0f6c51">http://www.sciencedirect.com/science/article/B6TF8-4NB99CH-2/2/80935868282f86b2d9685038ff0f6c51</a> </p><br />

    <p>20.   60691   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          The contribution of oxazolidinone frame to the biological activity of pharmaceutical drugs and natural products</p>

    <p>          Zappia, G, Menendez, P, Delle Monache, G, Misiti, D, Nevola, L, and Botta, B</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY <b>2007</b>.  7(4): 389-409, 21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244695600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244695600004</a> </p><br />

    <p>21.   60692   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Quinazolinone fungal efflux pump inhibitors. Part 3: (N-methyl)piperazine variants and pharmacokinetic optimization</p>

    <p>          Watkins, William J, Chong, Lee, Cho, Aesop, Hilgenkamp, Ramona, Ludwikow, Maria, Garizi, Negar, Iqbal, Nadeem, Barnard, John, Singh, Rajeshwar, Madsen, Deidre, Lolans, Karen, Lomovskaya, Olga, Oza, Uma, Kumaraswamy, Padmapriya, Blecken, Andrea, Bai, Shuang, Loury, David J, Griffith, David C, and Dudley, Michael N</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  892</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N4YMDV-3/2/79a70fc74404550cfda85ff20756059a">http://www.sciencedirect.com/science/article/B6TF9-4N4YMDV-3/2/79a70fc74404550cfda85ff20756059a</a> </p><br />
    <br clear="all">

    <p>22.   60693   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          Enoyl reductases as targets for the development of anti-tubercular and anti-malarial agents</p>

    <p>          Oliveir, JS, Vasconcelos, IB, Moreira, IS, Santos, DS, and Basso, LA</p>

    <p>          CURRENT DRUG TARGETS <b>2007</b>.  8(3): 399-411, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244702900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244702900003</a> </p><br />

    <p>23.   60694   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Evaluation of a Diverse Set of Potential P1 Carboxylic Acid Bioisosteres in Hepatitis C Virus NS3 Protease Inhibitors</p>

    <p>          Ronn, Robert, Gossas, Thomas, Sabnis, Yogesh A, Daoud, Hanna, Akerblom, Eva, Danielson, UHelena, and Sandstrom, Anja</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 2288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NDMN8W-1/2/b9d1556619239dfe9ec7f0e23eb4a703">http://www.sciencedirect.com/science/article/B6TF8-4NDMN8W-1/2/b9d1556619239dfe9ec7f0e23eb4a703</a> </p><br />

    <p>24.   60695   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Inhibitors of Hepatitis C Virus NS3-4A Protease. Effect of P4 Capping Groups on Inhibitory Potency and Pharmacokinetics</p>

    <p>          Perni, Robert B, Chandorkar, Gurudatt, Cottrell, Kevin M, Gates, Cynthia A, Lin, Chao, Lin, Kai, Luong, Yu-Ping, Maxwell, John P, Murcko, Mark A, Pitlik, Janos, Rao, Govinda, Schairer, Wayne C, Van Drie, John, and Wei, Yunyi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 2288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-1/2/7fbe660613e02d9dc3d24884ff9e8aa7">http://www.sciencedirect.com/science/article/B6TF9-4NDDM03-1/2/7fbe660613e02d9dc3d24884ff9e8aa7</a> </p><br />

    <p>25.   60696   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p><b>          Synthesis of novel potent hepatitis C virus NS3 protease inhibitors: Discovery of 4-hydroxy-cyclopent-2-ene-1,2-dicarboxylic acid as a N-acyl-l-hydroxyproline bioisostere</b> </p>

    <p>          Thorstensson, Fredrik, Wangsell, Fredrik, Kvarnstrom, Ingemar, Vrang, Lotta, Hamelink, Elizabeth, Jansson, Katarina, Hallberg, Anders, Rosenquist, Asa, and Samuelsson, Bertil </p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 827-838</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M6935M-6/2/25cb345f5d2242909aa455331b4bf488">http://www.sciencedirect.com/science/article/B6TF8-4M6935M-6/2/25cb345f5d2242909aa455331b4bf488</a> </p><br />

    <p>26.   60697   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          EthA, a common activator of thiocarbamide-containing drugs acting on different mycobacterial targets</p>

    <p>          Dover, LG, Alahari, A, Gratraud, P, Gomes, JM, Bhowruth, V, Reynolds, RC, Besra, GS, and Kremer, L</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(3): 1055-1063, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244665500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244665500037</a> </p><br />
    <br clear="all">

    <p>27.   60698   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          2-Aryl-2-hydroxyethylamine substituted 4-oxo-4,7-dihydrothieno[2,3-b]pyridines as broad-spectrum inhibitors of human herpesvirus polymerases</p>

    <p>          Schnute, Mark E, Anderson, David J, Brideau, Roger J, Ciske, Fred L, Collier, Sarah A, Cudahy, Michele M, Eggen, MariJean, Genin, Michael J, Hopkins, Todd A, Judge, Thomas M, Kim, Euibong J, Knechtel, Mary L, Nair, Sajiv K, Nieman, James A, Oien, Nancee L, Scott, Allen, Tanis, Steven P, Vaillancourt, Valerie A, Wathen, Michael W, and Wieber, Janet L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  2192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NFR535-2/2/222f530e43258e881418f5ad26de7cc8">http://www.sciencedirect.com/science/article/B6TF9-4NFR535-2/2/222f530e43258e881418f5ad26de7cc8</a> </p><br />

    <p>28.   60699   OI-LS-372; EMBASE-OI-4/19/2007</p>

    <p class="memofmt1-2">          Influence of 6 or 8-substitution on the antiviral activity of 3-phenethylthiomethylimidazo[1,2-a]pyridine against human cytomegalovirus (HCMV) and varicella-zoster virus (VZV)</p>

    <p>          Veron, Jean-Baptiste, Enguehard-Gueiffier, Cecile, Snoeck, Robert, Andrei, Graciela, de Clercq, Erik, and Gueiffier, Alain</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 922</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4NB99CH-5/2/4ed00e99d8e9e36524682716d7d3d5d9">http://www.sciencedirect.com/science/article/B6TF8-4NB99CH-5/2/4ed00e99d8e9e36524682716d7d3d5d9</a> </p><br />

    <p>29.   60700   OI-LS-372; WOS-OI-4/8/2007</p>

    <p class="memofmt1-2">          Purine nucleoside phosphorylase: A potential target for the development of drugs to treat T-cell- and apicomplexan parasite-mediated diseases</p>

    <p>          Silva, RG, Nunes, JES, Canduri, F, Borges, JC, Gava, LM, Moreno, FB, Basso, LA, and Santos, DS </p>

    <p>          CURRENT DRUG TARGETS <b>2007</b>.  8(3): 413-422, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244702900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244702900004</a> </p><br />

    <p>30.   60701   OI-LS-372; WOS-OI-4/15/2007</p>

    <p class="memofmt1-2">          Natural product growth inhibitors of Mycobacterium tuberculosis</p>

    <p>          Copp, BR and Pearce, AN</p>

    <p>          NATURAL PRODUCT REPORTS <b>2007</b>.  24(2): 278-297, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245261800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245261800002</a> </p><br />

    <p>31.   60702   OI-LS-372; WOS-OI-4/15/2007</p>

    <p class="memofmt1-2">          Characterization of the binding of isoniazid and analogues to Mycobacterium tuberculosis catalase-peroxidase</p>

    <p>          Zhao, XB, Yu, SW, and Magliozzo, RS</p>

    <p>          BIOCHEMISTRY <b>2007</b>.  46(11): 3161-3170, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244854800025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244854800025</a> </p><br />

    <p>32.   60703   OI-LS-372; WOS-OI-4/15/2007</p>

    <p class="memofmt1-2">          Cell culture system for the screening of anti-HCV reagents: combination therapy of statins with IFN</p>

    <p>          Ikeda, M</p>

    <p>          JOURNAL OF PHARMACOLOGICAL SCIENCES <b>2007</b>.  103: 22P-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245046500042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245046500042</a> </p><br />
    <br clear="all">

    <p>33.   60704   OI-LS-372; WOS-OI-4/15/2007</p>

    <p class="memofmt1-2">          Allicin-induced suppression of Mycobacterium tuberculosis 85B mRNA in human monocytes</p>

    <p>          Hasan, N, Siddiqui, MU, Toossi, Z, Khan, S, Iqbal, J, and Islam, N</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2007</b>.  355(2): 471-476, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244774700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244774700029</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
