

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="O3wNgenfl9tayv4CXjel6eHn4lI7dFNWgBBLMYPFtV9Dku00yiXr0LjxF710clvWsKUoejJp8YS+N05zkO3LzwmhcKzWjSWlWODH5GHDI/dCqZ9yG8+HvzgMikI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6E3A84B4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: June, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26643325">New Verapamil Analogs Inhibit Intracellular Mycobacteria without Affecting the Functions of Mycobacterium-specific T Cells.</a> Abate, G., P.G. Ruminiski, M. Kumar, K. Singh, F. Hamzabegovic, D.F. Hoft, C.S. Eickhoff, A. Selimovic, M. Campbell, and K. Chibale. Antimicrobial Agents and Chemotherapy, 2016. 60(3): p. 1216-1225. PMID[26643325]. PMCID[PMC4775996].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">2.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/25945747">Synthesis, Structure, Antimycobacterial and Anticancer Evaluation of New Pyrrolo-phenanthroline Derivatives.</a> Al Matarneh, C.M., Mangalagiu, II, S. Shova, and R. Danac. Journal of Enzyme Inhibition and Medicinal Chemistry, 2016. 31(3): p. 470-480. PMID[25945747]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26804238">Synthesis and Antimicrobial Activity of 4-Chloro-3-nitrophenylthiourea Derivatives Targeting Bacterial Type II Topoisomerases.</a> Bielenica, A., K. Stepien, A. Napiorkowska, E. Augustynowicz-Kopec, S. Krukowski, M. Wlodarczyk, and M. Struga. Chemical Biology and Drug Design, 2016. 87(6): p. 905-917. PMID[26804238]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27053677">In Vitro Comparison of Ertapenem, Meropenem, and Imipenem against Isolates of Rapidly Growing Mycobacteria and Nocardia by Use of Broth Microdilution and Etest.</a> Brown-Elliott, B.A., J. Killingley, S. Vasireddy, L. Bridge, and R.J. Wallace, Jr. Journal of Clinical Microbiology, 2016. 54(6): p. 1586-1592. PMID[27053677]. PMCID[PMC4879266].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">5.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000375721900010">Synthesis and Biological Activity of the Liposidomycins and Caprazamycins, Members of a Novel Class of Diazepanone-containing Nucleosides.</a> Cheng-Sanchez, I., C. Garcia-Ruiz, J.I. Trujillo, and F. Sarabia.  Targets in Heterocyclic Systems: Chemistry and Properties, 2015. 19: p. 239-273. ISI[000375721900010].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/25910087">Indole-based Hydrazide-hydrazones and 4-Thiazolidinones: Synthesis and Evaluation as Antitubercular and Anticancer Agents.</a> Cihan-Ustundag, G., D. Satana, G. Ozhan, and G. Capan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2016. 31(3): p. 369-380. PMID[25910087]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27242231">Anti-mycobacterial Activity of Polyketides from Penicillium sp. endophyte Isolated from Garcinia nobilis against Mycobacterium smegmatis.</a> Jouda, J.B., I.K. Mawabo, A. Notedji, C.D. Mbazoa, J. Nkenfou, J. Wandji, and C.N. Nkenfou. International Journal of Mycobacteriology, 2016. 5(2): p. 192-196. PMID[27242231]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">8.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27265685">2-Aryl-8-aza-3-deazaadenosine Analogues of 5&#39;-O- N-(salicyl)sulfamoyl Adenosine: Nucleoside Antibiotics That Block Siderophore Biosynthesis in Mycobacterium tuberculosis.</a> Krajczyk, A., J. Zeidler, P. Januszczyk, S. Dawadi, H.I. Boshoff, C.E. Barry, T. Ostrowski, and C.C. Aldrich. Bioorganic &amp; Medicinal Chemistry, 2016. 24(14): p. 3133-3143. PMID[27265685]. PMCID[PMC4914392].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">9.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26706033">Design, Synthesis and Bioactivity Evaluation of Galf Mimics as Antitubercular Agents.</a> Liu, C., L. Hou, A. Meng, G. Han, W. Zhang, and S. Jiang. Carbohydrate Research, 2016. 429: p. 135-142. PMID[26706033]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27271013">Novel TetR Family Transcriptional Factor Regulates Expression of Multiple Transport-related Genes and Affects Rifampicin Resistance in Mycobacterium smegmatis.</a> Liu, H., M. Yang, and Z.G. He. Scientific Reports, 2016. 6( 27489): 10pp. PMID[27271013]. PMCID[PMC4895335].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27061982">Benzo[d]thiazol-2-yl(piperazin-1-yl)methanones as New Anti-mycobacterial Chemotypes: Design, Synthesis, Biological Evaluation and 3D-QSAR Studies.</a> Pancholia, S., T.M. Dhameliya, P. Shah, P.S. Jadhavar, J.P. Sridevi, P. Yogeshwari, D. Sriram, and A.K. Chakraborti. European Journal of Medicinal Chemistry, 2016. 116: p. 187-199. PMID[27061982]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">12.   <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000375955900008">Synthesis and Evaluation of Original Bioisosteres of Bacterial Type IIA Topoisomerases Inhibitors.</a> Petrella, S., A. Aubry, G. Janvier, E.P. Coutant, A. Cartier, T.H. Dao, F.J. Bonhomme, L. Motreff, C. Pissis, C. Bizet, D. Clermont, E. Begaud, P. Retailleau, H. Munier-Lehmann, E. Capton, C. Mayer, and Y.L. Janin. Canadian Journal of Chemistry, 2016. 94(3): p. 240-250. ISI[000375955900008]. <b><br />
    [WOS]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/26887318">Screening of Antitubercular Compound Library Identifies Novel Shikimate Kinase Inhibitors of Mycobacterium tuberculosis.</a> Rajput, V.S., R. Mehra, S. Kumar, A. Nargotra, P.P. Singh, and I.A. Khan. Applied Microbiology and Biotechnology, 2016. 100(12): p. 5415-5426. PMID[26887318]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27114277">Small-molecule Inhibitors Targeting Topoisomerase I as Novel Antituberculosis Agents.</a> Sandhaus, S., T. Annamalai, G. Welmaker, R.A. Houghten, C. Paz, P.K. Garcia, A. Andres, G. Narula, C. Rodrigues Felix, S. Geden, M. Netherton, R. Gupta, K.H. Rohde, M.A. Giulianotti, and Y.C. Tse-Dinh. Antimicrobial Agents and Chemotherapy, 2016. 60(7): p. 4028-4036. PMID[27114277]. PMCID[PMC4914652].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27242232">Limonia acidissima L. Leaf Mediated Synthesis of Zinc Oxide Nanoparticles: A Potent Tool against Mycobacterium tuberculosis.</a> Taranath, T.C. and B.N. Patil. International Journal of Mycobacteriology, 2016. 5(2): p. 197-204. PMID[27242232]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27251120">Synthesis and Evaluation of New 2-Aminothiophenes against Mycobacterium tuberculosis.</a> Thanna, S., S.E. Knudson, A. Grzegorzewicz, S. Kapil, C.M. Goins, D.R. Ronning, M. Jackson, R.A. Slayden, and S.J. Sucheck. Organic &amp; Biomolecular Chemistry, 2016. 14(25): p. 6119-6133. PMID[27251120]. PMCID[PMC4918453].<b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27010218">Potent Inhibitors of Acetyltransferase Eis Overcome Kanamycin Resistance in Mycobacterium tuberculosis.</a> Willby, M.J., K.D. Green, C.S. Gajadeera, C. Hou, O.V. Tsodikov, J.E. Posey, and S. Garneau-Tsodikova. ACS Chemical Biology, 2016. 11(6): p. 1639-1646. PMID[27010218]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000376654400021">Reduction of Minimum Inhibitory Concentrations in Drug-resistant Mycobacterium tuberculosis Isolates in the Presence of Efflux Pump Inhibitors.</a> Yadav, R., S.K. Dhatwalia, A. Mewara, D. Behera, and S. Sethi. Journal of Global Antimicrobial Resistance, 2016. 5: p. 88-89. ISI[000376654400021].<b><br />
    [WOS]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27214150">Nontoxic Metal-cyclam Complexes, a New Class of Compounds with Potency against Drug-resistant Mycobacterium tuberculosis.</a> Yu, M., G. Nagalingam, S. Ellis, E. Martinez, V. Sintchenko, M. Spain, P.J. Rutledge, M.H. Todd, and J.A. Triccas. Journal of Medicinal Chemistry, 2016. 59(12): p. 5917-5921. PMID[27214150]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27241693">Antimycobacterial Activity of New N(1)-[1-[1-aryl-3-[ 4-(1H-imidazol-1-yl)phenyl]-3-oxo]propyl]-pyridine-2-carboxamidrazone Derivatives.</a> Zampieri, D., M.G. Mamolo, L. Vio, M. Romano, N. Skoko, M. Baralle, V. Pau, and A. De Logu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(14): p. 3287-3290. PMID[27241693]. <b><br />
    [PubMed]</b>. TB_06_2015.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000376512000014">Design, Synthesis and Anti-mycobacterial Evaluation of Some New N-Phenylpyrazine-2-carboxamides.</a> Zitko, J., B. Servusova-Vanaskova, P. Paterova, L. Navratilova, F. Trejtnar, J. Kunes, and M. Dolezal. Chemical Papers, 2016. 70(5): p. 649-657. ISI[000376512000014]. <b><br />
    [WOS]</b>. TB_06_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">22. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160602&amp;CC=WO&amp;NR=2016082934A1&amp;KC=A1">Novel Cystobactamides of Cystobacter as Antibiotics and Synthesis of Cystobactamide C Derivatives.</a> Baumann, S., J. Herrmann, K. Mohr, H. Steinmetz, K. Gerth, R. Raju, R. Mueller, R. Hartmann, M. Hamed, W.A.M. Elgaher, M. Moreno, F. Gille, L.L. Wang, A. Kirschning, and S. Huettel. Patent. 2016. 2015-EP2382 2016082934: 81pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>

    <br />

    <p class="plaintext">23. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160623&amp;CC=WO&amp;NR=2016095877A1&amp;KC=A1">Substituted 2-(2-Phenylhydrazinyl)pyrazine, Process for Its Preparation, Its Use and a Pharmaceutical Composition Containing the Same.</a> Dolezal, M., J. Zitko, O. Jandourek, and B. Servusova-Vanaskova. Patent. 2016. 2015-CZ127 2016095877: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>

    <br />

    <p class="plaintext">24. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160630&amp;CC=WO&amp;NR=2016102541A1&amp;KC=A1">Composition Comprising Vancomycin and Orlistat.</a> Fontaine, V. and P. Lefevre. Patent. 2016. 2015-EP80933 2016102541: 56pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>

    <br />

    <p class="plaintext">25. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160609&amp;CC=WO&amp;NR=2016087618A1&amp;KC=A1">Preparation of Imidazole-Based Antimicrobial Agents.</a> Oender, K., R. Datema, D. Mitchell, and I. Kondratov. Patent. 2016. 2015-EP78587 2016087618: 308pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>

    <br />

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160616&amp;CC=WO&amp;NR=2016091228A1&amp;KC=A1">Preparation of 3,5-Dinitrophenyltetrazoles and Pharmaceutical Preparation Containing the Same for the Treatment of Tuberculosis.</a> Roh, J., J. Nemecek, A. Hrabalek, V. Klimesova, G. Karabanovich, P. Pavek, and P. Sychra. Patent. 2016. 2015-CZ126 2016091228: 26pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160623&amp;CC=WO&amp;NR=2016095878A1&amp;KC=A1">Substituted Derivative of Oxyphosphorus acids, Its Use and Pharmaceutical Preparation Containing It.</a> Vinsova, J., M. Kratky, and G. Paraskevopoulos. Patent. 2016. 2015-CZ129 2016095878: 39pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_06_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
