

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yQA9HAj+/B5g3v3IfIdt0TJQ70t9p2oWpQ8pJtVMZ+y3AeVM0M0XBMlU/Q5kJUywDkLogkMRWpeuR/lacvxGbAd0e5IL1D3guJc/DuyO9SPQ2XA/SZWcLnGMYao=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E1605D49" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: October, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28743447">Dual-targeted anti-TB/anti-HIV Heterodimers.</a> Alexandrova, L., S. Zicari, E. Matyugina, A. Khandazhinskaya, T. Smirnova, S. Andreevskaya, L. Chernousova, C. Vanpouille, S. Kochetkov, and L. Margolis. Antiviral Research, 2017. 145: p. 175-183. PMID[28743447]. PMCID[PMC5576140].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28974052">Antimycobacterial Activities of N-Substituted-glycinyl 1H-1,2,3-triazolyl oxazolidinones and Analytical Method Development and Validation for a Representative Compound.</a> Al-Tannak, N.F. and O.A. Phillips. Scientia Pharmaceutica, 2017. 85(4): E34. PMID[28974052].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29065539">Ureidopyrazine Derivatives: Synthesis and Biological Evaluation as Anti-infectives and Abiotic Elicitors.</a> Bouz, G., M. Juhas, P. Niklova, O. Jandourek, P. Paterova, J. Janousek, L. Tumova, Z. Kovalikova, P. Kastner, M. Dolezal, and J. Zitko. Molecules, 2017. 22(10): E1797. PMID[29065539].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000411039200032">2-(3,4-Dichlorophenylimino)-5-((3-(p-substitutedphenyl)-1-phenyl-1H-pyrazol-4-yl)methylene)thiazolidin-4-one as an Antibacterial, Antifungal and Antimycobacterial Agent.</a> Brahmbhatt, H., A.K. Bhatt, A.K. Das, P. Paul, and S. Sharma. Journal of Heterocyclic Chemistry, 2017. 54(5): p. 2838-2843. ISI[000411039200032].
    <br />
    <b>[WOS]</b>. TB_10_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28815923">Biophysical Screening of a Focused Library for the Discovery of CYP121 Inhibitors as Novel Antimycobacterials.</a> Brengel, C., A. Thomann, A. Schifrin, G. Allegretta, A.A.M. Kamal, J. Haupenthal, I. Schnorr, S.H. Cho, S.G. Franzblau, M. Empting, J. Eberhard, and R.W. Hartmann. ChemMedChem, 2017. 12(19): p. 1616-1626. PMID[28815923].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28882483">Profiling of in Vitro Activities of Urea-based Inhibitors against Cysteine Synthases from Mycobacterium tuberculosis.</a> Brunner, K., E.M. Steiner, R.S. Reshma, D. Sriram, R. Schnell, and G. Schneider. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(19): p. 4582-4587. PMID[28882483].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28968083">Design, Synthesis, and Characterization of N-Oxide-containing Heterocycles with in Vivo Sterilizing Antitubercular Activity.</a> dos Santos Fernandes, G.F., P.C. de Souza, E. Moreno-Viguri, M. Santivanez-Veliz, R. Paucar, S. Perez-Silanes, K. Chegaev, S. Guglielmo, L. Lazzarato, R. Fruttero, C. Man Chin, P.B. da Silva, M. Chorilli, M.C. Solcia, C.M. Ribeiro, C.S.P. Silva, L.B. Marino, P.L. Bosquesi, D.M. Hunt, L.P.S. de Carvalho, C.A. de Souza Costa, S.H. Cho, Y. Wang, S.G. Franzblau, F.R. Pavan, and J.L. dos Santos. Journal of Medicinal Chemistry, 2017. 60(20): p. 8647-8660. PMID[28968083]. PMCID[PMC5677254].<b><br />
    [PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28953997">Antimycobacterial and Antimalarial Activities of Endophytic Fungi Associated with the Ancient and Narrowly Endemic Neotropical Plant Vellozia gigantea from Brazil.</a> Ferreira, M.C., C.L. Cantrell, D.E. Wedge, V.N. Goncalves, M.R. Jacob, S. Khan, C.A. Rosa, and L.H. Rosa. Memorias do Instituto Oswaldo Cruz, 2017. 112(10): p. 692-697. PMID[28953997]. PMCID[PMC5607518].<b><br />
    [PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000411813200024">Synthesis, Inhibition of Mycobacterium tuberculosis Enoyl-acyl Carrier Protein Reductase and Antimycobacterial Activity of Novel Pentacyanoferrate(II)-isonicotinoylhydrazones.</a> Gazzi, T.P., M. Rotta, A.D. Villela, V. Rodrigues, L.K.B. Martinelli, F.A.M. Sales, E.H.S. de Sousa, M.M. Campos, L.A. Basso, D.S. Santos, and P. Machado. Journal of the Brazilian Chemical Society, 2017. 28(10): p. 2028-2037. ISI[000411813200024].
    <br />
    <b>[WOS]</b>. TB_10_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28892605">Inhibitors of the Diadenosine tetraphosphate phosphorylase Rv2613c of Mycobacterium tuberculosis.</a> Gotz, K.H., S.M. Hacker, D. Mayer, J.N. Durig, S. Stenger, and A. Marx. ACS Chemical Biology, 2017. 12(10): p. 2682-2689. PMID[28892605].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28925956">Sulfadiazine salicylaldehyde-based Schiff Bases: Synthesis, Antimicrobial Activity and Cytotoxicity.</a> Kratky, M., M. Dzurkova, J. Janousek, K. Konecna, F. Trejtnar, J. Stolarikova, and J. Vinsova. Molecules, 2017. 22(9): E1573. PMID[28925956].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28953378">Discovery of a Potent and Specific M. tuberculosis Leucyl-tRNA Synthetase Inhibitor: (S)-3-(Aminomethyl)-4-chloro-7-(2-hydroxyethoxy)benzo[c][1,2]oxaborol-1(3H)-ol (GSK656).</a> Li, X., V. Hernandez, F.L. Rock, W. Choi, Y.S.L. Mak, M. Mohan, W. Mao, Y. Zhou, E.E. Easom, J.J. Plattner, W. Zou, E. Perez-Herran, I. Giordano, A. Mendoza-Losana, C. Alemparte, J. Rullas, I. Angulo-Barturen, S. Crouch, F. Ortega, D. Barros, and M.R.K. Alley. Journal of Medicinal Chemistry, 2017. 60(19): p. 8011-8026. PMID[28953378].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28771901">Synthetic Arabinomannan heptasaccharide glycolipids Inhibit Biofilm Growth and Augment Isoniazid Effects in Mycobacterium smegmatis.</a> Maiti, K., K. Syal, D. Chatterji, and N. Jayaraman. ChemBioChem, 2017. 18(19): p. 1959-1970. PMID[28771901].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28836767">Transition State Analogue Inhibitors of 5&#39;-Deoxyadenosine/5&#39;-Methylthioadenosine Nucleosidase from Mycobacterium tuberculosis.</a> Namanja-Magliano, H.A., G.B. Evans, R.K. Harijan, P.C. Tyler, and V.L. Schramm. Biochemistry, 2017. 56(38): p. 5090-5098. PMID[28836767].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28924204">Cyclipostins and Cyclophostin Analogs as Promising Compounds in the Fight against Tuberculosis.</a> Nguyen, P.C., V. Delorme, A. Benarouche, B.P. Martin, R. Paudel, G.R. Gnawali, A. Madani, R. Puppo, V. Landry, L. Kremer, P. Brodin, C.D. Spilling, J.F. Cavalier, and S. Canaan. Scientific Reports, 2017. 7(11751): 15pp. PMID[28924204]. PMCID[PMC5603573].<b><br />
    [PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000410778400018">Synthesis, Conformational Studies and Biological Profiles of Tetrahydrofuran Amino-acid-containing Cationic Antitubercular Peptides.</a> Pal, S., U. Ghosh, G. Singh, F. Alam, S. Singh, S. Chopra, S. Sinha, R.S. Ampapathi, and T.K. Chakraborty. Asian Journal of Organic Chemistry, 2017. 6(9): p. 1240-1249. ISI[000410778400018].
    <br />
    <b>[WOS]</b>. TB_10_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28763645">The Antitrypanosomal and Antitubercular Activity of Some Nitro(triazole/imidazole)-based Aromatic Amines.</a> Papadopoulou, M.V., W.D. Bloomer, H.S. Rosenzweig, and M. Kaiser. European Journal of Medicinal Chemistry, 2017. 138: p. 1106-1113. PMID[28763645].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000409918904002">Discovery of Selective Inhibitors of Imidazole glycerol phosphate dehydratase from Mycobacterium tuberculosis by Docking-based Virtual Screening.</a> Podshivalov, D., V. Timofeev, D. Sidorov-Biryukov, I. Mandzhieva, and I. Kuranova. FEBS Journal, 2017. 284: p. 310-310. ISI[000409918904002].
    <br />
    <b>[WOS]</b>. TB_10_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28994740">Antibacterial and Antitubercular Activities of Cinnamylideneacetophenones.</a> Polaquini, C.R., G.S. Torrezan, V.R. Santos, A.C. Nazare, D.L. Campos, L.A. Almeida, I.C. Silva, H. Ferreira, F.R. Pavan, C. Duque, and L.O. Regasini. Molecules, 2017. 22(10): E1685. PMID[28994740].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000409151700020">New Cyrhetrenyl and Ferrocenyl Sulfonamides: Synthesis, Characterization, X-Ray Crystallography, Theoretical Study and anti-Mycobacterium tuberculosis Activity.</a> Quintana, C., G. Silva, A.H. Klahn, V. Artigas, M. Fuentealba, C. Biot, I. Halloum, L. Kremer, N. Novoa, and R. Arancibia. Polyhedron, 2017. 134: p. 166-172. ISI[000409151700020].
    <br />
    <b>[WOS]</b>. TB_10_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28993827">Synthesis and Biological Evaluation of Novel Teixobactin Analogues.</a> Schumacher, C.E., P.W.R. Harris, X.B. Ding, B. Krause, T.H. Wright, G.M. Cook, D.P. Furkert, and M.A. Brimble. Organic &amp; Biomolecular Chemistry, 2017. 15(41): p. 8755-8760. PMID[28993827].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28782713">In Vitro Activity of alpha-Yiniferin Isolated from the Roots of Carex humilis against Mycobacterium tuberculosis.</a> Seo, H., M. Kim, S. Kim, H.A. Mahmud, M.I. Islam, K.W. Nam, M.L. Cho, H.S. Kwon, and H.Y. Song. Pulmonary Pharmacology &amp; Therapeutics, 2017. 46: p. 41-47. PMID[28782713].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27155134">Antibacterial Activities of Medicinal Plants Used in Mexican Traditional Medicine.</a> Sharma, A., R.D. Flores-Vallejo, A. Cardoso-Taketa, and M.L. Villarreal. Journal of Ethnopharmacology, 2017. 208: p. 264-329. PMID[27155134].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28869400">Azide-alkyne Cycloaddition en Route to 4-Aminoquinoline-ferrocenylchalcone Conjugates: Synthesis and anti-TB Evaluation.</a> Singh, A., A. Viljoen, L. Kremer, and V. Kumar. Future Medicinal Chemistry, 2017. 9(15): p. 1701-1708. PMID[28869400].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28972974">Differential Inhibition of Adenylylated and Deadenylylated Forms of M. tuberculosis Glutamine Synthetase as a Drug Discovery Platform.</a> Theron, A., R.L. Roth, H. Hoppe, C. Parkinson, C.W. van der Westhuyzen, S. Stoychev, I. Wiid, R.D. Pietersen, B. Baker, and C.P. Kenyon. Plos One, 2017. 12(10): e0185068. PMID[28972974]. PMCID[PMC5626031].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28692915">Triazole Derivatives and Their Anti-tubercular Activity.</a> Zhang, S., Z. Xu, C. Gao, Q.C. Ren, L. Chang, Z.S. Lv, and L.S. Feng. European Journal of Medicinal Chemistry, 2017. 138: p. 501-513. PMID[28692915].
    <br />
    <b>[PubMed]</b>. TB_10_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171012&amp;CC=WO&amp;NR=2017175182A1&amp;KC=A1">Preparation of Ring-fused Thiazolino 2-pyridones, and Their Use in the Treatment and Prevention of Tuberculosis.</a>Stallings, C.L., F. Almqvist, K. Flentie, J.A.D. Good, and F. Ponten. Patent. 2017. 2017-IB51999 2017175182: 99pp.
    <br />
    <b>[Patent]</b>. TB_10_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
