

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-09-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="J++ZFoBY/VSTT+h30P7MDGXPEDZpWyVVEMdc7WmQn/iVwfx0rg3RfEfcamyE4XAJWzKoy6fNIxzimkuN9lj2cl8damHD2c28Rs1G0sPZBdg2NwcAg3wPTwTR5KQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4B810348" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">August 20 - September 3, 2010</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20799693">3&#39;-Deoxy Phosphoramidate Dinucleosides as Improved Inhibitors of Hepatitis C Virus Subgenomic Replicon and NS5B Polymerase Activity.</a></p>

    <p class="NoSpacing">Priet S, Zlatev I, Barvik I, Geerts K, Leyssen P, Neyts J, Dutartre H, Canard B, Vasseur JJ, Morvan F, Alvarez K. J Med Chem. 2010 Aug 27. [Epub ahead of print]PMID: 20799693</p> 

    <p class="memofmt2-2">Hepatitis B Virus</p> 
    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17009935">Clevudine: a potent inhibitor of hepatitis B virus in vitro and in vivo.</a></p>

    <p class="NoSpacing">Korba BE, Furman PA, Otto MJ.</p>

    <p class="NoSpacing">Expert Rev Anti Infect Ther. 2006 Aug;4(4):549-61. Review.PMID: 17009935</p> 
    <p />

    <p class="memofmt2-2">Herpes</p> 

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20736512">Phenoxazine Derivatives Suppress the Infections Caused by Herpes Simplex Virus Type-1 and Herpes Simplex Virus Type-2 Intravaginally Inoculated into Mice.</a></p>

    <p class="NoSpacing">Hayashi K, Hayashi T, Miyazawa K, Tomoda A.</p>

    <p class="NoSpacing">J Pharmacol Sci. 2010 Aug 21. [Epub ahead of print]PMID: 20736512</p> 

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20803231">The multi-targeted kinase inhibitor sorafenib inhibits human cytomegalovirus replication.</a></p>

    <p class="NoSpacing">Michaelis M, Paulus C, Löschmann N, Dauth S, Stange E, Doerr HW, Nevels M, Cinatl J Jr.</p>

    <p class="NoSpacing">Cell Mol Life Sci. 2010 Aug 30. [Epub ahead of print]PMID: 20803231</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600002">Diphenylcarboxamides as Inhibitors of HCV Non-Structural Protein NS5a</a>.</p>

    <p class="NoSpacing">Carter, M, Baxter, B, Bushnell, D, Cockerill, S, Chapman, J, Fram, S, Goulding, E, Lockyer, M, Mathews, N, Najarro, P, Rupassara, D, Salter, J, Thomas, E, Wheelhouse, C, Borger, J, Powell, K. ANTIVIRAL RESEARCH. 86(1):2010. P. A19</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600004">In Vitro Selection and Characterization of Hepatitis C Virus Replicons Double or Triple Resistant to Various Non-nucleoside HCV Polymerase Inhibitors.</a></p>

    <p class="NoSpacing">Delang, L, Vliegen, I, Leyssen, P, Neyts, J. ANTIVIRAL RESEARCH. 86(1):2010. P. A19-A20.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600005">Identification and Characterization of a Hepatitis C Virus Capsid Assembly Inhibitor.</a></p>

    <p class="NoSpacing">Gentzsch, J, Hurt, CR, Lingappa, VR, Pietschmann, T. ANTIVIRAL RESEARCH. 86(1):2010. P. A20.</p>

    <p class="NoSpacing"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600007"></a></p>
    <p> </p>

    <p class="NoSpacing">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600007">Bound Structure and Biochemical Mechanism of Action of BI 201335, a Potent, Non-covalent Inhibitor of HCV NS3-NS4A Protease.</a></p>

    <p class="NoSpacing">Lemke, CT, Zhao, SP, Goudreau, N, Hucke, O, Thibeault, D, Llinas-Brunet, M, White, PW. ANTIVIRAL RESEARCH. 86(1):2010. P. A20-A21.</p>
    
    <p class="NoSpacing"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600008"></a></p>
    <p> </p>
    
    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600008">MK-5172, a Novel Macrocyclic Inhibitor of NS3/4a Protease Demonstrates Efficacy Against Viral Resistance in the Chimpanzee Model of Chronic Hepatitis C Virus Infection.</a></p>

    <p class="NoSpacing">Ludmerer, S, Carroll, S, McCauley, J, Rudd, M, Coleman, P, Liverton, N, Burlein, C, DiMuzio, J, Gates, A, Graham, D, McHale, C, Stahlhut, M, Fandozzi, C, Hazuda, D, Vacca, J, Olsen, D. ANTIVIRAL RESEARCH. 86(1):2010. P. A21.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277809600069">Novel Imino Sugars Potently Inhibit HCV Virion Secretion by Targeting Cellular Endoplasmic Reticulum a-Glucosidases.</a></p>

    <p class="NoSpacing">Qu, XW, Pan, XB, Weidner, J, Yu, WQ, Xu, M, Block, T, Guo, JT, Chang, JH. ANTIVIRAL RESEARCH. 86(1):2010. P. A42.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
