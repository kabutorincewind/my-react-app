

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KmU5qrsjqb/8uWLjK6mtK6afQKc6U3ogIItoU/fICDhoVL4fu1zwcw59wkAwSXmaR5FHw4/UbbuS+N2z3F2IB1Lt8Li83L+TVp6cLCBljsaR78lKAaB0S9o/vgg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B5B5A66C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 28, 2011 - November 10, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21981980">1,3-dihydro-2H-indol-2-ones Derivatives: Design, Synthesis, in vitro Antibacterial, Antifungal and Antitubercular Study.</a> Akhaja, T.N. and J.P. Raval, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5573-5579; PMID[21981980].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21905730">Design, Synthesis, and Functionalization of Dimeric Peptides Targeting Chemokine Receptor CXCR4.</a> Demmer, O., I. Dijkgraaf, U. Schumacher, L. Marinelli, S. Cosconati, E. Gourni, H.J. Wester, and H. Kessler, Journal of Medicinal Chemistry, 2011. 54(21): p. 7648-7662; PMID[21905730].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21945283">Combining Symmetry Elements Results in Potent Naphthyridinone (NTD) HIV-1 Integrase Inhibitors.</a> Johns, B.A., T. Kawasuji, J.G. Weatherhead, E.E. Boros, J.B. Thompson, E.P. Garvey, S.A. Foster, J.L. Jeffrey, W.H. Miller, N. Kurose, K. Matsumura, and T. Fujiwara. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6461-6464; PMID[21945283].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22037850">Molecular Dynamics Approaches Estimate the Binding Energy of HIV-1 Integrase Inhibitors and Correlate with in vitro Activity.</a>  Johnson, B.C., M. Metifiot, Y. Pommier, and S.H. Hughes, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22037850].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22060557">Synthesis of Novel Difluoro-cyclopropyl Guanine Nucleosides and their Phosphonate Analogues as Potent Antiviral Agents.</a> Li, H., J.C. Yoo, E. Kim, and J.H. Hong, Nucleosides Nucleotides Nucleic Acids, 2011. 30(11): p. 945-960; PMID[22060557].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22037848">F18, a Novel Small Molecule NNRTI, Inhibits HIV-1 Replication Using Distinct Binding Motifs as Demonstrated by Resistance Selection and Docking Analysis.</a> Lu, X., L. Liu, X. Zhang, T.C. Lau, S.K. Tsui, Y. Kang, P. Zheng, B. Zheng, G. Liu, and Z. Chen, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22037848].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896914">Inhibition of HIV-1 by Octadecyloxyethyl Esters of (S)-[3-Hydroxy-2-(Phosphonomethoxy)Propyl] Nucleosides and Evaluation of Their Mechanism of Action.</a> Magee, W.C., N. Valiaeva, J.R. Beadle, D.D. Richman, K.Y. Hostetler, and D.H. Evans, Antimicrobial Agents and Chemotherapy, 2011. 55(11): p. 5063-5072; PMID[21896914].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21771859">HIV-1 Integrase Inhibitor T30177 Forms a Stacked Dimeric G-quadruplex Structure Containing Bulges.</a> Mukundan, V.T., N.Q. Do, and A.T. Phan, Nucleic Acids Research, 2011. 39(20): p. 8984-8991; PMID[21771859].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21867733">Comparative Antiviral Activity of Integrase Inhibitors in Human Monocyte-derived Macrophages and Lymphocytes.</a> Scopelliti, F., M. Pollicita, F. Ceccherini-Silberstein, F. Di Santo, M. Surdo, S. Aquaro, and C.F. Perno, Antiviral Research, 2011. 92(2): p. 255-261; PMID[21867733].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22064534">Galectin-1 Specific Inhibitors as a New Class of Compounds to Treat HIV-1 Infection.</a> St-Pierre, C., M. Ouellet, D. Giguere, R. Ohtake, R. Roy, S. Sato, and M.J. Tremblay, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22064534].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21989940">An Optimized MM/PBSA Virtual Screening Approach Applied to an HIV-1 gp41 Fusion Peptide Inhibitor.</a> Venken, T., D. Krnavek, J. Munch, F. Kirchhoff, P. Henklein, M. De Maeyer, and A. Voet, Proteins, 2011. 79(11): p. 3221-3235; PMID[21989940].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22064539">In vitro Phenotypic Susceptibility of HIV-2 Clinical Isolates to CCR5 Inhibitors.</a> Visseaux, B., C. Charpentier, M. Hurtado-Nedelec, A. Storto, R. Antoine, G. Peytavin, F. Damond, S. Matheron, F. Brun-Vezinet, and D. Descamps, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22064539].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896906">Development of Dual-Acting Pyrimidinediones as Novel and Highly Potent Topical Anti-HIV Microbicides.</a> Watson Buckheit, K., L. Yang, and R.W. Buckheit, Jr., Antimicrobial Agents and Chemotherapy, 2011. 55(11): p. 5243-5254; PMID[21896906].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22068498">A Novel Ribonuclease with Potent HIV-1 Reverse Transcriptase Inhibitory Activity from Cultured Mushroom Schizophyllum Commune.</a> Zhao, Y.C., G.Q. Zhang, T.B. Ng, and H.X. Wang, Journal of Microbiology, 2011. 49(5): p. 803-808; PMID[22068498].
    <br />
    <b>[PubMed]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295790500117">Extensive Profile of the Anti-HIV Inhibitory Activity of Well-Known Monoclonal Antibodies, Possible Correlates for In Vivo Protection.</a> Biedma, M.S., S Decoville, T Laumond, G Holl, V Moog, C, Aids Research and Human Retroviruses, 2011. 27(10): p. A48-A48; ISI[000295790500117].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295822300010">Variability of a diterpene with potential anti-HIV activity isolated from the Brazilian brown alga Dictyota menstrualis</a>. Cavalcanti, D.d.O., MAR De-Paula, JC Barbosa, LS Fogel, T Pinto, MA Paixao, ICND Teixeira, VL, Journal of Applied Phycology, 2011. 23(5): p. 873-876; ISI[000295822300010]. <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295211000035">Synthesis of Quinolin-2-one Alkaloid Derivatives and Their Inhibitory Activities against HIV-1 Reverse Transcriptase</a>. Cheng, P.G., Q Liu, W Zou, JF Ou, YY Luo, ZY Zeng, JG, Molecules, 2011. 16(9): p. 7649-7661; ISI[000295211000035].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295864600017">Zidovudine (AZT) has a bactericidal effect on enterobacteria and induces genetic modifications in resistant strains European</a>. Doleans-Jordheim, A.B., E Bereyziat, F Ben-Larbi, S Dumitrescu, O Mazoyer, MA Morfin, F Dumontet, C Freney, J Jordheim, LP, Journal of Clinical Microbiology &amp; Infectious Diseases, 2011. 30(10): p. 1249-1256; ISI[000295864600017].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295911100002">Synthesis and biological evaluation of new 3,5-di(trifluoromethyl)-1,2,4-triazolesulfonylurea and thiourea derivatives as antidiabetic and antimicrobial agents</a>. Faidallah, H.K., KA Asiri, AM, Journal of Fluorine Chemistry, 2011. 132(11): p. 870-877; ISI[000295911100002].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295437600116">Isolation and identification of alkaloids of Erythrina abyssinica A Sudanese medicinal plant.</a>. Gagdim, A. and A. Musa, Clinical Biochemistry, 2011. 44(13): p. S40-S40; ISI[000295437600116].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295237400022">A novel and efficient one-pot synthesis of symmetrical diamide (bis-amidate) prodrugs of acyclic nucleoside phosphonates and evaluation of their biological activities.</a> Jansa, P., O. Baszczynski, M. Dracinsky, I. Votruba, Z. Zidek, G. Bahador, G. Stepan, T. Cihlar, R. Mackman, A. Holy, and Z. Janeba, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3748-3754; ISI[000295237400022]. <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295662600010">Total syntheses of (+)- and (-)-1,3,4,5-tetragalloylapiitol and revision of absolute configuration of naturally occurring (-)-1,3,4,5-tetragalloylapiitol.</a> Kojima, M., Y. Nakamura, S. Akai, K.I. Sato, and S. Takeuchi, Tetrahedron, 2011. 67(43): p. 8293-8299; ISI[000295662600010].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295662600011">Synthesis of enantiopure beta-amino alcohols via AKR/ARO of epoxides using recyclable macrocyclic Cr(III) salen complexes.</a>Kureshy, R.P., KJ Kumar, M Bera, PK Khan, NUH Abdi, SHR Bajaj, HC, Tetrahedron, 2011. 67(43): p. 8300-8307; ISI[000295662600011].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295710100008">Synthesis and biological activity of 4-chloro-2-hydroxy-N-(5-methylene-4-oxo-2-aryl-thiazolidin-3-yl) benzamide.</a> Maheta, S. and S.J. Patel, Bulgarian Chemical Communications, 2011. 43(3): p. 411-418; ISI[000295710100008].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295237400046">Synthesis and structural studies of pentacycloundecane-based HIV-1 PR inhibitors: A hybrid 2D NMR and docking/QM/MM/MD approach.</a> Makatini, M.P., K Sriharsha, SN Ndlovu, N Soliman, MES Honarparvar, B Parboosing, R Naidoo, A Arvidsson, PI Sayed, Y Govender, P Maguire, GEM Kruger, HG Govender, T, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3976-3985; ISI[000295237400046].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295437601518">Recombinant production of anti-HIV protein, Griffithsin, in E. coli.</a> Mohammad, A.Y., V Mesbah, B Houshang, A, Clinical Biochemistry, 2011. 44(13): p. S349-S349; ISI[000295437601518].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295237400069">Synthesis and identification of beta-aryloxyquinolines and their pyrano[3,2-c] chromene derivatives as a new class of antimicrobial and antituberculosis agents.</a> Mungra, D.C., M.R. Patel, D.P. Rajani, and R.G. Patel, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4192-4200; ISI[000295237400069].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295940700020">Computational analysis of aspartic protease plasmepsin II complexed with EH58 inhibitor: a QM/MM MD study.</a> Silva, N.D., J. Lameira, and C.N. Alves, Journal of Molecular Modeling, 2011. 17(10): p. 2631-2638; ISI[000295940700020].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295237400018">Synthesis and antiviral evaluation of alpha-L-2 &#39;-deoxythreofuranosyl nucleosides.</a> Toti, K.S., M. Derudas, C. McGuigan, J. Balzarini, and S. Van Calenbergh, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3704-3713; ISI[000295237400018].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295498600003">Synthesis and in vitro Study of Antiviral and Virucidal Activity of Novel 2-[(4-Methyl-4H-1,2,4-triazol-3-yl)sulfanyl]acetamide Derivatives.</a> Wujec, M.P., T Siwek, A Rajtar, B Polz-Dacewicz, M, Zeitschrift fur Naturforschung Section C-A Journal of Biosciences, 2011. 66(7-8): p. 333-339; ISI[000295498600003].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295437601432">Identification of a nonpeptide cyclic urea inhibitor of HIV protease using in silico molecular docking.</a> Yazdani, M. and M. Masomi, Clinical Biochemistry, 2011. 44(13): p. S321-S322; ISI[000295437601432].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295237400031">Synthesis, antiviral activity, cytotoxicity and cellular pharmacology of L-3 &#39;-azido-2 &#39;,3 &#39;-dideoxypurine nucleosides</a>. Zhang, H.D., M Herman, BD Solomon, S Bassit, L Nettles, JH Obikhod, A Tao, SJ Mellors, JW Sluis-Cremer, N Coats, SJ Schinazi, RF, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3832-3844; ISI[000295237400031].
    <br />
    <b>[WOS]</b>. HIV_1028-111011.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
