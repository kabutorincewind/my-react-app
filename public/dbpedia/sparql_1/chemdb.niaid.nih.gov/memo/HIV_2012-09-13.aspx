

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-09-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eJggNjS5ScJ7pLpTFEJvBa6X/eyEKg7jOsf90JdfZLKO5OUQssPyJaBaamRWDhK1c4x478OL58OKlWzFuL2r/LlgyvperBhaXKqjEcdAIwYy8Gu0/nVDYY/bTII=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F092FB2C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 31 - September 13, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22760295">A Multi-functional Peptide as an HIV-1 Entry Inhibitor Based on Self-concentration, Recognition, and Covalent Attachment.</a> Zhao, L., P. Tong, Y.X. Chen, Z.W. Hu, K. Wang, Y.N. Zhang, D.S. Zhao, L.F. Cai, K.L. Liu, Y.F. Zhao, and Y.M. Li. Organic and Biomolecular Chemistry, 2012. 10(32): p. 6512-6520. PMID[22760295].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0817-083012.</p>

    <p> </p>

    <p class="plaintext">2.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22687513">Bifunctional Cd4-Dc-sign Fusion Proteins Demonstrate Enhanced Avidity to gp120 and Inhibit HIV-1 Infection and Dissemination</a>. Du, T., K. Hu, J. Yang, J. Jin, C. Li, D. Stieh, G.E. Griffin, R.J. Shattock, and Q. Hu. Antimicrobial Agents and Chemotherapy, 2012. 56(9): p. 4640-4649. PMID[22687513]. <b>[PubMed].</b> HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">3.<a href="file:///C:/AppData/Local/0817-083012/.%20http:/www.ncbi.nlm.nih.gov/pubmed/22871486">One Pot Efficient Diversity Oriented Synthesis of Polyfunctional Styryl Thiazolopyrimidines and Their Bio-evaluation as Antimalarial and anti-HIV Agent</a>. Fatima, S., A. Sharma, R. Saxena, R. Tripathi, S.K. Shukla, S.K. Pandey, and R.P. Tripathi. European Journal of Medicinal Chemistry, 2012. 55: p. 195-204. PMID[22871486].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">4.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22959895">Feglymycin, a Unique Natural Bacterial Antibiotic Peptide, Inhibits HIV Entry by Targeting the Viral Envelope Protein gp120.</a> Ferir, G., A. Hanchen, K.O. Francois, B. Hoorelbeke, D. Huskens, F. Dettner, R.D. Sussmuth, and D. Schols. Virology, 2012. <b>[Epub ahead of print]</b>;  PMID[22959895].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">5.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22963666">Alkyl Hydroxybenzoic Acid Derivatives that Inhibit HIV-1 Protease Dimerization.</a> Flausino, O.A., L. Dufau, L.O. Regasini, M.S. Petronio, D.H. Silva, T. Rose, V.S. Bolzani, and M. Reboud-Ravaux. Current Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22963666].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">6.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22963631">Antiviral Properties against HIV of Water Soluble Copper Carbosilane Dendrimers and Their EPR Characterization</a>. Galan, M., J. Sanchez-Rodriguez, M. Cangiotti, S. Garcia-Gallego, J.L. Jimenez, R. Gomez, M.F. Ottaviani, M.A. Munoz-Fernandez, and F.J. de la Mata. Current Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22963631].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">7.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22954649">Cytolytic Nanoparticles Attenuate HIV-1 Infectivity</a>. Hood, J.L., A.P. Jallouk, N. Campbell, L. Ratner, and S.A. Wickline. Antiviral Therapy, 2012. <b>[Epub ahead of print]</b>; PMID[22954649].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">8.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22955454">Design, Practical Synthesis, and Biological Evaluation of Novel 6-(pyrazolylmethyl)-4-quinoline-3-carboxylic Acid Derivatives as HIV-1 Integrase Inhibitors.</a> Hu, L., S. Yan, Z. Luo, X. Han, Y. Wang, Z. Wang, and C. Zeng. Molecules, 2012. 17(9): p. 10652-10666. PMID[22955454].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">9.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22963135">Carbamoyl Pyridone HIV-1 Integrase Inhibitors. Molecular Design and Establishment of an Advanced Two-metal Binding Pharmacophore</a>. Kawasuji, T. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22963135].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">10.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22954307">Gold Nanoparticles as a HIV Entry Inhibitor</a>. Kumar, S.V. and S. Ganesan. Current HIV Research, 2012. <b>[Epub ahead of print]</b>; PMID[22954307].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">11.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22960154">The Antiviral Activity and Mechanism of Action of (S)-[3-Hydroxy-2-(phosphonomethoxy)propyl] (HPMP) Nucleosides</a>. Magee, W.C. and D.H. Evans. Antiviral Research, 2012. <b>[Epub ahead of print]</b>;  PMID[22960154].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">12.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22829059">Concise Synthesis and anti-HIV Activity of Pyrimido[1,2-C][1,3]benzothiazin-6-imines and Related Tricyclic Heterocycles</a>. Mizuhara, T., S. Oishi, H. Ohno, K. Shimura, M. Matsuoka, and N. Fujii. Organic and Biomolecular Chemistry, 2012. 10(33): p. 6792-6802. PMID[22829059].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">13.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22964257">Novel Method to Assess Antiretroviral Target Trough Concentrations Using in Vitro Susceptibility Data</a>. Acosta, E.P., K.L. Limoli, L. Trinh, N.T. Parkin, J.R. King, J.M. Weidler, I. Ofotokun, and C.J. Petropoulos. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22964257]. [<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">14.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22858097">Synthesis and Anti-HIV Activities of Symmetrical Dicarboxylate Esters of Dinucleoside Reverse Transcriptase Inhibitors.</a> Agarwal, H.K., K.W. Buckheit, R.W. Buckheit, Jr., and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5451-5454. PMID[22858097].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">15.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22858300">Synthesis and Evaluation of Hybrid Drugs for a Potential HIV/AIDS-Malaria Combination Therapy.</a> Aminake, M.N., A. Mahajan, V. Kumar, R. Hans, L. Wiesner, D. Taylor, C. de Kock, A. Grobler, P.J. Smith, M. Kirschner, A. Rethwilm, G. Pradel, and K. Chibale. Bioorganic &amp; Medicinal Chemistry, 2012. 20(17): p. 5277-5289. PMID[22858300].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">16.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22961335">Covalent Fusion Inhibitors Targeting HIV-1 gp41 Deep Pocket.</a> Bai, Y., H. Xue, K. Wang, L. Cai, J. Qiu, S. Bi, L. Lai, M. Cheng, S. Liu, and K. Liu. Amino Acids, 2012. <b>[Epub ahead of print]</b>; PMID[22961335].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">17.<a href="http://www.ncbi.nlm.nih.gov/pubmed/21936641">Anti-HIV-1 and Cytotoxicity of the Alkaloids of Erythrina abyssinica Lam. Growing in Sudan</a>. Mohammed, M.M., N.A. Ibrahim, N.E. Awad, A.A. Matloub, A.G. Mohamed-Ali, E.E. Barakat, A.E. Mohamed, and P.L. Colla. Natural Product Research, 2012. 26(17): p. 1565-1575. PMID[21936641].</p>
    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22858140">In Vitro Evolution of an HIV Integrase Binding Protein from a Library of C-terminal Domain Gammas-crystallin Variants</a>. Moody, I.S., S.C. Verde, C.M. Overstreet, W. Edward Robinson, Jr., and G.A. Weiss. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5584-5589. PMID[22858140]. [<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22958368">Synthesis and anti-HIV Properties of New Carbamate Prodrugs of AZT</a>. Solyev, P.N., A.V. Shipitsin, I.L. Karpenko, D.N. Nosik, L.B. Kalnina, S.N. Kochetkov, M.K. Kukhanova, and M.V. Jasko. Chemical Biology and Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22958368].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">20.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22955927">Neutralizing Antibodies Inhibit HIV-1 Transfer from Primary Dendritic Cells to Autologous CD4 T-lymphocytes</a>. Su, B., K. Xu, A. Lederle, M. Peressin, M.E. Biedma, G. Laumond, S. Schmidt, T. Decoville, A. Proust, M. Lambotin, V. Holl, and C. Moog. Blood, 2012. <b>[Epub ahead of print]</b>; PMID[22955927].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22949064">Discovery of Novel Low-molecular-weight HIV-1 Inhibitors Interacting with Cyclophilin a Using in Silico Screening and Biological Evaluations.</a> Tian, Y.S., C. Verathamjamras, N. Kawashita, K. Okamoto, T. Yasunaga, K. Ikuta, M. Kameoka, and T. Takagi. Journal of Molecular Modeling, 2012. <b>[Epub ahead of print]</b>; PMID[22949064].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">22.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22741604">Anti HIV-1 Agents 6. Synthesis and anti-HIV-1 Activity of Indolyl Glyoxamides</a>. Wang, Y., N. Huang, X. Yu, L.M. Yang, X.Y. Zhi, Y.T. Zheng, and H. Xu. Medicinal Chemistry, 2012. 8(5): p. 831-833. PMID[22741604].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">23.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22609684">Benzophenone Glycosides and Epicatechin Derivatives from Malania oleifera</a>. Wu, X.D., J.T. Cheng, J. He, X.J. Zhang, L.B. Dong, X. Gong, L.D. Song, Y.T. Zheng, L.Y. Peng, and Q.S. Zhao. Fitoterapia, 2012. 83(6): p. 1068-1071. PMID[22609684].</p>

    <p class="plaintext">[<b>PubMed</b>]. HIV_0831-091312.</p>

    <h2>ISI Web of Knowledge Citations</h2>
    
    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307198300043">Antiretroviral Agents Effectively Block HIV Replication after Cell-to-Cell Transfer</a>. Permanyer, M., E. Ballana, A. Ruiz, R. Badia, E. Riveira-Munoz, E. Gonzalo, B. Clotet, and J.A. Este. Journal of Virology, 2012. 86(16): p. 8773-8780. ISI[000307198300043].</p>
    <p class="plaintext">[<b>WOS</b>]. HIV_0831-091312.</p>
    <p> </p>
       
    <p class="plaintext">25.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307386900003">A Bitter Plant with a Sweet Future? A Comprehensive Review of an Oriental Medicinal Plant: Andrographis Paniculata</a>. Subramanian, R., M.Z. Asmawi, and A. Sadikun. Phytochemistry Reviews, 2012. 11(1): p. 39-75. ISI[000307386900003].</p>
    <p class="plaintext">[<b>WOS</b>]. HIV_0831-091312.</p>
    <p> </p>
    
    <p class="plaintext">26.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307198300013">Inhibiting Early-stage Events in HIV-1 Replication by Small-Molecule Targeting of the HIV-1 Capsid</a>. Kortagere, S., N. Madani, M.K. Mankowski, A. Schon, I. Zentner, G. Swaminathan, A. Princiotto, K. Anthony, A. Oza, L.J. Sierra, S.R. Passic, X.Z. Wang, D.M. Jones, E. Stavale, F.C. Krebs, J. Martin-Garcia, E. Freire, R.G. Ptak, J. Sodroski, S. Cocklin, and A.B. Smith. Journal of Virology, 2012. 86(16): p. 8472-8481. ISI[000307198300013].</p>
    <p class="plaintext">[<b>WOS</b>]. HIV_0831-091312.</p>
    <p> </p>
    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
