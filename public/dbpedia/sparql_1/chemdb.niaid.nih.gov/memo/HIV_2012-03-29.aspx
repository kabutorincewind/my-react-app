

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-03-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5A7YQKsrCgG4gBWxfGEAKEGaVmEhYHDex6rBQsGi/aV0/0k0eLBeRUqrvTdfSupE3YAy+zWuit64kmWwsu78dR5dATSjlikD39f8PgTN5ICUuWiRIkjv4WwgPZk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0AA02866" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 16 - March 29, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22424294">Leukotrienes Inhibit Early Stages of HIV-1 Infection in Monocyte-derived Microglia-like cells.</a> Bertin, J., C. Barat, D. Belanger, and M.J. Tremblay, Journal of Neuroinflammation, 2012. 9(1): p. 55; PMID[22424294].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22428563">Phenolic Compounds from Nicotiana tabacum and Their Biological Activities.</a> Chen, Y.K., X.S. Li, G.Y. Yang, Z.Y. Chen, Q.F. Hu, and M.M. Miao, Journal of Asian Natural Products Research, 2012. <b>[Epub ahead of print]</b>; PMID[22428563].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22431590">Soluble Factors from T Cells Inhibiting X4 Strains of HIV are a Mixture of Beta Chemokines and RNases.</a> Cocchi, F., A.L. Devico, W. Lu, M. Popovic, O. Latinovic, M.M. Sajadi, R.R. Redfield, M.K. Lafferty, M. Galli, A. Garzino-Demo, and R.C. Gallo, Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22431590].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430977">Characterization in an ex Vivo Ectocervical Model of UC781/Tenofovir Combination Gel Products for HIV-1 Prevention.</a> Cost, M., C.S. Dezzutti, M.R. Clark, D.R. Friend, A. Akil, and L.C. Rohan, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22430977].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22424437">Inhibition of HIV-1 Replication with Stable RNAi-mediated Knockdown of Autophagy Factors.</a> Eekels, J.J., S. Sagnier, D. Geerts, R.E. Jeeninga, M. Biard-Piechaczyk, and B. Berkhout, Virology Journal, 2012. 9(1): p. 69; PMID[22424437].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22447925">CCR5 Antagonist TD-0680 Uses a Novel Mechanism for Enhanced Potency against HIV-1 Entry, Cell-mediated Infection and a Resistant Variant.</a> Kang, Y., Z. Wu, T.C. Lau, X. Lu, L. Liu, A.K. Cheung, Z. Tan, J. Ng, J. Liang, H. Wang, S.K. Li, B. Zheng, B. Li, L. Chen, and Z. Chen, The Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22447925].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22437836">A Multimode, Cooperative Mechanism of Action of Allosteric HIV-1 Integrase Inhibitors.</a> Kessl, J.J., N. Jena, Y. Koh, H. Taskent-Sezgin, A. Slaughter, L. Feng, S. de Silva, L. Wu, S.F. Le Grice, A. Engelman, J.R. Fuchs, and M. Kvaratskhelia, The Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22437836].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22181350">First-In-Class Small Molecule Inhibitors of the Single-strand DNA Cytosine Deaminase APOBEC3G.</a> Li, M., S.M. Shandilya, M.A. Carpenter, A. Rathore, W.L. Brown, A.L. Perkins, D.A. Harki, J. Solberg, D.J. Hook, K.K. Pandey, M.A. Parniak, J.R. Johnson, N.J. Krogan, M. Somasundaran, A. Ali, C.A. Schiffer, and R.S. Harris, ACS Chemical Biology, 2012. 7(3): p. 506-517; PMID[22181350].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22429134">Capsid Assembly as a Point of Intervention for Novel Anti-viral Therapeutics.</a> Lingappa, V.R., C.R. Hurt, and E. Garvey, Current Pharmaceutical Biotechnology, 2012. <b>[Epub ahead of print]</b>; PMID[22429134].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430971">Sublimable C5A Delivery Provides Sustained and Prolonged anti-HIV Microbicidal Activities.</a> Maskiewicz, R., M. Bobardt, U. Chatterji, S. Gunaseelan, C.S. Dezzutti, F. Penin, and P.A. Gallay, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22430971].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22429098">Recent Advances in the Research of 2,3-Diaryl-1,3-thiazolidin-4-one Derivatives as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Tian, Y., P. Zhan, D. Rai, J. Zhang, E. De Clercq, and X. Liu, Current Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22429098].
    <br />
    <b>[PubMed]</b>. HIV_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148316">Library-based Discovery and Characterization of Daphnane Diterpenes as Potent and Selective HIV Inhibitors in Daphne gnidium.</a> Vidal, V., O. Potterat, S. Louvel, F. Hamy, M. Mojarrab, J.J. Sanglier, T. Klimkait, and M. Hamburger, Journal of Natural Products, 2012. 75(3): p. 414-419; PMID[22148316]. <b>[PubMed]</b>. HIV_0316-032912.</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300409000008">Combination of Antiretroviral Drugs as Microbicides</a>. Balzarini, J. and D. Schols, Current HIV Research, 2012. 10(1): p. 53-60; ISI[000300409000008].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300074600004">Denaturation of HIV-1 Protease (PR) Monomer by Acetic Acid: Mechanistic and Trajectory Insights from Molecular Dynamics Simulations and NMR</a>. Borkar, A., M. Rout, and R. Hosur, Journal of Biomolecular Structure &amp; Dynamics, 2012. 29(5): p. 893-903; ISI[000300074600004].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300029000011">Thymidine- and AZT-linked 5-(1,3-Dioxoalkyl)tetrazoles and 4-(1,3-Dioxoalkyl)-1,2,3-triazoles.</a> Bosch, L., O. Delelis, F. Subra, E. Deprez, M. Witvrow, and J. Vilarrasa, Tetrahedron Letters, 2012. 53(5): p. 514-518; ISI[000300029000011].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300364700014">Synthesis of 1,5-Benzothiazepine Derivatives Bearing 2-Phenoxy-quinoline Moiety via 1,3-Diplolar Cycloaddition Reaction.</a> Dong, Z.Q., F.M. Liu, F. Xu, and Z.L. Yuan, Molecular Diversity, 2011. 15(4): p. 963-970; ISI[000300364700014].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299909300014">Phenols with anti-HIV Activity from Daphne acutiloba.</a> Huang, S.Z., X. Zhang, X. Li, H. Jiang, Q. Ma, P. Wang, Y. Liu, J. Hu, Y. Zheng, J. Zhou, and Y. Zhao, Planta Medica, 2012. 78(2): p. 182-185; ISI[000299909300014].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300339700024">Evaluation of Competing Process Concepts for the Production of Pure Enantiomers.</a> Kaspereit, M., S. Swernath, and A. Kienle, Organic Process Research &amp; Development, 2012. 16(2): p. 353-363; ISI[000300339700024].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299859600009">A QSAR Study on a Series of N-Methyl pyrimidones Acting as HIV Integrase Inhibitors.</a> Kaushik, S., S. Gupta, P. Sharma, and Z. Anwer, Indian Journal of Biochemistry &amp; Biophysics, 2011. 48(6): p. 427-434; ISI[000299859600009].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300407400005">Recent Advances in the Development of Small-molecule Compounds Targeting HIV-1 gp41 as Membrane Fusion Inhibitors.</a> Kawashita, N., Y.S. Tian, U.C. de Silva, K. Okamoto, and T. Takagi, Mini-Reviews in Organic Chemistry, 2012. 9(1): p. 20-26; ISI[000300407400005].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300201300037">Three New Arylnaphthalene Lignans from Schisandra propinqua var. Sinensis.</a> Li, X.N., C. Lei, L.M. Yang, H.M. Li, S.X. Huang, X. Du, J.X. Pu, W.L. Xiao, Y.T. Zheng, and H.D. Sun, Fitoterapia, 2012. 83(1): p. 249-252; ISI[000300201300037].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300322700008">Involvement of K(v)1.3 and p38 MAPK Signaling in HIV-1 Glycoprotein 120-induced Microglia Neurotoxicity.</a> Liu, J., C. Xu, L. Chen, P. Xu, and H. Xiong, Cell Death &amp; Disease, 2012. 3; ISI[000300322700008].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000299949500016">Quinoxalinone Inhibitors of the Lectin DC-SIGN.</a> Mangold, S.L., L. Prost, and L. Kiessling, Chemical Science, 2012. 3(3): p. 772-777; ISI[000299949500016].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300228300025">HIV-1 Subtype and Virological Response to Antiretroviral Therapy: Acquired Drug Resistance.</a> Soares, E.A., A.F. Santos, and M.A. Soares, Clinical Infectious Diseases, 2012. 54(5): p. 738-738; ISI[000300228300025].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000300284100001">Dihydro-alkoxyl-benzyl-oxopyrimidine Derivatives (DABOs) As Non-Nucleoside Reverse Transcriptase Inhibitors: An Update Review (2001-2011).</a> Yang, S.Q., F.E. Chen, and E. De Clercq, Current Medicinal Chemistry, 2012. 19(2): p. 152-162; ISI[000300284100001].
    <br />
    <b>[WOS]</b>. HIV_0302-031512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
