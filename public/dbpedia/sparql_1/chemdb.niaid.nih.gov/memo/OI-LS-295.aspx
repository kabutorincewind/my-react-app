

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-295.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="I2UCMKXnanTSP9zs4FfdeW86N4sSZE8P6pg900m49sEEp9O1P08q9CVoCrqlqUmMtarwGuBnAadkOZPvUctGTOMKjPHOCZI0U+GhM8lyi09PeFJZh9VkbzIVak8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8389AC79" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-295-MEMO</b> </p>

<p>    1.    43343   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Arylamine N-Acetyltransferase Is Required for Synthesis of Mycolic Acids and Complex Lipids in Mycobacterium bovis BCG and Represents a Novel Drug Target</p>

<p>           Bhakta, S, Besra, GS, Upton, AM, Parish, T, Sholto-Douglas-Vernon, C, Gibson, KJ, Knutton, S, Gordon, S, DaSilva, RP, Anderton, MC, and Sim, E</p>

<p>           J Exp Med 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117974&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117974&amp;dopt=abstract</a> </p><br />

<p>    2.    43344   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Antimicrobial activity of clofazimine is not dependent on mycobacterial C-type phospholipases</p>

<p>           Bopape, MC, Steel, HC, Cockeran, R, Matlola, NM, Fourie, PB, and Anderson, R</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117926&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117926&amp;dopt=abstract</a> </p><br />

<p>    3.    43345   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Cellular RNA Helicase p68 Relocalization and Interaction with the Hepatitis C Virus (HCV) NS5B Protein and the Potential Role of p68 in HCV RNA Replication</p>

<p>           Goh, PY, Tan, YJ, Lim, SP, Tan, YH, Lim, SG, Fuller-Pace, F, and Hong, W</p>

<p>           J Virol 2004. 78(10): 5288-5298</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113910&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113910&amp;dopt=abstract</a> </p><br />

<p>    4.    43346   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Effect of antiretroviral protease inhibitors alone, and in combination with paromomycin, on the excystation, invasion and in vitro development of Cryptosporidium parvum</p>

<p>           Dugue, Geoffrey A</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(2): 403</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    5.    43347   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Design, synthesis, and computational affinity prediction of ester soft drugs as inhibitors of dihydrofolate reductase from Pneumocystis carinii</p>

<p>           Graffner-Nordberg, M, Kolmodin, K, Aqvist, J, Queener, SF, and Hallberg, A</p>

<p>           Eur J Pharm Sci 2004. 22(1): 43-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113582&amp;dopt=abstract</a> </p><br />

<p>    6.    43348   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Natural antimycobacterial metabolites: current status</p>

<p>           Okunade, AL, Elvin-Lewis, MP, and Lewis, WH</p>

<p>           Phytochemistry 2004. 65(8): 1017-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110681&amp;dopt=abstract</a> </p><br />

<p>    7.    43349   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Activities of benzimidazole d- and L-ribonucleosides in animal models of cytomegalovirus infections</p>

<p>           Kern, ER, Hartline, CB, Rybak, RJ, Drach, JC, Townsend, LB, Biron, KK, and Bidanset, DJ</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1749-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105130&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105130&amp;dopt=abstract</a> </p><br />

<p>    8.    43350   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Mechanism of Action of the Ribopyranoside Benzimidazole GW275175X against Human Cytomegalovirus</p>

<p>           Underwood, MR, Ferris, RG, Selleseth, DW, Davis, MG, Drach, JC, Townsend, LB, Biron, KK, and Boyd, FL</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1647-1651</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105116&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105116&amp;dopt=abstract</a> </p><br />

<p>    9.    43351   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           In Vitro Antifungal Activities of Inhibitors of Phospholipases from the Fungal Pathogen Cryptococcus neoformans</p>

<p>           Ganendren, R, Widmer, F, Singhal, V, Wilson, C, Sorrell, T, and Wright, L</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1561-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105106&amp;dopt=abstract</a> </p><br />

<p>  10.    43352   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis Growth Inhibition by Constituents of Sapiumhaematospermum</p>

<p>           Woldemichael, GM, Gutierrez-Lugo, MT, Franzblau, SG, Wang, Y, Suarez, E, and Timmermann, BN</p>

<p>           J Nat Prod 2004. 67(4): 598-603</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104489&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104489&amp;dopt=abstract</a> </p><br />

<p>  11.    43353   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Comparative genomics of Pneumocystis carinii with other protists: implications for life style</p>

<p>           Cushion, Melanie T</p>

<p>           Journal of Eukaryotic Microbiology 2004. 51(1): 30-37</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    43354   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis RmlC epimerase (Rv3465): a promising drug-target structure in the rhamnose pathway</p>

<p>           Kantardjieff, KA, Kim, CY, Naranjo, C, Waldo, GS, Lekin, T, Segelke, BW, Zemla, A, Park, MS, Terwilliger, TC, and Rupp, B</p>

<p>           Acta Crystallogr D Biol Crystallogr 2004. 60(Pt 5): 895-902</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15103135&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15103135&amp;dopt=abstract</a> </p><br />

<p>  13.    43355   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Flucytosine resistance is restricted to a single genetic clade of Candida albicans</p>

<p>           Pujol, Claude, Pfaller, Michael A, and Soll, David R</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(1): 262-266</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  14.    43356   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Synthesis, structure and anti-fungal activity of dimeric Ag(I) complexes containing bis-imidazole ligands</p>

<p>           Abuskhuna, Suaad, Briody, John, McCann, Malachy, Devereux, Michael, Kavanagh, Kevin, Fontecha, Julia Barreira, and McKee, Vickie</p>

<p>           Polyhedron 2004. 23(7): 1249-1255</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    43357   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Isoxazole derivative inhibitors of fungal invasion, and preparation thereof</b>((Microbia, Inc. USA)</p>

<p>           Talley, John Jeffrey, Fretzen, Angelika, Zimmerman, Craig, Barden, Timothy, Yang, Jing Jing, Martinez, Eduardo, Busby, Robert, Cordero, Etchell A, Houman, Fariba, Pierce, Christine M, and Summers, Eric F</p>

<p>           PATENT: WO 2004021997 A2;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 114 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43358   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           In vitro synergic antifungal effect of MUC7 12-mer with histatin-5 12-mer or miconazole</p>

<p>           Wei, GX and Bobek, LA</p>

<p>           J Antimicrob Chemother 2004. 53(5): 750-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15073161&amp;dopt=abstract</a> </p><br />

<p>  17.    43359   OI-LS-295; PUBMED-OI-5/3/2004</p>

<p class="memofmt1-2">           Global spotlight falls on tuberculosis</p>

<p>           Das, P</p>

<p>           Lancet Infect Dis 2004. 4(4): 188</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15072057&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15072057&amp;dopt=abstract</a> </p><br />

<p>  18.    43360   OI-LS-295; WOS-OI-4/25/2004</p>

<p class="memofmt1-2">           The structure of the RNA-dependent RNA polymerase from bovine viral diarrhea virus establishes the role of GTP in de novo initiation</p>

<p>           Choi, KH, Groarke, JM, Young, DC, Kuhn, RJ, Smith, JL, Pevear, DC, and Rossmann, MG</p>

<p>           P NATL ACAD SCI USA 2004. 101(13): 4425-4430</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220648700020">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220648700020</a> </p><br />

<p>  19.    43361   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Antifungal antibiotic polycyclic compound from marine sponge</b>((Yamanouchi Pharmaceutical Co., Ltd. Japan)</p>

<p>           Shibazaki, Mitsuharu, Watanabe, Masato, Fusetani, Nobuhiro, Matsunaga, Shigeki, and Nishimura, Shinichi</p>

<p>           PATENT: JP 2004059542 A2;  ISSUE DATE: 20040226</p>

<p>           APPLICATION: 2002-26811; PP: 8 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    43362   OI-LS-295; WOS-OI-4/25/2004</p>

<p class="memofmt1-2">           Chemogenomics: An emerging strategy for rapid target and drug discovery</p>

<p>           Bredel, M and Jacoby, E</p>

<p>           NAT REV GENET  2004. 5(4): 262-275</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220559200012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220559200012</a> </p><br />

<p>  21.    43363   OI-LS-295; WOS-OI-4/25/2004</p>

<p class="memofmt1-2">           Usefulness of graphical invariants in quantitative structure-activity correlations of tuberculostatic drugs of the isonicotinic acid hydrazide type</p>

<p>           Bagchi, MC, Maiti, BC, Mills, D, and Basak, SC</p>

<p>           J MOL MODEL 2004. 10(2): 102-111</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220564500003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220564500003</a> </p><br />

<p>  22.    43364   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           2,4-diaminoquinazoline and pyridopyrimidine ester derivatives as dihydrofolate reductase inhibitors</b>((Melacure Therapeutics Ab, Swed. and Pett, Christopher Phineas)</p>

<p>           Hallberg, Anders, Graffner-nordberg, Malin, Boman, Arne, and Seifert, Elisabeth</p>

<p>           PATENT: WO 2004020418 A1;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 41 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43365   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Mycobacterium tuberculosis cell envelope lipids and the host immune response</p>

<p>           Karakousis, Petros C, Bishai, William R, and Dorman, Susan E</p>

<p>           Cellular Microbiology 2004. 6(2): 105-116</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    43366   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Antibacterial inhibitors of Ftsz protein</b> ((Southern Research Institute, USA)</p>

<p>           White, Lucile E, Reynolds, Robert C, and Suling, William</p>

<p>           PATENT: WO 2004005472 A2;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 117 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    43367   OI-LS-295; WOS-OI-4/25/2004</p>

<p class="memofmt1-2">           Synthesis of 3,5-disubstituted isoxazolines as potential antibacterial and antifungal agents</p>

<p>           Imran, M and Khan, SA</p>

<p>           INDIAN J HETEROCY CH 2004. 13(3): 213-216</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220614800007">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220614800007</a> </p><br />

<p>  26.    43368   OI-LS-295; WOS-OI-4/25/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of some cyanopyridines and isoxazoles</p>

<p>           Hirpara, K, Patel, S, Joshi, A, and Parekh, H</p>

<p>           INDIAN J HETEROCY CH 2004. 13(3): 221-224</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220614800009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220614800009</a> </p><br />

<p>  27.    43369   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Preparation of phenyl oxazolidinone derivatives as antimycobacterial agents</b>((Lupin Limited, India)</p>

<p>           Arora, Sudershan Kumar, Patil, Vijaykumar Jagdishwar, Nair, Prathap Sreedharan, Dixit, Prasad Purushottam, Ajay, Shankar, and Sinha, Rakesh Kumar</p>

<p>           PATENT: WO 2004026848 A1;  ISSUE DATE: 20040401</p>

<p>           APPLICATION: 2002; PP: 84 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    43370   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Early bactericidal activity of moxifloxacin in treatment of pulmonary tuberculosis: a prospective, randomized study</p>

<p>           Pletz, Mathias WR, De Roux, Andres, Roth, Andreas, Neumann, Karl-Heinz, Mauch, Harald, and Lode, Hartmut</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(3): 780-782</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    43371   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Immunization with a DNA vaccine cocktail protects mice lacking CD4 cells against an aerogenic infection with Mycobacterium tuberculosis</p>

<p>           Derrick, Steven C, Repique, Charlene, Snoy, Philip, Yang, Amy Li, and Morris, Sheldon</p>

<p>           Infection and Immunity 2004. 72(3): 1685-1692</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    43372   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Preparation of heteroarylethanolamines as antiviral agents</b>((Pharmacia &amp; Upjohn Company, USA and Fleck, Bruce Francis)</p>

<p>           Fleck, Thomas J, Schnute, Mark E, Cudahy, Michele M, Anderson, David J, Judge, Thomas M, Herrington, Paul M, Nair, Sajiv K, Scott, Allen, Perrault, William R, Tanis, Steven P, Nieman, James A, and Collier, Sarah A</p>

<p>           PATENT: WO 2004022567 A1;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 90 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    43373   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Preparation of 4-oxo-4,7-dihydrothieno[2,3-b]pyridine-5-carboxamides as antiviral agents</b>((Pharmacia &amp; Upjohn Company, USA)</p>

<p>           Schnute, Mark E, Ciske, Fred L, Genin, Michael J, Strohbach, Joseph W, and Thaisrivongs, Suvit</p>

<p>           PATENT: WO 2004022566 A1;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 74 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    43374   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p><b>           Use of triptans as antiviral agents</b> ((Italy))</p>

<p>           Cassar Scalia, Antonio</p>

<p>           PATENT: WO 2004019934 A1;  ISSUE DATE: 20040311</p>

<p>           APPLICATION: 2003; PP: 10 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    43375   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           1-Deaza-5&#39;-noraisteromycin</p>

<p>           Yin, Xueqiang and Schneller, Stewart W</p>

<p>           Nucleosides, Nucleotides &amp; Nucleic Acids 2004. 23(1 &amp; 2): 67-76</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    43376   OI-LS-295; WOS-OI-5/2/2004</p>

<p class="memofmt1-2">           The human immune response to Mycobacterium tuberculosis in lung and lymph node</p>

<p>           Marino, S and Kirschner, DE</p>

<p>           J THEOR BIOL 2004. 227(4): 463-486</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220674100002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220674100002</a> </p><br />

<p>  35.    43377   OI-LS-295; WOS-OI-5/2/2004</p>

<p><b>           Design and syntheses of 1,6-naphthalene derivatives as selective HCMV protease inhibitors</b> </p>

<p>           Gopalsamy, A, Lim, K, Ellingboe, JW, Mitsner, B, Nikitenko, A, Upeslacis, J, Mansour, TS, Olson, MW, Bebernitz, GA, Grinberg, D, Feld, B, Moy, FJ, and O&#39;Connell, J</p>

<p>           J MED CHEM 2004. 47(8): 1893-1899</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100006</a> </p><br />

<p>  36.    43378   OI-LS-295; WOS-OI-5/2/2004</p>

<p class="memofmt1-2">           Distamycin analogues with enhanced lipophilicity: Synthesis and antimicrobial activity</p>

<p>           Khalaf, AI, Waigh, RD, Drummond, AJ, Pringle, B, McGroarty, I, Skellern, GG, and Suckling, CJ </p>

<p>           J MED CHEM 2004. 47(8): 2133-2156</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100026">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220642100026</a> </p><br />

<p>  37.    43379   OI-LS-295; WOS-OI-5/2/2004</p>

<p class="memofmt1-2">           Antimicrobial dihydrofolate reductase inhibitors achievements and future options: Review</p>

<p>           Then, RL</p>

<p>           J CHEMOTHERAPY 2004. 16(1): 3-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220644000001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220644000001</a> </p><br />

<p>  38.    43380   OI-LS-295; SCIFINDER-OI-4/27/2004</p>

<p class="memofmt1-2">           Antiviral action of ribavirin in chronic hepatitis C</p>

<p>           Pawlotsky, Jean-Michel, Dahari, Harel, Neumann, Avidan U, Hezode, Christophe, Germanidis, Georgios, Lonjon, Isabelle, Castera, Laurent, and Dhumeaux, Daniel</p>

<p>           Gastroenterology 2004. 126(3): 703-714</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
