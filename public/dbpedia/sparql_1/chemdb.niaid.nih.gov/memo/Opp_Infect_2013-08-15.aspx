

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-08-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OniOZAUKxZBUU/8WbU/IDjNm80Qb2BrnS8yiyocLl23pHV6VqcnU+nFKwq2GBNsLcta+Q895SrLBRH9LxZK1hH76or/ROujP/WRNRLWHyDiamW6P+HhcTQjTNcE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="962EDB33" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 2 - August 15, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23937170">Kannurin, a Novel Lipopeptide from Bacillus cereus Strain AK1: Isolation, Structural Evaluation and Antifungal Activities.</a> Ajesh, K., S. Sudarslal, C. Arunan, and K. Sreejith. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23937170].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23905016">Evaluation of Anti-bacterial and Anti-oxidant Potential of Andrographolide and Echiodinin Isolated from Callus Culture of Andrographis paniculata Nees.</a> Arifullah, M., N.D. Namsa, M. Mandal, K.K. Chiruvella, P. Vikrama, G.R. Gopal, and Z. Khanam. Asian Pacific Journal of Tropical Biomedicine, 2013. 3(8): p. 604-610. PMID[23905016].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23807823">Antimicrobial and Antiurease Activities of Newly Synthesized Morpholine Derivatives Containing an Azole Nucleus.</a> Bektas, H., S. Ceylan, N. Demirbas, S. Alpay-Karaoglu, and B.B. Sokmen. Medicinal Chemistry Research, 2013. 22(8): p. 3629-3639. PMID[23807823].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23911425">New Compounds, Chemical Composition, Antifungal Activity and Cytotoxicity of the Essential Oil from Myrtus nivellei Batt. &amp; Trab., an Endemic Species of Central Sahara.</a> Bouzabata, A., O. Bazzali, C. Cabral, M.J. Goncalves, M. Teresa Cruz, A. Bighelli, C. Cavaleiro, J. Casanova, L. Salgueiro, and F. Tomi. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23911425].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23643888">Isolation of (-)-Olivil-9&#39;-o-beta-D-glucopyranoside from Sambucus williamsii and Its Antifungal Effects with Membrane-disruptive Action.</a> Choi, H., J. Lee, Y.S. Chang, E.R. Woo, and D.G. Lee. Biochimica et Biophysica Acta, 2013. 1828(8): p. 2002-2006. PMID[23643888].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23831810">Synthesis, in Vitro Antifungal Activity and in Silico Study of 3-(1,2,4-Triazol-1-yl)flavanones.</a> Emami, S., S. Shojapour, M.A. Faramarzi, N. Samadi, and H. Irannejad. European Journal of Medicinal Chemistry, 2013. 66: p. 480-488. PMID[23831810].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23811090">Synthesis, Biological Evaluation and Molecular Docking Studies of Some Pyrimidine Derivatives.</a> Fargualy, A.M., N.S. Habib, K.A. Ismail, A.M. Hassan, and M.T. Sarg. European Journal of Medicinal Chemistry, 2013. 66: p. 276-295. PMID[23811090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23904484">Chemical Screening Identifies Filastatin, a Small Molecule Inhibitor of Candida albicans Adhesion, Morphogenesis, and Pathogenesis.</a> Fazly, A., C. Jain, A.C. Dehner, L. Issi, E.A. Lilly, A. Ali, H. Cao, P.L. Fidel, Jr., P.R. R, and P.D. Kaufman. Proceedings of the National Academy of Sciences of the United States of America, 2013. 110(33): p. 13594-13599. PMID[23904484].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23081773">Antifungal Effects of Citronella Oil against Aspergillus niger ATCC 16404.</a> Li, W.R., Q.S. Shi, Y.S. Ouyang, Y.B. Chen, and S.S. Duan. Applied Microbiology and Biotechnology, 2013. 97(16): p. 7483-7492. PMID[23081773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23792315">Synthesis, Self-aggregation and Biological Properties of Alkylphosphocholine and Alkylphosphohomocholine Derivatives of Cetyltrimethylammonium bromide, Cetylpyridinium bromide, Benzalkonium bromide (C16) and Benzethonium chloride.</a> Lukac, M., M. Mrva, M. Garajova, G. Mojzisova, L. Varinska, J. Mojzis, M. Sabol, J. Kubincova, H. Haragova, F. Ondriska, and F. Devinsky. European Journal of Medicinal Chemistry, 2013. 66: p. 46-55. PMID[23792315].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23594040">Hydroxyaldimines as Potent in Vitro Anticryptococcal Agents.</a> Magalhaes, T.F., C.M. da Silva, A. de Fatima, D.L. da Silva, L.V. Modolo, C.V. Martins, R.B. Alves, A.L. Ruiz, G.B. Longato, J.E. de Carvalho, and M.A. de Resende-Stoianoff. Letters in Applied Microbiology, 2013. 57(2): p. 137-143. PMID[23594040].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23740727">In Vitro Activities of Isavuconazole and Comparator Antifungal Agents Tested against a Global Collection of Opportunistic Yeasts and Molds.</a> Pfaller, M.A., S.A. Messer, P.R. Rhomberg, R.N. Jones, and M. Castanheira. Journal of Clinical Microbiology, 2013. 51(8): p. 2608-2616. PMID[23740727].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23711469">cgMolluscidin, a Novel Dibasic Residue Repeat Rich Antimicrobial Peptide, Purified from the Gill of the Pacific Oyster, Crassostrea gigas.</a> Seo, J.K., M.J. Lee, B.H. Nam, and N.G. Park. Fish Shellfish Immunology, 2013. 35(2): p. 480-488. PMID[23711469].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689719">High-throughput Screening of a Collection of Known Pharmacologically Active Small Compounds for Identification of Candida albicans Biofilm Inhibitors.</a> Siles, S.A., A. Srinivasan, C.G. Pierce, J.L. Lopez-Ribot, and A.K. Ramasubramanian. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3681-3687. PMID[23689719].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23941529">Didehydroroflamycoin (DDHR) Pentaene Macrolide Family from Streptomyces durmitorensis MS405 : Production Optimization and Antimicrobial Activity.</a> Stankovic, N., L. Senerovic, Z. Bojic-Trbojevic, I. Vuckovic, L. Vicovac, B. Vasiljevic, and J. Nikodinovic-Runic. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23941529].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23838261">Evaluation of (4-Aminobutyloxy)quinolines as a Novel Class of Antifungal Agents.</a> Vandekerckhove, S., H.G. Tran, T. Desmet, and M. D&#39;Hooghe. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4641-4643. PMID[23838261].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23925418">17. Doubly Prenylated Tryptamines: Cytotoxicity, Antimicrobial Activity and Cyclisation to the Marine Natural Product Flustramine A.</a> Adla, S.K., F. Sasse, G. Kelter, H.H. Fiebig, and T. Lindel. Organic &amp; Biomolecular Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23925418].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23889343">Mycobacterium tuberculosis Shikimate Kinase Inhibitors: Design and Simulation Studies of the Catalytic Turnover.</a> Blanco, B., V. Prado, E. Lence, J.M. Otero, C. Garcia-Doval, M.J. van Raaij, A.L. Llamas-Saiz, H. Lamb, A.R. Hawkins, and C. Gonzalez-Bello. Journal of the American Chemical Society, 2013. <b>[Epub ahead of print]</b>. PMID[23889343].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23927683">Development of Mycobacterium tuberculosis Whole Cell Screening Hits as Potential Antituberculosis Agents.</a> Cooper, C.B. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23927683].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23924993">Antibacterial and Herbicidal Activity of Ring-substituted 2-Hydroxynaphthalene-1-carboxanilides.</a> Gonec, T., J. Kos, I. Zadrazilova, M. Pesko, R. Govender, S. Keltosova, B. Chambel, D. Pereira, P. Kollar, A. Imramovsky, J. O&#39;Mahony, A. Coffey, A. Cizek, K. Kralova, and J. Jampilek. Molecules, 2013. 18(8): p. 9397-9419. PMID[23924993].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23898841">Identification of Novel Inhibitors of Nonreplicating Mycobacterium tuberculosis Using a Carbon Starvation Model.</a> Grant, S.S., T. Kawate, P.P. Nag, M.R. Silvis, K. Gordon, S.A. Stanley, E. Kazyanskaya, R. Nietupski, A. Golas, M. Fitzgerald, S. Cho, S.G. Franzblau, and D.T. Hung. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23898841].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23779132">Synthesis and Biological Evaluation of 3-(4-Chlorophenyl)-4-Substituted Pyrazole Derivatives.</a> Horrocks, P., M.R. Pickard, H.H. Parekh, S.P. Patel, and R.B. Pathak. Organic &amp; Biomolecular Chemistry, 2013. 11(29): p. 4891-4898. PMID[23779132].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23913123">Discovery of Q203, a Potent Clinical Candidate for the Treatment of Tuberculosis.</a> Pethe, K., P. Bifani, J. Jang, S. Kang, S. Park, S. Ahn, J. Jiricek, J. Jung, H.K. Jeon, J. Cechetto, T. Christophe, H. Lee, M. Kempf, M. Jackson, A.J. Lenaerts, H. Pham, V. Jones, M.J. Seo, Y.M. Kim, M. Seo, J.J. Seo, D. Park, Y. Ko, I. Choi, R. Kim, S.Y. Kim, S. Lim, S.A. Yim, J. Nam, H. Kang, H. Kwon, C.T. Oh, Y. Cho, Y. Jang, J. Kim, A. Chua, B.H. Tan, M.B. Nanjundappa, S.P. Rao, W.S. Barnes, R. Wintjens, J.R. Walker, S. Alonso, S. Lee, J. Kim, S. Oh, T. Oh, U. Nehrbass, S.J. Han, Z. No, J. Lee, P. Brodin, S.N. Cho, K. Nam, and J. Kim. Natural Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[23913123].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23835293">Antimalarial and Cytotoxic Activities of Chiral Triamines.</a> Dellai, A., J. Appel, A. Bouraoui, S. Croft, and A. Nefzi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4579-4582. PMID[23835293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23801363">In Vivo Antimalarial Activity of Trichilia megalantha Harms Extracts and Fractions in Animal Models.</a> Fadare, D.A., O.O. Abiodun, and E.O. Ajaiyeoba. Parasitology Research, 2013. 112(8): p. 2991-2995. PMID[23801363].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23716053">Interrogating a Hexokinase-selected Small-molecule Library for Inhibitors of Plasmodium falciparum Hexokinase.</a> Harris, M.T., D.M. Walker, M.E. Drew, W.G. Mitchell, K. Dao, C.E. Schroeder, D.P. Flaherty, W.S. Weiner, J.E. Golden, and J.C. Morris. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3731-3737. PMID[23716053].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23788004">Structure Determination and Total Synthesis of (+)-16-Hydroxy-16,22-dihydroapparicine.</a> Hirose, T., Y. Noguchi, Y. Furuya, A. Ishiyama, M. Iwatsuki, K. Otoguro, S. Omura, and T. Sunazuka. Chemistry, 2013. 19(32): p. 10741-10750. PMID[23788004].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23918316">Structure-activity Relationship Studies of Pyrrolone Antimalarial Agents.</a> Murugesan, D., M. Kaiser, K.L. White, S. Norval, J. Riley, P.G. Wyatt, S.A. Charman, K.D. Read, C. Yeates, and I.H. Gilbert. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23918316].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23583697">Synthesis of Carbocyclic Pyrimidine Nucleosides and Their Inhibitory Activities against Plasmodium falciparum Thymidylate Kinase.</a> Noguchi, Y., Y. Yasuda, M. Tashiro, T. Kataoka, Y. Kitamura, M. Kandeel, and Y. Kitade. Parasitology International, 2013. 62(4): p. 368-371. PMID[23583697].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23792317">Synthesis and Bioevaluation of Novel 4-Aminoquinoline-tetrazole Derivatives as Potent Antimalarial Agents.</a> Pandey, S., P. Agarwal, K. Srivastava, S. Rajakumar, S.K. Puri, P. Verma, J.K. Saxena, A. Sharma, J. Lal, and P.M. Chauhan. European Journal of Medicinal Chemistry, 2013. 66: p. 69-81. PMID[23792317].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850202">In Vivo Antimalarial Activity of Novel 2-Hydroxy-3-anilino-1,4-naphthoquinones Obtained by Epoxide Ring-opening Reaction.</a> Rezende, L.C., F. Fumagalli, M.S. Bortolin, M.G. Oliveira, M.H. Paula, V.F. Andrade-Neto, and S. Emery Fda. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4583-4586. PMID[23850202].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23922204">Antiplasmodial Activity of Ethanolic Extracts of Some Selected Medicinal Plants from the Northwest of Iran.</a> Sangian, H., H. Faramarzi, A. Yazdinezhad, S.J. Mousavi, Z. Zamani, M. Noubarani, and A. Ramazani. Parasitology Research, 2013. <b>[Epub ahead of print]</b></p>

    <p class="plaintext">PMID[23922204].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23811093">Synthesis of 4-Aminoquinoline-pyrimidine Hybrids as Potent Antimalarials and Their Mode of Action Studies.</a> Singh, K., H. Kaur, K. Chibale, and J. Balzarini. European Journal of Medicinal Chemistry, 2013. 66: p. 314-323. PMID[23811093].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p><br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23689711">Broad-spectrum Antimalarial Activity of Peptido Sulfonyl Fluorides, a New Class of Proteasome Inhibitors.</a> Tschan, S., A.J. Brouwer, P.R. Werkhoven, A.M. Jonker, L. Wagner, S. Knittel, M.N. Aminake, G. Pradel, F. Joanny, R.M. Liskamp, and B. Mordmuller. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3576-3584. PMID[23689711].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0802-081513.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320684600014">Synthesis of New Series of 3-Hydroxy/Acetoxy-2-phenyl-4H-chromen-4-ones and Their Biological Importance.</a> Gharpure, M., R. Choudhary, V. Ingle, and H. Juneja. Journal of Chemical Sciences, 2013. 125(3): p. 575-582. ISI[000320684600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319650800017">Identification of Compounds with Potential Antibacterial Activity against Mycobacterium through Structure-based Drug Screening.</a> Kinjo, T., Y. Koseki, M. Kobayashi, A. Yamada, K. Morita, K. Yamaguchi, R. Tsurusawa, G. Gulten, H. Komatsu, H. Sakamoto, J.C. Sacchettini, M. Kitamura, and S. Aoki. Journal of Chemical Information and Modeling, 2013. 53(5): p. 1200-1212. ISI[000319650800017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321360700001">Synthesis of New Fluorine Substituted Heterocyclic Nitrogen Systems Derived from P-Aminosalicylic acid as Antimycobacterial Agents.</a> Makki, M., R.M. Abdel-Rahman, H.M. Faidallah, and K.A. Khan. Journal of Chemistry, 2013. ISI[000321360700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320664200003">Antifungal Vanillin-imino-chitosan Biodynameric Films.</a> Marin, L., I. Stoica, M. Mares, V. Dinu, B.C. Simionescu, and M. Barboiu. Journal of Materials Chemistry B, 2013. 1(27): p. 3353-3358. ISI[000320664200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321384000001">Effects of Chitosan on Candida albicans: Conditions for Its Antifungal Activity.</a> Pena, A., N.S. Sanchez, and M. Calahorra. Biomed Research International, 2013. 527549 . ISI[000321384000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320928400010">Modification of Triclosan Scaffold in Search of Improved Inhibitors for Enoyl-Acyl Carrier Protein (ACP) Reductase in Toxoplasma gondii.</a> Stec, J., A. Fomovska, G.A. Afanador, S.P. Muench, Y. Zhou, B.S. Lai, K. El Bissati, M.R. Hickman, P.J. Lee, S.E. Leed, J.M. Auschwitz, C. Sommervile, S. Woods, C.W. Roberts, D. Rice, S.T. Prigge, R. McLeod, and A.P. Kozikowski. ChemMedChem, 2013<b>.</b> 8(7): p. 1138-1160. ISI[000320928400010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0802-081513.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
