

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-01-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dT7ljQtITCNmJh5Uc6KXtp3ZWXpgaXEF8wMTs9muIGYgIc80KlPdfEmKwkK4fhBgNRDUUQbRprOKJawvDHozmbay6XVjlkEvxRXsXj/js5CooUmzKg7Bwmz+37o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="23E4F698" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 16 - January 29, 2015</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25503645">Identification of Benzenesulfonamide quinoline Derivatives as Potent HIV-1 Replication Inhibitors Targeting Rev Protein.</a> Zhong, F., G. Geng, B. Chen, T. Pan, Q. Li, H. Zhang, and C. Bai. Organic &amp; Biomolecular Chemistry, 2015. 13(6): p. 1792-1799. PMID[25503645].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0116-012915.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25522204">Design, Synthesis, Biochemical, and Antiviral Evaluations of C6 Benzyl and C6 Biarylmethyl Substituted 2-Hydroxylisoquinoline-1,3-diones: Dual Inhibition against HIV Reverse Transcriptase-associated RNase H and Polymerase with Antiviral Activities.</a> Vernekar, S.K., Z. Liu, E. Nagy, L. Miller, K.A. Kirby, D.J. Wilson, J. Kankanala, S.G. Sarafianos, M.A. Parniak, and Z. Wang. Journal of Medicinal Chemistry, 2015. 58(2): p. 651-664. PMID[25522204].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0116-012915.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347231200046">Mechanism of Multivalent Nanoparticle Encounter with HIV-1 for Potency Enhancement of Peptide Triazole Virus Inactivation.</a> Bastian, A.R., A. Nangarlia, L.D. Bailey, A. Holmes, R.V.K. Sundaram, C. Ang, D.R.M. Moreira, K. Freedman, C. Duffy, M. Contarino, C. Abrams, M. Root, and I. Chaiken. Journal of Biological Chemistry, 2015. 290(1): p. 529-543. ISI[000347231200046].</p>
    
    <p class="plaintext"><b>[WOS]</b>. HIV_0116-012915.</p>
    
    <br />
    
    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000346878600023">Characterization of a Plant-produced Recombinant Human Secretory IgA with Broad Neutralizing Activity against HIV.</a> Paul, M., R. Reljic, K. Klein, P.M. Drake, D.C. van, M. Pabst, M. Windwarder, E. Arcalis, E. Stoger, F. Altmann, C. Cosgrove, A. Bartolf, S. Baden, and J.K.C. Ma. mAbs, 2014. 6(6): p. 1585-1597. ISI[000346878600023].</p>
   
    <p class="plaintext"><b>[WOS]</b>. HIV_0116-012915.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000346651004122">Isolation and Characterization of a Novel Class of Potent anti-HIV Proteins from an Australian Soft Coral.</a> Ramessar, K., C. Xiong, L. Krumpe, R. Buckheit, J. Wilson, J. McMahon, and B. O&#39;Keefe. FASEB Journal, 2014. 28(1). ISI[000346651004122].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0116-012915.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000347019800034">Evaluation of anti-HIV-1 Activity of a New Iridoid Glycoside Isolated from Avicenna marina, in Vitro.</a> Behbahani, M. International Immunopharmacology, 2014. 23(1): p. 262-266. ISI[000347019800034].
    <br />
    <b>[WOS]</b>. HIV_0116-012915.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
