

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-03-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5t6ovvmd9MeIePaCG1dGCROLty+zygqHKzxi5aM3iyUl0UZc4r59WdHJBIT/5Mh9+moHb+W25iP1IK1xkXU45aLqs8Kc6GUldOaTIlQ7kY0ba7U6ThxLabzIj2E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6A7B85FE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">March 2- March 15, 2012</h1>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22309915">New Heterocyclic Compounds from 1,2,4-Triazole and 1,3,4-Thiadiazole Class Bearing Diphenylsulfone Moieties. Synthesis, Characterization and Antimicrobial Activity Evaluation.</a>Barbuceanu, S.F., G. Saramet, G.L. Almajan, C. Draghici, F. Barbuceanu, and G. Bancescu. European Journal of Medicinal Chemistry, 2012. 49C: p. 417-423; PMID[22309915].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22405895">Gliotoxin Effects on Fungal Growth: Mechanisms and Exploitation.</a> Carberry, S., E. Molloy, S. Hammel, G. O&#39;Keeffe, G.W. Jones, K. Kavanagh, and S. Doyle. Fungal Genetics and Biology : FG &amp; B, 2012. <b>[Epub ahead of print]</b>; PMID[22405895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22406553">Antifungal Property of Dihydrodehydrodiconiferyl alcohol 9&#39;-O-beta-D-Glucoside and Its Pore-forming Action in Plasma Membrane of Candida albicans.</a> Choi, H., J. Cho, Q. Jin, E.R. Woo, and D.G. Lee. Biochimica et Biophysica Acta, 2012. <b>[Epub ahead of print]</b>; PMID[22406553].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22210485">Biocompatibility and Antimicrobial Activity of Poly(3-Hydroxyoctanoate) Grafted with Vinylimidazole.</a> Chung, M.G., H.W. Kim, B.R. Kim, Y.B. Kim, and Y.H. Rhee. International Journal of Biological Macromolecules, 2012. 50(2): p. 310-316; PMID[22210485].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22321993">Synthesis and Antifungal Activity of a New Series of 2-(1H-Imidazol-1-yl)-1-Phenylethanol Derivatives.</a>De Vita, D., L. Scipione, S. Tortorella, P. Mellini, B. Di Rienzo, G. Simonetti, F.D. D&#39;Auria, S. Panella, R. Cirilli, R. Di Santo, and A.T. Palamara. European Journal of Medicinal Chemistry, 2012. 49C: p. 334-342; PMID[22321993].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22397713">Antimicrobial Activity of Sesquiterpene Lactones Isolated from Traditional Medicinal Plant, Costus speciosus (Koen Ex.Retz.) Sm. Duraipandiyan, V., N.A.</a> Al-Harbi, S. Ignacimuthu, and C. Muthukumar. Bmc Complementary and Alternative Medicine, 2012. 12(1): p. 13; PMID[22397713].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22265751">Antimicrobial Activity of Ulopterol Isolated from Toddalia asiatica (L.) Lam.: A Traditional Medicinal Plant.</a>Karunai Raj, M., C. Balachandran, V. Duraipandiyan, P. Agastian, and S. Ignacimuthu. Journal of Ethnopharmacology, 2012. 140(1): p. 161-165; PMID[22265751].</p>

    <p class="plaintext"> <b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22167241">Antibiofilm Activity of Certain Phytocompounds and Their Synergy with Fluconazole against Candida albicans Biofilms.</a>Khan, M.S. and I. Ahmad. The Journal of Antimicrobial Chemotherapy, 2012. 67(3): p. 618-621; PMID[22167241].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p class="MsoNormal"></p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22380482">Four Butyrolactones and Diverse Bioactive Secondary Metabolites from Terrestrial Aspergillus flavipes Mm2: Isolation and Structure Determination.</a> Nagia, M.M., M.M. El-Metwally, M. Shaaban, S.M. El-Zalabani, and A.G. Hanna. Organic and Medicinal Chemistry Letters, 2012. 2(1): p. 9; PMID[22380482].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22281750/">Fungal Biofilm Inhibitors from a Human Oral Microbiome-derived Bacterium.</a> Wang, X., L. Du, J. You, J.B. King, and R.H. Cichewicz. Organic &amp; Biomolecular Chemistry, 2012. 10(10): p. 2044-2050; PMID[22281750].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22400916">Waikialoid A Suppresses Hyphal Morphogenesis and Inhibits Biofilm Development in Pathogenic Candida albicans.</a>Wang, X., J. You, J.B. King, D.R. Powell, and R.H. Cichewicz. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[22400916].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22391539">In Vitro Interactions between Aspirin and Amphotericin B against Planktonic Cells and Biofilm Cells of C. albicans and C. parapsilosis.</a> Zhou, Y., G. Wang, Y. Li, Y. Liu, Y. Song, W. Zheng, N. Zhang, X. Hu, S. Yan, and J. Jia. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22391539].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="memofmt2-3"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>    

    <p class="memofmt2-3"> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22325950">Novel Imidazo[2,1-B][1,3,4]thiadiazole Carrying Rhodanine-3-acetic acid as Potential Antitubercular Agents.</a> Alegaon, S.G., K.R. Alagawadi, P.V. Sonkusare, S.M. Chaudhary, D.H. Dadwe, and A.S. Shah. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(5): p. 1917-1921; PMID[22325950].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22409320">Evaluation of Small Molecule Tuberculostats for Targeting Tuberculosis Infections of the Central Nervous System.</a>Bartzatt, R. Central Nervous System Agents in Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22409320].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22405039">Preliminary Investigations of the Effect of Lipophilic Analogs of the Active Metabolite of Isoniazid Towards Bacterial and Plasmodial Strains.</a> Delaine, T., V. Bernardes-Genisson, A. Quemard, P. Constant, F. Cosledan, B. Meunier, and J. Bernadou. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22405039].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22301215">1,4-Diarylpiperazines and Analogs as Anti-tubercular Agents: Synthesis and Biological Evaluation.</a> Forge, D., D. Cappoen, J. Laurent, D. Stanicki, A. Mayence, T.L. Huang, L. Verschaeve, K. Huygen, and J.J. Vanden Eynde. European Journal of Medicinal Chemistry, 2012. 49C: p. 95-101; PMID[22301215].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22369127">Trisubstituted Imidazoles as Mycobacterium tuberculosis Glutamine Synthetase Inhibitors.</a> Gising, J., M.T. Nilsson, L.R. Odell, S. Yahiaoui, M. Lindh, H. Iyer, A.M. Sinha, B.R. Srinivasa, M. Larhed, S.L. Mowbray, and A. Karlen. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22369127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22321994">Novel Metal Based Anti-tuberculosis Agent: Synthesis, Characterization, Catalytic and Pharmacological Activities of Copper Complexes.</a> Joseph, J., K. Nagashri, and G.B. Janaki. European Journal of Medicinal Chemistry, 2012. 49C: p. 151-163; PMID[22321994].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22280817">Design, Synthesis of Some New (2-Aminothiazol-4-yl)methylester Derivatives as Possible Antimicrobial and Antitubercular Agents.</a> Karuvalam, R.P., K.R. Haridas, S.K. Nayak, T.N. Guru Row, P. Rajeesh, R. Rishikesan, and N.S. Kumari. European Journal of Medicinal Chemistry, 2012. 49C: p. 172-182; PMID[22280817].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22234257">Anti-Mycobacterial Diynes from the Canadian Medicinal Plant Aralia nudicaulis.</a> Li, H., T. O&#39;Neill, D. Webster, J.A. Johnson, and C.A. Gray. Journal of Ethnopharmacology, 2012. 140(1): p. 141-144; PMID[22234257].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22264895">Synthesis and Evaluation of Anti-tubercular and Antibacterial Activities of New 4-(2,6-Dichlorobenzyloxy)phenyl Thiazole, Oxazole and Imidazole Derivatives. Part 2.</a>Lu, X., X. Liu, B. Wan, S.G. Franzblau, L. Chen, C. Zhou, and Q. You. European journal of medicinal chemistry, 2012. 49C: p. 164-171; PMID[22264895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="memofmt2-3"> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22394198">In Vitro Antimalarial Activity and Molecular Docking Analysis of 4-Aminoquinoline Clubbed 1,3,5-Triazine Derivatives.</a> Bhat, H.R., S.K. Ghosh, A. Prakash, K. Gogoi, and U.P. Singh. Letters in Applied Microbiology, 2012. <b>[Epub ahead of print]</b>; PMID[22394198].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22404785">Biological Activities of Nitidine, a Potential Anti-malarial Lead Compound.</a> Bouquet, J., M. Rivaud, S. Chevalley, E. Deharo, V. Jullian, and A. Valentin. Malaria Journal, 2012. 11(1): p. 67; PMID[22404785].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22396598">Drug Screen Targeted at Plasmodium Liver Stages Identifies a Potent Multistage Antimalarial Drug.</a> da Cruz, F.P., C. Martin, K. Buchholz, M.J. Lafuente-Monasterio, T. Rodrigues, B. Sonnichsen, R. Moreira, F.J. Gamo, M. Marti, M.M. Mota, M. Hannus, and M. Prudencio. The Journal of Infectious Diseases, 2012. <b>[Epub ahead of print]</b>; PMID[22396598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22391526">Proveblue (Methylene Blue) as an Antimalarial Agent: In Vitro Synergy with Dihydroartemisinin and Atorvastatin.</a>Dormoi, J., A. Pascual, S. Briolant, R. Amalvict, S. Charras, E. Baret, E. Huyghues des Etages, M. Feraud, and B. Pradines. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22391526].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22116094">Defibrotide Interferes with Several Steps of the Coagulation-inflammation Cycle and Exhibits Therapeutic Potential to Treat Severe Malaria.</a> Francischetti, I.M., C.J. Oliveira, G.R. Ostera, S.B. Yager, F. Debierre-Grockiego, V. Carregaro, G. Jaramillo-Gutierrez, J.C. Hume, L. Jiang, S.E. Moretz, C.K. Lin, J.M. Ribeiro, C.A. Long, B.K. Vickers, R.T. Schwarz, K.B. Seydel, M. Iacobelli, H.C. Ackerman, P. Srinivasan, R.B. Gomes, X. Wang, R.Q. Monteiro, M. Kotsyfakis, A. Sa-Nunes, and M. Waisberg. Arteriosclerosis, Thrombosis, and Vascular Biology, 2012. 32(3): p. 786-798; PMID[22116094].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233734">Antimalarial Evaluation and Docking Studies of Hybrid Phenylthiazolyl-1,3,5-triazine Derivatives: A Novel and Potential Antifolate Lead for Pf-DHFR-TS Inhibition.</a> Gahtori, P., S.K. Ghosh, P. Parida, A. Prakash, K. Gogoi, H.R. Bhat, and U.P. Singh. Experimental Parasitology, 2012. 130(3): p. 292-299; PMID[22233734].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22390733">Absolute Configuration and Total Synthesis of a Novel Antimalarial Lipopeptide by the De Novo Preparation of Chiral Nonproteinogenic Amino Acids.</a> Ghosh, S.K., B. Somanadhan, K.S. Tan, M.S. Butler, and M.J. Lear. Organic Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22390733].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22378031">1,2-Disubstituted ferrocenyl carbohydrate Chloroquine Conjugates as Potential Antimalarial Agents.</a> Herrmann, C., P.F. Salas, B.O. Patrick, C. de Kock, P.J. Smith, M.J. Adam, and C. Orvig. Dalton Transactions (Cambridge, England : 2003), 2012. <b>[Epub ahead of print]</b>; PMID[22378031].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22364417">Identification, Design and Biological Evaluation of Heterocyclic Quinolones Targeting Plasmodium falciparum Type II NADH:Quinone Oxidoreductase (Pfndh2).</a> Leung, S.C., P. Gibbons, R. Amewu, G.L. Nixon, C. Pidathala, W.D. Hong, B. Pacorel, N.G. Berry, R. Sharma, P.A. Stocks, A. Srivastava, A.E. Shone, S. Charoensutthivarakul, L. Taylor, O. Berger, A. Mbekeani, A. Hill, N.E. Fisher, A.J. Warman, G.A. Biagini, S.A. Ward, and P.M. O&#39;Neill. Journal of Medicinal Chemistry, 2012. 55(5): p. 1844-1857; PMID[22364417].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155838">Novel Potent Metallocenes against Liver Stage Malaria.</a> Matos, J., F.P. da Cruz, E. Cabrita, J. Gut, F. Nogueira, V.E. do Rosario, R. Moreira, P.J. Rosenthal, M. Prudencio, and P. Gomes. Antimicrobial Agents and Chemotherapy, 2012. 56(3): p. 1564-1570; PMID[22155838].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22401346">Differences in Anti-Malarial Activity of 4-Aminoalcohol quinoline Enantiomers and Investigation of the Presumed Underlying Mechanism of Action.</a> Mullie, C., A. Jonet, C. Degrouas, N. Taudon, and P. Sonnet. Malaria Journal, 2012. 11(1): p. 65; PMID[22401346].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22392982">Lipophilic Analogs of Zoledronate and Risedronate Inhibit Plasmodium Geranylgeranyl Diphosphate Synthase (Ggpps) and Exhibit Potent Antimalarial Activity.</a> No, J.H., F. de Macedo Dossin, Y. Zhang, Y.L. Liu, W. Zhu, X. Feng, J.A. Yoo, E. Lee, K. Wang, R. Hui, L.H. Freitas-Junior, and E. Oldfield. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22392982].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22380766">Design, Synthesis and in Vitro Activity of Novel 2&#39;-O-Substituted 15-Membered Azalides.</a> Pesic, D., K. Starcevic, A. Toplak, E. Herreros, M.J. Vidal, M. Almela, D. Jelic, S. Alihodzic, R. Spaventi, and M. Peric. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22380766].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22364416">Identification, Design and Biological Evaluation of Bisaryl Quinolones Targeting Plasmodium falciparum Type II NADH:Quinone Oxidoreductase (Pfndh2).</a> Pidathala, C., R. Amewu, B. Pacorel, G.L. Nixon, P. Gibbons, W.D. Hong, S.C. Leung, N.G. Berry, R. Sharma, P.A. Stocks, A. Srivastava, A.E. Shone, S. Charoensutthivarakul, L. Taylor, O. Berger, A. Mbekeani, A. Hill, N.E. Fisher, A.J. Warman, G.A. Biagini, S.A. Ward, and P.M. O&#39;Neill. Journal of Medicinal Chemistry, 2012. 55(5): p. 1831-1843; PMID[22364416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22380711">Identification of Novel Antimalarial Chemotypes via Chemoinformatic Compound Selection Methods for a High Throughput Screening Program against the Novel Malarial Target, Pfndh2 : Increasing Hit Rate via Virtual Screening Methods.</a> Sharma, R., A.S. Lawrenson, N. Fisher, A. Warman, A.E. Shone, A. Hill, A. Mbekeani, C. Pidathala, R. Amewu, S. Leung, P. Gibbons, D.W. Hong, P. Stocks, G.L. Nixon, J. Chadwick, J. Shearer, I. Gowers, D. Cronk, S.P. Parel, P.M. O&#39;Neill, S.A. Ward, G. Biagini, and N.G. Berry. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22380711].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22321992">Novel Hybrid Molecules Based on 15-Membered Azalide as Potential Antimalarial Agents.</a> Starcevic, K., D. Pesic, A. Toplak, G. Landek, S. Alihodzic, E. Herreros, S. Ferrer, R. Spaventi, and M. Peric. European Journal of Medicinal Chemistry, 2012. 49C: p. 365-378; PMID[22321992].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22412858">Gibberellin Biosynthetic Inhibitors Make Human Malaria Parasite Plasmodium falciparum Cells Swell and Rupture to Death.</a> Toyama, T., M. Tahara, K. Nagamune, K. Arimitsu, Y. Hamashima, N.M. Palacpac, H. Kawaide, T. Horii, and K. Tanabe. Plos One, 2012. 7(3): p. e32246; PMID[22412858].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-3"> </p>

    <h2 class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p> </p>

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227102">Monoterpenic Aldehydes as Potential Anti-leishmania Agents: Activity of Cymbopogon citratus and citral on L. infantum, L. tropica and L. major.</a> Machado, M., P. Pires, A.M. Dinis, M. Santos-Rosa, V. Alves, L. Salgueiro, C. Cavaleiro, and M.C. Sousa. Experimental Parasitology, 2012. 130(3): p. 223-231; PMID[22227102].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22394563">In Vitro Antiprotozoal and Cytotoxic Activity of 33 Ethonopharmacologically Selected Medicinal Plants from DR Congo.</a> Musuyu Muganza, D., B.I. Fruth, J. Nzunzu Lami, G.K. Mesia, O.K. Kambu, G.L. Tona, R. Cimanga Kanyanga, P. Cos, L. Maes, S. Apers, and L. Pieters. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22394563].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21847598">The Effect of Verapamil on in Vitro Susceptibility of Promastigote and Amastigote Stages of Leishmania tropica to Meglumine Antimoniate.</a> Shokri, A., I. Sharifi, A. Khamesipour, N. Nakhaee, M. Fasihi Harandi, J. Nosratabadi, M. Hakimi Parizi, and M. Barati. Parasitology Research, 2012. 110(3): p. 1113-7; PMID[21847598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22393429">Discovery of Inhibitors of Leishmania beta-1,2-Mannosyltransferases Using a Click-Chemistry-Derived Guanosine Monophosphate Library.</a> Van der Peet, P., J.E. Ralton, M.J. McConville, and S.J. Williams. Plos One, 2012. 7(2): p. e32642; PMID[22393429].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0302-031512.</p>

    <p> </p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p> </p>

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299734600022">In Vitro Antimicrobial and Antioxidant Activities of Bark Extracts of Bauhinia purpurea.</a> Avinash, P., I.H. Attitalla, M. Ramgopal, S. Ch, and M. Balaji. African Journal of Biotechnology, 2011. 10(45): p. 9160-9164; [ISI:000299734600022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0217-030112.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300220800017">Effects of the Essential Oil of Zataria multiflora Boiss, a Thyme-like Medicinal Plant from Iran on the Growth and Sporulation of Aspergillus niger Both in Vitro and on Lime Fruits.</a>Abdollahi, M., H. Hamzehzarghani, and M.J. Saharkhiz. Journal of Food Safety, 2011. 31(3): p. 424-432; [ISI:000300220800017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300219200005">Aqueous Extracts from the Bulbs of Tulbaghia Violacea Are Antifungal against Aspergillus flavus.</a> Belewa, V., H. Baijnath, and B.M. Somai. Journal of Food Safety, 2011. 31(2): p. 176-184; [ISI:000300219200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300391200017">Synthesis and Evaluation of Malonate-based Inhibitors of Phosphosugar-metabolizing Enzymes: Class Ii Fructose-1,6-bis-Phosphate aldolases, Type I Phosphomannose Isomerase, and Phosphoglucose Isomerase.</a> Desvergnes, S., S. Courtiol-Legourd, R. Daher, M. Dabrowski, L. Salmon, and M. Therisod. Bioorganic &amp; Medicinal Chemistry, 2012. 20(4): p. 1511-1520; [ISI:000300391200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300221500003">In Vivo Effect of Mint (Mentha viridis) Essential Oil on Growth and Aflatoxin Production by Aspergillus flavus Isolated from Stored Corn.</a> Gibriel, Y.A.Y., A.S. Hamza, A.Y. Gibriel, and S.M. Mohsen. Journal of Food Safety, 2011. 31(4): p. 445-451; [ISI:000300221500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300221500017">Scopoletin in Cassava Products as an Inhibitor of Aflatoxin Production.</a> Gnonlonfin, G.J.B., Y. Adjovi, F. Gbaguidi, J. Gbenou, D. Katerere, L. Brimer, and A. Sanni. Journal of Food Safety, 2011. 31(4): p. 553-558; [ISI:000300221500017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300366900004">A Synthetic Peptide Derived from NK-lysin with Activity against Mycobacterium tuberculosis and Its Structure-function Relationship.</a> Gu, H., R.J. Dai, K. Qiu, Z.Q. Teng, and H.Y. Wang. International Journal of Peptide Research and Therapeutics, 2011. 17(4): p. 301-306; [ISI:000300366900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299996600005">Application of a Microcalorimetric Method for Determining Drug Susceptibility in Mycobacterium Species.</a>Howell, M., D. Wirz, A.U. Daniels, and O. Braissant. Journal of Clinical Microbiology, 2012. 50(1): p. 16-20; [ISI:000299996600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300386100002">New Oxadiazole Derivatives of Isonicotinohydrazide in the Search for Antimicrobial Agents: Synthesis and in Vitro Evaluation.</a> Malhotra, M., M. Sanduja, A. Samad, and A. Deep. Journal of the Serbian Chemical Society, 2012. 77(1): p. 9-16; [ISI:000300386100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300114900015">Rapid Throughput Analysis of Filamentous Fungal Growth Using Turbidimetric Measurements with the Bioscreen C: A Tool for Screening Antifungal Compounds.</a> Medina, A., R.J.W. Lambert, and N. Magan. Fungal Biology, 2012. 116(1): p. 161-169; [ISI:000300114900015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300079600002">Sesquiterpene Aryl Ester Natural Products in North American Armillaria Species.</a> Misiek, M. and D. Hoffmeister. Mycological Progress, 2012. 11(1): p. 7-15; [ISI:000300079600002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300386100001">Synthesis of 1,6-Hexanediyl-bis(semicarbazides) and 1,6-Hexanediyl-bis(1,2,4-triazol-5-ones) and Their Antiproliferative and Antimicrobial Activity.</a> Pitucha, M., J. Rzymowska, A. Olender, and L. Grzybowska-Szatkowska. Journal of the Serbian Chemical Society, 2012. 77(1): p. 1-8; [ISI:000300386100001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300122800003">Antimicrobial Activity of Nanoemulsion on Cariogenic Planktonic and Biofilm Organisms</a>. Ramalingam, K., B.T. Amaechi, R.H. Ralph, and V.A. Lee. Archives of Oral Biology, 2012. 57(1): p. 15-22; [ISI:000300122800003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300314200019">Capsicum Annuum L. trypsin Inhibitor as a Template Scaffold for New Drug Development against Pathogenic Yeast.</a>Ribeiro, S.F.F., M.S. Silva, M. Da Cunha, A.O. Carvalho, G.B. Dias, G. Rabelo, E.O. Mello, C. Santa-Catarina, R. Rodrigues, and V.M. Gomes. Antonie Van Leeuwenhoek International Journal of General and Molecular Microbiology, 2012. 101(3): p. 657-670; [ISI:000300314200019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300386100003">Synthesis and Biological Activity of 4-Thiazolidinone Derivatives of Phenothiazine.</a> Sharma, R., P. Samadhiya, S.D. Srivastava, and S.K. Srivastava. Journal of the Serbian Chemical Society, 2012. 77(1): p. 17-26; [ISI:000300386100003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300219200008">Composition and Antifungal Activity of the Brazilian Myristica fragrans Houtt Essential Oil.</a> Valente, V.M.M., G.N. Jham, O.D. Dhingra, and I. Ghiviriga. Journal of Food Safety, 2011. 31(2): p. 197-202; [ISI:000300219200008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">59. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300192700002">Screening, Identification, and Characterization of Mechanistically Diverse Inhibitors of the Mycobacterium tuberculosis Enzyme, Pantothenate Kinase (Coaa).</a> Venkatraman, J., J. Bhat, S.M. Solapure, J. Sandesh, D. Sarkar, S. Aishwarya, K. Mukherjee, S. Datta, K. Malolanarasimhan, B. Bandodkar, and K.S. Das. Journal of Biomolecular Screening, 2012. 17(3): p. 293-302; [ISI:000300192700002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>

    <p> </p>

    <p class="plaintext">60. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294379100024">Screening Peptide Inhibitors Using Phage Peptide Library with Isocitrate Lyase in Mycobacterium tuberculosis as Target.</a> Yin, Y.H., X. Niu, B. Sun, G.S. Teng, Y.H. Zhao, and C.M. Wu. Chemical Research in Chinese Universities, 2011. 27(4): p. 635-640; [ISI:000294379100024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0302-031512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
