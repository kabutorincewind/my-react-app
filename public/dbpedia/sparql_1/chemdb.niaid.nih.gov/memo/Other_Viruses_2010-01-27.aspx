

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-01-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bRsZ/yoTVsWuLtqHS+tSgQY1lfgFN3Ce2OPCxR4siF2RagVtEVwENfq+6NvPMau0mRosMvqv6Ki9g4na0mwnSde2vdqKkf5PmMzwt9CoWCZu5epJKZldne0IplE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="994FC8C6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other viruses Citation List: </p>

    <p class="memofmt2-h1">January 6, 2009 - January 19, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20071590?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=25">1a/1b Subtype Profiling of non-nucleoside Polymerase Inhibitors of Hepatitis C Virus.</a></p>

    <p class="plaintext">Nyanguile O, Devogelaere B, Vijgen L, Van den Broeck W, Pauwels F, Cummings MD, de Bondt H, Vos AM, Berke J, Lenz O, Vandercruyssen G, Vermeiren K, Mostmans W, Dehertogh P, Delouvroy F, Vendeville S, Vandyck K, Dockx K, Cleiren E, Raboisson P, Simmen KA, Fanning GC.</p>

    <p class="plaintext">J Virol. 2010 Jan 13. [Epub ahead of print]</p> 

    <p class="memofmt2-2">cytomegalovirus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20083141?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Peptide-Derivatized Dendrimers Inhibit Human Cytomegalovirus Infection by Blocking Virus Binding to Cell Surface Heparan Sulfate.</a></p>

    <p class="plaintext">Luganini A, Giuliani A, Pirri G, Pizzuto L, Landolfo S, Gribaudo G.</p>

    <p class="plaintext">Antiviral Res. 2010 Jan 15. [Epub ahead of print]PMID: 20083141 [PubMed - as supplied by publisher]</p>  

    <p class="memofmt2-1">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="plaintext">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Novel P2-P4 macrocyclic inhibitors of HCV NS3/4A protease by P3 succinamide fragment depeptidization strategy.</a></p>

    <p class="plaintext">Pompei, Marco, Di Francesco, Maria Emilia, Pesci, Silvia, Koch, Uwe, Vignetti, Sue Ellen, Veneziano, Maria, Pace, Paola, Summa, Vincenzo.</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(1). 2010. 168-74.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Discovery of benzimidazole-diamide finger loop (Thumb Pocket I) allosteric inhibitors of HCV NS5B polymerase: Implementing parallel synthesis for rapid linker optimization.</a></p>

    <p class="plaintext">Goulet, Sylvie, Poupart, Marc-Andre, Gillard, James, Poirier, Martin, Kukolj, George, Beaulieu, Pierre L.</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(1). 2010. 196-200.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">P4 capped amides and lactams as HCV NS3 protease inhibitors with improved potency and DMPK profile.</a></p>

    <p class="plaintext">Nair, Latha G., Sannigrahi, Mousumi, Bogen, Stephane, Pinto, Patrick, Chen, Kevin X., Prongay, Andrew, Tong, Xiao, Cheng, K. -C., Girijavallabhan, Viyyoor, Njoroge, F. George.</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(2). 2010. 567-70.</p>

    <p> </p>

    <p class="plaintext">6.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">SYNTHESIS AND ANTI-HEPATITIS C VIRUS ACTIVITY OF 2 &#39;(beta)-HYDROXYETHYL AND 4 &#39;(alpha)-HYDROXYMETHYL CARBODINE ANALOGUES</a>.</p>

    <p class="plaintext">Liu, Lian Jin, Hong, Joon Hee</p>

    <p class="plaintext">NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS. 28(11-12). 2009. 1007-15.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">SYNTHESIS OF 2 &#39;-N-FORMAMIDO NUCLEOSIDES AND BIOLOGICAL EVALUATION</a>.</p>

    <p class="plaintext">Abramov, Mikhail, Renders, Marleen, Herdewijn, Piet.</p>

    <p class="plaintext">NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS. 28(11-12). 2009. 1042-50.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">SYNTHESIS AND IN VITRO ACTIVITY OF 4 &#39; AND 5 &#39;-MODIFIED ANALOGUES OF APIOSYL NUCLEOSIDES AS POTENT ANTI-HCV AGENTS</a>.</p>

    <p class="plaintext">Li, Hua, Lee, Wonjae, Hong, Joon Hee</p>

    <p class="plaintext">NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS. 28(11-12). 2009. 1104-6.</p>

    <p> </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
