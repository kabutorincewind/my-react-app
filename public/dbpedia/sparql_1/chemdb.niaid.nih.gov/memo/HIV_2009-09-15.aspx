

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="q7Snnd8UhBzTC7C2gmyTYzm8pMA+U1kT0RWHBzllAuYcRzBWojzbmbNhOVup6+40GxHqCNqGYxtgFRLy5Ws6PaRvK77ILGno2jLVLEeCrugdaSMfBzef36gjmos=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2F7EA2BF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  September 2 - September 15, 2009</h1> 

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19753563?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and In-vitro Activity of 4&#39;-Modified Analogues of ddA as Potent Anti-HIV Agents.</a>  Hong JH, Oh CH.  Arch Pharm (Weinheim). 2009 Sep 14. [Epub ahead of print]  PMID: 19753563 [PubMed - as supplied by publisher]</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19748255?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ellagitannins from Tuberaria lignosa as entry inhibitors of HIV.</a>  Bedoya LM, Abad MJ, Sánchez-Palomino S, Alcami J, Bermejo P.  Phytomedicine. 2009 Sep 10. [Epub ahead of print]  PMID: 19748255 [PubMed - as supplied by publisher]</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19746983?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, Synthesis, and Structure-Activity Relationship of a Novel Series of 2-Aryl 5-(4-Oxo-3-phenethyl-2-thioxothiazolidinylidenemethyl)furans as HIV-1 Entry Inhibitors.</a>  Katritzky AR, Tala SR, Lu H, Vakulenko AV, Chen QY, Sivapackiam J, Pandya K, Jiang S, Debnath AK.  J Med Chem. 2009 Sep 11. [Epub ahead of print]  PMID: 19746983 [PubMed - as supplied by publisher]</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19746963?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, Synthesis, Protein-Ligand X-ray Structure, and Biological Evaluation of a Series of Novel Macrocyclic Human Immunodeficiency Virus-1 Protease Inhibitors to Combat Drug Resistance.</a>  Ghosh AK, Kulkarni S, Anderson DD, Hong L, Baldridge A, Wang YF, Chumanevich AA, Kovalevsky AY, Tojo Y, Masayuki A, Koh Y, Tang J, Weber IT, Mitsuya H.  J Med Chem. 2009 Sep 11. [Epub ahead of print]  PMID: 19746963 [PubMed - as supplied by publisher]</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19741733?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Foamy combinatorial anti-HIV vectors with MGMTP140K potently inhibit HIV-1 and SHIV replication and mediate selection in vivo.</a>  Kiem HP, Wu RA, Sun G, von Laer D, Rossi JJ, Trobridge GD.  Gene Ther. 2009 Sep 10. [Epub ahead of print]  PMID: 19741733 [PubMed - as supplied by publisher]</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19737995?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Apricitabine: A Nucleoside Reverse Transcriptase Inhibitor for HIV Infection (October) (CE).</a>  Gaffney MM, Belliveau PP, Spooner LM.  Ann Pharmacother. 2009 Sep 8. [Epub ahead of print]  PMID: 19737995 [PubMed - as supplied by publisher]</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19732019?ordinalpos=11&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification and Characterization of a Highly Efficient Anti-HIV Pol Hammerhead Ribozyme.</a>  Müller-Kuller T, Capalbo G, Klebba C, Engels JW, Klein SA.  Oligonucleotides. 2009 Sep 4. [Epub ahead of print]  PMID: 19732019 [PubMed - as supplied by publisher]</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19726509?ordinalpos=13&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">5-Azacytidine Can Induce Human Immunodeficiency Virus Type 1 Lethal Mutagenesis.</a>  Dapp MJ, Clouser CL, Patterson S, Mansky LM.  J Virol. 2009 Sep 2. [Epub ahead of print]  PMID: 19726509 [PubMed - as supplied by publisher]</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19678643?ordinalpos=14&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The nucleoside analogue D-carba T blocks HIV-1 reverse transcription.</a>  Boyer PL, Vu BC, Ambrose Z, Julias JG, Warnecke S, Liao C, Meier C, Marquez VE, Hughes SH.  J Med Chem. 2009 Sep 10;52(17):5356-64.  PMID: 19678643 [PubMed - in process</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19666220?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of diarylpyridine derivatives as novel non-nucleoside HIV-1 reverse transcriptase inhibitors.</a>  Tian X, Qin B, Lu H, Lai W, Jiang S, Lee KH, Chen CH, Xie L.  Bioorg Med Chem Lett. 2009 Sep 15;19(18):5482-5. Epub 2009 Jul 19.  PMID: 19666220 [PubMed - in process]</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="ListParagraph">11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269127000015">Human Immunodeficiency Virus Type 1 Nucleocapsid Inhibitors Impede trans Infopection in Cellular and Explant Models and Protect Nonhuman Primates from Infection.</a> Wallace, GS; Cheng-Mayer, C; Schito, ML; Fletcher, P; Jenkins, LMM; Hayashi, R; Neurath, AR; Appella, E; Shattock, RJ.  Journal of Virology, 2009; 83(18): 9175-9182.</p>

    <p class="ListParagraph">12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269096200001">Glucuronidation of the Antiretroviral Drug Efavirenz by UGT2B7 and an in Vitro Investigation of Drug-Drug Interaction with Zidovudine.</a> Belanger, AS; Caron, P; Harvey, M; Zimmerman, PA; Mehlotra, RK; Guillemette, C.  DRUG METABOLISM AND DISPOSITION, 2009; 37(9): 1793-1796.</p>

    <p class="ListParagraph">13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269185000027">Electronic transitions of neutral and anionic quinolinone HIV-1 integrase inhibitor: Joint theory/experiment investigation.</a> Vandurm, P; Cauvin, C; Wouters, J; Perpete, EA; Jacquemin, D. CHEMICAL PHYSICS LETTERS, 2009; 478(4-6): 243-248.</p>

    <p class="ListParagraph">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269340000041">Synthesis of (+/-)-4 &#39;-ethynyl-5 &#39;,5 &#39;-difluoro-2 &#39;,3 &#39;-dehydro-3&#39;-deoxy- carbocyclic thymidine: a difluoromethylidene analogue of promising anti-HIV agent Ed4T.</a> Kumamoto, H; Haraguchi, K; Ida, M; Nakamura, KT; Kitagawa, Y; Hamasaki, T; Baba, M; Matsubayashi, SS; Tanaka, H.  TETRAHEDRON, 2009; 65(36): 7630-7636.</p>

    <p class="ListParagraph">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269484200017">Synthesis and Biological Evaluation of 6-Substituted Purinylcarbanucleosides with a Cyclopenta[b]thiophene Pseudosugar.</a> Abeijon, P; Blanco, JM; Caamanoa, O; Fernandez, F; Garcia, MD; Garcia-Mera, X; Rodriguez-Borges, JE; Balzarini, J; De Clercq, E.  SYNTHESIS-STUTTGART, 2009; 16: 2766-2772.</p>

    <p class="ListParagraph">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269481300021">Hypericum in infection: Identification of anti-viral and anti-inflammatory constituents.</a> Birt, DF; Widrlechner, MP; Hammer, KDP; Hillwig, ML; Wei, JQ; Kraus, GA; Murphy, PA; Mccoy, JA; Wurtele, ES; Neighbors, JD; Wiemer, DE; Maury, WJ; Price, JP.  PHARMACEUTICAL BIOLOGY, 2009; 47(8): 774-782.</p>

    <p class="ListParagraph">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269327500025">Structure-activity relationship study on artificial CXCR4 ligands possessing the cyclic pentapeptide scaffold: the exploration of amino acid residues of pentapeptides by substitutions of several aromatic amino acids.</a> Tanaka, T; Nomura, W; Narumi, T; Esaka, A; Oishi, S; Ohashi, N; Itotani, K; Evans, BJ; Wang, ZX; Peiper, SC; Fujii, N; Tamamura, H. ORGANIC &amp; BIOMOLECULAR CHEMISTRY, 2009; 18: 3805-3809.</p>

    <p class="ListParagraph">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269443400002">SYNTHESIS OF 3 &#39;-O-PHOSPHONOETHYL NUCLEOSIDES WITH AN ADENINE AND A THYMINE BASE MOIETY.</a> Huang, QY; Herdewijn, P. NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2009; 28(5-7): 337-351.</p>

    <p class="ListParagraph">19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269443400014">SYNTHESIS AND ANTIVIRAL ACTIVITY OF PURINE 2 &#39;,3 &#39;-DIDEOXY-2 &#39;,3&#39;-DIFLUORO-D-ARABINOFURANOSYL NUCLEOSIDES.</a> Sivets, GG; Kalinichenko, EN; Mikhailopulo, IA; Detorio, MA; McBrayer, TR; Whitaker, T; Schinazi, RF.  NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2009; 28(5-7): 519-536.</p>

    <p class="ListParagraph">20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269443400020">CARBOCYCLIC THYMIDINE ANALOGUES FOR USE AS POTENTIAL THERAPEUTIC AGENTS.</a> Seley-Radtke, KL; Sunkara, NK. NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS, 2009; 28(5-7): 633-641.</p>

    <p class="ListParagraph">21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269433300022">Composition and Antimicrobial Activity of the Leaf Essential oil of Litsea kostermansii from Taiwan.</a> Ho, CL; Wang, EIC; Hsu, KP; Lee, PY; Su, YC. NATURAL PRODUCT COMMUNICATIONS, 2009; 4(8): 1123-1126.</p>

    <p class="ListParagraph">22.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269485100022">Isoprenylated Chromone Derivatives from the Plant Endophytic  Fungus Pestalotiopsis fici.</a> Liu, L; Liu, SC; Niu, SB; Guo, LD; Chen, XL; Che, YS.  JOURNAL OF NATURAL PRODUCTS, 2009; 72(8): 1482-1486.</p>

    <p class="ListParagraph">23.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269398900002">Synthesis, antiviral activity, and stability of nucleoside analogs containing tricyclic bases.</a> Amblard, F; Fromentin, E; Detorio, M; Obikhod, A; Rapp, KL; McBrayer, TR; Whitaker, T; Coats, SJ; Schinazi, RF.  EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY, 2009; 44(10): 3845-3851.</p>

    <p class="ListParagraph">24.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000269398900037">Synthesis and evaluation of A-seco type triterpenoids for anti-HIV-1 protease activity.</a> Wei, Y; Ma, CM; Hattori, M. EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY, 2009; 44(10): 4112-4120.</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
