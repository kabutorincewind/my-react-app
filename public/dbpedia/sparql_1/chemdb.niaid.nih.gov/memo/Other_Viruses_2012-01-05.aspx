

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-01-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ct7Kwe8ukuOuZaUCSavdi6XH21Hb2AR4leEzpHQ4wNRQKnb9g342wn+4dYdwNTYtaJJc3/AerH7JX+vjMCB5TpGQXjw9bPXYWzwj7YDZtEvjUO9plXNF5duOKoE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="96DA08D4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">December 23 - January 5, 2012</h1>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/22192328">Chromone Derivatives from Halenia elliptica and Their anti-HBV Activities.</a> Sun, Y.W., G.M. Liu, H. Huang, and P.Z. Yu, Phytochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22192328].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22213147">Mycophenolic acid Augments Interferon-stimulated Gene Expression and Inhibits Hepatitis C Virus Infection in Vitro and in Vivo.</a> Pan, Q., P.E. de Ruiter, H.J. Metselaar, J. Kwekkeboom, J. de Jonge, H.W. Tilanus, H.L. Janssen, and L.J. van der Laan, Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[22213147].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22204909">Synthesis and anti-HCV Activity Evaluation of Anilinoquinoline Derivatives.</a> Peng, H.K., C.K. Lin, S.Y. Yang, C.K. Tseng, C.C. Tzeng, J.C. Lee, and S.C. Yang, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22204909].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22204334">2-Hydroxy-1-oxo-1,2-dihydroisoquinoline-3-carboxylic Acid with Inbuilt beta-NHydroxy-gamma-keto-acid Pharmacophore as HCV NS5B Polymerase Inhibitors</a>. Deore, R.R., G.S. Chen, C.S. Chen, P.T. Chang, M.H. Chuang, T.R. Chern, H.C. Wang, and J.W. Chern, Current Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22204334].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22203602">Identification of Novel N-(Morpholine-4-carbonyloxy) amidine Compounds as Potent Inhibitors against Hepatitis C Virus Replication.</a> Kusano-Kitazume, A., N. Sakamoto, Y. Okuno, Y. Sekine-Osajima, M. Nakagawa, S. Kakinuma, K. Kiyohashi, S. Nitta, M. Murakawa, S. Azuma, Y. Nishimura-Sakurai, M. Hagiwara, and M. Watanabe, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22203602].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22203595">In Vitro Activity of BMS-790052 on Hepatitis C Virus Genotype 4 NS5A.</a> Wang, C., L. Jia, H. Huang, D. Qiu, L. Valera, X. Huang, J.H. Sun, P.T. Nower, D.R. O&#39;Boyle, 2nd, M. Gao, and R.A. Fridell, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22203595].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22202806">Synthesis of 1,4-Disubstituted Mono and Bis-triazolocarbo-acyclonucleoside Analogues of 9-(4-Hydroxybutyl)guanine by Cu(I)-Catalyzed Click Azide-Alkyne Cycloaddition.</a> Krim, J., M. Taourirte, and J.W. Engels, Molecules (Basel, Switzerland), 2011. 17(1): p. 179-190; PMID[22202806].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22197397">Pyridobenzothiazole Derivatives as New Chemotype Targeting the HCV NS5B Polymerase.</a> Manfroni, G., F. Meschini, M.L. Barreca, P. Leyssen, A. Samuele, N. Iraci, S. Sabatini, S. Massari, G. Maga, J. Neyts, and V. Cecchetti, Bioorganic &amp; Medicinal Chemistry, 2011; PMID[22197397].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22196120">Inhibitory Effects of Polyphenols Toward HCV from the Mangrove Plant Excoecaria agallocha L.</a> Li, Y., S. Yu, D. Liu, P. Proksch, and W. Lin, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22196120].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22189140">Novel, Potent and Orally Bioavailable Indolizidinone-derived Inhibitors of the Hepatitis C Virus NS3 Protease.</a> Clarke, M.O., D. Byun, X. Chen, E. Doerffler, S.A. Leavitt, X.C. Sheng, C.Y. Yang, and C.U. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22189140].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1223-010512.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601342">In Vitro Profiling of GSK2336805, a Potent and Selective Inhibitor of HCV NS5A.</a> Bechtel, J., R. Crosby, A. Wang, E. Woldu, S. Van Horn, J. Horton, K. Creech, L.H. Caballo, C. Voitenleitner, J. Vamathevan, M. Duan, A. Spaltenstein, W. Kazmierski, C. Roberts, and R. Hamatake, Journal of Hepatology, 2011. 54: p. S307-S308; ISI[000297625601342].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602335">Novel, Potent, Pan-genotypic HCV NS3/4A Protease Inhibitors with a High Barrier to Resistance.</a> Buckman, B.O., K. Kossen, J.B. Nicholas, V. Serebryany, D. Ruhrmund, R. Rajagopalan, S. Misialek, L. Hooi, N. Aleskovski, L. Pan, L. Huang, C.J. Schaefer, S. Rajyaguru, S. Le Pogam, I. Najera, K. Klumpp, and S. Seiwert, Journal of Hepatology, 2011. 54: p. S472-S472; ISI[000297625602335].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601352">Substitution of a Conserved Amino acid in the Transmembrane Domain of E2 Confers Resistance to a Novel Small-molecule Inhibitor of HCV Entry.</a> Coburn, G.A., D.N. Fisch, S. Moorji, J.D. Murga, J.M. de Muys, D. Paul, A.Q. Han, Y. Rotshteyn, D. Qian, P.J. Maddon, and W.C. Olson, Journal of Hepatology, 2011. <b>54</b>: p. S311-S311; ISI[000297625601352].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602339">Characterization and Identification of PPI-437, PPI-668 and PPI-833 as Potent and Selective HCV NS5A Inhibitors with Activity against all HCV Genotypes.</a> Colonno, R., N. Huang, Q. Huang, E. Peng, A. Huq, M. Lau, M. Bencsik, M. Zhong, and L. Li, Journal of Hepatology, 2011. 54: p. S474-S474; ISI[000297625602339].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601356">Preclinical Properties of the Novel HCV NS3 Protease Inhibitor GS-9451.</a> Corsa, A., M. Robinson, H. Yang, B. Peng, G. Cheng, B. Schultz, O. Barauskas, M. Hung, X. Liu, C. Yang, Y. Wang, G. Rhodes, R. Pakdaman, M. Shen, C. Sheng, C. Kim, and W. Delaney, Journal of Hepatology, 2011. 54: p. S313-S313; ISI[000297625601356].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601393">Idenix NS5A HCV Replication Inhibitors with Low Picomolar, Pan-genotypic in Vitro Antiviral Activity.</a> Dousson, C.B., C. Chapron, D. Standring, J.P. Bilello, J. McCarville, M. La Colla, M. Seifer, C. Parsy, D. Dukhan, C. Pierra, and D. Surleraux, Journal of Hepatology, 2011. 54: p. S326-S327; ISI[000297625601393].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602345">Nonclinical and Cross-genotypic Profiles of GS-9669, a Novel HCV NS5B Non-nucleoside Thumb Site II Inhibitor.</a> Fenaux, M., Y. Tian, M. Matles, E. Mabery, J.Y. Zhang, S. Eng, B. Murray, J. Mwangi, S. Lazerwith, W. Lew, E. Canales, Q. Liu, D. Byun, E. Doerffler, H. Ye, M. Clarke, M. Mertzman, P. Morganelli, J. Zhang, S. Leavitt, T. Appleby, A. Hashash, A. Bidgood, S. Krawczyk, and W. Watkins, Journal of Hepatology, 2011. 54: p. S476-S476; ISI[000297625602345].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601365">BMS-766, a Novel HCV NS5A Inhibitor with Enhanced Resistance Coverage.</a> Gao, M., R. Fridell, C. Wang, J.H. Sun, D. O&#39;Boyle, L. Valera, P. Nower, A. Monikowski, M. Kirk, H. Huang, C. Ngo, H. Fang, R. Knox, Y.K. Wang, V. Nguyen, F. Wang, L. Snyder, R. Lavoie, J. Bender, J. Kadow, M. Cockett, N. Meanwell, and M. Belema, Journal of Hepatology, 2011. 54: p. S316-S316; ISI[000297625601365].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602349">GNS-227: A New Potent and Selective HCV NS3 Protease Inhibitor with a High Genetic Barrier to Resistance.</a> Halfon, P., J. Courcambeck, T. Whitaker, M. Bouzidi, T.R. McBrayer, G. Roche, P.M. Tharnish, G. Pepe, R.F. Schinazi, and S.J. Coats, Journal of Hepatology, 2011. 54: p. S478-S478; ISI[000297625602349].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602351">ACH-2928: A Novel Highly Potent HCV NS5A Inhibitor with Favorable Preclinical Characteristics.</a> Huang, M., G. Yang, D. Patel, Y. Zhao, J. Fabrycki, C. Marlor, J. Rivera, K. Stauber, V. Gadhachanda, J. Wiles, A. Hashimoto, D. Chen, Q. Wang, G. Pais, X. Wang, M. Deshpande, and A. Phadke, Journal of Hepatology, 2011. 54: p. S479-S479; ISI[000297625602351].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602352">A Highly Potent HCV NS5A Inhibitor EDP-239 with Favorable Preclinical Pharmacokinetics.</a> Jiang, L.J., S. Liu, T. Phan, C. Owens, B. Brasher, A. Polemeropoulos, X. Luo, K. Hoang, M. Wang, X. Peng, H. Cao, Y. Qiu, and Y.S. Or, Journal of Hepatology, 2011. 54: p. S479-S479; ISI[000297625602352].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602354">Preclinical Characterization of the Hepatitis C Virus NS5B Polymerase Non-nucleoside Inhibitor BILB 1941.</a> Kukolj, G., P.C. Anderson, M. Boes, M.G. Cordingley, R. Coulombe, M. Garneau, J. Gillard, S. Goulet, E. Jolicoeur, L. Lagace, D. Lamarre, S. Laplante, M. Marquis, G. McKercher, C. Pellerin, M.A. Poupart, and P.L. Beaulieu, Journal of Hepatology, 2011. 54: p. S480-S480; ISI[000297625602354].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602355">Highly Potent HCV NS5B Inhibitor with Original Mechanism of Action Identified with the 3D-screen Technology.</a> Lahmar, M., A. Berecibar, I. Valarche, A. Martin, A. Piessens, L. Batard, C. Freslon-Evain, C. Fernagut, P. Guedat, and M. Mehtali, Journal of Hepatology, 2011. 54: p. S480-S480; ISI[000297625602355].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297603600017">Self-assembling Peptide Nanotubes with Antiviral Activity against Hepatitis C Virus.</a> Montero, A., P. Gastaminza, M. Law, G.F. Cheng, F.V. Chisari, and M.R. Ghadiri, Chemistry &amp; biology, 2011. 18(11): p. 1453-1462; ISI[000297603600017].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625602367">Characterization of Novel, Highly Potent NS5A Inhibitors with QD Dosing Potential and Robust Activity in an HCV Chimeric Animal Model.</a> Nicholas, J.B., B.O. Buckman, V. Serebryany, C.J. Schaefer, K. Kossen, D. Ruhrmund, L. Hooi, N. Aleskovski, A. Arfsten, S.R. Lim, L. Pan, L. Huang, R. Rajagopalan, S. Misialek, and S. Seiwert, Journal of Hepatology, 2011. 54: p. S485-S485; ISI[000297625602367].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601396">Preclinical Characterization of GSK2485852, a Novel HCV Polymerase Inhibitor.</a> Voitenleitner, C., R. Crosby, A. Wang, J. Bechtel, M. Xie, J. Pouliot, E. Woldu, S. Horn, J. Horton, K. Creech, L. Caballo, J. Shotwell, A. Spaltenstein, Z. Hong, and R. Hamatake, Journal of Hepatology, 2011. 54: p. S328-S328; ISI[000297625601396].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297625601357">Preclinical Characterization of the Novel HCV NS3 Protease Inhibitor GS-9256.</a> Yang, H., C. Yang, Y. Wang, G. Rhodes, M. Robinson, B. Schultz, O. Barauskas, B. Peng, G. Cheng, R. Wang, X. Liu, R. Pakdaman, C. Sheng, C. Kim, and W. Delaney, Journal of Hepatology, 2011. 54: p. S313-S313; ISI[000297625601357].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_1223-010512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
