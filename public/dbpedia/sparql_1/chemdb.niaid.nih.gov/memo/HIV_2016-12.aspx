

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="74fK2HMakYUY8DcT6j48/5U/DBVy9XnYoSOVBVCxy5m3Vsx8ZTcImq4ri0eRKp944VqmXsTyKLElYA+UNu8bnnf7+snPc55pE0P8QvwSfWwjUywwuLFVZgbN6gk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="21AE1FA3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: December, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27509245">Precursor Forms of Vitamin D Reduce HIV-1 Infection in Vitro.</a> Aguilar-Jimenez, W., S. Villegas-Ospina, S. Gonzalez, W. Zapata, I. Saulle, M. Garziano, M. Biasin, M. Clerici, and M.T. Rugeles. Journal of Acquired Immune Deficiency Syndromes, 2016. 73(5): p. 497-506. PMID[27509245].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27799218">TLR7 Agonist GS-9620 Is a Potent Inhibitor of Acute HIV-1 Infection in Human Peripheral Blood Mononuclear Cells.</a> Bam, R.A., D. Hansen, A. Irrinki, A. Mulato, G.S. Jones, J. Hesselgesser, C.R. Frey, T. Cihlar, and S.R. Yant. Antimicrobial Agents and Chemotherapy, 2017. 61(e01369-16): 14pp. PMID[27799218].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27941949">In Vitro Effects of the Small-molecule Protein Kinase C Agonists on HIV Latency Reactivation.</a> Brogdon, J., W. Ziani, X. Wang, R.S. Veazey, and H. Xu. Scientific Reports, 2016. 6(39032): 8pp. PMID[27941949]. PMCID[PMC5150635].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28002960">Characterization of New Cationic N,N-Dimethyl[70]fulleropyrrolidinium iodide Derivatives as Potent HIV-1 Maturation Inhibitors.</a> Castro, E., Z.S. Martinez, C.S. Seong, A. Cabrera-Espinoza, M. Ruiz, A. Hernandez Garcia, F. Valdez, M. Llano, and L. Echegoyen. Journal of Medicinal Chemistry, 2016. 59(24): p. 10963-10973. PMID[28002960].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27795431">Broadly Neutralizing Antibodies Display Potential for Prevention of HIV-1 Infection of Mucosal Tissue Superior to That of Nonneutralizing Antibodies.</a> Cheeseman, H.M., N.J. Olejniczak, P.M. Rogers, A.B. Evans, D.F. King, P. Ziprin, H.X. Liao, B.F. Haynes, and R.J. Shattock. Journal of Virology, 2017. 91(e01762-16): 16pp. PMID[27795431].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27878297">Hydroxyproline-containing Collagen Peptide Derived from the Skin of the Alaska Pollack Inhibits HIV-1 Infection.</a> Jang, I.S. and S.J. Park. Molecular Medicine Reports, 2016. 14(6): p. 5489-5494. PMID[27878297].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27631437">A Novel Bispecific Peptide HIV-1 Fusion Inhibitor Targeting the N-terminal Heptad Repeat and Fusion Peptide Domains in gp41.</a> Jiang, X., Q. Jia, L. Lu, F. Yu, J. Zheng, W. Shi, L. Cai, S. Jiang, and K. Liu. Amino Acids, 2016. 48(12): p. 2867-2873. PMID[27631437].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>
    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27994756">Design, Conformation, and Crystallography of 2-Naphthyl phenyl ethers as potent anti-HIV Agents.</a> Lee, W.G., A.H. Chan, K.A. Spasov, K.S. Anderson, and W.L. Jorgensen. ACS Medicinal Chemistry Letters, 2016. 7(12): p. 1156-1160. PMID[27994756]. PMCID[PMC5151141].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28008167">Corrigendum: Soybean-derived Bowman-Birk Inhibitor (BBI) Inhibits HIV Replication in Macrophages.</a> Ma, T.C., R.H. Zhou, X. Wang, J.L. Li, M. Sang, L. Zhou, K. Zhuang, W. Hou, D.Y. Guo, and W.Z. Ho. Scientific Reports, 2016. 6(37383): 1pp. PMID[28008167].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000388948900004">Synthesis of and HIV-1 Integrase Inhibition by 2-[7-(Fluorobenzyloxy)-4-oxo-4<i>h</i>chromen-3-yl]-1-hydroxyimidazoles.</a> Nikitina, P.A., Tkach, II, E.S. Knyazhanskaya, M.B. Gottikh, and V.P. Perevalov. Pharmaceutical Chemistry Journal, 2016. 50(8): p. 513-518. ISI[000388948900004].
    <br />
    <b>[WOS]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27978762">Total Syntheses of anti-HIV Cyclodepsipeptides Aetheramides A and B.</a> Qi, N., Z. Wang, S.R. Allu, Q. Liu, J. Guo, and Y. He. The Journal of Organic Chemistry, 2016. 81(24): p. 12466-12471. PMID[27978762].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27714265">Synthesis of HIV-1 Capsid Protein Assembly Inhibitor (CAP-1) and Its Analogues Based on a Biomass Approach.</a> Romashov, L.V. and V.P. Ananikov. Organic &amp; Biomolecular Chemistry, 2016. 14(45): p. 10593-10598. PMID[27714265].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27975981">Anti-HIV Natural Products (28): Preparation of Conjugate for 3-O-Acyl Betulin Derivative and AZT as anti-HIV Agents.</a> Wada, S., N. Tanaka, C.H. Chen, S.L. Morris-Natschke, K.H. Lee, and Y. Kashiwada. Planta Medica, 2016. 81(S 01): p. S1-S381. PMID[27975981].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27795437">A Helical Short-peptide Fusion Inhibitor with Highly Potent Activity against Human Immunodeficiency Virus Type 1 (HIV-1), HIV-2, and Simian Immunodeficiency Virus.</a> Xiong, S., P. Borrego, X. Ding, Y. Zhu, A. Martins, H. Chong, N. Taveira, and Y. He. Journal of Virology, 2017. 91(e01839-16). PMID[27795437].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_12_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27705755">Lindenane Sesquiterpenoid Dimers from Chloranthus japonicus Inhibit HIV-1 and HCV Replication.</a> Yan, H., M.Y. Ba, X.H. Li, J.M. Guo, X.J. Qin, L. He, Z.Q. Zhang, Y. Guo, and H.Y. Liu. Fitoterapia, 2016. 115: p. 64-68. PMID[27705755].
    <br />
    <b>[PubMed]</b>. HIV_12_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">16. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161215&amp;CC=WO&amp;NR=2016197589A1&amp;KC=A1">Preparation of Thienopyrimidine Derivatives and for Treating HIV Infection.</a> Liu, X., D. Kang, P. Zhan, Z. Fang, and Z. Li. Patent. 2016. 2015-CN99924 2016197589: 52pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2016.</p> 

    <br />
    
    <p class="plaintext">17. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161124&amp;CC=US&amp;NR=2016339030A1&amp;KC=A1">Treatment Agents for Inhibiting HIV and Cancer in HIV Infected Patients Using an mTOR Inhibitor.</a> Redfield, R.R., A. Heredia, C.E. Davis, R.B. Gartenhaus, and E.A. Sausville. Patent. 2016. 2016-15159444 20160339030: 36pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2016.</p> 

    <br />   

    <p class="plaintext">18. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161201&amp;CC=US&amp;NR=2016347739A1&amp;KC=A1">5-Oxopyrrolidine Derivatives as HIV Integrase Inhibitors for Treatment of HIV Infection.</a> Wang, X.S. Patent. 2016. 2016-15220049 20160347739: 14pp , Cont-in-part of Appl No PCT/US2015/017810.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2016.</p> 
    
    <br />
    
    <p class="plaintext">19. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161201&amp;CC=WO&amp;NR=2016187788A1&amp;KC=A1">Preparation of Fused Tricyclic Heterocyclic Compds. Useful for the Treatment of HIV Infection.</a> Yu, T., T.H. Graham, S.T. Waddell, J.A. McCauley, A.W. Stamford, J.M. Sanders, L. Hu, J. Cai, and L. Zhao. Patent. 2016. 2015-CN79730 2016187788: 64pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2016.</p> 

    <br />
    
    <p class="plaintext">20. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20161201&amp;CC=WO&amp;NR=2016191239A1&amp;KC=A1">Preparation of Fused Tricyclic Heterocyclic Compounds Useful for Treating HIV Infection.</a> Yu, T., T.H. Graham, S.T. Waddell, J.A. McCauley, A.W. Stamford, J.M. Sanders, L. Hu, J. Cai, and L. Zhao. Patent. 2016. 2016-US33414 2016191239: 89pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_12_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
