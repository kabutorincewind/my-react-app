

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q0TS+uDxhzKyEH/uXEgcGAhnQiLooM5FafAQE3e3Qzq9AkQxP0e4Xwu1lGKjvu40s0Y57IEHDbPnTFdDI1NPEzTe+rybd5Fg2t71nC3QEeDRNQvVox/kiutL0PU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0407FF45" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other viruses Citation List: </p>

    <p class="memofmt2-h1">November 11, 2009 -November 24, 2009</p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19918064?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=29">An adenosine nucleoside inhibitor of dengue virus.</a></p>

    <p class="NoSpacing">Yin Z, Chen YL, Schul W, Wang QY, Gu F, Duraiswamy J, Reddy Kondreddi R, Niyomrattanakit P, Lakshminarayana SB, Goh A, Xu HY, Liu W, Liu B, Lim JY, Ng CY, Qing M, Lim CC, Yip A, Wang G, Chan WL, Tan HP, Lin K, Zhang B, Zou G, Bernard KA, Garrett C, Beltz K, Dong M, Weaver M, He H, Pichota A, Dartois V, Keller TH, Shi PY.</p>

    <p class="NoSpacing">Proc Natl Acad Sci U S A. 2009 Nov 16. [Epub ahead of print]PMID: 19918064 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19904996?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=51">Lamiridosins, Hepatitis C Virus Entry Inhibitors from Lamium album.</a></p>

    <p class="NoSpacing">Zhang H, Rothwangl K, Mesecar AD, Sabahi A, Rong L, Fong HH.</p>

    <p class="NoSpacing">J Nat Prod. 2009 Nov 11. [Epub ahead of print]PMID: 19904996 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19896376?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=60">New small molecule inhibitors of hepatitis C virus.</a></p>

    <p class="NoSpacing">Wei W, Cai C, Kota S, Takahashi V, Ni F, Strosberg AD, Snyder JK.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Dec 15;19(24):6926-30. Epub 2009 Oct 21.PMID: 19896376 [PubMed - in process]</p>

    <p class="memofmt2-2">Hepatitis b virus</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19917760?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">Cellular Pharmacology of the Anti-Hepatitis B Virus Agent ss-L-2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxy-N4-hydroxycytidine: Relevance for the Activation in HepG2 Cells.</a></p>

    <p class="NoSpacing">Matthes E, Bünger H.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Nov 16. [Epub ahead of print]PMID: 19917760 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19889538?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=23">Design, synthesis and evaluation of novel oxazaphosphorine prodrugs of 9-(2-phosphonomethoxyethyl)adenine (PMEA, adefovir) as potent HBV inhibitors.</a></p>

    <p class="NoSpacing">Lu P, Liu J, Wang Y, Chen X, Yang Y, Ji R.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Dec 15;19(24):6918-21. Epub 2009 Oct 21.PMID: 19889538 [PubMed - in process]</p> 

    <p class="memofmt2-2">cytomegalovirus</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19786605?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=30">Susceptibilities of human cytomegalovirus clinical isolates and other herpesviruses to new acetylated, tetrahalogenated benzimidazole d-ribonucleosides.</a></p>

    <p class="NoSpacing">Hwang JS, Schilf R, Drach JC, Townsend LB, Bogner E.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Dec;53(12):5095-101. Epub 2009 Sep 28.PMID: 19786605 [PubMed - in process]</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">7. Chen Ke-Xiano, Xie Hai-Ying, Li Zu-Guang.</p>

    <p class="NoSpacing">2D-QSAR Studies on Anthranilic Acid Derivatives: A Novel Class of Allosteric Inhibitors of Hepatitis C NS5B Polymerase.</p>

    <p class="NoSpacing">CHINESE JOURNAL OF STRUCTURAL CHEMISTRY. 2009. 28(10): 1217-25.</p>

    <p class="ListParagraph">8. Farghaly, Thoraya A., Abdalla, Mohamed M.</p>

    <p class="NoSpacing">Synthesis, tautomerism, and antimicrobial, anti-HCV, anti-SSPE, antioxidant, and antitumor activities of arylazobenzosuberones.</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY. 2009. 17(23):8012-9.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
