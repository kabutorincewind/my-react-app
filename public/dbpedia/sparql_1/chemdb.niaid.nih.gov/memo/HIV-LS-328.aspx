

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-328.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mmIPsnvVllXjkifOAw5j7ZGWiem4unIzC3x4CL2Sx9+6kzyFQXvX4f2W17Ghcyq5skazRgsC2p9pyYEXnaf/7X38TDHv+V6tij5sjaUQisbm0mkOYMNtI/lt8r0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A0F2A92F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-328-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47717   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Human Immunodeficiency Virus (HIV) Reverse Transcriptase Activity Correlates with HIV RNA Load: Implications for Resource-Limited Settings</p>

    <p>          Sivapalasingam, S, Essajee, S, Nyambi, PN, Itri, V, Hanna, B, Holzman, R, and Valentine, F</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(8): 3793-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081912&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081912&amp;dopt=abstract</a> </p><br />

    <p>2.     47718   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Nonnucleoside HIV-1 Reverse-Transcriptase Inhibitors, Part 5. Synthesis and Anti-HIV-1 Activity of Novel 6-Naphthylthio HEPT Analogues</p>

    <p>          Sun, GF, Chen, XX, Chen, FE, Wang, YP, Clercq, ED, Balzarini, J, and Pannecouque, C</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2005</b>.  53(8): 886-92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079514&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16079514&amp;dopt=abstract</a> </p><br />

    <p>3.     47719   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 activity of anti-TAR polyamide nucleic acid conjugated with various membrane transducing peptides</p>

    <p>          Tripathi, S, Chaubey, B, Ganguly, S, Harris, D, Casale, RA, and Pandey, VN</p>

    <p>          Nucleic Acids Res <b>2005</b>.  33(13): 4345-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16077030&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16077030&amp;dopt=abstract</a> </p><br />

    <p>4.     47720   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Molecular modeling, design, synthesis, and biological evaluation of novel 3&#39;,4&#39;-dicamphanoyl-(+)-cis-khellactone (DCK) analogs as potent anti-HIV agents</p>

    <p>          Xie, L, Zhao, CH, Zhou, T, Chen, HF, Fan, BT, Chen, XH, Ma, JZ, Li, JY, Bao, ZY, Lo, Z, Yu, D, and Lee, KH</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16061386&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16061386&amp;dopt=abstract</a> </p><br />

    <p>5.     47721   HIV-LS-328; EMBASE-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 activity of propolis in CD4+ lymphocyte and microglial cell cultures</p>

    <p>          Gekker, Genya, Hu, Shuxian, Spivak, Marla, Lokensgard, James R, and Peterson, Phillip K</p>

    <p>          Journal of Ethnopharmacology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4GNKRNB-2/2/0c184a3f43b8625e12756aaf5846ef9d">http://www.sciencedirect.com/science/article/B6T8D-4GNKRNB-2/2/0c184a3f43b8625e12756aaf5846ef9d</a> </p><br />
    <br clear="all">

    <p>6.     47722   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Combinatorial design of nonsymmetrical cyclic urea inhibitors of aspartic protease of HIV-1</p>

    <p>          Frecer, V, Burello, E, and Miertus, S</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054372&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054372&amp;dopt=abstract</a> </p><br />

    <p>7.     47723   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Design and synthesis of a novel peptidomimetic inhibitor of HIV-1 Tat-TAR interactions: Squaryldiamide as a new potential bioisostere of unsubstituted guanidine</p>

    <p>          Lee, CW, Cao, H, Ichiyama, K, and Rana, TM</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054360&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16054360&amp;dopt=abstract</a> </p><br />

    <p>8.     47724   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Inhibition of HIV strains by GB virus C in cell culture can be mediated by CD4 and CD8 T-lymphocyte derived soluble factors</p>

    <p>          Jung, S, Knauer, O, Donhauser, N, Eichenmuller, M, Helm, M, Fleckenstein, B, and Reil, H</p>

    <p>          AIDS <b>2005</b>.  19 (12): 1267-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16052081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16052081&amp;dopt=abstract</a> </p><br />

    <p>9.     47725   HIV-LS-328; EMBASE-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Bioanalytical method development and validation for a large peptide HIV fusion inhibitor (Enfuvirtide, T-20) and its metabolite in human plasma using LC-MS/MS</p>

    <p>          Chang, D, Kolis, SJ, Linderholm, KH, Julian, TF, Nachi, R, Dzerk, AM, Lin, PP, Lee, JW, and Bansal, SK</p>

    <p>          Journal of Pharmaceutical and Biomedical Analysis <b>2005</b> .  38(3): 487-496</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGX-4FN76NM-1/2/17d2b04ffa2ac0cc9accf926405353ff">http://www.sciencedirect.com/science/article/B6TGX-4FN76NM-1/2/17d2b04ffa2ac0cc9accf926405353ff</a> </p><br />

    <p>10.   47726   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          TAK-220, a novel small-molecule CCR5 antagonist, has favorable anti-human immunodeficiency virus interactions with other antiretrovirals in vitro</p>

    <p>          Tremblay, CL, Giguel, F, Guan, Y, Chou, TC, Takashima, K, and Hirsch, MS</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(8): 3483-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048964&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048964&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   47727   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Anti-human immunodeficiency virus type 1 activity and resistance profile of 2&#39;,3&#39;-didehydro-3&#39;-deoxy-4&#39;-ethynylthymidine in vitro</p>

    <p>          Nitanda, T, Wang, X, Kumamoto, H, Haraguchi, K, Tanaka, H, Cheng, YC, and Baba, M</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(8): 3355-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048947&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16048947&amp;dopt=abstract</a> </p><br />

    <p>12.   47728   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Anti-HIV-1 activities in extracts from some medicinal plants as assessed in an in vitro biochemical HIV-1 reverse transcriptase assay</p>

    <p>          Kanyara, JN and Njagi, EN</p>

    <p>          Phytother Res <b>2005</b>.  19(4): 287-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041768&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041768&amp;dopt=abstract</a> </p><br />

    <p>13.   47729   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          A peptide inhibitor of HIV-1 assembly in vitro</p>

    <p>          Sticht, J, Humbert, M, Findlow, S, Bodem, J, Muller, B, Dietrich, U, Werner, J, and Krausslich, HG</p>

    <p>          Nat Struct Mol Biol <b>2005</b>.  12(8): 671-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041387&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16041387&amp;dopt=abstract</a> </p><br />

    <p>14.   47730   HIV-LS-328; EMBASE-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Effects of HIV Q151M-associated multi-drug resistance mutations on the activities of (-)-[beta]-d-1&#39;,3&#39;-dioxolan guanine</p>

    <p>          Feng, Joy Y, Myrick, Florence, Selmi, Boulbaba, Deval, Jerome, Canard, Bruno, and Borroto-Esoda, Katyna</p>

    <p>          Antiviral Research <b>2005</b>.  66(2-3): 153-158</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4G1R0JD-1/2/1a7a748fbdcef5c3514c1d42289f14c8">http://www.sciencedirect.com/science/article/B6T2H-4G1R0JD-1/2/1a7a748fbdcef5c3514c1d42289f14c8</a> </p><br />

    <p>15.   47731   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Synthesis and assay of isoquinoline derivatives as HIV-1 Tat-TAR interaction inhibitors</p>

    <p>          He, M, Yuan, D, Lin, W, Pang, R, Yu, X, and Yang, M</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(17): 3978-3981</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16039124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16039124&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   47732   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of novel phenyl branched cyclopropyl nucleosides</p>

    <p>          Wu, Y and Hong, JH</p>

    <p>          Farmaco <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16038909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16038909&amp;dopt=abstract</a> </p><br />

    <p>17.   47733   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Rational design of non-nucleoside, potent, and orally bioavailable adenosine deaminase inhibitors: predicting enzyme conformational change and metabolism</p>

    <p>          Terasaka, T, Tsuji, K, Kato, T, Nakanishi, I, Kinoshita, T, Kato, Y, Kuno, M, Inoue, T, Tanaka, K, and Nakamura, K</p>

    <p>          J Med Chem <b>2005</b>.  48(15): 4750-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033254&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16033254&amp;dopt=abstract</a> </p><br />

    <p>18.   47734   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          Anti-HIV activities of natural antioxidant caffeic acid derivatives: toward an antiviral supplementation diet</p>

    <p>          Bailly, F and Cotelle, P</p>

    <p>          Curr Med Chem <b>2005</b>.  12(15): 1811-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16029149&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16029149&amp;dopt=abstract</a> </p><br />

    <p>19.   47735   HIV-LS-328; PUBMED-HIV-8/8/2005</p>

    <p class="memofmt1-2">          2-Pyrone natural products and mimetics: isolation, characterisation and biological activity</p>

    <p>          McGlacken, GP and Fairlamb, IJ</p>

    <p>          Nat Prod Rep <b>2005</b>.  22(3): 369-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16010346&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16010346&amp;dopt=abstract</a> </p><br />

    <p>20.   47736   HIV-LS-328; WOS-HIV-7/31/2005</p>

    <p class="memofmt1-2">          Total synthesis and structural elucidation of natural products: (-)-delactonmycin, (+)-plumerinine, and (-)-parvistemoamide</p>

    <p>          Pilli, RA, Correa, IR, Maldaner, AO, and Rosso, GB</p>

    <p>          PURE AND APPLIED CHEMISTRY <b>2005</b>.  77(7): 1153-1160, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230442600008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230442600008</a> </p><br />
    <br clear="all">

    <p>21.   47737   HIV-LS-328; WOS-HIV-7/31/2005</p>

    <p class="memofmt1-2">          Interaction of HIV-1 reverse transcriptase with new minor groove binders and their conjugates with oligonucleotides</p>

    <p>          Zakharova, OD, Baranova, SV, Ryabinin, VA, Sinyakov, AN, Yarnkovoi, VI, Tarrago-Litvak, L, Litvak, S, and Nevinsky, GA</p>

    <p>          MOLECULAR BIOLOGY <b>2005</b>.  39(3): 421-429, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230337200014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230337200014</a> </p><br />

    <p>22.   47738   HIV-LS-328; WOS-HIV-7/31/2005</p>

    <p class="memofmt1-2">          HIV-1 protease and reverse-transcriptase mutations: Correlations with antiretroviral therapy in subtype B isolates and implications for drug-resistance surveillance</p>

    <p>          Rhee, SY, Fessel, WJ, Zolopa, AR, Hurley, L, Liu, T, Taylor, J, Nguyen, DP, Slome, S, Klein, D, Horberg, M, Flamm, J, Follansbee, S, Schapiro, JM, and Shafer, RW</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  192(3): 456-465, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230387500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230387500013</a> </p><br />

    <p>23.   47739   HIV-LS-328; WOS-HIV-7/31/2005</p>

    <p class="memofmt1-2">          Isolation and characterization of a glucose/mannose/rhamnosespecific lectin from the knife bean Canavalia gladiata</p>

    <p>          Wong, JH and Ng, TB</p>

    <p>          ARCHIVES OF BIOCHEMISTRY AND BIOPHYSICS <b>2005</b>.  439(1): 91-98, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230441300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230441300010</a> </p><br />

    <p>24.   47740   HIV-LS-328; WOS-HIV-8/7/2005</p>

    <p class="memofmt1-2">          Anti-viral activity of human antithrombin III</p>

    <p>          Elmaleh, DR, Brown, NV, and Geiben-Lynn, R</p>

    <p>          INTERNATIONAL JOURNAL OF MOLECULAR MEDICINE <b>2005</b>.  16(2): 191-200, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230553000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230553000001</a> </p><br />

    <p>25.   47741   HIV-LS-328; WOS-HIV-8/7/2005</p>

    <p class="memofmt1-2">          Virtual screening for anti-HIV-1 RT and anti-HIV-1 PR inhibitors from the Thai medicinal plants database: A combined docking with neural networks approach</p>

    <p>          Sangma, C, Chuakheaw, D, Jongkon, N, Saenbandit, K, Nunrium, P, Uthayopas, P, and Hannongbua, S</p>

    <p>          COMBINATORIAL CHEMISTRY &amp; HIGH THROUGHPUT SCREENING <b>2005</b>.  8(5): 417-429, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230612700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230612700005</a> </p><br />

    <p>26.   47742   HIV-LS-328; WOS-HIV-8/7/2005</p>

    <p class="memofmt1-2">          Synthesis and QSAR studies on thiazolidinones as anti-HIV agents</p>

    <p>          Rawal, RK, Solomon, VR, Prabhakar, YS, Katti, SB, and De, Clercq E</p>

    <p>          COMBINATORIAL CHEMISTRY &amp; HIGH THROUGHPUT SCREENING <b>2005</b>.  8(5): 439-443, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230612700007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230612700007</a> </p><br />
    <br clear="all">

    <p>27.   47743   HIV-LS-328; WOS-HIV-8/7/2005</p>

    <p class="memofmt1-2">          Redistribution of human immunodeficiency virus type 1 variants resistant to protease inhibitors after a protease inhibitor-sparing regimen</p>

    <p>          Gianotti, N, Seminari, E, Lazzarin, A, Boeri, E, Clementi, M, Danise, A, Salpietro, S, Fusetti, G, and Castagna, A</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2005</b>.  21(6): 545-554, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230550500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230550500004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
