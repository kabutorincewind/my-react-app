

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-09-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="APXRzM/vn2ZNUrp38F1PcxtNPEs9K9bvR721uc9k/L1rWU24OAgZYgEwx+WETquPTRSb9vdOwq46Jjo/15ZXXy4Gt1TU4ec9uFTkyds/7d3TiAuH3t19+hBc//8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="79A421A5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">September 16, 2009 - September 29, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19635951?ordinalpos=16&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Artemisone and artemiside control acute and reactivated toxoplasmosis in a murine model.</a></p>

    <p class="NoSpacing">Dunay IR, Chan WC, Haynes RK, Sibley LD.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Oct;53(10):4450-6. Epub 2009 Jul 27.</p>

    <p class="NoSpacing">PMID: 19635951 [PubMed - in process]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19736009?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and SAR of alkanediamide-linked bisbenzamidines with anti-trypanosomal and anti-pneumocystis activity.</a></p>

    <p class="NoSpacing">Huang TL, Vanden Eynde JJ, Mayence A, Collins MS, Cushion MT, Rattendi D, Londono I, Mazumder L, Bacchi CJ, Yarlett N.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Oct 15;19(20):5884-6. Epub 2009 Aug 23.</p>

    <p class="NoSpacing">PMID: 19736009 [PubMed - in process]</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19772489?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, antimicrobial and antiviral activity of substituted benzimidazoles.</a></p>

    <p class="NoSpacing">Sharma D, Narasimhan B, Kumar P, Judge V, Narang R, De Clercq E, Balzarini J.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2009 Oct;24(5):1161-8.</p>

    <p class="NoSpacing">PMID: 19772489 [PubMed - in process]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19748782?ordinalpos=34&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of highly potent novel antifungal azoles by structure-based rational design.</a></p>

    <p class="NoSpacing">Wang W, Sheng C, Che X, Ji H, Cao Y, Miao Z, Yao J, Zhang W.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Oct 15;19(20):5965-9. Epub 2009 Aug 3.</p>

    <p class="NoSpacing">PMID: 19748782 [PubMed - in process]</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19770277?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Activity of novel peptide Arminin against multiresistant human pathogens shows considerable potential of phylogenetically ancient organisms as drug source.</a></p>

    <p class="NoSpacing">Augustin R, Anton-Erxleben F, Jungnickel S, Hemmrich G, Spudy B, Podschun R, Bosch TC.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Sep 21. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19770277 [PubMed - as supplied by publisher].</p> 

    <p class="ListParagraph">6. Hearn, M.J., M.H. Cynamon, M.F. Chen, R. Coppins, J. Davis, H. Joo-On Kang, A. Noble, B. Tu-Sekine, M.S. Terrot, D. Trombino, M. Thai, E.R. Webster, and R. Wilson, Preparation and antitubercular activities in vitro and in vivo of novel Schiff bases of isoniazid. Eur J Med Chem, 2009. 44(10): p. 4169-78; PMID[19524330] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19524330?ordinalpos=300&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19524330?ordinalpos=300&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p>  

    <p class="ListParagraph">7. Lin, G., D. Li, L.P. de Carvalho, H. Deng, H. Tao, G. Vogt, K. Wu, J. Schneider, T. Chidawanyika, J.D. Warren, H. Li, and C. Nathan, Inhibitors selective for mycobacterial versus human proteasomes. Nature, 2009. 461(7264): p. 621-6; PMID[19759536] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19759536?ordinalpos=172&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19759536?ordinalpos=172&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">8. Maddry, J.A., S. Ananthan, R.C. Goldman, J.V. Hobrath, D.K. C, C. Maddox, L. Rasmussen, R.C. Reynolds, J.A. Secrist, 3rd, M.I. Sosa, E.L. White, and W. Zhang, Antituberculosis activity of the molecular libraries screening center network library. Tuberculosis (Edinb), 2009; PMID[19783214] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19783214?ordinalpos=72&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19783214?ordinalpos=72&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">9. Molina-Salinas, G.M., J. Borquez, S. Said-Fernandez, L.A. Loyola, A. Yam-Puc, P. Becerril-Montes, F. Escalante-Erosa, and L.M. Pena-Rodriguez, Antituberculosis activity of alkylated mulinane diterpenoids. Fitoterapia, 2009; PMID[19781604] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19781604?ordinalpos=83&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19781604?ordinalpos=83&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">10. Mori, S., K. Shibayama, J.I. Wachino, and Y. Arakawa, Purification and molecular characterization of a novel diadenosine 5&#39;,5&#39;&#39;&#39;-P(1),P(4)-tetraphosphate phosphorylase from Mycobacterium tuberculosis H37Rv. Protein Expr Purif, 2009; PMID[19778616] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19778616?ordinalpos=102&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19778616?ordinalpos=102&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="ListParagraph">11. Raparti, V., T. Chitre, K. Bothara, V. Kumar, S. Dangre, C. Khachane, S. Gore, and B. Deshmane, Novel 4-(morpholin-4-yl)-N&#39;-(arylidene)benzohydrazides: synthesis, antimycobacterial activity and QSAR investigations. Eur J Med Chem, 2009. 44(10): p. 3954-60; PMID[19464085] <a href="http://www.ncbi.nlm.nih.gov/pubmed/19464085?ordinalpos=312&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">http://www.ncbi.nlm.nih.gov/pubmed/19464085?ordinalpos=312&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum</a>.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">12. Lu, SE., et al</p>

    <p class="NoSpacing">Occidiofungin, a Unique Antifungal Glycopeptide Produced by a Strain of Burkholderia contaminans</p>
    
    <p class="NoSpacing">BIOCHEMISTRY  2009 (Sept.) 48: 8312 - 8321</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
