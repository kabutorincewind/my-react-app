

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-02-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Xds4oPy/GUy2Kp6HVHoyhdfK2koFdYVGTL3VJNck9Diz8wwtV5jaUF/dSZYOxFRxRbFp/dqiYzRhh+zyWFw81SBQseez9Zj6iN/6IoGgnfdcEHyBFW8hoAE2ySw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F3364378" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 31 - February 13, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24277040">Antifungal Application of Nonantifungal Drugs.</a>Stylianou, M., E. Kulesskiy, J.P. Lopes, M. Granlund, K. Wennerberg, and C.F. Urban. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 1055-1062. PMID[24277040].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23790588">Antimicrobial Efficacy of a Novel Silver Hydrogel Dressing Compared to Two Common Silver Burn Wound Dressings: Acticoat and Polymem Silver((R)).</a> Boonkaew, B., M. Kempf, R. Kimble, P. Supaphol, and L. Cuttle. Burns, 2014. 40(1): p. 89-96. PMID[23790588].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24196133">Antimicrobial Efficacy and Wound-healing Property of a Topical Ointment Containing Nitric-oxide-loaded Zeolites.</a>Neidrauer, M., U.K. Ercan, A. Bhattacharyya, J. Samuels, J. Sedlak, R. Trikha, K.A. Barbee, M.S. Weingarten, and S.G. Joshi. Journal of Medical Microbiology, 2014. 63(Pt 2): p. 203-209. PMID[24196133].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24411352">Antimicrobial Hyperbranched Poly(Ester amide)/Polyaniline Nanofiber Modified Montmorillonite Nanocomposites.</a>Pramanik, S., P. Bharali, B.K. Konwar, and N. Karak. Materials Science and Engineering C: Materials for Biological Applications, 2014. 35: p. 61-69. PMID[24411352].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24155020">Apotransferrin Has a Second Mechanism for Anticandidal Activity through Binding of Candida albicans.</a> Han, Y. Archives of Pharmacalogical Research, 2014. 37(2): p. 270-275. PMID[24155020]. <b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24215268">Canine Antimicrobial Peptides Are Effective against Resistant Bacteria and Yeasts.</a> Santoro, D. and C.W. Maddox. Veterinary Dermatology, 2014. 25(1): p. 35-e12. PMID[24215268].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23979826">Cyclic Dipeptides from Rhabditid Entomopathogenic Nematode-associated Bacillus cereus have Antimicrobial Activities.</a>Nishanth Kumar, S., V.S. Nath, R. Pratap Chandran, and B. Nambisan. World Journal of Microbiology and Biotechnology, 2014. 30(2): p. 439-449. PMID[23979826].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24247141">Histatin 5-spermidine Conjugates Have Enhanced Fungicidal Activity and Efficacy as a Topical Therapeutic for Oral Candidiasis.</a> Tati, S., R. Li, S. Puri, R. Kumar, P. Davidow, and M. Edgerton. Antimicrobial Agents and Chemotherapy<a name="_GoBack"></a>, 2014. 58(2): p. 756-766. PMID[24247141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24184471">Killing Rates Exerted by Caspofungin in 50% Serum and Its Correlation with in Vivo Efficacy in a Neutropenic Murine Model against Candida krusei and Candida inconspicua.</a> Kovacs, R., R. Gesztelyi, R. Berenyi, M. Doman, G. Kardos, B. Juhasz, and L. Majoros. Journal of Medical Microbiology, 2014. 63(Pt 2): p. 186-194. PMID[24184471].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24436010">The Influence of Chemical Composition of Commercial Lemon Essential Oils on the Growth of Candida Strains.</a> Bialon, M., T. Krzysko-Lupicka, M. Koszalkowska, and P.P. Wieczorek. Mycopathologia, 2014. 177(1-2): p. 29-39. PMID[24436010].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24411369">Synthesis, Characterization and Thermal Behavior of Antibacterial and Antifungal Active Zinc Complexes of bis (3(4-Dimethylaminophenyl)-allylidene-1,2-diaminoethane.</a> Montazerozohori, M., S. Zahedi, A. Naghiha, and M.M. Zohour. Materials Science and Engineering C: Materials for Biological Applications, 2014. 35: p. 195-204. PMID[24411369].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24498363">Synthetic Multivalent Antifungal Peptides Effective against Fungi.</a> Lakshminarayanan, R., S. Liu, J. Li, M. Nandhakumar, T.T. Aung, E. Goh, J.Y. Chang, P. Saraswathi, C. Tang, S.R. Safie, L.Y. Lin, H. Riezman, Z. Lei, C.S. Verma, and R.W. Beuerman. Plos One, 2014. 9(2): p. e87730. PMID[24498363]. <b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24360922">In Vitro Pharmacodynamics and in Vivo Efficacy of Fluconazole, Amphotericin B and Caspofungin in a Murine Infection by Candida lusitaniae.</a> Sandoval-Denis, M., F.J. Pastor, J. Capilla, D.A. Sutton, A.W. Fothergill, and J. Guarro. International Journal of Antimicrobial Agents, 2014. 43(2): p. 161-164. PMID[24360922].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24217700">In Vitro Study of Sequential Fluconazole and Caspofungin Treatment against Candida albicans Biofilms.</a> Sarkar, S., P. Uppuluri, C.G. Pierce, and J.L. Lopez-Ribot. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 1183-1186. PMID[24217700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24247136">Antimalarial Efficacy of Hydroxyethylapoquinine (SN-119) and Its Derivatives.</a> Sanders, N.G., D.J. Meyers, and D.J. Sullivan. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 820-827. PMID[24247136].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24474655">N-Cinnamoylation of Antimalarial Classics: Quinacrine Analogues with Decreased Toxicity and Dual-Stage Activity.</a>Gomes, A., B. Perez, I. Albuquerque, M. Machado, M. Prudencio, F. Nogueira, C. Teixeira, and P. Gomes. ChemMedChem, 2014. 9(2): p. 305-310. PMID[24474655].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24411478">Synthesis, in Vitro Antimalarial Activity and Cytotoxicity of Novel 4-Aminoquinolinyl-chalcone Amides.</a> Smit, F.J. and D. N&#39;Da. Bioorganic &amp; Medicinal Chemistry, 2014. 22(3): p. 1128-1138. PMID[24411478].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24424135">1H-1,2,3-Triazole-tethered Isatin-7-chloroquinoline and 3-Hydroxy-indole-7-chloroquinoline Conjugates: Synthesis and Antimalarial Evaluation.</a> Raj, R., J. Gut, P.J. Rosenthal, and V. Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 756-759. PMID[24424135].</p>

    <p class="plaintext"> <b>[PubMed]</b>. OI_0131-021314.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24290060">Activity of Linezolid-containing Regimens against Multidrug-resistant Tuberculosis in Mice.</a> Zhao, W., Z. Guo, M. Zheng, J. Zhang, B. Wang, P. Li, L. Fu, and S. Liu. International Journal of Antimicrobial Agents, 2014. 43(2): p. 148-153. PMID[24290060].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24314557">Anti-mycobacterial Activities of Synthetic Cationic alpha-Helical Peptides and Their Synergism with Rifampicin.</a>Khara, J.S., Y. Wang, X.Y. Ke, S. Liu, S.M. Newton, P.R. Langford, Y.Y. Yang, and P.L. Ee. Biomaterials, 2014. 35(6): p. 2032-2038. PMID[24314557].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24498115">Bactericidal Activity of an Imidazo[1, 2-a]pyridine Using a Mouse M. tuberculosis Infection Model.</a> Cheng, Y., G.C. Moraski, J. Cramer, M.J. Miller, and J.S. Schorey. Plos One, 2014. 9(1): p. e87483. PMID[24498115].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24424129">The Copper (II) Ion as a Carrier for the Antibiotic Capreomycin against Mycobacterium tuberculosis.</a> Manning, T., R. Mikula, H. Lee, A. Calvin, J. Darrah, G. Wylie, D. Phillips, and B.J. Bythell. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 976-982. PMID[24424129].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23968826">Naphthoquinones Isolated from Diospyros anisandra Exhibit Potent Activity against Pan-resistant First-line Drugs Mycobacterium tuberculosis Strains.</a> Uc-Cachon, A.H., R. Borges-Argaez, S. Said-Fernandez, J. Vargas-Villarreal, F. Gonzalez-Salazar, M. Mendez-Gonzalez, M. Caceres-Farfan, and G.M. Molina-Salinas. Pulmonary Pharmacology &amp; Therapeutics, 2014. 27(1): p. 114-120. PMID[23968826].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24333958">New Finding of an anti-TB Compound in the Genus Marsypopetalum (Annonaceae) from a Traditional Herbal Remedy of Laos.</a>Elkington, B.G., K. Sydara, A. Newsome, C.H. Hwang, D.C. Lankin, C. Simmler, J.G. Napolitano, R. Ree, J.G. Graham, C. Gyllenhaal, S. Bouamanivong, O. Souliya, G.F. Pauli, S.G. Franzblau, and D.D. Soejarto. Journal of Ethnopharmacology, 2014. 151(2): p. 903-911. PMID[24333958].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24505329">Pentacyclic nitrofurans with in Vivo Efficacy and Activity against Nonreplicating Mycobacterium tuberculosis.</a> Rakesh, D.F. Bruhn, M.S. Scherman, L.K. Woolhiser, D.B. Madhura, M.M. Maddox, A.P. Singh, R.B. Lee, J.G. Hurdle, M.R. McNeil, A.J. Lenaerts, B. Meibohm, and R.E. Lee. Plos One, 2014. 9(2): p. e87909. PMID[24505329].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24103309">Synthesis and Antimycobacterial Activity of Novel Thiadiazolylhydrazones of 1-Substitutedindole-3-carboxaldehydes.</a> Haj Mohammad Ebrahim Tehrani, K., V. Mashayekhi, P. Azerang, S. Sardari, F. Kobarfard, and K. Rostamizadeh. Chemical Biology &amp; Drug Design, 2014. 83(2): p. 224-236. PMID[24103309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24405701">Thiazolopyridone ureas as DNA Gyrase B Inhibitors: Optimization of Antitubercular Activity and Efficacy.</a> Kale, R.R., M.G. Kale, D. Waterson, A. Raichurkar, S.P. Hameed, M.R. Manjunatha, B.K. Kishore Reddy, K. Malolanarasimhan, V. Shinde, K. Koushik, L.K. Jena, S. Menasinakai, V. Humnabadkar, P. Madhavapeddi, H. Basavarajappa, S. Sharma, R. Nandishaiah, K.N. Mahesh Kumar, S. Ganguly, V. Ahuja, S. Gaonkar, C.N. Naveen Kumar, D. Ogg, P.A. Boriack-Sjodin, V.K. Sambandamurthy, S.M. de Sousa, and S.R. Ghorpade. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 870-879. PMID[24405701].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24398298">The Benzimidazole Based Drugs Show Good Activity against T. gondii but Poor Activity against Its Proposed Enoyl Reductase Enzyme Target.</a> Wilkinson, C., M.J. McPhillie, Y. Zhou, S. Woods, G.A. Afanador, S. Rawson, F. Khaliq, S.T. Prigge, C.W. Roberts, D.W. Rice, R. McLeod, C.W. Fishwick, and S.P. Muench. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 911-916. PMID[24398298].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24292545">Pyrimethamine-loaded Lipid-core Nanocapsules to Improve Drug Efficacy for the Treatment of Toxoplasmosis.</a> Pissinate, K., E. Dos Santos Martins-Duarte, S.R. Schaffazick, C.P. de Oliveira, R.C. Vommaro, S.S. Guterres, A.R. Pohlmann, and W. de Souza. Parasitology Research, 2014. 113(2): p. 555-564. PMID[24292545].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0131-021314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329607500025">Antimicrobial and Antiquorum-Sensing Studies. Part 2: Synthesis, Antimicrobial, Antiquorum-Sensing and Cytotoxic Activities of New Series of Fused 1,3,4 Thiadiazole and 1,3 Benzothiazole Derivatives.</a> El-Gohary, N.S. and M.I. Shaaban. Medicinal Chemistry Research, 2014. 23(1): p. 287-299. ISI[000329607500025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329607500016">Anti-mycobacterial, Cytotoxic Activities of Knoevenagel and (E)-alpha, beta-unsaturated Esters and Ketones from 2-Chloronicotinaldehydes.</a> Suman, P., R.N. Rao, B.C. Raju, D. Sriram, and P.V. Koushik. Medicinal Chemistry Research, 2014. 23(1): p. 199-206. ISI[000329607500016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329137900017">Base-promoted Expedient Access to Spiroisatins: Synthesis and Antitubercular Evaluation of 1H-1,2,3-Triazole-tethered Spiroisatin-Ferrocene and Isatin-Ferrocene Conjugates.</a> Kumar, K., C. Biot, S. Carrere-Kremer, L. Kremer, Y. Guerardel, P. Roussel, and V. Kumar. Organometallics, 2013. 32(24): p. 7386-7398. ISI[000329137900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329899800004">Compositions and Antifungal Activities of Essential Oils from Agarwood of Aquilaria sinensis (Lour.) Gilg Induced by Lasiodiplodia theobromae (Pat.) Griffon. &amp; Maubl.</a> Zhang, Z., X.M. Han, J.H. Wei, J. Xue, Y. Yang, L. Liang, X.J. Li, Q.M. Guo, Y.H. Xu, and Z.H. Gao. Journal of the Brazilian Chemical Society, 2014. 25(1): p. 20-26. ISI[000329899800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329275200002">Evaluation of Antibacterial, Antifungal and Cytotoxic Effects of Holothuria scabra from the North Coast of the Persian Gulf.</a> Mohammadizadeh, F., M. Ehsanpor, M. Afkhami, A. Mokhlesi, A. Khazaali, and S. Montazeri. Journal de Mycologie Medicale, 2013. 23(4): p. 225-229. ISI[000329275200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329256400042">Inhibitory Effect of Cinnamon Oil on Aflatoxin Produced by Aspergillus flavus Isolated from Shelled Hazelnuts.</a>Al-Othman, M.R., A.R.M. Abd El-Aziz, and M.A. Mahmoud. Journal of Pure and Applied Microbiology, 2013. 7: p. 395-400. ISI[000329256400042].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329268200003">Iodine Catalyzed Three-component Synthesis of beta-Amino-beta-keto-esters and Their Antimicrobial Activity.</a> Kataki, D., P. Bhattacharyya, M. Deka, D.K. Jha, and P. Phukan. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(12): p. 1505-1512. ISI[000329268200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329557400049">Nanosilver against Fungi. Silver Nanoparticles as an Effective Biocidal Factor.</a> Pulit, J., M. Banach, R. Szczyglowska, and M. Bryk. Acta Biochimica Polonica, 2013. 60(4): p. 795-798. ISI[000329557400049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329292600240">New Candidate for Treatment Both Actively Growing and Dormant Mycobacterium tuberculosis.</a>Salina, E.G., M.R. Pasca, S.Y. Ryabova, and V.A. Makarov. Respirology, 2013. 18: p. 78-78. ISI[000329292600240].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329607500022">Synthesis, Characterization and Antimicrobial Studies of a Few Novel Thiazole Derivatives.</a> Praveen, A.S., H.S. Yathirajan, B. Narayana, and B.K. Sarojini. Medicinal Chemistry Research, 2014. 23(1): p. 259-268. ISI[000329607500022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000328808200018">Synthesis, Characterization, Electrochemical Behavior, Thermal Study and Antibacterial/Antifungal Properties of Some New Zinc(II) Coordination Compounds.</a>Montazerozohori, M., S. Yadegari, A. Naghiha, and S. Veyseh. Journal of Industrial and Engineering Chemistry, 2014. 20(1): p. 118-126. ISI[000328808200018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329629300005">Synthesis, Spectral, Thermal, Potentiometric and Antimicrobial Studies of Transition Metal Complexes of Tridentate Ligand.</a> Jadhav, S.M., V.A. Shelke, S.G. Shankarwar, A.S. Munde, and T.K. Chondhekar. Journal of Saudi Chemical Society, 2014. 18(1): p. 27-34. ISI[000329629300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329629300008">Synthesis and Spectroscopic Studies of Biologically Active Tetraazamacrocyclic Complexes of Mn(II), Co(II), Ni(II), Pd(II) and Pt(II).</a> Tyagi, M. and S. Chandra. Journal of Saudi Chemical Society, 2014. 18(1): p. 53-58. ISI[000329629300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000329533300037">In-Vitro Analysis of Antifungal Activity of Epigallocatechin-gallate: Preliminary Study.</a>Guida, A., A. Lucchese, G. Minervini, V. De Gregorio, L. Coretti, E. Grimaldi, D. Minervini, R. Serpico, and G. Donnarumma. European Journal of Inflammation, 2013. 11(3): p. 911-917. ISI[000329533300037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0131-021314.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
