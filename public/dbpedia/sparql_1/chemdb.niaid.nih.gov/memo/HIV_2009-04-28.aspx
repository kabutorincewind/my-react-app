

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-04-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="91SwLy7NL/RqA/Vxwm+OuMKc3T8GbuHlIakpV+/dFMaEQHkqJ/BNPkMQ5uNCKTr/LPI3kb2J/0zTgUWpk8Va5Ty5nSARshw1B/DCy6Arl4xs7LbEM4ztZm21VPs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0B1CFF6D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 15 - April 28, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19401213?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification by high throughput screening of small compounds inhibiting the nucleic acid destabilization activity of the HIV-1 nucleocapsid protein.</a>  Shvadchak V, Sanglier S, Rocle S, Villa P, Haiech J, Hibert M, Van Dorsselaer A, Mély Y, de Rocquigny H.  Biochimie. 2009 Apr 25. [Epub ahead of print]  PMID: 19401213</p> 

    <p class="title">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19388685?ordinalpos=23&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-AIDS Agents. 78. (dagger) Design, Synthesis, Metabolic Stability Assessment, and Antiviral Evaluation of Novel Betulinic Acid Derivatives as Potent Anti-Human Immunodeficiency Virus (HIV) Agents.</a>  Qian K, Yu D, Chen CH, Huang L, Morris-Natschke SL, Nitz TJ, Salzwedel K, Reddick M, Allaway GP, Lee KH.  J Med Chem. 2009 Apr 23. [Epub ahead of print]  PMID: 19388685</p> 

    <p class="title">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19386595?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The lupane-type triterpene 30-oxo-calenduladiol is a CCR5 antagonist with anti-HIV-1 and anti-chemotactic activities.</a> Barroso-Gonzalez J, El Jaber-Vazdekis N, Garcia-Exposito L, Machado JD, Zarate R, Ravelo AG, Estevez-Braun A, Valenzuela-Fernandez A. J Biol Chem. 2009 Apr 22. [Epub ahead of print] PMID: 19386595</p> 

    <p class="title">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19374386?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and Antiviral Activity of 7-Benzyl-4-hydroxy-1,5-naphthyridin-2(1H)-one HIV Integrase Inhibitors.</a> Boros EE, Edwards CE, Foster SA, Fuji M, Fujiwara T, Garvey EP, Golden PL, Hazen RJ, Jeffrey JL, Johns BA, Kawasuji T, Kiyama R, Koble CS, Kurose N, Miller WH, Mote AL, Murai H, Sato A, Thompson JB, Woodward MC, Yoshinaga T. J Med Chem. 2009 Apr 17. [Epub ahead of print] PMID: 19374386</p> 

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19386130?ordinalpos=28&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Comparative study of the persistence of anti-HIV activity of deoxynucleoside HIV reverse transcriptase inhibitors after removal from culture.</a> Paintsil E, Grill SP, Dutschman GE, Cheng YC. AIDS Res Ther. 2009 Apr 22;6(1):5. [Epub ahead of print] PMID: 19386130</p> 

    <p class="title">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19382173?ordinalpos=36&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Study on the Inhibitory Mechanism and Binding Mode of the Hydroxycoumarin Compound NSC158393 to HIV-1 Integrase by Molecular Modeling.</a> Liu M, Cong XJ, Li P, Tan JJ, Chan WZ, Wang CX. Biopolymers. 2009 Apr 20. [Epub ahead of print] PMID: 19382173</p> 

    <p class="title">7.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19371030?ordinalpos=52&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In Silico Drug Screening Approach for the Design of Magic Bullets: A Successful Example with Anti-HIV Fullerene Derivatized Amino Acids.</a> Durdagi S, Supuran CT, Strom TA, Doostdar N, Kumar MK, Barron AR, Mavromoustakos T, Papadopoulos MG. J Chem Inf Model. 2009 Apr 16. [Epub ahead of print] PMID: 19371030</p>  

    <p class="title">8.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19339186?ordinalpos=58&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of dammarane-type triterpene derivatives and their ability to inhibit HIV and HCV proteases.</a> Wei Y, Ma CM, Hattori M. Bioorg Med Chem. 2009 Apr 15;17(8):3003-10. Epub 2009 Mar 14. PMID: 19339186</p> 

    <p class="title">9.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19328002?ordinalpos=59&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of dual inhibitors targeting both HIV-1 capsid and human cyclophilin A to inhibit the assembly and uncoating of the viral capsid.</a> Li J, Tan Z, Tang S, Hewlett I, Pang R, He M, He S, Tian B, Chen K, Yang M. Bioorg Med Chem. 2009 Apr 15;17(8):3177-88. Epub 2009 Mar 3. PMID: 19328002</p> 

    <p class="title">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19297155?ordinalpos=65&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antiviral activities of novel acylhydrazone derivatives targeting HIV-1 capsid protein.</a> Tian B, He M, Tang S, Hewlett I, Tan Z, Li J, Jin Y, Yang M. Bioorg Med Chem Lett. 2009 Apr 15;19(8):2162-7. Epub 2009 Mar 4. PMID: 19297155</p> 

    <p class="memofmt2-2">Patent citations:</p>

    <p class="title">11.  <b>Compounds with HIV-1 integrase inhibitory activity and use thereof as anti-HIV/AIDS therapeutics.</b>  <b>  </b>  Neamati, Nouri; Dayam, Raveendra S.  (University of Southern California, USA).    U.S. Pat. Appl. Publ.  (2009),     109pp.  CODEN: USXXCO  US  2009088420  A1  20090402  Patent  written in English.    Application: US  2008-102619  20080414.  Priority: US  2007-911446  20070412.  AN 2009:395326    CAPLUS     <b> </b></p>

    <p class="title">12.  <b>Preparation of piperidinyl 3,9-diazaspiro[5.5]undecanes as antiviral agents.</b>  <b>  </b>  Gabriel, Stephen Deems; Lin, Xiao-Fa; Makra, Ferenc; Rotstein, David Mark; Yang, Hanbiao.  (F. Hoffmann-La Roche AG, Switz.).    PCT Int. Appl.  (2009),     62pp.  CODEN: PIXXD2  WO  2009037168.  Application: WO  2008-EP62013  20080911.  Priority: US  2007-994426  20070919; US  2008-84724  20080730.  CAN 150:352121    AN 2009:364113    CAPLUS     <b> </b></p>

    <p class="title">13.  <b>Flexible, polyvalent antiviral dendritic conjugates for the treatment of HIV/AIDS and enveloped viral infection.</b>  <b>  </b>  Subramaniam, Sriram; Bennett, Adam.  (Government of the United States of America, As Represented by the Secretary, Department of Health and Human Services, USA).    PCT Int. Appl.  (2009),     49pp.  CODEN: PIXXD2  WO  2009038605  A2  20090326 Application: WO  2008-US6922  20080531.  Priority: US  2007-932464  20070531.  CAN 150:382960    AN 2009:360028    CAPLUS     <b> </b></p>

    <p class="title">14.  <b>Studies on anti-HIV quinolones: New insights on the C-6 position.</b>  <b>  </b>  Massari, Serena; Daelemans, Dirk; Manfroni, Giuseppe; Sabatini, Stefano; Tabarrini, Oriana; Pannecouque, Christophe; Cecchetti, Violetta.    Dipartimento di Chimica e Tecnologia del Farmaco,  Universita degli studi di Perugia,  Perugia,  Italy.    Bioorganic &amp; Medicinal Chemistry  (2009),  17(2),  667-674.  Publisher: Elsevier B.V.,  CODEN: BMECEP  ISSN: 0968-0896.  Journal  written in English.    CAN 150:344134    AN 2009:113896    CAPLUS     <b> </b></p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">15.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264637300032">Amide-containing diketoacids as HIV-1 integrase inhibitors: Synthesis, structure-activity relationship analysis, and biological activity.</a>  Li HC,   Wang C,   Sanchez T,   Tan YM,   Jiang CY,   Neamati N,   Zhao GS.BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(7): 2913-2919.</p>

    <p class="title">16.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264706800015">Synthesis and antiviral activities of novel acylhydrazone derivatives targeting HIV-1 capsid protein.</a>  Tian BH,   He MZ,   Tang SX,   Hewlett I,   Tan ZW,   Li JB,   Jin YX,   Yang M.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(8): 2162-2167. </p>

    <p class="title">17.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264706800037">Tricyclic HIV integrase inhibitors V. SAR studies on the benzyl moiety.</a>  Jin HL,   Metobo S,   Jabri S,   Mish M,   Lansdown R,   Chen XW,   Tsiang M,   Wright M,   Kim CU. IOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(8): 2263-2265. </p>

    <p class="title">18.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264637300016">Synthesis and biological evaluation of N-4-(hetero) arylsulfonylquinoxalinones as HIV-1 reverse transcriptase inhibitors.</a>  Xu BL,  Sun Y,  Guo Y,  Cao YL, Yu T. BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(7): 2767-2774. </p>

    <p class="title">19.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264637300034">Design and synthesis of novel dihydroquinoline-3-carboxylic acids as HIV-1 integrase inhibitors.</a>  Sechi M,   Rizzi G,   Bacchi A,   Carcelli M,   Rogolino D,   Pala N,   Sanchez TW,   Taheri L,   Dayam R,   Neamati N. BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2009; 17(7): 2925-2935. </p>

    <p class="title">20.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264758900058">Synthesis of Some Novel Non-Nucleoside Reverse Transcriptase Inhibitor.</a>  Bapna M, Nema RK. ASIAN JOURNAL OF CHEMISTRY, 2009; 21(2): 1244-1248. </p>

    <p class="title">21.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264835800014">Indolylarylsulfones Bearing Natural and Unnatural Amino Acids.Discovery of Potent Inhibitors of HIV-1 Non-Nucleoside Wild Type and Resistant Mutant Strains Reverse Transcriptase and Coxsackie B4 Virus</a>.  Piscitelli F,   Coluccia A,   Brancale A,   La Regina G,   Sansone A,   Giordano C,   Balzarini J,   Maga G,   Zanoli S,   Samuele A,   Cirilli R,   La Torre F,   Lavecchia A,   Novellino E,   Silvestri R.  JOURNAL OF MEDICINAL CHEMISTRY, 2009; 52(7): 1922-1934. </p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
