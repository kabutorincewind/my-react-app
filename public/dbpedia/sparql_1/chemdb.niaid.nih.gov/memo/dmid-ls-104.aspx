

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-104.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sd06cTT/llJhMKuH8/YAnn38+R6aTPambe+SWfRtyOpW3Wct331oW+7gQ1VT7gS8/iBdD4qfYOzSJtrxFEZTvpndRsRZP4AuakLkwPTpFJr1uYjsbEgfyqvFJc8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9B462C8B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-104-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57926   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential Impact of Antiviral Drug Use during Influenza Pandemic</p>

    <p>          Gani, R</p>

    <p>          Emerg Infect Dis <b>2005</b>.  11(9): 1355-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16229762&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16229762&amp;dopt=abstract</a> </p><br />

    <p>2.     57927   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian flu: isolation of drug-resistant H5N1 virus</p>

    <p>          Le, QM, Kiso, M, Someya, K, Sakai, YT, Nguyen, TH, Nguyen, KH, Pham, ND, Ngyen, HH, Yamada, S, Muramoto, Y, Horimoto, T, Takada, A, Goto, H, Suzuki, T, Suzuki, Y, and Kawaoka, Y</p>

    <p>          Nature <b>2005</b>.  437(7062): 1108</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16228009&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16228009&amp;dopt=abstract</a> </p><br />

    <p>3.     57928   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Study to Investigate the Impact of the Initiation of Highly Active Antiretroviral Therapy on the Hepatitis C Virus Viral Load in Hiv/Hcv-Coinfected Patients</p>

    <p>          Mijch, A. <i>et al.</i></p>

    <p>          Antiviral Therapy <b>2005</b>.  10(2): 277-284</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231962300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231962300010</a> </p><br />

    <p>4.     57929   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dissecting the interferon-induced inhibition of hepatitis C virus replication by using a novel host cell line</p>

    <p>          Windisch, MP, Frese, M, Kaul, A, Trippler, M, Lohmann, V, and Bartenschlager, R</p>

    <p>          J Virol <b>2005</b>.  79(21): 13778-93</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227297&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16227297&amp;dopt=abstract</a> </p><br />

    <p>5.     57930   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Advances in L-Nucleosides as Anti-Hiv and Anti-Hbv Agents</p>

    <p>          Dong, C. and Chang, J.</p>

    <p>          Progress in Chemistry <b>2005</b>.  17(5): 916-923</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232294200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232294200021</a> </p><br />

    <p>6.     57931   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of dendrimer SPL7013, a lead microbicide candidate against herpes simplex viruses</p>

    <p>          Gong, E, Matthews, B, McCarthy, T, Chu, J, Holan, G, Raff, J, and Sacks, S</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16219368&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16219368&amp;dopt=abstract</a> </p><br />

    <p>7.     57932   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Susceptibility of Coxsackievirus B3 Laboratory Strains and Clinical Isolates to the Capsid Function Inhibitor Pleconaril: Antiviral Studies With Virus Chimeras Demonstrate the Crucial Role of Amino Acid 1092 in Treatment</p>

    <p>          Schmidtke, M. <i> et al.</i></p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2005</b>.  56(4): 648-656</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232278300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232278300009</a> </p><br />

    <p>8.     57933   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza: forecast for a pandemic</p>

    <p>          de la Barrera, CA and Reyes-Teran, G</p>

    <p>          Arch Med Res <b>2005</b>.  36(6): 628-636</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216644&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216644&amp;dopt=abstract</a> </p><br />

    <p>9.     57934   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel class of potent influenza virus inhibitors: Polysubstituted acylthiourea and its fused heterocycle derivatives</p>

    <p>          Sun, C, Huang, H, Feng, M, Shi, X, Zhang, X, and Zhou, P</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216505&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216505&amp;dopt=abstract</a> </p><br />

    <p>10.   57935   DMID-LS-104; EMBASE-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, physicochemical properties and antiviral activities of ester prodrugs of ganciclovir</p>

    <p>          Patel, Kunal, Trivedi, Shrija, Luo, Shuanghui, Zhu, Xiaodong, Pal, Dhananjay, Kern, Earl R, and Mitra, Ashim K</p>

    <p>          International Journal of Pharmaceutics <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7W-4HC6KVN-1/2/2d414ff1a5050e3ad9fee0b2206e44e4">http://www.sciencedirect.com/science/article/B6T7W-4HC6KVN-1/2/2d414ff1a5050e3ad9fee0b2206e44e4</a> </p><br />

    <p>11.   57936   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Augmentation of fatality of influenza in mice by inhibition of phagocytosis</p>

    <p>          Watanabe, Y, Hashimoto, Y, Shiratsuchi, A, Takizawa, T, and Nakanishi, Y</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  337(3): 881-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216222&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216222&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   57937   DMID-LS-104; EMBASE-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, antiviral, and antitumor activity of 2-substituted purine methylenecyclopropane analogues of nucleosides</p>

    <p>          Qin, Xinrong, Chen, Xinchao, Wang, Kun, Polin, Lisa, Kern, Earl R, Drach, John C, Gullen, Elizabeth, Cheng, Yung-Chi, and Zemlicka, Jiri</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4HC0PXB-4/2/b7fe48e8c8ea94627bae2d8c107868bc">http://www.sciencedirect.com/science/article/B6TF8-4HC0PXB-4/2/b7fe48e8c8ea94627bae2d8c107868bc</a> </p><br />

    <p>13.   57938   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The synthesis and biological evaluation of lactose-based sialylmimetics as inhibitors of rotaviral infection</p>

    <p>          Liakatos, A, Kiefel, MJ, Fleming, F, Coulson, B, and von Itzstein, M</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214356&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214356&amp;dopt=abstract</a> </p><br />

    <p>14.   57939   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Polymer-bound 6&#39; sialyl-N-acetyllactosamine protects mice infected by influenza virus</p>

    <p>          Gambaryan, AS, Boravleva, EY, Matrosovich, TY, Matrosovich, MN, Klenk, HD, Moiseeva, EV, Tuzikov, AB, Chinarev, AA, Pazynina, GV, and Bovin, NV</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214231&amp;dopt=abstract</a> </p><br />

    <p>15.   57940   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hepatitis B and C virus activities of ring-expanded (&#39;fat&#39;) nucleobase analogues containing the imidazo[4,5-e][1,3]diazepine-4,8-dione ring system</p>

    <p>          Zhang, P, Zhang, N, Korba, BE, and Hosmane, RS</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213713&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213713&amp;dopt=abstract</a> </p><br />

    <p>16.   57941   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Exploration of acyl sulfonamides as carboxylic acid replacements in protease inhibitors of the hepatitis C virus full-length NS3</p>

    <p>          Ronn, R, Sabnis, YA, Gossas, T, Kerblom, E, Helena, Danielson U, Hallberg, A, and Johansson, A </p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213143&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213143&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   57942   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Proline-Based Macrocyclic Inhibitors of the Hepatitis C Virus: Stereoselective Synthesis and Biological Activity</p>

    <p>          Chen, KX, Njoroge, FG, Vibulbhan, B, Prongay, A, Pichardo, J, Madison, V, Buevich, A, and Chan, TM</p>

    <p>          Angew Chem Int Ed Engl <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211639&amp;dopt=abstract</a> </p><br />

    <p>18.   57943   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evolution of H5n1 Avian Influenza Viruses in Asia</p>

    <p>          Aubin, J. <i>et al.</i></p>

    <p>          Emerging Infectious Diseases <b>2005</b>.  11(10): 1515-1521</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232192000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232192000003</a> </p><br />

    <p>19.   57944   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-viral activity of the extracts of a Kenyan medicinal plant Carissa edulis against herpes simplex virus</p>

    <p>          Tolo, FM, Rukunga, GM, Muli, FW, Njagi, EN, Njue, W, Kumon, K, Mungai, GM, Muthaura, CN, Muli, JM, Keter, LK, Oishi, E, and Kofi-Tsekpo, MW</p>

    <p>          J Ethnopharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198524&amp;dopt=abstract</a> </p><br />

    <p>20.   57945   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Drug Therapy - Neuraminidase Inhibitors for Influenza</p>

    <p>          Moscona, A.</p>

    <p>          New England Journal of Medicine <b>2005</b>.  353(13): 1363-1373</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232146200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232146200008</a> </p><br />

    <p>21.   57946   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Current Concepts - Avian Influenza a (H5n1) Infection in Humans</p>

    <p>          Beigel, H. <i>et al.</i></p>

    <p>          New England Journal of Medicine <b>2005</b>.  353(13): 1374-1385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232146200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232146200009</a> </p><br />

    <p>22.   57947   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Toll-Like Receptor 3 Is Induced by and Mediates Antiviral Activity Against Rhinovirus Infection of Human Bronchial Epithelial Cells</p>

    <p>          Hewson, C. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(19): 12273-12279</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231992500014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231992500014</a> </p><br />
    <br clear="all">

    <p>23.   57948   DMID-LS-104; WOS-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Functions of Tyrosine Kinase 2 in the Antiviral Defense Against Murine Cytomegalovirus</p>

    <p>          Strobl, B. <i>et al.</i></p>

    <p>          Journal of Immunology <b>2005</b>.  175(6): 4000-4008</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232010800066">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232010800066</a> </p><br />

    <p>24.   57949   DMID-LS-104; PUBMED-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of respiratory syncytial virus replication target cotranscriptional mRNA guanylylation by viral RNA-dependent RNA polymerase</p>

    <p>          Liuzzi, M, Mason, SW, Cartier, M, Lawetz, C, McCollum, RS, Dansereau, N, Bolger, G, Lapeyre, N, Gaudette, Y, Lagace, L, Massariol, MJ, Do, F, Whitehead, P, Lamarre, L, Scouten, E, Bordeleau, J, Landry, S, Rancourt, J, Fazal, G, and Simoneau, B</p>

    <p>          J Virol <b>2005</b>.  79(20): 13105-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189012&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189012&amp;dopt=abstract</a> </p><br />

    <p>25.   57950   DMID-LS-104; EMBASE-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The 4&#39;,4&#39;-difluoro analog of 5&#39;-noraristeromycin: A new structural prototype for possible antiviral drug development toward orthopoxvirus and cytomegalovirus</p>

    <p>          Roy, Atanu, Schneller, Stewart W, Keith, Kathy A, Hartline, Caroll B, and Kern, Earl R</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(14): 4443-4449</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G7DY0X-2/2/0cc1862b6da480861f34c90f1c09bbc6">http://www.sciencedirect.com/science/article/B6TF8-4G7DY0X-2/2/0cc1862b6da480861f34c90f1c09bbc6</a> </p><br />

    <p>26.   57951   DMID-LS-104; EMBASE-DMID-10/24/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and activity of an octapeptide inhibitor designed for SARS coronavirus main proteinase</p>

    <p>          Gan, Yi-Ru, Huang, He, Huang, Yong-Dong, Rao, Chun-Ming, Zhao, Yang, Liu, Jin-Sheng, Wu, Lei, and Wei, Dong-Qing</p>

    <p>          Peptides <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T0M-4HC6KJW-3/2/ad317778097e6febdb70b3e985062bb8">http://www.sciencedirect.com/science/article/B6T0M-4HC6KJW-3/2/ad317778097e6febdb70b3e985062bb8</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
