

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-340.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JzJVUWdG/Ddtc+ShaFksAFSGIy3HBxNg+eX/2mqa1BGXdB37J6rD+84sfvIZU+8hcwZrHgqw4J81ZOvjwv8/wQAdrIJ+daMDE8ItMdrFLCpEUkPsW9H9+o1WqGg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A4CF4C29" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-340-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48401   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Mouse models for the genetic study of tuberculosis susceptibility</p>

    <p>          Di Pietrantonio, T and Schurr, E</p>

    <p>          Brief Funct Genomic Proteomic <b>2005</b>.  4(3): 277-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420753&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420753&amp;dopt=abstract</a> </p><br />

    <p>2.     48402   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Targeting FtsZ for Antituberculosis Drug Discovery: Noncytotoxic Taxanes as Novel Antituberculosis Agents</p>

    <p>          Huang, Q, Kirikae, F, Kirikae, T, Pepe, A, Amin, A, Respicio, L, Slayden, RA, Tonge, PJ, and Ojima, I</p>

    <p>          J Med Chem <b>2006</b>.  49(2): 463-466</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420032&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420032&amp;dopt=abstract</a> </p><br />

    <p>3.     48403   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Twenty-four hour kinetics of hepatitis C virus and antiviral effect of alpha-interferon</p>

    <p>          Boulestin, A, Kamar, N, Sandres-Saune, K, Legrand-Abravanel, F, Alric, L, Vinel, JP, Rostaing, L, and Izopet, J</p>

    <p>          J Med Virol <b>2006</b>.  78(3): 365-371</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16419107&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16419107&amp;dopt=abstract</a> </p><br />

    <p>4.     48404   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Effect of cell growth on hepatitis C virus (HCV) replication and a mechanism of cell confluence-based inhibition of HCV RNA and protein expression</p>

    <p>          Nelson, HB and Tang, H</p>

    <p>          J Virol <b>2006</b>.  80(3): 1181-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16414995&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16414995&amp;dopt=abstract</a> </p><br />

    <p>5.     48405   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Small-molecule inhibition of siderophore biosynthesis in Mycobacterium tuberculosis and Yersinia pestis</p>

    <p>          Ferreras, JA,  Ryu, JS, Di Lello, F, Tan, DS, and Quadri, LE</p>

    <p>          Nat Chem Biol <b>2005</b>.  1(1): 29-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16407990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16407990&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48406   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Synthesis, in vitro and in vivo antimycobacterial activities of diclofenac acid hydrazones and amides</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Devakaram, Ruth Vandana</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4J2KT9Y-7/2/3eb87cefe86b1239c68810c8ce3f85b9">http://www.sciencedirect.com/science/article/B6TF8-4J2KT9Y-7/2/3eb87cefe86b1239c68810c8ce3f85b9</a> </p><br />

    <p>7.     48407   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Structure of chorismate synthase from Mycobacterium tuberculosis</p>

    <p>          Dias, Marcio VB, Borges, Julio C, Ely, Fernanda, Pereira, Jose H, Canduri, Fernanda, Ramos, Carlos HI, Frazzon, Jeverson, Palma, Mario S, Basso, Luis A, and Santos, Diogenes S</p>

    <p>          Journal of Structural Biology <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WM5-4J2CMV5-2/2/0fb0a6e8243f4fd73bda465bda9d18bd">http://www.sciencedirect.com/science/article/B6WM5-4J2CMV5-2/2/0fb0a6e8243f4fd73bda465bda9d18bd</a> </p><br />

    <p>8.     48408   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of antitubercular activity of imidazo[2,1-b][1,3,4]thiadiazole derivatives</p>

    <p>          Kolavi, Gundurao, Hegde, Vinayak, Khazi, Imtiyaz ahmed, and Gadad, Pramod</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4J021JX-2/2/d3229fc024128c27c824409a858a868c">http://www.sciencedirect.com/science/article/B6TF8-4J021JX-2/2/d3229fc024128c27c824409a858a868c</a> </p><br />

    <p>9.     48409   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Assessment of age-related isoniazid hepatotoxicity during treatment of latent tuberculosis infection</p>

    <p>          Aziz, H, Shubair, M, Debari, VA, Ismail, M, and Khan, MA</p>

    <p>          Curr Med Res Opin <b>2006</b>.  22(1): 217-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16393447&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16393447&amp;dopt=abstract</a> </p><br />

    <p>10.   48410   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p><b>          Green tea polyphenol inhibits Mycobacterium tuberculosis survival within human macrophages</b> </p>

    <p>          Anand, Paras K, Kaul, Deepak, and Sharma, Meera</p>

    <p>          The International Journal of Biochemistry &amp; Cell Biology <b>2006</b>.  38(4): 600-609</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TCH-4HNYKB9-1/2/ff9267c37ac008fe4bc554c4ba0584ef">http://www.sciencedirect.com/science/article/B6TCH-4HNYKB9-1/2/ff9267c37ac008fe4bc554c4ba0584ef</a> </p><br />

    <p>11.   48411   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p class="memofmt1-2">          Nonprimate models of congenital cytomegalovirus (CMV) infection: gaining insight into pathogenesis and prevention of disease in newborns</p>

    <p>          Schleiss, MR</p>

    <p>          ILAR J <b>2006</b>.  47 (1): 65-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16391432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16391432&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   48412   OI-LS-340; PUBMED-OI-1/23/2006</p>

    <p><b>          Rationally designed nucleoside antibiotics that inhibit siderophore biosynthesis of Mycobacterium tuberculosis</b> </p>

    <p>          Somu, RV, Boshoff, H, Qiao, C, Bennett, EM, Barry, CE 3rd, and Aldrich, CC</p>

    <p>          J Med Chem <b>2006</b>.  49(1): 31-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392788&amp;dopt=abstract</a> </p><br />

    <p>13.   48413   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Novel inhibitors of hepatitis C NS3-NS4A serine protease derived from 2-aza-bicyclo[2.2.1]heptane-3-carboxylic acid</p>

    <p>          Venkatraman, Srikanth, Njoroge, FGeorge, Wu, Wanli, Girijavallabhan, Viyyoor, Prongay, Andrew J, Butkiewicz, Nancy, and Pichardo, John</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J2KTBF-3/2/da0b8547c32e3db760a55f36ad27c7b1">http://www.sciencedirect.com/science/article/B6TF9-4J2KTBF-3/2/da0b8547c32e3db760a55f36ad27c7b1</a> </p><br />

    <p>14.   48414   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Is combination antiviral therapy for CMV superior to monotherapy?</p>

    <p>          Drew, WLawrence</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4J2M718-3/2/aa6f241efd544c45097a63fe1406cf0a">http://www.sciencedirect.com/science/article/B6VJV-4J2M718-3/2/aa6f241efd544c45097a63fe1406cf0a</a> </p><br />

    <p>15.   48415   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Identification and analysis of fitness of resistance mutations against the HCV protease inhibitor SCH 503034</p>

    <p>          Tong, Xiao, Chase, Robert, Skelton, Angela, Chen, Tong, Wright-Minogue, Jackie, and Malcolm, Bruce A</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4J1JFFY-1/2/36f05a3cc9605ac85f561328483e6540">http://www.sciencedirect.com/science/article/B6T2H-4J1JFFY-1/2/36f05a3cc9605ac85f561328483e6540</a> </p><br />

    <p>16.   48416   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Association of rpoB mutations with rifampicin resistance in Mycobacterium avium</p>

    <p>          Obata, Saiko, Zwolska, Zofia, Toyota, Emiko, Kudo, Koichiro, Nakamura, Akio, Sawai, Tetsuo, Kuratsuji, Tadatoshi, and Kirikae, Teruo</p>

    <p>          International Journal of Antimicrobial Agents <b>2006</b>.  27(1): 32-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4HVDYPD-1/2/d837c18e4aa8f90ff51e5d2982ce589b">http://www.sciencedirect.com/science/article/B6T7H-4HVDYPD-1/2/d837c18e4aa8f90ff51e5d2982ce589b</a> </p><br />

    <p>17.   48417   OI-LS-340; WOS-OI-1/15/2006</p>

    <p class="memofmt1-2">          The antimycobacterial constituents of dill (Anethum graveolens)</p>

    <p>          Stavri, M and Gibbons, S</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2005</b>.  19(11): 938-941, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234165200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234165200005</a> </p><br />

    <p>18.   48418   OI-LS-340; WOS-OI-1/15/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activities of some 1-[(N,N-disubstitutedthiocarbamoylthio)acetyl] 3,5-diaryl-2-pyrazolines</p>

    <p>          Turan-Zitouni, G, Ozdemir, A, Kaplancikli, ZA, Chevallet, P, and Tunali, Y</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2005</b>.  180(12): 2717-2724, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234212900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234212900011</a> </p><br />

    <p>19.   48419   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro antitubercular activity of some 1-[(4-sub)phenyl]-3-(4-{1-[(pyridine-4-carbonyl)hydrazono]ethyl}phenyl)thiourea</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Madhu, Kasinathan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(4): 876-878</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HM82H7-4/2/cda366e18f1e961b4dbeb1f5876097b6">http://www.sciencedirect.com/science/article/B6TF9-4HM82H7-4/2/cda366e18f1e961b4dbeb1f5876097b6</a> </p><br />

    <p>20.   48420   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Synthesis and biological activity of 4-thiazolidinones, thiosemicarbazides derived from diflunisal hydrazide</p>

    <p>          Kucukguzel, Guniz, Kocatepe, Ayla, De Clercq, Erik, Sahin, Fikrettin, and Gulluce, Medine</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4J2M42Y-2/2/d4a67a4f3dda5816b73f2677a13c3a1a">http://www.sciencedirect.com/science/article/B6VKY-4J2M42Y-2/2/d4a67a4f3dda5816b73f2677a13c3a1a</a> </p><br />

    <p>21.   48421   OI-LS-340; WOS-OI-1/15/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral evaluation of some polyhalogenated indole C-nucleosides</p>

    <p>          Chen, JJ, Wei, YA, Williams, JD, Drach, JC, and Townsend, LB</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(10-12): 1417-1437, 21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900003</a> </p><br />

    <p>22.   48422   OI-LS-340; WOS-OI-1/15/2006</p>

    <p class="memofmt1-2">          Synthesis of 9-[1-(substituted)-3-(phosphonomethoxy)propyl]adenine derivatives as possible antiviral agents</p>

    <p>          Wu, MW, El-Kattan, Y, Lin, TH, Ghosh, A, Vadlakonda, S, Kotian, PL, Babu, YS, and Chand, P</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(10-12): 1543-1568, 26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900011</a> </p><br />

    <p>23.   48423   OI-LS-340; EMBASE-OI-1/24/2006</p>

    <p class="memofmt1-2">          Divergent activities of interferon-alpha subtypes against intracellular hepatitis C virus replication</p>

    <p>          Koyama, Tomoyuki, Sakamoto, Naoya, Tanabe, Yoko, Nakagawa, Mina, Itsui, Yasuhiro, Takeda, Yoshie, Kakinuma, Sei, Sekine, Yuko, Maekawa, Shinya, and Yanai, Yoshiaki</p>

    <p>          Hepatology Research <b>2006</b>.  34(1): 41-49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T74-4HVF148-3/2/fe0b3c73626a700922782912a6a4966c">http://www.sciencedirect.com/science/article/B6T74-4HVF148-3/2/fe0b3c73626a700922782912a6a4966c</a> </p><br />
    <br clear="all">

    <p>24.   48424   OI-LS-340; WOS-OI-1/15/2006</p>

    <p class="memofmt1-2">          Biological standardization of human interferon beta: Establishment of a replacement world health organization international biological standard for human glycosylated interferon beta</p>

    <p>          Meager, A and Das, RG</p>

    <p>          JOURNAL OF IMMUNOLOGICAL METHODS <b>2005</b>.  306(1-2): 1-15, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234174500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234174500001</a> </p><br />

    <p>25.   48425   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Unique GMP-Binding site in Mycobacterium tuberculosis guanosine monophosphate kinase</p>

    <p>          Hible, G, Christova, P, Renault, L, Seclaman, E, Thompson, A, Girard, E, Munier-Lehmann, H, and Cherfils, J</p>

    <p>          PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS <b>2006</b>.  62(2 ): 489-500, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234438800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234438800018</a> </p><br />

    <p>26.   48426   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis NAD(+)-dependent DNA ligase is selectively inhibited by glycosylamines compared with human DNA ligase I</p>

    <p>          Srivastava, SK, Dube, D, Tewari, N, Dwivedi, N, Tripathi, RP, and Ramachandran, R</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2005</b>.  33(22): 7090-7101, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234436200024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234436200024</a> </p><br />

    <p>27.   48427   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Antitumor and anti-Pneumocystis carinii activities of novel bisbenzamidines</p>

    <p>          Vanden, Eynde JJ, Mayence, A, Johnson, MT, Huang, TL, Collins, MS, Walzer, PD, Cushion, MT, and Donkor, IO</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH <b>2005</b>.  14(3): 143-157, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234386000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234386000002</a> </p><br />

    <p>28.   48428   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Production of infectious human cytomegalovirus virions is inhibited by drugs that disrupt calcium homeostasis in the endoplasmic reticulum</p>

    <p>          Isler, JA, Maguire, TG, and Alwine, JC</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(24): 15388-15397, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234276700040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234276700040</a> </p><br />

    <p>29.   48429   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Antimycobacterial scalarane-based sesterterpenes from the red sea sponge Hyrtios erecta</p>

    <p>          Youssef, DTA, Shaala, LA, and Emara, S</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2005</b>.  68(12): 1782-1784, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234358000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234358000016</a> </p><br />

    <p>30.   48430   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of cyclic secondary amine substituted phenyl and benzyl nitrofuranyl amides as novel antituberculosis agents</p>

    <p>          Tangallapally, RP, Yendapally, R, Lee, RE, Lenaerts, AJM, and Lee, RE</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(26): 8261-8269, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234301800019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234301800019</a> </p><br />

    <p>31.   48431   OI-LS-340; WOS-OI-1/22/2006</p>

    <p class="memofmt1-2">          Rv2131c gene product: An unconventional enzyme that is both inositol monophosphatase and fructose-1,6-bisphosphatase</p>

    <p>          Gu, XL, Chen, M, Shen, HB, Jiang, X, Huang, Y, and Wang, HH</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2006</b>.  339(3): 897-904, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234439200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234439200026</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
