

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-07-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/Y4UkKLV7uvoOFhWeiDEnzStBNtVe1T4yJvtVG7jBEYY0Yr7GWbOd7QsHmH4IJ//ytg9mBMxazOb9MV2hU6kVJ+3CdsZfUPeGrAbv1HVN4bWaePz3noyvc9PTbk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CBE4A595" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 5 - July 18, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23747224">Inhibition of Candida albicans Isocitrate Lyase Activity by Cadiolides and Synoilides from the Ascidian Synoicum Sp.</a> Ahn, C.H., T.H. Won, H. Kim, J. Shin, and K.B. Oh. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(14): p. 4099-4101. PMID[23747224].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856977">Convenient Syntheses of Cyanuric Chloride-derived NHC Ligands, Their Ag(I) and Au(I) Complexes and Antimicrobial Activity.</a> Almalioti, F., J. Macdougall, S. Hughes, M.M. Hasson, R.L. Jenkins, B.D. Ward, G.J. Tizzard, S.J. Coles, D.W. Williams, S. Bamford, I.A. Fallis, and A. Dervisi. Dalton Transactions, 2013. <b>[Epub ahead of print]</b>. PMID[23856977].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23829770">Medicinal Potential of Morella Serata (Lam.) Killick (Myricaceae) Root Extracts: Biological and Pharmacological Activities.</a> Ashafa, A.O. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 163. PMID[23829770].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856768">Amphotericin B+ and Voriconazole+Echinocandin Combinations against Aspergillus Spp.: Effect of Serum on Inhibitory and Fungicidal Interactions.</a> Elefanti, A., J.W. Mouton, P.E. Verweij, A. Takris, L. Zerva, and J. Meletiadis. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23856768].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23821127">Microbicidal and Anti-inflammatory Effects of Actinomadura spadix (EHA-2) Active Metabolites from Himalayan Soils, India.</a> Islam, V.I., S. Saravanan, and S. Ignacimuthu. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23821127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23769641">Synthesis and Antifungal Activities of Cyclic Octa-lipopeptide Burkholdine Analogues.</a> Konno, H., Y. Otsuki, K. Matsuzaki, and K. Nosaka. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(14): p. 4244-4247. PMID[23769641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23826891">Antifungal Activity of 3-(Heteroaryl-2-ylmethyl)thiazolidinone Derivatives.</a> Marques, G.H., A. Kunzler, V.D. Bareno, B.B. Drawanz, H.G. Mastelloto, F.R. Leite, G.G. Nascimento, P.S. Nascente, G.M. Siqueira, and W. Cunico. Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23826891].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23831437">Aug-MIA-QSAR Modeling of Antimicrobial Activities and Design of Multi-target Anilide Derivatives.</a> Nunes, C.A. and M.P. Freitas. Journal of Microbiological Methods, 2013. <b>[Epub ahead of print]</b>. PMID[23831437].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />    

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23665162">Antimicrobial and Acetylcholinesterase Inhibitory Activities of Buddleja salviifolia (L.) Lam. Leaf Extracts and Isolated Compounds.</a> Pendota, S.C., M.A. Aderogba, A.R. Ndhlala, and J. Van Staden. Journal of Ethnopharmacology, 2013. 148(2): p. 515-520. PMID[23665162].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23829334">Hippolachnin a, a New Antifungal Polyketide from the South China Sea Sponge Hippospongia lachne.</a> Piao, S.J., Y.L. Song, W.H. Jiao, F. Yang, X.F. Liu, W.S. Chen, B.N. Han, and H.W. Lin. Organic Letters, 2013. <b>[Epub ahead of print]</b>. PMID[23829334].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23828601">Effects of Sugar and Amino acid Supplementation on Aureobasidium pullulans NRRL 58536 Antifungal Activity against Four Aspergillus Species.</a> Prasongsuk, S., S. Ployngam, S. Wacharasindhu, P. Lotrakul, and H. Punnapayak. Applied Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23828601].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23842440">Assessment of Phytochemicals, Antimicrobial and Cytotoxic Activities of Extract and Fractions from Fagonia olivieri (Zygophyllaceae).</a> Rashid, U., M.R. Khan, S. Jan, J. Bokhari, and N.A. Shah. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 167. PMID[23842440].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23824511">Antifungal Susceptibility, Exoenzyme Production and Cytotoxicity of Novel Oximes against Candida.</a> Souza, J.L., F. Nedel, M. Ritter, P.H. Carvalho, C.M. Pereira, and R.G. Lund. Mycopathologia, 2013. <b>[Epub ahead of print]</b>. PMID[23824511].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23839114">Anti-candida Targets and Cytotoxicity of Casuarinin Isolated from Plinia cauliflora Leaves in a Bioactivity-guided Study.</a> Souza-Moreira, T.M., J.A. Severi, K. Lee, K. Preechasuth, E. Santos, N.A. Gow, C.A. Munro, W. Vilegas, and R.C. Pietro. Molecules, 2013. 18(7): p. 8095-8108. PMID[23839114].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23841542">Copper(I) (Pseudo)Halide Complexes with Neocuproine and Aminomethylphosphines Derived from Morpholine and Thiomorpholine - in Vitro Cytotoxic and Antimicrobial Activity and the Interactions with DNA and Serum Albumins.</a> Starosta, R., A. Bykowska, A. Kyziol, M. Plotek, M. Florek, J. Krol, and M. Jezowska-Bojczuk. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23841542].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23841574">Tricyclic Sesquiterpenes from Vetiveria zizanoides (L.) Nash as Antimycobacterial Agents.</a> Dwivedi, G.R., S. Gupta, S. Roy, K. Kalani, A. Pal, J.P. Thakur, D. Saikia, A. Sharma, N.S. Darmwal, M.P. Darokar, and S.K. Srivastava. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23841574].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23837573">Synthesis and Structure-Activity Relationship of 8-Substituted Protoberberine Derivatives as a Novel Class of Antitubercular Agents.</a> Li, Y.H., H.G. Fu, F. Su, L.M. Gao, S. Tang, C.W. Bi, Y.H. Li, Y.X. Wang, and D.Q. Song. Chemistry Central Journal, 2013. 7(1): p. 117. PMID[23837573].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856770">Pharmacokinetic and in Vivo Efficacy Studies of the Mycobactin Biosynthesis Inhibitor Salicyl-AMS in Mice.</a> Lun, S., H. Guo, J. Adamson, J.S. Cisar, T.D. Davis, S.S. Chavadi, J.D. Warren, L.E. Quadri, D.S. Tan, and W.R. Bishai. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23856770].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23837928">Antibacterial Activity of Head-to-head bis-Benzimidazoles.</a> Moreira, J.B., J. Mann, S. Neidle, T.D. McHugh, and P.W. Taylor. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23837928].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23837924">In Vitro Susceptibility of Mycobacterium avium Complex Mycobacteria to Trimethoprim and Sulfonamides.</a> Muhammed Ameen, S. and M. Drancourt. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23837924].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836539">Discovery of an Acyclic Nucleoside Phosphonate That Inhibits Mycobacterium tuberculosis ThyX Based on the Binding Mode of a 5-Alkynyl Substrate Analogue.</a> Parchina, A., M. Froeyen, L. Margamuljana, J. Rozenski, S. De Jonghe, Y. Briers, R. Lavigne, P. Herdewijn, and E. Lescrinier. Chemmedchem, 2013. <b>[Epub ahead of print]</b>. PMID[23836539].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23856773">Improved Efficacy of Fosmidomycin against Plasmodium and Mycobacterium Species by Combination with the Cell Penetrating Peptide Octaarginine.</a> Sparr, C., N. Purkayastha, B. Kolesinska, M. Gengenbacher, B. Amulic, K. Matuschewski, D. Seebach, and F. Kamena. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23856773].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23853120">Synthesis, Characterization and Antiplasmodial Activity of Polymer Incorporated Aminoquinolines.</a> Aderibigbe, B.A., E.W. Neuse, E.R. Sadiku, S.S. Ray, and P.J. Smith. Journal of Biomedical Materials Research. Part A, 2013. <b>[Epub ahead of print]</b>. PMID[23853120].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23735832">Exploring DOXP-reductoisomerase Binding Limits Using Phosphonated N-aryl and N-heteroarylcarboxamides as DXR Inhibitors.</a> Bodill, T., A.C. Conibear, M.K. Mutorwa, J.L. Goble, G.L. Blatch, K.A. Lobb, R. Klein, and P.T. Kaye. Bioorganic &amp; Medicinal Chemistry, 2013. 21(14): p. 4332-4341. PMID[23735832].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br />     

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23836641">Torins Are Potent Antimalarials That Block Replenishment of Plasmodium Liver Stage Parasitophorous Vacuole Membrane Proteins.</a> Hanson, K.K., A.S. Ressurreicao, K. Buchholz, M. Prudencio, J.D. Herman-Ornelas, M. Rebelo, W.L. Beatty, D.F. Wirth, T. Hanscheid, R. Moreira, M. Marti, and M.M. Mota. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[23836641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23819803">DXR Inhibition by Potent Mono- and Disubstituted Fosmidomycin Analogues.</a> Jansson, A.M., A. Wieckowska, C. Bjorkelid, S. Yahiaoui, S. Sooriyaarachchi, M. Lindh, T. Bergfors, S. Dharavath, M. Desroses, S. Suresh, M. Andaloussi, R. Nikhil, S. Sreevalli, B.R. Srinivasa, M. Larhed, T.A. Jones, A. Karlen, and S.L. Mowbray. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23819803].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23853126">An Endoperoxide-based Hybrid Approach to Deliver Falcipain Inhibitors inside Malaria Parasites.</a> Oliveira, R., A.S. Newton, R.C. Guedes, D. Miranda, R.K. Amewu, A. Srivastava, J. Gut, P.J. Rosenthal, P.M. O&#39;Neill, S.A. Ward, F. Lopes, and R. Moreira. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23853126].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21265542">A Chemotype That Inhibits Three Unrelated Pathogenic Targets: The Botulinum Neurotoxin Serotype A Light Chain, P. falciparum Malaria, and the Ebola Filovirus.</a> Opsenica, I., J.C. Burnett, R. Gussio, D. Opsenica, N. Todorovic, C.A. Lanteri, R.J. Sciotti, M. Gettayacamin, N. Basilico, D. Taramelli, J.E. Nuss, L. Wanner, R.G. Panchal, B.A. Solaja, and S. Bavari. Journal of Medicinal Chemistry, 2011. 54(5): p. 1157-1169. PMID[21265542].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23815186">4-Amino-7-chloroquinolines: Probing Ligand Efficiency Provides Botulinum Neurotoxin Serotype A Light Chain Inhibitors with Significant Antiprotozoal Activity.</a> Opsenica, I.M., M. Tot, L. Gomba, J.E. Nuss, R.J. Sciotti, S. Bavari, J.C. Burnett, and B.A. Solaja. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23815186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23841934">Biological Evaluation of Hydroxynaphthoquinones as Anti-malarials.</a> Schuck, D.C., S.B. Ferreira, L.N. Cruz, D.R. da Rocha, M. Moraes, M. Nakabashi, P.J. Rosenthal, V.F. Ferreira, and C.R. Garcia. Malaria Journal, 2013. 12(1): p. 234. PMID[23841934].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23837878">Novel Conjugated Quinoline-indoles Compromise Plasmodium falciparum Mitochondrial Function and Show Promising Antimalarial Activity.</a> Teguh, S.C., N. Klonis, S. Duffy, L. Lucantoni, V.M. Avery, C.A. Hutton, J.B. Baell, and L. Tilley. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23837878].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0705-071813.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319803400056">DNA Interaction, Antibacterial and Antifungal Studies of 3-Nitrophenylferrocene.</a> Ali, S., A.A. Altaf, A. Badshah, Imtiaz-Ud-Din, B. Lal, S. Kamal, and S. Ullah. Journal of the Chemical Society of Pakistan, 2013. 35(3): p. 922-928. ISI[000319803400056].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319521900021">Meridianin G and Its Analogs as Antimalarial Agents.</a> Bharate, S.B., R.R. Yadav, S.I. Khan, B.L. Tekwani, M.R. Jacob, I.A. Khan, and R.A. Vishwakarma. MedChemComm, 2013. 4(6): p. 1042-1048. ISI[000319521900021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319803400049">Synthesis of Acyl Analogues of Coniferyl alcohol and Their Antimycobacterial Activity.</a> Bhatti, H.A., N. Uddin, S. Begum, and B.S. Siddiqui. Journal of the Chemical Society of Pakistan, 2013. 35(3): p. 886-889. ISI[000319803400049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319446900062">Anti-infective Potential of Marine Invertebrates and Seaweeds from the Brazilian Coast.</a> Bianco, E.M., S.Q. de Oliveira, C. Rigotto, M.L. Tonini, T.D. Guimaraes, F. Bittencourt, L.P. Gouvea, C. Aresi, M.T.R. de Almeida, M.I.G. Moritz, C.D.L. Martins, F. Scherner, J.L. Carraro, P.A. Horta, F.H. Reginatto, M. Steindel, C.M.O. Simoes, and E.P. Schenkel. Molecules, 2013. 18(5): p. 5761-5778. ISI[000319446900062].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319609700001">The in Vitro Antimicrobial Activity of Lavandula angustifolia Essential Oil in Combination with Other Aroma-therapeutic Oils.</a> de Rapper, S., G. Kamatou, A. Viljoen, and S. van Vuuren. Evidence-Based Complementary and Alternative Medicine, 2013. <b>[Epub ahead of print]</b>. ISI[000319609700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319558500003">Chemical Composition and Antimicrobial Activity of the Essential Oils from the Aerial Parts of Astragalus hamzaoglui Extracted by Hydrodistillation and Microwave Distillation.</a> Iskender, N.Y., N. Kahriman, G. Tosun, S. Terzioglu, S.A. Karaoglu, and N. Yayli. Records of Natural Products, 2013. 7(3): p. 177-183. ISI[000319558500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319558500011">Sesquiterpene Lactones of Amphoricarpos autariatus ssp autariatus from Montenegro - Antifungal Leaf - Surface Constituents.</a> Jadranin, M., I. Dordevic, V. Tesevic, V. Vajs, N. Menkovic, M. Sokovic, J. Glamoclija, and S. Milosavljevic. Records of Natural Products, 2013. 7(3): p. 234-238. ISI[000319558500011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319225200002">In Vitro Anticandidal Evaluation of Novel Highly Functionalized Bis cyclohexenone ethyl carboxylates.</a> Kanagarajan, V., M.R. Ezhilarasi, D. Bhakiaraj, and M. Gopalakrishnan. European Review for Medical and Pharmacological Sciences, 2013. 17(3): p. 292-298. ISI[000319225200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319555200002">Antimicrobial Activity and Essential Oil Composition of Five Sideritis Taxa of Empedoclia and Hesiodia Sect. From Greece.</a> Koutsaviti, A., I. Bazos, M. Milenkovic, M. Pavlovic-Drobac, and O. Tzakou. Records of Natural Products, 2013. 7(1): p. 6-14. ISI[000319555200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319408900012">Synthesis, Characterisation and Antimicrobial Studies of Transition Metal Complexes of 2-Hydroxy-5-methoxybenzaldehydeisonicotinoylhydrazone.</a> Prasanna, M.K. and K.P. Kumar. Research Journal of Chemistry and Environment, 2013. 17(6): p. 61-67. ISI[000319408900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319077300027">Antifungal Activity of Fused Mannich Ketones Triggers an Oxidative Stress Response and Is Cap1-Dependent in Candida albicans.</a> Rossignol, T., B. Kocsis, O. Bouquet, I. Kustos, F. Kilar, A. Nyul, P.B. Jakus, K. Rajbhandari, L. Prokai, C. d&#39;Enfert, and T. Lorand. Plos One, 2013. 8(4): p. e62142. ISI[000319077300027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000319673300009">Evaluation of the Pathogenesis and Treatment of Mycobacterium marinum Infection in Zebrafish.</a> Takaki, K., J.M. Davis, K. Winglee, and L. Ramakrishnan. Nature Protocols, 2013. 8(6): p. 1114-1124. ISI[000319673300009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0705-071813.</p><br />

    <p class="plaintext">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320030800010">Synthesis and Different Biological Activities of Novel Benzoxazoles.</a> Temiz-Arpaci, O., B.E.C. Goztepe, F. Kaynak-Onurdag, S. Ozgen, F.S. Senol, and I.E. Orhan. Acta Biologica Hungarica, 2013. 64(2): p. 249-261. ISI[000320030800010].</p>

    <p class="plaintext"> <b>[WOS]</b>. OI_0705-071813.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
