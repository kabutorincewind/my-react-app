

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-385.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kO7mGNIO7Y/QIbt4/iJpcJmowMazrQsdJwR5fSlY+jf7X6g5ktURQc0AMhYKshI8O+O/LNmnKUWAkChOysfTfcqKXzp8sfkRR3N67ppWYk147dCZhYNwGHTtufM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9CE38E42" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-385-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.            HIV-LS-385; WOS-HIV-11/05/2007</p>

    <p class="memofmt1-2">          Indolyl Aryl Sulfones as Hiv-1 Non-Nucleoside Reverse Transcriptase Inhibitors: Role of Two Halogen Atoms at the Indole Ring in Developing New Analogues with Improved Antiviral Activity</p>

    <p>          LaRegina, G. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2007</b>.  50(20): 5034-5038</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249871300022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249871300022</a></span></p>

    <p>2.            HIV-LS-385; EMBASE-HIV-11/05/2007</p>

    <p class="memofmt1-2">          Improved and rapid synthesis of new coumarinyl chalcone derivatives and their antiviral activity</p>

    <p>          Trivedi, Jalpa C, Bariwal, Jitender B, Upadhyay, Kuldip D, Naliapara, Yogesh T, Joshi, Sudhir K, Pannecouque, Christophe C, De Clercq, Erik, and Shah, Anamik K</p>

    <p>          Tetrahedron Letters <b>2007</b>.  48(48): 8472-8474                   </p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6THS-4PT0Y3Y-8/2/eb017fe515863a82bfe241559df6f288</span></p>

    <p>  3.             HIV-LS-385; EMBASE-HIV-11/05/2007</p>

    <p class="memofmt1-2">          The synthesis and biological activity of novel spiro-isoxazoline C-disaccharides based on 1,3-dipolar cycloaddition of exo-glycals and sugar nitrile oxides</p>

    <p>          Zhang, Ping-Zhu, Li, Xiao-Liu, Chen, Hua, Li, Ya-Nan, and Wang, Rui                                 </p>

    <p>            Tetrahedron Letters <b>2007</b>.  48(44): 7813-7816</p>

    <p>          <span class="memofmt1-3"><a href="http://www.sciencedirect.com/science/article/B6THS-4PKPGP3-B/2/375f9ce9a7faaaf9d6970ed69d93f7bb">http://www.sciencedirect.com/science/article/B6THS-4PKPGP3-B/2/375f9ce9a7faaaf9d6970ed69d93f7bb</a></span></p>

    <p class="memofmt1-1"> </p>

    <p>4.            HIV-LS-385; WOS-HIV-11/05/2007</p>

    <p class="memofmt1-2">          Characterization of the Inhibition of Enveloped Virus Infectivity by the Cationic Acrylate Polymer Eudragit E100</p>

    <p>          Alasino, R. <i>et al.</i></p>

    <p>          Macromolecular Bioscience <b>2007</b>.  7(9-10): 1132-1138              </p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249643700005</span></p>

    <p>5.            HIV-LS-385; WOS-HIV-11/05/2007</p>

    <p class="memofmt1-2">          N- and S-Alpha-L-Arabinopyranosyl[1,2,4]Triazolo[3,4-B][1,3,4]Thiadiazoles. First Synthesis and Biological Evaluation</p>

    <p>          KhalilNsam</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(9): 1193-1199               </p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249540900005</span></p>

    <p>6.            HIV-LS-385; EMBASE-HIV-11/14/07</p>

    <p class="memofmt1-2">          8-Hydroxy-3,4-dihydropyrrolo[1,2-a]pyrazine-1(2H)-one HIV-1 integrase inhibitors</p>

    <p>          Fisher, Thorsten E., Kim, Boyoung, Staas, Donnette D., Lyle, Terry A., Young, Steven D., Vacca, Joseph P., Zrada, Matthew M., Hazuda, Daria J., Felock, Peter J., Schleif, William A., Gabryelski, Lori J., Anari, M. Reza, Kochansky, Christopher J., and Wai, John S.</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(23): 6511-6515            </p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4PSK8RT-4/2/ac135d7b853c619e46c6d4da4be9431a</span></p>

    <p>7.            HIV-LS-385; EMBASE-HIV-11/14/07</p>

    <p class="memofmt1-2">          Structure-activity relationships of novel HIV-1 protease inhibitors containing the 3-amino-2-chlorobenzoyl-allophenylnorstatine structure</p>

    <p>          Mimoto, Tsutomu, Nojima, Satoshi, Terashima, Keisuke, Takaku, Haruo, Shintani, Makoto, and Hayashi, Hideya</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 7816 </p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4PYP6YC-5/2/9767fdd73ba6accf7325f2838ff5d91c</span></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
