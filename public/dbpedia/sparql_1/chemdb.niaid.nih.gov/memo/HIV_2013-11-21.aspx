

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-11-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="DTguSEqytLGz5TaYtvWRux+SsKBSun9FARAeZpGSdMjrDKrgMnE0FRa8qZR66kacCfieMG4s4UCGBJ9Cu6CQN/cLAhEEKsF1svNM3PDM9NaShnzYTFtXsduELsM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B98A9775" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 8 - November 21, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24219908">Contribution of Oligomerization to the anti-HIV-1 Properties of SAMHD1.</a>Brandariz-Nunez, A., J.C. Valle-Casuso, T.E. White, L. Nguyen, A. Bhattacharya, Z. Wang, B. Demeler, S. Amie, C. Knowlton, B. Kim, D.N. Ivanov, and F. Diaz-Griffero. Retrovirology, 2013. 10(1): p. 131. PMID[24219908].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24217696">Evaluation of PD 404,182 as an anti-HIV and anti-HSV Microbicide.</a>Chamoun-Emanuelli, A.M., M. Bobardt, B. Moncla, M.K. Mankowski, R.G. Ptak, P. Gallay, and Z. Chen. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24217696].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24214163">Structure-based Design of an RNA-binding p-Terphenylene Scaffold That Inhibits HIV-1 Rev Protein Function.</a> Gonzalez-Bulnes, L., I. Ibanez, L.M. Bedoya, M. Beltran, S. Catalan, J. Alcami, S. Fustero, and J. Gallego. Angewandte Chemie, 2013. <b>[Epub ahead of print]</b>. PMID[24214163].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24227837">Novel Neuroprotective GSK-3beta Inhibitor Restricts Tat-mediated HIV-1 Replication.</a>Guendel, I., S. Iordanskiy, R. Van Duyne, K. Kehn-Hall, M. Saifuddin, R. Das, E. Jaworski, G.C. Sampey, S. Senina, L. Shultz, A. Narayanan, H. Chen, B. Lepene, C. Zeng, and F. Kashanchi. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[24227837].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24154727">IFI16 Senses DNA Forms of the Lentiviral Replication Cycle and Controls HIV-1 Replication.</a>Jakobsen, M.R., R.O. Bak, A. Andersen, R.K. Berg, S.B. Jensen, T. Jin, A. Laustsen, K. Hansen, L. Ostergaard, K.A. Fitzgerald, T.S. Xiao, J.G. Mikkelsen, T.H. Mogensen, and S.R. Paludan. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[24154727].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24160253">Design and Synthesis of P1-P3 Macrocyclic Tertiary-alcohol-comprising HIV-1 Protease Inhibitors.</a> Joshi, A., J.B. Veron, J. Unge, A. Rosenquist, H. Wallberg, B. Samuelsson, A. Hallberg, and M. Larhed. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24160253].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24199837">Synthesis of a C-glycoside Analogue of Beta-galactosyl Ceramide, a Potential HIV-1 Entry Inhibitor.</a> Thota, V.N., M. Brahmaiah, and S.S. Kulkarni. The Journal of Organic Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24199837].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24229420">Discovery of a Diaminoquinoxaline Benzenesulfonamide Antagonist of HIV-1 Nef Function Using a Yeast-based Phenotypic Screen.</a> Trible, R.P., P. Narute, L.A. Emert-Sedlak, J.J. Alvarado, K. Atkins, L. Thomas, T. Kodama, N. Yanamala, V. Korotchenko, B.W. Day, G. Thomas, and T.E. Smithgall. Retrovirology, 2013. 10(1): p. 135. PMID[24229420].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24150893">Highly Enantioselective Decarboxylative Mannich Reaction of Malonic acid Half Oxyesters with Cyclic Trifluoromethyl Ketimines: Synthesis of Beta-amino Esters and anti-HIV Drug DPC 083.</a> Yuan, H.N., S. Li, J. Nie, Y. Zheng, and J.A. Ma. Chemistry, 2013. 19(47): p. 15856-15860. PMID[24150893].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24237936">Dual-acting Stapled Peptides Target Both HIV-1 Entry and Assembly.</a>Zhang, H., F. Curreli, A.A. Waheed, P.Y. Mercredi, M. Mehta, P. Bhargava, D. Scacalossi, X. Tong, S. Lee, A. Cooper, M.F. Summers, E.O. Freed, and A.K. Debnath. Retrovirology, 2013. 10(1): p. 136. PMID[24237936].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24220107">A Novel Ribonuclease with Antiproliferative Activity toward Leukemia and Lymphoma Cells and HIV-1 Reverse Transcriptase Inhibitory Activity from the Mushroom, Hohenbuehelia serotina.</a> Zhang, R., L. Zhao, H. Wang, and T.B. Ng. International Journal of Molecular Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[24220107].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24256218">Characterization of VRC01, a Potent and Broadly Neutralizing anti-HIV mAb, Produced in Transiently and Stably Transformed Tobacco.</a> Teh, A.Y., D. Maresch, K. Klein, and J.K. Ma. Plant Biotechnology Journal, 2013. <b>[Epub ahead of print]</b>. PMID[24256218].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1108-112113.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100076">Characterization of Human anti-V3 Monoclonal Antibody 904 Isolated from an Indian Clade C Human Immunodeficiency Virus Type-1 (HIV-1) Infected Donor.</a>Andrabi, R., M. Makhdoomi, and K. Luthra. Retrovirology, 2013. 10: p. S24-S25. ISI[000326099100076].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325777200028">Human beta-Defensin 2 and 3 Inhibit HIV in Primary Macrophages: Implications for HIV Infection of the CNS.</a> Bharucha, J.P., M. Lafferty, O. Latinovic, W.Y. Lu, S. Gartner, and A. Garzino-Demo. Journal of Neurovirology, 2013. 19: p. S13-S14. ISI[000325777200028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325759800025">Design, Synthesis and Biological Evaluation of N-2,N-4-Disubstituted-1,1,3-trioxo-2H,4H-pyrrolo[1,2-b][1,2,4,6]thiatriazine Derivatives as HIV-1 NNRTIs.</a>Chen, W.M., P. Zhan, E. De Clercq, C. Pannecouque, J. Balzarini, X. Jiang, and X.Y. Liu. Bioorganic &amp; Medicinal Chemistry, 2013. 21(22): p. 7091-7100. ISI[000325759800025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100048">2-Hydroxyisoquinoline-1,3(2H, 4H)diones (HQDs), Novel Inhibitors of the HIV Integrase Catalytic Activity with a High Barrier to Resistance.</a>Christ, F., J. Demeulemeester, B. Desimmie, V. Suchaud, O. Taltynov, C. Lion, F. Bailly, S. Strelkov, P. Cotelle, and Z. Debyser. Retrovirology, 2013. 10: p. S15-S15. ISI[000326099100048].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100032">LEDGINs, a Novel Class of Antivirals Targeting HIV Integrase During Integration and Assembly.</a> Debyser, Z. Retrovirology, 2013. 10: p. S9-S9. ISI[000326099100032].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325777200074">Novel Neuroprotective GSK-3beta Inhibitor Restricts Tat-mediated HIV-1 Replication.</a> Guendel, I., R. Van Duyne, K. Kehn-Hall, M. Saifuddin, R. Das, E. Jaworski, G. Sampey, S. Senina, L. Shultz, A. Narayanan, H. Chen, S. Iordanskiy, B. Lepene, C. Zeng, and F. Kashanchi. Journal of Neurovirology, 2013. 19: p. S35-S35. ISI[000325777200074].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325137800016">Anti-HIV-1 Peptide Derivatives Based on the HIV-1 Co-receptor CXCR4.</a>Hashimoto, C., W. Nomura, T. Narumi, M. Fujino, H. Tsutsumi, M. Haseyama, N. Yamamoto, T. Murakami, and H. Tamamura. ChemMedChem, 2013. 8(10): p. 1668-1672. ISI[000325137800016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325973900001">Linking Genotype to Phenotype on Beads: High Throughput Selection of Peptides with Biological Function.</a> Huang, L.C., X.Y. Pan, H.B. Yang, L.K.D. Wan, G. Stewart-Jones, L. Dorrell, and G. Ogg. Scientific Reports, 2013. 3. ISI[000325973900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325924800019">Alkali Carbonate Effects on N-(2-Hydroxyethyl)urea Cyclization: Application to a 2-Imidazolidinone Scale-up.</a> Kaldor, I., T. Spitzer, M.S. Duan, W.M. Kazmierski, and E.E. Boros. Journal of Heterocyclic Chemistry, 2013. 50(4): p. 863-867. ISI[000325924800019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325777200089">Infection by CXCR4-tropic Human Immunodeficiency Virus Type 1 (HIV-1) is Inhibited by the Cationic Cell Penetrating Peptide Derived from HIV-1 Tat.</a>Keogan, S., S. Passic, B. Wigdahl, and F.C. Krebs. Journal of Neurovirology, 2013. 19: p. S42-S42. ISI[000325777200089].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325601600002">Efficient Electrophilic Fluorination for the Synthesis of Novel 2&#39;-Fluoro-3&#39;-methyl-5&#39;-deoxyphosphonic acid Apiosyl Nucleoside Analogues.</a>Kim, K.M. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2013. 32(10): p. 555-570. ISI[000325601600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100099">Membrane-associated RING-CH (March) 8 Protein Inhibits HIV-1 Infection.</a> Koyama, T., T. Tada, H. Fujita, and K. Tokunaga. Retrovirology, 2013. 10: p. S32-S32. ISI[000326099100099].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100084">Iron Depletion by Iron Chelators or Ferroportin Inhibits HIV-1 through the Induction of HIF1a, P21 and IKBa and the Inhibition of CDK9 and CDK2.</a>Kumari, N., D. Kovalsky, D. Breuer, X.M. Niu, and S. Nekhai. Retrovirology, 2013. 10: p. S27-S27. ISI[000326099100084].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100073">The Interferon-induced MxB Protein Inhibits an Early Step of HIV-1 Infection.</a> Liu, Z.L., Q.H. Pan, S.L. Ding, J. Qian, F.W. Xu, J.M. Zhou, S. Cen, F. Guo, and C. Liang. Retrovirology, 2013. 10: p. S23-S24. ISI[000326099100073].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325795700001">Identification of Myxobacteria-derived HIV Inhibitors by a High-throughput Two-step Infectivity Assay.</a> Martinez, J.P., B. Hinkelmann, E. Fleta-Soriano, H. Steinmetz, R. Jansen, J. Diez, R. Frank, F. Sasse, and A. Meyerhans. Microbial Cell Factories, 2013. 12 (1): p. 85. ISI[000325795700001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325777200121">The Novel Antiretroviral Compound, FX101, Reduces Virus and Provirus Burden in Cats Chronically Infected with FIV LU Infected with FIV.</a>Meeker, R., L. Hudson, and H. Kay. Journal of Neurovirology, 2013. 19: p. S57-S58. ISI[000325777200121].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326045600006">Toward the Discovery of Inhibitors of Babesipain-1, a Babesia bigemina Cysteine Protease: In Vitro Evaluation, Homology Modeling and Molecular Docking Studies.</a> Perez, B., S. Antunes, L.M. Goncalves, A. Domingos, J.R.B. Gomes, P. Gomes, and C. Teixeira. Journal of Computer-aided Molecular Design, 2013. 27(9): p. 823-835. ISI[000326045600006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326099100058">Targeting CCL2 Inhibits Viral DNA Accumulation and Induces APOBEC3A Expression in HIV-1 Infected Primary Human Macrophages.</a>Sabbatucci, M., A. Mallano, C. Purificato, D.A. Covino, M. Federico, S. Vella, S. Gessani, M. Andreotti, and L. Fantuzzi. Retrovirology, 2013. 10: p. S18-S19. ISI[000326099100058].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325096400054">Phenylethanoids from the Roots of Codonopsis cordifolioidea and Their Anti HIV-1 Activities.</a> Shen, Y.Q., J.H. Yao, L.D. Shu, L.Y. Yang, X.M. Gao, and Q.F. Hu. Asian Journal of Chemistry, 2013. 25(14): p. 7888-7890. ISI[000325096400054].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000324460900006">Synthesis of 4&#39;-C-Alkylated-5-iodo-2&#39;-deoxypyrimidine Nucleosides.</a>Strittmatter, T., J. Aschenbrenner, N. Hardt, and A. Marx. Arkivoc, 2013: p. 46-59. ISI[000324460900006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325786800003">A Novel Synthesis of Antiviral Nucleoside Phosphoramidate and Thiophosphoramidate Prodrugs Via Nucleoside H-phosphonamidates.</a>Sun, Q., X.J. Li, S.S. Gong, G. Liu, L. Shen, and L. Peng. Nucleosides Nucleotides &amp; Nucleic Acids, 2013. 32(11): p. 617-638. ISI[000325786800003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000326009300022">ZnO-mediated Regioselective C-Arylsulfonylation of Indoles: A Facile Solvent-free Synthesis of 2- and 3-Sulfonylindoles and Preliminary Evaluation of Their Activity against Drug-resistant Mutant HIV-1 Reverse Transcriptases (RTs).</a>Tocco, G., M. Begala, F. Esposito, P. Caboni, V. Cannas, and E. Tramontano. Tetrahedron Letters, 2013. 54(46): p. 6237-6241. ISI[000326009300022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325760300003">Modular Assembly of Dimeric HIV Fusion Inhibitor Peptides with Enhanced Antiviral Potency.</a> Xiao, J.P. and T.J. Tolbert. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6046-6051. ISI[000325760300003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325764000057">Role of the K101E Substitution in HIV-1 Reverse Transcriptase in Resistance to Rilpivirine and Other Nonnucleoside Reverse Transcriptase Inhibitors.</a> Xu, H.T., S.P. Colby-Germinario, W. Huang, M. Oliveira, Y.S. Han, Y.D. Quan, C.J. Petropoulos, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2013. 57(11): p. 5649-5657. ISI[000325764000057].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000325760300020">Design and Synthesis of Novel Pyrimidone Analogues as HIV-1 Integrase Inhibitors.</a> Yu, S., T.W. Sanchez, Y. Liu, Y.Z. Yin, N. Neamati, and G.S. Zhao. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(22): p. 6134-6137. ISI[000325760300020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1108-112113.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
