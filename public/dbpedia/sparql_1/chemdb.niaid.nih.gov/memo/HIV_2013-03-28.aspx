

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-03-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sttTsSPngj/jRrVTV+2qt3dHHtDsNjtO1qlr1MZsgEX2YcTyol2o+TV3oTY4ZRwMKGM3hX5dt4wkCtKYi1RUdMleGs/p+ghMc4r/U5Un7oWEt8BbSoNTeql6C00=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="480E8577" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 15 - March 28, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23529738">Impact of Primary Elvitegravir Resistance-associated Mutations in HIV-1 Integrase on Drug Susceptibility and Viral Replication Fitness.</a> Abram, M.E., R.M. Hluhanich, D.D. Goodman, K.N. Andreatta, N.A. Margot, L. Ye, A. Niedziela-Majka, T.L. Barnes, N. Novikov, X. Chen, E.S. Svarovskaia, D.J. McColl, K.L. White, and M.D. Miller. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23529738].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23527130">Interleukin-27 Is a Potent Inhibitor of cis HIV-1 Replication in Monocyte-derived Dendritic Cells via a Type I Interferon-independent Pathway.</a> Chen, Q., S. Swaminathan, D. Yang, L. Dai, H. Sui, J. Yang, R.L. Hornung, Y. Wang, W. Huang da, X. Hu, R.A. Lempicki, and T. Imamichi. PloS One, 2013. 8(3): p. e59194. PMID[23527130].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23517458">2-Hydroxyisoquinoline-1,3(2H,4H)-diones (HIDs), Novel Inhibitors of HIV Integrase with a High Barrier to Resistance.</a> Desimmie, B.A., J. Demeulemeester, V. Suchaud, O. Taltynov, M. Billamboz, C. Lion, F. Bailly, S. Strelkov, Z. Debyser, P. Cotelle, and F. Christ. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23517458].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23487403">Design, Synthesis and Docking Studies of New 4-Hydroxyquinoline-3-carbohydrazide Derivatives as anti-HIV-1 Agents.</a> Hajimahdi, Z., R. Zabihollahi, M.R. Aghasadeghi, and A. Zarghi. Drug Research, 2013. <b>[Epub ahead of print]</b>. PMID[23487403].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23510780">Inhibition of CXCR4 Expression by Recombinant Adenoviruses Containing Anti-sense RNA Resists HIV-1 Infection on MT4 Cell Lines.</a> Li, W.G., W.M. Nie, W.W. Chen, T.J. Jiang, M. Zhao, and X.Y. Xu. Gene, 2013. <b>[Epub ahead of print]</b>. PMID[23510780].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23500381">Six New Lignans from the Leaves and Stems of Schisandra sphenanthera.</a> Liang, C.Q., J. Hu, R.H. Luo, Y.M. Shi, S.Z. Shang, Z.H. Gao, R.R. Wang, Y.T. Zheng, W.Y. Xiong, H.B. Zhang, W.L. Xiao, and H.D. Sun. Fitoterapia, 2013. <b>[Epub ahead of print]</b>. PMID[23500381].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23527085">Surfactant Protein D Modulates HIV Infection of Both T-cells and Dendritic Cells.</a> Madsen, J., G.D. Gaiha, N. Palaniyar, T. Dong, D.A. Mitchell, and H.W. Clark. PloS One, 2013. 8(3): p. e59047. PMID[23527085].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23487463">Conformation-dependent Recognition of HIV gp120 by Designed Ankyrin Repeat Proteins Provides Access to Novel HIV Entry Inhibitors.</a> Mann, A., N. Friedrich, A. Krarup, J. Weber, E. Stiegeler, B. Dreier, P. Pugach, M. Robbiani, T. Riedel, K. Moehle, J.A. Robinson, P. Rusert, A. Pluckthun, and A. Trkola. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23487463].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23494217">A Novel Ribonuclease with HIV-1 Reverse Transcriptase Inhibitory Activity from the Edible Mushroom Hygrophorus russula.</a> Zhu, M., L. Xu, X. Chen, Z. Ma, H. Wang, and T.B. Ng. Applied Biochemistry and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23494217].
    <br />
    <b>[PubMed]</b>. HIV_0315-032813.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315071500014">Nitroimidazoles Part 8. Synthesis and anti-HIV Activity of New 4-Nitroimidazole Derivatives Using the Suzuki Cross-coupling Reaction (Vol 67b, Pg 925, 2012).</a> Al-Soud, Y.A., N.A. Al-Masoudi, H.H. Al-Suod, and C. Pannecouque. Zeitschrift fur Naturforschung Section B-A Journal of Chemical Sciences, 2012. 67(12): p. 1325-1325. ISI[000315071500014].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315098200002">Induction of CC-chemokines with Antiviral Function in Macrophages by the Human T Lymphotropic Virus Type 2 Transactivating Protein, Tax2.</a> Balistrieri, G., C.S. Barrios, L. Castillo, T.C. Umunakwe, C.Z. Giam, H.J. Zhi, and M.A. Beilke. Viral Immunology, 2013. 26(1): p. 3-12. ISI[000315098200002].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314893700033">Darunavir Is Predominantly Unbound to Protein in Cerebrospinal Fluid and Concentrations Exceed the Wild-type HIV-1 Median 90 Inhibitory Concentration.</a> Croteau, D., S.S. Rossi, B.M. Best, E. Capparelli, R.J. Ellis, D.B. Clifford, A.C. Collier, B.B. Gelman, C.M. Marra, J. McArthur, J.A. McCutchan, S. Morgello, D.M. Simpson, I. Grant, S. Letendre, and C. Grp. Journal of Antimicrobial Chemotherapy, 2013. 68(3): p. 684-689. ISI[000314893700033].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315251700013">Synthesis and Cytotoxicity of a Ruthenium Nitrosyl Nitric Oxide Donor with Isonicotinic Acid and a Cell Penetrating Peptide.</a> Figueiredo, L.E., E.M. Cilli, R.A.S. Molina, E.M. Espreafico, and E. Tfouni. Inorganic Chemistry Communications, 2013. 28: p. 60-63. ISI[000315251700013].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315379200005">Monitoring Binding of HIV-1 Capsid Assembly Inhibitors Using (19)F Ligand-and (15)N Protein-based NMR and X-ray Crystallography: Early Hit Validation of a Benzodiazepine Series.</a> Goudreau, N., R. Coulombe, A.M. Faucher, C. Grand-Maitre, J.E. Lacoste, C.T. Lemke, E. Malenfant, Y. Bousquet, L. Fader, B. Simoneau, J.F. Mercier, S. Titolo, and S.W. Mason. ChemMedChem, 2013. 8(3): p. 405-414. ISI[000315379200005].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314923000009">A New BET on the Control of HIV Latency.</a> Karn, J. Cell Cycle, 2013. 12(4): p. 545-546. ISI[000314923000009].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314172100015">Low-molecular-weight CXCR4 Ligands with Variable Spacers.</a> Narumi, T., H. Aikawa, T. Tanaka, C. Hashimoto, N. Ohashi, W. Nomura, T. Kobayakawa, H. Takano, Y. Hirota, T. Murakami, N. Yamamoto, and H. Tamamura. ChemMedChem, 2013. 8(1): p. 118-124. ISI[000314172100015].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314689200014">N-1,N-3-Disubstituted Uracils as Nonnucleoside Inhibitors of HIV-1 Reverse Transcriptase.</a> Novikov, M.S., V.T. Valuev-Elliston, D.A. Babkov, M.P. Paramonova, A.V. Ivanov, S.A. Gavryushov, A.L. Khandazhinskaya, S.N. Kochetkov, C. Pannecouque, G. Andrei, R. Snoeck, J. Balzarini, and K.L. Seley-Radtke. Bioorganic &amp; Medicinal Chemistry, 2013. 21(5): p. 1150-1158. ISI[000314689200014].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313569800027">Protocol for a Mammalian Cell-based Assay for Monitoring the HIV-1 Protease Activity.</a> Rajakuberan, C., B.J. Hilton, and R. Wolkowicz. Diagnosis of Sexually Transmitted Diseases: Methods and Protocols, 2012. 903: p. 393-405. ISI[000313569800027].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315133300006">Synthesis, Characterisation, Docking Analysis and Biological Evaluation of alpha,alpha &#39;-bis(p-Dimethylaminobenzylidene)-gamma-methylcyclohexanone.</a> Shalini, S., C.R. Girija, P. Karunakar, M.M. Jotani, K.N. Venugopala, and T.V. Venkatesha. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(2): p. 282-288. ISI[000315133300006].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315406800008">Verotoxin A Subunit Protects Lymphocytes and T Cell Lines against X4 HIV Infection in Vitro.</a> Shi, P.L., B. Binnington, D. Sakac, Y. Katsman, S. Ramkumar, J. Gariepy, M. Kim, D.R. Branch, and C. Lingwood. Toxins, 2012. 4(12): p. 1517-1534. ISI[000315406800008].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315137700032">Identification of a Candidate Therapeutic Autophagy-inducing Peptide.</a> Shoji-Kawata, S., R. Sumpter, M. Leveno, G.R. Campbell, Z.J. Zou, L. Kinch, A.D. Wilkins, Q.H. Sun, K. Pallauf, D. MacDuff, C. Huerta, H.W. Virgin, J.B. Helms, R. Eerland, S.A. Tooze, R. Xavier, D.J. Lenschow, A. Yamamoto, D. King, O. Lichtarge, N.V. Grishin, S.A. Spector, D.V. Kaloyanova, and B. Levine. Nature, 2013. 494(7436): p. 201-206. ISI[000315137700032].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315253900004">BST-2/tetherin: Structural Biology, Viral Antagonism, and Immunobiology of a Potent Host Antiviral Factor.</a> Swiecki, M., N.S. Omattage, and T.J. Brett. Molecular Immunology, 2013. 54(2): p. 132-139. ISI[000315253900004].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315062900008">Synthesis of Novel 3 &#39;-Methyl-5 &#39;-norcarbocyclic Nucleoside Phosphonates as Potential anti-HIV Agents.</a> Uttaro, J.P., S. Broussous, C. Mathe, and C. Perigaud. Tetrahedron, 2013. 69(9): p. 2131-2136. ISI[000315062900008].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000315355900016">Branched Peptide Boronic Acids (BPBAs): A Novel Mode of Binding Towards RNA.</a> Zhang, W.Y., D.I. Bryson, J.B. Crumpton, J. Wynn, and W.L. Santos. Chemical Communications, 2013. 49(24): p. 2436-2438. ISI[000315355900016].
    <br />
    <b>[WOS]</b>. HIV_0315-032813.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130228&amp;CC=WO&amp;NR=2013028890A1&amp;KC=A1">Compositions Comprising Substituted Perhydro pyrrolopyridines for Treating Viral Diseases.</a> Huberman, E. Patent. 2013. 2012-US52108 2013028890: 50pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130123&amp;CC=CN&amp;NR=102887887A&amp;KC=A">Preparation of Pyridine Derivatives as anti-HIV Agents.</a> Liu, X. and X. Chen. Patent. 2013. 2012-10444863 102887887: 19pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130307&amp;CC=WO&amp;NR=2013033061A1&amp;KC=A1">Preparation of Octahydropyrrolo[3,4-C]pyrrole Derivatives as HIV Attachment Inhibitors.</a> Wang, T., Z. Zhang, J.F. Kadow, and N.A. Meanwell. Patent. 2013. 2012-US52606 2013033061: 90pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130207&amp;CC=WO&amp;NR=2013019662A1&amp;KC=A1">Aryl Naphthalide Lignans as anti-HIV Agents.</a> Zhang, H. and D. Soejarto. Patent. 2013. 2012-US48657 2013019662: 52pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130206&amp;CC=CN&amp;NR=102911124A&amp;KC=A">(Hydroxy)pyrimidinone Compounds as HIV-1 Integrase Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of Aids.</a> Zhao, G., S. Yu, N. Nimati, J. Liu, Y. Liu, Y. Li, and Y. Wang. Patent. 2013. 2012-10410936 102911124: 28pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130131&amp;CC=WO&amp;NR=2013016441A1&amp;KC=A1">Preparation of Oxoisoindoline sulfonamides as Inhibitors of HIV-1 Integrase.</a> Zhao, X.Z., S.H. Hughes, B.-H.C. Vu, S. Smith, B. Johnson, Y. Pommier, and T.R. Burke, Jr. Patent. 2013. 2012-US48169 2013016441: 119pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0315-032813.</p>

    <h2>US Patent Office Citations</h2>

    <p class="plaintext">31. <a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&amp;Sect2=HITOFF&amp;d=PALL&amp;p=1&amp;u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&amp;r=1&amp;f=G&amp;l=50&amp;s1=6,787,525.PN.&amp;OS=PN/6,787,525&amp;RS=PN/6,787,525">Glyceryl Nucleotides, Method for the Production Thereof and Their Use.</a> Schott, H. and S. Ludwig. Patent. 2004. 2004-US6787525. 14pp.</p>

    <p class="plaintext"><b>[USPTO]</b>. HIV_0315-032813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
