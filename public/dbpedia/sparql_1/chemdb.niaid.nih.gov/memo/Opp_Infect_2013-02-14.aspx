

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-02-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5Uo68k+tVHPOeulPm1gnK9bf9AXl3ZbLrgi+f+9vnvcD+7Pbl9hEG/30FUnC7H1huI5PMJi+X/iXdQo3crSIRKAzj3PpnYDa4GjeCcwiWzHcoOEE6Qx34yl2XT0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="89B1FFF8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:<br />February 1 - February 14, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23246454">Anti-cancer, Anti-inflammatory and Anti-microbial Activities of Plant Extracts Used against Hematological Tumors in Traditional Medicine of Jordan.</a> Assaf, A.M., R.N. Haddadin, N.A. Aldouri, R. Alabbassi, S. Mashallah, M. Mohammad, and Y. Bustanji. Journal of Ethnopharmacology, 2013. 145(3): p. 728-736. PMID[23246454].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23266278">Antimicrobial Compounds from Alpinia conchigera.</a> Aziz, A.N., H. Ibrahim, D. Rosmy Syamsir, M. Mohtar, J. Vejayan, and K. Awang. Journal of Ethnopharmacology, 2013. 145(3): p. 798-802. PMID[23266278].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23243064">A Repurposing Approach Identifies Off-patent Drugs with Fungicidal Cryptococcal Activity, a Common Structural Chemotype, and Pharmacological Properties Relevant to the Treatment of Cryptococcosis.</a> Butts, A., L. Didone, K. Koselny, B.K. Baxter, Y. Chabrier-Rosello, M. Wellington, and D.J. Krysan. Eukaryotic Cell, 2013. 12(2): p. 278-287. PMID[23243064].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23137724">Triterpenoid Saponins from the Aerial Parts of Solidago virgaurea Alpestris with Inhibiting Activity of Candida albicans Yeast-hyphal Conversion.</a> Laurencon, L., E. Sarrazin, M. Chevalier, I. Precheur, G. Herbette, and X. Fernandez. Phytochemistry, 2013. 86: p. 103-111. PMID[23137724].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23026014">A Novel Antimicrobial Peptide Derived from Modified N-<b>t</b>erminal Domain of Bovine lactoferrin: Design, Synthesis, Activity against Multidrug-resistant Bacteria and Candida.</a> Mishra, B., G.D. Leishangthem, K. Gill, A.K. Singh, S. Das, K. Singh, I. Xess, A. Dinda, A. Kapil, I.K. Patro, and S. Dey. Biochimica et Biophysica Acta, 2013. 1828(2): p. 677-686. PMID[23026014].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23239523">A Novel Antifungal Agent with Broad Spectrum: 1-(4-Biphenylyl)-3-(1H-imidazol-1-yl)-1-propanone.</a> Roman, G., M. Mares, and V. Nastasa. Archiv der Pharmazie, 2013. 346(2): p. 110-118. PMID[23239523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23208712">Milbemycins: More Than Efflux Inhibitors for Fungal Pathogens.</a> Silva, L.V., M. Sanguinetti, P. Vandeputte, R. Torelli, B. Rochat, and D. Sanglard. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 873-886. PMID[23208712].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p> 
    
    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23378416">Synergy of the Antibiotic Colistin with Echinocandin Antifungals in Candida Species.</a> Zeidler, U., M.E. Bougnoux, A. Lupan, O. Helynck, A. Doyen, Z. Garcia, N. Sertour, C. Clavaud, H. Munier-Lehmann, C. Saveanu, and C. d&#39;Enfert. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23378416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021412.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330920">Pharmacokinetics of Sequential Doses of Capreomycin Powder for Inhalation in Guinea Pigs.</a> Garcia-Contreras, L., P. Muttil, J.K. Fallon, M. Kabadi, R. Gerety, and A.J. Hickey. Antimicrobial Agents and Chemotherapy, 2012. 56(5): p. 2612-2618. PMID[22330920].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22319585">Successful Shortening of Tuberculosis Treatment Using Adjuvant Host-directed Therapy with FDA-approved Phosphodiesterase Inhibitors in the Mouse Model.</a> Maiga, M., N. Agarwal, N.C. Ammerman, R. Gupta, H. Guo, M.C. Maiga, S. Lun, and W.R. Bishai. Plos One, 2012. 7(2): p. e30749. PMID[22319585].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22299588">Inhibition of the Beta-class Carbonic Anhydrases from Mycobacterium tuberculosis with Carboxylic Acids.</a> Maresca, A., D. Vullo, A. Scozzafava, G. Manole, and C.T. Supuran. Journal of Enzyme Inhibition and Medicinal Chemistry, 2013. 28(2): p. 392-396. PMID[22299588].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330206">Identification of Pharmacophore in Bioactive Metal Complexes: Synthesis, Spectroscopic Characterization and Application.</a> Mehrotra, R., S.N. Shukla, P. Gaur, and A. Dubey. European Journal of Medicinal Chemistry, 2012. 50: p. 149-153. PMID[22330206].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22236923">Fluoroquinolone and Pyrazinamide Resistance in Multidrug-resistant Tuberculosis.</a> Pierre-Audigier, C., C. Surcouf, V. Cadet-Daniel, A. Namouchi, S. Heng, A. Murray, B. Guillard, and B. Gicquel. The International Journal of Tuberculosis and Lung Disease, 2012. 16(2): p. 221-223, i-ii. PMID[22236923].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22318308">Protective Effect of Curcumin, Silymarin and N-Acetylcysteine on Antitubercular Drug-induced Hepatotoxicity Assessed in an in Vitro Model.</a> Singh, M., P. Sasi, V.H. Gupta, G. Rai, D.N. Amarapurkar, and P.P. Wangikar. Human &amp; Experimental Toxicology, 2012. 31(8): p. 788-797. PMID[22318308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22536648">Derivatives of Phenylcarbamic acid as Potential Antituberculotics.</a> Waisser, K. and J. Cizmarik. Ceska a Slovenska Farmacie, 2012. 61(1-2): p. 17-20. PMID[22536648].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22290959">Antigen 85c Inhibition Restricts Mycobacterium tuberculosis Growth through Disruption of Cord Factor Biosynthesis.</a> Warrier, T., M. Tropis, J. Werngren, A. Diehl, M. Gengenbacher, B. Schlegel, M. Schade, H. Oschkinat, M. Daffe, S. Hoffner, A.N. Eddine, and S.H. Kaufmann. Antimicrobial Agents and Chemotherapy, 2012. 56(4): p. 1735-1743. PMID[22290959].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23247255">Antimalarial Activity of 10-Alkyl/Aryl Esters and -<b>a</b>minoethylethers of Artemisinin.</a> Cloete, T.T., H.J. Krebs, J.A. Clark, M.C. Connelly, A. Orcutt, M.S. Sigal, R. Kiplin Guy, and D. N&#39;Da D. Bioorganic Chemistry, 2013. 46: p. 10-16. PMID[23247255].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23223036">Isoprenoid Biosynthesis Inhibition Disrupts Rab5 Localization and Food Vacuolar Integrity in Plasmodium falciparum.</a> Howe, R., M. Kelly, J. Jimah, D. Hodge, and A.R. Odom. Eukaryotic Cell, 2013. 12(2): p. 215-223. PMID[23223036].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23399920">Antiplasmodial Potential of Selected Medicinal Plants from Eastern Ghats of South India.</a> Kaushik, N.K., A. Bagavan, A.A. Rahuman, D. Mohanakrishnan, C. Kamaraj, G. Elango, A.A. Zahir, and D. Sahal. Experimental Parasitology, 2013. <b>[Epub ahead of print]</b>. PMID[23399920].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23229318">Antiprotozoal Activity of Ferroquine.</a> Pomel, S., C. Biot, C. Bories, and P.M. Loiseau. Parasitology Research, 2013. 112(2): p. 665-669. PMID[23229318].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23391499">Ketoconazole, a Cytochrome P(450) Inhibitor Can Potentiate the Antimalarial Action of alpha/beta Arteether against Mdr P. Yoelii nigeriensis</a>. Tripathi, R., A. Rizvi, S.K. Pandey, H. Dwivedi, and J.K. Saxena. Acta Tropica, 2013. <b>[Epub ahead of print]</b>. PMID[23391499].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0201-021413.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22200403">Analogs of Pentamidine as Potential Anti-pneumocystis Chemotherapeutics.</a> Maciejewska, D., J. Zabinski, P. Kazmierczak, M. Rezler, B. Krassowska-Swiebocka, M.S. Collins, and M.T. Cushion. European Journal of Medicinal Chemistry, 2012. 48: p. 164-173. PMID[22200403].</p>

    <p class="plaintext">            <b>[PubMed]</b>. OI_0201-021413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312481900017">New Antifungal Compounds from Aspergillus terreus Isolated from Desert Soil.</a> Awaad, A.S., A.J.A. Nabilah, and M.E. Zain. Phytotherapy Research, 2012. 26(12): p. 1872-1877. ISI[000312481900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0118-013113.</p> 
    
    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313130900001">Biocidal Polymers: Synthesis and Antimicrobial Properties of Benzaldehyde Derivatives Immobilized onto Amine-terminated Polyacrylonitrile.</a>  Alamri, A., M.H. El-Newehy, and S.S. Al-Deyab. Chemistry Central Journal, 2012. 6. ISI[000313130900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313154400029">Mechanism Studies of Substituted Triazol-1-yl-pyrimidine Derivatives Inhibition on Mycobacterium tuberculosis Acetohydroxyacid Synthase.</a> Chien, P.N., I.P. Jung, K.V. Reddy, and M.Y. Yoon. Bulletin of the Korean Chemical Society, 2012. 33(12): p. 4074-4078. ISI[000313154400029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312681100010">Synthesis and Evaluation of alpha-Thymidine Analogues as Novel Antimalarials.</a> Cui, H.Q., J. Carrero-Lerida, A.P.G. Silva, J.L. Whittingham, J.A. Brannigan, L.M. Ruiz-Perez, K.D. Read, K.S. Wilson, D. Gonzalez-Pacanowska, and I.H. Gilbert. Journal of Medicinal Chemistry, 2012. 55(24): p. 10948-10957. ISI[000312681100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313187800001">Antimicrobial Activity and Phytochemical Screening of Buchenavia tetraphylla (Aubl.) R. A. Howard (Combretaceae: Combretoideae).</a> de Oliveira, Y.L.C., L.C.N. da Silva, A.G. da Silva, A.J. Macedo, J.M. de Araujo, M.T.D. Correia, and M.V. da Silva. Scientific World Journal, 2012. Article ID 849302: pp. 6.  ISI[000313187800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312874100042">Novel 4H-1,2,4-Triazol-3-yl cycloalkanols as Potent Antitubercular Agents.</a> Desai, N.H.P., R. Bairwa, M. Kakwani, N. Tawari, M.K. Ray, M.G. Rajan, and M. Degani. Medicinal Chemistry Research, 2013. 22(1): p. 401-408. ISI[000312874100042].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312874100045">Synthesis and Antimicrobial Activity of Some (3-Phenyl-5-(1-phenyl-3-aryl-1H-pyrazol-4-yl)-4,5-dihydro-1H-pyrazol-1-yl)(pyridin-4-yl)methanones: New Derivatives of 1,3,5-Trisubstituted pyrazolines.</a>, S., Meenakshi, S. Kumar, and P. Kumar. Medicinal Chemistry Research, 2013. 22(1): p. 433-439. ISI[000312874100045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312942100010">Antibacterial Activity of Long-chain Fatty Alcohols against Mycobacteria.</a> Mukherjee, K., P. Tribedi, B. Mukhopadhyay, and A.K. Sil. FEMS Microbiology Letters, 2013. 338(2): p. 177-183. ISI[000312942100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312874100028">Synthesis, Characterization, and Biological Evaluation of Some N-Aryl hydrazones and Their 2,3-Disubstituted-4-thiazolidinone Derivatives.</a> Nandagokula, C., B. Poojary, S. Vittal, S. Shenoy, P. Shetty, and A. Tangavelu. Medicinal Chemistry Research, 2013. 22(1): p. 253-266. ISI[000312874100028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312874100021">Synthesis of Coumarin-based 1,3,4-Oxadiazol-2ylthio-N-phenyl/benzothiazolyl acetamides as Antimicrobial and Antituberculosis Agents.</a> Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia. Medicinal Chemistry Research, 2013. 22(1): p. 195-210. ISI[000312874100021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312874100024">Synthesis, Characterization, and Pharmacological Evaluation of 1- 2-(6-Nitro-4-oxo-2-phenyl-4H-quinazolin-3-yl)-ethyl -3-phenyl Ureas.</a> Rana, A.M., K.R. Desai, and S. Jauhari. Medicinal Chemistry Research, 2013. 22(1): p. 225-233. ISI[000312874100024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312416200014">Quinolinyl Pyrimidines: Potent Inhibitors of NDN-2 as a Novel Class of anti-TB Agents.</a> Shirude, P.S., B. Paul, N.R. Choudhury, C. Kedari, B. Bandodkar, and B.G. Ugarkar. ACS Medicinal Chemistry Letters, 2012. 3(9): p. 736-740. ISI[000312416200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003128741000014">Solvent- and Catalyst-free Synthesis of bis-Adducts of 3-Formylchromone as Potential Antimicrobial Agents.</a> Siddiqui, Z.N., T.N.M. Musthafa, and S. Praveen. Medicinal Chemistry Research, 2013. 22(1): p. 127-133. ISI[000312874100014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313136600006">The Effects of Wild Thyme (Thymus serpyllum L.) Essential Oil Components against Ochratoxin-Producing Aspergilli.</a> Sokolic-Mihalak, D., J. Frece, A. Slavica, F. Delas, H. Pavlovic, and K. Markov. Arhiv Za Higijenu Rada I Toksikologiju, 2012. 63(4): p. 457-462. ISI[000313136600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312940400064">Synthesis, Characterization, and Antifungal Activity of Nystatingum arabic Conjugates.</a> Stefanovic, J., D. Jakovljevic, G. Gojgic-Cvijovic, M. Lazic, and M. Vrvic. Journal of Applied Polymer Science, 2013. 127(6): p. 4736-4743. ISI[000312940400064].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003128741000024">Anti-tubercular Agents from Ammannia baccifera (Linn.).</a> Upadhyay, H.C., J.P. Thakur, D. Saikia, and S.K. Srivastava. Medicinal Chemistry Research, 2013. 22(1): p. 16-21. ISI[000312874100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312904900022">Novel Comb-like Ionenes with Aliphatic Side Chains: Synthesis and Antimicrobial Properties.</a> Xu, X., H.N. Xiao, Z. Ziaee, H. Wang, Y. Guan, and A.N. Zheng. Journal of Materials Science, 2013. 48(3): p. 1162-1171. ISI[000312904900022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0201-021413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
