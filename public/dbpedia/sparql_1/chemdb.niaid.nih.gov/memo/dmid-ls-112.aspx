

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-112.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xX74Iv7oD9qhTIpwqh8HkFvooFfYOnVINuaKwEsQ/p5g45pXZMOjURHehP8EKqvBN+KX8Yrod6C2XpNgjhsnmeSyj1RU9s4ydKemOLuRqTcd43iCo5XawPGPess=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52C6352E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-112-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58195   DMID-LS-112; EMBASE-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New medicines from nature&#39;s armamentarium</p>

    <p>          Crump, Andy</p>

    <p>          Trends in Parasitology <b>2006</b>.  22(2): 51-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7G-4J0WRGN-2/2/9579fdbfcb1cd2047dd2569dc3793e03">http://www.sciencedirect.com/science/article/B6W7G-4J0WRGN-2/2/9579fdbfcb1cd2047dd2569dc3793e03</a> </p><br />

    <p>2.     58196   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New targets and inhibitors of HBV replication to combat drug resistance</p>

    <p>          Cheng, YC, Ying, CX, Leung, CH, and Li, Y</p>

    <p>          J Clin Virol <b>2005</b>.  34 Suppl 1: S147-S150</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16461217&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16461217&amp;dopt=abstract</a> </p><br />

    <p>3.     58197   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological activity of tricyclic analogues of 9-{[cis-1&#39;,2&#39;-bis(hydroxymethyl)cycloprop-1&#39;-yl]methyl}guanine</p>

    <p>          Ostrowski, T, Golankiewicz, B, Clercq, ED, and Balzarini, J</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458009&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458009&amp;dopt=abstract</a> </p><br />

    <p>4.     58198   DMID-LS-112; EMBASE-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery of small molecule inhibitors of West Nile virus using a high-throughput sub-genomic replicon screen</p>

    <p>          Gu, Baohua, Ouzunov, Serguey, Wang, Liguan, Mason, Peter, Bourne, Nigel, Cuconati, Andy, and Block, Timothy M</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4J78WX6-1/2/8340a846e0e37577748d8745f4bea362">http://www.sciencedirect.com/science/article/B6T2H-4J78WX6-1/2/8340a846e0e37577748d8745f4bea362</a> </p><br />

    <p>5.     58199   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In Vitro Antiviral Activity of Marine Sponges Collected Off Brazilian Coast</p>

    <p>          Da Silva, A. <i>et al.</i></p>

    <p>          Biological &amp; Pharmaceutical Bulletin <b>2006</b>.  29(1): 135-140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234760200028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234760200028</a> </p><br />
    <br clear="all">

    <p>6.     58200   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Benzimidazole inhibitors of hepatitis C virus NS5B polymerase: Identification of 2-[(4-diarylmethoxy)phenyl]-benzimidazole</p>

    <p>          Ishida, T, Suzuki, T, Hirashima, S, Mizutani, K, Yoshida, A, Ando, I, Ikeda, S, Adachi, T, and Hashimoto, H</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16455252&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16455252&amp;dopt=abstract</a> </p><br />

    <p>7.     58201   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Dipeptidyl Glutaminyl Fluoromethyl Ketones as Potent Severe Acute Respiratory Syndrome Coronovirus (SARS-CoV) Inhibitors</p>

    <p>          Zhang, HZ, Zhang, H, Kemnitzer, W, Tseng, B, Cinatl, J Jr, Michaelis, M, Doerr, HW, and Cai, SX</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 1198-1201</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451084&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451084&amp;dopt=abstract</a> </p><br />

    <p>8.     58202   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Cyclopentenyl Carbocyclic Nucleosides as Potential Antiviral Agents Against Orthopoxviruses and SARS</p>

    <p>          Cho, JH, Bernard, DL, Sidwell, RW, Kern, ER, and Chu, CK</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 1140-1148</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451078&amp;dopt=abstract</a> </p><br />

    <p>9.     58203   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SAR and Mode of Action of Novel Non-Nucleoside Inhibitors of Hepatitis C NS5b RNA Polymerase</p>

    <p>          Powers, JP, Piper, DE, Li, Y, Mayorga, V, Anzola, J, Chen, JM, Jaen, JC, Lee, G, Liu, J, Peterson, MG, Tonn, GR, Ye, Q, Walker, NP, and Wang, Z</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 1034-1046</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451069&amp;dopt=abstract</a> </p><br />

    <p>10.   58204   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel potent hepatitis C virus NS3 serine protease inhibitors derived from proline-based macrocycles</p>

    <p>          Chen, KX, Njoroge, FG, Arasappan, A, Venkatraman, S, Vibulbhan, B, Yang, W, Parekh, TN, Pichardo, J, Prongay, A, Cheng, KC, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 995-1005</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451065&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58205   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          3-(1,1-Dioxo-2H-(1,2,4)-benzothiadiazin-3-yl)-4-hydroxy-2(1H)-quinolinones , Potent Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase</p>

    <p>          Tedesco, R, Shaw, AN, Bambal, R, Chai, D, Concha, NO, Darcy, MG, Dhanak, D, Fitch, DM, Gates, A, Gerhardt, WG, Halegoua, DL, Han, C, Hofmann, GA, Johnston, VK, Kaura, AC, Liu, N, Keenan, RM, Lin-Goerke, J, Sarisky, RT, Wiggall, KJ, Zimmerman, MN, and Duffy, KJ</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 971-983</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451063&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451063&amp;dopt=abstract</a> </p><br />

    <p>12.   58206   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-Hsv-1 Activity of Euclea Natalensis</p>

    <p>          Lall, N., Meyer, J., and Taylor, M.</p>

    <p>          South African Journal of Botany <b>2005</b>.  71(3-4): 444-446</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234938800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234938800018</a> </p><br />

    <p>13.   58207   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Development of Polyoxometalates as Antivirus Drugs</p>

    <p>          Liu, J., Wang, E., and Ji, L.</p>

    <p>          Progress in Chemistry <b>2006</b>.  18(1): 114-119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235017500017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235017500017</a> </p><br />

    <p>14.   58208   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antivirals for influenza in healthy adults: systematic review</p>

    <p>          Jefferson, T,  Demicheli, V, Rivetti, D , Jones, M, Di, Pietrantonj C, and Rivetti, A</p>

    <p>          Lancet <b>2006</b>.  367(9507): 303-313</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16443037&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16443037&amp;dopt=abstract</a> </p><br />

    <p>15.   58209   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Searching for a new anti-HCV therapy: Synthesis and properties of tropolone derivatives</p>

    <p>          Boguszewska-Chachulska, AM, Krawczyk, M, Najda, A, Kopanska, K, Stankiewicz-Drogon, A, Zagorski-Ostoja, W, and Bretner, M</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  341(2): 641-647</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438939&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438939&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   58210   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Macrophage-Mediated Inhibitory Effect of Zingiber officinale Rosc, A Traditional Oriental Herbal Medicine, on the Growth of Influenza A/Aichi/2/68 Virus</p>

    <p>          Imanishi, N, Andoh, T, Mantani, N, Sakai, S, Terasawa, K, Shimada, Y, Sato, M, Katada, Y, Ueda, K, and Ochiai, H</p>

    <p>          Am J Chin Med <b>2006</b>.  34(1): 157-169</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16437748&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16437748&amp;dopt=abstract</a> </p><br />

    <p>17.   58211   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          5-Alkynyl-2&#39;-Deoxyuridines, Containing Bulky Aryl Groups: Evaluation of Structure-Anti-Hsv-1 Activity Relationship</p>

    <p>          Skorobogatyi, M. <i>et al.</i></p>

    <p>          Tetrahedron <b>2006</b>.  62(6): 1279-1287</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234783600028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234783600028</a> </p><br />

    <p>18.   58212   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virucidal Activity of a Beta-Triketone-Rich Essential Oil of Leptospermum Scoparium (Manuka Oil) Against Hsv-1 and Hsv-2 in Cell Culture</p>

    <p>          Reichling, J. <i> et al.</i></p>

    <p>          Planta Medica <b>2005</b>.  71(12): 1123-1127</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234731400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234731400006</a> </p><br />

    <p>19.   58213   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human {alpha}-defensins block papillomavirus infection</p>

    <p>          Buck, CB, Day, PM, Thompson, CD, Lubkowski, J, Lu, W, Lowy, DR, and Schiller, JT</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.  103(5): 1516-1521</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16432216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16432216&amp;dopt=abstract</a> </p><br />

    <p>20.   58214   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian influenza</p>

    <p>          Wu, TZ and Huang, LM</p>

    <p>          Chang Gung Med J <b>2005</b>.  28(11): 753-757</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16422180&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16422180&amp;dopt=abstract</a> </p><br />

    <p>21.   58215   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The anti-yellow fever virus activity of ribavirin is independent of error-prone replication</p>

    <p>          Leyssen, P, De Clercq, E, and Neyts, J</p>

    <p>          Mol Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421290&amp;dopt=abstract</a> </p><br />

    <p>22.   58216   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of cell growth on hepatitis C virus (HCV) replication and a mechanism of cell confluence-based inhibition of HCV RNA and protein expression</p>

    <p>          Nelson, HB and Tang, H</p>

    <p>          J Virol <b>2006</b> .  80(3): 1181-1190</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16414995&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16414995&amp;dopt=abstract</a> </p><br />

    <p>23.   58217   DMID-LS-112; PUBMED-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian flu to human influenza</p>

    <p>          Lewis, DB</p>

    <p>          Annu Rev Med <b>2006</b>.  57: 139-154</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16409141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16409141&amp;dopt=abstract</a> </p><br />

    <p>24.   58218   DMID-LS-112; WOS-DMID-2/13/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phenolics With Anti-Hsv and Anti-Hiv Activities From Artocarpus Gomezianus , Mallotus Pallidus , and Triphasia Trifolia</p>

    <p>          Likhitwitayawuid, K. <i>et al.</i></p>

    <p>          Pharmaceutical Biology <b>2005</b>.  43(8): 651-657</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234398600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234398600002</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
