

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-09-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5jpxdzwHzd7LbBWK+tXWcFx1O2aSEQ58zxa8LI1PFJonVIr19getKhWsKx66tr8pZc3tcUfrTeHtgzl56lHdMwTZ3oPrbfD6pUbiL+RqIBWGeyy3qG1qDY/P7zc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6C8669C7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List:</p>

    <p class="memofmt2-h1">September 3 - September 16, 2010</p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20833168">Toxoplasma gondii: Inhibitory Activity and Encystation Effect of Securinine and Pyrrolidine Derivatives on Toxoplasma Growth.</a> Holmes, M., A.K. Crater, B. Dhudshia, A.N. Thadani, and S. Ananvoranich. Experimental Parasitology. 2010. <b>[Epub ahead of print]</b>; PMID[20833168].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"></p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20835742">Fungicidal activity of thymol and carvacrol by disrupting ergosterol biosynthesis and membrane integrity against Candida.</a> Ahmad, A., A. Khan, F. Akhtar, S. Yousuf, I. Xess, L.A. Khan, and N. Manzoor. European Journal of Clinical Microbiology &amp; Infectious Diseases.2010. <b>[Epub ahead of print]</b>; PMID[20835742].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20833782">Screening for antifungal peptides and their modes of action in Aspergillus nidulans.</a> Mania, D., K. Hilpert, S. Ruden, R. Fischer, and N. Takeshita. Applied and Environmental Microbiology. 2010.<b>[Epub ahead of print]</b>; PMID[20833782].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20819062">3-Phenyl-1H-Indole-5-Sulfonamides: Structure-Based Drug Design of a Promising Class of Carbonic Anhydrase Inhibitors.</a> Guzel, O., A. Innocenti, D. Vullo, A. Scozzafava, and C.T. Supuran. Current Pharmaceutical Design. 2010. <b>[Epub ahead of print]</b>; PMID[20819062].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20822121">Modular Approach to Triazole-Linked 1,6-alpha-d-Oligomannosides to the Discovery of Inhibitors of Mycobacterium tuberculosis Cell Wall Synthetase.</a> Lo Conte, M., A. Marra, A. Chambery, S.S. Gurcha, G.S. Besra, and A. Dondoni. Journal of Organic Chemistry. <b>[Epub ahead of print]</b>; PMID[20822121].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20727773">Identification of novel antitubercular compounds through hybrid virtual screening approach.</a> Muddassar, M., J.W. Jang, H.S. Gon, Y.S. Cho, E.E. Kim, K.C. Keum, T. Oh, S.N. Cho, and A.N. Pae. Bioorganic &amp; Medicinal Chemistry. 18(18): p. 6914-21; PMID[20727773].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20837170">The hydroxy-naphthoquinone lapachol arrests mycobacterial growth and immunomodulates host macrophages.</a> Oliveira, R.A., E. Azevedo-Ximenes, R. Luzzati, and R.C. Garcia. International Immunopharmacology. <b>[Epub ahead of print]</b>; PMID[20837170].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0903-091610.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281140600014">Antimycobacterial activity of UDP-galactopyranose mutase inhibitors.</a> Borrelli, S., W.F. Zandberg, S. Mohan, M. Ko, F. Martinez-Gutierrez, S.K. Partha, D.A.R. Sanders, Y. Av-Gay, and B.M. Pinto. International Journal of Antimicrobial Agents. 36(4): p. 364-368; ISI[000281140600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000280885500001">Efficacy of S-adenosylhomocysteine hydrolase inhibitors, D-eritadenine and (S)-DHPA, against the growth of Cryptosporidium parvum in vitro.</a> Ctrnacta, V., J.M. Fritzler, M. Surinova, I. Hrdy, G.A. Zhu, and F. Stejskal. Experimental Parasitology. 126(2): p. 113-116; ISI[000280885500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281095300001">1,3-Dipolar cycloaddition of nitrile oxides to (R)-1-(1-phenylethyl)-3,5-bis[(E)-arylmethylidene]tetrahydro-4(1H)-pyrid inones: synthesis and antimycobacterial evaluation of novel enantiomerically pure di- and trispiroheterocycles.</a> Kumar, R.S., S.M. Rajesh, S. Perumal, P. Yogeeswari, and D. Sriram. Tetrahedron-Asymmetry. 21(11-12): p. 1315-1327; ISI[000281095300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0903-091610.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281105600013">Identification of Thiazolidinones Spiro-Fused to Indolin-2-ones as Potent and Selective Inhibitors of the Mycobacterium tuberculosis Protein Tyrosine Phosphatase B.</a> Vintonyak, V.V., K. Warburg, H. Kruse, S. Grimme, K. Hubel, D. Rauh, and H. Waldmann. Angewandte Chemie-International Edition. 49(34): p. 5902-5905; ISI[000281105600013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0903-091610.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
