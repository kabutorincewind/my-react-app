

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-01-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iP5WQElWSs76YOrfV+emb/HTxIdH82TFS0y8svP5uhWK3GIPdqHBF7Jii3FoJeZ980DC51CROk+zsH32U/zqk3z9sJeOuLl1wUgiPDRo2LOGWrQRDUhfYuiCPho=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C2F7A645" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-h1">December 22 - January 6, 2011</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21189123">Development of Anti-Viral Agents Using Molecular Modeling and Virtual Screening Techniques</a>. Kirchmair, J., S. Distinto, K.R. Liedl, P. Markt, J.M. Rollinger, D. Schuster, G.M. Spitzer, and G. Wolber.  Infectious disorders drug targets, 2010. <b>[Epub ahead of print]</b>; PMID[21189123].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B virus</p> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21183354">Synthesis and in Vitro Anti-Hepatitis B Virus Activity of Six-Membered Azanucleoside Analogues</a>. Wang, D., Y.H. Li, Y.P. Wang, R.M. Gao, L.H. Zhang, and X.S. Ye. Bioorganic &amp; Medicinal Chemistry, 2011. 19(1): p. 41-51; PMID[21183354].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21181737">Synthesis and Anti-Hepatitis B Virus Activity of a Novel Class of Thiazolylbenzimidazole Derivatives</a>. Luo, Y., J.P. Yao, L. Yang, C.L. Feng, W. Tang, G.F. Wang, J.P. Zuo, and W. Lu. Archiv Der Pharmazie, 2010. <b>[Epub ahead of print]</b>; PMID[21181737].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21203938">Specific Interaction of Hepatitis C Virus Glycoproteins with Mannan Binding Lectin Inhibits Virus Entry</a>. Brown, K.S., M.J. Keogh, A.M. Owsianka, R. Adair, A.H. Patel, J.N. Arnold, J.K. Ball, R.B. Sim, A.W. Tarr, and T.P. Hickling. Protein &amp; cell, 2010. 1(7): p. 664-74; PMID[21203938].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21193848">An Efficient New Route to Dihydropyranobenzimidazole Inhibitors of Hcv Replication</a>. Parker, M.A., E. Satkiewicz, T. Hermann, and B.M. Bergdahl. Molecules (Basel, Switzerland), 2010. 16(1): p. 281-90; PMID[21193848].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21193836">Selection of Peptides Binding to Hcv E2 and Inhibiting Viral Infectivity</a>. Hong, H.W., S.W. Lee, and H. Myung. Journal of Microbiology and Biotechnology, 2010. 20(12): p. 1769-71; PMID[21193836].</p> 

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21180601">New Direct-Acting Antivirals in the Development for Hepatitis C Virus Infection</a>. Pockros, P.J. Therapeutic advances in gastroenterology, 2010. 3(3): p. 191-202; PMID[21180601].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21179047">A Novel Class of Geldanamycin Derivatives as Hcv Replication Inhibitors Targeting on Hsp90: Synthesis, Structure-Activity Relationships and Anti-Hcv Activity in Gs4.3 Replicon Cells</a>. Shan, G.Z., Z.G. Peng, Y.H. Li, D. Li, Y.P. Li, S. Meng, L.Y. Gao, J.D. Jiang, and Z.R. Li. The Journal of antibiotics, 2010. <b>[Epub ahead of print]</b>; PMID[21179047].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21173177">Inhibitors of Endoplasmic Reticulum Alpha-Glucosidases Potently Suppress Hepatitis C Virus Virion Assembly and Release</a>. Qu, X., X. Pan, J. Weidner, W. Yu, D. Alonzi, X. Xu, T. Butters, T. Block, J.T. Guo, and J. Chang. Antimicrobial Agents and Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21173177].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_1222-010611.</p> 

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
