

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-10-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eblKnLzdaAAEgIcqNvRaK4DgUbmf3pro0X0sOA8uXKI17sVCV2YDqgmZZH86E6nyxUCAwxiJoqh8oD5hDX4z7DomvEYyjqHLPRV49RwhbxOf7fMYERDgkL6o3Sg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="27BB1787" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List:</p>

    <p class="memofmt2-h1">October 14, 2009 -October 27, 2009</p>

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19856919?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=20">Novel Macrocyclic Inhibitors of Hepatitis C NS3/4A Protease Featuring a 2-Amino-1,3-thiazole as a P4 Carbamate Replacement.</a></p>

    <p class="NoSpacing">Di Francesco ME, Dessole G, Nizi E, Pace P, Koch U, Fiore F, Pesci S, Di Muzio J, Monteagudo E, Rowley M, Summa V.</p>

    <p class="NoSpacing">J Med Chem. 2009 Oct 26. [Epub ahead of print]PMID: 19856919 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19843980?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=56">Anti-hepatitis C virus activity of novel beta-d-2&#39;-C-methyl-4&#39;-azido pyrimidine nucleoside phosphoramidate prodrugs.</a></p>

    <p class="NoSpacing">Rondla R, Coats SJ, McBrayer TR, Grier J, Johns M, Tharnish PM, Whitaker T, Zhou L, Schinazi RF.</p>

    <p class="NoSpacing">Antivir Chem Chemother. 2009 Oct 19;20(2):99-106.PMID: 19843980 [PubMed - in process]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19841155?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=63">MK-7009: A Potent and Selective Inhibitor of Hepatitis C Virus NS3/4A Protease.</a></p>

    <p class="NoSpacing">Liverton NJ, Carroll SS, Dimuzio J, Fandozzi C, Graham DJ, Hazuda D, Holloway MK, Ludmerer SW, McCauley JA, McIntyre CJ, Olsen DB, Rudd MT, Stahlhut M, Vacca JP.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Oct 19. [Epub ahead of print]PMID: 19841155 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19840259?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=69">Double-stranded RNA-activated protein kinase inhibits hepatitis C virus replication but may be not essential in interferon treatment.</a></p>

    <p class="NoSpacing">Chang JH, Kato N, Muroyama R, Taniguchi H, Guleng B, Dharel N, Shao RX, Tateishi K, Jazag A, Kawabe T, Omata M.</p>

    <p class="NoSpacing">Liver Int. 2009 Oct 16. [Epub ahead of print]PMID: 19840259 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19839643?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=72">Hepatitis C virus NS3 protease is activated by low concentrations of protease inhibitors.</a></p>

    <p class="NoSpacing">Dahl G, Arenas OG, Danielson UH.</p>

    <p class="NoSpacing">Biochemistry. 2009 Oct 19. [Epub ahead of print]PMID: 19839643 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19833201?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=80">Hepatitis C virus NS3 protease inhibitors: Large, flexible molecules of peptide origin shows satisfactory permeability across Caco-2 cells.</a></p>

    <p class="NoSpacing">Bergström CA, Bolin S, Artursson P, Rönn R, Sandström A.</p>

    <p class="NoSpacing">Eur J Pharm Sci. 2009 Oct 13. [Epub ahead of print]PMID: 19833201 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19767736?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=107">Conformational inhibition of the hepatitis C virus internal ribosome entry site RNA.</a></p>

    <p class="NoSpacing">Parsons J, Castaldi MP, Dutta S, Dibrov SM, Wyles DL, Hermann T.</p>

    <p class="NoSpacing">Nat Chem Biol. 2009 Nov;5(11):823-5. Epub 2009 Sep 20.PMID: 19767736 [PubMed - in process]</p> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19721068?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=132">Development of NS3/4A protease-based reporter assay suitable for efficiently assessing hepatitis C virus infection.</a></p>

    <p class="NoSpacing">Pan KL, Lee JC, Sung HW, Chang TY, Hsu JT.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Nov;53(11):4825-34. Epub 2009 Aug 31.PMID: 19721068 [PubMed - in process]</p> 

    <p class="memofmt2-2">cytomegalovirus</p>

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19780760?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=35">Dimethylthiazolidine carboxylic acid as a rigid p3 unit in inhibitors of serine proteases: application to two targets.</a></p>

    <p class="NoSpacing">Kawai SH, Aubry N, Duceppe JS, Llinàs-Brunet M, LaPlante SR.</p>

    <p class="NoSpacing">Chem Biol Drug Des. 2009 Nov;74(5):517-22. Epub 2009 Sep 22.</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="NoSpacing">10. Ellis, DA, et al.</p>

    <p class="NoSpacing">5,5 &#39;- and 6,6 &#39;-Dialkyl-5,6-dihydro-1H-pyridin-2-ones as potent inhibitors of HCV NS5B polymerase</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS 19(21) 2009. 6047-52.</p>

    <p class="ListParagraph">11. Kang, IJ, et al.</p>

    <p class="NoSpacing">Design and efficient synthesis of novel arylthiourea derivatives as potent hepatitis C virus inhibitors</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2009 (Nov.) 19: 6063 - 6068</p>

    <p class="ListParagraph">12. Pacini, B, et al.</p>

    <p class="NoSpacing">2-(3-Thienyl)-5,6-dihydroxypyrimidine-4-carboxylic acids as inhibitors of HCV NS5B RdRp</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2009 (Nov.) 19: 6245 - 6249</p> 

    <p class="memofmt2-2">Articles from Reviews We Abstracted</p>

    <p class="NoSpacing">From Memo OV-LS-400, &quot;Arbidol: A Broad-Spectrum Antiviral Compound that Blocks Viral Fusion&quot;:</p> 

    <p class="ListParagraph">13. <a href="http://pubs.acs.org/doi/abs/10.1021/bi700181j">Biochemical Mechanism of Hepatitis C Virus Inhibition by the Broad-Spectrum Antiviral Arbidol</a></p>

    <p class="NoSpacing">Eve-Isabelle Pécheur, Dimitri Lavillette, Fanny Alcaras, Jennifer Molle, Yury S. Boriskin, Michael Roberts, François-Loïc Cosset, and Stephen J. Polyak</p>

    <p class="NoSpacing"><i>Biochemistry</i> 2007, 46. pp 6050-6059</p>

    <p class="NoSpacing"><strong>Publication Date (Web):</strong> April 25, 2007 (Article)</p>

    <p class="NoSpacing"><strong>DOI:</strong> 10.1021/bi700181j</p> 

    <p class="ListParagraph">14. <a href="http://www.virologyj.com/content/3/1/56">Arbidol: a broad-spectrum antiviral that inhibits acute and chronic HCV infection</a>Yury S Boriskin, Eve-Isabelle Pécheur, Stephen J Polyak</p>

    <p class="NoSpacing">Virology Journal 2006, <b>3</b>:56 (19 July 2006)</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
