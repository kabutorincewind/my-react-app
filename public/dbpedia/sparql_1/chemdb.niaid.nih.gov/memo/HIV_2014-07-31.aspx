

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-07-31.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/zmDuWbbaakhIallNGO/WtWGkr6jjV6oa9hBGKFOsdmjMwUGAHGYaiyzuHr+9bjO7Uz/eezbGIM6e5obF3+vXdmPvFfqzswYZOKEjzDsMSzVKeNSwQwij13SOm4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A012D77D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 18 - July 31, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24878961">A Cell-penetrating Antibody Fragment against HIV-1 Rev has High Antiviral Activity: Characterization of the Paratope</a>.Zhuang, X., S.J. Stahl, N.R. Watts, M.A. DiMattia, A.C. Steven, and P.T. Wingfield. The Journal ofBiologicalChemistry, 2014. 289(29): p. 20222-20233. PMID[24878961].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24927054">The Use of Hairpin DNA Duplexes as HIV-1 Fusion Inhibitors: Synthesis, Characterization, and Activity Evaluation.</a> Xu, L., X. Jiang, X. Xu, B. Zheng, X. Chen, T. Zhang, F. Gao, L. Cai, M. Cheng, and L. Keliang. European Journal of Medicinal Chemistry, 2014. 82: p. 341-346. PMID[24927054]. <b>[PubMed]</b>. HIV_0718-073114.</p>

    <p class="plaintext">                                                                             </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25034436">The Role of A-kinase Anchoring Protein 95-Like Protein in Annealing of tRNALys3 to HIV-1 RNA.</a> Xing, L., X. Zhao, F. Guo, and L. Kleiman. Retrovirology, 2014. 11(1): p. 58. PMID[25034436].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24976398">Down-regulation of Mitochondrial Thymidine Kinase 2 and Deoxyguanosine Kinase by Didanosine: Implication for Mitochondrial Toxicities of anti-HIV Nucleoside Analogs.</a> Sun, R., S. Eriksson, and L. Wang. Biochemical and Biophysical Research Communications, 2014. 450(2): p. 1021-1026. PMID[24976398].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24957203">Uralsaponins M-Y, Antiviral Triterpenoid Saponins from the Roots of Glycyrrhiza uralensis.</a> Song, W., L. Si, S. Ji, H. Wang, X.M. Fang, L.Y. Yu, R.Y. Li, L.N. Liang, D. Zhou, and M. Ye. Journal of Natural Products, 2014. 77(7): p. 1632-1643. PMID[24957203].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25072692">A Conserved Target Site in HIV-1 Gag RNA Is Accessible to Inhibition by Both an HDV Ribozyme and a Short Hairpin RNA.</a> Scarborough, R.J., M.V. Levesque, E. Boudrias-Dalle, I.C. Chute, S.M. Daniels, R.J. Ouellette, J.P. Perreault, and A. Gatignol. Molecular Therapy. Nucleic Acids, 2014. 3: p. e178. PMID[25072692].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">7. Combined <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24792351">Ligand and Structure-based Approaches on HIV-1 Integrase Strand Transfer Inhibitors.</a> Reddy, K.K. and S.K. Singh. Chemico-Biological Interactions, 2014. 218: p. 71-81. PMID[24792351]. <b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25056631">Dolabelladienols A-C, New Diterpenes Isolated from Brazilian Brown Alga Dictyota pfaffii.</a> Pardo-Vargas, A., I. de Barcelos Oliveira, P.R. Stephens, C.C. Cirne-Santos, I.C. de Palmer Paixao, F.A. Ramos, C. Jimenez, J. Rodriguez, J.A. Resende, V.L. Teixeira, and L. Castellanos. Marine Drugs, 2014. 12(7): p. 4247-4259. PMID[25056631].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25036364">Surfactant Protein D Inhibits HIV-1 Infection of Target Cells via Interference with gp120-CD4 Interaction and Modulates Pro-inflammatory Cytokine Production.</a> Pandit, H., S. Gopal, A. Sonawani, A.K. Yadav, A.S. Qaseem, H. Warke, A. Patil, R. Gajbhiye, V. Kulkarni, M.A. Al-Mozaini, S. Idicula-Thomas, U. Kishore, and T. Madan. PloS One, 2014. 9(7): p. e102395. PMID[25036364].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24952305">Design and Synthesis of a New Series of Modified CH-Diarylpyrimidines as Drug-resistant HIV Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Meng, G., Y. Liu, A. Zheng, F. Chen, W. Chen, E. De Clercq, C. Pannecouque, and J. Balzarini. European Journal of Medicinal Chemistry, 2014. 82: p. 600-611. PMID[24952305].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25033210">Effects of Treatment with Suppressive Combination Antiretroviral Drug Therapy and the Histone Deacetylase Inhibitor Suberoylanilide hydroxamic acid; (SAHA) on SIV-infected Chinese Rhesus macaques.</a> Ling, B., M. Piatak, Jr., L. Rogers, A.M. Johnson, K. Russell-Lodrigue, D.J. Hazuda, J.D. Lifson, and R.S. Veazey. PloS One, 2014. 9(7): p. e102795. PMID[25033210].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24904063">Allosteric Inhibition of Human Immunodeficiency Virus Integrase: Late Block During Viral Replication and Abnormal Multimerization Involving Specific Protein Domains.</a> Gupta, K., T. Brady, B.M. Dyer, N. Malani, Y. Hwang, F. Male, R.T. Nolte, L. Wang, E. Velthuisen, J. Jeffrey, G.D. Van Duyne, and F.D. Bushman. Journal of  BiologicalChemistry, 2014. 289(30): p. 20477-20488. PMID[24904063].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24990206">Anti-HIV Small-molecule Binding in the Peptide Subpocket of the CXCR4:CVX15 Crystal Structure.</a></p>

    <p class="plaintext">Cox, B.D., A.R. Prosser, B.M. Katzman, A.A. Alcaraz, D.C. Liotta, L.J. Wilson, and J.P. Snyder. ChemBioChem, 2014. 15(11): p. 1614-1620. PMID[24990206].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24954297">Regio-selective Chemical-Enzymatic Synthesis of Pyrimidine Nucleotides Facilitates RNA Structure and Dynamics Studies.</a> Alvarado, L.J., R.M. LeBlanc, A.P. Longhini, S.C. Keane, N. Jain, Z.F. Yildiz, B.S. Tolbert, V.M. D&#39;Souza, M.F. Summers, C. Kreutz, and T.K. Dayie. ChemBioChem, 2014. 15(11): p. 1573-1577. PMID[24954297].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0718-073114.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338016200003">Anti-HIV Host Factor SAMHD1 Regulates Viral Sensitivity to Nucleoside Reverse Transcriptase Inhibitors via Modulation of Cellular Deoxyribonucleoside Triphosphate (DNTP) Levels.</a> Amie, S.M., M.B. Daly, E. Noble, R.F. Schinazi, R.A. Bambara, and B. Kim. Journal of Biological Chemistry, 2014. 289(24): p. 16640-16640. ISI[000338016200003].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337696600162">Dolutegravir: A New Integrase Strand Transfer Inhibitor for the Treatment of HIV.</a> Shah, B.M. Pharmacotherapy, 2014. 34(6): p. 648-648. ISI[000337696600162].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338118900010">MicroRNA Binding to the HIV-1 Gag Protein Inhibits Gag Assembly and Virus Production.</a> Chen, A.K., P. Sengupta, K. Waki, E.S.B. Van, T. Ochiya, S.D. Ablan, E.O. Freed, and J. Lippincott-Schwartz. Proceedings of the NationalAcademy ofSciences of the United States of America, 2014. 111(26): p. E2676-E2683. ISI[000338118900010].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338109600006">Pnapuercleophilic Addition of Lewis Acid Complexed Alpha-Amino Carbanions to Arynes: Synthesis of 1-Aryl-N-methyl-1,2,3,4-tetrahydroisoquinolines.</a> Singh, K.N., P. Singh, E. Sharma, and Y.S. Deol. Synthesis, 2014. 46(13): p. 1739-1750. ISI[000338109600006].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338184500106">Broad-spectrum Antiviral Property of Polyoxometalate Localized on a Cell Surface.</a> Wang, J., Y. Liu, K. Xu, Y.F. Qi, J. Zhong, K. Zhang, J. Li, E.B. Wang, Z.Y. Wu, and Z.H. Kang. ACS Applied Materials &amp; Interfaces, 2014. 6(12): p. 9785-9789. ISI[000338184500106].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338431500001">The Biology and Synthesis of alpha-Hydroxytropolones.</a> Meck, C., M.P. D&#39;Erasmo, D.R. Hirsch, and R.P. Murelli. MedChemComm, 2014. 5(7): p. 842-852. ISI[000338431500001].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338462600001">Small Molecules That Inhibit Vif-induced Degradation of APOBEC3G.</a> Matsui, M., K. Shindo, T. Izumi, K. Io, M. Shinohara, J. Komano, M. Kobayashi, N. Kadowaki, R.S. Harris, and A. Takaori-Kondo. Virology Journal, 2014. 11(122): 8 pp. ISI[000338462600001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338489400005">Early and Late HIV-1 Membrane Fusion Events are Impaired by Sphinganine Lipidated Peptides That Target the Fusion Site.</a> Klug, Y.A., A. Ashkenazi, M. Viard, Z. Porat, R. Blumenthal, and Y. Shai. Biochemical Journal, 2014. 461: p. 213-222. ISI[000338489400005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0718-073114.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338442600013">A Small Molecule Inhibits Virion Attachment to Heparan Sulfate- or Sialic acid-containing Glycans.</a> Colpitts, C.C. and L.M. Schang. Journal of Virology, 2014. 88(14): p. 7806-7817. ISI[000338442600013].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0718-073114.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000338269900004">Design, Synthesis and Photochemical Reactivation of Caged Prodrugs of 8-Hydroxyquinoline-based Enzyme Inhibitors.</a> Ariyasu, S., Y. Mizuseda, K. Hanaya, and S. Aoki. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(7): p. 642-648. ISI[000338269900004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0718-073114.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
