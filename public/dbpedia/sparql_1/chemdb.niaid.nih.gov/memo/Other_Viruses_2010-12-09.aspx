

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-12-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="grgWXsdnDJQIqLbqzMpYLe3+HXf3VYWDHgCuDLNCTDOdHNSZ5PxbiekKU5trqiWcvKRVTCfuWGCcFbu7cG7ZdCX3lEgC2uWD4zGeCa583u5jxDAzXrHN2GNPuXA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3D8FB452" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">November 24 - December 09, 2010</p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p class="NoSpacing">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/21107017">Anti-Cytomegalovirus Activity of Sulfated Glucans Generated from a Commercial Preparation of Rice Bran</a>. Ghosh, T., S. Auerochs, S. Saha, B. Ray, and M. Marschall.  Antiviral Chemistry &amp; Chemotherapy, 2010. 21(2): p. 85-95; PMID[21107017].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21128666">Novel Antiviral C5-Substituted Pyrimidine Acyclic Nucleoside Phosphonates Selected as Human Thymidylate Kinase Substrates</a>. Topalis, D., U. Pradere, V. Roy, C. Caillat, A. Azzouzi, J. Broggi, R. Snoeck, G. Andrei, J. Lin, S. Eriksson, J.A. Alexandre, C. El-Amri, D. Deville-Bonne, P. Meyer, J. Balzarini, and L.A. Agrofoglio.  Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21128666].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21107018">Helioxanthin Analogue 8-1 Inhibits Duck Hepatitis B Virus Replication in Cell Culture</a>. Ying, C., S. Tan, and Y.C. Cheng.  Antiviral Chemistry &amp; Chemotherapy, 2010. 21(2): p. 97-103; PMID[21107018].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>
    
    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21138791">Molecular Modeling Based Approach, Synthesis and in Vitro Assay to New Indole Inhibitors of Hepatitis C Ns3/4a Serine Protease</a>. Ismail, N.S. and M. Hattori.  Bioorganic &amp; Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21138791].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>
    
    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21135183">Alkylated Porphyrins Have Broad Antiviral Activity against Hepadnaviuses, Flaviviruses, Filoviruses and Arenaviruses</a>. Guo, H., X. Pan, R. Mao, X. Zhang, L. Wang, X. Lu, J. Chang, J.T. Guo, S. Passic, F.C. Krebs, B. Wigdahl, T.K. Warren, C.J. Retterer, S. Bavari, X. Xu, A. Cuconati, and T.M. Block.  Antimicrobial Agents and Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21135183].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>
    
    <p> </p>    

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21105106">Biliverdin Inhibits Hepatitis C Virus Nonstructural 3/4a Protease Activity: Mechanism for the Antiviral Effects of Heme Oxygenase?</a> Zhu, Z., A.T. Wilson, B.A. Luxon, K.E. Brown, M.M. Mathahs, S. Bandyopadhyay, A.P. McCaffrey, and W.N. Schmidt.  Hepatology (Baltimore, Md.), 2010. 52(6): p. 1897-905; PMID[21105106].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OV_1124-120910.</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283621700032">Importance of Ligand Bioactive Conformation in the Discovery of Potent Indole-Diamide Inhibitors of the Hepatitis C Virus Ns5b</a>. LaPlante, S.R., J.R. Gillard, A. Jakalian, N. Aubry, R. Coulombe, C. Brochu, Y.S. Tsantrizos, M. Poirier, G. Kukolj, and P.L. Beaulieu.  Journal of the American Chemical Society, 2010. 132(43): p. 15204-15212; ISI[000283621700032].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_1124-120910.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
