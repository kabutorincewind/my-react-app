

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-09-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4KrCRHHXSypQYme/r6wnf6YwKSNMT0mNWTNvRFuH7lPdLbw8K83HVDkSWUwS5ivcfDi8OYc+zwbgWEdQuWzQeFvGxER7sn5oKs//milxhFbvPuHOd+SaJ+jRnyc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D629B2EC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 30 - September 12, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23843638">Heavy Chain-only IgG2b Llama Antibody Effects Near-Pan HIV-1 Neutralization by Recognizing a CD4-induced Epitope That Includes Elements of Coreceptor- and CD4-binding Sites.</a> Acharya, P., T.S. Luongo, I.S. Georgiev, J. Matz, S.D. Schmidt, M.K. Louder, P. Kessler, Y. Yang, K. McKee, S. O&#39;Dell, L. Chen, D. Baty, P. Chames, L. Martin, J.R. Mascola, and P.D. Kwong. Journal of Virology, 2013. 87(18): p. 10173-10181. PMID[23843638].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23657804">Vaginal Concentrations of Lactic acid Potently Inactivate HIV.</a> Aldunate, M., D. Tyssen, A. Johnson, T. Zakir, S. Sonza, T. Moench, R. Cone, and G. Tachedjian. The Journal of Antimicrobial Chemotherapy, 2013. 68(9): p. 2015-2025. PMID[23657804].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23774297">Progesterone and a Phospholipase Inhibitor Increase the Endosomal bis(Monoacylglycero)phosphate Content and Block HIV Viral Particle Intercellular Transmission.</a> Chapuy-Regaud, S., C. Subra, M. Requena, P. de Medina, S. Amara, I. Delton-Vandenbroucke, B. Payre, M. Cazabat, F. Carriere, J. Izopet, M. Poirot, and M. Record. Biochimie, 2013. 95(9): p. 1677-1688. PMID[23774297].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24020926">The PKR Activator, PACT, Becomes a PKR Inhibitor During HIV-1 Replication.</a> Clerzius, G., E. Shaw, A. Daher, S. Burugu, J.F. Gelinas, T. Ear, L. Sinck, J.P. Routy, A.J. Mouland, R.C. Patel, and A. Gatignol. Retrovirology, 2013. 10(1): p. 96. PMID[24020926].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23994876">Characterization of Permeability, Stability and anti-HIV-1 Activity of Decitabine and Gemcitabine Divalerate Prodrugs.</a> Clouser, C.L., L. Bonnac, L.M. Mansky, and S.E. Patterson. Antiviral Chemistry &amp; Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23994876].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23947685">Highly Potent HIV-1 Protease Inhibitors with Novel Tricyclic P2 Ligands: Design, Synthesis, and Protein-ligand X-ray Studies.</a> Ghosh, A.K., G.L. Parham, C.D. Martyr, P.R. Nyalapatla, H.L. Osswald, J. Agniswamy, Y.F. Wang, M. Amano, I.T. Weber, and H. Mitsuya. Journal of Medicinal Chemistry, 2013. 56(17): p. 6792-6802. PMID[23947685].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23911197">Modular Construction of Quaternary Hemiaminal-based Inhibitor Candidates and Their in Cellulo Assessment with HIV-1 Protease.</a> Gros, G., L. Martinez, A.S. Gimenez, P. Adler, P. Maurin, R. Wolkowicz, P. Falson, and J. Hasserodt. Bioorganic &amp; Medicinal Chemistry, 2013. 21(17): p. 5407-5413. PMID[23911197].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23757338">Interferon Alfa Partially Inhibits HIV Replication in Hepatocytes in Vitro.</a> Kong, L. and J.T. Blackard. The Journal of Infectious Diseases, 2013. 208(5): p. 865-866. PMID[23757338].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23510780">Inhibition of CXCR4 Expression by Recombinant Adenoviruses Containing Anti-sense RNA Resists HIV-1 Infection on MT4 Cell Lines.</a> Li, W.G., W.M. Nie, W.W. Chen, T.J. Jiang, X.Y. Xu, and M. Zhao. Gene, 2013. 526(2): p. 443-448. PMID[23510780].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23774428">Activity of the HIV-1 Attachment Inhibitor BMS-626529, the Active Component of the Prodrug BMS-663068, against CD4-independent Viruses and HIV-1 Envelopes Resistant to Other Entry Inhibitors.</a> Li, Z., N. Zhou, Y. Sun, N. Ray, M. Lataillade, G.J. Hanna, and M. Krystal. Antimicrobial Agents and Chemotherapy, 2013. 57(9): p. 4172-4180. PMID[23774428].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23906421">Increase of anti-HIV Activity of C-peptide Fusion Inhibitors Using a Bivalent Drug Design Approach.</a> Ling, Y., H. Xue, X. Jiang, L. Cai, and K. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4770-4773. PMID[23906421].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24012370">Substrate Envelope-designed Potent HIV-1 Protease Inhibitors to Avoid Drug Resistance.</a> Nalam, M.N., A. Ali, G.S. Reddy, H. Cao, S.G. Anjum, M.D. Altman, N.K. Yilmaz, B. Tidor, T.M. Rana, and C.A. Schiffer. Chemistry &amp; Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24012370].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24026672">Generation of a Replication-competent Simianhuman Immunodeficiency Virus, the Neutralisation Sensitivity of Which Can Be Enhanced in the Presence of a Small Molecule CD4 Mimic.</a> Otsuki, H., T. Hishiki, T. Miura, C. Hashimoto, T. Narumi, H. Tamamura, K. Yoshimura, S. Matsushita, and T. Igarashi. The Journal of General Virology, 2013. <b>[Epub ahead of print]</b>. PMID[24026672].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23988154">SIV Replication Is Directly Downregulated by Four Antiviral miRNAs.</a> Sisk, J.M., K.W. Witwer, P.M. Tarwater, and J.E. Clements. Retrovirology, 2013. 10(1): p. 95. PMID[23988154].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23769242">17&#946;-Estradiol Inhibits HIV-1 by Inducing a Complex Formation between &#946;-Catenin and Estrogen Receptor Alpha on the HIV Promoter to Suppress HIV Transcription.</a> Szotek, E.L., S.D. Narasipura, and L. Al-Harthi. Virology, 2013. 443(2): p. 375-383. PMID[23769242].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24002090">Roles of the K101E Substitution in HIV-1 Reverse Transcriptase in Resistance to Rilpivirine and Other Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Xu, H.T., S.P. Colby-Germinario, W. Huang, M. Oliveira, Y. Han, Y. Quan, C.J. Petropoulos, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24002090].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0830-091213.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322316700015">A Multi-targeted Drug Candidate with Dual anti-HIV and anti-HSV Activity.</a> Balzarini, J., G. Andrei, E. Balestra, D. Huskens, C. Vanpouille, A. Introini, S. Zicari, S. Liekens, R. Snoeck, A. Holy, C.F. Perno, L. Margolis, and D. Schols. Plos Pathogens, 2013. 9(7): p. e1003456. ISI[000322316700015].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321590200048">Replication Fitness of Multiple Nonnucleoside Reverse Transcriptase-resistant HIV-1 Variants in the Presence of Etravirine Measured by 454 Deep Sequencing.</a> Brumme, C.J., K.D. Huber, W. Dong, A.F.Y. Poon, P.R. Harrigan, and N. Sluis-Cremer. Journal of Virology, 2013. 87(15): p. 8805-8807. ISI[000321590200048].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322343700007">Lamivudine Salts with 1,2-Dicarboxylic acids: A New and a Rare Synthon with Double Pairing Motif Fine-tuning Their Solubility.</a> da Silva, C.C., M.D. Cirqueira, and F.T. Martins. CrystEngComm, 2013. 15(32): p. 6311-6317. ISI[000322343700007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322108300009">Synthesis and NMR Elucidation of Novel Pentacycloundecane-derived Peptides.</a> Karpoormath, R., O.K. Onajole, T. Naicker, T. Govender, G.E.M. Maguire, and H.G. Kruger. South African Journal of Chemistry, 2012. 65: p. 108-114. ISI[000322108300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321590200024">Selection of Drug-resistant Feline Immunodeficiency Virus (FIV) Encoding FIV/HIV Chimeric Protease in the Presence of HIV-specific Protease Inhibitors.</a> Lin, Y.C., M. Happer, and J.H. Elder. Journal of Virology, 2013. 87(15): p. 8524-8534. ISI[000321590200024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321590200047">Emergence of gp120 V3 Variants Confers Neutralization Resistance in an R5 Simian-Human Immunodeficiency Virus-infected Macaque Elite Neutralizer That Targets the N332 Glycan of the Human Immunodeficiency Virus Type 1 Envelope Glycoprotein.</a> Sadjadpour, R., O.K. Donau, M. Shingai, A. Buckler-White, S. Kao, K. Strebel, Y. Nishimura, and M.A. Martin. Journal of Virology, 2013. 87(15): p. 8798-8804. ISI[000321590200047].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323369500001">Ubiquitin Conjugation to Gag Is Essential for ESCRT-mediated HIV-1 Budding.</a> Sette, P., K. Nagashima, R.C. Piper, and F. Bouamr. Retrovirology, 2013. 10 (1): p. 79. ISI[000323369500001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322535600041">Retrovirus Restriction by TRIM5 Proteins Requires Recognition of Only a Small Fraction of Viral Capsid Subunits.</a> Shi, J., D.B. Friedman, and C. Aiken. Journal of Virology, 2013. 87(16): p. 9271-9278. ISI[000322535600041].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322903200001">Automethylation of Protein Arginine Methyltransferase 6 (PRMT6) Regulates Its Stability and Its anti-HIV-1 Activity.</a> Singhroy, D.N., T. Mesplede, A. Sabbah, P.K. Quashie, J.P. Falgueyret, and M.A. Wainberg. Retrovirology, 2013. 10. ISI[000322903200001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322891000002">Analog of Somatostatin Vapreotide Exhibits Biological Effects in Vitro Via Interaction with Neurokinin-1 Receptor.</a> Spitsin, S., F. Tuluc, J. Meshki, J.P. Lai, R. Tustin, and S.D. Douglas. NeuroImmunoModulation, 2013. 20(5): p. 247-255. ISI[000322891000002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323178100011">New 23-Spirocholestane Derivatives from Ypsilandra thibetica.</a> Xie, B.B., C.X. Chen, Y.H. Guo, Y.Y. Li, Y.J. Liu, W. Ni, L.M. Yang, N.B. Gong, Y.T. Zheng, R.R. Wang, Y. Lu, and H.Y. Liu. Planta Medica, 2013. 79(12): p. 1063-1067. ISI[000323178100011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322809900001">Novel Intravaginal Nanomedicine for the Targeted Delivery of Saquinavir to CD4(+) Immune Cells.</a> Yang, S.D., Y.F. Chen, K.E. Gu, A. Dash, C.L. Sayre, N.M. Davies, and E.A. Ho. International Journal of Nanomedicine, 2013. 8: p. 2847-2858. ISI[000322809900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322850100014">Molecular Design, Synthesis and Biological Evaluation of BP-O-DAPY and O-DAPY Derivatives as Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Yang, S.Q., C. Pannecouque, D. Daelemans, X.D. Ma, Y. Liu, F.E. Chen, and E. De Clercq. European Journal of Medicinal Chemistry, 2013. 65: p. 134-143. ISI[000322850100014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0830-091213.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
