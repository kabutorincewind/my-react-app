

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-75.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dyKhffB2U0zQGYbMUJPuTTifu9Ybr5/vJpzgYa+EXkfnkoBywU/XlV9ycIguMNiUyv09X/rkvIKhGDNt/rk0w5feAfTKnraCkGpLDa2Gu3sgA6ESHN7ToP5SykA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8922D7A7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-75-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6375   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Synthesis of Biln 2061, an Hcvns3 Protease Inhibitor With Proven Antiviral Effect in Humans</p>

    <p>          Faucher, A. <i>et al.</i></p>

    <p>          Organic Letters <b>2004</b>.  6(17): 2901-2904</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         6376   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Fatal disseminated mouse adenovirus type 1 infection in mice lacking B cells or Bruton&#39;s tyrosine kinase</p>

    <p>          Moore, Martin L, McKissic, Erin L, Brown, Corrie C, Wilkinson, John E, and Spindler, Katherine R</p>

    <p>          Journal of Virology <b>2004</b>.  78(11): 5584-5590</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>3.         6377   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          A Phase I/Ii Dose Escalation Trial Assessing Tolerance, Pharmacokinetics, and Antiviral Activity of Nm283, a Novel Antiviral Treatment for Hepatitis C</p>

    <p>          Godofsky, E. <i>et al.</i></p>

    <p>          Gastroenterology <b>2004</b>.  126(4): A681</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>4.         6378   DMID-LS-75; PUBMED-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Evaluation of sulfated fungal beta-glucans from the sclerotium of Pleurotus tuber-regium as a potential water-soluble anti-viral agent</p>

    <p>          Zhang, M, Cheung, PC, Ooi, VE, and Zhang, L</p>

    <p>          Carbohydr Res <b>2004</b>.  339(13): 2297-301</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15337458&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15337458&amp;dopt=abstract</a> </p><br />

    <p> </p>

    <p>5.         6379   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Pegylated Interferon (Alpha-2b) and Ribavirin (Rebetol) for the Treatment of Hcv Infected Patients Who Failed Prior Therapy: Sustained Response Data</p>

    <p>          Gaglio, P. <i>et al.</i></p>

    <p>          Gastroenterology <b>2004</b>.  126(4): A721</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>6.         6380   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p><b>          Antiviral Properties of Pegylated Versus Non-Pegylated Interferon Alpha-2b Against Hepatitis C in Cell Culture</b> </p>

    <p>          Joshi, V. <i>et al.</i></p>

    <p>          Gastroenterology <b>2004</b>.  126(4): A760</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>7.         6381   DMID-LS-75; PUBMED-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Safety and immunogenicity of CPG 7909 injection as an adjuvant to Fluarix influenza vaccine</p>

    <p>          Cooper, CL, Davis, HL, Morris, ML, Efler, SM, Krieg, AM, Li, Y, Laframboise, C, Al, Adhami MJ, Khaliq, Y, Seguin, I, and Cameron, DW</p>

    <p>          Vaccine <b>2004</b>.  22(23-24): 3136-43</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297066&amp;dopt=abstract</a> </p><br />

    <p>8.         6382   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Sulfated Seaweed Polysaccharides as Antiviral Agents</p>

    <p>          Damonte, E., Matulewicz, M., and Cerezo, A.</p>

    <p>          Current Medicinal Chemistry <b>2004</b>.  11(18): 2399-2419</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.         6383   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Synthesis of N-1-Alkylated 6-Benzyluracil-5-Carboxylic Esters as Potential Non-Nucleoside Reverse Transcriptase Inhibitors</p>

    <p>          Larsen, J., Pedersen, E., and Nielsen, C.</p>

    <p>          Synthesis-Stuttgart <b>2004</b>.(11): 1874-1878</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>10.       6384   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Peroxynitrite Inhibition of Coxsackievirus Infection by Prevention of Viral Rna Entry</p>

    <p>          Padalko, E. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2004</b>.  101(32): 11731-11736</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>11.       6385   DMID-LS-75; PUBMED-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Human Cytomegalovirus UL76 Encodes a Novel Virion-Associated Protein That Is Able To Inhibit Viral Replication</p>

    <p>          Wang, SK, Duh, CY, and Wu, CW</p>

    <p>          J Virol <b>2004</b>.  78(18): 9750-62</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15331708&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15331708&amp;dopt=abstract</a> </p><br />

    <p>12.       6386   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Anticancer and Antiviral Activities of Youngia Japonica (L.) Dc (Asteraceae, Compositae)</p>

    <p>          Ooi, L. <i>et al.</i></p>

    <p>          Journal of Ethnopharmacology <b>2004</b>.  94(1): 117-122</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>13.       6387   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          New Antivirals and Antiviral Resistance</p>

    <p>          Prober, CG  549: 9-12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>14.       6388   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Neumann, Gabriele, Noda, Takeshi, Takada, Ayato, Jasenosky, Luke D, and Kawaoka, Yoshihiro</p>

    <p>          &lt;10 Journal Title&gt; <b>2004</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.       6389   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Preparation of thiophenecarboxylates for the treatment or prevention of flavivirus infections</p>

    <p>          Chan Chun Kong, Laval, Das, Sanjoy Kumar, Nguyen-Ba, Nghe, Halab, Liliane, Hamelin, Bettina, Pereira, Oswy Z, Poisson, Carl, Proulx, Melanie, Reddy, Thumkunta Jagadeeswar, and Zhang, Ming-qiang</p>

    <p>          PATENT:  WO <b>2004052885</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 192 pp.</p>

    <p>          ASSIGNEE:  (Virochem Pharma Inc., Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>16.       6390   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Treatment of Flaviviridae infection with 2&#39;-branched nucleosides and another mutation-inducing drug such as interferon</p>

    <p>          Sommadossi, Jean-Pierre, La Colla, Paolo, Standring, David, Bichko, Vadim, and Qu, Lin</p>

    <p>          PATENT:  WO <b>2004046331</b>  ISSUE DATE:  20040603</p>

    <p>          APPLICATION: 2003  PP: 166 pp.</p>

    <p>          ASSIGNEE:  (Idenix (Cayman) Limited, Cayman I. and Universita Degli Studi Di Cagliari)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       6391   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Evaluation of Scopadulciol-Related Molecules for Their Stimulatory Effect on the Cytotoxicity of Acyclovir and Ganciclovir Against Herpes Simplex Virus Type 1 Thymidine Kinase Gene-Transfected Hela Cells</p>

    <p>          Hayashi, K. <i>et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2004</b>.  52(8): 1015-1017</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.       6392   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Agents with leukotriene B4-like antiviral (DNA) and anti-neoplastic activities</p>

    <p>          Gosselin, Jean and Borgeat, Pierre</p>

    <p>          PATENT:  US <b>2004132820</b>  ISSUE DATE:  20040708</p>

    <p>          APPLICATION: 2003-28522  PP: 26 pp., Cont.-in-part of U.S. Ser. No. 548,187, abandoned.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>19.       6393   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Process for preparation of pyrrolylquinones and indolylquinones and their use for treating neurodegenerative disease, viral infections and proliferative disease</p>

    <p>          Pirrung, Michael C and Rudolph, Johannes</p>

    <p>          PATENT:  US <b>2004063774</b>  ISSUE DATE:  20040401</p>

    <p>          APPLICATION: 2002-59289  PP: 25 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       6394   DMID-LS-75; WOS-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Using Antiviral Drugs for Orthopoxvirus Infections</p>

    <p>          Snoeck, R., De Clercq, E., and Andrei, G.</p>

    <p>          Medecine Et Maladies Infectieuses <b>2004</b>.  34: S48-S50</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>21.       6395   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Synthesis and structure-activity relationships of the halovirs, antiviral natural products from a marine-derived fungus</p>

    <p>          Rowley, David C, Kelly, Sara, Jensen, Paul, and Fenical, William</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  12(18): 4929-4936</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>22.       6396   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Metabolites produced by actinomycetes. Antiviral antibiotics and enzyme inhibitors</p>

    <p>          Uyeda, Masaru</p>

    <p>          Yakugaku Zasshi <b>2004</b>.  124(8): 469-480</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.       6397   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral effects of aciclovir injectable formulation (Acirovec and Zovirax for i.v. infusion) against herpes simplex virus type 1 in vitro and in vivo</p>

    <p>          Ogura, Takeharu, Ohtsubo, Yoshikazu, Ueda, Haruyasu, and Okamura, Haruki</p>

    <p>          Japanese Pharmacology &amp; Therapeutics <b>2004</b>.  32(6): 349-352</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       6398   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Anti-viral activity of norfloxacin and ampicillin and dibutyltin polymers derived from norfloxacin and ampicillin against reovirus ST3, vaccinia virus, herpes simplex virus (HSV-1) and varicella zoster</p>

    <p>          Roner, Michael R, Carraher, Charles E Jr, Roehr, Joanne L, Bassett, Kelly D, and Siegmann-Louda, Deborah W</p>

    <p>          Polymeric Materials: Science and Engineering <b>2004</b>.  91: 744-746</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       6399   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Preparation of isoxazole-containing thiourea inhibitors useful for treatment of varicella zoster virus</p>

    <p>          Bloom, Jonathan David</p>

    <p>          PATENT:  US <b>2004157900</b>  ISSUE DATE:  20040812</p>

    <p>          APPLICATION: 2004-51903  PP: 9 pp.</p>

    <p>          ASSIGNEE:  (Wyeth Holdings Corporation, USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       6400   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          A preparation of thieno[3,2-b]pyridinecarboxamide derivatives, useful as antiviral agents</p>

    <p>          Larsen, Scott D, May, Paul, Romines, Karen, Schnute, Mark E, and Tanis, Steven P</p>

    <p>          PATENT:  US <b>2004138449</b>  ISSUE DATE:  20040715</p>

    <p>          APPLICATION: 2003-17415  PP: 35 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       6401   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Methods and compositions for the treatment of herpes virus infections using cyclooxygenase-2 selective inhibitors or cyclooxygenase-2 inhibitors in combination with antiviral agents</p>

    <p>          Maziasz, Timothy</p>

    <p>          PATENT:  WO <b>2004056349</b>  ISSUE DATE:  20040708</p>

    <p>          APPLICATION: 2003  PP: 155 pp.</p>

    <p>          ASSIGNEE:  (Pharmacia Corporation, USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>28.       6402   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Antimicrobial activity of antibodies producing reactive oxygen species</p>

    <p>          Wentworth, Paul and Lerner, Richard A</p>

    <p>          PATENT:  WO <b>2004044191</b>  ISSUE DATE:  20040527</p>

    <p>          APPLICATION: 2003  PP: 131 pp.</p>

    <p>          ASSIGNEE:  (Novartis A.-G., Switz., Novartis Pharma G.m.b.H., and The Scripps Research Institute)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>29.       6403   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides targeting HBV</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  US <b>20040162253</b>  ISSUE DATE: 20040819</p>

    <p>          APPLICATION: 2003-5728  PP: 84 pp., Cont.-in-part of WO 2004 24,919.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>30.       6404   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides targeting HSV and CMV</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  US <b>20040162254</b>  ISSUE DATE: 20040819</p>

    <p>          APPLICATION: 2003-5737  PP: 84 pp., Cont.-in-part of WO 2004 24,919.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. Can.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       6405   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Mechanisms of Host Defense following Severe Acute Respiratory Syndrome-Coronavirus (SARS-CoV) Pulmonary Infection of Mice</p>

    <p>          Glass, William G, Subbarao, Kanta, Murphy, Brian, and Murphy, Philip M</p>

    <p>          Journal of Immunology <b>2004</b>.  173(6): 4030-4039</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>32.       6406   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          In vitro inhibition of severe acute respiratory syndrome coronavirus by chloroquine</p>

    <p>          Keyaerts, Els, Vijgen, Leen, Maes, Piet, Neyts, Johan, and Van Ranst, Marc</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  323(1): 264-268</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.       6407   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Antiviral medicament and method for producing and using the same for the prophylactic and therapeutic treatment of papillomavirus induced tumors, lesions and deseases</p>

    <p>          Albahri, Tareq Abduljalil</p>

    <p>          PATENT:  US <b>2004109899</b>  ISSUE DATE:  20040610</p>

    <p>          APPLICATION: 2002-53302  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (Kuwait)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       6408   DMID-LS-75; SCIFINDER-DMID-9/14/2004</p>

    <p class="memofmt1-2">          Pathogen-host standoff Immunity to polyomavirus infection and neoplasia</p>

    <p>          Lukacher, Aron E</p>

    <p>          Immunologic Research <b>2004</b>.  29(1-3): 139-150</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
