

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-330.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vnxXGvAsR5fnpNVd/BbOSUev75ZIAfm4XKfJgDLOUvRKKHUkRgXbjEIMcgY9f0ZKlGaPCAsPSa4Dp8Ox74E+8aex4Q896HI4SQOejVBo2F3bp9AkndZZojWMVBk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6AE2970F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-330-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47840   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of sesquiterpene lactones and analogues in the subgenomic HCV replicon system</p>

    <p>          Hwang, DR, Wu, YS, Chang, CW, Lien, TW, Chen, WC, Tan, UK, Hsu, JT, and Hsieh, HP</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140536&amp;dopt=abstract</a> </p><br />

    <p>2.     47841   OI-LS-330; EMBASE-OI-9/6/2005</p>

    <p class="memofmt1-2">          Novel 5-substituted, 2,4-diaminofuro[2,3-d]pyrimidines as multireceptor tyrosine kinase and dihydrofolate reductase inhibitors with antiangiogenic and antitumor activity</p>

    <p>          Gangjee, Aleem, Zeng, Yibin, Ihnat, Michael, Warnke, Linda A, Green, Dixy W, Kisliuk, Roy L, and Lin, Fu-Tyan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(18): 5475-5491</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4GP1VBK-5/2/6d0c41bddec4a53b46a4818be82fe4d0">http://www.sciencedirect.com/science/article/B6TF8-4GP1VBK-5/2/6d0c41bddec4a53b46a4818be82fe4d0</a> </p><br />

    <p>3.     47842   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Triketones active against antibiotic-resistant bacteria: Synthesis, structure-activity relationships, and mode of action</p>

    <p>          van Klink, JW, Larsen, L, Perry, NB, Weavers, RT, Cook, GM, Bremer, PJ, Mackenzie, AD, and Kirikae, T</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140015&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16140015&amp;dopt=abstract</a> </p><br />

    <p>4.     47843   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus replication by pol III-directed overexpression of RNA decoys corresponding to stem-loop structures in the NS5B coding region</p>

    <p>          Zhang, J, Yamada, O, Sakamoto, T, Yoshida, H, Araki, H, Murata, T, and Shimotohno, K</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16139319&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16139319&amp;dopt=abstract</a> </p><br />

    <p>5.     47844   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Structure of the Mycobacterium tuberculosis Flavin Dependent Thymidylate Synthase (MtbThyX) at 2.0A Resolution</p>

    <p>          Sampathkumar, P, Turley, S, Ulmer, JE, Rhie, HG, Sibley, CH, and Hol, WG</p>

    <p>          J Mol Biol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16139296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16139296&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47845   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          In vitro effect of free and complexed indium(III) against Mycobacterium tuberculosis</p>

    <p>          David, S, Barros, V, Cruz, C, and Delgado, R</p>

    <p>          FEMS Microbiol Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16137841&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16137841&amp;dopt=abstract</a> </p><br />

    <p>7.     47846   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          4-Oxo-4,7-dihydrothieno[2,3-b]pyridines as Non-Nucleoside Inhibitors of Human Cytomegalovirus and Related Herpesvirus Polymerases</p>

    <p>          Schnute, ME, Cudahy, MM, Brideau, RJ, Homa, FL, Hopkins, TA, Knechtel, ML, Oien, NL, Pitts, TW, Poorman, RA, Wathen, MW, and Wieber, JL</p>

    <p>          J Med Chem <b>2005</b>.  48(18): 5794-804</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134946&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16134946&amp;dopt=abstract</a> </p><br />

    <p>8.     47847   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          HCV antiviral resistance: The impact of in vitro studies on the development of antiviral agents targeting the viral NS5B polymerase</p>

    <p>          Tomei, L, Altamura, S, Paonessa, G, De, Francesco R, and Migliaccio, G</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(4): 225-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130521&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16130521&amp;dopt=abstract</a> </p><br />

    <p>9.     47848   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Development of a Simple Assay Protocol for High-Throughput Screening of Mycobacterium tuberculosis Glutamine Synthetase for the Identification of Novel Inhibitors</p>

    <p>          Singh, U, Panchanadikar, V, and Sarkar, D</p>

    <p>          J Biomol Screen <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129778&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129778&amp;dopt=abstract</a> </p><br />

    <p>10.   47849   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV and antitubercular activities of lamivudine prodrugs</p>

    <p>          Sriram, D, Yogeeswari, P, and Gopal, G</p>

    <p>          Eur J Med Chem  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129516&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16129516&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   47850   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Role of embB codon 306 mutations in Mycobacterium tuberculosis revisited: a novel association with broad drug resistance and IS6110 clustering rather than ethambutol resistance</p>

    <p>          Hazbon, MH, Bobadilla, del Valle M, Guerrero, MI, Varma-Basil, M, Filliol, I, Cavatore, M, Colangeli, R, Safi, H, Billman-Jacobe, H, Lavender, C, Fyfe, J, Garcia-Garcia, L, Davidow, A, Brimacombe, M, Leon, CI, Porras, T, Bose, M, Chaves, F, Eisenach, KD, Sifuentes-Osornio, J, Ponce, de Leon A, Cave, MD, and Alland, D</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(9): 3794-802</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127055&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16127055&amp;dopt=abstract</a> </p><br />

    <p>12.   47851   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Recent highlights in the development of new antiviral drugs</p>

    <p>          De Clercq, E</p>

    <p>          Curr Opin Microbiol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125443&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16125443&amp;dopt=abstract</a> </p><br />

    <p>13.   47852   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Synthesis and in vitro and in vivo antimycobacterial activity of isonicotinoyl hydrazones</p>

    <p>          Sriram, D, Yogeeswari, P, and Madhu, K</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115763&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16115763&amp;dopt=abstract</a> </p><br />

    <p>14.   47853   OI-LS-330; EMBASE-OI-9/6/2005</p>

    <p class="memofmt1-2">          Chemical compositions and antimicrobial activities of four different Anatolian propolis samples</p>

    <p>          Uzel, Atac, Sorkun, Kadri dot above ye, Oncag, Ozant, Cogulu, Dilsah, Gencay, Omur, and Sali[dot above]h, Beki dot above r</p>

    <p>          Microbiological Research <b>2005</b>.  160(2): 189-195</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7GJ8-4FMBK7V-1/2/e0973abd359c0fbe13c2e0bdd761e92f">http://www.sciencedirect.com/science/article/B7GJ8-4FMBK7V-1/2/e0973abd359c0fbe13c2e0bdd761e92f</a> </p><br />

    <p>15.   47854   OI-LS-330; PUBMED-OI-9/6/2005</p>

    <p class="memofmt1-2">          Design, synthesis, and antiviral activity of 2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine, a potent inhibitor of hepatitis C virus replication</p>

    <p>          Clark, JL, Hollecker, L, Mason, JC, Stuyver, LJ, Tharnish, PM, Lostia, S, McBrayer, TR, Schinazi, RF, Watanabe, KA, Otto, MJ, Furman, PA, Stec, WJ, Patterson, SE, and Pankiewicz, KW</p>

    <p>          J Med Chem <b>2005</b>.  48(17): 5504-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107149&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16107149&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   47855   OI-LS-330; EMBASE-OI-9/6/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus NS3-4A serine protease inhibitors: SAR of  moiety with improved potency</p>

    <p>          Arasappan, A, Njoroge, FG, Chan, T-Y, Bennett, F, Bogen, SL, Chen, K, Gu, H, Hong, L, Jao, E, and Liu, Y-T</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(19): 4180-4184</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4GTW8PX-2/2/a56651787a42464401c41ebdd00674a4">http://www.sciencedirect.com/science/article/B6TF9-4GTW8PX-2/2/a56651787a42464401c41ebdd00674a4</a> </p><br />

    <p>17.   47856   OI-LS-330; EMBASE-OI-9/6/2005</p>

    <p><b>          Synthesis and in vitro anti-mycobacterial activity of 5-substituted pyrimidine nucleosides</b> </p>

    <p>          Johar, Monika, Manning, Tracey, Kunimoto, Dennis Y, and Kumar, Rakesh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H0YYW8-6/2/682339ea66a51907e8967ef97bb57b1a">http://www.sciencedirect.com/science/article/B6TF8-4H0YYW8-6/2/682339ea66a51907e8967ef97bb57b1a</a> </p><br />

    <p>18.   47857   OI-LS-330; EMBASE-OI-9/6/2005</p>

    <p class="memofmt1-2">          Synthesis and biological activity of macrocyclic inhibitors of hepatitis C virus (HCV) NS3 protease</p>

    <p>          Chen, Kevin X, Njoroge, FGeorge, Prongay, Andrew, Pichardo, John, Madison, Vincent, and Girijavallabhan, Viyyoor</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4GX6HWN-4/2/3dae129fa7ef0907b4c4588610305b63">http://www.sciencedirect.com/science/article/B6TF9-4GX6HWN-4/2/3dae129fa7ef0907b4c4588610305b63</a> </p><br />

    <p>19.   47858   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Secondary metabolites from Chamaedora tepejilote (Palmae) are active against Mycobacterium tuberculosis</p>

    <p>          Jimenez, A, Meckes, M, Alvarez, V, Torres, J, and Parra, R</p>

    <p>          PHYTOTHERAPY RESEARCH <b>2005</b>.  19(4): 320-322, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230969200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230969200012</a> </p><br />

    <p>20.   47859   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Unravelling hepatitis C virus replication from genome to function</p>

    <p>          Lindenbach, BD and Rice, CM</p>

    <p>          NATURE <b>2005</b>.  436(7053): 933-938, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900034</a> </p><br />

    <p>21.   47860   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Challenges and successes in developing new therapies for hepatitis C</p>

    <p>          De Francesco, R and Migliaccio, G</p>

    <p>          NATURE <b>2005</b>.  436(7053): 953-960, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900037</a> </p><br />

    <p>22.   47861   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Mechanism of action of interferon and ribavirin in treatment of hepatitis C</p>

    <p>          Feld, JJ and Hoofnagle, JH</p>

    <p>          NATURE <b>2005</b>.  436(7053): 967-972, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231263900039</a> </p><br />

    <p>23.   47862   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Inhibition of protein kinases C prevents murine cytomegalovirus replication</p>

    <p>          Kucic, N, Mahmutefendic, H, and Lucin, P</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2005</b>.  86: 2153-2161, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231033300004</a> </p><br />

    <p>24.   47863   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Rv2358 and FurB: Two transcriptional regulators from Mycobacterium tuberculosis which respond to zincl</p>

    <p>          Canneva, F, Branzoni, M, Riccardi, G, Provvedi, R, and Milano, A</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(16): 5837-5840, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231026200038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231026200038</a> </p><br />

    <p>25.   47864   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          BHIVA treatment guidelines for tuberculosis (TB)/HIV infection 2005</p>

    <p>          Pozniak, AL, Miller, RF, Lipman, MCI, Freedman, AR, Ormerod, LP, Johnson, MA, Collins, S, and Lucas, SB</p>

    <p>          HIV MEDICINE <b>2005</b>.  6: 62-83, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230966300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230966300002</a> </p><br />

    <p>26.   47865   OI-LS-330; WOS-OI-8/28/2005</p>

    <p class="memofmt1-2">          Pharmacodynamic evidence that ciprofloxacin failure against tuberculosis is not due to poor microbial kill but to rapid emergence of resistance</p>

    <p>          Gumbo, T, Louie, A, Deziel, MR, and Drusano, GL</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(8): 3178-3181, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230950700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230950700012</a> </p><br />

    <p>27.   47866   OI-LS-330; WOS-OI-9/6/2005</p>

    <p class="memofmt1-2">          Preparation, surface-active properties, and antimicrobial activities of bis-quaternary ammonium salts from amines and epichlorohydrin</p>

    <p>          Chlebicki, J, Wegrzynska, J, Maliszewska, I, and Oswiecimska, M</p>

    <p>          JOURNAL OF SURFACTANTS AND DETERGENTS <b>2005</b>.  8(3): 227-232, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231078700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231078700001</a> </p><br />

    <p>28.   47867   OI-LS-330; WOS-OI-9/6/2005</p>

    <p><b>          Hepatitis C virus NS2/3 processing is required for NS3 stability and viral RNA replication</b> </p>

    <p>          Welbourn, S, Green, R, Gamache, I, Dandache, S, Lohmann, V, Bartenschlager, R, Meerovitch, K, and Pause, A</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(33): 29604-29611, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231176200028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231176200028</a> </p><br />
    <br clear="all">

    <p>29.   47868   OI-LS-330; WOS-OI-9/6/2005</p>

    <p class="memofmt1-2">          Interdomain communication in hepatitis C virus polymerase abolished by small molecule inhibitors bound to a novel allosteric site</p>

    <p>          Di Marco, S, Volpari, C, Tomei, L, Altamura, S, Harper, S, Narjes, F, Koch, U, Rowley, M, De, Francesco R, Migliaccio, G, and Carfi, A</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b> 2005</b>.  280(33): 29765-29770, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231176200046">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231176200046</a> </p><br />

    <p>30.   47869   OI-LS-330; WOS-OI-9/6/2005</p>

    <p class="memofmt1-2">          New alpha-methylene-gamma-butyrolactones with antimycobacterial properties</p>

    <p>          Hughes, MA, McFadden, JM, and Townsend, CA</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(17): 3857-3859, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231186000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231186000009</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
