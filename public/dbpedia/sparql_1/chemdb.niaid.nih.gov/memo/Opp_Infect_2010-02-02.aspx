

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-02-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XDByjzdw60HjUlK1QObOeQUC63xb3zBzzdy/fy6U4dVeu5Rkw0hmOFPw7/5FyxwkF/59fG0zZpAd0dSr/zwJK/G4uMpZTH6F0zZmy6R4IEJID96CpbhIvxN4SO0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FD5E9134" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">January 20 - February 2, 2010</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="memofmt2-h1"> </p>

    <p class="NoSpacing"> </p>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19926173?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=33">Original quinazoline derivatives displaying antiplasmodial properties.</a> Kabri, Y., N. Azas, A. Dumetre, S. Hutter, M. Laget, P. Verhaeghe, A. Gellis, and P. Vanelle. Eur J Med Chem. 45(2): p. 616-22; PMID[19926173].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="NoSpacing"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20118649?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Effects of Combined Addition of Atovaquone and Lithium on the in Vitro Cell Growth of Pathogenic Yeast Candida albicans.</a> Minagawa, N., M. Uehara, S. Seki, A. Nitta, and K. Kogawara. Yakugaku Zasshi. 130(2): p. 247-51; PMID[20118649].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p class="NoSpacing"> </p>

    <p> </p>

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19756586?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=191">Inhibition of Candida albicans growth by brominated furanones.</a> Duo, M., M. Zhang, Y.Y. Luk, and D. Ren. Appl Microbiol Biotechnol. 85(5): p. 1551-63; PMID[19756586].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19882381?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=166">In vitro antifungal activity of the berberine and its synergism with fluconazole.</a> Iwazaki, R.S., E.H. Endo, T. Ueda-Nakamura, C.V. Nakamura, L.B. Garcia, and B.P. Filho. Antonie Van Leeuwenhoek. 97(2): p. 201-5; PMID[19882381].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">5.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19944499?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=277">A microwave-assisted, facile, regioselective Friedlander synthesis and antitubercular evaluation of 2,9-diaryl-2,3-dihydrothieno-[3,2-b]quinolines.</a> Balamurugan, K., V. Jeyachandran, S. Perumal, T.H. Manjashetty, P. Yogeeswari, and D. Sriram. Eur J Med Chem. 45(2): p. 682-8; PMID[19944499].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20123700?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17">Novel Proteasome Inhibitors as Potential Drugs to Combat Tuberculosis.</a> Cheng, Y. and J. Pieters. J Mol Cell Biol. [Epub ahead of print]; PMID[20123700].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20056418?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=211">Design and synthesis of some new quinoline-3-carbohydrazone derivatives as potential antimycobacterial agents.</a> Eswaran, S., A.V. Adhikari, N.K. Pal, and I.H. Chowdhury. Bioorg Med Chem Lett. 20(3): p. 1040-1044; PMID[20056418].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">8.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20099263?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=115">QSAR Modeling of a Set of Pyrazinoate Esters as Antituberculosis Prodrugs.</a> Fernandes, J.P., K.F. Pasqualoto, V.M. Felli, E.I. Ferreira, and C.A. Brandt. Arch Pharm (Weinheim). [Epub ahead of print]; PMID[20099263].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">9.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20000470?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=257">Identification, synthesis, and pharmacological evaluation of tetrahydroindazole based ligands as novel antituberculosis agents.</a> Guo, S., Y. Song, Q. Huang, H. Yuan, B. Wan, Y. Wang, R. He, M.G. Beconi, S.G. Franzblau, and A.P. Kozikowski. J Med Chem. 53(2): p. 649-59; PMID[20000470].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20000577?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=256">Rational design of 5-phenyl-3-isoxazolecarboxylic acid ethyl esters as growth inhibitors of Mycobacterium tuberculosis. a potent and selective series for further drug development.</a> Lilienkampf, A., M. Pieroni, B. Wan, Y. Wang, S.G. Franzblau, and A.P. Kozikowski. J Med Chem. 53(2): p. 678-88; PMID[20000577].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">11.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20022500?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=234">Synthesis and antituberculosis activity of novel mefloquine-isoxazole carboxylic esters as prodrugs.</a> Mao, J., H. Yuan, Y. Wang, B. Wan, D. Pak, R. He, and S.G. Franzblau. Bioorg Med Chem Lett. 20(3): p. 1263-1268; PMID[20022500].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">12.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20118493?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=36">Determination of the susceptibility of Mycobacterium tuberculosis to pyrazinamide in liquid and solid media assessed by a colorimetric nitrate reductase assay.</a> Syre, H., K. Ovreas, and H.M. Grewal. J Antimicrob Chemother. [Epub ahead of print]; PMID[20118493].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p class="plaintext">13.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19926361?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=296">Identification of benzofuro[2,3-b]quinoline derivatives as a new class of antituberculosis agents.</a> Yang, C.L., C.H. Tseng, Y.L. Chen, C.M. Lu, C.L. Kao, M.H. Wu, and C.C. Tzeng. Eur J Med Chem. 45(2): p. 602-7; PMID[19926361].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0121-020410.</p>

    <p> </p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
