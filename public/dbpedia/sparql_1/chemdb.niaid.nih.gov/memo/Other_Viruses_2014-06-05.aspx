

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-06-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KrkNw+Jzo3M4mN9NEa/5Yntb+FXUss6GnqqzAo9dC/6Mm0I2Bs7JaCUPDeo48HLiii+KIAzmf0UZFUjdPyXklxpXewkdULYr+FQzrH/DlytLBKWEwF1ZMaCSvmk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F100785F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: May 23 - June 5, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24685503">Polyacetylenes and anti-Hepatitis B Virus Active Constituents from Artemisia capillaris<i>.</i></a> Zhao, Y., C.A. Geng, C.L. Sun, Y.B. Ma, X.Y. Huang, T.W. Cao, K. He, H. Wang, X.M. Zhang, and J.J. Chen. Fitoterapia, 2014. 95: p. 187-193; PMID[24685503].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24687498">The Combination of Alisporivir Plus an NS5A Inhibitor Provides Additive to Synergistic anti-Hepatitis C Virus Activity without Detectable Cross-resistance<i>.</i></a> Chatterji, U., J.A. Garcia-Rivera, J. Baugh, K. Gawlik, K.A. Wong, W. Zhong, C.A. Brass, N.V. Naoumov, and P.A. Gallay. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3327-3334; PMID[24687498].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24709254">Identification of AP80978, a Novel Small-molecule Inhibitor of Hepatitis C Virus Replication That Targets NA4B<i>.</i></a> Dufner-Beattie, J., A. O&#39;Guin, S. O&#39;Guin, A. Briley, B. Wang, J. Balsarotti, R. Roth, G. Starkey, U. Slomczynska, A. Noueiry, P.D. Olivo, and C.M. Rice. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3399-3410; PMID[24709254].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24795200">ABT-450/r-ombitasvir and Dasabuvir with or without Ribavirin for HCV<i>.</i></a> Ferenci, P., D. Bernstein, J. Lalezari, D. Cohen, Y. Luo, C. Cooper, E. Tam, R.T. Marinho, N. Tsai, A. Nyberg, T.D. Box, Z. Younes, P. Enayati, S. Green, Y. Baruch, B.R. Bhandari, F.A. Caruntu, T. Sepe, V. Chulanov, E. Janczewska, G. Rizzardini, J. Gervain, R. Planas, C. Moreno, T. Hassanein, W. Xie, M. King, T. Podsadecki, and K.R. Reddy. The New England Journal of Medicine, 2014. 370(21): p. 1983-1992; PMID[24795200].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24733465">Preclinical Characterization of BMS-791325, an Allosteric Inhibitor of Hepatitis C Virus NS5B Polymerase<i>.</i></a> Lemm, J.A., M. Liu, R.G. Gentles, M. Ding, S. Voss, L.A. Pelosi, Y.K. Wang, K.L. Rigat, K.W. Mosure, J.A. Bender, J.O. Knipe, R. Colonno, N.A. Meanwell, J.F. Kadow, K.S. Santone, S.B. Roberts, and M. Gao. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3485-3495; PMID[24733465].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24709263">Selective Inhibition of Hepatitis C Virus Infection by Hydroxyzine and Benztropine<i>.</i></a> Mingorance, L., M. Friesland, M. Coto-Llerena, S. Perez-Del-Pulgar, L. Boix, J.M. Lopez-Oliva, J. Bruix, X. Forns, and P. Gastaminza. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3451-3460; PMID[24709263].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24713119">Inhibition of Hepatitis C Virus Infection by NS5A-Specific Aptamer<i>.</i></a> Yu, X., Y. Gao, B. Xue, X. Wang, D. Yang, Y. Qin, R. Yu, N. Liu, L. Xu, X. Fang, and H. Zhu. Antiviral Research, 2014. 106: p. 116-124; PMID[24713119].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24768597">Inhibitors of Signal Peptide Peptidase (SPP) Affect HSV-1 Infectivity in Vitro and in Vivo<i>.</i></a> Allen, S.J., K.R. Mott, and H. Ghiasi. Experimental Eye Research, 2014. 123: p. 8-15; PMID[24768597].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24444941">Phenolic Compounds from Origanum vulgare and Their Antioxidant and Antiviral Activities<i>.</i></a> Zhang, X.L., Y.S. Guo, C.H. Wang, G.Q. Li, J.J. Xu, H.Y. Chung, W.C. Ye, Y.L. Li, and G.C. Wang. Food Chemistry, 2014. 152: p. 300-306; PMID[24444941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0523-060514.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335229500007">HCV GT2, GT3 and GT4 NS5B Polymerases Exhibit Increased Phenotypic Susceptibility to Ribavirin and a Nucleoside Polymerase Inhibitor<i>.</i></a> Anton, E.D., K. Strommen, S. Jauregui, M. Cheng, Y. Tan, A. Rivera, A. Newton, J. Larson, J.M. Whitcomb, C.J. Petropoulos, W. Huang, and J.D. Reeves. Antiviral Therapy, 2013. 18: p. A11-A11; ISI[000335229500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335491000056">Synthesis of Carbohydrate Methyl phosphoramidates<i>.</i></a> Ashmus, R.A. and T.L. Lowary. Organic Letters, 2014. 16(9): p. 2518-2521; ISI[000335491000056].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335229500009">In Vitro Efficacy and Resistance Profiling of Protease Inhibitors against a Novel HCV Genotype 3a Replicon<i>.</i></a> Chan, K., M. Yu, B. Peng, A. Corsa, A. Worth, R. Gong, S. Xu, X. Chen, T. Appleby, J. Taylor, W. Delaney, and G. Cheng. Antiviral Therapy, 2013. 18: p. A13-A13; ISI[000335229500009].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335229500006">Biochemical Characterization of beta-D-2 &#39;-C-Methyl-2,6-diaminopurine-ribonucleoside-5 &#39;-triphosphate as a Potent Inhibitor of Hepatitis C Virus (HCV) Polymerase<i>.</i></a> Ehteshami, M., S. Amiralaei, J.R. Shelton, S. Tao, J.E. Suesserman, H.W. Zhang, L. Zhou, S.J. Coats, and R.F. Schinazi. Antiviral Therapy, 2013. 18: p. A10-A10; ISI[000335229500006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335229500049">Implications of Baseline Polymorphisms for Potential Resistance to NS3 Protease and NS5A Inhibitors in Hepatitis C Virus Genotypes 1a, 2b and 3a<i>.</i></a> Lindstrom, I., N. Palanisamy, M. Kjellin, A. Danielsson, K. Bondeson, and J. Lennerstrand. Antiviral Therapy, 2013. 18: p. A55-A55; ISI[000335229500049].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335491000004">Enantioselective Synthesis of an HCV NS5A Antagonist<i>.</i></a> Mangion, I.K., C.Y. Chen, H.M. Li, P. Maligres, Y.G. Chen, M. Christensen, R. Cohen, I. Jeon, A. Klapars, S. Krska, H. Nguyen, R.A. Reamer, B.D. Sherry, and I. Zavialov. Organic Letters, 2014. 16(9): p. 2310-2313; ISI[000335491000004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br />    

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335229500051">Beta-D-2 &#39;-C-Me-2,6-diaminopurine ribonucleoside phosphoramidates are Potent and Selective Inhibitors of Hepatitis C Virus (HCV) and are Bioconverted Intracellularly to Bioactive 2,6-Diaminopurine- and Guanosine-5 &#39;-triphosphate Forms<i>.</i></a> Schinazi, R.F., L. Zhou, H.W. Zhang, S. Tao, L. Bassit, T. Whitaker, T.R. McBrayer, M. Ehteshami, S. Amiralaei, M. Detorio, and S.J. Coats. Antiviral Therapy, 2013. 18: p. A57-A57; ISI[000335229500051].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335203800004">Effect of Weak Acid Hypochlorous Solution on Selected Viruses and Bacteria of Laboratory Rodents<i>.</i></a> Taharaguchi, M., K. Takimoto, A. Zamoto-Niikura, and Y.K. Yamada. Experimental Animals, 2014. 63(2): p. 141-147; ISI[000335203800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334420300058">Synthesis and Broad-spectrum Antiviral Activity of Some Novel Benzo-heterocyclic Amine Compounds<i>.</i></a> Zhang, D.J., W.F. Sun, Z.J. Zhong, R.M. Gao, H. Yi, Y.H. Li, Z.G. Peng, and Z.R. Li. Molecules, 2014. 19(1): p. 925-939; ISI[000334420300058].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0523-060514.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
