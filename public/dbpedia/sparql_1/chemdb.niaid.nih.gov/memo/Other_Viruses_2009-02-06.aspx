

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-02-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TdCrcRbaNoiGEXAPE6i46A7IsqHk3skC2rG0Ynm+zMrEXkQzfNrDam4h+zaJsxYN9erqqw1AbKKOx6wewOwFDrAn7f8j3HJXMEHLrmj3tka4Vl56ShADQBGVZEA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="465FF674" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses  Citation List:  January 21, 2009 - February 3, 2009</h1>

    <p class="memofmt2-2">Hepatitis Virus</p>

    <p class="ListParagraphCxSpFirst">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19171797?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">In vitro activity and pre-clinical profile of TMC435350, a potent HCV protease inhibitor.</a>
    <br />
    Lin TI, Lenz O, Fanning G, Verbinnen T, Delouvroy F, Scholliers A, Vermeiren K, Rosenquist A, Edlund M, Samuelsson B, Vrang L, de Kock H, Wigerinck P, Raboisson P, Simmen K.
    <br />
    Antimicrob Agents Chemother. 2009 Jan 26. [Epub ahead of print]
    <br />
    PMID: 19171797 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19170598?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Discovery of Novel Arylethynyltriazole Ribonucleosides with Selective and Effective Antiviral and Antiproliferative Activity.</a>
    <br />
    Wan J, Xia Y, Liu Y, Wang M, Rocchi P, Yao J, Qu F, Neyts J, Iovanna JL, Peng L.
    <br />
    J Med Chem. 2009 Jan 26. [Epub ahead of print]
    <br />
    PMID: 19170598 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19102654?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Discovery and structure-activity relationship of P1-P3 ketoamide derived macrocyclic inhibitors of hepatitis C virus NS3 protease.</a>
    <br />
    Venkatraman S, Velazquez F, Wu W, Blackman M, Chen KX, Bogen S, Nair L, Tong X, Chase R, Hart A, Agrawal S, Pichardo J, Prongay A, Cheng KC, Girijavallabhan V, Piwinski J, Shih NY, Njoroge FG.
    <br />
    J Med Chem. 2009 Jan 22;52(2):336-46.
    <br />
    PMID: 19102654 [PubMed - in process]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpLast">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19173711?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Consensus siRNA for inhibition of HCV genotype-4 replication.</a>
    <br />
    Zekri AR, Bahnassy AA, Alam El-Din HM, Salama HM.
    <br />
    Virol J. 2009 Jan 27;6(1):13. [Epub ahead of print]
    <br />
    PMID: 19173711 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19131244?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Tetracyclic indole inhibitors of hepatitis C virus NS5B-polymerase.</a>
    <br />
    Stansfield I, Ercolani C, Mackay A, Conte I, Pompei M, Koch U, Gennari N, Giuliano C, Rowley M, Narjes F.
    <br />
    Bioorg Med Chem Lett. 2009 Feb 1;19(3):627-32. Epub 2008 Dec 24.
    <br />
    PMID: 19131244 [PubMed - in process]</p>

    <p class="ListParagraph">6.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19109015?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of pentacyclic compounds as potent inhibitors of hepatitis C virus NS5B RNA polymerase.</a>
    <br />
    Habermann J, Capitò E, Ferreira Mdel R, Koch U, Narjes F.
    <br />
    Bioorg Med Chem Lett. 2009 Feb 1;19(3):633-8. Epub 2008 Dec 13.
    <br />
    PMID: 19109015 [PubMed - in process]
    <br />

    <p class="memofmt2-2">Influenza Virus</p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19014974?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Aurintricarboxylic acid inhibits influenza virus neuraminidase.</a>
    <br />
    Hung HC, Tseng CP, Yang JM, Ju YW, Tseng SN, Chen YF, Chao YS, Hsieh HP, Shih SR, Hsu JT.
    <br />
    Antiviral Res. 2009 Feb;81(2):123-31. Epub 2008 Nov 17.
    <br />
    PMID: 19014974 [PubMed - in process]</p>

    <p class="NoSpacing"><br />
    <br />
    <br /></p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
