

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-291.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KsdOeJztKLB00reEXEwN6JlL7eTKpGh4/uY34wxyNReXDRP6T0NSo9N+ViPSR4Zl+5P+o/KQh6mZYRLNIDq5zZ7LSlZKhpbB2lKBHtSzikcaUxMNBqm6ai61Dyk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="28504196" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-291-MEMO</b> </p>

<p>    1.    42997   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Temperature-Mediated Heteroduplex Analysis for Detection of pncA Mutations Associated with Pyrazinamide Resistance and Differentiation between Mycobacterium tuberculosis and Mycobacterium bovis by Denaturing High- Performance Liquid Chromatography</p>

<p>           Mohamed, AM, BastoLa DR, Morlock, GP, Cooksey, RC, and Hinrichs, SH</p>

<p>           J Clin Microbiol 2004. 42(3): 1016-1023</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15004047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15004047&amp;dopt=abstract</a> </p><br />

<p>    2.    42998   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Chemotherapeutic potential of alginate-chitosan microspheres as anti-tubercular drug carriers</p>

<p>           Pandey, R and Khuller, GK</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998985&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998985&amp;dopt=abstract</a> </p><br />

<p>    3.    42999   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Design, Synthesis, and Evaluation of Aza-Peptide Epoxides as Selective and Potent Inhibitors of Caspases-1, -3, -6, and -8</p>

<p>           James, KE, Asgian, JL, Li, ZZ, Ekici, OD, Rubin, JR, Mikolajczyk, J, Salvesen, GS, and Powers, JC</p>

<p>           J Med Chem 2004. 47(6): 1553-1574</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14998341&amp;dopt=abstract</a> </p><br />

<p>    4.    43000   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Neonatal-mouse infectivity of intact Cryptosporidium parvum oocysts isolated after optimized in vitro excystation</p>

<p>           Hou, L, Li, X, Dunbar, L, Moeller, R, Palermo, B, and Atwill, ER</p>

<p>           Applied and Environmental Microbiology 2004. 70(1): 642-646</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    5.    43001   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Highlights of the 42nd Interscience Conference on Antimicrobial Agents and Chemotherapy (ICAAC)</p>

<p>           Prescott, LM</p>

<p>           Body Posit 2002. 15(7): 16-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14989207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14989207&amp;dopt=abstract</a> </p><br />

<p>    6.    43002   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Synthesis, anti-mycobacterial, anti-trichomonas and anti-candida in vitro activities of 2-substituted-6,7-difluoro-3-methylquinoxaline 1,4-dioxides</p>

<p>           Carta, A, Loriga, M, Paglietti, G, Mattana, A, Fiori, PL, Mollicotti, P, Sechi, L, and Zanetti, S</p>

<p>           Eur J Med Chem 2004. 39(2): 195-203</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987828&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987828&amp;dopt=abstract</a> </p><br />

<p>    7.    43003   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Hepatitis C NS3 protease inhibition by peptidyl-alpha-ketoamide inhibitors: kinetic mechanism and structure</p>

<p>           Liu, Y, Stoll, VS, Richardson, PL, Saldivar, A, Klaus, JL, MolLa A, Kohlbrenner, W, and Kati, WM</p>

<p>           Arch Biochem Biophys 2004. 421(2):  207-16</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14984200&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14984200&amp;dopt=abstract</a> </p><br />

<p>    8.    43004   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           In vitro activities of 3-(halogenated phenyl)-5-acyloxymethyl- 2,5-dihydrofuran-2-ones against common and emerging yeasts and molds</p>

<p>           Buchta, V, Pour, M, Kubanova, P, Silva, L, Votruba, I, Voprsalova, M, Schiller, R, Fakova, H, and Spulak, M</p>

<p>           Antimicrob Agents Chemother 2004.  48(3): 873-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982778&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982778&amp;dopt=abstract</a> </p><br />

<p>    9.    43005   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Anti-HIV natural product (+)-calanolide A is active against both drug-susceptible and drug-resistant strains of Mycobacterium tuberculosis</p>

<p>           Xu, ZQ, Barrow, WW, Suling, WJ, Westbrook, L, Barrow, E, Lin, YM, and Flavin, MT</p>

<p>           Bioorg Med Chem 2004. 12(5): 1199-207</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980631&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980631&amp;dopt=abstract</a> </p><br />

<p>  10.    43006   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           The search for new sterilizing anti-tuberculosis drugs</p>

<p>           Mitchison, DA </p>

<p>           Front Biosci 2004. 9: 1059-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14977529&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14977529&amp;dopt=abstract</a> </p><br />

<p>  11.    43007   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Preparation of antimicrobial bis(quaternary ammonium salts) and their uses</b>((Toa Gosei Chemical Industry Co., Ltd. Japan)</p>

<p>           Shibata, Shigeyuki, Kano, Muneaki, Tanaka, Yoichi, Nagata, Toshiyuki, and Koma, Hiroki</p>

<p>           PATENT: JP 2004026722 A2;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2002-54636; PP: 17 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    43008   OI-LS-291; PUBMED-OI-3/9/2004</p>

<p class="memofmt1-2">           Mechanisms of hepatitis C virus infection</p>

<p>           Moriishi, K and Matsuura, Y</p>

<p>           Antivir Chem Chemother 2003. 14(6): 285-97</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968935&amp;dopt=abstract</a> </p><br />

<p>  13.    43009   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Fungicides PF1237A, B, and M manufacture with Fusarium</b>((Meiji Seika Kaisha, Ltd. Japan)</p>

<p>           Yaguchi, Takashi, Ueda, Masaru, Fushimi, Hideki, Amano, Shoichi, and Iida, Maiko</p>

<p>           PATENT: WO 2004013342 A1;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2003; PP: 22 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  14.    43010   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           2-Mercaptopyridine N-oxide derivative, and antimicrobials and antimicrobial compositions containing it</b> ((API Corporation, Japan)</p>

<p>           Suga, Mamoru, Ishimaru, Katsutoshi, Sato, Toshio, and Takahashi, Hideo</p>

<p>           PATENT: JP 2004043421 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2002-42025; PP: 9 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    43011   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Antifungal susceptibilities of Cryptococcus neoformans</p>

<p>           Archibald, Lennox K, Tuohy, Marion J, Wilson, Deborah A, Nwanyanwu, Okey, Kazembe, Peter N, Tansuphasawadikul, Somsit, Eampokalap, Boonchuay, Chaovavanich, Achara, Reller, LBarth, Jarvis, William R, Hall, Gerri S, and Procop, Gary W</p>

<p>           Emerging Infectious Diseases 2004.  10(1): 143-145</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43012   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Antimicrobial theta defensins, analogs thereof, and methods of use</b>((The Regents of the University of California, A California Corporation USA)</p>

<p>           Selsted, Michael E and Tran, Dat Q</p>

<p>           PATENT: US 2004014669 A1;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003-34499; PP: 46 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    43013   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           In vitro and in vivo efficacies of the new triazole albaconazole against Cryptococcus neoformans</p>

<p>           Miller, JL, Schell, WA, Wills, EA, Toffaletti, DL, Boyce, M, Benjamin, DK Jr, Bartroli, J, and Perfect, JR</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(2): 384-387</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  18.    43014   OI-LS-291; WOS-OI-2/29/2004</p>

<p class="memofmt1-2">           Chips with everything: DNA microarrays in infectious diseases</p>

<p>           Bryant, PA, Venter, D, Robins-Browne, R, and Curtis, N</p>

<p>           LANCET INFECT DIS 2004. 4(2): 100-111</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188766100021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188766100021</a> </p><br />

<p>  19.    43015   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           New 2,4-Diamino-5-(2&#39;,5&#39;-substituted benzyl)pyrimidines as Potential Drugs against Opportunistic Infections of AIDS and Other Immune Disorders. Synthesis and Species-Dependent Antifolate Activity</p>

<p>           Rosowsky, Andre, Forsch, Ronald A, Sibley, Carol Hopkins, Inderlied, Clark B, and Queener, Sherry F</p>

<p>           Journal of Medicinal Chemistry 2004.  47(6): 1475-1486</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    43016   OI-LS-291; WOS-OI-2/29/2004</p>

<p class="memofmt1-2">           Novel heteroarotinoids as potential antagonists of Mycobacterium bovis BCG</p>

<p>           Brown, CW, Liu, SQ, Klucik, J, Berlin, KD, Brennan, PJ, Kaur, D, and Benbrook, DM</p>

<p>           J MED CHEM 2004. 47(4): 1008-1017</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188831300024">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188831300024</a> </p><br />

<p>  21.    43017   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Mice Deficient in LRG-47 Display Increased Susceptibility to Mycobacterial Infection Associated with the Induction of Lymphopenia</p>

<p>           Feng, Carl G, Collazo-Custodio, Carmen M, Eckhaus, Michael, Hieny, Sara, Belkaid, Yasmine, Elkins, Karen, Jankovic, Dragana, Taylor, Gregory A, and Sher, Alan</p>

<p>           Journal of Immunology 2004. 172(2):  1163-1168</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    43018   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Oxazolidinone derivatives as antimicrobials</b> ((Ranbaxy Laboratories Limited, India)</p>

<p>           Mehta, Anita, Rudra, Sonali, Raja Rao, Ajjarapu Venkata Subrahmanya, Yadav, Ajay Singh, and Rattan, Ashok</p>

<p>           PATENT: WO 2004014392 A1;  ISSUE DATE: 20040219</p>

<p>           APPLICATION: 2002; PP: 62 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    43019   OI-LS-291; WOS-OI-2/29/2004</p>

<p class="memofmt1-2">           Telbivudine. Anti-HBV agent</p>

<p>           Sorbera, LA, Castaner, J, Castaner, RM, and Bayes, M</p>

<p>           DRUG FUTURE 2003. 28(9): 870-879</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188810400004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188810400004</a> </p><br />

<p>  24.    43020   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Animal models expressing ICAM-1 domains and uses for screening drugs for human rhinovirus infection</b> ((Glaxo Group Limited, UK)</p>

<p>           Blair, Edward Duncan, Clarke, Neil James, Johnston, Sebastian Lennox, and Rowlands, David John</p>

<p>           PATENT: WO 2004009810 A2;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 42 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    43021   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Preparation of 2-amino-9-(2-hydroxymethylcyclopropylidenemethyl)-purines as antiviral agents</b>((Wayne State University, USA and The Regents of the University of Michigan))</p>

<p>           Zemlicka, Jiri and Drach, John C</p>

<p>           PATENT: WO 2004006867 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 36 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    43022   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p><b>           Pyrimidones with antiviral properties</b> ((Axxima Pharmaceuticals Aktiengesellschaft, Germany and 4SC AG))</p>

<p>           Missio, Andrea, Herget, Thomas, Aschenbrenner, Andrea, Kramer, Bernd, Leban, Johann, and Wolf, Kristina</p>

<p>           PATENT: EP 1389461 A1;  ISSUE DATE: 20040218</p>

<p>           APPLICATION: 2002-18357; PP: 46 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    43023   OI-LS-291; WOS-OI-2/29/2004</p>

<p class="memofmt1-2">           Structure and fungicidal activity of a synthetic antimicrobial peptide, P18, and its truncated peptides</p>

<p>           Lee, DG, Hahm, KS, and Shin, SY</p>

<p>           BIOTECHNOL LETT 2004. 26(4): 337-341</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188852800014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188852800014</a> </p><br />

<p>  28.    43024   OI-LS-291; WOS-OI-2/29/2004</p>

<p class="memofmt1-2">           Evidence of metyrapone reduction by two Mycobacterium strains shown by H-1 NMR</p>

<p>           Combourieu, B, Besse, P, Sancelme, M, Maser, E, and Delort, AM</p>

<p>           BIODEGRADATION 2004. 15(2): 125-132</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188875600005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188875600005</a> </p><br />

<p>  29.    43025   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Treatment of herpes simplex virus infections</p>

<p>           Brady, Rebecca C and Bernstein, David I</p>

<p>           Antiviral Research 2004. 61(2): 73-81</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    43026   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Review: Multidrug-resistant tuberculosis: public health challenges</p>

<p>           Coker, RJ</p>

<p>           TROP MED INT HEALTH 2004. 9(1): 25-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189083600004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189083600004</a> </p><br />

<p>  31.    43027   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Antiviral activity of caspase inhibitors: effect on picornaviral 2A proteinase</p>

<p>           Deszcz, Luiza, Seipelt, Joachim, Vassilieva, Elena, Roetzer, Andreas, and Kuechler, Ernst</p>

<p>           FEBS Letters 2004. 560(1-3): 51-55</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    43028   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Epidemiology of tuberculosis</p>

<p>           Bouvet, E, Abiteboul, D, Antoun, F, Bessa, Z, Billy, C, Dautzenberg, B, Decludt, B, Gaudelus, J, Jarlier, V, LerasLe S, Siruguet, O, and Vincent, V</p>

<p>           REV MAL RESPIR 2003. 20(6): S13-S104</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188786800003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188786800003</a> </p><br />

<p>  33.    43029   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           The hepatitis C virus replicase: Insights into RNA-dependent RNA replication and prospects for rational drug design</p>

<p>           Frick, David N</p>

<p>           Current Organic Chemistry 2004. 8(3): 223-241</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    43030   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Specific inhibitors of HCV polymerase identified using an NS5B with lower affinity for template/primer substrate</p>

<p>           McKercher, G, Beaulieu, PL, Lamarre, D, LaPlante, S, Lefebvre, S, Pellerin, C, Thauvette, L, and Kukolj, G</p>

<p>           NUCLEIC ACIDS RES 2004. 32(2): 422-431</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188989000012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188989000012</a> </p><br />

<p>  35.    43031   OI-LS-291; SCIFINDER-OI-3/2/2004</p>

<p class="memofmt1-2">           Inhibition of the subgenomic hepatitis C virus replicon in Huh-7 cells by 2&#39;-deoxy-2&#39;-fluorocytidine</p>

<p>           Stuyver, Lieven J, McBrayer, Tamara R, Whitaker, Tony, Tharnish, Phillip M, Ramesh, Mangala, Lostia, Stefania, Cartee, Leanne, Shi, Junxing, Hobbs, Ann, Schinazi, Raymond F, Watanabe, Kyoichi A, and Otto, Michael J</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(2): 651-654</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  36.    43032   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Evidence for radical formation at Tyr-353 in Mycobacterium tuberculosis catalase-peroxidase (KatG)</p>

<p>           Zhao, XB, Girotto, S, Yu, SW, and Magliozzo, RS</p>

<p>           J BIOL CHEM 2004. 279(9): 7606-7612</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300030">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300030</a> </p><br />

<p>  37.    43033   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           The YefM antitoxin defines a family of natively unfolded proteins - Implications as a novel antibacterial target</p>

<p>           Cherny, I and Gazit, E</p>

<p>           J BIOL CHEM 2004. 279(9): 8252-8261</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300107">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189103300107</a> </p><br />

<p>  38.    43034   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Non-nucleoside inhibitors of the hepatitis C virus NS5B polymerase: discovery of benzimidazole 5-carboxylic amide derivatives with low-nanomolar potency</p>

<p>           Beaulieu, PL, Bos, M, Bousquet, Y, DeRoy, P, Fazal, G, Gauthier, J, Gillard, J, Goulet, S, McKercher, G, Poupart, MA, Valois, S, and Kukolj, G</p>

<p>           BIOORG MED CHEM LETT 2004. 14(4): 967-971</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188963600027">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188963600027</a> </p><br />

<p>  39.    43035   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Synthesis and antifungal activity of 3,5-disubstituted phenyl imino-1,2,4-triazoles</p>

<p>           Siddiqui, AA, Islam, MU, and Siddiqui, N</p>

<p>           ASIAN J CHEM 2004. 16(1): 534-536</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188935500090">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188935500090</a> </p><br />

<p>  40.    43036   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Inhibitors of the hepatitis C virus NS5b RNA-dependent RNA polymerase.</p>

<p>           Beaulieu, PL, Austel, V, Bos, M, Bousquet, Y, Fazal, G, Gauthier, J, Gillard, J, Goulet, S, LaPlante, S, Poupart, MA, Lamarre, D, Lefebvre, S, McKercher, G, Marquis, M, Pause, A, Pellerin, C, and Kukolj, G</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500022">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500022</a> </p><br />

<p>  41.    43037   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Hepatitis CNS3 center dot 4A protease inhibitors: Design of reversible covalent warheads.</p>

<p>           Perni, RB, Britt, SD, Court, JJ, Courtney, LF, Deininger, DD, Farmer, LJ, Gates, CA, Harbeson, SL, Kim, JL, Levin, R, Moon, YC, Luong, YP, O&#39;Malley, E, Pitlik, J, Rao, BG, Van Drie, JH, Tung, RD, and Thomson, JA</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500098">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500098</a> </p><br />

<p>  42.    43038   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Hepatitis CNS3 center dot 4A protease inhibitors: Extensive structure-activity relationship exploration of potent, covalent ketoamide based inhibitors.</p>

<p>           Pitlik, J, Britt, SD, Cottrell, KC, Court, JJ, Courtney, LF, Deininger, DD, Gates, CA, Lin, C, Lin, K, Luong, YP, Perni, RB, Rao, BG, Thomson, JA, Tung, RD, and Wei, YY</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500099">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500099</a> </p><br />

<p>  43.    43039   OI-LS-291; WOS-OI-3/7/2004</p>

<p class="memofmt1-2">           Synthesis and SAR of 2-(4,5-dihydroxy-6-carboxy-pyrimidinyl) thiophenes as inhibitors of the hepatitis C virus NS5B polymerase.</p>

<p>           Malancona, S, Attenni, B, Colarusso, S, Conte, I, Harper, S, Summa, V, Altamura, S, Koch, U, Matassa, VG, and Narjes, F</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U22-U23</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500123">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062500123</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
