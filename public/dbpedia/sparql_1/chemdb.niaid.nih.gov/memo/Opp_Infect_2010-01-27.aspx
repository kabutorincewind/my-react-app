

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-01-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iffPdxzYmO9/5L+nwOkmkmGumO7yO9OsdJkbi0bKuednnAJ/A/WYMayzliWN8Ryyw5EPvcGcA0if8tjAYdkS/5MVoPo0NdwTK/Cb34fQ/J6d0XVguJUb52oGd0c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="69AC5379" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">January 6, 2009 - January 19, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p class="memofmt2-h1"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20052976?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">The Structural Basis of Cryptosporidium -Specific IMP Dehydrogenase Inhibitor Selectivity.</a></p>

    <p class="plaintext">Macpherson IS, Kirubakaran S, Gorla SK, Riera TV, D&#39;Aquino JA, Zhang M, Cuny GD, Hedstrom L.</p>

    <p class="plaintext">J Am Chem Soc. 2010 Jan 6. [Epub ahead of print]PMID: 20052976</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20030516?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=26">Synthesis, structure, and antifungal evaluation of some novel 1,2,4-triazolylmercaptoacetylthiosemicarbazide and 1,2,4-triazolylmercaptomethyl-1,3,4-thiadiazole analogs.</a></p>

    <p class="plaintext">Klip NT, Capan G, Gürsoy A, Uzun M, Satana D.</p>

    <p class="plaintext">J Enzyme Inhib Med Chem. 2010 Feb;25(1):126-31. PMID: 20030516</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20077521?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Synthesis of alpha- and beta-Pyran Naphthoquinones as a New Class of Antitubercular Agents.</a></p>

    <p class="plaintext">Ferreira SB, de Carvalho da Silva F, Bezerra FA, Lourenço MC, Kaiser CR, Pinto AC, Ferreira VF.</p>

    <p class="plaintext">Arch Pharm (Weinheim). 2010 Jan 14. [Epub ahead of print]PMID: 20077521</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20078138?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=49">Triazaspirodimethoxybenzoyls as selective inhibitors of mycobacterial lipoamide dehydrogenase.</a></p>

    <p class="plaintext">Bryk, R., N. Arango, A. Venugopal, J.D. Warren, Y.H. Park, M.S. Patel, C.D. Lima, and C. Nathan</p>

    <p class="plaintext">Biochemistry; PMID[20078138]</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20075118?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=57">In vitro synergistic interactions of oleanolic acid in combination with isoniazid, rifampicin or ethambutol against Mycobacterium tuberculosis.</a></p>

    <p class="plaintext">Ge, F., F. Zeng, S. Liu, N. Guo, H. Ye, Y. Song, J. Fan, X. Wu, X. Wang, X. Deng, Q. Jin, and L. Yu</p>

    <p class="plaintext">J Med Microbiol; PMID[20075118]</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20084637?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=22">Synthesis and NMR assignment of pentacycloundecane precursors of potential pharmaceutical agents.</a></p>

    <p class="plaintext">Onajole, O.K., M.M. Makatini, P. Govender, T. Govender, G.E. Maguire, and H.G. Kruger</p>

    <p class="plaintext">Magn Reson Chem; PMID[20084637]</p>

    <p> </p>

    <p class="memofmt2-h1">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>  

    <p class="plaintext">7. Bogatcheva, E</p>

    <p class="plaintext">Discovery of dipiperidines as new antitubercular agents</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2010 (Jan.) 20: 201 - 205</p>

    <p> </p>

    <p class="plaintext">8. Karthikeyan, SV., et al</p>

    <p class="plaintext">A highly atom economic, chemo-, regio- and stereoselective synthesis and evaluation of spiro-pyrrolothiazoles as antitubercular agents</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS   2010 (Jan.) 20: 350 - 353</p>

    <p> </p>

    <p class="plaintext">9. Ramalingam, P., et al</p>

    <p class="plaintext">In vitro antitubercular and antimicrobial activities of 1-substituted quinoxaline-2,3(1H,4H)-diones</p>

    <p class="plaintext">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS  2010 (Jan.) 20: 406 - 408</p>

    <p> </p>

    <p class="plaintext">10. Bajohr, LL., et al</p>

    <p class="plaintext">In Vitro and In Vivo Activities of 1-Hydroxy-2-Alkyl-4(1H)Quinolone Derivatives against Toxoplasma gondii</p>

    <p class="plaintext">ANTIMICROBIAL AGENTS AND CHEMOTHERAPY  2010 (Jan.) 54: 517 - 521</p>         
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
