

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-03-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SYToPYBSy5W3V05RM3Fq3xAJV04EGg+t99Vlk81FcC0ER5ZhJAv0UWJIvBp9apzuhvLdePmIBaNEJObyhAaV5cOzLYdRlTWEIzSNjAf2Ic/MxhQN0K0bQLaXrVI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B015DC3E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: February 28 - March 13, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24487969">Isothiafludine, a Novel Non-nucleoside Compound, Inhibits Hepatitis B Virus Replication through Blocking Pregenomic RNA Encapsidation<i>.</i></a> Yang, L., L.P. Shi, H.J. Chen, X.K. Tong, G.F. Wang, Y.M. Zhang, W.L. Wang, C.L. Feng, P.L. He, F.H. Zhu, Y.H. Hao, B.J. Wang, D.L. Yang, W. Tang, F.J. Nan, and J.P. Zuo. Acta Pharmacologica Sinica, 2014. 35(3): p. 410-418; PMID[24487969].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-03131414.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24572531">New Drug for Hepatitis C Infection<i>.</i></a> Aschenbrenner, D.S. The American Journal of Nursing, 2014. 114(3): p. 23; PMID[24572531].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-03131414.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24262278">Efficacy of Nucleotide Polymerase Inhibitor Sofosbuvir Plus the NS5A Inhibitor Ledipasvir or the NS5B Non-Nucleoside Inhibitor GS-9669 against HCV Genotype 1 Infection<i>.</i></a> Gane, E.J., C.A. Stedman, R.H. Hyland, X. Ding, E. Svarovskaia, G.M. Subramanian, W.T. Symonds, J.G. McHutchison, and P.S. Pang. Gastroenterology, 2014. 146(3): p. 736-743; PMID[24262278].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-03131414.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24587329">Inhibition of Hepatitis C Virus Infection by DNA Aptamer against NS2 Protein<i>.</i></a> Gao, Y., X. Yu, B. Xue, F. Zhou, X. Wang, D. Yang, N. Liu, L. Xu, X. Fang, and H. Zhu. Plos One, 2014. 9(2): p. e90333; PMID[24587329].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24485784">4&#39;-Substituted Pyrimidine Nucleosides Lacking 5&#39;-Hydroxyl Function as Potential anti-HCV Agents<i>.</i></a> Shakya, N., S. Vedi, C. Liang, F. Yang, B. Agrawal, and R. Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(5): p. 1407-1409; PMID[24485784].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-031314.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24564313">Antiviral Efficacy of Nine Nucleoside Reverse Transcriptase Inhibitors against Feline Immunodeficiency Virus in Feline Peripheral Blood Mononuclear Cells<i>.</i></a> Schwartz, A.M., M.A. McCrackin, R.F. Schinazi, P.B. Hill, T.W. Vahlenkamp, M.B. Tompkins, and K. Hartmann. American Journal of Veterinary Research, 2014. 75(3): p. 273-281; PMID[24564313].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-031314.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24046087">Topical Application of Polyethylenimine as a Candidate for Novel Prophylactic Therapeutics against Genital Herpes Caused by Herpes Simplex Virus<i>.</i></a> Hayashi, K., H. Onoue, K. Sasaki, J.B. Lee, P.K. Kumar, S.C. Gopinath, Y. Maitani, T. Kai, and T. Hayashi. Archives of Virology, 2014. 159(3): p. 425-435; PMID[24046087].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24275606">Formulation of Abalone Hemocyanin with High Antiviral Activity and Stability<i>.</i></a> Zanjani, N.T., F. Sairi, G. Marshall, M.M. Saksena, P. Valtchev, V.G. Gomes, A.L. Cunningham, and F. Dehghani. European Journal of Pharmaceutical Sciences, 2014. 53: p. 77-85; PMID[24275606].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0228-031314.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330637500045">Novel Cell-based Hepatitis C Virus Infection Assay for Quantitative High-throughput Screening of anti-Hepatitis C Virus Compounds<i>.</i></a> Hu, Z.Y., K.H. Lan, S.S. He, M. Swaroop, X. Hu, N. Southall, W. Zheng, and T.J. Liang. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 995-1004; ISI[000330637500045].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330554400027">Synthesis and Evaluation of Hexahydropyrimidines and Diamines as Novel Hepatitis C Virus Inhibitors<i>.</i></a> Hwang, J.Y., H.Y. Kim, S. Jo, E. Park, J. Choi, S. Kong, D.S. Park, J.M. Heo, J.S. Lee, Y. Ko, I. Choi, J. Cechetto, J. Kim, J. Lee, Z. No, and M.P. Windisch. European Journal of Medicinal Chemistry, 2013. 70: p. 315-325; ISI[000330554400027].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252202089">A Novel Uridine Nucleotide Prodrug, IDX20963, and Its Potential for Use in a Low-dose Direct-acting Antiviral Regimen for HCV<i>.</i></a> Lallos, L., X.R. Pan-Zhou, J. McCarville, S.Q. Luo, I. Serra, H. Rashidzadeh, M. LaColla, B. Hernandez-Santiago, J.P. Bilello, C. Chapron, F. Danehy, A.N. Allen, M. Camire, M. Seifer, K.S. Gupta, J.F. Griffon, C.C. Parsy, F.R. Alexandre, C.B. Dousson, N.L. Golitsina, S. Bhadresa, U. Miner, R. Rush, and D.N. Standring. Hepatology, 2013. 58: p. 441A-442A; ISI[000330252202089].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252202084">Evaluation of Second-generation NS5A Inhibitors against Hepatitis C Virus by Using NS5A Replaced JFH-1 Viruses and Full-length Infectious Clones Other Than JFH-1<i>.</i></a> Murayama, A., N. Sugiyama, T. Wakita, and T. Kato. Hepatology, 2013. 58: p. 439A-439A; ISI[000330252202084].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204289">IFN-Alpha and RBV Combination Synergistically Inhibit HCV IRES Translation at the Level of Polyribosome Formation<i>.</i></a> Panigrahi, R., S. Hazari, S. Chandra, P.K. Chandra, C.E. Cameron, Z.H. Huang, L.A. Balart, and S. Dash. Hepatology, 2013. 58: p. 937A-938A; ISI[000330252204289].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204275">Microsomal Triglyceride Transfer Protein (MTP) Inhibitors Efficiently Block the Production of Infectious Hepatitis C Virus (HCV) in Primary Human Adult Hepatocytes<i>.</i></a> Pene, V.M. and A.R. Rosenberg. Hepatology, 2013. 58: p. 931A-931A; ISI[000330252204275].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204286">HMG-CoA Reductase Inhibition Alters the Lipidome and Infectious Properties of HCV Virus Particles<i>.</i></a> Peng, L.F., J. Meixiong, A. Deik, E.A. Schaefer, N. Jilg, P. Chusri, C. Brisac, S. Chevaliez, C.L. Zhu, J. Luther, D. Wambua, D.N. Fusco, W.Y. Lin, C.B. Clish, and R.T. Chung. Hepatology, 2013. 58: p. 936A-936A; ISI[000330252204286].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br />  

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204264">Quercetin Inhibits HCV Replication by Modulating Lipid Droplets Morphology and Core Protein Co-Localization<i>.</i></a> Rojas, A., J.A. Del Campo, M. Garcia-Valdecasas, S. Clement, F. Negro, and M. Romero-Gomez. Hepatology, 2013. 58: p. 926A-926A; ISI[000330252204264].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252202080">Phosphoramidate Prodrugs of beta-D-2&#39;-C-Me-2,6-diaminopurine ribonucleoside are Potent Inhibitors of Hepatitis C Virus (HCV) Replication Delivering Two Active Nucleoside Triphosphates<i>.</i></a> Schinazi, R.F., L.H. Zhou, H.W. Zhang, M. Ehteshami, S.J. Tao, L.C. Bassit, J. Suesserman, T. Whitaker, T.R. McBrayer, J.H. Cho, S. Amiralaei, J. Shelton, M. Detorio, and S. Coats. Hepatology, 2013. 58: p. 437A-437A; ISI[000330252202080].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000331128000003">Tributyltin(IV) 3-(3&#39;,5&#39;-Dimethylphenylamido)propanoate as a Potent HCV Inhibitor, Its Delivery Study, Controlled Release and Binding Sites Using CTAB as a Standard Cell Membrane<i>.</i></a> Shah, F.A., K. Fatima, S. Sabir, S. Ali, and I. Qadri. Applied Organometallic Chemistry, 2014. 28(2): p. 74-80; ISI[000331128000003].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204272">A Cyclic Retinoid, Peretinoin, Inhibits Hepatitis C Virus Replication in Cell Culture<i>.</i></a> Shimakami, T., M. Honda, T. Shirasaki, S.M. Lemon, and S. Kaneko. Hepatology, 2013. 58: p. 930A-930A; ISI[000330252204272].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252202079">Preclinical Characteristics of ACH-3422: A Potent Uridine Nucleotide Prodrug for Inhibition of Hepatitis C Virus Polymerase<i>.</i></a> Yang, W.G., J. Wiles, D. Patel, S. Podos, J.L. Fabrycki, Y.S. Zhao, L.L. Jia, G.W. Yang, J.O. Rivera, M. Elliot, C. Marlor, A. Hashimoto, G. Pais, V. Gadachanda, X.Z. Wang, D.W. Chen, Q.P. Wang, M. Deshpande, M.J. Huang, K. Stauber, and A. Phadke. Hepatology, 2013. 58: p. 436A-437A; ISI[000330252202079].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000330252204290">Antiviral Toxicity and PK Studies of Novel Picomolar Potent HCV NS5A Inhibitors<i>.</i></a> Zhan, Z.Y.J., X.D. Wu, and H. Yan. Hepatology, 2013. 58: p. 938A-938A; ISI[000330252204290].</p>

    <p class="plaintext"><b>[WOS</b><b>]</b>. OV_0228-031314.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
