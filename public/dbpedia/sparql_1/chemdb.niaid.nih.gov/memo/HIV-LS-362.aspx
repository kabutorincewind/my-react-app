

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-362.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="I/xiDuR8jh8PvSQv3voM24cT8JE4cPdOpNHGlzvjte7V9Svoc0wacrKNAFQR53NzhRSKtY61IXUhL9ZRIIiesv0RDzrUhz2gZKGd90hE696Hu1jUZYjQ4HwKLv4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="21F933C0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-362-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59481   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Pronounced in vitro and in vivo antiretroviral activity of 5-substituted 2,4-diamino-6-[2-(phosphonomethoxy)ethoxy] pyrimidines</p>

    <p>          Balzarini, J,  Schols, D, Van Laethem, K, De Clercq, E, Hockova, D, Masojidkova, M, and Holy, A</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17124193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17124193&amp;dopt=abstract</a> </p><br />

    <p>2.     59482   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Synthesis of novel benzofuran and related benzimidazole derivatives for evaluation of in vitro anti-HIV-1, anticancer and antimicrobial activities</p>

    <p>          Rida, SM, El-Hawash, SA, Fahmy, HT, Hazzaa, AA, and El-Meligy, MM</p>

    <p>          Arch Pharm Res  <b>2006</b>.  29(10): 826-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17121175&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17121175&amp;dopt=abstract</a> </p><br />

    <p>3.     59483   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p><b>          A Rationally Designed Synthetic Mimic of the Discontinuous CD4-Binding Site of HIV-1 gp120</b> </p>

    <p>          Franke, R, Hirsch, T, and Eichler, J</p>

    <p>          J Recept Signal Transduct Res <b>2006</b>.  26(5): 453-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17118792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17118792&amp;dopt=abstract</a> </p><br />

    <p>4.     59484   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Cytotoxic and Anti-HIV-1 Caged Xanthones from the Resin and Fruits of Garcinia hanburyi</p>

    <p>          Reutrakul, V, Anantachoke, N, Pohmakotr, M, Jaipetch, T, Sophasan, S, Yoosook, C, Kasisit, J, Napaswat, C, Santisuk, T, and Tuchinda, P</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17117343&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17117343&amp;dopt=abstract</a> </p><br />

    <p>5.     59485   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          A Novel Non-nucleoside Analogue That Inhibits HIV-1 Isolates Resistant to Current Non-Nucleoside RT Inhibitors</p>

    <p>          Zhang, Z, Xu, W, Koh, YH, Shim, JH, Girardet, JL, Yeh, LT, Hamatake, RK, and Hong, Z</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116677&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59486   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Isolation and Characterization of Human Immunodeficiency Virus Type 1 Resistant to the Small-Molecule CCR5 Antagonist TAK-652</p>

    <p>          Baba, M, Miyake, H, Wang, X, Okamotoand, M, and Takashima, K</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116673&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116673&amp;dopt=abstract</a> </p><br />

    <p>7.     59487   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Substrate-dependent inhibition or stimulation of HIV RNase H activity by non-nucleoside reverse transcriptase inhibitors (NNRTIs)</p>

    <p>          Hang, JQ, Li, Y, Yang, Y, Cammack, N, Mirzadegan, T, and Klumpp, K</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113568&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113568&amp;dopt=abstract</a> </p><br />

    <p>8.     59488   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          A peptide with HIV-1 reverse transcriptase inhibitory activity from the medicinal mushroom Russula paludosa</p>

    <p>          Wang, J, Wang, HX, and Ng, TB</p>

    <p>          Peptides <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113195&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17113195&amp;dopt=abstract</a> </p><br />

    <p>9.     59489   HIV-LS-362; EMBASE-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Structural and dynamical properties of different protonated states of mutant HIV-1 protease complexed with the saquinavir inhibitor studied by molecular dynamics simulations</p>

    <p>          Aruksakunwong, Ornjira, Wittayanarakul, Kitiyaporn, Sompornpisut, Pornthep, Sanghiran, Vannajan, Parasuk, Vudthichai, and Hannongbua, Supot</p>

    <p>          Journal of Molecular Graphics and Modelling <b>2006</b>.  25(3): 324-332</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGP-4JCBPK1-1/2/b2849990a89f19d95935f6fb89bf50a3">http://www.sciencedirect.com/science/article/B6TGP-4JCBPK1-1/2/b2849990a89f19d95935f6fb89bf50a3</a> </p><br />

    <p>10.   59490   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Highly selective action of triphosphate metabolite of 4&#39;-ethynyl D4T: A novel anti-HIV compound against HIV-1 RT</p>

    <p>          Yang, G, Dutschman, GE, Wang, CJ, Tanaka, H, Baba, M, Anderson, KS, and Cheng, YC</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109975&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17109975&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59491   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Amino Acid Conjugated Sophorolipids: A New Family of Biologically Active Functionalized Glycolipids</p>

    <p>          Azim, A, Shah, V, Doncel, GF, Peterson, N, Gao, W, and Gross, R</p>

    <p>          Bioconjug Chem  <b>2006</b>.  17(6): 1523-1529</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17105232&amp;dopt=abstract</a> </p><br />

    <p>12.   59492   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Protease inhibitors and their peptidomimetic derivatives as potential drugs</p>

    <p>          Fear, G, Komarnytsky, S, and Raskin, I</p>

    <p>          Pharmacol Ther  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098288&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17098288&amp;dopt=abstract</a> </p><br />

    <p>13.   59493   HIV-LS-362; EMBASE-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Crystal Structures of Clinically Relevant Lys103Asn/Tyr181Cys Double Mutant HIV-1 Reverse Transcriptase in Complexes with ATP and Non-nucleoside Inhibitor HBY 097</p>

    <p>          Das, Kalyan, Sarafianos, Stefan G, Clark, Jr Arthur D, Boyer, Paul L, Hughes, Stephen H, and Arnold, Eddy</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4KWT6DY-1/2/5d3a4adcad20d61d196f844d9c8476c6">http://www.sciencedirect.com/science/article/B6WK7-4KWT6DY-1/2/5d3a4adcad20d61d196f844d9c8476c6</a> </p><br />

    <p>14.   59494   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Virological and clinical implications of resistance to HIV-1 protease inhibitors</p>

    <p>          Condra, JH</p>

    <p>          Drug Resist Updat <b>1998</b>.  1(5): 292-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092810&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17092810&amp;dopt=abstract</a> </p><br />

    <p>15.   59495   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Kalanchosides A-C, new cytotoxic bufadienolides from the aerial parts of Kalanchoe gracilis</p>

    <p>          Wu, PL, Hsu, YL, Wu, TS, Bastow, KF, and Lee, KH</p>

    <p>          Org Lett <b>2006</b>.  8(23): 5207-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17078679&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17078679&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   59496   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Etravirine: R165335, TMC 125, TMC-125, TMC125</p>

    <p>          Anon</p>

    <p>          Drugs R D <b>2006</b>.  7(6): 367-373</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17073519&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17073519&amp;dopt=abstract</a> </p><br />

    <p>17.   59497   HIV-LS-362; PUBMED-HIV-11/27/2006</p>

    <p class="memofmt1-2">          Initial highly-active antiretroviral therapy with a protease inhibitor versus a non-nucleoside reverse transcriptase inhibitor: discrepancies between direct and indirect meta-analyses</p>

    <p>          Chou, R, Fu, R, Huffman, LH, and Korthuis, PT</p>

    <p>          Lancet <b>2006</b>.  368(9546): 1503-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17071284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17071284&amp;dopt=abstract</a> </p><br />

    <p>18.   59498   HIV-LS-362; EMBASE-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Establishment of a new cell line inducibly expressing HIV-1 protease for performing safe and highly sensitive screening of HIV protease inhibitors</p>

    <p>          Fuse, Takayuki, Watanabe, Ken, Kitazato, Kaio, and Kobayashi, Nobuyuki</p>

    <p>          Microbes and Infection <b>2006</b>.  8(7): 1783-1789</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4JTR4KP-1/2/08e830099d3b5ce2642b730c5cde586e">http://www.sciencedirect.com/science/article/B6VPN-4JTR4KP-1/2/08e830099d3b5ce2642b730c5cde586e</a> </p><br />

    <p>19.   59499   HIV-LS-362; EMBASE-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Anti-HIV drugs and the mitochondria: 14th European Bioenergetics Conference</p>

    <p>          Pinti, Marcello, Salomoni, Paolo, and Cossarizza, Andrea</p>

    <p>          Biochimica et Biophysica Acta (BBA) - Bioenergetics <b>2006</b>.  1757(5-6): 700-707</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1S-4JXR900-1/2/c996b8261642f6c42f858a4081e62041">http://www.sciencedirect.com/science/article/B6T1S-4JXR900-1/2/c996b8261642f6c42f858a4081e62041</a> </p><br />

    <p>20.   59500   HIV-LS-362; WOS-HIV-11/19/2006</p>

    <p class="memofmt1-2">          Research of siRNA inhibition effect in HIV-1 gene expression</p>

    <p>          Wei, L, Liu, X, and Cao, C</p>

    <p>          PROGRESS IN BIOCHEMISTRY AND BIOPHYSICS <b>2006</b>.  33(10): 1007-1013, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241569200015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241569200015</a> </p><br />

    <p>21.   59501   HIV-LS-362; WOS-HIV-11/19/2006</p>

    <p class="memofmt1-2">          Identification of APOBEC3DE as another antiretroviral factor from the human APOBEC family</p>

    <p>          Dang, Y, Wang, XJ, Esselman, WJ, and Zheng, YH</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(21): 10522-10533, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241606100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241606100023</a> </p><br />
    <br clear="all">

    <p>22.   59502   HIV-LS-362; WOS-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Synthesis of novel L-N-MCd4T as a potent anti-HIV agent</p>

    <p>          Park, AY, Moon, HR, Kim, KR, Chun, MW, and Jeong, LS</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2006</b>.  4(22): 4065-4067, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241720500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241720500005</a> </p><br />

    <p>23.   59503   HIV-LS-362; WOS-HIV-11/26/2006</p>

    <p class="memofmt1-2">          In vitro resistance to the human immunodeficiency virus type 1 maturation inhibitor PA-457 (Bevirimat)</p>

    <p>          Adamson, CS, Ablan, SD, Boeras, I, Goila-Gaur, R, Soheilian, F, Nagashima, K, Li, F, Salzwedel, K, Sakalian, M, Wild, CT, and Freed, EO</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(22): 10957-10971, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241821300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241821300006</a> </p><br />

    <p>24.   59504   HIV-LS-362; WOS-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Pertussis toxin and its binding unit inhibit HIV-1 infection of human cervical tissue and macrophages involving a CD14 pathway</p>

    <p>          Hu, QX, Younson, J, Griffin, GE, Kelly, C, and Shattock, RJ</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2006</b>.  194(11): 1547-1556, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241820800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241820800011</a> </p><br />

    <p>25.   59505   HIV-LS-362; WOS-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Regulation of CD4 expression via recycling by HRES-1/RAB4 controls susceptibility to HIV infection</p>

    <p>          Nagy, G, Ward, J, Mosser, DD, Koncz, A, Gergely, P, Stancato, C, Qian, YM, Fernandez, D, Niland, B, Grossman, CE, Telarico, T, Banki, K, and Perl, A</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2006</b>.  281(45): 34574-34591, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241767600077">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241767600077</a> </p><br />

    <p>26.   59506   HIV-LS-362; WOS-HIV-11/26/2006</p>

    <p class="memofmt1-2">          Synthesis of 4/5-deoxy-4/5-nucleobase derivatives of 3-O-methyl-D-chiro-inositol as potential antiviral agents</p>

    <p>          Zhan, TR, Ma, YD, Fan, PH, Ji, M, and Lou, HX</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY <b>2006</b>.  3(10): 1126-1137, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241773400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241773400004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
