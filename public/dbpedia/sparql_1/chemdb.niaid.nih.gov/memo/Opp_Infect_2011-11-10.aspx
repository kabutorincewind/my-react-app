

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eLpTqlRrAEfii4tRPDdIgqvxUr9kB1b5pm6LnSMCLzyPhpoK4ObnVMSRGoYjYgGPoamMMBvVDf1GyyHFpwnuV5psxKgaXjJSmZbY2csdkCJi4QODu4Mt0XioBEk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3B8A50B4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">October 28 - November 10, 2011</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20981980">1,3-Dihydro-2h-indol-2-ones Derivatives: Design, Synthesis, In Vitro Antibacterial, Antifungal and Antitubercular Study</a>. Akhaja, T.N. and J.P. Raval, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5573-5579; PMID[21981980].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22045512">Synthesis and Antimicrobial Evaluation of Some New Cyclooctanones and Cyclooctane-based Heterocycles.</a>  Ali, K.A., H.M. Hosni, E.A. Ragab, and S.I. El-Moez, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[22045512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21959231">Synthesis and Anticandidal Activity of New Triazolothiadiazine Derivatives.</a> Altintop, M.D., Z.A. Kaplancikli, G. Turan-Zitouni, A. Ozdemir, G. Iscan, G. Akalin, and S.U. Yildirim, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5562-5566; PMID[21959231].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21937152">A Facile Synthesis of Carbocycle-fused Mono and Bis-1,2,3-selenadiazoles and Their Antimicrobial and Antimycobacterial Studies.</a> Chitra, S., N. Paul, S. Muthusubramanian, P. Manisankar, P. Yogeeswari, and D. Sriram, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5465-5472; PMID[21937152]. <b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22006736">Susceptibility of Clinical Isolates of Candida to Photodynamic Effects of Curcumin.</a> Dovigo, L.N., A.C. Pavarina, J.C. Carmello, A.L. Machado, I.L. Brunetti, and V.S. Bagnato, Lasers in Surgery and Medicine, 2011. 43(9): p. 927-934; PMID[22006736].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876042">Ctriporin, a New anti-methicillin-resistant Staphylococcus Aureus Peptide from the Venom of the Scorpion Chaerilus tricostatus.</a> Fan, Z., L. Cao, Y. He, J. Hu, Z. Di, Y. Wu, W. Li, and Z. Cao, Antimicrobial Agents and Chemotherapy, 2011. 55(11): p. 5220-5229; PMID[21876042].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21873289">Combinatorial Activities of Ionic Silver and Sodium Hexametaphosphate against Microorganisms Associated with Chronic Wounds.</a> Humphreys, G., G.L. Lee, S.L. Percival, and A.J. McBain, The Journal of Antimicrobial Chemotherapy, 2011. 66(11): p. 2556-2561; PMID[21873289].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21962987">Synthesis, Molecular Modeling and Preliminary Biological Evaluation of a Set of 3-acetyl-2,5-disubstituted-2,3-dihydro-1,3,4-oxadiazole as Potential Antibacterial, anti-Trypanosoma cruzi and Antifungal Agents.</a> Ishii, M., S.D. Jorge, A.A. de Oliveira, F. Palace-Berl, I.Y. Sonehara, K.F. Pasqualoto, and L.C. Tavares, Bioorganic &amp; Medicinal Chemistry, 2011. 19(21): p. 6292-6301; PMID[21962987]. <b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21926199">Rapid Screening Method for Compounds That Affect the Growth and Germination of Candida albicans, Using a Real-time PCR Thermocycler.</a> Jarosz, L.M. and B.P. Krom, Applied and Environmental Microbiology, 2011. 77(22): p. 8193-8196; PMID[21926199].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22047191">Antimicrobial Flavonoids from Tridax procumbens.</a> Jindal, A. and P. Kumar, Natural Product Research, 2011. <b>[Epub ahead of print]</b>; PMID[22047191].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21824811">Synthesis, Characterization and Biological Activity of Some New Vo(Iv), Co(Ii), Ni(Ii), Cu(Ii) and Zn(Ii) Complexes of Chromone Based NNO Schiff Base Derived from 2-aminothiazole.</a> Kalanithi, M., D. Kodimunthiri, M. Rajarajan, and P. Tharmaraj, Spectrochimica acta. Part A, Molecular and Biomolecular Spectroscopy, 2011. 82(1): p. 290-298; PMID[21824811].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21939748">Antimicrobial Evaluation of Huilliche Plant Medicine Used to Treat Wounds.</a> Molgaard, P., J.G. Holler, B. Asar, I. Liberna, L.B. Rosenbaek, C.P. Jebjerg, L. Jorgensen, J. Lauritzen, A. Guzman, A. Adsersen, and H.T. Simonsen, Journal of Ethnopharmacology, 2011. 138(1): p. 219-227; PMID[21939748]. <b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22038181">Lasiocepsin, a Novel Cyclic Antimicrobial Peptide from the Venom of Eusocial Bee Lasioglossum laticeps (Hymenoptera: Halictidae).</a> Monincova, L., J. Slaninova, V. Fucik, O. Hovorka, Z. Voburka, L. Bednarova, P. Malon, J. Stokrova, and V. Cerovsky, Amino acids, 2011. <b>[Epub ahead of print]</b>; PMID[22038181].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21940175">Synthesis and Evaluation of Novel Macrocyclic Antifungal Peptides.</a> Mulder, M.P., J.A. Kruijtzer, E.J. Breukink, J. Kemmink, R.J. Pieters, and R.M. Liskamp, Bioorganic &amp; Medicinal Chemistry, 2011. 19(21): p. 6505-6517; PMID[21940175].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21900519">Triazole and Echinocandin MIC Distributions with Epidemiological Cutoff Values for Differentiation of Wild-type Strains from Non-wild-type Strains of Six Uncommon Species of Candida.</a> Pfaller, M.A., M. Castanheira, D.J. Diekema, S.A. Messer, and R.N. Jones, Journal of Clinical Microbiology, 2011. 49(11): p. 3800-3804; PMID[21900519].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22011503">Natamycin in the Treatment of Keratomycosis: Correlation of Treatment Outcome and In Vitro Susceptibility of Fungal Isolates.</a> Pradhan, L., S. Sharma, S. Nalamada, S.K. Sahu, S. Das, and P. Garg, Indian journal of ophthalmology, 2011. 59(6): p. 512-514; PMID[22011503].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21958543">Synthesis, Stereochemistry and in Vitro Antimicrobial Evaluation of Novel 2-[(2,4-diaryl-3-azabicyclo[3.3.1]nonan-9-ylidene)hydrazono]-4-phenyl-2,3-dihydro thiazoles.</a> Ramachandran, R., P. Parthiban, M. Rani, S. Jayanthi, S. Kabilan, and Y.T. Jeong, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6301-6304; PMID[21958543].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21930375">Micelles Catalyzed Chemoselective Synthesis &#39;in Water&#39; and Biological Evaluation of Oxygen Containing Hetero-1,4-naphthoquinones as Potential Antifungal Agents.</a> Tandon, V.K., H.K. Maurya, N.N. Mishra, and P.K. Shukla, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6398-6403; PMID[21930375].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21844320">Discovery of a Novel Class of Orally Active Antifungal {Beta}-1,3-D-glucan Synthase Inhibitors.</a> Walker, S.S., Y. Xu, I. Triantafyllou, M.F. Waldman, C. Mendrick, N. Brown, P. Mann, A. Chau, R. Patel, N. Bauman, C. Norris, B. Antonacci, M. Gurnani, A. Cacciapuoti, P.M. McNicholas, S. Wainhaus, R.J. Herr, R. Kuang, R.G. Aslanian, P.C. Ting, and T.A. Black, Antimicrobial Agents and Chemotherapy, 2011. 55(11): p. 5099-5106; PMID[21844320].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21978838">Synthesis and Antimycobacterial Evaluation of 3a,4-dihydro-3h-indeno [1,2-C] Pyrazole-2-carboxamide Analogues.</a> Ahsan, M.J., J.G. Samy, H. Khalilullah, M.A. Bakht, and M.Z. Hassan, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5694-5697; PMID[21978838].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21975068">Chemical Synthesis, Biological Evaluation and Structure-Activity Relationship Analysis of Azaisoindolinones, a Novel Class of Direct Enoyl-ACP Reductase Inhibitors as Potential Antimycobacterial Agents.</a> Deraeve, C., I.M. Dorobantu, F. Rebbah, F. Le Quemener, P. Constant, A. Quemard, V. Bernardes-Genisson, J. Bernadou, and G. Pratviel, Bioorganic &amp; Medicinal Chemistry, 2011. 19(21): p. 6225-6232; PMID[21975068].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">22.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/22057598">In Vitro Efficacy of Acetohydroxyacid Synthase Inhibitors against Clinical Strains of Mycobacterium tuberculosis Isolated from a Hospital in Beijing, China.</a> Dong, M., D. Wang, Y. Jiang, L. Zhao, C. Yang, and C. Wu, Saudi medical journal, 2011. 32(11): p. 1122-1126; PMID[22057598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">23.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21920421">Antibacterial Activity of Two Biflavonoids from Garcinia livingstonei Leaves Against Mycobacterium smegmatis.</a> Kaikabo, A.A. and J.N. Eloff, Journal of Ethnopharmacology, 2011. 138(1): p. 253-255; PMID[21920421].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">24.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21917452">Synthesis and Preliminary Biological Evaluation of Novel N-(3-aryl-1,2,4-triazol-5-yl) Cinnamamide Derivatives as Potential Antimycobacterial Agents: An Operational Topliss Tree Approach.</a> Kakwani, M.D., N.H. Palsule Desai, A.C. Lele, M. Ray, M.G. Rajan, and M.S. Degani, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6523-6526; PMID[21917452].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22060033">5-amino-2-aroylquinolines as Highly Potent Tubulin Polymerization Inhibitors. Part 2 The Impact of Bridging Groups at C-2 Position.</a> Lee, H.Y., J.Y. Chang, C.Y. Nien, C.C. Kuo, K.H. Shih, C.H. Wu, C.Y. Chang, W.Y. Lai, and J.P. Liou, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22060033].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">26.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21944473">Synthesis and Biological Activities of Triazole Derivatives as Inhibitors of INHA and Antituberculosis Agents.</a> Menendez, C., S. Gau, C. Lherbet, F. Rodriguez, C. Inard, M.R. Pasca, and M. Baltas, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5524-5531; PMID[21944473].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">27.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21920745">A Pd-Mediated New Strategy to Functionalized 2-aminochromenes: Their In Vitro Evaluation as Potential anti-Tuberculosis Agents.</a> Ram Reddy, T., L. Srinivasula Reddy, G. Rajeshwar Reddy, V.S. Nuthalapati, Y. Lingappa, S. Sandra, R. Kapavarapu, P. Misra, and M. Pal, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6433-6439; PMID[21920745].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="plaintext">28.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21917615">Characterization of 7-amino-4-methylcoumarin as an Effective Antitubercular Agent: Structure-activity Relationships.</a> Tandon, R., P. Ponnan, N. Aggarwal, R. Pathak, A.S. Baghel, G. Gupta, A. Arya, M. Nath, V.S. Parmar, H.G. Raj, A.K. Prasad, and M. Bose, The Journal of Antimicrobial Chemotherapy, 2011. 66(11): p. 2543-2555; PMID[21917615].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1029-111011.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21937228">Biological Evaluation of Glycosyl-isoindigo Derivatives against the Pathogenic Agents of Tropical Diseases (Malaria, Chagas Disease, Leishmaniasis and Human African Trypanosomiasis).</a> Bouchikhi, F., F. Anizon, R. Brun, and P. Moreau, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(21): p. 6319-6321; PMID[21937228].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21958335">Anti-Inflammatory and Antimalarial Meroterpenoids from the New Zealand Ascidian Aplidium scabellum.</a> Chan, S.T., A.N. Pearce, A.H. Januario, M.J. Page, M. Kaiser, R.J. McLaughlin, J.L. Harper, V.L. Webb, D. Barker, and B.R. Copp, The Journal of Organic Chemistry, 2011. 76(21): p. 9151-9156; PMID[21958335].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876053">In Vitro Activity of Antiretroviral Drugs against Plasmodium falciparum.</a> Nsanzabana, C. and P.J. Rosenthal, Antimicrobial Agents and Chemotherapy, 2011. 55(11): p. 5073-5077; PMID[21876053]. <b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21940072">Antimalarial Activity of Imidazo[2,1-a]lsoindol-5-ol Derivatives and Related Compounds</a>. Olmo, E.D., B. Barboza, L.D. Chiaradia, A. Moreno, J. Carrero-Lerida, D. Gonzalez-Pacanowska, V. Munoz, J.L. Lopez-Perez, A. Gimenez, A. Benito, A.R. Martinez, L.M. Ruiz-Perez, and A. San Feliciano, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5379-5386; PMID[21940072].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21995542">Antiplasmodial and Antiproliferative Pseudoguaianolides of Athroisma proteiforme from the Madagascar Dry Forest (1).</a> Pan, E., A.P. Gorka, J.N. Alumasa, C. Slebodnick, L. Harinantenaina, P.J. Brodie, P.D. Roepe, R. Randrianaivo, C. Birkinshaw, and D.G. Kingston, Journal of Natural Products, 2011. 74(10): p. 2174-2180; PMID[21995542].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">34.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/22054038">A New Class of Antimalarial Dioxanes Obtained through a Simple Two-step Synthetic Approach: Rational Design and Structure-activity Relationship Studies.</a>Persico, M., A. Quintavalla, F. Rondinelli, C. Trombini, M. Lombardo, C. Fattorusso, V. Azzarito, D. Taramelli, S. Parapini, Y. Corbett, G. Chianese, E. Fattorusso, and O. Taglialatela-Scafati, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22054038].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">35.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21958736">Identification of New Antimalarial Leads by Use of Virtual Screening against Cytochrome bc(1).</a> Rodrigues, T., R. Moreira, J. Gut, P.J. Rosenthal, O.N. PM, G.A. Biagini, F. Lopes, D.J. Dos Santos, and R.C. Guedes, Bioorganic &amp; Medicinal Chemistry, 2011. 19(21): p. 6302-6308; PMID[21958736].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">36.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21954931">Antimalarial Activity of Physalins B, D, F, and G.</a> Sa, M.S., M.N. de Menezes, A.U. Krettli, I.M. Ribeiro, T.C. Tomassini, R. Ribeiro Dos Santos, W.F. de Azevedo, and M.B. Soares, Journal of Natural Products, 2011. 74(10): p. 2269-2272; PMID[21954931].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">37.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21862474">Indolone-N-oxide Derivatives: In Vitro Activity against Fresh Clinical Isolates of Plasmodium falciparum, Stage Specificity and In Vitro Interactions with Established Antimalarial Drugs.</a> Tahar, R., L. Vivas, L. Basco, E. Thompson, H. Ibrahim, J. Boyer, and F. Nepveu, The Journal of Antimicrobial Chemotherapy, 2011. 66(11): p. 2566-2572; PMID[21862474].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">38.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21982338">Synthesis and Antimalarial Activity of Thioetherhydroxyethylsulfonamides, Potential Aspartyl Protease Inhibitors, Part 3.</a> Vellasco Junior, W.T., G.P. Guedes, T.R. Vasconcelos, M.G. Vaz, M.V. de Souza, A.U. Krettli, L.G. Krettli, A.C. Aguiar, C.R. Gomes, and W. Cunico, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5688-5693; PMID[21982338].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">39.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21944972">The anti-Malarial Activity of Bivalent Imidazolium Salts.</a> Vlahakis, J.Z., S. Mitu, G. Roman, E. Patricia Rodriguez, I.E. Crandall, and W.A. Szarek, Bioorganic &amp; Medicinal Chemistry, 2011. 19(21): p. 6525-6542; PMID[21944972].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

    <p class="plaintext">40.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21940071">Synthesis and Evaluation of the anti-Parasitic Activity of Aromatic Nitro Compounds.</a> Lopes, M.S., R.C. de Souza Pietra, T.F. Borgati, C.F. Romeiro, P.A. Junior, A.J. Romanha, R.J. Alves, E.M. Souza-Fagundes, A.P. Fernandes, and R.B. de Oliveira, European Journal of Medicinal Chemistry, 2011. 46(11): p. 5443-5447; PMID[21940071].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295797300027">Synthesis, Characterization and Biological Evaluation of Cyclomontanin D.</a> Dahiya, R., H. Gautam, African Journal of Pharmacy and Pharmacology, 2011. 5(3): p. 447-453; ISI[000295797300027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295754100004">Synthesis of Quinoline Coupled 1,2,3 -triazoles as a Promising Class of Anti-tuberculosis Agents.</a> Kumar, K.K., S.P. Seenivasan, V. Kumar, and T.M. Das, Carbohydrate research, 2011. 346(14): p. 2084-2090; ISI[000295754100004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">43.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295864900069">A Novel Antimicrobial Phenanthrene Alkaloid from Bryopyllum pinnatum</a>. Okwu, D.E. and F.U. Nnamdi, E-Journal of Chemistry, 2011. 8(3): p. 1456-1461; ISI[000295864900069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">44.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295864900031">Biological Important Ni(II) Ternary Complexes Derived from 2-Substituted Benzothiazoles and Amino Acids.</a> Pal, N., M. Kumar, and G. Seth, E-Journal of Chemistry, 2011. 8(3): p. 1174-1179; ISI[000295864900031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">45.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295800500025">Evaluation of the Stem Bark of Pistacia integerrima Stew Ex Brandis for Its Antimicrobial and Phytotoxic Activities.</a> Rahman, S.U., M. Ismail, N. Muhammad, F. Ali, K.A. Chishti, and M. Imran, African Journal of Pharmacy and Pharmacology, 2011. 5(8): p. 1170-1174; ISI[000295800500025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">46.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000296039400007">The Control of Aspergillus flavus with Cinnamomum jensenianum Hand-mazz Essential Oil and Its Potential Use as a Food Preservative.</a> Tian, J., B. Huang, X.L. Luo, H. Zeng, X.Q. Ban, J.S. He, and Y.W. Wang, Food Chemistry, 2012. 130(3): p. 520-527; ISI[000296039400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1028-111011.</p>

    <p> </p>

    <p class="plaintext">47.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295546200018">Discovery of Novel Alkylated (Bis)Urea and (Bis)Thiourea Polyamine Analogues with Potent Antimalarial Activities.</a> Verlinden, B.K., J. Niemand, J. Snyman, S.K. Sharma, R.J. Beattie, P.M. Woster, and L.M. Birkholtz, Journal of Medicinal Chemistry, 2011. 54(19): p. 6624-6633; ISI[000295546200018].</p>

    <p class="ListParagraphCxSpLast"><b>[WOS]</b>. OI_1028-111011.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
