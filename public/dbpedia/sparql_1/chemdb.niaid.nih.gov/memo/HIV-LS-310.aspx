

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-310.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9tSBw80/ZWEDtw0Q1Up3Eor7VH37G+3Kn8zIsCfnc1rv6IKmxJggJ6C4QWxG0e127Hvj0q17wDBGMjOsbYtxCSa+6yc3MV8I8wo26FErNJja9larsD9qV600Lmo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="037E0164" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-310-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44412   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Negative feedback inhibition of HIV-1 by TAT-inducible expression of siRNA</p>

    <p>          Unwalla, HJ, Li, MJ, Kim, JD, Li, HT, Ehsani, A, Alluin, J, and Rossi, JJ</p>

    <p>          Nat Biotechnol  <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568018&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15568018&amp;dopt=abstract</a> </p><br />

    <p>2.         44413   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          The effect of iron overload on in vitro HIV-1 infection</p>

    <p>          Traore, HN and Meyer, D</p>

    <p>          J Clin Virol <b>2004</b>.  31 Suppl 1: 92-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15567100&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15567100&amp;dopt=abstract</a> </p><br />

    <p>3.         44414   HIV-LS-310; EMBASE-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Fixed Conformation Nucleoside Analogs Effectively Inhibit Excision-proficient HIV-1 Reverse Transcriptases</p>

    <p>          Boyer, Paul L, Julias, John G, Marquez, Victor E, and Hughes, Stephen H</p>

    <p>          Journal of Molecular Biology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4DTKYCJ-2/2/1bdea11a69c17f276841437d6eb0b74c">http://www.sciencedirect.com/science/article/B6WK7-4DTKYCJ-2/2/1bdea11a69c17f276841437d6eb0b74c</a> </p><br />

    <p>4.         44415   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          TMC125, a Novel Next-Generation Nonnucleoside Reverse Transcriptase Inhibitor Active against Nonnucleoside Reverse Transcriptase Inhibitor-Resistant Human Immunodeficiency Virus Type 1</p>

    <p>          Andries, K, Azijn, H, Thielemans, T, Ludovici, D, Kukla, M, Heeres, J, Janssen, P, De, Corte B, Vingerhoets, J, Pauwels, R, and de, Bethune MP</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4680-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561844&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561844&amp;dopt=abstract</a> </p><br />

    <p>5.         44416   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Gln145Met/Leu Changes in Human Immunodeficiency Virus Type 1 Reverse Transcriptase Confer Resistance to Nucleoside and Nonnucleoside Analogs and Impair Virus Replication</p>

    <p>          Paolucci, S, Baldanti, F, Maga, G, Cancio, R, Zazzi, M, Zavattoni, M, Chiesa, A, Spadari, S, and Gerna, G</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4611-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561833&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561833&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44417   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Globoidnan A: a lignan from Eucalyptus globoidea inhibits HIV integrase</p>

    <p>          Ovenden, SP, Yu, J, San, Wan S, Sberna, G, Murray, Tait R, Rhodes, D, Cox, S, Coates, J, Walsh, NG, and Meurer-Grimes, BM</p>

    <p>          Phytochemistry  <b>2004</b>.  65(24): 3255-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561191&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561191&amp;dopt=abstract</a> </p><br />

    <p>7.         44418   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Symmetric fluoro-substituted diol-based HIV protease inhibitors</p>

    <p>          Lindberg, J, Pyring, D, Lowgren, S, Rosenquist, A, Zuccarello, G, Kvarnstrom, I, Zhang, H, Vrang, L, Classon, B, Hallberg, A, Samuelsson, B, and Unge, T</p>

    <p>          Eur J Biochem <b>2004</b>.  271(22): 4594-602</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15560801&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15560801&amp;dopt=abstract</a> </p><br />

    <p>8.         44419   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Role of hydroxyl group and R/S configuration of isostere in binding properties of HIV-1 protease inhibitors</p>

    <p>          Petrokova, H, Duskova, J, Dohnalek, J, Skalova, T, Vondrackova-Buchtelova, E, Soucek, M, Konvalinka, J, Brynda, J, Fabry, M, Sedlacek, J, and Hasek, J</p>

    <p>          Eur J Biochem <b>2004</b>.  271(22): 4451-61</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15560786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15560786&amp;dopt=abstract</a> </p><br />

    <p>9.         44420   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Optimization of unique, uncharged thioesters as inhibitors of HIV replication</p>

    <p>          Srivastava, P, Schito, M, Fattah, RJ, Hara, T, Hartman, T, Buckheit, RW Jr, Turpin, JA, Inman, JK, and Appella, E</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(24): 6437-50</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15556761&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15556761&amp;dopt=abstract</a> </p><br />

    <p>10.       44421   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          QSAR modeling of anti-HIV activities of alkenyldiarylmethanes using topological and physicochemical descriptors</p>

    <p>          Leonard, JT and Roy, K</p>

    <p>          Drug Des Discov <b>2003</b>.  18(4): 165-80</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15553927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15553927&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44422   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          HIV-1 reverse transcriptase variants: molecular modeling of Y181C, V106A, L100I, and K103N mutations with nonnucleoside inhibitors using Monte Carlo simulations in combination with a linear response method</p>

    <p>          Smith, MB, Ruby, S, Horouzhenko, S, Buckingham, B, Richardson, J, Puleri, I, Potts, E, Jorgensen, WL, Arnold, E, Zhang, W, Hughes, SH, Michejda, CJ, and Smith, RH Jr</p>

    <p>          Drug Des Discov <b>2003</b>.  18(4): 151-63</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15553926&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15553926&amp;dopt=abstract</a> </p><br />

    <p>12.       44423   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Effect of highly active antiretroviral therapy (HAART) on pharmacokinetics and pharmacodynamics of doxorubicin in patients with HIV-associated non-Hodgkin&#39;s lymphoma</p>

    <p>          Toffoli, G, Corona, G, Cattarossi, G, Boiocchi, M, Di, Gennaro G, Tirelli, U, and Vaccher, E</p>

    <p>          Ann Oncol <b>2004</b>.  15(12): 1805-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550586&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550586&amp;dopt=abstract</a> </p><br />

    <p>13.       44424   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p><b>          Mechanistic insights into the suppression of drug-resistance by human immunodeficiency virus type-1 reverse transcriptase using alpha-boranophosphate nucleoside analogues</b> </p>

    <p>          Deval, J, Alvarez, K, Selmi, B, Bermond, M, Boretto, J, Guerreiro, C, Mulard, L, and Canard, B </p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550379&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550379&amp;dopt=abstract</a> </p><br />

    <p>14.       44425   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Xanthohumol, a novel anti-HIV-1 agent purified from Hops Humulus lupulus</p>

    <p>          Wang, Q, Ding, ZH, Liu, JK, and Zheng, YT</p>

    <p>          Antiviral Res <b>2004</b>.  64(3): 189-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550272&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550272&amp;dopt=abstract</a> </p><br />

    <p>15.       44426   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Antiretrovirals, Part II: focus on non-protease inhibitor antiretrovirals (NRTIs, NNRTIs, and fusion inhibitors)</p>

    <p>          Zapor, MJ, Cozza, KL, Wynn, GH, Wortmann, GW, and Armstrong, SC</p>

    <p>          Psychosomatics  <b>2004</b>.  45(6): 524-35</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546830&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546830&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.       44427   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          QSAR modelling of HIV-1 reverse transcriptase inhibition by benzoxazinones using a combination of P_VSA and pharmacophore feature descriptors</p>

    <p>          Balaji, S, Karthikeyan, C, Hari, Narayana Moorthy NS, and Trivedi, P</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(24): 6089-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546736&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546736&amp;dopt=abstract</a> </p><br />

    <p>17.       44428   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Synthesis of Poly(ethylene glycol)-Based Saquinavir Prodrug Conjugates and Assessment of Release and Anti-HIV-1 Bioactivity Using a Novel Protease Inhibition Assay</p>

    <p>          Gunaseelan, S, Debrah, O, Wan, L, Leibowitz, MJ, Rabson, AB, Stein, S, and Sinko, PJ</p>

    <p>          Bioconjug Chem  <b>2004</b>.  15(6): 1322-1333</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546199&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546199&amp;dopt=abstract</a> </p><br />

    <p>18.       44429   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Discovery, structure and biological activities of the cyclotides</p>

    <p>          Craik, DJ, Daly, NL, Mulvenna, J, Plan, MR, and Trabi, M</p>

    <p>          Curr Protein Pept Sci <b>2004</b>.  5(5): 297-315</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15544527&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15544527&amp;dopt=abstract</a> </p><br />

    <p>19.       44430   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Target cell cyclophilin a modulates human immunodeficiency virus type 1 infectivity</p>

    <p>          Sokolskaja, E, Sayah, DM, and Luban, J</p>

    <p>          J Virol <b>2004</b>.  78(23): 12800-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542632&amp;dopt=abstract</a> </p><br />

    <p>20.       44431   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Analysis of HIV-1 Vif-mediated proteasome-dependent depletion of APOBEC3G: correlating function and subcellular localization</p>

    <p>          Wichroski, MJ, Ichiyama, K, and Rana, T</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537645&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44432   HIV-LS-310; EMBASE-HIV-11/30/2004</p>

    <p class="memofmt1-2">          HIV resistance to the fusion inhibitor enfuvirtide: mechanisms and clinical implications</p>

    <p>          Miller, Michael D and Hazuda, Daria J</p>

    <p>          Drug Resistance Updates <b>2004</b>.  7(2): 89-95</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WDK-4CBW4T9-2/2/d5ce69cecfcfa3a1805266d8bd9c31d4">http://www.sciencedirect.com/science/article/B6WDK-4CBW4T9-2/2/d5ce69cecfcfa3a1805266d8bd9c31d4</a> </p><br />

    <p>22.       44433   HIV-LS-310; PUBMED-HIV-11/30/2004</p>

    <p class="memofmt1-2">          Flexible docking of pyridinone derivatives into the non-nucleoside inhibitor binding site of HIV-1 reverse transcriptase</p>

    <p>          Medina-Franco, JL, Rodriguez-Morales, S, Juarez-Gordiano, C, Hernandez-Campos, A, Jimenez-Barbero, J, and Castillo, R</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(23): 6085-6095</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519154&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519154&amp;dopt=abstract</a> </p><br />

    <p>23.       44434   HIV-LS-310; WOS-HIV-11/21/2004</p>

    <p><b>          Flipping the switch from monomeric to dimeric CV-N has little effect on antiviral activity</b> </p>

    <p>          Barrientos, LG, Lasala, F, Delgado, R, Sanchez, A, and Gronenborn, AM</p>

    <p>          STRUCTURE <b>2004</b>.  12(10): 1799-1807, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540800007</a> </p><br />

    <p>24.       44435   HIV-LS-310; WOS-HIV-11/21/2004</p>

    <p class="memofmt1-2">          In vitro antiviral diterpenes from the Brazilian brown alga Dictyota pfaffii</p>

    <p>          Barbosa, JP, Pereira, RC, Abrantes, JL, dos, Santos CCC, Rebello, MA, Frugulhetti, ICDP, and Teixeira, VL</p>

    <p>          PLANTA MEDICA <b>2004</b>.  70(9): 856-860, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224440700014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224440700014</a> </p><br />

    <p>25.       44436   HIV-LS-310; WOS-HIV-11/21/2004</p>

    <p class="memofmt1-2">          Correlation between the predicted and the observed biological activity of the symmetric and nonsymmetric cyclic urea derivatives used as HIV-1 protease inhibitors. A 3D-QSAR-CoMFA method for new antiviral drug design</p>

    <p>          Avram, S, Svab, I, Bologa, C, and Flonta, ML</p>

    <p>          JOURNAL OF CELLULAR AND MOLECULAR MEDICINE <b>2003</b>.  7(3): 287-296, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224538000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224538000010</a> </p><br />

    <p>26.       44437   HIV-LS-310; WOS-HIV-11/21/2004</p>

    <p class="memofmt1-2">          Three new compounds from the plant Lippia alva as inhibitors of chemokine receptor 5 (CCRS)</p>

    <p>          Hegde, VR, Pu, HY, Patel, M, Das, PR, Strizki, J, Gullo, VP, Chou, CC, Buevich, AV, and Chan, TM</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(21): 5339-5342, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224446600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224446600019</a> </p><br />
    <br clear="all">

    <p>27.       44438   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          Differential effects on human immunodeficiency virus type 1 replication by alpha-defensins with comparable bactericidal activities</p>

    <p>          Tanabe, H, Ouellette, AJ, Cocco, MJ, and Robinson, WE</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 11622-11631, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900016</a> </p><br />

    <p>28.       44439   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          Lv1 inhibition of human immunodeficiency virus type 1 is counteracted by factors that stimulate synthesis or nuclear translocation of viral cDNA</p>

    <p>          Berthoux, L, Sebastian, S, Sokolskaja, E, and Luban, J</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 11739-11750, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900028</a> </p><br />

    <p>29.       44440   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          Amino acid insertions near gag cleavage sites restore the otherwise compromised replication of human immunodeficiency virus type 1 variants resistant to protease inhibitors</p>

    <p>          Tamiya, S, Mardy, S, Kavlick, MF, Yoshimura, K, and Mistuya, H</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 12030-12040, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900055</a> </p><br />

    <p>30.       44441   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          The ability of chloroquine to prevent Tat-induced cytokine secretion by monocytes is implicated in its in vivo anti-human immunodeficiency virus type 1 activity</p>

    <p>          Rayne, F, Vendeville, A, Bonhoure, A, and Beaumelle, B</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 12054-12057, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900058">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900058</a> </p><br />

    <p>31.       44442   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          APOBEC3G incorporation into human immunodeficiency virus type 1 particles</p>

    <p>          Zennou, W, Perez-Caballero, D, Gottlinger, H, and Bieniasz, PD</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 12058-12061, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900059</a> </p><br />

    <p>32.       44443   HIV-LS-310; WOS-HIV-11/28/2004</p>

    <p class="memofmt1-2">          Statin compounds reduce human immunodeficiency virus type 1 replication by preventing the interaction between virion-associated host intercellular adhesion molecule 1 and its natural cell surface ligand LFA-1</p>

    <p>          Giguere, JF and Tremblay, MJ</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 12062-12065, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900060">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900060</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
