

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-03-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="F0EZ4B4xce0O6fETLh7mMhcBcxE/Qm8bU2Zk4XPYyLXOs7wKxUImLbLFxbnwtoJRRW0zJzQL0JjOLQw2Z96fv0SBMv4Vpl8tjwFOlTX7v6z9CAmECo+I18OlwAI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BF62A2A5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: February 27 - March 12, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25595716">MicroRNA-130a Can Inhibit Hepatitis B Virus Replication via Targeting PGC1alpha and PPARgamma.</a> Huang, J.Y., S.F. Chou, J.W. Lee, H.L. Chen, C.M. Chen, M.H. Tao, and C. Shih. RNA, 2015. 21(3): p. 385-400. PMID[25595716].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25650312">Design and Synthesis of a Novel Candidate Compound NTI-007 Targeting Sodium Taurocholate Cotransporting Polypeptide [NTCP]-APOA1-Hbx-Beclin1-mediated Autophagic Pathway in HBV Therapy.</a> Zhang, J., L.L. Fu, M. Tian, H.Q. Liu, J.J. Li, Y. Li, J. He, J. Huang, L. Ouyang, H.Y. Gao, and J.H. Wang. Bioorganic &amp; Medicinal Chemistry, 2015. 23(5): p. 976-984. PMID[25650312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25599836">Multi-parameter Optimization of AZA-follow-ups to BI 207524, a Thumb Pocket 1 HCV NS5B Polymerase Inhibitor. Part 2: Impact of Lipophilicity on Promiscuity and in Vivo Toxicity.</a> Beaulieu, P.L., G. Bolger, D. Deon, M. Duplessis, G. Fazal, A. Gagnon, M. Garneau, S. LaPlante, T. Stammers, G. Kukolj, and J. Duan. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(5): p. 1140-1145. PMID[25599836].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25575656">AZA Follow-ups to BI 207524, a Thumb Pocket 1 HCV NS5B Polymerase Inhibitor. Part 1: Mitigating the Genotoxic Liability of an Aniline Metabolite.</a> Beaulieu, P.L., G. Bolger, M. Duplessis, A. Gagnon, M. Garneau, T. Stammers, G. Kukolj, and J. Duan. Bioorganic &amp; Medicinal Chemistry Letters, 2015<b>.</b> 25(5): p. 1135-1139. PMID[25575656].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25573299">SKI-1/S1P Inhibitor PF-429242 Impairs the Onset of HCV Infection.</a> Blanchet, M., C. Sureau, C. Guevin, N.G. Seidah, and P. Labonte. Antiviral Research, 2015. 115: p. 94-104. PMID[25573299].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25557602">Natural Compounds Isolated from Brazilian Plants are Potent Inhibitors of Hepatitis C Virus Replication in Vitro.</a> Jardim, A.C., Z. Igloi, J.F. Shimizu, V.A. Santos, L.G. Felippe, B.F. Mazzeu, Y. Amako, M. Furlan, M. Harris, and P. Rahal. Antiviral Research, 2015. 115: p. 39-47. PMID[25557602].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25534735">In Vitro Activity and Resistance Profile of Dasabuvir, a Nonnucleoside Hepatitis C Virus Polymerase Inhibitor.</a> Kati, W., G. Koev, M. Irvin, J. Beyer, Y. Liu, P. Krishnan, T. Reisch, R. Mondal, R. Wagner, A. Molla, C. Maring, and C. Collins. Antimicrobial Agents and Chemotherapy, 2015. 59(3): p. 1505-1511. PMID[25534735].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25650256">Synthesis and Characterization of 1&#39;-C-Cyano-2&#39;-fluoro-2&#39;-C-methyl pyrimidine Nucleosides as HCV Polymerase Inhibitors.</a> Kirschberg, T.A., M.R. Mish, L. Zhang, N.H. Squires, K.Y. Wang, A. Cho, J.Y. Feng, M. Fenaux, D. Babusis, Y. Park, A.S. Ray, and C.U. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(5): p. 1046-1049. PMID[25650256].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25450204">Saikosaponin b2 Is a Naturally Occurring Terpenoid That Efficiently Inhibits Hepatitis C Virus Entry.</a> Lin, L.T., C.Y. Chung, W.C. Hsu, S.P. Chang, T.C. Hung, J. Shields, R.S. Russell, C.C. Lin, C.F. Li, M.H. Yen, D.L. Tyrrell, C.C. Lin, and C.D. Richardson. Journal of Hepatology, 2015. 62(3): p. 541-548. PMID[25450204].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25617695">Linear and Branched Alkyl-esters and Amides of Gallic acid and Other (Mono-, Di- and Tri-) Hydroxy benzoyl Derivatives as Promising anti-HCV Inhibitors.</a> Rivero-Buceta, E., P. Carrero, E.G. Doyaguez, A. Madrona, E. Quesada, M.J. Camarasa, M.J. Perez-Perez, P. Leyssen, J. Paeshuyse, J. Balzarini, J. Neyts, and A. San-Felix. European Journal of Medicinal Chemistry, 2015. 92: p. 656-671. PMID[25617695].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25665437">Dasabuvir : A New Direct Antiviral Agent for the Treatment of Hepatitis C.</a> Trivella, J.P., J. Gutierrez, and P. Martin. Expert Opinion on Pharmacotherapy, 2015. 16(4): p. 617-624. PMID[25665437].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24848265">Synergy of Entry Inhibitors with Direct-acting Antivirals Uncovers Novel Combinations for Prevention and Treatment of Hepatitis C.</a> Xiao, F., I. Fofana, C. Thumann, L. Mailly, R. Alles, E. Robinet, N. Meyer, M. Schaeffer, F. Habersetzer, M. Doffoel, P. Leyssen, J. Neyts, M.B. Zeisel, and T.F. Baumert. Gut, 2015. 64(3): p. 483-494. PMID[24848265].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25756650">LC-ESI-MS/MS Analysis and Pharmacokinetics of GP205, an Innovative Potent Macrocyclic Inhibitor of Hepatitis C Virus NS3/4A Protease in Rats.</a> Yang, N., Q. Sun, Z. Xu, X. Wang, X. Zhao, Y. Cao, L. Chen, and G. Fan. Molecules, 2015. 20(3): p. 4319-4336. PMID[25756650].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25534746">A High-affinity Native Human Antibody Neutralizes Human Cytomegalovirus Infection of Diverse Cell Types.</a> Kauvar, L.M., K. Liu, M. Park, N. DeChene, R. Stephenson, E. Tenorio, S.L. Ellsworth, T. Tabata, M. Petitt, M. Tsuge, J. Fang-Hoover, S.P. Adler, X. Cui, M.A. McVoy, and L. Pereira. Antimicrobial Agents and Chemotherapy, 2015. 59(3): p. 1558-1568. PMID[25534746].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25604263">Anti-herpes Simplex Virus Activity of Polysaccharides from Eucheuma gelatinae.</a> Jin, F., C. Zhuo, Z. He, H. Wang, W. Liu, R. Zhang, and Y. Wang. World Journal of Microbiology &amp; Biotechnology, 2015. 31(3): p. 453-460. PMID[25604263].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25583021">Anti-HSV1 Activity of Brown Algal Polysaccharides and Possible Relevance to the Treatment of Alzheimer&#39;s Disease.</a> Wozniak, M., T. Bell, A. Denes, R. Falshaw, and R. Itzhaki. International Journal of Biological Macromolecules, 2015. 74: p. 530-540. PMID[25583021].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0227-031215.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348831200020">Inhibitory Effects of Pinus massoniana Bark Extract on Hepatitis C Virus in Vitro.</a> Wang, C.F., L.F. Zhang, P. Cheng, and Q. Zhang. Pharmaceutical Biology, 2015. 53(3): p. 451-456. ISI[000348831200020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0227-031215.</p><br /> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
