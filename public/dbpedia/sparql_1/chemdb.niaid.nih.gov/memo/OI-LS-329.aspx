

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-329.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7NfihOy7J9jvZe6cEcWb2kJfM961A5yMg0gVK01p2/McltJW4asFYNHGywuH9UZT2dm9WTf1paaeKyqt5bkAd6Bp9fY5NVJ/7DNfGxCCvgPqXywGz6PbzF5uLTs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3E5ECB25" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-329-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47786   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Anidulafungin (eli lilly and co)</p>

    <p>          Hawser, S</p>

    <p>          IDrugs <b>1999</b>.  2(12): 1327-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16113965&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16113965&amp;dopt=abstract</a> </p><br />

    <p>2.     47787   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Investigations into microsporidian methionine aminopeptidase type 2: A therapeutic target for microsporidiosis</p>

    <p>          Zhang, Hong, Huang, Huan, Cali, Ann, Takvorian, Peter M, Feng, Xiaochuan, Zhou, Ghou, and Weiss, Louis M</p>

    <p>          Folia Parasitologica <b>2005</b>.  52(1/2): 182-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     47788   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus NS3-4A serine protease inhibitors: Use of a P(2)-P(1) cyclopropyl alanine combination for improved potency</p>

    <p>          Bogen, S, Saksena, AK, Arasappan, A, Gu, H, Njoroge, FG, Girijavallabhan, V, Pichardo, J, Butkiewicz, N, Prongay, A, and Madison, V</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16112862&amp;dopt=abstract</a> </p><br />

    <p>4.     47789   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Characterization of acetohydroxyacid synthase from Mycobacterium tuberculosis and the identification of its new inhibitor from the screening of a chemical library</p>

    <p>          Choi, KJ, Yu, YG, Hahn, HG, Choi, JD, and Yoon, MY</p>

    <p>          FEBS Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16111681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16111681&amp;dopt=abstract</a> </p><br />

    <p>5.     47790   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Identification of the Mycobacterium tuberculosis SUF Machinery as the Exclusive Mycobacterial System of [Fe-S] Cluster Assembly: Evidence for Its Implication in the Pathogen&#39;s Survival</p>

    <p>          Huet, G, Daffe, M, and Saves, I</p>

    <p>          J Bacteriol <b>2005</b>.  187(17): 6137-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16109955&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16109955&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47791   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Antimicrosporidial activity of (fluoro)quinolones in vitro and in vivo</p>

    <p>          Didier, Elizabeth S, Bowers, Lisa, Stovall, Mary E, Kuebler, Dorothy, Mittleider, Derek, Brindley, Paul J, and Didier, Peter J</p>

    <p>          Folia Parasitologica <b>2005</b>.  52(1/2): 173-181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     47792   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Encephalitozoon cuniculi mRNA Cap (Guanine N-7) Methyltransferase: methyl acceptor specificity, inhibition by S-adenosylmethionine analogs, and structure-guided mutational analysis</p>

    <p>          Hausmann, Stephane, Zheng, Sushuang, Fabrega, Carme, Schneller, Stewart W, Lima, Christopher D, and Shuman, Stewart</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(21): 20404-20412</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     47793   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          New antifungal agents</p>

    <p>          Kauffman, CA</p>

    <p>          Semin Respir Crit Care Med <b>2004</b>.  25(2): 233-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16088465&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16088465&amp;dopt=abstract</a> </p><br />

    <p>9.     47794   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Novel inhibitors of the hepatitis C virus NS3 proteinase</p>

    <p>          Bennett, F, Liu, YT, Saksena, AK, Arasappan, A, Butkiewicz, N, Dasmahapatra, B, Pichardo, JS, Njoroge, FG, Patel, NM, Huang, Y, and Yang, X</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(19): 4275-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087334&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16087334&amp;dopt=abstract</a> </p><br />

    <p>10.   47795   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Pharmacoproteomic Effects of Isoniazid, Ethambutol and SQ109 on Mycobacterium tuberculosis H37Rv</p>

    <p>          Jia, L, Coward, L, Gorman, GS, Noker, PE, and Tomaszewsky, JE</p>

    <p>          J Pharmacol Exp Ther <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16085758&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16085758&amp;dopt=abstract</a> </p><br />

    <p>11.   47796   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Toxoplasma gondii: Microneme protein MIC2</p>

    <p>          Brossier, F and David, Sibley L</p>

    <p>          Int J Biochem Cell Biol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16084754&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16084754&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   47797   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Pneumocystis pneumonia: an update</p>

    <p>          Sritangratanakul, S, Nuchprayoon, S, and Nuchprayoon, I</p>

    <p>          J Med Assoc Thai <b>2004</b>.  87 Suppl 2: S309-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16083208&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16083208&amp;dopt=abstract</a> </p><br />

    <p>13.   47798   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          In vitro susceptibilities of clinical isolates of Candida species, Cryptococcus neoformans, and Aspergillus species to itraconazole: global survey of 9,359 isolates tested by clinical and laboratory standards institute broth microdilution methods</p>

    <p>          Pfaller, MA, Boyken, L, Hollis, RJ, Messer, SA, Tendolkar, S, and Diekema, DJ</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(8): 3807-10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081915&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16081915&amp;dopt=abstract</a> </p><br />

    <p>14.   47799   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          Molecular design of two sterol 14alpha-demethylase homology models and their interactions with the azole antifungals ketoconazole and bifonazole</p>

    <p>          Rupp, B, Raub, S, Marian, C, and Holtje, HD</p>

    <p>          J Comput Aided Mol Des <b>2005</b>.  19(3): 149-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059669&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16059669&amp;dopt=abstract</a> </p><br />

    <p>15.   47800   OI-LS-329; PUBMED-OI-8/23/2005</p>

    <p class="memofmt1-2">          A commercial line probe assay for the rapid detection of rifampicin resistance in Mycobacterium tuberculosis: a systematic review and meta-analysis</p>

    <p>          Morgan, M, Kalantri, S, Flores, L, and Pai, M</p>

    <p>          BMC Infect Dis  <b>2005</b>.  5: 62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16050959&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16050959&amp;dopt=abstract</a> </p><br />

    <p>16.   47801   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Preparation of 2-(azolylmethyl)-4-(piperazinylphenoxymethyl)-1,3-dioxolanes as antifungals with reduced interaction with metabolic cytochromes</p>

    <p>          Pinori, Massimo, Lattanzio, Maria, Modena, Daniela, and Mascagni, Paolo</p>

    <p>          PATENT:  WO <b>2005040156</b>  ISSUE DATE:  20050506</p>

    <p>          APPLICATION: 2004  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (Italfarmaco S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   47802   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          In-vitro antibacterial, antifungal and cytotoxic properties of sulfonamide-derived Schiff&#39;s bases and their metal complexes</p>

    <p>          Chohan, Zahid H, Mahmood-Ul-Hassan, Khan, Khalid M, and Supuran, Claudiu T</p>

    <p>          Journal of Enzyme Inhibition and Medicinal Chemistry <b>2005</b>.  20(2): 183-188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   47803   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          Drugs against tuberculose: Past, present and future</p>

    <p>          de Souza, MVN and Vasconcelos, TRA</p>

    <p>          QUIMICA NOVA <b>2005</b>.  28(4): 678-682, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230649400022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230649400022</a> </p><br />

    <p>19.   47804   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          Slow-onset feedback inhibition: Inhibition of Mycobacterium tuberculosis alpha-isopropylmalate synthase by L-leucine</p>

    <p>          de Carvalho, LPS, Argyrou, A, and Blanchard, JS</p>

    <p>          JOURNAL OF THE AMERICAN CHEMICAL SOCIETY <b>2005</b>.  127(28): 10004-10005, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230657900029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230657900029</a> </p><br />

    <p>20.   47805   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Synthesis of 2,4-bis(alkylamino)pyrimidines and their use as antimicrobials</p>

    <p>          Marquais-Bienewald, Sophie, Hoelzl, Werner, Preuss, Andrea, and Mehlin, Andreas</p>

    <p>          PATENT:  WO <b>2005011758</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 38 pp.</p>

    <p>          ASSIGNEE:  (Ciba Specialty Chemicals Holding Inc., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47806   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Cationic substituted benzofurans as antimicrobial agents</p>

    <p>          Werbovetz, Karl, Franzblau, Scott Gary, Tidwell, Richard R, Bakunova, Svetlana, and Bakunov, Stanislav</p>

    <p>          PATENT:  WO <b>2005055935</b>  ISSUE DATE:  20050623</p>

    <p>          APPLICATION: 2004  PP: 146 pp.</p>

    <p>          ASSIGNEE:  (The University of North Carolina at Chapel Hill, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   47807   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          3-nitropropionic acid (3-NPA), a potent antimycobacterial agent from endophytic fungi: Is 3-NPA in some plants produced by endophytes?</p>

    <p>          Chomeheon, P, Wiyakrutta, S, Sriubolmas, N, Ngamrojanavanich, N, Isarangkul, D, and Kittakoop, P</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2005</b>.  68(7): 1103-1105, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230706800027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230706800027</a> </p><br />

    <p>23.   47808   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          5 &#39;-Homoneplanocin A inhibits hepatitis B and hepatitis C</p>

    <p>          Yang, MM, Schneller, SW, and Korba, B</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(15): 5043-5046, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230800300032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230800300032</a> </p><br />
    <br clear="all">

    <p>24.   47809   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Synthesis of some new thiosemicarbazide and 1,3,4-thiadiazole heterocycles bearing benzo[b]thiophene nucleus as a potent antitubercular and antimicrobial agents</p>

    <p>          Vasoya, SL, Paghdar, DJ, Chovatia, PT, and Joshi, HS</p>

    <p>          Journal of Sciences, Islamic Republic of Iran <b>2005</b>.  16(1): 33-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   47810   OI-LS-329; SCIFINDER-OI-8/15/2005</p>

    <p class="memofmt1-2">          Synthesis of purinyl homo-carbonucleoside derivatives of 2-benzylcyclopenta[c]pyrazol</p>

    <p>          Garcia, Marcos D, Caamano, Olga, Fernandez, Franco, Lopez, Carmen, and De Clercq, Erick</p>

    <p>          Synthesis <b>2005</b>.(6): 925-932</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   47811   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          High-throughput screening-driven lead discovery: Meeting the challenges of finding new therapeutics</p>

    <p>          Posner, BA</p>

    <p>          CURRENT OPINION IN DRUG DISCOVERY &amp; DEVELOPMENT <b>2005</b>.  8(4 ): 487-494, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230330200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230330200009</a> </p><br />

    <p>27.   47812   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          The World Health Organization/International Union against Tuberculosis and Lung Disease Global Project on Surveillance for Anti-Tuberculosis Drug Resistance: A model for other infectious diseases</p>

    <p>          Aziz, MA and Wright, A</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2005</b>.  41: S258-S262, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230611300008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230611300008</a> </p><br />

    <p>28.   47813   OI-LS-329; WOS-OI-8/14/2005</p>

    <p class="memofmt1-2">          Investigation of glycine alpha-ketoamide HCVNS3 protease inhibitors: Effect of carboxylic acid isosteres</p>

    <p>          Han, W, Jiang, XJ, Hu, ZL, Wasserman, ZR, and Decicco, CP</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(15): 3487-3490, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230627800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230627800002</a> </p><br />

    <p>29.   47814   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          The ultimate model organism: progress in experimental medicine</p>

    <p>          Littman, BH and Williams, SA</p>

    <p>          NATURE REVIEWS DRUG DISCOVERY <b>2005</b>.  4(8): 631-638, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230957400015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230957400015</a> </p><br />

    <p>30.   47815   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          Identification and structural characterization of an unusual mycobacterial monomeromycolyl-diacylglycerol</p>

    <p>          Kremer, L, de Chastellier, C, Dobson, G, Gibson, KJC, Bifani, P, Balor, S, Gorvel, JP, Locht, C, Minnikin, DE, and Besra, GS</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2005</b>.  57(4): 1113-1126, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230888300020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230888300020</a> </p><br />
    <br clear="all">

    <p>31.   47816   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          The hepatitis C virus replicon system: From basic research to clinical application</p>

    <p>          Bartenschlager, R</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2005</b>.  43(2): 210-216, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230803900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230803900004</a> </p><br />

    <p>32.   47817   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          A virtual screening approach for thymidine monophosphate kinase inhibitors as antitubercular agents based on docking and pharmaeophore models</p>

    <p>          Gopalakrishnan, B, Aparna, V, Jeevan, J, Ravi, M, and Desiraju, GR</p>

    <p>          JOURNAL OF CHEMICAL INFORMATION AND MODELING <b>2005</b>.  45(4): 1101-1108, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230864300032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230864300032</a> </p><br />

    <p>33.   47818   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of naphthalene-1,4-diones modified at positions 2, 3, and 5</p>

    <p>          Ryu, CK and Chae, MJ</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2005</b>.  28(7): 750-755, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230856700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230856700002</a> </p><br />

    <p>34.   47819   OI-LS-329; WOS-OI-8/21/2005</p>

    <p class="memofmt1-2">          Update on tuberculosis and other opportunistic infections associated with biologic therapies</p>

    <p>          Winthrop, KL</p>

    <p>          ANNALS OF THE RHEUMATIC DISEASES <b>2005</b>.  64: 2-3, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229909100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229909100007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
