

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gQbqyzEqSfUaRBBpdJZSxKXekrTYTTTE1/kIwtSv0xBeNXnvtM6O33Vd4CCCswB4BDQa5aCb/k35SNDH6VjSP0mAFA0YG80BthUGsLhKlc9z8FTXPY75E+c7qOo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9DFFD005" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">September 2 - September 15, 2011</h1>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21882529">Synthesis and Antiviral Activities of Geldanamycin Analog TC-GM in Vitro</a>. Li, C.X., G.Z. Shan, B. Fan, P.Z. Tao, L.X. Zhao, J.D. Jiang, Y.H. Li, and Z.R. Li. Yao xue xue bao. Acta Pharmaceutica Sinica, 2011. 46(6): p. 683-687; PMID[21882529].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21881252">Anti-Hepatitis B Virus and Cytotoxic Diterpenoids from Isodon lophanthoides var. gerardianus</a>. Yang, L.B., L. Li, S.X. Huang, J.X. Pu, Y. Zhao, Y.B. Ma, J.J. Chen, C.H. Leng, Z.M. Tao, and H.D. Sun. Chemical &amp; Pharmaceutical Bulletin, 2011. 59(9): p. 1102-5; PMID[21881252].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21893371">Synthesis and SAR Optimization of Diketo acid Pharmacophore for HCV NS5B Polymerase Inhibition</a>. Bhatt, A., K.R. Gurukumar, A. Basu, M.R. Patel, N. Kaushik-Basu, and T.T. Talele. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21893371].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896910">Griffithsin has Antiviral Activity against Hepatitis C Virus</a>. Meuleman, P., A. Albecka, S. Belouzard, K. Vercauteren, L. Verhoye, C. Wychowski, G. Leroux-Roels, K.E. Palmer, and J. Dubuisson. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21896910].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21880398">Synthesis and in Vitro Activity of Novel N-3 Acylated TSAO-T Compounds against HIV-1 and HCV</a>. Moura, M., S. Josse, A.N. Van Nhien, C. Fournier, G. Duverlie, S. Castelain, E. Soriano, J. Marco-Contelles, J. Balzarini, and D. Postel. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21880398].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">HCMV</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21904628">An Artemisinin-derived Dimer has Highly Potent Anti-cytomegalovirus (CMV) and Anti-cancer Activities</a>. He, R., B.T. Mott, A.S. Rosenthal, D.T. Genna, G.H. Posner, and R. Arav-Boger. Plos One, 2011. 6(8): p. e24334; PMID[21904628].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">HSV-1</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21915933">Effects of Ilex paraguariensis A. St. Hil. (Yerba Mate) on Herpes Simplex Virus Types 1 and 2 Replication</a>. Luckemeyer, D.D., V.D. Muller, M.I. Moritz, P.H. Stoco, E.P. Schenkel, C.R. Barardi, F.H. Reginatto, and C.M. Simoes. Phytotherapy Research : PTR, 2011. <b>[Epub ahead of print]</b>; PMID[21915933].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21895729">Antiviral Activity of Distictella elongata (Vahl) Urb. (Bignoniaceae), a Potentially Useful Source of Anti-dengue Drugs from the State of Minas Gerais, Brazil</a>. Simoes, L.R., G.M. Maciel, G.C. Brandao, E.G. Kroon, R.O. Castilho, and A.B. Oliveira. Letters in applied microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21895729].</p>

    <p class="NoSpacing"><b>PubMed]</b>. OV_0902-091511.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800044">Novel HCV NS5B Polymerase Inhibitors: Discovery of Indole 2-carboxylic acids with C3-Heterocycles</a>. Anilkumar, G.N., C.A. Lesburg, O. Selyutin, S.B. Rosenblum, Q.B. Zeng, Y.H. Jiang, T.Y. Chan, H.Y. Pu, H. Vaccaro, L. Wang, F. Bennett, K.X. Chen, J. Duca, S. Gavalas, Y.H. Huang, P. Pinto, M. Sannigrahi, F. Velazquez, S. Venkatraman, B. Vibulbhan, S. Agrawal, N. Butkiewicz, B. Feld, E. Ferrari, Z.Q. He, C.K. Jiang, R.E. Palermo, P. McMonagle, H.C. Huang, N.Y. Shih, G. Njoroge, and J.A. Kozlowski. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5336-5341; ISI[000294051800044].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294242800022">Swerilactones L-O, Secoiridoids with C(12) and C(13) Skeletons from Swertia mileensis</a>. Geng, C.A., X.M. Zhang, Y.B. Ma, J. Luo, and J.J. Chen. Journal of Natural Products, 2011. 74(8): p. 1822-1825; ISI[000294242800022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293419400011">Discovery of Naturally Occurring Aurones That Are Potent Allosteric Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase</a>. Haudecoeur, R., A. Ahmed-Belkacem, W. Yi, A. Fortune, R. Brillet, C. Belle, E. Nicolle, C. Pallier, J.M. Pawlotsky, and A. Boumendjel. Journal of Medicinal Chemistry, 2011. 54(15): p. 5395-5402; ISI[000293419400011]. [WOS]. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294077300010">Quinoxalin-2(1H)-One Derivatives As Inhibitors Against Hepatitis C Virus</a>. Liu, R., Z.H. Huang, M.G. Murray, X.Y. Guo, and G. Liu. Journal of medicinal chemistry, 2011. 54(16): p. 5747-5768; ISI[000294077300010]. [WOS]. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294246700012">DDX60, a DEXD/H Box Helicase, Is a Novel Antiviral Factor Promoting RIG-I-Like Receptor-Mediated Signaling</a>. Miyashita, M., H. Oshiumi, M. Matsumoto, and T. Seya. Molecular and Cellular Biology, 2011. 31(18): p. 3802-3819; ISI[000294246700012]. [WOS]. OV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294051800043">Inhibition of the Helicase Activity of the HCV NS3 Protein by Symmetrical Dimeric bis-Benzimidazoles</a>. Tunitskaya, V.L., A.V. Mukovnya, A.A. Ivanov, A.V. Gromyko, A.V. Ivanov, S.A. Streltsov, A.L. Zhuze, and S.N. Kochetkov. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5331-5335; ISI[000294051800043].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0902-091511.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
