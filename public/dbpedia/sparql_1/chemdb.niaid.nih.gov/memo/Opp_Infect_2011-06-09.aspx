

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-06-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0flvXbDrH8OcXKqNsl6LT8cN+Iw5LaDMj9IjDyhQnMhSdTTlBTNqdjW4nP4ALMIVgYUc2uDZAE/ufTqpVM+Alz3/VcFkbKmKB2xNsmOqFYget0XZwZOGosokuJY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4371A9E7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">May 27 - June 9, 2011</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21647940">Synthesis of 4-Hydroxycoumarin Heteroarylhybrids as Potential Antimicrobial Agents.</a> Siddiqui, Z.N., N.M. T, A. Ahmad, and A.U. Khan. Archiv Der Pharmazie, 2011. 344(6): p. 394-401; PMID[21647940].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21644248">Two Antimicrobial and Nematicidal Peptides Derived from Sequences Encoded Picea sitchensis.</a> Liu, R., L. Mu, H. Liu, L. Wei, T. Yan, M. Chen, K. Zhang, J. Li, D. You, and R. Lai. Journal of Peptide Science, 2011. <b>[Epub ahead of print]</b>; PMID[21644248].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21618272">Synthesis and In-vitro Antimicrobial Activity of Secondary and Tertiary Amines Containing 2-Chloro-6-methylquinoline Moiety.</a> Kumar, S., S. Bawa, D. Kaushik, and B.P. Panda. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21618272].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21640132">A Physico-Chemical and Biological Study of Novel Chitosan-Chloroquinoline Derivative for Biomedical Applications.</a> Kumar, S., P.K. Dutta, and J. Koh. International Journal of Biological Macromolecules, 2011. <b>[Epub ahead of print]</b>; PMID[21640132].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21627838">Antifungal Activity of Redox-Active Benzaldehydes that Target Cellular Antioxidation.</a> Kim, J.H., K.L. Chan, N. Mahoney, and B.C. Campbell. Annals of Clinical Microbiology and Antimicrobials, 2011. 10(1): p. 23; PMID[21627838].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21623314">Phytochemical Analysis and Antimicrobial Activities of Methanolic Extracts of Leaf, Stem and Root from Different Varieties of Labisa pumila Benth.</a> Karimi, E., H.Z. Jaafar, and S. Ahmad. Molecules, 2011. 16(6): p. 4438-4450; PMID[21623314].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21615543">Effect of Licorice Compounds Licochalcone A, Glabridin and Glycyrrhizic acid on Growth and Virulence Properties of Candida albicans.</a> Messier, C. and D. Grenier. Mycoses, 2011. <b>[Epub ahead of print]</b>; PMID[21615543].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21615531">In vitro Antifungal Evaluation and Structure-Activity Relationship of Diphenyl Diselenide and Synthetic Analogues.</a> Loreto, E.S., D.A. Nunes Mario, J.M. Santurio, S.H. Alves, C.W. Nogueira, and G. Zeni. Mycoses, 2011. <b>[Epub ahead of print]</b>; PMID[21615531].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21613975">Synthesis, Characterization, and Antimicrobial Evaluation of Oxadiazole Congeners.</a> Sadek, B. and K.M. Fahelelbom. Molecules, 2011. 16(6): p. 4339-4347; PMID[21613975].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21612375">Chemical Reactivity and Antimicrobial Activity of N-Substituted Maleimides.</a> Salewska, N., J. Boros-Majewska, I. Lacka, K. Chylinska, M. Sabisz, S. Milewski, and M.J. Milewska. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21612375].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21440963">Synthesis and Characterization of New Types of Halogenated and Alkylated Imidazolidineiminothiones and a Comparative Study of their Antitumor, Antibacterial, and Antifungal Activities.</a> Moussa, Z., M.A. El-Sharief, and A.M. El-Sharief. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2280-2289; PMID[21440963].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21637807">New Classes of Alanine Racemase Inhibitors Identified by High-Throughput Screening Show Antimicrobial Activity against Mycobacterium tuberculosis.</a> Anthony, K.G., U. Strych, K.R. Yeung, C.S. Shoen, O. Perez, K.L. Krause, M.H. Cynamon, P.A. Aristoff, and R.A. Koski. Plos One, 2011. 6(5): p. e20374; PMID[21637807].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21633789">Targeting Tuberculosis Through a Small Focused Library of 1,2,3-Triazoles.</a> Labadie, G.R., A. de la Iglesia, and H.R. Morbidoni. Molecular Diversity, 2011. <b>[Epub ahead of print]</b>; PMID[21633789].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21628538">Novel Inhibitors of InhA Efficiently Kill Mycobacterium tuberculosis under Aerobic and Anaerobic Conditions.</a> Vilcheze, C., A.D. Baughn, J. Tufariello, L. Leung, M. Kuo, C. Basler, D. Alland, J.C. Sacchettini, J.S. Freundlich, and W.R. Jacobs, Jr. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21628538].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21622974">Interaction of N-methyl-2-alkenyl-4-quinolones with ATP-dependent MurE ligase of Mycobacterium tuberculosis: antibacterial activity, molecular docking and inhibition kinetics.</a> Guzman, J.D., A. Wube, D. Evangelopoulos, A. Gupta, A. Hufner, C. Basavannacharya, M.M. Rahman, C. Thomaschitz, R. Bauer, T.D. McHugh, I. Nobeli, J.M. Prieto, S. Gibbons, F. Bucar, and S. Bhakta. Journal of Antimicrobial Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21622974].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21592801">Antimycobacterial activity of Novel 1,2,4-Oxadiazole-pyranopyridine/Chromene Hybrids Generated by Chemoselective 1,3-Dipolar Cycloadditions of Nitrile oxides.</a> Ranjith Kumar, R., S. Perumal, J.C. Menendez, P. Yogeeswari, and D. Sriram. Bioorganic and Medicinal Chemistry, 2011. 19(11): p. 3444-3450; PMID[21592801].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21546254">Synthesis and Antimycobacterial Activities of Non-purine Analogs of 6-Aryl-9-benzylpurines: Imidazopyridines, Pyrrolopyridines, Benzimidazoles, and Indoles.</a> Khoje, A.D., C. Charnock, B. Wan, S. Franzblau, and L.L. Gundersen. Bioorganic &amp; Medicinal Chemistry, 2011. 19(11): p. 3483-3491; PMID[21546254].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21489660">New Quinolin-4-yl-1,2,3-triazoles Carrying Amides, Sulphonamides and Amidopiperazines as Potential Antitubercular Agents.</a> Thomas, K.D., A.V. Adhikari, I.H. Chowdhury, E. Sumesh, and N.K. Pal. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2503-2512; PMID[21489660].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21469696">Antituberculosis Cycloartane Triterpenoids from Radermachera boniana</a>. Truong, N.B., C.V. Pham, H.T. Doan, H.V. Nguyen, C.M. Nguyen, H.T. Nguyen, H.J. Zhang, H.H. Fong, S.G. Franzblau, D.D. Soejarto, and M.V. Chau. Journal of Natural Products, 2011. 74(5): p. 1318-1322; PMID[21469696].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21468712">Effects of Hydroxypropyl-beta-cyclodextrin on Cell Growth, Activity, and Integrity of Steroid-transforming Arthrobacter simplex and Mycobacterium sp.</a> Shen, Y., M. Wang, L. Zhang, Y. Ma, B. Ma, Y. Zheng, H. Liu, and J. Luo. Applied Microbiology and Biotechnology, 2011. 90(6): p. 1995-2003; PMID[21468712].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21465667">Inhibition of Mycobacterium tuberculosis Methionine Aminopeptidases by Bengamide Derivatives.</a> Lu, J.P., X.H. Yuan, H. Yuan, W.L. Wang, B. Wan, S.G. Franzblau, and Q.Z. Ye. ChemMedChem, 2011. 6(6): p. 1041-1048; PMID[21465667].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21464254">Efficacy of Antimicrobial Peptoids against Mycobacterium tuberculosis.</a> Kapoor, R., P.R. Eimerman, J.W. Hardy, J.D. Cirillo, C.H. Contag, and A.E. Barron. Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 3058-62; PMID[21464254].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21450375">Facile Transformation of Biginelli Pyrimidin-2(1H)-ones to Pyrimidines.</a> Singh, K., B. Wan, S. Franzblau, K. Chibale, and J. Balzarini. In vitro evaluation as inhibitors of Mycobacterium tuberculosis and modulators of cytostatic activity. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2290-2294; PMID[21450375].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21440336">2,3-Dideoxy hex-2-enopyranosid-4-uloses as Promising New Anti-tubercular Agents: Design, Synthesis, Biological Evaluation and SAR Studies.</a> Saquib, M., I. Husain, S. Sharma, G. Yadav, V.K. Singh, S.K. Sharma, P. Shah, M.I. Siddiqi, B. Kumar, J. Lal, G.K. Jain, B.S. Srivastava, R. Srivastava, and A.K. Shaw. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2217-2223; PMID[21440336].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21429630">Synthesis of N-substituted 2-[(1E)-alkenyl]-4-(1H)-quinolone Derivatives as Antimycobacterial Agents against Non-tubercular Mycobacteria.</a> Wube, A.A., F. Bucar, C. Hochfellner, M. Blunder, R. Bauer, and A. Hufner. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2091-2101; PMID[21429630].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21623457">Synthesis and Anti-Mycobacterium tuberculosis Evaluation of Aza-Stilbene Derivatives.</a> Pavan, F., G.S. de Carvalho, A.D. da Silva, and C.Q. Leite. TheScientificWorldJournal, 2011. 11: p. 1113-1119; PMID[21623457].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p class="ListParagraph"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21644570">Imidazolopiperazines (I): Optimization for New Antimalarial Agents</a>.  Wu, T., A. Nagle, K. Kuhen, K. Gagaring, R. Borboa, C. Francek, Z. Chen, D. Plouffe, A. Goh, S.B. Lakshminarayana, J. Wu, H.Q. Ang, P. Zeng, M.L. Kang, W. Tan, M. Tan, N. Ye, X. Lin, C. Caldwell, J. Ek, S. Skolnik, F. Liu, J. Wang, J. Chang, C. Li, T. Hollenbeck, T. Tuntland, J. Isbell, C. Fischli, R. Brun, M. Rottmann, V. Dartois, T. Keller, T. Diagana, E. Winzeler, R. Glynne, D.C. Tully, and A.K. Chatterjee.. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21644570].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21646484">Antiplasmodial Properties of Acyl-lysyl Oligomers in Culture and Animal Models of Malaria.</a> Zaknoon, F., S. Wein, M. Krugliak, O. Meir, S. Rotem, H. Ginsburg, H. Vial, and A. Mor. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21646484].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21644541">Antimalarial Pyrido[1,2-a]benzimidazoles.</a> Ndakala, A.J., R.K. Gessner, P.W. Gitari, N. October, K.L. White, A. Hudson, F. Fakorede, D.M. Shackleford, M. Kaiser, C.L. Yeates, S.A. Charman, and K. Chibale. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21644541].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21643655">Antiplasmodial Potential of Medicinal Plant Extracts from Malaiyur and Javadhu Hills of South India.</a> Kamaraj, C., N.K. Kaushik, D. Mohanakrishnan, G. Elango, A. Bagavan, A.A. Zahir, A.A. Rahuman, and D. Sahal. Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[21643655].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.     </p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21627120">Synthesis and Antimalarial Activity of 2-Guanidino-4-oxoimidazoline Derivatives.</a> Liu, X., X. Wang, Q. Li, M.P. Kozar, V. Melendez, M.T. O&#39;Neil, and A.J. Lin. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21627120].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21629181">Synthesis and Antimalarial Activity of Novel Dihydro-artemisinin Derivatives.</a> Liu, Y., K. Cui, W. Lu, W. Luo, J. Wang, J. Huang, and C. Guo. Molecules, 2011. 16(6): p. 4527-4538; PMID[21629181].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21625717">Tandem Allylic Oxidation-Condensation/Esterification Catalyzed by Silica Gel: An Expeditious Approach Towards Antimalarial Diaryldienones and Enones from Natural Methoxylated phenylpropenes.</a> Sharma, A., N. Sharma, A. Shard, R. Kumar, D. Mohanakrishnan, Saima, A.K. Sinha, and D. Sahal. Organic &amp; Biomolecular Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21625717].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21619045">Isoflavone Dimers and Other Bioactive Constituents from the Figs of Ficus mucuso.</a>  Bankeu, J.J., R. Khayala, B.N. Lenta, D.T. Noungoue, S.A. Ngouela, S.A. Mustafa, K. Asaad, M.I. Choudhary, S.T. Prigge, R. Hasanov, A.E. Nkengfack, E. Tsamo, and M.S. Ali. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21619045].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21614544">Anti-malarial, Anti-trypanosomal, and Anti-leishmanial Activities of Jacaranone Isolated from Pentacalia desiderabilis (Vell.) Cuatrec. (Asteraceae).</a> Morais, T.R., P. Romoff, O.A. Favero, J.Q. Reimao, W.C. Lourenco, A.G. Tempone, A.D. Hristov, S.M. Di Santi, J.H. Lago, P. Sartorelli, and M.J. Ferreira. Parasitology Research, 2011. <b>[Epub ahead of print]</b>; PMID[21614544].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21531557">Identification of Plasmepsin Inhibitors as Selective Anti-malarial Agents using Ligand Based Drug Design.</a> McKay, P.B., M.B. Peters, G. Carta, C.T. Flood, E. Dempsey, A. Bell, C. Berry, D.G. Lloyd, and D. Fayne. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3335-3341; PMID[21531557].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21420762">Expeditious Synthesis and Biological Evaluation of Novel 2,N(6)-Disubstituted 1,2-dihydro-1,3,5-triazine-4,6-diamines as Potential Antimalarials.</a> Gravestock, D., A.L. Rousseau, A.C. Lourens, S.S. Moleele, R.L. van Zyl, and P.A. Steenkamp. European Journal of Medicinal Chemistry, 2011. 46(6): p. 2022-2030; PMID[21420762].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21524573">New Antiplasmodial Indole alkaloids from Hunteria zeylanica.</a> Nugroho, A.E., M. Sugai, Y. Hirasawa, T. Hosoya, K. Awang, A.H. Hadi, W. Ekasari, A. Widyawaruyanti, and H. Morita. Bioorganic and Medicinal Chemistry Letters, 2011. 21(11): p. 3417-3419; PMID[21524573].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21354139">Aryl piperazine and pyrrolidine as Antimalarial Agents. Synthesis and Investigation of Structure-activity Relationships.</a> Mendoza, A., S. Perez-Silanes, M. Quiliano, A. Pabon, S. Galiano, G. Gonzalez, G. Garavito, M. Zimic, A. Vaisberg, I. Aldana, A. Monge, and E. Deharo. Experimental Parasitology, 2011. 128(2): p. 97-103; PMID[21354139].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21120527">In vitro Antiplasmodial Activity of Ethanolic Extracts of Seaweed Macroalgae against Plasmodium falciparum.</a> Ravikumar, S., S. Jacob Inbaneson, P. Suganthi, R. Gokulakrishnan, and M. Venkatesan. Parasitology Research, 2011. 108(6): p. 1411-6; PMID[21120527].</p>

    <p class="NoSpacing"> <b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21120528">Mangrove Plants as a Source of Lead Compounds for the Development of New Antiplasmodial Drugs from South East Coast of India.</a> Ravikumar, S., S. Jacob Inbaneson, P. Suganthi, M. Venkatesan, and A. Ramu. Parasitology Research, 2011. 108(6): p. 1405-1410; PMID[21120528].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="memofmt2-1"> </p>

    <p class="NoSpacing">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290747200015">Identification, in Vitro Activity and Mode of Action of Phosphoinositide-Dependent-1 Kinase Inhibitors as Antifungal Molecules.</a> Baxter, B.K., L. DiDone, D. Ogu, S. Schor, and D.J. Krysan. ACS Chemical Biology, 2011. 6(5): p. 502-510; ISI[000290747200015].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">43. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290587700010">Antitubercular Activity of New Coumarins.</a> Cardoso, S.H., M.B. Barreto, M.C.S. Lourenco, M. Henriques, A.L.P. Candea, C.R. Kaiser, and M.V.N. de Souza. Chemical Biology &amp; Drug Design, 2011. 77(6): p. 489-493; ISI[000290587700010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">44. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290713400054">Structure-Activity Studies and Therapeutic Potential of Host Defense Peptides of Human Thrombin. Antimicrobial Agents and Chemotherapy.</a> Kasetty, G., P. Papareddy, M. Kalle, V. Rydengard, M. Morgelin, B. Albiger, M. Malmsten, and A. Schmidtchen. Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 2880-2890; ISI[000290713400054].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">45. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290667500019">Antimicrobial and Antioxidative Effects of Ugandan Medicinal Barks.</a> Kuglerova, M., H. Tesarova, J.T. Grade, K. Halamova, O. Wanyana-Maganyi, P. Van Damme, and L. Kokoska. African Journal of Biotechnology, 2011. 10(18): p. 3628-3632; ISI[000290667500019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">46. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290460800002">Synthesis, Characterization and Antimicrobial Activity of New Cobalt (II), Nickel (II) and Copper (II) Complexes with 2-(2-Hydroxy-1, 2-diphenylethylideneamino) benzoic acid.</a>  Manikshete, A.H., S.K. Sarsamkar, S.A. Deodware, V.N. Kamble, and M.R. Asabe. Inorganic Chemistry Communications, 2011. 14(5): p. 618-621; ISI[000290460800002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">47. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290541400006">Short-term Antimicrobial Properties of Mineral Trioxide Aggregate with Incorporated Silver-zeolite.</a> Odabas, M.E., C. Cinar, G. Akca, I. Araz, T. Ulusu, and H. Yucel. Dental Traumatology, 2011. 27(3): p. 189-194; ISI[000290541400006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">48. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290815400040">Synthesis, Spectroscopy and Biological Investigations of Manganese(III) Schiff base Complexes Derived from Heterocyclic Beta-diketone with Various Primary Amine and 2,2 &#39;-Bipyridyl.</a> Surati, K.R. Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2011. 79(1): p. 272-277; ISI[000290815400040].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">49. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290713400028">Chemical Characterization and Biological Properties of NVC-422, a Novel, Stable N-Chlorotaurine Analog.</a> Wang, L., B. Belisle, M. Bassiri, P. Xu, D. Debabov, C. Celeri, N. Alvarez, M.C. Robson, W.G. Payne, R. Najafi, and B. Khosrovi. Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 2688-2692; ISI[000290713400028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0527-060911.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
