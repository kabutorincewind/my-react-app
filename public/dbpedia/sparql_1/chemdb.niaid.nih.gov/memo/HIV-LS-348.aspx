

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-348.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9HHZ3jFcKthralWGGNYwmpB1bS7TO0na8Qu7mC5hxIsmvJ26Z4Dq7NCeEiFYCXPFgE1/fsRqTFfvfEVCJYfU+X30S52xCtrKTtDSC7soZea9JDdPL4ji2GoAeVc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="13F9225C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-348 MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48869   HIV-LS-348; EMBASE-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Synthesis, antiviral activity, and pharmacokinetic evaluation of P3 pyridylmethyl analogs of oximinoarylsulfonyl HIV-1 protease inhibitors</p>

    <p>          Randolph, John T, Huang, Peggy P, Flosi, William J, DeGoey, David, Klein, Larry L, Yeung, Clinton M, Flentge, Charles, Sun, Mingua, Zhao, Chen, and Dekhtyar, Tatyana</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(12): 4035-4046</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4JCBN69-5/2/359cbaf21b0bba60c34e8493cd2f50ea">http://www.sciencedirect.com/science/article/B6TF8-4JCBN69-5/2/359cbaf21b0bba60c34e8493cd2f50ea</a> </p><br />

    <p>2.     48870   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by a peptide dimerization inhibitor of HIV-1 protease</p>

    <p>          Davis, DA, Brown, CA, Singer, KE, Wang, V, Kaufman, J, Stahl, SJ, Wingfield, P, Maeda, K, Harada, S, Yoshimura, K, Kosalaraksa, P, Mitsuya, H, and Yarchoan, R</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16687179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16687179&amp;dopt=abstract</a> </p><br />

    <p>3.     48871   HIV-LS-348; EMBASE-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Mechanism of Drug Resistance Revealed by the Crystal Structure of the Unliganded HIV-1 Protease with F53L Mutation</p>

    <p>          Liu, Fengling, Kovalevsky, Andrey Y, Louis, John M, Boross, Peter I, Wang, Yuan-Fang, Harrison, Robert W, and Weber, Irene T</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  358(5): 1191-1199</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4JHMHCT-4/2/e5dd2681a5918c5811fc1790d45500b4">http://www.sciencedirect.com/science/article/B6WK7-4JHMHCT-4/2/e5dd2681a5918c5811fc1790d45500b4</a> </p><br />

    <p>4.     48872   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV and antitubercular activities of isatin derivatives</p>

    <p>          Sriram, D, Yogeeswari, P, and Meena, K</p>

    <p>          Pharmazie <b>2006</b>.  61(4): 274-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16649536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16649536&amp;dopt=abstract</a> </p><br />

    <p>5.     48873   HIV-LS-348; EMBASE-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Hexapeptides that interfere with HIV-1 fusion peptide activity in liposomes block GP41-mediated membrane fusion</p>

    <p>          Gomara, Maria J, Lorizate, Maier, Huarte, Nerea, Mingarro, Ismael, Perez-Paya, Enrique, and Nieva, Jose L</p>

    <p>          FEBS Letters <b>2006</b>.  580(11): 2561-2566</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4JSF4WN-9/2/c471ea4fb4bfd626149bc29ba5492b08">http://www.sciencedirect.com/science/article/B6T36-4JSF4WN-9/2/c471ea4fb4bfd626149bc29ba5492b08</a> </p><br />
    <br clear="all">

    <p>6.     48874   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          theta -Defensins prevent HIV-1 env-mediated fusion by binding gp41 and blocking 6-helix bundle formation</p>

    <p>          Gallo, SA, Wang, W, Rawat, SS, Jung, G, Waring, AJ, Cole, AM, Lu, H, Yan, X, Daly, NL, Craik, DJ, Jiang, S, Lehrer, RI, and Blumenthal, R</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16648135&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16648135&amp;dopt=abstract</a> </p><br />

    <p>7.     48875   HIV-LS-348; EMBASE-HIV-5/15/2006</p>

    <p class="memofmt1-2">          HIV co-receptor inhibitors as novel class of anti-HIV drugs</p>

    <p>          Schols, Dominique</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-3/2/97a23695f4e3249b71f681c276f5ee14">http://www.sciencedirect.com/science/article/B6T2H-4JWMSCF-3/2/97a23695f4e3249b71f681c276f5ee14</a> </p><br />

    <p>8.     48876   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Cyclophilin A renders human immunodeficiency virus type 1 sensitive to old world monkey but not human TRIM5{alpha} antiviral activity</p>

    <p>          Keckesova, Z, Ylinen, LM, and Towers, GJ</p>

    <p>          J Virol <b>2006</b>.  80(10): 4683-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641261&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641261&amp;dopt=abstract</a> </p><br />

    <p>9.     48877   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Discovery of a piperidine-4-carboxamide CCR5 antagonist (TAK-220) with highly potent Anti-HIV-1 activity</p>

    <p>          Imamura, S, Ichikawa, T, Nishikawa, Y, Kanzaki, N, Takashima, K, Niwa, S, Iizawa, Y, Baba, M, and Sugihara, Y</p>

    <p>          J Med Chem <b>2006</b>.  49(9): 2784-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640339&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640339&amp;dopt=abstract</a> </p><br />

    <p>10.   48878   HIV-LS-348; EMBASE-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Clinical pharmacodynamics of HIV-1 protease inhibitors: use of inhibitory quotients to optimise pharmacotherapy</p>

    <p>          Morse, Gene D, Catanzaro, Linda M, and Acosta, Edward P</p>

    <p>          The Lancet Infectious Diseases <b>2006</b>.  6(4): 215-225</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8X-4JHV0JG-P/2/980f0a967c7019304ef409595b76bb05">http://www.sciencedirect.com/science/article/B6W8X-4JHV0JG-P/2/980f0a967c7019304ef409595b76bb05</a> </p><br />
    <br clear="all">

    <p>11.   48879   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Delivery of double-stranded DNA thioaptamers into HIV-1 infected cells for antiviral activity</p>

    <p>          Ferguson, MR, Rojo, DR, Somasunderam, A, Thiviyanathan, V, Ridley, BD, Yang, X, and Gorenstein, DG</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  344(3): 792-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631118&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16631118&amp;dopt=abstract</a> </p><br />

    <p>12.   48880   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel open-chain analogues of neplanocin A</p>

    <p>          Hong, JH, Kim, SY, Oh, CH, Yoo, KH, and Cho, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(3): 341-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16629127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16629127&amp;dopt=abstract</a> </p><br />

    <p>13.   48881   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          A new structural theme in C(2)-symmetric HIV-1 protease inhibitors: ortho-Substituted P1/P1&#39; side chains</p>

    <p>          Wannberg, J, Sabnis, YA, Vrang, L, Samuelsson, B, Karlen, A, Hallberg, A, and Larhed, M</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621572&amp;dopt=abstract</a> </p><br />

    <p>14.   48882   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          Antiviral drug resistance</p>

    <p>          Richman, DD</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621040&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621040&amp;dopt=abstract</a> </p><br />

    <p>15.   48883   HIV-LS-348; PUBMED-HIV-5/15/2006</p>

    <p class="memofmt1-2">          HIV-1 Tat interaction with cyclin T1 represses mannose receptor and the bone morphogenetic protein receptor-2 transcription</p>

    <p>          Caldwell, RL, Lane, KB, and Shepherd, VL</p>

    <p>          Arch Biochem Biophys <b>2006</b>.  449(1-2): 27-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16615932&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16615932&amp;dopt=abstract</a> </p><br />

    <p>16.   48884   HIV-LS-348; WOS-HIV-5/7/2006</p>

    <p class="memofmt1-2">          Evolutionary dynamics of HIV-1 and the control of AIDS</p>

    <p>          Mullins, JI and Jensen, MA</p>

    <p>          QUASISPECIES: CONCEPT AND IMPLICATIONS FOR VIROLOGY <b>2006</b>.  299: 171-192, 22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236797900006</a> </p><br />

    <p>17.   48885   HIV-LS-348; WOS-HIV-5/7/2006</p>

    <p class="memofmt1-2">          Hexamethylbisacetamide remodels the human immunodeficiency virus type 1 (HIV-1) promoter and induces tat-independent HIV-1 expression but blunts cell activation</p>

    <p>          Klichko, V, Archin, N, Kaur, R, Lehrman, G, and Margolis, D</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(9): 4570-4579, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236980000038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236980000038</a> </p><br />

    <p>18.   48886   HIV-LS-348; WOS-HIV-5/7/2006</p>

    <p class="memofmt1-2">          Biosensor-based kinetic characterization of the interaction between HIV-1 reverse transcriptase and non-nucleoside inhibitors</p>

    <p>          Geitmann, M, Unge, T, and Danielson, UH</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(8): 2367-2374, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236979100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236979100002</a> </p><br />

    <p>19.   48887   HIV-LS-348; WOS-HIV-5/7/2006</p>

    <p class="memofmt1-2">          Interaction kinetic characterization of HIV-1 reverse transcriptase non-nucleoside inhibitor resistance</p>

    <p>          Geitmann, M, Unge, T, and Danielson, UH</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(8): 2375-2387, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236979100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236979100003</a> </p><br />

    <p>20.   48888   HIV-LS-348; WOS-HIV-5/7/2006</p>

    <p class="memofmt1-2">          Further screening of Venda medicinal plants for activity against HIV type 1 reverse transcriptase and integrase</p>

    <p>          Bessong, PO, Rojas, LB, Obi, LC, Tshisikawe, PM, and Igunbor, EO</p>

    <p>          AFRICAN JOURNAL OF BIOTECHNOLOGY <b>2006</b>.  5(6): 526-528, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237014700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237014700008</a> </p><br />

    <p>21.   48889   HIV-LS-348; WOS-HIV-5/14/2006</p>

    <p class="memofmt1-2">          Fast and selective synthesis of novel cyclic sulfamide HIV-1 protease inhibitors under controlled microwave heating</p>

    <p>          Gold, H, Ax, A, Vrang, L, Samuelsson, B, Karlen, A, Hallberga, A, and Larhed, M</p>

    <p>          TETRAHEDRON <b>2006</b>.  62(19): 4671-4675, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237126500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237126500006</a> </p><br />

    <p>22.   48890   HIV-LS-348; WOS-HIV-5/14/2006</p>

    <p class="memofmt1-2">          Divergent synthesis and biological evaluation of carbocyclic alpha-, iso- and 3 &#39;-epi-nucleosides and their lipophilic nucleotide prodrugs</p>

    <p>          Ludek, OR, Kramer, T, Balzarini, J, and Meier, C</p>

    <p>          SYNTHESIS-STUTTGART <b>2006</b>.(8): 1313-1324, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237076800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237076800014</a> </p><br />

    <p>23.   48891   HIV-LS-348; WOS-HIV-5/14/2006</p>

    <p class="memofmt1-2">          Advances in human immunodeficiency virus therapeutics</p>

    <p>          Clay, PG, Dong, BJ, Sorensen, SJ, Tseng, A, Rornanelli, F, and Antoniou, T</p>

    <p>          ANNALS OF PHARMACOTHERAPY <b>2006</b>.  40(4): 704-709, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237005900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237005900017</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
