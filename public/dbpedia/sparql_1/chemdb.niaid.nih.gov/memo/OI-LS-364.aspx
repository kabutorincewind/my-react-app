

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-364.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9c1+kikbc+o0mpctt7ZAKoGG5oKAseUXyxOz0qpfqVZiuG47fcUou9Oi3Ku2VWTIwNL0EvtCg53HUblIsvGw9+YNFmuts5v3LUeoNUDU35Ww/6VoMy/pIu3hKBI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2F4DC1CF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-364-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59790   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Iron enhances the susceptibility of pathogenic mycobacteria to isoniazid, an antitubercular drug</p>

    <p>          Sritharan, M, Yeruva, VC, Sivasailappan, SC, and Duggirala, S</p>

    <p>          WORLD JOURNAL OF MICROBIOLOGY &amp; BIOTECHNOLOGY <b>2006</b>.  22(12 ): 1357-1364, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242288800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242288800016</a> </p><br />

    <p>2.     59791   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          Superexpression of tuberculosis antigens in plant leaves</p>

    <p>          Dorokhov, Yuri L, Sheveleva, Anna A, Frolova, Olga Y, Komarova, Tatjana V, Zvereva, Anna S, Ivanov, Peter A, and Atabekov, Joseph G</p>

    <p>          Tuberculosis <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXK-4MM2639-1/2/9247334ea142ba9bd223bdb9f4ca59e0">http://www.sciencedirect.com/science/article/B6WXK-4MM2639-1/2/9247334ea142ba9bd223bdb9f4ca59e0</a> </p><br />

    <p>3.     59792   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Plants and fungal products with activity against tuberculosis</p>

    <p>          De Souza, MVN</p>

    <p>          THESCIENTIFICWORLDJOURNAL <b>2005</b>.  5: 609-628, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385200005</a> </p><br />

    <p>4.     59793   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Marine natural products against tuberculosis</p>

    <p>          De Souza, MVN</p>

    <p>          THESCIENTIFICWORLDJOURNAL <b>2006</b>.  6: 847-861, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242388800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242388800011</a> </p><br />

    <p>5.     59794   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Peginterferon and ribavirin for chronic hepatitis C</p>

    <p>          Hoofnagle, JH and Seeff, LB</p>

    <p>          NEW ENGLAND JOURNAL OF MEDICINE <b>2006</b>.  355(23): 2444-2451, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242581400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242581400008</a> </p><br />

    <p>6.     59795   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          CoMFA and CoMSIA 3D-QSAR analysis of diaryloxy-methano-phenanthrene derivatives as anti-tubercular agents</p>

    <p>          Shagufta, Kumar, A, Panda, G, and Siddiqi, MI</p>

    <p>          JOURNAL OF MOLECULAR MODELING <b>2007</b>.  13(1): 99-109, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242326700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242326700011</a> </p><br />

    <p>7.     59796   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Quantitative structure-activity relationship (QSAR) studies of quinolone antibacterials against M-fortuitum and M-smegmatis using theoretical molecular descriptors</p>

    <p>          Bagchi, MC, Mills, D, and Basak, SC</p>

    <p>          JOURNAL OF MOLECULAR MODELING <b>2007</b>.  13(1): 111-120, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242326700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242326700012</a> </p><br />

    <p>8.     59797   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Microaerophilic conditions increase viability and affect responses of Pneumocystis carinii to drugs in vitro</p>

    <p>          Joffrion, TM, Collins, MS, and Cushion, MT</p>

    <p>          JOURNAL OF EUKARYOTIC MICROBIOLOGY <b>2006</b>.  53: S117-S118, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900043">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900043</a> </p><br />

    <p>9.     59798   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Detection of a proteasome in Pneumocystis carinii and its modulation by specific proteasome inhibitors</p>

    <p>          Tronconi, E, Mazza, F, Valerio, A, Rinaudo, MT, Piccinini, M, Anselmino, A, Cargnel, A, and Atzori, C</p>

    <p>          JOURNAL OF EUKARYOTIC MICROBIOLOGY <b>2006</b>.  53: S142-S143, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900054</a> </p><br />

    <p>10.   59799   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Mutational analysis of Encephalitozoon cuniculi mRNA cap (guanine-N7) methyltransferase, structure of the enzyme bound to sinefungin, and evidence that cap methyltransferase is the target of sinefungin&#39;s antifungal activity</p>

    <p>          Zheng, SS, Hausmann, S, Hausmann, P, Liu, QS, Ghosh, A, Schwer, B, Lima, CD, and Shuman, S</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2006</b>.  281(47): 35904-35913, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242100500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242100500037</a> </p><br />

    <p>11.   59800   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Effects of weak acids, UV and proton motive force inhibitors on pyrazinamide activity against Mycobacterium tuberculosis in vitro</p>

    <p>          Wade, MM and Zhang, Y</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2006</b>.  58(5): 936-941, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900004</a> </p><br />

    <p>12.   59801   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          New antifungal flavonoid glycoside from Vitex negundo</p>

    <p>          Sathiamoorthy, B, Gupta, Prasoon, Kumar, Manmeet, Chaturvedi, Ashok K, Shukla, PK, and Maurya, Rakesh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(1): 239-242</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M21SX1-9/2/c277dc12090c1c087accdcfb9ee1271a">http://www.sciencedirect.com/science/article/B6TF9-4M21SX1-9/2/c277dc12090c1c087accdcfb9ee1271a</a> </p><br />

    <p>13.   59802   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis, characterization and antimicrobial activity of Fe(II), Zn(II), Cd(II) and Hg(II) complexes with 2,6-bis(benzimidazol-2-yl) pyridine ligand</p>

    <p>          Aghatabay, Naz M, Neshat, A, Karabiyik, T, Somer, M, Haciu, D, and Dulger, B</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MNJ2R1-1/2/adeb65109cccb73db98b9edb7e31674e">http://www.sciencedirect.com/science/article/B6VKY-4MNJ2R1-1/2/adeb65109cccb73db98b9edb7e31674e</a> </p><br />
    <br clear="all">

    <p>14.   59803   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Surfactant protein D increases fusion of Mycobacterium tuberculosis-containing phagosomes with lysosomes in human macrophages</p>

    <p>          Ferguson, JS, Martin, JL, Azad, AK, McCarthy, TR, Kang, PB, Voelker, DR, Crouch, EC, and Schlesinger, LS</p>

    <p>          INFECTION AND IMMUNITY <b>2006</b>.  74(12): 7005-7009, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242308100053">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242308100053</a> </p><br />

    <p>15.   59804   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          Silencing of USP18 Potentiates the Antiviral Activity of Interferon Against Hepatitis C Virus Infection</p>

    <p>          Randall, Glenn, Chen, Limin, Panis, Maryline, Fischer, Andrew K, Lindenbach, Brett D, Sun, Jing, Heathcote, Jenny, Rice, Charles M, Edwards, Aled M, and McGilvray, Ian D</p>

    <p>          Gastroenterology <b>2006</b>.  131(5): 1584-1591</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4KPN7G4-1/2/f7bd4ad5d8292e8c02655b4958fd84e1">http://www.sciencedirect.com/science/article/B6WFX-4KPN7G4-1/2/f7bd4ad5d8292e8c02655b4958fd84e1</a> </p><br />

    <p>16.   59805   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          The oriented development of antituberculotics: Salicylanilides</p>

    <p>          Waisser, K, Matyk, J, Divisova, H, Husakova, P, Kunes, J, Klimesova, V, Kaustova, J, Mollmann, U, Dahse, HM, and Miko, M</p>

    <p>          ARCHIV DER PHARMAZIE <b>2006</b>.  339(11): 616-620, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385800004</a> </p><br />

    <p>17.   59806   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          In vitro antimicrobial and resistance-modifying activities of aqueous crude khat extracts against oral microorganisms</p>

    <p>          Al-hebshi, Nezar, Al-haroni, Mohammed, and Skaug, Nils</p>

    <p>          Archives of Oral Biology <b>2006</b>.  51(3): 183-188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4J-4HD8DMV-1/2/93170cf3b431b08ffd37cb8c808fcfa5">http://www.sciencedirect.com/science/article/B6T4J-4HD8DMV-1/2/93170cf3b431b08ffd37cb8c808fcfa5</a> </p><br />

    <p>18.   59807   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          RNAi-Based Therapy for the Treatment of HCV</p>

    <p>          Couto, Linda B, Parker, Amy E, Haniff, Gabriel, Suhy, David A, Kolykhalov, Alexander A, Roelvink, Peter W, Maria Garcia, Luz, Schroeder, Rosanna, Kay, Mark A, and Cunningham, Sara M</p>

    <p>          Molecular Therapy <b>2006</b>.  13(Supplement 1): S422-S423</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WNJ-4M10KTK-1C8/2/42e671bd0884dbb7e820139d8077a33a">http://www.sciencedirect.com/science/article/B6WNJ-4M10KTK-1C8/2/42e671bd0884dbb7e820139d8077a33a</a> </p><br />

    <p>19.   59808   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          Micafungin (FK463), alone or in combination with other systemic antifungal agents, for the treatment of acute invasive aspergillosis</p>

    <p>          Denning, David W, Marr, Kieren A, Lau, Wendi M, Facklam, David P, Ratanatharathorn, Voravit, Becker, Cornelia, Ullmann, Andrew J, Seibel, Nita L, Flynn, Patricia M, and van Burik, Jo-Anne H</p>

    <p>          Journal of Infection <b>2006</b>.  53(5): 337-349</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WJT-4JWMT1M-1/2/a4e47e855b523e3894e5b709379ef151">http://www.sciencedirect.com/science/article/B6WJT-4JWMT1M-1/2/a4e47e855b523e3894e5b709379ef151</a> </p><br />

    <p>20.   59809   OI-LS-364; EMBASE-OI-12/26/2006</p>

    <p class="memofmt1-2">          Antimicrobial activities of the leaf extracts of two Moroccan Cistus L. species</p>

    <p>          Bouamama, H, Noel, T, Villard, J, Benharref, A, and Jana, M</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  104(1-2): 104-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4H8FPT6-4/2/ca4ed8a43b075f429955d2cc8138b265">http://www.sciencedirect.com/science/article/B6T8D-4H8FPT6-4/2/ca4ed8a43b075f429955d2cc8138b265</a> </p><br />

    <p>21.   59810   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Toxoplasma gondii: Presentations at the IX International Workshop on Opportunistic Protists and the International Society of Protistologists 57th Annual Meetings</p>

    <p>          Mitchell, S and Sinai, A</p>

    <p>          JOURNAL OF EUKARYOTIC MICROBIOLOGY <b>2006</b>.  53: S159-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900061">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242309900061</a> </p><br />

    <p>22.   59811   OI-LS-364; WOS-OI-12/18/2006</p>

    <p class="memofmt1-2">          Evaluation of dosing regimen of respirable rifampicin biodegradable microspheres in the treatment of tuberculosis in the guinea pig</p>

    <p>          Garcia-Contreras, L, Sethuraman, V, Kazantseva, M, Godfrey, V, and Hickey, AJ</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2006</b>.  58(5): 980-986, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242342900010</a> </p><br />

    <p>23.   59812   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          N-terminal positively charged amino acids, but not their exact position, are important for apicoplast transit peptide fidelity in Toxoplasma gondii</p>

    <p>          Tonkin, CJ, Roos, DS, and McFadden, GI</p>

    <p>          MOLECULAR AND BIOCHEMICAL PARASITOLOGY <b>2006</b>.  150(2): 192-200, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242476900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242476900008</a> </p><br />

    <p>24.   59813   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          A new hydrogenated azaphilone Sch 725680 from Aspergillus sp</p>

    <p>          Yang, SW, Chan, TM, Terracciano, J, Patel, R, Patel, M, Gullo, V, and Chu, M</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2006</b>.  59(11): 720-723, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242489800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242489800006</a> </p><br />

    <p>25.   59814   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          Synthesis and characterization of novel hydrazide-hydrazones and the study of their structure-antituberculosis activity</p>

    <p>          Bedia, KK, Elcin, O, Seda, U, Fatma, K, Nathaly, S, Sevim, R, and Dimoglo, A</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(11): 1253-1261, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242454100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242454100004</a> </p><br />

    <p>26.   59815   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          Inhibitors of the hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Colarusso, S, Attenni, B, Avolio, S, Malancona, S, Harper, S, Altamura, S, Koch, U, and Narjes, F</p>

    <p>          ARKIVOC <b>2006</b>.: 479-495, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900033</a> </p><br />

    <p>27.   59816   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          Designing heterocyclic selective kinase inhibitors: from concept to new drug candidates</p>

    <p>          Halazy, S</p>

    <p>          ARKIVOC <b>2006</b>.: 496-508, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900034</a> </p><br />

    <p>28.   59817   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          Functional analysis of DNA gyrase mutant enzymes carrying mutations at position 88 in the A subunit found in clinical strains of Mycobacterium tuberculosis resistant to fluoroquinolones</p>

    <p>          Matrat, S, Veziris, N, Mayer, C, Jarlier, V, Truffot-Pernot, C, Camuset, J, Bouvet, E, Cambau, E, and Aubry, A</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(12): 4170-4173, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242470200026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242470200026</a> </p><br />

    <p>29.   59818   OI-LS-364; WOS-OI-12/24/2006</p>

    <p class="memofmt1-2">          8-aminoquinolines: future role as antiprotozoal drugs</p>

    <p>          Tekwani, BL and Walker, LA</p>

    <p>          CURRENT OPINION IN INFECTIOUS DISEASES <b>2006</b>.  19(6): 623-631, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242513400014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242513400014</a> </p><br />

    <p>30.   59819   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of quaternized promazine and promethazine derivatives</p>

    <p>          Bate, AB, Kalin, JH, Fooksman, EM, Amorose, EL, Price, CM, Williams, HM, Rodig, MJ, Mitchell, MO, Cho, SH, Wang, Y, and Franzblau, SG</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188865&amp;dopt=abstract</a> </p><br />

    <p>31.   59820   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of novel 1-(1H-1,2,4-triazol-1-yl)-2-(2,4-difluorophenyl)-3-[(4-substitutedphenyl)- piperazin-1-yl]-propan-2-ols as antifungal agents</p>

    <p>          Sun, QY, Zhang, WN, Xu, JM, Cao, YB, Wu, QY, Zhang, DZ, Liu, CM, Yu, SC, and Jiang, YY</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17184885&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17184885&amp;dopt=abstract</a> </p><br />

    <p>32.   59821   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis, antifungal activity, and structure-activity relationships of coruscanone a analogues</p>

    <p>          Babu, KS, Li, XC, Jacob, MR, Zhang, Q, Khan, SI, Ferreira, D, and Clark, AM</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7877-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181171&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181171&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   59822   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and Antituberculosis Activity of a Novel Series of Optically Active 6-Nitro-2,3-dihydroimidazo[2,1-b]oxazoles</p>

    <p>          Sasaki, H, Haraguchi, Y, Itotani, M, Kuroda, H, Hashizume, H, Tomishige, T, Kawasaki, M, Matsumoto, M, Komatsu, M, and Tsubouchi, H</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7854-7860</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181168&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181168&amp;dopt=abstract</a> </p><br />

    <p>34.   59823   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Quinoline derivative MC1626, a putative GCN5 histone acetyltransferase (HAT) inhibitor, exhibits HAT-independent activity against Toxoplasma gondii</p>

    <p>          Smith, AT, Livingston, MR, Mai, A, Filetici, P, Queener, SF, and Sullivan, WJ Jr</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178801&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178801&amp;dopt=abstract</a> </p><br />

    <p>35.   59824   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Virally activated CD8 T cells home to BCG-induced granulomas, but enhance anti-mycobacterial protection only in immunodeficient mice</p>

    <p>          Hogan, LH, Co, DO, Karman, J, Heninger, E, Suresh, M, and Sandor, M</p>

    <p>          Infect Immun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178783&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178783&amp;dopt=abstract</a> </p><br />

    <p>36.   59825   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p><b>          Kinetic and chemical mechanisms of shikimate dehydrogenase from Mycobacterium tuberculosis</b> </p>

    <p>          Fonseca, IO, Silva, RG, Fernandes, CL, de, Souza ON, Basso, LA, and Santos, DS</p>

    <p>          Arch Biochem Biophys <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178095&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178095&amp;dopt=abstract</a> </p><br />

    <p>37.   59826   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          A Novel Inhibitor of Mycobacterium tuberculosis Pantothenate Synthetase</p>

    <p>          White, EL, Southworth, K, Ross, L, Cooley, S, Gill, RB, Sosa, MI, Manouvakhova, A, Rasmussen, L, Goulding, C, Eisenberg, D, and Fletcher, TM 3rd</p>

    <p>          J Biomol Screen <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17175524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17175524&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>38.   59827   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          A novel anticancer agent ARC antagonizes HIV-1 and HCV</p>

    <p>          Nekhai, S, Bhat, UG, Ammosova, T, Radhakrishnan, SK, Jerebtsova, M, Niu, X, Foster, A, Layden, TJ, and Gartel, AL</p>

    <p>          Oncogene <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17173067&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17173067&amp;dopt=abstract</a> </p><br />

    <p>39.   59828   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          The Non-Peptidic HIV Protease Inhibitor Tipranavir and Two Synthetic Peptidomimetics (TS98 and TS102) Modulate Pneumocystis carinii Growth and Proteasome Activity of HEL299 Cell Line</p>

    <p>          Mazza, F, Tronconi, E, Valerio, A, Groettrup, M, Kremer, M, Tossi, A, Benedetti, F, Cargnel, A, and Atzori, C</p>

    <p>          J Eukaryot Microbiol <b>2006</b>.  53 Suppl 1: S144-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169036&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169036&amp;dopt=abstract</a> </p><br />

    <p>40.   59829   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          The suppressive effect of Mekabu fucoidan on an attachment of Cryptosporidium parvum oocysts to the intestinal epithelial cells in neonatal mice</p>

    <p>          Maruyama, H, Tanaka, M, Hashimoto, M, Inoue, M, and Sasahara, T</p>

    <p>          Life Sci <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157323&amp;dopt=abstract</a> </p><br />

    <p>41.   59830   OI-LS-364; PUBMED-OI-12/26/2006</p>

    <p class="memofmt1-2">          Diacyltrehalose of Mycobacterium tuberculosis inhibits lipopolysaccharide- and mycobacteria-induced proinflammatory cytokine production in human monocytic cells</p>

    <p>          Lee, KS, Dubey, VS, Kolattukudy, PE, Song, CH, Shin, AR, Jung, SB, Yang, CS, Kim, SY, Jo, EK, Park, JK, and Kim, HJ</p>

    <p>          FEMS Microbiol Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17156119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17156119&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
