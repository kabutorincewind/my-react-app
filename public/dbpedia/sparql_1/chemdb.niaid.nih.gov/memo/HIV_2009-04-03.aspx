

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-04-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="O7dfB8lt5LmSxtEcNorJ2C0BxD/SRSasmSc4ZzlmRks+ZscaUrDabOpHc2bxPofzD6807TOOFF3yoYp3qD3VGVET5RqTIxHe6Vd3iMMM6khnZnqdQ+57Y3f5NFc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F3FC1329" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 18 - March 31, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19334055?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">WR1065 mitigates AZT-ddI-induced mutagenesis and inhibits viral replication.</a>  Walker DM, Kajon AE, Torres SM, Carter MM, McCash CL, Swenberg JA, Upton PB, Hardy AW, Olivero OA, Shearer GM, Poirier MC, Walker VE.  Environ Mol Mutagen. 2009 Mar 30. [Epub ahead of print] </p>

    <p class="title">PMID: 19334055</p>

    <p class="title">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19332801?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Scaleable manufacture of HIV-1 entry inhibitor griffithsin and validation of its safety and efficacy as a topical microbicide component.</a>  O&#39;Keefe BR, Vojdani F, Buffa V, Shattock RJ, Montefiori DC, Bakke J, Mirsalis J, d&#39;Andrea AL, Hume SD, Bratcher B, Saucedo CJ, McMahon JB, Pogue GP, Palmer KE.  Proc Natl Acad Sci U S A. 2009 Mar 30. [Epub ahead of print] </p>

    <p class="title">PMID: 19332801</p>

    <p class="title">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19323562?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">2-Pyridyl P1&#39;-Substituted Symmetry-Based Human Immunodeficiency Virus Protease Inhibitors (A-792611 and A-790742) with Potential for Convenient Dosing and Reduced Side Effects (dagger).</a>  Degoey DA, Grampovnik DJ, Flentge CA, Flosi WJ, Chen HJ, Yeung CM, Randolph JT, Klein LL, Dekhtyar T, Colletti L, Marsh KC, Stoll V, Mamo M, Morfitt DC, Nguyen B, Schmidt JM, Swanson SJ, Mo H, Kati WM, Molla A, Kempf DJ.  J Med Chem. 2009 Mar 26. [Epub ahead of print] </p>

    <p class="title">PMID: 19323562</p>

    <p class="title">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19299483?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">15-Deoxy-{Delta}12,14-prostaglandin J2 inhibits HIV-1 transactivating protein, Tat, through covalent modification.</a>  Kalantari P, Narayan V, Henderson AJ, Prabhu KS.  FASEB J. 2009 Mar 19. [Epub ahead of print] </p>

    <p class="title">PMID: 19299483</p>

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19297617?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Addition of a cholesterol group to an HIV-1 peptide fusion inhibitor dramatically increases its antiviral potency.</a>  Ingallinella P, Bianchi E, Ladwa NA, Wang YJ, Hrin R, Veneziano M, Bonelli F, Ketas TJ, Moore JP, Miller MD, Pessi A.  Proc Natl Acad Sci U S A. 2009 Mar 18. [Epub ahead of print] </p>

    <p class="title">PMID: 19297617</p>

    <p class="title">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19296658?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Peptide-Poly(l-lysine citramide) Conjugates and their In Vitro Anti-HIV Behavior.</a>  Couffin-Hoarau AC, Aubertin AM, Boustta M, Schmidt S, Fehrentz JA, Martinez J, Vert M.  Biomacromolecules. 2009 Mar 18. [Epub ahead of print] </p>

    <p class="title">PMID: 19296658</p>

    <p class="title">7.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19222168?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Enantiodivergent synthesis of cyclohexenyl nucleosides.</a>  Ferrer E, Alibés R, Busqué F, Figueredo M, Font J, de March P.  J Org Chem. 2009 Mar 20;74(6):2425-32. </p>

    <p class="title">PMID: 19222168</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">8.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263946000006">Purification and characterization of a novel lectin from the toxic wild mushroom Inocybe umbrinella</a>.  Zhao JK, Wang HX, Ng TB.  Toxicon.  2009 Mar; 53(3):360-366.</p>

    <p class="title">9.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263933800013">[2-(4-Phenyl-4-piperidinyl)ethyl]amine based CCR5 antagonists: derivatizations at the N-terminal of the piperidine ring</a>.  Duan MS, Aquino C, Ferris R, Kazmierski WM, Kenakin T, Koble C, Wheelan P, Watson C, Youngman M.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009; 19(6): 1610-1613. </p>

    <p class="title">10.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263945800022">Convenient, two-step synthesis of 2-styrylquinolines: an application of the CAN-catalyzed vinylogous type-II Povarov reaction</a>.  Sridharan V, Avendano C, Menendez JC.  TETRAHEDRON.  2009; 65(10): 2087-2096. </p> 

    <p class="title">11.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264245600021">Synthesis of New Cyclic and Acyclic 5-Halouridine Derivatives as Potential Antiviral Agents</a>.  Elshehry MF, Balzarini J, Meier C.  SYNTHESIS-STUTTGART.  2009; 5: 841-847. </p> 

    <p class="title">12.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263980400009">Trypsin-Chymotrypsin Inhibitors from Vigna mungo Seeds</a>.  Cheung AHK, Wong JH, Ng TB.  PROTEIN AND PEPTIDE LETTERS.  2009; 16(3): 277-284. </p> 

    <p class="title">13.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264236000019">Synthesis, spectroscopy, computational study and prospective biological activity of two novel 1,3,2 diazaphospholidine-2,4,5-triones</a>.  Gholivand K, Oroujzadeh N, Erben MF   Della Vedova CO.   POLYHEDRON.  2009; 28(3): 541-547. </p> 

    <p class="title">14.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263979000006">Synthesis of Tipranavir Analogues as Non-Peptidic HIV Protease Inhibitors</a>.  Ding YL, Prasad CVNSV, Smith KL, Chang E, Hong J, Yao NH.  LETTERS IN ORGANIC CHEMISTRY.  2009; 6(2): 130-133. </p> 

    <p class="title">15.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263970800009">Presentation of the beta-Carboxamidophosphonate Arrangement in Substrate Structures Targeting HIV-1 PR</a>.  Wardle NJ, Hudson HR, Matthews RW, Nunn C, Vella C, Bligh SWA.  LETTERS IN DRUG DESIGN &amp; DISCOVERY.  2009; 6(2): 139-145. </p> 

    <p class="title">16.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264165300026">Synthesis of Novel 3-Acetyl-2-aryl-5-(3-aryl-1-phenyl-pyrazol-4-yl)-2,3-dihydro-1,3,4-oxadiazoles</a>.  Li CK, Ma YJ, Cao LH.  JOURNAL OF THE CHINESE CHEMICAL SOCIETY.  2009; 56(1):  182-185. </p> 

    <p class="title">17.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264165300028">Synthesis of Spiro-Heterocycles with the Thiazolidinone Moiety from Nitrilimines</a>.  Dalloul HM.  JOURNAL OF THE CHINESE CHEMICAL SOCIETY.  2009; 56(1): 196-201. </p> 

    <p class="title">18.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264165300030">QSAR Study of Anti-HIV Activities Against HIV-1 and Some of Their Mutant Strains for a Group of HEPT Derivatives</a>.  Zarei K, Atabati M.  JOURNAL OF THE CHINESE CHEMICAL SOCIETY.  2009; 56(1): 206-213. </p> 

    <p class="title">19.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264226500006">Analysis of Schisandra chinensis and Schisandra sphenanthera</a>.  Lu Y, Chen DF.  JOURNAL OF CHROMATOGRAPHY A.  2009; 1216(11): 1980-1990. </p> 

    <p class="title">20.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264049800028">Electrostatically constrained alpha-helical peptide inhibits replication of HIV-1 resistant to enfuvirtide</a>.  Nishikawa H, Nakamura S, Kodama E, Ito S, Kajiwara K, Izumi K, Sakagami Y, Oishi S, Ohkubo T, Kobayashi Y, Otaka A, Fujii N, Matsuoka M.  INTERNATIONAL JOURNAL OF BIOCHEMISTRY &amp; CELL BIOLOGY.  2009; 41(4): 891-899. </p> 

    <p class="title">21.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264201400016">Total synthesis of polyyne natural products</a>.  Gung BW.  COMPTES RENDUS CHIMIE.  2009; 12(3-4): 49-505. </p> 

    <p class="title">22.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264023500017">Stereoselective Reformatskii-Claisen rearrangement: synthesis of 2 &#39;,3 &#39;-dideoxy-6 &#39;,6 &#39;-difluoro-2 &#39;-thionucleosides</a>.  Zheng F, Zhang XG, Qing FL.  CHEMICAL COMMUNICATIONS.  2009; 12: 1505-7. </p> 

    <p class="title">23.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264169700014">Poly(styrene-alt-maleic anhydride) derivatives as potent anti-HIV microbicide candidates</a>.  Fang WJ, Cai YJ, Chen XP, Su RM, Chen T, Xia NS, Li L, Yang QL, Han JH, Han SF.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009; 19(7): 1903-1907. </p> 

    <p class="title">24.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264169700020">3-Hydroxy-4-oxo-4H-pyrido[1,2-a]pyrimidine-2-carboxylates-A new class of HIV-1 integrase inhibitors</a>.  Donghi M, Kinzel OD, Summa V.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009;  19(7): 1930-4. </p> 

    <p class="title">25.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264169700029">Inhibitors of HIV-1 attachment. Part 2: An initial survey of indole substitution patterns</a>.  Meanwell NA, Wallace OB, Fang HQ, Wang H, Deshpande M, Wang T, Yin ZW, Zhang ZX, Pearce BC, James J, Yeung KS, Qiu ZL, Wright JJK, Yang Z, Zadjura L, Tweedie DL, Yeola S, Zhao F, Ranadive S, Robinson BA, Gong YF, Wang HGH, Blair WS, Shi PY, Colonno RJ, Lin PF.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009; 19(7): 1977-1981. </p> 

    <p class="title">26.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264236700033">Receptor- and ligand-based 3D-QSAR study for a series of non-nucleoside HIV-1 reverse transcriptase inhibitors</a>. Hua RJ, Barbault F, Delamar M, Zhang RS.  BIOORGANIC &amp; MEDICINAL CHEMISTRY.  2009; 17(6): 2400-9.  </p>

    <p class="title">27.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000264160500004">SJ23B, a jatrophane diterpene activates classical PKCs and displays strong activity against HIV in vitro</a>.  Bedoya LM, Marquez N, Martinez N, Gutierrez-Eisman S, Alvarez A, Calzado MA, Rojas JM, Appendino G, Munoz E, Alcami J.  BIOCHEMICAL PHARMACOLOGY.  2009; 77(6): 965-978. </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
