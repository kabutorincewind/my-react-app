

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-11-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+i+3G6TFN/5lEoodPeTwGPgqRZJ30v9Zu21x+DK3QFizznzIbdLErQhnNGKuPOcWQOT42AeQ2zdtOkgVvaoZgpw+xrwLODiZLMzTkzVP2F3GQZxoF842SYJ3JA8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="16E9498B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 12 - November 23, 2010</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20958050">Structure-based design, synthesis, and structure-activity relationship studies of HIV-1 protease inhibitors incorporating phenyloxazolidinones.</a> Ali, A., G.S. Reddy, M.N. Nalam, S.G. Anjum, H. Cao, C.A. Schiffer, and T.M. Rana.  Journal of Medicinal Chemistry, 2010. 53(21): p. 7699-7708; PMID[20958050].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>

    <p class="NoSpacing">2.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/21081094">Discovery of Entry Inhibitors for HIV-1 via a New De Novo Protein Design Framework.</a> Bellows, M.L., M.S. Taylor, P.A. Cole, L. Shen, R.F. Siliciano, H.K. Fung, and C.A. Floudas. Biophysical Journal, 2010. 99(10): p. 3445-3453; PMID[21081094].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>    

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20827161">In-vitro phenotypic susceptibility of HIV-2 clinical isolates to the integrase inhibitor S/GSK1349572.</a> Charpentier, C., L. Larrouy, G. Collin, F. Damond, S. Matheron, G. Chene, T. Nie, R. Schinazi, F. Brun-Vezinet, and D. Descamps. Aids, 2010. 24(17): p. 2753-2755; PMID[20827161].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>    

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20880706">Xiamycin, a pentacyclic indolosesquiterpene with selective anti-HIV activity from a bacterial mangrove endophyte.</a> Ding, L., J. Munch, H. Goerls, A. Maier, H.H. Fiebig, W.H. Lin, and C. Hertweck. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6685-6687; PMID[20880706].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>    

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21084190">Triterpene derivatives that inhibit human immunodeficiency virus type 1 replication.</a> Dorr, C.R., S. Yemets, O. Kolomitsyna, P. Krasutsky, and L.M. Mansky. Bioorganic &amp; Medicinal Chemistry Letters, 2010. <b>[Epub ahead of print]</b>; PMID[21084190].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>    

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20864343">Development of the next generation of HIV-1 integrase inhibitors: pyrazolone as a novel inhibitor scaffold.</a> Hadi, V., Y.H. Koh, T.W. Sanchez, D. Barrios, N. Neamati, and K.W. Jung. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6854-6857; PMID[20864343].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21078951">Antiviral Activity, Pharmacokinetics, and Safety of BMS-488043, a Novel Oral Small-Molecule HIV-1 Attachment Inhibitor, in HIV-1-Infected Subjects.</a> Hanna, G.J., J. Lalezari, J.A. Hellinger, D.A. Wohl, R. Nettles, A. Persson, M. Krystal, P. Lin, R. Colonno, and D.M. Grasela. Antimicrobial Agents &amp; Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21078951].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20932761">Solid-phase synthesis and screening of N-acylated polyamine (NAPA) combinatorial libraries for protein binding.</a> Iera, J.A., L.M. Jenkins, H. Kajiyama, J.B. Kopp, and D.H. Appella. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6500-6503; PMID[20932761]<b>.</b></p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21084994">Attachment and fusion inhibitors potently prevent dendritic cell-driven HIV infection.</a> Ines, F. and R. Melissa. Journal of Acquired Immune Deficiency Syndromes, 2010. <b>[Epub ahead of print]</b>; PMID[21084994].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20888224">Discovery and SAR of a series of 4,6-diamino-1,3,5-triazin-2-ol as novel non-nucleoside reverse transcriptase inhibitors of HIV-1.</a> Liu, B., Y. Lee, J. Zou, H.M. Petrassi, R.W. Joseph, W. Chao, E.L. Michelotti, M. Bukhtiyarova, E.B. Springman, and B.D. Dorsey. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6592-6596; PMID[20888224].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20833717">Amyloid-binding Small Molecules Efficiently Block SEVI (Semen-derived Enhancer of Virus Infection)- and Semen-mediated Enhancement of HIV-1 Infection.</a> Olsen, J.S., C. Brown, C.C. Capule, M. Rubinshtein, T.M. Doran, R.K. Srivastava, C. Feng, B.L. Nilsson, J. Yang, and S. Dewhurst. Journal of Biological Chemistry, 2010. 285(46): p. 35488-35496; PMID[20833717].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21077659">Chemoenzymatic Syntheses and Anti-HIV-1 Activity of Glucose-Nucleoside Conjugates as Prodrugs.</a> Rodriguez-Perez, T., S. Fernandez, Y.S. Sanghvi, M. Detorio, R.F. Schinazi, V. Gotor, and M. Ferrero. Bioconjugate Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21077659].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20869872">Potent and selective HIV-1 ribonuclease H inhibitors based on a 1-hydroxy-1,8-naphthyridin-2(1H)-one scaffold.</a> Williams, P.D., D.D. Staas, S. Venkatraman, H.M. Loughran, R.D. Ruzek, T.M. Booth, T.A. Lyle, J.S. Wai, J.P. Vacca, B.P. Feuston, L.T. Ecto, J.A. Flynn, D.J. DiStefano, D.J. Hazuda, C.M. Bahnck, A.L. Himmelberger, G. Dornadula, R.C. Hrin, K.A. Stillmock, M.V. Witmer, M.D. Miller, and J.A. Grobler. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(22): p. 6754-6757; PMID[20869872].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20973495">Synthesis of N-Terminally Linked Protein and Peptide Dimers by Native Chemical Ligation.</a> Xiao, J., B.S. Hamilton, and T.J. Tolbert. Bioconjugate Chemistry, 2010. 21(11): p. 1943-1947; PMID[20973495].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
    <p> </p>
    
    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2095805021078948">In Vivo Resistance Patterns to the HIV Attachment Inhibitor BMS-488043.</a> Zhou, N., B. Nowicka-Sans, S. Zhang, L. Fan, J. Fang, H. Fang, Y.F. Gong, B. Eggers, D.R. Langley, T. Wang, J. Kadow, D. Grasela, G.J. Hanna, L. Alexander, R. Colonno, M. Krystal, and P.F. Lin. Antimicrobial Agents &amp; Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21078948].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1112-112410.</p> 
        
    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">16. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">QSAR Studies on the Calanolide Analogues as Anti-HIV-1 Agents.</a> Qiu, K.X., H.D. Xie, Y.P. Guo, Y. Huang, B. Liu, and W. Li. Chinese Journal of Structural Chemistry, 2010. 29(10): p. 1477-1482; ISI[000283481700004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">17. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Advances in Aptamers.</a> Syed, M.A. and S. Pervaiz. Oligonucleotides, 2010. 20(5): p. 215-224; ISI[000283489700001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Synthesis and Antiviral Activity of New Indole-Based Heterocycles.</a> Abdel-Gawad, H., H.A. Mohamed, K.M. Dawood, and F.A.R. Badria. Chemical &amp; Pharmaceutical Bulletin, 2010. 58(11): p. 1529-1531; ISI[000283522300019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Human Immunodeficiency Virus Type 1 Protease Inhibitor Drug-Resistant Mutants Give Discordant Results When Compared in Single-Cycle and Multiple-Cycle Fitness Assays.</a> Dykes, C., H.L. Wu, M. Sims, J. Holden-Wiltse, and L.M. Demeter. Journal of Clinical Microbiology, 2010. 48(11): p. 4035-4043; ISI[000283588500030].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Naturally occurring circular proteins: distribution, biosynthesis and evolution.</a> Cascales, L. and D.J. Craik. Organic &amp; Biomolecular Chemistry, 2010. 8(22): p. 5035-5047; ISI[000283602000001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Deriving four functional anti-HIV siRNAs from a single Pol III-generated transcript comprising two adjacent long hairpin RNA precursors.</a> Saayman, S., P. Arbuthnot, and M.S. Weinberg. Nucleic Acids Research, 2010. 38(19): p. 6652-6663; ISI[000283682100036].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">22.  <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">Synthesis, antiviral and contraceptive activities of nucleoside-sodium cellulose sulfate acetate and succinate conjugates.</a> Agarwal, H.K., A. Kumar, G.F. Doncel, and K. Parang. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(23): p. 6993-6997; ISI[000283801400025].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>

    <p> </p>

    <p class="NoSpacing">23.  <a href="../../../../../../../../../Users/Chris/Desktop/%3chttp:/gateway.isiknowledge.com/gateway/Gateway.cgi">QSAR Studies of 3,3 &#39;-(Substituted-benzylidene)-bis-4-hydroxycoumarin, Potential HIV-1 Integrase Inhibitor.</a> Li, B.J., C.C. Chiang, and L.Y. Hsu. Journal of the Chinese Chemical Society, 2010. 57(4A): p. 742-749; ISI[000283803400021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1112-112410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
