

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-01-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uc/vEjWA8p6SBdFGLcwiDOTPco1NoRrfaVNezinED7r1Vf29uvJtXRwQTjlen+JYyN9zj2v/DcKuq/onDlyZcCPsKVAP0SrCL8kTC816jZ8mnOcWe/adAKO473I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F9815224" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">January 7 - January 20, 2011</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21221936">Isolation and characterization of antifungal peptides produced by Bacillus amyloliquefaciens LBM5006.</a> Benitez, L.B., R.V. Velho, M.P. Lisboa, L.F. da Costa Medina, and A. Brandelli. Journal of Microbiology, 2010. 48(6): p. 791-7; PMID[21221936].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21241212">Novel small molecules for the treatment of infections caused by Candida albicans: a patent review (2002 - 2010).</a> Calugi, C., A. Trabocchi, and A. Guarna. Expert Opinion on Therapeutic Patents, 2011. <b>[Epub ahead of print]</b>; PMID[21241212].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20946868">Purification, characterization and inhibition of sterol C24-methyltransferase from Candida albicans.</a> Ganapathy, K., R. Kanagasabai, T.T. Nguyen, and W.D. Nes. Archives of Biochemistry and Biophysics, 2011. 505(2): p. 194-201; PMID[20946868].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21217705">Antimicrobial activity of Citrox((R)) bioflavonoid preparations against oral microorganisms.</a> Hooper, S.J., M.A. Lewis, M.J. Wilson, and D.W. Williams. British Dental Journal, 2011. 210(1): p. 1-5; PMID[21217705].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21241455">In-vitro antifungal and antibacterial activities of pentacycloundecane tetra-amines.</a> Onajole, O.K., Y. Coovadia, T. Govender, H.G. Kruger, G.E. Maguire, D. Naidu, N. Singh, and P. Govender. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21241455].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21168331">The effect of 5-substitution on the electrochemical behavior and antitubercular activity of PA-824.</a> Bollo, S., L.J. Nunez-Vergara, S. Kang, L. Zhang, H.I. Boshoff, C.E. Barry, 3rd, J.A. Squella, and C.S. Dowd. Bioorganic and Medicinal Chemistry Letters, 2011. 21(2): p. 812-7; PMID[21168331].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21059649">Cisplatin inhibits protein splicing, suggesting inteins as therapeutic targets in mycobacteria.</a> Zhang, L., Y. Zheng, B. Callahan, M. Belfort, and Y. Liu. The Journal of Biological Chemistry, 2011. 286(2): p. 1277-82; PMID[21059649].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0106-011911.</p>  

    <p class="memofmt2-3">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285076700013">2- 4-(4-Methoxyphenylcarbonyloxy)benzylidene -6-dimethylaminomethyl cyclohexanone hydrochloride: A Mannich base which inhibits the growth of some drug-resistant strains of Mycobacterium tuberculosis.</a> Das, S., U. Das, B. Bandy, D.K.J. Gorecki, and J.R. Dimmock. Pharmazie, 2010. 65(11): p. 849-850; ISI[000285076700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285238300008">Evaluation of Substituted Benzaldehydes Against Mycobacterium tuberculosis.</a> Ferreira, M.D., A.L.P. Candea, M. Henriques, M.C.S. Lourenco, C.R. Kaiser, and M.V.N. de Souza. Letters in Drug Design &amp; Discovery, 2010. 7(10): p. 754-758; ISI[000285238300008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285021300006">Secondary Metabolites from the Sponges Aplysina fistularis and Dysidea sp and the Antituberculosis Activity of 11-Ketofistularin-3.</a> Gandolfi, R.C., M.B. Medina, R.G.S. Berlinck, S.P. Lira, F.C.D. Galetti, C.L. Silva, K. Veloso, A.G. Ferreira, E. Hajdu, and S. Peixinho. Quimica Nova, 2010. 33(9): p. 1853-1858; ISI[000285021300006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011911.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285176900014">Synthesis and Antifungal Activities in vitro of Novel Pyrazino 2,1-a Isoquinolin Derivatives.</a> Tang, H., C.H. Zheng, J.G. Lu, B.Y. Fu, Y.J. Zhou, and J. Zhu. Acta Chimica Sinica, 2010. 68(22): p. 2338-2346; ISI[000285176900014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0106-011911.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
