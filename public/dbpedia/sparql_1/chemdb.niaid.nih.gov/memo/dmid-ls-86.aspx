

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-86.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7xw2WlH0CxdQ/ziZs0tx7wCWi4TWUH86cBtox2DVe2s97BdZpc0Mq+ELObCJtSNgLwKwMsOIYuUiU6YxDBUtmZCOfNaTahg9grYsvbik7ZJs+Rh9+L4SKRi5nzg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="58A92356" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-86-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56149   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential importance of error catastrophe to the development of antiviral strategies for hantaviruses</p>

    <p>          Jonsson, Colleen B, Milligan, Brook G, and Arterburn, Jeffrey B</p>

    <p>          Virus Research  <b>2005</b>.  107(2): 195-205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4F1SVRB-1/2/7765e43436f07ce2af0d3598aa740936">http://www.sciencedirect.com/science/article/B6T32-4F1SVRB-1/2/7765e43436f07ce2af0d3598aa740936</a> </p><br />

    <p>2.     56150   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Establishment of a Subgenomic Replicon for Bovine Viral Diarrhea Virus in Huh-7 Cells and Modulation of Interferon-Regulated Factor 3-Mediated Antiviral Response</p>

    <p>          Horscroft, N, Bellows, D, Ansari, I, Lai, VC, Dempsey, S, Liang, D, Donis, R, Zhong, W, and Hong, Z</p>

    <p>          J Virol <b>2005</b>.  79(5): 2788-2796</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708997&amp;dopt=abstract</a> </p><br />

    <p>3.     56151   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Generation of eGFP expressing recombinant Zaire ebolavirus for analysis of early pathogenesis events and high-throughput antiviral drug screening</p>

    <p>          Towner, Jonathan S, Paragas, Jason, Dover, Jason E, Gupta, Manisha, Goldsmith, Cynthia S, Huggins, John W, and Nichol, Stuart T</p>

    <p>          Virology <b>2005</b>.  332(1): 20-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4F08547-1/2/ccd01d280f32802721936fbc3c7bc5e9">http://www.sciencedirect.com/science/article/B6WXR-4F08547-1/2/ccd01d280f32802721936fbc3c7bc5e9</a> </p><br />

    <p>4.     56152   DMID-LS-86; PUBMED-DMID-2/15/2005; LR-13607-LWC ; DMID-LSLOAD</p>

    <p><b>          YM-53403, a unique anti-respiratory syncytial virus agent with a novel mechanism of action</b> </p>

    <p>          Sudo, K, Miyazaki, Y, Kojima, N, Kobayashi, M, Suzuki, H, Shintani, M, and Shimizu, Y</p>

    <p>          Antiviral Res <b>2005</b>.  65(2): 125-131</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15708639&amp;dopt=abstract</a> </p><br />

    <p>5.     56153   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and synthesis of a potent macrocyclic 1,6-napthyridine anti-human cytomegalovirus (HCMV) inhibitors</p>

    <p>          Falardeau, Guy, Lachance, Hugo, St-Pierre, Annie, Yannopoulos, Constantin G, Drouin, Marc, Bedard, Jean, and Chan, Laval</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FFGJ50-7/2/9a87fc85f79b22dc244fb6e2c58702be">http://www.sciencedirect.com/science/article/B6TF9-4FFGJ50-7/2/9a87fc85f79b22dc244fb6e2c58702be</a> </p><br />
    <br clear="all">

    <p>6.     56154   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Fluorosubstitution and 7-alkylation as prospective modifications of biologically active 6-aryl derivatives of tricyclic acyclovir and ganciclovir analogues</p>

    <p>          Ostrowski, Tomasz, Golankiewicz, Bozenna, Clercq, Erik De, and Balzarini, Jan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FD0MNM-2/2/4f3ab1cb24e1e8925e60f6857042672d">http://www.sciencedirect.com/science/article/B6TF8-4FD0MNM-2/2/4f3ab1cb24e1e8925e60f6857042672d</a> </p><br />

    <p>7.     56155   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Adaptation of West Nile Virus Replicons to Cells in Culture and Use of Replicon-Bearing Cells to Probe Antiviral Action</p>

    <p>          Rossi, S. <i>et al.</i></p>

    <p>          Virology <b>2005</b>.  331(2): 457-470</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329300023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226329300023</a> </p><br />

    <p>8.     56156   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p><b>          The value and limitations of long-term nucleoside antiviral therapy in chronic hepatitis B</b> </p>

    <p>          Dienstag, Jules L</p>

    <p>          Journal of Hepatology <b>2005</b>.  42(2): 158-162</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4F00RCT-1/2/461eaefc7101f7a8b61f91c511255072">http://www.sciencedirect.com/science/article/B6W7C-4F00RCT-1/2/461eaefc7101f7a8b61f91c511255072</a> </p><br />

    <p>9.     56157   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ether Lipid-Ester Prodrugs of Acyclic Nucleoside Phosphonates: Activity Against Adenovirus Replication in Vitro</p>

    <p>          Hartline, C. <i>et al.</i></p>

    <p>          Journal of Infectious Diseases <b>2005</b>.  191(3): 396-399</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226130500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226130500011</a> </p><br />

    <p>10.   56158   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of HHV-8/KSHV infected primary effusion lymphomas in NOD/SCID mice by azidothymidine and interferon-[alpha]</p>

    <p>          Wu, William, Rochford, Rosemary, Toomey, Lan, Harrington, Jr William, and Feuer, Gerold</p>

    <p>          Leukemia Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T98-4F923YC-4/2/b20d5c9d897cb1bc312758b56bbcb93a">http://www.sciencedirect.com/science/article/B6T98-4F923YC-4/2/b20d5c9d897cb1bc312758b56bbcb93a</a> </p><br />

    <p>11.   56159   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Past, Present and Future of Antiviral Drug Discovery</p>

    <p>          Littler, E.</p>

    <p>          Idrugs <b>2004</b>.  7(12): 1104-1112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226344000015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226344000015</a> </p><br />
    <br clear="all">

    <p>12.   56160   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          SARS-CoV protease inhibitors design using virtual screening method from natural products libraries</p>

    <p>          Liu, B and Zhou, J</p>

    <p>          J Comput Chem <b>2005</b>.  26(5): 484-490</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15693056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15693056&amp;dopt=abstract</a> </p><br />

    <p>13.   56161   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular modeling and chemical modification for finding peptide inhibitor against severe acute respiratory syndrome coronavirus main proteinase</p>

    <p>          Du, Q, Wang, S, Wei, D, Sirois, S, and Chou, KC</p>

    <p>          Anal Biochem <b>2005</b>.  337(2): 262-270</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15691506&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15691506&amp;dopt=abstract</a> </p><br />

    <p>14.   56162   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV replication by siRNA</p>

    <p>          Wu, Chang-Jer, Huang, Hui-Wen, Liu, Chiu-Yi, Hong, Cheng-Fong, and Chan, Yi-Lin</p>

    <p>          Antiviral Research <b>2005</b>.  65(1): 45-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DVBBK3-1/2/fa83a0ab4508c30101818c51888f9e9c">http://www.sciencedirect.com/science/article/B6T2H-4DVBBK3-1/2/fa83a0ab4508c30101818c51888f9e9c</a> </p><br />

    <p>15.   56163   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral chemotherapy facilitates control of poxvirus infections through inhibition of cellular signal transduction</p>

    <p>          Yang, H, Kim, SK, Kim, M, Reche, PA, Morehead, TJ, Damon, IK, Welsh, RM, and Reinherz, EL</p>

    <p>          J Clin Invest <b>2005</b>.  115(2): 379-387</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15690085&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15690085&amp;dopt=abstract</a> </p><br />

    <p>16.   56164   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Host-based antipoxvirus therapeutic strategies: turning the tables</p>

    <p>          Fauci, AS and  Challberg, MD</p>

    <p>          J Clin Invest <b>2005</b>.  115(2): 231-233</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15690079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15690079&amp;dopt=abstract</a> </p><br />

    <p>17.   56165   LR-13637; DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and inhibitory activity of benzoic acid and pyridine derivatives on influenza neuraminidase</p>

    <p>          Chand, Pooran, Kotian, Pravin L, Morris, Philip E, Bantia, Shanta, Walsh, David A, and Babu, Yarlagadda S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FF8WFM-3/2/45f8bd0e6418e4c355b9201e3ddfef46">http://www.sciencedirect.com/science/article/B6TF8-4FF8WFM-3/2/45f8bd0e6418e4c355b9201e3ddfef46</a> </p><br />

    <p>18.   56166   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antioxidant and Antiviral Activities of Plastoquinones from the Brown Alga Sargassum micracanthum, and a New Chromene Derivative Converted from the Plastoquinones</p>

    <p>          Iwashima, M, Mori, J, Ting, X, Matsunaga, T, Hayashi, K, Shinoda, D, Saito, H, Sankawa, U, and Hayashi, T</p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(2): 374-377</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15684504&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15684504&amp;dopt=abstract</a> </p><br />

    <p>19.   56167   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          VSV replication in neurons is inhibited by type I IFN at multiple stages of infection</p>

    <p>          Trottier, Jr Mark D, Palian, Beth M, and Shoshkes Reiss, Carol</p>

    <p>          Virology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4FCRFG5-3/2/ebd70b19571bf41b6928cecd42f9f90b">http://www.sciencedirect.com/science/article/B6WXR-4FCRFG5-3/2/ebd70b19571bf41b6928cecd42f9f90b</a> </p><br />

    <p>20.   56168   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Facile, Comprehensive, High-Throughput Genotyping of Human Genital Papillomaviruses Using Spectrally Addressable Liquid Bead Microarrays</p>

    <p>          Wallace, J, Woda, BA, and Pihan, G</p>

    <p>          J Mol Diagn <b>2005</b>.  7(1): 72-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681477&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15681477&amp;dopt=abstract</a> </p><br />

    <p>21.   56169   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virucidal activities of medium- and long-chain fatty alcohols, fatty acids and monoglycerides against herpes simplex virus types 1 and 2: comparison at different pH levels</p>

    <p>          Hilmarsson, H, Kristmundsdottir, T, and Thormar, H</p>

    <p>          APMIS <b>2005</b>.  113(1): 58-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15676016&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15676016&amp;dopt=abstract</a> </p><br />

    <p>22.   56170   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Genotypic characterization of the DNA polymerase and sensitivity to antiviral compounds of foscarnet-resistant herpes simplex virus type 1 (HSV-1) derived from a foscarnet-sensitive HSV-1 strain</p>

    <p>          Saijo, M, Suzutani, T, Morikawa, S, and Kurane, I</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 606-611</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673740&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673740&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   56171   DMID-LS-86; EMBASE-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of secretory phospholipase A2. 2-Synthesis and structure-activity relationship studies of 4,5-dihydro-3-(4-tetradecyloxybenzyl)-1,2,4-4H-oxadiazol-5-one (PMS1062) derivatives specific for group II enzyme</p>

    <p>          Dong, Chang-Zhi, Ahamada-Himidi, Azali, Plocki, Stephanie, Aoun, Darina, Touaibia, Mohamed, Meddad-Bel Habich, Nadia, Huet, Jack, Redeuilh, Catherine, Ombetta, Jean-Edouard, and Godfroid, Jean-Jacques</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FFX935-1/2/e4cc2dca418aa17218ff37c8f1dd74c9">http://www.sciencedirect.com/science/article/B6TF8-4FFX935-1/2/e4cc2dca418aa17218ff37c8f1dd74c9</a> </p><br />

    <p>24.   56172   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacokinetics of the antiviral agent beta-L-3&#39;-fluoro-2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxycytidine in rhesus monkeys</p>

    <p>          Asif, G, Hurwitz, SJ, Gumina, G, Chu, CK, McClure, HM, and Schinazi, RF</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(2): 560-564</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673733&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15673733&amp;dopt=abstract</a> </p><br />

    <p>25.   56173   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Induction of Inducible Nitric Oxide (No) Synthase Mrna and No Production in Macrophages Infected With Influenza a/Pr/8 Virus and Stimulated With Its Ether-Split Product</p>

    <p>          Imanishi, N. <i>et al.</i></p>

    <p>          Microbiology and Immunology <b>2005</b>.  49(1): 41-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226371700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226371700006</a> </p><br />

    <p>26.   56174   DMID-LS-86; PUBMED-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The threat of an avian influenza pandemic</p>

    <p>          Monto, AS</p>

    <p>          N Engl J Med <b>2005</b>.  352(4): 323-325</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15668220&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15668220&amp;dopt=abstract</a> </p><br />

    <p>27.   56175   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influenza Virus Activates Human Immunodeficiency Virus Type-1 Gene Expression in Human Cd4-Expressing T Cells Through an Nf-Kappa B-Dependent Pathway</p>

    <p>          Sun, J. <i>et al.</i></p>

    <p>          Clinical Immunology <b>2005</b>.  114(2): 190-198</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226440700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226440700012</a> </p><br />

    <p>28.   56176   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Enzymatic Activity Characterization of Sars Coronavirus 3c-Like Protease by Fluorescence Resonance Energy Transfer Technique</p>

    <p>          Chen, S. <i>et al.</i></p>

    <p>          Acta Pharmacologica Sinica <b>2005</b>.  26(1): 99-106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226426600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226426600015</a> </p><br />

    <p>29.   56177   DMID-LS-86; WOS-DMID-2/15/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure of a Proteolytically Resistant Core From the Severe Acute Respiratory Syndrome Coronavirus S2 Fusion Protein</p>

    <p>          Supekar, V. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2004</b>.  101(52): 17958-17963</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226102700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000226102700016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
