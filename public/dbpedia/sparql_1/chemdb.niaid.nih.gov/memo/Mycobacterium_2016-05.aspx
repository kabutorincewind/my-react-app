

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Hc+D/Z+0WmfBk6uQtwdMNlZES8ZAfiSEdOTbgsna+cvNAY7BEv4P6fZ9K7M5U9+d1CDCvjcLLLH98xYDZ4mw2WDkMRUj/vjSATdjVgxuU4i1mq9Us+VHY9kuc80=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AACA576B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: May, 2016</h1>
    
    <h2>Literature Citations</h2>

    <p class="plaintext">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27144547">Design, Synthesis, Antimicrobial Evaluation and Molecular Modeling Study of 1,2,4-Triazole-based 4-thiazolidinones.</a> Ahmed, S., M.F. Zayed, S.M. El-Messery, M.H. Al-Agamy, and H.M. Abdel-Rahman. Molecules, 2016. 21(5): E568. PMID[27144547].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">2.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000374385600011">Synthesis, Cytotoxic and Antitubercular Activities of Copper(II) Complexes with Heterocyclic Bases and 3-Hydroxypicolinic acid.</a> Almeida, J.D., I.M. Marzano, M. Pivatto, N.P. Lopes, A.M.D. Ferreira, F.R. Pavan, I.C. Silva, E.C. Pereira-Maia, G. Von Poelhsitz, and W. Guerra. Inorganica Chimica Acta, 2016. 446: p. 87-92. ISI[000374385600011].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">3.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26785029">Combined Genetic and Bioactivity-based Prioritization Leads to the Isolation of an Endophyte-derived Antimycobacterial Compound.</a> Alvin, A., J.A. Kalaitzis, B. Sasia, and B.A. Neilan. Journal of Applied Microbiology, 2016. 120(5): p. 1229-1239. PMID[26785029].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27017114">Synthesis and Docking Studies of Pyrazine-thiazolidinone Hybrid Scaffold Targeting Dormant Tuberculosis.</a> Chitre, T.S., K.D. Asgaonkar, P.B. Miniyar, A.B. Dharme, M.A. Arkile, A. Yeware, D. Sarkar, V.M. Khedkar, and P.C. Jha. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(9): p. 2224-2228. PMID[27017114].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">5.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/26994489">In Vitro Evaluation of the Cyto-genotoxic Potential of Ruthenium(II) Scar Complexes: A Promising Class of Antituberculosis Agents.</a> De Grandis, R.A., F.A. Resende, M.M. da Silva, F.R. Pavan, A.A. Batista, and E.A. Varanda. Mutation Research-Genetic Toxicology and Environmental Mutagenesis, 2016. 798: p. 11-18. PMID[26994489].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">6.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27044545.">Oxadiazoles Have Butyrate-specific Conditional Activity against Mycobacterium tuberculosis.</a> Early, J.V., A. Casey, M.A. Martinez-Grau, I.C. Gonzalez Valcarcel, M. Vieth, J. Ollinger, M.A. Bailey, T. Alling, M. Files, Y. Ovechkina, and T. Parish. Antimicrobial Agents and Chemotherapy, 2016. 60(6): p. 3608-3616. PMID[27044545].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">7.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27018907">Structure-Activity Relationships of a Diverse Class of Halogenated Phenazines that Targets Persistent, Antibiotic-tolerant Bacterial Biofilms and Mycobacterium tuberculosis.</a></p>

    <p class="plaintext">Garrison, A.T., Y. Abouelhassan, V.M. Norwood, D. Kallifidas, F. Bai, T. Nguyen, M. Rolfe, G.M. Burch, S. Jin, H. Luesch, and R.W. Huigens. Journal of Medicinal Chemistry, 2016. 59(8): p. 3808-3825. PMID[27018907].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">8.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/27148868">G1-4A, a Polysaccharide from Tinospora cordifolia Inhibits the Survival of Mycobacterium tuberculosis by Modulating Host Immune Responses in TLR4 Dependent Manner.</a> Gupta, P.K., P. Chakraborty, S. Kumar, P.K. Singh, M.G. Rajan, K.B. Sainis, and S. Kulkarni. Plos One, 2016. 11(5): p. e0154725. PMID[27148868].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26971857">Ebselen and Analogs as Inhibitors of Bacillus anthracis Thioredoxin Reductase and Bactericidal Antibacterials Targeting Bacillus Species, Staphylococcus aureus and Mycobacterium tuberculosis.</a> Gustafsson, T.N., H. Osman, J. Werngren, S. Hoffner, L. Engman, and A. Holmgren. Biochimica Et Biophysica Acta-General Subjects, 2016. 1860(6): p. 1265-1271. PMID[26971857].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/27156623">Activity of Nitazoxanide and Tizoxanide against Mycobacterium tuberculosis in Vitro and in Whole Blood Culture.</a> Harausz, E.P., K.A. Chervenak, C.E. Good, M.R. Jacobs, R.S. Wallis, M. Sanchez-Felix, and W.H. Boom. Tuberculosis, 2016. 98: p. 92-96. PMID[27156623]. PMCID[PMC4864490].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27020299">Synthesis, Antimycobacterial and Antibacterial Activity of 1-(6-Amino-3,5-difluoropyridin-2-yl) fluoroquinolone Derivatives Containing an Oxime Functional Moiety.</a> Huang, J., M.H. Wang, B. Wang, Z.Y. Wu, M.L. Liu, L.S. Feng, J. Zhang, X.N. Li, Y. Yang, and Y. Lu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(9): p. 2262-2267. PMID[27020299].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26937999">Routes of Synthesis of Carbapenems for Optimizing both the Inactivation of l,d-Transpeptidase LdtMt1 of Mycobacterium tuberculosis and the Stability toward Hydrolysis by beta-Lactamase BlaC.</a> Iannazzo, L., D. Soroka, S. Triboulet, M. Fonvielle, F. Compain, V. Dubee, J.L. Mainardi, J.E. Hugonnet, E. Braud, M. Arthur, and M. Etheve-Quelquejeu. Journal of Medicinal Chemistry, 2016. 59(7): p. 3427-3438. PMID[26937999].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27171280">Synthesis and Evaluation of the 2-Aminothiazoles as Anti-tubercular Agents.</a> Kesicki, E.A., M.A. Bailey, Y. Ovechkina, J.V. Early, T. Alling, J. Bowman, E.S. Zuniga, S. Dalai, N. Kumar, T. Masquelin, P.A. Hipskind, J.O. Odingo, and T. Parish. Plos One, 2016. 11(5): p. e0155209. PMID[27171280].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26922579">Development and Validation of a Highly Sensitive LC-MS/MS-ESI Method for Quantification of IIIM-019-A Novel Nitroimidazole Derivative with Promising Action against Tuberculosis: Application to Drug Development.</a> Kour, G., B.K. Chandan, M. Khullar, G. Munagala, P.P. Singh, A. Bhagat, A.P. Gupta, R.A. Vishwakarma, and Z. Ahmed. Journal Pharmaceutical Biomedical Analysis, 2016. 124: p. 26-33. PMID[26922579].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27135906">Design, Synthesis, and Characterization of Some Hybridized Pyrazolone Pharmacophore Analogs against Mycobacterium tuberculosis.</a> Krishnasamy, S.K., V. Namasivayam, S. Mathew, R.S. Eakambaram, I.A. Ibrahim, A. Natarajan, and S. Palaniappan. Archiv der Pharmazie, 2016. 349(5): p. 383-397. PMID[27135906].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26905094">Constituents of Fagraea fragrans with Antimycobacterial Activity in Combination with Erythromycin.</a> Madmanang, S., N. Cheyeng, S. Heembenmad, W. Mahabusarakam, J. Saising, M. Seeger, S. Chusri, and S. Chakthong. Journal of Natural Products, 2016. 79(4): p. 767-774. PMID[26905094].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27149193">Benzothiazole Derivative as a Novel Mycobacterium tuberculosis Shikimate Kinase Inhibitor: Identification and Elucidation of Its Allosteric Mode of Inhibition.</a> Mehra, R., V.S. Rajput, M. Gupta, R. Chib, A. Kumar, P. Wazir, I.A. Khan, and A. Nargotra. Journal of Chemical Information and Modeling, 2016. 56(5): p. 930-940. PMID[27149193].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000374607900008">Synthesis and Antitubercular and Antibacterial Activity of Some Active Fluorine Containing Quinoline-pyrazole Hybrid Derivatives.</a> Nayak, N., J. Ramprasad, and U. Dalimba. Journal of Fluorine Chemistry, 2016. 183: p. 59-68. ISI[000374607900008].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000374799400014">Synthesis and Antimycobacterial Screening of New N-(4-(5-Aryl-3-(5-methyl-1,3,4-oxadiazol-2-yl)-1H-pyrazol-1-yl)phenyl)-4 -amide Derivatives.</a> Nayak, N., J. Ramprasad, U. Dalimba, P. Yogeeswari, and D. Sriram. Chinese Chemical Letters, 2016. 27(3): p. 365-369. ISI[000374799400014].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26778457">Anti-mycobacterium tuberculosis Activity of Calophyllum brasiliense Extracts Obtained by Supercritical Fluid Extraction and Conventional Techniques.</a> Pires, C.T.A., R.B.D. Scodro, M.A. Brenzan, D.A.G. Cortez, V.L.D. Siqueira, L. Cardozo, R.M. Goncalves, K.R. Caleffi-Ferracioli, and R.F. Cardoso. Current Pharmaceutical Biotechnology, 2016. 17(6): p. 532-539. PMID[26778457].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27020522">Synthesis and Antitubercular Activity of 1,2,4-Trisubstitued Piperazines.</a> Rohde, K.H., H.A. Michaels, and A. Nefzi. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(9): p. 2206-2209. PMID[27020522].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27025343">Design and Synthesis of Novel Quinoxaline Derivatives as Potential Candidates for Treatment of Multidrug-resistant and Latent Tuberculosis.</a> Santivanez-Veliz, M., S. Perez-Silanes, E. Torres, and E. Moreno-Viguri. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(9): p. 2188-2193. PMID[27025343]. PMCID[PMC4856737].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27021329">Antitubercular Agent Delamanid and Metabolites as Substrates and Inhibitors of ABC and Solute Carrier Transporters.</a> Sasabe, H., Y. Shimokawa, M. Shibata, K. Hashizume, Y. Hamasako, Y. Ohzone, E. Kashiyama, and K. Umehara. Antimicrobial Agents and Chemotherapy, 2016. 60(6): p. 3497-3508. PMID[27021329].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27196901">In Vitro Activity of Copper(II) Complexes, Loaded or Unloaded into a Nanostructured Lipid System, against Mycobacterium tuberculosis.</a> Silva, P.B., P.C. Souza, G.M. Calixto, O. Lopes Ede, R.C. Frem, A.V. Netto, A.E. Mauro, F.R. Pavan, and M. Chorilli. International Journal of Molecular Sciences, 2016. 17(5). PMID[27196901]. PMCID[PMC4881567].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26976868">MIC of Delamanid (OPC-67683) against Mycobacterium tuberculosis Clinical Isolates and a Proposed Critical Concentration.</a> Stinson, K., N. Kurepina, A. Venter, M. Fujiwara, M. Kawasaki, J. Timm, E. Shashkina, B.N. Kreiswirth, Y. Liu, M. Matsumoto, and L. Geiter. Antimicrobial Agents and Chemotherapy, 2016. 60(6): p. 3316-3322. PMID[26976868].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26809052">Antimicrobial Compounds from Endophytic Streptomyces sp. BCC72023 Isolated from Rice (Oryza sativa L.).</a> Supong, K., C. Thawai, W. Choowong, C. Kittiwongwattana, D. Thanaboripat, C. Laosinwattana, P. Koohakan, N. Parinthawong, and P. Pittayakhajonwut. Research in Microbiology, 2016. 167(4): p. 290-298. PMID[26809052].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27097919">Triazolophthalazines: Easily Accessible Compounds with Potent Antitubercular Activity.</a> Veau, D., S. Krykun, G. Mori, B.S. Orena, M.R. Pasca, C. Frongia, V. Lobjois, S. Chassaing, C. Lherbet, and M. Baltas. ChemMedChem, 2016. 11(10): p. 1078-1089. PMID[27097919].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27156615.">Neuroleptic Drugs in the Treatment of Tuberculosis: Minimal Inhibitory Concentrations of Different Phenothiazines against Mycobacterium tuberculosis.</a> Vesenbeckh, S., D. Krieger, G. Bettermann, N. Schonfeld, T.T. Bauer, H. Russmann, and H. Mauch. Tuberculosis, 2016. 98: p. 27-29. PMID[27156615].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_05_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160506&amp;CC=WO&amp;NR=2016067009A1&amp;KC=A1">Preparation of Pyrimidoindolyl Compounds with Activity against Bacteria and Mycobacteria.</a> Cooper, I., M. Pichowicz, and N. Stokes. Patent. 2016. 2015-GB53212 2016067009: 63pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_05_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160512&amp;CC=WO&amp;NR=2016073524A1&amp;KC=A1">Multi-drug Therapies for Tuberculosis Treatment.</a> Ho, C.-M., D.L. Clemens, B.-Y.L. Clemens, M.A. Horwitz, A.M. Silva Vite, T. Kee, and X. Ding. Patent. 2016. 2015-US58892 2016073524: 80pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_05_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
