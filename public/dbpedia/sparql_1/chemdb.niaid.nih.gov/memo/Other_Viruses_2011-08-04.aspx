

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="e1kDOJyU/V6K4xIFvrJItS8kp3pOcezxDl5TZwOpjIABsawlW98wthbGM7OBf32kZFqbS5K/UWWtat2D+BE/yqpz7encDbMRkOfsuXwVYvvqcJK6Jls/s/s7+/0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DB6ED123" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">July 22 - August 4, 2011</p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21803462">Acyclic Nucleoside Thiophosphonates as Potent Inhibitors of HIV and HBV Replication.</a> Barral, K., C. Weck, N. Payrot, L. Roux, C. Durafour, F. Zoulim, J. Neyts, J. Balzarini, B. Canard, S. Priet, and K. Alvarez.  European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21803462].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21786803">Design, Synthesis, and Biological Evaluation of Triazolo-pyrimidine Derivatives as Novel Inhibitors of Hepatitis B Virus Surface Antigen (HBsAg) Secretion.</a> Yu, W., C. Goddard, E. Clearfield, C. Mills, T. Xiao, H. Guo, J.D. Morrey, N.E. Motter, K. Zhao, T.M. Block, A. Cuconati, and X. Xu. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21786803].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21801309">Rational Design of Peptides with Anti-HCV/HIV Activities and Enhanced Specificity.</a> Li, G.R., L.Y. He, X.Y. Liu, A.P. Liu, Y.B. Huang, C. Qiu, X.Y. Zhang, J.Q. Xu, W. Yang, and Y.X. Chen. Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21801309].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21800000">The Synthesis of Novel Heteroaryl-fused 7,8,9,10-Tetrahydro-6H-azepino[1,2-a]indoles, 4-oOxo-2,3-dihydro-1H-[1,4]diazepino[1,7-a]indoles and 1,2,4,5-Tetrahydro-[1,4]oxazepino[4,5-a]indoles. Effective Inhibitors of HCV NS5B Polymerase.</a> Ding, M., F. He, M.A. Poss, K.L. Rigat, Y.K. Wang, S.B. Roberts, D. Qiu, R.A. Fridell, M. Gao, and R.G. Gentles. Organic &amp; Biomolecular Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21800000].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21793032">Vitamin-D: An Innate Antiviral Agent Suppressing Hepatitis C Virus in Human Hepatocytes.</a> Gal-Tanamy, M., L. Bachmetov, A. Ravid, R. Koren, A. Erman, R. Tur-Kaspa, and R. Zemel. Hepatology (Baltimore, Md.), 2011. <b>[Epub ahead of print]</b>; PMID[21793032].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21782454">P2-P1&#39; Macrocyclization of P2 Phenylglycine Based HCV NS3 Protease Inhibitors Using Ring-closing Metathesis.</a> Lampa, A., A.E. Ehrenberg, A. Vema, E. Kerblom, G. Lindeberg, U. Helena Danielson, A. Karlen, and A. Sandstrom. Bioorganic &amp; Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21782454].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21780908">Design and Synthesis of Novel Carbocyclic Versions of 2&#39;-spirocyclopropyl Ribonucleosides as Potent Anti-HCV Agents.</a> Oh, C.H., E. Kim, and J.H. Hong. Nucleosides, Nucleotides &amp; Nucleic acids, 2011. 30(6): p. 423-439; PMID[21780908].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21804247">Synthesis and Antiviral Activity of Novel 1,3,4-Thiadiazine Derivatives.</a> Yang, Y., Z. Feng, J. Jiang, X. Pan, and P. Zhang. Chemical &amp; Pharmaceutical Bulletin, 2011. 59(8): p. 1016-1019; PMID[21804247].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21787804">Antiherpetic Activity of a Sulfated Polysaccharide from Agaricus brasiliensis mycelia.</a> de Sousa Cardozo, F.T., C.M. Camelini, A. Mascarello, M. Jose Rossi, R. Jose Nunes, C.R. Monte Barardi, M.M. de Mendonca, and C.M. Simoes. Antiviral research, 2011. <b>[Epub ahead of print]</b>; PMID[21787804].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0722-080411.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292733800022">Antiviral Activity and Mode of Action of TMC647078, a Novel Nucleoside Inhibitor of the Hepatitis C Virus NS5B Polymerase.</a> Berke, J.M., L. Vijgen, S. Lachau-Durand, M.H. Powdrill, S. Rawe, E. Sjuvarsson, S. Eriksson, M. Gotte, E. Fransen, P. Dehertogh, C. Van den Eynde, L. Leclercq, T.H.M. Jonckers, P. Raboisson, M. Nilsson, B. Samuelsson, A. Rosenquist, G.C. Fanning, and T.I. Lin.  Antimicrobial Agents and Chemotherapy, 2011. 55(8): p. 3812-3820; ISI[000292733800022].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0722-080411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
