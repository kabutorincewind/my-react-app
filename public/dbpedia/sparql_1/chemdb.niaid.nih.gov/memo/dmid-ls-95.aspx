

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-95.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OzyEkxkFj4POWM5kmUXSdj7GQb/0YoN1/mx6TuH1P4ubN9H8+aNZfO2Oi2e3Vb2uKUIadzKCJ/sSnsSEglDzSP0RcVxdROsZfKBy6YBynCbZgjMsSPQM3ArsHMU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E0EF0D9B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-95-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57662   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, Antiviral Activity, and Mechanism of Drug Resistance of D- and L-2 &#39;,3 &#39;-Didehydro-2 &#39;,3 &#39;-Dideoxy-2 &#39;-Fluorocarbocyclic Nucleosides</p>

    <p>          Wang, J. <i>et al.</i></p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(11): 3736-3748</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229443700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229443700011</a> </p><br />

    <p>2.     57663   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of hexamethylene bisacetamide on replication of human cytomegalovirus</p>

    <p>          Kitagawa, R, Hagihara, K, Uhara, M, Matsutani, K, Kirita, A, and Tanaka, J</p>

    <p>          Arch Virol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15959837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15959837&amp;dopt=abstract</a> </p><br />

    <p>3.     57664   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide mimetics of gamma interferon possess antiviral properties against vaccinia virus and other viruses in the presence of poxvirus B8R protein</p>

    <p>          Ahmed, Chulbul MI, Burkhart, Marjorie A, Subramaniam, Prem S, Mujtaba, Mustafa G, and Johnson, Howard M</p>

    <p>          Journal of Virology <b>2005</b>.  79(9): 5632-5639</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     57665   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Attenuation of Equine Influenza Viruses through Truncations of the NS1 Protein</p>

    <p>          Quinlivan, M, Zamarin, D, Garcia-Sastre, A, Cullinane, A, Chambers, T, and Palese, P</p>

    <p>          J Virol <b>2005</b>.  79(13): 8431-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956587&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956587&amp;dopt=abstract</a> </p><br />

    <p>5.     57666   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyrrolidine Dithiocarbamate Reduces Coxsackievirus B3 Replication through Inhibition of the Ubiquitin-Proteasome Pathway</p>

    <p>          Si, X, McManus, BM, Zhang, J, Yuan, J, Cheung, C, Esfandiarei, M, Suarez, A, Morgan, A, and Luo, H</p>

    <p>          J Virol <b>2005</b>.  79(13): 8014-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956547&amp;dopt=abstract</a> </p><br />

    <p>6.     57667   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide Antagonists That Inhibit Sin Nombre Virus and Hantaan Virus Entry Through the Beta 3-Integrin Receptor</p>

    <p>          Larson, R. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(12): 7319-7326</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100005</a> </p><br />

    <p>7.     57668   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Alpha/Beta Interferon Signaling by the NS4B Protein of Flaviviruses</p>

    <p>          Munoz-Jordan, JL, Laurent-Rolle, M, Ashour, J, Martinez-Sobrido, L, Ashok, M, Lipkin, WI, and Garcia-Sastre, A</p>

    <p>          J Virol <b>2005</b>.  79(13): 8004-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956546&amp;dopt=abstract</a> </p><br />

    <p>8.     57669   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interferon alfacon-1 protects hamsters from lethal pichinde virus infection</p>

    <p>          Gowen, Brian B, Barnard, Dale L, Smee, Donald F, Wong, Min-Hui, Pace, Anne M, Jung, Kie-Hoon, Winslow, Scott G, Bailey, Kevin W, Blatt, Lawrence M, and Sidwell, Robert W</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(6): 2378-2386</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     57670   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Interdomain communication in hepatitis C virus polymerase abolished by small-molecule inhibitors bound to a novel allosteric site</p>

    <p>          Di Marco, S, Volpari, C, Tomei, L, Altamura, S, Harper, S, Narjes, F, Koch, U, Rowley, M, De Francesco, R, Migliaccio, G, and Carfi, A</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955819&amp;dopt=abstract</a> </p><br />

    <p>10.   57671   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p><b>          Methods of treating disease through the administration of a manzamine analog or derivative</b> </p>

    <p>          Hamann, Mark T, Rao, Karumanchi Venkateswara, and Peng, Jiangnan</p>

    <p>          PATENT:  US <b>2005085554</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004-26734  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   57672   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of cationic beta-vinyl substituted meso-tetraphenylporphyrins and their in vitro activity against herpes simplex virus type 1</p>

    <p>          Silva, EM, Giuntini, F, Faustino, MA, Tome, JP, Neves, MG, Tome, AC, Silva, AM, Santana-Marques, MG, Ferrer-Correia, AJ, Cavaleiro, JA, Caeiro, MF, Duarte, RR, Tavares, SA, Pegado, IN, d&#39;Almeida, B, De, Matos AP, and Valdeira, ML</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951174&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15951174&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   57673   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Induction of Protective Antibodies Against Dengue Virus by Tetravalent Dna Immunization of Mice With Domain Iii of the Envelope Protein</p>

    <p>          Mota, J. <i>et al.</i></p>

    <p>          Vaccine <b>2005</b>.  23(26): 3469-3476</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229196500011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229196500011</a> </p><br />

    <p>13.   57674   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Birds, Migration and Emerging Zoonoses: West Nile Virus, Lyme Disease, Influenza A and Enteropathogens</p>

    <p>          Reed, KD, Meece, JK, Henkel, JS, and Shukla, SK</p>

    <p>          Clin Med Res <b>2003</b>.  1(1): 5-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15931279&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15931279&amp;dopt=abstract</a> </p><br />

    <p>14.   57675   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of purine nucleoside analogs for treating flaviviridae including hepatitis C</p>

    <p>          Storer, Richard, Gosselin, Gilles, Dukhan, David, and Leroy, Frederic</p>

    <p>          PATENT:  WO <b>2005009418</b>  ISSUE DATE:  20050203</p>

    <p>          APPLICATION: 2004  PP: 139 pp.</p>

    <p>          ASSIGNEE:  (Idenix Cayman Limited, Cayman I., Centre National De La Recherche Scientifique, and L&#39;universite Montpellier II)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   57676   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A West Nile virus (WNV) reverse genetics dual-reporter system for high throughput cell-based screening and identifying antivirals and vaccines against flaviviral infections</p>

    <p>          Shi, Pei-Yong, Lo, Michael, and Tilgner, Mark</p>

    <p>          PATENT:  US <b>2005058987</b>  ISSUE DATE:  20050317</p>

    <p>          APPLICATION: 2003-51532  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   57677   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Efficacy of various disinfectants against SARS coronavirus</p>

    <p>          Rabenau, HF, Kampf, G, Cinatl, J, and Doerr, HW</p>

    <p>          J Hosp Infect <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923059&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923059&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   57678   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytokine responses in severe acute respiratory syndrome coronavirus-infected macrophages in vitro: possible relevance to pathogenesis</p>

    <p>          Cheung, CY, Poon, LL, Ng, IH, Luk, W, Sia, SF, Wu, MH, Chan, KH, Yuen, KY, Gordon, S, Guan, Y, and Peiris, JS</p>

    <p>          J Virol <b>2005</b>.  79(12): 7819-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15919935&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15919935&amp;dopt=abstract</a> </p><br />

    <p>18.   57679   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Synergism</p>

    <p>          Carrington, M. </p>

    <p>          Nature Genetics <b>2005</b>.  37(6): 565-566</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229495300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229495300005</a> </p><br />

    <p>19.   57680   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antioxidant and antiviral properties of Pseudopiptadenia contorta (Leguminosae) and of quebracho (Schinopsis sp.) extracts</p>

    <p>          de LMoreira, Davyson, Leitao, Suzana G, Goncalves, Jorge Luiz S, Wigg, Marcia D, and Leitao, Gilda G</p>

    <p>          Quimica Nova <b>2005</b>.  28(3): 421-425</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   57681   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of some pyrazoloquinolines as inhibitors of herpes simplex virus type 1 replication</p>

    <p>          Bekhit, Adnan A, El-Sayed, Ola A, Aboul-Enein, Hassan Y, Siddiqui, Yunus M, and Al-Ahdal, Mohammed N</p>

    <p>          Archiv der Pharmazie (Weinheim, Germany) <b>2005</b>.  338(2-3 ): 74-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   57682   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of indole nucleosides as therapeutic antiviral agents</p>

    <p>          Townsend, Leroy B and Drach, John C</p>

    <p>          PATENT:  WO <b>2005034943</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004  PP: 87 pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of Michigan, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   57683   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Study of the early steps of the Hepatitis B Virus life cycle</p>

    <p>          Lu, X and Block, T</p>

    <p>          Int J Med Sci <b>2004</b>.  1(1): 21-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15912187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15912187&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   57684   DMID-LS-95; PUBMED-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the anti-influenza virus activity of cyclopentane derivatives with oseltamivir and zanamivir in vivo</p>

    <p>          Chand, P, Bantia, S, Kotian, PL, El-Kattan, Y, Lin, TH, and Babu, YS</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(12): 4071-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911320&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15911320&amp;dopt=abstract</a> </p><br />

    <p>24.   57685   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virucidal activities of cetylpyridinium chloride with enhancers</p>

    <p>          Capps, Charles L</p>

    <p>          PATENT:  US <b>2005100612</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004-21803  PP: 11 pp., Cont.-in-part of U.S. Ser. No. 703,969.</p>

    <p>          ASSIGNEE:  (Viratox, L. L. C. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   57686   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Indoleamine 2,3-Dioxygenase Mediates Cell Type-Specific Anti-Measles Virus Activity of Gamma Interferon</p>

    <p>          Obojes, K. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(12): 7768-7776</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100048</a> </p><br />

    <p>26.   57687   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Stringent Requirement for the C Protein of Wild-Type Measles Virus for Growth Both in Vitro and in Macaques</p>

    <p>          Takeuchi, K. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(12): 7838-7844</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416100056</a> </p><br />

    <p>27.   57688   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methods and compositions for the identification of anti-poxvirus agents</p>

    <p>          Lu, Henry H, Huang, Jianing, and Payan, Donald G</p>

    <p>          PATENT:  WO <b>2005043119</b>  ISSUE DATE:  20050512</p>

    <p>          APPLICATION: 2004  PP: 77 pp.</p>

    <p>          ASSIGNEE:  (Rigel Pharmaceuticals, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   57689   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Computational Modeling of Sars Coronavirus Main Proteinase-Inhibitor Complexes</p>

    <p>          Rao, M. and Andrea, T.</p>

    <p>          Protein Science <b>2004</b>.  13: 119-120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100214">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224796100214</a> </p><br />
    <br clear="all">

    <p>29.   57690   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of natural compounds with antiviral activities against SARS-associated coronavirus</p>

    <p>          Li, Shi-you, Chen, Cong, Zhang, Hai-qing, Guo, Hai-yan, Wang, Hui, Wang, Lin, Zhang, Xiang, Hua, Shi-neng, Yu, Jun, Xiao, Pei-gen, Li, Rong-song, and Tan, Xuehai</p>

    <p>          Antiviral Research <b>2005</b>.  67(1): 18-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   57691   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development and evaluation of an enzyme-linked immunosorbent assay for detection of antibodies against the spike protein of SARS-coronavirus</p>

    <p>          Zhao, Jincun, Wang, Wei, Wang, Guang-Fa, Li, Yonghua, Zhuang, Hui, Xu, Xiaoyuan, Ren, Furong, Zhao, Zhendong, and Gao, Xiao-Ming</p>

    <p>          Journal of Clinical Virology <b>2005</b>.  33(1): 12-18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   57692   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomavirus inhibitors</p>

    <p>          Meneses, Patricio I, Koehler, Angela N, Wong, Jason C, Howley, Peter M, and Schreiber, Stuart L</p>

    <p>          PATENT:  US <b>20050123902</b>  ISSUE DATE: 20050609</p>

    <p>          APPLICATION: 2004-64975  PP: 23 pp.</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   57693   DMID-LS-95; SCIFINDER-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of Cidofovir on a naturally human papillomavirus-16 infected squamous cell carcinoma of the head and neck (SCCHN) cell line improves radiation sensitivity</p>

    <p>          Sirianni, Nicky, Wang, Jun, and Ferris, Robert L</p>

    <p>          Oral Oncology <b>2005</b>.  41(4): 423-428</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   57694   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Improvement of Influenza a/Fujian/411/02 (H3n2) Virus Growth in Embryonated Chicken Eggs by Balancing the Hemagglutinin and Neuraminidase Activities, Using Reverse Genetics</p>

    <p>          Bin, L. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(11): 6763-6771</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400020</a> </p><br />

    <p>34.   57695   DMID-LS-95; WOS-DMID-6/20/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cinanserin Is an Inhibitor of the 3c-Like Proteinase of Severe Acute Respiratory Syndrome Coronavirus and Strongly Reduces Virus Replication in Vitro</p>

    <p>          Chen, L. <i>et al.</i></p>

    <p>          Journal of Virology <b>2005</b>.  79(11): 7095-7103</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400054">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229085400054</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
