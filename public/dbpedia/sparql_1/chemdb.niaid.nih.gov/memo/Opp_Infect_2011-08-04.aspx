

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-08-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="J8FkXfLnuf2OJPfeSKvhYP36IZXnr+a62ZtTq5ZCW5zOgf596tY9x4v9rhnzlf8W17toJuvYU32hY8VgrOLKxMEbFP8nuJ+zVLOzBmSaPBcK2X6sLNN262rJVbA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="44F611FC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">July 22 - August 4, 2011</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21755533">Antifungal Activity of Coriaria nepalensis Essential Oil by Disrupting Ergosterol Biosynthesis and Membrane Integrity Against Candida.</a> Ahmad, A., A. Khan, P. Kumar, R.P. Bhatt, and N. Manzoor. Yeast (Chichester, England), 2011. 28(8): p. 611-617; PMID[21755533].</p>

    <p class="NoSpacing"> <b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21602333">Broad-Spectrum Biofilm Inhibition by Kingella kingae Exopolysaccharide.</a>Bendaoud, M., E. Vinogradov, N.V. Balashova, D.E. Kadouri, S.C. Kachlany, and J.B. Kaplan. Journal of Bacteriology, 2011. 193(15): p. 3879-3886; PMID[21602333].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21674619">A Novel Assay of Biofilm Antifungal Activity Reveals that Amphotericin B and Caspofungin Lyse Candida albicans Cells in Biofilms.</a> Didone, L., D. Oga, and D.J. Krysan. Yeast (Chichester, England), 2011. 28(8): p. 561-568; PMID[21674619].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21805018">7-Chloroquinolin-4-yl Arylhydrazone Derivatives: Synthesis and Antifungal Activity.</a> Duval, A.R., P.H. Carvalho, M.C. Soares, D.P. Gouvea, G.M. Siqueira, R.G. Lund, and W. Cunico. The Scientific World Journal, 2011. 11: p. 1489-1495; PMID[21805018].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21788125">Antifungal Activity of Coriandrum sativum Essential Oil, its Mode of Action Aainst Candida Secies and Poential Synergism with Amphotericin B.</a> Silva, F., S. Ferreira, A. Duarte, D.I. Mendonca, and F.C. Domingues. Phytomedicine : International Journal of Phytotherapy and Phytopharmacology, 2011. <b>[Epub ahead of print]</b>; PMID[21788125].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>.  OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21778264">Blocking Candida albicans Biofilm Formation by BDSF and trans-BDSF.</a> Zhang, Y., C. Cai, Y. Yang, L. Weng, and L. Wang. Journal of Medical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21778264].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21776985">Novel 1,2,3-triazole Derivatives for use Against Mycobacterium tuberculosis H37Rv (ATCC 27294) Strains.</a> Boechat, N., V.F. Ferreira, S.B. Ferreira, M.D. Ferreira, F.D. da Silva, M.M. Bastos, M.D. Costa, M.C. Lourenco, A. Pinto, A.U. Krettli, A.C. Aguiar, B.M. Teixeira, N.V. da Silva, P.R. Martins, F.A. Bezerra, A.L. Camilo, G.P. da Silva, and C.C. Costa. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21776985].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21737274">Synthesis, Molecular Modeling and Bio-evaluation of Cycloalkyl Fused 2-Aminopyrimidines as Antitubercular and Antidiabetic Agents.</a> Singh, N., S.K. Pandey, N. Anand, R. Dwivedi, S. Singh, S.K. Sinha, V. Chaturvedi, N. Jaiswal, A.K. Srivastava, P. Shah, M.I. Siddiqui, and R.P. Tripathi. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4404-4408; PMID[21737274].</p>
    
    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21780742">A Prodrug Approach for Improving Anti-tuberculosis Activity of Potent Mycobacterium tuberculosis Type II Dehydroquinase Inhibitors.</a> Tizon, L., J.M. Otero, V.F. Prazeres, A.L. Llamas-Saiz, G.C. Fox, M.J. van Raaij, H. Lamb, A.R. Hawkins, J.A. Ainsa, L. Castedo, and C. Gonzalez-Bello. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21780742].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21724395">New Salicylamide and Sulfonamide Derivatives of Quinoxaline1,4-di-N-oxide with Antileishmanial and Antimalarial Activities.</a> Barea, C., A. Pabon, D. Castillo, M. Zimic, M. Quiliano, S. Galiano, S. Perez-Silanes, A. Monge, E. Deharo, and I. Aldana. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4498-4502; PMID[21724395].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21807973">Design and Evaluation of Primaquine-Artemisinin Hybrids as a Multistage Anti-Malarial Strategy.</a> Capela, R., G.G. Cabal, P.J. Rosenthal, J. Gut, M.M. Mota, R. Moreira, F. Lopes, and M. Prudencio. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21807973].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21596024">Effects of Highly Active Novel Artemisinin-Chloroquinoline Hybrid Compounds on beta-Hematin Formation, Parasite Morphology and Endocytosis in Plasmodium falciparum.</a> Feng, T.S., E.M. Guantai, M. Nell, C.E. van Rensburg, K. Ncokazi, T.J. Egan, H.C. Hoppe, and K. Chibale. Biochemical Pharmacology, 2011. 82(3): p. 236-247; PMID[21596024].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723121">Synthesis and Biological Activities of 4-N-(Anilinyl-n-[oxazolyl])-7-chloroquinolines (n=3&#39; or 4&#39;) Against Plasmodium falciparum in In vitro Models.</a> Gordey, E.E., P.N. Yadav, M.P. Merrin, J. Davies, S.A. Ward, G.M. Woodman, A.L. Sadowy, T.G. Smith, and R.A. Gossage. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4512-4515; PMID[21723121].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21786822">Antiplasmodial and Antitrypanosomal Activity of Pyrethrins and Pyrethroids.</a>Hata, Y., S. Zimmermann, M. Quitschau, M. Kaiser, M. Hamburger, and M. Adams. Journal of Agricultural and Food Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21786822].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21689375">Multi-targeted Activity of Maslinic acid as an Antimalarial Natural Compound.</a>Moneriz, C., J. Mestres, J.M. Bautista, A. Diez, and A. Puyet. The FEBS Journal, 2011. 278(16): p. 2951-2961; PMID[21689375].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21682307">Glutathione Reductase-Catalyzed Cascade of Redox Reactions To Bioactivate Potent Antimalarial 1,4-Naphthoquinones - A New Strategy to Combat Malarial Parasites.</a> Muller, T., L. Johann, B. Jannack, M. Bruckner, D.A. Lanfranchi, H. Bauer, C. Sanchez, V. Yardley, C. Deregnaucourt, J. Schrevel, M. Lanzer, R.H. Schirmer, and E. Davioud-Charvet. Journal of the American Chemical Society, 2011. 133(30): p. 11557-11571; PMID[21682307].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21302338">In vivo and In vitro Antimalarial Activity of 4-Nerolidylcatechol.</a>Rocha, E.S.L.F., A.C. Silva Pinto, A.M. Pohlit, E.L. Quignard, P.P. Vieira, W.P. Tadei, F.C. Chaves, J.F. Samonek, C.A. Lima, M.R. Costa, M.G. Alecrim, and V.F. Andrade-Neto. Phytotherapy Research: PTR, 2011. 25(8): p. 1181-1188; PMID[21302338].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723734">Synthesis and Antimalarial Activity of New Heterocyclic Hybrids Based on Chloroquine and Thiazolidinone Scaffolds.</a> Rojas Ruiz, F.A., R.N. Garcia-Sanchez, S.V. Estupinan, A. Gomez-Barrio, D.F. Torres Amado, B.M. Perez-Solorzano, J.J. Nogal-Ruiz, A.R. Martinez-Fernandez, and V.V. Kouznetsov. Bioorganic &amp; Medicinal Chemistry, 2011. 19(15): p. 4562-4573; PMID[21723734].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21705220">Antiplasmodial and Cytotoxicity Evaluation of 3-Functionalized 2-Azetidinone Derivatives.</a> Singh, P., S. Sachdeva, R. Raj, V. Kumar, M.P. Mahajan, S. Nasser, L. Vivas, J. Gut, P.J. Rosenthal, T.S. Feng, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4561-4563; PMID[21705220].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIA COMPOUNDS</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21805542">Studies on the Antileishmanial Properties of the Antimicrobial Peptides Temporin A, B and 1Sa.</a> Chadbourne, F.L., C. Raleigh, H.Z. Ali, P.W. Denny, and S.L. Cobb. Journal of Peptide Science : An Official Publication of the European Peptide Society, 2011. <b>[Epub ahead of print]</b>; PMID[21805542].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21601896">Antiprotozoal and Antiangiogenic Saponins from Apodytes dimidiata.</a> Foubert, K., F. Cuyckens, A. Matheeussen, A. Vlietinck, S. Apers, L. Maes, and L. Pieters. Phytochemistry, 2011. 72(11-12): p. 1414-1423; PMID[21601896].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21571403">Selenocyanates and Diselenides: A New Class of Potent Antileishmanial Agents.</a> Plano, D., Y. Baquedano, D. Moreno-Mateos, M. Font, A. Jimenez-Ruiz, J.A. Palop, and C. Sanmartin. European Journal of Medicinal Chemistry, 2011. 46(8): p. 3315-3323; PMID[21571403].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21796699">Antiparasitic Activity of C-Geranyl Flavonoids from Mimulus bigelovii.</a> Salem, M.M., J. Capers, S. Rito, and K.A. Werbovetz. Phytotherapy Research : PTR, 2011. 25(8): p. 1246-1249; PMID[21796699].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21783345">Uptake and Antileishmanial Activity of Meglumine Antimoniate-containing Liposomes in Leishmania (Leishmania) Major-infected Macrophages.</a>Treiger Borborema, S.E., R.A. Schwendener, J.A. Osso Junior, H.F. de Andrade Junior, and N. do Nascimento. International Journal of Antimicrobial Agents, 2011. <b>[Epub ahead of print]</b>; PMID[21783345].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292729900014">Design, Synthesis and Antimycobacterial Evaluation of Novel 3-Substituted-N-Aryl-6,7-dimethoxy-3a,4-dihydro-3H-indeno 1,2-c pyrazole -2-carboxamide Analogues.</a> Ahsan, M.J., J.G. Samy, K.R. Dutt, U.K. Agrawal, B.S. Yadav, S. Vyas, R. Kaur, and G. Yadav, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4451-4453. ISI[000292729900014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292698100017">Microbial Transformation of (+)-Heraclenin by Aspergillus niger and Evaluation of its Antiplasmodial and Antimicrobial Activities.</a> Gowri, P.M., K. Haribabu, H. Kishore, O. Manjusha, S. Biswas, and U.S.N. Murty, Current Science, 2011. 100(11): p. 1706-1711. ISI[000292698100017].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292797700021">Antifungal Activity of Lariciresinol Derived from Sambucus williamsii and Their Membrane-active Mechanisms in Candida albicans.</a>Hwang, B., J. Cho, I.S. Hwang, H.G. Jin, E.R. Woo, and D.G. Lee, Biochemical and Biophysical Research Communications, 2011. 410(3): p. 489-493. ISI[000292797700021].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292670000039">Synthesis and Antimicrobial Evaluation of New Benzofuran Derivatives.</a> Jiang, X.Z., W.L. Liu, W. Zhang, F.Q. Jiang, Z. Gao, H. Zhuang, and L. Fu, European Journal of Medicinal Chemistry, 2011. 46(8): p. 3526-3530. ISI[000292670000039].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292729900019">Synthesis, In vitro Evaluation and Molecular Docking Studies of New Triazole Derivatives as Antifungal Agents.</a> Jiang, Y.W., J. Zhang, Y.B. Cao, X.Y. Chai, Y. Zou, Q.Y. Wu, D.Z. Zhang, Y.Y. Jiang, and Q.Y. Sun, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(15): p. 4471-4475. ISI[000292729900019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292484100001">Chemical Composition and Antimicrobial Activity of the Essential Oil of Rosemary.</a> Jiang, Y., N. Wu, Y.J. Fu, W. Wang, M. Luo, C.J. Zhao, Y.G. Zu, and X.L. Liu, Environmental Toxicology and Pharmacology, 2011. 32(1): p. 63-68. ISI[000292484100001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292716800013">Assessment of Antimicrobial Activity of Catechu and Its Dyed Substrate.</a> Khan, M.I., A. Ahmad, S.A. Khan, M. Yusuf, M. Shahid, N. Manzoor, and F. Mohammad, Journal of Cleaner Production, 2011. 19(12): p. 1385-1394. ISI[000292716800013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292631900001">Effective Inhibitors of the Essential Kinase PknB and Their Potential as anti-Mycobacterial Agents.</a> Lougheed, K.E.A., S.A. Osborne, B. Saxty, D. Whalley, T. Chapman, N. Bouloc, J. Chugh, T.J. Nott, D. Patel, V.L. Spivey, C.A. Kettleborough, J.S. Bryans, D.L. Taylor, S.J. Smerdon, and R.S. Buxton, Tuberculosis, 2011. 91(4): p. 277-286. ISI[000292631900001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292792400005">Study on Chemical Composition and Biological Activities of Essential Oil and Extracts from Stevia rebaudiana Bertoni Leaves.</a>Muanda, F.N., R. Soulimani, B. Diop, and A. Dicko, Lwt-Food Science and Technology, 2011. 44(9): p. 1865-1872. ISI[000292792400005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292579200007">Studies on Microbial Activity and Dyeing Performance of Novel Acid Azo Dyes Based on 3-(4-Aminophenyl)-2-phenylquinazolin-4(3h)-one.</a>Parekh, N., K. Maheria, and P. Patel, Journal of Scientific &amp; Industrial Research, 2011. 70(7): p. 525-532. ISI[000292579200007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292721000012">Phorboxazole Synthetic Studies: Design, Synthesis and Biological Evaluation of Phorboxazole A and Hemi-phorboxazole</a> A related analogues. Smith, A.B., A.M.L. Hogan, Z.Q. Liu, T.M. Razler, R.M. Meis, B.I. Morinaka, and T.F. Molinski, Tetrahedron, 2011. 67(27-28): p. 5069-5078. ISI[000292721000012]. <b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292851900014">Biological Activity and Structure Dependent Properties of Cuprous iodide Complexes with Phenanthrolines and Water soluble tris (Aminomethyl) phosphanes.</a> Starosta, R., K. Stokowa, M. Florek, J. Krol, A. Chwilkowska, J. Kulbacka, J. Saczko, J. Skala, and M. Jezowska-Bojczuk, Journal of Inorganic Biochemistry, 2011. 105(8): p. 1102-1108. ISI[000292851900014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">37.  <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292710900002">Antimicrobial Potential and Chemical Composition of Mentha piperita Oil in Liquid and Vapour Phase Against Food Spoiling Microorganisms.</a> Tyagi, A.K. and A. MalikFood Control, 2011. 22(11): p. 1707-1714. ISI[000292710900002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292733800031">Novel Inhibitors of InhA Efficiently Kill Mycobacterium tuberculosis under Aerobic and Anaerobic Conditions.</a> Vilcheze, C., A.D. Baughn, J. Tufariello, L.W. Leung, M. Kuo, C.F. Basler, D. Alland, J.C. Sacchettini, J.S. Freundlich, and W.R. Jacobs, Antimicrobial Agents and Chemotherapy, 2011. 55(8): p. 3889-3898. ISI[000292733800031].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292636200013">Synthesis and Antituberculosis Activity of Derivatives of the Diterpenoid isosteviol with Azine, Hydrazide, and Hydrazone Moieties.</a> Garifullin, B.F., I.Y. Strobykina, G.G. Mordovskoi, V.F. Mironov, and V.E. Kataev, Chemistry of Natural Compounds, 2011. 47(1): p. 55-58. ISI[000292636200013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292436800020">Antimicrobial Activity of Two Essential Oils.</a>Mickiene, R., B. Bakutis, and V. Baliukoniene, Annals of Agricultural and Environmental Medicine, 2011. 18(1): p. 139-144. ISI[000292436800020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>

    <p> </p>

    <p class="NoSpacing">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000292501600001">Silver Enhances the In vitro Antifungal Activity of the Saponin, CAY-1.</a> De Lucca, A.J., S. Boue, T. Sien, T.E. Cleveland, and T.J. Walsh, Mycoses, 2011. 54(4): p. E1-E9. ISI[000292501600001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0722-080411.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
