

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-126.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kqnoV8uQP3atsE/G1XkwcOWxI81/IcCHG8CgSvnltdqF4xM+AixVynRxjmCKJmXMWT0gVn9C/xJAf99a66SV8tTKxFiHX0ZPzIdY33f5mJkTBLRtWq6QnBSmsZY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EDDDB34A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-126-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58801   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Pathogenicity and response to topical antiviral therapy in a murine model of acyclovir-sensitive and acyclovir-resistant herpes simplex viruses isolated from the same patient</p>

    <p>          Lebel, Annie and Boivin, Guy</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  37(1): 34-37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4K6CR0K-1/2/237f2b9378e3da591139b3cd64175b26">http://www.sciencedirect.com/science/article/B6VJV-4K6CR0K-1/2/237f2b9378e3da591139b3cd64175b26</a> </p><br />

    <p>2.     58802   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Prediction of Activity, Synthesis and Biological Testing of anti-HSV Active Peptides</p>

    <p>          Jenssen, H, Gutteberg, TJ, Rekdal, O, and Lejon, T</p>

    <p>          Chem Biol Drug Des <b>2006</b>.  68(1): 58-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923027&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923027&amp;dopt=abstract</a> </p><br />

    <p>3.     58803   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Potent inhibition of human Hepatitis B virus replication by a host factor Vps4</p>

    <p>          Kian Chua, Pong, Lin, Min-Hui, and Shih, Chiaho</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4KPFKFC-2/2/35fed8911b19d98fb80d89978ff7e718">http://www.sciencedirect.com/science/article/B6WXR-4KPFKFC-2/2/35fed8911b19d98fb80d89978ff7e718</a> </p><br />

    <p>4.     58804   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Antigenic and cellular localisation analysis of the severe acute respiratory syndrome coronavirus nucleocapsid protein using monoclonal antibodies</p>

    <p>          Bussmann, BM, Reiche, S, Jacob, LH, Braun, JM, and Jassoy, C</p>

    <p>          Virus Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920216&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16920216&amp;dopt=abstract</a> </p><br />

    <p>5.     58805   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Antiviral interactions of an HCV polymerase inhibitor with an HCV protease inhibitor or interferon in vitro</p>

    <p>          Koev, Gennadiy, Dekhtyar, Tatyana, Han, Lixin, Yan, Ping, Ng, Teresa I, Lin, CThomas, Mo, Hongmei, and Molla, Akhteruzzaman</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4KNM92B-2/2/323bab93d931a3343b80e097090e726c">http://www.sciencedirect.com/science/article/B6T2H-4KNM92B-2/2/323bab93d931a3343b80e097090e726c</a> </p><br />
    <br clear="all">

    <p>6.     58806   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          A convenient cell fusion assay for the study of SARS-CoV entry and inhibition</p>

    <p>          Sha, Y, Wu, Y, Cao, Z, Xu, X, Wu, W, Jiang, D, Mao, X, Liu, H, Zhu, Y, Gong, R, and Li, W</p>

    <p>          IUBMB Life <b>2006</b>.  58(8): 480-486</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16916786&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16916786&amp;dopt=abstract</a> </p><br />

    <p>7.     58807   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Structure-Based Drug Design and Structural Biology Study of Novel Nonpeptide Inhibitors of Severe Acute Respiratory Syndrome Coronavirus Main Protease</p>

    <p>          Lu, IL, Mahindroo, N, Liang, PH, Peng, YH, Kuo, CJ, Tsai, KC, Hsieh, HP, Chao, YS, and Wu, SY</p>

    <p>          J Med Chem <b>2006</b>.  49(17): 5154-5161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913704&amp;dopt=abstract</a> </p><br />

    <p>8.     58808   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Nonpeptide Inhibitors of Measles Virus Entry</p>

    <p>          Sun, A, Prussia, A, Zhan, W, Murray, EE, Doyle, J, Cheng, LT, Yoon, JJ, Radchenko, EV, Palyulin, VA, Compans, RW, Liotta, DC, Plemper, RK, and Snyder, JP</p>

    <p>          J Med Chem <b>2006</b>.  49(17): 5080-5092</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913698&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913698&amp;dopt=abstract</a> </p><br />

    <p>9.     58809   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          2-Deoxy-2,3-didehydro-N-acetylneuraminic acid analogues structurally modified at the C-4 position: Synthesis and biological evaluation as inhibitors of human parainfluenza virus type 1</p>

    <p>          Ikeda, K, Sato, K, Kitani, S, Suzuki, T, Maki, N, Suzuki, Y, and Sato, M</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908163&amp;dopt=abstract</a> </p><br />

    <p>10.   58810   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Anti-infective potential of natural products: How to develop a stronger in vitro `proof-of-concept&#39;</p>

    <p>          Cos, Paul, Vlietinck, Arnold J, Berghe, Dirk Vanden, and Maes, Louis</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  106(3): 290-302</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4JRT33S-7/2/14bcae22ded1211502c257a98a5a1577">http://www.sciencedirect.com/science/article/B6T8D-4JRT33S-7/2/14bcae22ded1211502c257a98a5a1577</a> </p><br />
    <br clear="all">

    <p>11.   58811   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Establishment of a novel foreign gene delivery system combining an HSV amplicon with an attenuated replication-competent virus, HSV-1 HF10</p>

    <p>          Zhang, Lumin, Daikoku, Tohru, Ohtake, Koichiro, Ohtsuka, Junpei, Nawa, Akihiro, Kudoh, Ayumi, Iwahori, Satoko, Isomura, Hiroki, Nishiyama, Yukihiro, and Tsurumi, Tatsuya</p>

    <p>          Journal of Virological Methods <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4KF7814-3/2/b8348e8fbb0e82f794b8d55a912da30e">http://www.sciencedirect.com/science/article/B6T96-4KF7814-3/2/b8348e8fbb0e82f794b8d55a912da30e</a> </p><br />

    <p>12.   58812   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Safety of neuraminidase inhibitors for influenza</p>

    <p>          Jones, M and Del Mar, C</p>

    <p>          Expert Opin Drug Saf <b>2006</b>.  5(5): 603-608</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16907649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16907649&amp;dopt=abstract</a> </p><br />

    <p>13.   58813   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Cytostatic and antiviral 6-arylpurine ribonucleosides. Part 7: Synthesis and evaluation of 6-substituted purine l-ribonucleosides</p>

    <p>          Hocek, M, Silhar, P, Shih, IH, Mabery, E, and Mackman, R</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905315&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16905315&amp;dopt=abstract</a> </p><br />

    <p>14.   58814   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Tricyclic etheno analogs of PMEG and PMEDAP: Synthesis and biological activity</p>

    <p>          Horejsi, K, Andrei, G, Clercq, ED, Snoeck, R, Pohl, R, and Holy, A</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904327&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904327&amp;dopt=abstract</a> </p><br />

    <p>15.   58815   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Novel approaches for therapy of chronic hepatitis C</p>

    <p>          Stauber, Rudolf E and Stadlbauer, Vanessa</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  36(2): 87-94</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4JHMY9N-1/2/96e02f063d7ed33f6b6b11234026beb2">http://www.sciencedirect.com/science/article/B6VJV-4JHMY9N-1/2/96e02f063d7ed33f6b6b11234026beb2</a> </p><br />
    <br clear="all">

    <p>16.   58816   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Defining a Role for Antiviral Drugs in the Treatment of Persons with HHV-8 Infection</p>

    <p>          Casper, C</p>

    <p>          Herpes <b>2006</b>.  13(2): 42-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895654&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895654&amp;dopt=abstract</a> </p><br />

    <p>17.   58817   DMID-LS-126; PUBMED-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome-associated coronavirus 3a protein forms an ion channel and modulates virus release</p>

    <p>          Lu, W, Zheng, BJ, Xu, K, Schwarz, W, Du, L, Wong, CK, Chen, J, Duan, S, Deubel, V, and Sun, B</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.  103(33): 12540-12545</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16894145&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16894145&amp;dopt=abstract</a> </p><br />

    <p>18.   58818   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Evolution of the susceptibility to antiviral drugs of A/H3N2 influenza viruses isolated in France from 2002 to 2005</p>

    <p>          Ferraris, Olivier, Kessler, Nicole, Valette, Martine, and Lina, Bruno</p>

    <p>          Vaccine <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD4-4K4PVW5-6/2/ebbb01d4bb7c9ce4b690de5a397f37c5">http://www.sciencedirect.com/science/article/B6TD4-4K4PVW5-6/2/ebbb01d4bb7c9ce4b690de5a397f37c5</a> </p><br />

    <p>19.   58819   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Pharmacokinetic Boosting of Vx-950, an Inhibitor of Hcv Protease, by Co-Dosing With Ritonavir</p>

    <p>          Kempf, D. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100005</a> </p><br />

    <p>20.   58820   DMID-LS-126; EMBASE-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Encapsulation of antiviral nucleotide analogues azidothymidine-triphosphate and cidofovir in poly(iso-butylcyanoacrylate) nanocapsules</p>

    <p>          Hillaireau, H, Le Doan, T, Besnard, M, Chacun, H, Janin, J, and Couvreur, P</p>

    <p>          International Journal of Pharmaceutics <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7W-4KCH733-3/2/54c6f67c9b153c1a3c4c7ef717e201d7">http://www.sciencedirect.com/science/article/B6T7W-4KCH733-3/2/54c6f67c9b153c1a3c4c7ef717e201d7</a> </p><br />

    <p>21.   58821   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Inhibition of Hepatitis C Virus Replication and Expression by Small Interfering Rna Targeting Host Cellular Genes</p>

    <p>          Qi, Z. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100412">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100412</a> </p><br />
    <br clear="all">

    <p>22.   58822   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Tenofovir (Tnv) Has Stronger Antiviral Effect Than Adefovir Dipivoxil (Adv) Against Lamivudine (Lam) Resistant Hepatitis B Virus (Hbv)</p>

    <p>          Hann, H., Chae, H., and Dunn, S.</p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S184-S185</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100495">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100495</a> </p><br />

    <p>23.   58823   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Safety and Pharmacokinetics of the Nonnucleoside Polymerase Inhibitor, Hcv-796: Results of a Randomized, Double-Blind, Placebo-Controlled, Ascending Single-Dose Study in Healthy Subjects</p>

    <p>          Chandra, P. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S208-S209</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100561">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100561</a> </p><br />

    <p>24.   58824   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Anti-Viral Activity of Vx-950 Resolves Expression of an Hcv-Associated Gene Signature</p>

    <p>          Ramachandran, R. <i>et al.</i></p>

    <p>          Journal of Hepatology <b>2006</b>.  44: S223</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100601">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100601</a> </p><br />

    <p>25.   58825   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Mouse Models for the Evaluation of Antiviral and Immunotherapy in the Treatment of Systemic Adenovirus Infections</p>

    <p>          Lenaerts, L. <i>et al.</i></p>

    <p>          International Journal of Infectious Diseases <b>2006</b>.  10: S42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239428800074">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239428800074</a> </p><br />

    <p>26.   58826   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Antiviral Activity of the Non-Nucleoside Polymerase Inhibitor, Hcv-796, in Patients With Chronic Hepatitis C Virus: Preliminary Results From a Randomized, Double-Blind, Placebo-Controlled, Ascending Multiple Dose Study</p>

    <p>          Chandra, P. <i>et al.</i></p>

    <p>          Gastroenterology <b>2006</b>.  130(4): A748</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705492">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705492</a> </p><br />

    <p>27.   58827   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Generation and Characterization of Hcv Replicons With Reduced Sensitivity to Itmn 191, a Macrocyclic Inhibitor of Ns3/4a</p>

    <p>          Seiwert, S. <i>et al.</i></p>

    <p>          Gastroenterology <b>2006</b>.  130(4): A754</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705522">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705522</a> </p><br />
    <br clear="all">

    <p>28.   58828   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Antiviral Activity of Cho-Ss-Derived Omega Interferon and Other Interferons Against Hcv Rna Replicons and Related Viruses</p>

    <p>          Buckwold, V. <i>et al.</i></p>

    <p>          Gastroenterology <b>2006</b>.  130(4): A770</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705595">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961705595</a> </p><br />

    <p>29.   58829   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Nim811 Inhibits Hcv Replication Through Interaction With Cyclophilins</p>

    <p>          Tao, J. <i>et al.</i></p>

    <p>          Gastroenterology <b>2006</b>.  130(4): A829</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961706268">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236961706268</a> </p><br />

    <p>30.   58830   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Synthesis of 2 &#39;-C-Methylcytidine and 2 &#39;-C-Methyluridine Derivatives Modified in the 3 &#39;-Position as Potential Antiviral Agents</p>

    <p>          Pierra, C. <i>et al.</i></p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(7): 991-1010</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900006</a> </p><br />

    <p>31.   58831   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          New N-4-Hydroxycytidine Derivatives: Synthesis and Antiviral Activity</p>

    <p>          Ivanov, M. <i>et al.</i></p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(7): 1099-1106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900013</a> </p><br />

    <p>32.   58832   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Adenosine N-1-Oxide Analogues as Inhibitors of Orthopox Virus Replication</p>

    <p>          Khandazhinskaya, A. <i>et al.</i></p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(7): 1107-1121</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900014</a> </p><br />

    <p>33.   58833   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          The 3-Deaza and 7-Deaza Derivatives of 5 &#39;-Amino-5 &#39;-Deoxy-5 &#39;-Noraristeromycin</p>

    <p>          Yang, M., Serbessa, T., and Schneller, S.</p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(7): 1122-1129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900015</a> </p><br />

    <p>34.   58834   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Functional Characterization of the Ns2b/Ns3 Protease Complex From Seven Viruses Belonging to Different Groups Inside the Genus Flavivirus</p>

    <p>          Bessaud, M. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  120(1-2): 79-90</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800008</a> </p><br />
    <br clear="all">

    <p>35.   58835   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Inhibition of Severe Acute Respiratory Syndrome-Associated Coronavirus (Sars-Cov) Infectivity by Peptides Analogous to the Viral Spike Protein</p>

    <p>          Sainz, B. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  120(1-2): 146-155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239294800016</a> </p><br />

    <p>36.   58836   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Exo-Imino to Endo-Iminocyclitol Rearrangement. A General Route to Five-Membered Antiviral Azasugars</p>

    <p>          Moriarty, R. <i>et al.</i></p>

    <p>          Organic Letters <b>2006</b>.  8(16): 3465-3467</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239309900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239309900014</a> </p><br />

    <p>37.   58837   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Prenylation of Hdag and Antiviral Drug Development</p>

    <p>          Glenn, JS <b>2006</b>.  307: 133-149</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238999800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238999800007</a> </p><br />

    <p>38.   58838   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Ribonucleotide Reductase Inhibitors and Future Drug Design</p>

    <p>          Shao, J. <i>et al.</i></p>

    <p>          Current Cancer Drug Targets <b>2006</b>.  6(5): 409-431</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239265100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239265100003</a> </p><br />

    <p>39.   58839   DMID-LS-126; WOS-DMID-8/28/2006</p>

    <p class="memofmt1-2">          Classifying Dengue: a Review of the Difficulties in Using the Who Case Classification for Dengue Haemorrhagic Fever</p>

    <p>          Bandyopadhyay, S., Lum, L., and Kroeger, A.</p>

    <p>          Tropical Medicine &amp; International Health <b>2006</b>.  11(8): 1238-1255</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239194100011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239194100011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
