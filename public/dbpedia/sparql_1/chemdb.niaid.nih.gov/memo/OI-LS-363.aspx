

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-363.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0PF3heIjFTnRqfBJI7hSLs27mi4EjkBktjACylbDsgsT7fgQTmnxrUVkXksk1M5bFFTeIbCSWXRAsTs/1qMOTbyrhwbSL/SHucUuUzowDeTRfaMStZtfJLUVaPQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="73DF15A9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-363-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59615   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          In vitro antiviral activity of SCH446211 (SCH6), a novel inhibitor of the hepatitis C virus NS3 serine protease</p>

    <p>          Liu, R, Abid, K, Pichardo, J, Pazienza, V, Ingravallo, P, Kong, R, Agrawal, S, Bogen, S, Saksena, A, Cheng, KC, Prongay, A, Njoroge, FG, Baroudy, BM, and Negro, F</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17151003&amp;dopt=abstract</a> </p><br />

    <p>2.     59616   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Designing and analysis of a potent bi-functional aptamers that inhibit protease and helicase activities of HCV NS3</p>

    <p>          Umehara, T, Fukuda, K, Nishikawa, F, Sekiya, S, Kohara, M, Hasegawa, T, and Nishikawa, S</p>

    <p>          Nucleic Acids Symp Ser (Oxf) <b>2004</b>.(48): 195-196</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150545&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17150545&amp;dopt=abstract</a> </p><br />

    <p>3.     59617   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of pyrazinecarboxamides</p>

    <p>          Dolezal, Martin, Cmedlova, Pavlina, Palek, Lukas, Kunes, Jiri, Buchta, Vladimir, Jampilek, Josef, and Kralova, Katarina</p>

    <p>          Proceedings of ECSOC-9, International Electronic Conference on Synthetic Organic Chemistry, 9th, Nov. 1-30, 2005 <b>2005</b>.: C010-1-C010/16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59618   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Moxifloxacin, ofloxacin, sparfloxacin, and ciprofloxacin against Mycobacterium tuberculosis: Evaluation of in vitro and pharmacodynamic indices that best predict in vivo efficacy</p>

    <p>          Shandil, RK, Jayaram, R, Kaur, P, Gaonkar, S, Suresh, BL, Mahesh, BN, Jayashree, R, Nandi, V, Bharath, S, and Balasubramanian, V</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145798&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17145798&amp;dopt=abstract</a> </p><br />

    <p>5.     59619   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity of 3,4-disubstituted-1,2,4-triazole derivatives</p>

    <p>          Kutkowska, Jolanta, Modzelewska-Banachiewicz, Bozena, Ziolkowska, Grazyna, Rzeski, Wojciech, Urbanik-Sypniewska, Teresa, Zwolska, Zofia, and Prus, Monika</p>

    <p>          Acta Poloniae Pharmaceutica <b>2005</b>.  62(4): 303-306</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     59620   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial evaluation of pyranone and pyridinone metal-chelator bearing fluoroquinolones</p>

    <p>          Chen, Yeh-Long, Huang, Hung-Yuan, Chen, Yu-Wen, Tzeng, Cherng-Chyi, Liu, Chiao-Ling, and Yao, Chen-Wen</p>

    <p>          Chinese Pharmaceutical Journal (Taipei, Taiwan) <b>2005</b>.  57(2-6): 57-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     59621   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Evaluation of antitubercular drug loaded alginate nanoparticles against experimental tuberculosis</p>

    <p>          Ahmad, Zahoor, Pandey, Rajesh, Sharma, Sadhna, and Khuller, Gopal Krishan</p>

    <p>          Nanoscience <b>2005</b>.  1(2): 81-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59622   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Drugs that inhibit mycolic acid biosynthesis in Mycobacterium tuberculosis - an update</p>

    <p>          Basso, LA and Santos, DS</p>

    <p>          Medicinal Chemistry Reviews--Online <b>2005</b>.  2(5): 393-413</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     59623   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Overexpression, purification, crystallization and preliminary X-ray analysis of uracil N-glycosylase from Mycobacterium tuberculosis in complex with a proteinaceous inhibitor</p>

    <p>          Singh, P, Talawar, RK, Krishna, PD, Varshney, U, and Vijayan, M</p>

    <p>          Acta Crystallograph Sect F Struct Biol Cryst Commun <b>2006</b>.  62(Pt 12): 1231-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17142904&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17142904&amp;dopt=abstract</a> </p><br />

    <p>10.   59624   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Capreomycin resistance in Mycobacterium tuberculosis: Identification the molecular mechanism of resistance and characterization of cross-resistance to ribosome-inhibiting drugs</p>

    <p>          Maus, Courtney E</p>

    <p>          DISSERATION - Emory Univ.,Atlanta,GA <b>2005</b>.: 136 PP</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   59625   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial evaluation of various 7-substituted ciprofloxacin derivatives</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, Basha, Jafar Sadik, Radha, Deshpande R, and Nagaraja, Valakunja</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(20): 5774-5778</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   59626   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Studies on pyrazine derivatives, XLII: Synthesis and tuberculostatic activity of 6-(1,4-dioxa-8-azaspiro[4,5]decano)- and 6-(1-ethoxycarbonylpiperazino)pyrazinocarboxylic acid derivatives</p>

    <p>          Milczarska, Barbara, Foks, Henryk, and Zwolska, Zofia</p>

    <p>          Phosphorus, Sulfur and Silicon and the Related Elements <b>2005</b>.  180(9): 1977-1992</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   59627   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Present and future therapy for hepatitis C virus</p>

    <p>          Cornberg, M, Deterding, K, and Manns, MP</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(5): 781-793</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140355&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140355&amp;dopt=abstract</a> </p><br />

    <p>14.   59628   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro and in vivo antimycobacterial activity of isonicotinoyl hydrazones</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Madhu, Kasinathan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(20): 4502-4505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59629   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Antimicrobial resistance in tuberculosis: an international perspective</p>

    <p>          van Helden, PD, Donald, PR, Victor, TC, Schaaf, HS, Hoal, EG, Walzl, G, and Warren, RM</p>

    <p>          Expert Rev Anti Infect Ther <b>2006</b>.  4(5): 759-766</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140353&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140353&amp;dopt=abstract</a> </p><br />

    <p>16.   59630   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Molecular analysis of cross-resistance to capreomycin, kanamycin, amikacin, and viomycin in Mycobacterium tuberculosis</p>

    <p>          Maus, Courtney E, Plikaytis, Bonnie B, and Shinnick, Thomas M</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(8): 3192-3197</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59631   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Oligosaccharides as inhibitors of mycobacterial arabinosyltransferases. Di- and trisaccharides containing C-3 modified arabinofuranosyl residues</p>

    <p>          Cociorva, Oana M, Gurcha, Sudagar S, Besra, Gurdyal S, and Lowary, Todd L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(4): 1369-1379</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59632   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of some Mannich bases derived from isatin isonicotinoylhydrazone</p>

    <p>          Hussein, Mostafa A, Aboul-Fadl, Tarek, and Hussein, Asmma</p>

    <p>          Bulletin of Pharmaceutical Sciences, Assiut University <b>2005</b>.  28(1): 131-136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.   59633   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          OPC-67683, a nitro-dihydro-imidazooxazole derivative with promising action against tuberculosis in vitro and in mice</p>

    <p>          Matsumoto, M, Hashizume, H, Tomishige, T, Kawasaki, M, Tsubouchi, H, Sasaki, H, Shimokawa, Y, and Komatsu, M</p>

    <p>          PLoS Med <b>2006</b>.  3(11): e466</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132069&amp;dopt=abstract</a> </p><br />

    <p>20.   59634   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Substituted 4H-1,2,4-triazoles and 1,3,4-thiadiazoles derived from 5(4)-methyl-1(3)H-imidazole-4(5)-carboxylic acid hydrazide</p>

    <p>          Bijev, AT</p>

    <p>          Dokladi na Bulgarskata Akademiya na Naukite <b>2005</b>.  58(2): 171-174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59635   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial, and anti-inflammatory activities of novel 2-(1-adamantyl)-5-substituted-1,3,4-oxadiazoles and 2-(1-adamantylamino)-5-substituted-1,3,4-thiadiazoles</p>

    <p>          Kadi, AA, El-Brollosy, NR, Al-Deeb, OA, Habib, EE, Ibrahim, TM, and El-Emam, AA</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17129641&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17129641&amp;dopt=abstract</a> </p><br />

    <p>22.   59636   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Functional phosphoglucose isomerase from Mycobacterium tuberculosis H37Rv: Rapid purification with high yield and purity</p>

    <p>          Mathur, D and Garg, LC</p>

    <p>          Protein Expr Purif <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17126561&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17126561&amp;dopt=abstract</a> </p><br />

    <p>23.   59637   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis, X-ray structure and antimycobacterial activity of silver complexes with alpha-hydroxycarboxylic acids</p>

    <p>          Cuin, A, Massabni, AC, Leite, CQ, Sato, DN, Neves, A, Szpoganicz, B, Silva, MS, and Bortoluzzi, AJ</p>

    <p>          J Inorg Biochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125838&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17125838&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>24.   59638   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Tuberculosis in London a decade and a half of no decline - TB epidemiology and control</p>

    <p>          Anderson, SR, Maguire, H, and Carless, J</p>

    <p>          Thorax <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101738&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101738&amp;dopt=abstract</a> </p><br />

    <p>25.   59639   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Pyrazinoic Acid and its n-Propyl Ester Inhibit Fatty Acid Synthase I in Replicating Tubercle Bacilli</p>

    <p>          Zimhony, O, Vilcheze, C, Arai, M, Welch, J, and Jacobs, WR Jr</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101678&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17101678&amp;dopt=abstract</a> </p><br />

    <p>26.   59640   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of 2-(aminoalkyl)-5-benzimidazolecarboxamide derivatives as peptide deformylase inhibitors</p>

    <p>          Hjelmencrantz, Anders, Cali, Patrizia, Groth, Thomas, Jensen, Christian Eeg, and Naerum, Lars</p>

    <p>          PATENT:  WO <b>2005037272</b>  ISSUE DATE:  20050428</p>

    <p>          APPLICATION: 2004  PP: 73 pp.</p>

    <p>          ASSIGNEE:  (Arpida A/S, Den.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   59641   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of isoxazoles as peptide deformylase inhibitors</p>

    <p>          Cali, Patrizia, Hjelmencrantz, Anders, and Naerum, Lars</p>

    <p>          PATENT:  WO <b>2005026133</b>  ISSUE DATE:  20050324</p>

    <p>          APPLICATION: 2004  PP: 63 pp.</p>

    <p>          ASSIGNEE:  (Combio A/S, Den.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   59642   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Mutation of tlyA confers capreomycin resistance in Mycobacterium tuberculosis</p>

    <p>          Maus, Courtney E, Plikaytis, Bonnie B, and Shinnick, Thomas M</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(2): 571-577</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   59643   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Tyrosine phosphatase MptpA of Mycobacterium tuberculosis inhibits phagocytosis and increases actin polymerization in macrophages</p>

    <p>          Castandet, Jerome, Prost, Jean-Francois, Peyron, Pascale, Astarie-Dequeker, Catherine, Anes, Elsa, Cozzone, Alain J, Griffiths, Gareth, and Maridonneau-Parini, Isabelle</p>

    <p>          Research in Microbiology <b>2005</b>.  156(10): 1005-1013</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   59644   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Regulation of nitrate reductase activity in Mycobacterium tuberculosis by oxygen and nitric oxide</p>

    <p>          Sohaskey, Charles D</p>

    <p>          Microbiology (Reading, United Kingdom) <b>2005</b>.  151(11): 3803-3810</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   59645   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis, characterization and in vitro biological studies of novel cyano derivatives of N-alkyl and N-aryl piperazine</p>

    <p>          Chaudhary, P, Nimesh, S, Yadav, V, Verma, AK, and Kumar, R</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140705&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17140705&amp;dopt=abstract</a> </p><br />

    <p>32.   59646   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Structure of Mycobacterium tuberculosisthioredoxin C</p>

    <p>          Hall, G, Shah, M, McEwan, PA, Laughton, C, Stevens, M, Westwell, A, and Emsley, J</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>2006</b>.  62(Pt 12): 1453-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139080&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17139080&amp;dopt=abstract</a> </p><br />

    <p>33.   59647   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV and antitubercular activities of lamivudine prodrugs</p>

    <p>          Sriram, Dharmarajan, Yogeeswari, Perumal, and Gopal, Gayatri</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(12): 1373-1376</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   59648   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          The Rv0805 Gene from Mycobacterium tuberculosis Encodes a 3&#39;,5&#39;-Cyclic Nucleotide Phosphodiesterase: Biochemical and Mutational Analysis</p>

    <p>          Shenoy, Avinash R, Sreenath, Nandini, Podobnik, Marjetka, Kovacevic, Miroslav, and Visweswariah, Sandhya S</p>

    <p>          Biochemistry <b>2005</b>.  44(48): 15695-15704</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   59649   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Colorimetric redox-indicator methods for the rapid detection of multidrug resistance in Mycobacterium tuberculosis: a systematic review and meta-analysis</p>

    <p>          Martin, A, Portaels, F, and Palomino, JC</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17135182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17135182&amp;dopt=abstract</a> </p><br />

    <p>36.   59650   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Pharmacoproteomic effects of isoniazid, ethambutol, and N-geranyl-N&#39;-(2-adamantyl)ethane-1,2-diamine (SQ109) on Mycobacterium tuberculosis H37Rv</p>

    <p>          Jia, Lee, Coward, Lori, Gorman, Gregory S, Noker, Patricia E, and Tomaszewski, Joseph E</p>

    <p>          Journal of Pharmacology and Experimental Therapeutics <b>2005</b>.  315(2): 905-911</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>37.   59651   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Once-Daily Regimen of Saquinavir, Ritonavir, Didanosine, and Lamivudine in HIV-Infected Patients With Standard Tuberculosis Therapy (TBQD Study)</p>

    <p>          Ribera, Esteban, Azuaje, Carlos, Lopez, Rosa M, Domingo, Pere, Soriano, Alex, Pou, Leonor, Sanchez, Paquita, Mallolas, Josep, Sambea, Maria Antonia, Falco, Vicenc, Ocana, Imma, Lopez-Colomes, Josep Lluis, Gatell, Josep M, and Pahissa, Albert</p>

    <p>          JAIDS, Journal of Acquired Immune Deficiency Syndromes <b>2005</b>.  40(3): 317-323</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   59652   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Comparison of Mycobacterium tuberculosis isocitrate dehydrogenases (ICD-1 and ICD-2) reveals differences in coenzyme affinity, oligomeric state, pH tolerance and phylogenetic affiliation</p>

    <p>          Banerjee, Sharmistha, Nandyala, Ashok, Podili, Raviprasad, Katoch, Viswa M, and Hasnain, Seyed E</p>

    <p>          BMC Biochemistry <b>2005</b>.  6: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   59653   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Glutamine di- and tripeptides and polypeptides having a polyglutamine domain for selective inhibition of proteasomes of tuberculosis and other bacteria</p>

    <p>          Goldberg, Alfred L</p>

    <p>          PATENT:  WO <b>2005094423</b>  ISSUE DATE:  20051013</p>

    <p>          APPLICATION: 2005  PP: 64 pp.</p>

    <p>          ASSIGNEE:  (President and Fellows of Harvard College, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   59654   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Evaluation of antimycobacterial and DNA gyrase inhibition of fluoroquinolone derivatives</p>

    <p>          Sriram, D, Bal, TR, Yogeeswari, P, Radha, DR, and Nagaraja, V</p>

    <p>          J Gen Appl Microbiol <b>2006</b>.  52(4): 195-200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116967&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17116967&amp;dopt=abstract</a> </p><br />

    <p>41.   59655   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of furanyl amides with anti-tuberculosis activity</p>

    <p>          Lee, Richard E, Tangallapally, Rajendra Prasad, Yendapally, Raghunandan, McNeil, Michael, and Lenaerts, Anne</p>

    <p>          PATENT:  US <b>2005222408</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005-44420  PP: 63 pp., Cont.-in-part of U.S. Ser. No. 890,750.</p>

    <p>          ASSIGNEE:  (University of Tennessee Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   59656   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of alkyl 1-heteroaryl-1H-1,2,3-triazole-4-carboxylates</p>

    <p>          Japelj, Bostjan, Recnik, Simon, Cebasek, Petra, Stanovnik, Branko, and Svete, Jurij</p>

    <p>          Journal of Heterocyclic Chemistry <b>2005</b>.  42(6): 1167-1173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>43.   59657   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Synthesis of isonicotinoylhydrazones from anacardic acid and their in vitro activity against Mycobacterium smegmatis</p>

    <p>          Swamy, BN, Suma, TK, Rao, GV, and Reddy, GC</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112641&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112641&amp;dopt=abstract</a> </p><br />

    <p>44.   59658   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Effect of substituents on diarylmethanes for antitubercular activity</p>

    <p>          Panda, G, Parai, MK, Das, SK, Shagufta, Sinha, M, Chaturvedi, V, Srivastava, AK, Manju, YS, Gaikwad, AN, and Sinha, S</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17112639&amp;dopt=abstract</a> </p><br />

    <p>45.   59659   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          The in vitro activity of phenothiazines against Mycobacterium avium: potential of thioridazine for therapy of the co-infected AIDS patient</p>

    <p>          Viveiros Miguel, Martins Marta, Couto Isabel, Kristiansen Jette E, Molnar Joseph, and Amaral Leonard</p>

    <p>          In vivo (Athens, Greece) <b>2006</b>.  19(4): 733-6.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   59660   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Antimicrobial and general toxicity activities of Gymnosperma glutinosum: A comparative study</p>

    <p>          Canales, M, Hernandez, T, Serrano, R, Hernandez, LB, Duran, A, Rios, V, Sigrist, S, Hernandez, HL, Angeles-Lopez, O, Fernandez-Araiza, MA, and Avila, G</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17110067&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17110067&amp;dopt=abstract</a> </p><br />

    <p>47.   59661   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          In vitro study on cross resistance of rifampin and rifapentine for Mycobacterium tuberculosis</p>

    <p>          Duanmu Hong-Jin, Liu Yu-hong, Jiang Guang-lu, Wang Su-min, and Fu Yu-hong</p>

    <p>          Zhonghua jie he he hu xi za zhi = Zhonghua jiehe he huxi zazhi = Chinese journal of tuberculosis and respiratory diseases <b>2006</b>.  28(3): 192-4.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>48.   59662   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Detection of Multi-Drug Resistance in Mycobacterium tuberculosis</p>

    <p>          Sekiguchi, JI, Miyoshi-Akiyama, T, Augustynowicz-Kopec, E, Zwolska, Z, Kirikae, F, Toyota, E, Kobayashi, I, Morita, K, Kudo, K, Kato, S, Kuratsuji, T, Mori, T, and Kirikae, T </p>

    <p>          J Clin Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17108078&amp;dopt=abstract</a> </p><br />

    <p>49.   59663   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Relationship between clinical efficacy of treatment of pulmonary Mycobacterium avium complex disease and drug-sensitivity testing of Mycobacterium avium complex isolates</p>

    <p>          Kobashi, Yoshihiro, Yoshida, Kouichiro, Miyashita, Naoyuki, Niki, Yoshihito, and Oka, Mikio</p>

    <p>          Journal of Infection and Chemotherapy <b>2006</b>.  12(4): 195-202</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.   59664   OI-LS-363; PUBMED-OI-12/11/2006</p>

    <p class="memofmt1-2">          Rv3303c of Mycobacterium tuberculosis protects tubercle bacilli against oxidative stress in vivo and contributes to virulence in mice</p>

    <p>          Akhtar, P, Srivastava, S, Srivastava, A, Srivastava, M, Srivastava, BS, and Srivastava, R</p>

    <p>          Microbes Infect <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17097323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17097323&amp;dopt=abstract</a> </p><br />

    <p>51.   59665   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          ICAT-based comparative proteomic analysis of nonreplicating persistent Mycobacterium tuberculosis</p>

    <p>          Cho, SH, Goodlett, D, and Franzblau, S</p>

    <p>          TUBERCULOSIS <b>2006</b>.  86(6): 445-460, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242066600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242066600006</a> </p><br />

    <p>52.   59666   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of oxindole oxazolidinones as antibacterials</p>

    <p>          Gordeev, Mikhail Fedor, Josyula, Vara Prasad Venkata Nagendra, and Luehr, Gary Wayne</p>

    <p>          PATENT:  WO <b>2006106426</b>  ISSUE DATE:  20061012</p>

    <p>          APPLICATION: 2006  PP: 35pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>53.   59667   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of 7-fluoro-1,3-dihydroindol-2-one oxazolidinones as antibacterials</p>

    <p>          Grodeev, Mikhail Fedor, Josyula, Vara Prasad Venkata Nagendra, and Luehr, Gary Wayne</p>

    <p>          PATENT:  WO <b>2006106427</b>  ISSUE DATE:  20061012</p>

    <p>          APPLICATION: 2006  PP: 35pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>54.   59668   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Crystal structure of uncleaved L-aspartate-alpha-decarboxylase from Mycobacterium tuberculosis</p>

    <p>          Gopalan, G, Chopra, S, Ranganathan, A, and Swaminathan, K</p>

    <p>          PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS <b>2006</b>.  65(4 ): 796-802, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242056500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242056500002</a> </p><br />

    <p>55.   59669   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Pharmaceuticals containing adenosines for prophylactic and therapeutic treatment of cryptosporidiosis</p>

    <p>          Tokoro, Masaharu, Kitade, Yukio, and Nozaki, Tomoyoshi</p>

    <p>          PATENT:  JP <b>2006151849</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2004-15074  PP: 10 pp.</p>

    <p>          ASSIGNEE:  (Kanazawa University, Japan, Gifu University, and National Institute Infectious Diseases)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>56.   59670   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          The key DNA-binding residues in the C-terminal domain of Mycobacterium tuberculosis DNA gyrase A subunit (GyrA)</p>

    <p>          Huang, YY, Deng, JY, Gu, J, Zhang, ZP, Maxwell, A, Bi, LJ, Chen, YY, Zhou, YF, Yu, ZN, and Zhang, XE</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2006</b>.  34(19): 5650-5659, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241955200040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241955200040</a> </p><br />

    <p>57.   59671   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Growth inhibition of Candida species and Aspergillus fumigatus by statins</p>

    <p>          Macreadie, Ian G, Johnson, Georgia, Schlosser, Tanja, and Macreadie, Peter I</p>

    <p>          FEMS Microbiology Letters <b>2006</b>.  262(1): 9-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>58.   59672   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Mechanisms of action of isoniazid</p>

    <p>          Timmins, GS and Deretic, V</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2006</b>.  62(5): 1220-1227, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241832000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241832000002</a> </p><br />

    <p>59.   59673   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Design, synthesis, biological screening, toxicity evaluation and ADME prediction of novel squalene epoxidase inhibitors as antifungal agents</p>

    <p>          Kharkar, Prashant S and Kulkarni, Vithal M</p>

    <p>          Proceedings of the European Symposium on Structure-Activity Relationships (QSAR) and Molecular Modelling <b>2006</b>.  15: 524-525</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>60.   59674   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Selective delivery of rifampicin incorporated into poly (DL-lactic-co-glycolic) acid microspheres after phagocytotic uptake by alveolar macrophages, and the killing effect against intracellular Mycobacterium bovis Calmette-Guerin</p>

    <p>          Yoshida, A, Matumoto, M, Hshizume, H, Oba, Y, Tomishige, T, Inagawa, H, Kohchi, C, Hino, M, Ito, F, Tomoda, K, Nakajima, T, Makino, K, Terada, H, Hori, H, and Soma, GI</p>

    <p>          MICROBES AND INFECTION <b>2006</b>.  8(9-10): 2484-2491, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241992800020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241992800020</a> </p><br />

    <p>61.   59675   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Temporin A and its retro-analogs: synthesis, conformational analysis and antimicrobial activities</p>

    <p>          Kamysz, Wojciech, Mickiewicz, Beata, Rodziewicz-Motowidio, Sylwia, Greber, Katarzyna, and Okroj, Marcin</p>

    <p>          Journal of Peptide Science <b>2006</b>.  12(8): 533-537</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>62.   59676   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Screening for antiyeast activities from selected medicinal plants</p>

    <p>          Darah, I, Jain, K, Suraya, S, Lim, SH, Hazarina, N, and Adnalizawati, ASN</p>

    <p>          JOURNAL OF TROPICAL FOREST SCIENCE <b>2006</b>.  18(4): 231-235, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241913300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241913300004</a> </p><br />
    <br clear="all">

    <p>63.   59677   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Use of 5-hydroxy-1-(4-hydroxyphenyl)-7-phenyl-3-heptanone for preparing anti-candida albicans fungicide</p>

    <p>          Jiang, Linghuo </p>

    <p>          PATENT:  CN <b>1864671</b>  ISSUE DATE: 20061122</p>

    <p>          APPLICATION: 1001-4401  PP: 4pp.</p>

    <p>          ASSIGNEE:  (Tianjin University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>64.   59678   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Antiplatelet, antibacterial and antifungal activities of Achillea falcata extracts and evaluation of volatile oil composition</p>

    <p>          Aburjai, Talal and Hudaib, Mohammad</p>

    <p>          Pharmacognosy Magazine <b>2006</b>.  2(7): 188-195</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>65.   59679   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Pharmacognostical, antibacterial and antifungal potentials of the leaf extracts of Pithecellobium dulce Benth</p>

    <p>          Shanmugakumar, SD, Amerjothy, S, and Balakrishna, K</p>

    <p>          Pharmacognosy Magazine <b>2006</b>.  2(7): 160-164</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>66.   59680   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Antibacterial and antifungal activities of 2-arylidene-4-(4-methylphenyl)but-3-en-4-olides and their pyrrolone derivatives</p>

    <p>          Husain, A, Hasan, SM, Lal, S, and Alam, MM</p>

    <p>          Indian Journal of Pharmaceutical Sciences <b>2006</b>.  68(4): 536-538</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>67.   59681   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          AntiHIV, antibacterial and antifungal activities of some novel 1,4-disubstituted-1,2,4-triazolo[4,3-a] quinazolin-5(4H)-ones</p>

    <p>          Alagarsamy, V, Giridhar, R, Yadav, MR, Revathi, R, Ruckmani, K, and De Clercq, E</p>

    <p>          Indian Journal of Pharmaceutical Sciences <b>2006</b>.  68(4): 532-535</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>68.   59682   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Use of genistein 4&#39;,7-dirhamnoside and genistein 7-rhamnoside having enhanced antifungal activity and no harmful effect on liver cell produced from kitasatospora kifunensis mjm-134</p>

    <p>          Suh, Joo Won, Kim, Jong Woo, Han, Jae Jin, Yang, Young Yell, and Yoon, Tae Mi</p>

    <p>          PATENT:  KR <b>2006055587</b>  ISSUE DATE:  20060524</p>

    <p>          APPLICATION: 2004-28925  PP: No pp. given</p>

    <p>          ASSIGNEE:  (B.N.C. Bio Pharm Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>69.   59683   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Molecular characterization of isoniazid-resistant clinical isolates of Mycobacterium tuberculosis from the USA</p>

    <p>          Guo, HL, Seet, Q, Denkin, S, Parsons, L, and Zhang, Y</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2006</b>.  55(11): 1527-1531, 5</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241931000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241931000009</a> </p><br />
    <br clear="all">

    <p>70.   59684   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis of heterocyclic analogs of 5-(4-methylcarboxamidophenyl)-2-furoic acid as potent antimicrobial agents</p>

    <p>          Dahiya, Rajiv and Pathak, Devender</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2006</b>.  16(1): 53-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>71.   59685   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of triazolylarylindazolylalkanols as medical fungicides</p>

    <p>          Park, Joon Seok, Yu, Kyung A, Jeong, Il Yeong, Kim, Sun Young, and Lee, Bong Yong</p>

    <p>          PATENT:  WO <b>2006109933</b>  ISSUE DATE:  20061019</p>

    <p>          APPLICATION: 2006  PP: 136pp.</p>

    <p>          ASSIGNEE:  (Daewoong Pharmaceutical Co., Ltd. S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>72.   59686   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity of 1,3-diphenylpyrazole and its azomethine derivatives</p>

    <p>          Mohite, SK and Magdum, CS</p>

    <p>          Journal of Microbial World <b>2006</b>.  8(1): 116-118</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>73.   59687   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          The Ser/Thr protein kinase PknB is essential for sustaining mycobacterial growth</p>

    <p>          Fernandez, P, Saint-Joanis, B, Barilone, N, Jackson, M, Gicquel, B, Cole, ST, and Alzari, PM</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2006</b>.  188(22): 7778-7784, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241938100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241938100007</a> </p><br />

    <p>74.   59688   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          A new perspective on Mycobacterial cell wall biosynthesis and the identification of potential drug targets</p>

    <p>          Alderwick, LJ, Seidel, M, Eggeling, L, and Besra, GS</p>

    <p>          GLYCOBIOLOGY <b>2006</b>.  16(11): 1144-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241093300217">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241093300217</a> </p><br />

    <p>75.   59689   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Cytostatic and antiviral 6-arylpurine ribonucleosides VIII+. Synthesis and evaluation of 6-substituted purine 3&#39;-deoxyribonucleosides</p>

    <p>          Hocek, Michal, Silhar, Peter, and Pohl, Radek</p>

    <p>          Collection of Czechoslovak Chemical Communications <b>2006</b>.  71(10): 1484-1496</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>76.   59690   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preliminary report of anti-hepatitis C virus activity of chloroquine and hydroxychloroquine in Huh-5-2 cell line</p>

    <p>          Chandramohan, M, Vivekanathan, SC, Sivakumar, D, Selvam, P, Neyts, J, Katrien, G, and De Clercq, E</p>

    <p>          Indian Journal of Pharmaceutical Sciences <b>2006</b>.  68(4): 538-540</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>77.   59691   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Pyrido[2,3-d]pyrimidines useful as hepatitis C virus (HCV) inhibitors, and methods for the preparation thereof</p>

    <p>          Simmen, Kenneth Alan, Surleraux, Dominique Louis Nestor Ghislain, Lin, Tse-I, Lenz, Oliver, and Raboisson, Pierre Jean-Marie Bernard</p>

    <p>          PATENT:  WO <b>2006120252</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 47pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>78.   59692   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Preparation of 6-hydrazinopurine 2&#39;-methyl ribonucleosides and nucleotides as antiviral agents for treatment of HCV</p>

    <p>          Gunic, Esmir and Rong, Frank</p>

    <p>          PATENT:  WO <b>2006122207</b>  ISSUE DATE:  20061116</p>

    <p>          APPLICATION: 2006  PP: 21pp.</p>

    <p>          ASSIGNEE:  (Valeant Research &amp; Development, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>79.   59693   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Design, synthesis and adenosine kinase activity of triazole nucleoside derivatives as potential antiviral agents</p>

    <p>          Kumarapperuma, Sidath C, Je-elnik, Marjan, Chung, Kiwon, Sun, Yanjie, Parker, William B, and Arterburn, Jeffrey B</p>

    <p>          19th Rocky Mountain Regional Meeting of the American Chemical Society <b>2006</b>.  19: RM-288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>80.   59694   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Recent advances in pegylated interferon antiviral therapy of chronic hepatitis C</p>

    <p>          Coffin, Carla S and Lee, Samuel S</p>

    <p>          Anti-Infective Agents in Medicinal Chemistry <b>2006</b>.  5(4): 379-387</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>81.   59695   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Cycloalkyl, aryl and heteroaryl amino isothiazoles for the treatment of hepatitis C virus infection</p>

    <p>          Gunic, Esmir, Appleby, Todd, Zhong, Weidong, Yan, Shunqi, and Lai, Ching Ha</p>

    <p>          PATENT:  US <b>2006217390</b>  ISSUE DATE:  20060928</p>

    <p>          APPLICATION: 2006-33901  PP: 60pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>82.   59696   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Experimental and modelling studies on antifungal compounds</p>

    <p>          Doble, Mukesh and Kumar, KAnil</p>

    <p>          Central European Journal of Chemistry <b>2006</b>.  4(3): 428-439</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>83.   59697   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          New rifabutin analogs: Synthesis and biological activity against Mycobacterium tuberculosis</p>

    <p>          Barluenga, J, Aznar, F, Garcia, AB, Cabal, MP, Palacios, JJ, and Menendez, MA</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(22): 5717-5722, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241850400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241850400001</a> </p><br />
    <br clear="all">

    <p>84.   59698   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of substituted arylthioureas</p>

    <p>          Gupta, MK, Sachan, AK, Pandeya, SN, and Gangwar, VS</p>

    <p>          Asian J. Chem.  <b>2006</b>.  18(4): 2959-2962</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>85.   59699   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Naphtho[2,3-b][1,4]-thiazine-5,10-diones and 3-substituted-1,4-dioxo-1,4-dihydronaphthale -2-yl-thioalkanoate derivatives: Synthesis and biological evaluation as potential antibacterial and antifungal agents</p>

    <p>          Tandon, VK, Maurya, HK, Yadav, DB, Tripathi, A, Kumar, M, and Shukla, PK</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(22): 5883-5887, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241850400036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241850400036</a> </p><br />

    <p>86.   59700   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis of 1,5-benzothiazepines: part XXXII - synthesis and antimicrobial studies of 8-substituted-2,5-dihydro-2-(4-pyridyl)-4-(2-thienyl)-1,5-benzothiazepines</p>

    <p>          Pant, Seema, Chandra, Hem, Sharma, Priyanka, and Pant, Umesh C</p>

    <p>          Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry <b>2006</b>.  45B(6): 1525-1530</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>87.   59701   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Syntheses of 1,5-benzothiazepines. Part XXIX. Syntheses and antimicrobial studies of 10-substituted-6a,7-dihydro-6H-7-(4-ethoxyphenyl)-6-phenyl[1]benzopyrano[3,4-c][1,5]benzothiazepines</p>

    <p>          Pant, Seema, Sharma, Priyanka, Chandra, Hem, and Pant, Umesh C</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2006</b>.  15(3): 289-290</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>88.   59702   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis of 1,5-benzothiazepines. Part XXX. Synthesis and antimicrobial studies of 10-substituted 6a,7-dihydro-6H-7-(4-fluorophenyl)-6-phenyl[1]benzopyrano[3,4-c][1,5]benzothiazepines</p>

    <p>          Pant, Umesh C, Chandra, Hem, Goyal, Shweta, Sharma, Priyanka, and Pant, Seema</p>

    <p>          Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry <b>2006</b>.  45B(3): 752-757</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>89.   59703   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Metal-based isatin-bearing sulfonamides: their synthesis, characterization and biological properties</p>

    <p>          Chohan, ZH, Shaikh, AU, and Naseer, MM</p>

    <p>          APPLIED ORGANOMETALLIC CHEMISTRY <b>2006</b>.  20(11): 729-739, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241999700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241999700002</a> </p><br />

    <p>90.   59704   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Peptide deformylase inhibitors as potent antimycobacterial agents</p>

    <p>          Teo, JWP, Thayalan, P, Beer, D, Yap, ASL, Nanjundappa, M, Ngew, X, Duraiswamy, J, Liung, S, Dartois, V, Schreiber, M, Hasan, S, Cynamon, M, Ryder, NS, Yang, X, Weidmann, B, Bracken, K, Dick, T, and Mukherjee, K</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(11): 3665-3673, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241935200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241935200020</a> </p><br />
    <br clear="all">

    <p>91.   59705   OI-LS-363; SCIFINDER-OI-12/4/2006</p>

    <p class="memofmt1-2">          Synthesis, characterization, spectral studies and antifungal activity of Mn(II), Fe(II), Co(II), Ni(II), Cu(II) and Zn(II) complexes with 3,3&#39;-bis[N,N,di(carboxymethyl)-aminomethyl]-o-cresol sulphonphthalein</p>

    <p>          Pandey, Gajanan and Narang, Krishan K</p>

    <p>          Journal of Coordination Chemistry <b>2006</b>.  59(13): 1495-1507</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>92.   59706   OI-LS-363; WOS-OI-12/04/2006</p>

    <p class="memofmt1-2">          Enhancement of the fungicidal activity of amphotericin B by allicin, an allyl-sulfur compound from garlic, against the yeast Saccharomyces cerevisiae as a model system</p>

    <p>          Ogita, A, Fujita, K, Taniguchi, M, and Tanaka, T</p>

    <p>          PLANTA MEDICA <b>2006</b>.  72(13): 1247-1250, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241876600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241876600016</a> </p><br />

    <p>93.   59707   OI-LS-363; WOS-OI-12/11/2006</p>

    <p class="memofmt1-2">          Trends in tuberculosis treatment duration</p>

    <p>          Veziris, N, Aubry, A, and Truffot-Pernot, C</p>

    <p>          PRESSE MEDICALE <b>2006</b>.  35(11): 1758-1764, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242164600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242164600015</a> </p><br />

    <p>94.   59708   OI-LS-363; WOS-OI-12/11/2006</p>

    <p class="memofmt1-2">          Toxoplasmosis</p>

    <p>          Dodds, EM</p>

    <p>          CURRENT OPINION IN OPHTHALMOLOGY <b>2006</b>.  17(6): 557-561, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242091500012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242091500012</a> </p><br />

    <p>95.   59709   OI-LS-363; WOS-OI-12/11/2006</p>

    <p class="memofmt1-2">          Sulfonylurea is a non-competitive inhibitor of acetohydroxyacid synthase from Mycobacterium tuberculosis</p>

    <p>          Choi, KJ, Noh, KM, Choi, JD, Park, JS, Won, HS, Kim, JR, Kim, JS, and Yoon, MY</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2006</b>.  27(10): 1697-1700, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242123000041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242123000041</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
