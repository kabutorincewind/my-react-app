

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-09-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lifrBvO4HnzbxFCOIaIeiO4J78lKvjw6RldGtXrh4NxEePJFMdkR3MFcQLR/kciMc98G/D4bzUPJFwjbe3di/fQ7RxT289SDfPdwdMAgogudKnvX1R5OHr8SGAE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A6B46E5A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">

<h1 class="memofmt2-h1">Opportunistic Infections Citation List:</h1>

<h1 class="memofmt2-h1">September 16 – September 29, 2011</h1>

<p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

<p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21932255">Efficient Regioselective Three-Component Domino Synthesis of 3-(1,2,4-Triazol-5-yl)-1,3-thiazolidin-4-ones as Potent Antifungal and Antituberculosis Agents.</a>El Bialy, S.A., M.M. Nagy, and H.M. Abdel-Rahman, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21932255]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21851100">Lysozyme Possesses Novel Antimicrobial Peptides within Its N-terminal Domain that Target Bacterial Respiration.</a> Ibrahim, H.R., K. Imazato, and H. Ono, Human Journal of Agricultural and Food Chemistry, 2011. 59(18): p. 10336-45; PMID[21851100]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>  
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21924600">Comparison between Allicin and fluconazole in Candida albicans Biofilm Inhibition and in Suppression of HWP1 Gene Expression.</a> Khodavandi, A., N.S. Harmal, F. Alizadeh, O.J. Scully, S.M. Sidik, F. Othman, Z. Sekawi, K.P. Ng, and P.P. Chong, Phytomedicine : international journal of phytotherapy and phytopharmacology, 2011. <b>[Epub ahead of print]</b>; PMID[21924600]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21821413">Synthesis of Rhodamine B-benzenesulfonamide Conjugates and Their Inhibitory Activity against Human Alpha- and Bacterial/Fungal Beta-carbonic anhydrases.</a> Rami, M., A. Innocenti, J.L. Montero, A. Scozzafava, J.Y. Winum, and C.T. Supuran, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5210-3; PMID[21821413]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21937186">In Vitro Activity of Isavuconazole Against 208 Aspergillus flavus Isolates in Comparison with 7 Other Antifungal Agents: Assessment According to the Methodology of the European Committee on Antimicrobial Susceptibility Testing.</a> Rudramurthy, S.M., A. Chakrabarti, E. Geertsen, J.W. Mouton, and J.F. Meis, Diagnostic Microbiology and Infectious Disease, 2011. <b>[Epub ahead of print]</b>; PMID[21937186]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21939347">In Vitro Antifungal Activity of DNA Topoisomerase Inhibitors.</a> Steverding, D., P. Evans, L. Msika, B. Riley, J. Wallington, and S. Schelenz, Medical mycology : official publication of the International Society for Human and Animal Mycology, 2011. <b>[Epub ahead of print]</b>; PMID[21939347]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21802287">Yojironins E-I,Pprenylated acylphloroglucinols from Hypericum yojiroanum.</a> Tanaka, N., T. Mamemura, A. Shibazaki, T. Gonoi, and J. Kobayashi, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(18): p. 5393-7; PMID[21802287]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21936887">The in-Vitro Evaluation of Antibacterial, Antifungal and Cytotoxic Properties of Marrubium vulgare L. Essential Oil Grown in Tunisia.</a> Zarai, Z., A. Kadri, I. Ben Chobba, R. Ben Mansour, A. Bekir, H. Mejdoub, and N. Gharsallah, Lipids in Health and Disease, 2011. 10(1): p. 161; PMID[21936887]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 

<p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,&nbsp; Drug Resistant, MAC, MDR)</p>  

<p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21933353">Synthesis and Antimycobacterial Activity of Novel Amino alcohols Containing Central Core of the anti-HIV Drugs Lopinavir and Ritonavir.</a> Gomes, C.R., M. Moreth, D. Cardinot, V. Kopke, W. Cunico, M.C. da Silva Lourenco, and M.V. de Souza, Chemical Biology &amp; Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21933353]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21704310">Design and Synthesis of Novel Cell Wall Inhibitors of Mycobacterium tuberculosis GlmM and GlmU.</a> Li, Y., Y. Zhou, Y. Ma, and X. Li, Carbohydrate research, 2011. 346(13): p. 1714-20; PMID[21704310]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>  
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21859082">Stereochemical Analysis of Leubethanol, an Anti-TB-Active Serrulatane, from Leucophyllum frutescens.</a> Molina-Salinas, G.M., V.M. Rivas-Galindo, S. Said-Fernandez, D.C. Lankin, M.A. Munoz, P. Joseph-Nathan, G.F. Pauli, and N. Waksman, Journal of Natural Products, 2011. 74(9): p. 1842-1850; PMID[21859082]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>  
<p class="plaintext">&nbsp;</p>

<p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21714959">Synthesis and Biological Evaluation of Sugar-derived Chiral Nitroimidazoles as Potential Antimycobacterial Agents.</a> Mugunthan, G., D. Sriram, P. Yogeeswari, and K.P. Ravindranathan Kartha, Carbohydrate research, 2011. 346(13): p. 1760-6; PMID[21714959]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21936546">Synthesis of Functionalized Cinnamaldehyde Derivatives by an Oxidative Heck Reaction and Their Use as Starting Materials for Preparation of Mycobacterium tuberculosis 1-Deoxy-D-xylulose-5-phosphate Reductoisomerase Inhibitors.</a> Nordqvist, A., C. Bjorkelid, M. Andaloussi, A.M. Jansson, S.L. Mowbray, A. Karlen, and M. Larhed, The Journal of organic chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21936546]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>

<p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

<p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21846091">Antimalarial beta-Carbolines from the New Zealand Ascidian Pseudodistoma opacum.</a> Chan, S.T., A.N. Pearce, M.J. Page, M. Kaiser, and B.R. Copp, Journal of Natural Products, 2011. 74(9): p. 1972-1979; PMID[21846091]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>

<p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21932256">Facile Synthesis and In-Vitro Antimalarial Activity of Novel alpha-Hydroxy Hydrazonates.</a> Khankischpur, M., F.K. Hansen, R. Meurer, T. Mauz, B. Bergmann, R.D. Walter, and D. Geffken, Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21932256]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21937493">Harmonine, a Defence Compound from the Harlequin Ladybird, Inhibits Mycobacterial Growth and Demonstrates Multi-stage Antimalarial Activity.</a> Rohrich, C.R., C.J. Ngwa, J. Wiesner, H. Schmidtberg, T. Degenkolb, C. Kollewe, R. Fischer, G. Pradel, and A. Vilcinskas, Biology letters, 2011. Epub ahead of print; PMID[21937493]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21933377">Discovery of Potent, Novel, Non-toxic Anti-malarial Compounds via Quantum Modelling, Virtual Screening and in Vitro Experimental Validation.</a> Sullivan, D.J., Jr., N. Kaludov, and M.N. Martinov, Malaria journal, 2011. 10(1): p. 274; PMID[21933377]. <br /> <b>[PubMed]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21935647">Sensitivity of Plasmodium vivax to Chloroquine, Mefloquine, Artemisinin and Atovaquone in North-western Thailand.</a> Treiber, M., G. Wernsdorfer, U. Wiedermann, K. Congpuong, J. Sirichaisinthop, and W.H. Wernsdorfer, Wiener klinische Wochenschrift, 2011. <b>[Epub ahead of print]</b>; PMID[21935647]. <b>[PubMed]</b>. OI_0916-092911. </p>

<p class="memofmt2-2">Citations from the ISI Web of&nbsp; Knowledge Listings for O.I.</p>

<p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294613400014">Phytochemical Screening and Antimicrobial Potential of Andrographis ovata Clarke.</a> Alagesaboopathi, C., African Journal of Biotechnology, 2011. 10(25): p. 5033-5036; PMID[WOS:000294613400014] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294860000015">In Vitro and In Vivo Antifungal Activity of 5-Phenylthio-2,4-Bisbenzyloxypyrimidine: A Novel Nucleobase.</a> Amareshwar, V., S.J. Patil, and N.M. Goudgaon, Synthesis, Indian Journal of Pharmaceutical Sciences, 2011. 72(6): p. 778-U113; PMID[WOS:000294860000015]&nbsp; <br /><b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294674900015">Antimicrobial Potentials of Eclipta alba by Disc Diffusion Method.</a>&nbsp; Bakht, J., A. Islam, H. Ali, M. Tayyab, and M. Shafi, African Journal of Biotechnology, 2011. 10(39): p. 7658-7667; PMID[WOS:000294674900015] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">22. <a  href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294711100031">The Structure-Activity Relationship of Urea Derivatives as Anti-tuberculosis Agents.</a> Brown, J.R., E.J. North, J.G. Hurdle, C. Morisseau, J.S. Scarborough, D.Q. Sun, J. Kordulakova, M.S. Scherman, V. Jones, A. Grzegorzewicz, R.M. Crew, M. Jackson, M.R. McNeil, and R.E. Lee, Bioorganic &amp; Medicinal Chemistry, 2011. 19(18): p. 5585-5595; PMID[WOS:000294711100031] <br /><b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>

<p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294766900018">Antioxidant, Antihypertensive and Antimicrobial Properties of Ovine Milk Caseinate Hydrolyzed with a Microbial Protease.</a> Correa, A.P.F., D.J. Daroit, J. Coelho, S.M.M. Meira, F.C. Lopes, J. Segalin, P.H. Risso, and A. Brandelli, Journal of the Science of Food and Agriculture, 2011. 91(12): p. 2247-2254; PMID[WOS:000294766900018] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>

<p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294572700005">Synthesis, Characterization, and Derivatization of Some Novel Types of Fluorinated mono- and bis-Imidazolidineiminothiones with Antitumor, Antiviral, Antibacterial, and Antifungal Activities.</a> El-Sharief, M., Z. Moussa, and A.M.S. El-Sharief, Journal of Fluorine Chemistry, 2011. 132(9): p. 596-611; PMID[WOS:000294572700005] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294652100006">Synthesis of Some Triazolophthalazine Derivatives for Their Anti-Inflammatory and Antimicrobial Activities.</a> Habib, N.S., A.M. Farghaly, F.A. Ashour, A.A. Bekhit, H.A. Abd El Razik, and T. Abd El Azeim, Archiv Der Pharmazie, 2011. 344(8): p. 530-542; PMID[WOS:000294652100006] <br /><b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294603100010">In Vitro Antimicrobial Activities of Chloroformic, Hexane and Ethanolic Extracts of Citrullus lanatus var. citroides (Wild Melon).</a> Hassan, L.E.A., H.M. Sirat, S.M.A. Yagi, W.S. Koko, and S.I. Abdelwahab, Journal of Medicinal Plants Research, 2011. 5(8): p. 1338-1344; PMID[WOS:000294603100010] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294711100033">Synthesis, Antitubercular Activity, and SAR Study of N-Substituted-phenylamino-5-methyl-1H-1,2,3-triazole-4-carbohydrazides.</a> Jordao, A.K., P.C. Sathler, V.F. Ferreira, V.R. Campos, M. de Souza, H.C. Castro, A. Lannes, A. Lourenco, C.R. Rodrigues, M.L. Bello, M.C.S. Lourenco, G.S.L. Carvalho, M.C.B. Almeida, and A.C. Cunha, Bioorganic &amp; Medicinal Chemistry, 2011. 19(18): p. 5605-5611; PMID[WOS:000294711100033] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>

<p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294767900030">Antimicrobial Activity of Essential Oil of Salvia officinalis L. Collected in Syria.</a> Khalil, R. and Z.G. Li, African Journal of Biotechnology, 2011. 10(42): p. 8397-8402; PMID[WOS:000294767900030] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294600300016">Synthesis and in Vitro Antimalarial and Antitubercular Activity of Gold(III) Complexes Containing Thiosemicarbazone Ligands.</a> Khanye, S.D., B.J. Wan, S.G. Franzblau, J. Gut, P.J. Rosenthal, G.S. Smith, and K. Chibale, Journal of Organometallic Chemistry, 2011. 696(21): p. 3392-3396; PMID[WOS:000294600300016] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294844900001">In Vitro Cytotoxicity of Two Novel Oral Formulations of Amphotericin B (iCo-009 and iCo-010) against Candida albicans, Human Monocytic and Kidney Cell Lines.</a> Leon, C.G., J. Lee, K. Bartlett, P. Gershkovich, E.K. Wasan, J.Y. Zhao, J.G. Clement, and K.M. Wasan, Lipids in Health and Disease, 2011. 10; PMID[WOS:000294844900001] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>

<p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294572800023">Antifungal Activity of Novel Synthetic Peptides by Accumulation of Reactive Oxygen Species (ROS) and Disruption of Cell Wall against Candida albicans.</a> Maurya, I.K., S. Pathak, M. Sharma, H. Sanwal, P. Chaudhary, S. Tupe, M. Deshpande, V.S. Chauhan, and R. Prasad, Peptides, 2011. 32(8): p. 1732-1740; PMID[WOS:000294572800023] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294661600005">Antifungal Activity of Root, Bark, Leaf and Soil Extracts of Androstachys johnsonii Prain.</a> Molotja, G.M., M.H. Ligavha-Mbelengwa, and R.B. Bhat, African Journal of Biotechnology, 2011. 10(30): p. 5725-5727; PMID[WOS:000294661600005] <br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294603100014">Antioxidant and Antifungal Activity of Some Aromatic Plant Extracts.</a> Ozcan, M.M. and F.Y. Al Juhaimi, Journal of Medicinal Plants Research, 2011. 5(8): p. 1361-1366; PMID[WOS:000294603100014] <br /><b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294586100024">Synthesis, Characterization and Biological Evaluation of a Novel Series of Mixed Ligand Complexes of Lanthanides(III) with 4 N-(Furfural)amino antipyrine Semicarbazone as Primary Ligand and Diphenyl Sulfoxide as Secondary Ligand.</a>&nbsp;Prasad, S., R.K. Agarwal, and A. Kumar, Journal of the Iranian Chemical Society, 2011. 8(3): p. 825-839; PMID[WOS:000294586100024]<br /> <b>[WOS]</b>. OI_0916-092911.</p>
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294589200004">Antimicrobial Activity of Mangrove Plant (Lumnitzera littorea).</a> Saad, S., M. Taher, D. Susanti, H. Qaralleh, and N. Rahim, Asian Pacific Journal of Tropical Medicine, 2011. 4(7): p. 523-525; PMID[WOS:000294589200004] <br /> <b>[WOS]</b>. OI_0916-092911.</p>  
<p class="plaintext">&nbsp;</p> 

<p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294651800018">Screening of Berberis aristata DC. for Antimicrobial Potential against the Pathogens Causing Ear Infection.</a> Sharma, C., K.R. Aneja, and R. Kasera, International Journal of Pharmacology, 2011. 7(4): p. 536-541; PMID[WOS:000294651800018]. <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>  

<p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294718600007">Synthesis and Antituberculous Activity of Quinoline isosteres of Isoniazid.</a> Zimichev, A.V., M.N. Zemtsova, A.G. Kashaev, and Y.N. Klimochkin, Pharmaceutical Chemistry Journal, 2011. 45(4): p. 217-219; PMID[WOS:000294718600007] <br /> <b>[WOS]</b>. OI_0916-092911.</p> 
<p class="plaintext">&nbsp;</p>  

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
