

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-07-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CEMuaEpy6aQolof5JLSwceTO+MWmwpLT/2RCDecUyoM1arip78H8YkyHW3Xn2+IGclk1j/DmogAWdA6axFHdSxKRwRnRh2e8ab1t+lOaoXCvb2N0aaL5cgDsaiE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2958AB21" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 4 - July 17, 2014</h1>

    <h2>ANTIPROTOZOAL COMPOUNDS (incl. pneumocystis, cryptosporidium, toxoplasma, microspridia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25010466">1,4-Disubstituted thiosemicarbazide Derivatives are Potent Inhibitors of Toxoplasma gondii Proliferation.</a> Dzitko, K., A. Paneth, T. Plech, J. Pawelczyk, P. Staczek, J. Stefanska, and P. Paneth. Molecules, 2014. 19(7): p. 9926-9943. PMID[25010466].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24856062">Synthesis and Antituberculosis Activity of Novel 5-Styryl-4-(hetero)aryl-pyrimidines via Combination of the Pd-Catalyzed Suzuki Cross-Coupling and Sn(H) Reactions.</a> Kravchenko, M.A., E.V. Verbitskiy, I.D. Medvinskiy, G.L. Rusinov, and V.N. Charushin. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 3118-3220. PMID[24856062].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24856305">Novel Synthetic Route for Antimalarial Benzo[a]phenoxazine Derivative SSJ-183 and Two Active Metabolites.</a> Mizukawa, Y., J.F. Ge, A. Bakar Md, I. Itoh, C. Scheurer, S. Wittlin, R. Brun, H. Matsuoka, and M. Ihara. Bioorganic &amp; Medicinal Chemistry, 2014. 22(14): p. 3749-3752. PMID[24856305].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24892980">Synthesis and Biological Activities of Novel Pleuromutilin Derivatives with a Substituted Thiadiazole Moiety as Potent Drug-resistant Bacteria Inhibitors.</a> Shang, R., X. Pu, X. Xu, Z. Xin, C. Zhang, W. Guo, Y. Liu, and J. Liang. Journal of Medicinal Chemistry, 2014. 57(13): p. 5664-5678. PMID[24892980].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24894561">Synthesis of New Verapamil Analogues and Their Evaluation in Combination with Rifampicin against Mycobacterium tuberculosis and Molecular Docking Studies in the Binding Site of Efflux Protein Rv1258c.</a> Singh, K., M. Kumar, E. Pavadai, K. Naran, D.F. Warner, P.G. Ruminski, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 2985-2990. PMID[24894561].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24878196">Theophylline-7-acetic acid Derivatives with Amino acids as Anti-tuberculosis Agents.</a> Voynikov, Y., V. Valcheva, G. Momekov, P. Peikov, and G. Stavrakov. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 3043-3045. PMID[24878196].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24882676">Novel 3-Arylfuran-2(5H)-one-fluoroquinolone Hybrid: Design, Synthesis and Evaluation as Antibacterial Agent.</a> Wang, X.D., W. Wei, P.F. Wang, Y.T. Tang, R.C. Deng, B. Li, S.S. Zhou, J.W. Zhang, L. Zhang, Z.P. Xiao, H. Ouyang, and H.L. Zhu. Bioorganic &amp; Medicinal Chemistry, 2014. 22(14): p. 3620-3628. PMID[24882676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24995776">Activity of Ocimum basilicum, Ocimum canum, and Cymbopogon citratus Essential Oils against Plasmodium falciparum and Mature-stage Larvae of Anopheles funestus S.S.</a> Akono Ntonga, P., N. Baldovini, E. Mouray, L. Mambu, P. Belong, and P. Grellier. Parasite, 2014. 21: p. 33. PMID[24995776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24906512">Antiplasmodial Activity of New 4-Aminoquinoline Derivatives against Chloroquine Resistant Strain.</a> Sinha, M., V.R. Dola, P. Agarwal, K. Srivastava, W. Haq, S.K. Puri, and S.B. Katti. Bioorganic &amp; Medicinal Chemistry, 2014. 22(14): p. 3573-3586. PMID[24906512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0704-071714.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337101400003">In Vitro Evaluation of Antimicrobial Features of Vasopressors.</a> Bostan, H., Y. Tomak, S.A. Karaoglu, B. Erdivanli, and V. Hanci. Revista Brasileira de Anestesiologia, 2014. 64(2): p. 84-88. ISI[000337101400003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">11.<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337063700006">In Vitro Antimicrobial Activity and Characterization of Mangrove Isolates of Streptomycetes Effective against Bacteria and Fungi of Nosocomial Origin.</a> Das, A., S. Bhattacharya, A.Y.H. Mohammed, and S.S. Rajan. Brazilian Archives of Biology and Technology, 2014. 57(3): p. 349-356. ISI[000337063700006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337105100014">Synthesis and Biological Activity of Thiazole Dithiocarbamate Derivatives.</a> Gundogdu-Karaburun, N. Letters in Drug Design &amp; Discovery, 2014. 11(6): p. 814-823. ISI[000337105100014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337323900001">The Antimicrobial Effects of Selenium Nanoparticle-enriched Probiotics and Their Fermented Broth against Candida albicans.</a> Kheradmand, E., F. Rafii, M.H. Yazdi, A.A. Sepahi, A.R. Shahverdi, and M.R. Oveisi. Daru-Journal of Pharmaceutical Sciences, 2014. 22(48): p. 6. ISI[000337323900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336800200029">Synthesis and Antitubercular Activity of Berberine Derivatives.</a> Mahapatra, A., V. Maheswari, N.P. Kalia, V.S. Rajput, and I.A. Khan. Chemistry of Natural Compounds, 2014. 50(2): p. 321-325. ISI[000336800200029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336960700044">Stem Wood and Bark Extracts of Delonix regia (Boj. Ex. Hook): Chemical Analysis and Antibacterial, Antifungal, and Antioxidant Properties.</a> Salem, M.Z.M., A. Abdel-Megeed, and H.M. Ali. Bioresources, 2014. 9(2): p. 2382-2395. ISI[000336960700044].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337160500033">Isolation and Identification of Antitrypanosomal and Antimycobacterial Active Steroids from the Sponge Haliclona simulans.</a> Viegelmann, C., J. Parker, T. Ooi, C. Clements, G. Abbott, L. Young, J. Kennedy, A.D.W. Dobson, and R. Edrada-Ebel. Marine Drugs, 2014. 12(5): p. 2937-2952. ISI[000337160500033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337017600036">Chemical Composition and Antifungal Activity of Essential Oils from Ocimum Species.</a> Vieira, P.R.N., S.M. de Morais, F.H.Q. Bezerra, P.A.T. Ferreira, I.R. Oliveira, and M.G.V. Silva. Industrial Crops and Products, 2014. 55: p. 267-271. ISI[000337017600036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000336819400020">Flavonoids and Stilbenoids from Derris eriocarpa.</a> Zhang, H.X., P.K. Lunga, Z.J. Li, Q. Dai, and Z.Z. Du. Fitoterapia, 2014. 95: p. 147-153. ISI[000336819400020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0704-071714.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
