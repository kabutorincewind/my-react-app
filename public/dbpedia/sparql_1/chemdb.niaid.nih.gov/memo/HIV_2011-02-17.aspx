

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-02-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="S7017tfVUDN3NcfTEaR2gFOXOPAsK1+vd3CbFgMMsUm6yL6AQjCeYSMOiPAiMyzy+cK2yutbsQXfSwgq685XTCzWQEa2q5Y/xveUojxcO2Rp9sPkBx2Kxaprv7g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A5730011" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 4, 2011- February 17, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21256010">HIV-1 protease inhibition potential of functionalized polyoxometalates.</a> Flutsch, A., T. Schroeder, M.G. Grutter, and G.R. Patzke. Bioorganic and Medicinal Chemistry Letters, 2011. 21(4): p. 1162-1166; PMID[21256010].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21314946">Differences in the mannose oligomer specificities of the closely related lectins from Galanthus nivalis and Zea mays strongly determine their eventual anti-HIV activity.</a> Hoorelbeke, B., E.J. Van Damme, P. Rouge, D. Schols, K. Van Laethem, E. Fouquaert, and J. Balzarini. Retrovirology, 2011. 8(1): p. 10; PMID[21314946].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21098485">In Vitro Selection and Characterization of HIV-1 Variants with Increased Resistance to Sifuvirtide, a Novel HIV-1 Fusion Inhibitor.</a> Liu, Z., M. Shan, L. Li, L. Lu, S. Meng, C. Chen, Y. He, S. Jiang, and L. Zhang. Journal of Biological Chemistry, 2011. 286(5): p. 3277-3287; PMID[21098485].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21256008">Spirodiketopiperazine-based CCR5 antagonist: Discovery of an antiretroviral drug candidate,</a> Nishizawa, R., T. Nishiyama, K. Hisaichi, C. Minamoto, N. Matsunaga, Y. Takaoka, H. Nakai, S. Jenkinson, W.M. Kazmierski, H. Tada, K. Sagawa, S. Shibayama, D. Fukushima, K. Maeda, and H. Mitsuya. Bioorganic and Medicinal Chemistry Letters, 2011. 21(4): p. 1141-1145; PMID[21256008].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21288305">Some Binding-Related Drug Properties are Dependent on Thermodynamic Signature.</a> Schon, A., N. Madani, A.B. Smith, J.M. Lalonde, and E. Freire. Chemical Biology and Drug Design, 2011. 77(3): p. 161-165; PMID[21288305].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21312334">Azamacrocyclic Metal Complexes as CXCR4 Antagonists.</a> Tanaka, T., T. Narumi, T. Ozaki, A. Sohma, N. Ohashi, C. Hashimoto, K. Itotani, W. Nomura, T. Murakami, N. Yamamoto, and H. Tamamura. ChemMedChem, 2011.  <b>[Epub ahead of print]</b>; PMID[21312334].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21306110">HIV-1 Integrase and Neuraminidase Inhibitors from Alpinia zerumbet.</a> Upadhyay, A., J. Chompoo, W. Kishimoto, T. Makise, and S. Tawata. Journal of Agricultural and Food Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21306110].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21275048">Fragment-based design of ligands targeting a novel site on the integrase enzyme of human immunodeficiency virus 1.</a> Wielens, J., S.J. Headey, J.J. Deadman, D.I. Rhodes, M.W. Parker, D.K. Chalmers, and M.J. Scanlon. ChemMedChem, 2011. 6(2): p. 258-261; PMID[21275048].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21322110">Synthesis and Biological Evaluation of 6-Substituted 5-Alkyl-2-(phenylaminocarbonylmethylthio)pyrimidin-4(3H)-ones as Potent HIV-1 NNRTIs.</a> Yu, M., Z. Li, S. Liu, E. Fan, C. Pannecouque, E. De Clercq, and X. Liu. ChemMedChem, 2011.  <b>[Epub ahead of print]</b>; PMID[21322110].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21298684">Isolation and characterization of a novel thermostable lectin from the wild edible mushroom Agaricus arvensis.</a> Zhao, J.K., Y.C. Zhao, S.H. Li, H.X. Wang, and T.B. Ng. Journal of Basic Microbiology, 2011.  <b>[Epub ahead of print]</b>; PMID[21298684].</p>

    <p class="plaintext"><b>[Pubmed]</b>, HIV_0204-021711.</p>

    <p> </p> 

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285780000004">Inhibitors from Natural Products to HIV-1 Reverse Transcriptase, Protease and Integrase.</a> Jiang, Y., T.B. Ng, C.R. Wang, D. Zhang, Z.H. Cheng, Z.K. Liu, W.T. Qiao, Y.Q. Geng, N. Li, and F. Liu. Mini-Reviews in Medicinal Chemistry, 2010. 10(14): p. 1331-1344; ISI[000285780000004].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283938400001">Selective killing of human immunodeficiency virus infected cells by non-nucleoside reverse transcriptase inhibitor-induced activation of HIV protease.</a> Jochmans, D., M. Anders, I. Keuleers, L. Smeulders, H.G. Krausslich, G. Kraus, and B. Muller. Retrovirology, 2010. 7; ISI[000283938400001].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285712500004">Bioactive Dehydrotyrosyl and Dehydrodopyl Compounds of Marine Origin.</a> Sugumaran, M. and W.E. Robinson. Marine Drugs, 2010. 8(12): p. 2906-2935; ISI[000285712500004].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286306200013">N-3 Hydroxylation of Pyrimidine-2,4-diones Yields Dual Inhibitors of HIV Reverse Transcriptase and Integrase.</a> Tang, J., K. Maddali, C.D. Dreis, Y.Y. Sham, R. Vince, Y. Pommier, and Z.Q. Wang. ACS Medicinal Chemistry Letters, 2011. 2(1): p. 63-67; ISI[000286306200013].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286385600033">AIDS Drugs that prevent HIV infection.</a> Wainberg, M.A. Nature, 2011. 469(7330): p. 306-307; ISI[000286385600033].</p>

    <p class="plaintext"><b>[WOS]</b>, HIV_0204-021711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
