

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-01-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fKMp5F1qVtR+hiO3w6UrAcW0+mjrfiPSnryVfXmxh84mf9lRnMDg9XULF0LFilmD9XIPgPLhLFVEcBkyCIxbiIiZ9jSTQQUPyl9qdN8mQ0q6mwhejSMQJ0VoO0c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="474A86DF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 16 - January 29, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25461891">Design, Synthesis, and Bioevaluation of Paeonol Derivatives as Potential anti-HBV Agents.</a> Huang, T.J., H. Chuang, Y.C. Liang, H.H. Lin, J.C. Horng, Y.C. Kuo, C.W. Chen, F.Y. Tsai, S.C. Yen, S.C. Chou, and M.H. Hsu. European Journal of Medicinal Chemistry, 2015. 90: p. 428-435. PMID[25461891].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25490700">Hydroxamic acids Block Replication of Hepatitis C Virus.</a> Ai, T., Y. Xu, L. Qiu, R.J. Geraghty, and L. Chen. Journal of Medicinal Chemistry, 2015. 58(2): p. 785-800. PMID[25490700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25483263">New 1-Phenyl-5-(1H-pyrrol-1-yl)-1H-pyrazole-3-carboxamides Inhibit Hepatitis C Virus Replication via Suppression of Cyclooxygenase-2.</a> Manvar, D., S. Pelliccia, G. La Regina, V. Famiglini, A. Coluccia, A. Ruggieri, S. Anticoli, J.C. Lee, A. Basu, O. Cevik, L. Nencioni, A.T. Palamara, C. Zamperini, M. Botta, J. Neyts, P. Leyssen, N. Kaushik-Basu, and R. Silvestri. European Journal of Medicinal Chemistry, 2015. 90: p. 497-506. PMID[25483263].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25511707">Triptolide Inhibits Proliferation of Epstein-Barr Virus-positive B Lymphocytes by Down-regulating Expression of a Viral Protein LMP1.</a> Zhou, H., W. Guo, C. Long, H. Wang, J. Wang, and X. Sun. Biochemical and Biophysical Research Communications, 2015. 456(3): p. 815-820. PMID[25511707].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25378484">MicroRNA miR-21 Attenuates Human Cytomegalovirus Replication in Neural Cells by Targeting Cdc25a.</a> Fu, Y.R., X.J. Liu, X.J. Li, Z.Z. Shen, B. Yang, C.C. Wu, J.F. Li, L.F. Miao, H.Q. Ye, G.H. Qiao, S. Rayner, S. Chavanas, C. Davrinche, W.J. Britt, Q. Tang, M. McVoy, E. Mocarski, and M.H. Luo. Journal of Virology, 2015. 89(2): p. 1070-1082. PMID[25378484].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25603348">Enzyme-assisted Extraction of Bioactive Material from Chondrus crispus and Codium fragile and its Effect on Herpes Simplex Virus (HSV-1).</a> Kulshreshtha, G., A.S. Burlot, C. Marty, A. Critchley, J. Hafting, G. Bedoux, N. Bourgougnon, and B. Prithiviraj. Marine Drugs, 2015. 13(1): p. 558-580. PMID[25603348].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0116-012915.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346796500023">Territrem and Butyrolactone Derivatives from a Marine-derived Fungus Aspergillus terreus.</a> Nong, X.H., Y.F. Wang, X.Y. Zhang, M.P. Zhou, X.Y. Xu, and S.H. Qi. Marine Drugs, 2014. 12(12): p. 6113-6124. ISI[000346796500023].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346885500020">Potent Antiviral Activity of Solanum rantonnetii and the Isolated Compounds against Hepatitis C Virus in Vitro.</a> Rashed, K., M.E. Sahuc, G. Deloison, N. Calland, P. Brodin, Y. Rouille, and K. Seron. Journal of Functional Foods, 2014. 11: p. 185-191. ISI[000346885500020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346878500021">Interference of HCV Replication by Cell Penetrable Human Monoclonal scFv Specific to NS5B Polymerase.</a> Thueng-In, K., J. Thanongsaksrikul, S. Jittavisutthikul, W. Seesuay, M. Chulanetra, Y. Sakolvaree, P. Srimanote, and W. Chaicumpa. mAbs, 2014. 6(5): p. 1327-1339. ISI[000346878500021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346651000429">In Vivo Inhibition of Hepatitis C Virus Infection by anti-Claudin-1 Monoclonal Antibodies.</a> Iida, M., S. Nagase, M. Yamashita, Y. Shirasago, M. Fukasawa, M. Tada, A. Ishii, A. Watari, K. Yagi, and M. Kondoh. FASEB Journal, 2014. 28(1). ISI[000346651000429].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346900000002">Synthesis and Antiviral Activity of Substituted 2,4-bis-Aminomethyl-5-hydroxy-1H-indole-3-carboxylic acid ethyl esters and Their Derivatives.</a> Ivashchenko, A.V., P.M. Yamanushkin, O.D. Mit&#39;kin, V.M. Kisil, O.M. Korzinov, V.Y. Vedenskii, I.A. Leneva, E.A. Bulanova, V.V. Bychko, I.M. Okun, A.A. Ivashchenko, and Y.A. Ivanenkov. Pharmaceutical Chemistry Journal, 2014. 48(9): p. 569-581. ISI[000346900000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347019000016">Hepatitis C Virus Inhibitory Hydrolysable Tannins from the Fruits of Terminalia chebula.</a> Ajala, O.S., A. Jukov, and C.M. Ma. Fitoterapia, 2014. 99: p. 117-123. ISI[000347019000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346796500019">Secocrassumol, a Seco-cembranoid from the Dongsha Atoll Soft Coral Lobophytum crassum.</a> Cheng, S.Y., S.K. Wang, and C.Y. Duh. Marine Drugs, 2014. 12(12): p. 6028-6037. ISI[000346796500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0116-012915.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
