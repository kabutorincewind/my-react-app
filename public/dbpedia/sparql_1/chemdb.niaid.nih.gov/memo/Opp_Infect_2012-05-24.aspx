

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-05-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dF3s51YkmhM3Z15s/fa+WKzsnNAWNlyjUau4EDdswQCbXxzUxqogROpi7wSuIqCeOKO472UrReXgLZQbIcne4Ir/xqoQyUjYcW8UmSl/DMAD9a+eiCUsZ6bIjqM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3BD297BA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  May 11 - May 24, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22592091">Synthesis of 1,2,3-Triazole Derivatives and in Vitro Antifungal Evaluation on Candida Strains</a>. Lima-Neto, R.G., N.N. Cavalcante, R.M. Srivastava, F.J. Mendonca Junior, A.G. Wanderley, R.P. Neves, and J.V. Dos Anjos. Molecules (Basel, Switzerland), 2012. 17(5): p. 5882-5892; PMID[22592091].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22583961">Investigations of Antimicrobial Activity of Some Cameroonian Medicinal Plant Extracts against Bacteria and Yeast with Gastrointestinal Relevance</a>. Tekwu, E.M., A.C. Pieme, and V.P. Beng. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22583961].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/22615290">Identification of the (+)- Erythro-Mefloquine as the Active Enantiomer with Greater Efficacy Than Mefloquine against Mycobacterium avium Infection in Mice</a>. Bermudez, L.E., C.B. Inderlied, P. Kolonoski, C.B. Chee, P. Aralar, M. Petrofsky, T. Parman, C.E. Green, A.H. Lewin, W.Y. Ellis, and L.S. Young. Antimicrobial agents and chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22615290]. <b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22615274">Anti-Tubercular Activity of Disulfiram, an Anti-Alcoholism Drug, against Multi-Drug and Extensively Drug-Resistant Mycobacterium tuberculosis Isolates</a>. Horita, Y., T. Takii, T. Yagi, K. Ogawa, N. Fujiwara, E. Inagaki, L. Kremer, Y. Sato, R. Kuroishi, Y. Lee, T. Makino, H. Mizukami, T. Hasegawa, R. Yamamoto, and K. Onozaki. Antimicrobial agents and chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22615274].</p>
    <p> </p>
    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22589723">Nitazoxanide Stimulates Autophagy and Inhibits Mtorc1 Signaling and Intracellular Proliferation of Mycobacterium tuberculosis</a>. Lam, K.K., X. Zheng, R. Forestieri, A.D. Balgi, M. Nodwell, S. Vollett, H.J. Anderson, R.J. Andersen, Y. Av-Gay, and M. Roberge. PLoS pathogens, 2012. 8(5): p. e1002691; PMID[22589723].</p>
    <p> </p>
    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22577943">Identification of Novel Inhibitors of M. tuberculosis Growth Using Whole Cell Based High-Throughput Screening</a>. Stanley, S.A., S. Schmidt Grant, T. Kawate, N. Iwase, M. Shimizu, C. Wivagg, M. Silvis, E. Kazyanskaya, J. Aquadro, A. Golas, M. Fitzgerald, H. Dai, L. Zhang, and D.T. Hung. ACS chemical biology, 2012. <b>[Epub ahead of print]</b>; PMID[22577943].</p>
    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22595661">Antibacterial Activity of Selected Cameroonian Dietary Spices Ethno-Medically Used Against Strains of Mycobacterium tuberculosis</a>. Tekwu, E.M., T. Askun, V. Kuete, A.E. Nkengfack, B. Nyasse, F.X. Etoa, and V.P. Beng. Journal of ethnopharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22595661]. <b>[PubMed]</b>. OI_0511-052412.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22591034">Disulfide Prodrugs of Albitiazolium (T3/Sar97276): Synthesis and Biological Activities</a>. Caldarelli, S.A., M. Hamel, J.F. Duckert, M. Ouattara, M. Calas, M. Maynadier, S. Wein, C. Perigaud, A. Pellet, H.J. Vial, and S. Peyrottes. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22591034]. <b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22512909">Investigating the Activity of Quinine Analogues Versus Chloroquine Resistant Plasmodium falciparum</a>. Dinio, T., A.P. Gorka, A. McGinniss, P.D. Roepe, and J.B. Morgan. Bioorganic &amp; Medicinal Chemistry, 2012. 20(10): p. 3292-3297; PMID[22512909].</p>
    <p> </p>
    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22618151">Exploring the Trifluoromenadione Core as a Template to Design Antimalarial Redox-Active Agents Interacting with Glutathione Reductase</a>. Lanfranchi, D.A., D. Belorgey, T. Muller, H. Vezin, M. Lanzer, and E. Davioud-Charvet. Organic &amp; biomolecular chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22618151].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22612231">The Discovery and Biochemical Characterization of Plasmodium Thioredoxin Reductase Inhibitors from an Antimalarial Set</a>. Theobald, A.J., I. Caballero, I. Coma, G. Colmenarejo, C. Cid, F.J. Gamo, M.J. Hibbs, A.L. Bass, and D. Thomas. Biochemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22612231]. <b>[PubMed]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22607473">Tight-Binding Inhibitors Efficiently Inactivate Both Reaction Centers of Monomeric Plasmodium falciparum Glyoxalase 1</a>. Urscher, M., S.S. More, R. Alisch, R. Vince, and M. Deponte. The FEBS journal, 2012. <b>[Epub ahead of print]</b>; PMID[22607473].</p>
    <p class="plaintext"><b>[PubMed]</b>. OI_0511-052412.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302964300062">Substituted Aminopyrimidine Protein Kinase B (Pknb) Inhibitors Show Activity against Mycobacterium tuberculosis</a>. Chapman, T.M., N. Bouloc, R.S. Buxton, J. Chugh, K.E.A. Lougheed, S.A. Osborne, B. Saxty, S.J. Smerdon, D.L. Taylor, and D. Whalley. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3349-3353; [ISI:000302964300062].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303471900002">Synthesis, Characterization and Antimicrobial Activity of 4-amino-1-alkyl Pyridinium Salts</a>. Ilangovan, A., P. Venkatesan, M. Sundararaman, and R.R. Kumar. Medicinal Chemistry Research, 2012. 21(6): p. 694-702; [ISI:000303471900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303284400047">Antimicrobial Activity of Sulfonamides Containing 5-chloro-2-hydroxybenzaldehyde and 5-chloro-2-hydroxybenzoic Acid Scaffold</a>. Kratky, M., J. Vinsova, M. Volkova, V. Buchta, F. Trejtnar, and J. Stolarikova. European Journal of Medicinal Chemistry, 2012. 50: p. 433-440; [ISI:000303284400047].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p>   
    <p> </p>
    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303454700003">Isolation and Purification of a Novel Deca-Antifungal Peptide from Potato (Solanum tuberosum L. Cv. Jopung) against Candida albicans</a>. Lee, J.K., R. Gopal, C.H. Seo, H.S. Cheong, and Y.K. Park. International Journal of Molecular Sciences, 2012. 13(4): p. 4021-4032; [ISI:000303454700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303385900029">Molecular Characterization of Antarctic Actinobacteria and Screening for Antimicrobial Metabolite Production</a>. Lee, L.H., Y.K. Cheah, S.M. Sidik, N.S. Ab Mutalib, Y.L. Tang, H.P. Lin, and K. Hong. World Journal of Microbiology &amp; Biotechnology, 2012. 28(5): p. 2125-2137; [ISI:000303385900029]. <b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303101500004">Biological Activities of the Sea Cucumber Holothuria leucospilota</a>. Mokhlesi, A., S. Saeidnia, A.R. Gohari, A.R. Shahverdi, A. Nasrolahi, F. Farahani, R. Khoshnood, and N. Es&#39;haghi. Asian Journal of Animal and Veterinary Advances, 2012. 7(3): p. 243-249; [ISI:000303101500004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302902200016">Synthesis, Characterization, and Antimicrobial Properties of New Polystyrene-Bound Schiff Bases and Their Some Complexes</a>. Nartop, D., N. Sari, A. Altundas, and H. Ogutcu. Journal of Applied Polymer Science, 2012. 125(3): p. 1796-1803; [ISI:000302902200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303454700016">Pharmacological Assessment of the Medicinal Potential of Acacia mearnsii De Wild.: Antimicrobial and Toxicity Activities</a>. Olajuyigbe, O.O. and A.J. Afolayan. International Journal of Molecular Sciences, 2012. 13(4): p. 4255-4267; [ISI:000303454700016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303471900020">Comparative Study of Conventional and Microwave-Assisted Synthesis of Some Schiff Bases and Their Potential as Antimicrobial Agents</a>. Pandey, V., V. Chawla, and S.K. Saraf. Medicinal Chemistry Research, 2012. 21(6): p. 844-852; [ISI:000303471900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303330100007">A New Class of 2-(4-Cyanophenyl amino)-4-(6-bromo-4-quinolinyloxy)-6-piperazinyl (Piperidinyl)-1,3,5-triazine Analogues with Antimicrobial/Antimycobacterial Activity</a>. Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia. Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. 27(3): p. 370-379; [ISI:000303330100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303456900008">Chemical Composition and Antimicrobial Activity of the Volatile Oils of Geranium sanguineum L. And G. robertianum L. (Geraniaceae).</a> Radulovic, N., M. Dekic, and Z. Stojanovic-Radic. Medicinal Chemistry Research, 2012. 21(5): p. 601-615; [ISI:000303456900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303456900013">Design and Synthesis of Novel Piperazine Unit Condensed 2,6-diarylpiperidin-4-one Derivatives as Antituberculosis and Antimicrobial Agents</a>. Rani, M., P. Parthiban, R. Ramachandran, and S. Kabilan. Medicinal Chemistry Research, 2012. 21(5): p. 653-662; [ISI:000303456900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p>     
    <p> </p>
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303530900013">Screening a Library of 1600 Adamantyl Ureas for Anti-Mycobacterium tuberculosis Activity in vitro and for Better Physical Chemical Properties for Bioavailability</a>. Scherman, M.S., E.J. North, V. Jones, T.N. Hess, A.E. Grzegorzewicz, T. Kasagami, I.H. Kim, O. Merzlikin, A.J. Lenaerts, R.E. Lee, M. Jackson, C. Morisseau, and M.R. McNeil. Bioorganic &amp; Medicinal Chemistry, 2012. 20(10): p. 3255-3262; [ISI:000303530900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303284400022">Total Synthesis and Structure-Activity Relationships of New Echinocandin-like Antifungal Cyclolipohexapeptides</a>. Yao, J.Z., H.M. Liu, T. Zhou, H. Chen, Z.Y. Miao, C.Q. Sheng, and W.N. Zhang. European Journal of Medicinal Chemistry, 2012. 50: p. 196-208; [ISI:000303284400022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303243500003">Synthesis and Structures of Halo-Substituted Aroylhydrazones with Antimicrobial Activity</a>. Zhang, M., D.M. Xian, H.H. Li, J.C. Zhang, and Z.L. You. Australian Journal of Chemistry, 2012. 65(4): p. 343-350; [ISI:000303243500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0511-052412.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
