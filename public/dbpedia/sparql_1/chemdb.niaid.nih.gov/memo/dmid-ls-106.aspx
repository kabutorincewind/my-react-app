

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-106.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5vAvNrnzmQaXrGjQda7xeCiNaJE86tft2ZS4XH93eJHoylI/w1RQme0b1iPfLxT4PCkX7wGEubLisN4XknYNuGN2fGSzJtYoMDARoqc89K7cIYn8jK5SCg1ePos=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E175C43A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-106-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57993   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Novel Isonucleosides With 1,2,4-Triazole-3-Carboxamide</p>

    <p>          Kim, M., Chung, S., and Chun, M.</p>

    <p>          Synthetic Communications <b>2005</b>.  35(20): 2653-2663</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233013500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233013500007</a> </p><br />

    <p>2.     57994   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Viral vectors for use in the development of biodefense vaccines</p>

    <p>          Lee, John S, Hadjipanayis, Angela G, and Parker, Michael D</p>

    <p>          Advanced Drug Delivery Reviews <b>2005</b>.  57(9): 1293-1314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T3R-4FY9M53-1/2/343eb41e01be5d112c964d085343a82c">http://www.sciencedirect.com/science/article/B6T3R-4FY9M53-1/2/343eb41e01be5d112c964d085343a82c</a> </p><br />

    <p>3.     57995   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mouse Adenovirus Type 1 Infection in Scid Mice: an Experimental Model for Antiviral Therapy of Systemic Adenovirus Infections</p>

    <p>          Lenaerts, L. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(11): 4689-4699</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233020900035</a> </p><br />

    <p>4.     57996   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, Conformation and Antiviral Activity of Nucleoside Analogues With the (2-Hydroxy-1-Phenylethoxy) Methyl Glycone - a Family of Nucleoside Analogues Related to D4t and Aciclovir</p>

    <p>          Ewing, D. <i>et al.</i></p>

    <p>          New Journal of Chemistry <b>2005</b>.  29(11): 1461-1468</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232846000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232846000016</a> </p><br />

    <p>5.     57997   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Galactans from cystocarpic plants of the red seaweed Callophyllis variegata (Kallymeniaceae, Gigartinales)</p>

    <p>          Rodriguez, MC, Merino, ER, Pujol, CA, Damonte, EB, Cerezo, AS, and Matulewicz, MC</p>

    <p>          Carbohydr Res <b>2005</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289051&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16289051&amp;dopt=abstract</a> </p><br />

    <p>6.     57998   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Highly Efficient, Asymmetric Synthesis of Benzothiadiazine-Substituted Tetramic Acids: Potent Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase</p>

    <p>          Fitch, DM, Evans, KA, Chai, D, and Duffy, KJ</p>

    <p>          Org Lett <b>2005</b>.  7(24): 5521-5524</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16288546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16288546&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     57999   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Reemergence of Monkeypox: Prevalence, Diagnostics, and Countermeasures</p>

    <p>          Nalca, A, Rimoin, AW, Bavari, S, and Whitehouse, CA</p>

    <p>          Clin Infect Dis <b>2005</b>.  41(12): 1765-1771</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16288402&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16288402&amp;dopt=abstract</a> </p><br />

    <p>8.     58000   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The severe acute respiratory syndrome (SARS)</p>

    <p>          Wong, SS and Yuen, K</p>

    <p>          J Neurovirol <b>2005</b>.  11(5): 455-468</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16287687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16287687&amp;dopt=abstract</a> </p><br />

    <p>9.     58001   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effect of the heparan sulfate mimetic, PI-88, against dengue and encephalitic flaviviruses</p>

    <p>          Lee, Eva, Pavy, Megan, Young, Nicolie, Freeman, Craig, and Lobigs, Mario</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4HHX29K-1/2/8209b389ad23d139a78e9165e4b17a4e">http://www.sciencedirect.com/science/article/B6T2H-4HHX29K-1/2/8209b389ad23d139a78e9165e4b17a4e</a> </p><br />

    <p>10.   58002   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomaviruses: basic mechanisms of pathogenesis and oncogenicity</p>

    <p>          Hebner, CM and Laimins, LA</p>

    <p>          Rev Med Virol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16287204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16287204&amp;dopt=abstract</a> </p><br />

    <p>11.   58003   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral prodrugs - the development of successful prodrug strategies for antiviral chemotherapy</p>

    <p>          De Clercq, E and Field, HJ</p>

    <p>          Br J Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16284630&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16284630&amp;dopt=abstract</a> </p><br />

    <p>12.   58004   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Antiviral Agents for Chronic Hepatitis B</p>

    <p>          Lai, C.</p>

    <p>          Antiviral Therapy <b>2004</b>.  9(6): H9-H10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000168">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000168</a> </p><br />
    <br clear="all">

    <p>13.   58005   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of an improved microneutralization assay for respiratory syncytial virus by automated plaque counting using imaging analysis</p>

    <p>          Zielinska, E, Liu, D, Wu, HY, Quiroz, J, Rappaport, R, and Yang, DP</p>

    <p>          Virol J <b>2005</b>.  2(1): 84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281972&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16281972&amp;dopt=abstract</a> </p><br />

    <p>14.   58006   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral therapeutic efficacy of foscarnet in hepatitis B virus infection</p>

    <p>          Han, YX, Xue, R, Zhao, W, Zhou, ZX, Li, JN, Chen, HS, Chen, XH, Wang, YL, Li, YH, Wu, YW, You, XF, Zhao, LX, and Jiang, JD</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16280177&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16280177&amp;dopt=abstract</a> </p><br />

    <p>15.   58007   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A cyclic PNA-based compound targeting domain IV of HCV IRES RNA inhibits in vitro IRES-dependent translation</p>

    <p>          Caldarelli, Sergio A, Mehiri, Mohamed, Giorgio, Audrey Di, Martin, Amaury, Hantz, Olivier, Zoulim, Fabien, Terreux, Raphael, Condom, Roger, and Patino, Nadia</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(20): 5700-5709</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4GSJR1M-1/2/cfe8ac78a8cdfafb9edc42a41a1ee515">http://www.sciencedirect.com/science/article/B6TF8-4GSJR1M-1/2/cfe8ac78a8cdfafb9edc42a41a1ee515</a> </p><br />

    <p>16.   58008   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel inhibitor of respiratory syncytial virus isolated from ethnobotanicals</p>

    <p>          Ojwang, JO, Wang, YH, Wyde, PR, Fischer, NH, Schuehly, W, Appleman, JR, Hinds, S, and Shimasaki, CD</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16280176&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16280176&amp;dopt=abstract</a> </p><br />

    <p>17.   58009   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A Randomized Trial of Pegylated Interferon alpha-2b Plus Ribavirin in the Retreatment of Chronic Hepatitis C</p>

    <p>          Jacobson, IM,  Gonzalez, SA, Ahmed, F, Lebovics, E, Min, AD, Bodenheimer, HC, Esposito, SP, Brown, RS, Brau, N, Klion, FM, Tobias, H, Bini, EJ, Brodsky, N, Cerulli, MA, Aytaman, A, Gardner, PW, Geders, JM, Spivack, JE, Rahmin, MG, Berman, DH, Ehrlich, J, Russo, MW, Chait, M, Rovner, D, and Edlin, BR</p>

    <p>          Am J Gastroenterol <b>2005</b>.  100(11): 2453-2462</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279900&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16279900&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58010   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral agents for influenza: a comparison of cost-effectiveness data</p>

    <p>          Lynd, LD, Goeree, R, and O&#39;brien, BJ</p>

    <p>          Pharmacoeconomics <b>2005</b>.  23(11): 1083-106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16277546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16277546&amp;dopt=abstract</a> </p><br />

    <p>19.   58011   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel anomeric branched carbocyclic nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          Arch Pharm Res <b>2005</b>.  28(10): 1105-1110</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16276962&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16276962&amp;dopt=abstract</a> </p><br />

    <p>20.   58012   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Viral and Cellular Rna Helicases as Antiviral Targets</p>

    <p>          Kwong, A., Rao, B., and Jeang, K.</p>

    <p>          Nature Reviews Drug Discovery <b>2005</b>.  4(10): 845-853</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232546100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232546100023</a> </p><br />

    <p>21.   58013   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rational development of beta -peptide inhibitors of human cytomegalovirus entry</p>

    <p>          English, EP, Chumanov, RS, Gellman, SH, and Compton, T</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275647&amp;dopt=abstract</a> </p><br />

    <p>22.   58014   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of 7-deazaneplanocin A against orthopoxviruses (vaccinia and cowpox virus)</p>

    <p>          Arumugham, B, Kim, HJ, Prichard, MN, Kern, ER, and Chu, CK</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275078&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16275078&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   58015   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and synthesis of 3,4-dihydro-1H-[1]-benzothieno[2,3-c]pyran and 3,4-dihydro-1H-pyrano[3,4-b]benzofuran derivatives as non-nucleoside inhibitors of HCV NS5B RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, A, Aplasca, A, Ciszewski, G, Park, K, Ellingboe, JW, Orlowski, M, Feld, B, and Howe, AY</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274990&amp;dopt=abstract</a> </p><br />

    <p>24.   58016   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A phase I trial of an antisense inhibitor of hepatitis c virus (ISIS 14803), administered to chronic Hepatitis C patients</p>

    <p>          McHutchison, JG, Patel, K, Pockros, P, Nyberg, L, Pianko, S, Yu, RZ, Andrew Dorr, F, and Jesse, Kwoh T</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274834&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16274834&amp;dopt=abstract</a> </p><br />

    <p>25.   58017   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sensitivity of Various Influenza Virus Strains to Arbidol. Influence of Arbidol Combination With Different Antiviral Drugs on Reproduction of Influenza Virus a</p>

    <p>          Leneva, I. <i>et al.</i></p>

    <p>          Terapevticheskii Arkhiv <b>2005</b>.  77(8): 84-88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232889000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232889000017</a> </p><br />

    <p>26.   58018   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Detection of anti-hepatitis C virus effects of interferon and ribavirin by a sensitive replicon system</p>

    <p>          Kato, T, Date, T, Miyamoto, M, Sugiyama, M, Tanaka, Y, Orito, E, Ohno, T, Sugihara, K, Hasegawa, I, Fujiwara, K, Ito, K, Ozasa, A, Mizokami, M, and Wakita, T</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(11): 5679-5684</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16272504&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16272504&amp;dopt=abstract</a> </p><br />

    <p>27.   58019   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Drug insight: Nucleoside and nucleotide analog inhibitors for hepatitis B</p>

    <p>          Fung, SK and Lok, AS</p>

    <p>          Nat Clin Pract Gastroenterol Hepatol <b>2004</b>.  1(2): 90-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16265070&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16265070&amp;dopt=abstract</a> </p><br />

    <p>28.   58020   DMID-LS-106; WOS-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of Phosphonate Analogues of the Antiviral Cyclopropane Nucleoside a-5021</p>

    <p>          Onishi, T., Sekiyama, T., and Tsuji, T.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(8): 1187-1197</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900003</a> </p><br />

    <p>29.   58021   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Detection of amantadine-resistant variants among avian influenza viruses isolated in North America and Asia</p>

    <p>          Ilyushina, Natalia A, Govorkova, Elena A, and Webster, Robert G</p>

    <p>          Virology <b>2005</b>.  341(1): 102-106</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4GSJXB9-4/2/e253ad28484de54bf9f99636e7a188bf">http://www.sciencedirect.com/science/article/B6WXR-4GSJXB9-4/2/e253ad28484de54bf9f99636e7a188bf</a> </p><br />

    <p>30.   58022   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sensitivity of influenza viruses to zanamivir and oseltamivir: A study performed on viruses circulating in France prior to the introduction of neuraminidase inhibitors in clinical practice</p>

    <p>          Ferraris, O, Kessler, N, and Lina, B</p>

    <p>          Antiviral Research <b>2005</b>.  68(1): 43-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-8/2/b1c9b3996f91859c5ec6e4e9dc19c6f3">http://www.sciencedirect.com/science/article/B6T2H-4GV2PBP-8/2/b1c9b3996f91859c5ec6e4e9dc19c6f3</a> </p><br />

    <p>31.   58023   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Organotypic epithelial raft cultures as a model for evaluating compounds against alphaherpesviruses</p>

    <p>          Andrei, G, van den Oord, J, Fiten, P, Opdenakker, G, De Wolf-Peeters, C, De Clercq, E, and Snoeck, R</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4671-4680</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251311&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251311&amp;dopt=abstract</a> </p><br />

    <p>32.   58024   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Susceptibilities of antiviral-resistant influenza viruses to novel neuraminidase inhibitors</p>

    <p>          Mishin, VP, Hayden, FG, and Gubareva, LV</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4515-4520</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251290&amp;dopt=abstract</a> </p><br />

    <p>33.   58025   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nutritional and therapeutic potential of Spirulina</p>

    <p>          Khan, Z, Bhadouria, P, and Bisen, PS</p>

    <p>          Curr Pharm Biotechnol <b>2005</b>.  6(5): 373-379</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248810&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248810&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   58026   DMID-LS-106; PUBMED-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The journey towards elucidating the anti-HCMV activity of alkylated bicyclic furano pyrimidines</p>

    <p>          Kelleher, MR,  McGuigan, C, Bidet, O, Carangio, A, Weldon, H, Andrei, G, Snoeck, R, De Clercq, E, and Balzarini, J</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 643-645</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248004&amp;dopt=abstract</a> </p><br />

    <p>35.   58027   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-SARS-CoV immunity induced by a novel CpG oligodeoxynucleotide</p>

    <p>          Bao, Musheng, Zhang, Yi, Wan, Min, Dai, Li, Hu, Xiaoping, Wu, Xiuli, Wang, Li, Deng, Ping, Wang, Junzhi, and Chen, Jianzhu</p>

    <p>          Clinical Immunology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WCJ-4HKCYYH-1/2/20fb48ad69f3c28701068ac0f565c359">http://www.sciencedirect.com/science/article/B6WCJ-4HKCYYH-1/2/20fb48ad69f3c28701068ac0f565c359</a> </p><br />

    <p>36.   58028   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Crystal Structures of the Main Peptidase from the SARS Coronavirus Inhibited by a Substrate-like Aza-peptide Epoxide</p>

    <p>          Lee, Ting-Wai, Cherney, Maia M, Huitema, Carly, Liu, Jie, James, Karen Ellis, Powers, James C, Eltis, Lindsay D, and James, Michael NG</p>

    <p>          Journal of Molecular Biology <b>2005</b>.  353(5): 1137-1151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4H6GW7F-1/2/672976e1aaac4cac74b2b9a9b2d8227e">http://www.sciencedirect.com/science/article/B6WK7-4H6GW7F-1/2/672976e1aaac4cac74b2b9a9b2d8227e</a> </p><br />

    <p>37.   58029   DMID-LS-106; EMBASE-DMID-11/21/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Why are HIV-1 fusion inhibitors not effective against SARS-CoV? Biophysical evaluation of molecular interactions</p>

    <p>          Veiga, Salome, Yuan, Yunyun, Li, Xuqin, Santos, Nuno C, Liu, Gang, and Castanho, Miguel ARB</p>

    <p>          Biochimica et Biophysica Acta (BBA) - General Subjects <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T1W-4HF56BH-1/2/f0f2c7a7a52c08d3f54897c635d4ef43">http://www.sciencedirect.com/science/article/B6T1W-4HF56BH-1/2/f0f2c7a7a52c08d3f54897c635d4ef43</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
