

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-07-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nGyitva8Uc8rX/AukYFlnapjH2I0uxKmmkX02IF8Qsahrz3N/gC56Ml8WM2yGmjKTpFDFKNHzxnSw9+GOXmChgNwMz/HHRCyXvrAPxlj9iyPhddQPCTPuMukHQA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CCAF37B3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 21 - July 4, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23794478">Multiple Antigen Peptide Mimetics Containing Gp41 Membrane-proximal External Region Elicit Broad Neutralizing Antibodies against Human Immunodeficiency Virus Type 1 in Guinea Pigs.</a> Zhang, L., L. Miao, X. Gong, H. Zhang, L. Yang, Y. Shi, W. Kong, C. Jiang, and Y. Shan. Journal of Peptide Science, 2013. <b>[Epub ahead of print]</b>. PMID[23794478].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23794600">An Engineered HIV-1 Gp41 Trimeric Coiled Coil with Increased Stability and anti-HIV-1 Activity: Implication for Developing anti-HIV Microbicides.</a> Tong, P., Z. Lu, X. Chen, Q. Wang, F. Yu, P. Zou, X. Yu, Y. Li, L. Lu, Y.H. Chen, and S. Jiang. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23794600].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23673016">Identification of Potent and Orally Bioavailable Nucleotide Competing Reverse Transcriptase Inhibitors: In Vitro and in Vivo Optimization of a Series of Benzofurano[3,2-D]pyrimidin-2-one Derived Inhibitors.</a> Sturino, C.F., Y. Bousquet, C.A. James, P. Deroy, M. Duplessis, P.J. Edwards, T. Halmos, J. Minville, L. Morency, S. Morin, B. Thavonekham, M. Tremblay, J. Duan, M. Ribadeneira, M. Garneau, A. Pelletier, S. Tremblay, L. Lamorte, R. Bethell, M.G. Cordingley, D. Rajotte, and B. Simoneau. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(13): p. 3967-3975. PMID[23673016].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23800377">Hypersusceptibility Mechanism of Tenofovir-resistant HIV to EFdA.</a> Michailidis, E., E.M. Ryan, A. Hachiya, K.A. Kirby, B. Marchand, M.D. Leslie, A.D. Huber, Y.T. Ong, J.C. Jackson, K. Singh, E.N. Kodama, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos. Retrovirology, 2013. 10(1): p. 65. PMID[23800377].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23658440">A Carboxy-terminally Truncated Human CPSF6 Lacking Residues Encoded by Exon 6 Inhibits HIV-1 cDNA Synthesis and Promotes Capsid Disassembly.</a> Hori, T., H. Takeuchi, H. Saito, R. Sakuma, Y. Inagaki, and S. Yamaoka. Journal of Virology, 2013. 87(13): p. 7726-7736. PMID[23658440].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23738539">Bioactive Dibenzocyclooctadiene Lignans from the Stems of Schisandra neglecta.</a> Gao, X.M., R.R. Wang, D.Y. Niu, C.Y. Meng, L.M. Yang, Y.T. Zheng, G.Y. Yang, Q.F. Hu, H.D. Sun, and W.L. Xiao. Journal of Natural Products, 2013. 76(6): p. 1052-1057. PMID[23738539].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23417324">Activation of GCN2 Upon HIV-1 Infection and Inhibition of Translation.</a> Cosnefroy, O., A. Jaspart, C. Calmels, V. Parissi, H. Fleury, M. Ventura, S. Reigadas, and M.L. Andreola. Cellular and Molecular Life Sciences, 2013. 70(13): p. 2411-2421. PMID[23417324].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23750312">Chemically Programmed Antibodies as HIV-1 Attachment Inhibitors.</a> Sato, S., T. Inokuma, N. Otsubo, D.R. Burton, and C.F. Barbas, 3rd. ACS Medicinal Chemistry Letters, 2013. 4(5): p. 460-465. PMID[23750312].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23640125">Inhibition of Infection and Transmission of HIV-1 and Lack of Significant Impact on the Vaginal Commensal Lactobacilli by Carbohydrate-binding Agents.</a> Petrova, M.I., L. Mathys, S. Lebeer, S. Noppen, E.J. Van Damme, H. Tanaka, Y. Igarashi, M. Vaneechoutte, J. Vanderleyden, and J. Balzarini. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23640125].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23427247">IFN-alpha Induces APOBEC3G, F, and A in Immature Dendritic Cells and Limits HIV-1 Spread to Cd4+ T Cells.</a> Mohanram, V., A.E. Skold, S.M. Bachle, S.K. Pathak, and A.L. Spetz. Journal of Immunology, 2013. 190(7): p. 3346-3353. PMID[23427247].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23702242">The Seventh (and Last?) International Microbicides Conference: From Discovery to Delivery.</a> McGregor, S., G. Tachedjian, B.G. Haire, and J.M. Kaldor. Sexual Health, 2013. 10(3): p. 240-245. PMID[23702242].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23738946">In Vitro and Ex Vivo Evaluation of Polymeric Nanoparticles for Vaginal and Rectal Delivery of the anti-HIV Drug Dapivirine.</a> das Neves, J., F. Araujo, F. Andrade, J. Michiels, K.K. Arien, G. Vanham, M. Amiji, M.F. Bahia, and B. Sarmento. Molecular Pharmaceutics, 2013. <b>[Epub ahead of print]</b>. PMID[23738946].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0621-070413.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318892500027">Pyridine-substituted Desoxyritonavir Is a More Potent Inhibitor of Cytochrome P450 3A4 Than Ritonavir.</a> Sevrioukova, I.F. and T.L. Poulos. Journal of Medicinal Chemistry, 2013. 56(9): p. 3733-3741. ISI[000318892500027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319019200027">Structure-Activity Relationships of the Antiviral D4T and Seven 4 &#39;-Substituted Derivatives Using MP2 and DFT Methods.</a> Palafox, M.A. and N. Iza. Structural Chemistry, 2013. 24(3): p. 967-980. ISI[000319019200027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000306806600130">Silibinin Inhibits HIV-1 Infection by Reducing Cellular Activation and Proliferation.</a> McClure, J., E.S. Lovelace, S. Elahi, N.J. Maurice, J. Wagoner, J. Dragavon, J.E. Mittler, Z. Kraft, L. Stamatatos, H. Horton, S.C. De Rosa, R.W. Coombs, and S.J. Polyak. PloS One, 2012. 7(7): p. e41832. ISI[000306806600130].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318870600010">N&#39;-4-(Substituted imino)methylbenzylidene-substituted benzohydrazides: Synthesis, Antimicrobial, Antiviral, and Anticancer Evaluation, and QSAR Studies.</a> Kumar, P., B. Narasimhan, K. Ramasamy, V. Mani, R.K. Mishra, A.A. Majeed, and E. De Clercq. Monatshefte fur Chemie, 2013. 144(6): p. 825-849. ISI[000318870600010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318253400020">Alpha-helix Mimicry with Alpha/Beta-peptides.</a> Johnson, L.M. and S.H. Gellman. Methods in Protein Design, 2013. 523: p. 407-429. ISI[000318253400020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319067200009">Mechanisms of Pharmacokinetic Enhancement between Ritonavir and Saquinavir; Micro/Small Dosing Tests Using Midazolam (CYP3A4), Fexofenadine (P-glycoprotein), and Pravastatin (OATP1B1) as Probe Drugs.</a> Ieiri, I., S. Tsunemitsu, K. Maeda, Y. Ando, N. Izumi, M. Kimura, N. Yamane, T. Okuzono, M. Morishita, N. Kotani, E. Kanda, M. Deguchi, K. Matsuguma, S. Matsuki, T. Hirota, S. Irie, H. Kusuhara, and Y. Sugiyama. Journal of Clinical Pharmacology, 2013. 53(6): p. 654-661. ISI[000319067200009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319417100006">Indolizine Derivatives as HIV-1 Vifelonginc Interaction Inhibitors</a>. Huang, W.L., T. Zuo, X. Luo, H.W. Jin, Z.M. Liu, Z.J. Yang, X.H. Yu, L.R. Zhang, and L.H. Zhang. Chemical Biology &amp; Drug Design, 2013. 81(6): p. 730-741. ISI[000319417100006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319525200026">Reducing Infectivity of HIV Upon Exposure to Surfaces Coated with N,N-dodecyl, Methyl-polyethylenimine.</a> Gerrard, S.E., A.M. Larson, A.M. Klibanov, N.K.H. Slater, C.V. Hanson, B.F. Abrams, and M.K. Morris. Biotechnology and Bioengineering, 2013. 110(7): p. 2058-2062. ISI[000319525200026].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319359800030">Molecular Docking Studies of Novel Thiazolidinedione Analogs as HIV-1-RT Inhibitors.</a> Ganguly, S. and R.S. Bahare. Medicinal Chemistry Research, 2013. 22(7): p. 3350-3363. ISI[000319359800030].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000319066700022">Telaprevir and Boceprevir: A Potential Role for Therapeutic Drug Monitoring.</a> Dolton, M.J., J.E. Ray, and A.J. McLachlan. Therapeutic Drug Monitoring, 2013. 35(3): p. 414-415. ISI[000319066700022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318983000009">Gardiquimod: A Toll-like Receptor-7 Agonist That Inhibits HIV Type 1 Infection of Human Macrophages and Activated T Cells.</a> Buitendijk, M., S.K. Eszterhas, and A.L. Howell. AIDS Research and Human Retroviruses, 2013. 29(6): p. 907-918. ISI[000318983000009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318836000004">Synthesis and anti-HIV Activity of New Fused Chromene Derivatives Derived from 2-Amino-4-(1-naphthyl)-5-oxo-4h,5h-pyrano 3,2-C Chromene-3-carbonitrile.</a> Al-Masoudi, N.A., H.H. Mohammed, A.M. Hamdy, O.A. Akrawi, N. Eleya, A. Spannenberg, C. Pannecouque, and P. Langer. Zeitschrift fur Naturforschung Section B-A Journal of Chemical Sciences, 2013. 68(3): p. 229-238. ISI[000318836000004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0621-070413.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130530&amp;CC=WO&amp;NR=2013075606A1&amp;KC=A1">Preparation of Small Molecule-polypeptide Conjugate for Inhibition of HIV Infection.</a> Liu, K., C. Wang, W. Shi, L. Cai, K. Wang, B. Zheng, Q. Jia, S. Feng, and Y. Bai. Patent. 2013. 2012-CN84859 2013075606: 64pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130606&amp;CC=WO&amp;NR=2013079586A1&amp;KC=A1">Heteroarylpyrazole-3-carboxamides as Antiviral Agents and Their Preparation and Use in the Treatment of Retroviral Infection.</a> Wildum, S., B. Klenke, and A. Wendt. Patent. 2013. 2012-EP73944 2013079586: 222pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0621-070413.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130522&amp;CC=CN&amp;NR=103113285A&amp;KC=A">Preparation of Indole Derivatives as HIV-1 Reverse Transcriptase Inhibitors.</a> Wu, S., H. Zhou, B. Tian, C. Dong, W. Ouyang, and X. Han. Patent. 2013. 2013-10076738 103113285: 12pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0621-070413.</p>

    <br />
    
    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130508&amp;CC=CN&amp;NR=103087061A&amp;KC=A">Preparation of Indolizine Derivatives as Antiviral Agents.</a> Zhang, L., X. Yu, W. Huang, T. Zuo, Z. Yang, and L. Zhang. Patent. 2013. 2013-10014922 103087061: 41pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0621-070413.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
