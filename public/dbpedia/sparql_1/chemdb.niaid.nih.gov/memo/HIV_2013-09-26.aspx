

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-09-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4lSghYXznczUucq9JlvxELVuojW8QWsqv1xBsg6jtnchdHmfMvZb2+vZhjpgFCZmV1toZjouEmZGlJOC2Fd0bBsNQHkrBDbI8yNp/Z/zjhLSOm25jTNFxXJjOyE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="57839D88" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 13 - September 26, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23937980">Optimization of Diarylazines as anti-HIV Agents with Dramatically Enhanced Solubility.</a>Bollini, M., J.A. Cisneros, K.A. Spasov, K.S. Anderson, and W.L. Jorgensen. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(18): p. 5213-5216. PMID[23937980].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24015986">Structure-guided Discovery of a Novel Aminoglycoside Conjugate Targeting HIV-1 RNA Viral Genome.</a> Ennifar, E., M.W. Aslam, P. Strasser, G. Hoffmann, P. Dumas, and F.L. van Delft. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24015986]. .</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23916148">Synthesis and Evaluation of Novel 3-(3,5-Dimethylbenzyl)uracil Analogs as Potential anti-HIV-1 Agents.</a> Sakakibara, N., T. Hamasaki, M. Baba, Y. Demizu, M. Kurihara, K. Irie, M. Iwai, E. Asada, Y. Kato, and T. Maruyama. Bioorganic &amp; Medicinal Chemistry, 2013. 21(18): p. 5900-5906. PMID[23916148].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24032650">Efficient Convergent Synthesis of Bi-, Tri-, and Tetraantennary Complex Type N-glycans and Their HIV-1 Antigenecity.</a> Shivatare, S.S., S.H. Chang, T.I. Tsai, C.T. Ren, H.Y. Chuang, L. Hsu, C.W. Lin, S.T. Li, C.Y. Wu, and C.H. Wong. Journal of the American Chemical Society, 2013. <b>[Epub ahead of print]</b>. PMID[24032650].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24033292">Anti-HIV-1 Integrase Activity of Mimusops elengi Leaf Extracts.</a>Suedee, A., S. Tewtrakul, and P. Panichayupakaranant. Pharmaceutical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24033292].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24030490">Structure of the CCR5 Chemokine Receptor-HIV Entry Inhibitor Maraviroc Complex.</a>Tan, Q., Y. Zhu, J. Li, Z. Chen, G.W. Han, I. Kufareva, T. Li, L. Ma, G. Fenalti, J. Li, W. Zhang, X. Xie, H. Yang, H. Jiang, V. Cherezov, H. Liu, R.C. Stevens, Q. Zhao, and B. Wu. Science, 2013. 341(6152): p. 1387-1390. PMID[24030490].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0913-092613.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323542400006">Antiretroviral Therapy: Dolutegravir Sets SAIL(ING).</a>Boyd, M.A. and B. Donovan. Lancet, 2013. 382(9893): p. 664-666. ISI[000323542400006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323206000018">Synthesis of a New Series of Biologically Interesting 6 &#39;-Chloro-1&#39;,1&#39;-dioxospiro[4H-benzo[D][1,3,7]oxadiazocine-4,3&#39;(2&#39;H)-[1,4,2]benzodithiazine]-2,6(1H,5H)dione Derivatives.</a>Brzozowski, Z., B. Zolnowska, and J. Slawinski. Monatshefte fur Chemie, 2013. 144(9): p. 1397-1405. ISI[000323206000018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323604600002">Heterocyclic HIV-protease Inhibitors.</a>Calugi, C., A. Guarna, and A. Trabocchi. Current Medicinal Chemistry, 2013. 20(30): p. 3693-3710. ISI[000323604600002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322987104118">Protective Role of Anti-viral Human Defensins During Acute HIV Infection.</a>Corleis, B., W. Gostic, and D. Kwon. Journal of Immunology, 2013. 190: 131.25. ISI[000322987104118].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323474900003">Harnessing the CRISPR/Cas9 System to Disrupt Latent HIV-1 Provirus.</a>Ebina, H., N. Misawa, Y. Kanemura, and Y. Koyanagi. Scientific Reports, 2013. 3: 2510. ISI[000323474900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322361200044">Development of an HIV-1 Microbicide Based on Caulobacter crescentus: Blocking Infection by High-density Display of Virus Entry Inhibitors.</a>Farr, C., J.F. Nomellini, E. Ailon, I. Shanina, S. Sangsari, L.A. Cavacini, J. Smit, and M.S. Horwitz. PloS one, 2013. 8(6): p. e65965. ISI[000322361200044].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322853000043">Total Synthesis of the Proposed Structure of Didemnaketal B.</a>Fuwa, H., K. Sekine, and M. Sasaki. Organic Letters, 2013. 15(15): p. 3970-3973. ISI[000322853000043].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322987108027">Human anti-HIV-1 Envelope Monoclonal Antibodies with a Low Percentage of Mutations Have the Ability to Neutralize Sensitive Tier 1 Viruses.</a>Gorny, M., L.Z. Li, X.H. Wang, C. Williams, B. Volsky, M. Seaman, and S. Zolla-Pazner. Journal of Immunology, 2013. 190: 118.18. ISI[000322987108027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323480900017">N-Methylpyridinium tosylate Catalysed Green Synthesis, X-ray Studies and Antimicrobial Activities of Novel (E)-3-Amino-2-(e)-(3,4-dihydronaphthalen-1(2H)-ylidene)hydrazono)thiazolidin-4-ones.</a> Gupta, R. and R.P. Chaudhary. Phosphorus Sulfur and Silicon and the Related Elements, 2013. 188(9): p. 1296-1304. ISI[000323480900017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323628500025">Synthesis and Biological Activity Evaluation of 5-Pyrazoline Substituted 4-Thiazolidinones.</a> Havrylyuk, D., B. Zimenkovsky, O. Vasylenko, C.W. Day, D.F. Smee, P. Grellier, and R. Lesyk. European Journal of Medicinal Chemistry, 2013. 66: p. 228-237. ISI[000323628500025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323593800006">Structure-based Design and Synthesis of an HIV-1 Entry Inhibitor Exploiting X-ray and Thermodynamic Characterization.</a>LaLonde, J.M., M. Le-Khac, D.M. Jones, J.R. Courter, J. Park, A. Schon, A.M. Princiotto, X.L. Wu, J.R. Mascola, E. Freire, J. Sodroski, N. Madani, W.A. Hendrickson, and A.B. Smith. ACS Medicinal Chemistry Letters, 2013. 4(3): p. 338-343. ISI[000323593800006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323187000004">Increase of anti-HIV Activity of C-peptide Fusion Inhibitors Using a Bivalent Drug Design Approach.</a> Ling, Y.B., H.F. Xue, X.F. Jiang, L.F. Cai, and K.L. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(17): p. 4770-4773. ISI[000323187000004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321884200028">Design of Cell-permeable Stapled Peptides as HIV-1 Integrase Inhibitors.</a> Long, Y.Q., S.X. Huang, Z. Zawahir, Z.L. Xu, H.Y. Li, T.W. Sanchez, Y. Zhi, S. De Houwer, F. Christ, Z. Debyser, and N. Neamati. Journal of Medicinal Chemistry, 2013. 56(13): p. 5601-5612. ISI[000321884200028].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000323215300013">Evaluation of the Antimicrobial Activity of Some 4H-Pyrano[3,2-h]quinoline,7H-pyrimido[4&#39;,5&#39;:6,5]pyrano[3,2-h]quinoline Derivatives.</a>Mohamed, H.M., I.A. Radini, A.M. Al-Ghamdi, and A.M. El-Agrody. Letters in Drug Design &amp; Discovery, 2013. 10(8): p. 758-775. ISI[000323215300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323424100013">Inhibition of Infection and Transmission of HIV-1 and Lack of Significant Impact on the Vaginal Commensal Lactobacilli by Carbohydrate-binding Agents.</a> Petrova, M.I., L. Mathys, S. Lebeer, S. Noppen, E.J.M. Van Damme, H. Tanaka, Y. Igarashi, M. Vaneechoutte, J. Vanderleyden, and J. Balzarini. Journal of Antimicrobial Chemotherapy, 2013. 68(9): p. 2026-2037. ISI[000323424100013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323020000052">New Solid Forms of the anti-HIV Drug Etravirine: Salts, Cocrystals, and Solubility.</a> Rajput, L., P. Sanphui, and G.R. Desiraju. Crystal Growth &amp; Design, 2013. 13(8): p. 3681-3690. ISI[000323020000052].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323721200006">Anti-HIV-1 Integrase Compound from Pometia pinnata Leaves.</a>Suedee, A., S. Tewtrakul, and P. Panichayupakaranant. Pharmaceutical Biology, 2013. 51(10): p. 1256-1261. ISI[000323721200006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322342800143">Macrophage Resistance to HIV-1 Infection Is Enhanced by the Neuropeptides VIP and PACAP.</a> Temerozo, J.R., R. Joaquim, E.G. Regis, W. Savino, and D.C. Bou-Habib. PloS one, 2013. 8(6): p. e67701 . ISI[000322342800143].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000323378600003">Purification and Characterization of a Protein with Antifungal, Antiproliferative, and HIV-1 Reverse Transcriptase Inhibitory Activities from Small Brown-eyed Cowpea Seeds.</a> Tian, G.T., M.J. Zhu, Y.Y. Wu, Q. Liu, H.X. Wang, and T.B. Ng. Biotechnology and Applied Biochemistry, 2013. 60(4): p. 393-398. ISI[000323378600003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0913-092613.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130808&amp;CC=WO&amp;NR=2013115884A2&amp;KC=A2">Pyridopyrimidinone Inhibitors of Viruses.</a> Schaus, S.E., L. Brown, J. Connor, and K.W. Dower. Patent. 2013. 2012-US65245 2013115884: 104pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130815&amp;CC=US&amp;NR=2013210857A1&amp;KC=A1">Preparation of Tetrahydro-1,6-naphthyridinylacetic acid Compounds as Inhibitors of Human Immunodeficiency Virus Replication.</a> Walker, M.A., M.E. Sorenson, B.N. Naidu, K. Peese, B.L. Johnson, and M. Patel. Patent. 2013. 2013-766587 20130210857: 37pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130807&amp;CC=CN&amp;NR=103232490A&amp;KC=A">Nucleoside Compound with Inhibitory Effect on HIV-1/HBV Viral Replication, Preparation Method and Application in Treating Virus.</a> You, G., H. Liu, and S. Yang. Patent. 2013. 2013-10134492</p>

    <p class="plaintext">103232490: 23pp. <b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">29. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130731&amp;CC=CN&amp;NR=103224530A&amp;KC=A">A Group of Tenofovir Disoproxil Compounds and and Their Pharmaceutically Acceptable Salts, Their Preparation Method and Application to Antiviral Agent.</a>You, G., H. Liu, and S. Yang. Patent. 2013. 2013-10134160 103224530: 40pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">30. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130904&amp;CC=CN&amp;NR=103275105A&amp;KC=A">Azasugar-Thiazinone Fuse Ring Derivatives, Synthesis Method and Application in Pharmaceutical Preparation.</a> Li, X., H. Chen, T. Yang, S. Wei, Z. Qin, P. Zhang, and K. Wang. Patent. 2013. 2012-10277577 103275105: 18pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130912&amp;CC=WO&amp;NR=2013134113A1&amp;KC=A1">Pyrimidine Derivatives as HIV Inhibitors Useful in Treatment of Diseases Infected by Human Immunodeficiency Virus and Their Preparation.</a>Pendri, A., D.R. Langley, S. Gerritz, G. Li, W. Zhai, S. D&#39;Andrea, M. Patel, B.N. Naidu, K. Peese, and Z. Wang. Patent. 2013. 2013-US28846 2013134113: 114pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130822&amp;CC=WO&amp;NR=2013123019A1&amp;KC=A1">Preparation of C3 Cycloalkenyl Triterpenoids with HIV Maturation Inhibitory Activity.</a>Swidorski, J., N.A. Meanwell, A. Regueiro-Ren, S.-Y. Sit, J. Chen, and Y. Chen. Patent. 2013. 2013-US25897 2013123019: 261pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>

    <br />

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130912&amp;CC=WO&amp;NR=2013134142A1&amp;KC=A1">Pyrimidine Derivatives as HIV Replication Inhibitors Useful in Treatment of Diseases Infected by Human Immunodeficiency Virus and Their Preparation.</a>Zheng, Z.B., S. D&#39;Andrea, D.R. Langley, and B.N. Naidu. Patent. 2013. 2013-US28903 2013134142: 63pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0913-092613.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
