

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-07-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Cx2x7mz0wccfqPv7vv/FRLi6yFLnC2HZ+22T09WmxFqbJyW+fEN53J+doRBlDXEMY/4yJvp8oUXux3msAh0u2FGC6aU8+2d/WN5e/EdOvEjgZE3RITMTGdPuY5I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="317253D0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">July 8, 2009 - July  21, 2009</p>

    <p class="memofmt2-2">Opportunistic protozoa (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="ListParagraph">1.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19618935?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and Evaluation of 4-Acyl-2-thiazolylhydrazone Derivatives for Anti-Toxoplasma Efficacy in Vitro.</a></p>

    <p class="NoSpacing">Chimenti F, Bizzarri B, Bolasco A, Secci D, Chimenti P, Carradori S, Granese A, Rivanera D, Frishberg N, Bordo&#769;n C, Jones-Brando L.</p>

    <p class="NoSpacing">J Med Chem. 2009 Jul 20. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19618935 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19560363?ordinalpos=40&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vitro biological activity and structural analysis of 2,4-diamino-5-(2&#39;-arylpropargyl)pyrimidine inhibitors of Candida albicans.</a></p>

    <p class="NoSpacing">Paulsen JL, Liu J, Bolstad DB, Smith AE, Priestley ND, Wright DL, Anderson AC.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jul 15;17(14):4866-72. Epub 2009 Jun 17.</p>

    <p class="NoSpacing">PMID: 19560363 [PubMed - in process]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19487691?ordinalpos=44&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Chemical Genetic Profiling and Characterization of Small-molecule Compounds That Affect the Biosynthesis of Unsaturated Fatty Acids in Candida albicans.</a></p>

    <p class="NoSpacing">Xu D, Sillaots S, Davison J, Hu W, Jiang B, Kauffman S, Martel N, Ocampo P, Oh C, Trosok S, Veillette K, Wang H, Yang M, Zhang L, Becker J, Martin CE, Roemer T.</p>

    <p class="NoSpacing">J Biol Chem. 2009 Jul 17;284(29):19754-64. Epub 2009 Jun 1.</p>

    <p class="NoSpacing">PMID: 19487691 [PubMed - in process]</p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19439410?ordinalpos=123&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">S-Adenosyl-N-decyl-aminoethyl, a Potent Bisubstrate Inhibitor of Mycobacterium tuberculosis Mycolic Acid Methyltransferases.</a></p>

    <p class="NoSpacing">Vaubourgeix J, Bardou F, Boissier F, Julien S, Constant P, Ploux O, Daffé M, Quémard A, Mourey L.</p>

    <p class="NoSpacing">J Biol Chem. 2009 Jul 17;284(29):19321-30. Epub 2009 May 13.</p>

    <p class="NoSpacing">PMID: 19439410 [PubMed - in process]</p>

    <h2>Antimicrobials, Antifungals (General)</h2> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19598182?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design of potent 9-mer antimicrobial peptide analogs of protaetiamycine and investigation of mechanism of antimicrobial action.</a></p>

    <p class="NoSpacing">Shin S, Kim JK, Lee JY, Jung KW, Hwang JS, Lee J, Lee DG, Kim I, Shin SY, Kim Y.</p>

    <p class="NoSpacing">J Pept Sci. 2009 Jul 13. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19598182 [PubMed - as supplied by publisher]</p>   

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">6. Stavri, M., et al.</p>

    <p class="NoSpacing">Antibacterial Diterpenes from Plectranthus ernstii</p>
    
    <p class="NoSpacing"></p>JOURNAL OF NATURAL PRODUCTS  2009 ( June) 72: 1191 - 1194</p> 

    <p class="ListParagraph">7. Mann, S., et al.</p>

    <p class="NoSpacing">Inhibition of 7,8-diaminopelargonic acid aminotransferase from Mycobacterium tuberculosis by chiral and achiral anologs of its substrate: Biological implications
    
    <p class="NoSpacing"></p>BIOCHIMIE  2009 (July) 91: 826 - 834</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
