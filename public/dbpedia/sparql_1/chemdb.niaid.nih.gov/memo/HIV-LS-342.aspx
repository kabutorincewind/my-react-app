

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-342.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+JrOH8z1OeQkKuDW8GhSMGqzVmCKm2tpPMjpvL1fU21ImhnKEdYF6sWXE9cjM3vsicQ6RSYGfjJbMdRy43mVsrsJ/fZ0ufav6RVIixiuuJGDYLmn/eDNVV14bhw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D0592731" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-342-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48482   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Design and synthesis of novel HIV-1 protease inhibitors incorporating oxyindoles as the P(2)(&#39;)-ligands</p>

    <p>          Ghosh, AK, Schiltz, G, Perali, RS, Leshchenko, S, Kay, S, Walters, DE, Koh, Y, Maeda, K, and Mitsuya, H</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480871&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480871&amp;dopt=abstract</a> </p><br />

    <p>2.     48483   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluations of novel oxindoles as HIV-1 non-nucleoside reverse transcriptase inhibitors. Part I</p>

    <p>          He, Y, Jiang, T, Kuhen, KL, Wolff, K, Yin, H, Bieza, K, Caldwell, J, Bursulaya, B, and Wu, TY</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480865&amp;dopt=abstract</a> </p><br />

    <p>3.     48484   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Effectiveness of Nonpeptide Clinical Inhibitor TMC-114 on HIV-1 Protease with Highly Drug Resistant Mutations D30N, I50V, and L90M</p>

    <p>          Kovalevsky, AY, Tie, Y, Liu, F, Boross, PI, Wang, YF, Leshchenko, S, Ghosh, AK, Harrison, RW, and Weber, IT</p>

    <p>          J Med Chem <b>2006</b>.  49(4): 1379-1387</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480273&amp;dopt=abstract</a> </p><br />

    <p>4.     48485   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Structural and molecular interactions of CCR5 inhibitors with CCR5</p>

    <p>          Maeda, K, Das, D, Ogata-Aoki, H, Nakata, H, Miyakawa, T, Tojo, Y, Norman, R, Takaoka, Y, Ding, J, Arnold, E, and Mitsuya, H</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16476734&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16476734&amp;dopt=abstract</a> </p><br />

    <p>5.     48486   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          The Level of Reverse Transcriptase (RT) in Human Immunodeficiency Virus Type 1 Particles Affects Susceptibility to Nonnucleoside RT Inhibitors but Not to Lamivudine</p>

    <p>          Ambrose, Z, Julias, JG, Boyer, PL, Kewalramani, VN, and Hughes, SH</p>

    <p>          J Virol <b>2006</b>.  80(5): 2578-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16474164&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16474164&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48487   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          In vitro selection of mutations in human immunodeficiency virus type 1 reverse transcriptase that confer resistance to capravirine, a novel nonnucleoside reverse transcriptase inhibitor</p>

    <p>          Sato, A, Hammond, J, Alexander, TN, Graham, JP, Binford, S, Sugita, KI, Sugimoto, H, Fujiwara, T, and Patick, AK</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16472877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16472877&amp;dopt=abstract</a> </p><br />

    <p>7.     48488   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Experimental/theoretical electrostatic properties of a styrylquinoline-type HIV-1 integrase inhibitor and its progenitors</p>

    <p>          Firley, D, Courcot, B, Gillet, JM, Fraisse, B, Zouhiri, F, Desmaele, D, d&#39;Angelo, J, and Ghermani, NE</p>

    <p>          J Phys Chem B Condens Matter Mater Surf Interfaces Biophys <b>2006</b>.  110(1): 537-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16471566&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16471566&amp;dopt=abstract</a> </p><br />

    <p>8.     48489   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          HIV entry inhibitors: mechanisms of action and resistance pathways</p>

    <p>          Briz, V, Poveda, E, and Soriano, V</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464888&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464888&amp;dopt=abstract</a> </p><br />

    <p>9.     48490   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluations of novel oxindoles as HIV-1 non-nucleoside reverse transcriptase inhibitors. Part 2</p>

    <p>          He, Y, Jiang, T, Kuhen, KL, Wolff, K, Yin, H, Bieza, K, Caldwell, J, Bursulaya, B, Tuntland, T, Zhang, K, and Karanewsky, D</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464578&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464578&amp;dopt=abstract</a> </p><br />

    <p>10.   48491   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Ultra-potent P1 modified arylsulfonamide HIV protease inhibitors: The discovery of GW0385</p>

    <p>          Miller, JF, Andrews, CW, Brieger, M, Furfine, ES, Hale, MR, Hanlon, MH, Hazen, RJ, Kaldor, I, McLean, EW, Reynolds, D, Sammond, DM, Spaltenstein, A, Tung, R, Turner, EM, Xu, RX, and Sherrill, RG</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458505&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458505&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48492   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          A novel soluble mimic of the glycolipid, globotriaosyl ceramide inhibits HIV infection</p>

    <p>          Lund, N, Branch, DR, Mylvaganam, M, Chark, D, Ma, XZ, Sakac, D, Binnington, B, Fantini, J, Puri, A, Blumenthal, R, and Lingwood, CA</p>

    <p>          AIDS <b>2006</b>.  20 (3): 333-343</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439866&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439866&amp;dopt=abstract</a> </p><br />

    <p>12.   48493   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          beta-Diketo acids with purine nucleobase scaffolds: Novel, selective inhibitors of the strand transfer step of HIV integrase</p>

    <p>          Nair, V, Uchil, V, and Neamati, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16439124&amp;dopt=abstract</a> </p><br />

    <p>13.   48494   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Pyridine N-oxide derivatives inhibit viral transactivation by interfering with NF-kappaB binding</p>

    <p>          Stevens, M, Pannecouque, C, De Clercq, E, and Balzarini, J</p>

    <p>          Biochem Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438940&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16438940&amp;dopt=abstract</a> </p><br />

    <p>14.   48495   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Inhibition of human immunodeficiency virus type 1 replication in latently infected cells by a novel IkappaB kinase inhibitor</p>

    <p>          Victoriano, AF, Asamitsu, K, Hibi, Y, Imai, K, Barzaga, NG, and Okamoto, T</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(2): 547-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436709&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436709&amp;dopt=abstract</a> </p><br />

    <p>15.   48496   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Dawn of non-nucleoside inhibitor-based anti-HIV microbicides</p>

    <p>          D&#39;Cruz, OJ and Uckun, FM</p>

    <p>          J Antimicrob Chemother <b>2006</b>.  57(3): 411-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16431862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16431862&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   48497   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          Structure-Based QSAR Analysis of a Set of 4-Hydroxy-5,6-dihydropyrones as Inhibitors of HIV-1 Protease: An Application of the Receptor-Dependent (RD) 4D-QSAR Formalism</p>

    <p>          Santos-Filho, OA and Hopfinger, AJ</p>

    <p>          J Chem Inf Model <b>2006</b>.  46(1): 345-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16426069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16426069&amp;dopt=abstract</a> </p><br />

    <p>17.   48498   HIV-LS-342; PUBMED-HIV-2/21/2006</p>

    <p class="memofmt1-2">          A Multivariate Analysis of HIV-1 Protease Inhibitors and Resistance Induced by Mutation</p>

    <p>          Almerico, AM, Tutone, M, Lauria, A, Diana, P, Barraja, P, Montalbano, A, Cirrincione, G, and Dattolo, G</p>

    <p>          J Chem Inf Model <b>2006</b>.  46(1): 168-79</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16426053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16426053&amp;dopt=abstract</a> </p><br />

    <p>18.   48499   HIV-LS-342; WOS-HIV-2/12/2006</p>

    <p class="memofmt1-2">          Influence of naturally occurring insertions in the fingers subdomain of human immunodeficiency virus type 1 reverse transcriptase on polymerase fidelity and mutation frequencies in vitro</p>

    <p>          Curr, K, Tripathi, S, Lennerstrand, J, Larder, BA, and Prasad, VR</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2006</b>.  87: 419-428, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234906800021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234906800021</a> </p><br />

    <p>19.   48500   HIV-LS-342; WOS-HIV-2/12/2006</p>

    <p class="memofmt1-2">          Concentricolide, an anti-HIV agent from the ascomycete Daldinia concentrica</p>

    <p>          Qin, XD, Dong, ZJ, Liu, JK, Yang, LM, Wang, RR, Zheng, YT, Lu, Y, Wu, YS, and Zheng, QT</p>

    <p>          HELVETICA CHIMICA ACTA <b>2006</b>.  89(1): 127-133, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234988100014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234988100014</a> </p><br />

    <p>20.   48501   HIV-LS-342; WOS-HIV-2/12/2006</p>

    <p class="memofmt1-2">          Preliminary mapping of a putative inhibitor-binding pocket for human immunodeficiency virus type 1 integrase inhibitors</p>

    <p>          Lee, DJ and Robinson, WE</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(1): 134-142, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234988000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234988000017</a> </p><br />

    <p>21.   48502   HIV-LS-342; WOS-HIV-2/19/2006</p>

    <p class="memofmt1-2">          Integrase HIV-1: inhibition of catalytic activity by modified oligonucleotides</p>

    <p>          Prikazchikova, T, Mouscadet, JF, and Gottikh, M</p>

    <p>          FEBS JOURNAL <b>2005</b>.  272: 17-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234826100056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234826100056</a> </p><br />
    <br clear="all">

    <p>22.   48503   HIV-LS-342; WOS-HIV-2/19/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro evaluation of some novel benzofuran derivatives as potential anti-HIVI-1, anticancer, and antimicrobial agents</p>

    <p>          Rida, SM, El-Hawash, SAM, Fahmy, HTY, Hazza, AA, and El-Meligy, MMM</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2006</b>.  29(1): 16-25, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235121200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235121200002</a> </p><br />

    <p>23.   48504   HIV-LS-342; WOS-HIV-2/19/2006</p>

    <p class="memofmt1-2">          Purification of a laccase from fruiting bodies of the mushroom Pleurotus eryngii</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          APPLIED MICROBIOLOGY AND BIOTECHNOLOGY <b>2006</b>.  69(5): 521-525, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235059000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235059000007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
