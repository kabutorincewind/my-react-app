

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-02-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MLg+1xnt1CDqecMkP4Fbv1+Ev8NatFq1GEuOJLI9rBiikiMCU99OzMgpMvYNSWgzYn1OskUUoDzs1Jv1wQeC+Wq0BEM+hV30Jbsl7YcTMGhhne/Kp7jgYv9RQiM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2B090DE0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: February 15 - February 28, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23439083">Antiviral Strategies Combining Antiretroviral Drugs with RNAi-mediated Attack on HIV-1 and Cellular Co-factors.</a> Boutimah, F., J.J. Eekels, Y.P. Liu, and B. Berkhout. Antiviral Research, 2013. <b>[Epub ahead of print]</b>. PMID[23439083].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23435291">Development of Sulphated and Naphthylsulfonated Carbosilane Dendrimers as Topical Microbicides to Prevent HIV-1 Sexual Transmission.</a> Cordoba, E.V., E. Arnaiz, M. Relloso, C. Sanchez-Torres, F. Garcia, L. Perez-Alvarez, R. Gomez, F.J. de la Mata, M. Pion, and M.A. Munoz-Fernandez. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[23435291].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23427154">Antibody Conjugation Approach Enhances Breadth and Potency of Neutralization of anti-HIV-1 Antibodies and CD4-IgG.</a> Gavrilyuk, J., H. Ban, H. Uehara, S. Sirk, K. Saye-Francisco, A. Cuevas, E. Zablowsky, A. Oza, M.S. Seaman, D.R. Burton, and C.F. Barbas, 3rd. Journal of Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23427154].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23412767">Purification and Characterization of an Antifungal Peptide with Potent Antifungal Activity but Devoid of Antiproliferative and HIV Reverse Transcriptase Activities from Legumi secchi Beans.</a> Lam, S.K. and T.B. Ng. Applied Biochemistry and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23412767].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23306052">Discovery of Novel Inhibitors of LEDGF/p75-IN Protein-protein Interactions.</a> Sanchez, T.W., B. Debnath, F. Christ, H. Otake, Z. Debyser, and N. Neamati. Bioorganic &amp; Medicinal Chemistry, 2013. 21(4): p. 957-963. PMID[23306052].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23429884">Highly Efficient Inhibition of Human Immunodeficiency Virus Type 1 Reverse Transcriptase by Aptamers Functionalized Gold Nanoparticles.</a> Shiang, Y.C., C.M. Ou, S.J. Chen, T.Y. Ou, H.J. Lin, C.C. Huang, and H.T. Chang. Nanoscale, 2013. <b>[Epub ahead of print]</b>. PMID[23429884].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23305922">Discovery of a Small-molecule Antiviral Targeting the HIV-1 Matrix Protein.</a> Zentner, I., L.J. Sierra, L. Maciunas, A. Vinnik, P. Fedichev, M.K. Mankowski, R.G. Ptak, J. Martin-Garcia, and S. Cocklin. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(4): p. 1132-1135. PMID[23305922].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23410170">Rationally Designed Multitarget anti-HIV Agents.</a> Zhan, P. and X. Liu. Current Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23410170].
    <br />
    <b>[PubMed]</b>. HIV_0215-022813.</p>

    <h2> </h2>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313393800017">Safety and Efficacy of Tenofovir/IQP-0528 Combination Gels - a Dual Compartment Microbicide for HIV-1 Prevention.</a> Dezzutti, C.S., C. Shetler, A. Mahalingam, S.R. Ugaonkar, G. Gwozdz, K.W. Buckheit, and R.W. Buckheit. Antiviral Research, 2012. 96(2): p. 221-225. ISI[000313393800017].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313810100002">Preclinical Evaluation of HIV Eradication Strategies in the Simian Immunodeficiency Virus-infected Rhesus Macaque: A Pilot Study Testing Inhibition of Indoleamine 2,3-dioxygenase.</a> Dunham, R.M., S.N. Gordon, M. Vaccari, M. Piatak, Y. Huang, S.G. Deeks, J. Lifson, G. Franchini, and J.M. McCune. AIDS Research and Human Retroviruses, 2013. 29(2): p. 207-214. ISI[000313810100002].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313393800018">In Vitro Profiling of the Vaginal Permeation Potential of anti-HIV Microbicides and the Influence of Formulation Excipients.</a> Grammen, C., P. Augustijns, and J. Brouwers. Antiviral Research, 2012. 96(2): p. 226-233. ISI[000313393800018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313610600008">PEGylation of a Proprotein Convertase Peptide Inhibitor for Vaginal Route of Drug Delivery: In Vitro Bioactivity, Stability and in Vivo Pharmacokinetics.</a> Ho, H.T., T.L. Nero, H. Singh, M.W. Parker, and G.Y. Nie. Peptides, 2012. 38(2): p. 266-274. ISI[000313610600008].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314139600004">In Vitro Antibacterial and Antiviral Activities of Some Novel 1,3,4-Thiadiazole Derivatives.</a> Joshi, S.D., U.A. More, S. Dixit, G.S. Gadaginamath, and V.H. Kulkarni. Indian Journal of Heterocyclic Chemistry, 2012. 22(2): p. 109-114. ISI[000314139600004].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314032900017">4862F, a New Inhibitor of HIV-1 Protease, from the Culture of Streptomyces I03A-04862.</a> Liu, X., M.L. Gan, B. Dong, T. Zhang, Y.Z. Li, Y.Q. Zhang, X.Y. Fan, Y.X. Wu, S.K. Bai, M.H. Chen, L.Y. Yu, P.Z. Tao, W. Jiang, and S.Y. Si. Molecules, 2013. 18(1): p. 236-243. ISI[000314032900017].
    <br />
    <b>[WOS]</b>. HIV_0215-022813</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313661900003">A Phenotypic Assay to Identify Chikungunya Virus Inhibitors Targeting the Nonstructural Protein nsP2.</a> Lucas-Hourani, M., A. Lupan, P. Despres, S. Thoret, O. Pamlard, J. Dubois, C. Guillou, F. Tangy, P.O. Vidalain, and H. Munier-Lehmann. Journal of Biomolecular Screening, 2013. 18(2): p. 172-179. ISI[000313661900003].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313851000039">Nanoencapsulation and Characterization of Zidovudine on Poly(L-lactide) and Poly(L-lactide)-poly(ethylene glycol)-blend Nanoparticles.</a> Mainardes, R.M. and M.P.D. Gremiao. Journal of Nanoscience and Nanotechnology, 2012. 12(11): p. 8513-8521. ISI[000313851000039].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313601100025">Synthesis, Spectroscopy and Computational Studies of Selected Hydroxyquinoline Carboxylic Acids and Their Selected Fluoro-, Thio-, and Dithioanalogues.</a> Nycz, J.E. and G.J. Malecki. Journal of Molecular Structure, 2013. 1032: p. 159-168. ISI[000313601100025].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313393800008">Poly (4-styrenesulfonic acid-co-maleic acid) Is an Entry Inhibitor against Both HIV-1 and HSV Infections - Potential as a Dual Functional Microbicide.</a> Qiu, M., Y. Chen, S.W. Song, H.Y. Song, Y. Chu, Z.P. Yuan, L. Cheng, D.T. Zheng, Z.W. Chen, and Z.W. Wu. Antiviral Research, 2012. 96(2): p. 138-147. ISI[000313393800008].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313475000015">Viral Enzymes Containing Magnesium: Metal Binding as a Successful Strategy in Drug Design.</a> Rogolino, D., M. Carcelli, M. Sechi, and N. Neamati. Coordination Chemistry Reviews, 2012. 256(23-24): p. 3063-3086. ISI[000313475000015].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313696900013">SiO2 Nanoparticles as Platform for Delivery of Nucleoside Triphosphate Analogues into Cells.</a> Vasilyeva, S.V., V.N. Silnikov, N.V. Shatskaya, A.S. Levina, M.N. Repkova, and V.F. Zarytova. Bioorganic &amp; Medicinal Chemistry, 2013. 21(3): p. 703-711. ISI[000313696900013].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000314003800010">Contribution of SAM and HD Domains to Retroviral Restriction Mediated by Human SAMHD1.</a> White, T.E., A. Brandariz-Nunez, J.C. Valle-Casuso, S. Amie, L. Nguyen, B. Kim, J. Brojatsch, and F. Diaz-Griffero. Virology, 2013. 436(1): p. 81-90. ISI[000314003800010].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313661900001">Parallel Screening of Low Molecular Weight Fragment Libraries: Do Differences in Methodology Affect Hit Identification.</a> Wielens, J., S.J. Headey, D.I. Rhodes, R.J. Mulder, O. Dolezal, J.J. Deadman, J. Newman, D.K. Chalmers, M.W. Parker, T.S. Peat, and M.J. Scanlon. Journal of Biomolecular Screening, 2013. 18(2): p. 147-159. ISI[000313661900001].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313618800031">Effective Inhibition of Human Immunodeficiency Virus 1 Replication by Engineered RNase P Ribozyme.</a> Zeng, W.B., Y.C. Chen, Y. Bai, P. Trang, G.P. Vu, S.W. Lu, J.G. Wu, and F.Y. Liu. Plos One, 2012. 7(12). ISI[000313618800031].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000313530200012">Three-dimensional Quantitative Structure-activity Relationship Models of HIV-1 Integrase Inhibitors of DKAs.</a> Zhang, M.Q., W.N. Zhao, and S.Y. Lu. Chinese Journal of Structural Chemistry, 2012. 31(12): p. 1769-1781. ISI[000313530200012].
    <br />
    <b>[WOS]</b>. HIV_0215-022813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
