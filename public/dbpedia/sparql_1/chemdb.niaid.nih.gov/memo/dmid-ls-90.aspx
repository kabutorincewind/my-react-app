

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-90.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BqDernGbpRv8PSYcHIA45s8el2/qPulNXVL1ScOCdBgStviDPaPp8F26QK2++e/4pHgVc+vhxxaFglrIiVE7fjU+AzWM66NIdgFtPTN4WolGy+b3JVuTTH2Z4II=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE7AD41E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-90-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56318   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Spectrum of antiviral activity of o-(acetoxyphenyl)hept-2-ynyl sulphide (APHS)</p>

    <p>          Pereira, Candida F, Rutten, Karla, Stranska, Ruzena, Huigen, Marleen CDG, Aerts, Piet C, de Groot, Raoul J, Egberink, Herman F, Schuurman, Rob, and Nottet, Hans SLM</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4FX23FV-1/2/28b9812c3725d3ddc542f22baedd31d1">http://www.sciencedirect.com/science/article/B6T7H-4FX23FV-1/2/28b9812c3725d3ddc542f22baedd31d1</a> </p><br />

    <p>2.     56319   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expression, purification, and characterization of SARS coronavirus RNA polymerase</p>

    <p>          Cheng, Ao, Zhang, Wei, Xie, Youhua, Jiang, Weihong, Arnold, Eddy, Sarafianos, Stefan G, and Ding, Jianping</p>

    <p>          Virology <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4FSK7NJ-1/2/c0a504b145563e82a571ac723357e786">http://www.sciencedirect.com/science/article/B6WXR-4FSK7NJ-1/2/c0a504b145563e82a571ac723357e786</a> </p><br />

    <p>3.     56320   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          How does ribavirin improve interferon-[alpha] response rates in hepatitis C virus infection?</p>

    <p>          Pawlotsky, Jean-Michel</p>

    <p>          Journal of Hepatology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4FWK2VC-1/2/007fdcf4f35c970ec98eeb755b0d5e9e">http://www.sciencedirect.com/science/article/B6W7C-4FWK2VC-1/2/007fdcf4f35c970ec98eeb755b0d5e9e</a> </p><br />

    <p>4.     56321   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C Virus and the Related Bovine Viral Diarrhea Virus Considerably Differ in the Functional Organization of the 5 &#39; Non-Translated Region: Implications for the Viral Life Cycle</p>

    <p>          Grassmann, C. <i> et al.</i></p>

    <p>          Virology <b>2005</b>.  333(2): 349-366</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450300015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450300015</a> </p><br />

    <p>5.     56322   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus and the related bovine viral diarrhea virus considerably differ in the functional organization of the 5&#39; non-translated region: implications for the viral life cycle</p>

    <p>          Wilhelm Grassmann, Claus, Yu, Haiying, Isken, Olaf, and Behrens, Sven-Erik</p>

    <p>          Virology <b>2005</b>.  333(2): 349-366</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4FD79MP-1/2/c1ad5091d560159e6370efee2e26ac49">http://www.sciencedirect.com/science/article/B6WXR-4FD79MP-1/2/c1ad5091d560159e6370efee2e26ac49</a> </p><br />
    <br clear="all">

    <p>6.     56323   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Combination of nucleoside analogues in the treatment of chronic hepatitis B virus infection: lesson from experimental models</p>

    <p>          Zoulim, F</p>

    <p>          J Antimicrob Chemother <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15814602&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15814602&amp;dopt=abstract</a> </p><br />

    <p>7.     56324   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus-related resistance mechanisms to interferon [alpha]-based antiviral therapy</p>

    <p>          Hofmann, Wolf Peter, Zeuzem, Stefan, and Sarrazin, Christoph</p>

    <p>          Journal of Clinical Virology <b>2005</b>.  32(2): 86-91</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4F4HB1P-1/2/8a627f9c69dadd98b50b810896c09dad">http://www.sciencedirect.com/science/article/B6VJV-4F4HB1P-1/2/8a627f9c69dadd98b50b810896c09dad</a> </p><br />

    <p>8.     56325   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory effect of jaceosidin isolated from Artemisiaargyi on the function of E6 and E7 oncoproteins of HPV 16</p>

    <p>          Lee, HG, Yu, KA, Oh, WK, Baeg, TW, Oh, HC, Ahn, JS, Jang, WC, Kim, JW, Lim, JS, Choe, YK, and Yoon, DY</p>

    <p>          J Ethnopharmacol <b>2005</b>.  98(3): 339-343</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15814270&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15814270&amp;dopt=abstract</a> </p><br />

    <p>9.     56326   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rational design of dual-functional aptamers that inhibit the protease and helicase activities of HCV NS3</p>

    <p>          Umehara, T, Fukuda, K, Nishikawa, F, Kohara, M, Hasegawa, T, and Nishikawa, S</p>

    <p>          J Biochem (Tokyo) <b>2005</b>.  137(3): 339-347</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809335&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809335&amp;dopt=abstract</a> </p><br />

    <p>10.   56327   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Zidampidine, an aryl phosphate derivative of AZT: in vivo pharmacokinetics, metabolism, toxicity, and anti-viral efficacy against hemorrhagic fever caused by Lassa virus</p>

    <p>          Uckun, FM, Venkatachalam, TK, Erbeck, D, Chen, CL, Petkevich, AS, and Vassilev, A</p>

    <p>          Bioorg Med Chem <b>2005</b>.  13(9): 3279-3288</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809163&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   56328   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mizoribine inhibits hepatitis C virus RNA replication: Effect of combination with interferon-alpha</p>

    <p>          Naka, K, Ikeda, M, Abe, K, Dansako, H, and Kato, N</p>

    <p>          Biochem Biophys Res Commun <b>2005</b>.  330(3): 871-879</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809077&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15809077&amp;dopt=abstract</a> </p><br />

    <p>12.   56329   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Biocatalysed synthesis of [beta]-O-glucosides from 9-fluorenon-2-carbohydroxyesters. Part 3: IFN-inducing and anti-HSV-2 properties</p>

    <p>          Alcaro, Stefano, Arena, Adriana, Di Bella, Rosaria, Neri, Simonetta, Ottana, Rosaria, Ortuso, Francesco, Pavone, Bernadette, Trincone, Antonio, and Vigorita, Maria Gabriella</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FWT3V5-2/2/2147175964d296d7ef48684249328b54">http://www.sciencedirect.com/science/article/B6TF8-4FWT3V5-2/2/2147175964d296d7ef48684249328b54</a> </p><br />

    <p>13.   56330   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Dynamics of the antiviral activity of N-methanocarbathymidine against herpes simplex virus type 1 in cell culture</p>

    <p>          Huleihel, Mahmoud, Talishanisky, Marina, Ford, Jr Harry, Marquez, Victor E, Kelley, James A, Johns, David G, and Agbaria, Riad</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4FWKDRM-1/2/136ce4e66bd5b2729428fd23e03db1aa">http://www.sciencedirect.com/science/article/B6T7H-4FWKDRM-1/2/136ce4e66bd5b2729428fd23e03db1aa</a> </p><br />

    <p>14.   56331   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          From 1-acyl-beta-lactam human cytomegalovirus protease inhibitors to 1-benzyloxycarbonylazetidines with improved antiviral activity. A straightforward approach to convert covalent to noncovalent inhibitors</p>

    <p>          Gerona-Navarro, G, Perez, de Vega MJ, Garcia-Lopez, MT, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, and Gonzalez-Muniz, R</p>

    <p>          J Med Chem <b>2005</b>.  48(7): 2612-2621</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801851&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801851&amp;dopt=abstract</a> </p><br />

    <p>15.   56332   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening and Characterization of Aptamers of Hepatitis C Virus Ns3 Helicase</p>

    <p>          Zhan, L. <i>et al.</i></p>

    <p>          Progress in Biochemistry and Biophysics <b>2005</b>.  32(3): 245-250</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227789000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227789000010</a> </p><br />
    <br clear="all">

    <p>16.   56333   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pitx2a Binds to Human Papillomavirus (Hpv) E6 Protein and Inhibits E6-Mediated P53 Degradation in Hela Cells</p>

    <p>          Wei, Q.</p>

    <p>          Molecular Biology of the Cell <b>2004</b>.  15: 250A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224648802108">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224648802108</a> </p><br />

    <p>17.   56334   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mining SARS-CoV protease cleavage data using non-orthogonal decision trees, a novel method for decisive template selection</p>

    <p>          Yang, ZR</p>

    <p>          Bioinformatics  <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15797903&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15797903&amp;dopt=abstract</a> </p><br />

    <p>18.   56335   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of dengue virus serotypes 1 to 4 in vero cell cultures with morpholino oligomers</p>

    <p>          Kinney, RM, Huang, CY, Rose, BC, Kroeker, AD, Dreher, TW, Iversen, PL, and Stein, DA</p>

    <p>          J Virol <b>2005</b> .  79(8): 5116-5128</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795296&amp;dopt=abstract</a> </p><br />

    <p>19.   56336   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a Mouse Encephalitis Model to Measles Virus for in Vivo Evaluation of Antiviral Activity of Ribavirin Complexed With Cyclodextrins</p>

    <p>          Grancher, N. <i>et al.</i></p>

    <p>          International Journal of Antimicrobial Agents <b>2004</b>.  24: S244</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225734000543">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225734000543</a> </p><br />

    <p>20.   56337   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p><b>          Development of Cell-Based Assays for In Vitro Characterization of Hepatitis C Virus NS3/4A Protease Inhibitors</b> </p>

    <p>          Chung, V, Carroll, AR, Gray, NM, Parry, NR, Thommes, PA, Viner, KC, and D&#39;Souza, EA</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(4): 1381-1390</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793116&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15793116&amp;dopt=abstract</a> </p><br />

    <p>21.   56338   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Should There Be Stockpiles of Antiviral Drugs and Pneumococcal Vaccine in Europe in Anticipation on the Next Influenza Pandemic?</p>

    <p>          Duerden, B.</p>

    <p>          European Journal of Public Health <b>2004</b>.  14(4): 55-56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227034700143">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227034700143</a> </p><br />
    <br clear="all">

    <p>22.   56339   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Growth Kinetics of Sars-Coronavirus in Vero E6 Cells</p>

    <p>          Keyaerts, E. <i>et al.</i></p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  329(3): 1147-1151</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227885800047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227885800047</a> </p><br />

    <p>23.   56340   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          2-Ethoxybenzoxazole as a bioisosteric replacement of an ethyl benzoate group in a human rhinovirus (HRV) capsid binder</p>

    <p>          Brown, Renee N, Cameron, Rachel, Chalmers, David K, Hamilton, Stephanie, Luttick, Angela, Krippner, Guy Y, McConnell, Darryl B, Nearn, Roland, Stanislawski, Pauline C, Tucker, Simon P, and Watson, Keith G</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(8): 2051-2055</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FPYWP1-7/2/55573d4322b2ce2bb896d9dfc709d205">http://www.sciencedirect.com/science/article/B6TF9-4FPYWP1-7/2/55573d4322b2ce2bb896d9dfc709d205</a> </p><br />

    <p>24.   56341   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amantadine inhibits hepatitis A virus internal ribosomal entry site-mediated translation in human hepatoma cells</p>

    <p>          Kanda, Tatsuo, Yokosuka, Osamu, Imazeki, Fumio, Fujiwara, Keiichi, Nagao, Keiichi, and Saisho, Hiromitsu</p>

    <p>          Biochemical and Biophysical Research Communications <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4FX0Y71-3/2/30ee33f4a9d93335a8059fffc70121db">http://www.sciencedirect.com/science/article/B6WBK-4FX0Y71-3/2/30ee33f4a9d93335a8059fffc70121db</a> </p><br />

    <p>25.   56342   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Marburg virus protein expression and viral release by RNA interference</p>

    <p>          Fowler, T, Bamberg, S, Moller, P, Klenk, HD, Meyer, TF, Becker, S, and Rudel, T</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 4): 1181-1188</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784912&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784912&amp;dopt=abstract</a> </p><br />

    <p>26.   56343   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Leflunomide for polyomavirus type BK nephropathy</p>

    <p>          Williams, JW,  Javaid, B, Kadambi, PV, Gillen, D, Harland, R, Thistlewaite, JR, Garfinkel, M, Foster, P, Atwood, W, Millis, JM, Meehan, SM, and Josephson, MA</p>

    <p>          N Engl J Med <b>2005</b>.  352(11): 1157-1158</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784677&amp;dopt=abstract</a> </p><br />

    <p>27.   56344   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Activity of Human Lactoferrin: Inhibition of Alphavirus Interaction With Heparan Sulfate</p>

    <p>          Waarts, B. <i>et al.</i></p>

    <p>          Virology <b>2005</b>.  333(2): 284-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227450300009</a> </p><br />

    <p>28.   56345   DMID-LS-90; PUBMED-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Advances on cyclin-dependent kinases (CDKs) as novel targets for antiviral drugs</p>

    <p>          Schang, LM</p>

    <p>          Curr Drug Targets Infect Disord <b>2005</b>.  5(1): 29-37</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15777196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15777196&amp;dopt=abstract</a> </p><br />

    <p>29.   56346   DMID-LS-90; WOS-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Putative Structure and Function of Orf3 in Sars Coronavirus</p>

    <p>          Zhang, X. and Yap, Y.</p>

    <p>          Journal of Molecular Structure-Theochem <b>2005</b>.  715(1-3): 55-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227590100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227590100008</a> </p><br />

    <p>30.   56347   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of potent and selective inhibitors of Candida albicans N-myristoyltransferase based on the benzothiazole structure</p>

    <p>          Yamazaki, Kazuo, Kaneko, Yasushi, Suwa, Kie, Ebara, Shinji, Nakazawa, Kyoko, and Yasuno, Kazuhiro</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(7): 2509-2522</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4FH0D6M-2/2/af50e1883f94ddf2d2ca1cfe5ee2336e">http://www.sciencedirect.com/science/article/B6TF8-4FH0D6M-2/2/af50e1883f94ddf2d2ca1cfe5ee2336e</a> </p><br />

    <p>31.   56348   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure-based design of a new class of highly selective aminoimidazo[1,2-a]pyridine-based inhibitors of cyclin dependent kinases</p>

    <p>          Hamdouchi, Chafiq, Zhong, Boyu, Mendoza, Jose, Collins, Elizabeth, Jaramillo, Carlos, De Diego, Jose Eugenio, Robertson, Daniel, Spencer, Charles D, Anderson, Bryan D, and Watkins, Scott A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(7): 1943-1947</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4FGX7X8-2/2/72b32bd060858833c2b03bfb7e18cb03">http://www.sciencedirect.com/science/article/B6TF9-4FGX7X8-2/2/72b32bd060858833c2b03bfb7e18cb03</a> </p><br />

    <p>32.   56349   DMID-LS-90; EMBASE-DMID-4/12/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          [alpha]-Glucosidase inhibition of 6-hydroxyflavones. Part 3: Synthesis and evaluation of 2,3,4-trihydroxybenzoyl-containing flavonoid analogs and 6-aminoflavones as [alpha]-glucosidase inhibitors</p>

    <p>          Gao, Hong and Kawabata, Jun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(5): 1661-1671</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4F4NY7H-1/2/a5937245e2ab7c3eafd35ec977e1515b">http://www.sciencedirect.com/science/article/B6TF8-4F4NY7H-1/2/a5937245e2ab7c3eafd35ec977e1515b</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
