

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-02-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3EbqqC5NoTSXYK2ZkyqFncvrRkA5czmuqJF3EzLFUcfFQcNcI+1Ak8LA/Z2rAwWYiqn6XBkaJHadxKN5RQzYjBe9LZY5D/JMpPiKEwqalT3lXyxg4ZEx08wRExQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="221269EF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">February 3 - February 16, 2012</h1>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22305613">Synthesis, anti-HBV Activity and Renal Cell Toxicity Evaluation of Mixed Phosphonate Prodrugs of Adefovir.</a> Fu, X.Z., Y. Ou, J.Y. Pei, Y. Liu, J. Li, W. Zhou, Y.Y. Lan, A.M. Wang, and Y.L. Wang, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22305613].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2">HCMV</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22303989">Design and Synthesis of Novel 1&#39;,3&#39;-Dioxolane 5&#39;-deoxyphosphonic acid Purine Analogues as Potent Antiviral Agents.</a> Kim, E., L.J. Liu, W. Lee, and J.H. Hong, Nucleosides, Nucleotides &amp; Nucleic Acids, 2012. 31(2): p. 85-96; PMID[22303989].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22297735">Antiviral Function and Efficacy of Polyvalent Immunoglobulin Products against CMV isolates in different Human Cell Lines.</a> Frenzel, K., S. Ganepola, D. Michel, E. Thiel, D.H. Kruger, L. Uharek, and J. Hofmann, Medical Microbiology and Immunology, 2012. <b>[Epub ahead of print]</b>; PMID[22297735].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2">HSV-1</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22313053">Antiviral Properties of Polymeric Aziridine- and Biguanide- Modified Core-shell Magnetic Nanoparticles.</a> Bromberg, L., D.J. Bromberg, T.A. Hatton, I. Bandin, A. Concheiro, and C. Alvarez-Lorenzo, Langmuir : the ACS Journal of Surfaces and Colloids, 2012. <b>[Epub ahead of print]</b>; PMID[22313053].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22312346">Antiviral and Virucidal Activities of N-alpha-Cocoyl-L-arginine ethyl ester.</a> Yamasaki, H., K. Tsujimoto, K. Ikeda, Y. Suzuki, T. Arakawa, and A.H. Koyama, Advances in Virology, 2011. 2011: p. 572868; PMID[22312346].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">HSV-2</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22318977">Bioactivity-guided Fractionation of Phyllanthus orbicularis and Identification of the Principal Anti HSV-2 Compounds</a>. Alvarez, A.L., K.P. Dalton, I. Nicieza, Y. Dineiro, A. Picinelli, S. Melon, A. Roque, B. Suarez, and F. Parra, Phytotherapy Research : PTR, 2012. <b>[Epub ahead of print]</b>; PMID[22318977].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298549800036">Antiviral and Antifungal Activity of Some Dermaseptin S4 Analogues.</a> Belaid, A. and K. Hani, African Journal of Biotechnology, 2011. 10(66): p. 14962-14967; ISI[000298549800036].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298928300007">Polyhydroxylated Steroids from the Bamboo Coral Isis hippuris</a>. Chen, W.H., S.K. Wang, and C.Y. Duh, Marine Drugs, 2011. 9(10): p. 1829-1839; ISI[000298928300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298927100001">New Cembranolides from the Dongsha Atoll Soft Coral Lobophytum durum</a>. Cheng, S.Y., P.W. Chen, H.P. Chen, S.K. Wang, and C.Y. Duh, Marine Drugs, 2011. 9(8): p. 1307-1318; ISI[000298927100001].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298404900006">Resistance Analysis and Characterization of a Thiazole Analogue, BP008, as a Potent Hepatitis C Virus NS5A Inhibitor</a>. Lin, H.M., J.C. Wang, H.S. Hu, P.S. Wu, C.C. Yang, C.P. Wu, S.Y. Pu, T.A. Hsu, W.T. Jiaang, Y.S. Chao, J.H. Chern, T.K. Yeh, and A. Yueh, Antimicrobial Agents and Chemotherapy, 2012. 56(1): p. 44-53; ISI[000298404900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0203-021612.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299238600034">Synthesis of New 4, 5-Dihydrofuranoindoles and Their Evaluation as HCV NS5B Polymerase Inhibitors</a>. Velazquez, F., S. Venkatraman, C.A. Lesburg, J. Duca, S.B. Rosenblum, J.A. Kozlowski, and F.G. Njoroge, Organic Letters, 2012. 14(2): p. 556-559; ISI[000299238600034].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0203-021612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
