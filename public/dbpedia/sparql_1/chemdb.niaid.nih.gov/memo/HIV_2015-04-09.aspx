

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-04-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="w29zhNDPYCPNwqPkHj8ECWq56jUyxlLDbzsBSHbSDc5YFo5nBKmnX8ANiOMBTPkZeiQP7oq8sC9x8vkQCeawHqcxUJ5b7KnOdQ6rb/l21irMWTdebfVUZnuqPSw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9366441B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: March 27 - April 9, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25740376">From the Traditional Chinese Medicine Plant Schisandra chinensis New Scaffolds Effective on HIV-1 Reverse Transcriptase Resistant to Non-nucleoside Inhibitors.</a> Xu, L., N. Grandi, C. Del Vecchio, D. Mandas, A. Corona, D. Piano, F. Esposito, C. Parolin, and E. Tramontano. Journal of Microbiology, 2015. 53(4): p. 288-293. PMID[25740376]. <b><br />
    [PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25705792">miR-146a Controls CXCR4 Expression in a Pathway That Involves PLZF and Can Be Used to Inhibit HIV-1 Infection of CD4(+) T Lymphocytes.</a> Quaranta, M.T., E. Olivetta, M. Sanchez, I. Spinello, R. Paolillo, C. Arenaccio, M. Federico, and C. Labbaye. Virology, 2015. 478: p. 27-38. PMID[25705792].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25798893">Synthesis, Conformational Studies, and Biological Properties of Phosphonomethoxyethyl Derivatives of Nucleobases with a Locked Conformation via a Pyrrolidine Ring.</a> Pohl, R., L. Postova Slavetinska, W.S. Eng, D.T. Keough, L.W. Guddat, and D. Rejman. Organic &amp; Biomolecular Chemistry, 2015.</p>

    <p class="plaintext">13(16): p. 4693-4705. PMID[25798893]. <b><br />
    [PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25800792">Investigations of Possible Prodrug Structures for 2-(2-Mercaptophenyl)tetrahydropyrimidines: Reductive Conversion from anti-HIV Agents with Pyrimidobenzothiazine and Isothiazolopyrimidine Scaffolds.</a> Okazaki, S., S. Oishi, T. Mizuhara, K. Shimura, H. Murayama, H. Ohno, M. Matsuoka, and N. Fujii. Organic &amp; Biomolecular Chemistry, 2015. 13(16): p. 4706-4713. PMID[25800792].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25744188">Identification of anti-HIV Agents with a Novel Benzo[4,5]isothiazolo[2,3-a]Pyrimidine Scaffold.</a> Okazaki, S., T. Mizuhara, K. Shimura, H. Murayama, H. Ohno, S. Oishi, M. Matsuoka, and N. Fujii. Bioorganic &amp; Medicinal Chemistry, 2015. 23(7): p. 1447-1452. PMID[25744188]. <b><br />
    [PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25820360">A Trypsin Inhibitor from Rambutan Seeds with Antitumor, anti-HIV-1 Reverse Transcriptase, and Nitric Oxide-inducing Properties</a>. Fang, E.F. and T.B. Ng. Applied Biochemistry and Biotechnology, 2015. 175(8): p. 3828-3839. PMID[25820360]. <b><br />
    [PubMed]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25583709">Insights into the Mechanism of Inhibition of CXCR4: Identification of Piperidinylethanamine Analogs as anti-HIV-1 Inhibitors.</a> Das, D., K. Maeda, Y. Hayashi, N. Gavande, D.V. Desai, S.B. Chang, A.K. Ghosh, and H. Mitsuya. Antimicrobial Agents and Chemotherapy, 2015. 59(4): p. 1895-1904. PMID[25583709]. <b><br />
    [PubMed]</b>. HIV_0327-040915.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350853400001">The CRISPR/Cas9 System Inactivates Latent HIV-1 Proviral DNA.</a> Zhu, W.J., R.Y. Lei, Y. Le Duff, J. Li, F. Guo, M.A. Wainberg, and C. Liang. Retrovirology, 2015. 12(22): 7pp. ISI[000350853400001]. <b><br />
    [WOS.]</b> HIV_0327-040915.<b><br />
    <br /></b></p>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350143000025">Acyclovir Phosphoramidates as Potential anti-HIV Drugs.</a> Zakirova, N.F., I.L. Karpenko, M.M. Prokofjeva, C. Vanpouille, V.S. Prassolov, A.V. Shipitsyn, and S.N. Kochetkov. Russian Chemical Bulletin, 2014. 63(5): p. 1192-1196. ISI[000350143000025]. <b><br />
    [WOS.]</b> HIV_0327-040915.<b><br />
    <br /></b></p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350328600021">Sesquiterpenoid Tropolone Glycosides from Liriosma ovata.</a> Ma, J., R.S. Pawar, E. Grundel, E.P. Mazzola, C.D. Ridge, T. Masaoka, S.F.J. Le Grice, J. Wilson, J.A. Beutler, and A.J. Krynitsky. Journal of Natural Products, 2015. 78(2): p. 315-319. ISI[000350328600021]. <b><br />
    [WOS.]</b> HIV_0327-040915.<b><br />
    <br /></b></p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000349972700010">Diastereoselective Synthesis of (1,3-Dioxan-4-yl) Pyrimidine and Purin nucleoside Analogues.</a> Battisti, U.M., C. Sorbi, A. Quotadamo, S. Franchini, A. Tait, D. Schols, L.S. Jeong, S.K. Lee, J. Song, and L. Brasili. European Journal of Organic Chemistry, 2015(6): p. 1235-1245. ISI[000349972700010]. <b><br />
    [WOS.]</b> HIV_0327-040915.
    <br />
    <br /></p>

    <p class="plaintext"><a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000350518400002">12. Synthesis, Structure-Activity Relationship and Molecular Docking of Cyclohexenone Based Analogous as Potent Non-Nucleoside Reverse-Transcriptase Inhibitors.</a> Nazar, M.F., M.I. Abdullah, A. Badshah, A. Mahmood, U.A. Rana, and S.U.D. Khan. Journal of Molecular Structure, 2015. 1086: p. 8-16. ISI[000350518400002].
    <br />
    <b>[WOS]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000351095400009">Discovery of Novel 5-Fluoro-N-2,N(4-)diphenylpyrimidine-2,4-diamines as Potent Inhibitors against CDK2 and CDK9.</a> Gao, J.D., C. Fang, Z.Y. Xiao, L. Huang, C.H. Chen, L.T. Wang, and K.H. Lee. MedChemComm, 2015. 6(3): p. 444-454. ISI[000351095400009].
    <br />
    <b>[WOS]</b>. HIV_0327-040915.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150312&amp;CC=WO&amp;NR=2015035003A1&amp;KC=A1">Thioether Prodrug Compositions as anti-HIV and Anti-retroviral Agents.</a> Appella, D., P. Kumar, N. Shank, and M.D. Hassink. Patent. 2015. 2014-US54019 2015035003: 45pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150305&amp;CC=WO&amp;NR=2015027334A1&amp;KC=A1">Antisense-based Small RNA Agents Targeting the Gag Open Reading Frame of HIV-1 RNA.</a> Gatignol, A. and R. Scarborough. Patent. 2015. 2014-CA50814 2015027334: 66pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150312&amp;CC=US&amp;NR=2015072958A1&amp;KC=A1">Preparation of gem-Difluoro Heterobicyclic Compounds as HIV-1 Protease Inhibitors.</a> Ghosh, A.K., H. Mitsuya, and S. Yashchuk. Patent. 2015. 2014-14483043 20150072958: 46pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">17. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150319&amp;CC=WO&amp;NR=2015038596A1&amp;KC=A1">Preparation of Amino acid-containing Nucleotide and Nucleoside Triphosphates as Antiviral Agents.</a> Liotta, D.C., G.R. Painter, G.R. Bluemling, and A. De La Rosa. Patent. 2015. 2014-US54930 2015038596: 331pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0327-040915.</p>

    <br />

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20150304&amp;CC=CN&amp;NR=104387379A&amp;KC=A">Preparation of Triazole Derivatives as anti-AIDS Agents.</a> Lu, S., H. Zhang, J. Yu, and X. Yu. Patent. 2015. 2014-10696031 104387379: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0327-040915.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
