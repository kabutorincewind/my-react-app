

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-05-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="nUlbN345bu4QfIvCxBE3dRWWtyeMGIu4VviY1+AAQxPnmVjeL71Vbcw1riy242kAFG6eic0/sHWr0SzcdLT38zxLr8Ei/SocQSidvWw2NpKzj3+P11OZj3Vw/+M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="410F6E5E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: May 10 - May 23, 2013</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23684750">Thromboxane A2 Synthase Inhibitors Prevent Production of Infectious Hepatitis C Virus in Mice with Humanized Livers</a><i>.</i> Abe, Y., H.H. Aly, N. Hiraga, M. Imamura, T. Wakita, K. Shimotohno, K. Chayama, and M. Hijikata. Gastroenterology, 2013. <b>[Epub ahead of print]</b>; PMID[23684750].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23672640">Discovery of Danoprevir (ITMN-191/R7227), a Highly Selective and Potent Inhibitor of Hepatitis C Virus (HCV) NS3/4A Protease</a><i>.</i> Jiang, Y., S.W. Andrews, K. Condroski, B. Buckman, V. Serebryany, S. Wenglowsky, A. Kennedey, M. Madduru, B. Wang, M. Lyon, G. Doherty, B. Woodard, C. Lemieux, M.G. Do, H. Zhang, J.A. Ballard, G. Vigers, B. Brandhuber, P. Stengel, J. Josey, L. Beigelman, L. Blatt, and S.D. Seiwert. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23672640].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23583031">Analogs Design, Synthesis and Biological Evaluation of Peptidomimetics with Potential anti-HCV Activity</a><i>.</i> Lasheen, D.S., M.A. Ismail, D.A. Abou El Ella, N.S. Ismail, S. Eid, S. Vleck, J.S. Glenn, A.G. Watts, and K.A. Abouzid. Bioorganic &amp; Medicinal Chemistry, 2013. 21(10): p. 2742-2755; PMID[23583031].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23672667">Discovery of a Potent Boronic acid Derived Inhibitor of the HCV RNA-dependent RNA Polymerase</a><i>.</i> Maynard, A., R.M. Crosby, B. Ellis, R. Hamatake, Z. Hong, B.A. Johns, K.M. Kahler, C. Koble, A.L. Leivers, M.R. Leivers, A. Mathis, A.J. Peat, J.J. Pouliot, C.D. Roberts, V. Samano, R.M. Schmidt, G.K. Smith, A. Spaltenstein, E.L. Stewart, P. Thommes, E.M. Turner, C. Voitenleitner, J.T. Walker, G. Waitt, J. Weatherhead, K.L. Weaver, S. Williams, L. Wright, Z.Z. Xiong, D. Haigh, and J.B. Shotwell. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23672667].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23688081">Resistance Mutations against HCV Protease Inhibitors and Antiviral Drug Design</a><i>.</i> Shang, L., K. Lin, and Z. Yin. Current Pharmaceutical Design, 2013. <b>[Epub ahead of print]</b>; PMID[23688081].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23662817">Development of Oleanane-type Triterpenes as a New Class of HCV Entry Inhibitors</a><i>.</i> Yu, F., Q. Wang, Z. Zhang, Y.Y. Peng, Y.Y. Qiu, Y.Y. Shi, Y.X. Zheng, S.L. Xiao, H. Wang, X.X. Huang, L. Zhu, K.B. Chen, C.K. Zhao, C.L. Zhang, M.R. Yu, D.A. Sun, L.H. Zhang, and D.M. Zhou. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23662817].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23669381">Synthesis and Antiviral Activity of Methylenecyclopropane Analogs with 6-Alkoxy and 6-Alkylthio Substitutions that Exhibit Broad Spectrum Antiviral Activity against Human Herpesviruses</a><i>.</i> Prichard, M.N., J.D. Williams, G. Komazin-Meredith, A.R. Khan, N.B. Price, G.M. Jefferson, E.A. Harden, C.B. Hartline, N.P. Peet, and T.L. Bowlin. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23669381].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0510-052313.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317209300012">Recent Progress in Biological Activities and Synthetic Methodologies of Pyrroloquinoxalines</a><i>.</i> Huang, A.P. and C. Ma. Mini-reviews in Medicinal Chemistry, 2013. 13(4): p. 607-616; ISI[000317209300012].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317168200009">Antiviral Efficacy of a Short PNA Targeting MicroRNA-122 Using Galactosylated Cationic Liposome as a Carrier for the Delivery of the PNA-DNA Hybrid to Hepatocytes</a><i>.</i> Kim, H., K.H. Lee, K.B. Kim, Y.S. Park, K.S. Kim, and D.E. Kim. Bulletin of the Korean Chemical Society, 2013. 34(3): p. 735-742; ISI[000317168200009].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317709400001">A Cyclic Peptide Mimic of an RNA Recognition Motif of Human LA Protein Is a Potent Inhibitor of Hepatitis C Virus</a><i>.</i> Manna, A.K., A. Kumar, U. Ray, S. Das, G. Basu, and S. Roy. Antiviral Research, 2013. 97(3): p. 223-226; ISI[000317709400001].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0510-052313.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000317153100033">Anti-Hepatitis B Virus Activity of Alpha-DDB-DU, a Novel Nucleoside Analogue, in Vitro and in Vivo</a><i>.</i> Zheng, L.Y., L.M. Zang, Q.H. Yang, W.Q. Yu, X.Z. Fang, Y.H. Zhang, X.J. Zhao, N. Wan, Y.T. Zhang, Q.D. Wang, and J.B. Chang. European Journal of Pharmacology, 2013. 702(1-3): p. 258-263; ISI[000317153100033].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0510-052313.</p>

    <br />

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
