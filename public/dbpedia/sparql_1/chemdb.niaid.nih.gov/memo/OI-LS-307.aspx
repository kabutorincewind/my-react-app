

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-307.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="j+TjhcS9RiwKU5pb0KgacxymsBTBTk1S3nsPz/Jo42pOEhel4PmESZrfW8RPZeuudrjp3nIcHQJV0JMduQAtFVYehulCXZAg9X1mbLwF61jjpzsouN5Slv6OM6Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="89B97261" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-307-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44228   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          The TRK1 potassium transporter is the critical target for killing of candida albicans by the cationic protein, histatin 5</p>

    <p>          Baev, D, Rivetta, A, Vylkova, S, Sun, JN, Zeng, GF, Slayman, CL, and Edgerton, M</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15485849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15485849&amp;dopt=abstract</a> </p><br />

    <p>2.         44229   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Antimicrobial formulation comprising microthecin or derivatives thereof and their uses in prevention and therapy</p>

    <p>          Morgan, Andrew John, Turner, Mark, Yu, Shukun, Weiergan, Inge, and Pedersen, Hans Christian</p>

    <p>          PATENT:  WO <b>2004083226</b>  ISSUE DATE:  20040930</p>

    <p>          APPLICATION: 2004  PP: 53 pp.</p>

    <p>          ASSIGNEE:  (Danisco A/S, Den.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>3.         44230   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Fluconazole for the treatment of candidiasis: 15 years experience</p>

    <p>          Cha, R and Sobel, JD</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(3): 357-66</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482201&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482201&amp;dopt=abstract</a> </p><br />

    <p>4.         44231   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Current and emerging therapeutic approaches to hepatitis C infection</p>

    <p>          Durantel, D, Escuret, V, and Zoulim, F</p>

    <p>          Expert Rev Anti Infect Ther <b>2003</b>.  1(3): 441-54</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15482141&amp;dopt=abstract</a> </p><br />

    <p>5.         44232   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Synthesis and Potent Antimicrobial Activity of Some Novel 4-(5, 6-Dichloro-1H-benzimidazol-2-yl)-N-substituted Benzamides</p>

    <p>          Ozden, S, Karatas, H, Yildiz, S, and Goker, H</p>

    <p>          Arch Pharm (Weinheim) <b>2004</b>.  337(10): 556-562</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476288&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476288&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44233   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Synthesis and Antimycobacterial Activity of Pyridylmethylsulfanyl and Naphthylmethylsulfanyl Derivatives of Benzazoles, 1, 2, 4-Triazole, and Pyridine-2-carbothioamide/-2-carbonitrile</p>

    <p>          Zahajska, L, Klimesova, V, Koci, J, Waisser, K, and Kaustova, J</p>

    <p>          Arch Pharm (Weinheim) <b>2004</b>.  337(10): 549-555</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476287&amp;dopt=abstract</a> </p><br />

    <p>7.         44234   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Antifungal treatment with carvacrol and eugenol of oral candidiasis in immunosuppressed rats</p>

    <p>          Chami, N, Chami, F, Bennis, S, Trouillas, J, and Remmal, A</p>

    <p>          Braz J Infect Dis <b>2004</b>.  8(3): 217-26</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476054&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15476054&amp;dopt=abstract</a> </p><br />

    <p>8.         44235   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Crystallographic structure of PNP from Mycobacterium tuberculosis at 1.9A resolution</p>

    <p>          Nolasco, DO, Canduri, F, Pereira, JH, Cortinoz, JR, Palma, MS, Oliveira, JS, Basso, LA, de, Azevedo WF Jr, and Santos, DS</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  324(2): 789-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474496&amp;dopt=abstract</a> </p><br />

    <p>9.         44236   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          New method for detection of Mycobacterium tuberculosis Direct Test inhibitors in clinical specimens</p>

    <p>          Sloutsky, A, Han, LL, and Werner, BG</p>

    <p>          Diagn Microbiol Infect Dis <b>2004</b>.  50(2): 109-11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474319&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474319&amp;dopt=abstract</a> </p><br />

    <p>10.       44237   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Screening of Compounds Toxicity against Human Monocytic cell line-THP-1 by Flow Cytometry</p>

    <p>          Pick, N, Cameron, S, Arad, D, and Av-Gay, Y</p>

    <p>          Biol Proced Online <b>2004</b>.  6: 220-225</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472722&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472722&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44238   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Microscopic observation drug susceptibility assay, a rapid, reliable diagnostic test for multidrug-resistant tuberculosis suitable for use in resource-poor settings</p>

    <p>          Moore, DA, Mendoza, D, Gilman, RH, Evans, CA, Hollm, Delgado MG, Guerra, J, Caviedes, L, Vargas, D, Ticona, E, Ortiz, J, Soto, G, and Serpa, J</p>

    <p>          J Clin Microbiol <b>2004</b>.  42(10): 4432-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472289&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15472289&amp;dopt=abstract</a> </p><br />

    <p>12.       44239   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Branched-chain amino acid aminotransferase and methionine formation in Mycobacterium tuberculosis</p>

    <p>          Venos, ES, Knodel, MH, Radford, CL, and Berger, BJ</p>

    <p>          BMC Microbiol <b>2004</b>.  4(1): 39</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15471546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15471546&amp;dopt=abstract</a> </p><br />

    <p>13.       44240   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Antimicrobial Chlorinated Bibenzyls from the Liverwort Riccardia marginata</p>

    <p>          Baek, Seung-Hwa, Phipps, Richard K, and Perry, Nigel B</p>

    <p>          Journal of Natural Products <b>2004</b>.  67(4): 718-720</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>14.       44241   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Conjugation of amino-containing drugs to polysaccharides by tosylation: amphotericin B-arabinogalactan conjugates</p>

    <p>          Ehrenfreund-Kleinman, Tirsta, Golenser, Jacob, and Domb, Abraham J</p>

    <p>          Biomaterials <b>2004</b>.  25(15): 3049-3057</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.       44242   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Evaluation of carvacrol and eugenol as prophylaxis and treatment of vaginal candidiasis in an immunosuppressed rat model</p>

    <p>          Chami, F, Chami, N, Bennis, S, Trouillas, J, and Remmal, A</p>

    <p>          J Antimicrob Chemother <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456732&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456732&amp;dopt=abstract</a> </p><br />

    <p>16.       44243   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral furanose-type bicyclic carbohydrates</p>

    <p>          Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, Peys, Eric, Van Der Eycken, Johan, and Van Hoof, Steven</p>

    <p>          PATENT:  WO <b>2004080410</b>  ISSUE DATE:  20040923</p>

    <p>          APPLICATION: 2004  PP: 28 pp.</p>

    <p>          ASSIGNEE:  (Kemin Pharma Europe B.V.B.A., USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.       44244   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Bile acid amides derived from chiral amino alcohols: novel antimicrobials and antifungals</p>

    <p>          Hazra, Braja G, Pore, Vandana S, Dey, Sanjeev Kumar, Datta, Suchitra, Darokar, Mahendra P, Saikia, Dharmendra, Khanuja, SPS, and Thakur, Anup P</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(3): 773-777</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.       44245   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Fighting tuberculosis: An old disease with new challenges</p>

    <p>          Tripathi, RP, Tewari, N, Dwivedi, N, and Tiwari, VK</p>

    <p>          Med Res Rev <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15389729&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15389729&amp;dopt=abstract</a> </p><br />

    <p>19.       44246   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          DB75, a novel trypanocidal agent, disrupts mitochondrial function in Saccharomyces cerevisiae</p>

    <p>          Lanteri, CA, Trumpower, BL, Tidwell, RR, and Meshnick, SR</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3968-74</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388460&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388460&amp;dopt=abstract</a> </p><br />

    <p>20.       44247   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Antimicrobial evaluation of nocathiacins, a thiazole peptide class of antibiotics</p>

    <p>          Pucci, MJ, Bronson, JJ, Barrett, JF, DenBleyker, KL, Discotto, LF, Fung-Tomc, JC, and Ueda, Y</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3697-701</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388422&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388422&amp;dopt=abstract</a> </p><br />

    <p>21.       44248   OI-LS-307; PUBMED-OI-10/20/2004</p>

    <p class="memofmt1-2">          Quinazolinone fungal efflux pump inhibitors. Part 2: In vitro structure-activity relationships of (N-methyl-piperazinyl)-containing derivatives</p>

    <p>          Watkins, WJ, Lemoine, RC, Chong, L, Cho, A, Renau, TE, Kuo, B, Wong, V, Ludwikow, M, Garizi, N, Iqbal, N, Barnard, J, Jankowska, R, Singh, R, Madsen, D, Lolans, K, Lomovskaya, O, Oza, U, and Dudley, MN</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(20): 5133-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380214&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380214&amp;dopt=abstract</a> </p><br />

    <p>22.       44249   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Synthesis and anti-tuberculosis activity of N-aryl-C-nitroazoles</p>

    <p>          Walczak, Krzysztof, Gondela, Andrzej, and Suwinski, Jerzy</p>

    <p>          European Journal of Medicinal Chemistry <b>2004</b>.  39(10): 849-853</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.       44250   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          The down-regulation of cathepsin G in THP-1 monocytes after infection with Mycobacterium tuberculosis is associated with increased intracellular survival of bacilli</p>

    <p>          Rivera-Marrero, Carlos A, Stewart, Julie, Shafer, William M, and Roman, Jesse</p>

    <p>          Infection and Immunity <b>2004</b>.  72(10): 5712-5721</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       44251   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          Preventing hepatitis C: &#39;common sense&#39;, &#39;the bug&#39; and other perspectives from the risk narratives of people who inject drugs</p>

    <p>          Davis, M, Rhodes, T, and Martin, A</p>

    <p>          SOCIAL SCIENCE &amp; MEDICINE <b>2004</b>.  59(9): 1807-1818, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223734100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223734100003</a> </p><br />

    <p>25.       44252   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Identification of Mycobacterium tuberculosis counterimmune (cim) mutants in immunodeficient mice by differential screening</p>

    <p>          Hisert, Katherine B, Kirksey, Meghan A, Gomez, James E, Sousa, Alexandra O, Cox, Jeffery S, Jacobs, William R Jr, Nathan, Carl F, and McKinney, John D</p>

    <p>          Infection and Immunity <b>2004</b>.  72(9): 5315-5321</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>26.       44253   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Sphingosine 1-phosphate induces antimicrobial activity both in vitro and in vivo</p>

    <p>          Garg, Sanjay K, Volpe, Elisabetta, Palmieri, Graziana, Mattei, Maurizio, Galati, Domenico, Martino, Angelo, Piccioni, Maria S, Valente, Emanuela, Bonnano, Elena, De Vito, Paolo, Baldini, Patrizia M, Spagnoli, Luigi G, Colizzi, Vittorio, and Fraziano, Maurizio</p>

    <p>          Journal of Infectious Diseases <b>2004</b>.  189(11): 2129-2138</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       44254   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          Regulation of hepatitis C virus replication by interferon regulatory factor 1</p>

    <p>          Kanazawa, N, Kurosaki, M, Sakamoto, N, Enomoto, N, Itsui, Y, Yamashiro, T, Tanabe, Y, Maekawa, S, Nakagawa, M, Chen, CH, Kakinuma, S, Oshima, S, Nakamura, T, Kato, T, Wakita, T, and Watanabe, M</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(18): 9713-9720, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100015</a> </p><br />

    <p>28.       44255   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          Newly synthesized hepatitis C virus replicon RNA is protected from nuclease activity by a protease-sensitive factor(s)</p>

    <p>          Yang, G, Pevear, DC, Collett, MS, Chunduru, S, Young, DC, Benetatos, C, and Jordan, R</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(18): 10202-10205, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223701100065</a> </p><br />

    <p>29.       44256   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Synthesis of some new cyanopyrans and cyanopyridines and their biological activities</p>

    <p>          Popat, KH, Kachhadia, VV, Nimavat, Kiran S, and Joshi, HS</p>

    <p>          Journal of the Indian Chemical Society <b>2004</b>.  81(2): 157-159</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.       44257   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Relations between clinical subtypes of Mycobacterium avium pulmonary disease and polyclonal infections detected by IS1245 based restriction fragment length polymorphism analysis</p>

    <p>          Kuwabara, Katsuhiro, Watanabe, Yasushi, Wada, Koichi, and Tsuchiya, Toshiaki</p>

    <p>          Kekkaku <b>2004</b>.  79(2): 39-46</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       44258   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          HIV/HCV coinfection: Management update</p>

    <p>          Alvarez, D</p>

    <p>          JOURNAL OF THE NATIONAL MEDICAL ASSOCIATION <b>2004</b>.  96(2): 25-26, 2</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223740600007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223740600007</a> </p><br />

    <p>32.       44259   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis Growth Inhibition by Constituents of Sapium haematospermum</p>

    <p>          Woldemichael, Girma M, Gutierrez-Lugo, Maria-Teresa, Franzblau, Scott G, Wang, Yuehong, Suarez, Enrique, and Timmermann, Barbara N</p>

    <p>          Journal of Natural Products <b>2004</b>.  67(4): 598-603</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.       44260   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Fluoroquinolones as chemotherapeutics against mycobacterial infections</p>

    <p>          Jacobs, Michael R</p>

    <p>          Current Pharmaceutical Design <b>2004</b>.  10(26): 3213-3220</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       44261   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Novel bacterial RNase P proteins and their use in identifying antibacterial compounds</p>

    <p>          Gopalan, Venkat, Jovanovic, Milan, Eder, Paul S, Giordano, Tony, Powers, Gordon D, and Xavier, KAsish</p>

    <p>          PATENT:  US <b>2004127480</b>  ISSUE DATE:  20040701</p>

    <p>          APPLICATION: 2001-12203  PP: 53 pp., Cont.-in-part of U.S. Ser. No. 516,061.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>35.       44262   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          Is second-line anti-tuberculosis drug susceptibility testing reliable?</p>

    <p>          Kim, SJ, Espinal, MA, Abe, C, Bai, GH, Boulahbals, F, Fattorinit, L, Gilpin, C, Hoffner, S, Kam, KM, Martin-Casabona, N, Rigouts, L, and Vincent, V</p>

    <p>          INTERNATIONAL JOURNAL OF TUBERCULOSIS AND LUNG DISEASE <b>2004</b>.  8(9): 1157-1158, 2</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223680300021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223680300021</a> </p><br />

    <p>36.       44263   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Cytomegalovirus antiviral resistance associated with treatment induced UL97 (protein kinase) and UL54 (DNA polymerase) mutations</p>

    <p>          Scott, GM, Isaacs, MA, Zeng, F, Kesson, AM, and Rawlinson, WD</p>

    <p>          Journal of Medical Virology <b>2004</b>.  74(1): 85-93</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>37.       44264   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Antiviral properties and cytotoxic activity of platinum(II) complexes with 1,10-phenanthrolines and acyclovir or penciclovir</p>

    <p>          Margiotta, Nicola, Bergamo, Alberta, Sava, Gianni, Padovano, Giacomo, De Clercq, Erik, and Natile, Giovanni</p>

    <p>          Journal of Inorganic Biochemistry <b>2004</b>.  98(8): 1385-1390</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>38.       44265   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          Silent nucleotide polymorphisms and a phyogeny for Mycobacterium tuberculosis</p>

    <p>          Baker, L, Brown, T, Maiden, MC, and Drobniewski, F</p>

    <p>          EMERGING INFECTIOUS DISEASES <b>2004</b>.  10(9): 1568-1577, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223740200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223740200008</a> </p><br />

    <p>39.       44266   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          From penicillin to the ribosome: Revolutions in the determination and use of molecular structure in chemistry and biology</p>

    <p>          Baker, EN</p>

    <p>          AUSTRALIAN JOURNAL OF CHEMISTRY <b>2004</b>.  57(9): 829-836, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700003</a> </p><br />

    <p>40.       44267   OI-LS-307; WOS-OI-10/10/2004</p>

    <p class="memofmt1-2">          The synthesis of N-ribosyl transferase inhibitors based on a transition state blueprint</p>

    <p>          Evans, GB</p>

    <p>          AUSTRALIAN JOURNAL OF CHEMISTRY <b>2004</b>.  57(9): 837-854, 18</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223736700004</a> </p><br />

    <p>41.       44268   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of 4-thio substituted coumarin derivatives as anti-HCV agent</p>

    <p>          Wu, Jie, Yang, Zhen, Fathi, Reza, and Zhu, Qiang</p>

    <p>          PATENT:  US <b>2004180950</b>  ISSUE DATE:  20040916</p>

    <p>          APPLICATION: 2004-31758  PP: 43 pp., Cont.-in-part of U.S. Pat. Appl. 2004 2,538.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>42.       44269   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Infectious diseases. Part 4. Infections due to human immunodeficiency virus (HIV) and hepatitis-C virus (HCV)</p>

    <p>          Stock, Ingo</p>

    <p>          PZ Prisma <b>2004</b>.  11(3): 188-198</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>43.       44270   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of triazolyl nucleoside derivatives as Hepatitis C antiviral agents</p>

    <p>          Hendricks, Robert Than, Humphreys, Eric Roy, Martin, Joseph Armstrong, Prince, Anthony, and Sarma, Keshab</p>

    <p>          PATENT:  WO <b>2004052905</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 40 pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann-La Roche Ag, Switz.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.       44271   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Role of human neutrophil peptide-1 as a possible adjunct to antituberculosis chemotherapy</p>

    <p>          Kalita, A, Verma, I, and Khuller, GK</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2004</b>.  190(8): 1476-1480, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223968700015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223968700015</a> </p><br />

    <p>45.       44272   OI-LS-307; SCIFINDER-OI-10/14/2004</p>

    <p class="memofmt1-2">          Preparation of 6-methyl-2-(4-morpholinoanilino)nicotinic acid as anti-HCV agent</p>

    <p>          Kim, Jongwoo, Lee, Sangwook, Lee, Geunhyung, Han, Jaejin, Park, Sangjin, Park, Eulyong, and Shin, Joongchul</p>

    <p>          PATENT:  WO <b>2004033450</b>  ISSUE DATE:  20040422</p>

    <p>          APPLICATION: 2003  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (B &amp; C Biopharm Co., Ltd. S. Korea</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>46.       44273   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Definition of the Beijing/W lineage of Mycobacterium tuberculosis on the basis of genetic markers</p>

    <p>          Kremer, K, Glynn, JR, Lillebaek, T, Niemann, S, Kurepina, NE, Kreiswirth, BN, Bifani, PJ, and van, Soolingen D</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2004</b>.  42(9): 4040-4049, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223902000020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223902000020</a> </p><br />

    <p>47.       44274   OI-LS-307; WOS-OI-10/17/2004</p>

    <p><b>          Use of the MagNA pure LC automated nucleic acid extraction system followed by real-time reverse transcription-PCR for ultrasensitive quantitation of hepatitis C virus RNA</b> </p>

    <p>          Cook, L, Ng, KW, Bagabag, A, Corey, L, and Jerome, KR</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2004</b>.  42(9): 4130-4136, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223902000033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223902000033</a> </p><br />

    <p>48.       44275   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Correlation of mutations detected by INNO-LiPA with levels of rifampicin resistance in Mycobacterium tuberculosis</p>

    <p>          Srivastava, K, Das, R, Jakhmola, P, Gupta, P, Chauhan, DS, Sharma, VD, Singh, HB, Sachan, AS, and Katoch, VM</p>

    <p>          INDIAN JOURNAL OF MEDICAL RESEARCH <b>2004</b>.  120(2): 100-105, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223870000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223870000008</a> </p><br />

    <p>49.       44276   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Synthesis, spectroscopic studies, and crystal structures of phenylorganotin derivatives with [bis(2,6-dimethylphenyl)amino]benzoic acid: Novel antituberculosis agents</p>

    <p>          Dokorou, V, Kovala-Demertzi, D, Jasinski, JP, Galani, A, and Demertzis, MA</p>

    <p>          HELVETICA CHIMICA ACTA <b>2004</b>.  87(8): 1940-1950, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223883200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223883200004</a> </p><br />
    <br clear="all">

    <p>50.       44277   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Anidulafungin: an echinocandin antifungal</p>

    <p>          Pfaller, MA</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2004</b>.  13(9): 1183-1197, 15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223845700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223845700009</a> </p><br />

    <p>51.       44278   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Ring-substituted imidazoles as a new class of anti-tuberculosis agents</p>

    <p>          Gupta, P, Hameed, S, and Jain, R</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  39(9): 805-814, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223874100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223874100008</a> </p><br />

    <p>52.       44279   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Expression and characterization of the protein Rv1399c from Mycobacterium tuberculosis - A novel carboxyl esterase structurally related to the HSL family</p>

    <p>          Canaan, S, Maurin, D, Chahinian, H, Pouilly, B, Durousseau, C, Frassinetti, F, Scappuccini-Calvo, L, Cambillau, C, and Bourne, Y</p>

    <p>          EUROPEAN JOURNAL OF BIOCHEMISTRY <b>2004</b>.  271(19): 3953-3961, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223954800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223954800016</a> </p><br />

    <p>53.       44280   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Identification and validation of novel drug targets in tuberculosis</p>

    <p>          Duncan, K</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(26): 3185-3194, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000002</a> </p><br />

    <p>54.       44281   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Structural bioinformatic approaches to the discovery of new antimycobacterial drugs</p>

    <p>          Kantardjieff, K and Rupp, B</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(26): 3195-3211, 17</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000003</a>55.                 44282   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Liposome technology for drug delivery against mycobacterial infections</p>

    <p>          Khuller, GK, Kapur, M, and Sharma, S</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(26): 3263-3274, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000008</a> </p><br />

    <p>56.       44283   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Predictive in vitro models of the sterilizing activity of anti-tuberculosis drugs</p>

    <p>          Mitchison, DA and Coates, ARM</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2004</b>.  10(26): 3285-3295, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854000010</a> </p><br />
    <br clear="all">

    <p>57.       44284   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Biopharmaceutic classification system: A scientific framework for pharmacokinetic optimization in drug research</p>

    <p>          Varma, MVS, Khandavilli, S, Ashokraj, Y, Jain, A, Dhanikula, A, Sood, A, Thomas, NS, Pillai, O, Sharma, P, Gandhi, R, Agrawal, S, Nair, V, and Panchagnula, R</p>

    <p>          CURRENT DRUG METABOLISM <b>2004</b>.  5(5): 375-388, 14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854300002</a> </p><br />

    <p>58.       44285   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Human Hepatocytes in primary culture: The choice to investigate drug metabolism in man</p>

    <p>          Gomez-Lechon, MJ, Donato, MT, Castell, JV, and Jover, R</p>

    <p>          CURRENT DRUG METABOLISM <b>2004</b>.  5(5): 443-462, 20</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223854300006</a> </p><br />

    <p>59.       44286   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          A mycobacterial lipoarabinomannan specific monoclonal antibody and its F(ab &#39;)(2) fragment prolong survival of mice infected with Mycobacterium tuberculosis</p>

    <p>          Hamasur, B, Haile, M, Pawlowski, A, Schroder, U, Kallenius, G, and Svenson, SB</p>

    <p>          CLINICAL AND EXPERIMENTAL IMMUNOLOGY <b>2004</b>.  138(1): 30-38, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223953500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223953500005</a> </p><br />

    <p>60.       44287   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Combined effect of atovaquone and pyrrolidine dithiocarbamate in the treatment of acute murine toxoplasmosis</p>

    <p>          Djurkovic-Djakovic, O, Nikolic, A, Bobic, B, and Klun, I</p>

    <p>          CHEMOTHERAPY <b>2004</b>.  50(3): 155-156, 2</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223897900009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223897900009</a> </p><br />

    <p>61.       44288   OI-LS-307; WOS-OI-10/17/2004</p>

    <p class="memofmt1-2">          Antimycobacterial 1-aryl-5-benzylsulfanyltetrazoles</p>

    <p>          Waisser, K, Adamec, J, Kunes, J, and Kaustova, B</p>

    <p>          CHEMICAL PAPERS-CHEMICKE ZVESTI <b>2004</b>.  58(3): 214-219, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223853100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223853100013</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
