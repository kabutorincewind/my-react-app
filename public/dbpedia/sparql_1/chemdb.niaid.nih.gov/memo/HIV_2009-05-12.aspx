

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tSnYRotTAXjr3goJI8/PBq+ErqkpN6UdjYTogZfuwZnPqRV8YPGBA53PJOAlyw4WZIEhFbRc+EXVPMr9/oLTfKaAKxxsE5DxCbIvMdnfhfF+Qn3RDNptrQMr1bA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E8D2B22" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  April 29 - May 12, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19415671?ordinalpos=28&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and Studies of New 2-(Coumarin-4-yloxy)-4,6-(substituted)-s-Triazine Derivatives as Potential Anti-HIV Agents.</a>  Mahajan DH, Pannecouque C, De Clercq E, Chikhalia KH.  Arch Pharm (Weinheim). 2009 May 4. [Epub ahead of print]  PMID: 19415671</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19413342?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Lignans with Anti-HIV Activity from Schisandra propinqua var. sinensis.</a>  Li XN, Pu JX, Du X, Yang LM, An HM, Lei C, He F, Luo X, Zheng YT, Lu Y, Xiao WL, Sun HD.  J Nat Prod. 2009 May 4. [Epub ahead of print]  PMID: 19413342</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19409877?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A novel sorting strategy of trichosanthin for hijacking human immunodeficiency virus type 1.</a>  Zhao WL, Zhang F, Feng D, Wu J, Chen S, Sui SF.  Biochem Biophys Res Commun. 2009 May 3. [Epub ahead of print]  PMID: 19409877</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19282192?ordinalpos=86&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Aryl nucleoside H-phosphonates. Part 16: synthesis and anti-HIV-1 activity of di-aryl nucleoside phosphotriesters.</a>  Romanowska J, Szyma&#324;ska-Michalak A, Boryski J, Stawi&#324;ski J, Kraszewski A, Loddo R, Sanna G, Collu G, Secci B, La Colla P.  Bioorg Med Chem. 2009 May 1;17(9):3489-98. Epub 2009 Feb 23.  PMID: 19282192</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19279091?ordinalpos=90&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A limited group of class I histone deacetylases acts to repress human immunodeficiency virus type 1 expression.</a>  Keedy KS, Archin NM, Gates AT, Espeseth A, Hazuda DJ, Margolis DM.  J Virol. 2009 May;83(10):4749-56. Epub 2009 Mar 11.  PMID: 19279091</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19345581?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of potent antitumor and antiviral benzofuran derivatives.</a>  Galal SA, Abd El-All AS, Abdallah MM, El-Diwani HI.  Bioorg Med Chem Lett. 2009 May 1;19(9):2420-8. Epub 2009 Mar 21.  PMID: 19345581</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19342232?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">New Rev-export inhibitor from Alpinia galanga and structure-activity relationship.</a>  Tamura S, Shiomi A, Kaneko M, Ye Y, Yoshida M, Yoshikawa M, Kimura T, Kobayashi M, Murakami N.  Bioorg Med Chem Lett. 2009 May 1;19(9):2555-7. Epub 2009 Mar 17.  PMID: 19342232</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265292700001">Harnessing Nature&#39;s Insight: Design of Aspartyl Protease Inhibitors from Treatment of Drug-Resistant HIV to Alzheimer&#39;s Disease.</a>  Ghosh AK.  JOURNAL OF MEDICINAL CHEMISTRY, 2009; 52(8): 2163-2176. </p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265354100009">STRUCTURAL MODIFICATION OF DIKETO ACID PORTION IN 1H-BENZYLINDOLE DERIVATIVES HIV-1 INTEGRASE INHIBITORS.</a> Ferro S, De Grazia S, De Luca L, Barreca ML, Debyser Z, Chimirri A. HETEROCYCLES, 2009; 78(4): 947-959. </p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265339900045">Parallel synthesis, molecular modelling and further structure-activity relationship studies of new acylthiocarbamates as potent non-nucleoside HIV-1 reverse transcriptase inhibitors.</a> Spallarossa A, Cesarini S, Ranise A, Schenone S, Bruno O, Borassi A, La Colla P, Pezzullo M, Sanna G, Collu G, Secci B, Loddo R.  EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY, 2009; 44(5): 2190-2201. </p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000265331500001">GSK983: A novel compound with broad-spectrum antiviral activity.</a>  Harvey R, Brown K, Zhang Q, Gartland M, Walton L, Talarico C, Lawrence W, Selleseth D, Coffield N, Leary J, Moniri K, Singer S, Strum J, Gudmundsson K, Biron K, Romines KR, Sethna P.  ANTIVIRAL RESEARCH, 2009; 82(1): 1-11. </p>             
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
