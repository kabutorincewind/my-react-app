

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-05-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EAJjL7aKWL/DtLXYNCbSavtHiuU7sPmNmidKcDGW5GMPrjBry0CDVvpR5xT8rbPoK64ASrkSdeBi6SvY6NtckUuPQV1aoVLIh6JKe9NGNLRoUBRUSeOcTiPGMj0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="89D7DCF9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: April 27 - May 10, 2012</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22533850">Synthesis and Biological Evaluation of Fatty Acyl Ester Derivatives of (-)-2&#39;,3&#39;-Dideoxy-3&#39;-thiacytidine.</a> Agarwal, H.K., B.S. Chhikara, M.J. Hanley, G. Ye, G.F. Doncel, and K. Parang, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22533850].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22555953">Synthesis and Structure-Activity Relationship Studies of HIV-1 Virion Infectivity Factor (Vif) Inhibitors That Block Viral Replication.</a> Ali, A., J. Wang, R.S. Nathans, H. Cao, N. Sharova, M. Stevenson, and T.M. Rana, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22555953].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22555163">Histone Methyltransferase Inhibitors Induce HIV-1 Recovery in Resting CD4+ T Cells from HIV-1+ HAART-treated Patients</a><i>.</i> Bouchat, S., J.S. Gatot, K. Kabeya, C. Cardona, L. Colin, G. Herbein, S. de Wit, N. Clumeck, O. Lambotte, C. Rouzioux, O. Rohr, and C. van Lint, AIDS, 2012. <b>[Epub ahead of print]</b>; PMID[22555163].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22345475">Single-domain Antibody-SH3 Fusions for Efficient Neutralization of HIV-1 Nef Functions.</a> Bouchet, J., C. Herate, C.A. Guenzel, C. Verollet, A. Jarviluoma, J. Mazzolini, S. Rafie, P. Chames, D. Baty, K. Saksela, F. Niedergang, I. Maridonneau-Parini, and S. Benichou, Journal of Virology, 2012. 86(9): p. 4856-4867; PMID[22345475].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21827278">Monoclonal Antibody-based Candidate Therapeutics against HIV Type 1.</a> Chen, W. and D.S. Dimitrov, AIDS Research and Human Retroviruses, 2012. 28(5): p. 425-434; PMID[21827278].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22405288">Synthesis and Biological Evaluation of Piperidine-substituted Triazine Derivatives as HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Chen, X., P. Zhan, C. Pannecouque, J. Balzarini, E. De Clercq, and X. Liu, European Journal of Medicinal Chemistry, 2012. 51: p. 60-66; PMID[22405288].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22554282">Protein Kinase C-delta Regulates HIV-1 Replication at an Early Post-entry Step in Macrophages.</a> Contreras, X., O. Mzoughi, F. Gaston, B.M. Peterlin, and E. Bahraoui, Retrovirology, 2012. 9(1): p. 37; PMID[22554282].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22455441">Polyvalent Side Chain Peptide-synthetic Polymer Conjugates as HIV-1 Entry Inhibitors.</a> Danial, M., M.J. Root, and H.A. Klok, Biomacromolecules, 2012. <b>[Epub ahead of print]</b>; PMID[22455441].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22403408">Peptides from Second Extracellular Loop of C-C Chemokine Receptor Type 5 (CCR5) Inhibit Diverse Strains of HIV-1.</a> Dogo-Isonagie, C., S. Lam, E. Gustchina, P. Acharya, Y. Yang, S. Shahzad-Ul-Hussan, G.M. Clore, P.D. Kwong, and C.A. Bewley, Journal of Biological Chemistry, 2012. 287(18): p. 15076-15086; PMID[22403408].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22464131">Design, Synthesis, and Biological Activity of Novel 1,4-Disubstituted Piperidine/Piperazine Derivatives as CCR5 Antagonist-based HIV-1 Entry Inhibitors.</a> Dong, M.X., L. Lu, H. Li, X. Wang, H. Lu, S. Jiang, and Q.Y. Dai, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3284-3286; PMID[22464131].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22438246">Galectin-9 Binding to Tim-3 Renders Activated Human CD4+ T Cells Less Susceptible to HIV-1 Infection.</a> Elahi, S., T. Niki, M. Hirashima, and H. Horton, Blood, 2012. 119(18): p. 4192-4204; PMID[22438246].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21902586">Susceptibility of HIVType 2 Primary Isolates to CCR5 and CXCR4 Monoclonal Antibodies, Ligands, and Small Molecule Inhibitors.</a> Espirito-Santo, M., Q. Santos-Costa, M. Calado, P. Dorr, and J.M. Azevedo-Pereira, AIDS Research and Human Retroviruses, 2012. 28(5): p. 478-485; PMID[21902586].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22545664">Design of a Modular Tetrameric Scaffold for the Synthesis of Membrane-localized D-peptide Inhibitors of HIV-1 Entry.</a> Francis, J.N., J.S. Redman, D.M. Eckert, and M.S. Kay, Bioconjugate Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22545664].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233531">Identification of Novel HIV-1 Integrase Inhibitors Using Shape-based Screening, QSAR, and Docking Approach.</a> Gupta, P., P. Garg, and N. Roy, Chemical Biology and Drug Design, 2012. 79(5): p. 835-849; PMID[22233531].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330930">Preclinical Evaluation of the HIV-1 Fusion Inhibitor L&#39;644 as a Potential Candidate Microbicide.</a> Harman, S., C. Herrera, N. Armanasco, J. Nuttall, and R.J. Shattock, Antimicrobial Agents and  Chemotherapy, 2012. 56(5): p. 2347-2356; PMID[22330930].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22550269">Boosting of HIV Protease Inhibitors by Ritonavir in the Intestine: The Relative Role of Cyp and P-gp Inhibition Based on Caco-2 Monolayers Versus in Situ Intestinal Perfusion in Mice.</a> Holmstock, N.F., P.P. Annaert, and P. Augustijns, Drug Metabolism and Disposition, 2012. <b>[Epub ahead of print]</b>; PMID[22550269].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22544389">Pentacycloundecane-diol-based HIV-1 Protease Inhibitors: Biological Screening, 2D NMR, and Molecular Simulation Studies.</a> Honarparvar, B., M.M. Makatini, S.A. Pawar, K. Petzold, M.E. Soliman, P.I. Arvidsson, Y. Sayed, T. Govender, G.E. Maguire, and H.G. Kruger, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22544389].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22457281">A New Class of Synthetic Peptide Inhibitors Blocks Attachment and Entry of Human Pathogenic Viruses.</a> Krepstakies, M., J. Lucifora, C.H. Nagel, M.B. Zeisel, B. Holstermann, H. Hohenberg, I. Kowalski, T. Gutsmann, T.F. Baumert, K. Brandenburg, J. Hauber, and U. Protzer, Journal of Infectious Disease, 2012. <b>[Epub ahead of print]</b>; PMID[22457281].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22326358">Old Plants Newly Discovered: Cassia sieberiana D.C. And Cassia abbreviata Oliv. Oliv. Root Extracts Inhibit in vitro HIV-1c Replication in Peripheral Blood Mononuclear Cells (PBMCs) by Different Modes of Action.</a> Leteane, M.M., B.N. Ngwenya, M. Muzila, A. Namushe, J. Mwinga, R. Musonda, S. Moyo, Y.B. Mengestu, B.M. Abegaz, and K. Andrae-Marobela, Journal of Ethnopharmacology, 2012. 141(1): p. 48-56; PMID[22326358].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22390847">Synthesis, Drug Release and Anti-HIV Activity of a Series of PEGylated Zidovudine Conjugates.</a> Li, W., J. Wu, P. Zhan, Y. Chang, C. Pannecouque, E. De Clercq, and X. Liu, International Journal of Biological Macromolecules, 2012. 50(4): p. 974-980; PMID[22390847].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22497256">Design and Synthesis of Novel Threosyl-5&#39;- deoxyphosphonic Acid Purine Analogues as Potent Anti-HIV Agents.</a> Liu, L.J., E. Kim, and J.H. Hong, Nucleosides Nucleotides Nucleic Acids, 2012. 31(5): p. 411-422; PMID[22497256].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22558263">IFN-Lambda3 Inhibits HIV Infection of Macrophages through the JAK-STAT Pathway.</a> Liu, M.Q., D.J. Zhou, X. Wang, W. Zhou, L. Ye, J.L. Li, Y.Z. Wang, and W.Z. Ho, PLoS ONE, 2012. 7(4): p. e35902; PMID[22558263].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22330914">Sustained Release of the CCR5 Inhibitors CMPD167 and Maraviroc from Vaginal Rings in Rhesus Macaques.</a> Malcolm, R.K., R.S. Veazey, L. Geer, D. Lowry, S.M. Fetherston, D.J. Murphy, P. Boyd, I. Major, R.J. Shattock, P.J. Klasse, L.A. Doyle, K.K. Rasmussen, L. Goldman, T.J. Ketas, and J.P. Moore, Antimicrobial Agents and Chemotherapy, 2012. 56(5): p. 2251-2258; PMID[22330914].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21947448">Docking, Molecular Dynamics and Quantitative Structure-Activity Relationship Studies for HEPTs and DABOs as HIV-1 Reverse Transcriptase Inhibitors.</a> Mao, Y., Y. Li, M. Hao, S. Zhang, and C. Ai, Journal of Molecular Modeling, 2012. 18(5): p. 2185-2198; PMID[21947448].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22480850">Synthesis, Biological Activity and Docking Study of Imidazol-5-one as Novel Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Mokale, S.N., D. Lokwani, and D.B. Shinde, Bioorganic &amp; Medicinal Chemistry, 2012. 20(9): p. 3119-3127; PMID[22480850].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22379083">UCLA1, a Synthetic Derivative of a gp120 RNA Aptamer, Inhibits Entry of Human Immunodeficiency Virus Type 1 Subtype C.</a> Mufhandu, H.T., E.S. Gray, M.C. Madiga, N. Tumba, K.B. Alexandre, T. Khoza, C.K. Wibmer, P.L. Moore, L. Morris, and M. Khati, Journal of Virology, 2012. 86(9): p. 4989-4999; PMID[22379083].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22547625">In Vitro Antiviral Characteristics of HIV-1 Attachment Inhibitor BMS-626529, the Active Component of the Prodrug BMS-663068.</a> Nowicka-Sans, B., Y.F. Gong, B. McAuliffe, I. Dicker, H.T. Ho, N. Zhou, B. Eggers, P.F. Lin, N. Ray, M. Wind-Rotolo, L. Zhu, A. Majumdar, D. Stock, M. Lataillade, G.J. Hanna, J.D. Matiskella, Y. Ueda, T. Wang, J.F. Kadow, N.A. Meanwell, and M. Krystal, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22547625].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22546925">Applications of 3-Aminolactams: Design, Synthesis, and Biological Evaluation of a Library of Potential Dimerisation Inhibitors of HIV1-protease.</a> Pinyol, E., S. Frutos, D. Grillo-Bosch, E. Giralt, B. Clotet, J.A. Este, and A. Diez, Organic and Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22546925].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22547820">Kinetic Mechanism for HIV-1 Neutralization by Antibody 2G12 Entails Reversible Glycan Binding That Slows Cell Entry.</a> Platt, E.J., M.M. Gomes, and D. Kabat, Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[22547820].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22558266">Lignosulfonic Acid Exhibits Broadly anti-HIV-1 activity - Potential as a Microbicide Candidate for the Prevention of HIV-1 Sexual Transmission.</a> Qiu, M., Q. Wang, Y. Chu, Z. Yuan, H. Song, Z. Chen, and Z. Wu, PLoS ONE, 2012. 7(4): p. e35906; PMID[22558266].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22419605">Synthesis, Biological Activity, and ADME Properties of Novel S-DABOs/N-DABOs as HIV Reverse Transcriptase Inhibitors.</a> Radi, M., M. Pagano, L. Franchi, D. Castagnolo, S. Schenone, G. Casaluce, C. Zamperini, E. Dreassi, G. Maga, A. Samuele, E. Gonzalo, B. Clotet, J.A. Este, and M. Botta, ChemMedChem, 2012. 7(5): p. 883-896; PMID[22419605].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22523293">Biological Activity of SE-10, Granulated Powder of Sasa aenanensis Rehder Leaf Extract.</a> Sakagami, H., T. Matsuta, K. Satoh, S. Ohtsuki, C. Shimada, T. Kanamoto, S. Terakubo, H. Nakashima, Y. Morita, A. Ohkubo, T. Tsuda, K. Sunaga, J. Maki, T. Sugiura, M. Kitajima, H. Oizumi, and T. Oizumi, In Vivo, 2012. 26(3): p. 411-418; PMID[22523293].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22378594">Target-selective Photodegradation of HIV-1 Protease and Inhibition of HIV-1 Replication in Living Cells by Designed Fullerene-sugar Hybrids.</a> Tanimoto, S., S. Sakai, E. Kudo, S. Okada, S. Matsumura, D. Takahashi, and K. Toshima, Chemistry - An Asian Journal, 2012. 7(5): p. 911-914; PMID[22378594].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22483582">Discovery of Small Molecule HIV-1 Integrase Dimerization Inhibitors.</a> Tintori, C., J. Demeulemeester, L. Franchi, S. Massa, Z. Debyser, F. Christ, and M. Botta, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3109-3114; PMID[22483582].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22487177">Blocking HIV-1 Entry by a gp120 Surface Binding Inhibitor<i>.</i></a> Tsou, L.K., C.H. Chen, G.E. Dutschman, Y.C. Cheng, and A.D. Hamilton, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(9): p. 3358-3361; PMID[22487177].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22569018">Preventing the Formation of P-TEFb by CycT1-Binding RNA Aptamer for anti-HIV Transcription.</a> Um, H.J., M. Kim, S.H. Lee, and Y.H. Kim, AIDS, 2012. <b>[Epub ahead of print]</b>; PMID[22569018].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22550346">Treatment of SIV-infected Sooty Mangabeys with a Type-I IFN Agonist Results in Decreased Virus Replication without Inducing Hyper Immune Activation.</a> Vanderford, T.H., C. Slichter, K.A. Rogers, B.O. Lawson, R. Obaede, J. Else, F. Villinger, S.E. Bosinger, and G. Silvestri, Blood, 2012. <b>[Epub ahead of print]</b>; PMID[22550346].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22417684">Ivermectin Is a Specific Inhibitor of Importin Alpha/Beta-mediated Nuclear Import able to Inhibit Replication of HIV-1 and Dengue Virus.</a> Wagstaff, K.M., H. Sivakumaran, S.M. Heaton, D. Harrich, and D.A. Jans, Biochemistry Journal, 2012. 443(3): p. 851-856; PMID[22417684].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22569890">Biomimetic Oxidation of Aromatic Xenobiotics: Synthesis of the Phenolic Metabolites from the anti-HIV Drug Efavirenz.</a> Wanke, R., D.A. Novais, S.G. Harjivan, M.M. Marques, and A.M. Antunes, Organic and Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22569890].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22480197">Identification of Small Molecule Inhibitors of the HIV-1 Nucleocapsid-stem-loop 3 RNA Complex.</a> Warui, D.M. and A.M. Baranger, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22480197].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22465634">Anti-AIDS Agents 89. Identification of DCX Derivatives as anti-HIV and Chemosensitizing Dual Function Agents to Overcome P-gp-mediated Drug Resistance for AIDS Therapy.</a> Zhou, T., E. Ohkoshi, Q. Shi, K.F. Bastow, and K.H. Lee, Bioorganic and Medicinal Chemistry Letters, 2012. 22(9): p. 3219-3222; PMID[22465634].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22296826">New Active HIV-1 Protease Inhibitors Derived from 3-hexanol: Conformation Study of the Free Inhibitors in Crystalline State and in Complex with the Enzyme.</a> Ziolkowska, N.E., A. Bujacz, R.S. Randad, J.W. Erickson, T. Skalova, J. Hasek, and G. Bujacz, Chemical Biology and Drug Design, 2012. 79(5): p. 798-809; PMID[22296826].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22379088">Small-Molecule Inhibition of Human Immunodeficiency Virus Type 1 Replication by Targeting the Interaction between Vif and ElonginC.</a> Zuo, T., D. Liu, W. Lv, X. Wang, J. Wang, M. Lv, W. Huang, J. Wu, H. Zhang, H. Jin, L. Zhang, W. Kong, and X. Yu, Journal of Virology, 2012. 86(10): p. 5497-5507; PMID[22379088].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0427-051012.</p>
    

    <p class="memofmt2-2">ISI Web of Knowledge Citations</p>

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301767000013">Synthesis and anti-HIV Activities of Glutamate and Peptide Conjugates of Nucleoside Reverse Transcriptase Inhibitors.</a> Agarwa, H.K., B.S. Chhikara, M. Quiterio, G.F. Doncel, and K. Parang, Journal of Medicinal Chemistry, 2012. 55(6): p. 2672-2687; ISI[000301767000013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300662300005">Phytochemical and Biological Studies of Ochna Species.</a> Bandi, A.K.R., D.U. Lee, R.G. Tih, D. Gunasekar, and B. Bodo, Chemistry &amp; Biodiversity, 2012. 9(2): p. 251-271; ISI[000300662300005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302514400008">Diverse HIV Viruses are Targeted by a Conformationally Dynamic Antiviral.</a> Caines, M.E.C., K. Bichel, A.J. Price, W.A. McEwan, G.J. Towers, B.J. Willett, S.M.V. Freund, and L.C. James, Nature Structural &amp; Molecular Biology, 2012. 19(4): p. 411-416; ISI[000302514400008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">47. <a href="file:///C:/Users/Matthew/AppData/Local/Temp/000302288800006">Efficacy and Safety Tests of Long-acting Nanoformulated Anti-retroviral Drugs in HIV-1 Infected Humanized Mice.</a> Dash, P.K., S. Gorantla, U. Roy, J. Knibbe, S. Balkundi, J. McMillan, H.A. Gelbard, L.Y. Poluektova, and H.E. Gendelman, Journal of Neuroimmune Pharmacology, 2012. 7: p. S14-S14; ISI[000302288800006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301767000020">Fluorophosphonylated Nucleoside Derivatives as New Series of Thymidine Phosphorylase Multisubstrate Inhibitors.</a> Diab, S.A., C. De Schutter, M. Muzard, R. Plantier-Royon, E. Pfund, and T. Lequeux, Journal of Medicinal Chemistry, 2012. 55(6): p. 2758-2768; ISI[000301767000020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301884400012">Peptide Inhibitors of Viral Assembly: A Novel Route to Broad-spectrum Antivirals.</a> ElSawy, K.M., R. Twarock, C.S. Verma, and L.S.D. Caves, Journal of Chemical Information and Modeling, 2012. 52(3): p. 770-776; ISI[000301884400012] .</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302194400013">Two New 4-Hydroxyisoflavanes from the Root of Codonopsis cordifolioidea and Their Anti-virus Activities.</a> Gao, X.M., H.X. Mu, X.S. Li, G.Y. Yang, G.P. Li, and Q.F. Hu, Journal of the Chinese Chemical Society, 2012. 59(4): p. 540-543; ISI[000302194400013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302288800170">Antiretroviral Activity and Brain Penetrance of Folate-coated Nanoformulated Antiretroviral Drugs.</a> Kanmogne, G.D., U. Roy, Z. Liu, J. McMillan, S. Gorantla, S. Balkundi, N. Smith, Y. Alnouti, N. Gautam, L. Poluektova, A. Kabanov, T. Bronich, and H.E. Gendelman, Journal of Neuroimmune Pharmacology, 2012. 7: p. S70-S71; ISI[000302288800170].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302288800093">Theranostic Developments of Small Magnetite Antiretroviral Particles-smart.</a> Li, T., X. Liu, A.V. Kabanov, S. Kayandan, J.S. Riffle, H.E. Gendelman, S. Balasubramaniam, and R.M. Davis, Journal of Neuroimmune Pharmacology, 2012. 7: p. S43-S44; ISI[000302288800093].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302006900007">Synthesis, Characterization and Bioactivity Determination of Ferrocenyl Urea Derivatives.</a> Liu, W., Y.T. Tang, Y. Guo, B. Sun, H.M. Zhu, Y.Y. Xiao, D.D. Dong, and C. Yang, Applied Organometallic Chemistry, 2012. 26(4): p. 189-193; ISI[000302006900007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302288800115">Cystatin B Inhibits the IFN-Beta Response by Preventing STAT-1 Translocation and Decreasing Levels of STAT-1PY: Implication of HIV Replication in Macrophages.</a> Rivera-Rivera, L., K. Colon, and L.M. Melendez, Journal of Neuroimmune Pharmacology, 2012. 7: p. S51-S51; ISI[000302288800115].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302246800003">Inhibition of Interferon Response by Cystatin B: Implication in HIV Replication of Macrophage Reservoirs.</a> Rivera-Rivera, L., J. Perez-Laspiur, K. Colon, and L.M. Melendez, Journal of Neurovirology, 2012. 18(1): p. 20-29; ISI[000302246800003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301767000017">Synthesis, X-Ray Analysis, and Biological Evaluation of a New Class of Stereopure Lactam-based HIV-1 Protease Inhibitors.</a> Wu, X.Y., P. Ohrngren, A.A. Joshi, A. Trejos, M. Persson, R.K. Arvela, H. Wallberg, L. Vrang, A. Rosenquist, B.B. Samuelsson, J. Unge, and M. Larhed, Journal of Medicinal Chemistry, 2012. 55(6): p. 2724-2736; ISI[000301767000017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000302297200005">Effect of Bond Linkage on in Vitro Drug Release and anti-HIV Activity of Chitosan-stavudine Conjugates.</a> Zeng, R., Z.H. Wang, H.R. Wang, L.Q. Chen, L. Yang, R.Z. Qiao, L.M. Hu, and Z.L. Li, Macromolecular Research, 2012. 20(4): p. 358-365; ISI[000302297200005]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0427-051012.</p>

    <p class="memofmt2-2">Patent Citations</p>

    <p class="plaintext">58. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120412&amp;CC=WO&amp;NR=2012048033A2&amp;KC=A2">Novel Compositions for Inhibiting Virus Entry and Promoting Virolysis, and Methods Thereof.</a> Chaiken, I., E. Papazoglou, A.R. Bastian, and Kantharaju. Patent. 2012. 2011-US54969 2012048033: 141pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">59. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120405&amp;CC=US&amp;NR=2012082685A1&amp;KC=A1">Dock-and-lock (Dnl) Constructs for Human Immunodeficiency Virus (HIV) Therapy.</a> Chang, C.-H. and D.M. Goldenberg. Patent. 2012. 2011-288202 20120082685: 84pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">60. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120419&amp;CC=WO&amp;NR=2012051492A2&amp;KC=A2">Compounds and Methods for Inhibiting HIV Latency.</a> Karn, J., J. Friedman, and D.C.K. Chu. Patent. 2012. 2011-US56284 2012051492: 61pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">61. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120405&amp;CC=US&amp;NR=2012083498A1&amp;KC=A1">Modulators of Viral Transcription for Treatment of Human Immunodeficiency Virus (HIV) Infection.</a> Kashanchi, F. Patent. 2012. 2011-162832 20120083498: 43pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">62. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120418&amp;CC=CN&amp;NR=102417515A&amp;KC=A">Thiazo(azine)lidone-azasugar Derivative with Fine anti-HIV Activity and Synthesis and Application Thereof in Pharmaceutical Preparation.</a> Li, X., H. Chen, H. Zhang, Z. Qin, P. Zhang, Q. Yin, and J. Zhang. Patent. 2012. 2010-10293994 102417515: 22pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">63. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120412&amp;CC=WO&amp;NR=2012046653A1&amp;KC=A1">Preparation of Amine Compound as CXCR4 Antagonist with anti-HIV Activity.</a> Matsui, R., T. Yamazaki, and S. Kikumoto. Patent. 2012. 2011-JP72563 2012046653: 23pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">64. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120322&amp;CC=WO&amp;NR=2012035147A1&amp;KC=A1">Nuclear Export Inhibitors to Inhibit Export of Antiviral and Tumor-suppressing Cellular Factors.</a> Stauber, R., V. Fetz, S.K. Knauer, and W. Mann. Patent. 2012. 2011-EP66129 2012035147: 30pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>

    <p class="plaintext">65. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120412&amp;CC=WO&amp;NR=2012047630A2&amp;KC=A2">N-Alkyl or N-Aryl Substituted Guanide and Biguanide Compounds and Methods of Their Use.</a> Teintze, M., R.A. Wilkinson, and M. Labib. Patent. 2012. 2011-US53401 2012047630: 117pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p> </p>


    <p class="plaintext">66. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120322&amp;CC=WO&amp;NR=2012035321A1&amp;KC=A1">Methods and Compositions Comprising Anti-Microtubule Compounds and Antiviral Agents for Reactivating a Latent Virus to Eradicate Viral Infections.</a> Tsao, E.H.F., P. Kellam, and R.G. Jenner. Patent. 2012. 2011-GB51693 2012035321: 70pp.
    <br />
    <b>[Patent]</b>. HIV_0427-051012.</p>

    <p class="memofmt2-2">Meeting Citations</p>

    <p class="plaintext">67. <a href="http://www.isar-icar.com/?page=25_ICAR">Grl-007: A Novel Small Molecule Ccr5 Antagonist Potent against a Wide Spectrum of HIV-1 Isolates.</a> Nakata, H., D. Das, K. Maeda, K.V. Rao, A.K. Ghosh, and H. Mitsuya, 25<sup>th</sup> International Conference for Antiviral Research (ICAR - Japan), 2012. p. 22.
    <br />
    <b>[ICAR]</b>. HIV_0427-051012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
