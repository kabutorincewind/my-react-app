

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-333.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iAz+Fp8uATmoi/1Rz98iR0HF8h7/5wbnREwJlcAKqAXj1cjcqD6SnSuKfU2GJfTxR93pFwFfovsFpwZSYrwc87J/jDf9EF/LpiSUjBbbvGv5TMPWu9SxioU/SCc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F6B75C1A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-333-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47980   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Improving the Antiviral Efficacy and Selectivity of HIV-1 Reverse Transcriptase Inhibitor TSAO-T by the Introduction of Functional Groups at the N-3 Position</p>

    <p>          Bonache, MC, Chamorro, C, Velazquez, S , De Clercq, E, Balzarini, J, Barrios, FR, Gago, F, Camarasa, MJ, and San-Felix, A</p>

    <p>          J Med Chem <b>2005</b>.  48(21): 6653-6660</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16220981&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16220981&amp;dopt=abstract</a> </p><br />

    <p>2.     47981   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          HIV-1. Viral assembly inhibitors on the horizon</p>

    <p>          Brazil, M</p>

    <p>          Nat Rev Drug Discov <b>2005</b>.  4(9): 716-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211702&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211702&amp;dopt=abstract</a> </p><br />

    <p>3.     47982   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Substituted benzodiazepines as inhibitors of the chemokine receptor CXCR4 and use in the inhibition of HIV infectivity</p>

    <p>          Marshall, Garland</p>

    <p>          PATENT:  US <b>2005192272</b>  ISSUE DATE:  20050901</p>

    <p>          APPLICATION: 2004-21345  PP: 15 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>
  
    <br />

    <p>4.     47983   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Non-nucleoside HIV Reverse Transcriptase Inhibitors, Part 6[1]: Synthesis and Anti-HIV Activity of Novel 2-[(Arylcarbonylmethyl)thio]-6-arylthio DABO Analogues</p>

    <p>          Sun, GF, Kuang, YY, Chen, FE, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.  338(10): 457-461</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211654&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16211654&amp;dopt=abstract</a> </p><br />

    <p>5.     47984   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Antiviral activity and safety of 873140, a novel CCR5 antagonist, during short-term monotherapy in HIV-infected adults</p>

    <p>          Lalezari, Jacob, Thompson, Melanie, Kumar, Priny, Piliero, Peter, Davey, Richard, Patterson, Kristine, Shachoy-Clark, Anne, Adkison, Kimberly, Demarest, James, Lou, Yu, Berrey, Michelle, and Piscitelli, Stephen</p>

    <p>          AIDS (London, United Kingdom) <b>2005</b>.  19(14): 1443-1448</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     47985   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Preparation of naphthyridinecarboxamides as HIV integrase inhibitors</p>

    <p>          Johns, Brian Alvin, Boros, Eric Eugene, Kawasuji, Takashi, Koble, Cecilia S, Kurose, Noriyuki, Murai, Hitoshi, Sherrill, Ronald George, and Weatherhead, Jason Gordon</p>

    <p>          PATENT:  WO <b>2005077050</b>  ISSUE DATE:  20050825</p>

    <p>          APPLICATION: 2005  PP: 447 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA and Shionogi &amp; Co., Ltd</p>
    
    <br />
    
    <p>7.     47986   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Synthesis and activity of N-acyl azacyclic urea HIV-1 protease inhibitors with high potency against multiple drug resistant viral strains</p>

    <p>          Zhao, C, Sham, HL, Sun, M, Stoll, VS, Stewart, KD, Lin, S, Mo, H, Vasavanonda, S, Saldivar, A, Park, C, McDonald, EJ, Marsh, KC, Klein, LL, Kempf, DJ, and Norbeck, DW</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16203141&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16203141&amp;dopt=abstract</a> </p><br />

    <p>8.     47987   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          2-(Aryl)-3-furan-2-ylmethyl-thiazolidin-4-ones as selective HIV-RT Inhibitors</p>

    <p>          Rawal, RK, Prabhakar, YS, Katti, SB, and De, Clercq E</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198576&amp;dopt=abstract</a> </p><br />

    <p>9.     47988   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Oxadiazols: a new class of rationally designed anti-human immunodeficiency virus compounds targeting the nuclear localization signal of the viral matrix protein</p>

    <p>          Haffar, O, Dubrovsky, L, Lowe, R, Berro, R, Kashanchi, F, Godden, J, Vanpouille, C, Bajorath, J, and Bukrinsky, M</p>

    <p>          J Virol <b>2005</b>.  79(20): 13028-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189005&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189005&amp;dopt=abstract</a> </p><br />

    <p>10.   47989   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          HIV gp41 HR2-derived synthetic peptides, and their use in anti-AIDS therapy to inhibit transmission of human immunodeficiency virus</p>

    <p>          Delmedico, Mary K and Dwyer, John</p>

    <p>          PATENT:  WO <b>2005067960</b>  ISSUE DATE:  20050728</p>

    <p>          APPLICATION: 2004  PP: 93 pp.</p>

    <p>          ASSIGNEE:  (Trimeris, Inc. USA</p>

    <br />

    <p>11.   47990   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Protease inhibitors and non-nucleoside reverse transcriptase inhibitors have a comparable effect on the CD4 cell change after switching to tenofovir-based regimens</p>

    <p>          van Leth, F, Prins, JM, Lange, JM, and Geerlings, SE</p>

    <p>          AIDS <b>2005</b>.  19(15): 1722-1723</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184059&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184059&amp;dopt=abstract</a> </p><br />

    <p>12.   47991   HIV-LS-333; PUBMED-HIV-10/17/2005</p>

    <p class="memofmt1-2">          Safety and distribution of cellulose acetate 1,2-benzenedicarboxylate (CAP), a candidate anti-HIV microbicide in rhesus macaques</p>

    <p>          Ratterree, M, Gettie, A, Williams, V, Malenbaum, S, Neurath, AR, Cheng-Mayer, C, and Blanchard, J</p>

    <p>          AIDS <b>2005</b>.  19 (15): 1595-1599</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184028&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184028&amp;dopt=abstract</a> </p><br />

    <p>13.   47992   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          Design of a folding inhibitor of the HIV-1 protease</p>

    <p>          Tiana, G, Broglia, RA, Sutto, L, and Provasi, D</p>

    <p>          MOLECULAR SIMULATION <b>2005</b>.  31(11): 765-771, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232032600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232032600004</a> </p><br />

    <p>14.   47993   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Polypeptide multimers comprising coiled coil structure and a-helical structure for use as antiviral, anti-HIV and anti-SIV agents</p>

    <p>          Weiss, Carol D, He, Yong, Vassell, Russell, and De, Rosny Eve</p>

    <p>          PATENT:  WO <b>2005018666</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2003  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (The Government of the United States of America, as Represented by the Department of Health and Human Services USA and De Rosny, Eve</p>

    <br />

    <p>15.   47994   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Hiv integrase inhibitors</p>

    <p>          Williams, Peter D, Wai, John S, Embrey, Mark W, Staas, Donnette D, Zhuang, Linghang, and Langford, HMarie</p>

    <p>          PATENT:  WO <b>2005092099</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005  PP: 135 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <br />

    <p>16.   47995   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          A soluble factor secreted by an HIV-1-resistant cell line blocks transcription through inactivating the DNA-binding capacity of the NF-kappa B p65/p50 dimer</p>

    <p>          Lesner, A, Li, YC, Nitkiewicz, J, Li, GH, Kartvelishvili, A, Kartvelishvili, M, and Simm, M</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2005</b>.  175(4): 2548-2554, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232010400065">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232010400065</a> </p><br />

    <p>17.   47996   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Methods using a combination of 1-benzoyl-4-[2-[4-methoxy-7-(3-methyl-1H-1,2,4-triazol-1-yl)-1H-pyrrolo[2,3-c]pyridin-3-yl]-1,2-dioxoethyl]-piperazine and another anti-HIV agent for treating HIV infection</p>

    <p>          Lin, Pin-Fang, Nowicka-Sans, Beata, and Yamanaka, Gregory</p>

    <p>          PATENT:  US <b>20050215545</b>  ISSUE DATE: 20050929</p>

    <p>          APPLICATION: 2005-64683  PP: 14 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <br />

    <p>18.   47997   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Methods using a combination of 1-benzoyl-4-[2-[4-fluoro-7-(1H-1,2,3-triazol-1-yl)-1H-pyrrolo[2,3-c]pyridin-3-yl]-1,2-dioxoethyl]piperazine and another anti-HIV agent for treating HIV infection</p>

    <p>          Lin, Pin-Fang, Nowicka-Sans, Beata, and Yamanaka, Gregory</p>

    <p>          PATENT:  US <b>20050215544</b>  ISSUE DATE: 20050929</p>

    <p>          APPLICATION: 2005-61273  PP: 14 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <br />
    
    <p>19.   47998   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Methods using a combination of 1-benzoyl-4-[2-(4,7-dimethoxy-1H-pyrrolo[2,3-c]pyridin-3-yl)-1,2-dioxoethyl]-piperazine and another anti-HIV agent for treating HIV infection</p>

    <p>          Lin, Pin-Fang, Nowicka-Sans, Beata, and Yamanaka, Gregory</p>

    <p>          PATENT:  US <b>20050215543</b>  ISSUE DATE: 20050929</p>

    <p>          APPLICATION: 2005-57667  PP: 14 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <br />

    <p>20.   47999   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          Hiv integrase inhibitors</p>

    <p>          Han, Wei, Egbertson, Melissa, Wai, John S, Zhuang, Linghang, Ruzek, Rowena D, Perlow, Debra S, and Obligado, Vanessa E</p>

    <p>          PATENT:  WO <b>2005087767</b>  ISSUE DATE:  20050922</p>

    <p>          APPLICATION: 2005  PP: 83 pp.</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <br />
    
    <p>21.   48000   HIV-LS-333; SCIFINDER-HIV-10/11/2005</p>

    <p class="memofmt1-2">          5-Hydroxyindole-3-carboxylates derivatives and their application as anti-HBV or anti-HIV agents</p>

    <p>          Gong, Ping and Zhao, Yanfang</p>

    <p>          PATENT:  WO <b>2005087729</b>  ISSUE DATE:  20050922</p>

    <p>          APPLICATION: 2005  PP: 68 pp.</p>

    <p>          ASSIGNEE:  (Shenyang Pharmaceutical University, Peop. Rep. China</p>

    <br />

    <p>22.   48001   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          Development and application of a high-throughput screening assay for HIV-1 integrase enzyme activities</p>

    <p>          John, S, Fletcher, TM, and Jonsson, CB</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2005</b>.  10(6): 606-614, 9</p>

    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231985900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231985900008</a> </p><br />
    <br clear="all">

    <p>23.   48002   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          A novel homodimeric lactose-binding lectin from the edible split gill medicinal mushroom Schizophyllum commune</p>

    <p>          Han, CH, Liu, QH, Ng, TB, and Wang, HX</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2005</b>.  336(1): 252-257, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231941500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231941500037</a> </p><br />

    <p>24.   48003   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          Antiviral activity of SPD754 against clinical isolates of HIV-1 resistant to other nucleoside reverse transcriptase inhibitors</p>

    <p>          Bethell, RC, Parkin, N, and Lie, Y</p>

    <p>          ANTIVIRAL THERAPY <b>2003</b>.  8(3): U21-U22, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900015</a> </p><br />

    <p>25.   48004   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          Molecular mechanisms of resistance to tenofovir by HIV-1 reverse transcriptase containing a di-serine insertion after residue 69 and multiple thymidine analogue-associated mutations</p>

    <p>          White, KL, Chen, JM, Margot, NA, Wrin, T, Petropoulos, CJ, Naeger, LK, Swaminathan, S, and Miller, MD</p>

    <p>          ANTIVIRAL THERAPY <b>2003</b>.  8(3): U44-U45, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900045">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900045</a> </p><br />

    <p>26.   48005   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p><b>          Rare 1 and 2 amino acid insertions in the non-nucleoside reverse transcriptase inhibitor (NNRTI) binding pocket of HIV-1 reverse transcriptase affect NNRTI susceptibility</b> </p>

    <p>          Winters, MA, Kagan, RM, Heseltine, PNR, Kovari, L, and Merigan, TC</p>

    <p>          ANTIVIRAL THERAPY <b>2003</b>.  8(3): U52-U53, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900056</a> </p><br />

    <p>27.   48006   HIV-LS-333; WOS-HIV-10/09/2005</p>

    <p class="memofmt1-2">          The impact of the HIV reverse transcriptase M184V mutation in combination with single thymidine analogue mutations on nucleoside reverse transcriptase inhibitor resistance in clinical samples from a large patient database</p>

    <p>          Ross, L, Parkin, N, Bates, M, Fisher, R, St, Clair M, Tisdale, M, and Lanier, R</p>

    <p>          ANTIVIRAL THERAPY <b>2003</b>.  8(3): U134-U135, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900153">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231265900153</a> </p><br />

    <p>28.   48007   HIV-LS-333; WOS-HIV-10/16/2005</p>

    <p class="memofmt1-2">          HIV-1 Nef regulates the release of superoxide anions from human macrophages</p>

    <p>          Olivetta, E, Pietraforte, D, Schiavoni, I, Minetti, M, Federico, M, and Sanchez, M</p>

    <p>          BIOCHEMICAL JOURNAL <b>2005</b>.  390: 591-602, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232069900022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232069900022</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
