

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-359.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lxi/9NnVLmvUEGW3Kei1i78RszuopingUtQrNk3K90W8qi+D3YKI8A4ryoHtyWc3ccMWpl1AHTZN7VpjVQeQaomkSyrQUymgZKMxo1cAw94XSq7ZSvqXxSWIqiQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B2549C10" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-359-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59163   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by vesicular stomatitis virus envelope glycoprotein pseudotyped baculovirus vector-transduced ribozyme in mammalian cells</p>

    <p>          Kaneko, Hiroyasu, Suzuki, Hitoshi, Abe, Takashi, Miyano-Kurosaki, Naoko, and Takaku, Hiroshi</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  349(4): 1220-1227</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     59164   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p class="memofmt1-2">          The design and synthesis of 9-phenylcyclohepta[d]pyrimidine-2,4-dione derivatives as potent non-nucleoside inhibitors of HIV reverse transcriptase</p>

    <p>          Wang, X, Lou, Q, Guo, Y, Xu, Y, Zhang, Z, and Liu, J</p>

    <p>          Org Biomol Chem <b>2006</b>.  4(17): 3252-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17036113&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17036113&amp;dopt=abstract</a> </p><br />

    <p>3.     59165   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          HIV-1 virion maturation inhibitors, and compositions and treatments using them</p>

    <p>          Blair, Wade Stanton, Butler, Scott Lee, Cao, Joan Qun, Chu, Wai-Lam Alexis, Jackson, Roberta Lynn, Patick, Amy Karen, and Peng, Qinghai</p>

    <p>          PATENT:  WO <b>2006097848</b>  ISSUE DATE:  20060921</p>

    <p>          APPLICATION: 2006  PP: 62pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59166   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p class="memofmt1-2">          HIV-1 Resistance to the Small Molecule Maturation Inhibitor 3-O-{3&#39;,3&#39;-dimethylsuccinyl}-betulinic acid is Conferred by a Variety of Single Amino Acid Substitutions at the CA-SP1 Cleavage Site in Gag</p>

    <p>          Zhou, J, Chen, CH, and Aiken, C</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035324&amp;dopt=abstract</a> </p><br />

    <p>5.     59167   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p class="memofmt1-2">          4-Aminopyrimidines as novel HIV-1 inhibitors</p>

    <p>          Gadhachanda, VR, Wu, B, Wang, Z, Kuhen, KL, Caldwell, J, Zondler, H, Walter, H, Havenhand, M, and He, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035019&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17035019&amp;dopt=abstract</a> </p><br />

    <p>6.     59168   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p><b>          Simple synthesis of novel acyclic (E)-bromovinyl nucleosides as potential antiviral agents</b> </p>

    <p>          Kim, Jin Woo and Hong, Joon Hee</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2006</b>.  25(8): 879-887</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     59169   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Mutational pathways, resistance profile, and side effects of cyanovirin relative to human immunodeficiency virus type 1 strains with N-glycan deletions in their gp120 envelopes</p>

    <p>          Balzarini, Jan, Van Laethem, Kristel, Peumans, Willy J, Van Damme, Els JM, Bolmstedt, Anders, Gago, Federico, and Schols, Dominique</p>

    <p>          Journal of Virology <b>2006</b>.  80(17): 8411-8421</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     59170   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Novel HIV-1 integrase inhibitors: 8-Hydroxy-3,4-dihydropyrrolo[1,2-a]pyrazine-1(2H)-ones</p>

    <p>          Kim, Boyoung, Fisher, Thorsten, Lyle, Terry A, Young, Steven D, Vacca, Joseph P, Hazuda, Daria J, Felock, Peter J, Schleif, William A, Gabryelski, Lori, and Wai, John S</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-249</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     59171   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV and CD4 Down-modulation activities of novel CADA compounds</p>

    <p>          Bell, Thomas W, Anugu, Sreenivasa, Duffy, Noah, Vermeire, Kurt, and Schols, Dominique</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-227</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   59172   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p><b>          Synthesis and Anti-HIV Activity of Novel Cyclopentenyl Nucleoside Analogues of 8-Azapurine</b> </p>

    <p>          Canoa, P, Gonzalez-Moa, MJ, Teijeira, M, Teran, C, Uriarte, E, Pannecouque, C, and De, Clercq E</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2006</b>.  54(10): 1418-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015980&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17015980&amp;dopt=abstract</a> </p><br />

    <p>11.   59173   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Non-nucleoside-reverse-transcriptase-inhibitor-based HAART and osteoporosis in HIV-infected subjects</p>

    <p>          Bongiovanni, Marco, Fausto, Alfonso, Cicconi, Paola, Aliprandi, Alberto, Cornalba, Giampaolo, Bini, Teresa, Sardanelli, Francesco, and D&#39;Arminio Monforte, Antonella</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2006</b>.  58(2): 485-486</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   59174   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Is HIV drug resistance a limiting factor in the development of anti-HIV NNRTI and NRTI-based vaginal microbicide strategies?</p>

    <p>          Martinez, Jorge, Coplan, Paul, and Wainberg, Mark A</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 343-350</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   59175   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Synthesis of amino acid derived seven-membered lactams by RCM and their evaluation against HIV protease</p>

    <p>          Zaman, S, Campaner, P, and Abell, AD</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17010620&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17010620&amp;dopt=abstract</a> </p><br />

    <p>14.   59176   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of amino acid hydrazide derivatives and related compounds as HIV protease inhibitors</p>

    <p>          Ekegren, Jenny, Hallberg, Anders, Wallberg, Hans, Samuelsson, Bertil, and Kannan, Mahalingan</p>

    <p>          PATENT:  WO <b>2006084688</b>  ISSUE DATE:  20060817</p>

    <p>          APPLICATION: 2006  PP: 125pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59177   HIV-LS-359; PUBMED-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Differential regulation of CXCR4 and CCR5 expression by interleukin (IL)-4 and IL-13 is associated with inhibition of chemotaxis and human immunodeficiency Virus (HIV) type 1 replication but not HIV entry into human monocytes</p>

    <p>          Creery, D, Weiss, W, Graziani-Bowering, G, Kumar, R, Aziz, Z, Angel, JB, and Kumar, A</p>

    <p>          Viral Immunol <b>2006</b>.  19(3): 409-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16987060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16987060&amp;dopt=abstract</a> </p><br />

    <p>16.   59178   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Alpha interferon potently enhances the anti-human immunodeficiency virus type 1 activity of APOBEC3G in resting primary CD4 T cells</p>

    <p>          Chen, Keyang, Huang, Jialing, Zhang, Chune, Huang, Sophia, Nunnari, Giuseppe, Wang, Feng-xiang, Tong, Xiangrong, Gao, Ling, Nikisher, Kristi, and Zhang, Hui</p>

    <p>          Journal of Virology <b>2006</b>.  80(15): 7645-7657</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59179   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Inhibitors of HIV-1 protease: 10 years after</p>

    <p>          Mastrolorenzo, Antonio, Rusconi, Stefano, Scozzafava, Andrea, and Supuran, Claudiu T</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2006</b>.  16(8): 1067-1091</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   59180   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Current state-of-the-art in preclinical and clinical development of novel non-nucleoside HIV-1 reverse transcriptase inhibitors</p>

    <p>          Silvestri, Romano and Maga, Giovanni</p>

    <p>          Expert Opinion on Therapeutic Patents <b>2006</b>.  16(7): 939-962</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   59181   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;,3&#39;-didehydro-2&#39;,3&#39;-dideoxynucleosides via nucleoside route</p>

    <p>          Len, Christophe and Postel, Denis</p>

    <p>          Current Organic Synthesis <b>2006</b>.  3(3): 261-281</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   59182   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Preparation of 2-oxo-3-cyano-1,6a-diaza-tetrahydro-fluoranthenes as anti-HIV agents</p>

    <p>          Kesteleyn, Bart Rudolf Romanie, Raboisson, Pierre Jean-Marie, Van De Vreken, Wim, and Canard, Maxime Francis Jean-Marie Ghislain</p>

    <p>          PATENT:  WO <b>2006072636</b>  ISSUE DATE:  20060713</p>

    <p>          APPLICATION: 2006  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   59183   HIV-LS-359; WOS-HIV-10/8/2006</p>

    <p class="memofmt1-2">          Proteasome inhibition reveals that a functional preintegration complex intermediate can be generated during restriction by diverse TRIM5 proteins</p>

    <p>          Anderson, JL, Campbell, EM, Wu, XL, Vandegraaff, N, Engelman, A, and Hope, TJ</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(19): 9754-9760, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240647200039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240647200039</a> </p><br />

    <p>22.   59184   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Anti-HIV activity of stilbene-related heterocyclic compounds</p>

    <p>          Bedoya, Luis M, Del Olmo, Esther, Sancho, Rocio, Barboza, Bianca, Beltran, Manuela, Garcia-Cadenas, Ana E, Sanchez-Palomino, Sonsoles, Lopez-Perez, Jose L, Munoz, Eduardo, San Feliciano, Arturo, and Alcami, Jose</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(15): 4075-4079</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   59185   HIV-LS-359; SCIFINDER-HIV-10/9/2006</p>

    <p class="memofmt1-2">          Synthesis and antiproliferative activity of benzo[d]isothiazole hydrazones</p>

    <p>          Vicini, Paola, Incerti, Matteo, Doytchinova, Irini A, La Colla, Paolo, Busonera, Bernadetta, and Loddo, Roberta</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(5): 624-632</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   59186   HIV-LS-359; WOS-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Effects of lysine to arginine mutations in HIV-1 Vif on its expression and viral infectivity</p>

    <p>          Khamsri, B, Fujita, M, Kamada, K, Piroozmand, A, Yamashita, T, Uchiyama, T, and Adachi, A</p>

    <p>          INTERNATIONAL JOURNAL OF MOLECULAR MEDICINE <b>2006</b>.  18(4): 679-683, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240803700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240803700023</a> </p><br />

    <p>25.   59187   HIV-LS-359; WOS-HIV-10/16/2006</p>

    <p class="memofmt1-2">          HIV-1 entry inhibitors: Classes, applications and factors affecting potency</p>

    <p>          Sterjovski, J, Churchill, MJ, Wesselingh, SL, and Gorry, PR</p>

    <p>          CURRENT HIV RESEARCH <b>2006</b>.  4(4): 387-400, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240670400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240670400001</a> </p><br />

    <p>26.   59188   HIV-LS-359; WOS-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Azurin, Plasmodium falciparum Malaria and HIV/AIDS - Inhibition of parasitic and viral growth by azurin</p>

    <p>          Chaudhari, A, Fialho, AM, Ratner, D, Gupta, P, Hong, CS, Kahali, S, Yamada, T, Haldar, K, Murphy, S, Cho, WW, Chauhan, VS, Das Gupta, TK, and Chakrabarty, AM</p>

    <p>          CELL CYCLE <b>2006</b>.  5(15): 1642-1648, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240698300011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240698300011</a> </p><br />

    <p>27.   59189   HIV-LS-359; WOS-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Nucleosides with self-complementary hydrogen-bonding motifs: Synthesis and base-pairing studies of two nucleosides containing the imidazo[4,5-d]pyridazine ring system</p>

    <p>          Ujjinamatada, RK, Paulman, RL, Ptak, RG, and Hosmane, RS</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2006</b>.  14(18): 6359-6367, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615300023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240615300023</a> </p><br />

    <p>28.   59190   HIV-LS-359; WOS-HIV-10/16/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel cyclopropyl nucleosides, phosphonate nucleosides and phosphonic acid nucleosides</p>

    <p>          Oh, CH and Hong, JH</p>

    <p>          ARCHIV DER PHARMAZIE <b>2006</b>.  339(9): 507-512, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240793200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240793200003</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
