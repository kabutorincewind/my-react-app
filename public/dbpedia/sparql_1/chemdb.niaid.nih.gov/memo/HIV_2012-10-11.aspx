

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-10-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tblG8ZBXvwplaWB//0jMlYtchWXqOM3Gbt92bd8ghO03sxRkNAg/fgMitarIwhRL/dsDHAjp6qpRCOS8XdfHNvDo8FftUGDee9pbe78moJWdnCEQ2/91MAlkk+E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F33878A6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 28 - October 11, 2012</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985312">Ionic Derivatives of Betulinic acid as Novel HIV-1 Protease Inhibitors.</a> Zhao, H., S.S. Holmes, G.A. Baker, S. Challa, H.S. Bose, and Z. Song. Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. 27(5): p. 715-721. PMID[21985312].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22954898">Phosphoramidate Derivatives of Acyclovir: Synthesis and Antiviral Activity in HIV-1 and HSV-1 Models in Vitro.</a> Zakirova, N.F., A.V. Shipitsyn, M.V. Jasko, M.M. Prokofjeva, V.L. Andronova, G.A. Galegov, V.S. Prassolov, and S.N. Kochetkov. Bioorganic &amp; Medicinal Chemistry, 2012. 20(19): p. 5802-5809. PMID[22954898].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233539">Nitrogen-containing Polyhydroxylated Aromatics as HIV-1 Integrase Inhibitors: Synthesis, Structure-Activity Relationship Analysis, and Biological Activity.</a> Yu, S., L. Zhang, S. Yan, P. Wang, T. Sanchez, F. Christ, Z. Debyser, N. Neamati, and G. Zhao. Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. 27(5): p. 628-640. PMID[22233539].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22892890">CXCR4 Chemokine Receptor Antagonists: Nickel(II) Complexes of Configurationally Restricted Macrocycles.</a> Smith, R., D. Huskens, D. Daelemans, R.E. Mewis, C.D. Garcia, A.N. Cain, T.N. Freeman, C. Pannecouque, E.D. Clercq, D. Schols, T.J. Hubin, and S.J. Archibald. Dalton Transactions, 2012. 41(37): p. 11369-11377. PMID[22892890].</p>

    <p class="plaintext"><b>[PubMed].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22748181">Caveolin-1 Suppresses Human Immunodeficiency Virus-1 Replication by Inhibiting Acetylation of Nf-Kappa-b.</a> Simmons, G.E., Jr., H.E. Taylor, and J.E. Hildreth. Virology, 2012. 432(1): p. 110-119. PMID[22748181].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22811524">Transmembrane Protein Aptamers that Inhibit CCR5 Expression and HIV Coreceptor Function.</a> Scheideman, E.H., S.A. Marlatt, Y. Xie, Y. Hu, R.E. Sutton, and D. Dimaio. Journal of Virology, 2012. 86(19): p. 10281-10292. PMID[22811524].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22897493">A Suite of Modular Fluorescence Assays Interrogate the Human Immunodeficiency Virus Glycoprotein-41 Coiled Coil and Assist in Determining Binding Mechanism of Low Molecular Weight Fusion Inhibitors.</a> Gochin, M. Assay and Drug Development Technologies, 2012. 10(5): p. 407-416. PMID[22897493].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22819150">In Vitro anti-HIV-1 Activity of Salicylidene acylhydrazide Compounds.</a> Forthal, D.N., T.B. Phan, A.V. Slepenkin, G. Landucci, H. Chu, M. Elofsson, and E. Peterson. International Journal of Antimicrobial Agents, 2012. 40(4): p. 354-360. PMID[22819150].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22875968">A Quantitative Measurement of Antiviral Activity of Anti-Human Immunodeficiency Virus Type 1 Drugs against Simian Immunodeficiency Virus Infection: Dose-response Curve Slope Strongly Influences Class-specific Inhibitory Potential.</a> Deng, K., M.C. Zink, J.E. Clements, and R.F. Siliciano. Journal of Virology, 2012. 86(20): p. 11368-11372. PMID[22875968].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23009326">Correlating Conformational Shift Induction with Altered Inhibitor Potency in a Multidrug Resistant HIV-1 Protease Variant.</a> de Vera, I.M., M.E. Blackburn, and G.E. Fanucci. Biochemistry, 2012. 51(40): p. 7813-7815. PMID[23009326].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22892387">The M-T Hook Structure Is Critical for Design of HIV-1 Fusion Inhibitors.</a> Chong, H., X. Yao, J. Sun, Z. Qiu, M. Zhang, S. Waltersperger, M. Wang, S. Cui, and Y. He. The Journal of Biological Chemistry, 2012. 287(41): p. 34558-34568. PMID[22892387].</p>

    <p class="plaintext"><b>[PubMed].</b>HIV_0928-101112.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308121600029">Topical Gel Formulation of Broadly Neutralizing anti-HIV-1 Monoclonal Antibody Vrc01 Confers Protection against HIV-1 Vaginal Challenge in a Humanized Mouse Model.</a> Veselinovic, M., C.P. Neff, L.R. Mulder, and R. Akkina. Virology, 2012. 432(2): p. 505-510. ISI[000308121600029].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308353101087">In Search for a New anti-HIV-1 Drug through Inhibition of Ca-Cypa Interaction.</a> Verathamjamras, C., Y.S. Tian, T. Yasunaga, T. Takagi, N. Kawashita, and M. Kameoka. International Journal of Infectious Diseases, 2012. 16: p. E194-E194. ISI[000308353101087].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307627800012">Selected Drugs with Reported Secondary Cell-Differentiating Capacity Prime Latent HIV-1 Infection for Reactivation.</a> Shishido, T., F. Wolschendorf, A. Duverger, F. Wagner, J. Kappes, J. Jones, and O. Kutsch. Journal of Virology, 2012. 86(17): p. 9055-9069. ISI[000307627800012].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308115300020">Synthesis and Conformation of Novel 3&#39;-branched Threosyl-5&#39;-deoxyphosphonic acid Nucleoside Analogues.</a> Shen, G.H., L. Kang, E. Kim, W. Lee, and J.H. Hong. Bulletin of the Korean Chemical Society, 2012. 33(8): p. 2574-2580. ISI[000308115300020].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308091500341">Identification and Optimization of a Novel Class of HIV Fusion Inhibitors via High-Throughput Synthesis &amp; Screening of Peptide Arrays.</a> Schepens, W., W. Schaaper, P. Timmerman, C. Buyck, J. Slootstra, O. Goethals, B. Stoops, N. Kriek, K. Amssoms, J. Benschop, J. Wu, G. Dams, K. Van Emelen, J.W. Thuring, and B. Malcolm. Journal of Peptide Science, 2012. 18: p. S142-S142. ISI[000308091500341].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>
    
    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307920600017">Diverse Combinatorial Design, Synthesis and in Vitro Evaluation of New Hept Analogues as Potential Non-nucleoside HIV-1 Reverse Transcription Inhibitors.</a></p>

    <p class="plaintext">Puig-de-la-Bellacasa, R., L. Gimenez, S. Pettersson, R. Pascual, E. Gonzalo, J.A. Este, B. Clotet, J.I. Borrell, and J. Teixido. European Journal of Medicinal Chemistry, 2012. 54: p. 159-174. ISI[000307920600017].</p>

    <p class="plaintext"><b>[WOS].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308211100039">3-Phenylcoumarins as Inhibitors of HIV-1 Replication.</a> Olmedo, D., R. Sancho, L.M. Bedoya, J.L. Lopez-Perez, E. del Olmo, E. Munoz, J. Alcami, M.P. Gupta, and A.S. Feliciano. Molecules, 2012. 17(8): p. 9245-9257. ISI[000308211100039].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307027100024">Use of ATP Analogs to Inhibit HIV-1 Transcription.</a> Narayanan, A., G. Sampey, R. Van Duyne, I. Guendel, K. Kehn-Hall, J. Roman, R. Currer, H. Galons, N. Oumata, B. Joseph, L. Meijer, M. Caputi, S. Nekhai, and F. Kashanchi. Virology, 2012. 432(1): p. 219-231. ISI[000307027100024].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307268100045">(Naphthalen-1-tloxy)-acetic acid benzylidene/(1-phenyl-ethylidene)-hydrazide Derivatives: Synthesis, Antimicrobial Evaluation, and QSAR Studies.</a> Narang, R., B. Narasimhan, and S. Sharma. Medicinal Chemistry Research, 2012. 21(9): p. 2526-2547. ISI[000307268100045].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307883100024">Inhibition of XMRV and HIV-1 Proteases by Pepstatin A and Acetyl-pepstatin.</a> Matuz, K., J. Motyan, M. Li, A. Wlodawer, and J. Tozser. Federation of European Biochemical Societies Journal, 2012. 279(17): p. 3276-3286. ISI[000307883100024].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308091500011">De novo Design of Artificial Peptides That Specifically Interact with HIV-1 gp41 to Inhibit Viral-Cell Membrane Fusion and Infection.</a> Liu, K., L. Cai, W. Shi, C. Wang, and K. Wang. Journal of Peptide Science, 2012. 18: p. S22-S23. ISI[000308091500011].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308286900016">APOBEC3G Inhibits MicroRNA-mediated Repression of Translation by Interfering with the Interaction between Argonaute-2 and MOV10.</a> Liu, C., X. Zhang, F. Huang, B. Yang, J. Li, B.F. Liu, H.H. Luo, P. Zhang, and H. Zhang. Journal of Biological Chemistry, 2012. 287(35): p. 29373-29383. ISI[000308286900016].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307937800004">First Total Syntheses of Oresbiusins A and B, Their Antipodes, and Racemates: Configuration Revision and anti-HIV Activity.</a> Hwu, J.R., T.G. Varadaraju, I.S. Abd-Elazem, and R.C.C. Huang. European Journal of Organic Chemistry, 2012. (25): p. 4684-4688. ISI[000307937800004]. <b>[WOS].</b> HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308091500306">Use of Substituted Oxamide Structure in Designing Pseudo-symmetric HIV Protease Inhibitors to Employ Multiple Bridging Water Molecules.</a> Hidaka, K., Y. Toda, M. Adachi, R. Kuroki, and Y. Kiso. Journal of Peptide Science, 2012. 18: p. S129-S130. ISI[000308091500306].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307627800051">Identification of Novel T Cell Factor 4 (TCF-4) Binding Sites on the HIV Long Terminal Repeat Which Associate with TCF-4, Beta-catenin, and SMAR1 to Repress HIV Transcription.</a> Henderson, L.J., S.D. Narasipura, V. Adarichev, F. Kashanchi, and L. Al-Harthi. Journal of Virology, 2012. 86(17): p. 9495-9503. ISI[000307627800051].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307437800011">8-O-4&#39;-Neolignans from Flower Buds of Magnolia fargesii and Their Biological Activities.</a> Gao, X.M., Y.Q. Shen, L.Y. Yang, L.D. Shu, G.P. Li, and Q.F. Hu. Journal of the Brazilian Chemical Society, 2012. 23(7): p. 1274. ISI[000307437800011].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000307215600053">A Conformationally Frozen Peptoid Boosts CXCR4 Affinity and anti-HIV Activity.</a> Demmer, O., A.O. Frank, F. Hagn, M. Schottelius, L. Marinelli, S. Cosconati, R. Brack-Werner, S. Kremb, H.J. Wester, and H. Kessler. Angewandte Chemie-International Edition, 2012. 51(32): p. 8110-8113. ISI[000307215600053].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308091500075">A Synthetic Heparan Sulfate-mimetic Peptide Conjugated to a Mini CD4 Displays Very High anti-HIV Activity.</a> Connell, B.J., F. Baleux, Y.M. Coic, P. Clayette, D. Bonnaffe, and H. Lortat-Jacob. Journal of Peptide Science, 2012. 18: p. S45-S46. ISI[000308091500075].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308116600020">Cleidbrevoids A-C, New Clerodane Diterpenoids from Cleidion brevipetiolatum.</a> Cheng, Y.Y., S.F. Li, Y. Zhang, G.H. Tang, Y.T. Di, X.J. Hao, S.L. Li, and H.P. He. Fitoterapia, 2012. 83(6): p. 1100-1104. ISI[000308116600020].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000308042000010">An Efficient Synthesis of a Hydroxyethylamine (HEA) Isostere and Its a-Aminophosphonate and Phosphoramidate Derivatives as Potential anti-HIV Agents.</a> Bhattacharya, A.K., K.C. Rana, C. Pannecouque, and E. De Clercq. Chemmedchem, 2012. 7(9): p. 1601-1611. ISI[000308042000010].</p>

    <p class="plaintext"><b>[WOS].</b>HIV_0928-101112.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120907&amp;CC=WO&amp;NR=2012118124A1&amp;KC=A1">Preparation of Cyclic Peptides Containing Amidine Bond as Novel Chemokine Receptor Antagonist.</a> Fujii, N., H. Ohno, S. Oishi, E. Inokuchi, T. Kubo, M. Matsuoka, and K. Shimura. Patent. 2012. 2012-JP55099 2012118124: 60pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120912&amp;CC=CN&amp;NR=102659714A&amp;KC=A">Preparation of 1,2,3-Thiadiazole Derivatives for Treatment of HIV Infection.</a> Liu, X., X. Jiang, and P. Zhan. Patent. 2012. 2012-10146323 102659714: 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120816&amp;CC=WO&amp;NR=2012109314A2&amp;KC=A2">Potent HIV-Inhibiting Chimeric Polypeptides Targeting CCR5 and gp41.</a> Liwang, P. and B. Zhao. Patent. 2012. 2012-US24265 2012109314: 87pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0928-101112.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20120920&amp;CC=WO&amp;NR=2012125900A1&amp;KC=A1">Preparation of 2&#39;-Allene-substituted Nucleoside Derivs. As Antiviral Agents.</a> Or, Y.S., J. Ma, G. Wang, I.J. Kim, J. Long, and Y.-L. Qiu. Patent. 2012. 2012-US29381 2012125900: 269pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0928-101112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
