

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-03-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="torswG95IGdALG5DVtvrAjY30d/08VmQGllSwxJLkDnZ6yP/oHfq2mbZ80RShoFtDVmlew0NExKSFAkuYiN/5UsGHQugJxNMdDeMimbw0QVItwjdpXW3ZKSi4iQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="88E849BA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  March 16 - March 29, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22426528">Synthesis and Antimicrobial Activity of N&#39;-Heteroarylidene-1-adamantylcarbohydrazides and (+/-)-2-(1-Adamantyl)-4-acetyl-5-[5-(4-substituted phenyl-3-isoxazolyl)]-1,3,4-oxadiazolines.</a> El-Emam, A.A., K.A. Alrashood, M.A. Al-Omar, and A.M. Al-Tamimi. Molecules (Basel, Switzerland), 2012. 17(3): p. 3475-3483; PMID[22426528].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>     
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430639">Antimicrobial Activity of Saponin-Rich Fraction from Camellia oleifera Cake and Its Effect on Cell Viability of Mouse Macrophage Raw 264.7.</a> Hu, J.L., S.P. Nie, D.F. Huang, C. Li, M.Y. Xie, and Y. Wan. Journal of the Science of Food and Agriculture, 2012. <b>[Epub ahead of print]</b>; PMID[22430639].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>     
    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22420410">Structure Elucidation of a New Natural Diketopiperazine from a Microbispora aerata Strain Isolated from Livingston Island, Antarctica.</a> Ivanova, V., H. Laatsch, M. Kolarova, and K. Aleksieva. Natural Product Research, 2012. <b>[Epub ahead of print]</b>; PMID[22420410].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22326355">Biofilm Inhibition by Cymbopogon citratus and Syzygium aromaticum Essential Oils in the Strains of Candida albicans.</a> Khan, M.S. and I. Ahmad. Journal of Ethnopharmacology, 2012. 140(2): p. 416-423; PMID[22326355].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22438852">Targeting the Oxidative Stress Response System of Fungi with Redox-potent Chemosensitizing Agents.</a> Kim, J.H., K.L. Chan, N.C. Faria, L. Martins Mde, and B.C. Campbell. Frontiers in Microbiology, 2012. 3: p. 88; PMID[22438852].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22441334">Synthesis of a New Group of Aliphatic Hydrazide Derivatives and the Correlations between Their Molecular Structure and Biological Activity.</a> Kostecka, M. Molecules (Basel, Switzerland), 2012. 17(3): p. 3560-3573; PMID[22441334].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22310557">The Anti-candida Activity of Thymbra capitata Essential Oil: Effect Upon Pre-formed Biofilm.</a> Palmeira-de-Oliveira, A., C. Gaspar, R. Palmeira-de-Oliveira, A. Silva-Dias, L. Salgueiro, C. Cavaleiro, C. Pina-Vaz, J. Martinez-de-Oliveira, J.A. Queiroz, and A.G. Rodrigues. Journal of Ethnopharmacology, 2012. 140(2): p. 379-383; PMID[22310557].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22365752">Novel N&#39;-Benzylidene benzofuran-3-carbohydrazide Derivatives as Antitubercular and Antifungal Agents.</a> Telvekar, V.N., A. Belubbi, V.K. Bairwa, and K. Satardekar. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(6): p. 2343-2346; PMID[22365752].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22450678">In Vitro Antimicrobial and Antioxidant Activities of Ethanolic Extract of Lyophilized Mycelium of Pleurotus ostreatus PQMZ91109.</a> Vamanu, E. Molecules (Basel, Switzerland), 2012. 17(4): p. 3653-3671; PMID[22450678].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430960">A Potent Plant-derived Antifungal Acetylenic acid Mediates Its Activity by Interfering with Fatty Acid Homeostasis.</a> Xu, T., S.K. Tripathi, Q. Feng, M.C. Lorenz, M.A. Wright, M.R. Jacob, M.M. Mask, S.R. Baerson, X.C. Li, A.M. Clark, and A.K. Agarwal. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22430960].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>
    <p> </p>
    
    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22447158">Synthesis of 3-Alkyl enol Mimics Inhibitors of Type II Dehydroquinase: Factors Influencing Their Inhibition Potency.</a> Blanco, B., A. Sedes, A. Peon, H. Lamb, A.R. Hawkins, L. Castedo, and C. Gonzalez-Bello. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22447158].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22422605">Antibacterial and Antimycobacterial Lignans and Flavonoids from Larrea tridentata.</a> Favela-Hernandez, J.M., A. Garcia, E. Garza-Gonzalez, V.M. Rivas-Galindo, and M.R. Camacho-Corona. Phytotherapy Research : PTR, 2012. <b>[Epub ahead of print]</b>; PMID[22422605].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22361273">Anti-mycobacterial Activities of Some Cationic and Anionic Calix[4]arene Derivatives</a>. Mourer, M., H. Massimba Dibama, P. Constant, M. Daffe, and J.B. Regnouf-de-Vains. Bioorganic &amp; Medicinal Chemistry, 2012. 20(6): p. 2035-2041; PMID[22361273].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22313627">Effect of an Alkaloidal Fraction of Tabernaemontana elegans (Stapf.) on Selected Micro-organisms.</a> Pallant, C.A., A.D. Cromarty, and V. Steenkamp. Journal of Ethnopharmacology, 2012. 140(2): p. 398-404; PMID[22313627].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22449006">Design, Synthesis and Structure-activity Correlations of Novel Dibenzo[B,D]furan, Dibenzo[B,D]thiophene and N-Methylcarbazole Clubbed 1,2,3-Triazoles as Potent Inhibitors of Mycobacterium tuberculosis.</a> Patpi, S.R., L. Pulipati, P. Yogeeswari, D. Sriram, N. Jain, B. Sridhar, R. Murthy, A.D. T, S.V. Kalivendhi, and S. Kantevari. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22449006].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22452568">Structure-based Design of Novel Benzoxazinorifamycins with Potent Binding Affinity to Wild-type and Rifampin-resistant Mutant Mycobacterium tuberculosis RNA Polymerases.</a> Showalter, H.D., S.K. Gill, H. Xu, P.D. Kirchhoff, G.A. Garcia, S.G. Franzblau, B. Wan, N. Zhang, K.W. Peng, T. Cierpicki, and A.J. Turbiak. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22452568].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>
    <p> </p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22422350">Design and Synthesis of Screening Libraries Based on the Muurolane Natural Product Scaffold.</a> Barnes, E.C., V. Choomuenwai, K.T. Andrews, R.J. Quinn, and R.A. Davis. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22422350].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22421887">Synthesis and Biological Activity of Cymantrene and Cyrhetrene 4-Aminoquinoline Conjugates against Malaria, Leishmaniasis, and Trypanosomiasis.</a> Glans, L., W. Hu, C. Jost, C. de Kock, P.J. Smith, M. Haukka, H. Bruhn, U. Schatzschneider, and E. Nordlander. Dalton Transactions (Cambridge, England: 2003), 2012. <b>[Epub ahead of print]</b>; PMID[22421887].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22439843">Discovery of Plasmodium Vivax N-Myristoyltransferase Inhibitors: Screening, Synthesis, and Structural Characterization of Their Binding Mode.</a> Goncalves, V., J.A. Brannigan, D. Whalley, K.H. Ansell, B. Saxty, A.A. Holder, A.J. Wilkinson, E.W. Tate, and R.J. Leatherbarrow. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22439843].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22430967">In Vitro Antimalarial Activity and Drug Interactions of Fenofibric Acid.</a> Wong, R.P. and T.M. Davis. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22430967].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22429524">Antimalarial Activity of Newly Synthesised Chalcone Derivatives in Vitro.</a> Yadav, N., S.K. Dixit, A. Bhattacharya, L.C. Mishra, M. Sharma, S.K. Awasthi, and V.K. Bhasin. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22429524].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22390538">3,5-Diaryl-2-aminopyridines as a Novel Class of Orally Active Antimalarials Demonstrating Single Dose Cure in Mice and Clinical Candidate Potential.</a> Younis, Y., F. Douelle, T.S. Feng, D.G. Cabrera, C.L. Manach, A.T. Nchinda, S. Duffy, K.L. White, D.M. Shackleford, J. Morizzi, J. Mannila, K. Katneni, R. Bhamidipati, K.M. Zabiulla, J.T. Joseph, S. Bashyam, D. Waterson, M.J. Witty, D. Hardick, S. Wittlin, V. Avery, S.A. Charman, and K. Chibale. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22390538].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22435599">Lead Optimization of 3-Carboxyl-4(1h)-quinolones to Deliver Orally Bioavailable Antimalarials.</a> Zhang, Y., J.A. Clark, M.C. Connelly, F. Zhu, J. Min, W.A. Guiguemde, A. Pradhan, L. Iyer, A. Furimsky, J. Gow, T. Parman, F. El Mazouni, M.A. Phillips, D.E. Kyle, J. Mirsalis, and R.K. Guy. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22435599].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>
    <p> </p>
    
    <h2>ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22324915">Identification of Novel S-Adenosyl-L-homocysteine hydrolase Inhibitors through Homology-model-based Virtual Screening, Synthesis, and Biological Evaluation.</a> Khare, P., A.K. Gupta, P.K. Gajula, K.Y. Sunkari, A.K. Jaiswal, S. Das, P. Bajpai, T.K. Chakraborty, A. Dube, and A.K. Saxena. Journal of Chemical Information and Modeling, 2012. 52(3): p. 777-791; PMID[22324915].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22448965">Searching for New Chemotherapies for Tropical Diseases: Ruthenium-Clotrimazole Complexes Display High in Vitro Activity against Leishmania major and Trypanosoma cruzi and Low Toxicity toward Normal Mammalian Cells.</a> Martinez, A., T. Carreon, E.A. Iniguez, A. Anzellotti, A. Sanchez, M. Tyan, A. Sattler, L. Herrera, R.A. Maldonado, and R.A. Sanchez-Delgado. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22448965].
    <br />
    <b>[PubMed]</b>. OI_0316-032912.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300715700010">Chemical Composition and in Vitro Antifungal Activity Screening of the Allium ursinum L. (Liliaceae).</a> Bagiu, R.V., B. Vlaicu, and M. Butnariu. International Journal of Molecular Sciences, 2012. 13(2): p. 1426-1436; [ISI:000300715700010].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300674000044">Spectral Characterization and Antimicrobial Activity of Some Schiff Bases Derived from 4-Chloro-2-aminophenol and Various Salicylaldehyde Derivatives.</a> Cinarli, A., D. Gurbuz, A. Tavman, and A.S. Birteksoz. Chinese Journal of Chemistry, 2012. 30(2): p. 449-459; [ISI:000300674000044].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300633400008">Alcl3 Induced (Hetero)Arylation of 2,3-Dichloroquinoxaline: A One-Pot Synthesis of Mono/Disubstituted Quinoxalines as Potential Antitubercular Agents.</a> Kumar, K.S., D. Rambabu, S. Sandra, R. Kapavarapu, G.R. Krishna, M.V.B. Rao, K. Chatti, C.M. Reddy, P. Misra, and M. Pal. Bioorganic &amp; Medicinal Chemistry, 2012. 20(5): p. 1711-1722; [ISI:000300633400008].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300562500001">Antimicrobial Effects of Plant Defence Peptides Expressed by Bovine Endothelial Cells on Intracellular Pathogens.</a> Loeza-Angeles, H., J.E. Lopez-Meza, and A. Ochoa-Zarzosa. Electronic Journal of Biotechnology, 2011. 14(5); [ISI:000300562500001].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300484900037">Synthesis and Biological Evaluation of Some Thiazolidinones as Antimicrobial Agents.</a> Patel, D., P. Kumari, and N. Patel. European Journal of Medicinal Chemistry, 2012. 48: p. 354-362; [ISI:000300484900037].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300642900010">Synthesis, Characterization and Antimicrobial Activity of Palladium(Ii) Complexes with Some Alkyl Derivates of Thiosalicylic acids: Crystal Structure of the Bis(S-Benzyl-thiosalicylate)-palladium(Ii) Complex, Pd(S-Bz-Thiosal)(2).</a> Radic, G.P., V.V. Glodovic, I.D. Radojevic, O.D. Stefanovic, L.R. Comic, Z.R. Ratkovic, A. Valkonen, K. Rissanen, and S.R. Trifunovic. Polyhedron, 2012. 31(1): p. 69-76; [ISI:000300642900010].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p> 
    <p> </p>
    
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300782000012">Synthesis, Antimicrobial Evaluation, and Qsar Analysis of 2-Isopropyl-5-methylcyclohexanol Derivatives.</a> Singh, M., S. Kumar, A. Kumar, P. Kumar, and B. Narasimhan. Medicinal Chemistry Research, 2012. 21(4): p. 511-522; [ISI:000300782000012].
    <br />
    <b>[WOS]</b>. OI_0316-032912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
