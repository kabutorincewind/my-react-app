

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-297.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1o01In6ZZfsDxMmtUWHIbaVTW3tNhzsgZou1/nYX0e8JzHPbE74k1E2L0JxxQMWdr0LBDeaCPsg8LlkJRGegcidrvz52JcSKcNG2aosH693HXpDEprM1L0ODVXE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="98DC4F0F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-297-MEMO</b> </p>

<p>    1.    43458   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           A Peptidomimetic HIV-Entry Inhibitor Directed against the CD4 Binding Site of the Viral Glycoprotein gp120</p>

<p>           Neffe, AT and Meyer, B</p>

<p>           Angew Chem Int Ed Engl 2004. 43(22):  2937-2940</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170309&amp;dopt=abstract</a> </p><br />

<p>    2.    43459   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Identification and characterization of peptides that bind to cyanovirin-N, a potent human immunodeficiency virus-inactivating protein</p>

<p>           Han, Z, Simpson, JT, Fivash, MJ, Fisher, R, and Mori, T</p>

<p>           Peptides 2004. 25(4): 551-61</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165709&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165709&amp;dopt=abstract</a> </p><br />

<p>    3.    43460   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Isolation, Structure, Absolute Stereochemistry, and HIV-1 Integrase Inhibitory Activity of Integrasone, a Novel Fungal Polyketide</p>

<p>           Herath, KB, Jayasuriya, H, Bills, GF, Polishook, JD, Dombrowski, AW, Guan, Z, Felock, PJ, Hazuda, DJ, and Singh, SB</p>

<p>           J Nat Prod 2004. 67(5): 872-874</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165153&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15165153&amp;dopt=abstract</a> </p><br />

<p>    4.    43461   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Nef Stimulates Human Immunodeficiency Virus Type 1 Replication in Primary T Cells by Enhancing Virion-Associated gp120 Levels: Coreceptor-Dependent Requirement for Nef in Viral Replication</p>

<p>           Lundquist, CA, Zhou, J, and Aiken, C</p>

<p>           J Virol 2004. 78(12): 6287-96</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163722&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163722&amp;dopt=abstract</a> </p><br />

<p>    5.    43462   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Design, Synthesis, Anti-HIV Activities, and Metabolic Stabilities of Alkenyldiarylmethane (ADAM) Non-nucleoside Reverse Transcriptase Inhibitors</p>

<p>           Silvestri, MA, Nagarajan, M, De, Clercq E, Pannecouque, C, and Cushman, M</p>

<p>           J Med Chem 2004. 47(12): 3149-62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163195&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163195&amp;dopt=abstract</a> </p><br />

<p>    6.    43463   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Anti-HIV Activity and Conformational Studies of Peptides Derived from the C-Terminal Sequence of SDF-1</p>

<p>           Dettin, M, Pasquato, A, Scarinci, C, Zanchetta, M, De, Rossi A, and Di, Bello C</p>

<p>           J Med Chem 2004. 47(12): 3058-64</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163187&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163187&amp;dopt=abstract</a> </p><br />

<p>    7.    43464   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Quinolones with anti-HIV activity, and preparation thereof</b>((IRM LLC, Bermuda)</p>

<p>           He, Yun, Ellis, David Archer, Anaclerio, Beth Marie, Kuhen, Kelli L, Wu, Baogen, and Jiang, Tao</p>

<p>           PATENT: WO 2004037853 A2;  ISSUE DATE: 20040506</p>

<p>           APPLICATION: 2003; PP: 71 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    8.    43465   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           The interaction between HIV-1 Gag and APOBEC3G</p>

<p>           Cen, S, Guo, F, Niu, M, Saadatmand, J, Deflassieux, J, and Kleiman, L</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159405&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15159405&amp;dopt=abstract</a> </p><br />

<p>    9.    43466   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Potential new anti-human immunodeficiency virus type 1 compounds depress virus replication in cultured human macrophages</p>

<p>           Ewart, GD, Nasr, N, Naif, H, Cox, GB, Cunningham, AL, and Gage, PW</p>

<p>           Antimicrob Agents Chemother 2004.  48(6): 2325-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155246&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15155246&amp;dopt=abstract</a> </p><br />

<p>  10.    43467   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Non-nucleoside reverse transcriptase inhibitors for use in treatment of HIV infection</b>((Ribapharm Inc., USA)</p>

<p>           Girardet, Jean-Luc, Zhang, Zhijun, Hamatake, Robert, de la Rosa Hernandez, Martha A, Gunic, Esmir, Hong, Zhi, Kim, Hongwoo, Koh, Yung-Hyo, Nilar, Shahul, Shaw, Stephanie, and Yao, Nanhua</p>

<p>           PATENT: WO 2004030611 A2;  ISSUE DATE: 20040415</p>

<p>           APPLICATION: 2003; PP: 70 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  11.    43468   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           A second human antiretroviral factor, APOBEC3F, is suppressed by the HIV-1 and HIV-2 Vif proteins</p>

<p>           Wiegand, HL, Doehle, BP, Bogerd, HP, and Cullen, BR</p>

<p>           EMBO J 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15152192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15152192&amp;dopt=abstract</a> </p><br />

<p>  12.    43469   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Anti-AIDS agents. Part 58: Synthesis and anti-HIV activity of 1-thia-di-O-( [Formula: see text] )-camphanoyl-( [Formula: see text] )-cis-khellactone (1-thia-DCK) analogues</p>

<p>           Xia, P, Yin, ZJ, Chen, Y, Zhang, Q, Zhang, B, Xia, Y, Yang, ZY, Kilgore, N, Wild, C, Morris-Natschke, SL, and Lee, KH</p>

<p>           Bioorg Med Chem Lett 2004. 14(12):  3341-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149703&amp;dopt=abstract</a> </p><br />

<p>  13.    43470   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Human immunodeficiency virus 1 integrase interacting protein Inip76 (hepatoma-derived growth factor), and diagnostic and therapeutic use for viral infection</b>((K.U. Leuven Research and Development, Belg.)</p>

<p>           Debyser, Zeger, Cherepanov, Peter, and De Clercq, Erik</p>

<p>           PATENT: WO 2004029246 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 101 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  14.    43471   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           5-Alkyl-2-[(aryl and alkyloxylcarbonylmethyl)thio]-6-(1-naphthylmethyl) pyrimidin-4(3H)-ones as an unique HIV reverse transcriptase inhibitors of S-DABO series</p>

<p>           He, Y, Chen, F, Sun, G, Wang, Y, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

<p>           Bioorg Med Chem Lett 2004. 14(12):  3173-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149669&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149669&amp;dopt=abstract</a> </p><br />

<p>  15.    43472   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Preparation of monoacylated betulin and dihydrobetulin derivatives for their therapeutic use as anti-HIV agents</b> ((Panacos Pharmaceuticals, Inc. USA, Niigata University of Pharmacy and Applied Science, and The University of North Carolina At Chapel Hill))</p>

<p>           Allaway, Graham P, Wild, Carl T, Kashiwada, Yoshiki, and Lee, Kuo-hsiung</p>

<p>           PATENT: WO 2004028455 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 37 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    43473   HIV-LS-297; PUBMED-HIV-6/02/2004</p>

<p class="memofmt1-2">           Synthesis and biological evaluation of novel beta-carboline derivatives as Tat-TAR interaction inhibitors</p>

<p>           Yu, X, Lin, W, Li, J, and Yang, M</p>

<p>           Bioorg Med Chem Lett 2004. 14(12):  3127-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149658&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15149658&amp;dopt=abstract</a> </p><br />

<p>  17.    43474   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p class="memofmt1-2">           Susceptibility of feline immunodeficiency virus/human immunodeficiency virus type 1 reverse transcriptase chimeras to non-nucleoside RT inhibitors</p>

<p>           Auwerx, Joeri, Esnouf, Robert, De Clercq, Erik, and Balzarini, Jan</p>

<p>           Molecular Pharmacology 2004. 65(1): 244-251</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  18.    43475   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p class="memofmt1-2">           R and D project at the School of Engineering and architecture of Fribourg. Development of the synthesis of a novel anti-AIDS drug: NU 1320</p>

<p>           Poffet, Martine and Bourgeois, Jean-Marc</p>

<p>           Chimia 2004. 58(1-2): 62-64</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  19.    43476   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Methods for using gold(III) complexes as antitumor and anti-HIV agents</b>((The University of Hong Kong, Peop. Rep. China)</p>

<p>           Che, Chiming</p>

<p>           PATENT: WO 2004024146 A1;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 80 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    43477   HIV-LS-297; WOS-HIV-5/23/2004</p>

<p class="memofmt1-2">           HIV-1 Vif versus APOBEC3G: newly appreciated warriors in the ancient battle between virus and host</p>

<p>           Argyris, EG and Pomerantz, RJ</p>

<p>           TRENDS MICROBIOL 2004. 12(4): 145-148</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221051600002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221051600002</a> </p><br />

<p>  21.    43478   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Anti-HIV agent</b> ((Fuso Pharmaceutical Industries, Ltd. Japan)</p>

<p>           Wakamiya, Nobutaka, Ohtani, Katsuki, Sakamoto, Takashi, Keshi, Hiroyuki, and Kishi, Yuichiro</p>

<p>           PATENT: WO 2004002511 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 44 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    43479   HIV-LS-297; WOS-HIV-5/23/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of novel prodrugs of foscarnet and dideoxycytidine with a universal carrier compound comprising a chemiluminescent and a photochromic conjugate</p>

<p>           Mills, R and Wu, GZ</p>

<p>           J PHARM SCI-US 2004. 93(5): 1320-1336</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221039800023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221039800023</a> </p><br />

<p>  23.    43480   HIV-LS-297; WOS-HIV-5/23/2004</p>

<p class="memofmt1-2">           Topological estimation of cytotoxic activity of some anti-HIV agents: HEPT analogues</p>

<p>           Agrawal, VK, Mishra, K, Sharma, R, and Khadikar, PV</p>

<p>           J CHEM SCI 2004. 116(2): 93-99</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221122800005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221122800005</a> </p><br />

<p>  24.    43481   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p class="memofmt1-2">           Profiles of prototype antiviral agents interfering with the initial stages of HIV infection</p>

<p>           De Clercq, Erik</p>

<p>           CONFERENCE: Drug Discovery Strategies and Methods 2004: 309-336</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    43482   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Fusion polypeptides of HIV-Nef mutants and truncated human LNGFR, and retroviral vectors encoding them, for cell transduction and targetable anti-HIV-1 gene therapy</b> ((Molmed SpA, Italy)</p>

<p>           Federico, Maurizio</p>

<p>           PATENT: US 20040087537 A1;  ISSUE DATE: 20040506</p>

<p>           APPLICATION: 2003-62012; PP: 36 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    43483   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           HIV-1 gp41-derived HR1 peptides modified to form stable trimers, and their use in therapy to inhibit transmission of human immunodeficiency virus</b>((Trimeris, Inc. USA)</p>

<p>           Delmedico, Mary K and Dwyer, John</p>

<p>           PATENT: WO 2004029074 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 94 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    43484   HIV-LS-297; SCIFINDER-HIV-5/25/2004</p>

<p><b>           Preparation of 9H-imidazo[1,2-d]dipyrido[2,3-b:3&#39;,2&#39;-f][1,4]diazepine derivatives as tetracyclic non-nucleoside reverse transcriptase inhibitors useful against wild type and double-mutation K103N/Y181C enzymes</b> ((Boehringer Ingelheim (Canada) Ltd., Can.)</p>

<p>           Yoakim, Christiane, O&#39;Meara, Jeffrey, Simoneau, Bruno, Ogilvie, William W, and Deziel, Robert </p>

<p>           PATENT: WO 2004026875 A1;  ISSUE DATE: 20040401</p>

<p>           APPLICATION: 2003; PP: 54 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    43485   HIV-LS-297; WOS-HIV-5/23/2004</p>

<p class="memofmt1-2">           Molecular dynamics simulation on the complex of HIV-1 integrase and the inhibitor aurintricarboxylic acid</p>

<p>           Zhu, HM, Chen, WZ, and Wang, CX</p>

<p>           ACTA CHIM SINICA 2004. 62(8): 745-749</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221044800001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221044800001</a> </p><br />

<p>  29.    43486   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           A genomic selection strategy to identify accessible and dimerization blocking targets in the 5 &#39;-UTR of HIV-1 RNA</p>

<p>           Jakobsen, MR, Damgaard, CK, Andersen, ES, Podhajska, A, and Kjems, J</p>

<p>           NUCLEIC ACIDS RES 2004. 32(7): art. no.-e67</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221145400033">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221145400033</a> </p><br />

<p>  30.    43487   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           Repression of the human immunodeficiency virus type-1 long terminal repeat by the c-Myc oncoprotein</p>

<p>           Stojanova, A, Caro, C, Jarjour, RJV, Oster, SK, Penn, LZ, and Germinario, RJ</p>

<p>           J CELL BIOCHEM 2004. 92(2): 400-413</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221154600016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221154600016</a> </p><br />

<p>  31.    43488   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           HIV co-receptors as targets for antiviral therapy</p>

<p>           Schols, D</p>

<p>           CURR TOP MED CHEM 2004. 4(9): 883-893</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500003</a> </p><br />

<p>  32.    43489   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           TSAO compounds: The comprehensive story of a unique family of HIV-1 specific inhibitors of reverse transcriptase</p>

<p>           Camarasa, MJ, San-Felix, A, Velazquez, S, Perez-Perez, MJ, Gago, F, and Balzarini, J</p>

<p>           CURR TOP MED CHEM 2004. 4(9): 945-963</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500006</a> </p><br />

<p>  33.    43490   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           HIV protease inhibition: Limited recent progress and advances in understanding current pitfalls</p>

<p>           Rodriguez-Barrios, F and Gago, F</p>

<p>           CURR TOP MED CHEM 2004. 4(9): 991-1007</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221176500009</a> </p><br />

<p>  34.    43491   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           2 &#39;,3 &#39;-Didehydro-2 &#39;,3 &#39;-dideoxynucleosides are degraded to furfuryl alcohol under acidic conditions</p>

<p>           Shi, JX, Ray, AS, Mathew, JS, Anderson, KS, Chu, CK, and Schinazi, RF</p>

<p>           BIOORG MED CHEM LETT 2004. 14(9): 2159-2162</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221160200029">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221160200029</a> </p><br />

<p>  35.    43492   HIV-LS-297; WOS-HIV-5/30/2004</p>

<p class="memofmt1-2">           Inhibition of human immunodeficiency virus type 1 Tat-trans-activation-responsive region interaction by an antiviral quinolone derivative</p>

<p>           Richter, S, Parolin, C, Gatto, B, Del, Vecchio C, Brocca-Cofano, E, Fravolini, A, Palu, G, and Palumbo, M</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(5): 1895-1899</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221227900065">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221227900065</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
