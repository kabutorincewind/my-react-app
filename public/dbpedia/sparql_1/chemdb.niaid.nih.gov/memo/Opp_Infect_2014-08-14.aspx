

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-08-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gD8KFBBMOiwDdjOiLdB1jAQt4wx1+DrSrnN0L+m6j8AjzN2FbbS5agvUdBQif4vEEx/5Ctx0KMsvP1gAghsys/EplCUyOP0tPKv8cwg83vY4tzz1dXBW2MaElK8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="643EFF4F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 1 - August 14, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24836638">In Vitro Evaluation of Antimicrobial Activity of Lactobacillus rhamnosus IMC 50, Lactobacillus paracasei IMC 502 and SYNBIO against Pathogens.</a> Coman, M.M., M.C. Verdenelli, C. Cecchini, S. Silvi, C. Orpianesi, N. Boyko, and A. Cresci. Journal of Applied Microbiology, 2014. 117(2): p. 518-527. PMID[24836638].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24930836">Synthesis and Bioactive Evaluation of a Novel Series of Coumarinazoles.</a> Damu, G.L., S.F. Cui, X.M. Peng, Q.M. Wen, G.X. Cai, and C.H. Zhou. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3605-3608. PMID[24930836].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25101960">An Antifungal Role of Hydrogen Sulfide on the Postharvest Pathogens Aspergillus niger and Penicillium italicum.</a> Fu, L.H., K.D. Hu, L.Y. Hu, Y.H. Li, L.B. Hu, H. Yan, Y.S. Liu, and H. Zhang. PloS One, 2014. 9(8): p. e104206. PMID[25101960].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24836571">Self-assembled Cardanol Azo Derivatives as Antifungal Agent with Chitin-binding Ability.</a> Mahata, D., S.M. Mandal, R. Bharti, V.K. Gupta, M. Mandal, A. Nag, and G.B. Nando. International Journal of Biological Macromolecules, 2014. 69: p. 5-11. PMID[24836571].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24707849">Antioxidant and Antifungal Activities of Smilax campestris Griseb. (Smilacaceae).</a> Morais, M.I., M.E. Pinto, S.G. Araujo, A.H. Castro, J.M. Duarte-Almeida, L.H. Rosa, C.A. Rosa, S. Johann, and L.A. Lima. Natural Product Research, 2014. 28(16): p. 1275-1279. PMID[24707849].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24909079">Scaffold-switching: An Exploration of 5,6-Fused Bicyclic Heteroaromatics Systems to Afford Antituberculosis Activity Akin to the Imidazo[1,2-a]pyridine-3-carboxylates.</a> Moraski, G.C., A.G. Oliver, L.D. Markley, S. Cho, S.G. Franzblau, and M.J. Miller. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3493-3498. PMID[24909079].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24951333">One Pot Three Components Microwave Assisted and Conventional Synthesis of New 3-(4-Chloro-2-hydroxyphenyl)-2-(substituted) thiazolidin-4-one as Antimicrobial Agents.</a> Pansare, D.N., N.A. Mulla, C.D. Pawar, V.R. Shende, and D.B. Shinde. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3569-3573. PMID[24951333].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24723295">Inhibition of Candida albicans Virulence Factors by Novel Levofloxacin Derivatives.</a> Raja Mohamed, B.S., M. Subramanian, and K.P. Shunmugiah. Applied Microbiology and Biotechnology, 2014. 98(15): p. 6775-6785. PMID[24723295].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24913712">Synthesis and SAR Study of Novel Anticancer and Antimicrobial Naphthoquinone amide Derivatives.</a> Sreelatha, T., S. Kandhasamy, R. Dinesh, S. Shruthy, S. Shweta, D. Mukesh, D. Karunagaran, R. Balaji, N. Mathivanan, and P.T. Perumal. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3647-3651. PMID[24913712].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24713101">A Rapid Microplate Method for the Proliferation Assay of Fungi and the Antifungal Susceptibility Testing using the Colorimetric Microbial Viability Assay.</a> Tsukatani, T., H. Suenaga, M. Shiga, and K. Matsumoto. Letters in Applied Microbiology, 2014. 59(2): p. 184-192. PMID[24713101].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24974345">Kojic acid Derived Hydroxypyridinone-chloroquine Hybrids: Synthesis, Crystal Structure, Antiplasmodial Activity and beta-Haematin Inhibition.</a> Andayi, W.A., T.J. Egan, and K. Chibale. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3263-3267. PMID[24974345].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24751257">Synthesis, Characterization and Anti-tubercular Activity of Ferrocenyl hydrazones and Their beta-Cyclodextrin Conjugates.</a> Dandawate, P., K. Vemuri, E.M. Khan, M. Sritharan, and S. Padhye. Carbohydrate Polymers, 2014. 108: p. 135-144. PMID[24751257].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24786571">Antibacterial Activities of Plants from Central Africa Used Traditionally by the Bakola Pygmies for Treating Respiratory and Tuberculosis-related Symptoms.</a> Fomogne-Fodjo, M.C., S. Van Vuuren, D.T. Ndinteh, R.W. Krause, and D.K. Olivier. Journal of Ethnopharmacology, 2014. 155(1): p. 123-131. PMID[24786571].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24915878">Pentapeptide boronic acid Inhibitors of Mycobacterium tuberculosis MycP1 protease.</a> Frasinyuk, M.S., S. Kwiatkowski, J.M. Wagner, T.J. Evans, R.W. Reed, K.V. Korotkov, and D.S. Watt. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(15): p. 3546-3548. PMID[24915878].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24953953">Synthesis and Biological Activity of New Salicylanilide N,N-Disubstituted Carbamates and Thiocarbamates.</a> Kratky, M., M. Volkova, E. Novotna, F. Trejtnar, J. Stolarikova, and J. Vinsova. Bioorganic &amp; Medicinal Chemistry, 2014. 22(15): p. 4073-4082. PMID[24953953].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24953948">Identification and Development of 2-Methylimidazo[1,2-a]pyridine-3-carboxamides as Mycobacterium tuberculosis Pantothenate synthetase Inhibitors.</a> Samala, G., R. Nallangi, P.B. Devi, S. Saxena, R. Yadav, J.P. Sridevi, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2014. 22(15): p. 4223-4232. PMID[24953948].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24955560">Antitubercular Activity of Arctium lappa and Tussilago farfara Extracts and Constituents.</a> Zhao, J., D. Evangelopoulos, S. Bhakta, A.I. Gray, and V. Seidel. Journal of Ethnopharmacology, 2014. 155(1): p. 796-800. PMID[24955560].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24919925">Synthesis and Biological Evaluation of Papain-family Cathepsin L-like Cysteine Protease Inhibitors Containing a 1,4-Benzodiazepine Scaffold as Antiprotozoal Agents.</a> Ettari, R., A. Pinto, L. Tamborini, I.C. Angelo, S. Grasso, M. Zappala, N. Capodicasa, L. Yzeiraj, E. Gruber, M.N. Aminake, G. Pradel, T. Schirmeister, C. De Micheli, and P. Conti. ChemMedChem, 2014. 9(8): p. 1817-1825. PMID[24919925].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25002232">Dual-stage Triterpenoids from an African Medicinal Plant Targeting the Malaria Parasite.</a> Ramalhete, C., F.P. da Cruz, S. Mulhovo, I.J. Sousa, M.X. Fernandes, M. Prudencio, and M.J. Ferreira. Bioorganic &amp; Medicinal Chemistry, 2014. 22(15): p. 3887-3890. PMID[25002232].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24817179">Antiplasmodial Activity Study of Angiotensin II via Ala Scan Analogs.</a> Silva, A.F., E.L. Bastos, M.D. Torres, A.L. Costa-da-Silva, R.S. Ioshino, M.L. Capurro, F.L. Alves, A. Miranda, R. de Freitas Fischer Vieira, and V.X. Oliveira, Jr. Journal of Peptide Science, 2014. 20(8): p. 640-648. PMID[24817179].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24830752">6-Trifluoromethyl-2-thiouracil Possesses anti-Toxoplasma gondii Effect in Vitro and in Vivo with Low Hepatotoxicity.</a> Choi, H.J., S.T. Yu, K.I. Lee, J.K. Choi, B.Y. Chang, S.Y. Kim, M.H. Ko, H.O. Song, and H. Park. Experimental Parasitology, 2014. 143: p. 24-29. PMID[24830752].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0801-081414.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338583800001">Evaluation of the anti-Mycobacterium tuberculosis Activity and in Vivo Acute Toxicity of Annona sylvatic.</a> Araujo, R.C.P., F.A.R. Neves, A.S.N. Formagio, C.A.L. Kassuya, M.E.A. Stefanello, V.V. Souza, F.R. Pavan, and J. Croda. BMC Complementary and Alternative Medicine, 2014. 14(209): pp. 10. ISI[000338583800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338478700008">A Novel Nerolidol-rich Essential Oil from Piper claussenianum Modulates Candida albicans Biofilm.</a> Curvelo, J.A.R., A.M. Marques, A.L.S. Barreto, M.T.V. Romanos, M.B. Portela, M.A.C. Kaplan, and R.M.A. Soares. Journal of Medical Microbiology, 2014. 63: p. 697-702. ISI[000338478700008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338809400028">Studies on Molecular Properties Prediction, Antitubercular and Antimicrobial Activities of Novel Quinoline Based Pyrimidine Motifs.</a> Desai, N.C., G.M. Kotadiya, and A.R. Trivedi. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 3126-3130. ISI[000338809400028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338621000005">Comparative Antimicrobial Activity of South East Asian Plants used in Bornean Folkloric Medicine.</a> Galappathie, S., E.A. Palombo, T.C. Yeo, D.L.S. Ley, C.L. Tu, F.M. Malherbe, and P.J. Mahon. Journal of Herbal Medicine, 2014. 4(2): p. 96-105. ISI[000338621000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338853000001">Antimicrobial and Antiproliferative Potential of Anadenanthera colubrina (Vell.) Brenan.</a> Lima, R.D., E.P. Alves, P.L. Rosalen, A. Ruiz, M.C.T. Duarte, V.F.F. Goes, A.C.D. de Medeiros, J.V. Pereira, G.P. Godoy, and E. Costa. Evidence-Based Complementary and Alternative Medicine, 2014. 802696: pp. 7. ISI[000338853000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338605100009">Synthesis, Characterization and Antimicrobial Evaluation of Lanthanide(III) Complexes with Meloxicam.</a> Marinescu, G., D.C. Culita, L. Patron, S. Nita, L. Marutescu, N. Stanica, and O. Oprea. Revista de Chimie, 2014. 65(4): p. 426-430. ISI[000338605100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338347300004">Chemical Composition Profiling and Antifungal Activity of the Essential Oil and Plant Extracts of Mesembryanthemum edule (L.) Bolus Leaves.</a> Omoruyi, B.E., A.J. Afolayan, and G. Bradley. African Journal of Traditional Complementary and Alternative Medicines, 2014. 11(4): p. 19-30. ISI[000338347300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338776900040">Assessment of Mycobacterium tuberculosis Pantothenate Kinase Vulnerability through Target Knockdown and Mechanistically Diverse Inhibitors.</a> Reddy, B.K.K., S. Landge, S. Ravishankar, V. Patil, V. Shinde, S. Tantry, M. Kale, A. Raichurkar, S. Menasinakai, N.V. Mudugal, A. Ambady, A. Ghosh, R. Tunduguru, P. Kaur, R. Singh, N. Kumar, S. Bharath, A. Sundaram, J. Bhat, V.K. Sambandamurthy, C. Bjorkelid, T.A. Jones, K. Das, B. Bandodkar, K. Malolanarasimhan, K. Mukherjee, and V. Ramachandran. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3312-3326. ISI[000338776900040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338269900008">Synthesis of Benzofuro [6,7-d]thiazoles, Benzofuro [7,6-d]thiazoles and 6-Arylaminobenzo[d]thiazole-4,7-diones as Antifungal Agent.</a> Ryu, C.K., J.H. Nho, G. Jin, S.Y. Oh, and S.J. Choi. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(7): p. 668-674. ISI[000338269900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0801-081414.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
