

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-347.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pouGmMiVaOCYqcv4scqw63XdLC7XrYz1L93dqGuiF+bvbQtBGWbs9Mb9dDqEH6BCVZe58mOpKs2bUXDN3UD9Jm1R4hd5SkxF7lE6Cto9COPIjWH4CCeLMOvlWbc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="59FA57DE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-347-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48817   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis dihydrofolate reductase is a target for isoniazid</p>

    <p>          Argyrou, A, Vetting, MW, Aladegbami, B, and Blanchard, JS</p>

    <p>          Nat Struct Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16648861&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16648861&amp;dopt=abstract</a> </p><br />

    <p>2.     48818   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          VX-950, a Novel Hepatitis C Virus (HCV) NS3-4A Protease Inhibitor, Exhibits Potent Antiviral Activities in HCV Replicon Cells</p>

    <p>          Lin, K, Perni, RB, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(5): 1813-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641454&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16641454&amp;dopt=abstract</a> </p><br />

    <p>3.     48819   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          The potential of azole antifungals against latent/persistent tuberculosis</p>

    <p>          Ahmad, Z, Sharma, S, and Khuller, GK</p>

    <p>          FEMS Microbiol Lett <b>2006</b>.  258(2): 200-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640573&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16640573&amp;dopt=abstract</a> </p><br />

    <p>4.     48820   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Oxidized low-density lipoprotein inhibits hepatitis C virus cell entry in human hepatoma cells</p>

    <p>          von, Hahn T, Lindenbach, BD, Boullier, A, Quehenberger, O, Paulson, M, Rice, CM, and McKeating, JA</p>

    <p>          Hepatology <b>2006</b>.  43(5): 932-942</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16628670&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16628670&amp;dopt=abstract</a> </p><br />

    <p>5.     48821   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Drugs for preventing tuberculosis in people at risk of multiple-drug-resistant pulmonary tuberculosis</p>

    <p>          Fraser, A, Paul, M, Attamna, A, and Leibovici, L</p>

    <p>          Cochrane Database Syst Rev <b>2006</b>.(2): CD005435</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16625639&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16625639&amp;dopt=abstract</a> </p><br />

    <p>6.     48822   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis, antibacterial and antifungal activity of some new pyridazinone metal complexes</p>

    <p>          Soenmez, Mehmet, Berber, Ismet, and Akbas, Esvet</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  41(1): 101-105</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     48823   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Significant improvement of antifungal activity of polyene macrolides by bisalkylation of the mycosamine</p>

    <p>          Paquet, V and Carreira, EM</p>

    <p>          Org Lett <b>2006</b>.  8(9): 1807-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16623556&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16623556&amp;dopt=abstract</a> </p><br />

    <p>8.     48824   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Deletion of the Mycobacterium tuberculosis resuscitation-promoting factor Rv1009 gene results in delayed reactivation from chronic tuberculosis</p>

    <p>          Tufariello, JM, Mi, K, Xu, J, Manabe, YC, Kesavan, AK, Drumm, J, Tanaka, K, Jacobs, WR Jr, and Chan, J</p>

    <p>          Infect Immun <b>2006</b>.  74(5): 2985-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16622237&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16622237&amp;dopt=abstract</a> </p><br />

    <p>9.     48825   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Protein Splicing of SufB Is Crucial for the Functionality of the Mycobacterium tuberculosis SUF Machinery</p>

    <p>          Huet, G, Castaing, JP, Fournier, D, Daffe, M, and Saves, I</p>

    <p>          J Bacteriol <b>2006</b>.  188(9): 3412-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621837&amp;dopt=abstract</a> </p><br />

    <p>10.   48826   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Antiviral activity of cyclosaligenyl prodrugs of the nucleoside analogue bromovinyldeoxyuridine against herpes viruses</p>

    <p>          Meerbach, A, Meier, C, Sauerbrei, A, Meckel, HM, and Wutzler, P</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16621459&amp;dopt=abstract</a> </p><br />

    <p>11.   48827   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          In vitro anticandidal activity of xanthorrhizol isolated from Curcuma xanthorrhiza Roxb</p>

    <p>          Rukayadi, Y, Yong, D, and Hwang, JK</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16617064&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16617064&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   48828   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Antimicrobial peptides enhance the candidacidal activity of antifungal drugs by promoting the efflux of ATP from Candida cells</p>

    <p>          Tanida Toyohiro, Okamoto Tetsuro, Ueta Eisaku, Yamamoto Tetsuya, and Osaki Tokio</p>

    <p>          The Journal of antimicrobial chemotherapy <b>2006</b>.  57(1): 94-103.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   48829   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Benzofuro[3,2-f][1]benzopyrans: A new class of antitubercular agents</p>

    <p>          Prado, S, Ledeit, H, Michel, S, Koch, M, Darbord, JC, Cole, ST, Tillequin, F, and Brodin, P</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616504&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16616504&amp;dopt=abstract</a> </p><br />

    <p>14.   48830   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Molecular Virology of Hepatitis C Virus (HCV): 2006 Update</p>

    <p>          Brass, V, Moradpour, D, and Blum, HE</p>

    <p>          Int J Med Sci <b>2006</b>.  3(2): 29-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16614739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16614739&amp;dopt=abstract</a> </p><br />

    <p>15.   48831   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of antimicrobial and anticonvulsant activities of some new 3-[2- (5-aryl-1,3,4-oxadiazol-2-yl/4-carbethoxymethylthiazol-2-yl) imino-4-thiazolidinon-5-ylidene]-5-substituted/nonsubstituted 1H-indole-2-ones and investigation of their structure-activity relationships</p>

    <p>          Altintas Handan, Ates Oznur, Uyde-Dogan B Sonmez, Alp F Ilkay, Kaleli Deniz, Ozdemir Osman, Birteksoz Seher, Otuk Gulten, Atana Dilek, and Uzun Meltem</p>

    <p>          Arzneimittel-Forschung <b>2006</b>.  56(3): 239-48.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   48832   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Recent advances in antiviral agents: antiviral drug discovery for hepatitis viruses</p>

    <p>          Tanikawa, K</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(11): 1371-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16611121&amp;dopt=abstract</a> </p><br />

    <p>17.   48833   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis of New 3-(Substituted Phenacyl)-5-[3&#39;-(4H-4-oxo-1-benzopyran-2-yl)-benzylidene]-2,4-thiazolidinediones and their Antimicrobial Activity</p>

    <p>          Tuncbilek Meral and Altanlar Nurten</p>

    <p>          Archiv der Pharmazie <b>2006</b>.  339(4): 213-6.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   48834   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Antimicrobial principle from Aframomum longifolius</p>

    <p>          Tatsimo Simplice Joel Ndendoung, Tane Pierre, Melissa Jacob, Sondengam Beibam L, Okunji Christopher O, Schuster Brian M, Iwu Maurice M, and Khan Ikhlas A</p>

    <p>          Planta medica <b>2006</b>.  72(2): 132-5.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48835   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Oligonucleotide-based antiviral strategies</p>

    <p>          Schubert, S and Kurreck, J</p>

    <p>          Handb Exp Pharmacol <b>2006</b>.(173): 261-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16594620&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16594620&amp;dopt=abstract</a> </p><br />

    <p>20.   48836   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Tea polyphenol epigallocatechin-3-gallate inhibits ergosterol synthesis by disturbing folic acid metabolism in Candida albicans</p>

    <p>          Navarro-Martinez, MD, Garcia-Canovas, F, and Rodriguez-Lopez, JN</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16585130&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16585130&amp;dopt=abstract</a> </p><br />

    <p>21.   48837   OI-LS-347; PUBMED-OI-5/2/2006</p>

    <p class="memofmt1-2">          Stepwise decrease in moxifloxacin susceptibility amongst clinical isolates of multidrug-resistant Mycobacterium tuberculosis: correlation with ofloxacin susceptibility</p>

    <p>          Kam, KM, Yip, CW, Cheung, TL, Tang, HS, Leung, OC, and Chan, MY</p>

    <p>          Microb Drug Resist <b>2006</b>.  12(1): 7-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16584301&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16584301&amp;dopt=abstract</a> </p><br />

    <p>22.   48838   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Antimicrobials: targeting virulence genes necessary for intracellular multiplication</p>

    <p>          Liautard, JP, Jubier-Maurin, W, Boigegrain, RA, and Kohler, S</p>

    <p>          TRENDS IN MICROBIOLOGY <b>2006</b>.  14(3): 109-113, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236650400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236650400004</a> </p><br />

    <p>23.   48839   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Antimicrobial and antimycobacterial activity of cyclostellettamine alkaloids from sponge Pachychalina sp</p>

    <p>          de Oliveira, Jaine HHL, Seleghim, Mirna HR, Timm, Christoph, Grube, Achim, Kock, Matthias, Nascimento, Gislene GF, Martins, Ana Claudia T, Silva, Elissa GO, de Souza, Ana Olivia, Minarini, Paulo RR, Galetti, Fabio CS, Silva, Celio L, Hajdu, Eduardo, and Berlinck, Roberto GS</p>

    <p>          Marine Drugs <b>2006</b>.  4(1): 1-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>24.   48840   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Tissue predilection sites and effect of dose on Mycobacterium avium subs. paratuberculosis organism recovery in a short-term bovine experimental oral infection model</p>

    <p>          Sweeney, RW, Uzonna, J, Whitlock, RH, Habecker, PL, Chilton, P, and Scott, P</p>

    <p>          RESEARCH IN VETERINARY SCIENCE <b>2006</b>.  80(3): 253-259, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236558500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236558500002</a> </p><br />

    <p>25.   48841   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel 4&#39;-hydroxymethyl branched apiosyl nucleosides</p>

    <p>          Kim Jin Woo and Hong Joon Hee</p>

    <p>          Nucleosides, nucleotides &amp; nucleic acids <b>2006</b>.  25(1): 109-17.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   48842   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Use of [d-meala]3-[etval]4-cyclosporin for the treatment of hepatitis c infection and pharmaceutical composition comprising said [d-meala]3-[etval]4-cyclosporin</p>

    <p>          Scalfaro, Pietro, Dumont, Jean-Maurice, Vuagniaux, Gregoire, and Mauvernay, Rolland-Yves</p>

    <p>          PATENT:  WO <b>2006038088</b>  ISSUE DATE:  20060413</p>

    <p>          APPLICATION: 2005  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Debiopharm SA, Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   48843   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of nucleoside analogs as antiviral agents for treating flaviviruses, pestiviruses and hepacivirus</p>

    <p>          Sommadossi, Jean-Pierre, Gosselin, Gilles, Storer, Richard, and Egan, James</p>

    <p>          PATENT:  WO <b>2006037028</b>  ISSUE DATE:  20060406</p>

    <p>          APPLICATION: 2005  PP: 73 pp.</p>

    <p>          ASSIGNEE:  (Idenix (Cayman) Limited, Cayman I. and Centre National de la Recherche Scientifique)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   48844   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Discovery of SCH446211 (SCH6): A New Ketoamide Inhibitor of the HCV NS3 Serine Protease and HCV Subgenomic RNA Replication</p>

    <p>          Bogen, Stephane L, Arasappan, Ashok, Bennett, Frank, Chen, Kevin, Jao, Edwin, Liu, Yi-Tsung, Lovey, Raymond G, Venkatraman, Srikanth, Pan, Weidong, Parekh, Tajel, Pike, Russel E, Ruan, Sumei, Liu, Rong, Baroudy, Bahige, Agrawal, Sony, Chase, Robert, Ingravallo, Paul, Pichardo, John, Prongay, Andrew, Brisson, Jean-Marc, Hsieh, Tony Y, Cheng, Kuo-Chi, Kemp, Scott J, Levy, Odile E, Lim-Wilby, Marguerita, Tamura, Susan Y, Saksena, Anil K, Girijavallabhan, Viyyoor, and Njoroge, FGeorge</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(9): 2750-2757</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   48845   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of indoleacetamides as antivirals for treatment of hepatitis C infection</p>

    <p>          Colarusso, Stefania, Conte, Immacolata, Habermann, Joerg, Narjes, Frank, and Ponzi, Simona</p>

    <p>          PATENT:  WO <b>2006029912</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.   48846   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of alkyl-substituted 2-deoxy-2-fluoro-D-ribofuranosyl pyrimidine and purine nucleoside analogs via condensation of the lactone to nucleosides as potential antiviral agents</p>

    <p>          Chun, Byoung-Kwon and Wang, Peiyuan</p>

    <p>          PATENT:  WO <b>2006031725</b>  ISSUE DATE:  20060323</p>

    <p>          APPLICATION: 2005  PP: 74 pp.</p>

    <p>          ASSIGNEE:  (Pharmasset, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   48847   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Synthesis and biological properties of 5&#39;-methyl modified 3-deazaaristeromycin analogs</p>

    <p>          Ye, Wei, Liu, Chong, and Schneller, Stewart W</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-373</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   48848   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of naphthalimide derivatives as antiviral agents</p>

    <p>          Carroll, Steve, Hernando, Jose Ignacio Martin, Malancona, Savina, Migliaccio, Giovanni, Narjes, Frank, Olsen, David, Ontoria Ontoria, Jesus Maria, Rowley, Michael, and Summa, Vincenzo</p>

    <p>          PATENT:  WO <b>2006027628</b>  ISSUE DATE:  20060316</p>

    <p>          APPLICATION: 2005  PP: 55 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy and Merck &amp; Co., Inc.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   48849   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Imidazo[4,5-d]pyrimidines as antiviral agents and their methods of preparation, pharmaceutical compositions, and use for treatment of Flaviviridae and Picornaviridae, in particular HCV infections</p>

    <p>          Kim, Choung U, Neyts, Johan, Oare, David A, and Puerstinger, Gerhard</p>

    <p>          PATENT:  US <b>2006052602</b>  ISSUE DATE:  20060309</p>

    <p>          APPLICATION: 2005-59679  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   48850   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of pyrimidine nucleoside derivs. and use as antiviral agent against Hepatitis C Virus</p>

    <p>          Martin, Joseph Armstrong, Sarma, Keshab, and Smith, David Bernard</p>

    <p>          PATENT:  US <b>2006040890</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005-11902  PP: 22 pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   48851   OI-LS-347; SCIFINDER-OI-4/24/2006</p>

    <p class="memofmt1-2">          Preparation of indolecarboxylic acid derivatives as inhibitors of HCV replication</p>

    <p>          Hudyma, Thomas W, Zheng, Xiaofan, He, Feng, Ding, Min, Bergstrom, Carl P, Hewawasam, Piyasena, Martin, Scott W, and Gentles, Robert G</p>

    <p>          PATENT:  WO <b>2006020082</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 353 pp.</p>

    <p>          ASSIGNEE:  (Bristol-Myers Squibb Company, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>36.   48852   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity of 1,2-Bis-[2-(5-R)-1H-benzimidazolyl]-1,2-ethanediols, 1,4-Bis-[2-(5-R)1H-benzimidazolyl]-1,2,3,4-butanetet aols and their Fe-III, Cu-II, and Ag-I complexes</p>

    <p>          Tavman, A, Birteksoz, S, and Otuk, G</p>

    <p>          FOLIA MICROBIOLOGICA <b>2005</b>.  50(6): 467-472, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236636700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236636700001</a> </p><br />

    <p>37.   48853   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          New antimycobacterial S-alkyllsothiosemicarbazones</p>

    <p>          Waisser, K, Heinisch, L, Slosarek, M, and Janota, J</p>

    <p>          FOLIA MICROBIOLOGICA <b>2005</b>.  50(6): 479-481, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236636700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236636700003</a> </p><br />

    <p>38.   48854   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          3-bromo-4-(1H-3-indolyl)-2,5-dihydro-1H-2,5-pyrroledion derivatives as new lead compounds for antibacterially active substances</p>

    <p>          Mahboobi, S, Eichhorn, E, Popp, A, Sellmer, A, Elz, S, and Mollmann, U</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(2): 176-191, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236643200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236643200003</a> </p><br />

    <p>39.   48855   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Synthesis and evaluation of new quinazolone derivatives of nalidixic acid as potential antibacterial and antifungal agents</p>

    <p>          Grover, G and Kini, SG</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(2): 256-262, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236643200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236643200013</a> </p><br />

    <p>40.   48856   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Synthesis, in vitro and in vivo antimycobacterial activities of diclofenac acid hydrazones and amides</p>

    <p>          Sriram, D, Yogeeswari, P, and Devakaram, RV</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2006</b>.  14(9): 3113-3118, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236591200025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236591200025</a> </p><br />

    <p>41.   48857   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          The membrane-active regions of the hepatitis C virus E1 and E2 envelope glycoproteins</p>

    <p>          Perez-Berna, AJ, Moreno, MR, Guillen, J, Bernabeu, A, and Villalain, J</p>

    <p>          BIOCHEMISTRY <b>2006</b>.  45(11): 3755-3768, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236320400032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236320400032</a> </p><br />

    <p>42.   48858   OI-LS-347; WOS-OI-4/23/2006</p>

    <p class="memofmt1-2">          Hydrogen peroxide-mediated isoniazid activation catalyzed by Mycobacterium tuberculosis catalase-peroxidase (KatG) and its S315T mutant</p>

    <p>          Zhao, XB, Yu, H, Yu, SW, Wang, F, Sacchettini, JC, and Magliozzo, RS</p>

    <p>          BIOCHEMISTRY <b>2006</b>.  45(13): 4131-4140, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236521300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236521300010</a> </p><br />
    <br clear="all">

    <p>43.   48859   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Antiparasite and antimycobacterial activity of passifloricin analogues</p>

    <p>          Cardona, W, Quinones, W, Robledo, S, Velez, ID, Murga, J, Garcia-Fortanet, J, Carda, M, Cardona, D, and Echeverri, F</p>

    <p>          TETRAHEDRON <b>2006</b>.  62(17): 4086-4092, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236814800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236814800018</a> </p><br />

    <p>44.   48860   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Structures of S-aureus thymidylate kinase reveal an atypical active site configuration and an intermediate conformational state upon substrate binding</p>

    <p>          Kotaka, M, Dhaliwal, B, Ren, JS, Nichols, CE, Angell, R, Lockyer, M, Hawkins, AR, and Stammers, DK</p>

    <p>          PROTEIN SCIENCE <b>2006</b>.  15(4): 774-784, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236734200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236734200011</a> </p><br />

    <p>45.   48861   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Modulation of Mycobacterium tuberculosis proliferation by MtrA, an essential two-component response regulator</p>

    <p>          Fol, M, Chauhan, A, Nair, NK, Maloney, E, Moomey, M, Jagannath, C, Madiraju, MVVS, and Rajagopalan, M</p>

    <p>          MOLECULAR MICROBIOLOGY <b>2006</b>.  60(3): 643-657, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236767800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236767800010</a> </p><br />

    <p>46.   48862   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Activity of novel non-amphipathic cationic antimicrobial peptides against Candida species</p>

    <p>          Burrows, LL, Stark, M, Chan, C, Glukhov, E, Sinnadurai, S, and Deber, CM</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2006</b>.  57(5): 899-907, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236810300016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236810300016</a> </p><br />

    <p>47.   48863   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          FR209602 and related compounds, novel antifungal lipopeptides from Coleophoma crateriformis no. 738 - II. In vitro and in vivo antifungal activity</p>

    <p>          Kanasaki, R, Abe, F, Furukawa, S, Yoshikawa, K, Fujie, A, Hino, M, Hashimoto, S, and Hori, Y</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2006</b>.  59(3): 145-148, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600002</a> </p><br />

    <p>48.   48864   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          FR220897 and FR220899, novel antifungal upopeptides from Coleophoma empetri no. 14573</p>

    <p>          Kanasaki, R, Abe, F, Kobayashi, M, Katsuoka, M, Hashimoto, M, Takase, S, Tsurumi, Y, Fujie, A, Hino, M, Hashimoto, S, and Hori, Y</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2006</b>.  59(3): 149-157, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600003</a> </p><br />
    <br clear="all">

    <p>49.   48865   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          FR227673 and FR190293, novel antifungal lipopeptides from Chalara sp no. 22210 and Tolypociadium parasiticum no. 16616</p>

    <p>          Kanasaki, R, Kobayashi, M, Fujine, K, Sato, I, Hashimoto, M, Takase, S, Tsurumi, Y, Fujie, A, Hino, M, Hashimoto, S, and Hori, Y</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2006</b>.  59(3): 158-167, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236681600004</a> </p><br />

    <p>50.   48866   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Simultaneous ethambutol &amp; isoniazid resistance in clinical isolates of Mycobacterium tuberculosis</p>

    <p>          Gupta, P, Jadaun, GPS, Das, R, Gupta, UD, Srivastava, K, Chauhan, A, Sharma, VD, Chauhan, DS, and Katoch, VM</p>

    <p>          INDIAN JOURNAL OF MEDICAL RESEARCH <b>2006</b>.  123(2): 125-130, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236805500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236805500007</a> </p><br />

    <p>51.   48867   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Back to the future: Where now for antituberculosis drugs?</p>

    <p>          Villarino, ME and Bliven, EE</p>

    <p>          ENFERMEDADES INFECCIOSAS Y MICROBIOLOGIA CLINICA <b>2006</b>.  24(2): 69-70, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236845300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236845300001</a> </p><br />

    <p>52.   48868   OI-LS-347; WOS-OI-4/30/2006</p>

    <p class="memofmt1-2">          Triaryl pyrazoline compound inhibits flavivirus RNA replication</p>

    <p>          Puig-Basagoiti, F, Tilgner, M, Forshey, BM, Philpott, SM, Espina, NG, Wentworth, DE, Goebel, SJ, Masters, PS, Falgout, B, Ren, P, Ferguson, DM, and Shi, PY</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(4): 1320-1329, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685700029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236685700029</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
