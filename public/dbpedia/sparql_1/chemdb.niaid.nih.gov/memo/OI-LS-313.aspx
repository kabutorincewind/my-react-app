

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-313.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cDOySeZ2QNREUQBjesbdAWpPRywZjFO6KTf2ewiLXdjxZb0pIx1Kx5jlx8TQs2CEHzPNwal7Ix9V92ay+1EpKRt51Ld94kIIij/ETWhmwwlldJ2MqpnxEmZUTp8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2BBA6685" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-313-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44662   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Clinical utility of antifungal pharmacokinetics and pharmacodynamics</p>

    <p>          Andes, D</p>

    <p>          Curr Opin Infect Dis <b>2004</b>.  17(6): 533-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640707&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15640707&amp;dopt=abstract</a> </p><br />

    <p>2.         44663   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Antitubercular activity of triterpenoids from asteraceae flowers</p>

    <p>          Akihisa, T, Franzblau, SG, Ukiya, M, Okuda, H, Zhang, F, Yasukawa, K, Suzuki, T, and Kimura, Y </p>

    <p>          Biol Pharm Bull <b>2005</b>.  28(1): 158-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15635183&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15635183&amp;dopt=abstract</a> </p><br />

    <p>3.         44664   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          IFN-{alpha}{beta} Secreted during Infection Is Necessary but Not Sufficient for Negative Feedback Regulation of IFN-{alpha}{beta} Signaling by Mycobacterium tuberculosis</p>

    <p>          Prabhakar, S, Qiao, Y, Canova, A, Tse, DB, and Pine, R</p>

    <p>          J Immunol <b>2005</b>.  174(2): 1003-1012</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634924&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15634924&amp;dopt=abstract</a> </p><br />

    <p>4.         44665   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Evaluation of Mycograb(R), amphotericin B, caspofungin, and fluconazole in combination against Cryptococcus neoformans by checkerboard and time-kill methodologies</p>

    <p>          Nooney, L, Matthews, RC, and Burnie, JP</p>

    <p>          Diagn Microbiol Infect Dis <b>2005</b>.  51(1): 19-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15629225&amp;dopt=abstract</a> </p><br />

    <p>5.         44666   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          The Crystal Structure of Mycobacterium tuberculosis Thymidylate Kinase in Complex with 3&#39;-Azidodeoxythymidine Monophosphate Suggests a Mechanism for Competitive Inhibition(,)</p>

    <p>          Fioravanti, E, Adam, V, Munier-Lehmann, H, and Bourgeois, D</p>

    <p>          Biochemistry <b>2005</b>.  44(1): 130-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628853&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15628853&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44667   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Phosphatidylinositol 3-Kinase and Frabin Mediate Cryptosporidium parvum Cellular Invasion via Activation of Cdc42</p>

    <p>          Chen, Xian-Ming, Splinter, Patrick L, Tietz, Pamela S, Huang, Bing Q, Billadeau, Daniel D, and LaRusso, Nicholas F</p>

    <p>          Journal of Biological Chemistry <b>2004</b>.  279(30): 31671-31678</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.         44668   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Further Acquisition of Drug-Resistance in Multidrug-Resistant Tuberculosis during Chemotherapy</p>

    <p>          Toyota, E, Sekiguchi, J, Shimizu, H, Fujino, T, Otsuka, Y, Yoshikura, H, Kuratsuji, T, Kirikae, T, and Kudo, K</p>

    <p>          Jpn J Infect Dis <b>2004</b>.  57(6): 292-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15623961&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15623961&amp;dopt=abstract</a> </p><br />

    <p>8.         44669   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Antimycobacterial Compounds from Piper sanctum</p>

    <p>          Mata, R, Morales, I, Perez, O, Rivero-Cruz, I, Acevedo, L, Enriquez-Mendoza, I, Bye, R, Franzblau, S, and Timmermann, B</p>

    <p>          J Nat Prod <b>2004</b>.  67(12): 1961-1968</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15620234&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15620234&amp;dopt=abstract</a> </p><br />

    <p>9.         44670   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          In vitro activity on Cryptosporidium parvum oocyst of different drugs with recognized anticryptosporidial efficacy</p>

    <p>          Castro-Hermida, JA, Pors, I, Ares-Mazas, E, and Chartier, C</p>

    <p>          Revue de Medecine Veterinaire (Toulouse, France) <b>2004</b>.  155(8-9): 453-456</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       44671   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          In Vitro Antibacterial and Antifungal Activities of Extracts and Compounds from Uvaria scheffleri</p>

    <p>          Moshi, Mainen, Joseph, Cosam, Innocent, Esther, and Nkunya, Mayunga</p>

    <p>          Pharmaceutical Biology (Lisse, Netherlands) <b>2004</b>.  42(4-5): 269-273</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       44672   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and potent antimicrobial activity of some novel 4-(5,6-dichloro-1H-benzimidazol-2-yl)-N-substituted benzamides</p>

    <p>          Oezden, Seckin, Karatas, Hacer, Yildiz, Sulhiye, and Goeker, Hakan</p>

    <p>          Archiv der Pharmazie (Weinheim, Germany) <b>2004</b>.  337(10): 556-562</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.       44673   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          The past, present and future of antiviral drug discovery</p>

    <p>          Littler, E</p>

    <p>          IDrugs <b>2004</b>.  7(12): 1104-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15599804&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15599804&amp;dopt=abstract</a> </p><br />

    <p>13.       44674   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and microbiological activity of some substituted N-(2-hydroxy-4-nitrophenyl)benzamides and phenylacetamides as possible metabolites of antimicrobial active benzoxazoles</p>

    <p>          Oeren, Ilkay Yildiz, Aki-Sener, Esin, Ertas, Cengiz, Arpaci, Oezlem Temiz, Yalcin, Ismail, and Altanlar, Nurten</p>

    <p>          Turkish Journal of Chemistry <b>2004</b>.  28(4): 441-449</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       44675   OI-LS-313; PUBMED-OI-1/11/2004</p>

    <p class="memofmt1-2">          Inhibitors of virus replication: recent developments and prospects</p>

    <p>          Magden, J, Kaariainen, L, and Ahola, T</p>

    <p>          Appl Microbiol Biotechnol <b>2004</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592828&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592828&amp;dopt=abstract</a> </p><br />

    <p>15.       44676   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of 1-(1H-1,2,4-triazol-1-yl)-2-(2,4-difluorophenyl)-3-(4-substituted acyl piperazin-1-yl)-2-propanols</p>

    <p>          Liang, Shuang, Liu, Chao-mei, Zhu, Jie, He, Qiu-qin, Jiang, Yuan-ying, and Cao, Yong-bing</p>

    <p>          Journal of Medical Colleges of PLA <b>2004</b>.  19(3): 142-145</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.       44677   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p><b>          Isatin-derived Antibacterial and Antifungal Compounds and their Transition Metal Complexes</b> </p>

    <p>          Chohan, Zahid H, Pervez, Humayun, Rauf, A, Khan, Khalid M, and Supuran, Claudiu T</p>

    <p>          Journal of Enzyme Inhibition and Medicinal Chemistry <b>2004</b>.  19(5): 417-423</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.       44678   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          In vitro anti-candidal activity of the essential oil of Illicium verum</p>

    <p>          Zhao, Junli, Luo, Zhicheng, Wu, Sanmao, Zhou, Xiaoli, Xue, Xiaoyun, Shi, Lei, and Li, Wenzhu</p>

    <p>          Zhonghua Pifuke Zazhi <b>2004</b>.  37(8): 475-477</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.       44679   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of cholesterol-hydrazone derivatives</p>

    <p>          Loncle, Celine, Brunel, Jean Michel, Vidal, Nicolas, Dherbomez, Michel, and Letourneux, Yves</p>

    <p>          European Journal of Medicinal Chemistry <b>2004</b>.  39(12): 1067-1071</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>19.       44680   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Preparation of 3-deoxyanthocyanidine compounds and their use as antimicrobial agents</p>

    <p>          George, Florian and Fellague, Toufik</p>

    <p>          PATENT:  FR <b>2855172</b>  ISSUE DATE: 20041126</p>

    <p>          APPLICATION: 2003-6088  PP: 60 pp.</p>

    <p>          ASSIGNEE:  (Synth&#39;e 163, Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.       44681   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Albuferon (TM) - A novel therapeutic agent for hepatitis C: Results of a phase 1/2 study in treatment experienced subjects with chronic hepatitis C</p>

    <p>          Balan, V, Sulkowski, M, Nelson, D, Everson, G, Lambiase, L, Post, A, Redfield, R, Wiesner, R, Davis, G, Neumann, A, Recta, J, Moore, P, Osborn, B, Novello, L, Freimuth, W, and Subramanian, M</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 280A-281A, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100267">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100267</a> </p><br />

    <p>21.       44682   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Diastereoselective Synthesis and Antifungal Activity of Glycosyl Isoxazolines</p>

    <p>          Mishra, Ram Chandra, Tewari, Neetu, Verma, Shyam Sunder, Tripathi, Rama Pati, Kumar, Manish, and Shukla, Praveen Kumar</p>

    <p>          Journal of Carbohydrate Chemistry <b>2004</b>.  23(6 &amp; 7): 353-374</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.       44683   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Final results of ANRS HC02-RIBAVIC: A randomized controlled trial of pegylated-interferon alfa-2B plus ribavirin vs interferon alfa-2B plus ribavirin for the initial treatment of chronic hepatitis C in HIV co-infected</p>

    <p>          Pol, S, Carrat, F, Bani-Sadr, F, Rosenthal, E, Lunel, F, Morand, P, Salmon, D, Pialoux, G, Patrice, C, and Christian, P</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 315A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100353">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100353</a> </p><br />

    <p>23.       44684   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Dynamics of HCV free-virion and immunecomplex during interferon therapy with and without ribavirin in genotype-1B chronic hepatitis C</p>

    <p>          Fujita, N, Kaito, M, Takeo, M, Sugimoto, R, Horiike, S, Tanaka, H, Konishi, M, Watanabe, S, and Adachi, Y</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 319A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100361">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100361</a> </p><br />

    <p>24.       44685   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Highly active anti-Pneumocystis carinii compounds in a library of novel piperazine-linked bisbenzamidines and related compounds</p>

    <p>          Cushion, Melanie T, Walzer, Peter D, Collins, Margaret S, Rebholz, Sandra, Vanden Eynde, Jean Jacques, Mayence, Annie, and Huang, Tien L</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(11): 4209-4216</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>25.       44686   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Molecular mechanism susceptibility profile of BILN-2061 to various hepatitis C virus genotypes and NS3-NS4A protease inhibitor mutation: D168A and D168V</p>

    <p>          Courcambeck, J, Perbost, R, Chabaud, P, Pepe, G, Bouzidi, M, Mabrouk, K, Sabatier, JM, and Halfon, P</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 385A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100515">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100515</a> </p><br />

    <p>26.       44687   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          In vitro resistance mutations against VX-950 and BILN 2061, two HCV protease inhibitor clinical candidates: Single-resistance, cross-resistance, and fitness</p>

    <p>          Lin, C, Rao, BG, Luong, YP, Fulghum, JR, Brennan, DL, Wei, YY, Frantz, JD, Lippke, J, Hsiao, HM, Ma, S, Lin, K, Maxwell, JP, Cottrell, KM, Gates, CA, Perni, RB, and Kwong, AD </p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 404A-1</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100554">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100554</a> </p><br />

    <p>27.       44688   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          High-throughput screening of anti-HCV drug candidates using an ELISA-based HCV replicon assay</p>

    <p>          Tan, H, Hong, J, Seiwert, S, and Blatt, LM</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 407A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100560">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102100560</a> </p><br />

    <p>28.       44689   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Preparation of 2,4-diaminoquinazoline, 2-amino-4-hydroxypteridine and 2,4-diaminopyrimidine ester derivatives as dihydrofolate reductase inhibitors</p>

    <p>          Boman, Arne, Lundstedt, Torbjorn, Andersson, Per, and Seifert, Elisabeth</p>

    <p>          PATENT:  WO <b>2004020417</b>  ISSUE DATE:  20040311</p>

    <p>          APPLICATION: 2003  PP: 41 pp.</p>

    <p>          ASSIGNEE:  (Melacure Therapeutics AB, Swed. and Pett, Christopher Phineas</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.       44690   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Discovery of Bicyclic Thymidine Analogues as Selective and High-Affinity Inhibitors of Mycobacterium tuberculosis Thymidine Monophosphate Kinase</p>

    <p>          Vanheusden, Veerle, Munier-Lehmann, Helene, Froeyen, Matheus, Busson, Roger, Rozenski, Jef, Herdewijn, Piet, and Van Calenbergh, Serge</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(25): 6187-6194</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.       44691   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Few amino acid positions in are associated with most of the rifampin resistance in Mycobacterium tuberculosis</p>

    <p>          Cummings, Michael P and Segal, Mark R</p>

    <p>          BMC Bioinformatics <b>2004</b>.  5: No pp. given</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.       44692   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Metaferia, Belhu B and Bewley, Carole A</p>

    <p>          &lt;10 Journal Title&gt; <b>2004</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>32.       44693   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis mutants with disabled Rv3351c (NecB) and Rv3874 genes as vaccines, for producing diagnostic antibodies and for screening tuberculosis inhibitors</p>

    <p>          King, CHarold, Jacobs, William, and Okenu, Daniel</p>

    <p>          PATENT:  WO <b>2004067718</b>  ISSUE DATE:  20040812</p>

    <p>          APPLICATION: 2004  PP: 144 pp.</p>

    <p>          ASSIGNEE:  (Emory University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.       44694   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Selection and characterization of hepatitis C virus replicons resistant to potent NS5B polymerase inhibitors</p>

    <p>          Lu, LJ, Mo, HM, Pilot-Matias, T, Dekhtyar, T, Ng, T, Pithawalla, R, Masse, S, Pratt, J, Donner, P, Maring, C, and Molla, A</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 694A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101219">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101219</a> </p><br />

    <p>34.       44695   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          A mutant of Mycobacterium tuberculosis H37Rv that lacks expression of antigen 85A is attenuated in mice but retains vaccinogenic potential</p>

    <p>          Copenhaver, Robert H, Sepulveda, Eliud, Armitige, Lisa Y, Actor, Jeffrey K, Wanger, Audrey, Norris, Steven J, Hunter, Robert L, and Jagannath, Chinnaswamy</p>

    <p>          Infection and Immunity <b>2004</b>.  72(12): 7084-7095</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.       44696   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          1,3,4-Thiadiazole Derivatives. Synthesis, Structure Elucidation, and Structure-Antituberculosis Activity Relationship Investigation</p>

    <p>          Oruc, Elcin E, Rollas, Sevim, Kandemirli, Fatma, Shvets, Nathaly, and Dimoglo, Anatholy S</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(27): 6760-6767</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.       44697   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Results of a phase I single-dose escalation study of the hepatitis C protease inhibitor VX950 in healthy volunteers</p>

    <p>          Chu, HM, McNair, L, and Purdy, S</p>

    <p>          HEPATOLOGY <b>2004</b>.  40(4): 735A-1</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101309">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224102101309</a> </p><br />

    <p>37.       44698   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Preparation of benzimidazolyl ureas and related compounds as gyrase inhibitors for treating bacterial infections</p>

    <p>          Charifson, Paul S, Deininger, David D, Grillot, Anne-Laure, Liao, Yusheng, Ronkin, Steven M, Stamos, Dean, Perola, Emanuele, Wang, Tiansheng, Letiran, Arnaud, and Drumm, Joseph</p>

    <p>          PATENT:  US <b>2004235886</b>  ISSUE DATE:  20041125</p>

    <p>          APPLICATION: 2004-46742  PP: 148 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>38.       44699   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Inhibitors of mycobacterial serine/threonine protein kinases for the treatment of mycobacterial infections</p>

    <p>          Pato, Janos, Keri, Gyorgy, Orfi, Laszlo, Waczek, Frigyes, Horvath, Zoltan, Banhegyi, Peter, Szabadkai, Istavan, Marosfalvi, Jeno, Hegymegi-Barakonyi, Balint, Szekelyhidi, Zsolt, Greff, Zoltan, Choidas, Axel, Bacher, Gerald, Missio, Andrea, and Koul, Anil </p>

    <p>          PATENT:  US <b>2004171603</b>  ISSUE DATE:  20040902</p>

    <p>          APPLICATION: 2003-60231  PP: 51 pp., Cont.-in-part of Appl. No. PCT/EP03-03697.</p>

    <p>          ASSIGNEE:  (Hung.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.       44700   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Toxicogenomics in drug discovery: from preclinical studies to clinical trials</p>

    <p>          Yang, Y, Blomme, EAG, and Waring, JF</p>

    <p>          CHEMICO-BIOLOGICAL INTERACTIONS <b>2004</b>.  150(1): 71-85, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225349900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225349900007</a> </p><br />

    <p>40.       44701   OI-LS-313; WOS-OI-1/2/2005</p>

    <p class="memofmt1-2">          Interaction of shikimic acid with shikimate kinase</p>

    <p>          Pereira, JH, de Oliveira, JS, Canduri, F, Dias, MVB, Palma, MS, Basso, LA, de Azevedo, WF, and Santos, DS</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2004</b>.  325(1): 10-17, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225173400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225173400003</a> </p><br />

    <p>41.       44702   OI-LS-313; WOS-OI-1/2/2005 </p>

    <p class="memofmt1-2">          Cell biology of Mycobacterium tuberculosis phagosome</p>

    <p>          Vergne, I, Chua, J, Singh, SB, and Deretic, V</p>

    <p>          ANNUAL REVIEW OF CELL AND DEVELOPMENTAL BIOLOGY <b>2004</b>.  20: 367-394, 28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225318200014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225318200014</a> </p><br />

    <p>42.       44703   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Statistical limitations to the Cornell model of latent tuberculosis infection for the study of relapse rates</p>

    <p>          Lenaerts, AJ, Chapman, PL, and Orme, IM</p>

    <p>          TUBERCULOSIS <b>2004</b>.  84(6): 361-364, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225476500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225476500004</a> </p><br />

    <p>43.       44704   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          A subinhibitory concentration of clarithromycin inhibits Mycobacterium avium biofilm formation</p>

    <p>          Carter, George, Young, Lowell S, and Bermudez, Luiz E</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(12): 4907-4910</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>44.       44705   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Heterocyclic compounds, specifically 3,6-disubstituted 3H-furo[2,3-d]pyrimidin-2-ones and 2,6-disubstituted furo[2,3-d]pyrimidines, for use as novel nucleoside analogs and antivirals in the treatment of viral infections, particularly cytomegalovirus</p>

    <p>          McGuigan, Christopher, Balzarini, Jan, and De Clercq, Erik</p>

    <p>          PATENT:  WO <b>2004096813</b>  ISSUE DATE:  20041111</p>

    <p>          APPLICATION: 2004  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (University College Cardiff Consultants Limited, UK and Rega Foundation)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.       44706   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Human cytomegalovirus encodes a highly specific RANTES decoy receptor</p>

    <p>          Wang, D, Bresnahan, W, and Shenk, T</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2004</b>.  101(47): 16642-16647, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225347400048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225347400048</a> </p><br />

    <p>46.       44707   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activities of 1&#39;-carbon-substituted 4&#39;-thiothymidines</p>

    <p>          Haraguchi, Kazuhiro, Takahashi, Haruhiko, Tanaka, Hiromichi, Hayakawa, Hiroyuki, Ashida, Noriyuki, Nitanda, Takao, and Baba, Masanori</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  12(20): 5309-5316</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.       44708   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Screening of antibacterial and antifungal activities of ten medicinal plants from Ghana</p>

    <p>          Hoffman, BR, DelasAlas, H, Blanco, K, Wiederhold, N, Lewis, RE, and Williams, L</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2004</b>.  42(1): 13-17, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225492600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225492600003</a> </p><br />

    <p>48.       44709   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          Suppression of hepatitis C virus replicon by TGF-b</p>

    <p>          Murata, Takayuki, Ohshima, Takayuki, Yamaji, Masashi, Hosaka, Masahiro, Miyanari, Yusuke, Hijikata, Makoto, and Shimotohno, Kunitada</p>

    <p>          Virology <b>2005</b>.  331(2): 407-417</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>49.       44710   OI-LS-313; SCIFINDER-OI-1/5/2005</p>

    <p class="memofmt1-2">          A strategy for obtaining near full-length HCV cDNA clones (assemblicons) by assembly PCR</p>

    <p>          Sheehy, P, Scallan, M, Kenny-Walsh, E, Shanahan, F, and Fanning, LJ</p>

    <p>          Journal of Virological Methods <b>2005</b>.  123(2): 115-124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>50.       44711   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Discovery of bicyclic thymidine analogues as selective and high-affinity inhibitors of Mycobacterium tuberculosis thymidine monophosphate kinase</p>

    <p>          Vanheusden, V, Munier-Lehmann, H, Froeyen, M, Busson, R, Rozenski, J, Herdewijn, P, and Van, Calenbergh S</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  47(25): 6187-6194, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225409400010</a> </p><br />

    <p>51.       44712   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Three-dimensional quantitative structure-activity and structure-selectivity relationships of dihydrofolate reductase inhibitors</p>

    <p>          Sutherland, JJ and Weaver, DF</p>

    <p>          JOURNAL OF COMPUTER-AIDED MOLECULAR DESIGN <b>2004</b>.  18(5): 309-331, 23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225572900002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225572900002</a> </p><br />
    <br clear="all">

    <p>52.       44713   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Improving the decision-making process in the structural modification of drug candidates: enhancing metabolic stability</p>

    <p>          Nassar, AEF, Kamel, AM, and Clarimont, C</p>

    <p>          DRUG DISCOVERY TODAY <b>2004</b>.  9(23): 1020-1028, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225329500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225329500007</a> </p><br />

    <p>53.       44714   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Synthesis of trehalose-based compounds and their inhibitory activities against Mycobacterium smegmatis</p>

    <p>          Wang, J, Elchert, B, Hui, Y, Takemoto, JY, Bensaci, M, Wennergren, J, Chang, H, Rai, R, and Chang, CWT</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(24): 6397-6413, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200007</a> </p><br />

    <p>54.       44715   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Crystallographic and NMR studies of antiinfective tricyclic guanidine alkaloids from the sponge Monanchora unguifera</p>

    <p>          Hua, HM, Peng, JN, Fronczek, FR, Kelly, M, and Hamann, MT</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(24): 6461-6464, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200012</a> </p><br />

    <p>55.       44716   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Ring-substituted quinolines. Part 2: Synthesis and antimycobacterial activities of ring-substituted quinolinecarbohydrazide and ring-substituted quinolinecarboxamide analogues</p>

    <p>          Monga, V, Nayyar, A, Vaitilingam, B, Palde, PB, Jhamb, SS, Kaur, S, Singh, PP, and Jain, R</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(24): 6465-6472, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225523200013</a> </p><br />

    <p>56.       44717   OI-LS-313; WOS-OI-1/9/2005 </p>

    <p class="memofmt1-2">          Synthesis and study of antibacterial and antifungal activities of novel 2-[[(benzoxazole/benzimidazole-2-yl)sulfanyl] acetylamino]thiazoles</p>

    <p>          Kaplancikli, ZA, Turan-Zitouni, G, Revial, G, and Guven, K</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2004</b>.  27(11): 1081-1085, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225443900001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225443900001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
