

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-379.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dFj3z2iZrCP6QpLQgEU7kfeK13lLKnoU3TIr91gJI2Vzly4Jk3scmBdPGgUVCZdZylwzjCj5U3idcCqNkBPjO1tv+Ab6+6v/uuxvZPQFlZfG8038SUZID9kbdnk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DACF3E2A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-379-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61356   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Compositions and methods using carbon monoxide dehydrogenase inhibitors for the prevention, treatment and detection of tuberculosis and other diseases</p>

    <p>          Leishman, Kathryn</p>

    <p>          PATENT:  US <b>2007148689</b>  ISSUE DATE:  20070628</p>

    <p>          APPLICATION: 2007-48436  PP: 21pp., Cont.-in-part of U.S. Ser. No. 265,190.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61357   OI-LS-379; WOS-OI-7/13/2007</p>

    <p class="memofmt1-2">          Novel Simple Efficient Synthetic Approach Toward 6-Substituted-2h-[1,2,4]Triazolo[3,4-B][1,3,4]Thiadiazole-3-Thiones and First Synthesis and Biological Evaluation of N- and S-Beta-D-Glucosides of the [1,2,4]Triazolo[3,4-B][1,3,4]Thiadiazole Ring System</p>

    <p>          Khalil, N.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2007</b>.  26(4): 347-359</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246536100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246536100003</a> </p><br />

    <p>3.     61358   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis of novel substituted pyrazolyl-2-toluidinomethanethione and pyrazolyl-2-methoxyanilinomethanethione as potential antitubercular agents</p>

    <p>          Ali, Mohamed Ashraf and Yar, Mohammad Shahar</p>

    <p>          Acta Pol. Pharm. <b>2007</b>.  64(2): 139-146</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61359   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of substituted novel pyrazoline derivatives</p>

    <p>          Ali Mohamed Ashraf, Yar Mohammad Shahar, Kumar Mahesh, and Pandian Ganesan Suresh</p>

    <p>          Nat Prod Res <b>2007</b>.  21(7): 575-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61360   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Antimycobacterial activity of novel 1-(5-cyclobutyl-1,3-oxazol-2-yl)-3-(sub)phenyl/pyridylthiourea compounds endowed with high activity toward multidrug-resistant Mycobacterium tuberculosis</p>

    <p>          Sriram Dharmarajan, Yogeeswari Perumal, Dinakaran Murugesan, and Thirumurugan Rathinasababathy </p>

    <p>          J Antimicrob Chemother <b>2007</b>.  59(6): 1194-6.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61361   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Novel Boron-Containing, Nonclassical Antifolates: Synthesis and Preliminary Biological and Structural Evaluation</p>

    <p>          Reynolds, Robert C, Campbell, Shiela R, Fairchild, Ralph G, Kisliuk, Roy L, Micca, Peggy L, Queener, Sherry F, Riordan, James M, Sedwick, WDavid, Waud, William R, Leung, Adelaine KW, Dixon, Richard W, Suling, William J, and Borhani, David W</p>

    <p>          J. Med. Chem. <b>2007</b>.  50(14): 3283-3289</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61362   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of triazole derivatives with side chain containing tert-butyl and 4-substituted-piperazin-1-yl</p>

    <p>          He, Qiu-qin, Liu, Chao-mei, Li, Ke, Cao, Yong-bing, Zhao, Li-hua, and Dong, Huan-wen</p>

    <p>          Dier Junyi Daxue Xuebao <b>2007</b>.  28(2): 179-182</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     61363   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Bioassay-guided isolation of antifungal alkaloids from Melochia odorata</p>

    <p>          Emile, Aurelie, Waikedre, Jean, Herrenknecht, Christine, Fourneau, Christophe, Gantier, Jean-Charles, Hnawia, Edouard, Cabalion, Pierre, Hocquemiller, Reynald, and Fournet, Alain</p>

    <p>          Phytother. Res. <b>2007</b>.  21(4): 398-400</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61364   OI-LS-379; WOS-OI-7/13/2007</p>

    <p class="memofmt1-2">          Pharmacokinetics and Metabolism of the Prodrug Db289 (2,5-Bis[4-(N-Methoxyamidino)Phenyl]Furan Monomaleate) in Rat and Monkey and Its Conversion to the Antiprotozoal/Antifungal Drug Db75 (2,5-Bis(4-Guanylphenyl)Furan Dihydrochloride)</p>

    <p>          Midgley, I. <i>et al.</i></p>

    <p>          Drug Metabolism and Disposition <b>2007</b>.  35(6): 955-967</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246596700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246596700016</a> </p><br />

    <p>10.   61365   OI-LS-379; SCIFINDER-OI-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral 2-carboxy-thiophene compounds</p>

    <p>          Corfield, John Andrew, Grimes, Richard Martin, Harrison, David, Hartley, Charles David, Howes, Peter David, Le, Joelle, Meeson, Malcolm Lees, Mordaunt, Jacqueline Elizabeth, Shah, Pritom, Slater, Martin John, and White, Gemma Victoria</p>

    <p>          PATENT:  WO <b>2007071434</b>  ISSUE DATE:  20070628</p>

    <p>          APPLICATION: 2006  PP: 155pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61366   OI-LS-379; WOS-OI-7/20/2007</p>

    <p class="memofmt1-2">          Qsar Study of Antimicrobial Activity of Some 3-Nitrocoumarins and Related Compounds</p>

    <p>          Debeljak, Z. <i>et al.</i></p>

    <p>          Journal of Chemical Information and Modeling <b>2007</b>.  47(3): 918-926</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246776400025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246776400025</a> </p><br />

    <p>12.   61367   OI-LS-379; WOS-OI-7/20/2007</p>

    <p class="memofmt1-2">          Investigation of Beta-Amino Acids as Building Blocks in Hepatitis C Virus Ns3 Protease Inhibitors</p>

    <p>          Nurbo, J. <i>et al.</i></p>

    <p>          Biopolymers <b>2007</b>.  88(4): 580</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900269">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247425900269</a> </p><br />

    <p>13.   61368   OI-LS-379; WOS-OI-7/20/2007</p>

    <p class="memofmt1-2">          Amper: a Database and an Automated Discovery Tool for Antimicrobial Peptides</p>

    <p>          Fjell, C., Hancock, R., and Cherkasov, A.</p>

    <p>          Bioinformatics  <b>2007</b>.  23(9): 1148-1155</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246773300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246773300014</a> </p><br />
    <br clear="all">

    <p>14.   61369   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Barriers to reaching the targets for tuberculosis control: multidrug-resistant tuberculosis</p>

    <p>          Blondal, K</p>

    <p>          Bull World Health Organ <b>2007</b>.  85(5): 387-390</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639225&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639225&amp;dopt=abstract</a> </p><br />

    <p>15.   61370   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Reaching the targets for tuberculosis control: the impact of HIV</p>

    <p>          Laserson, K and Wells, C</p>

    <p>          Bull World Health Organ <b>2007</b>.  85(5): 377-381</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639223&amp;dopt=abstract</a> </p><br />

    <p>16.   61371   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Did we reach the 2005 targets for tuberculosis control?</p>

    <p>          Dye, C, Hosseini, M, and Watt, C</p>

    <p>          Bull World Health Organ <b>2007</b>.  85(5): 364-369</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17639221&amp;dopt=abstract</a> </p><br />

    <p>17.   61372   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          A New Dihydroagarofuranoid Sesquiterpene from Microtropis fokienensis with Antituberculosis Activity</p>

    <p>          Chou, TH, Chen, IS, Sung, PJ, Peng, CF, Shieh, PC, and Chen, JJ</p>

    <p>          Chem Biodivers  <b>2007</b>.  4(7): 1594-1600</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17638341&amp;dopt=abstract</a> </p><br />

    <p>18.   61373   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          New Insight into the Mechanism of Action of and Resistance to Isoniazid: Interaction of Mycobacterium tuberculosis enoyl-ACP Reductase with INH-NADP</p>

    <p>          Argyrou, A, Vetting, MW, and Blanchard, JS</p>

    <p>          J Am Chem Soc <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636923&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636923&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>19.   61374   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Fragment-Based Substrate Activity Screening Method for the Identification of Potent Inhibitors of the Mycobacterium tuberculosis Phosphatase PtpB</p>

    <p>          Soellner, MB, Rawls, KA, Grundner, C, Alber, T, and Ellman, JA</p>

    <p>          J Am Chem Soc <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17636914&amp;dopt=abstract</a> </p><br />

    <p>20.   61375   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Rv1106c from Mycobacterium tuberculosis Is a 3beta-Hydroxysteroid Dehydrogenase</p>

    <p>          Yang, X, Dubnau, E, Smith, I, and Sampson, NS</p>

    <p>          Biochemistry <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17630785&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17630785&amp;dopt=abstract</a> </p><br />

    <p>21.   61376   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Phase 2 study of the combination of merimepodib with peginterferon-alpha2b, and ribavirin in nonresponders to previous therapy for chronic hepatitis C</p>

    <p>          Marcellin, P, Horsmans, Y, Nevens, F, Grange, JD, Bronowicki, JP, Vetter, D, Purdy, S, Garg, V, Bengtsson, L, McNair, L, and Alam, J</p>

    <p>          J Hepatol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629590&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629590&amp;dopt=abstract</a> </p><br />

    <p>22.   61377   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Scriptaid and suberoylanilide hydroxamic acid are histone deacetylase inhibitors with potent anti-Toxoplasma gondii activity in vitro</p>

    <p>          Strobl, JS, Cassell, M, Mitchell, SM, Reilly, CM, and Lindsay, DS</p>

    <p>          J Parasitol <b>2007</b>.  93(3): 694-700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626366&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626366&amp;dopt=abstract</a> </p><br />

    <p>23.   61378   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Encapsulation of moxifloxacin within poly(butyl cyanoacrylate) nanoparticles enhances efficacy against intracellular Mycobacterium tuberculosis</p>

    <p>          Kisich, KO, Gelperina, S, Higgins, MP, Shipulo, E, Oganesyan, E, and Heifets, L</p>

    <p>          Int J Pharm <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624699&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624699&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>24.   61379   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          Lariatins, Novel Anti-mycobacterial Peptides with a Lasso Structure, Produced by Rhodococcus jostii K01-B0171</p>

    <p>          Iwatsuki, M, Uchida, R, Takakusagi, Y, Matsumoto, A, Jiang, CL, Takahashi, Y, Arai, M, Kobayashi, S, Matsumoto, M, Inokoshi, J, Tomoda, H, and Omura, S</p>

    <p>          J Antibiot (Tokyo) <b>2007</b>.  60(6): 357-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617692&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617692&amp;dopt=abstract</a> </p><br />

    <p>25.   61380   OI-LS-379; PUBMED-OI-7/23/2007</p>

    <p class="memofmt1-2">          WHO launches plan to fight drug resistant tuberculosis</p>

    <p>          Moszynski, P</p>

    <p>          BMJ <b>2007</b>.  334(7608): 1340-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17599995&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17599995&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
