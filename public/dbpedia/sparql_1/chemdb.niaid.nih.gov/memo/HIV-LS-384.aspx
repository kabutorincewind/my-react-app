

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-384.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1y6OMV8Uj/2sA6HWgfUkEcRQKg19KPhfJ0tqd+h+fPEzjr/mN9MQOFs/OsNWPXqVXKbC4HZEWQMHROgPYfVdmmPbvwLMSlkTd1FpOR1vqLDhfUbXuMUxxv0t6Gc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2603EADC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p> </p>

    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-OLS-384-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p class="memofmt1-1"> </p>

    <p class="memofmt1-1"> </p>

    <p>1.    61729   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Antiviral peptide and antiviral agent</p>

    <p>          Yoshida, Tetsuhiko, Kobayashi, Nahoko, and Sato, Takanori</p>

    <p>          PATENT:  WO <b>2007099993</b>  ISSUE DATE:  20070907</p>

    <p>          APPLICATION: 2007  PP: 34pp.</p>

    <p>          ASSIGNEE:  (Toagosei Co., Ltd. Japan)</p>

    <p> </p>

    <p>2.    61730   HIV-LS-384; PUBMED-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of new dimeric inhibitors against HIV-1</p>

    <p>          Danel, K, Larsen, LM, Pedersen, EB, Sanna, G, La Colla, P, and Loddo, R</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p> <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17904371&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17904371&amp;dopt=abstract</a> </p><br /> 

    <p>3.     61733   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Preparation of bis(imidazolylmethyl)amine compounds as chemokine receptor CXCR4 antagonists for the treatment of HIV infection, rheumatism, and cancer metastasis</p>

    <p>          Yamazaki, Toru, Kikumoto, Shigeyuki, Ono, Masahiro, Saitou, Atsushi, Takahashi, Haruka, Kumakura, Sei, Hirose, Kunitaka, Yanaka, Mikiro, Takemura, Yoshiyuki, Suzuki, Shigeru, and Matsui, Ryo</p>

    <p>          PATENT:  US <b>2007208033</b>  ISSUE DATE:  20070906</p>

    <p>          APPLICATION: 2007-49500  PP: 154pp., Cont.-in-part of U.S. Ser. No. 516,158.                       </p>

    <p>        ASSIGNEE:  (Japan)</p>

    <p>         </p>

    <p>4.     61735   HIV-LS-384; WOS-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Griffithsin, a potent HIV entry inhibitor, is an excellent candidate for anti-HIV microbicide</p>

    <p>          Emau, P, Tian, B, O&#39;Keefa, BR, Mori, T, McMahon, JB, Palmer, KE, Jiang, Y, Bekele, G, and Tsai, CC</p>

    <p>          JOURNAL OF MEDICAL PRIMATOLOGY: J. Med. Primatol <b>2007</b>.  36(4-5): 244-253, 10</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248482700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248482700009</a></span></p>

    <p>5.     61741   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Indolyl Aryl Sulfones as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: Role of Two Halogen Atoms at the Indole Ring in Developing New Analogues with Improved Antiviral Activity</p>

    <p>          La Regina, Giuseppe, Coluccia, Antonio, Piscitelli, Francesco, Bergamini, Alberto, Sinistro, Anna, Cavazza, Antonella, Maga, Giovanni, Samuele, Alberta, Zanoli, Samantha, Novellino, Ettore, Artico, Marino, and Silvestri, Romano</p>

    <p>          J. Med. Chem. <b>2007</b>.  50(20): 5034-5038 </p>

    <p>         </p>

    <p>6.     61746   HIV-LS-384; PUBMED-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Potent and selective inhibition of Tat-dependent HIV-1 replication in chronically infected cells by a novel naphthalene derivative JTK-101</p>

    <p>          Wang, X, Yamataka, K, Okamoto, M, Ikeda, S, and Baba, M</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(4): 201-11              </p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17907378&amp;dopt=abstract</span></p>

    <p>7.     61747   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          CCR5 antagonists useful for treating HIV</p>

    <p>          Ramanathan, Ragulan, Miller, Michael W, Chowdhury, Swapan K, Alton, Kevin B, Grotz, Diane, Rindgen, Diane, and Tendolkar, Amol</p>

    <p>          PATENT:  US <b>2007203149</b>  ISSUE DATE:  20070830</p>

    <p>          APPLICATION: 2007-22529  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA)           </p>

    <p>        </p>

    <p>8.     61750   HIV-LS-384; WOS-HIV-10/10/2007</p>

    <p class="memofmt1-2">          In vitro antiviral activity of the novel, tyrosyl-based human immunodeficiency virus (HIV) type 1 protease inhibitor brecanavir (GW640385) in combination with other antiretrovirals and against a panel of protease inhibitor-resistant HIV</p>

    <p>          Hazen, R, Harvey, R, Ferris, R, Craig, C, Yates, P, Griffin, P, Miller, J, Kaldor, I, Ray, J, Samano, V, Furfine, E, Spaltenstein, A, Michael, HT, Tung, R, Clair, MS, Hanlon, M, and Boone, L</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY: Antimicrob. Agents Chemother <b>2007</b>.  51( 9): 3147-3154, 8</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249175400015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249175400015</a></span></p>

    <p>9.     61754   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Antiviral agents containing polysaccharide sulfates</p>

    <p>          Tanamoto, Kenichi, Ushijima, Koji, Miyamoto, Keiichi, and Sato, Ikuo</p>

    <p>          PATENT:  JP <b>2007210908</b>  ISSUE DATE:  20070823</p>

    <p>          APPLICATION: 2006-30248  PP: 8pp.</p>

    <p>          ASSIGNEE:  (National Pharmaceutical and Dietary Hygiene Research Institute, Japan and Chisso Corp.)              </p>

    <p>         </p>

    <p>10.   61755   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Preparation of 3,7-diazabicyclo[3.3.0]octane compounds as antiviral agents</p>

    <p>          Lemoine, Remy, Melville, Chris Richard, Padilla, Fernando, Rotstein, David Mark, and Wanner, Jutta</p>

    <p>          PATENT:  WO <b>2007093515</b>  ISSUE DATE:  20070823</p>

    <p>          APPLICATION: 2007  PP: 80pp.</p>

    <p>          ASSIGNEE:  (F. Hoffmann La-Roche A.-G., Switz.)             </p>

    <p>         </p>

    <p>11.   61758   HIV-LS-384; WOS-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Neurokinin-1 receptor antagonist (aprepitant) inhibits drug-resistant HIV-1 infection of macrophages in vitro</p>

    <p>          Wang, X, Douglas, SD, Lai, JP, Tuluc, F, Tebas, P, and Ho, WZ</p>

    <p>          JOURNAL OF NEUROIMMUNE PHARMACOLOGY: J. Neuroimmune Pharm <b>2007</b>.  2(1): 42-48, 7</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248869500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248869500008</a></span></p>

    <p>12.   61759   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          pyrrolo[3,4-c]pyrrole derivatives as heterocyclic antiviral compounds and CCR5 receptor antagonists and their preparation, pharmaceutical compositions and use in the treatment of HIV infection, AIDS and ARC</p>

    <p>          Lemoine, Rcmy, Melville, Chris Richard, Padilla, Fernando, Rotstein, David Mark, and Wanner, Jutta</p>

    <p>          PATENT:  US <b>2007191406</b>  ISSUE DATE:  20070816</p>

    <p>          APPLICATION: 2007-51368  PP: 45pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA)             </p>

    <p> </p>

    <p>13.   61760   HIV-LS-384; WOS-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Pyridin-2(1H)-ones: A promising class of HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Meclina-Franco, JL, Martfnez-Mayorga, K, Jurez-Gordiano, C, and Castillo, R</p>

    <p>          CHEMMEDCHEM: ChemMedChem <b>2007</b>.  2(8): 1141-1147, 7</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248851900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248851900003</a></span></p>

    <p>14.   61763   HIV-LS-384; WOS-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Synthesis, anti-HIV activity, and resistance profile of thymidine phosphonomethoxy nucleosides and their bis-isopropyloxymethylcarbonyl (bisPOC) prodrugs</p>

    <p>          Mackman, RL, Zhang, LJ, Prasad, V, Boojamra, CG, Douglas, J, Grant, D, Hui, H, Kim, CU, Laflamme, G, Parrish, J, Stoycheva, AD, Swaminathan, S, Wang, KY, and Cihlar, T</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY: Bioorg. Med. Chem <b>2007</b>.  15(16): 5519-5528, 10</p>

    <p>          <span class="memofmt1-3"><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248172700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248172700016</a></span></p>

    <p>15.   61766   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Activity against human immunodeficiency virus type 1, intracellular metabolism, and effects on human DNA polymerases of 4&#39;-ethynyl-2-fluoro-2&#39;-deoxyadenosine</p>

    <p>          Nakata, Hirotomo, Amano, Masayuki, Koh, Yasuhiro, Kodama, Eiichi, Yang, Guangwei, Bailey, Christopher M, Kohgo, Satoru, Hayakawa, Hiroyuki, Matsuoka, Masao, Anderson, Karen S, Cheng, Yung-Chi, and Mitsuya, Hiroaki</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(8): 2701-2708   </p>

    <p>        </p>

    <p>16.   61770   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Preparation of imidazo[1,2-a]pyridine compds. useful in the treatment of HIV infection and other chemokine receptor-mediated diseases</p>

    <p>          Gudmundsson, Kristjan, Boggs, Sharon Davis, Miller, John Franklin, and Svolto, Angilique Christina</p>

    <p>          PATENT:  WO <b>2007087549</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007  PP: 148pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA)</p>

    <p>         </p>

    <p>17.   61771   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Imidazo[1,2-a]pyridine-3-carboxamides as anti-HIV agents and their preparation, pharmaceutical compositions and their use in monotherapy and in combination therapy of diseases</p>

    <p>          Gudmundsson, Kristjan and Turner, Elizabeth Madalena</p>

    <p>          PATENT:  WO <b>2007087548</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007  PP: 104pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA and Svolto, Angilique Christina)</p>

    <p>         </p>

    <p>18.   61772   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          HIV fusion inhibitor peptides derived from HIV gp41 with improved pharmacological properties, solubility and stability, and anti-AIDS uses</p>

    <p>          Dwyer, John J, Bray, Brian L, Schneider, Stephen E, Zhang, Huyi, Tvermoes, Nicolai A, Johnston, Barbara E, and Friedrich, Paul E</p>

    <p>          PATENT:  US <b>2007179278</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007-46550  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Trimeris, Inc. USA)</p>

    <p> </p>

    <p>19.   61773   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Synthesis of 8-halogenated adenosine compounds as antiviral agents</p>

    <p>          Chang, Junbiao, Cheng, Senxiang, Wang, Limin, Hao, Jia, Wang, Qiang, and Guo, Xiaohe</p>

    <p>          PATENT:  CN <b>101003550</b>  ISSUE DATE: 20070725</p>

    <p>          APPLICATION: 1016-30  PP: 14pp.</p>

    <p>          ASSIGNEE:  (Henan Center of Analysis and Test, Peop. Rep. China)</p>

    <p>         </p>

    <p>20.   61778   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Novel CXCR4 antagonist and use thereof</p>

    <p>          Fujii, Nobutaka, Hamachi, Itaru, Ojida, Akio, Tamamura, Hirokazu, and Nakashima, Hideki</p>

    <p>          PATENT:  WO <b>2007074871</b>  ISSUE DATE:  20070705</p>

    <p>          APPLICATION: 2006  PP: 26pp.</p>

    <p>          ASSIGNEE:  (Kyoto University, Japan)</p>

    <p>         </p>

    <p>21.   61782   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Diketo acids with nucleobase scaffolds: anti-hiv replication inhibitors targeted at hiv integrase in combination therapy</p>

    <p>          Nair, Vasu, Chi, Guochen, and Uchil, Vinod R</p>

    <p>          PATENT:  WO <b>2007106450</b>  ISSUE DATE:  20070920</p>

    <p>          APPLICATION: 2007  PP: 110pp.</p>

    <p>          ASSIGNEE:  (University of Georgia Research Foundation, Inc. USA)</p>

    <p>         </p>

    <p>22.   61783   HIV-LS-384; SCIFINDER-HIV-10/10/2007</p>

    <p class="memofmt1-2">          Use of specific hydrazone compounds for the treatment of viral infections</p>

    <p>          Hauber, Joachim, Hauber, Ilona, Schaefer, Birgit, and Bevec, Dorian</p>

    <p>          PATENT:  EP <b>1834669</b>  ISSUE DATE: 20070919</p>

    <p>          APPLICATION: 2006-5327  PP: 19pp.</p>

    <p>          ASSIGNEE:  (Heinrich-Pette-Institut, Germany)</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
