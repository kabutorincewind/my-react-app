

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-08-16.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="84HcEJRP3oqVEYTO91gJgYLMVOlLYc6OfM+6M8k6To3j2OaZAhwY6rSTn5tZn0tD2JiWJRftNNuvQR8Ip7QwxBo7gkZ0cqbhp9GPBqVf9OODd2GQcC86i9v7hV8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7E0B65FA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 3 - August 16, 2012</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22835135">Mulberrofuran G and Isomulberrofuran G from Morus alba L.: anti-Hepatitis B Virus Activity and Mass Spectrometric Fragmentation</a><i>.</i> Geng, C.A., Y.B. Ma, X.M. Zhang, S.Y. Yao, D.Q. Xue, R.P. Zhang, and J.J. Chen. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22835135].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0802-081612.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22869577">Preclinical Profile and Characterization of the Hepatitis C Virus NS3 Protease Inhibitor Asunaprevir (BMS-650032)<i>.</i></a> McPhee, F., A.K. Sheaffer, J. Friborg, D. Hernandez, P. Falk, G. Zhai, S. Levine, S. Chaniewski, F. Yu, D. Barry, C. Chen, M.S. Lee, K. Mosure, L.Q. Sun, M. Sinz, N.A. Meanwell, R.J. Colonno, J. Knipe, and P. Scola. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22869577].</p>

    <p class="plaintext"><b>[PubMed]</b>.  OV_0802-081612.</p>
    <p> </p>
    
    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22789428">Antiviral Activity and Synthesis of Quaternized Promazine Derivatives against HSV-1</a><i>.</i> Purohit, A.K., M.D. Balish, J.J. Leichty, A. Roe, L.M. Ward, M.O. Mitchell, and S.C. Hsia. Bioorganic and Medicinal Chemistry Letters, 2012. 22(16): p. 5308-5312; PMID[22789428].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0802-081612.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22826233">Effective Inhibition of Cytomegalovirus Infection by External Guide Sequences in Mice</a><i>.</i> Jiang, X., H. Gong, Y.C. Chen, G.P. Vu, P. Trang, C.Y. Zhang, S. Lu, and F. Liu. Proceedings of the National Academy of Sciences USA, 2012. 109(32): p. 13070-13075; PMID[22826233].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0802-081612. </p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">5. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=1&amp;SID=4AI39gGg5m6jInmjkH2&amp;page=1&amp;doc=1">Synthesis and Evaluation of Novel Potent HCV NS5A Inhibitors</a><i>.</i> Zhang, H.W., L.H. Zhou, F. Amblard, J.X. Shi, D.R. Bobeck, S.J. Tao, T.R. McBrayer, P.M. Tharnish, T. Whitaker, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(14): p. 4864-4868; ISI[000306101300073].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0802-081612. </p>

    <p> </p>

    <p class="plaintext">6. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=2&amp;SID=4AI39gGg5m6jInmjkH2&amp;page=1&amp;doc=1">Human Mannose-binding Lectin Inhibits Human Cytomegalovirus Infection in Human Embryonic Pulmonary Fibroblast</a><i>.</i> Wu, W., Y.H. Chen, H.J. Qiao, R. Tao, W.Z. Gu, and S.Q. Shang. APMIS, 2012. 120(8): p. 675-682; ISI[000306310500010].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0802-081612. </p>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=4&amp;SID=4AI39gGg5m6jInmjkH2&amp;page=1&amp;doc=1">Hepatocytes That Express Variants of Cyclophilin A Are Resistant to HCV Infection and Replication</a><i>.</i> von Hahn, T., C. Schiene-Fischer, N.D. Van, S. Pfaender, B. Karavul, E. Steinmann, A. Potthoff, C. Strassburg, N. Hamdi, A.I. Abdelaziz, C. Sarrazin, T. Muller, T. Berg, E. Trepo, H. Wedemeyer, M.P. Manns, T. Pietschmann, and S. Ciesek. Gastroenterology, 2012. 143(2): p. 439-+; ISI[000306661400035].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0802-081612.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
