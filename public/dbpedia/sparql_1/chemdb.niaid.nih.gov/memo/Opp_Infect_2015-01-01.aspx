

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-01-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="g6L4BYn7TALuDtJaPTlLv8/KORW4WTFp6Sc4abgKecQZwWpCRuV1Olnf6ub6GdUrPb/E33ZklJJBxjGOdVPF//G+UItV7t1LA9fclkYsrGhsXCB9mftUMPSovmY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E13916F1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 19 - January 1, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25437914">Jatrophanes from Euphorbia squamosa as Potent Inhibitors of Candida albicans Multidrug Transporters.</a> Rawal, M.K., Y. Shokoohinia, G. Chianese, B. Zolfaghari, G. Appendino, O. Taglialatela-Scafati, R. Prasad, and A. Di Pietro. Journal of Natural Products, 2014. 77(12): p. 2700-2706. PMID[25437914].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25534679">Synthesis, Characterization, Antimicrobial Activity and Mechanism of a Novel Hydroxyapatite Whisker/Nano Zinc Oxide Biomaterial.</a> Yu, J., W. Zhang, Y. Li, G. Wang, L. Yang, J. Jin, Q. Chen, and M. Huang. Biomedical Materials, 2014. 10(1): p. 015001. PMID[25534679].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25531369">Comparative Study of Surface-active Properties and Antimicrobial Activities of Disaccharide Monoesters.</a> Zhang, X., F. Song, M. Taxipalati, W. Wei, and F. Feng. Plos One, 2014. 9(12): p. e114845. PMID[25531369].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25375643">Polyacrylic acid-coated Iron oxide Nanoparticles for Targeting Drug Resistance in Mycobacteria.</a> Padwal, P., R. Bandyopadhyaya, and S. Mehra. Langmuir, 2014. 30(50): p. 15266-15276. PMID[25375643].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25412465">Synthesis of Gallinamide A Analogues as Potent Falcipain Inhibitors and Antimalarials.</a> Conroy, T., J.T. Guo, N. Elias, K.M. Cergol, J. Gut, J. Legac, L. Khatoon, Y. Liu, S. McGowan, P.J. Rosenthal, N.H. Hunt, and R.J. Payne. Journal of Medicinal Chemistry, 2014. 57(24): p. 10557-10563. PMID[25412465].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25532844">Design and Synthesis of Chalcone Derivatives as Inhibitors of the Ferredoxin - Ferredoxin-NADP+ Reductase Interaction of Plasmodium falciparum: Pursuing New Antimalarial Agents.</a> Suwito, H., Jumina, Mustofa, P. Pudjiastuti, M.Z. Fanani, Y. Kimata-Ariga, R. Katahira, T. Kawakami, T. Fujiwara, T. Hase, H.M. Sirat, and N.N. Puspaningsih. Molecules, 2014. 19(12): p. 21473-21488. PMID[25532844].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25474504">Synthesis, in Vitro Evaluation and Cocrystal Structure of 4-Oxo-[1]benzopyrano[4,3-c]pyrazole Cryptosporidium parvum Inosine 5&#39;-Monophosphate Dehydrogenase (CpIMPDH) Inhibitors.</a> Sun, Z., J. Khan, M. Makowska-Grzyska, M. Zhang, J.H. Cho, C. Suebsuwong, P. Vo, D.R. Gollapalli, Y. Kim, A. Joachimiak, L. Hedstrom, and G.D. Cuny. Journal of Medicinal Chemistry, 2014. 57(24): p. 10544-10550. PMID[25474504].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1219-010115.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345752800011">In Vitro Screening of Cytotoxic, Antimicrobial and Antioxidant Activities of Clinacanthus nutans (Acanthaceae) Leaf Extracts.</a> Arullappan, S., P. Rajamanickam, N. Thevar, and C.C. Kodimani. Tropical Journal of Pharmaceutical Research, 2014. 13(9): p. 1455-1461. ISI[000345752800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345498800008">Al-2 of Aggregatibacter actinomycetemcomitans Inhibits Candida albicans Biofilm Formation.</a> Bachtiar, E.W., B.M. Bachtiar, L.M. Jarosz, L.R. Amir, H. Sunarto, H. Ganin, M.M. Meijler, and B.P. Krom. Frontiers in Cellular and Infection Microbiology, 2014. 4(94): 8pp. ISI[000345498800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344996600006">Design, Synthesis and Biological Evaluation of Benzimidazole-pyridine-piperidine Hybrids as a New Class of Potent Antimicrobial Agents.</a> Beulah, K., A.R. Kumar, B.P.V. Lingaiah, P.S. Rao, B. Narsaiah, A.S.K. Reddy, and U.S.N. Murty. Letters in Drug Design &amp; Discovery, 2015. 12(1): p. 38-45. ISI[000344996600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345232500022">Antimicrobial Mechanism of Copper (II) 1,10-Phenanthroline and 2,2&#39;-Bipyridyl Complex on Bacterial and Fungal Pathogens.</a> Chandraleka, S., K. Ramya, G. Chandramohan, D. Dhanasekaran, A. Priyadharshini, and A. Panneerselvam. Journal of Saudi Chemical Society, 2014. 18(6): p. 953-962. ISI[000345232500022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344970400011">In Vitro Biological Investigations of Novel Piperazine Based Heterocycles.</a> Chhatriwala, N.M., A.B. Patel, R.V. Patel, and P. Kumari. Journal of Chemical Research, 2014. 38(10): p. 611-616. ISI[000344970400011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345232500023">Green Synthesis of Novel Quinoline Based Imidazole Derivatives and Evaluation of their Antimicrobial Activity.</a> Desai, N.C., A.S. Maheta, K.M. Rajpara, V.V. Joshi, H.V. Vaghani, and H.M. Satodiya. Journal of Saudi Chemical Society, 2014. 18(6): p. 963-971. ISI[000345232500023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345065200008">Synthesis, Antimicrobial Activity and Physico-chemical Properties of some N-Alkyldimethylbenzylammonium halides.</a> El Hage, S., B. Lajoie, J.L. Stigliani, A. Furiga-Chusseau, C. Roques, and G. Baziard. Journal of Applied Biomedicine, 2014. 12(4): p. 245-253. ISI[000345065200008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345581600027">Sodium Houttuyfonate and EDTA-Na2 in Combination Effectively Inhibits Pseudomonas aeruginosa, Staphylococcus aureus and Candida albicans in Vitro and in Vivo.</a> Huang, W.F., Q.J. Duan, F. Li, J. Shao, H.J. Cheng, and D.Q. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(1): p. 142-147. ISI[000345581600027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345110500024">Chemical Composition and Antimicrobial Activity of the Essential Oils of Pinus peuce (Pinaceae) Growing Wild in R. Macedonia.</a> Karapandzova, M., G. Stefkov, I. Cvetkovikj, E. Trajkovska-Dokik, A. Kaftandzieva, and S. Kulevanova. Natural Product Communications, 2014. 9(11): p. 1623-1628. ISI[000345110500024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345752800016">Screening for Anticandidal and Antibiofilm Activity of some Herbs in Thailand.</a> Kawsud, P., J. Puripattanavong, and R. Teanpaisan. Tropical Journal of Pharmaceutical Research, 2014. 13(9): p. 1495-1501. ISI[000345752800016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345106100069">Encapsulation of Thyme Essential Oils in Chitosan-benzoic acid Nanogel with Enhanced Antimicrobial Activity against Aspergillus flavus.</a> Khalili, S.T., A. Mohsenifar, M. Beyki, S. Zhaveh, T. Rahmani-Cherati, A. Abdollahi, M. Bayat, and M. Tabatabaei. LWT - Food Science and Technology, 2015. 60(1): p. 502-508. ISI[000345106100069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344996600005">Design, Synthesis and Antitubercular Evaluation of New 2-Amino-5-(4-(benzyloxy)benzyl)thiophene-3-carboxylic acid Derivatives. Part 3.</a> Lu, X.Y., J. Tang, Q.D. You, B.J. Wan, and S.G. Franzblau. Letters in Drug Design &amp; Discovery, 2015. 12(1): p. 29-37. ISI[000344996600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345232500007">Conventional and Microwave Synthesis, Spectral, Thermal and Antimicrobial Studies of some Transition Metal Complexes Containing 2-Amino-5-methylthiazole Moiety.</a> Mishra, A.P. and R.K. Jain. Journal of Saudi Chemical Society, 2014. 18(6): p. 814-824. ISI[000345232500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344965500001">Antifungal Activities of Thiosemicarbazones and Semicarbazones against Mycotoxigenic Fungi.</a> Paiva, R.D., L.F. Kneipp, C.M. Goular, M.A. Albuquerque, and A. Echevarria. Ciência e Agrotecnologia, 2014. 38(6): p. 531-537. ISI[000344965500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345457700012">Different Heterocycles Functionalized S-Triazine Analogues: Design, Synthesis and in Vitro Antimicrobial, Antituberculosis, and Anti-HIV Assessment.</a> Patel, P.K., R.V. Patel, D.H. Mahajan, P.A. Parikh, G.N. Mehta, C. Pannecouque, E. De Clercq, and K.H. Chikhalia. Journal of Heterocyclic Chemistry, 2014. 51(6): p. 1641-1658. ISI[000345457700012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345201000007">Uncatalyzed Synthesis of Furo(2,3-d)pyrimidine-2,4(1H,3H)-diones in Water and their Antimicrobial Activity.</a> Sambavekar, P.P., M.M. Aitawade, G.B. Kolekar, M.B. Deshmukh, and P.V. Anbhule. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2014. 53(11): p. 1454-1461. ISI[000345201000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345110500018">Antifungal Activity of Plant Extracts against Aspergillus niger and Rhizopus stolonifer.</a> Surapuram, V., W.N. Setzer, R.L. McFeeters, and H. McFeeters. Natural Product Communications, 2014. 9(11): p. 1603-1605. ISI[000345110500018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000345457700026">(3+2) Annulation of Amidinothioureas with Binucleophile: Synthesis and Antimicrobial Activity of 3-Phenylamino-5-aryl/alkyl-1,2,4-oxadiazole Derivatives.</a> Yerande, S.G., A.B. Ghaisas, K.M. Newase, W. Wang, K. Wang, and A. Domling. Journal of Heterocyclic Chemistry, 2014. 51(6): p. 1752-1756. ISI[000345457700026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1219-010115.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
