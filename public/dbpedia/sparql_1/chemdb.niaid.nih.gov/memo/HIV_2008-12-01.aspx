

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2008-12-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3Miu5o2+lJ0vNXDN1aS8WR4YU5WX5lnFvM7nhbRlPTVq1jEguBOmwyPseG9tQCuMbzI7z+1IBguIVUmp6v72Q/5qOsn+SVV9dlVw9iBH2jjjtfbVA0oPO4BVhU0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="08521941" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 13 - November 26, 2008</h1>

    <h2>Anti*HIV</h2>

    <p class="title">1.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19031451?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Immunotherapy of HIV-infected patients with Gc protein-derived macrophage activating factor (GcMAF).</a></p>

    <p class="title">Yamamoto N, Ushijima N, Koga Y.</p>

    <p class="source">J Med Virol. 2008 Nov 21;81(1):16-26. [Epub ahead of print]</p>

    <p class="source">PMID: 19031451 [PubMed - as supplied by publisher]                </p>

    <p class="source">&nbsp;</p>

    <p class="title">2.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19005871?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Efavirenz Mannich bases: Synthesis, anti-HIV and antitubercular activities.</a></p>

    <p class="authors">Sriram D, Banerjee D, Yogeeswari P.</p>

    <p class="source">J Enzyme Inhib Med Chem. 2008 Nov 13:1. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19005871 [PubMed - as supplied by publisher]                            </p>

    <p class="source">&nbsp;</p>

    <p class="title">3.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19004761?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Highly potent, fully recombinant anti-HIV chemokines: reengineering a low-cost microbicide.</a></p>

    <p class="authors">Gaertner H, Cerini F, Escola JM, Kuenzi G, Melotti A, Offord R, Rossitto-Borlat I, Nedellec R, Salkowitz J, Gorochov G, Mosier D, Hartley O.</p>

    <p class="source">Proc Natl Acad Sci U S A. 2008 Nov 18;105(46):17706-11. Epub 2008 Nov 12.</p>

    <p class="pmid">PMID: 19004761 [PubMed - in process]            </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">4.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18840408?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Denbinobin, a naturally occurring 1,4-phenanthrenequinone, inhibits HIV-1 replication through an NF-kappaB-dependent pathway.</a></p>

    <p class="authors">            Sánchez-Duffhues G, Calzado MA, de Vinuesa AG, Caballero FJ, Ech-Chahad A, Appendino G, Krohn K, Fiebich BL, Muñoz E.</p>

    <p class="source">            Biochem Pharmacol. 2008 Nov 15;76(10):1240-50. Epub 2008 Sep 18.</p>

    <p class="pmid">            PMID: 18840408 [PubMed - indexed for MEDLINE]</p>

    <h2>(Drug or Compound or Therapeutic) and (HIV or AIDS or Human Immunodeficiency Virus)</h2>

    <p class="title">5.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19029331?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of HIV-1 infection by the candidate microbicide, dapivirine, a non-nucleoside reverse transcriptase inhibitor.</a></p>

    <p class="authors">Fletcher P, Harman S, Azijn H, Armanasco N, Manlow P, Perumal D, de Bethune MP, Nuttall J, Romano J, Shattock R.</p>

    <p class="source">Antimicrob Agents Chemother. 2008 Nov 24. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19029331 [PubMed - as supplied by publisher]    </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">6.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18926711?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel piperidinylpyrimidine derivatives as inhibitors of HIV-1 LTR activation.</a></p>

    <p class="authors">Fujiwara N, Nakajima T, Ueda Y, Fujita H, Kawakami H.</p>

    <p class="source">Bioorg Med Chem. 2008 Nov 15;16(22):9804-16. Epub 2008 Sep 30.</p>

    <p class="pmid">PMID: 18926711 [PubMed - in process]            </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">7.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18818198?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The Antiherpetic Drug Acyclovir Inhibits HIV Replication and Selects the V75I Reverse Transcriptase Multidrug Resistance Mutation.</a></p>

    <p class="authors">McMahon MA, Siliciano JD, Lai J, Liu JO, Stivers JT, Siliciano RF, Kohli RM.</p>

    <p class="source">J Biol Chem. 2008 Nov 14;283(46):31289-93. Epub 2008 Sep 24.</p>

    <p class="pmid">PMID: 18818198 [PubMed - in process]</p>

    <h2>Inhibitor and (HIV or AIDS or Human Immunodeficiency Virus)</h2>

    <p class="title">8.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19019643?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A protein with antiproliferative, antifungal and HIV-1 reverse transcriptase inhibitory activities from caper (Capparis spinosa) seeds.</a></p>

    <p class="authors">Lam SK, Ng TB.</p>

    <p class="source">Phytomedicine. 2008 Nov 17. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19019643 [PubMed - as supplied by publisher]    </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">9.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19007201?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design of Annulated Pyrazoles as Inhibitors of HIV-1 Reverse Transcriptase.</a></p>

    <p class="authors">Sweeney ZK, Harris SF, Arora N, Javanbakht H, Li Y, Fretland J, Davidson JP, Billedeau JR, Gleason SK, Hirschfeld D, Kennedy-Smith JJ, Mirzadegan T, Roetz R, Smith M, Sperry S, Suh JM, Wu J, Tsing S, Villasen&#771;or AG, Paul A, Su G, Heilek G, Hang JQ, Zhou AS, Jernelius JA, Zhang FJ, Klumpp K.</p>

    <p class="source">J Med Chem. 2008 Nov 14. [Epub ahead of print]          </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">10.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18928291?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and synthesis of stereochemically defined novel spirocyclic P2-ligands for HIV-1 protease inhibitors.</a></p>

    <p class="authors">Ghosh AK, Chapsal BD, Baldridge A, Ide K, Koh Y, Mitsuya H.</p>

    <p class="source">Org Lett. 2008 Nov 20;10(22):5135-8. Epub 2008 Oct 18.</p>

    <p class="pmid">PMID: 18928291 [PubMed - in process]            </p>

    <h2>Journal Check</h2>

    <p class="title">11.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18926711?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel piperidinylpyrimidine derivatives as inhibitors of HIV-1 LTR activation.</a></p>

    <p class="authors">Fujiwara N, Nakajima T, Ueda Y, Fujita H, Kawakami H.</p>

    <p class="source">Bioorg Med Chem. 2008 Nov 15;16(22):9804-16. Epub 2008 Sep 30.</p>

    <p class="pmid">PMID: 18926711 [PubMed - in process]            </p>

    <p class="pmid">&nbsp;</p>

    <p class="title">12.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18834110?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pharmacophore modeling and molecular docking led to the discovery of inhibitors of human immunodeficiency virus-1 replication targeting the human cellular aspartic acid-glutamic acid-alanine-aspartic acid box polypeptide 3.</a></p>

    <p class="authors">Maga G, Falchi F, Garbelli A, Belfiore A, Witvrouw M, Manetti F, Botta M.</p>

    <p class="source">J Med Chem. 2008 Nov 13;51(21):6635-8. Epub 2008 Oct 4.</p>

    <p class="pmid">PMID: 18834110 [PubMed - in process]            </p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
