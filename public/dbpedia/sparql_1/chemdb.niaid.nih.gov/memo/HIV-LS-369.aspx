

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-369.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uONrKpphE2dbSP1bPHLUBPY597Wv1vH6A2ACGNxXTdUfdqhnoJ/8GABnoe3DRjTAZpKuxLnN6ZCiv99qB7Z3W95vX441N3fKT3wwTAO6A/1UBfyZ+s2obEZ1R1U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="768E0AA7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-369-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60291   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          The role of N-glycosylation sites on the CXCR4 receptor for CXCL-12 binding and signaling and X4 HIV-1 viral infectivity</p>

    <p>          Huskens, D, Princen, K, Schreiber, M, and Schols, D</p>

    <p>          Virology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331556&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331556&amp;dopt=abstract</a> </p><br />

    <p>2.     60292   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Resistance Mutations in Subtype C HIV Type 1 Isolates from Indian Patients of Mumbai Receiving NRTIs Plus NNRTIs and Experiencing a Treatment Failure: Resistance to AR</p>

    <p>          Deshpande, A, Jauvin, V, Magnin, N, Pinson, P, Faure, M, Masquelier, B, Aurillac-Lavignolle, V, and Fleury, HJ</p>

    <p>          AIDS Res Hum Retroviruses <b>2007</b>.  23(2): 335-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331042&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331042&amp;dopt=abstract</a> </p><br />

    <p>3.     60293   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Pyranopyridines and oxepinopyridines as protectants from HIV infection, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2007008539</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006  PP: 70pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60294   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of 1-[2&#39;,3&#39;-dideoxy-3&#39;-C-(hydroxymethyl)-b-D-erythropentofuranosyl] cytosine derivatives as HIV inhibitors</p>

    <p>          Zhou, Xiao-Xiong and Sahlberg, Christer</p>

    <p>          PATENT:  WO <b>2007006707</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006  PP: 81pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     60295   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p><b>          Bis-cycloSal-d4T-monophosphates: Drugs That Deliver Two Molecules of Bioactive Nucleotides</b> </p>

    <p>          Ducho, C, Gorbig, U, Jessel, S, Gisch, N, Balzarini, J, and Meier, C</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17328534&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17328534&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60296   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Williams, Theresa M and Zhang, Xu-Fang</p>

    <p>          PATENT:  WO <b>2007021629</b>  ISSUE DATE:  20070222</p>

    <p>          APPLICATION: 2006</p>

    <p>          ASSIGNEE:  (Merck &amp; Co., Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     60297   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Anti HIV-1 virucidal activity of polyamide nucleic acid-membrane transducing peptide conjugates targeted to primer binding site of HIV-1 genome</p>

    <p>          Tripathi, S, Chaubey, B, Barton, BE, and Pandey, VN</p>

    <p>          Virology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320140&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17320140&amp;dopt=abstract</a> </p><br />

    <p>8.     60298   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV-1 activities of novel podophyllotoxin derivatives</p>

    <p>          Chen, SW, Wang, YH, Jin, Y, Tian, X, Zheng, YT, Luo, DQ, and Tu, YQ</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317161&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317161&amp;dopt=abstract</a> </p><br />

    <p>9.     60299   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Evidence against a direct antiviral activity of the proteasome during the early steps of HIV-1 replication</p>

    <p>          Dueck, M and Guatelli, J</p>

    <p>          Virology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17316734&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17316734&amp;dopt=abstract</a> </p><br />

    <p>10.   60300   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Preparation and application of 2-alkylthio-5-alkyl-6-(1-cyanoarylmethyl) uracil compound as s-dabo reverse transcriptase inhibitor</p>

    <p>          Chen, Fener and Ji, Lei</p>

    <p>          PATENT:  CN <b>1903847</b>  ISSUE DATE: 20070131</p>

    <p>          APPLICATION: 1002-9770  PP: 10pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>11.   60301   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Arzanol, an Anti-inflammatory and Anti-HIV-1 Phloroglucinol alpha-Pyrone from Helichrysum italicum ssp. microphyllum</p>

    <p>          Appendino, G, Ottino, M, Marquez, N, Bianchi, F, Giana, A, Ballero, M, Sterner, O, Fiebich, BL, and Munoz, E</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315926&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17315926&amp;dopt=abstract</a> </p><br />

    <p>12.   60302   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          HIV entry inhibition by the envelope 2 glycoprotein of GB virus C</p>

    <p>          Jung, S, Eichenmuller, M, Donhauser, N, Neipel, F, Engel, AM, Hess, G, Fleckenstein, B, and Reil, H</p>

    <p>          AIDS <b>2007</b>.  21 (5): 645-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314528&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314528&amp;dopt=abstract</a> </p><br />

    <p>13.   60303   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Primary structure and carbohydrate-binding specificity of a potent anti-HIV lectin isolated from the filamentous cyanobacterium, oscillatoria agardhii</p>

    <p>          Sato, Y, Okuyama, S, and Hori, K</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314091&amp;dopt=abstract</a> </p><br />

    <p>14.   60304   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Identification of Nonpeptide CCR5 Receptor Agonists by Structure-based Virtual Screening</p>

    <p>          Kellenberger, E, Springael, JY, Parmentier, M, Hachet-Haas, M, Galzi, JL, and Rognan, D</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17311371&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17311371&amp;dopt=abstract</a> </p><br />

    <p>15.   60305   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p><b>          Abacavir sulfate/lamivudine/zidovudine fixed combination in the treatment of HIV infection</b> </p>

    <p>          Keiser, P and Nassar, N</p>

    <p>          Expert Opin Pharmacother <b>2007</b>.  8(4): 477-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17309342&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17309342&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60306   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Direct Inactivation of HIV-1 by a Novel Small Molecule Entry Inhibitor DCM205</p>

    <p>          Duong, YT, Meadows, DC, Srivastava, IK, Gervay-Hague, J, and North, TN</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17307982&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17307982&amp;dopt=abstract</a> </p><br />

    <p>17.   60307   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Noninfectious papilloma virus-like particles inhibit HIV-1 replication: implications for immune control of HIV-1 infection by IL-27</p>

    <p>          Fakruddin J Mohamad, Lempicki Richard A, Gorelick Robert J, Yang Jun, Adelsberger Joseph W, Garcia-Pineres Alfonso J, Pinto Ligia A, Lane H Clifford, and Imamichi Tomozumi</p>

    <p>          Blood <b>2007</b>.  109(5): 1841-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60308   HIV-LS-369; SCIFINDER-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Preparation of analogs and derivatives of prostratin and related phorbol compounds for use as analgesics</p>

    <p>          Xu, Rensheng, Zhao, Weimin, and Jiang, Chun</p>

    <p>          PATENT:  WO <b>2007009055</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006  PP: 80pp.</p>

    <p>          ASSIGNEE:  (Salvia Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60309   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          A New Benzofuranone and Anti-HIV Constituents from the Stems of Rhus chinensis</p>

    <p>          Gu, Q, Wang, RR, Zhang, XM, Wang, YH, Zheng, YT, Zhou, J, and Chen, JJ</p>

    <p>          Planta Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17290322&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17290322&amp;dopt=abstract</a> </p><br />

    <p>20.   60310   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Characterization of a replication-competent, integrase-defective human immunodeficiency virus (HIV)/simian virus 40 chimera as a powerful tool for the discovery and validation of HIV integrase inhibitors</p>

    <p>          Daelemans, D,  Lu, R, De Clercq, E, and Engelman, A</p>

    <p>          J Virol <b>2007</b> .</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17287285&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17287285&amp;dopt=abstract</a> </p><br />

    <p>21.   60311   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Prediction of the binding model of HIV-1 gp41 with small molecule inhibitors</p>

    <p>          Jun, Tan J, Kong, R, Xin, Wang C, and Zu, Chen W</p>

    <p>          Conf Proc IEEE Eng Med Biol Soc <b>2005</b>.  5: 4755-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17281304&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17281304&amp;dopt=abstract</a> </p><br />

    <p>22.   60312   HIV-LS-369; PUBMED-HIV-3/5/2007</p>

    <p class="memofmt1-2">          Targeting potential and anti-HIV activity of lamivudine loaded mannosylated poly (propyleneimine) dendrimer</p>

    <p>          Dutta, T and Jain, NK</p>

    <p>          Biochim Biophys Acta <b>2007</b>.  1770(4): 681-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276009&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276009&amp;dopt=abstract</a> </p><br />

    <p>23.   60313   HIV-LS-369; WOS-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Synthesis of diastereomeric 3-hydroxy-4-pyrrolidinyl derivatives of nucleobases</p>

    <p>          Rejman, D, Kocalka, P, Budesinsky, M, Pohl, R, and Rosenberg, I</p>

    <p>          TETRAHEDRON <b>2007</b>.  63(5): 1243-1253, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243742800022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243742800022</a> </p><br />

    <p>24.   60314   HIV-LS-369; WOS-HIV-2/26/2007</p>

    <p class="memofmt1-2">          Binding kinetics, uptake and intracellular accumulation of F105, an anti-gp 120 human IgG1 kappa monoclonal antibody, in HIV-1 infected cells</p>

    <p>          Clayton, R, Ohagen, A, Goethals, O, Smets, A, Van Loock, M, Michiels, L, Kennedy-Johnston, E, Cunningham, M, Jiang, HY, Bola, S, Gutshall, L, Gunn, G, Del Vecchio, A, Sarisky, R, Hallenberger, S, and Hertogs, K</p>

    <p>          JOURNAL OF VIROLOGICAL METHODS <b>2007</b>.  139(1): 17-23, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243740100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243740100003</a> </p><br />

    <p>25.   60315   HIV-LS-369; WOS-HIV-3/4/2007</p>

    <p class="memofmt1-2">          The tyrosine kinase inhibitor genistein blocks HIV-1 infection in primary human macrophages</p>

    <p>          Stantchev, TS, Markovic, I, Telford, WG, Clouse, KA, and Broder, CC</p>

    <p>          VIRUS RESEARCH  <b>2007</b>.  123(2): 178-189, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244017300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244017300009</a> </p><br />

    <p>26.   60316   HIV-LS-369; WOS-HIV-3/4/2007</p>

    <p class="memofmt1-2">          Antiviral activity of HIV type 1 protease inhibitors nelfinavir and indinavir in vivo is not influenced by P-glycoprotein activity on CD4(+) T cells</p>

    <p>          Sankatsing, SUC, Cornelissen, M, Kloosterboer, N, Crommentuyn, KML, Bosch, TM, Mul, FP, Jurriaans, S, Huitema, ADR, Beijnen, JH, Lange, JMA, Prins, JM, and Schuitemaker, H</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2007</b>.  23(1): 19-27, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243995300003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243995300003</a> </p><br />

    <p>27.   60317   HIV-LS-369; WOS-HIV-3/4/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV type 1 replication in CD4(+) and CD14(+) cells purified from HIV type 1-infected individuals by the 2-5A agonist immunomodulator, 2-5A(N6B)</p>

    <p>          Dimitrova, DI, Reichenbach, NL, Yang, XW, Pfleiderer, W, Charubala, R, Gaughan, JP, Suh, B, Henderson, EE, Suhadolnik, RJ, and Rogers, TJ</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2007</b>.  23(1): 123-134, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243995300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243995300017</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
