

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-03-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="n69gP2Zq5UjPEx/2AY3a08NknT41Ta6AanaRYvJ+4QlbfZqZioZ7gTQPBIee2NvVm5Nxocpb/uC33s0x4kfuY0zfa1Vu9cFb5yi5rPNYTgeuw5UvA/NAsctiq6E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="28423E95" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses  Citation List:  February 18, 2009 - March 3, 2009</h1>

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p class="ListParagraph">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226140?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">4-Benzyloxy-gamma-Sultone Derivatives: Discovery of a Novel Family of Non-Nucleoside Inhibitors of Human Cytomegalovirus and Varicella Zoster Virus.</a>
    <br />
    De Castro S, Garci&#769;a-Aparicio C, Andrei G, Snoeck R, Balzarini J, Camarasa MJ, Vela&#769;zquez S.
    <br />
    J Med Chem. 2009 Feb 18. [Epub ahead of print]
    <br>
    PMID: 19226140 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">Hepatitis Virus</p>

    <p class="ListParagraphCxSpFirst">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19251415?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Design, synthesis, and anti-HCV activity of thiourea compounds.</a>
    <br />
    Kang IJ, Wang LW, Lee CC, Lee YC, Chao YS, Hsu TA, Chern JH.
    <br />
    Bioorg Med Chem Lett. 2009 Feb 20. [Epub ahead of print]
    <br />
    PMID: 19251415 [PubMed - as supplied by publisher]
    <br />
    <br></p>

    <p class="ListParagraphCxSpMiddle">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19243496?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Suppression of hepatitis C virus replication by protein kinase C-related kinase 2 inhibitors that block phosphorylation of viral RNA polymerase.</a>
    <br>
    Kim SJ, Kim JH, Sun JM, Kim MG, Oh JW.
    <br>
    J Viral Hepat. 2009 Feb 23. [Epub ahead of print]
    <br>
    PMID: 19243496 [PubMed - as supplied by publisher
    <br />
    <br></p>

    <p class="ListParagraphCxSpMiddle">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226162?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Inhibitors of Hepatitis C Virus Polymerase: Synthesis and Biological Characterization of Unsymmetrical Dialkyl-Hydroxynaphthalenoyl-benzothiadiazines.</a>
    <br />
    Wagner R, Larson DP, Beno DW, Bosse TD, Darbyshire JF, Gao Y, Gates BD, He W, Henry RF, Hernandez LE, Hutchinson DK, Jiang WW, Kati WM, Klein LL, Koev G, Kohlbrenner W, Krueger AC, Liu J, Liu Y, Long MA, Maring CJ, Masse SV, Middleton T, Montgomery DA, Pratt JK, Stuart P, Molla A, Kempf DJ.
    <br />
    J Med Chem. 2009 Feb 18. [Epub ahead of print]
    <br />
    PMID: 19226162 [PubMed - as supplied by publisher]
    <br />
    <br /></p>

    <p class="ListParagraphCxSpMiddle">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19109388?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Arsenic trioxide inhibits hepatitis C virus RNA replication through modulation of the glutathione redox system and oxidative stress.</a>
    <br />
    Kuroki M, Ariumi Y, Ikeda M, Dansako H, Wakita T, Kato N.
    <br />
    J Virol. 2009 Mar;83(5):2338-48. Epub 2008 Dec 24.
    <br />
    PMID: 19109388 [PubMed - indexed for MEDLINE]</p>

    <p class="ListParagraphCxSpMiddle"><br>
    <br /></p>

    <p class="ListParagraphCxSpLast">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19075052?ordinalpos=9&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Robust antiviral efficacy upon administration of a nucleoside analog to hepatitis C virus-infected chimpanzees.</a>
    <br>
    Carroll SS, Ludmerer S, Handt L, Koeplinger K, Zhang NR, Graham D, Davies ME, MacCoss M, Hazuda D, Olsen DB.
    <br />
    Antimicrob Agents Chemother. 2009 Mar;53(3):926-34. Epub 2008 Dec 15.
    <br />
    PMID: 19075052 [PubMed - in process]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
