

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-10-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="rIQbqeyFUl9TEhIiZQ6pjSCHv9wMXnUad1PlZLKmUxZnctFTNj13FTEFXwQzvo9pslV81EjGRfmYeqGT0rW2Ni7OPAhNTqP+AGaiN3CcNuwGNdRKbWMEo/6knDE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7E6E1DD7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">October 1 - October 14, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p class="plaintext">1.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20541538">Carbocyclic 6-benzylthioinosine analogues as subversive substrates of Toxoplasma gondii adenosine kinase: biological activities and selective toxicities.</a> Al Safarjalani, O.N., R.H. Rais, Y.A. Kim, C.K. Chu, F.N. Naguib, and M.H. El Kouni. Biochemical Pharmacology, 2010. 80(7): p. 955-963; PMID[20541538].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">2.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20936361">In Vitro Antifungal Activity of Dihydroxyacetone Against Causative Agents of Dermatomycosis</a>.  Stopiglia, C.D., F.J. Vieira, A.G. Mondadori, T.P. Oppe, and M.L. Scroferneker. Mycopathologia, 2010; <b>[Epub ahead of print]</b>. PMID[20936361].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20702006">Synthesis and antimicrobial activities of novel 1H-dibenzo[a,c]carbazoles from dehydroabietic acid.</a> Gu, W. and S. Wang. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4692-4696; PMID[20702006].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20708826">Synthesis of novel sulfanilamide-derived 1,2,3-triazoles and their evaluation for antibacterial and antifungal activities</a>. Wang, X.L., K. Wan, and C.H. Zhou. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4631-4639; PMID[20708826].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20801655">2-, 3-, and 4-(1-Oxo-1H-2,3-dihydroisoindol-2-yl)benzoic acids and their corresponding organotin carboxylates: synthesis, characterization, fluorescent, and biological activities</a>. Cai, S.L., Y. Chen, W.X. Sun, H. Li, and S.S. Yuan. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(19): p. 5649-5652; PMID[20801655].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20634332">In vitro synergy of eugenol and methyleugenol with fluconazole against clinical Candida isolates</a>. Ahmad, A., A. Khan, L.A. Khan, and N. Manzoor, In vitro synergy of eugenol and methyleugenol with fluconazole against clinical Candida isolates. Journal of Medical Microbiology, 2010. 59(Pt 10): p. 1178-1184; PMID[20634332].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20674282">In vitro activity of isavuconazole against 140 reference fungal strains and 165 clinically isolated yeasts from Japan</a>. Yamazaki, T., Y. Inagaki, T. Fujii, J. Ohwada, M. Tsukazaki, I. Umeda, K. Kobayashi, N. Shimma, M.G. Page, and M. Arisawa. International Journal of Antimicrobial Agents, 2010. 36(4): p. 324-331; PMID[20674282].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20621141">Mammalian cell toxicity and candidacidal mechanism of Arg- or Lys-containing Trp-rich model antimicrobial peptides and their d-enantiomeric peptides</a>. Nan, Y.H., S.H. Lee, H.J. Kim, and S.Y. Shin. Peptides, 2010. 31(10): p. 1826-1831; PMID[20621141].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19888697">Inhibition of fungi and gram-negative bacteria by bacteriocin BacTN635 produced by Lactobacillus plantarum sp. TN635</a>.  Smaoui, S., L. Elleuch, W. Bejar, I. Karray-Rebai, I. Ayadi, B. Jaouadi, F. Mathieu, H. Chouayekh, S. Bejar, and L. Mellouli. Applied Biochemistry and Biotechnology, 2010. 162(4): p. 1132-1146; PMID[19888697].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20696500">Schiff bases of indoline-2,3-dione (isatin) derivatives and nalidixic acid carbohydrazide, synthesis, antitubercular activity and pharmacophoric model building</a>. Aboul-Fadl, T., F.A. Bin-Jubair, and O. Aboul-Wafa. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4578-4586; PMID[20696500].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20691508">Isoxazole analogs of curcuminoids with highly potent multidrug-resistant antimycobacterial activity</a>.  Changtam, C., P. Hongmanee, and A. Suksamrarn. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4446-4457; PMID[20691508].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20705367">E-2-[3-(3,4-dichlorophenyl)-1-oxo-2-propenyl]-3-methylquinoxaline-1,4-diox ide: a lead antitubercular agent which alters mitochondrial respiration in rat liver</a>. Das, U., S. Das, B. Bandy, D.K. Gorecki, and J.R. Dimmock. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4682-4686; PMID[20705367].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">13.<a href="http://www.ncbi.nlm.nih.gov/pubmed/20696503">Development of isoniazid-NAD truncated adducts embedding a lipophilic fragment as potential bi-substrate InhA inhibitors and antimycobacterial agents</a> . Delaine, T., V. Bernardes-Genisson, A. Quemard, P. Constant, B. Meunier, and J. Bernadou. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4554-4561; PMID[20696503].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20719764">Anti-tubercular screening of natural products from Colombian plants: 3-methoxynordomesticine, an inhibitor of MurE ligase of Mycobacterium tuberculosis.</a> Guzman, J.D., A. Gupta, D. Evangelopoulos, C. Basavannacharya, L.C. Pabon, E.A. Plazas, D.R. Munoz, W.A. Delgado, L.E. Cuca, W. Ribon, S. Gibbons, and S. Bhakta. Journal of Antimicrobial Chemotherapy, 2010. 65(10): p. 2101-2107; PMID[20719764].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20705368">Anti-tubercular agents. Part 5: synthesis and biological evaluation of benzothiadiazine 1,1-dioxide based congeners</a>.  Kamal, A., R.V. Shetti, S. Azeeza, S.K. Ahmed, P. Swapna, A.M. Reddy, I.A. Khan, S. Sharma, and S.T. Abdullah. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4545-4553; PMID[20705368].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20443682">Synthesis and antitubercular activity of heterocycle substituted diphenyl ether derivatives</a>.  Kini, S.G., A. Bhat, Z. Pan, and F.E. Dayan. Journal of Enzyme Inhibition and Medicinal Chemistry, 2010. 25(5): p. 730-736; PMID[20443682].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937542">New amino acid esters of salicylanilides active against MDR-TB and other microbes</a>.  Kratky, M., J. Vinsova, V. Buchta, K. Horvati, S. Bosze, and J. Stolarikova. European Journal of Medicinal Chemistry, 2010; <b>[Epub ahead of print]</b>. PMID[20937542].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20599632">Evaluation of flavonoids from Dorstenia barteri for their antimycobacterial, antigonorrheal and anti-reverse transcriptase activities</a>.  Kuete, V., B. Ngameni, A.T. Mbaveng, B. Ngadjui, J.J. Meyer, and N. Lall. Acta Tropica, 2010. 116(1): p. 100-104; PMID[20599632].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20934382">Leads for antitubercular compounds from kinase inhibitor library screens</a>.  Magnet, S., R.C. Hartkoorn, R. Szekely, J. Pato, J.A. Triccas, P. Schneider, C. Szantai-Kis, L. Orfi, M. Chambon, D. Banfi, M. Bueno, G. Turcatti, G. Keri, and S.T. Cole. Tuberculosis (Edinburg), 2010; <b>[Epub ahead of print]</b>. PMID[20934382].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20656380">Synthesis and antimycobacterial activity of new quinoxaline-2-carboxamide 1,4-di-N-oxide derivatives</a>. Moreno, E., S. Ancizu, S. Perez-Silanes, E. Torres, I. Aldana, and A. Monge. European Journal of Medicinal Chemistry, 2010. 45(10): p. 4418-4426; PMID[20656380].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20927480">Synthesis and structure of azole-fused indeno[2,1-c]quinolines and their anti-mycobacterial properties</a>. Upadhayaya, R.S., P.D. Shinde, A.Y. Sayyed, S.A. Kadam, A.N. Bawane, A. Poddar, O. Plashkevych, A. Foldesi, and J. Chattopadhyaya. Organic and Biomolecular Chemistry, 2010; <b>[Epub ahead of print]</b>. PMID[20927480].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1001-101410.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281750900005">Synthesis of novel 1,3,4-oxadiazole derivatives as potential antimicrobial agents.</a> Chawla, R., A. Arora, M.K. Parameswaran, P. Chan-Der Sharma, S. Michael, and T.K. Ravi. Acta Poloniae Pharmaceutica, 2010. 67(3): p. 247-253; ISI[000281750900005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282144100002">Activity of beta-lapachone derivatives against rifampicin-susceptible and -resistant strains of Mycobacterium tuberculosis.</a> Coelho, T.S., R.S.F. Silva, A.V. Pinto, M. Pinto, C.J. Scaini, K.C.G. Moura, and P.A. da Silva. Tuberculosis, 2010. 90(5): p. 293-297; ISI[000282144100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281963700032">Novel dihydropyrimidines as a potential new class of antitubercular agents.</a> Trivedi, A.R., V.R. Bhuva, B.H. Dholariya, D.K. Dodiya, V.B. Kataria, and V.H. Shah. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(20): p. 6100-6102; ISI[000281963700032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">25.<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281907200058">In Vivo Efficacy of Anidulafungin against Mature Candida albicans Biofilms in a Novel Rat Model of Catheter-Associated Candidiasis Kucharikova.</a> Kucharikova, S., H. Tournu, M. Holtappels, P. Van Dijck, and K. Lagrou. Antimicrobial Agents and Chemotherapy, 2010. 54(10): p. 4474-4475; ISI[000281907200058].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1001-101410.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281751000014">Novel Chromeneimidazole Derivatives as Antifungal Compounds: Synthesis and In Vitro Evaluation.</a> Thareja, S., A. Verma, A. Kalra, S. Gosain, P.V. Rewatkar, and G.R. Kokil. Acta Poloniae Pharmaceutica, 2010. 67(4): p. 423-427; ISI[000281751000014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1001-101410.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
