

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="PliMV318d4vpxJGvvpJXbifJcv/y3H7QQ9Pf5Ew5omnSr2uweGhtxsDpq20w8sng+CsbbB3fnMSXvL6eeLRjdFMptmXxSmWWtv79NAYZFrzluYedXX4JsdSwYAw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA27456E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List:</p>

    <p class="memofmt2-h1">September 2, 2009 - September 15, 2009</p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19740508?ordinalpos=33&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A cell-permeable peptide inhibits hepatitis C virus replication by sequestering IRES transacting factors.</a></p>

    <p class="NoSpacing">Fontanes V, Raychaudhuri S, Dasgupta A.</p>

    <p class="NoSpacing">Virology. 2009 Sep 7. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19740508 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19725579?ordinalpos=56&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Phosphoramidate prodrugs of 2&#39;-C-methylcytidine for therapy of hepatitis C virus infection.</a></p>

    <p class="NoSpacing">Gardelli C, Attenni B, Donghi M, Meppen M, Pacini B, Harper S, Di Marco A, Fiore F, Giuliano C, Pucci V, Laufer R, Gennari N, Marcucci I, Leone JF, Olsen DB, MacCoss M, Rowley M, Narjes F.</p>

    <p class="NoSpacing">J Med Chem. 2009 Sep 10;52(17):5394-407.</p>

    <p class="NoSpacing">PMID: 19725579 [PubMed - in process]</p>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19589358?ordinalpos=79&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of allosteric inhibitors blocking the hepatitis C virus polymerase NS5B in the RNA synthesis initiation step.</a></p>

    <p class="NoSpacing">Betzi S, Eydoux C, Bussetta C, Blemont M, Leyssen P, Debarnot C, Ben-Rahou M, Haiech J, Hibert M, Gueritte F, Grierson DS, Romette JL, Guillemot JC, Neyts J, Alvarez K, Morelli X, Dutartre H, Canard B.</p>

    <p class="NoSpacing">Antiviral Res. 2009 Oct;84(1):48-59. Epub 2009 Jul 7.</p>

    <p class="NoSpacing">PMID: 19589358 [PubMed - in process]</p>

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19740994?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of human cytomegalovirus replication via peptide aptamers directed against the non-conventional nuclear localization signal of the essential viral replication factor pUL84.</a></p>

    <p class="NoSpacing">Kaiser N, Lischka P, Wagenknecht N, Stamminger T.</p>

    <p class="NoSpacing">J Virol. 2009 Sep 9. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19740994 [PubMed - as supplied by publisher]</p>

    <p class="NoSpacing">Citation from a Review we abstracted, &quot;Diastereoselective 1,3-Dipolar Cycloaddition Reactions between Azomethine Ylides and Chiral Acrylates Derived from Methyl (S)- and (R)-Lactate -Synthesis of Hepatitis C Virus RNA-Dependent RNA Polymerase Inhibitors&quot;:</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17269759?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Optimization of novel acyl pyrrolidine inhibitors of hepatitis C virus RNA-dependent RNA polymerase leading to a development candidate.</a></p>

    <p class="NoSpacing">Slater MJ, Amphlett EM, Andrews DM, Bravi G, Burton G, Cheasty AG, Corfield JA, Ellis MR, Fenwick RH, Fernandes S, Guidetti R, Haigh D, Hartley CD, Howes PD, Jackson DL, Jarvest RL, Lovegrove VL, Medhurst KJ, Parry NR, Price H, Shah P, Singh OM, Stocker R, Thommes P, Wilkinson C, Wonacott A.</p>

    <p class="NoSpacing">J Med Chem. 2007 Mar 8;50(5):897-900. Epub 2007 Feb 2.</p>

    <p class="NoSpacing">PMID: 17269759 [PubMed - indexed for MEDLINE]</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">6. Sivets, GG., et al</p>

    <p class="NoSpacing">SYNTHESIS AND ANTIVIRAL ACTIVITY OF PURINE 2 &#39;,3 &#39;-DIDEOXY-2 &#39;,3  &#39;-DIFLUORO-D-ARABINOFURANOSYL NUCLEOSIDES</p>

    <p class="NoSpacing">NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS  2009 (May-July) 28: 519 - 536</p>

    <p class="ListParagraph">7. Amblard, F., et al</p>

    <p class="NoSpacing">Synthesis, antiviral activity, and stability of nucleoside analogs containing tricyclic bases</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (October) 44: 3845 - 3851</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
