

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-124.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dhOBuhsr+yjWpvdAoeV0WHdcKxSi1nni1B2AESzLQ8/KfrYVUaqGG9Uiddznojv6IlsnUdFx6D+p6WrcPJ357FUqUpGTfcz69ZSf5/ojodDc+JvdbDIlO/qcIZQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="952477DB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-124-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58669   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication by APOBEC3G in vitro and in vivo</p>

    <p>          Lei, YC, Hao, YH, Zhang, ZM, Tian, YJ, Wang, BJ, Yang, Y, Zhao, XP, Lu, MJ, Gong, FL, and Yang, DL</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(28): 4492-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16874860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16874860&amp;dopt=abstract</a> </p><br />

    <p>2.     58670   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Research Hits Its Stride - Promising New Treatments for Hbv and Hcv Emerge From Icar Meeting</p>

    <p>          Lipp, E.</p>

    <p>          Genetic Engineering News <b>2006</b>.  26(13): 1-+</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239019200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239019200001</a> </p><br />

    <p>3.     58671   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Antiviral Target Revealed by the Hexameric Structure of Mouse Hepatitis Virus Nonstructural Protein nsp15</p>

    <p>          Xu, X, Zhai, Y, Sun, F, Lou, Z, Su, D, Xu, Y, Zhang, R, Joachimiak, A, Zhang, XC, Bartlam, M, and Rao, Z</p>

    <p>          J Virol <b>2006</b>.  80(16): 7909-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16873248&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16873248&amp;dopt=abstract</a> </p><br />

    <p>4.     58672   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A randomised trial to compare the pharmacokinetic, pharmacodynamic, and antiviral effects of peginterferon alfa-2b and peginterferon alfa-2a in patients with chronic hepatitis C (COMPARE)</p>

    <p>          Silva, Marcelo, Poo, Jorge, Wagner, Frank, Jackson, Mary, Cutler, David, Grace, Michael, Bordens, Ronald, Cullen, Connie, Harvey, Joann, and Laughlin, Mark</p>

    <p>          Journal of Hepatology <b>2006</b>.  45(2): 204-213</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4JRV765-1/2/54f42af2eaf4aaf8d301cf69019a3eaa">http://www.sciencedirect.com/science/article/B6W7C-4JRV765-1/2/54f42af2eaf4aaf8d301cf69019a3eaa</a> </p><br />

    <p>5.     58673   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Production of infectious hepatitis C virus particles in three-dimensional cultures of the cell line carrying the genome-length dicistronic viral RNA of genotype 1b</p>

    <p>          Murakami, Kyoko, Ishii, Koji, Ishihara, Yousuke, Yoshizaki, Sayaka, Tanaka, Keiko, Gotoh, Yasufumi, Aizaki, Hideki, Kohara, Michinori, Yoshioka, Hiroshi, and Mori, Yuichi</p>

    <p>          Virology <b>2006</b>.  351(2): 381-392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JWMT2K-5/2/3e07eab0848ccb40bb08142eb25e03b2">http://www.sciencedirect.com/science/article/B6WXR-4JWMT2K-5/2/3e07eab0848ccb40bb08142eb25e03b2</a> </p><br />
    <br clear="all">

    <p>6.     58674   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Different Anti-Hcv Profiles of Statins and Their Potential for Combination Therapy With Interferon</p>

    <p>          Ikeda, M. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(1): 117-125</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238690900016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238690900016</a> </p><br />

    <p>7.     58675   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Expression of vector-based small interfering RNA against West Nile virus effectively inhibits virus replication</p>

    <p>          Ong, SP, Choo, BG, Chu, JJ, and Ng, ML</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870272&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16870272&amp;dopt=abstract</a> </p><br />

    <p>8.     58676   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Influence of neopterin and 7,8-dihydroneopterin on the replication of Coxsackie type B5 and influenza A viruses</p>

    <p>          Bratslavska, O, Platace, D, Miklasevics, E, Fuchs, D, and Martinsons, A</p>

    <p>          Med Microbiol Immunol (Berl) <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16868770&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16868770&amp;dopt=abstract</a> </p><br />

    <p>9.     58677   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Probing the Substrate Specificity of the Dengue Virus Type 2 Ns3 Serine Protease by Using Internally Quenched Fluorescent Peptides</p>

    <p>          Niyomrattanakit, P. <i>et al.</i></p>

    <p>          Biochemical Journal <b>2006</b>.  397: 203-211</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238699200023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238699200023</a> </p><br />

    <p>10.   58678   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure of the catalytic domain of the hepatitis C virus NS2-3 protease</p>

    <p>          Lorenz, IC, Marcotrigiano, J, Dentzer, TG, and Rice, CM</p>

    <p>          Nature <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16862121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16862121&amp;dopt=abstract</a> </p><br />

    <p>11.   58679   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Bax Activity Is Crucial for the Antiapoptotic Function of the Human Papillomavirus E6 Oncoprotein</p>

    <p>          Vogt, M. <i>et al.</i></p>

    <p>          Oncogene <b>2006</b>.  25(29): 4009-4015</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238802400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238802400001</a> </p><br />
    <br clear="all">

    <p>12.   58680   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral acyclic nucleoside phosphonates structure activity studies</p>

    <p>          Holy, A</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16857275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16857275&amp;dopt=abstract</a> </p><br />

    <p>13.   58681   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A novel mutation in the UL54 gene of human cytomegalovirus isolates that confers resistance to foscarnet</p>

    <p>          Ducancelle, A, Champier, G, Alain, S, Petit, F, Le, Pors MJ, and Mazeron, MC</p>

    <p>          Antivir Ther <b>2006</b>.  11(4): 537-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856628&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856628&amp;dopt=abstract</a> </p><br />

    <p>14.   58682   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structure and Regulatory Profile of the Monkeypox Inhibitor of Complement: Comparison to Homologs in Vaccinia and Variola and Evidence for Dimer Formation</p>

    <p>          Liszewski, M. <i> et al.</i></p>

    <p>          Journal of Immunology <b>2006</b>.  176(6): 3725-3734</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238768400048">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238768400048</a> </p><br />

    <p>15.   58683   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rapid identification of small interfering RNA that can effectively inhibit the replication of multiple influenza B virus strains</p>

    <p>          Gao, Y, Sun, L, Dong, J, Xu, X, Shu, Y, Chen, M, Yin, L, Liang, Z, and Jin, Q</p>

    <p>          Antivir Ther <b>2006</b>.  11(4): 431-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856616&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16856616&amp;dopt=abstract</a> </p><br />

    <p>16.   58684   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A cell-based luminescence assay is effective for high-throughput screening of potential influenza antivirals</p>

    <p>          Noah, James W, Severson, William, Noah, Diana L, Rasmussen, Lynn, White, ELucile, and Jonsson, Colleen B</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4KHBYF0-1/2/83f7f05bce14051aca243749ddb7576a">http://www.sciencedirect.com/science/article/B6T2H-4KHBYF0-1/2/83f7f05bce14051aca243749ddb7576a</a> </p><br />
    <br clear="all">

    <p>17.   58685   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of influenza-virus-induced cytopathy by sialylglycoconjugates</p>

    <p>          Terabayashi, T, Morita, M, Ueno, M, Nakamura, T, and Urashima, T</p>

    <p>          Carbohydr Res <b>2006</b>.  341(13): 2246-53</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854400&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854400&amp;dopt=abstract</a> </p><br />

    <p>18.   58686   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Arbidol: a broad-spectrum antiviral that inhibits acute and chronic HCV infection</p>

    <p>          Boriskin, YS, Pecheur, EI, and Polyak, SJ</p>

    <p>          Virol J <b>2006</b>.  3(1): 56</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854226&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854226&amp;dopt=abstract</a> </p><br />

    <p>19.   58687   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hepatitis B Virus Activity of Novel Benzimidazole Derivatives</p>

    <p>          Li, YF, Wang, GF, He, PL, Huang, WG, Zhu, FH, Gao, HY, Tang, W, Luo, Y, Feng, CL, Shi, LP, Ren, YD, Lu, W, and Zuo, JP</p>

    <p>          J Med Chem <b>2006</b>.  49(15): 4790-4794</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854087&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854087&amp;dopt=abstract</a> </p><br />

    <p>20.   58688   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Benzimidazole Derivatives Bearing Substituted Biphenyls as Hepatitis C Virus NS5B RNA-Dependent RNA Polymerase Inhibitors: Structure-Activity Relationship Studies and Identification of a Potent and Highly Selective Inhibitor JTK-109</p>

    <p>          Hirashima, S, Suzuki, T, Ishida, T, Noji, S, Yata, S, Ando, I, Komatsu, M, Ikeda, S, and Hashimoto, H</p>

    <p>          J Med Chem <b>2006</b>.  49(15): 4721-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854079&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16854079&amp;dopt=abstract</a> </p><br />

    <p>21.   58689   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Molecular determinants of Ebola virus virulence in mice</p>

    <p>          Ebihara, H, Takada, A, Kobasa, D, Jones, S, Neumann, G, Theriault, S, Bray, M, Feldmann, H, and Kawaoka, Y</p>

    <p>          PLoS Pathog <b>2006</b>.  2(7): e73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16848640&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16848640&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   58690   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Experimental study on the action of allitridin against human cytomegalovirus in vitro: Inhibitory effects on immediate-early genes</p>

    <p>          Zhen, H, Fang, F, Ye, DY, Shu, SN, Zhou, YF, Dong, YS, Nie, XC, and Li, G</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844239&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844239&amp;dopt=abstract</a> </p><br />

    <p>23.   58691   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis B virus is inhibited by RNA interference in cell culture and in mice</p>

    <p>          Ying, RS, Zhu, C, Fan, XG, Li, N, Tian, XF, Liu, HB, and Zhang, BX</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844238&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16844238&amp;dopt=abstract</a> </p><br />

    <p>24.   58692   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti-RNA virus activity of polyoxometalates</p>

    <p>          Shigeta, Shiro, Mori, Shuichi, Yamase, Toshihiro, Yamamoto, Norio, and Yamamoto, Naoki</p>

    <p>          Biomedecine &amp; Pharmacotherapy <b>2006</b>.  60(5): 211-219</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKN-4K1HCM3-3/2/f850053b2c407413a3e6088e3e26f27f">http://www.sciencedirect.com/science/article/B6VKN-4K1HCM3-3/2/f850053b2c407413a3e6088e3e26f27f</a> </p><br />

    <p>25.   58693   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Selective inhibitors of hepatitis C virus replication</p>

    <p>          Neyts, J</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16843538&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16843538&amp;dopt=abstract</a> </p><br />

    <p>26.   58694   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of Methyl Inosine Monophosphate (Mimp) and Peramivir Activities in a Murine Model of Lethal Influenza a Virus Infection</p>

    <p>          Mishin, V. <i>et al.</i></p>

    <p>          Antiviral Research <b>2006</b>.  71(1): 64-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238904100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238904100008</a> </p><br />

    <p>27.   58695   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral drug discovery against SARS-CoV</p>

    <p>          Wu, YS, Lin, WH, Hsu, JT, and Hsieh, HP</p>

    <p>          Curr Med Chem <b>2006</b>.  13(17): 2003-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842194&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16842194&amp;dopt=abstract</a> </p><br />

    <p>28.   58696   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Antiviral Drugs</p>

    <p>          Bitsch, A. and Prange, H.</p>

    <p>          Aktuelle Neurologie <b>2006</b>.  33(5): 257-262</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238845700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238845700002</a> </p><br />

    <p>29.   58697   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytomegalovirus disease in the era of highly active antiretroviral therapy (HAART)</p>

    <p>          Steininger, Christoph, Puchhammer-Stockl, Elisabeth, and Popow-Kraupp, Theresia</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4JW15PV-2/2/a7fd42a0e2713455bd15ce5d7e701a59">http://www.sciencedirect.com/science/article/B6VJV-4JW15PV-2/2/a7fd42a0e2713455bd15ce5d7e701a59</a> </p><br />

    <p>30.   58698   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral therapy of chronic hepatitis B</p>

    <p>          Zoulim, Fabien </p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JT8VNP-1/2/6742f8949ae2a07600559bba971f4fe4">http://www.sciencedirect.com/science/article/B6T2H-4JT8VNP-1/2/6742f8949ae2a07600559bba971f4fe4</a> </p><br />

    <p>31.   58699   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Resveratrol inhibition of varicella-zoster virus replication in vitro</p>

    <p>          Docherty, John J, Sweet, Thomas J, Bailey, Erin, Faith, Seth A, and Booth, Tristan</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4KGX7HT-1/2/acf9ff3694b5e168718739d6d0278fd4">http://www.sciencedirect.com/science/article/B6T2H-4KGX7HT-1/2/acf9ff3694b5e168718739d6d0278fd4</a> </p><br />

    <p>32.   58700   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Unravelling the Complexities of Respiratory Syncytial Virus Rna Synthesis</p>

    <p>          Cowton, V., Mcgivern, D., and Fearns, R.</p>

    <p>          Journal of General Virology <b>2006</b>.  87: 1805-1821</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238707600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238707600003</a> </p><br />

    <p>33.   58701   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and structure-based optimization of novel dihydropyrones as potent HCV RNA polymerase inhibitors</p>

    <p>          Li, H, Tatlock, J, Linton, A, Gonzalez, J, Borchardt, A, Dragovich, P, Jewell, T, Prins, T, Zhou, R, Blazel, J, Parge, H, Love, R, Hickey, M, Doan, C, Shi, S, Duggal, R, Lewis, C, and Fuhrman, S</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16824756&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16824756&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   58702   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nonstructural protein 5B of hepatitis C virus</p>

    <p>          Lee, JH, Nam, IY, and Myung, H</p>

    <p>          Mol Cells <b>2006</b>.  21(3): 330-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16819294&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16819294&amp;dopt=abstract</a> </p><br />

    <p>35.   58703   DMID-LS-124; PUBMED-DMID-7/31/2006 ; DMID-LSLOAD</p>

    <p><b>          Hexamethylene amiloride blocks E protein ion channels and inhibits coronavirus replication</b> </p>

    <p>          Wilson, L, Gage, P, and Ewart, G</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16815524&amp;dopt=abstract</a> </p><br />

    <p>36.   58704   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral drug resistance</p>

    <p>          Richman, Douglas D</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JKRVW8-2/2/336d25aaddf9598ab309424b37496b03">http://www.sciencedirect.com/science/article/B6T2H-4JKRVW8-2/2/336d25aaddf9598ab309424b37496b03</a> </p><br />

    <p>37.   58705   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of glutamic acid and glutamine peptides possessing a trifluoromethyl ketone group as SARS-CoV 3CL protease inhibitors</p>

    <p>          Sydnes, Magne O, Hayashi, Yoshio, Sharma, Vinay K, Hamada, Takashi, Bacha, Usman, Barrila, Jennifer, Freire, Ernesto, and Kiso, Yoshiaki</p>

    <p>          Tetrahedron <b>2006</b>.  62(36): 8601-8609</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4KDBKY1-1/2/db3afef6f81120f2c3b240407fc20158">http://www.sciencedirect.com/science/article/B6THR-4KDBKY1-1/2/db3afef6f81120f2c3b240407fc20158</a> </p><br />

    <p>38.   58706   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Three-Dimensional Modeling of Cytomegalovirus Dna Polymerase and Preliminary Analysis of Drug Resistance</p>

    <p>          Shi, R. <i>et al.</i></p>

    <p>          Proteins-Structure Function and Bioinformatics <b>2006</b>.  64(2 ): 301-307</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238624300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238624300002</a> </p><br />

    <p>39.   58707   DMID-LS-124; WOS-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Pkr by Rna and Dna Viruses</p>

    <p>          Langland, J. <i>et al.</i></p>

    <p>          Virus Research  <b>2006</b>.  119(1): 100-110</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238302800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238302800010</a> </p><br />
    <br clear="all">

    <p>40.   58708   DMID-LS-124; EMBASE-DMID-7/30/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Coronaviruses and their therapy</p>

    <p>          Haagmans, Bart L and Osterhaus, Albert DME</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K7158K-2/2/acdcc9763a4bf403b96248c3b3c4dae6">http://www.sciencedirect.com/science/article/B6T2H-4K7158K-2/2/acdcc9763a4bf403b96248c3b3c4dae6</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
