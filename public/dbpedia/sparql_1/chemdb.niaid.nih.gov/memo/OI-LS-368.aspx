

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-368.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cDLZ6FaE7nrl7O37rvTg961YLQRoZG+EiVJ+jiKB+5+iTiuXjxD18xx8ZThZFL3qdwPGYjHkFC1fVGIxqbe6FMDuJrIHOE0Z9YfcROPscuSCbU4d8cF7eqq0/wQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BA56179D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-368-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60199   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Novel substrates of Mycobacterium tuberculosis PknH Ser/Thr kinase</p>

    <p>          Zheng, Xingji, Papavinasasundaram, KG, and Av-Gay, Yossef</p>

    <p>          Biochemical and Biophysical Research Communications <b>2007</b>.  355(1): 162-168</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4MY0S1B-8/2/df14a70a676e6e4ba668b9749f25968d">http://www.sciencedirect.com/science/article/B6WBK-4MY0S1B-8/2/df14a70a676e6e4ba668b9749f25968d</a> </p><br />

    <p>2.     60200   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Is planarity of pyridin-2-yl- and pyrazin-2-yl-formamide thiosemicarbazones related to their tuberculostatic activity? X-ray structures of two pyrazine-2-carboxamide-N&#39;-carbonothioyl-hydrazones</p>

    <p>          Olczak, Andrzej, Glowka, Marek L, Golka, Jolanta, Szczesio, Malgorzata, Bojarska, Joanna, Kozlowska, Krystyna, Foks, Henryk, and Orlewska, Czeslawa</p>

    <p>          Journal of Molecular Structure <b>2007</b>.  830(1-3): 171-175</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGS-4KSD8DR-2/2/c62c05c9a781f4fefaaaee07d25c8863">http://www.sciencedirect.com/science/article/B6TGS-4KSD8DR-2/2/c62c05c9a781f4fefaaaee07d25c8863</a> </p><br />

    <p>3.     60201   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p><b>          Synthesis, antimycobacterial activity evaluation, and QSAR studies of chalcone derivatives</b> </p>

    <p>          Sivakumar, PM, Seenivasan, SPrabu, Kumar, Vanaja, and Doble, Mukesh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(6): 1695-1700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MT5JXW-1/2/0bd97303a8d2fe806adb35aaf569e710">http://www.sciencedirect.com/science/article/B6TF9-4MT5JXW-1/2/0bd97303a8d2fe806adb35aaf569e710</a> </p><br />

    <p>4.     60202   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          New thiopyrazolo[3,4-d]pyrimidine derivatives as anti-mycobacterial agents</p>

    <p>          Ballell, Lluis, Field, Robert A, Chung, Gavin AC, and Young, Robert J</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(6): 1736-1740</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MMPNGN-2/2/8c2a9e70aa935d1ebdc05a7256f0a9ea">http://www.sciencedirect.com/science/article/B6TF9-4MMPNGN-2/2/8c2a9e70aa935d1ebdc05a7256f0a9ea</a> </p><br />

    <p>5.     60203   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of galactofuranosyl N,N-dialkyl sulfenamides and sulfonamides as antimycobacterial agents</p>

    <p>          Owen, DJ, Davis, CB, Hartnell, RD, Madge, PD, Thomson, RJ, Chong, AK, Coppel, RL, and Itzstein, MV</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303419&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303419&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60204   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial activity, and QSAR studies of furan-3-carboxamides</p>

    <p>          Zanatta, Nilo, Alves, Sydney H, Coelho, Helena S, Borchhardt, Deise M, Machado, Pablo, Flores, Kelen M, da Silva, Fabio M, Spader, Tatiana B, Santurio, Janio M, Bonacorso, Helio G, and Martins, Marcos AP</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(5): 1947-1958</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MRG08G-2/2/360230b016bb690b34ef23ad89cbcc3f">http://www.sciencedirect.com/science/article/B6TF8-4MRG08G-2/2/360230b016bb690b34ef23ad89cbcc3f</a> </p><br />

    <p>7.     60205   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial evaluation of benzofurobenzopyran analogues</p>

    <p>          Prado, Soizic, Janin, Yves L, Saint-Joanis, Brigitte, Brodin, Priscille, Michel, Sylvie, Koch, Michel, Cole, Stewart T, Tillequin, Francois, and Bost, Pierre-Etienne</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(5): 2177-2186</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MJJH05-2/2/b2c5bdec82889d230a6dd2475456766c">http://www.sciencedirect.com/science/article/B6TF8-4MJJH05-2/2/b2c5bdec82889d230a6dd2475456766c</a> </p><br />

    <p>8.     60206   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Resistance of hepatitis C virus to NS3-4A protease inhibitors: mechanisms of drug resistance induced by R155Q, A156T, D168A and D168V mutations</p>

    <p>          Courcambeck, J, Bouzidi, M, Perbost, R, Jouirou, B, Amrani, N, Cacoub, P, Pepe, G, Sabatier, JM, and Halfon, P</p>

    <p>          Antivir Ther <b>2006</b>.  11(7): 847-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17302247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17302247&amp;dopt=abstract</a> </p><br />

    <p>9.     60207   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Reversal of Human Cytomegalovirus Major Immediate-Early Enhancer/Promoter Silencing in Quiescently Infected Cells via the Cyclic-AMP Signaling Pathway</p>

    <p>          Keller, MJ, Wu, AW, Andrews, JI, McGonagill, PW, Tibesar, EE, and Meier, JL</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17301150&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17301150&amp;dopt=abstract</a> </p><br />

    <p>10.   60208   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          In vitro and ex vivo activity of thioridazine derivatives against Mycobacterium tuberculosis</p>

    <p>          Martins, Marta, Schelz, Zsuzsanna, Martins, Ana, Molnar, Joseph, Hajos, Gyorgy, Riedl, Zsuzsanna, Viveiros, Miguel, Yalcin, Ismail, Aki-Sener, Esin, and Amaral, Leonard</p>

    <p>          International Journal of Antimicrobial Agents <b>2007</b>.  29(3): 338-340</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4MVDVGM-2/2/3c10aab65e020720ade09d02d8ac3875">http://www.sciencedirect.com/science/article/B6T7H-4MVDVGM-2/2/3c10aab65e020720ade09d02d8ac3875</a> </p><br />
    <br clear="all">

    <p>11.   60209   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Antiviral Activity of Triazine Analogues of 1-(S)-[3-Hydroxy-2-(phosphonomethoxy)propyl]cytosine (Cidofovir) and Related Compounds</p>

    <p>          Krecmerova, M, Holy, A, Piskala, A, Masojidkova, M, Andrei, G, Naesens, L, Neyts, J, Balzarini, J, Clercq, ED, and Snoeck, R</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298047&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17298047&amp;dopt=abstract</a> </p><br />

    <p>12.   60210   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Multi Drug-Resistant Tuberculosis Doing Well on Standard Treatment?</p>

    <p>          McNicholas, S, McCarthy, E, Glynn, N, Reddin, D, Corbett-Feeney, G, and Cormican, M</p>

    <p>          Journal of Infection <b>2007</b>.  54(3): 192-193</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WJT-4MVDVBX-9/2/b23859e077ab8679060e2892dee71bb9">http://www.sciencedirect.com/science/article/B6WJT-4MVDVBX-9/2/b23859e077ab8679060e2892dee71bb9</a> </p><br />

    <p>13.   60211   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p><b>          EDP-420, a Bicyclolide (Bridged Bicyclic Macrolide), is Active against Mycobacterium avium</b> </p>

    <p>          Bermudez, LE, Motamedi, N, Chee, C, Baimukanova, G, Kolonoski, P, Inderlied, C, Aralar, P, Wang, G, Phan, LT, and Young, LS</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296742&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296742&amp;dopt=abstract</a> </p><br />

    <p>14.   60212   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Modification of the active site of Mycobacterium tuberculosis KatG after disruption of the Met-Tyr-Trp cross-linked adduct</p>

    <p>          Kapetanaki, Sofia M, Zhao, Xiangbo, Yu, Shengwei, Magliozzo, Richard S, and Schelvis, Johannes PM</p>

    <p>          Journal of Inorganic Biochemistry <b>2007</b>.  101(3): 422-433</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGG-4MC81M4-2/2/267eb3d4406240809eb04bc3de0dc639">http://www.sciencedirect.com/science/article/B6TGG-4MC81M4-2/2/267eb3d4406240809eb04bc3de0dc639</a> </p><br />

    <p>15.   60213   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Effects of picolinic acid on the antimicrobial functions of host macrophages against Mycobacterium avium complex</p>

    <p>          Tomioka, H, Shimizu, T, and Tatano, Y</p>

    <p>          Int J Antimicrob Agents <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296287&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17296287&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   60214   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Drug resistance in tuberculosis--a reinfection model</p>

    <p>          Rodrigues, Paula, Gomes, MGabriela M, and Rebelo, Carlota</p>

    <p>          Theoretical Population Biology <b>2007</b>.  71(2): 196-212</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXD-4M6HY79-1/2/6fbdc985b7a779f9a8188d91e5c3cbcc">http://www.sciencedirect.com/science/article/B6WXD-4M6HY79-1/2/6fbdc985b7a779f9a8188d91e5c3cbcc</a> </p><br />

    <p>17.   60215   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          The Three-dimensional Structure of N-Succinyldiaminopimelate Aminotransferase from Mycobacterium tuberculosis</p>

    <p>          Weyand, S, Kefala, G, and Weiss, MS</p>

    <p>          J Mol Biol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17292400&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17292400&amp;dopt=abstract</a> </p><br />

    <p>18.   60216   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          The Solution Structure of Antigen MPT64 from Mycobacterium tuberculosis Defines a New Family of Beta-Grasp Proteins</p>

    <p>          Wang, Zhonghua, Potter, Belinda M, Gray, Amanda M, Sacksteder, Katherine A, Geisbrecht, Brian V, and Laity, John H</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  366(2): 375-381</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4MBJK4T-3/2/f0d4dee2f8d3eb2ff1d514a99e8b74e5">http://www.sciencedirect.com/science/article/B6WK7-4MBJK4T-3/2/f0d4dee2f8d3eb2ff1d514a99e8b74e5</a> </p><br />

    <p>19.   60217   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Anti-HCV Bioactivity of Pseudoguaianolides from Parthenium hispitum</p>

    <p>          Hu, JF, Patel, R, Li, B, Garo, E, Hough, GW, Goering, MG, Yoo, HD, O&#39;neil-Johnson, M, and Eldridge, GR</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17291045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17291045&amp;dopt=abstract</a> </p><br />

    <p>20.   60218   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Hybrid molecules of estrone: new compounds with potential antibacterial, antifungal and antiproliferative activities</p>

    <p>          Adamec, J, Beckert, R, Wei[ss], D, Klimesova, V, Waisser, K, Mollmann, U, Kaustova, J, and Buchta, V</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4N206XD-9/2/f0132352ebe4324a0e7de5908f59fb14">http://www.sciencedirect.com/science/article/B6TF8-4N206XD-9/2/f0132352ebe4324a0e7de5908f59fb14</a> </p><br />
    <br clear="all">

    <p>21.   60219   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Functional and therapeutic analysis of hepatitis C virus NS3/4A protease control of antiviral immune defense</p>

    <p>          Johnson, CL, Owen, DM, and Gale, M Jr</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289677&amp;dopt=abstract</a> </p><br />

    <p>22.   60220   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis of new sugar derivatives from Stachys sieboldi Miq and antibacterial evaluation against Mycobacterium tuberculosis, Mycobacterium avium and Staphylococcus aureus</p>

    <p>          Chiba, Taku, Takii, Takemasa, Nishimura, Kenji, Yamamoto, Yoshifumi, Morikawa, Hiroko, Abe, Chiyoji, and Onozaki, Kikuo</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N206XH-4/2/6179959cfb90770f32eeb9819701c3d2">http://www.sciencedirect.com/science/article/B6TF9-4N206XH-4/2/6179959cfb90770f32eeb9819701c3d2</a> </p><br />

    <p>23.   60221   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some novel phenyl and benzimidazole substituted benzyl ethers</p>

    <p>          Guven, OO, Erdogan, T, Goker, H, and Yildiz, S</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289382&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17289382&amp;dopt=abstract</a> </p><br />

    <p>24.   60222   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Inhibition of mycobacterial arylamine N-acetyltransferase contributes to anti-mycobacterial activity of Warburgia salutaris</p>

    <p>          Madikane, Vukani Eliya, Bhakta, Sanjib, Russell, Angela J, Campbell, William E, Claridge, Timothy DW, Elisha, BGay, Davies, Stephen G, Smith, Peter, and Sim, Edith</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript: 883</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4N1JRN3-3/2/914351021409926ffd285644114bcc5f">http://www.sciencedirect.com/science/article/B6TF8-4N1JRN3-3/2/914351021409926ffd285644114bcc5f</a> </p><br />

    <p>25.   60223   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Anti-TB activity of Evodia elleryana bark extract</p>

    <p>          Barrows, Louis R, Powan, Emma, Pond, Christopher D, and Matainaho, Teatulohi</p>

    <p>          Fitoterapia <b>2007</b>.  In Press, Accepted Manuscript: 88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VSC-4MYVG5N-7/2/f5cb59ab80efa804414da579d8c90a45">http://www.sciencedirect.com/science/article/B6VSC-4MYVG5N-7/2/f5cb59ab80efa804414da579d8c90a45</a> </p><br />
    <br clear="all">

    <p>26.   60224   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          A new modification of anti-tubercular active molecules</p>

    <p>          Imramovsky, Ales, Polanc, Slovenko, Vinsova, Jarmila, Kocevar, Marijan, Jampilek, Josef, Reckova, Zuzana, and Kaustova, Jarmila</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4MYMFS0-2/2/dbc9297821fee6ef8b3a1bbbffda91f1">http://www.sciencedirect.com/science/article/B6TF8-4MYMFS0-2/2/dbc9297821fee6ef8b3a1bbbffda91f1</a> </p><br />

    <p>27.   60225   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Rationalization of physicochemical characters of oxazolyl thiosemicarbazones analogs towards multi-drug resistant tuberculosis: A QSAR approach</p>

    <p>          Gupta, Revathi A, Gupta, Arun K, Soni, Love K, and Kaskhedikar, SG</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Accepted Manuscript:  392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MXBF77-4/2/cde45497a94219463ad30b0795638600">http://www.sciencedirect.com/science/article/B6VKY-4MXBF77-4/2/cde45497a94219463ad30b0795638600</a> </p><br />

    <p>28.   60226   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          N-Hydroxythiosemicarbazones: Synthesis and in vitro antitubercular activity</p>

    <p>          Sriram, D, Yogeeswari, P, Dhakla, P, Senthilkumar, P, and Banerjee, D</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276683&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276683&amp;dopt=abstract</a> </p><br />

    <p>29.   60227   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Indian medicinal plants as a source of antimycobacterial agents</p>

    <p>          Gautam, R, Saklani, A, and Jachak, SM</p>

    <p>          J Ethnopharmacol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276637&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17276637&amp;dopt=abstract</a> </p><br />

    <p>30.   60228   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Inhibition of leukotriene biosynthesis abrogates the host control of Mycobacterium tuberculosis</p>

    <p>          Peres, Camila M, de Paula, Lucia, Medeiros, Alexandra I, Sorgi, Carlos A, Soares, Edson G, Carlos, Daniela, Peters-Golden, Marc, Silva, Celio L, and Faccioli, Lucia H</p>

    <p>          Microbes and Infection <b>2007</b>.  In Press, Uncorrected Proof: 392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4MW323M-2/2/0f294ef6d1534f14b098900c991176d9">http://www.sciencedirect.com/science/article/B6VPN-4MW323M-2/2/0f294ef6d1534f14b098900c991176d9</a> </p><br />
    <br clear="all">

    <p>31.   60229   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Crystal structure of N-acetyl-[gamma]-glutamyl-phosphate reductase from Mycobacterium tuberculosis in complex with NADP+</p>

    <p>          Cherney, Leonid T, Cherney, Maia M, Garen, Craig R, Niu, Chunying, Moradian, Fatemeh, and James, Michael NG</p>

    <p>          Journal of Molecular Biology <b>2007</b>.  In Press, Accepted Manuscript: 392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4MVVSW8-5/2/89cd55b47048976b080aa19b0258d59b">http://www.sciencedirect.com/science/article/B6WK7-4MVVSW8-5/2/89cd55b47048976b080aa19b0258d59b</a> </p><br />

    <p>32.   60230   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Inositol-1-phosphate synthetase mRNA as a new target for antisense inhibition of Mycobacterium tuberculosis</p>

    <p>          Li, Yuanyuan, Chen, Zhifei, Li, Xiaobo, Zhang, Hongling, Huang, Qiang, Zhang, Ying, and Xu, Shunqing</p>

    <p>          Journal of Biotechnology <b>2007</b>.  In Press, Corrected Proof: 447</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T3C-4MSPV0Y-1/2/554f7bbe4b479952af5efab28f85c806">http://www.sciencedirect.com/science/article/B6T3C-4MSPV0Y-1/2/554f7bbe4b479952af5efab28f85c806</a> </p><br />

    <p>33.   60231   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Anti-mycobacterial activity of a bis-sulfonamide</p>

    <p>          Wilkinson, BL, Bornaghi, LF, Wright, AD, Houston, TA, and Poulsen, SA</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.  17(5): 1355-1357</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258454&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258454&amp;dopt=abstract</a> </p><br />

    <p>34.   60232   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Binding of 5-phospho-D-arabinonohydroxamate and 5-phospho-D-arabinonate inhibitors to zinc phosphomannose isomerase from Candida albicans studied by polarizable molecular mechanics and quantum mechanics</p>

    <p>          Roux, C, Gresh, N, Perera, LE, Piquemal, JP, and Salmon, L</p>

    <p>          J Comput Chem <b>2007</b>.  28(5): 938-957</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253648&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17253648&amp;dopt=abstract</a> </p><br />

    <p>35.   60233   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Clubbed triazoles: A novel approach to antitubercular drugs</p>

    <p>          Shiradkar, M, Suresh, Kumar GV, Dasari, V, Tatikonda, S, Akula, KC, and Shah, R</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17239490&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>36.   60234   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis, biological evaluation and molecular modeling studies of N6-benzyladenosine analogues as potential anti-toxoplasma agents</p>

    <p>          Kim, Young Ah, Sharon, Ashoke, Chu, Chung K, Rais, Reem H, Al Safarjalani, Omar N, Naguib, Fardos NM, and el Kouni, Mahmoud H</p>

    <p>          Biochemical Pharmacology <b>2007</b>.  In Press, Corrected Proof: 44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4MW31YX-1/2/bc1da1e2278836ce0b64f4fc14d8db80">http://www.sciencedirect.com/science/article/B6T4P-4MW31YX-1/2/bc1da1e2278836ce0b64f4fc14d8db80</a> </p><br />

    <p>37.   60235   OI-LS-368; PUBMED-OI-2/20/2007</p>

    <p class="memofmt1-2">          Antimycobacterial triterpenoids from Lantana hispida (Verbenaceae)</p>

    <p>          Jimenez-Arellanes, A, Meckes, M, Torres, J, and Luna-Herrera, J</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17236730&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17236730&amp;dopt=abstract</a> </p><br />

    <p>38.   60236   OI-LS-368; WOS-OI-2/12/2007</p>

    <p class="memofmt1-2">          Synthesis of the isonicotinoylnicotinamide scaffolds of the naturally occurring isoniazid-NAD(P) adducts</p>

    <p>          Delaine, T, Bernardes-Genisson, V, Meunier, B, and Bernadou, J</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2007</b>.  72(2): 675-678, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243416300053">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243416300053</a> </p><br />

    <p>39.   60237   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          N/C-4 substituted azetidin-2-ones: Synthesis and preliminary evaluation as new class of antimicrobial agents</p>

    <p>          Halve, Anand K, Bhadauria, Deepti, and dubey, Rakesh</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(2): 341-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M6HY7N-4/2/423cdedaaf1d771c97e701bde8c8b598">http://www.sciencedirect.com/science/article/B6TF9-4M6HY7N-4/2/423cdedaaf1d771c97e701bde8c8b598</a> </p><br />

    <p>40.   60238   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          A novel dodecapeptide from a combinatorial synthetic library exhibits potent antifungal activity and synergy with standard antimycotic agents</p>

    <p>          Duggineni, S, Srivastava, G, Kundu, B, Kumar, M, Chaturvedi, AK, and Shukla, PK</p>

    <p>          International Journal of Antimicrobial Agents <b>2007</b>.  29(1): 73-78</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4MH8BBP-1/2/82f07bb10493b07ca560325e7f9ecf97">http://www.sciencedirect.com/science/article/B6T7H-4MH8BBP-1/2/82f07bb10493b07ca560325e7f9ecf97</a> </p><br />
    <br clear="all">

    <p>41.   60239   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of isosteres of N-methyl indolo[3,2-b]-quinoline (cryptolepine) as new antiinfective agents</p>

    <p>          Zhu, Xue Y, Mardenborough, Leroy G, Li, Shouming, Khan, Abdul, Zhang, Wang, Fan, Pincheng, Jacob, Melissa, Khan, Shabana, Walker, Larry, and Ablordeppey, Seth Y</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 686-695</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M7YK0B-8/2/440b10ae7b97a5153c354726a5498ef0">http://www.sciencedirect.com/science/article/B6TF8-4M7YK0B-8/2/440b10ae7b97a5153c354726a5498ef0</a> </p><br />

    <p>42.   60240   OI-LS-368; WOS-OI-2/12/2007</p>

    <p class="memofmt1-2">          Recent developments in antifungal drug discovery</p>

    <p>          Di Santo, R</p>

    <p>          ANNUAL REPORTS IN MEDICINAL CHEMISTRY, VOL 41 <b>2006</b>.  41: 299-315, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243457700020</a> </p><br />

    <p>43.   60241   OI-LS-368; WOS-OI-2/12/2007</p>

    <p class="memofmt1-2">          Design, synthesis and in vitro antifungal activities of non-azole lead compound based on lanosterol 14 alpha-demethylase of fungi</p>

    <p>          Zhu, J, Lu, JG, Zhou, YJ, Li, YW, Chen, J, and Zheng, CH</p>

    <p>          ACTA CHIMICA SINICA <b>2007</b>.  65(1): 37-42, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243606300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243606300007</a> </p><br />

    <p>44.   60242   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Selection of a phage-displayed peptide recognized by monoclonal antibody directed blocking the site of hepatitis C virus E2 for human CD81</p>

    <p>          Cao, J, Liao, XL, Wu, SM, Zhao, P, Zhao, LJ, Wu, WB, and Qi, ZT</p>

    <p>          Journal of Microbiological Methods <b>2007</b>.  68(3): 601-604</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T30-4MKTXMY-2/2/f39cfc2f3bf4911ada096854de49c474">http://www.sciencedirect.com/science/article/B6T30-4MKTXMY-2/2/f39cfc2f3bf4911ada096854de49c474</a> </p><br />

    <p>45.   60243   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          6-Hydrazinopurine 2&#39;-methyl ribonucleosides and their 5&#39;-monophosphate prodrugs as potent hepatitis C virus inhibitors</p>

    <p>          Gunic, Esmir, Chow, Suetying, Rong, Frank, Ramasamy, Kanda, Raney, Anneke, Li, David, Huang, Jingfan, Hamatake, Robert, Hong, Zhi, and Girardet, Jean-Luc</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 604</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N25VRV-2/2/69645b571ec1621c2a79383017f3ce30">http://www.sciencedirect.com/science/article/B6TF9-4N25VRV-2/2/69645b571ec1621c2a79383017f3ce30</a> </p><br />

    <p>46.   60244   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Cyclic Monophosphate Prodrugs of Base-Modified 2&#39;-C-Methyl Ribonucleosides as Potent Inhibitors of Hepatitis C Virus RNA Replication</p>

    <p>          Gunic, Esmir, Girardet, Jean-Luc, Ramasamy, Kanda, Stoisavljevic-Petkov, Vesna, Chow, Suetying, Yeh, Li-Tain, Hamatake, Robert, Raney, Anneke, and Hong, Zhi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 604</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N25VRV-3/2/479c055b93effcdac8997ce557a7f037">http://www.sciencedirect.com/science/article/B6TF9-4N25VRV-3/2/479c055b93effcdac8997ce557a7f037</a> </p><br />

    <p>47.   60245   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          Analysis of the domain interactions between the protease and helicase of NS3 in dengue and hepatitis C virus</p>

    <p>          Rosales-Leon, L, Ortega-Lule, G, and Ruiz-Ordaz, B</p>

    <p>          Journal of Molecular Graphics and Modelling <b>2007</b>.  25(5): 585-594</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGP-4JXR90P-1/2/2de30af901349a221130ac68756f732b">http://www.sciencedirect.com/science/article/B6TGP-4JXR90P-1/2/2de30af901349a221130ac68756f732b</a> </p><br />

    <p>48.   60246   OI-LS-368; EMBASE-OI-2/20/2007</p>

    <p class="memofmt1-2">          SAR Studies on a Novel Series of Human Cytomegalovirus Primase Inhibitors</p>

    <p>          Chen, X, Adrian, J, Cushing, T, DiMaio, H, Liang, L, Mayorga, V, Miao, S, Peterson, MG, Powers, JP, Spector, F, Stein, C, Wright, M, Xu, D, Ye, Q, and Jaen, J</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 131</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4N0X5MR-1/2/ceb259dde478852e29a66950f556cc71">http://www.sciencedirect.com/science/article/B6TF9-4N0X5MR-1/2/ceb259dde478852e29a66950f556cc71</a> </p><br />

    <p>49.   60247   OI-LS-368; WOS-OI-2/12/2007</p>

    <p class="memofmt1-2">          Mathematical modeling of chemotherapy of human TB infection</p>

    <p>          Magombedze, G, Garira, W, and Mwenje, E</p>

    <p>          JOURNAL OF BIOLOGICAL SYSTEMS <b>2006</b>.  14(4): 509-553, 45</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243498800002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243498800002</a> </p><br />

    <p>50.   60248   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;,3&#39;-dihydrosolanesyl analogues of beta-D-arabinofuranosyl-1-monophosphoryldecaprenoI with promising antimycobacterial activity</p>

    <p>          Bosco, M, Bisseret, P, Constant, P, and Eustache, J</p>

    <p>          TETRAHEDRON LETTERS <b>2007</b>.  48(1): 153-157, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243630300033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243630300033</a> </p><br />

    <p>51.   60249   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Design and in vitro evaluation of five inhibitors of Mycobacterium tuberculosis</p>

    <p>          Bartzatt, R, Cirillo, SLG, and Cirillo, JD</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2007</b>.  4(2): 137-143, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243855800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243855800009</a> </p><br />

    <p>52.   60250   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Synthesis of indolizine derivatives with selective antibacterial activity against Mycobacterium tuberculosis</p>

    <p>          Gundersen, LL, Charnock, C, Negussie, AH, Rise, F, and Teklu, S</p>

    <p>          EUROPEAN JOURNAL OF PHARMACEUTICAL SCIENCES <b>2007</b>.  30(1): 26-35, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243610700005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243610700005</a> </p><br />

    <p>53.   60251   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Glucosidase inhibitors as antiviral agents for hepatitis B and C</p>

    <p>          Durantel, D, Alotte, C, and Zoulim, F</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2007</b>.  8(2): 125-129, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243838800004</a> </p><br />

    <p>54.   60252   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Investigational drug for hepatitis C successful in large trial</p>

    <p>          Anon</p>

    <p>          CLINICAL INFECTIOUS DISEASES <b>2007</b>.  44(4): IV-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243597700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243597700003</a> </p><br />

    <p>55.   60253   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Coley halts work on hepatitis drug</p>

    <p>          Anon</p>

    <p>          CHEMICAL &amp; ENGINEERING NEWS <b>2007</b>.  85(5): 25-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243809900026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243809900026</a> </p><br />

    <p>56.   60254   OI-LS-368; WOS-OI-2/18/2007</p>

    <p class="memofmt1-2">          Synthesis and biological activity studies of di- and mono-halogenofluoro benzenes</p>

    <p>          Logoglu, E, Katircioglu, H, Tilki, T, and Oktemer, A</p>

    <p>          ASIAN JOURNAL OF CHEMISTRY <b>2007</b>.  19(3): 2029-2035, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243616900055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243616900055</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
