

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-11-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lT/o+AlNN1hFvoAY4xLG+k595O6Hmae7mtkyQnl24oXlkgX9esGfR5tvRcZIz18Be5z6hmoAPD5mlhhZlnX6gqslUcUYzQYF0e5ApK1UOEP0kvnXiTc7VAfRmO4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="64E4BD55" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">November 12 - November 23, 2010</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21075607">Fungicidal activity of human lactoferrin-derived peptides based on the antimicrobial alphabeta region.</a> Kondori, N., L. Baltzer, G.T. Dolphin, and I. Mattsby-Baltzer. International Journal of Antimicrobial Agents, 2010; <b>[Epub ahead of print]</b>. PMID[21075607].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20943406">Synthesis of new antifungal peptides selective against Cryptococcus neoformans.</a> Grimaldi, M., M. De Rosa, S. Di Marino, M. Scrima, B. Posteraro, M. Sanguinetti, G. Fadda, A. Soriente, and A.M. D&#39;Ursi. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 18(22): p. 7985-90; PMID[20943406].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20932752">Synthesis and antifungal activity of a novel series of 13-(4-isopropylbenzyl)berberine derivatives.</a> Park, K.D., S.J. Cho, J.S. Moon, and S.U. Kim. Bioorganic &amp;  Medicinal Chemistry Letters, 2010. 20(22): p. 6551-4; PMID[20932752].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20851600">Synthesis and antifungal activity of benzofuran-5-ols.</a> Ryu, C.K., A.L. Song, J.Y. Lee, J.A. Hong, J.H. Yoon, and A. Kim. Bioorganic &amp;  Medicinal Chemistry Letters, 2010. 20(22): p. 6777-80; PMID[20851600].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20934344">Identification of a novel pyrrole derivative endowed with antimycobacterial activity and protection index comparable to that of the current antitubercular drugs streptomycin and rifampin.</a> Biava, M., G.C. Porretta, G. Poce, C. Battilocchio, S. Alfonso, A. De Logu, N. Serra, F. Manetti, and M. Botta. Bioorganic and Medicinal Chemistry, 2010. 18(22): p. 8076-84; PMID[20934344].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21082329">Computer-assisted combinatorial design of bicyclic thymidine analogs as inhibitors of Mycobacterium tuberculosis thymidine monophosphate kinase.</a> Frecer, V., P. Seneci, and S. Miertus. Journal of Computer-Aided Molecular Design, 2010; <b>[Epub ahead of print]</b>. PMID[21082329].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21073150">Novel Small Molecule Inhibitors of MDR Mycobacterium tuberculosis by NMR Fragment Screening of Antigen 85C.</a> Scheich, C., V. Puetter, and M. Schade. Journal of Medicinal Chemistry, 2010; <b>[Epub ahead of print]</b>. PMID[21073150].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21081761">Identification of novel diphenyl urea inhibitors of Mt-Guab2 active against Mycobacterium tuberculosis.</a> Usha, V., S.S. Gurcha, A. Lovering, A.J. Lloyd, A. Papaemmanouil, R.C. Reynolds, and G.S. Besra. Microbiology, 2010; <b>[Epub ahead of print]</b>. PMID[21081761]. [Pubmed]. OI_1112-112310.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283739000002">Evaluation of the Mycobacterium smegmatis and BCG models for the discovery of Mycobacterium tuberculosis inhibitors.</a> Altaf, M., C.H. Miller, D.S. Bellows, and R. O&#39;Toole. Tuberculosis, 2010. 90(6): p. 333-337; ISI[000283739000002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1112-112310.</p>

    <p class="NoSpacing"><br>
    10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283739000005">Leads for antitubercular compounds from kinase inhibitor library screens.</a> Magnet, S., R.C. Hartkoorn, R. Szekely, J. Pato, J.A. Triccas, P. Schneider, C. Szantai-Kis, L. Orfi, M. Chambon, D. Banfi, M. Bueno, G. Turcatti, G. Keri, and S.T. Cole. Tuberculosis, 2010. 90(6): p. 354-360; ISI[000283739000005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283801400009">Pyrazole derivatives from azines of substituted phenacyl aryl/cyclohexyl sulfides and their antimycobacterial activity.</a> Manikannan, R., R. Venkatesan, S. Muthusubramanian, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2010. 20(23): p. 6920-6924; ISI[000283801400009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283714900001">Synthesis, Spectroscopic and Pharmacological Studies of Bivalent Copper, Zinc and Mercury Complexes of Thiourea.</a> Parmar, S., Y. Kumar, and A. Mittal. South African Journal of Chemistry-Suid-Afrikaanse Tydskrif Vir Chemie, 2010. 63: p. 123-129; ISI[000283714900001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1112-112310.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283618900004">In vitro inhibition of toxigenic filamentous fungi.</a> Ramos, D.M.B., C.F. Silva, L.R. Batista, and R.F. Schwan. Acta Scientiarum Agronomy 2010. 32(3): p. 397-402; ISI[000283618900004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_1112-112310.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
