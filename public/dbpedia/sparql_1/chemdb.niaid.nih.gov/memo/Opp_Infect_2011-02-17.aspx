

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-02-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d94PbmsapXnffTsahVaARrx+YN4aKQ4TjUil8IaaAEN+i+iTLtcOz4QsdJYXKxAxTKx4UMiswojt+/GfmsMnMXSl7sjvOeIwcUUHPM7nlmZuuZQXeIHZ0tZ7wrE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="219170CF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">February 4 - February 17, 2011</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21321363">Chemical composition and antifungal activity of the essential oils of Lavandula viridis L&#39;Her.</a> Zuzarte, M., M.J. Goncalves, C. Cavaleiro, J. Canhoto, L. Vale-Silva, M.J. Silva, E. Pinto, and L. Salgueiro. Journal of Medical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21321363].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21312033">Synthesis, conformational analysis and biological properties of a dicarba derivative of the antimicrobial peptide, brevinin-1BYa.</a> Hossain, M.A., L. Guilhaudis, A. Sonnevend, S. Attoub, B.J. van Lierop, A.J. Robinson, J.D. Wade, and J.M. Conlon. European Biophysics Journal, 2011. <b>[Epub ahead of print]</b>; PMID[21312033].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21311886">Environmental Burkholderia cepacia Strain Cs5 Acting by Two Analogous Alkyl-Quinolones and a Didecyl-Phthalate Against a Broad Spectrum of Phytopathogens Fungi.</a> Kilani-Feki, O., G. Culioli, A. Ortalo-Magne, N. Zouari, Y. Blache, and S. Jaoua. Current Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21311886].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21289395">Antifungal activity of ZnO nanoparticles-the role of ROS mediated cell injury.</a> Lipovsky, A., Y. Nitzan, A. Gedanken, and R. Lubart. Nanotechnology, 2011. 22(10): p. 105101; PMID[21289395].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21291939">Antimicrobial decapeptide KSL-W attenuates Candida albicans virulence by modulating its effects on Toll-like receptor, human beta-defensin, and cytokine expression by engineered human oral mucosa.</a> Semlali, A., K.P. Leung, S. Curt, and M. Rouabhia. Peptides, 2011. <b>[Epub ahead of print]</b>; PMID[21291939].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21291481">Inhibitory Effects of Gossypol-Related Compounds on Growth of Aspergillus flavus.</a> Mellon, J.E., C.A. Zelaya, and M.K. Dowd. Letters in Applied Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21291481].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21294705">Properties and Potency of Small Molecule Agents for Treatment of Mycobacterium Tuberculosis Infections of the Central Nervous System.</a> Bartzatt, R. Central Nervous System Agents in Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21294705].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21309577">Design, Synthesis, and Biological Evaluation of New Cinnamic Derivatives as Antituberculosis Agents.</a> De, P., G. Koumba Yoya, P. Constant, F. Bedos-Belval, H. Duran, N. Saffon, M. Daffe, and M. Baltas. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21309577].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21251821">Analysis of beta-amino alcohols as inhibitors of the potential anti-tubercular target N-acetyltransferase.</a> Fullam, E., A. Abuhammad, D.L. Wilson, M.C. Anderton, S.G. Davies, A.J. Russell, and E. Sim. Bioorganic and Medicinal Chemistry Letters, 2011. 21(4): p. 1185-1190; PMID[21251821].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21237641">Synthesis and antitubercular evaluation of novel substituted aryl and thiophenyl tethered dihydro-6H-quinolin-5-ones.</a> Kantevari, S., S.R. Patpi, B. Sridhar, P. Yogeeswari, and D. Sriram. Bioorganic and Medicinal Chemistry Letters, 2011. 21(4): p. 1214-1217; PMID[21237641].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.    </p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21305448">Cytotoxic and Antimicrobial Activities of Aporphine Alkaloids Isolated from Stephania venosa (Blume) Spreng.</a> Makarasen, A., W. Sirithana, S. Mogkhuntod, N. Khunnawutmanotham, N. Chimnoi, and S. Techasakul. Planta Medica, 2011. <b>[Epub ahead of print]</b>; PMID[21305448].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21320779">Microwave assisted one-pot synthesis of highly potent novel isoniazid analogues.</a> Manjashetty, T.H., P. Yogeeswari, and D. Sriram. Bioorganic and Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21320779].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21094237">Antituberculosis potential of some ethnobotanically selected Malaysian plants.</a> Mohamad, S., N.M. Zin, H.A. Wahab, P. Ibrahim, S.F. Sulaiman, A.S. Zahariluddin, and S.S. Noor. Journal of Ethnopharmacology, 2011. 133(3): p. 1021-1026; PMID[21094237].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21273083">Synthesis and antimycobacterial properties of N-substituted 6-amino-5-cyanopyrazine-2-carboxamides.</a> Zitko, J., M. Dolezal, M. Svobodova, M. Vejsova, J. Kunes, R. Kucera, and P. Jilek. Bioorganic and Medicinal Chemistry, 2011. 19(4): p. 1471-1476; PMID[21273083].</p>

    <p class="plaintext"><b>[Pubmed]</b>. HIV_0204-021711.</p> 

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285847700019">Antimicrobial properties of 4-Carboxyl-2, 6-dinitrophenylazohydroxynaphthalenes.</a> Adegoke, O.A., A.O. Ogunleye, O.T. Lawal, O.S. Idowu, and M.A. Adeniyi-Akee. African Journal of Microbiology Research, 2010. 4(22): p. 2444-2450; ISI[000285847700019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285692800002">Isolation of 3-(4-hydroxyphenyl) Methylpropenoate and Bioactivity Evaluation of Gomphrena Celosioides Extracts.</a> Dosumu, O.O., P.A. Idowu, P.A. Onocha, and O. Ekundayo. Excli Journal, 2010. 9: p. 173-180; ISI[000285692800002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285952500027">Essential oil composition and antimicrobial activity of Sphallerocarpus gracilis seeds against selected food-related bacteria.</a> Gao, C.Y., C.R. Tian, Y.H. Lu, J.G. Xu, J.Y. Luo, and X.P. Guo.  Food Control, 2011. 22(3-4): p. 517-522; ISI[000285952500027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285846200007">Effect of fertilizer treatment on the antimicrobial activity of the leaves of Ocimum gratissimum (L.) and Gongronema latifolium (Benth).</a> Osuagwu, G.G.E. and H.O. Edeoga. African Journal of Biotechnology ISI[000285846200007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285677900008">Antifungal and Antibacterial Activities of Four Malaysian Sponge Species (Petrosiidae).</a> Qaralleh, H., S. Idid, S. Saad, D. Susanti, M. Taher, and K. Khleifat. Journal De Mycologie Medicale, 2010. 20(4): p. 315-320; ISI[000285677900008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285677900005">Antifungal and antifeedant activities of extracellular product of Streptomyces spp. ERI-04 isolated from Western Ghats of Tamil Nadu.</a> Valanarasu, M., P. Kannan, S. Ezhilvendan, G. Ganesan, S. Ignacimuthu, and P. Agastian. Journal De Mycologie Medicale, 2010. 20(4): p. 290-297; ISI[000285677900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0204-021711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
