

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-03-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="A/ZxGNCDGyvNlEuBjtejgFvbxYKCap8xwhSCrrtR3WwRwCEFLV2XNK9lK/3m4Ks60/gfIk3gKhqabeLUkjXeB4We6lqNcqzhn8Ol5y53QY501qPLq/TG9Rk5ipw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="702AB81A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  March 4, 2011- March 17, 2011</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21323359">HIV-1 IN Strand Transfer Chelating Inhibitors: A Focus on Metal Binding.</a> Bacchi, A., M. Carcelli, C. Compari, E. Fisicaro, N. Pala, G. Rispoli, D. Rogolino, T.W. Sanchez, M. Sechi, and N. Neamati. Molecular Pharmacology, 2011.  <b>[Epub ahead of print]</b>; PMID[21323359].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21334901">Antiviral activity of seed extract from Citrus bergamia towards human retroviruses.</a> Balestrieri, E., F. Pizzimenti, A. Ferlazzo, S.V. Giofre, D. Iannazzo, A. Piperno, R. Romeo, M.A. Chiacchio, A. Mastino, and B. Macchi. Bioorganic &amp; Medicinal Chemistry, 2011. 19(6): p. 2084-2089; PMID[21334901].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21366258">Magnesium Chelating 2-Hydroxyisoquinoline-1,3(2H,4H)-diones, as Inhibitors of HIV-1 Integrase and/or the HIV-1 Reverse Transcriptase Ribonuclease H Domain: Discovery of a Novel Selective Inhibitor of the Ribonuclease H Function.</a> Billamboz, M., F. Bailly, C. Lion, N. Touati, H. Vezin, C. Calmels, M.L. Andreola, F. Christ, Z. Debyser, and P. Cotelle. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21366258].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/11430013">A Novel Anti-HIV Macrocyclic Peptide from Palicourea c ondensata.</a> Bokesch, H.R., L.K. Pannell, P.K. Cochran, R.C. Sowder II, T.C. McKee, and M.R. Boyd. Journal of Natural Products, 2001. 64(2): p. 249-250; PMID[11430013].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18057750">Synthesis and HIV-1 integrase inhibition of novel bis-or tetra-coumarin analogues.</a> Chiang, C.C., J.F. Mouscadet, H.J. Tsai, C.T. Liu, and L.Y. Hsu. Chemical &amp; Pharmaceutical Bulletin, 2007. 55(12): p. 1740-1743; PMID[18057750].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21370477">From beta-Amino-gamma-sultone to Unusual Bicyclic Pyridine and Pyrazine Heterocyclic Systems: Synthesis and Cytostatic and Antiviral Activities.</a> de Castro, S., O. Familiar, G. Andrei, R. Snoeck, J. Balzarini, M.J. Camarasa, and S. Velazquez. ChemMedChem, 2011.  <b>[Epub ahead of print]</b>; PMID[21370477].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21389095">Agonist-induced internalization of CCR5 as a mechanism to inhibit HIV replication.</a> Ferain, T., H.R. Hoveyda, F. Ooms, D. Schols, J. Bernard, and G. Fraser. Journal of Pharmacology and Experimental Therapeutics, 2011.  <b>[Epub ahead of print]</b>; PMID[21389095].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21377446">Study of the inhibition capacity of an 18-mer peptide domain of GBV-C virus on gp41-FP HIV-1 activity.</a> Haro, I., M.J. Gomara, R. Galatola, O. Domenech, J. Prat, V. Girona, and M.A. Busquets. Biochimica et Biophysica Acta, 2011.  <b>[Epub ahead of print]</b>; PMID[21377446].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21366296">Indolylarylsulfones as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors: New Cyclic Substituents at Indole-2-carboxamide.</a> La Regina, G., A. Coluccia, A. Brancale, F. Piscitelli, V. Gatti, G. Maga, A. Samuele, C. Pannecouque, D. Schols, J. Balzarini, E. Novellino, and R. Silvestri. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21366296].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21381981">S/GSK1349572, a new integrase inhibitor for the treatment of HIV: promises and challenges.</a> Lenz, J.C. and J.K. Rockstroh. Expert Opinion on Investigational Drugs, 2011. 20(4): p. 537-548; PMID[21381981].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21316963">Novel potent pyrimido[4,5-c], Pubmed, HIV_0304-031811quinoline inhibitors of protein kinase CK2: SAR and preliminary assessment of their analgesic and anti-viral properties.</a> Pierre, F., S.E. O&#39;Brien, M. Haddach, P. Bourbon, M.K. Schwaebe, E. Stefan, L. Darjania, R. Stansfield, C. Ho, A. Siddiqui-Jain, N. Streiner, W.G. Rice, K. Anderes, and D.M. Ryckman. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(6): p. 1687-1691; PMID[21316963].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21391685">RNA-Binding and Viral Reverse Transcriptase Inhibitory Activity of a Novel Cationic Diamino Acid-Based Peptide.</a> Roviello, G.N., S. Di Gaetano, D. Capasso, S. Franco, C. Crescenzo, E.M. Bucci, and C. Pedone. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21391685].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21365365">Anti-HIV activity of Indian medicinal plants.</a> Sabde, S., H.S. Bodiwala, A. Karmase, P.J. Deshpande, A. Kaur, N. Ahmed, S.K. Chauthe, K.G. Brahmbhatt, R.U. Phadke, D. Mitra, K.K. Bhutani, and I.P. Singh. Journal of Natural Medicines, 2011.  <b>[Epub ahead of print]</b>; PMID[21365365].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21402840">Mechanism of resistance to GS-9148 by the Q151L mutation in HIV-1 Reverse Transcriptase.</a> Scarth, B.J., K.L. White, J.M. Chen, E.B. Lansdon, S. Swaminathan, M.D. Miller, and M. Gotte. Antimicrob Agents and Chemotherapy, 2011.  <b>[Epub ahead of print]</b>; PMID[21402840].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21371895">Synthesis, biological evaluation and 3D-QSAR studies of 3-keto salicylic acid chalcones and related amides as novel HIV-1 integrase inhibitors.</a> Sharma, H., S. Patil, T.W. Sanchez, N. Neamati, R.F. Schinazi, and J.K. Buolamwini. Bioorganic &amp; Medicinal Chemistry, 2011. 19(6): p. 2030-2045; PMID[21371895].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21381765">3-Hydroxypyrimidine-2,4-diones as an Inhibitor Scaffold of HIV Integrase.</a> Tang, J., K. Maddali, M. Metifiot, Y.Y. Sham, R. Vince, Y. Pommier, and Z. Wang. Journal of Medicinal Chemistry, 2011.  <b>[Epub ahead of print]</b>; PMID[21381765].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21391888">Antiviral hyperactivation-limiting therapeutics as a novel class for the treatment of HIV/AIDS: focus on VS411.</a> Uglietti, A. and R. Maserati. Expert Opinion on Investigational Drugs, 2011. 20(4): p. 559-565; PMID[21391888].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21375388">Analyses of Anti-HIV-1 Activity of a Small CCR5 Peptide Antagonist.</a> Wang, F., S. He, Z.J. Zhang, L.F. He, X. Chen, and T. Teng. AIDS Research and Human Retroviruses, 2011.  <b>[Epub ahead of print]</b>; PMID[21375388].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2"> ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287204700009">Dimeric 2G12 as a Potent Protection against HIV-1.</a> Luo, X.M., M.Y.Y. Lei, R.A. Feidi, A.P. West, A.B. Balazs, P.J. Bjorkman, L.L. Yang, and D. Baltimore. PLoS Pathogens, 2010. 6(12); ISI[000285587500013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285998000031">6-Benzylamino 4-oxo-1,4-dihydro-1,8-naphthyridines and 4-oxo-1,4-dihydroquinolines as HIV integrase inhibitors.</a> Nagasawa, J.Y., J. Song, H.M. Chen, H.W. Kim, J. Blazel, S. Ouk, B. Groschel, V. Borges, V. Ong, L.T. Yeh, J.L. Girardet, J.M. Vernier, A.K. Raney, and A.B. Pinkerton. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(2): p. 760-763; ISI[000285998000031]</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286798100002">Identification of a Novel Scaffold for Allosteric Inhibition of Wild Type and Drug Resistant HIV-1 Reverse Transcriptase by Fragment Library Screening.</a> Geitmann, M., M. Elinder, C. Seeger, P. Brandt, I.J.P. de Esch, and U.H. Danielson. Journal of Medicinal Chemistry, 2011. 54(3): p. 699-708; ISI[000286798100002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286798100003">Deconstruction of Non-Nucleoside Reverse Transcriptase Inhibitors of Human Immunodeficiency Virus Type 1 for Exploration of the Optimization Landscape of Fragments.</a> Brandt, P., M. Geitmann, and U.H. Danielson. Journal of Medicinal Chemistry, 2011. 54(3): p. 709-718; ISI[000286798100003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286906100045">Synthesis and Anti-HIV Activity of New 3 &#39;-O-Phosphonylmethyl Nucleosides.</a> Cesnek, M. and P. Herdewijn. Heterocycles, 2010. 82(1): p. 663-687; ISI[000286906100045].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286957400008">Triple Hyp -&gt; Pro Replacement in Integramide A, a Peptaib Inhibitor of HIV-1 Integrase: Effect on Conformation and Bioactivity.</a> De Zotti, M., W. De Borggraeve, B. Kaptein, Q.B. Broxterman, S.B. Singh, P.J. Felock, D.J. Hazuda, F. Formaggio, and C. Toniolo. Biopolymers, 2011. 96(1): p. 49-59; ISI[000286957400008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286964500011">Modeling and Spectroscopic Studies of Synthetic Diazabicyclo Analogs of the HIV-1 Inhibitor BMS-378806 and Evaluation of Their Antiviral Activity.</a> Legnani, L., D. Colombo, E. Cocchi, L. Solano, S. Villa, L. Lopalco, V. Asti, L. Diomede, F.M. Albini, and L. Toma. European Journal of Organic Chemistry, 2011(2): p. 287-294; ISI[000286964500011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287008700019">Construction, Expression and Activities of a Novel Anti-HIV-1 Protein GRFT.</a> Li, C., X.A. Li, Y.S. Liu, F. Yu, J.L. Han, B. Hu, M. Ye, J. Wang, S.W. Du, and N.Y. Jin. Chemical Journal of Chinese Universities-Chinese, 2011. 32(1): p. 95-99; ISI[000287008700019].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287017000020">A Quantitative Structure-Activity Relationship Studies On a Series of 4,1-Benzoxazepinone Analogues of Efavirenz, as HIV-1 Reverese Transcriptase Inhibitors.</a> Srivastava, P.L. and N. Kesharwani. Oxidation Communications, 2010. 33(4): p. 943-947; ISI[000287017000020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287076500004">Water-soluble Phenolic Compounds and Their Anti-HIV-1 Activities from the Leaves of Cyclocarya paliurus.</a> Zhang, J.A., N. Huang, J.C. Lu, X. Li, Y.H. Wang, L.M. Yang, Y.T. Zheng, and K. Xiao. Journal of Food and Drug Analysis, 2010. 18(6): p. 398-404; ISI[000287076500004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287204700009">Modification and biological evaluation of novel 4-hydroxy-pyrone derivatives as non-peptidic HIV-1 protease inhibitors.</a> He, M.Z., N. Yang, C.L. Sun, X.J. Yao, and M. Yang. Medicinal Chemistry Research, 2011. 20(2): p. 200-209; ISI[000287204700009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287274000016">Recombinant expression of Polygonatum cyrtonema lectin with anti-viral, apoptosis-inducing activities and preliminary crystallization.</a> Li, C.Y., P. Luo, J.J. Liu, E.Q. Wang, W.W. Li, Z.H. Ding, L. Mou, and J.K. Bao. Process Biochemistry, 2011. 46(2): p. 533-542; ISI[000287274000016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287340700016">Synthesis and anti-HIV activities of bis-(cycloSaligenyl) pronucleotides derivatives of 3 &#39;-fluoro-3 &#39;-deoxythymidine and 3 &#39;-azido-3 &#39;-deoxythymidine.</a> Ahmadibeni, Y., R. Tiwari, C. Swepson, J. Pandhare, C. Dash, G.F. Doncel, and K. Parang. Tetrahedron Letters, 2011. 52(7): p. 802-805; ISI[000287340700016].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287350800009">In vitro antibacterial, antioxidant, total phenolic contents and anti-HIV-1 reverse transcriptase activities of extracts of seven Phyllanthus sp.</a> Eldeen, I.M.S., E.M. Seow, R. Abdullah, and S.F. Sulaiman. South African Journal of Botany, 2011. 77(1): p. 75-79; ISI[000287350800009].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287594400014">Synthesis and biological activity of the pinostrobin oxime complex compounds with some d-metals.</a> Mashentseva, A.A., T.S. Seytembetov, S.M. Adekenov, B.I. Tuleuov, O.P. Loiko, and A.I. Khalitova. Russian Journal of General Chemistry, 2011. 81(1): p. 96-101; ISI[000287594400014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287767800003">An antifungal defensin from Phaseolus vulgaris cv. &#39;Cloud Bean&#39;.</a> Wu, X.L., J.A. Sun, G.Q. Zhang, H.X. Wang, and T.B. Ng. Phytomedicine, 2011. 18(2-3): p. 104-109; ISI[000287767800003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0304-031711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
