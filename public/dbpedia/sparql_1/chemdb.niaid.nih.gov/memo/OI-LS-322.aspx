

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-322.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8lc4ka7lI6YcbsHFK3SEr1Y5XQJ/TjRysQFgFwb5hxiaMCAUgCUiOB+t1CXXU0qZu+zRvvTLi7gRGATYlxr7xv9Kx1hKmaVWntWIp+n3mfz9aoCUdLQqdOBY9IM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7A818F16" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-322-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45298   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Geldanamycin, a potent and specific inhibitor of Hsp90, inhibits gene expression and replication of human cytomegalovirus</p>

    <p>          Basha, W, Kitagawa, R, Uhara, M, Imazu, H, Uechi, K, and Tanaka, J</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(2): 135-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15889536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15889536&amp;dopt=abstract</a> </p><br />

    <p>2.     45299   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          In vitro antifungal activity of 2-(3,4-dimethyl-2,5-dihydro-1H-pyrrol-2-yl)-1-methylethyl pentanoate, a dihydropyrrole derivative</p>

    <p>          Dabur, R, Chhillar, AK, Yadav, V, Kamal, PK, Gupta, J, and Sharma, GL</p>

    <p>          J Med Microbiol <b>2005</b>.  54(Pt 6): 549-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15888463&amp;dopt=abstract</a> </p><br />

    <p>3.     45300   OI-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Getting inside the bug: New targets for TB drugs</p>

    <p>          Sutherland, Stephani</p>

    <p>          Drug Discovery Today <b>2005</b>.  10(10): 679-680</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T64-4G5H3N9-3/2/eb3ba3051469f25c0d46feb7e76a1d27">http://www.sciencedirect.com/science/article/B6T64-4G5H3N9-3/2/eb3ba3051469f25c0d46feb7e76a1d27</a> </p><br />

    <p>4.     45301   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Activity of RBx 7644 and RBx 8700, new investigational oxazolidinones, against Mycobacterium tuberculosis infected murine macrophages</p>

    <p>          Sood, R, Rao, M, Singhal, S, and Rattan, A</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885988&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885988&amp;dopt=abstract</a> </p><br />

    <p>5.     45302   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Antimicrobial and cytolytic properties of the frog skin peptide, kassinatuerin-1 and its l- and d-lysine-substituted derivatives</p>

    <p>          Conlon, JM, Abraham, B, Galadari, S, Knoop, FC, Sonnevend, A, and Pal, T</p>

    <p>          Peptides <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885852&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15885852&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     45303   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Antimicrobial activities of some amino derivatives of 5, 7-dibromo-2-methyl-8-benzoyloxyquinoline</p>

    <p>          Nwodo, NJ, Okide, GB, and Esimone, CO</p>

    <p>          Boll Chim Farm  <b>2004</b>.  143(9): 341-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15881812&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15881812&amp;dopt=abstract</a> </p><br />

    <p>7.     45304   OI-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Synthesis, antimalarial, antileishmanial, and antimicrobial activities of some 8-quinolinamine analogues</p>

    <p>          Jain, Meenakshi, Khan, Shabana I, Tekwani, Babu L, Jacob, Melissa R, Singh, Savita, Singh, Prati Pal, and Jain, Rahul</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G3KBT0-7/2/bcb54c200a2a7936fe103521918e01b7">http://www.sciencedirect.com/science/article/B6TF8-4G3KBT0-7/2/bcb54c200a2a7936fe103521918e01b7</a> </p><br />

    <p>8.     45305   OI-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          2-Arylidene-4-(4-phenoxy-phenyl)but-3-en-4-olides: Synthesis, reactions and biological activity</p>

    <p>          Husain, Asif, Khan, MSY, Hasan, SM, and Alam, MM</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4G3CWJR-2/2/48a444fe2c2d882c4e110d9361f4b640">http://www.sciencedirect.com/science/article/B6VKY-4G3CWJR-2/2/48a444fe2c2d882c4e110d9361f4b640</a> </p><br />

    <p>9.     45306   OI-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          An adenylyl cyclase pseudogene in Mycobacterium tuberculosis has a functional ortholog in Mycobacterium avium</p>

    <p>          Shenoy, AR, Srinivas, A, Mahalingam, M, and Visweswariah, SS</p>

    <p>          Biochimie <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VRJ-4G35XPW-2/2/6a30b10893a817a9767ae9cf303240fe">http://www.sciencedirect.com/science/article/B6VRJ-4G35XPW-2/2/6a30b10893a817a9767ae9cf303240fe</a> </p><br />

    <p>10.   45307   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Mutant Prevention Concentration of Isoniazid, Rifampicin and Rifabutin against Mycobacterium tuberculosis</p>

    <p>          Rodriguez, JC, Cebrian, L, Ruiz, M, Lopez, M, and Royo, G</p>

    <p>          Chemotherapy <b>2005</b>.  51(2-3): 76-79</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15870500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15870500&amp;dopt=abstract</a> </p><br />

    <p>11.   45308   OI-LS-322; EMBASE-OI-5/16/2005</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial evaluation of certain fluoroquinolone derivatives</p>

    <p>          Zhao, Yue-Ling, Chen, Yeh-Long, Sheu, Jia-Yuh, Chen, I-Li, Wang, Tai-Chi, and Tzeng, Cherng-Chyi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4G24XBC-6/2/cf212d3443978cf71b53ad2d78de22b7">http://www.sciencedirect.com/science/article/B6TF8-4G24XBC-6/2/cf212d3443978cf71b53ad2d78de22b7</a> </p><br />

    <p>12.   45309   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          A novel class of dual-family immunophilins</p>

    <p>          Adams, B, Musiyenko, A, Kumar, R, and Barik, S</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845546&amp;dopt=abstract</a> </p><br />

    <p>13.   45310   OI-LS-322; PUBMED-OI-5/16/2005</p>

    <p class="memofmt1-2">          Mutants of Mycobacterium tuberculosis lacking three of the five rpf-like genes are defective for growth in vivo and for resuscitation in vitro</p>

    <p>          Downing, KJ, Mischenko, VV, Shleeva, MO, Young, DI, Young, M, Kaprelyants, AS, Apt, AS, and Mizrahi, V</p>

    <p>          Infect Immun <b>2005</b>.  73(5): 3038-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845511&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15845511&amp;dopt=abstract</a> </p><br />

    <p>14.   45311   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Molecular models of protein targets from Mycobacterium tuberculosis</p>

    <p>          da Silveria, NJF, Uchoa, HB, Pereira, JH, Canduri, F, Basso, LA, Palma, MS, Santos, DS, and de Azevedo, WF</p>

    <p>          JOURNAL OF MOLECULAR MODELING <b>2005</b>.  11(2): 160-166, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228538100009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228538100009</a> </p><br />

    <p>15.   45312   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Synthesis of some 1,2,4-triazoles as potential antifungal agents</p>

    <p>          Siddiqui, AA, Arora, A, Siddiqui, N, and Misra, A</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2005</b>.  44 (4): 838-841, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228530400018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228530400018</a> </p><br />

    <p>16.   45313   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of 4-thiazolidinone derivatives as potential antimycobacterial agents</p>

    <p>          Srivastava, T, Gaikwad, AK, Haq, W, Sinha, S, and Katti, SB</p>

    <p>          ARKIVOC <b>2005</b>.: 120-130, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228575600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228575600009</a> </p><br />

    <p>17.   45314   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Cyclic HPMPC is a highly effective therapy for CMV-induced deafness in a guinea pig model</p>

    <p>          White, DR, Choo, DI, Stroup, G, and Schleiss, MR</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900017</a> </p><br />
    <br clear="all">

    <p>18.   45315   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Cyclic HPMPC therapy improves the outcome of guinea pig cytomegalovirus congenital infection and decreases the viral load in the placenta and fetus</p>

    <p>          Bernstein, DI, Bravo, FJ, and Cardin, RD</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A35-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900018</a> </p><br />

    <p>19.   45316   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          Potent and selective inhibition of hepatitis C virus replication by the non-immunosuppressive cyclosporin analogue DEBIO-025</p>

    <p>          Paeshuyse, J,  Dumont, JM, Rosenwirth, B, De Clercq, E, and Neyts, J</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A41-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900029</a> </p><br />

    <p>20.   45317   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          In vitro characterization of celgosivir, a clinical stage compound for the treatment of hepatitis C viral infections</p>

    <p>          Dugourd, D, Fenn, J, Siu, R, Clement, JJ, and Coulson, R</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A60-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900073">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900073</a> </p><br />

    <p>21.   45318   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          A new family of non-nucleoside inhibitors of human cytomegalovirus (HCMV) and varicella-zoster virus (VZV) based on the beta-keto-gamma-sultone template</p>

    <p>          De Castro, S,  Garcia-Aparicio, C, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A74-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900106">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900106</a> </p><br />

    <p>22.   45319   OI-LS-322; WOS-OI-5/8/2005</p>

    <p class="memofmt1-2">          The in vitro inhibition of cytomegalovirus by novel ribonucleotide reductase inhibitors didox and trimidox</p>

    <p>          Inayat, MS, Gallicchio, VS, Garvy, BA, Elford, HL, and Oakley, OR</p>

    <p>          ANTIVIRAL RESEARCH <b>2005</b>.  65(3): A74-A75, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900107">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227989900107</a> </p><br />

    <p>23.   45320   OI-LS-322; WOS-OI-5/15/2005</p>

    <p class="memofmt1-2">          Simultaneous estimation of pharmacokinetic properties in mice of three anti-tubercular ethambutol analogs obtained from combinatorial lead optimization</p>

    <p>          Jia, L, Tomaszewski, JE, Noker, PE, Gorman, GS, Glaze, E, and Protopopova, M</p>

    <p>          JOURNAL OF PHARMACEUTICAL AND BIOMEDICAL ANALYSIS <b>2005</b> .  37(4): 793-799, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228783800023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228783800023</a> </p><br />
    <br clear="all">

    <p>24.   45321   OI-LS-322; WOS-OI-5/15/2005</p>

    <p class="memofmt1-2">          Phenatic acids A and B, new potentiators of antifungal miconazole activity produced by Streptomyces sp K03-0132</p>

    <p>          Fukuda, T, Matsumoto, A, Takahashi, Y, Tomoda, H, and Omura, S</p>

    <p>          JOURNAL OF ANTIBIOTICS <b>2005</b>.  58(4): 252-259, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228778500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228778500003</a> </p><br />

    <p>25.   45322   OI-LS-322; WOS-OI-5/15/2005</p>

    <p class="memofmt1-2">          Synthesis of some 1-[(N,N-disubstituted thiocarbamoylthio)acetyl]-3-(2-thienyl) 5-aryl-2-pyrazoline derivatives and investigation of their antibacterial and antifungal activities</p>

    <p>          Turan-Zitouni, G, Ozdemir, A, and Guven, K</p>

    <p>          ARCHIV DER PHARMAZIE <b>2005</b>.  338(2-3): 96-104, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228756500005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228756500005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
