

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-03-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="GZzG5rsMvssF+bPQyhvN3M6hEmJ5ajng5aG+LX9qnQ4G2RjAr+7elmMxCxyow7uyAtZeJ3m8QewVtxhkMvnSID/dKqNtyE3bfzZ8Wu28goSerGDzNgvV9e/dYQM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FC50E60D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">March 4 - March 18, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20231781">Combination therapy for hepatitis C virus with heat-shock protein 90 inhibitor 17-AAG and proteasome inhibitor MG132.</a></p>

    <p class="NoSpacing">Ujino S, Yamaguchi S, Shimotohno K, Takaku H.</p>

    <p class="NoSpacing">Antivir Chem Chemother. 2010 Mar 9;20(4):161-7.PMID: 20231781 [PubMed - in process]</p> 
    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20231449">Identification of hepatoprotective flavonolignans from silymarin.</a></p>

    <p class="NoSpacing">Polyak SJ, Morishima C, Lohmann V, Pal S, Lee DY, Liu Y, Graf TN, Oberlies NH.</p>

    <p class="NoSpacing">Proc Natl Acad Sci U S A. 2010 Mar 15. [Epub ahead of print]PMID: 20231449 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20226659">Potent ketoamide inhibitors of HCV NS3 protease derived from quaternized P(1) groups.</a></p>

    <p class="NoSpacing">Venkatraman S, Velazquez F, Wu W, Blackman M, Madison V, Njoroge FG.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Feb 14. [Epub ahead of print]PMID: 20226659 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">4.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/20219368">Inhibitors of hepatitis C virus polymerase: Synthesis and characterization of novel 6-fluoro-N-[2-hydroxy-1(S)-benzamides.</a></p>

    <p class="NoSpacing">Cheng CC, Shipps GW Jr, Yang Z, Kawahata N, Lesburg CA, Duca JS, Bandouveres J, Bracken JD, Jiang CK, Agrawal S, Ferrari E, Huang HC.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2010 Feb 18. [Epub ahead of print]PMID: 20219368 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20204428">Hydroxyurea as an inhibitor of hepatitis C virus RNA replication.</a></p>

    <p class="NoSpacing">Nozaki A, Morimoto M, Kondo M, Oshima T, Numata K, Fujisawa S, Kaneko T, Miyajima E, Morita S, Mori K, Ikeda M, Kato N, Tanaka K.</p>

    <p class="NoSpacing">Arch Virol. 2010 Mar 5. [Epub ahead of print]PMID: 20204428 [PubMed - as supplied by publisher]</p> 
    <p />

    <h2 class="memofmt2-2">Hepatitis B Virus</h2> 

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20211652">Simvastatin potentiates the anti-hepatitis B virus activity of FDA-approved nucleoside analogue inhibitors in vitro.</a> Bader T, Korba B. Antiviral Res. 2010 Mar 6. [Epub ahead of print]PMID: 20211652 [PubMed - as supplied by publisher]</p> 
    <p />

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="NoSpacing">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274773300010">Synthesis, anti-HCV, antioxidant, and peroxynitrite inhibitory activity of fused benzosuberone derivatives.</a> Farghaly, Thoraya A., Hafez, Naglaa A. Abdel, Ragab, Eman A., Awad, Hanem M., Abdalla, Mohamed M . EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY. 45(2): 2010. P.492-500.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274714600029">Tellimagrandin I, HCV invasion inhibitor from Rosae Rugosae Flos.</a> Tamura, Satoru, Yang, Gang-Ming, Yasueda, Natsuko, Matsuura, Yoshiharu, Komoda, Yasumasa, Murakami, Nobutoshi. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(5): 2010. P.1598-1600.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000274733300002">Mechanism of Hepatitis C Virus RNA Polymerase Inhibition with Dihydroxypyrimidines.</a></p>

    <p class="NoSpacing">Powdrill, Megan H., Deval, Jerome, Narjes, Frank, De Francesco, Raffaele, Goette, Matthias. ANTIMICROBIAL AGENTS AND CHEMOTHERAPY. 54(3): 2010. P.977-83.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
