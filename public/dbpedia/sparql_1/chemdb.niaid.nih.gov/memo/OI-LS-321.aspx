

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-321.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="iJT5+aILzs8KJPBQiRIK3s5sBcX0mSd9oiJJj1DGMYdDs2NhN4Ixz6cr0bKFkzO7u+wnBy1ZycUfpRY7oc704pPUcD7I7pS3ZRmIAlOawMbktIQYTfr8nCM3A1M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7BBAD7DC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-321-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45249   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          New Antiviral Pathway That Mediates Hepatitis C Virus Replicon Interferon Sensitivity through ADAR1</p>

    <p>          Taylor, DR, Puig, M, Darnell, ME, Mihalik, K, and Feinstone, SM</p>

    <p>          J Virol <b>2005</b> .  79(10): 6291-6298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858013&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15858013&amp;dopt=abstract</a> </p><br />

    <p>2.     45250   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          Bisphosphonate Inhibitors of Toxoplasma gondi Growth: In Vitro, QSAR, and In Vivo Investigations</p>

    <p>          Ling, Y, Sahota, G, Odeh, S, Chan, JM, Araujo, FG, Moreno, SN, and Oldfield, E</p>

    <p>          J Med Chem <b>2005</b>.  48(9): 3130-3140</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15857119&amp;dopt=abstract</a> </p><br />

    <p>3.     45251   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          Antifungal activity of steroidal glycosides from Yucca gloriosa L</p>

    <p>          Favel, A, Kemertelidze, E, Benidze, M, Fallague, K, and Regli, P</p>

    <p>          Phytother Res <b>2005</b>.  19(2): 158-161</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15852482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15852482&amp;dopt=abstract</a> </p><br />

    <p>4.     45252   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Microsporidiosis: An emerging and opportunistic infection in humans and animals</p>

    <p>          Didier, Elizabeth S</p>

    <p>          Acta Tropica <b>2005</b>.  94(1): 61-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     45253   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of antifungal cyclic lipopeptides</p>

    <p>          Mizuno, Hiroaki, Matsuda, Hiroshi, Matsuya, Takahiro, and Barrett, David</p>

    <p>          PATENT:  WO <b>2005005463</b>  ISSUE DATE:  20050120</p>

    <p>          APPLICATION: 2004  PP: 75 pp.</p>

    <p>          ASSIGNEE:  (Fujisawa Pharmaceutical Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     45254   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Structural modifications and antimicrobial activity of N-cycloalkenyl-2-acylalkylidene-2,3-dihydro-1,3-benzothiazoles</p>

    <p>          Latrofa, Andrea, Franco, Massimo, Lopedota, Angela, Rosato, Antonio, Carone, Dora, and Vitali, Cesare</p>

    <p>          Farmaco <b>2005</b>.  60(4): 291-297</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     45255   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          In Vitro Anti-Cytomegalovirus Activity of Kampo (Japanese Herbal) Medicine</p>

    <p>          Murayama, T, Yamaguchi, N, Matsuno, H, and Eizuru, Y</p>

    <p>          Evid Based Complement Alternat Med <b>2004</b>.  1(3): 285-289</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841262&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15841262&amp;dopt=abstract</a> </p><br />

    <p>8.     45256   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          Functional Characterization of a Novel ArgA from Mycobacterium tuberculosis</p>

    <p>          Errey, JC and  Blanchard, JS</p>

    <p>          J Bacteriol <b>2005</b>.  187(9): 3039-3044</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15838030&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15838030&amp;dopt=abstract</a> </p><br />

    <p>9.     45257   OI-LS-321; PUBMED-OI-5/2/2005</p>

    <p class="memofmt1-2">          Antifungal Synergistic Effect of Scopoletin, a Hydroxycoumarin Isolated from Melia azedarach L. Fruits</p>

    <p>          Carpinella, MC, Ferrayoli, CG, and Palacios, SM</p>

    <p>          J Agric Food Chem <b>2005</b>.  53(8): 2922-2927</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15826040&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15826040&amp;dopt=abstract</a> </p><br />

    <p>10.   45258   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Synthesis and potent antimicrobial activity of some novel 2-phenyl or methyl-4H-1-benzopyran-4-ones carrying amidinobenzimidazoles</p>

    <p>          Goeker, Hakan, Boykin, David W, and Yildiz, Sulhiye</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(5): 1707-1714</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   45259   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Dihydropteroate synthase and novel dihydrofolate reductase gene mutations in strains of Pneumocystis jirovecii from South Africa</p>

    <p>          Robberts, FJL, Chalkley, LJ, Weyer, K, Goussard, P, and Liebowitz, LD</p>

    <p>          Journal of Clinical Microbiology <b>2005</b>.  43(3): 1443-1444</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   45260   OI-LS-321; WOS-OI-4/24/2005</p>

    <p class="memofmt1-2">          Antimicrobial activity of actinomycetes isolated from aquatic environments in Southern Chile</p>

    <p>          Leiva, S, Yanez, M, Zaror, L, Rodriguez, H, and Garcia-Quintana, H</p>

    <p>          REVISTA MEDICA DE CHILE <b>2004</b>.  132(2): 151-159, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228070000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228070000003</a> </p><br />
    <br clear="all">

    <p>13.   45261   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of oxazolidinone derivatives as antimicrobial agents</p>

    <p>          Mehta, Anita, Rudra, Sonali, Rao, Ajjarapu Venkata Subrahmanya Rajo, Yadav, Ajay Singh, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2004089944</b>  ISSUE DATE:  20041021</p>

    <p>          APPLICATION: 2003  PP: 66 pp.</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   45262   OI-LS-321; WOS-OI-4/24/2005</p>

    <p class="memofmt1-2">          From 1-acyl-beta-lactam human cytomegalovirus protease inhibitors to 1-benzyloxycarbonylazetidines with improved antiviral activity. A straightforward approach to convert covalent to noncovalent inhibitors</p>

    <p>          Gerona-Navarro, G, de Vega, MJP, Garcia-Lopez, MT, Andrei, G, Snoeck, R, De Clercq, E, Balzarini, J, and Gonzalez-Muniz, R</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  48(7): 2612-2621, 10</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228111500037">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228111500037</a> </p><br />

    <p>15.   45263   OI-LS-321; WOS-OI-4/24/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus (HCV) NS5A protein downregulates HCVIRES-dependent translation</p>

    <p>          Kalliampakou, KI, Kalamvoki, M, and Mavromara, P</p>

    <p>          JOURNAL OF GENERAL VIROLOGY <b>2005</b>.  86: 1015-1025, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228236300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228236300014</a> </p><br />

    <p>16.   45264   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Fluorosubstitution and 7-alkylation as prospective modifications of biologically active 6-aryl derivatives of tricyclic acyclovir and ganciclovir analogues</p>

    <p>          Ostrowski, Tomasz, Golankiewicz, Bozenna, De Clercq, Erik, and Balzarini, Jan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(6): 2089-2096</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   45265   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          PDX1, a cellular homeoprotein, binds to and regulates the activity of human cytomegalovirus immediate early promoter: screening of herpesviral replication inhibitors</p>

    <p>          Chao, Sheng-Hao and Caldwell, Jeremy S</p>

    <p>          PATENT:  WO <b>2005014859</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 40 pp.</p>

    <p>          ASSIGNEE:  (IRM LLC, Bermuda</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   45266   OI-LS-321; WOS-OI-4/24/2005</p>

    <p class="memofmt1-2">          Identification of cyclic AMP-regulated genes in Mycobacterium tuberculosis complex bacteria under low-oxygen conditions</p>

    <p>          Gazdik, MA and McDonough, KA</p>

    <p>          JOURNAL OF BACTERIOLOGY <b>2005</b>.  187(8): 2681-2692, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228204700014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228204700014</a> </p><br />
    <br clear="all">

    <p>19.   45267   OI-LS-321; SCIFINDER-OI-4/25/2005</p>

    <p class="memofmt1-2">          Preparation of purine nucleoside analogs for treating flaviviridae including hepatitis C</p>

    <p>          Storer, Richard, Gosselin, Gilles, Dukhan, David, and Leroy, Frederic</p>

    <p>          PATENT:  WO <b>2005009418</b>  ISSUE DATE:  20050203</p>

    <p>          APPLICATION: 2004  PP: 139 pp.</p>

    <p>          ASSIGNEE:  (Idenix Cayman Limited, Cayman I., Centre National De La Recherche Scientifique, and L&#39;universite Montpellier II)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   45268   OI-LS-321; WOS-OI-4/24/2005</p>

    <p class="memofmt1-2">          Antitubercular agents. Part 1: Synthesis of phthalimido- and naphthalimido-linked phenazines as new prototype antitubercular agents</p>

    <p>          Kamal, A, Babu, AH, Ramana, AV, Sinha, R, Yadav, JS, and Arora, SK</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(7): 1923-1926, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228125300032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228125300032</a> </p><br />

    <p>21.   45269   OI-LS-321; WOS-OI-5/1/2005</p>

    <p class="memofmt1-2">          Easy-to-execute carbonylations: Microwave synthesis of acyl sulfonamides using Mo(CO)(6) as a solid carbon monoxide source</p>

    <p>          Wu, XY, Ronn, R, Gossas, T, and Larhed, M</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2005</b>.  70(8): 3094-3098, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228367100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228367100023</a> </p><br />

    <p>22.   45270   OI-LS-321; WOS-OI-5/1/2005</p>

    <p class="memofmt1-2">          Single- and multiple-dose pharmacokinetics of levovirin valinate hydrochloride (R1518) in healthy volunteers</p>

    <p>          Huang, Y, Ostrowitzki, S, Hill, G, Navarro, M, Berger, N, Kopeck, P, Mau, CI, Alfredson, T, and Lal, R</p>

    <p>          JOURNAL OF CLINICAL PHARMACOLOGY <b>2005</b>.  45(5): 578-588, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228412700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228412700012</a> </p><br />

    <p>23.   45271   OI-LS-321; WOS-OI-5/1/2005</p>

    <p class="memofmt1-2">          High-throughput microplate phosphorylation assays based on DevR-DevS/Rv2027c 2-component signal transduction pathway to screen for novel antitubercular compounds</p>

    <p>          Saini, DK and Tyagi, JS</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2005</b>.  10(3): 215-224, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228384900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228384900003</a> </p><br />

    <p>24.   45272   OI-LS-321; WOS-OI-5/1/2005</p>

    <p class="memofmt1-2">          In vitro susceptibility testing of Candida and Aspergillus spp. to voriconazole and other antifungal agents using Etest (R): results of a French multicentre study</p>

    <p>          Mallie, M, Bastide, JM, Blancard, A, Bonnin, A, Bretagne, S, Cambon, M, Chandenier, J, Chauveau, V, Couprie, B, Datry, A, Feuilhade, M, Grillot, R, Guiguen, C, Lavarde, V, Letscher, V, Linas, MD, Michel, A, Morin, O, Paugam, A, Piens, MA, Raberin, H, Tissot, E, Toubas, D, and Wade, A</p>

    <p>          INTERNATIONAL JOURNAL OF ANTIMICROBIAL AGENTS <b>2005</b>.  25(4): 321-328, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228406100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228406100008</a> </p><br />
    <br clear="all">

    <p>25.   45273   OI-LS-321; WOS-OI-5/1/2005</p>

    <p class="memofmt1-2">          Antibacterial antisense</p>

    <p>          Geller, BL</p>

    <p>          CURRENT OPINION IN MOLECULAR THERAPEUTICS <b>2005</b>.  7(2): 109-113, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228420800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228420800003</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
