

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-12-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Psb9s6X13zQbFO0znihVgCeL6FmgKDf8X2qragh0Qcg5LE5j71HPkcHOrZVlmk1Eb3V1Ur2NJjQ9pFYC4kFWMnAqVjL6NbRflrp90JV1c87CBz2XNijw7NWBX18=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1A070C7A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 7 - December 20, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147074">Design, Synthesis and Antimicrobial Evaluation of Novel 1-Benzyl 2-butyl-4-chloroimidazole Embodied 4-Azafluorenones <b>v</b>ia Molecular Hybridization Approach.</a> Addla, D., Bhima, B. Sridhar, A. Devi, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7475-7480. PMID[23147074].</p>

    <p class="plaintext">[PubMed]. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23223175">A Saccharomyces cerevisiae Wine Strain Inhibits Growth and Decreases Ochratoxin a Biosynthesis by Aspergillus carbonarius and Aspergillus ochraceus.</a> Cubaiu, L., H. Abbas, A.D. Dobson, M. Budroni, and Q. Migheli. Toxins, 2012. 4(12): p. 1468-1481. PMID[23223175].</p>

    <p class="plaintext">[PubMed]. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23237384">Sulfonylureas Have Antifungal Activity and Are Potent Inhibitors of Candida albicans Acetohydroxyacid Synthase.</a> Lee, Y.T., C.J. Cui, E.W. Chow, N. Pue, T. Lonhienne, J.G. Wang, J.A. Fraser, and L.W. Guddat. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[23237384].</p>

    <p class="plaintext">[PubMed]. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23238369">IQ-Motif Peptides as Novel Anti-microbial Agents.</a> McLean, D.T., F.T. Lundy, and D.J. Timson. Biochimie, 2012. <b>[Epub ahead of print]</b>; PMID[23238369].</p>

    <p class="plaintext">[PubMed]. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23239523">A Novel Antifungal Agent with Broad Spectrum: 1-(4-Biphenylyl)-3-(1H-imidazol-1-yl)-1-propanone.</a> Roman, G., M. Mares, and V. Nastasa. Archiv der Pharmazie, 2012. <b>[Epub ahead of print]</b>; PMID[23239523].</p>

    <p class="plaintext">[PubMed]. OI_1207-122012.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23122863">6. A Regio- and Stereoselective 1,3-Dipolar Cycloaddition for the Synthesis of New-fangled Dispiropyrrolothiazoles as Antimycobacterial Agents.</a> Almansour, A.I., S. Ali, M.A. Ali, R. Ismail, T.S. Choon, V. Sellappan, K.K. Elumalai, and S. Pandian. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7418-7421. PMID[23122863].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23220041">Capreomycin Supergenerics for Pulmonary Tuberculosis Treatment: Preparation, in Vitro, and in Vivo Characterization.</a> Aurelie, S., B. Paolo, M.M. Luisa, B. Lanfranco, G. Stefano, C. Carlo, and R. Maurizio. European Journal of Pharmaceutics and Biopharmaceutics. 2012. <b>[Epub ahead of print]</b>; PMID[23220041].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p>  
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23240649">A Novel F(420)-dependent Anti-oxidant Mechanism Protects Mycobacterium tuberculosis against Oxidative Stress and Bactericidal Agents.</a> Gurumurthy, M., M. Rao, T. Mukherjee, S.P. Rao, H.I. Boshoff, T. Dick, C.E. Barry, 3rd, and U.H. Manjunatha. Molecular Microbiology, 2012. <b>[Epub ahead of print]</b>; PMID[23240649].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23137434">Development of 5-Nitrothiazole Derivatives: Identification of Leads against Both Replicative and Latent Mycobacterium tuberculosis.</a> Jeankumar, V.U., M. Chandran, G. Samala, M. Alvala, P.V. Koushik, P. Yogeeswari, E.G. Salina, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7414-7417. PMID[23137434].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23231581">The Synthetic Cathelicidin HHC-10 Inhibits Mycobacterium bovis BCG in Vitro and in C57BL/6 Mice.</a> Llamas-Gonzalez, Y.Y., C. Pedroza-Roldan, M.B. Cortes-Serna, A.L. Marquez-Aguirre, F.J. Galvez-Gastelum, and M.A. Flores-Valdez. Microbial Drug Resistance, 2012. <b>[Epub ahead of print]</b>; PMID[23231581].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23122523">Microwave-assisted Synthesis, Molecular Docking and Antitubercular Activity of 1,2,3,4-Tetrahydropyrimidine-5-carbonitrile Derivatives.</a> Mohan, S.B., B.V. Ravi Kumar, S.C. Dinda, D. Naik, S. Prabu Seenivasan, V. Kumar, D.N. Rana, and P.S. Brahmkshatriya. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7539-7542. PMID[23122523].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23225597">Studies of Inositol 1-phosphate Analogues as Inhibitors of the Phosphatidylinositol Phosphate Synthase in Mycobacteria.</a> Morii, H., T. Okauchi, H. Nomiya, M. Ogawa, K. Fukuda, and H. Taniguchi. Journal of Biochemistry, 2012. <b>[Epub ahead of print]</b>; PMID[23225597].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23116887">Synthesis and Antimycobacterial Activity of Calpinactam Derivatives.</a> Nagai, K., N. Koyama, N. Sato, C. Yanagisawa, and H. Tomoda. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7739-7741. PMID[23116887].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23228936">Antitubercular Pharmacodynamics of Phenothiazines.</a> Warman, A.J., T.S. Rito, N.E. Fisher, D.M. Moss, N.G. Berry, P.M. O&#39;Neill, S.A. Ward, and G.A. Biagini. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[23228936].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23151429">Molecular Hybridization of Bioactives: Synthesis and Antitubercular Evaluation of Novel Dibenzofuran Embodied Homoisoflavonoids Via Baylis-Hillman Reaction.</a> Yempala, T., D. Sriram, P. Yogeeswari, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7426-7430. PMID[23151429].
    <br />
    <b>[PubMed]</b>. OI_1207-122012.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23117170">Synthesis and Antimalarial Evaluation of a Screening Library Based on a Tetrahydroanthraquinone Natural Product Scaffold.</a> Choomuenwai, V., K.T. Andrews, and R.A. Davis. Bioorganic &amp; Medicinal Chemistry, 2012. 20(24): p. 7167-7174. PMID[23117170].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23240776">Synthesis and Evaluation of &#945;-Thymidine Analogues as Novel Antimalarials.</a> Cui, H., J. Carrero-Lerida, A.P. Silva, J.L. Whittingham, J.A. Brannigan, L.M. Ruiz-Perez, K.D. Read, K.S. Wilson, D. Gonzalez-Pacanowska, and I.H. Gilbert. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[23240776].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23214499">3,6-Diamino-4-(2-halophenyl)-2-benzoylthieno[2,3-b]pyridine-5-carbonitriles Are Selective Inhibitors of Plasmodium falciparum Glycogen Synthase Kinase-3 (PFGSK-3).</a> Fugel, W., A.E. Oberholzer, B. Gschloessl, R. Dzikowski, N. Pressburger, L. Preu, P. Laurence, B. Baratte, M. Ratin, I.M. Okun, C. Doerig, S. Kruggel, T. Lemcke, L. Meijer, and C. Kunick. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[23214499].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23236186">Targeting the Erad Pathway Via Inhibition of Signal Peptide Peptidase for Antiparasitic Therapeutic Design.</a> Harbut, M.B., B.A. Patel, B.K. Yeung, C.W. McNamara, A.T. Bright, J. Ballard, F. Supek, T.E. Golde, E.A. Winzeler, T.T. Diagana, and D.C. Greenbaum. Proceedings of the National Academy of Sciences of the United States of America, 2012. <b>[Epub ahead of print]</b>; PMID[23236186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23092722">Comparative in Vitro and in Vivo Antimalarial Activity of the Indole Alkaloids Ellipticine, Olivacine, Cryptolepine and a Synthetic Cryptolepine Analog.</a> Rocha, E.S.L.F., A. Montoia, R.C. Amorim, M.R. Melo, M.C. Henrique, S.M. Nunomura, M.R. Costa, V.F. Andrade Neto, D.S. Costa, G. Dantas, J. Lavrado, R. Moreira, A. Paulo, A.C. Pinto, W.P. Tadei, R.S. Zacardi, M.N. Eberlin, and A.M. Pohlit. Phytomedicine, 2012. 20(1): p. 71-76. PMID[23092722].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23215035">Alpha-Heteroatom Derivatised Analogues of 3-(Acetylhydroxyamino)propyl phosphonic acid (FR900098) as Antimalarials.</a> Verbrugghen, T., P. Vandurm, J. Pouyez, L. Maes, J. Wouters, and S. Van Calenbergh. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[23215035]. <b>[PubMed]</b>. OI_1207-122012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310896400001">Synthesis and Evaluation of Some New Thiazolidin-4-one Derivatives as Potential Antimicrobial Agents.</a> Baviskar, B.A., S.S. Khadabadi, and S.L. Deore. Journal of Chemistry, 2013. <b>[Epub ahead of print]</b>; ISI[000310896400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p>  
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310660600260">Design, Synthesis and Study of Reactive Oxygen Species Generators as Mycobacterium Tuberculosis Inhibitors.</a> Chakrapani, H. and A.T. Dharmaraja. Free Radical Biology and Medicine, 2012. 53: p. S103-S103. ISI[000310660600260].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310721000012">Antimicrobial Activity of poly(Propylene imine) Dendrimers.</a> Felczak, A., N. Wronska, A. Janaszewska, B. Klajnert, M. Bryszewska, D. Appelhans, B. Voit, S. Rozalska, and K. Lisowska. New Journal of Chemistry, 2012. 36(11): p. 2215-2222. ISI[000310721000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p>
    <p> </p>
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310901000001">Ultrasonication-induced Synthesis and Antimicrobial Evaluation of Some Multifluorinated Pyrazolone Derivatives.</a> Gadhave, A., S. Kuchekar, and B. Karale. Journal of Chemistry, 2013. <b>[Epub ahead of print]</b>; ISI[000310901000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00010931600055">Chemodegradable Gemini Alanine-Based Cationic Surfactants: Synthesis and Antifungal Activity.</a> Luczynski, J., R. Frackowiak, J. Szczepaniak, and A. Krasowska. Chemistry Letters, 2012. 41(10): p. 1176-1177. ISI[000310931600055].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310353600014">Design, Synthesis, Spectral and Biological Evaluation of Novel 1-Allyl substituted 2,6-diphenylpiperidin-4-ones and Its Derivatives of Oximes/Oxime Ethers.</a> Narayanan, K., M. Shanmugam, S. Jothivel, and S. Kabilan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(21): p. 6602-6607. ISI[000310353600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031049300011">Antimicrobial Activity of Rhodobryum Ontariense.</a> Pejin, B., A. Sabovljevic, M. Sokovic, J. Glamoclija, A. Ciric, M. Vujicic, and M. Sabovljevic. Hemijska Industrija, 2012. 66(3): p. 381-384. ISI[000310493000011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310984900002">The Effect of Histatin 5, Adsorbed on PMMA and Hydroxyapatite, on Candida albicans Colonization.</a> Vukosavljevic, D., W. Custodio, A.A.D. Cury, and W.L. Siqueira. Yeast, 2012. 29(11): p. 459-466. ISI[000310984900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310573100003">Hierarchical Virtual Screening for the Discovery of New Molecular Scaffolds in Antibacterial Hit Identification.</a> Ballester, P.J., M. Mangold, N.I. Howard, R.L.M. Robinson, C. Abell, J. Blumberger, and J.B.O. Mitchell. Journal of the Royal Society Interface, 2012. 9(77): p. 3196-3207. ISI[000310573100003].
    <br />
    <b>[WOS]</b>. OI_1207-122012.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310662700016">Inhibitory Effect of Shikonin on Candida albicans Growth.</a> Miao, H., L.Y. Zhao, C.L. Li, Q.H. Shang, H. Lu, Z.J. Fu, L. Wang, Y.Y. Jiang, and Y.Y. Cao. Biological &amp; Pharmaceutical Bulletin, 2012. 35(11): p. 1956-1963. ISI[000310662700016].
    <br />
    <b>[WOS]</b>. OI_1207-122012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
