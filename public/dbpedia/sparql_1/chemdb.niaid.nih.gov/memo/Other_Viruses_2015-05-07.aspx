

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-05-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VBphvQBegL1KyPAx+fnBwn2b4SuiHyV4NKhYJeKQ9DhM3uhnm04zt08BAR/K/qYN9JgSyzBUmKUCE2/V3wKQzZPy5qtZQbgm55pEQX02eS19PYEdFHV/YX4QPcg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B634C66C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 24 - May 7, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25847765">Design, Synthesis, Biological Evaluation and Molecular Docking Studies of Phenylpropanoid Derivatives as Potent anti-Hepatitis B Virus Agents.</a> Liu, S., W. Wei, Y. Li, X. Liu, X. Cao, K. Lei, and M. Zhou. European Journal of Medicinal Chemistry, 2015. 95: p. 473-482. PMID[25847765].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25822739">Selective Inhibitors of Cyclin G Associated Kinase (GAK) as anti-Hepatitis C Agents.</a> Kovackova, S., L. Chang, E. Bekerman, G. Neveu, R. Barouch-Bentov, A. Chaikuad, C. Heroven, M. Sala, S. De Jonghe, S. Knapp, S. Einav, and P. Herdewijn. Journal of Medicinal Chemistry, 2015. 58(8): p. 3393-3410. PMID[25822739].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25849139">Aspartic acid Based Nucleoside Phosphoramidate Prodrugs as Potent Inhibitors of Hepatitis C Virus Replication.</a> Maiti, M., M. Maiti, J. Rozenski, S. De Jonghe, and P. Herdewijn. Organic &amp; Biomolecular Chemistry, 2015. 13(18): p. 5158-5174. PMID[25849139].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25703928">Epoxide Based Inhibitors of the Hepatitis C Virus Non-structural 2 Autoprotease.</a> Shaw, J., C.W. Fishwick, and M. Harris. Antiviral Research, 2015. 117: p. 20-26. PMID[25703928].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25849312">Beta-D-2&#39;-C-Methyl-2,6-diaminopurine ribonucleoside phosphoramidates are Potent and Selective Inhibitors of Hepatitis C Virus (HCV) and are Bioconverted Intracellularly to Bioactive 2,6-Diaminopurine and Guanosine 5&#39;-Triphosphate Forms.</a> Zhou, L., H.W. Zhang, S. Tao, L. Bassit, T. Whitaker, T.R. McBrayer, M. Ehteshami, S. Amiralaei, U. Pradere, J.H. Cho, F. Amblard, D. Bobeck, M. Detorio, S.J. Coats, and R.F. Schinazi. Journal of Medicinal Chemistry, 2015. 58(8): p. 3445-3458. PMID[25849312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25922598">Anti-herpes Activity of Vinegar-processed Daphne Genkwa Flos via Enhancement of Natural Killer Cell Activity.</a> Uyangaa, E., J.Y. Choi, H.W. Ryu, S.R. Oh, and S.K. Eo. Immune Network, 2015. 15(2): p. 91-99. PMID[25922598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0424-050715.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351815900001">Virocidal Activity of Egyptian Scorpion Venoms against Hepatitis C Virus.</a> El-Bitar, A.M.H., M.M.H. Sarhan, C. Aoki, Y. Takahara, M. Komoto, L. Deng, M.A. Moustafa, and H. Hotta. Virology Journal, 2015. 12(47): 9pp. ISI[000351815900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352219600016">Monoclonal Antibodies against Extracellular Domains of Claudin-1 Block Hepatitis C Virus Infection in a Mouse Model.</a> Fukasawa, M., S. Nagase, Y. Shirasago, M. Iida, M. Yamashita, K. Endo, K. Yagi, T. Suzuki, T. Wakita, K. Hanada, H. Kuniyasu, and M. Kondoh. Journal of Virology, 2015. 89(9): p. 4866-4879. ISI[000352219600016].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352083900027">Depsides: Lichen Metabolites Active against Hepatitis C Virus.</a> Vu, T.H., A.C. Le Lamer, C. Lalli, J. Boustie, M. Samson, F.L. Devehat, and J. Le Seyec. Plos One, 2015. 10(3): p. e0120405. ISI[000352083900027].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0424-050715.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000352678900014">Synthesis and Antiviral Activity of Anthracene Derivatives of Isoxazolino-carbocyclic nucleoside Analogues.</a> Memeo, M.G., F. Lapolla, G. Maga, and P. Quadrelli. Tetrahedron Letters, 2015. 56(15): p. 1986-1990. ISI[000352678900014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0424-050715.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
