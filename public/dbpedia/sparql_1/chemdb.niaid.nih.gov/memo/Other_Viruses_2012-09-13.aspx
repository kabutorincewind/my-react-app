

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-09-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8zJuGMCF958mBU3VzcSlLUjBKLVVS99V8EbKGJmgFtQ935jx/c+nMUnosgPMq8QeXfoLGGImX2t83njW5MMvexuCgQvpTvh6CEd/4dImWRxengL6rx9uC5gE5JY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="55F8288B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 31 - September 13, 2012</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22271421">MxA Inhibits Hepatitis B Virus Replication by Interaction with Hepatitis B Core Antigen<i>.</i></a> Li, N., L. Zhang, L. Chen, W. Feng, Y. Xu, F. Chen, X. Liu, Z. Chen, and W. Liu. Hepatology, 2012. 56(3): p. 803-811; PMID[22271421].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22380514">Interleukin-15 Suppresses Hepatitis B Virus Replication Via IFN-beta Production in a C57bl/6 Mouse Model</a><i>.</i> Yin, W., L. Xu, R. Sun, H. Wei, and Z. Tian. Liver International, 2012. 32(8): p. 1306-1314; PMID[22380514].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22710121">TMC647055, a Potent Nonnucleoside Hepatitis C Virus NS5B Polymerase Inhibitor with Cross-genotypic Coverage</a><i>.</i> Devogelaere, B., J.M. Berke, L. Vijgen, P. Dehertogh, E. Fransen, E. Cleiren, L. van der Helm, O. Nyanguile, A. Tahri, K. Amssoms, O. Lenz, M.D. Cummings, R.F. Clayton, S. Vendeville, P. Raboisson, K.A. Simmen, G.C. Fanning, and T.I. Lin. Antimicrobial Agents and Chemotherapy, 2012. 56(9): p. 4676-4684; PMID[22710121].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22858143">Synthesis and SAR of Pyridothiazole Substituted Pyrimidine Derived HCV Replication Inhibitors</a><i>.</i> Girijavallabhan, V.M., C. Alvarez, F. Bennett, L. Chen, S. Gavalas, Y. Huang, S.H. Kim, A. Kosinski, P. Pinto, R. Rizvi, R. Rossman, B. Shankar, L. Tong, F. Velazquez, S. Venkatraman, V.A. Verma, J. Kozlowski, N.Y. Shih, J.J. Piwinski, M. Maccoss, C.D. Kwong, N. Bansal, J.L. Clark, A.T. Fowler, H.S. Kezar, 3rd, J. Valiyaveettil, R.C. Reynolds, J.A. Maddry, S. Ananthan, J.A. Secrist, 3rd, C. Li, R. Chase, S. Curry, H.C. Huang, X. Tong, F.G. Njoroge, and A. Arasappan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(17): p. 5652-5657; PMID[22858143].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22877872">Sugar-modified Derivatives of Cytostatic 7-(HET)Aryl-7-deazaadenosines: 2&#39;-C-methylribonucleosides, 2&#39;-Deoxy-2&#39;-fluoroarabinonucleosides, Arabinonucleosides and 2&#39;-Deoxyribonucleosides</a><i>.</i> Naus, P., P. Perlikova, A. Bourderioux, R. Pohl, L. Slavetinska, I. Votruba, G. Bahador, G. Birkus, T. Cihlar, and M. Hocek. Bioorganic &amp; Medicinal Chemistry, 2012. 20(17): p. 5202-5214; PMID[22877872].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22917194">Synthesis of 5&#39;-Methylene-phosphonate furanonucleoside Prodrugs: Application to D-2&#39;-Deoxy-2&#39;-alpha-fluoro-2&#39;-beta-C-methyl nucleosides</a><i>.</i> Pradere, U., F. Amblard, S.J. Coats, and R.F. Schinazi. Organic Letters, 2012. 14(17): p. 4426-4429; PMID[22917194].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22728816">Mycophenolate Mofetil Inhibits Hepatitis C Virus Replication in Human Hepatic Cells<i>.</i></a> Ye, L., J. Li, T. Zhang, X. Wang, Y. Wang, Y. Zhou, J. Liu, H.K. Parekh, and W. Ho. Virus Research, 2012. 168(1-2): p. 33-40; PMID[22728816].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22960154">The Antiviral Activity and Mechanism of Action of (S)-[3-Hydroxy-2-(phosphonomethoxy)propyl] (HPMP) nucleosides</a><i>.</i> Magee, W.C. and D.H. Evans. Antiviral Research, 2012.  <b>[Epub ahead of print]</b>; PMID[22960154].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0831-091312.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=8&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">A Cell-based, Microplate Colorimetric Screen Identifies 7,8-Benzoflavone and Green Tea Gallate Catechins as Inhibitors of the Hepatitis C Virus</a><i>.</i> Fukazawa, H., T. Suzuki, T. Wakita, and Y. Murakami. Biological &amp; Pharmaceutical Bulletin, 2012. 35(8): p. 1320-1327; ISI[000307039400020].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=7&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">Anti-Hepatitis B Virus Activity of Chickweed [Stellaria media (L.) Vill.] Extracts in HepG2.2.15 Cells</a><i>.</i> Ma, L.H., J. Song, Y.Q. Shi, C.M. Wang, B. Chen, D.H. Xie, and X.B. Jia. Molecules, 2012. 17(7): p. 8633-8646; ISI[000306752700076].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=6&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">MAP-kinase Regulated Cytosolic Phospholipase A2 Activity Is Essential for Production of Infectious Hepatitis C Virus Particles</a><i>.</i> Menzel, N., W. Fischl, K. Hueging, D. Bankwitz, A. Frentzen, S. Haid, J. Gentzsch, L. Kaderali, R. Bartenschlager, and T. Pietschmann. Plos Pathogens, 2012. 8(7): e1002829; ISI[000306837700046].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=5&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">Sangamides, a New Class of Cyclophilin-inhibiting Host-targeted Antivirals for Treatment of HCV Infection</a><i>.</i> Moss, S.J., M. Bobardt, P. Leyssen, N. Coates, U. Chatterji, D.J. Xie, T. Foster, J.L. Liu, M. Nur-e-Alam, D. Suthar, Y.S. Chen, T. Warneck, M.Q. Zhang, J. Neyts, P. Gallay, B. Wilkinson, and M.A. Gregory. MedChemComm, 2012. 3(8): p. 938-943; ISI[000306759900011].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=3&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">A Human Claudin-1-Derived Peptide Inhibits Hepatitis C Virus Entry</a><i>.</i> Si, Y.H., S.F. Liu, X.Y. Liu, J.L. Jacobs, M. Cheng, Y.Q. Niu, Q. Jin, T.Y. Wang, and W. Yang. Hepatology, 2012. 56(2): p. 507-515; ISI[000306804500014].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://apps.webofknowledge.com.proxy-um.researchport.umd.edu/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=2&amp;SID=4DF11kAdPa36OmLEKae&amp;page=1&amp;doc=1">Antiviral Lectins from Red and Blue-green Algae Show Potent in Vitro and in Vivo Activity against Hepatitis C Virus</a><i>.</i> Takebe, Y., C.J. Saucedo, G. Lund, R. Uenishi, S. Hase, T. Tsuchiura, N. Knetman, K. Ramessar, D.L.J. Tyrrell, M. Shirakura, T. Wakita, J.B. McMahon, and B.R. O&#39;Keefe. Planta Medica, 2012. 78(11): p. 1060-1060; ISI[000307042800116].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0831-091312.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
