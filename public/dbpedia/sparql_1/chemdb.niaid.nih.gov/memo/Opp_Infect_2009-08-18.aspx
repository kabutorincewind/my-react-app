

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-08-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kpPqc7x6oAfZMbe8pM9FrbQcpP3jAbdvJ+WgPsFcHF+exeQ38MYY8k5PL+t9JzuMvJMTPYM5GljB9aXvsCDVgN+ga30n7NVgAseF31HewlL+01s4JL/7iZpEYIo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F29F93EB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">August 5, 2009 - August 18, 2009</p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19686813?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A novel antifungal anthraquinone from seeds of Aegle marmelos Correa (family Rutaceae).</a></p>

    <p class="NoSpacing">Mishra BB, Kishore N, Tiwari VK, Singh DD, Tripathi V.</p>

    <p class="NoSpacing">Fitoterapia. 2009 Aug 14. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19686813 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19539408?ordinalpos=88&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">New azoles with potent antifungal activity: design, synthesis and molecular docking.</a></p>

    <p class="NoSpacing">Che X, Sheng C, Wang W, Cao Y, Xu Y, Ji H, Dong G, Miao Z, Yao J, Zhang W.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Oct;44(10):4218-26. Epub 2009 May 24.</p>

    <p class="NoSpacing">PMID: 19539408 [PubMed - in process]</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19671445?ordinalpos=34&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The Mycobacterium tuberculosis Proteasome: More Than Just a Barrel-shaped Protease.</a></p>

    <p class="NoSpacing">Cerda-Maira F, Darwin KH.</p>

    <p class="NoSpacing">Microbes Infect. 2009 Aug 8. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19671445 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19620006?ordinalpos=71&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Development of antitubercular compounds based on a 4-quinolylhydrazone scaffold. Further structure-activity relationship studies.</a></p>

    <p class="NoSpacing">Gemma S, Savini L, Altarelli M, Tripaldi P, Chiasserini L, Coccone SS, Kumar V, Camodeca C, Campiani G, Novellino E, Clarizio S, Delogu G, Butini S.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Aug 15;17(16):6063-72. Epub 2009 Jun 27.</p>

    <p class="NoSpacing">PMID: 19620006 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19560924?ordinalpos=78&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Functionalized 3-amino-imidazo[1,2-a]pyridines: a novel class of drug-like Mycobacterium tuberculosis glutamine synthetase inhibitors.</a></p>

    <p class="NoSpacing">Odell LR, Nilsson MT, Gising J, Lagerlund O, Muthas D, Nordqvist A, Karlén A, Larhed M.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Aug 15;19(16):4790-3. Epub 2009 Jun 14.</p>

    <p class="NoSpacing">PMID: 19560924 [PubMed - in process]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19540630?ordinalpos=87&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and evaluation of in vitro anti-microbial and anti-tubercular activity of 2-styryl benzimidazoles.</a></p>

    <p class="NoSpacing">Shingalapur RV, Hosamani KM, Keri RS.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Oct;44(10):4244-8. Epub 2009 May 28.</p>

    <p class="NoSpacing">PMID: 19540630 [PubMed - in process]</p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">7. Zhao, YJJ., et al.</p>

    <p class="NoSpacing">Discovery and Development of the Covalent Hydrates of Trifluoromethylated Pyrazoles as Riboflavin Synthase Inhibitors with Antibiotic Activity Against Mycobacterium tuberculosis</p>

    <p class="NoSpacing">JOURNAL OF ORGANIC CHEMISTRY  2009 (August) 74: 5297 - 5303 </p> 

    <p class="ListParagraph">8. Chimenti, F., et al.</p>

    <p class="NoSpacing">Synthesis and Evaluation of 4-Acyl-2-thiazolylhydrazone Derivatives for Anti-Toxoplasma Efficacy in Vitro</p>

    <p class="NoSpacing">JOURNAL OF MEDICINAL CHEMISTRY 2009 (August) 52: 4574 - 4577 </p> 

    <p class="ListParagraph">9. Maurya, SK., et al.</p>

    <p class="NoSpacing">Triazole Inhibitors of Cryptosporidium parvum Inosine 5 &#39;-Monophosphate Dehydrogenase</p>

    <p class="NoSpacing">JOURNAL OF MEDICINAL CHEMISTRY 2009 (August) 52: 4623 - 4630 </p> 

    <p class="ListParagraph">10. Gangjee, A., et al.</p>

    <p class="NoSpacing">Design, Synthesis, and X-ray Crystal Structure of Classical and Nonclassical 2-Amino-4-oxo-5-substituted-6-ethylthieno[2,3-d]pyrimidines as Dual Thymidylate Synthase and Dihydrofolate Reductase Inhibitors and as Potential Antitumor Agents</p>

    <p class="NoSpacing">JOURNAL OF MEDICINAL CHEMISTRY 2009 (August) 52: 4892 - 4902</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
