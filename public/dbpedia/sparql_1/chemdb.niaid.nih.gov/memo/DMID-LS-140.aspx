

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-140.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eSi7w9qUObprMviQozICgR7BsbSuv/N3aDBE2BILacgU83JQSUib30np4Zg/J1oVwGXDjuUlXT1hPMRta1FA0lLDs/xM+X1J/h44VQr8HAbwyPay0nmDiErMX6s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7958406B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-140-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60358   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Antiviral strategies against human coronaviruses</p>

    <p>          Pyrc, K, Berkhout, B, and van der Hoek, L</p>

    <p>          Infect Disord Drug Targets <b>2007</b>.  7(1): 59-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346212&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346212&amp;dopt=abstract</a> </p><br />

    <p>2.     60359   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Novel targets for the development of anti-herpes compounds</p>

    <p>          Greco, A, Diaz, JJ, Thouvenot, D, and Morfin, F</p>

    <p>          Infect Disord Drug Targets <b>2007</b>.  7(1): 11-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17346207&amp;dopt=abstract</a> </p><br />

    <p>3.     60360   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Specific Inhibition of Hbv Replication in Vitro and in Vivo With Expressed Long Hairpin Rna</p>

    <p>          Weinberg, M. <i>et al.</i></p>

    <p>          Molecular Therapy <b>2007</b>.  15(3): 534-541</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244405700017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244405700017</a> </p><br />

    <p>4.     60361   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Antivirals of Ethnomedicinal Origin: Structure-Activity Relationship and Scope</p>

    <p>          Chattopadhyay, D. and Naik, T.</p>

    <p>          Mini-Reviews in Medicinal Chemistry <b>2007</b>.  7(3): 275-301</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244158200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244158200006</a> </p><br />

    <p>5.     60362   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          1,4-Benzodiazepines as Inhibitors of Respiratory Syncytial Virus. The Identification of a Clinical Candidate</p>

    <p>          Henderson, EA, Alber, DG, Baxter, RC, Bithell, SK, Budworth, J, Carter, MC, Chubb, A, Cockerill, GS, Dowdell, VC, Fraser, IJ, Harris, RA, Keegan, SJ, Kelsey, RD, Lumley, JA, Stables, JN, Weerasekera, N, Wilson, LJ, and Powell, KL</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17341059&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17341059&amp;dopt=abstract</a> </p><br />

    <p>6.     60363   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Functional characterization of cis and trans protease activity of the flavivirus NS2B-NS3 protease</p>

    <p>          Bera, AK, Kuhn, RJ, and Smith, JL</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337448&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337448&amp;dopt=abstract</a> </p><br />

    <p>7.     60364   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Anti-Viral Effects, Pharmacokinetics and Safety of Two Human Monoclonal Anti-Hbs (Libivirumab/Exbivirumab) Versus Hepatitis B Immune Globulin (Hbig) in Hepatits B Virus (Hbv) in Liver Transplant (Lt) Recipients</p>

    <p>          Shouval, D. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 196A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300023</a> </p><br />

    <p>8.     60365   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          An efficient synthesis of 3-fluoro-5-thio-xylofuranosyl nucleosides of thymine, uracil, and 5-fluorouracil as potential antitumor or/and antiviral agents</p>

    <p>          Tsoukala, E, Agelis, G, Dolinsek, J, Botic, T, Cencic, A, and Komiotis, D</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17337193&amp;dopt=abstract</a> </p><br />

    <p>9.     60366   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Expression and purification of a two-component flaviviral proteinase resistant to autocleavage at the NS2B-NS3 junction region</p>

    <p>          Shiryaev, Sergey A, Aleshin, Alexander E, Ratnikov, Boris I, Smith, Jeffrey W, Liddington, Robert C, and Strongin, Alex Y</p>

    <p>          Protein Expression and Purification <b>2007</b>.  52(2): 334-339</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WPJ-4MG5WBS-2/2/62d8c08d90c5dc149c1cea734e4bae7c">http://www.sciencedirect.com/science/article/B6WPJ-4MG5WBS-2/2/62d8c08d90c5dc149c1cea734e4bae7c</a> </p><br />

    <p>10.   60367   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of nucleoside analogues having 6-chloropurine as anti-SARS-CoV agents</p>

    <p>          Ikejiri, M, Saijo, M, Morikawa, S, Fukushi, S, Mizutani, T, Kurane, I, and Maruyama, T</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17336519&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17336519&amp;dopt=abstract</a> </p><br />

    <p>11.   60368   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Inhibitory Activity of the 2,4-Diamino-6-[2-(Phosphonomethoxy)Ethoxy]-Pyrimidine (Pmeo) Against Wild Type and Drug Resistant Mutants of Hbv</p>

    <p>          Brunelle, M. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 223A-224A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300095">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300095</a> </p><br />
    <br clear="all">

    <p>12.   60369   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Antiviral effect of octyl gallate against influenza and other RNA viruses</p>

    <p>          Yamasaki, H, Uozaki, M, Katsuyama, Y, Utsunomiya, H, Arakawa, T, Higuchi, M, Higuti, T, and Koyama, AH</p>

    <p>          Int J Mol Med <b>2007</b>.  19(4): 685-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17334645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17334645&amp;dopt=abstract</a> </p><br />

    <p>13.   60370   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Discovery, Characterization and Anti-Hcv Activity of a Novel Ns5b Inhibitor</p>

    <p>          Wagner, R. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 236A-237A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300129">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300129</a> </p><br />

    <p>14.   60371   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Characterization of the Intracellular Metabolism of [beta]-d-2&#39;-Deoxy-2&#39;-Fluoro-2&#39;-C-Methyl-Cytidine and the Inhibition of HCV Polymerase NS5B by its 5&#39;-Triphosphate Species</p>

    <p>          Ma, Han, Jiang, Wen-Rong, Robledo, Nicole, Leveque, Vincent, Ali, Samir, Smith, David, Masjedizadeh, Mohammad, Lara-Jaime, Teresa, Cammack, Nick, Klumpp, Klaus, and Symons, Julian</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-V/2/b924071a60466bcc30a87bdc40f6de20">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-V/2/b924071a60466bcc30a87bdc40f6de20</a> </p><br />

    <p>15.   60372   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Hepatitis C Virus Infection-Pathobiology and Implications for New Therapeutic Options</p>

    <p>          Davis, GL, Krawczynski, K, and Szabo, G</p>

    <p>          Dig Dis Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17333350&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17333350&amp;dopt=abstract</a> </p><br />

    <p>16.   60373   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Sub Micromolar Inhibitors of HCV Generated from Inactive Nucleosides by Application of ProTide Technology</p>

    <p>          McGuigan, Christopher, Perrone, Plinio, Luoni, Giovanna, Kelleher, Mary Rose, Daverio, Felice, Angell, Annette, Mulready, Sinead, Congiatu, Costantino, Rajyaguru, Sonal, Martin, Joseph, Leveque, Vincent, Le Pogam, Sophie, Najera, Isabel, Klumpp, Klaus, and Smith, David</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-W/2/d6f610f40226ba9e4c6902e9e026157c">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-W/2/d6f610f40226ba9e4c6902e9e026157c</a> </p><br />
    <br clear="all">

    <p>17.   60374   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Substituted Imidazopyridines as Potent Inhibitors of Hepatitis C Virus Replication that Target the Viral Polymerase</p>

    <p>          Vliegen, Inge, Paeshuyse, Jan, Lehman, Laura S, Zhong, Weidong, Roofthooft, Sofie, Dutartre, Helene, Selisko, Barbara, Canard, Bruno, Boddeker, Nina, Bondy, Steven, Oare, David, De Clercq, Erik, Lee, William A, Purstinger, Gerhard, and Neyts, Johan</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-Y/2/2ad0c9e1d28b5d678adce11ed41a39f3">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-Y/2/2ad0c9e1d28b5d678adce11ed41a39f3</a> </p><br />

    <p>18.   60375   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          6-Hydrazinopurine 2&#39;-methyl ribonucleosides and their 5&#39;-monophosphate prodrugs as potent hepatitis C virus inhibitors</p>

    <p>          Gunic, E, Chow, S, Rong, F, Ramasamy, K, Raney, A, Yunzhi, Li D, Huang, J, Hamatake, RK, Hong, Z, and Girardet, JL</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331718&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17331718&amp;dopt=abstract</a> </p><br />

    <p>19.   60376   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Cowaniin, a C-glucosidic ellagitannin dimer linked through catechin from Cowania mexicana</p>

    <p>          Ito, H, Miyake, M, Nishitani, E, Miyashita, K, Yoshimura, M, Yoshida, T, Takasaki, M, Konoshima, T, Kozuka, M, and Hatano, T</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2007</b>.  55(3): 492-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329901&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17329901&amp;dopt=abstract</a> </p><br />

    <p>20.   60377   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          7-Deaza Neplanocin Analogs Inhibit Hepatitis C Virus (HCV) in Vitro</p>

    <p>          Kim, HJ, Wang, JN, Huang, ZH, Murray, MG, Schinazi, RF, and Chu, CK</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1X/2/60d250a629a833572f8ba696a73da7d3">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1X/2/60d250a629a833572f8ba696a73da7d3</a> </p><br />

    <p>21.   60378   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Potent HCV NS5B Polymerase Inhibitors Derived From 5-Hydroxy-3(2H)-Pyridazinones: Part 2: Variation of the 2- and 6- Pyridazinone Substituents</p>

    <p>          Zhou, Y, Li, L-S, Webber, S, Ayida, B, Bertolini, T, Sun, Z, Zhao, J, Stankovic, N, Patel, R, Li, B, LeBrun, L, Kamran, R, Sergeeva, M, Bartkowski, D, and Khandurina, J</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-24/2/8a1353d17b4aafc6ad331974664f1a06">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-24/2/8a1353d17b4aafc6ad331974664f1a06</a> </p><br />
    <br clear="all">

    <p>22.   60379   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Discovery of Two Novel Classes of Inhibitors of Hepatitis C Virus (HCV) Replication utilizing a Dicistronic Reporter HCV Replicon High Throughput Assay</p>

    <p>          Hao, Weidong, Weady, Peter, Maldonado, Fausto, Patick, Amy, and Duggal, Rohit</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 2874</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2R/2/2d89ee9fb2b7b0f84e0b43c38f04c9d8">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2R/2/2d89ee9fb2b7b0f84e0b43c38f04c9d8</a> </p><br />

    <p>23.   60380   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          The Antiviral Efficacy of an Hcv Polymerase Inhibitor in the Chimpanzee Model: Genotypic and Phenotypic Analyses</p>

    <p>          Chen, C. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 342A-343A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300407">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300407</a> </p><br />

    <p>24.   60381   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          The Cyclophilin Inhibitor Debio-025 Is a Potent Inhibitor of Hepatitis C Virus Replication in Vitro</p>

    <p>          Paeshuyse, J. <i> et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 346A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300417">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300417</a> </p><br />

    <p>25.   60382   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          In Vitro Antiviral Effects of Combinations of Abbott Hcv Polymerase Inhibitors With Ifn or Ns3/4a Protease Inhibitors</p>

    <p>          Koev, G. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 346A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300418">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362300418</a> </p><br />

    <p>26.   60383   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Finding new medicines for flaviviral targets</p>

    <p>          Keller, TH, Chen, YL, Knox, JE, Lim, SP, Ma, NL, Patel, SJ, Sampath, A, Wang, QY, Yin, Z, and Vasudevan, SG</p>

    <p>          Novartis Found Symp <b>2006</b>.  277: 102-14; discussion 114-9, 251-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319157&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319157&amp;dopt=abstract</a> </p><br />

    <p>27.   60384   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Towards the design of flavivirus helicase/NTPase inhibitors: crystallographic and mutagenesis studies of the dengue virus NS3 helicase catalytic domain</p>

    <p>          Xu, T, Sampath, A, Chao, A, Wen, D, Nanao, M, Luo, D, Chene, P, Vasudevan, SG, and Lescar, J</p>

    <p>          Novartis Found Symp <b>2006</b>.  277: 87-97; discussion 97-101, 251-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319156&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319156&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   60385   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Enhanced Antiviral Activity of Hexadecyloxypropyl-PME-N6-Cyclopropyl-diaminopurine Against Herpesviruses, Hepatitis B Virus and Vaccinia Virus, In Vitro</p>

    <p>          Andrei, Graciela, Snoeck, Robert, Neyts, Johan, De Clercq, Erik, Valiaeva, Nadejda, Beadle, James, and Hostetler, Karl</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-47/2/e4bf5519b5d30634e74e38a81ff9d704">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-47/2/e4bf5519b5d30634e74e38a81ff9d704</a> </p><br />

    <p>29.   60386   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Development of novel antivirals against flaviviruses</p>

    <p>          Patkar, CG and Kuhn, RJ</p>

    <p>          Novartis Found Symp <b>2006</b>.  277: 41-52; discussion 52-6, 71-3, 251-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319153&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17319153&amp;dopt=abstract</a> </p><br />

    <p>30.   60387   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          In Vitro Antiviral Interactions of a Novel Hcv Inhibitor R1479 With Interferon Alpha-2a, Ribavirin and Other Hcv Inhibitors</p>

    <p>          Jiang, W. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 533A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302023</a> </p><br />

    <p>31.   60388   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Hepatitis C virus infection, cryoglobulinaemia, and beyond</p>

    <p>          Sansonno, D, Carbone, A, De, Re V, and Dammacco, F</p>

    <p>          Rheumatology (Oxford) <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17317717&amp;dopt=abstract</a> </p><br />

    <p>32.   60389   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p><b>          In Vitro Synergistic Antiviral Activity of Itmn-191, an Orally Active Inhibitor of the Hepatitis C Virus (Hcv) Ns3/4a Protease, in Combination With Peg-Interferon Alfa-2a</b> </p>

    <p>          Tan, H., Seiwert, S., and Blatt, L.</p>

    <p>          Hepatology <b>2006</b>.  44(4): 534A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302027</a> </p><br />

    <p>33.   60390   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Preclinical Evaluation of Scy-635, a Cyclophilin Inhibitor With Potent Anti-Hcv Activity</p>

    <p>          Houck, D. and Hopkins, S.</p>

    <p>          Hepatology <b>2006</b>.  44(4): 534A-535A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302028</a> </p><br />
    <br clear="all">

    <p>34.   60391   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Induction of Resistance by a Novel Class of Compounds Active Against Hepatitis C Virus With No Cross-Resistance to Ns3 Protease and Ns5b Polymerase Inhibitors</p>

    <p>          Yang, W. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 536A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302031</a> </p><br />

    <p>35.   60392   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          A Common Molecular Pathway Leads to Inhibition of Interferon-Alpha Signaling in Both Hepatitis B and Hepatitis C Virus Infection</p>

    <p>          Christen, V. and Heim, M.</p>

    <p>          Hepatology <b>2006</b>.  44(4): 546A-547A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302059">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302059</a> </p><br />

    <p>36.   60393   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Synergistic copathogens--HIV-1 and HSV-2</p>

    <p>          Corey, L</p>

    <p>          N Engl J Med <b>2007</b>.  356(8): 854-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314346&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17314346&amp;dopt=abstract</a> </p><br />

    <p>37.   60394   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          The Cyclophilin Inhibitor Debio-025 Has a Potent Dual Anti-Hiv and Anti-Hcv Activity in Treatment-Naive Hiv/Hcv Co-Infected Subjects</p>

    <p>          Flisiak, R. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 609A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302224">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302224</a> </p><br />

    <p>38.   60395   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Interim Antiviral and Safety Data With Albumin Interferon Alfa-2b Combined With Ribavirin in a Phase 2b Study Conducted in a Genotype 1, Ifn-Naive, Chronic Hepatitis C Population</p>

    <p>          Mchutchison, J. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 614A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302235">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302235</a> </p><br />

    <p>39.   60396   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Current Status of Subjects Receiving Peg-Interferon-Alfa-2a (Peg-Ifn) and Ribavirin (Rbv) After a 14-Day Study of the Hepatitis C Protease Inhibitor Telaprevir (Vx-950), With Peg-Ifn</p>

    <p>          Forestier, N. <i> et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 614A-615A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302236">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302236</a> </p><br />
    <br clear="all">

    <p>40.   60397   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Combined Anti-Influenza Virus Effect of a Plant Polyphenol-Rich Extract and Ribavirin</p>

    <p>          Serkedjieva, Julia and Teodosieva, Ani</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1N/2/3e9fabc9fb7e3fd0e8942a451061591b">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1N/2/3e9fabc9fb7e3fd0e8942a451061591b</a> </p><br />

    <p>41.   60398   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Chloroquine Possesses a Potent Inhibitory Effect of Replication of Hcv Replicon</p>

    <p>          Mizui, T. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 620A-621A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302251">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302251</a> </p><br />

    <p>42.   60399   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Anti-Influenza A Synergistic Combination Effect of Rimantadine and Oseltamivir in Mice</p>

    <p>          Simeonova, Lora, Galabov, Angel S, and Gegova, Galina</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1P/2/e4c9aa55b3c8d3576fe6f87925bb5b0a">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1P/2/e4c9aa55b3c8d3576fe6f87925bb5b0a</a> </p><br />

    <p>43.   60400   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          A Novel Class of Amphipathic Dna Polymers Inhibits Hepatitis C Virus Infection by Blocking Viral Entry</p>

    <p>          Matsumura, T. <i> et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 693A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302444">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302444</a> </p><br />

    <p>44.   60401   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p><b>          Susceptibility of German Porcine H3N2 Influenza A viruses Against Existing Antiviral Drugs</b> </p>

    <p>          Bauer, Katja, Schrader, Christina, Suess, Jochen, Wutzler, Peter, and Schmidtke, Michaela</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2J/2/b1e80aaf103e90f970997e34929af1d3">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2J/2/b1e80aaf103e90f970997e34929af1d3</a> </p><br />

    <p>45.   60402   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Synthesis of Three Novel Nucleoside Analogs as Hepatitis B Virus Specific Antiviral Reagents</p>

    <p>          Niu, J. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 699A-700A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302461">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302461</a> </p><br />

    <p>46.   60403   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Pharmacodynamics of NPI-5291, an Adamantane Class Compound, for Influenza A Viruses</p>

    <p>          McSharry, James, Zager, Kris, Weng, Qingmei, Chernoff, David, and Drusano, George</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2Y/2/7e20b75b882b9aa88fb8fdf48c34a0b5">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-2Y/2/7e20b75b882b9aa88fb8fdf48c34a0b5</a> </p><br />

    <p>47.   60404   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Design, Synthesis, and Biological Evaluation of Novel Anti-VZV Agents</p>

    <p>          Derudas, Marco, McGuigan, Christopher, Snoeck, Robert, Andrei, Graciella, De Clercq, Erik, and Balzarini, Jan</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 200</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3W/2/30af3db89dfc6af5d29f14837ef33e5d">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-3W/2/30af3db89dfc6af5d29f14837ef33e5d</a> </p><br />

    <p>48.   60405   DMID-LS-140; PUBMED-DMID-3/12/2007</p>

    <p class="memofmt1-2">          In vivo and in vitro antiviral activity of hyperoside extracted from Abelmoschus manihot (L) medik</p>

    <p>          Wu, LL, Yang, XB, Huang, ZM, Liu, HZ, and Wu, GX</p>

    <p>          Acta Pharmacol Sin <b>2007</b>.  28(3): 404-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17303004&amp;dopt=abstract</a> </p><br />

    <p>49.   60406   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          The Ugi reaction in the generation of new nucleosides as potential antiviral and antileishmanial agents</p>

    <p>          Fan, Xuesen, Zhang, Xinying, Bories, Christian, Loiseau, Philippe M, and Torrence, Paul F</p>

    <p>          Bioorganic Chemistry <b>2007</b>.  35(2): 121-136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBT-4KYXHNG-1/2/7d6edcbe7f63476d848de1d59b238b92">http://www.sciencedirect.com/science/article/B6WBT-4KYXHNG-1/2/7d6edcbe7f63476d848de1d59b238b92</a> </p><br />

    <p>50.   60407   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Practical Synthesis of (-)-Carbocyclic Cytosine (Carbodine) and its In Vitro Antiviral Activity against Venezuelan Equine Encephalitis (VEE) Virus and Yellow Fever Virus</p>

    <p>          Rao, JR, Julander, JG, Sidwell, RW, and Chu, CK</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1S/2/63dcafc4d23615ff696fe227513f76f6">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-1S/2/63dcafc4d23615ff696fe227513f76f6</a> </p><br />

    <p>51.   60408   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Antiviral Activities of New Cidofovir Analogs Against Camelpox Virus, Used as a Model of Variola Virus, in Human Skin Equivalent Cultures</p>

    <p>          Duraffour, Sophie, Snoeck, Robert, Van Den Oord, Joost, Krecmerova, Marcela, Holy, Antonin, Crance, Jean-Marc, Garin, Daniel, De Clercq, Erik, and Andrei, Graciela</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4D/2/438d0b8934fdc6707ab3df948cb68935">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4D/2/438d0b8934fdc6707ab3df948cb68935</a> </p><br />
    <br clear="all">

    <p>52.   60409   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Development of a Cell-Based Assay for Identification of Viral Entry Inhibitors Against SARS-CoV by High Throughput Screening (HTS)</p>

    <p>          Zhang, Chengsheng, Feng, Yan, Wong, Gillian, Wang, Liping, Cechetto, Jonathan, Blanchard, Jan, Brown, Eric, and Mahony, James</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4V/2/15d35b90d0b48a47d7131ffa9505c291">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-4V/2/15d35b90d0b48a47d7131ffa9505c291</a> </p><br />

    <p>53.   60410   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          The Combination of Anti-poxvirus Compounds ST-246 and TTP-018 are Synergistic In Vitro</p>

    <p>          Chen, Yali, Harver, Chris, Yang, Guang, Hruby, Dennis, Andrews, Robert, and Jordan, Robert</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-51/2/508812d13fb8b3ca0d8430de3d8f34c1">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-51/2/508812d13fb8b3ca0d8430de3d8f34c1</a> </p><br />

    <p>54.   60411   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Bicyclol for Chronic Hepatitis C</p>

    <p>          Yang, X. <i>et al.</i></p>

    <p>          Cochrane Database of Systematic Reviews <b>2007</b>.(1)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243747900055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243747900055</a> </p><br />

    <p>55.   60412   DMID-LS-140; EMBASE-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Efficacy of 5-Halogenated 2&#39;-Deoxyuridines on Vaccinia Virus Thymidine Kinase Positive and Negative Strains, and Influence of Cell Type on Antiviral Potency</p>

    <p>          Smee, Donald, Humphreys, Daniel, Hurst, Brett, and Sidwell, Robert</p>

    <p>          Antiviral Research <b>2007</b>.  In Press, Uncorrected Proof: 136</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4N3X611-58/2/a509eb2a23a31f14b489b2c8f30ccaf1">http://www.sciencedirect.com/science/article/B6T2H-4N3X611-58/2/a509eb2a23a31f14b489b2c8f30ccaf1</a> </p><br />

    <p>56.   60413   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Neuraminidase Inhibitors Reduce Nitric Oxide Production in Influenza Virus-Infected and Gamma Interferon-Activated Raw 264.7 Macrophages</p>

    <p>          Kacergius, T. <i> et al.</i></p>

    <p>          Pharmacological Reports <b>2006</b>.  58(6): 924-930</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244233800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244233800016</a> </p><br />

    <p>57.   60414   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          A Line Probe Assay (Lipa) for Detecting Novel Hepatitis B Drug Resistance Mutations Associated With Entecavir, Tenofovir and Adefovir Dipivoxil Therapy</p>

    <p>          Doutreloigne, J. <i>et al.</i></p>

    <p>          Hepatology <b>2006</b>.  44(4): 552A-553A</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302072">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241362302072</a> </p><br />
    <br clear="all">

    <p>58.   60415   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Synthesis and Biological Activation of an Ethylene Glycol-Linked Amino Acid Conjugate of Cyclic Cidofovir</p>

    <p>          Eriksson, U. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(3): 583-586</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244170700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000244170700001</a> </p><br />

    <p>59.   60416   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Ifn-Stimulated Gene 15 Functions as a Critical Antiviral Molecule Against Influenza, Herpes, and Sindbis Viruses</p>

    <p>          Lenschow, D. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2007</b>.  104(4): 1371-1376</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243849900047">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243849900047</a> </p><br />

    <p>60.   60417   DMID-LS-140; WOS-DMID-3/12/2007</p>

    <p class="memofmt1-2">          Antiviral Activity of Diverse Classes of Broad-Acting Agents and Natural Compounds in Hhv-6-Infected Lymphoblasts</p>

    <p>          Naesens, L. and De Clercq, E.</p>

    <p>          Journal of Clinical Virology <b>2006</b>.  37: S112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243845000076">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243845000076</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
