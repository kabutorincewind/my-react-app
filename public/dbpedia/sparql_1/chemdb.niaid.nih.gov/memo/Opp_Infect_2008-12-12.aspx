

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2008-12-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Zh3/nIx+8j3jevJslW1u3cDxlzEmfD1nSPYpDsTBvVjMuYyCVBwT03FY+IEICVMIQBlH5csqQtVyym8WzSobwBEliU5Whwx25dSl5fffjKTuLdJiPlccyxjBl0U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CCFB676F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Pathogens Citation List: November 27 - December 10, 2008</h1>

    <p class="memofmt2-1">Opportunistic protozoa (pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma)          </p>

    <p class="memofmt2-1">&nbsp;</p>

    <p class="memofmt2-1">&nbsp;</p>

    <p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19043212?ordinalpos=13&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-toxoplasmosis Effects of Oleuropein Isolated from Fraxinus rhychophylla.</a></p>

    <p>Jiang JH, Jin CM, Kim YC, Kim HS, Park WC, Park H.</p>

    <p>Biol Pharm Bull. 2008 Dec;31(12):2273-6.</p>

    <p>PMID: 19043212          </p>

    <p>&nbsp;</p>

    <p>2. <a href="/pubmed/18824607?ordinalpos=24&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of Toxoplasma gondii by indirubin and tryptanthrin analogs.</a></p>

    <p>Krivogorsky B, Grundt P, Yolken R, Jones-Brando L.</p>

    <p>Antimicrob Agents Chemother. 2008 Dec;52(12):4466-9. Epub 2008 Sep 29.</p>

    <p>PMID: 18824607 [PubMed - in process]            </p>

    <p class="title1">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19067555?ordinalpos=5&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Lanostane-Type Triterpenes from the Mushroom Astraeus pteridis with Antituberculosis Activity.</a></p>

    <p class="authors1">Stanikunaite R, Radwan MM, Trappe JM, Fronczek F, Ross SA.</p>

    <p class="source1">J Nat Prod. 2008 Dec 9. [Epub ahead of print]</p>

    <p class="pmid1">PMID: 19067555 [PubMed - as supplied by publisher]    </p>

    <p class="title1">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19063713?ordinalpos=18&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Knowledge Based Identification of Potent Antitubercular Compounds Using Structure Based Virtual Screening and Structure Interaction Fingerprints.</a></p>

    <p class="authors1">Kumar A, Chaturvedi V, Bhatnagar S, Sinha S, Siddiqi MI.</p>

    <p class="source1">J Chem Inf Model. 2008 Dec 9. [Epub ahead of print]</p>

    <p class="pmid1">PMID: 19063713 [PubMed - as supplied by publisher]    </p>

    <p class="title1">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19006090?ordinalpos=185&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Highly active potential antituberculotics: 3-(4-alkylphenyl)-4-thioxo-2H-1,3-benzoxazine-2(3H)-ones and 3-(4-alkylphenyl)-2H-1,3-benzoxazine-2,4(3H)-dihiones substituted in ring-B by halogen.</a></p>

    <p class="authors1">Waisser K, Matyk J, Kunes J, Dolezal R, Kaustová J, Dahse HM.</p>

    <p class="source1">Arch Pharm (Weinheim). 2008 Dec;341(12):800-3.</p>

    <p class="pmid1">PMID: 19006090 [PubMed - in process]</p>

    <p>&nbsp;</p>

    <p class="title1">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18930654?ordinalpos=229&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Clubbed [1,2,3] triazoles by fluorine benzimidazole: a novel approach to H37Rv inhibitors as a potential treatment for tuberculosis.</a></p>

    <p class="authors1">Gill C, Jadhav G, Shaikh M, Kale R, Ghawalkar A, Nagargoje D, Shiradkar M.</p>

    <p class="source1">Bioorg Med Chem Lett. 2008 Dec 1;18(23):6244-7. Epub 2008 Oct 2.</p>

    <p class="pmid1">PMID: 18930654 [PubMed - in process]            </p>

    <p class="title1">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18930396?ordinalpos=230&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthetic chalcones as efficient inhibitors of Mycobacterium tuberculosis protein tyrosine phosphatase PtpA.</a></p>

    <p class="authors1">Chiaradia LD, Mascarello A, Purificação M, Vernal J, Cordeiro MN, Zenteno ME, Villarino A, Nunes RJ, Yunes RA, Terenzi H.</p>

    <p class="source1">Bioorg Med Chem Lett. 2008 Dec 1;18(23):6227-30. Epub 2008 Oct 5.</p>

    <p class="pmid1">PMID: 18930396 [PubMed - in process]            </p>

    <p class="title1">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18926698?ordinalpos=235&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of Arabino glycosyl triazoles as potential inhibitors of mycobacterial cell wall biosynthesis.</a></p>

    <p class="authors1">Wilkinson BL, Long H, Sim E, Fairbanks AJ.</p>

    <p class="source1">Bioorg Med Chem Lett. 2008 Dec 1;18(23):6265-7. Epub 2008 Sep 26.</p>

    <p class="pmid1">PMID: 18926698 [PubMed - in process]            </p>

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18835805?ordinalpos=257&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">In vivo efficacy and pharmacokinetics of tomopenem (CS-023), a novel carbapenem, against Pseudomonas aeruginosa in a murine chronic respiratory tract infection model.</a></p>

    <p>Morinaga Y, Yanagihara K, Nakamura S, Yamamoto K, Izumikawa K, Seki M, Kakeya H, Yamamoto Y, Yamada Y, Kohno S, Kamihira S.</p>

    <p>J Antimicrob Chemother. 2008 Dec;62(6):1326-31. Epub 2008 Oct 3.</p>

    <p>PMID: 18835805 [PubMed - in process]            </p>

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p>10. <a href="/pubmed/18621113?ordinalpos=104&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimicrobial anthraquinones from Morinda angustifolia.</a></p>

    <p>Xiang W, Song QS, Zhang HJ, Guo SP.</p>

    <p>Fitoterapia. 2008 Dec;79(7-8):501-504. Epub 2008 Jun 22.</p>

    <p>PMID: 18621113 [PubMed - as supplied by publisher]           </p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
