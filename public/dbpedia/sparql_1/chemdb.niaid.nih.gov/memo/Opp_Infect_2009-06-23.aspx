

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-06-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6NHtkxg6KYdpU1iP+E06+TRLG7lSqywKZI05Dyvbeq7OMgylqrXqI3wa5J+tydDvTtVGVGoK1czOsynSfoGDVtQmtzd53WZAM/hiNWG7/xuQqpK8rHhNNQFL658=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C71DB17E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">June  10, 2009 - June  23, 2009</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19546364?ordinalpos=19&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Dihydrophthalazine Antifolates, a Novel Family of Antibacterial Drugs: In Vitro and in Vivo Properties.</a></p>

    <p class="NoSpacing">Caspers P, Bury L, Gaucher B, Heim J, Shapiro S, Siegrist S, Schmitt-Hoffmann A, Thenoz L, Urwyler H.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jun 22. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19546364 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19545132?ordinalpos=24&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery and Development of the Covalent Hydrates of Trifluoromethylated Pyrazoles as Riboflavin Synthase Inhibitors with Antibiotic Activity Against Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Zhao Y, Bacher A, Illarionov B, Fischer M, Georg G, Ye QZ, Fanwick PE, Franzblau SG, Wan B, Cushman M.</p>

    <p class="NoSpacing">J Org Chem. 2009 Jun 22. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19545132 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19543820?ordinalpos=27&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthetic Lipopeptide MALP-2 Inhibits Intracellular Growth of Mycobacterium bovis BCG in Alveolar Macrophages-Preliminary Data.</a></p>

    <p class="NoSpacing">Jörgens G, Bange FC, Mühlradt PF, Pabst R, Maus UA, Tschernig T.</p>

    <p class="NoSpacing">Inflammation. 2009 Jun 19. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19543820 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19536781?ordinalpos=71&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Preparation and in-vitro Evaluation of 4-Benzylsulfanylpyridine-2-carbohydrazides as Potential Antituberculosis Agents.</a></p>

    <p class="NoSpacing">Herzigová P, Klime&#353;ová V, Palát K, Kaustová J, Dahse HM, Möllmann U.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Jun 17. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19536781 [PubMed - as supplied by publisher]                </p> 

    <p class="ListParagraph">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19525487?ordinalpos=111&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Dequalinium, a Novel Inhibitor of Mycobacterium tuberculosis Mycothiol Ligase Identified by High-Throughput Screening.</a></p>

    <p class="NoSpacing">Gutierrez-Lugo MT, Baker H, Shiloach J, Boshoff H, Bewley CA.</p>

    <p class="NoSpacing">J Biomol Screen. 2009 Jun 12. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19525487 [PubMed - as supplied by publisher]</p>  

    <p class="ListParagraph">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19427200?ordinalpos=150&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and antimycobacterial activity of 5-formylaminopyrimidines; analogs of antibacterial purines.</a></p>

    <p class="NoSpacing">Braendvang M, Charnock C, Gundersen LL.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jun 15;19(12):3297-9. Epub 2009 Apr 23.</p>

    <p class="NoSpacing">PMID: 19427200 [PubMed - in process]</p> 

    <p class="ListParagraph">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19394344?ordinalpos=155&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structures of glycinamide ribonucleotide transformylase (PurN) from Mycobacterium tuberculosis reveal a novel dimer with relevance to drug discovery.</a></p>

    <p class="NoSpacing">Zhang Z, Caradoc-Davies TT, Dickson JM, Baker EN, Squire CJ.</p>

    <p class="NoSpacing">J Mol Biol. 2009 Jun 19;389(4):722-33. Epub 2009 Apr 24.          </p>

    <p class="NoSpacing">PMID: 19394344 [PubMed - in process]</p>

    <h2>Antimicrobials, Antifungals (General)</h2>

    <p class="ListParagraph">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19544478?ordinalpos=8&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis, Antibacterial and Antifungal Activities of Novel 1,2,4-Triazolium Derivatives.</a></p>

    <p class="NoSpacing">Luo Y, Lu YH, Gan LL, Zhou CH, Wu J, Geng RX, Zhang YY.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Jun 19. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19544478 [PubMed - as supplied by publisher]    </p>  

    <p class="ListParagraph">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19528279?ordinalpos=26&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The In Vitro Activity of TR-700, the Active Ingredient of the Antibacterial Prodrug TR-701, a Novel Oxazolidinone Antibacterial Agent.</a></p>

    <p class="NoSpacing">Schaadt R, Sweeney D, Shinabarger D, Zurenko G.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jun 15. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19528279 [PubMed - as supplied by publisher]    </p> 

    <p class="ListParagraph">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19413326?ordinalpos=47&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of antibacterial biotin carboxylase inhibitors by virtual screening and fragment-based approaches.</a></p>

    <p class="NoSpacing">Mochalkin I, Miller JR, Narasimhan L, Thanabal V, Erdman P, Cox PB, Prasad JV, Lightle S, Huband MD, Stover CK.</p>

    <p class="NoSpacing">ACS Chem Biol. 2009 Jun 19;4(6):473-83.</p>

    <p class="NoSpacing">PMID: 19413326 [PubMed - in process]            </p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">11.   Kulkarni, A, et al.</p>

    <p class="NoSpacing">Synthesis, characterization, DNA cleavage and in vitro antimicrobial studies of La(III), Th(IV) and VO(IV) complexes with Schiff bases of coumarin derivatives</p>
    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (July) 44: 2904 - 2912                        </p> 

    <p class="ListParagraph">12.   Tandon, VK, et al.</p>

    <p class="NoSpacing">Design, synthesis and biological evaluation of novel nitrogen and sulfur containing hetero-1,4-naphthoquinones as potent antifungal and antibacterial agents</p>
    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (August) 44: 3130 - 3137</p> 

    <p class="ListParagraph">13.  Fassihi, A, et al.            </p>

    <p class="NoSpacing">Synthesis and antitubercular activity of novel 4-substituted imidazolyl-2,6-dimethyl-N-3,N-5-bisaryl-1,4-dihydropyridine-3,5-dicarbox amides
    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY 2009 (August) 44: 3253 - 3258 </p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
