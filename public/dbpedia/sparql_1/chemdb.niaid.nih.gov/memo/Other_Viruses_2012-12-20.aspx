

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-12-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3LesDA8tKbm3Iq4WQC0f1df6q5URe0vzhLvjysNnY+TJ9Kh2gbPpo2af3WsrS0NAXcWpLr37HSfY+6oHkFyg5kby2wIfVO/qKQKo3P5q2bwv3jChAFgqDlcQFKQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D1BF3851" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: December 7 - December 20, 2012</h1>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23251493">Resveratrol Prevents EBV Transformation and Inhibits the Outgrowth of EBV-immortalized Human B Cells</a>. Espinoza, J.L., A. Takami, L.Q. Trung, S. Kato, and S. Nakao. PloS One, 2012. <b>7</b>(12): p. e51306; PMID[23251493].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23123017">Synthesis and Docking Studies of Novel Antitumor Benzimidazoles</a>. Omar, M.A., Y.M. Shaker, S.A. Galal, M.M. Ali, S.M. Kerwin, J. Li, H. Tokuda, R.A. Ramadan, and H.I. El Diwani. Bioorganic &amp; Medicinal Chemistry, 2012. 20(24): p. 6989-7001; PMID[23123017].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23232323">New Amide Alkaloids from Piper longum</a>. Jiang, Z.Y., W.F. Liu, C.G. Huang, and X.Z. Huang. Fitoterapia, 2012. <b>[Epub ahead of print]</b>; PMID[23232323].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23220096">Development and in Vivo/in Vitro Evaluation of Novel Herpetrione Nanosuspension</a>. Guo, J.J., P.F. Yue, J.L. Lv, J. Han, S.S. Fu, S.X. Jin, S.Y. Jin, and H.L. Yuan. International Journal of Pharmaceutics, 2012. <b>[Epub ahead of print]</b>; PMID[23220096].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23238077">Dioscin&#39;s Antiviral Effect in Vitro</a>. Liu, C., Y. Wang, C. Wu, R. Pei, J. Song, S. Chen, and X. Chen. Virus Research, 2012. <b>[Epub ahead of print]</b>; PMID[23238077].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23159569">Discovery and Characterization of a Novel 7-Aminopyrazolo[1,5-a]pyrimidine Analog as a Potent Hepatitis C Virus Inhibitor</a>. Hwang, J.Y., M.P. Windisch, S. Jo, K. Kim, S. Kong, H.C. Kim, S. Kim, H. Kim, M.E. Lee, Y. Kim, J. Choi, D.S. Park, E. Park, J. Kwon, J. Nam, S. Ahn, J. Cechetto, J. Kim, M. Liuzzi, Z. No, and J. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7297-7301; PMID[23159569].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23230035">Boceprevir: A Recently Approved Protease Inhibitor for Hepatitis C Virus Infection</a>. Lam, J.T. and S. Jacob. American Journal of Health-System Pharmacy, 2012. 69(24): p. 2135-2139; PMID[23230035].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23142614">Synthesis and Antiviral Activity of Novel HCV NS3 Protease Inhibitors with P4 Capping Groups</a>. Li, X., Y. Liu, Y.K. Zhang, J.J. Plattner, S.J. Baker, W. Bu, L. Liu, Y. Zhou, C.Z. Ding, S. Zhang, W.M. Kazmierski, R. Hamatake, M. Duan, L.L. Wright, G.K. Smith, R.L. Jarvest, J.J. Ji, J.P. Cooper, M.D. Tallant, R.M. Crosby, K. Creech, and A. Wang. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7351-7356; PMID[23142614].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23122860">Structure Based Medicinal Chemistry Approach to Develop 4-Methyl-7-deazaadenine carbocyclic nucleosides as anti-HCV Agent</a>. Thiyagarajan, A., M.T. Salim, T. Balaraju, C. Bal, M. Baba, and A. Sharon. Bioorganic &amp; medicinal chemistry letters, 2012. 22(24): p. 7742-7747; PMID[23122860].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23242611">2-Arachidonoyl-glycerol- and Arachidonic acid-stimulated Neutrophils Release Antimicrobial Effectors against E. coli, S. aureus, HSV-1, and RSV</a>. Chouinard, F., C. Turcotte, X. Guan, M.C. Larose, S. Poirier, L. Bouchard, V. Provost, L. Flamand, N. Grandvaux, and N. Flamand. Journal of Leukocyte</p>

    <p class="plaintext">Biology, 2012. <b>[Epub ahead of print]</b>; PMID[23242611].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23141915">Synthesis of C3-arylated-3-deazauridine Derivatives with Potent anti-HSV-1 Activities</a>. Lalut, J., L. Tripoteau, C. Marty, H. Bares, N. Bourgougnon, and F.X. Felpin. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(24): p. 7461-7464; PMID[23141915].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1207-122012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311080200012">Preclinical Studies of PF-04849285, an Interferon-alpha 8 Fusion Protein for the Treatment of HCV</a>. Flores, M.V., T.P. Hickling, S. Sreckovic, M.D. Fidock, N. Horscroft, M. Katragadda, B. Savic, J. Rawal, O.E. Delpuech-Adams, N. Robas, T. Corey, L. Nelms, M. Lawton, J. Marcek, M. Stubbs, M. Westby, and G. Ciaramella. Antiviral Therapy, 2012. 17(5): p. 869-881; ISI[000311080200012]</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310952700012">Treatment with Silymarin for Hepatitis C Virus Reply</a>. Fried, M.W., R.L. Hawke, and K.R. Reddy. JAMA-Journal of the American Medical Association, 2012. 308(18): p. 1857-1857; ISI[000310952700012].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310193400005">Iron Regulator Hepcidin Exhibits Antiviral Activity against Hepatitis C Virus</a>. Liu, H.Y., T. Le Trinh, H.J. Dong, R. Keith, D. Nelson, and C. Liu. PloS One, 2012. <b>7</b>(10): p. e46631; ISI[000310193400005].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310896400001">Anti-retroviral Drugs Do Not Facilitate Hepatitis C Virus (HCV) Infection in Vitro</a>. Sandmann, L., M. Wilson, D. Back, H. Wedemeyer, M.P. Manns, E. Steinmann, T. Pietschmann, T. von Hahn, and S. Ciesek. Antiviral Research, 2012. 96(1): p. 51-58; ISI[000310668000006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000310668000010">Anti-Hepatitis C Virus E2 (HCV/E2) Glycoprotein Monoclonal Antibodies and Neutralization Interference</a>. Sautto, G., N. Mancini, R.A. Diotti, L. Solforosi, M. Clementi, and R. Burioni. Antiviral Research, 2012. 96(1): p. 82-89; ISI[000310668000010].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:0003112406000">Direct-acting Antiviral Agents for the Treatment of HCV</a>. Thompson, A.J. and S. Locarnini. Antiviral Therapy, 2012. 17(6): p. 1105-1107; ISI[0003112406000].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1207-122012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
