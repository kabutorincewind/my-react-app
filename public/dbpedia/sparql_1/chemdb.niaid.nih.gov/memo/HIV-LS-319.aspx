

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-319.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+AXlr3AwrtnOTZ6hPx+cmi4cZ5CRacyZ62GJK4WNIJ2313+jXT3ckzpPmOpQVSuihS6pn0+Kg3Ad1ED9Lde1am5iOm+I0CnjH7LA+x/KMgXqRu2/JziagkvR5yQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0E9F4983" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-319-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         45079   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          Synthesis of 2&#39;,3&#39;-Dideoxynucleoside 5&#39;-alpha-P-Borano-beta,gamma-(difluoromethylene)triphosphates and Their Inhibition of HIV-1 Reverse Transcriptase</p>

    <p>          Boyle, NA, Rajwanshi, VK, Prhavc, M, Wang, G, Fagan, P, Chen, F, Ewing, GJ, Brooks, JL, Hurd, T, Leeds, JM, Bruice, TW, and Cook, PD</p>

    <p>          J Med Chem <b>2005</b>.  48(7): 2695-2700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801860&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15801860&amp;dopt=abstract</a> </p><br />

    <p>2.         45080   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Selective Small Molecules Blocking HIV-1 Tat and Coactivator PCAF Association</p>

    <p>          Zeng, Lei, Li, Jiaming, Muller, Michaela, Yan, Sherry, Mujtaba, Shiraz, Pan, Chongfeng, Wang, Zhiyong, and Zhou, Ming-Ming</p>

    <p>          Journal of the American Chemical Society <b>2005</b>.  127(8): 2376-2377</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.         45081   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          Simple and Simultaneous Determination of the HIV-Protease Inhibitors Amprenavir, Atazanavir, Indinavir, Lopinavir, Nelfinavir, Ritonavir and Saquinavir Plus M8 Nelfinavir Metabolite and the Nonnucleoside Reverse Transcriptase Inhibitors Efavirenz and Nevirapine in Human Plasma by Reversed-Phase Liquid Chromatography</p>

    <p>          Poirier, JM, Robidou, P, and Jaillon, P</p>

    <p>          Ther Drug Monit <b>2005</b>.  27(2): 186-192</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795650&amp;dopt=abstract</a> </p><br />

    <p>4.         45082   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          Peptides derived from the reverse transcriptase of human immunodeficiency virus type 1 as novel inhibitors of the viral integrase</p>

    <p>          Oz-Gleenberg, I, Avidan, O, Goldgur, Y, Herschhorn, A, and Hizi, A</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15790559&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15790559&amp;dopt=abstract</a> </p><br />

    <p>5.         45083   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          The antimicrobial peptide dermaseptin S4 inhibits HIV-1 infectivity in vitro</p>

    <p>          Lorin, C, Saidi, H, Belaid, A, Zairi, A, Baleux, F, Hocini, H, Belec, L, Hani, K, and Tangy, F</p>

    <p>          Virology <b>2005</b>.  334(2): 264-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15780876&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15780876&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         45084   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Synthesis and Evaluation of Double-Prodrugs against HIV. Conjugation of D4T with 6-Benzyl-1-(ethoxymethyl)-5-isopropyluracil (MKC-442, Emivirine)-Type Reverse Transcriptase Inhibitors via the SATE Prodrug Approach</p>

    <p>          Petersen, Lene, Jorgensen, Per T, Nielsen, Claus, Hansen, Thomas H, Nielsen, John, and Pedersen, Erik B</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(4): 1211-1220</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.         45085   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          4-Benzyl and 4-benzoyl-3-dimethylaminopyridin-2(1H)-ones: in vitro evaluation of new C-3-amino-substituted and C-5,6-alkyl-substituted analogues against clinically important HIV Mutant Strains</p>

    <p>          Benjahad, A, Croisy, M, Monneret, C, Bisagni, E, Mabire, D, Coupa, S, Poncelet, A, Csoka, I, Guillemont, J, Meyer, C, Andries, K, Pauwels, R, de, Bethune MP, Himmel, DM, Das, K, Arnold, E, Nguyen, CH, and Grierson, DS</p>

    <p>          J Med Chem <b>2005</b>.  48(6): 1948-1964</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771439&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15771439&amp;dopt=abstract</a> </p><br />

    <p>8.         45086   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Identification of authentic inhibitors of HIV-1 integration</p>

    <p>          Witvrouw, M, Fikkert, V, Vercammen, J, Van Maele, B, Engelborghs, Y, and Debyser, Z</p>

    <p>          Current Medicinal Chemistry: Anti-Infective Agents <b>2005</b>.  4(2): 153-165</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.         45087   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Hiv protease inhibitors, compositions containing the same and their pharmaceutical uses</p>

    <p>          Alegria, Larry Andrew, Dress, Klaus Ruprecht, Huang, Buwen, Kumpf, Robert Arnold, Lewis, Kathleen Kingsley, Matthews, Jean Joo, Sakata, Sylvie Kim, and Wallace, Michael Brennan</p>

    <p>          PATENT:  WO <b>2005026114</b>  ISSUE DATE:  20050324</p>

    <p>          APPLICATION: 2004</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.       45088   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          The Novel L- and D-Amino Acid Derivatives of Hydroxyurea and Hydantoins: Synthesis, X-ray Crystal Structure Study, and Cytostatic and Antiviral Activity Evaluations</p>

    <p>          Opacic, Ninoslav, Barbaric, Monika, Zorc, Branka, Cetina, Mario, Nagl, Ante, Frkovic, Danijel, Kralj, Marijeta, Pavelic, Kresimir, Balzarini, Jan, Andrei, Graciela, Snoeck, Robert, De Clercq, Erik, Raic-Malic, Silvana, and Mintas, Mladen</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(2): 475-482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.       45089   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of novel 4&#39;,5&#39;-branched pyrimidine nucleosides</p>

    <p>          Kim, Aihong, Kooh, Dae-Ho, Ko, Ok Hyun, and Hong, Joon Hee</p>

    <p>          Yakhak Hoechi <b>2005</b>.  49(1): 20-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.       45090   HIV-LS-319; PUBMED-HIV-4/5/2005</p>

    <p class="memofmt1-2">          Nuclear translocation as a novel target for anti-HIV drugs</p>

    <p>          Haffar, O and Bukrinsky, M</p>

    <p>          Expert Rev Anti Infect Ther <b>2005</b>.  3(1): 41-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15757456&amp;dopt=abstract</a> </p><br />

    <p>13.       45091   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Compositions and methods for use of antiviral drugs in the treatment of retroviral diseases resistant to nucleoside reverse transcriptase inhibitors</p>

    <p>          Parniak, Michael, Mellors, John W, Oldfield, Eric, Tovian, Zev, and Chan, Julian Mun Weng</p>

    <p>          PATENT:  WO <b>2005023270</b>  ISSUE DATE:  20050317</p>

    <p>          APPLICATION: 2004  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (University of Pittsburgh-of the Commonwealth System of Higher Education, USA and University of Illinois)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.       45092   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Combinations of pyrimidine-containing nonnucleoside reverse transcriptase inhibitor (NNRTI) TMC278 with reverse transcriptase inhibitors for the treatment of HIV infection</p>

    <p>          Stoffels, Paul </p>

    <p>          PATENT:  WO <b>2005021001</b>  ISSUE DATE:  20050310</p>

    <p>          APPLICATION: 2004  PP: 34 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.       45093   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Structure activity relationship of disulfone-containing HIV-1 integrase inhibitors</p>

    <p>          Meadows, DChristopher, Gervay-Hague, Jacquelyn, Matthews, Timothy B, and North, Thomas W</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-338</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>16.       45094   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Discovery of the piperidine-4-carboxamide derivative TAK-220, a highly potent CCR5 antagonist anti-HIV-1 agent</p>

    <p>          Imamura, Shinichi, Ichikawa, Takashi, Nishikawa, Youichi, Hashiguchi, Shohei, Kanzaki, Naoyuki, Takashima, Katsunori, Niwa, Shinichi, Yamamoto, Yoshio, Baba, Masanori, and Sugihara, Yoshihiro</p>

    <p>          229th ACS National Meeting <b>2005</b>.  229: MEDI-090</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.       45095   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Structural and Functional Modeling of Human Lysozyme Reveals a Unique Nonapeptide, HL9, with Anti-HIV Activity</p>

    <p>          Lee-Huang, Sylvia, Maiorov, Vladimir, Huang, Philip L, Ng, Angela, Lee, Hee Chul, Chang, Young-Tae, Kallenbach, Neville, Huang, Paul L, and Chen, Hao-Chia</p>

    <p>          Biochemistry <b>2005</b>.  44(12): 4648-4655</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.       45096   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Novel HIV integrase inhibitors and HIV therapy based on drug combinations including integrase inhibitors</p>

    <p>          Robinson, WEdward, King, Peter J, and Reinecke, Manfred G</p>

    <p>          PATENT:  US <b>20050049242</b>  ISSUE DATE: 20050303</p>

    <p>          APPLICATION: 2003-17364  PP: 32 pp., Cont. of U.S. Ser. No. 647,270, abandoned.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.       45097   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Preparation of pyrido[1,2-a]pyrazine-1,8-dione derivatives as HIV integrase inhibitors</p>

    <p>          Miyazaki, Susumu, Katoh, Susumu, Adachi, Kaoru, Isoshima, Hirotaka, Kobayashi, Satoru, Matsuzaki, Yuji, Watanabe, Wataru, Yamataka, Kazunobu, Kiyonari, Shinichi, and Wamaki, Shuichi</p>

    <p>          PATENT:  WO <b>2005016927</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 355 pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.       45098   HIV-LS-319; SCIFINDER-HIV-3/29/2005</p>

    <p class="memofmt1-2">          Topological models for the prediction of anti-HIV activity of dihydro (alkylthio) (naphthylmethyl) oxopyrimidines</p>

    <p>          Lather, Viney and Madan, AK</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(5): 1599-1604</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.       45099   HIV-LS-319; WOS-HIV-3/27/2005</p>

    <p class="memofmt1-2">          Exploring the stereochemical requirements for protease inhibition by ureidopeptides</p>

    <p>          Barth, BS, Myers, AC, and Lipton, MA</p>

    <p>          JOURNAL OF PEPTIDE RESEARCH <b>2005</b>.  65(3): 352-354, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227499400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227499400005</a> </p><br />

    <p>22.       45100   HIV-LS-319; WOS-HIV-3/27/2005</p>

    <p class="memofmt1-2">          HIV-1 Vif can directly inhibit apolipoprotein B mRNA-editing enzyme catalytic polypeptide-like 3G-mediated cytidine deamination by using a single amino acid interaction and without protein degradation</p>

    <p>          Santa-Marta, M, da, Silva FA, Fonseca, AM, and Goncalves, J</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(10): 8765-8775, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227453100019</a> </p><br />

    <p>23.       45101   HIV-LS-319; WOS-HIV-3/27/2005</p>

    <p class="memofmt1-2">          Racemic and enantioselective synthesis of enol-lactone and their biological activity as potent HIV-1 protease inhibitors</p>

    <p>          Ibrahimi, S, Sauve, G, Yelle, J, and Essassi, E</p>

    <p>          COMPTES RENDUS CHIMIE <b>2005</b>.  8(1): 75-83, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227238000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227238000010</a> </p><br />
    <br clear="all">

    <p>24.       45102   HIV-LS-319; WOS-HIV-3/27/2005</p>

    <p class="memofmt1-2">          Synthesis of racemic 9-(6-and 2,6-substituted 9H-purin-9-yl)-5-oxatricyclo[4.2.1.0(3,7)]non ne-3-methanols, novel conformationally locked carbocyclic nucleosides</p>

    <p>          Hrebabecky, H, Masojidkova, M, and Holy, A</p>

    <p>          COLLECTION OF CZECHOSLOVAK CHEMICAL COMMUNICATIONS <b>2005</b>.  70(1): 103-123, 21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227529600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227529600009</a> </p><br />

    <p>25.       45103   HIV-LS-319; WOS-HIV-3/27/2005</p>

    <p class="memofmt1-2">          A novel synthetic chemokine containing D-amino acids that binds to the CXCR4 receptor and inhibits HIV-1 infection</p>

    <p>          Huang, ZW, Liu, DX, Madani, N, Kumar, S, Choi, WT, Cao, R, Li, Y, Gao, YG, Dong, CZ, Wang, J, An, J, and Sodroski, JG</p>

    <p>          BLOOD <b>2004</b>.  104(11): 174A-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225127500606">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000225127500606</a> </p><br />

    <p>26.       45104   HIV-LS-319; WOS-HIV-4/3/2005</p>

    <p class="memofmt1-2">          Plant based HIV-1 vaccine candidate: Tat protein produced in spinach</p>

    <p>          Karasev, AV, Foulke, S, Wellens, C, Rich, A, Shon, KJ, Zwierzynski, I, Hone, D, Koprowski, H, and Reitz, M</p>

    <p>          VACCINE <b>2005</b>.  23(15): 1875-1880, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227769600024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227769600024</a> </p><br />

    <p>27.       45105   HIV-LS-319; WOS-HIV-4/3/2005</p>

    <p class="memofmt1-2">          HIV-1 replication inhibitors of the styrylquinoline class: introduction of an additional carboxyl group at the C-5 position of the quinoline</p>

    <p>          Zouhiri, F, Danet, M, Benard, C, Normand-Bayle, M, Mouscadet, JF, Leh, H, Thomas, CM, Mbemba, G, d&#39;Angelo, J, and Desmaele, D</p>

    <p>          TETRAHEDRON LETTERS <b>2005</b>.  46(13): 2201-2205, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227754000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227754000006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
