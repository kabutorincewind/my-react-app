

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OJZVci5+I23/sGwKjegrWPyv18DMMBYvXnoCMac5r/YiMbL4BDa2Fosc36bwjHlqDepwzlX80Xz3JgBMDfPvIqPL+iY+A9tnJrni0PDkisoBwShlUggprH8TNsU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5E06DC1A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: December, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p>1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28662527">Anti-inflammatory and anti-HIV Compounds from Swertia bimaculata.</a> Dong, M., L.Q. Quan, W.F. Dai, S.L. Yan, C.H. Chen, X.Q. Chen, and R.T. Li. Planta Medica, 2017. 83(17): p. 1368-1373. PMID[28662527].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28926402">Reversing HIV Latency via Sphingosine-1-phosphate Receptor 1 Signaling.</a> Duquenne, C., S. Gimenez, A. Guigues, B. Viala, C. Boulouis, C. Mettling, D. Maurel, N. Campos, E. Doumazane, L. Comps-Agrar, J. Tazi, L. Prezeau, C. Psomas, P. Corbeau, and V. Francois. AIDS, 2017. 31(18): p. 2443-2454. PMID[28926402].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29098886">Diaryl ethers with Carboxymethoxyphenacyl Motif as Potent HIV-1 Reverse Transcriptase Inhibitors with Improved Solubility.</a> Fraczek, T., R. Kaminski, A. Krakowiak, E. Naessens, B. Verhasselt, and P. Paneth. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 33(1): p. 9-16. PMID[29098886].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29267399">Novel Mechanisms to Inhibit HIV Reservoir Seeding Using Jak Inhibitors.</a> Gavegnano, C., J.H. Brehm, F.P. Dupuy, A. Talla, S.P. Ribeiro, D.A. Kulpa, C. Cameron, S. Santos, S.J. Hurwitz, V.C. Marconi, J.P. Routy, L. Sabbagh, R.F. Schinazi, and R.P. Sekaly. Plos Pathogens, 2017. 13(12): p. e1006740. PMID[29267399].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29110408">Design, Synthesis, Biological Evaluation, and X-ray Studies of HIV-1 Protease Inhibitors with Modified P2&#39; Ligands of Darunavir.</a> Ghosh, A.K., W.S. Fyvie, M. Brindisi, M. Steffey, J. Agniswamy, Y.F. Wang, M. Aoki, M. Amano, I.T. Weber, and H. Mitsuya. ChemMedChem, 2017. 12(23): p. 1942-1952. PMID[29110408].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29214223">Novel Elvitegravir Nanoformulation Approach to Suppress the Viral Load in HIV-infected Macrophages.</a> Gong, Y., P. Chowdhury, N.M. Midde, M.A. Rahman, M.M. Yallapu, and S. Kumar. Biochemistry and Biophysics Reports, 2017. 12: p. 214-219. PMID[29214223]. PMCID[PMC5704044].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29098212">Synthesis and Biological Assessment of 3,7-Dihydroxytropolones.</a> Hirsch, D.R., D.V. Schiavone, A.J. Berkowitz, L.A. Morrison, T. Masaoka, J.A. Wilson, E. Lomonosova, H. Zhao, B.S. Patel, S.H. Datla, S.G. Hoft, S.J. Majidi, R.K. Pal, E. Gallicchio, L. Tang, J.E. Tavis, S.F.J. Le Grice, J.A. Beutler, and R.P. Murelli. Organic &amp; Biomolecular Chemistry, 2017. 16(1): p. 62-69. PMID[29098212]. PMCID[PMC5748270].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29031062">Design, Synthesis and Biological Evaluations of N-hydroxy Thienopyrimidine-2,4-diones as Inhibitors of HIV Reverse Transcriptase-associated RNase H.</a>Kankanala, J., K.A. Kirby, A.D. Huber, M.C. Casey, D.J. Wilson, S.G. Sarafianos, and Z. Wang. European Journal of Medicinal Chemistry, 2017. 141: p. 149-161. PMID[29031062]. PMCID[PMC5682218].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28949787">Exploration of Broadly Neutralizing Antibody Fragments Produced in Bacteria for the Control of HIV.</a> Lloyd, S.B., K.P. Niven, B.R. Kiefel, D.C. Montefiori, A. Reynaldi, M.P. Davenport, S.J. Kent, and W.R. Winnall. Human Vaccines &amp; Immunotherapeutics, 2017. 13(11): p. 2726-2737. PMID[28949787]. PMCID[PMC5703365].<b><br />
    [PubMed]</b>. HIV_12_2017.</p>

    <p>10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29192216">BET Inhibitors RVX-208 and PFI-1 Reactivate HIV-1 from Latency.</a> Lu, P., Y. Shen, H. Yang, Y. Wang, Z. Jiang, X. Yang, Y. Zhong, H. Pan, J. Xu, H. Lu, and H. Zhu. Scientific Reports, 2017. 7(16646): 12pp. PMID[29192216]. PMCID[PMC5709369].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29040829">Soybean-derived Bowman-Birk Inhibitor (BBI) Blocks HIV Entry into Macrophages.</a> Ma, T.C., L. Guo, R.H. Zhou, X. Wang, J.B. Liu, J.L. Li, Y. Zhou, W. Hou, and W.Z. Ho. Virology, 2018. 513: p. 91-97. PMID[29040829].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27766892">Isatin Thiazoline Hybrids as Dual Inhibitors of HIV-1 Reverse Transcriptase.</a> Meleddu, R., S. Distinto, A. Corona, E. Tramontano, G. Bianco, C. Melis, F. Cottiglia, and E. Maccioni. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 130-136. PMID[27766892].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28812250">MIV-150 and Zinc acetate Combination Provides Potent and Broad Activity against HIV-1.</a> Mizenina, O., M. Hsu, N. Jean-Pierre, M. Aravantinou, K. Levendosky, G. Paglini, T.M. Zydowsky, M. Robbiani, and J.A. Fernandez-Romero. Drug Delivery and Translational Research, 2017. 7(6): p. 859-866. PMID[28812250].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28827122">New Anionic Carbosilane Dendrons Functionalized with a DO3A Ligand at the Focal Point for the Prevention of HIV-1 Infection.</a> Moreno, S., D. Sepulveda-Crespo, F.J. de la Mata, R. Gomez, and M.A. Munoz-Fernandez. Antiviral Research, 2017. 146: p. 54-64. PMID[28827122].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000417552000003">Methoxyflavones from Marcetia taxifolia as HIV-1 Reverse Transcriptase Inhibitors.</a> Ortega, J.T., M.L. Serrano, A.I. Suarez, J. Baptista, F.H. Pujol, and H.R. Rangel. Natural Product Communications, 2017. 125(11): p. 1677-1680. ISI[000417552000003].
    <br />
    <b>[WOS]</b>. HIV_12_2017.</p>

    <p>16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29021396">Quiescence Promotes Latent HIV Infection and Resistance to Reactivation from Latency with Histone Deacetylase Inhibitors.</a> Painter, M.M., T.D. Zaikos, and K.L. Collins. Journal of Virology, 2017. 91(24): p. e01080-01017. PMID[29021396]. PMCID[PMC5709582].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29080495">Application of the Morita-Baylis-Hillman Reaction in the Synthesis of 3-[(N-Cycloalkylbenzamido)methyl]-2-quinolones as Potential HIV-1 Integrase Inhibitors.</a> Sekgota, K.C., S. Majumder, M. Isaacs, D. Mnkandhla, H.C. Hoppe, S.D. Khanye, F.H. Kriel, J. Coates, and P.T. Kaye. Bioorganic Chemistry, 2017. 75: p. 310-316. PMID[29080495].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28945945">Macromolecular Antiviral Agents against Zika, Ebola, SARS, and Other Pathogenic Viruses.</a> Schandock, F., C.F. Riber, A. Rocker, J.A. Muller, M. Harms, P. Gajda, K. Zuwala, A.H.F. Andersen, K.B. Lovschall, M. Tolstrup, F. Kreppel, J. Munch, and A.N. Zelikin. Advanced Healthcare Materials, 2017. 6(23). PMID[28945945].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000416526800021">Design, Synthesis, and Biological Activity of Novel Octahydro-1H-pyrrolo[3,2-c]pyridine Derivatives as C-C Chemokine Receptor Type 5 (CCR5) Antagonists.</a> Wang, Y.J., U. Halambage, C.C. Zeng, and L.M. Hu. Chinese Journal of Organic Chemistry, 2017. 37(9): p. 2385-2391. ISI[000416526800021].
    <br />
    <b>[WOS]</b>. HIV_12_2017.</p>

    <p>20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29162455">Synthesis and Biological Evaluation of Water-soluble Derivatives of Chiral Gossypol as HIVFusion Inhibitors Targeting gp41.</a> Yang, J., L.L. Li, J.R. Li, J.X. Yang, F. Zhang, G. Chen, R. Yu, W.J. Ouyang, and S.W. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2018. 28(1): p. 49-52. PMID[29162455].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <p>21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29185738">Oxazole-containing Diterpenoids from Cell Cultures of Salvia miltiorrhiza and their anti-HIV-1 Activities.</a> Zhang, D., J. Guo, M. Zhang, X. Liu, M. Ba, X. Tao, L. Yu, Y. Guo, and J. Dai. Journal of Natural Products, 2017. 80(12): p. 3241-3246. PMID[29185738].
    <br />
    <b>[PubMed]</b>. HIV_12_2017.</p>

    <h2>Patent Citations</h2>

    <p>22. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171215&amp;CC=CN&amp;NR=107474045A&amp;KC=A">Ritonavir Selenazole Derivative with anti-HIV-1 Activity and Its Synthesis Method.</a> Du, Y. and J. Qiao. Patent. 2017. 2017-10721786 107474045: 7pp.
    <br />
    <b>[Patent]</b>. HIV_12_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
