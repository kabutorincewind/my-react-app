

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-12-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NRhKL69OyyUbpQ8lvdN6++FRW+7egColZhGDNBueFU7Y4i3XDEhFVLiXwQvQFuvb30cG6jBwpmfzODQmHmbwy7NGFl+92Wnt3sHRtz13D6++xPZh4QrzfNswCsE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2E5B81C2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 5 - December 18, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25514407">Antimicrobial and Hypoglycemic Activities of Novel N-Mannich Bases Derived from 5-(1-Adamantyl)-4-substituted-1,2,4-triazoline-3-thiones.</a> Al-Abdullah, E.S., H.M. Al-Tuwaijri, H.M. Hassan, M.E. Haiba, E.E. Habib, and A.A. El-Emam. International Journal of Molecular Sciences, 2014. 15(12): p. 22995-23010. PMID[25514407].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25513734">Antimicrobial and Antibiofilm Activity of Chitosan on the Oral Pathogen Candida albicans.</a> Costa, E., S. Silva, F. Tavaria, and M. Pintado. Pathogens, 2014. 3(4): p. 908-919. PMID[25513734].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25038706">Synthesis of Mannich Bases: 2-(3-Phenylaminopropionyloxy)-benzoic acid and 3-Phenylamino-1-(2,4,6-trimethoxy-phenyl)-propan-1-one, their Toxicity, Ionization Constant, Antimicrobial and Antioxidant Activities.</a> Oloyede, G.K., I.E. Willie, and O.O. Adeeko. Food Chemistry, 2014. 165: p. 515-521. PMID[25038706].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25493550">Efficient Synthesis and Anti-tubercular Activity of a Series of Spirocycles: An Exercise in Open Science.</a> Badiola, K.A., D.H. Quan, J.A. Triccas, and M.H. Todd. PLoS One, 2014. 9(12): p. e111782. PMID[25493550].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25456390">Synthesis and Evaluation of the 2,4-Diaminoquinazoline Series as Anti-tubercular Agents.</a> Odingo, J., T. O&#39;Malley, E.A. Kesicki, T. Alling, M.A. Bailey, J. Early, J. Ollinger, S. Dalai, N. Kumar, R.V. Singh, P.A. Hipskind, J.W. Cramer, T. Ioerger, J. Sacchettini, R. Vickers, and T. Parish. Bioorganic &amp; Medicinal Chemistry, 2014. 22(24): p. 6965-6979. PMID[25456390].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25464882">Antitubercular Activity of Quinolizidinyl/Pyrrolizidinylalkyliminophenazines.</a> Tonelli, M., F. Novelli, B. Tasso, A. Sparatore, V. Boido, F. Sparatore, S. Cannas, P. Molicotti, S. Zanetti, S. Parapini, and R. Loddo. Bioorganic &amp; Medicinal Chemistry, 2014. 22(24): p. 6837-6845. PMID[25464882].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25465394">Antiplasmodial Activity of Solvent Fractions of Methanolic Root Extract of Dodonaea angustifolia in Plasmodium berghei Infected Mice.</a> Amelo, W., P. Nagpal, and E. Makonnen. BMC Complementary and Alternative Medicine, 2014. 14(1): p. 462. PMID[25465394].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25210872">Development of Inhibitors of the 2C-Methyl-D-erythritol 4-phosphate (MEP) Pathway Enzymes as Potential Anti-infective Agents.</a> Masini, T. and A.K. Hirsch. Journal of Medicinal Chemistry, 2014. 57(23): p. 9740-9763. PMID[25210872].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25470995">Metal-chloroquine Derivatives as Possible Anti-malarial Drugs: Evaluation of Anti-malarial Activity and Mode of Action.</a> Navarro, M., W. Castro, M. Madamet, R. Amalvict, N. Benoit, and B. Pradines. Malaria Journal, 2014. 13(1): p. 471. PMID[25470995].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25476467">In Vitro Activity of Immunosuppressive Drugs against Plasmodium falciparum.</a> Veletzky, L., K. Rehman, T. Lingscheid, W. Poeppl, F. Loetsch, H. Burgmann, and M. Ramharter. Malaria Journal, 2014. 13(1): p. 476. PMID[25476467].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1205-121814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344211500037">Comparison of the Antimicrobial Activities of Gallium nitrate and Gallium maltolate against Mycobacterium avium subsp. paratuberculosis in Vitro.</a> Fecteau, M.E., H.W. Aceto, L.R. Bernstein, and R.W. Sweeney. Veterinary Journal, 2014. 202(1): p. 195-197. ISI[000344211500037].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344390000033">Green Silver Nanoparticles: Enhanced Antimicrobial and Antibiofilm Activity with Effects on DNA Replication and Cell Cytotoxicity.</a> Gupta, K., S. Barua, S.N. Hazarika, A.K. Manhar, D. Nath, N. Karak, N.D. Namsa, R. Mukhopadhyay, V.C. Kalia, and M. Mandal. RSC Advances, 2014. 4(95): p. 52845-52855. ISI[000344390000033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344801500004">Bioactivity-guided Isolation and Structural Characterization of the Antifungal Compound, Plumbagin, from Nepenthes gracilis.</a> Gwee, P.S., K.S. Khoo, H.C. Ong, and N.W. Sit. Pharmaceutical Biology, 2014. 52(12): p. 1526-1531. ISI[000344801500004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344862300069">Inhibitory Effect of Essential Oils on Aspergillus ochraceus Growth and Ochratoxin a Production.</a> Hua, H.J., F.G. Xing, J.N. Selvaraj, Y. Wang, Y.J. Zhao, L. Zhou, X. Liu, and Y. Liu. PLoS One, 2014. 9(9). ISI[000344862300069].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344516800012">Aliphatic Dicarboxylate Directed Assembly of Silver(I) 1,3,5-Triaza-7-phosphaadamantane Coordination Networks: Topological Versatility and Antimicrobial Activity.</a> Jaros, S.W., M. da Silva, M. Florek, M.C. Oliveira, P. Smolenski, A.J.L. Pombeiro, and A.M. Kirillov. Crystal Growth &amp; Design, 2014. 14(11): p. 5408-5417. ISI[000344516800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344433400014">Synthesis and Antimicrobial Activity of Novel Oxazolidinones Having Benzo Thiazine Derivatives.</a> Kumar, P., J. Fernandes, and A. Kumar. Indian Journal of Heterocyclic Chemistry, 2014. 24(1): p. 71-76. ISI[000344433400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344158600001">Pantothenic acid Biosynthesis in the Parasite Toxoplasma gondii: A Target for Chemotherapy.</a> Mageed, S.N., F. Cunningham, A.W. Hung, H.L. Silvestre, S.J. Wen, T.L. Blundell, C. Abell, and G.A. McConkey. Antimicrobial Agents and Chemotherapy, 2014. 58(11): p. 6345-6353. ISI[000344158600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343995800012">Benzylidene/2-Aminobenzylidene hydrazides: Synthesis, Characterization and in Vitro Antimicrobial Evaluation.</a> Malhotra, M., R. Sharma, D. Rathee, P. Phogat, and A. Deep. Arabian Journal of Chemistry, 2014. 7(5): p. 666-671. ISI[000343995800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344970300010">Synthesis and Antifungal Activity of Phenacyl azoles.</a> Nelson, R., V. Kesternich, M. Perez-Fehrmann, F. Salazar, L. Marcourt, P. Christen, and P. Godoy. Journal of Chemical Research, 2014(9): p. 549-552. ISI[000344970300010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344490900010">Cobalt(III) Complexes with Thiosemicarbazones as Potential Anti-Mycobacterium tuberculosis Agents.</a> Oliveira, C.G., P.I.D. Maia, M. Miyata, F.R. Pavan, C.Q.F. Leite, E.T. de Almeida, and V.M. Deflon. Journal of the Brazilian Chemical Society, 2014. 25(10): p. 1848. ISI[000344490900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344471800042">Development of Benzo[d]oxazol-2(3H)-ones Derivatives as Novel Inhibitors of Mycobacterium tuberculosis InhA.</a> Pedgaonkar, G.S., J.P. Sridevi, V.U. Jeankumar, S. Saxena, P.B. Devi, J. Renuka, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2014. 22(21): p. 6134-6145. ISI[000344471800042].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343995800011">Synthesis and Biological Evaluation of 4-Thiazolidinone Derivatives as Antitubercular and Antimicrobial Agents.</a> Samadhiya, P., R. Sharma, S.K. Srivastava, and S.D. Srivastava. Arabian Journal of Chemistry, 2014. 7(5): p. 657-665. ISI[000343995800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344088500005">Mycobacterial DNA GyrB Inhibitors: Ligand Based Pharmacophore Modelling and in Vitro Enzyme Inhibition Studies.</a> Saxena, S., J. Renuka, V.U. Jeankumar, P. Yogeeswari, and D. Sriram. Current Topics in Medicinal Chemistry, 2014. 14(17): p. 1990-2005. ISI[000344088500005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344865100004">Synthesis, Crystal Structure, and Antifungal Activity of a Newly Synthesized Polymeric Mixed Ligand Complex of Zn(II) with 1,1-Dithiolate and Nitrogen Donors.</a> Singh, M.K., S. Sutradhar, B. Paul, S. Adhikari, R.J. Butcher, S. Acharya, and A. Das. Journal of Coordination Chemistry, 2014. 67(22): p. 3613-3620. ISI[000344865100004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344432300003">Synthesis, Characterization and Antimicrobial Activity of Copper(II) Complexes of Some Ortho-substituted aniline Schiff Bases; Crystal Structure of bis(2-Methoxy-6-imino)methylphenol copper(II) Complex.</a> Sobola, A.O., G.M. Watkins, and B. Van Brecht. South African Journal of Chemistry, 2014. 67: p. 45-51. ISI[000344432300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344471800034">Celastrol Inhibits Plasmodium falciparum Enoyl-acyl Carrier Protein Reductase.</a> Tallorin, L., J.D. Durrant, Q.G. Nguyen, J.A. McCammon, and M.D. Burkart. Bioorganic &amp; Medicinal Chemistry, 2014. 22(21): p. 6053-6061. ISI[000344471800034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344444100028">Synthesis, Characterization, Antimicrobial, DNA-cleavage and Antioxidant Activities of 3-((5-Chloro-2-phenyl-1H-indol-3-ylimino)methyl)quinoline-2(1H)-thione and its Metal Complexes.</a> Vivekanand, B., K.M. Raj, and B.H.M. Mruthyunjayaswamy. Journal of Molecular Structure, 2015. 1079: p. 214-224. ISI[000344444100028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000344633900007">Synthesis, Characterization, Crystal Structures, and Antimicrobial Activities of N, N &#39;-Bis(3-Bromo-2-hydroxybenzylidene)-1,3-diaminopropane and its Dinuclear Copper(II) Complex.</a> Wang, N. and X.Y. Qiu. Synthesis and Reactivity in Inorganic, Metal-organic and Nano-metal Chemistry, 2015. 45(2): p. 185-190. ISI[000344633900007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1205-121814.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
