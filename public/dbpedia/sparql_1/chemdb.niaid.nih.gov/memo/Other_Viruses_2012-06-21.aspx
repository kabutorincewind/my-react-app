

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-06-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tJznaB+xKw7ofUUhHEneFvMEjgtIjEpc4ynEFyUUzn8Ox1pAUEt2JOY6RwVVV4rdr4ECfQ8CZ/poqKUS6JHETSxdGssVioByLhAfmtMM5DHJbsaM2Ps9FKEgdl0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E779966F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: June 8 - June 21, 2012</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22711940">Deoxy Derivatives of L-like 5&#39;-Noraristeromycin.</a> Hinton, S., A. Riddick, and T. Serbessa, Tetrahedron Letters, 2012. 53(14): p. 1753-1755; PMID[22711940].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22701506">Anti-Hepatitis B Virus Effect and Possible Mechanism of Action of 3,4-O-Dicaffeoylquinic Acid in Vitro and in Vivo.</a> Wu, Y.H., B.J. Hao, H.C. Cao, W. Xu, Y.J. Li, and L.J. Li, Evidence-Based Complementary and Alternative Medicine, 2012. 2012: p. 356806; PMID[22701506].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22687441">Design, Synthesis, and Molecular Hybrids of Caudatin and Cinnamic Acids as Novel anti-Hepatitis B Virus Agents.</a> Wang, L.J., C.A. Geng, Y.B. Ma, J. Luo, X.Y. Huang, H. Chen, N.J. Zhou, X.M. Zhang, and J.J. Chen, European Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22687441].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22710121">TMC647055, A Potent Non-nucleoside HCV NS5B Polymerase Inhibitor with Cross-genotypic Coverage.</a> Devogelaere, B., J.M. Berke, L. Vijgen, P. Dehertogh, E. Fransen, E. Cleiren, L. van der Helm, O. Nyanguile, A. Tahri, K. Amssoms, O. Lenz, M.D. Cummings, R.F. Clayton, S. Vendeville, P. Raboisson, K.A. Simmen, G.C. Fanning, and T.I. Lin, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22710121].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704887">Synthesis and Evaluation of Novel Potent HCV NS5A Inhibitors.</a> Zhang, H., L. Zhou, F. Amblard, J. Shi, D.R. Bobeck, S. Tao, T.R. McBrayer, P.M. Tharnish, T. Whitaker, S.J. Coats, and R.F. Schinazi, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22704887].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22697413">Identification of Non-macrocyclic Small Molecule Inhibitors against the NS3/4A Serine Protease of Hepatitis C Virus through In-silico Screening.</a>. Chaudhuri, R., H. Lee, L. Truong, J. Torres, K.K. Patel, and M.E. Johnson, Journal of Chemical Information and Modeling, 2012. <b>[Epub ahead of print]</b>; PMID[22697413].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22696300">Total Synthesis of a Noricumazole A Library and Evaluation of HCV Inhibition.</a> Barbier, J., J. Wegner, S. Benson, J. Gentzsch, T. Pietschmann, and A. Kirschning, Chemistry (Weinheim an der Bergstrasse, Germany), 2012. <b>[Epub ahead of print]</b>; PMID[22696300].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22690141">Inhibition of Hepatitis C Virus Replication and Viral Helicase by Ethyl Acetate Extract of the Marine Feather Star Alloeocomatella polycladia.</a> Yamashita, A., K.A. Salam, A. Furuta, Y. Matsuda, O. Fujita, H. Tani, Y. Fujita, Y. Fujimoto, M. Ikeda, N. Kato, N. Sakamoto, S. Maekawa, N. Enomoto, M. Nakakoshi, M. Tsubuki, Y. Sekiguchi, S. Tsuneda, N. Akimitsu, N. Noda, J. Tanaka, and K. Moriishi, Marine Drugs, 2012. 10(4): p. 744-61; PMID[22690141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <h2>SIV</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22713337">Response of SIV to the Novel Nucleoside Reverse Transcriptase Inhibitor 4&#39; -Ethynyl-2-fluoro-2&#39; -deoxyadenosine (EFdA) in Vitro and in Vivo.</a> Murphey-Corb, M., P. Rajakumar, H. Michael, J. Nyaundi, P.J. Didier, A.B. Reeve, H. Mitsuya, S.G. Sarafianos, and M.A. Parniak, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22713337].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22711259">17-Allylamino-17-(demethoxy)geldanamycin (17-AAG) is a Potent and Effective Inhibitor of Human Cytomegalovirus Replication in Primary Fibroblast Cells.</a> Evers, D.L., C.F. Chao, Z. Zhang, and E.S. Huang, Archives of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22711259].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704890">Anti-Herpes Simplex Virus Efficacies of 2-Aminobenzamide Derivatives as Novel HSP90 Inhibitors.</a> Xiang, Y.F., C.W. Qian, G.W. Xing, J. Hao, M. Xia, and Y.F. Wang, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22704890].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22690151">Structural Characterization and anti-HSV-1 and HSV-2 Activity of Glycolipids from the Marine Algae Osmundaria obtusiloba Isolated from Southeastern Brazilian Coast.</a> de Souza, L.M., G.L. Sassaki, M.T. Romanos, and E. Barreto-Bergter, Marine Drugs, 2012. 10(4): p. 918-31; PMID[22690151].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0608-062112.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304185400002">Intramolecular Regulation of the Sequence-specific mRNA Interferase Activity of MazF Fused to a MazE Fragment with a Linker Cleavable by Specific Proteases.</a> Park, J.H., Y. Yamaguchi, and M. Inouye, Applied and Environmental Microbiology, 2012. 78(11): p. 3794-3799; ISI[000304185400002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0608-062112.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304409900015">Non-hydrolysable Analogues of Inorganic Pyrophosphate as Inhibitors of Hepatitis C Virus RNA-dependent RNA-polymerase.</a> Yanvarev, D.V., A.N. Korovina, N.N. Usanov, and S.N. Kochetkov, Russian Journal of Bioorganic Chemistry, 2012. 38(2): p. 224-229; ISI[000304409900015].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0608-062112.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
