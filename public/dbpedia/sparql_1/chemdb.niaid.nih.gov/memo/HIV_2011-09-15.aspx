

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-09-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="f2yvJA5ZGTFnznDACO5Kl02pesWiOas2OZ3/8PRDfOQ7mr2AKJyrHZGPbUdY7pLYPdEpf+3j6g/jQqPCECA2ssliRxqFZABzXzaW5WROUclAhYdekOnoq4xaASI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2CC50EB2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  September 2, 2011 - September 15, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21900867">Synthesis of Quinolin-2-one Alkaloid Derivatives and Their Inhibitory Activities against HIV-1 Reverse Transcriptase.</a> Cheng, P., Q. Gu, W. Liu, J.F. Zou, Y.Y. Ou, Z.Y. Luo, and J.G. Zeng, Molecules, 2011. 16(9): p. 7649-7661; PMID[21900867].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21785785">Selection of a Synthetic Glycan Oligomer from a Library of DNA-templated Fragments against DC-SIGN and Inhibition of HIV gp120 Binding to Dendritic Cells.</a> Ciobanu, M., K.T. Huang, J.P. Daguer, S. Barluenga, O. Chaloin, E. Schaeffer, C.G. Mueller, D.A. Mitchell, and N. Winssinger, Chemical Communications (Cambridge, England), 2011. 47(33): p. 9321-9323; PMID[21785785].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21899332">P1-Substituted Symmetry-Based Human Immunodeficiency Virus Protease Inhibitors with Potent Antiviral Activity against Drug-Resistant Viruses.</a> Degoey, D.A., D. Grampovnik, H.J. Chen, W. Flosi, L. Klein, T. Dekhtyar, V. Stoll, M. Mamo, A. Molla, and D. Kempf, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21899332].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896904">TMC310911, a Novel Human Immunodeficiency Virus Type-1 Protease Inhibitor shows in Vitro an Improved Resistance Profile and Higher Genetic Barrier to Resistance Compared with Current Protease Inhibitors.</a> Dierynck, I., H. Van Marck, M. Van Ginderen, T.H. Jonckers, M.N. Nalam, C.A. Schiffer, A. Raoof, G. Kraus, and G. Picchio, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21896904].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21902586">Susceptibility of HIV-2 Primary Isolates to CCR5 and CXCR4 Monoclonal Antibodies, Ligands and Small Molecule Inhibitors.</a> Espirito-Santo, M., Q. Santos-Costa, M. Calado, P. Dorr, and J.M. Azevedo Pereira, AIDS Research and Human Retroviruses, 2011. <b>[Epub ahead of print]</b>; PMID[21902586].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21802104">Synergistic Activity Profile of Griffithsin in Combination with Tenofovir, Maraviroc and Enfuvirtide against HIV-1 Clade C.</a> Ferir, G., K.E. Palmer, and D. Schols, Virology, 2011. 417(2): p. 253-258; PMID[21802104].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896914">Inhibition of HIV-1 by Octadecyloxyethyl Esters of (S)-[3- Hydroxy-2-(phosphonomethoxy)propyl] Nucleosides and Evaluation of Their Mechanism of Action.</a> Magee, W.C., N. Valiaeva, J.R. Beadle, D.D. Richman, K.Y. Hostetler, and D.H. Evans, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21896914].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21895456">In Vitro Antiviral Activities of Extracts Derived from Daucus maritimus Seeds.</a> Miladi, S., N. Abid, C. Debarnot, M. Damak, B. Canard, M. Aouni, and B. Selmi, National Product Research, 2011. <b>[Epub ahead of print]</b>; PMID[21895456].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21905195">Biophysical Investigations of GBV-C E1 Peptides as Potential Inhibitors of HIV-1 Fusion Peptide.</a> Sanchez-Martin, M.J., P. Urban, M. Pujol, I. Haro, M.A. Alsina, and M.A. Busquets, Chemphyschem, 2011. <b>[Epub ahead of print]</b>; PMID[21905195].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21896906">Development of Dual-Acting Pyrimidinediones as Novel and Highly Potent Topical Anti-HIV Microbicides.</a> Watson-Buckheit, K., L. Yang, and R.W. Buckheit, Jr., Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21896906].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294185500005">Synthesis and Anti-HIV Activity of Novel 3-substituted phenyl-6,7-dimethoxy-3a,4-dihydro-3h-indeno[1,2-c]isoxazole Analogues.</a> Ali, M.A., R. Ismail, T.S. Choon, Y.K. Yoon, A.C. Wei, S. Pandian, J.G. Samy, E. De Clercq, and C. Pannecouque, Acta Poloniae Pharmaceutica, 2011. 68(3): p. 343-348; ISI[000294185500005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294112900001">Cell-Free HIV-1 Virucidal Action by Modified Peptide Triazole Inhibitors of Env gp120.</a>  Bastian, A.R., Kantharaju, K. McFadden, C. Duffy, S. Rajagopal, M.R. Contarino, E. Papazoglou, and I. Chaiken, ChemMedChem, 2011. 6(8): p. 1335-1339; ISI[000294112900001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294010600007">Hydrogen Bonds of Anti-Hiv Active Aminophenols.</a> Belkov, M.V., G.A. Ksendzova, I.V. Skornyakov, V.L. Sorokin, G.B. Tolstorozhev, and O.I. Shadyro, Journal of Applied Spectroscopy, 2011. 78(2): p. 197-202; ISI[000294010600007].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293956400008">High-Functional-Avidity Cytotoxic T Lymphocyte Responses to HLA-B-Restricted Gag-Derived Epitopes Associated with Relative HIV Control.</a> Berger, C.T., N. Frahm, D.A. Price, B. Mothe, M. Ghebremichael, K.L. Hartman, L.M. Henry, J.M. Brenchley, L.E. Ruff, V. Venturi, F. Pereyra, J. Sidney, A. Sette, D.C. Douek, B.D. Walker, D.E. Kaufmann, and C. Brander, Journal of Virology, 2011. 85(18): p. 9334-9345; ISI[000293956400008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294035300011">Inhibition on HIV-1 Integrase Activity and Nitric Oxide Production of Compounds from Ficus glomerata.</a> Bunluepuech, K., T. Sudsai, C. Wattanapiromsakul, and S. Tewtrakul, Natural Product Communications, 2011. 6(8): p. 1095-1098; ISI[000294035300011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293956400038">Inhibition of HIV-1 Integration in Ex Vivo-Infected CD4 T Cells from Elite Controllers.</a> Buzon, M.J., K. Seiss, R. Weiss, A.L. Brass, E.S. Rosenberg, F. Pereyra, X.G. Yu, and M. Lichterfeld, Journal of Virology, 2011. 85(18): p. 9646-9650; ISI[000293956400038].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294085500008">New bis(Thiosemicarbazonate) Gold(III) Complexes Inhibit HIV Replication at Cytostatic Concentrations: Potential for Incorporation into Virostatic Cocktails.</a> Fonteh, P.N., F.K. Keter, and D. Meyer, Journal of Inorganic Biochemistry, 2011. 105(9): p. 1173-1180; ISI[000294085500008].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293626100057">Epigenetic Silencing of HIV-1 by the Histone H3 Lysine 27 Methyltransferase Enhancer of Zeste 2.</a>  Friedman, J., W.K. Cho, C.K. Chu, K.S. Keedy, N.M. Archin, D.M. Margolis, and J. Karn, Journal of Virology, 2011. 85(17): p. 9078-9089; ISI[000293626100057].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294108600003">Structure and Conformational Analysis of the Anti-HIV AZT 5 &#39;-Aminocarbonylphosphonate Prodrug using DFT Methods.</a> Molina, A.T. and M.A. Palafox, Chemical Physics, 2011. 387(1-3): p. 11-24; ISI[000294108600003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294092800027">Synthesis and Chromatographic Enantioresolution of Anti-HIV Quinolone Derivatives.</a> Natalini, B., R. Sardella, S. Massari, F. Ianni, O. Tabarrini, and V. Cecchetti, Talanta, 2011. 85(3): p. 1392-1397; ISI[000294092800027].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293637300285">An Optimized MM/PBSA Virtual Screening Approach Applied on an HIV-1 gp41 Fusion Peptide Inhibitor.</a> Venken, T., D. Krnavek, J. Munch, F. Kirchhoff, P. Henklein, M. De Maeyer, and A. Voet, European Biophysics Journal with Biophysics Letters, 2011. 40: p. 120-120; ISI[000293637300285].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293637300286">Structure Based Discovery and Optimization of First-in-class Inhibitors of the HIV-1 IN-LEDGF/p75 Interaction.</a> Voet, A.R.D., F. Christ, A. Marchand, P. Chaltin, S. Nicolet, S. Strelkov, Z. Debyser, and M. De Maeyer, European Biophysics Journal with Biophysics Letters, 2011. 40: p. 120-121; ISI[000293637300286].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0902-091511.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
