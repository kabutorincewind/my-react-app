

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-06-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zodWKkYhtKY2GIc8C/wj7Si/DCio6bO46c/dPnz8TUsVd6HfC8fQRD9NjKGKUzSur4VeIMy2g1Uju+FnNtvnTCkPetNOZZoVT8RKoS2ZixxpDcDAgNK145hVRbI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D1C32D69" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  May 27, 2011 - June 09, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21637109">Interleukin-2 Production by Polyfunctional HIV-1-Specific CD8 T-Cells is Associated with Enhanced Viral Suppression.</a> Akinsiku, O.T., A. Bansal, S. Sabbaj, S.L. Heath, and P.A. Goepfert. Journal of Acquired Immune Deficiency Syndrome, 2011. <b>[Epub ahead of print];</b> PMID[21637109].</p>
   
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21626526">Compromising Mitochondrial Function with the Antiretroviral Drug Efavirenz Induces Cell Survival-promoting Autophagy.</a> Apostolova, N., L.J. Gomez-Sucerquia, A. Gortat, A. Blas-Garcia, and J.V. Esplugues. Hepatology, 2011. <b>[Epub ahead of print]</b>; PMID[21626526].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21561135">Stelleralides A-C, Novel Potent Anti-HIV Daphnane-Type Diterpenoids from Stellera chamaejasme L</a>. Asada, Y., A. Sukemori, T. Watanabe, K.J. Malla, T. Yoshikawa, W. Li, K. Koike, C.H. Chen, T. Akiyama, K. Qian, K. Nakagawa-Goto, S.L. Morris-Natschke, and K.H. Lee. Organic Letters, 2011. 13(11): p. 2904-2907; PMID[21561135].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21515046">Synthesis and In vitro Anti-HIV Activity of N-1,3-Benzo[d]thiazol-2-yl-2-(2-oxo-2H-chromen-4-yl)acetamide Derivatives using MTT Method.</a> Bhavsar, D., J. Trivedi, S. Parekh, M. Savant, S. Thakrar, A. Bavishi, A. Radadiya, H. Vala, J. Lunagariya, M. Parmar, L. Paresh, R. Loddo, and A. Shah. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3443-3446; PMID[21515046].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568335">Synthesis, Activity, and Structural Analysis of Novel alpha-Hydroxytropolone Inhibitors of Human Immunodeficiency Virus Reverse Transcriptase-Associated Ribonuclease H</a>. Chung, S., D.M. Himmel, J.K. Jiang, K. Wojtak, J.D. Bauman, J.W. Rausch, J.A. Wilson, J.A. Beutler, C.J. Thomas, E. Arnold, and S.F. Le Grice. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21568335].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21501631">Synergistic In vitro Anti-HIV Type 1 Activity of Tenofovir with Carbohydrate-binding Agents (CBAs).</a> Ferir, G., K. Vermeire, D. Huskens, J. Balzarini, E.J. Van Damme, J.C. Kehr, E. Dittmann, M.D. Swanson, D.M. Markovitz, and D. Schols, Antiviral Research, 2011. 90(3): p. 200-204; PMID[21501631].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21553876">Combination Anti-HIV Therapy with the Self-Assemblies of an Asymmetric Bolaamphiphilic Zidovudine/Didanosine Prodrug.</a> Jin, Y., R. Xin, L. Tong, L. Du, and M. Li, Molecular Pharmaceutics, 2011. 8(3): p. 867-876; PMID[21553876].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21444700">Inhibition of Hepatitis C Virus Replicon RNA Synthesis by PSI-352938, a Cyclic Phosphate Prodrug of {beta}-D-2&#39;-Deoxy-2&#39;-{alpha}-Fluoro-2&#39;-{beta}-C-Methylguanosine.</a> Lam, A.M., C. Espiritu, E. Murakami, V. Zennou, S. Bansal, H.M. Micolochick Steuer, C. Niu, M. Keilman, H. Bao, N. Bourne, R.L. Veselenak, P.G. Reddy, W. Chang, J. Du, D. Nagarathnam, M.J. Sofia, M.J. Otto, and P.A. Furman. Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 2566-2575; PMID[21444700].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21456073">Synthesis and Anti-HIV-1 Activity of New Fluoro-HEPT Analogues: An Investigation on Fluoro versus Hydroxy Substituents.</a> Loksha, Y.M., E.B. Pedersen, R. Loddo, and P. La Colla, Archiv der Pharmazie (Weinheim), 2011. 344(6): p. 366-371; PMID[21456073].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21646480">The Base Component of 3&#39; -Azido-2&#39; ,3&#39; -Dideoxynucleosides Influences Resistance Mutations Selected in HIV-1 Reverse Transcriptase.</a> Meteer, J.D., D. Koontz, G. Asif, H.W. Zhang, M. Detorio, S. Solomon, S.J. Coats, N. Sluis-Cremer, R.F. Schinazi, and J.W. Mellors. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21646480].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21436155">Inhibition of Hepatitis C Virus Replication by Semi-synthetic Derivatives of Glycopeptide Antibiotics.</a> Obeid, S., S.S. Printsevskaya, E.N. Olsufyeva, K. Dallmeier, D. Durantel, F. Zoulim, M.N. Preobrazhenskaya, J. Neyts, and J. Paeshuyse. The Journal of Antimicrobial Chemotherapy, 2011. 66(6): p. 1287-1294; PMID[21436155].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21633286">Synergy Against Drug Resistant HIV-1 with the Microbicide Antiretrovirals, Dapivirine and Tenofovir, in Combination.</a> Schader, S.M., S.P. Colby-Germinario, J.R. Schachter, H. Xu, and M.A. Wainberg, AIDS, 2011. <b>[Epub ahead of print]</b>; PMID[21633286].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21565516">New Prodrugs of Adefovir and Cidofovir.</a> Tichy, T., G. Andrei, M. Dracinsky, A. Holy, J. Balzarini, R. Snoeck, and M. Krecmerova, Bioorganic &amp; Medicinal Chemistry, 2011. 19(11): p. 3527-3539; PMID[21565516].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21444708">Gentian Violet Exhibits Activity against Biofilms Formed by Oral Candida Isolates Obtained from HIV-Infected Patients.</a> Traboulsi, R.S., P.K. Mukherjee, J. Chandra, R.A. Salata, R. Jurevic, and M.A. Ghannoum, Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 3043-3045; PMID[21444708].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21585381">Isolation of a Polysaccharide with Antiproliferative, Hypoglycemic, Antioxidant and HIV-1 Reverse Transcriptase Inhibitory Activities from the Fruiting Bodies of the Abalone Mushroom Pleurotus abalonus.</a> Wang, C.R., T.B. Ng, L. Li, J.C. Fang, Y. Jiang, T.Y. Wen, W.T. Qiao, N. Li, and F. Liu, The Journal of Pharmacy and Pharmacology, 2011. 63(6): p. 825-832; PMID[21585381].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21430048">Caveolin 1 Inhibits HIV Replication by Transcriptional Repression Mediated through NF-{kappa}B.</a> Wang, X.M., P.E. Nadeau, S. Lin, J.R. Abbott, and A. Mergia, Journal of Virology, 2011. 85(11): p. 5483-5493; PMID[21430048].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21628544">A Novel Method for the Simultaneous Quantification of Phenotypic Resistance to Maturation, Protease, Reverse Transcriptase, and Integrase HIV Inhibitors Based on 3&#39; Gag(p2/p7/p1/p6)/PR/RT/INT-Recombinant Viruses: A Useful Tool in the Multi-Target Era of Antiretroviral Therapy.</a> Weber, J., A.C. Vazquez, D. Winner, J.D. Rose, D. Wylie, A.M. Rhea, K. Henry, J. Pappas, A. Wright, N. Mohamed, R. Gibson, B. Rodriguez, V. Soriano, K. King, E.J. Arts, P.D. Olivo, and M.E. Quinones-Mateu, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21628544].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21524575">Synthesis of 5-Thiodidehydropyranylcytosine Derivatives as Potential Anti-HIV Agents.</a> Yoshimura, Y., Y. Yamazaki, Y. Saito, Y. Natori, T. Imamichi, and H. Takahata, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3313-3316; PMID[21524575].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21298684">Isolation and Characterization of a Novel Thermostable Lectin from the Wild Edible Mushroom Agaricus arvensis.</a> Zhao, J.K., Y.C. Zhao, S.H. Li, H.X. Wang, and T.B. Ng, Journal of Basic Microbiology, 2011. 51(3): p. 304-311; PMID[21298684].</p>
    
    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290602000013">Synthesis of 2 &#39;,3 &#39;-Dideoxy-6 &#39;-fluorocarbocyclic nucleosides via Reformatskii-Claisen Rearrangement.</a> Yang, Y., F. Zheng, and F.L. Qing. Tetrahedron, 2011. 67(19): p. 3388-3394; ISI[000290602000013].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290485500011">Understanding the Binding Mode and Function of BMS-488043 against HIV-1 Viral Entry.</a> Da, L.T., J.M. Quan, and Y.D. Wu. Proteins-Structure Function and Bioinformatics, 2011. 79(6): p. 1810-1819; ISI[000290485500011].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290501200015">The Evolution of Catalytic Function in the HIV-1 Protease.</a> Singh, M.K., K. Streu, A.J. McCrone, and B.N. Dominy. Journal of Molecular Biology, 2011. 408(4): p. 792-805; ISI[000290501200015].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290620000018">Complex Drug Interactions of HIV Protease Inhibitors 1: Inactivation, Induction, and Inhibition of Cytochrome P450 3A by Ritonavir or Nelfinavir.</a> Kirby, B.J., A.C. Collier, E.D. Kharasch, D. Whittington, K.E. Thummel, and J.D. Unadkat, Drug Metabolism and Disposition, 2011. 39(6): p. 1070-1078; ISI[000290620000018].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290024200009">New Monoterpene Glycosides and Phenolic Compounds from Distylium racemosum and Their Inhibitory Activity against Ribonuclease H.</a> Kim, J.A., S.Y. Yang, A. Wamiru, J.B. McMahon, S.F.J. Le Grice, J.A. Beutler, and Y.H. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2840-2844; ISI[000290024200009].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290024200041">Development of Tricyclic Hydroxy-1H-pyrrolopyridine-trione Containing HIV-1 Integrase Inhibitors.</a> Zhao, X.Z., K. Maddali, M. Metifiot, S.J. Smith, B.C. Vu, C. Marchand, S.H. Hughes, Y. Pommier, and T.R. Burke, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2986-2990; ISI[000290024200041].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290944800001">Caging the Beast: TRIM5 alpha Binding to the HIV-1 Core.</a> Diaz-Griffero, F. Viruses-Basel, 2011. 3(5): p. 423-428; ISI[000290944800001].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290944800007">Inhibition of Enveloped Virus Release by Tetherin/BST-2: Action and Counteraction.</a> Le Tortorec, A., S. Willey, and S.J.D. Neil. Antiviral Viruses-Basel, 2011. 3(5): p. 520-540; ISI[000290944800007].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290787100014">Synthesis and Biological Evaluation of Modified 2-Deoxystreptamine Dimers.</a> Coste, G., T. Horlacher, L. Molina, A.J. Moreno-Vargas, A.T. Carmona, I. Robina, P.H. Seeberger, and S. Gerber-Lemaire, Synthesis-Stuttgart, 2011(11): p. 1759-1770; ISI[000290787100014].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290323700025">Small Dumbbell Oligonucleotides Inhibitors of RNase H Activity of MOMULV Reverse Transcriptase.</a> Kumar, A., E-Journal of Chemistry, 2011. 8(2): p. 629-634; ISI[000290323700025].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290708300002">2-Arylmethylaminomethyl-5,6-dihydroxychromone Derivatives with Selective Anti-HCV Activity.</a> Park, H.R., K.S. Park, and Y. Chong, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3202-3205; ISI[000290708300002].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290708300024">Synthesis of 5-Thiodidehydropyranylcytosine Derivatives as Potential Anti-HIV Agents.</a> Yoshimura, Y., Y. Yamazaki, Y. Saito, Y. Natori, T. Imamichi, and H. Takahata, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(11): p. 3313-3316; ISI[000290708300024].</p>
    
    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290713400057">Structural and Binding Analysis of Pyrimidinol Carboxylic Acid and N-Hydroxy Quinazolinedione HIV-1 RNase H Inhibitors.</a> Lansdon, E.B., Q. Liu, S.A. Leavitt, M. Balakrishnan, J.K. Perry, C. Lancaster-Moyer, N. Kutty, X.H. Liu, N.H. Squires, W.J. Watkins, and T.A. Kirschberg, Antimicrobial Agents and Chemotherapy, 2011. 55(6): p. 2905-2915; ISI[000290713400057].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="memofmt2-2">Patent citations:</p>

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110413&amp;CC=CN&amp;NR=102010398A&amp;KC=A">Preparation of Biphenyl Derivatives as Anti-HIV Agents.</a> Liu, J., H. Sun, H. Zhang, Y. Zheng, W. Xiao, J. Pu, R. Wang, and L. Yang. Patent. 2011. 2010-10512452 102010398.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110421&amp;CC=WO&amp;NR=2011046873A1&amp;KC=A1">5-Hydroxypyrimidin-4(3H)-ones as HIV Integrase Inhibitors and Synthesis and Antiviral Activity.</a> Naidu, B.N., K. Peese, I.B. Dicker, C. Li, M. Patel, N.A. Meanwell, and M.A. Walker. Patent. 2011. 2010-US52177 2011046873.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0527-060911.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110421&amp;CC=WO&amp;NR=2011045330A1&amp;KC=A1">Macrocyclic Derivatives as HIV Replication Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of Viral Infection.</a> Thuring, J.W.J., J.-F. Bonfanti, and J.M.C. Fortin. Patent. 2011. 2010-EP65300 2011045330.</p>

    <p class="NoSpacing"><b>[Patent]</b>. HIV_0527-060911.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
