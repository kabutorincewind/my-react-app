

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-03-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OZqJLEYZmiPIxmY7G0ddjeCdJL4EMxLLBlns/fmHSDK0qaGKm1Su+qfn1RGGy24kVTRl4xrsSOOtiSB718E0DuUNu0UB4gW0QDA5eZZXaxD1DzH5IFQA/c/lXvM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E52236A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">February 18 - March 3, 2011</p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21359560">Rapid discovery of inhibitors of Toxoplasma gondii using hybrid structure-based computational approach.</a> Kortagere, S., E. Mui, R. McLeod, and W.J. Welsh. Journal of Computer-Aided Molecular Design, 2011. <b>[Epub ahead of print]</b>; PMID[21359560].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21354675">Design, synthesis and antifungal evaluation of 1-(2-(2,4-difluorophenyl)-2-hydroxy-3-(1H-1,2,4-triazol-1-yl)propyl)-1H-1, 2,4-triazol-5(4H)-one.</a> Jiang, Y., Y. Cao, J. Zhang, Y. Zou, X. Chai, H. Hu, Q. Zhao, Q. Wu, D. Zhang, and Q. Sun. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21354675].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21358761">Curcumin as a promising anticandidal of clinical interest.</a> Neelofar, K., S. Shreaz, B. Rimple, S. Muralidhar, M. Nikhat, and L.A. Khan. Canadian Journal of Microbiology, 2011. 57(3): p. 204-210; PMID[21358761].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21354246">Chemoprevention by essential oil of turmeric leaves (Curcuma longa L.) on the growth of Aspergillus flavus and aflatoxin production.</a> Sindhu, S., B. Chempakam, N.K. Leela, and R.S. Bhai. Food and Chemical Toxicology, 2011. <b>[Epub ahead of print]</b>; PMID[21354246].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21353508">Inhibition of Candida albicans biofilm formation and yeast-hyphal transition by 4-hydroxycordoin.</a> Messier, C., F. Epifano, S. Genovese, and D. Grenier. Phytomedicine, 2011. <b>[Epub ahead of print]</b>; PMID[21353508].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21353348">Pyridine-derived thiosemicarbazones and their tin(IV) complexes with antifungal activity against Candida spp.</a> Parrilha, G.L., J.G. da Silva, L.F. Gouveia, A.K. Gasparoto, R.P. Dias, W.R. Rocha, D.A. Santos, N.L. Speziali, and H. Beraldo. European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21353348].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21350853">Surface-modified sulfur nanoparticles: an effective antifungal agent against Aspergillus niger and Fusarium oxysporum</a>. Roy Choudhury, S., M. Ghosh, A. Mandal, D. Chakravorty, M. Pal, S. Pradhan, and A. Goswami. Applied Microbiology and Biotechnology, 2011. <b>[Epub ahead of print]</b>; PMID[21350853].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21343465">Activities of Triazole-Echinocandin Combinations against Candida Species in Biofilms and as Planktonic Cells.</a> Chatzimoschou, A., A. Katragkou, M. Simitsopoulou, C. Antachopoulos, E. Georgiadou, T.J. Walsh, and E. Roilides. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21343465].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21347974">Chemical constituents of Talauma arcabucoana (Magnoliaceae): their brine shrimp lethality and antimicrobial activity.</a> Corredor Barinas, J.A. and L.E. Cuca Suarez. Natural Product Research, 2011. <b>[Epub ahead of print]</b>: p. 1-8; PMID[21347974].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21350802">Milk Kefir: Ultrastructure, Antimicrobial Activity and Efficacy on Aflatoxin B1 Production by Aspergillus flavus.</a> Ismaiel, A.A., M.F. Ghaly, and A.K. El-Naggar. Current Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21350802].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21338375">Evaluation of antimicrobial effectiveness of C-8 xylitol monoester as an alternative preservative for cosmetic products.</a> Amaral, L.F., N.S. Camilo, M.D. Pereda, C.E. Levy, P. Moriel, and P.G. Mazzola. International Journal of Cosmetic Science, 2011. <b>[Epub ahead of print]</b>; PMID[21338375].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21336686">Antifungal activity of essential oils and their synergy with fluconazole against drug-resistant strains of Aspergillus fumigatus and Trichophyton rubrum.</a> Khan, M.S. and I. Ahmad. Applied Microbiology and Biotechnology, 2011. <b>[Epub ahead of print]</b>; PMID[21336686].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21341176">Chemical Constituents, Antimicrobial and Antimalarial Activities of Zanthoxylum monophyllum.</a> Rodriguez-Guzman, R., L.C. Johansmann Fulks, M.M. Radwan, C.L. Burandt, and S.A. Ross. Planta Medica, 2011. <b>[Epub ahead of print]</b>; PMID[21341176].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21244637">Antifungal Activity of Xanthones: Evaluation of their Effect on Ergosterol Biosynthesis by High-performance Liquid Chromatography.</a> Pinto, E., C. Afonso, S. Duarte, L. Vale-Silva, E. Costa, E. Sousa, and M. Pinto. Chemical Biology &amp; Drug Design, 2011. 77(3): p. 212-222; PMID[21244637].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21333271">Short- and medium-chain fatty acids exhibit antimicrobial activity for oral microorganisms.</a> Huang, C.B., Y. Altimova, T.M. Myers, and J.L. Ebersole. Archives of Oral Biology, 2011. <b>[Epub ahead of print]</b>; PMID[21333271].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21332761">Enhanced activity of antifungal drugs using natural phenolics against yeast strains of Candida and Cryptococcus.</a> Faria, N.C., J.H. Kim, L.A. Goncalves, M.D. Martins, K.L. Chan, and B.C. Campbell. Letters in Applied Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21332761].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21204933">Anti-Candida effect of bacillomycin D-like lipopeptides from Bacillus subtilis B38.</a> Tabbene, O., L. Kalai, I. Ben Slimene, I. Karkouch, S. Elkahoui, A. Gharbi, P. Cosette, M.L. Mangoni, T. Jouenne, and F. Limam. FEMS Microbiol Letters, 2011. 316(2): p. 108-114; PMID[21204933].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21145345">Interaction of a copper(II)-Schiff base complexes with calf thymus DNA and their antimicrobial activity.</a> Sabolova, D., M. Kozurkova, T. Plichta, Z. Ondrusova, D. Hudecova, M. Simkovic, H. Paulikova, and A. Valent. International Journal of Biological Macromolecules, 2011. 48(2): p. 319-325; PMID[21145345].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20963418">Chitosan-EDTA New Combination is a Promising Candidate for Treatment of Bacterial and Fungal Infections.</a> El-Sharif, A.A. and M.H. Hussain. Current Microbiology, 2011. 62(3): p. 739-745; PMID[20963418].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21341373">Developing Pyrrole-Derived Antimycobacterial Agents: a Rational Lead Optimization Approach.</a> Biava, M., G.C. Porretta, G. Poce, C. Battilocchio, S. Alfonso, A. de Logu, F. Manetti, and M. Botta. ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21341373].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21295888">Synthesis and anti-mycobacterial activity of novel amino alcohol derivatives.</a> Cunico, W., C.R. Gomes, M.L. Ferreira, T.G. Ferreira, D. Cardinot, M.V. de Souza, and M.C. Lourenco. European Journal of Medicinal Chemistry, 2011. 46(3): p. 974-978; PMID[21295888].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21272965">Anti-tubercular agents. Part 6: Synthesis and antimycobacterial activity of novel arylsulfonamido conjugated oxazolidinones.</a> Kamal, A., R.V. Shetti, S. Azeeza, P. Swapna, M.N. Khan, I.A. Khan, S. Sharma, and S.T. Abdullah. European Journal of Medicinal Chemistry, 2011. 46(3): p. 893-900; PMID[21272965].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21277200">Synthesis and antitubercular activity of monocyclic nitroimidazoles: Insights from econazole.</a> Lee, S.H., S. Kim, M.H. Yun, Y.S. Lee, S.N. Cho, T. Oh, and P. Kim. Bioorganic and Medicinal Chemistry Letters, 2011. 21(5): p. 1515-1518; PMID[21277200].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21353489">Comparative in vitro and in vivo antimicrobial activities of sitafloxacin, gatifloxacin and moxifloxacin against Mycobacterium avium.</a> Sano, C., Y. Tatano, T. Shimizu, S. Yamabe, K. Sato, and H. Tomioka. International Journal of Antimicrobial Agents, 2011. <b>[Epub ahead of print]</b>; PMID[21353489].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0218-030311.</p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-3"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285881800018">Galactomannan-amphotericin B conjugate: synthesis and biological activity.</a> Farber, S., D. Ickowicz, E. Sionov, S. Kagan, I. Polacheck, and A.J. Domb. Polymers Advanced Technologies, 2010. Special Issue: p. 110-125; ISI[000285881800018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286150900036">Synthesis of Azole-containing Piperazine Derivatives and Evaluation of their Antibacterial, Antifungal and Cytotoxic Activities.</a> Gan, L.L., B. Fang, and C.H. Zhou. Bulletin of the Korean Chemical Society, 2010. 31(12): p. 3684-3692; ISI[000286150900036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286433900002">Chemical Composition and Antimicrobial Activity of the Essential Oils from the Flower, Leaf, and Stem of Senecio pandurifolius.</a> Kahriman, N., G. Tosun, S. Terzioglu, S.A. Karaoglu, and N. Yayli. Records of Natural Products, 2011. 5(2): p. 82-91; ISI[000286433900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285688300005">Phytochemical screening and antimicrobial activities of Euphorbia balsamifera leaves, stems and root against some pathogenic microorganisms.</a> Kamba, A.S. and L.G. Hassan. African Journal of Pharmacy and Pharmacology, 2010. 4(9): p. 645-652; ISI[000285688300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286150900010">Silver Nanoparticles Effect on Antimicrobial and Antifungal Activity of New Heterocycles.</a> Kandile, N.G., H.T. Zaky, M.I. Mohamed, and H.M. Mohamed. Bulletin of the Korean Chemical Society, 2010. 31(12): p. 3530-3538; ISI[000286150900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286320600027">The Potential Inhibitory Effect of Cuminum Cyminum, Ziziphora Clinopodioides and Nigella Sativa Essential Oils on the Growth of Aspergillus Fumigatus and Aspergillus Flavus.</a> Khosravi, A.R., M.H. Minooeianhaghighi, H. Shokri, S.A. Emami, S.M. Alavi, and J. Asili. Brazilian Journal of Microbiology, 2011. 42(1): p. 216-224; ISI[000286320600027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285675900028">Antimicrobial Activity Evaluation of Cassia spectabilis Leaf Extracts.</a> Krishnan, N., S. Ramanathan, S. Sasidharan, V. Murugaiyah, and S.M. Mansor. International Journal of Pharmacology, 2010. 6(4): p. 510-514; ISI[000285675900028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286526500024">Extraction and characterization of chitin and chitosan from crustacean by-products: Biological and physicochemical properties.</a> Limam, Z., S. Selmi, S. Sadok, and A. El Abed. African Journal of Biotechnology, 2011. 10(4): p. 640-647; ISI[000286526500024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286433900003">Biological Assays and Chemical Composition of Volatile Oils of Bupleurum fruticosum L. (Apiaceae).</a> Maxia, A., M.A. Frau, B. Marongiu, A. Piras, S. Porcedda, D. Falconieri, M.J. Goncalves, C. Cavaleiro, and L. Salgueiro. Records of Natural Products, 2011. 5(2): p. 92-99; ISI[000286433900003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285724800053">Identification of 3-phenylaminoquinolinium and 3-phenylaminopyridinium salts as new agents against opportunistic fungal pathogens.</a> Mazu, T.K., J.R. Etukala, X.Y. Zhu, M.R. Jacob, S.I. Khan, L.A. Walker, and S.Y. Ablordeppey. Bioorganic &amp; Medicinal Chemistry, 2011. 19(1): p. 524-533; ISI[000285724800053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286538600056">Synthesis and Activity of Novel 1-Halogenobenzylindole Linked Triazole Derivatives as Antifungal Agents.</a> Na, Y.M. Bulletin of the Korean Chemical Society, 2011. 32(1): p. 307-310; ISI[000286538600056].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000281784800011">Antimicrobial activity of Gunnera perpensa and Heteromorpha arborescens var. abyssinica.</a> Nkomo, M. and L. Kambizi. Journal of Medicinal Plants Research, 2009. 3(12): p. 1051-1055; ISI[000281784800011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000287016000013">Synthetic UDP-Furanoses as Potent Inhibitors of Mycobacterial Galactan Biogenesis.</a> Peltier, P., M. Belanova, P. Dianiskova, R.K. Zhou, R.B. Zheng, J.A. Pearcey, M. Joe, P.J. Brennan, C. Nugier-Chauvin, V. Ferrieres, T.L. Lowary, R. Daniellou, and K. Mikusova. Chemistry &amp; Biology, 2010. 17(12): p. 1356-1366; ISI[000287016000013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285690000002">Antimicrobial and free radical scavenging activity of extracts of some Indian medicinal plants.</a> Salar, R.K. and A. Dhall. Journal of Medicinal Plants Research, 2010. 4(22): p. 2313-2320; ISI[000285690000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286701000013">Efficacy of Origanum essential oils for inhibition of potentially pathogenic fungi.</a> Souza, N.A.B., E.D. Lima, D.N. Guedes, F.D. Pereira, E.L. de Souza, and F.B. de Sousa. Brazilian Journal of Pharmaceutical Sciences, 2010. 46(3): p. 499-508; ISI[000286701000013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286568600004">Conventional as well as microwave assisted synthesis and antimicrobial screening of 4-aryl-3-chloro-1- (5-nitroindazol-1-yl) acetamido -2-oxo-azetidines.</a> Upadhyay, A., S.K. Srivastava, S.D. Srivastava, and R. Yadav. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2011. 50(1): p. 89-97; ISI[000286568600004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286538600043">Synthesis of Novel D-Glucose-derived Benzyl and Alkyl 1,2,3-Triazoles as Potential Antifungal and Antibacterial Agents.</a> Wei, J.J., L. Jin, K. Wan, and C.H. Zhou. Bulletin of the Korean Chemical Society, 2011. 32(1): p. 229-238; ISI[000286538600043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000286148100010">Antimicrobial screening of Terminalia avicennoides and Acalypha wilkesiana. Akpomie, O.O. and S. Olorungbon.</a> African Journal of Biotechnology, 2011. 10(2): p. 180-182; ISI[000286148100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0218-030311.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
