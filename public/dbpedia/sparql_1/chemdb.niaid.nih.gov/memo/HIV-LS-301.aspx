

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-301.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/5oAfhVSaVl6RMyCywqYpcfD1Zv8qP08ZB9sev7ypNjNHn0uyZBpKT42LvQCi7dlizes7b46zC7YFcINRFXk2pV5OefHHZOSrftu8hhE20R16EyRBXd+R7U381o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8B62AC24" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-301-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43757   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Use of b defensin or its inducing agents for treating HIV-1 associated with chemokine receptor CXCR4</b> (Case Western Reserve University, USA)</p>

<p>           Weinberg, Aaron</p>

<p>           PATENT: WO 2004054603 A2;  ISSUE DATE: 20040701</p>

<p>           APPLICATION: 2003; PP: 63 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     2.    43758   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Nido-carborane-containing porphyrin compounds for the treatment and prevention of viral infections</b> ((USA))</p>

<p>           Vicente, Maria Da Graca Henriques and Marzilli, Luigi G</p>

<p>           PATENT: US 2004116385 A1;  ISSUE DATE: 20040617</p>

<p>           APPLICATION: 2003-56868; PP: 22 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     3.    43759   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Anti-AIDS Agents. 60. Substituted 3&#39;R,4&#39;R-Di-O-(-)-camphanoyl-2&#39;,2&#39;-dimethyldihydropyrano[2,3-f]chromone (DCP) Analogues as Potent Anti-HIV Agents</p>

<p>           Yu, D, Chen, CH, Brossi, A, and Lee, KH</p>

<p>           J Med Chem 2004. 47(16): 4072-82</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267246&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15267246&amp;dopt=abstract</a> </p><br />

<p>     4.    43760   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Clinical utility of current NNRTIs and perspectives of new agents in this class under development</p>

<p>           Zhang, Z, Hamatake, R, and Hong, Z</p>

<p>           Antivir Chem Chemother 2004. 15(3): 121-34</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266894&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266894&amp;dopt=abstract</a> </p><br />

<p>     5.    43761   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Modulation of HIV replication by RNA interference</b>((University of Massachusetts, USA)</p>

<p>           Stevenson, Mario and Jacque, Jean-Marc</p>

<p>           PATENT: WO 2004047764 A2;  ISSUE DATE: 20040610</p>

<p>           APPLICATION: 2003; PP: 59 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     6.    43762   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           In Vitro Interaction of the HIV Protease Inhibitor Ritonavir with Herbal Constituents: Changes in P-gp and CYP3A4 Activity</p>

<p>           Patel, J, Buddha, B, Dey, S, Pal, D, and Mitra, AK</p>

<p>           Am J Ther 2004. 11(4): 262-277</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266218&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15266218&amp;dopt=abstract</a> </p><br />

<p>    7.     43763   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Substituted N-phenylpyrrole compounds for inhibition of HIV infection by blocking HIV entry</b>((New York Blood Center, USA)</p>

<p>           Jiang, Shibo and Debnath, Asim Kumar</p>

<p>           PATENT: WO 2004047730 A2;  ISSUE DATE: 20040610</p>

<p>           APPLICATION: 2003; PP: 61 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     8.    43764   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Synthesis of novel thiazolothiazepine based HIV-1 integrase inhibitors</p>

<p>           Aiello, F, Brizzi, A, Garofalo, A, Grande, F, Ragno, G, Dayam, R, and Neamati, N</p>

<p>           Bioorg Med Chem 2004. 12(16): 4459-66</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15265496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15265496&amp;dopt=abstract</a> </p><br />

<p>     9.    43765   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           a-Glucosidase inhibitors from a natural source, and therapeutic use</b>((Council of Scientific and Industrial Research, India)</p>

<p>           Rao, Janaswamy Madhusudana, Srinivas, Pullela Venkata, Anuradha, Vummenthala, Tiwari, Ashok Kumar, Ali, Amtul Zehra, Yadav, Jhillu Singh, and Raghavan, Kondapura Vijaya</p>

<p>           PATENT: WO 2004041295 A1;  ISSUE DATE: 20040521</p>

<p>           APPLICATION: 2002; PP: 28 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   10.    43766   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Synthetic Bivalent CD4-Mimetic Miniproteins Show Enhanced Anti-HIV Activity over the Monovalent Miniprotein</p>

<p>           Li, H, Song, H, Heredia, A, Le, N, Redfield, R, Lewis, GK, and Wang, LX</p>

<p>           Bioconjug Chem 2004. 15(4): 783-9</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15264865&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15264865&amp;dopt=abstract</a> </p><br />

<p>   11.    43767   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           Current status of the non-nucleoside reverse transcriptase inhibitors of human immunodeficiency virus type 1</p>

<p>           Balzarini, J</p>

<p>           Current Topics in Medicinal Chemistry (Sharjah, United Arab Emirates) 2004. 4(9): 921-944</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   12.    43768   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           A unidirectional crosslinking strategy for HIV-1 protease dimerization inhibitors</p>

<p>           Hwang, YS and Chmielewski, J</p>

<p>           Bioorg Med Chem Lett 2004. 14(16):  4297-300</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15261290&amp;dopt=abstract</a> </p><br />

<p>   13.    43769   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Aryl phosphate derivatives of d4T having activity against resistant HIV strains</b>((USA))</p>

<p>           Uckun, Fatih M</p>

<p>           PATENT: US 2004087549 A1;  ISSUE DATE: 20040506</p>

<p>           APPLICATION: 2002-19189; PP: 31 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    43770   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Methods for preventing HIV-1 or HIV-2 infection</b>((New York Blood Center, Inc. USA)</p>

<p>           Neurath, Alexander R, Jiang, Shibo, and Debnath, Asim Kumar</p>

<p>           PATENT: US 6727240 B1;  ISSUE DATE: 20040427</p>

<p>           APPLICATION: 94-15065; PP: 11 pp., Cont.-in-part of U.S. Ser. No. 174,662.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   15.    43771   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           The differential sensitivity of human and rhesus macaque CCR5 to small-molecule inhibitors of human immunodeficiency virus type 1 entry is explained by a single amino acid difference and suggests a mechanism of action for these inhibitors</p>

<p>           Billick, Erika, Seibert, Christoph, Pugach, Pavel, Ketas, Tom, Trkola, Alexandra, Endres, Michael J, Murgolo, Nicholas J, Coates, Elizabeth, Reyes, Gregory R, Baroudy, Bahige M, Sakmar, Thomas P, Moore, John P, and Kuhmann, Shawn E</p>

<p>           Journal of Virology 2004. 78(8): 4134-4144</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   16.    43772   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p><b>           Gp41 peptide derivative fusion inhibitors of HIV infection</b>((USA))</p>

<p>           Xie, Dong and Jiang, He</p>

<p>           PATENT: WO 2004029201 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 51 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   17.    43773   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Orally active CCR5 antagonists as anti-HIV-1 agents 2: synthesis and biological activities of anilide derivatives containing a pyridine N-oxide moiety</p>

<p>           Seto, M, Aramaki, Y, Imoto, H, Aikawa, K, Oda, T, Kanzaki, N, Iizawa, Y, Baba, M, and Shiraishi, M</p>

<p>           Chem Pharm Bull (Tokyo) 2004. 52(7): 818-29</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15256702&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15256702&amp;dopt=abstract</a> </p><br />

<p>   18.    43774   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Inhibition of HIV-1 multiplication by antisense U7 snRNAs and siRNAs targeting cyclophilin A</p>

<p>           Liu, S, Asparuhova, M, Brondani, V, Ziekau, I, Klimkait, T, and Schumperli, D</p>

<p>           Nucleic Acids Res 2004. 32(12): 3752-9</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254276&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15254276&amp;dopt=abstract</a> </p><br />

<p>   19.    43775   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Prospects of HIV-1 entry inhibitors as novel therapeutics</p>

<p>           Pierson, TC, Doms, RW, and Pohlmann, S</p>

<p>           Rev Med Virol  2004. 14(4): 255-70</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15248253&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15248253&amp;dopt=abstract</a> </p><br />

<p>   20.    43776   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           Novel inhibitors of HIV-1 integration</p>

<p>           Witvrouw, M, Van Maele, B, Vercammen, J, Hantson, A, Engelborghs, Y, De Clercq, E, Pannecouque, C, and Debyser, Z</p>

<p>           Current Drug Metabolism 2004. 5(4): 291-304</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   21.    43777   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Recent insights into HIV-1 Vif</p>

<p>           Navarro, F and Landau, NR</p>

<p>           Curr Opin Immunol 2004. 16(4): 477-82</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245742&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15245742&amp;dopt=abstract</a> </p><br />

<p>   22.    43778   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           New Phosphonoformic Acid Derivatives of 3&#39;-Azido-3&#39;-Deoxythymidine</p>

<p>           Shirokova, EA, Jasko, MV, Khandazhinskaya, AL, Ivanov, AV, Yanvarev, DV, Skoblov, YuS, Pronyaeva, TR, Fedyuk, NV, Pokrovskii, AG, and Kukhanova, MK</p>

<p>           Russian Journal of Bioorganic Chemistry (Translation of Bioorganicheskaya Khimiya) 2004. 30(3): 242-249</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    43779   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           In vitro and in vivo transport and delivery of phosphorothioate oligonucleotides with cationic liposomes</p>

<p>           Naoko, Miyano-Kurosaki, Barnor, Jacob Samson, Takeuchi, Hiroaki, Owada, Takashi, Nakashima, Hideki, Yamamoto, Naoki, Matsuzaki, Tetsuo, Shimada, Fumiyuki, and Takaku, Hiroshi</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 2004. 15(2): 93-100</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    43780   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Efficacy and mode of action of hammerhead and hairpin ribozymes against various HIV-1 target sites</p>

<p>           Hotchkiss, G, Maijgren-Steffensson, C, and Ahrlund-Richter, L</p>

<p>           Mol Ther 2004. 10(1): 172-80</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15233952&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15233952&amp;dopt=abstract</a> </p><br />

<p>   25.    43781   HIV-LS-301; PUBMED-HIV-7/26/2004</p>

<p class="memofmt1-3">           Peptide inhibitors of virus-cell fusion: enfuvirtide as a case study in clinical discovery and development</p>

<p>           Cooper, DA and Lange, JM</p>

<p>           Lancet Infect Dis 2004. 4(7): 426-36</p>

<p>HYPERLINK:</p><p><a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15219553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15219553&amp;dopt=abstract</a> </p><br />

<p>   26.    43782   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           HIV-1 gp41 as a target for viral entry inhibition</p>

<p>           Root, Michael J and Steger, HKirby</p>

<p>           Current Pharmaceutical Design 2004.  10(15): 1805-1825</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   27.    43783   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           N-linked glycosylation in the CXCR4 N-tenninus inhibits binding to HIV-1 envelope glycoproteins</p>

<p>           Wang, JB, Babcock, GJ, Choe, H, Farzan, M, Sodroski, J, and Gabuzda, D</p>

<p>           VIROLOGY 2004. 324(1): 140-150</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222140800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222140800014</a> </p><br />

<p>  28.     43784   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           A study on the anti-HIV activity of biflavonoid compounds by using quantum chemical and chemometric methods</p>

<p>           Molfetta, FA, Honorio, KM, Alves, CN, and da Silva, ABF</p>

<p>           THEOCHEM 2004. 674(1-3): 191-197</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   29.    43785   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           Roles of conformational and positional adaptability in structure-based design of TMC125-R165335 (Etravirine) and related non-nucleoside reverse transcriptase inhibitors that are highly potent and effective against wild-type and drug-resistant HIV-1 variants</p>

<p>           Das, Kalyan, Clark, Arthur D Jr, Lewi, Paul J, Heeres, Jan, De Jonge, Marc R, Koymans, Lucien MH, Vinkers, HMaarten, Daeyaert, Frederik, Ludovici, Donald W, Kukla, Michael J, De Corte, Bart, Kavash, Robert W, Ho, Chih Y, Ye, Hong, Lichtenstein, MarkA, Andries, Koen, Pauwels, Rudi, De Bethune, Marie-Pierre, Boyer, Paul L, Clark, Patrick, Hughes, Stephen H, Janssen, Paul AJ, and Arnold, Eddy</p>

<p>           Journal of Medicinal Chemistry 2004.  47(10): 2550-2560</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   30.    43786   HIV-LS-301; SCIFINDER-HIV-7/21/2004</p>

<p class="memofmt1-3">           Non-nucleoside reverse transcriptase inhibitors (NNRTIs): past, present, and future</p>

<p>           De Clercq, Erik</p>

<p>           Chemistry &amp; Biodiversity 2004. 1(1): 44-64</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   31.    43787   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           Chemically sulfated Escherichia coli K5 polysaccharide derivatives as extracellular HIV-1 Tat protein antagonists</p>

<p>           Urbinati, C, Bugatti, A, Oreste, P, Zoppetti, G, Waltenberger, J, Mitola, S, Ribatti, D, Presta, M, and Rusnati, M</p>

<p>           FEBS LETT 2004. 568(1-3): 171-177</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222205200034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222205200034</a> </p><br />

<p>   32.    43788   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           Small-molecule antagonists of CCR5 and CXCR4: A promising new class of anti-HIV-1 drugs</p>

<p>           Seibert, C and Sakmar, TP</p>

<p>           CURR PHARM DESIGN 2004. 10(17): 2041-2062</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222130200006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222130200006</a> </p><br />

<p>   33.    43789   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           Highly active Antiretroviral therapy and viral response in HIV type 2 infection</p>

<p>           Mullins, C, Eisen, G, Popper, S, Sarr, AD, Sankale, JL, Berger, JJ, Wright, SB, Chang, HR, Coste, G, Cooley, TP, Rice, P, Skolnik, PR, Sullivan, M, and Kanki, PJ</p>

<p>           CLIN INFECT DIS 2004. 38(12): 1771-1779</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222087500021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222087500021</a> </p><br />

<p>   34.    43790   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           Down-regulation of CXCR-4 and CCR-5 expression by interferon-gamma is associated with inhibition of chemotaxis and human immunodeficiency virus (HIV) replication but not HIV entry into human monocytes</p>

<p>           Creery, D, Weiss, W, Lim, WT, Aziz, Z, Angel, JB, and Kumar, A</p>

<p>           CLIN EXP IMMUNOL 2004. 137(1): 156-165</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222050100022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222050100022</a> </p><br />

<p>  35.     43791   HIV-LS-301; WOS-HIV-7/18/2004</p>

<p class="memofmt1-3">           The natural history of HIV-1 and HIV-2 infections in adults in Africa: a literature review</p>

<p>           Jaffar, S, Grant, AD, Whitworth, J, Smith, PG, and Whittle, H</p>

<p>           B WORLD HEALTH ORGAN 2004. 82(6): 462-469</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222126900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222126900012</a> </p><br />

<p>   36.    43792   HIV-LS-301; WOS-HIV-7/25/2004</p>

<p class="memofmt1-3">           LNA/DNA chimeric oligomers mimic RNA aptamers targeted to the TAR RNA element of HIV-1</p>

<p>           Darfeuille, F, Hansen, JB, Orum, H, Primo, CD, and Toulme, JJ</p>

<p>           NUCLEIC ACIDS RES 2004. 32(10): 3101-3107</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222118600022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222118600022</a> </p><br />

<p>   37.    43793   HIV-LS-301; WOS-HIV-7/25/2004</p>

<p class="memofmt1-3">           Regulation of CXCR4 receptor dimerization by the chemokine SDF-1 alpha and the HIV-1 coat protein gp120: A fluorescence resonance energy transfer (FRET) study</p>

<p>           Toth, PT, Ren, DJ, and Miller, RJ</p>

<p>           J PHARMACOL EXP THER 2004. 310(1):  8-17</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222138300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222138300002</a> </p><br />

<p>   38.    43794   HIV-LS-301; WOS-HIV-7/25/2004</p>

<p class="memofmt1-3">           Isolation and structure of antagonists of chemokine receptor (CCR5)</p>

<p>           Jayasuriya, H, Herath, KB, Ondeyka, JG, Polishook, JD, Bills, GF, Dombrowski, AW, Springer, MS, Siciliano, S, Malkowitz, L, Sanchez, M, Guan, ZQ, Tiwari, S, Stevenson, DW, Borris, RP, and Singh, SB</p>

<p>           J NAT PROD 2004. 67(6): 1036-1038</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222277100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222277100023</a> </p><br />

<p>   39.    43795   HIV-LS-301; WOS-HIV-7/25/2004</p>

<p class="memofmt1-3">           Narrow substrate specificity and sensitivity toward ligand-binding site mutations of human T-cell leukemia virus type 1 protease</p>

<p>           Kadas, J, Weber, IT, Bagossi, P, Miklossy, G, Boross, P, Oroszlan, S, and Tozser, J</p>

<p>           J BIOL CHEM 2004. 279(26): 27148-27157</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222120400041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222120400041</a> </p><br />

<p>   40.    43796   HIV-LS-301; WOS-HIV-7/25/2004</p>

<p class="memofmt1-3">           Structural and biochemical effects of human immunodeficiency virus mutants resistant to non-nucleoside reverse transcriptase inhibitors</p>

<p>           Domaoal, RA and Demeter, LM</p>

<p>           INT J BIOCHEM CELL B 2004. 36(9): 1735-1751</p>

<p>HYPERLINK:</p><p><a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222255900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222255900007</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
