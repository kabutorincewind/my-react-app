

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-02-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="b+8ShGveGzzNOcmjK8cM+fyp3tN06rpxaQdO7ru8hD8vdgWlGgFz9V5r1AS+3gMqaiq3eCXgg/3spBOlRZcbQY0Gxi2a8x7v6S9AtVoFrAlmiYb6/Q+50CBFuK0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE81DAEB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 4 - February 17, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p> 

    <p class="title">1)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19219846?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antiviral drug discovery: Ten more compounds, and ten more stories (part B).</a>  De Clercq E.  Med Res Rev. 2009 Feb 13. [Epub ahead of print]</p>

    <p class="source">PMID: 19219846</p> 

    <p class="title">2)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19211937?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The F12-Vif derivative Chim3 inhibits HIV-1 replication in CD4+ T lymphocytes and CD34+-derived macrophages by blocking HIV-1 DNA integration.</a>  Porcellini S, Alberici L, Gubinelli F, Lupo R, Olgiati C, Rizzardi GP, Bovolenta C.  Blood. 2009 Feb 11. [Epub ahead of print]</p>

    <p class="source">PMID: 19211937</p> 

    <p class="title">3)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19208267?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pharmaceutical approaches to eradication of persistent HIV infection.</a>  Bowman MC, Archin NM, Margolis DM.  Expert Rev Mol Med. 2009 Feb 11;11:e6.</p>

    <p class="source">PMID: 19208267</p> 

    <p class="title">4)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19206074?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Effects of dimerization of the cell-penetrating peptide Tat analog on antimicrobial activity and mechanism of bactericidal action.</a>  Zhu WL, Shin SY.  J Pept Sci. 2009 Feb 10. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19206074</p> 

    <p class="title">5)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19193159?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Additivity in the analysis and design of HIV protease inhibitors.</a>  Jorissen RN, Reddy GS, Ali A, Altman MD, Chellappan S, Anjum SG, Tidor B, Schiffer CA, Rana TM, Gilson MK.  J Med Chem. 2009 Feb 12;52(3):737-54.</p>

    <p class="pmid">PMID: 19193159</p> 

    <p class="title">6)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19171484?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">1-Amido-1-phenyl-3-piperidinylbutanes - CCR5 antagonists for the treatment of HIV. Part 1.</a>  Barber CG, Blakemore DC, Chiva JY, Eastwood RL, Middleton DS, Paradowski KA.  Bioorg Med Chem Lett. 2009 Feb 15;19(4):1075-1079. Epub 2009 Jan 10.</p>

    <p class="pmid">PMID: 19171484</p> 

    <p class="title">7)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19167884?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The design and discovery of novel amide CCR5 antagonists.</a>  Pryde DC, Corless M, Fenwick DR, Mason HJ, Stammen BC, Stephenson PT, Ellis D, Bachelor D, Gordon D, Barber CG, Wood A, Middleton DS, Blakemore DC, Parsons GC, Eastwood R, Platts MY, Statham K, Paradowski KA, Burt C, Klute W.  Bioorg Med Chem Lett. 2009 Feb 15;19(4):1084-1088. Epub 2009 Jan 10.</p>

    <p class="pmid">PMID: 19167884</p> 

    <p class="title">8)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19167883?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Tricyclic HIV integrase inhibitors: VI. SAR studies of &#39;benzyl flipped&#39; C3-substituted pyrroloquinolines.</a>  Metobo S, Mish M, Jin H, Jabri S, Lansdown R, Chen X, Tsiang M, Wright M, Kim CU.  Bioorg Med Chem Lett. 2009 Feb 15;19(4):1187-1190. Epub 2008 Dec 25.</p>

    <p class="pmid">PMID: 19167883</p> 

    <p class="title">9)      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19140683?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery of chiral cyclopropyl dihydro-alkylthio-benzyl-oxopyrimidine (S-DABO) derivatives as potent HIV-1 reverse transcriptase inhibitors with high activity against clinically relevant mutants.</a>  Radi M, Maga G, Alongi M, Angeli L, Samuele A, Zanoli S, Bellucci L, Tafi A, Casaluce G, Giorgi G, Armand-Ugon M, Gonzalez E, Esté JA, Baltzinger M, Bec G, Dumas P, Ennifar E, Botta M.  J Med Chem. 2009 Feb 12;52(3):840-51.</p>

    <p class="pmid">PMID: 19140683</p> 

    <p class="title">10)  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19090525?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structural Modifications of DAPY Analogues with Potent Anti-HIV-1 Activity.</a>  Feng XQ, Liang YH, Zeng ZS, Chen FE, Balzarini J, Pannecouque C, De Clercq E.  ChemMedChem. 2009 Feb 13;4(2):219-224.</p>

    <p class="source">PMID: 19090525</p> 

    <p class="title">11)  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19073602?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Interactions of HIV-1 Inhibitory Peptide T20 with the gp41 N-HR Coiled Coil.</a>  Champagne K, Shishido A, Root MJ.  J Biol Chem. 2009 Feb 6;284(6):3619-27. Epub 2008 Dec 10.</p>

    <p class="pmid">PMID: 19073602</p>

    <p class="memofmt2-2">ISI Web of Science citations:</p>

    <p class="title">12)  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262529800001">The inhibition of assembly of HIV-1 virus-like particles by 3-O-(3 &#39;,3 &#39;-dimethylsuccinyl) betulinic acid (DSB) is counteracted by Vif and requires its Zinc-binding domain.</a>  DaFonseca S, Coric P, Gay B, Hong SS, Bouaziz S, Boulanger P.  VIROLOGY JOURNAL.  2008 Dec 23;5:162. </p>

    <p class="title">13)  <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000262613000013&amp;SID=2FOKa7bDCIeE6Bd3fde&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFai">Synthesis and antimicrobial evaluation of 6-azauracil non-nucleosides</a>.  El-Brollosy NR.  MONATSHEFTE FUR CHEMIE.  2008 DEC;139(12):1483-1493. </p>

    <p class="title">14)  <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000262544800002&amp;SID=2FOKa7bDCIeE6Bd3fde&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFail">In search of small molecules blocking interactions between HIV proteins and intracellular cofactors</a>.  Busschots K, De Rijck J, Christ F, Debyser Z. MOLECULAR BIOSYSTEMS.  2009;5(1):21-31. </p>

    <p class="title">15)  <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000262622600003&amp;SID=2FOKa7bDCIeE6Bd3fde&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFail">DESIGN OF HYBRID INHIBITORS TO HIV-1 PROTEASE</a>.  Zhang DW, Huang PL, Lee-Huang S, Zhang JZH.  JOURNAL OF THEORETICAL &amp; COMPUTATIONAL CHEMISTRY.  2008 AUG;7(4):484-503. </p>

    <p class="title">16)  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262589900006">Non-nucleoside HIV-1 reverse transcriptase inhibitors di-halo-indolyl aryl sulfones achieve tight binding to drug-resistant mutants by targeting the enzyme-substrate complex.</a>  Samuele A, Kataropoulou A, Viola M, Zanoli S, La Regina G, Piscitelli F, Silvestri R, Maga G.  ANTIVIRAL RESEARCH.  2009 JAN;81(1):47-55. </p>

    <p class="title">17)  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262538500012">Characterization of Cyclodextrin Inclusion Complexes of the Anti-HIV Non-Nucleoside Reverse Transcriptase Inhibitor UC781.</a>  Yang HT, Parniak MA, Isaacs CE, Hillier SL, Rohan LC.  AAPS JOURNAL.  2008 DEC;10(4):606-613. </p>

    <p class="title">18)  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262578200006">Computer-based design of novel HIV-1 entry inhibitors: neomycin conjugated to arginine peptides at two specific sites.</a>  Berchanski A, Lapidot A.  JOURNAL OD MOLECULAR MODELING.  2009 MAR;15(3):281-294. </p>

    <p class="memofmt2-2">Patent citations:</p>

    <p class="title">19)                                                <a href="http://www.ncbi.nlm.nih.gov/pubmed/19219742?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Short Synthesis and Antiviral Activity of Acyclic Phosphonic Acid Nucleoside Analogues</a>.     Liu, Lian Jin; Yoo, Jin Cheol; Hong, Joon Hee.    College of Pharmacy,  Chosun University,  Kwangju,  S. Korea.    Nucleosides, Nucleotides &amp; Nucleic Acids  (2009),  28(2),  150-164.  Publisher: Taylor &amp; Francis, Inc.,  CODEN: NNNAFY  ISSN: 1525-7770.  Journal  written in English.    AN 2009:188831    CAPLUS      </p>

    <p class="title">20)                                                <a href="http://www.ncbi.nlm.nih.gov/pubmed/19006142?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Diphenyl ether non-nucleoside reverse transcriptase inhibitors with excellent potency against resistant mutant viruses and promising pharmacokinetic properties</a>.      Sweeney, Zachary K.; Kennedy-Smith, Joshua J.; Wu, Jeffrey; Arora, Nidhi; Billedeau, J. Roland; Davidson, James P.; Fretland, Jennifer; Hang, Julie Q.; Heilek, Gabrielle M.; Harris, Seth F.; Hirschfeld, Donald; Inbar, Petra; Javanbakht, Hassan; Jernelius, Jesper A.; Jin, Qingwu; Li, Yu; Liang, Weiling; Roetz, Ralf; Sarma, Keshab; Smith, Mark; Stefanidis, Dimitrio; Su, Guoping; Suh, Judy M.; Villasenor, Armando G.; Welch, Michael; Zhang, Fang-Jie; Klumpp, Klaus.    Department of Medicinal Chemistry,  Roche Palo Alto,  Palo Alto,  CA,  USA.    ChemMedChem  (2009),  4(1),  88-99.  Publisher: Wiley-VCH Verlag GmbH &amp; Co. KGaA,  CODEN: CHEMGX  ISSN: 1860-7179.  Journal  written in English.    AN 2009:166475    CAPLUS      </p>

    <p class="title">21)  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18498083?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Introducing metallocene into a triazole peptide conjugate reduces its off-rate and enhances its affinity and antiviral potency for HIV-1 gp120</a>.     Gopi Hosahudya; Cocklin Simon; Pirrone Vanessa; McFadden Karyn; Tuzer Ferit; Zentner Isaac; Ajith Sandya; Baxter Sabine; Jawanda Navneet; Krebs Fred C; Chaiken Irwin M    Department of Biochemistry and Molecular Biology, Drexel University College of Medicine, Philadelphia, PA 19102, USA      Journal of molecular recognition : JMR  (2009),  22(2),  169-74.  Journal code: 9004580.  ISSN:0952-3499.  Journal; Article; (JOURNAL ARTICLE)  written in English.    PubMed ID 18498083 AN 2009127898    In-process for MEDLINE (Copyright (C) 2009 U.S. National Library of Medicine on SciFinder (R))</p>

    <p class="title">22)                                                <b>Preparation of piperidine derivatives as agonists of chemokine receptor activity.</b>  <b>  </b>  Huck, Jacques; Ooms, Frederic; Tyrchan, Christian; Hoveyda, Hamid R.  (Euroscreen S.A., Belg.).    PCT Int. Appl.  (2009),     199pp.  CODEN: PIXXD2  WO  2009010478  A2  20090122  Application: WO  2008-EP59137  20080711.    <b> </b></p>

    <p class="title">23)                                                <b>Preparation of piperidine-4-acetic acid derivatives as chemokine receptor agonists.</b>  <b>  </b>  Huck, Jacques; Ooms, Frederic; Parcq, Julien; Regereau, Yannick; Hoveyda, Hamid R.; Dutheuil, Guillaume.  (Euroscreen S.A., Belg.).    PCT Int. Appl.  (2009),     224pp.  CODEN: PIXXD2  WO  2009010477  A1  20090122  Application: WO  2008-EP59136  20080711.    <b> </b></p>

    <p class="title">24)                                                <b>Preparation of novel therapeutic compounds containing heterocylic carboxamide cores for use as kinase inhibitors.</b>  <b>  </b>  Breinlinger, Eric C.; Cusack, Kevin P.; Hobson, Adrian D.; Li, Bin; Gordon, Thomas D.; Stoffel, Robert H.; Wallace, Grier A.; Gronsgaard, Pintipa; Wang, Lu.  (Abbott Laboratories, USA).    PCT Int. Appl.  (2009),     224pp.  Application: WO  2008-US8645  20080715.      <b> </b></p>

    <p class="title">25)                                                <b>Preparation of diphenylaminoethylpiperidinecarboxylic acid benzylamide derivatives and analogs as CCR5 agonists.</b>  <b>  </b>  Huck, Jacques; Ooms, Frederic; Parcq, Julien.  (Euroscreen S.A., Belg.).    PCT Int. Appl.  (2009),     184pp.  CODEN: PIXXD2  WO  2009010480  A1  20090122  Application: WO  2008-EP59139  20080711.    <b> </b></p>

    <p class="title">26)                                                <b>Preparation of heterocyclic methylene piperidine derivatives as therapeutic chemokine receptor modulators.</b>  <b>  </b>  Huck, Jacques; Ooms, Frederic; Regereau, Yannick.  (Euroscreen S.A., Belg.).    PCT Int. Appl.  (2009),     167pp.  CODEN: PIXXD2  WO  2009010479  A2  20090122  Application: WO  2008-EP59138  20080711.    <b> </b></p>

    <p class="title">27)                                                <b>Antiviral properties of zosteric acid and related molecules.</b>  <b>  </b>  Michael, Scott F.; Isern, Sharon; Costin, Joshua.  (Florida Gulf Coast University, USA).    PCT Int. Appl.  (2009),     36pp.  Application: WO  2008-US69808  20080711.    <b> </b></p>

    <p class="title">28)                                                <b>Apricitabine - a novel nucleoside reverse transcriptase inhibitor for the treatment of HIV infection that is refractory to existing drugs.</b>  <b>  </b>  Cox, Susan; Southby, Justine.    Avexa Ltd.,  Richmond,  VA,  USA.    Expert Opinion on Investigational Drugs  (2009),  18(2),  199-209.  Publisher: Informa Healthcare,  CODEN: EOIDER  ISSN: 1354-3784.  Journal  written in English.    AN 2009:74143    CAPLUS     <b> </b></p>

    <p class="title">29)                                                <b>Substituted nucleoside derivatives with antiviral and antimicrobial properties.</b>  <b>  </b>  Doncel, Gustavo F.; Parang, Keykavous; Agrawal, Hitesh Kumar.  (Eastern Virginia Medical School, USA).    PCT Int. Appl.  (2009),     96pp.  CODEN: PIXXD2  WO  2009009625  A2  20090115  Application: WO  2008-US69571  20080709.       <b> </b></p>

    <p class="title">30)                                                <b>Preparation of deuterium substituted pyrimidine derivatives as HIV integrase inhibitors.</b>  <b> </b>Harbeson, Scott L.  (Concert Pharmaceuticals Inc., USA).    PCT Int. Appl.  (2009),     40pp.  CODEN: PIXXD2  WO  2009009531  A2  20090115  Application: WO  2008-US69425  20080708.    <b> </b></p>

    <p class="title">31)                                                <b>Crystalline form of 4-[[4-[[4-(2-cyanoethenyl)-2,6-dimethylphenyl]amino]-2 pyrimidinyl]amino]benzonitrile, its preparation and therapeutical uses thereof.</b>  <b>  </b>  Stokbroekx, Sigrid Carl Maria; Leys, Carina; Theunissen, Elisabeth Maria Helene Egide Ghislaine; Baert, Lieven Elvire Colette.  (Tibotec Pharmaceuticals Ltd., Ire.).    PCT Int. Appl.  (2009),     35pp.  Application: WO  2008-EP59054  20080711.      <b> </b></p>

    <p class="title">32)                                                <b>A CXC chemokine receptor 4 (CXCR4) antagonistic polypeptide.</b>  <b>  </b>  Forssmann, Wolf-Georg; Kirchhoff, Frank; Muench, Jan; Staendker, Ludger.  (Pharis Biotec G.m.b.H., Germany).    PCT Int. Appl.  (2009),     55pp.  CODEN: PIXXD2  WO  2009004054  A2  20090108.  Application: WO  2008-EP58566  20080703.    <b> </b></p>

    <p class="title">33)                                                <b>Novel triazolopyridazine derivatives as kinase inhibitors and their preparation and use in the treatment of diseases.</b>  <b>  </b>  Calderwood, David J.; Bonafoux, Dominique F.; Burchat, Andrew; Ding, Ping; Frank, Kristine E.; Hoemann, Michael Z.; Mullen, Kelly D.; Davis, Heather M.  (Abbott Laboratories, USA).    PCT Int. Appl.  (2009),     226pp.  CODEN: PIXXD2  WO  2009005675  A1  20090108  Application: WO  2008-US7926  20080626.      <b> </b></p>

    <p class="title">34)                                                <b>Stereoisomers of tricyclodecan-9-yl-xanthogenate and their preparation and use in the treatment of viral diseases.</b>  <b>  </b>  Tomioka, Miyuki; Hasegawa, Ko.  (Shogoo Pharmaceuticals A.-G., Switz.).    PCT Int. Appl.  (2009),     94pp.  CODEN: PIXXD2  WO  2009003711  A2  20090108  Application: WO  2008-EP5460  20080703.    <b> </b></p>

    <p class="title">35)                                                <b>Preparation of pyrimidindiones as HIV reverse transcriptase inhibitors.</b>  <b>  </b>  Guo, Hongyan; Kim, Choung U.; Lee, Ill Young; Mitchell, Michael L.; Rhodes, Gerry; Son, Jong Chan; Xu, Lianhong.  (Gilead Sciences, Inc., USA; Korean Research Institute of Chemical Technology).    PCT Int. Appl.  (2009),     466pp.  CODEN: PIXXD2  WO  2009005674  A2  20090108    Application: WO  2008-US7925  20080626.    <b> </b></p>

    <p class="title">36)                                                <b>Preparation of pyrimidinediones as HIV reverse transcriptase inhibitors.</b>  <b>  </b>  He, Gong-Xin; Kim, Choung U.; Mitchell, Michael L.; Xu, Lianhong.  (Gilead Sciences, Inc., USA).    PCT Int. Appl.  (2009),     145pp.  CODEN: PIXXD2  WO  2009005693  A1  20090108  Application: WO  2008-US7970  20080626.    <b> </b></p>

    <p class="title">37)                                                <b>Novel azoles and related derivatives as non-nucleoside reverse transcriptase inhibitors (NNRTIs) in antiviral therapy (HIV).</b>  <b>  </b>  Jorgensen, William L.  (Yale University, USA).    PCT Int. Appl.  (2009),     68pp.  CODEN: PIXXD2  WO  2009005811  A1  20090108  Application: WO  2008-US8214  20080702.    <b> </b></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
