

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-385.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ogQwfQGTVHGxgaRTRDqBF0v22sVijqD2Iip6+MgaSDqVpjAqtRJULAbhs8Rcnne/ZqAkdUBJZJRqCkWkjhM5IS/tjV2QUOnBQppz0fQ1RHxw6zeAPaMNMGzOfZU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="69FB145E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-385-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.            OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of N-(aryl)-2-thiophen-2-ylacetamides series as a new class of antitubercular agents</p>

    <p>          Lourenco, Maria Cristina Silva, Vicente, Felipe Rodrigues, Henriques, Maria das Gracas Muller de Oliveira, Peixoto Candea, Andre Luis, Borges Goncalves, Raoni Schroeder, Nogueira, Thais Cristina M, de Lima Ferreira, Marcelle, and Nora de Souza, Marcus Vinicius</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(24): 6895-6898</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4PTMXRK-3/2/fd2b97b4c368b47b7e6a0f4f0c3731de</span></p>

    <p>2.            OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          In vitro antimycobacterial spectrum of a diarylquinoline ATP synthase inhibitor</p>

    <p>          Huitric E, Verhasselt P, Andries K, and Hoffner SE</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.  51(11): 4202-4</p>

    <p>            <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709466&amp;dopt=abstract</span></p>

    <p>3.            OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis, antifungal and antimycobacterial activities of new bis-imidazole derivatives, and prediction of their binding to P45014DM by molecular docking and MM/PBSA method</p>

    <p>          Zampieri, Daniele, Mamolo, Maria Grazia, Vio, Luciano, Banfi, Elena, Scialino, Giuditta, Fermeglia, Maurizio, Ferrone, Marco, and Pricl, Sabrina</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(23): 7444-7458</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-3/2/7a48f0f3fbff12b369af939215c0a0f6</span></p>

    <p>4.            OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Inhibition of the Mycobacterium tuberculosis enoyl acyl carrier protein reductase InhA by arylamides</p>

    <p>          He X, Alian A, and Ortiz de Montellano PR</p>

    <p>          Bioorg Med Chem <b>2007</b>.  15(21): 6649-58</p>

    <p>            <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17723305&amp;dopt=abstract</span></p>

    <p>5.            OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          A Novel Mechanism for Substrate Inhibition in Mycobacterium tuberculosis D-3-Phosphoglycerate Dehydrogenase</p>

    <p>          Burton RL, Chen S, Xu XL, and Grant GA</p>

    <p>          J Biol Chem <b>2007</b>.  282(43): 31517-24</p>

    <p>            <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17761677&amp;dopt=abstract</span></p>

    <p>6.            OI-LS-385; WOS-OI-11//7/2007</p>

    <p class="memofmt1-2">          Immunological and Microbiological Activity of Davilla Elliptica St. Hill. (Dilleniaceae) Against Mycobacterium Tuberculosis</p>

    <p>          Lopes, F. C. M. <i>et al.</i></p>

    <p>          Memorias Do Instituto Oswaldo Cruz <b>2007</b>.  102(6): 769-772</p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000250007400018</span></p>

    <p>7.            OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis, biological activity, and SAR of antimycobacterial 2- and 8-substituted 6-(2-furyl)-9-(p-methoxybenzyl)purines</p>

    <p>          Braendvang, Morten and Gundersen, Lise-Lotte</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(22): 7144-7165</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-9/2/f095dfa0ccdb8c542d480c3ee36982cd</span></p>

    <p class="NoSpacing">  8.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="NoSpacing memofmt1-2">          Design, synthesis and antitubercular activity of diarylmethylnaphthol derivatives</p>

    <p class="NoSpacing">          Das SK, Panda G, Chaturvedi V, Manju YS, Gaikwad AK, and Sinha S</p>

    <p class="NoSpacing">          Bioorg Med Chem Lett <b>2007</b>.  17(20): 5586-9</p>

    <p class="NoSpacing">                <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764933&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17764933&amp;dopt=abstract</a> </p><br />

    <p class="memofmt1-1"> </p>

    <p>9.            OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Antibacterial and antimycobacterial activities of South African Salvia species and isolated compounds from S. chamelaeagnea</p>

    <p>          Kamatou, G P P, Van Vuuren, S F, Van Heerden, F R, Seaman, T, and Viljoen, A M</p>

    <p>          South African Journal of Botany <b>2007</b>.  73(4): 552-557</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B7XN9-4P18BNF-1/2/d60d84089ac378f84a1bd3085a0b9b3f</span></p>

    <p> </p>

    <p>10.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Marine natural products from the Turkish sponge Agelas oroides that inhibit the enoyl reductases from Plasmodium falciparum, Mycobacterium tuberculosis and Escherichia coli</p>

    <p>          Tasdemir D, Topaloglu B, Perozzo R, Brun R, O&#39;Neill R, Carballeira NM, Zhang X, Tonge PJ, Linden A, and Ruedi P</p>

    <p>          Bioorg Med Chem <b>2007</b>.  15(21): 6834-45</p>

    <p>            <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17765547&amp;dopt=abstract</span></p>

    <p>11.          OI-LS-385; WOS-OI-11//7/2007</p>

    <p class="memofmt1-2">          14b-Chloro-4a-Methoxy-3,3-Dimethyl-2,3,4a,14b-Tetrahydro-1h-Benzo[a]Pyra No[2,3-C] Phenazine: a New Active Structural Type Against Mycobacterium Tuberculosis</p>

    <p>          Nunes, I. K. D. <i>et al.</i></p>

    <p>          Acta Crystallographica Section E-Structure Reports Online <b>2007</b>.  63: O3686-U1945</p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249759900178</span></p>

    <p> </p>

    <p>12.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis and antituberculosis activity of new thiazolylhydrazone derivatives</p>

    <p>          Turan-Zitouni, Gulhan, Ozdemir, Ahmet, Asim Kaplancikli, Zafer, Benkli, Kadriye, Chevallet, Pierre, and Akalin, Gulsen</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 5508</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6VKY-4P6M7WK-4/2/a72cffbae32456e7a7fd6e1769de7de2</span></p>

    <p>13.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Antimycobacterial activity of diphenylpyraline derivatives</p>

    <p>          Weis, Robert, Faist, Johanna, di Vora, Ulrike, Schweiger, Klaus, Brandner, Barbara, Kungl, Andreas J, and Seebacher, Werner</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 5508</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6VKY-4P59WXV-6/2/1fe389b72dc1cb518a6ed27a5cb8e68c</span></p>

    <p>14.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Rational Design of 5&#39;-Thiourea-Substituted alpha-Thymidine Analogues as Thymidine Monophosphate Kinase Inhibitors Capable of Inhibiting Mycobacterial Growth</p>

    <p>          Daele IV, Munier-Lehmann H, Froeyen M, Balzarini J, and Calenbergh SV</p>

    <p>          J Med Chem <b>2007</b>.  50(22): 5281-5292</p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17910427&amp;dopt=abstract</span></p>

    <p>15.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Oxadiazole mannich bases: Synthesis and antimycobacterial activity</p>

    <p>          Ali, Mohamed Ashraf and Shaharyar, Mohammad</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(12): 3314-3316</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4NF4F18-4/2/4dcb3d555c1a666ffdb8889aa9ab3b45</span></p>

    <p>16.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Hybrid molecules of estrone: New compounds with potential antibacterial, antifungal, and antiproliferative activities</p>

    <p>          Adamec, J, Beckert, R, Wei[ss], D, Klimesova, V, Waisser, K, Mollmann, U, Kaustova, J, and Buchta, V</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(8): 2898-2906</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4N206XD-9/2/f0132352ebe4324a0e7de5908f59fb14</span></p>

    <p>17.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Antimycobacterial Activities of Novel 1-(Cyclopropyl/tert-butyl/4-fluorophenyl)-1,4-dihydro- 6-nitro-4-oxo-7-(substituted secondary amino)-1,8-naphthyridine-3-carboxylic Acid</p>

    <p>          Sriram D, Senthilkumar P, Dinakaran M, Yogeeswari P, China A, and Nagaraja V</p>

    <p>          J Med Chem <b>2007</b>.                           </p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17960928&amp;dopt=abstract</span></p>

    <p>18.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          5&#39;-O-[(N-Acyl)sulfamoyl]adenosines as Antitubercular Agents that Inhibit MbtA: An Adenylation Enzyme Required for Siderophore Biosynthesis of the Mycobactins</p>

    <p>          Qiao C, Gupte A, Boshoff HI, Wilson DJ, Bennett EM, Somu RV, Barry CE 3rd, and Aldrich CC</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17967002&amp;dopt=abstract</span></p>

    <p>19.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of guanylsulfonamides</p>

    <p>          Patel, Pratik R, Ramalingan, Chennan, and Park, Yong-Tae</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(23): 6610-6614</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4PPMXM9-2/2/6063d2d0c604f835470773dffbbfccf7</span></p>

    <p>20.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Rational design of N-alkyl derivatives of 2-amino-2-deoxy-d-glucitol-6P as antifungal agents</p>

    <p>          Melcer, Anna, Lacka, Izabela, Gabriel, Iwona, Wojciechowski, Marek, Liberek, Beata, Wisniewski, Andrzej, and Milewski, Slawomir</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(23): 6602-6606</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4PRRBHV-1/2/c067592962a2aee851bce54e30eea651</span></p>

    <p>21.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Synthesis and antifungal properties of alpha-methoxy and alpha-hydroxyl substituted 4-thiatetradecanoic acids</p>

    <p>          Carballeira NM, O&#39;neill R, and Parang K</p>

    <p>          Chem Phys Lipids <b>2007</b>.  150(1): 82-8</p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662704&amp;dopt=abstract</span></p>

    <p>22.          OI-LS-385; WOS-OI-11/07/2007</p>

    <p class="memofmt1-2">          Synthesis of Novel Triazole Derivatives as Inhibitors of Cytochrome P450 14 Alpha-Demethylase (Cyp51)</p>

    <p>          Sun, Q. Y. <i>et al.</i></p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(9): 1226-1233</p>

    <p>               <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000249540900009</span></p>

    <p>23.          OI-LS-385; PUBMED-OI-11/09/07</p>

    <p class="memofmt1-2">          Synthesis and Antifungal Activities of Novel 2-Aminotetralin Derivatives</p>

    <p>          Yao B, Ji H, Cao Y, Zhou Y, Zhu J, Lu J, Li Y, Chen J, Zheng C, Jiang Y, Liang R, and Tang H</p>

    <p>          J Med Chem <b>2007</b>.  50(22): 5293-5300</p>

    <p>          <span class="memofmt1-3">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17900179&amp;dopt=abstract</span></p>

    <p>24.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis of chlorogenic acid derivatives with promising antifungal activity</p>

    <p>          Ma, Chao-Mei, Kully, Maureen, Khan, Jehangir K, Hattori, Masao, and Daneshtalab, Mohsen</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(21): 6830-6833</p>

    <p>            <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4PGGP0Y-F/2/395b904b5c2593ca239d289f004ce2ce</span></p>

    <p> </p>

    <p>25.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Mechanism-of-Action Determination of GMP Synthase Inhibitors and Target Validation in Candida albicans and Aspergillus fumigatus</p>

    <p>          Rodriguez-Suarez, Roberto, Xu, Deming, Veillette, Karynn, Davison, John, Sillaots, Susan, Kauffman, Sarah, Hu, Wenqi, Bowman, Joel, Martel, Nick, Trosok, Steve, Wang, Hao, Zhang, Li, Huang, Li-Yin, Li, Yang, Rahkhoodaee, Fariba, Ransom, Tara, Gauvin, Daniel, Douglas, Cameron, Youngman, Phil, Becker, Jeff, Jiang, Bo, and Roemer, Terry</p>

    <p>          Chemistry &amp; Biology <b>2007</b>.  14(10): 1163-1175</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6VRP-4R0C108-C/2/6fbc0b171bd7af71432bf01077408aec</span></p>

    <p>26.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of new chalcones containing piperazine or 2,5-dichlorothiophene moiety</p>

    <p>          Tomar, V, Bhattacharjee, G, Kamaluddin, and Ashok Kumar</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(19): 5321-5324</p>

    <p>            <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF9-4PF1WC3-B/2/ea3a137881420fb6b0cbcbe5fc1be002</span></p>

    <p> </p>

    <p>27.          OI-LS-385; EMBASE-OI-11/9/2007</p>

    <p class="memofmt1-2">          Synthesis, anti-Toxoplasma gondii and antimicrobial activities of benzaldehyde 4-phenyl-3-thiosemicarbazones and 2-[(phenylmethylene)hydrazono]-4-oxo-3-phenyl-5-thiazolidineacetic acids</p>

    <p>          de Aquino, Thiago M, Liesen, Andre P, da Silva, Rosa E A, Lima, Vania T, Carvalho, Cristiane S, de Faria, Antonio R, de Araujo, Janete M, de Lima, Jose G, Alves, Antonio J, de Melo, Edesio J T, and Goes, Alexandre J S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof</p>

    <p>          <span class="memofmt1-3">http://www.sciencedirect.com/science/article/B6TF8-4PP773F-2/2/da78fb846563815de1c8bea059791b3a</span></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
