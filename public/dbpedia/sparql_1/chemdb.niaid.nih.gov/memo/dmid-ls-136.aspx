

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-136.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RzmC5dlcal9b02zNL8+OwKhwMzjHSadwGy51NXzG1pJ9G41YCjwPbt4mb/r0b8lKxL2XQzoat4Q3kOfgeNGwDLKXdCXGFK8W+iW4JuFOKSJymp2y8okitPGYSOQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DBEA7E4B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-136-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59929   DMID-LS-136; PUBMED-DMID-1/15/2007 </p>

    <p class="memofmt1-2">          Non-compact nucleocapsid protein multimers in influenza-virus-infected cells</p>

    <p>          Prokudina, EN, Semenova, NP, Chumakov, VM, and Grigorieva, TA</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17216139&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17216139&amp;dopt=abstract</a> </p><br />

    <p>2.     59930   DMID-LS-136; PUBMED-DMID-1/15/2007 </p>

    <p class="memofmt1-2">          Avian influenza: A review</p>

    <p>          Thomas, JK and Noppenberger, J</p>

    <p>          Am J Health Syst Pharm <b>2007</b>.  64(2): 149-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17215466&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17215466&amp;dopt=abstract</a> </p><br />

    <p>3.     59931   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Azapteridine Inhibitors of Hepatitis C Virus Rna-Dependent Rna Polymerase</p>

    <p>          Middleton, T. <i> et al.</i></p>

    <p>          Letters in Drug Design &amp; Discovery <b>2007</b>.  4(1): 1-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242910300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242910300001</a> </p><br />

    <p>4.     59932   DMID-LS-136; PUBMED-DMID-1/15/2007 </p>

    <p class="memofmt1-2">          In Vitro Study of the Effects of Precore and Lamivudine Resistant Mutations on HBV Replication</p>

    <p>          Heipertz, RA Jr, Miller, TG, Kelley, CM, Delaney, WE 4th, Locarnini, SA, and Isom, HC</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17215289&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17215289&amp;dopt=abstract</a> </p><br />

    <p>5.     59933   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          A Structure-Based 3d-Qsar(Comsia) Study on a Series of Aryl Diketoacids (Adk) as Inhibitors of Hcv Rna-Dependent Rna Polymerase</p>

    <p>          Kim, J., Han, J., and Chong, Y.</p>

    <p>          Bulletin of the Korean Chemical Society <b>2006</b>.  27(11): 1919-1922</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242822500044">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242822500044</a> </p><br />

    <p>6.     59934   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Synthesis and Biological Activity of 5 &#39;,9-Anhydro-3-Purine-Isonucleosides as Potential Anti-Hepatitis C Virus Agents</p>

    <p>          Chun, B. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2007</b>.  26(1): 83-97</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242711400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242711400008</a> </p><br />
    <br clear="all">

    <p>7.     59935   DMID-LS-136; EMBASE-DMID-1/15/2007 </p>

    <p class="memofmt1-2">          Enantioselective synthesis and antiviral activity of purine and pyrimidine cyclopentenyl C-nucleosides</p>

    <p>          Rao, Jagadeeshwar R, Schinazi, Raymond F, and Chu, Chung K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(2): 839-846</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M6935M-1/2/009d1ee3cfa74deb7a221b2b197f0a33">http://www.sciencedirect.com/science/article/B6TF8-4M6935M-1/2/009d1ee3cfa74deb7a221b2b197f0a33</a> </p><br />

    <p>8.     59936   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Insights to Substrate Binding and Processing by West Nile Virus Ns3 Protease Through Combined Modeling, Protease Mutagenesis, and Kinetic Studies</p>

    <p>          Chappell, K. <i>et al.</i></p>

    <p>          Journal of Biological Chemistry <b>2006</b>.  281(50): 38448-38458</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242709500038">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242709500038</a> </p><br />

    <p>9.     59937   DMID-LS-136; EMBASE-DMID-1/15/2007 </p>

    <p class="memofmt1-2">          Factors that affect in vitro measurement of the susceptibility of herpes simplex virus to nucleoside analogues</p>

    <p>          Weinberg, Adriana, Leary, Jeffry J, Sarisky, Robert T, and Levin, Myron J</p>

    <p>          Journal of Clinical Virology <b>2007</b>.  38(2): 139-145</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJV-4MJS0CT-1/2/508c3b26289425fcc8efc68a252f0199">http://www.sciencedirect.com/science/article/B6VJV-4MJS0CT-1/2/508c3b26289425fcc8efc68a252f0199</a> </p><br />

    <p>10.   59938   DMID-LS-136; PUBMED-DMID-1/15/2007</p>

    <p class="memofmt1-2">          N-Long-chain Monoacylated Derivatives of 2,6-Diaminopyridine with Antiviral Activity</p>

    <p>          Mibu, N, Yokomizo, K, Kashige, N, Miake, F, Miyata, T, Uyeda, M, and Sumoto, K</p>

    <p>          Chem Pharm Bull (Tokyo) <b>2007</b>.  55(1): 111-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202712&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17202712&amp;dopt=abstract</a> </p><br />

    <p>11.   59939   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Synergy of Small Molecular Inhibitors of Hepatitis C Virus Replication Directed at Different Viral Targets</p>

    <p>          Wyles, D., Kaihara, K., and Schooley, R.</p>

    <p>          Antiviral Therapy <b>2006</b>.  11(5): S4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239984700022</a> </p><br />

    <p>12.   59940   DMID-LS-136; PUBMED-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Development and Validation of a High-Throughput Screen for Inhibitors of SARS CoV and Its Application in Screening of a 100,000-Compound Library</p>

    <p>          Severson, WE, Shindo, N, Sosa, M, Fletcher, T 3rd, White, EL, Ananthan, S, and Jonsson, CB</p>

    <p>          J Biomol Screen <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17200104&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17200104&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   59941   DMID-LS-136; PUBMED-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Emerging infectious diseases that threaten the blood supply</p>

    <p>          Alter, HJ, Stramer, SL, and Dodd, RY</p>

    <p>          Semin Hematol <b>2007</b>.  44(1): 32-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17198845&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17198845&amp;dopt=abstract</a> </p><br />

    <p>14.   59942   DMID-LS-136; EMBASE-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Synthesis and evaluation of 4-O -alkylated 2-deoxy-2,3-didehydro-N-acetylneuraminic acid derivatives as inhibitors of human parainfluenza virus type-3 sialidase activity</p>

    <p>          Tindal, David J, Dyason, Jeffrey C, Thomson, Robin J, Suzuki, Takashi, Ueyama, Hiroo, Kuwahara, Yohta, Maki, Naoyoshi, Suzuki, Yasuo, and Itzstein, Mark von</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MS9RC7-D/2/0d615842e89614dc9c3f8595333a9e9b">http://www.sciencedirect.com/science/article/B6TF9-4MS9RC7-D/2/0d615842e89614dc9c3f8595333a9e9b</a> </p><br />

    <p>15.   59943   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Definitions of Resistance to Analogue Antiviral Drugs in the Treatment of Chronic Hepatitis B</p>

    <p>          Zoulim, F.</p>

    <p>          Gastroenterologie Clinique Et Biologique <b>2006</b>.  30(10): S9-S11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601200002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242601200002</a> </p><br />

    <p>16.   59944   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          The &quot;Return&quot; of Hepatitis B</p>

    <p>          Krastev, Z.</p>

    <p>          World Journal of Gastroenterology <b>2006</b>.  12(44): 7081-7086</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242527600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242527600003</a> </p><br />

    <p>17.   59945   DMID-LS-136; PUBMED-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Main structural and stereochemical aspects of the antiherpetic activity of nonahydroxyterphenoyl-containing C-glycosidic ellagitannins</p>

    <p>          Quideau, S, Varadinova, T, Karagiozova, D, Jourdes, M, Pardon, P, Baudry, C, Genova, P, Diakov, T, and Petrova, R</p>

    <p>          Chem Biodivers  <b>2004</b>.  1(2): 247-58</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191843&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17191843&amp;dopt=abstract</a> </p><br />

    <p>18.   59946   DMID-LS-136; EMBASE-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of (Z)-5-arylidenerhodanines</p>

    <p>          Sortino, Maximiliano, Delgado, Paula, Juarez, Sabina, Quiroga, Jairo, Abonia, Rodrigo, Insuasty, Braulio, Nogueras, Manuel, Rodero, Laura, Garibotto, Francisco M, Enriz, Ricardo D, and Zacchino, Susana A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(1): 484-494</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4M4CMXN-2/2/9650dd97985e04d3a51664b14b81c9a3">http://www.sciencedirect.com/science/article/B6TF8-4M4CMXN-2/2/9650dd97985e04d3a51664b14b81c9a3</a> </p><br />
    <br clear="all">

    <p>19.   59947   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Quantitative Structure-Property Relationship for Predicting the Activity of Inhibitor of Influenza Virus Neuraminidase</p>

    <p>          Tsygankova, I. and Zhenodarova, S.</p>

    <p>          Russian Journal of General Chemistry <b>2006</b>.  76(10): 1618-1624</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242841600020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242841600020</a> </p><br />

    <p>20.   59948   DMID-LS-136; PUBMED-DMID-1/15/2007</p>

    <p class="memofmt1-2">          WHO Rapid Advice Guidelines for pharmacological management of sporadic human infection with avian influenza A (H5N1) virus</p>

    <p>          Schunemann, HJ, Hill, SR, Kakad, M, Bellamy, R, Uyeki, TM, Hayden, FG, Yazdanpanah, Y, Beigel, J, Chotpitayasunondh, T, Del, Mar C, Farrar, J, Tran, TH, Ozbay, B, Sugaya, N, Fukuda, K, Shindo, N, Stockman, L, Vist, GE, Croisier, A, Nagjdaliyev, A, Roth, C, Thomson, G, Zucker, H, and Oxman, AD</p>

    <p>          Lancet Infect Dis <b>2007</b>.  7(1): 21-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17182341&amp;dopt=abstract</a> </p><br />

    <p>21.   59949   DMID-LS-136; WOS-DMID-1/15/2007</p>

    <p class="memofmt1-2">          Influenza a Virus and the Cell Nucleus</p>

    <p>          Amorim, M. and Digard, P.</p>

    <p>          Vaccine <b>2006</b>.  24(44-46): 6651-6655</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242444000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242444000022</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
