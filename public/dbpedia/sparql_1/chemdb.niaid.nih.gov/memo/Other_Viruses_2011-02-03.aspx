

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-02-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AMZIwNN2hAXWajXG5pkeGVAwk+vYRBT3EiSL6oGdCZdW3w1DYuCKORi3lAjWyf/gcXM0+vWeNp5sVVF9TTeltyCxyla2YhkYi/RFxkw1+2PSYLingkfkPlqetJc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1199D2B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">January 21 - February 3, 2011</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21259187">Stage of Action of Naturally Occurring Andrographolides and Their Semisynthetic Analogues against Herpes Simplex Virus Type 1 in Vitro.</a> Aromdee C, Suebsasana S, Ekalaksananan T, Pientong C, Thongchai S. Planta Medica. 2011 Jan 21. <b>[Epub ahead of print]</b>. PMID[21259187].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0121-020311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21277330">Oxymatrine Inhibits Hepatitis B Infection with an Advantage of Overcoming Drug-Resistance.</a> Wang YP, Zhao W, Xue R, Zhou ZX, Liu F, Han YX, Ren G, Peng ZG, Cen S, Chen HS, Li YH, Jiang JD. Antiviral Research. 2011 Jan 27. <b>[Epub ahead of print]</b>. PMID[21277330].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0121-020311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21281131">Efficient In Silico Assay of Inhibitors of Hepatitis C Virus RNA-Dependent RNA Polymerase by Structure-Based Virtual Screening and In Vitro Evaluation.</a> Lin YT, Huang KJ, Tseng CK, Chen KJ, Wang HM, Lee JC. Assay and Drug Development Technologies. 2011 Jan 31. <b>[Epub ahead of print]</b>. PMID[21281131].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0121-020311.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21247464">In-vitro antiviral activity of Solanum nigrum against Hepatitis C Virus.</a> Javed T, Ashfaq UA, Riaz S, ehman S, Riazuddin S. Virology Journal. 2011 Jan 19;8(1):26. PMID[21247464].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0121-020311.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285649000011">Biological Activities of Peganum Harmala Leaves. African Journal of Biotechnology.</a> Hayet, E., M. Maha, M. Mata, Z. Mighri, G. Laurent, and A. Mahjoub. 2010. 9(48): p. 8199-8205; ISI[000285649000011].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0121-020310.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285504500007">Chemoselective Staudinger Strategy in the Practical, Fit for Purpose, Gram-Scale Synthesis of an Hcv Rna Polymerase Inhibitor.</a> Campeau, L.C. and P.D. O&#39;Shea. Synlett, 2011(1): p. 57-60. ISI[000285504500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0121-020310.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285554300039">Indoleamine 2,3-Dioxygenase Mediates the Antiviral Effect of Gamma Interferon against Hepatitis B Virus in Human Hepatocyte-Derived Cells.</a> Mao, R.C., J.M. Zhang, D. Jiang, D.W. Cai, J.M. Levy, A. Cuconati, T.M. Block, J.T. Guo, and H.T. Guo. Journal of Virology, 2011. 85(2): p. 1048-1057; ISI[000285554300039].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0121-020310.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285544400011">Quinolones as HCV NS5B Polymerase Inhibitors.</a> Kumar, D.V., R. Rai, K.A. Brameld, J.R. Somoza, R. Rajagopalan, J.W. Janc, Y.M. Xia, T.L. Ton, M.B. Shaghafi, H.Y. Hu, I. Lehoux, N. To, W.B. Young, and M.J. Green. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(1): p. 82-87; ISI[000285544400011].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0121-020310.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
