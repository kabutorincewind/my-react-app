

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-09-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="GGGw5d1ti+fTNgGNw9SQQozM4NwALPwAokip+KXZvkZXBchNqLj8aoFa4sOh0mGvgOh72yzn3zza1oYOIsf5SrQ6rKz1dFVdhTSS6CP9z0VOZNKLgHSs9ZYWFrc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C49D7A6B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: September 11 - September 24, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26306007">Design of HIV-1 Protease Inhibitors with Amino-bis-tetrahydrofuran Derivatives as P2-ligands to Enhance Backbone-binding Interactions: Synthesis, Biological Evaluation, and Protein-ligand X-ray Studies.</a> Ghosh, A.K., C.D. Martyr, H.L. Osswald, V.R. Sheri, L.A. Kassekert, S. Chen, J. Agniswamy, Y.F. Wang, H. Hayashi, M. Aoki, I.T. Weber, and H. Mitsuya. Journal of  Medicinal Chemistry, 2015. 58(17): p. 6994-7006. PMID[26306007].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26274532">Development of Water-soluble Polyanionic Carbosilane Dendrimers as Novel and Highly Potent Topical anti-HIV-2 Microbicides.</a> Briz, V., D. Sepulveda-Crespo, A.R. Diniz, P. Borrego, B. Rodes, F.J. de la Mata, R. Gomez, N. Taveira, and M.A. Munoz-Fernandez. Nanoscale, 2015. 7(35): p. 14669-14683. PMID[26274532].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26276432">Discovery of Potent HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors from Arylthioacetanilide Structural Motif.</a> Li, W., X. Li, E. De Clercq, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2015. 102: p. 167-179. PMID[26276432].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26276435">Pyrimidine Sulfonylacetanilides with Improved Potency against Key Mutant Viruses of HIV-1 by Specific Targeting of a Highly Conserved Residue.</a> Wan, Z.Y., J. Yao, T.Q. Mao, X.L. Wang, H.F. Wang, W.X. Chen, H. Yin, F.E. Chen, E. De Clercq, D. Daelemans, and C. Pannecouque. European Journal of Medicinal Chemistry, 2015. 102: p. 215-222. PMID[26276435].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26301736">Structure-based Design of a Small Molecule CD4-Antagonist with Broad Spectrum anti-HIV-1 Activity.</a> Curreli, F., Y.D. Kwon, H. Zhang, D. Scacalossi, D.S. Belov, A.A. Tikhonov, I.A. Andreev, A. Altieri, A.V. Kurkin, P.D. Kwong, and A.K. Debnath. Journal of Medicinal Chemistry, 2015. 58(17): p. 6909-6927. PMID[26301736].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26289550">Total Chemical Synthesis of the Site-selective Azide-labeled [I66a]HIV-1 Protease.</a> Qi, Y.K., H.N. Chang, K.M. Pan, C.L. Tian, and J.S. Zheng. Chemical Communications, 2015. 51(78): p. 14632-14635. PMID[26289550].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26402662">G-quadruplex Forming Oligonucleotides as anti-HIV Agents.</a> Musumeci, D., C. Riccardi, and D. Montesarchio. Molecules, 2015. 20(9): p. 17511-17532. PMID[26402662].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0911-092415.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000360221900006">An Artificial Peptide-based HIV-1 Fusion Inhibitor Containing M-T Hook Structure Exhibiting Improved Antiviral Potency and Drug Resistance Profile.</a> Zhu, X.J., F. Yu, K.L. Liu, L. Lu, and S.B. Jiang. Future Virology, 2015. 10(8): p. 961-969. ISI[000360221900006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000360179400003">Engineering Soya Bean Seeds as a Scalable Platform to Produce Cyanovirin-N, a non-ARV Microbicide against HIV.</a> O&#39;Keefe, B.R., A.M. Murad, G.R. Vianna, K. Ramessar, C.J. Saucedo, J. Wilson, K.W. Buckheit, N.B. da Cunha, A.C.G. Araujo, C.C. Lacorte, L. Madeira, J.B. McMahon, and E.L. Rech. Plant Biotechnology Journal, 2015. 13(7): p. 884-892. ISI[000360179400003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000360334000001">In Vitro Antiretroviral Activity and In Vivo Toxicity of the Potential Topical Microbicide Copper phthalocyanine Sulfate.</a> Styczynski, A.R., K.N. Anwar, H. Sultana, A. Ghanem, N. Lurain, A.S. Chua, M. Ghassemi, and R.M. Novak. Virology Journal, 2015. 12(132): 10pp. ISI[000360334000001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0911-092415.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000360508300007">Anti-HIV Activity of New Pyrazolobenzothiazine 5,5-dioxide-based Acetohydrazides.</a> Khalid, Z., S. Aslam, M. Ahmad, M.A. Munawar, C. Montero, M. Detorio, M. Parvez, and R.F. Schinazi. Medicinal Chemistry Research, 2015. 24(10): p. 3671-3680. ISI[000360508300007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0911-092415.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
