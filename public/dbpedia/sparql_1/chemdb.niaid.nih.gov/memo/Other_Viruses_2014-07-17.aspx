

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-07-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9hLa8Xn4YeHpUOCi7nG3j1anRsGo+oPVAE2NDs4oH0Wso6iEUCuaX5n3rCzMijAyz6UD1/iZHQrDHV+TC/uBADNFBPrAN6oqz6nOzGDOWtOMMyr0oZjr8jXazBw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C088857E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 4 - July 17, 2014</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24999657">Suppression of La Antigen Exerts Potential Antiviral Effects Against Hepatitis A Virus</a>. Jiang, X., T. Kanda, S. Wu, S. Nakamoto, K. Saito, H. Shirasawa, T. Kiyohara, K. Ishii, T. Wakita, H. Okamoto, and O. Yokosuka. PLoS One, 2014. 9(7): p. e101993. PMID[24999657].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24789779">A Small Molecule Inhibits Virion Attachment to Heparan Sulfate- or Sialic acid-containing Glycans</a>. Colpitts, C.C. and L.M. Schang. Journal of Virology, 2014. 88(14): p. 7806-7817. PMID[24789779].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24856063">Aryl-substituted Aminobenzimidazoles Targeting the Hepatitis C Virus Internal Ribosome Entry Site</a>. Ding, K., A. Wang, M.A. Boerneke, S.M. Dibrov, and T. Hermann. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 3113-3117. PMID[24856063].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24907145">Preparation and Biological Evaluation of 1&#39;-Cyano-2&#39;-C-methyl pyrimidine Nucleosides as HCV NS5B Polymerase Inhibitors</a>. Mish, M.R., A. Cho, T. Kirschberg, J. Xu, C. Sebastian Zonte, M. Fenaux, Y. Park, D. Babusis, J.Y. Feng, A.S. Ray, and C.U. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(14): p. 3092-3095. PMID[24907145].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24928988">Ficolin-2 Inhibits Hepatitis C Virus Infection, Whereas Apolipoprotein E3 Mediates Viral Immune Escape</a>. Zhao, Y., Y. Ren, X. Zhang, P. Zhao, W. Tao, J. Zhong, Q. Li, and X.L. Zhang. The Journal of Immunology, 2014. 193(2): p. 783-796. PMID[24928988].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24989498">Inhibition of Cytomegalovirus Infection and Photothermolysis of Infected Cells Using Bioconjugated Gold Nanoparticles</a>. DeRussy, B.M., M.A. Aylward, Z. Fan, P.C. Ray, and R. Tandon. Scientific Reports, 2014. 4(5550): p.7. PMID[24989498].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p>

    <h2>Herpes Simplex Virus I</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24906510">Design, Synthesis, Antiviral and Cytostatic Activity of omega-(1H-1,2,3-Triazol-1-yl)(polyhydroxy)alkylphosphonates as Acyclic Nucleotide Analogues</a>. Glowacka, I.E., J. Balzarini, G. Andrei, R. Snoeck, D. Schols, and D.G. Piotrowska. Bioorganic &amp; Medicinal Chemistry, 2014. 22(14): p. 3629-3641. PMID[24906510].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0704-071714.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337729200004">Hepatitis C Virus Serine Protease: Synthesis of Radioactive and Stable Isotope-labeled Potent Inhibitors</a>. Latli, B., M. Hrapchak, V. Gorys, M. Llinas-Brunet, S.S. Campbell, J.H. Song, and C.H. Senanayake. Journal of Labeled Compounds &amp; Radiopharmaceuticals, 2014. 57(5): p. 350-357. ISI[000337729200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0704-071714.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337557900033">Identification of Peptides That Bind Hepatitis C Virus Envelope Protein E2 and Inhibit Viral Cellular Entry from a Phage-display Peptide Library</a>. Lu, X., M. Yao, J.M. Zhang, J. Yang, Y.F. Lei, X.J. Huang, Z.S. Jia, L. Ma, H.Y. Lan, Z.K. Xu, and W. Yin. International Journal of Molecular Medicine, 2014. 33(5): p. 1312-1318. ISI[000337557900033].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0704-071714.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000337497200015">Discovery and Synthesis of C-Nucleosides as Potential New anti-HCV Agents</a>. Draffan, A.G., B. Frey, B. Pool, C. Gannon, E.M. Tyndall, M. Lilly, P. Francom, R. Hufton, R. Halim, S. Jahangiri, S. Bond, V.T.T. Nguyen, T.P. Jeynes, V. Wirth, A. Luttick, D. Tilmanis, J.D. Thomas, M. Pryor, K. Porter, C.J. Morton, B. Lin, J.M. Duan, G. Kukolj, B. Simoneau, G. McKercher, L. Lagace, M. Amad, R.C. Bethell, and S.P. Tucker. ACS Medicinal Chemistry Letters, 2014. 5(6): p. 679-684. ISI[000337497200015].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0704-071714.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
