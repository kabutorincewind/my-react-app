

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="c+k2wm/btYfEPZ+IPKBRaSH7OFECvNHATK+Y9HfPImH1MAWpR8N5HhIzpWfpl+yeXiUnGyQZUm6ZKiI8volSU+A8y2OZLF1zWked+TqgTO0OcrnSFGjZctVBxaw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AC2F7979" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: August, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406049400010">Anti-HIV Screening and Molecular Docking Studies of Benzothiazolyl thioureas.</a> Rafique, H., A. Saeed, F. Perveen, M.N. Zafar, M. Sharif, and N.A. Masoudi. Latin American Journal of Pharmacy, 2017. 36(5): p. 918-923. ISI[000406049400010].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406602000027">Anti-HIV-1 and Cytotoxicity of a New Dimeric Thiazepine Alkaloid Isolated from Ixora undulata Roxb. Leaves.</a> Mohammed, M. and K. Mohamed. Medicinal Chemistry Research, 2017. 26(9): p. 2119-2126. ISI[000406602000027].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28705263">Antiviral Activity of Dual-acting Hydrocarbon-stapled Peptides against HIV-1 Predominantly Circulating in China.</a> Wang, Y., F. Curreli, W.S. Xu, Z.P. Li, D.S. Kong, L. Ren, K.X. Hong, S.B. Jiang, Y.M. Shao, A.K. Debnath, and L.Y. Ma. Biomedical and Environmental Sciences, 2017. 30(6): p. 398-406. PMID[28705263].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28628334">Chiral Indolylarylsulfone Non-Nucleoside Reverse Transcriptase Inhibitors as New Potent and Broad Spectrum anti-HIV-1 Agents.</a> Famiglini, V., G. La Regina, A. Coluccia, D. Masci, A. Brancale, R. Badia, E. Riveira-Munoz, J.A. Este, E. Crespan, A. Brambilla, G. Maga, M. Catalano, C. Limatola, F.R. Formica, R. Cirilli, E. Novellino, and R. Silvestri. Journal of Medicinal Chemistry, 2017. 60(15): p. 6528-6547. PMID[28628334].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28785069">Combinations of Isoform-targeted Histone deacetylase Inhibitors and Bryostatin Analogues Display Remarkable Potency to Activate Latent HIV without Global T-cell Activation.</a> Albert, B.J., A. Niu, R. Ramani, G.R. Marshall, P.A. Wender, R.M. Williams, L. Ratner, A.B. Barnes, and G.B. Kyei. Scientific Reports, 2017. 7(7456): 12pp. PMID[28785069]. PMCID[PMC5547048].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28592534">Comprehensive Cross-clade Characterization of Antibody-mediated Recognition, Complement-mediated Lysis, and Cell-mediated Cytotoxicity of HIV-1 Envelope-specific Antibodies toward Eradication of the HIV-1 Reservoir.</a> Mujib, S., J. Liu, A. Rahman, J.A. Schwartz, P. Bonner, F.Y. Yue, and M.A. Ostrowski. Journal of Virology, 2017. 91(16): e00634-00617. PMID[28592534]. PMCID[PMC5533900].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28767697">Cyclophilin A Potentiates TRIM5alpha Inhibition of HIV-1 Nuclear Import without Promoting TRIM5alpha Binding to the Viral Capsid.</a> Burse, M., J. Shi, and C. Aiken. Plos One, 2017. 12(8): e0182298. PMID[28767697]. PMCID[PMC5540582].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405467600023">Cytotoxic, anti-HIV-1 and Anti-inflammatory Activities of Lanostanes from Fruits of Garcinia speciosa.</a> Pailee, P., T. Kruahong, S. Hongthong, C. Kuhakarn, T. Jaipetch, M. Pohmakotr, S. Jariyawat, K. Suksen, R. Akkarawongsapat, J. Limthongkul, A. Panthong, P. Kongsaeree, S. Prabpai, P. Tuchinda, and V. Reutrakul. Phytochemistry Letters, 2017. 20: p. 111-118. ISI[000405467600023].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28494151">Design of HIV Coreceptor Derived Peptides that Inhibit Viral Entry at Submicromolar Concentrations.</a> Bobyk, K.D., S.R. Mandadapu, K. Lohith, C. Guzzo, A. Bhargava, P. Lusso, and C.A. Bewley. Molecular Pharmaceutics, 2017. 14(8): p. 2681-2689. PMID[28494151].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406145000009">Design of New Uracil Derivatives Possessing Inhibitory Activity with Respect to Reverse Transcriptase of HIV-1 Mutant K103N/Y181C.</a> Pechinskii, S.V., A.G. Kuregyan, A.A. Ozerov, and M.S. Novikov. Pharmaceutical Chemistry Journal, 2016. 49(10): p. 683-686. ISI[000406145000009].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000407802100013">Design, Synthesis and Antiviral Evaluation of Novel Heteroarylcarbothioamide Derivatives as Dual Inhibitors of HIV-1 Reverse Transcriptase-associated RNase H and RDDP Functions.</a> Corona, A., V. Onnis, A. Deplano, G. Bianco, M. Demurtas, S. Distinto, Y.C. Cheng, S. Alcaro, F. Esposito, and E. Tramontano. Pathogens and Disease, 2017. 75(6). ISI[000407802100013].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28659246">Discovery of Novel DAPY-IAS Hybrid Derivatives as Potential HIV-1 Inhibitors Using Molecular Hybridization Based on Crystallographic Overlays.</a> Huang, B., X. Wang, X. Liu, Z. Chen, W. Li, S. Sun, H. Liu, D. Daelemans, E. De Clercq, C. Pannecouque, P. Zhan, and X. Liu. Bioorganic &amp; Medicinal Chemistry, 2017. 25(16): p. 4397-4406. PMID[28659246].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28659478">Enfuvirtide (T20)-based Lipopeptide Is a Potent HIV-1 Cell Fusion Inhibitor: Implications for Viral Entry and Inhibition.</a> Ding, X., X. Zhang, H. Chong, Y. Zhu, H. Wei, X. Wu, J. He, X. Wang, and Y. He. Journal of Virology, 2017. 91(18): e00831-00817. PMID[28659478]. PMCID[PMC5571253].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406892500012">Glycosylated Flavonoids from Psidium guineense as Major Inhibitors of HIV-1 Replication in Vitro.</a> Ortega, J.T., O. Estrada, M.I. Serrano, W. Contreras, G. Orsini, F.H. Pujol, and H.R. Rangel. Natural Product Communications, 2017. 12(7): p. 1049-1052. ISI[000406892500012].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28533248">Impact of HIV-1 Integrase L74F and V75I Mutations in a Clinical Isolate on Resistance to Second-generation Integrase Strand Transfer Inhibitors.</a> Hachiya, A., K.A. Kirby, Y. Ido, U. Shigemi, M. Matsuda, R. Okazaki, J. Imamura, S.G. Sarafianos, Y. Yokomaku, and Y. Iwatani. Antimicrobial Agents and Chemotherapy, 2017. 61(8): e00315-00317. PMID[28533248]. PMCID[PMC5527620].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28648081">In Silico and in Vitro Screening for P-Glycoprotein Interaction with Tenofovir, Darunavir, and Dapivirine: An Antiretroviral Drug Combination for Topical Prevention of Colorectal HIV Transmission.</a> Swedrowska, M., S. Jamshidi, A. Kumar, C. Kelly, K.M. Rahman, and B. Forbes. Molecular Pharmaceutics, 2017. 14(8): p. 2660-2669. PMID[28648081].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28805676">Inhibitors of Deubiquitinating Enzymes Block HIV-1 Replication and Augment the Presentation of Gag-derived MHC-I Epitopes.</a> Setz, C., M. Friedrich, P. Rauch, K. Fraedrich, A. Matthaei, M. Traxdorf, and U. Schubert. Viruses, 2017. 9(8): E222. PMID[28805676].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000406429800096">Membrane-associated Ring-CH (MARCH) 1 and 2 Are Other Members of MARCH Proteins that Inhibit HIV-1 Infection.</a> Yao, W., T. Tada, Y. Zhang, H. Fujita, S. Yamaoka, and K. Tokunaga. Journal of the International AIDS Society, 2017. 20: p. 49-49. ISI[000406429800096].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000407802100004">Multi-target Activity of Hemidesmus indicus Decoction against Innovative HIV-1 Drug Targets and Characterization of Lupeol Mode of Action.</a>Esposito, F., M. Mandrone, C. del Vecchio, I. Carli, S. Distinto, A. Corona, M. Lianza, D. Piano, M. Tacchini, E. Maccioni, F. Cottiglia, E. Saccon, F. Poli, C. Parolin, and E. Tramontano. Pathogens and Disease, 2017. 75(6). ISI[000407802100004].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28652233">Potent Inhibition of HIV-1 Replication in Resting CD4 T Cells by Resveratrol and Pterostilbene.</a> Chan, C.N., B. Trinite, and D.N. Levy. Antimicrobial Agents and Chemotherapy, 2017. 61(9): e00408-00417. PMID[28652233].PMCID[PMC5571302].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405461402192">Quantification of the in Vitro Inhibitory Effects of the Arachis hypogaea Lectin on HIV-1 Reverse Transcriptase.</a> Garza, C.A. and R. Ynalvez. FASEB Journal, 2017. 31. ISI[000405461402192].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28790381">Selective Cytotoxicity of a Novel Immunotoxin Based on Pulchellin a Chain for Cells Expressing HIV Envelope.</a>Sadraeian, M., F.E.G. Guimaraes, A.P.U. Araujo, D.K. Worthylake, L. LeCour, and S.H. Pincus. Scientific Reports, 2017. 7(7579): 12pp. PMID[28790381]. PMCID[PMC5548917].
    <br />
    <b>[</b><b>PubMed</b><b>]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28553929">Silencing of HIV-1 by AgoshRNA Molecules.</a>Herrera-Carrillo, E., A. Harwig, and B. Berkhout. Gene Therapy, 2017. 24(8): p. 453-461. PMID[28553929].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405467600057">Stachybotrysams A-E, Prenylated Isoindolinone Derivatives with anti-HIV Activity from the Fungus Stachybotrys chartarum.</a> Zhao, J.L., J.M. Liu, Y. Shen, Z. Tan, M. Zhang, R.D. Chen, J.Y. Zhao, D.W. Zhang, L.Y. Yu, and J.G. Dai. Phytochemistry Letters, 2017. 20: p. 289-294. ISI[000405467600057].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28582696">Structural Basis for the Potent Inhibition of the HIV Integrase-LEDGF/p75 Protein-protein Interaction.</a> Ribone, S.R. and M.A. Quevedo. Journal of Molecular Graphics &amp; Modelling, 2017. 75: p. 189-198. PMID[28582696].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28677862">Structural Study of a New HIV-1 Entry Inhibitor and Interaction with the HIV-1 Fusion Peptide in Dodecylphosphocholine Micelles.</a> Perez, Y., M.J. Gomara, E. Yuste, P. Gomez-Gutierrez, J.J. Perez, and I. Haro. Chemistry, 2017. 23(48): p. 11703-11713. PMID[28677862].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28077629">SUN2 Silencing Impairs CD4 T Cell Proliferation and Alters Sensitivity to HIV-1 Infection Independently of Cyclophilin A.</a> Donahue, D.A., F. Porrot, N. Couespel, and O. Schwartz. Journal of Virology, 2017. 91(6): e02303-16. PMID[28077629]. PMCID[PMC5331816].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28739156">Synthesis and Biological Evaluation in Vitro and in Mammalian Cells of New Heteroaryl carboxyamides as HIV-protease Inhibitors.</a>Funicello, M., L. Chiummiento, F. Tramutola, M.F. Armentano, F. Bisaccia, R. Miglionico, L. Milella, F. Benedetti, F. Berti, and P. Lupattelli. Bioorganic &amp; Medicinal Chemistry, 2017. 25(17): p. 4715-4722. PMID[28739156].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28817720">Targeting of CDK9 with Indirubin 3&#39;-monoxime Safely and Durably Reduces HIV Viremia in Chronically Infected Humanized Mice.</a>Medina-Moreno, S., T.C. Dowling, J.C. Zapata, N.M. Le, E. Sausville, J. Bryant, R.R. Redfield, and A. Heredia. Plos One, 2017. 12(8): e0183425. PMID[28817720]. PMCID[PMC5560554]
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28533249">The Myxobacterial Metabolite Soraphen A Inhibits HIV-1 by Reducing Virus Production and Altering Virion Composition.</a>Fleta-Soriano, E., K. Smutna, J.P. Martinez, C. Lorca Oro, S.K. Sadiq, G. Mirambeau, C. Lopez-Iglesias, M. Bosch, A. Pol, M. Bronstrup, J. Diez, and A. Meyerhans. Antimicrobial Agents and Chemotherapy, 2017. 61(8): e00739-00717. PMID[28533249]. PMCID[PMC5527598].
    <br />
    <b>[PubMed]</b>. HIV_08_2017.</p><br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405528000001">Two Flavonoids First Isolated from the Seed of Syzygium nervosum and Preliminary Study of their Anticancer and Anti-HIV-1 Reverse Transcriptase Activities.</a>Chailungka, A., T. Junpirom, W. Pompimon, N. Nuntasaen, and P. Meepowpan. Maejo International Journal of Science and Technology, 2017. 11(1): p. 58-67. ISI[000405528000001].
    <br />
    <b>[WOS]</b>. HIV_08_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">32. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170810&amp;CC=WO&amp;NR=2017134596A1&amp;KC=A1">Preparation of C-3 and C-17 Modified Triterpenoids as HIV-1 Inhibitors.</a> Chen, J., Y. Chen, I.B. Dicker, R.A. Hartz, N.A. Meanwell, A. Regueiro-Ren, S.-Y. Sit, N. Sin, J. Swidorski, B.L. Venables, and B. Nowicka-Sans. Patent. 2017. 2017-IB50568 2017134596: 335pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">33. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170817&amp;CC=WO&amp;NR=2017139519A1&amp;KC=A1">Methods for Treatment and Prophylaxis of HIV and AIDS.</a> Hazuda, D., M.D. Miller, J.A. Grobler, and D.A. Nicoll-Griffith. Patent. 2017. 2017-US17283 2017139519: 34pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170727&amp;CC=WO&amp;NR=2017125870A1&amp;KC=A1">Preparation of Amine Derivatives of Lupanes with HIV Maturation Inhibitory Activity.</a> Johns, B.A. Patent. 2017. 2017-IB50279 2017125870: 60pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170802&amp;CC=EP&amp;NR=3199534A1&amp;KC=A1">Preparation of Benzenesulfonamides and Use Thereof in Medicine and Cosmetics.</a> Ouvry, G., Y. Bhurruth-Alcor, C. Harris, B. Deprez, and M. Bourotte. Patent. 2017. 2016-153702 3199534: 64pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170810&amp;CC=WO&amp;NR=2017134262A1&amp;KC=A1">Use of Antiapoptosis Clone 11 Protein (AAC-11) Inhibitors for the Treatment of Viral Infection.</a> Poyet, J.-L., A. Saez-Cirion, L. Jagot-Lacoussiere, H. Bruzzoni-Giovanelli, A. David, and A. Mikhailova. Patent. 2017. 2017-EP52448 2017134262: 39pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">37. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170803&amp;CC=WO&amp;NR=2017130156A1&amp;KC=A1">Methods of Preparing Ingenol Analogs, Pharmaceutical Compositions and Use in Treating HIV.</a> Tai, V.W.-F. and J. Tang. Patent. 2017. 2017-IB50452 2017130156: 98pp.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>

    <br />

    <p class="plaintext">38. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170810&amp;CC=US&amp;NR=2017226095A1&amp;KC=A1">Preparation of Quinoline and Quinoxaline Compounds for Preventing, Inhibiting, or Treating Cancer, AIDS and/or Premature Aging.</a> Tazi, J., F. Mahuteau, P. Roux, R. Najman, D. Scherrer, C. Brock, N. Cahuzac, G. Gadea, N. Campos, A. Garcel, and J. Santo. Patent. 2017. 2017-15486836 20170226095: 84pp , Cont -in-part of U S Ser No 789,250.
    <br />
    <b>[Patent]</b>. HIV_08_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
