

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-296.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7+xkKLFicQkyvVkMLW5jfQIbDLJgYKPyz3owJlvWFNNCbqCy0qF5L2d97gb79/xjSDwtDtx0qzeqpgvXwpdjH1eE2BuxsHYZkx3nOVlz297rvcnZ4GS+db2NkFU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FF1D01E6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-296-MEMO</b> </p>

<p>    1.    43381   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           HIV protease inhibitor nelfinavir inhibits replication of SARS-associated coronavirus</p>

<p>           Yamamoto, N, Yang, R, Yoshinaka, Y, Amari, S, Nakano, T, Cinatl, J, Rabenau, H, Doerr, HW, Hunsmann, G, Otaka, A, Tamamura, H, Fujii, N, and Yamamoto, N</p>

<p>           Biochem Biophys Res Commun 2004. 318(3):  719-25</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15144898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15144898&amp;dopt=abstract</a> </p><br />

<p>    2.    43382   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Human Immunodeficiency Virus Type 1 (HIV-1) Integrase: Resistance to Diketo Acid Integrase Inhibitors Impairs HIV-1 Replication and Integration and Confers Cross-Resistance to L-Chicoric Acid</p>

<p>           Lee, DJ and Robinson, WE Jr</p>

<p>           J Virol 2004. 78(11): 5835-47</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140981&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140981&amp;dopt=abstract</a> </p><br />

<p>    3.    43383   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Structure-Based Design and Synthesis of Non-Nucleoside, Potent, and Orally Bioavailable Adenosine Deaminase Inhibitors</p>

<p>           Terasaka, T, Okumura, H, Tsuji, K, Kato, T, Nakanishi, I, Kinoshita, T, Kato, Y, Kuno, M, Seki, N, Naoe, Y, Inoue, T, Tanaka, K, and Nakamura, K</p>

<p>           J Med Chem 2004. 47(11): 2728-2731</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15139750&amp;dopt=abstract</a> </p><br />

<p>    4.    43384   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           High-performance liquid chromatography assay for the quantification of HIV protease inhibitors and non-nucleoside reverse transcriptase inhibitors in human plasma</p>

<p>           Rezk, NL, Tidwell, RR, and Kashuba, AD</p>

<p>           J Chromatogr B Analyt Technol Biomed Life Sci 2004. 805(2): 241-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15135096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15135096&amp;dopt=abstract</a> </p><br />

<p>    5.    43385   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Antiviral profile of HIV inhibitors in macrophages: implications for therapy</p>

<p>           Perno, CF, Balestra, E, Francesconi, M, Abdelahad, D, Calio, R, Balzarini, J, and Aquaro, S</p>

<p>           Curr Top Med Chem 2004. 4(9): 1009-15</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134554&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134554&amp;dopt=abstract</a> </p><br />

<p>    6.    43386   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           Novel nonpeptidic inhibitors of HIV-1 protease obtained via a new multicomponent chemistry strategy</p>

<p>           Yehia, Nasser AM, Antuch, Walfrido, Beck, Barbara, Hess, Sibylle, Schauer-Vukasinovic, Vesna, Almstetter, Michael, Furer, Patrick, Herdtweck, Eberhardt, and Domling, Alexander</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CB07PR-C/2/e414ecf323b0234aa60aa43abe86c3a5">http://www.sciencedirect.com/science/article/B6TF9-4CB07PR-C/2/e414ecf323b0234aa60aa43abe86c3a5</a> </p><br />

<p>    7.    43387   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           The viral infectivity factor (Vif) of HIV-1 unveiled</p>

<p>           Rose, Kristine M, Marin, Mariana, Kozak, Susan L, and Kabat, David</p>

<p>           Trends in Molecular Medicine 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7J-4CC7P82-3/2/bab6ab2256ef89d34c8b13eb87b110f5">http://www.sciencedirect.com/science/article/B6W7J-4CC7P82-3/2/bab6ab2256ef89d34c8b13eb87b110f5</a> </p><br />

<p>    8.    43388   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           Isolation and characterization of anti-HIV peptides from Dorstenia contrajerva and Treculia obovoidea</p>

<p>           Bokesch, Heidi R, Charan, Romila D, Meragelman, Karina M, Beutler, John A, Gardella, Roberta, O&#39;Keefe, Barry R, McKee, Tawnya C, and McMahon, James B</p>

<p>           FEBS Letters 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T36-4CCCT3R-2/2/023860a033aed26c64893351f6b27672">http://www.sciencedirect.com/science/article/B6T36-4CCCT3R-2/2/023860a033aed26c64893351f6b27672</a> </p><br />

<p>    9.    43389   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Nucleoside inhibitors of human immunodeficiency virus type 1 reverse transcriptase</p>

<p>           Sharma, PL, Nurpeisov, V, Hernandez-Santiago, B, Beltran, T, and Schinazi, RF</p>

<p>           Curr Top Med Chem 2004. 4(9): 895-919</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134548&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134548&amp;dopt=abstract</a> </p><br />

<p>  10.    43390   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           Thermodynamic rules for the design of high affinity HIV-1 protease inhibitors with adaptability to mutations and high selectivity towards unwanted targets</p>

<p>           Ohtaka, Hiroyasu, Muzammil, Salman, Schon, Arne, Velazquez-Campoy, Adrian, Vega, Sonia, and Freire, Ernesto</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CBVKTD-2/2/9d242653dee9627e420ce4274cc03c7f">http://www.sciencedirect.com/science/article/B6TCH-4CBVKTD-2/2/9d242653dee9627e420ce4274cc03c7f</a> </p><br />

<p>  11.    43391   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           Designing anti-AIDS drugs targeting the major mechanism of HIV-1 RT resistance to nucleoside analog drugs</p>

<p>           Sarafianos, Stefan G, Hughes, Stephen H, and Arnold, Eddy</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CBVS67-4/2/dbc0a5428d09b1b68415773ac2c85e98">http://www.sciencedirect.com/science/article/B6TCH-4CBVS67-4/2/dbc0a5428d09b1b68415773ac2c85e98</a> </p><br />

<p>  12.    43392   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Orally Active CCR5 Antagonists as Anti-HIV-1 Agents: Synthesis and Biological Activity of 1-Benzothiepine 1,1-Dioxide and 1-Benzazepine Derivatives Containing a Tertiary Amine Moiety</p>

<p>           Seto, M, Aramaki, Y, Okawa, T, Miyamoto, N, Aikawa, K, Kanzaki, N, Niwa, S, Iizawa, Y, Baba, M, and Shiraishi, M</p>

<p>           Chem Pharm Bull (Tokyo) 2004. 52(5): 577-90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133211&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133211&amp;dopt=abstract</a> </p><br />

<p>  13.    43393   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           A hierarchical model of HIV-1 protease drug resistance</p>

<p>           Goodsell, DS</p>

<p>           Appl Bioinformatics 2002. 1(1): 3-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130852&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130852&amp;dopt=abstract</a> </p><br />

<p>  14.    43394   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           In vivo examination of hydroxyurea and the novel ribonucleotide reductase inhibitors trimidox and didox in combination with the reverse transcriptase inhibitor abacavir: suppression of retrovirus-induced immunodeficiency disease</p>

<p>           Sumpter, LR, Inayat, MS, Yost, EE, Duvall, W, Hagan, E, Mayhew, CN, Elford, HL, and Gallicchio, VS</p>

<p>           Antiviral Res  2004. 62(3): 111-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130534&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130534&amp;dopt=abstract</a> </p><br />

<p>  15.    43395   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Antiretrovirals, part 1: overview, history, and focus on protease inhibitors</p>

<p>           Wynn, GH, Zapor, MJ, Smith, BH, Wortmann, G, Oesterheld, JR, Armstrong, SC, and Cozza, KL</p>

<p>           Psychosomatics 2004. 45(3): 262-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15123854&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15123854&amp;dopt=abstract</a> </p><br />

<p>  16.    43396   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Are fusion inhibitors active against all HIV variants?</p>

<p>           Poveda, E, Rodes, B, Toro, C, and Soriano, V</p>

<p>           AIDS Res Hum Retroviruses 2004. 20(3): 347-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15117459&amp;dopt=abstract</a> </p><br />

<p>  17.    43397   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Mutations in HIV-1 Reverse Transcriptase Potentially Associated with Hypersusceptibility to Nonnucleoside Reverse-Transcriptase Inhibitors: Effect on Response to Efavirenz-Based Therapy in an Urban Observational Cohort</p>

<p>           Tozzi, V, Zaccarelli, M, Narciso, P, Trotta, MP, Ceccherini-Silberstein, F, De, Longis P, D&#39;Offizi, G, Forbici, F, D&#39;Arrigo, R, Boumis, E, Bellagamba, R, Bonfigli, S, Carvelli, C, Antinori, A, and Perno, CF</p>

<p>           J Infect Dis 2004. 189(9): 1688-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15116307&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15116307&amp;dopt=abstract</a> </p><br />

<p>  18.    43398   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Blocking of Human Immunodeficiency Virus Type-1 Virion Autolysis by Autologous p2(gag) Peptide</p>

<p>           Misumi, S, Morikawa, Y, Tomonaga, M, Ohkuma, K, Takamune, N, and Shoji, S</p>

<p>           J Biochem (Tokyo) 2004. 135(3): 447-53</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113844&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113844&amp;dopt=abstract</a> </p><br />

<p>  19.    43399   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Old drugs as lead compounds for a new disease? Binding analysis of SARS coronavirus main proteinase with HIV, psychotic and parasite drugs</p>

<p>           Zhang, XW and Yap, YL</p>

<p>           Bioorg Med Chem 2004. 12(10): 2517-21</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110833&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110833&amp;dopt=abstract</a> </p><br />

<p>  20.    43400   HIV-LS-296; EMBASE-HIV-5/18/2004</p>

<p class="memofmt1-2">           Green tea polyphenol epigallocatechin gallate binding to CD4 as a model for the inhibition of HIV-1-gp120 binding to CD4+ T cells</p>

<p>           McCormick, TG, Nance, CL, Williamson, MP, and Shearer, WT</p>

<p>           Journal of Allergy and Clinical Immunology 2004. 113(2, Supplement 1): S256</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-151/2/314fd3049fa9bc8479c3797d32528777">http://www.sciencedirect.com/science/article/B6WH4-4BRJ5RW-151/2/314fd3049fa9bc8479c3797d32528777</a> </p><br />

<p>  21.    43401   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Threshold interaction energy of NRTI&#39;s (2(&#39;)-deoxy 3(&#39;)-substituted nucleosidic analogs of reverse transcriptase inhibitors) to undergo competitive inhibition</p>

<p>           Yadav, A and Singh, SK</p>

<p>           Bioorg Med Chem Lett 2004. 14(10):  2677-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109677&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109677&amp;dopt=abstract</a> </p><br />

<p>  22.    43402   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Linker-modified quinoline derivatives targeting HIV-1 integrase: synthesis and biological activity</p>

<p>           Benard, C, Zouhiri, F, Normand-Bayle, M, Danet, M, Desmaele, D, Leh, H, Mouscadet, JF, Mbemba, G, Thomas, CM, Bonnenfant, S, Le, Bret M, and D&#39;Angelo, J</p>

<p>           Bioorg Med Chem Lett 2004. 14(10):  2473-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109635&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15109635&amp;dopt=abstract</a> </p><br />

<p>  23.    43403   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Heterogeneity of Envelope Molecules Shown by Different Sensitivities to Anti-V3 Neutralizing Antibody and CXCR4 Antagonist Regulates the Formation of Multiple-Site Binding of HIV-1</p>

<p>           Harada, S, Yusa, K, and Maeda, Y</p>

<p>           Microbiol Immunol 2004. 48(4): 357-65</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107547&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107547&amp;dopt=abstract</a> </p><br />

<p>  24.    43404   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Adsorption and infectivity of human immunodeficiency virus type 1 are modified by the fluidity of the plasma membrane for multiple-site binding</p>

<p>           Harada, S, Akaike, T, Yusa, K, and Maeda, Y</p>

<p>           Microbiol Immunol 2004. 48(4): 347-55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107546&amp;dopt=abstract</a> </p><br />

<p>  25.    43405   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Novel polysulfated galactose-derivatized dendrimers as binding antagonists of human immunodeficiency virus type 1 infection</p>

<p>           Kensinger, RD, Catalone, BJ, Krebs, FC, Wigdahl, B, and Schengrund, CL</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1614-23</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105112&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105112&amp;dopt=abstract</a> </p><br />

<p>  26.    43406   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           A structural and thermodynamic escape mechanism from a drug resistant mutation of the HIV-1 protease</p>

<p>           Vega, S, Kang, LW, Velazquez-Campoy, A, Kiso, Y, Amzel, LM, and Freire, E</p>

<p>           Proteins 2004. 55(3): 594-602</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15103623&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15103623&amp;dopt=abstract</a> </p><br />

<p>  27.    43407   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Comparison of Drug Resistance Mutations and Their Interpretation in Patients Infected With Non-B HIV-1 Variants and Matched Patients Infected With HIV-1 Subtype B</p>

<p>           Montes, B, Vergne, L, Peeters, M, Reynes, J, Delaporte, E, and Segondy, M</p>

<p>           J Acquir Immune Defic Syndr 2004.  35(4): 329-336</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15097148&amp;dopt=abstract</a> </p><br />

<p>  28.    43408   HIV-LS-296; PUBMED-HIV-5/18/2004</p>

<p class="memofmt1-2">           Site-directed PEGylation of trichosanthin retained its anti-HIV activity with reduced potency in vitro</p>

<p>           Wang, JH, Tam, SC, Huang, H, Ouyang, DY, Wang, YY, and Zheng, YT</p>

<p>           Biochem Biophys Res Commun 2004. 317(4):  965-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15094363&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15094363&amp;dopt=abstract</a> </p><br />

<p>  29.    43409   HIV-LS-296; WOS-HIV-5/09/2004</p>

<p class="memofmt1-2">           Interaction of HIV-1 reverse transcriptase with modified oligonucleotide primers containing 2 &#39;-O-beta-D-ribofuranosyladenosine</p>

<p>           Golubeva, AS, Ermolinsky, BS, Efimtseva, EV, Tunitskaya, VL, van, Aerschot A, Herdewijn, P, Mikhailov, SN, and Kochetkov, SN</p>

<p>           BIOCHEMISTRY-MOSCOW+ 2004. 69(2): 130-136</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220754400002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220754400002</a> </p><br />

<p>  30.    43410   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Mutations at position 184 of human immunodeficiency virus type-1 reverse transcriptase affect virus titer and viral DNA synthesis</p>

<p>           Julias, JG, Boyer, PL, McWilliams, MJ, Alvord, WG, and Hughes, SH</p>

<p>           VIROLOGY 2004. 322(1): 13-21</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220861100002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220861100002</a> </p><br />

<p>  31.    43411   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           A single amino acid substitution in human APOBEC3G antiretroviral enzyme confers resistance to HIV-1 virion infectivity factor-induced depletion</p>

<p>           Xu, HZ, Svarovskaia, ES, Barr, R, Zhang, YJ, Khan, MA, Strebel, K, and Pathak, VK</p>

<p>           P NATL ACAD SCI USA 2004. 101(15): 5652-5657</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220861500064">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220861500064</a> </p><br />

<p>  32.    43412   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Dendritic cells cross-present HIV antigens from live as well as apoptotic infected CD4(+) T lymphocytes</p>

<p>           Maranon, C, Desoutter, JF, Hoeffel, G, Cohen, W, Hanau, D, and Hosmalin, A</p>

<p>           P NATL ACAD SCI USA 2004. 101(16): 6092-6097</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220978000075">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220978000075</a> </p><br />

<p>  33.    43413   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Isolation of salicin derivatives from Homalium cochinchinensis and their antiviral activities</p>

<p>           Ishikawa, T, Nishigaya, K, Takami, K, Uchikoshi, H, Chen, IS, and Tsai, IL</p>

<p>           J NAT PROD 2004. 67(4): 659-663</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221001000021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221001000021</a> </p><br />

<p>  34.    43414   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Structural and energetic analyses of the effects of the K103N mutation of HIV-1 reverse transcriptase on efavirenz analogues</p>

<p>           Udier-Blagovic, M, Tirado-Rives, J, and Jorgensen, WL</p>

<p>           J MED CHEM 2004. 47(9): 2389-2392</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220918500023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220918500023</a> </p><br />

<p>  35.    43415   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Once-daily protease inhibitor - REYATAZ (TM) (atazanavir) - Approved by European Commission for treatment-experienced people with HIV</p>

<p>           Simond, A</p>

<p>           INFECTION 2004. 32(2): A8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221002900014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000221002900014</a> </p><br />

<p>  36.    43416   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Polyamidoamine dendrimers inhibit binding of Tat peptide to TAR RNA</p>

<p>           Zhao, H, Li, JR, Xi, F, and Jiang, L</p>

<p>           FEBS LETT 2004. 563(1-3): 241-245</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220773200045">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220773200045</a> </p><br />

<p>  37.    43417   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Can HIV be cured?  Mechanisms of HIV persistence and strategies to combat it</p>

<p>           Hamer, DH</p>

<p>           CURR HIV RES 2004. 2(2): 99-111</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220935000001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220935000001</a> </p><br />

<p>  38.    43418   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Progress towards the development of a HIV-1 gp41-directed vaccine</p>

<p>           McGaughey, GB, Barbato, G, Bianchi, E, Freidinger, RM, Garsky, VM, Hurni, WM, Joyce, JG, Liang, XP, Miller, MD, Pessi, A, Shiver, JW, and Bogusky, MJ</p>

<p>           CURR HIV RES 2004. 2(2): 193-204</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220935000009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220935000009</a> </p><br />

<p>  39.    43419   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Pharmacokinetic enhancement of protease inhibitor therapy</p>

<p>           King, JR, Wynn, H, Brundage, R, and Acosta, EP</p>

<p>           CLIN PHARMACOKINET 2004. 43(5): 291-310</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220989400003">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220989400003</a> </p><br />

<p>  40.    43420   HIV-LS-296; WOS-HIV-5/16/2004</p>

<p class="memofmt1-2">           Enhancement of the inhibitory activity of oatp, antisense oligonucleotides by incorporation of 2 &#39;-O,4 &#39;-C-ethylene-bridged nucleic acids (ENA) without a loss of subtype selectivity</p>

<p>           Takagi, M, Morita, K, Nakai, D, Nakagomi, R, Tokui, T, and Koizumi, M</p>

<p>           BIOCHEMISTRY-US 2004. 43(15): 4501-4510</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220812900011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000220812900011</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
