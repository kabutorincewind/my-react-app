

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-326.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tjZal1Q9ZpZ5TXHGazZVuVWSoULXAh/1N5yK/DSWfXZFFagrQMH3hLmxmQg3nDGRMvIhVRPf6h0PvHBRqRYFX0bS/rUes39hkHtIXFcNw1v6WPGvz5gI1KAhw1A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="86CAC108" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-326-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47628   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Drug Susceptibility Testing of Mycobacterium tuberculosis by a Nitrate Reductase Assay Applied Directly on Microscopy-Positive Sputum Samples</p>

    <p>          Musa, HR, Ambroggi, M, Souto, A, and Angeby, KA</p>

    <p>          J Clin Microbiol <b>2005</b>.  43(7): 3159-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16000429&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16000429&amp;dopt=abstract</a> </p><br />

    <p>2.     47629   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          In vivo efficiency of targeted norfloxacin against persistent, isoniazid-insensitive, Mycobacterium bovis BCG present in the physiologically hypoxic mouse liver</p>

    <p>          Balazuc, AM, Lagranderie, M, Chavarot, P, Pescher, P, Roseeuw, E, Schacht, E, Domurado, D, and Marchal, G</p>

    <p>          Microbes Infect <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994108&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15994108&amp;dopt=abstract</a> </p><br />

    <p>3.     47630   OI-LS-326; EMBASE-OI-7/11/2005</p>

    <p class="memofmt1-2">          Identification of heteroarylenamines as a new class of antituberculosis lead molecules</p>

    <p>          Copp, Brent R, Christiansen, Holly C, Lindsay, Brent S, and GFranzblau, Scott</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4GJVB2J-F/2/231bd3c989f0aafb84b289c547ab54ee">http://www.sciencedirect.com/science/article/B6TF9-4GJVB2J-F/2/231bd3c989f0aafb84b289c547ab54ee</a> </p><br />

    <p>4.     47631   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Moxifloxacin (Bay 12-8039): a new methoxy quinolone antibacterial</p>

    <p>          Macgowan, AP</p>

    <p>          Expert Opin Investig Drugs <b>1999</b>.  8(2): 181-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15992072&amp;dopt=abstract</a> </p><br />

    <p>5.     47632   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Comparative molecular study of Mycobacterium tuberculosis strains, in times of antimicrobial drug resistance</p>

    <p>          Varela, G, Carvajales, S, Gadea, P, Sirok, A, Grotiuz, G, Mota, MI, Ritacco, V, Reniero, A, Rivas, C, and Schelotto, F</p>

    <p>          Rev Argent Microbiol <b>2005</b>.  37(1): 11-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15991474&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15991474&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47633   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Cyclophilin B is a functional regulator of hepatitis C virus RNA polymerase</p>

    <p>          Watashi, K, Ishii, N, Hijikata, M, Inoue, D, Murata, T, Miyanari, Y, and Shimotohno, K</p>

    <p>          Mol Cell <b>2005</b>.  19(1): 111-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15989969&amp;dopt=abstract</a> </p><br />

    <p>7.     47634   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Cryptococcus neoformans resistance to echinocandins: (1,3)beta-glucan synthase activity is sensitive to echinocandins</p>

    <p>          Maligie, MA and Selitrennikoff, CP</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(7): 2851-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980360&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15980360&amp;dopt=abstract</a> </p><br />

    <p>8.     47635   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Inhibition effect of Chinese herbal medicine on transcription of hepatitis C virus structural gene in vitro</p>

    <p>          Dou, J, Chen, Q, and Wang, J</p>

    <p>          World J Gastroenterol <b>2005</b>.  11(23): 3619-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15962388&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15962388&amp;dopt=abstract</a> </p><br />

    <p>9.     47636   OI-LS-326; PUBMED-OI-7/11/2005</p>

    <p class="memofmt1-2">          Multidrug-resistant tuberculosis (MDR-TB): epidemiology, prevention and treatment</p>

    <p>          Ormerod, LP</p>

    <p>          Br Med Bull <b>2005</b>.  73: 17-24</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956357&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15956357&amp;dopt=abstract</a> </p><br />

    <p>10.   47637   OI-LS-326; WOS-OI-7/3/2005</p>

    <p class="memofmt1-2">          Antifungal effect and possible mode of activity of a compound from the marine sponge Dysidea herbacea</p>

    <p>          Sionov, E, Roth, D, Sandovsky-Losica, H, Kashman, Y, Rudi, A, Chill, L, Berdicevsky, I, and Segal, E</p>

    <p>          JOURNAL OF INFECTION <b>2005</b>.  50(5): 453-460, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229807600015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229807600015</a> </p><br />

    <p>11.   47638   OI-LS-326; WOS-OI-7/3/2005</p>

    <p class="memofmt1-2">          Activation of interferon-stimulated response element in Huh-7 cells replicating hepatitis C virus subgenomic RNA</p>

    <p>          Pai, M, Prabhu, R, Panebra, A, Nangle, S, Haque, S, Bastian, F, Garry, R, Agrawal, K, Goodbourn, S, and Dash, S</p>

    <p>          INTERVIROLOGY <b>2005</b>.  48(5): 301-311, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229781400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229781400006</a> </p><br />

    <p>12.   47639   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          Expression and characterization of the carboxyl esterase Rv3487c from Mycobacterium tuberculosis</p>

    <p>          Zhang, M, Wang, JD, Li, ZF, Xie, J, Yang, YP, Zhong, Y, and Wang, HH</p>

    <p>          PROTEIN EXPRESSION AND PURIFICATION <b>2005</b>.  42(1): 59-66, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229915500009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229915500009</a> </p><br />

    <p>13.   47640   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          A family of mycothiol analogues</p>

    <p>          Knapp, S, Amorelli, B, Darout, E, Ventocilla, CC, Goldman, LM, Huhn, RA, and Minnihan, EC</p>

    <p>          JOURNAL OF CARBOHYDRATE CHEMISTRY <b>2005</b>.  24(2): 103-130, 28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229970100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229970100002</a> </p><br />

    <p>14.   47641   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          In vitro activity of linezolid, clarithromycin and moxifloxacin against clinical isolates of Mycobacterium kansasii</p>

    <p>          Guna, R, Munoz, C, Dominguez, V, Garcia-Garcia, A, Galvez, J, de, Julian-Ortiz JV, and Borras, R</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2005</b>.  55(6): 950-953, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230028100023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230028100023</a> </p><br />

    <p>15.   47642   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          Antileishmanial and antifungal activities of xanthanolides isolated from Xanthium macrocarpum</p>

    <p>          Lavault, M, Landreau, A, Larcher, G, Bouchara, JP, Pagniez, F, Le, Pape P, and Richomme, P</p>

    <p>          FITOTERAPIA <b>2005</b>.  76(3-4): 363-366, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230042600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230042600016</a> </p><br />

    <p>16.   47643   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          The synthesis and antimicrobial activity of some new methyl N-arylthiocarbamates, dimethyl N-aryldithiocarbonimidates and 2-arylamino-2-imidazolines</p>

    <p>          Servi, S, Genc, M, Gur, S, and Koca, M</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2005</b>.  40(7): 687-693, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230012900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000230012900008</a> </p><br />

    <p>17.   47644   OI-LS-326; WOS-OI-7/10/2005</p>

    <p class="memofmt1-2">          From candidate to drug</p>

    <p>          Hutchison, J</p>

    <p>          CHEMISTRY &amp; INDUSTRY <b>2005</b>.(11): 23-25, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229826000021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229826000021</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
