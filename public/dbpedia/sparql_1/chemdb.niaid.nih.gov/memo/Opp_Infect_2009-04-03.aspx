

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-04-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8dn5FPaa++lx54naJyGms4Wmur3ivoV8r+u1Ph+In6TjmkPf7GmW+pRCmnVl8DYANksOsYWPgl9k+jOiG+5Xl/oyPLe6M5MGiVvD92rciubUOXsgpm7//5zSKvo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A7526CE7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">March 18, 2009 - March 31, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p> 

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18603337?ordinalpos=70&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and anti-microbial screening of some Schiff bases of 3-amino-6,8-dibromo-2-phenylquinazolin-4(3H)-ones.</a></p>

    <p class="NoSpacing">Panneerselvam P, Rather BA, Ravi Sankar Reddy D, Ramesh Kumar N.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 May;44(5):2328-33. Epub 2008 Apr 29.</p>

    <p class="NoSpacing">PMID: 18603337 [PubMed - in process]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19139291?ordinalpos=76&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Activity of anidulafungin in a murine model of Candida krusei infection: evaluation of mortality and disease burden by quantitative tissue cultures and measurement of serum (1,3)-beta-D-glucan levels.</a></p>

    <p class="NoSpacing">Ostrosky-Zeichner L, Paetznick VL, Rodriguez J, Chen E, Sheehan DJ.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Apr;53(4):1639-41. Epub 2009 Jan 12.</p>

    <p class="NoSpacing">PMID: 19139291 [PubMed - in process]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18769871?ordinalpos=93&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antifungal activity and mode of action of silver nano-particles on Candida albicans.</a></p>

    <p class="NoSpacing">Kim KJ, Sung WS, Suh BK, Moon SK, Choi JS, Kim JG, Lee DG.</p>

    <p class="NoSpacing">Biometals. 2009 Apr;22(2):235-42. Epub 2008 Sep 4.</p>

    <p class="NoSpacing">PMID: 18769871 [PubMed - in process]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19297172?ordinalpos=2&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Carbonic anhydrase inhibitors. Inhibition of the beta-class enzymes from the fungal pathogens Candida albicans and Cryptococcus neoformans with aliphatic and aromatic carboxylates.</a></p>

    <p class="NoSpacing">Innocenti A, Hall RA, Schlicker C, Mühlschlegel FA, Supuran CT.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Apr 1;17(7):2654-7. Epub 2009 Mar 5.</p>

    <p class="NoSpacing">PMID: 19297172 [PubMed - in process]</p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19303801?ordinalpos=135&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">On the effect of thalidomide on Mycobacterium avium subspecies paratuberculosis in culture.</a></p>

    <p class="NoSpacing">Greenstein RJ, Su L, Brown ST.</p>

    <p class="NoSpacing">Int J Infect Dis. 2009 Mar 19. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19303801 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19285414?ordinalpos=166&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis, biological evaluation and molecular modelling studies of novel quinoline derivatives against Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Upadhayaya RS, Vandavasi JK, Vasireddy NR, Sharma V, Dixit SS, Chattopadhyaya J.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Apr 1;17(7):2830-41. Epub 2009 Feb 21.</p>

    <p class="NoSpacing">PMID: 19285414 [PubMed - in process]</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19266617?ordinalpos=179&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Piscidin 4, a novel member of the piscidin family of antimicrobial peptides.</a></p>

    <p class="NoSpacing">Noga EJ, Silphaduang U, Park NG, Seo JK, Stephenson J, Kozlowicz S.</p>

    <p class="NoSpacing">Comp Biochem Physiol B Biochem Mol Biol. 2009 Apr;152(4):299-305.</p>

    <p class="NoSpacing">PMID: 19266617 [PubMed - in process]</p> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18608758?ordinalpos=301&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Anti-tubercular activity of ruthenium (II) complexes with polypyridines.</a></p>

    <p class="NoSpacing">Hadda TB, Akkurt M, Baba MF, Daoudi M, Bennani B, Kerbal A, Chohan ZH.</p>

    <p class="NoSpacing">J Enzyme Inhib Med Chem. 2009 Apr;24(2):457-63.</p>

    <p class="NoSpacing">PMID: 18608758 [PubMed - in process]</p>  

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="ListParagraph">9. Rostom S.A.F., et al.</p>

    <p class="NoSpacing">Azole antimicrobial pharmacophore-based tetrazoles: Synthesis and biological evaluation as potential antimicrobial and anticonvulsant agents</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY  2009  (March) 17: 2410 - 2422</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
