

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-02-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="EG9h/17jrsSMa9XNP5kcwg+g+1gDtoE92BslgI14r3q4mEjdrQgeN6Fz+mbQzTE1j8azRD5jcFMpxJAvFvT0vk3P5tEOL389XxZoI/4EP6W77PQ7ndwSE0oabIk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9BE8DFDC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">February 4 - February 17, 2011</p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21300829">Benzimidazole Analogs Inhibit Human Herpesvirus 6</a>. Prichard, M.N., S.L. Frederick, S. Daily, K.Z. Borysko, L.B. Townsend, J.C. Drach, and E.R. Kern. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21300829].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Herpes simplex virus</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21307190">Hydrolyzable Tannins (Chebulagic Acid and Punicalagin) Target Viral Glycoprotein-Glycosaminoglycan Interactions to Inhibit Herpes Simplex Virus Type 1 Entry and Cell-to-Cell Spread</a>. Lin, L.T., T.Y. Chen, C.Y. Chung, R.S. Noyce, T.B. Grindley, C. McCormick, T.C. Lin, G.H. Wang, C.C. Lin, and C.D. Richardson. Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[21307190].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21294043">In Vitro Cytotoxic and Antiviral Activities of Ficus Carica Latex Extracts</a>. Lazreg Aref, H., B. Gaaliche, A. Fekih, M. Mars, M. Aouni, J. Pierre Chaumon, and K. Said. Natural Product Research, 2011. 25(3): p. 310-319; PMID[21294043].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21292495">Synthesis and Biological Assay of 4-Aryl-6-Chloro-Quinoline Derivatives as Novel Non-Nucleoside Anti-Hbv Agents</a>. Guo, R.H., Q. Zhang, Y.B. Ma, X.Y. Huang, J. Luo, L.J. Wang, C.A. Geng, X.M. Zhang, J. Zhou, Z.Y. Jiang, and J.J. Chen. Bioorganic &amp; Medicinal Chemistry, 2011. <b>19</b>(4): p. 1400-1408; PMID[21292495].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21316235">Hepatitis C Ns5b Polymerase Inhibitors: Functional Equivalents for the Benzothiadiazine Moiety</a>. Hutchinson, D.K., C.A. Flentge, P.L. Donner, R. Wagner, C.J. Maring, W.M. Kati, Y. Liu, S.V. Masse, T. Middleton, H. Mo, D. Montgomery, W.W. Jiang, G. Koev, D.W. Beno, K.D. Stewart, V.S. Stoll, A. Molla, and D.J. Kempf. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21316235].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21301021">Biliverdin and Heme Oxygenase Antiviral Activity against Hepatitis C Virus</a>. Gutierrez-Grobe, Y., L. Vitek, C. Tiribelli, R.A. Kobashi-Margain, M. Uribe, and N. Mendez-Sanchez. Annals of hepatology : official journal of the Mexican Association of Hepatology, 2011. 10(1): p. 105-107; PMID[21301021].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21297992">Differential in Vitro Effects of Intravenous Versus Oral Formulations of Silibinin on the Hcv Life Cycle and Inflammation</a>. Wagoner, J., C. Morishima, T.N. Graf, N.H. Oberlies, E. Teissier, E.I. Pecheur, J.E. Tavis, and S.J. Polyak. Plos One, 2011. <b>6</b>(1): p. e16464; PMID[21297992].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21297107">Optimized High-Throughput Screen for Hepatitis C Virus Translation Inhibitors</a>. Berry, K.E., B. Peng, D. Koditek, D. Beeman, N. Pagratis, J.K. Perry, J. Parrish, W. Zhong, J.A. Doudna, and I.H. Shih. Journal of biomolecular screening : the official journal of the Society for Biomolecular Screening, 2011. 16(2): p. 211-220; PMID[21297107].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0204-021711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
