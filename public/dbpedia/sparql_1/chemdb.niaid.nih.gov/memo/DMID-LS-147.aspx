

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-147.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AKrpszBiSLLqJYnXJD1IWm6driTtAsDCmY3oqIsOyiHpl7iqtPdQ6mkOlI4/C7V8ca+cugjSkW1rpeMgiE50shlHsZHeuW9+4SQuxXmR7Nf1eltwmG6mRrvw9mc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="44F3E10C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-147-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61096   DMID-LS-147; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Preparation of pyrrolo[3,2-c]quinoline derivatives useful in prepn. of medicaments for treatment and prevention of microbial infections by killing clin. latent microorganisms</p>

    <p>          Beck, Petra Helga, Brown, Marc Barry, Clark, David Edward, Coates, Anthony, Dyke, Hazel Joan, Hu, Yanmin, Londesbrough, Derek John, Mills, Keith, Pallin, Thomas David, Reid, Gary Patrick, and Stoddart, Gerlinda</p>

    <p>          PATENT:  WO <b>2007054693</b>  ISSUE DATE:  20070518</p>

    <p>          APPLICATION: 2006  PP: 179pp.</p>

    <p>          ASSIGNEE:  (Helperby Therapeutics Ltd., UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61097   DMID-LS-147; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Preparation of pyridazine, pyrimidine, and pyridine heterocyclic compounds as antiviral agents against hepatitis C virus</p>

    <p>          Ueno, Hiroshi, Shimada, Takashi, Aoyagi, Kouichi, Katoh, Susumu, Shinkai, Hisashi, Motomura, Takahisa, Komoda, Yasumasa, Otsubaki, Tomoko, Soejima, Yuki, and Kawahara, Iichiro</p>

    <p>          PATENT:  WO <b>2007058392</b>  ISSUE DATE:  20070524</p>

    <p>          APPLICATION: 2006  PP: 1247pp.</p>

    <p>          ASSIGNEE:  (Japan Tobacco Inc., Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61098   DMID-LS-147; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          A new 1,2,4-triazole acyclonucleosides: synthesis and biological evaluation</p>

    <p>          Radi, Smaail and Lazrek, Hassan B</p>

    <p>          Lett. Drug Des. Discovery <b>2007</b>.  4(3): 212-214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61099   DMID-LS-147; SCIFINDER-OI-6/18/2007</p>

    <p class="memofmt1-2">          Synthesis of novel 2&#39;-methyl carbovir analogues as potent antiviral agents</p>

    <p>          Hong, JH</p>

    <p>          Arch Pharm Res <b>2007</b>.  30(2): 131-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61100   DMID-LS-147; WOS-DMID-6/15/2007</p>

    <p class="memofmt1-2">          Genomic Analysis and Geographic Visualization of the Spread of Avian Influenza (H5n1)</p>

    <p>          Janies, D. <i>et al.</i></p>

    <p>          Systematic Biology <b>2007</b>.  56(2): 321-329</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246448300013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246448300013</a> </p><br />

    <p>6.     61101   DMID-LS-147; WOS-DMID-6/15/2007</p>

    <p class="memofmt1-2">          Getting the Message Out: Rna Interference of Human Papillomavirus</p>

    <p>          Wright, J. and Mutch, D.</p>

    <p>          Reproductive Sciences <b>2007</b>.  14(1): 6-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246460600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246460600002</a> </p><br />

    <p>7.     61102   DMID-LS-147; WOS-DMID-6/15/2007</p>

    <p class="memofmt1-2">          In Silico Prediction of Sars Protease Inhibitors by Virtual High Throughput Screening</p>

    <p>          Plewczynski, D. <i>et al.</i></p>

    <p>          Chemical Biology &amp; Drug Design <b>2007</b>.  69(4): 269-279</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245990100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245990100006</a> </p><br />

    <p>8.     61103   DMID-LS-147; WOS-DMID-6/15/2007</p>

    <p class="memofmt1-2">          Specific Biochemical Features of Replication of Clinical Influenza Viruses in Human Intestinal Cell Culture</p>

    <p>          Zhirnov, O. <i>et al.</i></p>

    <p>          Biochemistry-Moscow <b>2007</b>.  72(4): 398-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245966000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245966000006</a> </p><br />

    <p>9.     61104   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Surveillance of Resistance to Adamantanes among Influenza A(H3N2) and A(H1N1) Viruses Isolated Worldwide</p>

    <p>          Deyde, VM, Xu, X, Bright, RA, Shaw, M, Smith, CB, Zhang, Y, Shu, Y, Gubareva, LV, Cox, NJ, and Klimov, AI</p>

    <p>          J Infect Dis <b>2007</b>.  196(2): 249-257</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570112&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570112&amp;dopt=abstract</a> </p><br />

    <p>10.   61105   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          John F. Enders Lecture 2006: Antivirals for Influenza</p>

    <p>          Ong, AK and Hayden, FG</p>

    <p>          J Infect Dis <b>2007</b>.  196(2): 181-190</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570104&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17570104&amp;dopt=abstract</a> </p><br />

    <p>11.   61106   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Pharmacokinetics of antiviral agent {beta}-D-2&#39;-deoxy-2&#39;-fluoro-2&#39;-C-methylcytidine in rhesus monkeys</p>

    <p>          Asif, G, Hurwitz, SJ, Shi, J, Hernandez-Santiago, BI, and Schinazi, RF</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562805&amp;dopt=abstract</a> </p><br />

    <p>12.   61107   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Increased Survival after Gemfibrozil Treatment of Severe Mouse Influenza</p>

    <p>          Budd, A, Alleva, L, Alsharifi, M, Koskinen, A, Smythe, V, Mullbacher, A, Wood, J, and Clark, I </p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562808&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562808&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>13.   61108   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Conversion of 3-Arylazo-5-phenyl-2(3H)-furanones into Other Heterocycles of Anticipated Biological Activity</p>

    <p>          Sayed, HH, Hashem, AI, Yousif, NM, and El-Sayed, WA</p>

    <p>          Arch Pharm (Weinheim) <b>2007</b>.  340(6): 315-319</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562565&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17562565&amp;dopt=abstract</a> </p><br />

    <p>14.   61109   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Virulence of a mouse-adapted Semliki Forest virus strain is associated with reduced susceptibility to interferon</p>

    <p>          Deuber, SA and Pavlovic, J</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 7): 1952-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17554028&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17554028&amp;dopt=abstract</a> </p><br />

    <p>15.   61110   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          N-Linked glycosylation attenuates H3N2 influenza viruses</p>

    <p>          Vigerust, DJ, Ulett, KB, Boyd, KL, Madsen, J, Hawgood, S, and McCullers, JA</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17553891&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17553891&amp;dopt=abstract</a> </p><br />

    <p>16.   61111   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Identification of individual structural fragments of N,N&#39;-(bis-5-nitropyrimidyl)dispirotripiperazine derivatives for cytotoxicity and antiherpetic activity allows the prediction of new highly active compounds</p>

    <p>          Artemenko, AG, Muratov, EN, Kuz&#39;min, VE, Kovdienko, NA, Hromov, AI, Makarov, VA, Riabova, OB, Wutzler, P, and Schmidtke, M</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17550890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17550890&amp;dopt=abstract</a> </p><br />

    <p>17.   61112   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Isolation and identification of an scFv antibody against nucleocapsid protein of SARS-CoV</p>

    <p>          Zhao, A, Qin, W, Han, Y, Wen, W, Zhang, W, Lian, Z, Chen, G, Zhang, Z, Peng, J, Wang, H, and Guo, Y</p>

    <p>          Microbes Infect <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17548223&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   61113   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Recent developments in human cytomegalovirus diagnosis</p>

    <p>          Halwachs-Baumann, G</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(3): 427-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17547507&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17547507&amp;dopt=abstract</a> </p><br />

    <p>19.   61114   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          A rapid DNA hybridization assay for the evaluation of antiviral compounds against Epstein-Barr virus</p>

    <p>          Prichard, MN, Daily, SL, Jefferson, GM, Perry, AL, and Kern, ER</p>

    <p>          J Virol Methods <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17540461&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17540461&amp;dopt=abstract</a> </p><br />

    <p>20.   61115   DMID-LS-147; PUBMED-DMID-6/18/2007</p>

    <p class="memofmt1-2">          Remarkable Loop Flexibility in Avian Influenza N1 and Its Implications for Antiviral Drug Design</p>

    <p>          Amaro, RE, Minh, DD, Cheng, LS, Lindstrom, WM Jr, Olson, AJ, Lin, JH, Li, WW, and McCammon, JA </p>

    <p>          J Am Chem Soc <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17539643&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
