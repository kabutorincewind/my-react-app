

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-151.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lkregGI8r+S6762n9nUu0jXSDmrGmo2DXVZ374HmT1S4uQA0KnCjVxLolM5AC0llKdfTkjV07Ogc43JKNhWBAfvo260AimfSnfKcSgTyX6OCLx3dbcbWWkK+Ntw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="796A621B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-151-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61451   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Characterization of Rift Valley fever virus transcriptional terminations</p>

    <p>          Ikegami, Tetsuro, Won, Sungyong, Peters, CJ, and Makino, Shinji</p>

    <p>          J. Virol. <b>2007</b>.  81(16): 8421-8438</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61452   DMID-LS-151; WOS-DMID-8/10/2007</p>

    <p class="memofmt1-2">          Hepatitis B Virus Infection</p>

    <p>          Chang, M.</p>

    <p>          Seminars in Fetal &amp; Neonatal Medicine <b>2007</b>.  12(3): 160-167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247024200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247024200003</a> </p><br />

    <p>3.     61453   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Preparation of epirubicin analogs for use as antiviral prodrugs</p>

    <p>          Kulikowski, Tadeusz, Bretner, Maria, Najda, Andzelika, Cova, Lucyna, Trepo, Christian, Narayan, Ramamurthy, Piasek, Andrzej, Lipniacki, Andrzej, and Zagorski-Ostoja, Wlodzimierz</p>

    <p>          PATENT:  WO <b>2007075092</b>  ISSUE DATE:  20070705</p>

    <p>          APPLICATION: 2005  PP: 16pp.</p>

    <p>          ASSIGNEE:  (Instytut Biochemii i Biofizyki, Pol., Institut National de la Sante et de la Recherche Medicale, INSERM, and Instytut Medycyny Doswiadczalnej i Klinicznej)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61454   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Synthesis, characterization and antiviral activity of 2,3-disubstituted quinazolones</p>

    <p>          Pandey, VK, Kumar, Jitendra, Saxena, SK, Mukesh, Joshi, MN, and Bajpai, SK</p>

    <p>          J. Indian Chem. Soc. <b>2007</b>.  84(6): 593-597</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61455   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Anti HSV-1 activity of Spirulina platensis polysaccharide</p>

    <p>          Chirasuwan, Nattayaporn, Chaiklahan, Ratana, Ruengjitchatchawalya, Marasri, Bunnag, Boosya, and Tanticharoen, Morakot</p>

    <p>          Kasetsart J.: Nat. Sci. <b>2007</b>.  41(2): 311-318</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61456   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Addition of a C-Terminal cysteine improves the anti-herpes simplex virus activity of a peptide containing the human immunodeficiency virus type 1 TAT protein transduction domain</p>

    <p>          Bultmann, Hermann, Teuton, Jeremy, and Brandt, Curtis R</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(5): 1596-1607</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61457   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Synthesis, antiviral, antituberculostic and antibacterial activities of some novel, 4-(4&#39;-substituted phenyl)-6-(4&#39;&#39;-hydroxyphenyl)-2-(substituted imino) pyrimidines</p>

    <p>          Siddiqui, Anees A, Rajesh, Ramadoss, Mojahid-Ul-Islam, Alagarsamy, Veerachamy, Meyyanathan, Subramania N, Kumar, Bommenahalli P, and Suresh, Bhojraj</p>

    <p>          Acta Pol. Pharm. <b>2007</b>.  64(1): 17-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61458   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Strong antiviral activity of heated and hydrated dolomite--preliminary investigation</p>

    <p>          Yamana Hideaki, Ito Hiroshi, Ito Toshihiro, Murase Toshiyuki, Motoike Koichi, Wakabayashi Kazuo, and Otsuki Koichi</p>

    <p>          J Vet Med Sci <b>2007</b>.  69(2): 217-9.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61459   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Cysteamine compounds for the treatment of viral infections</p>

    <p>          Liang, Hao Yi, Chi, Francis, Xu, Qingfu, and Chan, Bill Piu</p>

    <p>          PATENT:  WO <b>2007062272</b>  ISSUE DATE:  20070531</p>

    <p>          APPLICATION: 2006  PP: 39pp.</p>

    <p>          ASSIGNEE:  (Omega-Biopharma (H.K.) Limited, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61460   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Chymotrypsin-like cysteine protease inhibitor antiviral compositions and methods for the treatment of coronavirus infections</p>

    <p>          Pang, Yuan-Ping, Dooley, Andrea J, and Park, Jewn Giew</p>

    <p>          PATENT:  US <b>2007149487</b>  ISSUE DATE:  20070628</p>

    <p>          APPLICATION: 2006-33108  PP: 33pp.</p>

    <p>          ASSIGNEE:  (Mayo Foundation for Medical Education and Research, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61461   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Dendrimers and dendritic polymers as anti-infective agents: new antimicrobial strategies for therapeutic drugs</p>

    <p>          Rojo, J and Delgado, R</p>

    <p>          Anti-Infect. Agents Med. Chem. <b>2007</b>.  6(3): 151-174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61462   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Five-membered iminocyclitol derivatives as selective and potent glycosidase inhibitors: new structures for antivirals and osteoarthritis therapeutics</p>

    <p>          Liang, Pi-Hui, Lin, Yi-Ling, and Wong, Chi-Huey</p>

    <p>          PATENT:  WO <b>2007067515</b>  ISSUE DATE:  20070614</p>

    <p>          APPLICATION: 2006  PP: 33pp.</p>

    <p>          ASSIGNEE:  (Academia Sinica, Taiwan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   61463   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Antiviral traditional Chinese medicinal composition, its manufacturing method, quality control method and application</p>

    <p>          Yu, Wenfeng</p>

    <p>          PATENT:  CN <b>1970000</b>  ISSUE DATE: 20070530</p>

    <p>          APPLICATION: 1012-3993  PP: 23pp.</p>

    <p>          ASSIGNEE:  (Beijing Qiyuan Yide Institute of Materia Medica, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>14.   61464   DMID-LS-151; SCIFINDER-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Screening peptides which binds to membrane protein of SARS coronavirus and inhibits the binding between M protein and nucleocapsid (N) protein and their uses in treating SARS</p>

    <p>          Hatakeyama, Seisuke, Akiyama, Toru, Kirikae, Teruo, Sasazuki, Takehiko, Ishizaka, Yukito, Shichijo, Shigeki, and Ishida, Isao</p>

    <p>          PATENT:  JP <b>2007129942</b>  ISSUE DATE:  20070531</p>

    <p>          APPLICATION: 2005-63292  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Kirin Brewery Co., Ltd. Japan, Kokuritsu Kokusai Iryo Center, and Kurume University)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   61465   DMID-LS-151; WOS-DMID-8/10/2007</p>

    <p class="memofmt1-2">          Effective Replication of Human Influenza Viruses in Mice Lacking a Major Alpha 2,6 Sialyltransferase</p>

    <p>          Glaser, L. <i>et al.</i></p>

    <p>          Virus Research  <b>2007</b>.  126(1-2): 9-18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247169700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247169700002</a> </p><br />

    <p>16.   61466   DMID-LS-151; WOS-DMID-8/10/2007</p>

    <p class="memofmt1-2">          An Amino Acid Substitution in the Influenza a Virus Hemagglutinin Associated With Escape From Recognition by Human Virus-Specific Cd4(+) T-Cells</p>

    <p>          Berkhoff, E. <i>et al.</i></p>

    <p>          Virus Research  <b>2007</b>.  126(1-2): 282-287</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247169700033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247169700033</a> </p><br />

    <p>17.   61467   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          Exotic Animal Diseases Bulletin - Rift Valley Fever</p>

    <p>          Anon</p>

    <p>          Australian Veterinary Journal <b>2007</b>.  85(6): N18-N19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247032800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247032800011</a> </p><br />

    <p>18.   61468   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          In Vitro Resistance to Interferon-Alpha of Hepatitis B Virus With Basic Core Promoter Double Mutation</p>

    <p>          Wang, Y. <i>et al.</i></p>

    <p>          Antiviral Research <b>2007</b>.  75(2): 139-145</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>          <span class="memofmt1-3">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246956500006</span></p>

    <p>19.   61469   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          Broad-Spectrum Antiviral Activity of Small Interfering Rna Targeting the Conserved Rna Termini of Lassa Virus</p>

    <p>          Muller, S. and Gunther, S.</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(6): 2215-2218</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246991400052">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246991400052</a> </p><br />
    <br clear="all">

    <p>20.   61470   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          Antiviral Agents for the Treatment of Recurrent Respiratory Papillomatosis: a Systematic Review of the English-Language Literature</p>

    <p>          Chadha, N. and James, A.</p>

    <p>          Otolaryngology-Head and Neck Surgery <b>2007</b>.  136(6): 863-869</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247104400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247104400001</a> </p><br />

    <p>21.   61471   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          Camelpox Virus Encodes a Schlafen-Like Protein That Affects Orthopoxvirus Virulence</p>

    <p>          Gubser, C. <i>et al.</i></p>

    <p>          Journal of General Virology <b>2007</b>.  88: 1667-1676</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247087900003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247087900003</a> </p><br />

    <p>22.   61472   DMID-LS-151; WOS-DMID-8/3/2007</p>

    <p class="memofmt1-2">          Progress in the Treatment of Chronic Hepatitis B: Long-Term Experience With Adefovir Dipivoxil</p>

    <p>          Delaney, W.</p>

    <p>          Journal of Antimicrobial Chemotherapy <b>2007</b>.  59(5): 827-832</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247006200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247006200003</a> </p><br />

    <p>23.   61473   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Novel protease and polymerase inhibitors for the treatment of hepatitis C virus infection</p>

    <p>          Sheldon, J, Barreiro, P, and Vincent, V</p>

    <p>          Expert Opin Investig Drugs <b>2007</b>.  16(8): 1171-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17685867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17685867&amp;dopt=abstract</a> </p><br />

    <p>24.   61474   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Identification of Novel Epoxide Inhibitors of HCV Replication Using a High- Throughput Screen</p>

    <p>          Peng, LF, Kim, SS, Matchacheep, S, Lei, X, Su, S, Lin, W, Runguphan, W, Choe, WH, Sakamoto, N, Ikeda, M, Kato, N, Beeler, AB, Porco, JA Jr, Schreiber, SL, and Chung, RT</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17682098&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17682098&amp;dopt=abstract</a> </p><br />

    <p>25.   61475   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral and Cytostatic Evaluations of the New C-5 Substituted Pyrimidine and Furo[2,3-d]pyrimidine 4&#39;,5&#39;-Didehydro-l-ascorbic Acid Derivatives</p>

    <p>          Gazivoda, T, Sokcevic, M, Kralj, M, Suman, L, Pavelic, K, Clercq, ED, Andrei, G, Snoeck, R, Balzarini, J, Mintas, M, and Raic-Malic, S</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17672445&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17672445&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   61476   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Recent advances in the search for antiviral agents against human papillomaviruses</p>

    <p>          Fradet-Turcotte, A and Archambault, J</p>

    <p>          Antivir Ther <b>2007</b>.  12(4): 431-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668552&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668552&amp;dopt=abstract</a> </p><br />

    <p>27.   61477   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Non-nucleoside inhibitors of the HCV NS5B polymerase: Progress in the discovery and development of novel agents for the treatment of HCV infections</p>

    <p>          Beaulieu, PL</p>

    <p>          Curr Opin Investig Drugs <b>2007</b>.  8(8): 614-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668364&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17668364&amp;dopt=abstract</a> </p><br />

    <p>28.   61478   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Anti-herpes Simplex Virus Target of an Acidic Polysaccharide, Nostoflan, from the Edible Blue-Green Alga Nostoc flagelliforme</p>

    <p>          Kanekiyo, K, Hayashi, K, Takenaka, H, Lee, JB, and Hayashi, T</p>

    <p>          Biol Pharm Bull <b>2007</b>.  30(8): 1573-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17666824&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17666824&amp;dopt=abstract</a> </p><br />

    <p>29.   61479   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Quantitative Structure-Activity Relationship Studies of [(Biphenyloxy)propyl]isoxazole Derivatives. Inhibitors of Human Rhinovirus 2 Replication</p>

    <p>          Kuz&#39;min, VE, Artemenko, AG, Muratov, EN, Volineckaya, IL, Makarov, VA, Riabova, OB, Wutzler, P, and Schmidtke, M</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17665898&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17665898&amp;dopt=abstract</a> </p><br />

    <p>30.   61480   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Specific Plant Terpenoids and Lignoids Possess Potent Antiviral Activities against Severe Acute Respiratory Syndrome Coronavirus</p>

    <p>          Wen, CC, Kuo, YH, Jan, JT, Liang, PH, Wang, SY, Liu, HG, Lee, CK, Chang, ST, Kuo, CJ, Lee, SS, Hou, CC, Hsiao, PW, Chien, SC, Shyur, LF, and Yang, NS</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17663539&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17663539&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>31.   61481   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Allosteric Inhibitors of Hepatitis C Polymerase: Discovery of Potent and Orally Bioavailable Carbon-Linked Dihydropyrones</p>

    <p>          Li, H, Linton, A, Tatlock, J, Gonzalez, J, Borchardt, A, Abreo, M, Jewell, T, Patel, L, Drowns, M, Ludlum, S, Goble, M, Yang, M, Blazel, J, Rahavendran, R, Skor, H, Shi, S, Lewis, C, and Fuhrman, S</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17658778&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17658778&amp;dopt=abstract</a> </p><br />

    <p>32.   61482   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          A model of human cytomegalovirus infection in severe combined immunodeficient mice</p>

    <p>          Bravo, FJ, Cardin, RD, and Bernstein, DI</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17658624&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17658624&amp;dopt=abstract</a> </p><br />

    <p>33.   61483   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          In vitro methods to study RNA interference during an adenovirus infection</p>

    <p>          Andersson, G, Xu, N, and Akusjarvi, G</p>

    <p>          Methods Mol Med <b>2007</b>.  131: 47-61</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17656774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17656774&amp;dopt=abstract</a> </p><br />

    <p>34.   61484   DMID-LS-151; PUBMED-DMID-8/13/2007</p>

    <p class="memofmt1-2">          Influenza pandemic intervention planning using InfluSim: pharmaceutical and non- pharmaceutical interventions</p>

    <p>          Duerr, HP, Brockmann, SO, Piechotowski, I, Schwehm, M, and Eichner, M</p>

    <p>          BMC Infect Dis  <b>2007</b>.  7: 76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629919&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17629919&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
