

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-08-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vONgBIThmIOtBbkGfX7ODK4nFCtw50T0VFz2Ds2JExs3ZR0St3ijOP20+9QkE0i/nIya+bhbvYZhiyR0O9c4loEpq8cV1zgE2DWAcwQhfXiL7VzTxmDdzYZdylI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="60D9D873" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: July 31 - August 13, 2015</h1>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26033732">Gallium Compounds Exhibit Potential as New Therapeutic Agents against Mycobacterium abscessus.</a> Abdalla, M.Y., B.L. Switzer, C.H. Goss, M.L. Aitken, P.K. Singh, and B.E. Britigan. Antimicrobial Agents and Chemotherapy, 2015. 59(8): p. 4826-4834. PMID[26033732].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26033731">Disulfiram and Copper Ions Kill Mycobacterium Tuberculosis in a Synergistic Manner.</a> Dalecki, A.G., M. Haeili, S. Shah, A. Speer, M. Niederweis, O. Kutsch, and F. Wolschendorf. Antimicrobial Agents and Chemotherapy, 2015. 59(8): p. 4835-4844. PMID[26033731].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26048808">Synthesis and Evaluation of Novel Fluorinated Pyrazolo-1,2,3-triazole Hybrids as Antimycobacterial Agents.</a> Emmadi, N.R., C. Bingi, S.S. Kotapalli, R. Ummanni, J.B. Nanubolu, and K. Atmakur. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(15): p. 2918-2922. PMID[26048808].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26183541">Ring-substituted 8-Hydroxyquinoline-2-carboxanilides as Potential Antimycobacterial Agents.</a> Kos, J., I. Zadrazilova, E. Nevin, M. Soral, T. Gonec, P. Kollar, M. Oravec, A. Coffey, J. O&#39;Mahony, T. Liptaj, K. Kralova, and J. Jampilek. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 23(15): p. 4188-4196. PMID[26183541].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26009020">Chemical and Biological Metal Nanoparticles as Antimycobacterial Agents: A Comparative Study.</a> Singh, R., L.U. Nawale, M. Arkile, U.U. Shedbalkar, S.A. Wadhwani, D. Sarkar, and B.A. Chopade. International Journal of Antimicrobial Agents, 2015. 46(2): p. 183-188. PMID[26009020].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26022960">Evaluation of Macrolides for Possible Use against Multidrug-resistant Mycobacterium tuberculosis.</a> van der Paardt, A.F., B. Wilffert, O.W. Akkerman, W.C. de Lange, D. van Soolingen, B. Sinha, T.S. van der Werf, J.G. Kosterink, and J.W. Alffenaar. European Respiratory Journal, 2015. 46(2): p. 444-455. PMID[26022960].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0731-081315.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000357468200016">Discovery of Rimonabant and its Potential Analogues as anti-TB Drug Candidates.</a> Gajbhiye, J.M., N.A. More, M.D. Patil, R. Ummanni, S.S. Kotapalli, P. Yogeeswari, D. Sriram, and V.H. Masand. Medicinal Chemistry Research, 2015. 24(7): p. 2960-2971. ISI[000357468200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000357786200012">Alkylamino Derivatives of N-Benzylpyrazine-2-carboxamide: Synthesis and Antimycobacterial Evaluation.</a> Servusova-Vanaskova, B., O. Jandourek, P. Paterova, J. Kordulakova, M. Plevakova, V. Kubicek, R. Kucera, V. Garaj, L. Naesens, J. Kunes, M. Dolezal, and J. Zitko. MedChemComm, 2015. 6(7): p. 1311-1317. ISI[000357786200012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000357762800032">Synthesis, Characterisation and Antitubercular Screening of 5(4H)-Oxazolone Derivatives</a>. Suhasini, K.P., Y. Christopher, S.R.M. Rao, C.P. Kumar, and Y.L.N. Murthy. Journal of the Indian Chemical Society, 2015. 92(6): p. 951-955. ISI[000357762800032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0731-081315.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000357572000033">Whole Cell Screen Based Identification of Spiropiperidines with Potent Antitubercular Properties.</a> Tantry, S.J., G. Degiacomi, S. Sharma, L.K. Jena, A. Narayan, S. Guptha, G. Shanbhag, S. Menasinakai, M. Mallya, D. Awasthy, G. Balakrishnan, P. Kaur, D. Bhattacharjee, C. Narayan, J. Reddy, C.N.N. Kumar, R. Shandil, F. Boldrin, M. Ventura, R. Manganelli, R.C. Hartkoorn, S.T. Cole, M. Panda, S.D. Markad, V. Ramachandran, S.R. Ghorpade, and N. Dinesh. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(16): p. 3234-3245. ISI[000357572000033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0731-081315.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
