

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RLD9+MxSb+C3ojY5Fn+8ERaRgNjjhoaZOJQ+YuM1RXFuD+1bTzrGH5BkfAcfdrX/ERnCyTwwwyGZtt3WQuOvBZvjkhsadH5fF7I+umANefA4/RcJSQYRMBJbQGs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="073A7151" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: September, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27581223">Identification of KasA as the Cellular Target of an Anti-tubercular Scaffold.</a> Abrahams, K.A., C.W. Chung, S. Ghidelli-Disse, J. Rullas, M.J. Rebollo-Lopez, S.S. Gurcha, J.A. Cox, A. Mendoza, E. Jimenez-Navarro, M.S. Martinez-Martinez, M. Neu, A. Shillings, P. Homes, A. Argyrou, R. Casanueva, N.J. Loman, P.J. Moynihan, J. Lelievre, C. Selenski, M. Axtman, L. Kremer, M. Bantscheff, I. Angulo-Barturen, M.C. Izquierdo, N.C. Cammack, G. Drewes, L. Ballell, D. Barros, G.S. Besra, and R.H. Bates. Nature Communications, 2016. 7(12581): 13pp. PMID[27581223]. PMCID[PMC5025758].<b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27236064">Anti-mycobacterial Activity of Thymine Derivatives Bearing Boron Clusters.</a> Adamska, A., A. Rumijowska-Galewicz, A. Ruszczynska, M. Studzinska, A. Jabionska, E. Paradowska, E. Bulska, H. Munier-Lehmann, J. Dziadek, Z.J. Lesnikowski, and A.B. Olejniczak. European Journal of Medicinal Chemistry, 2016. 121: p. 71-81. PMID[27236064]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000382295100013">Synthesis, Antimicrobial and Antitubercular Activities of Some Novel Pyrazoline Derivatives.</a> Ahmad, A., A. Husain, S.A. Khan, M. Mujeeb, and A. Bhandari. Journal of Saudi Chemical Society, 2016. 20(5): p. 577-584. ISI[000382295100013]. <b><br />
    [WOS]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27588436">Total Synthesis of (+/-)-Isoperbergins and Correction of the Chemical Structure of Perbergin.</a> Almabruk, K.H., J.H. Chang, and T. Mahmud. Journal of Natural Products, 2016. 79(9): p. 2391-2396. PMID[27588436]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000382199000021">Mono- and Polynuclear Ferrocenylthiosemicarbazones: Synthesis, Characterisation and Antimicrobial Evaluation.</a> Baartzes, N., T. Stringer, J. Okombo, R. Seldon, D.F. Warner, C. de Kock, P.J. Smith, and G.S. Smith. Journal of Organometallic Chemistry, 2016. 819: p. 166-172. ISI[000382199000021]. <b><br />
    [WOS]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27287372">Spirooxindoles as Novel 3D-Fragment Scaffolds: Synthesis and Screening against CYP121 from M. tuberculosis.</a> Davis, H.J., M.E. Kavanagh, T. Balan, C. Abell, and A.G. Coyne. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3735-3740. PMID[27287372 ]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27240272">Discovery of in Vitro Antitubercular Agents through in Silico Ligand-based Approaches.</a> De Vita, D., F. Pandolfi, R. Cirilli, L. Scipione, R. Di Santo, L. Friggeri, M. Mori, D. Fiorucci, G. Maccari, R.S.A. Christopher, C. Zamperini, V. Pau, A. De Logu, S. Tortorella, and M. Botta. European Journal of Medicinal Chemistry, 2016. 121: p. 169-180. PMID[27240272]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27553416">Micrococcin P1 - a Bactericidal Thiopeptide Active against Mycobacterium tuberculosis.</a> Degiacomi, G., Y. Personne, G. Mondesert, X. Ge, C.S. Mandava, R.C. Hartkoorn, F. Boldrin, P. Goel, K. Peisker, A. Benjak, M.B. Barrio, M. Ventura, A.C. Brown, V. Leblanc, A. Bauer, S. Sanyal, S.T. Cole, S. Lagrange, T. Parish, and R. Manganelli. Tuberculosis, 2016. 100: p. 95-101. PMID[27553416]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27617988">Synthesis and Antimicrobial Evaluation of 1-[(2-Substituted phenyl)carbamoyl]naphthalen-2-yl Carbamates.</a> Gonec, T., S. Pospisilova, L. Holanova, J. Stranik, A. Cernikova, V. Pudelkova, J. Kos, M. Oravec, P. Kollar, A. Cizek, and J. Jampilek. Molecules, 2016. 21(9): 1189. PMID[27617988]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27387357">Antibacterial and Anticancer Activity of a Series of Novel Peptides Incorporating Cyclic Tetra-substituted C(alpha) Amino acids.</a> Hicks, R.P. Bioorganic Medicinal Chemistry, 2016. 24(18): p. 4056-4065. PMID[27387357]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27529822">Asymmetric Total Synthesis of (-)-Erogorgiaene and Its C-11 Epimer and Investigation of Their Antimycobacterial Activity.</a> Incerti-Pradillos, C.A., M.A. Kabeshov, P.S. O&#39;Hora, S.A. Shipilovskikh, A.E. Rubtsov, V.A. Drobkova, S.Y. Balandina, and A.V. Malkov. Chemistry, 2016. 22(40): p. 14390-14396. PMID[27529822]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27451857">Development of Potent Chemical Antituberculosis Agents Targeting Mycobacterium tuberculosis Acetohydroxyacid Synthase.</a> Jung, I.P., N.R. Ha, S.C. Lee, S.W. Ryoo, and M.Y. Yoon. International Journal of Antimicrobial Agents, 2016. 48(3): p. 247-258. PMID[27451857]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27432475">Substrate Fragmentation for the Design of M. tuberculosis CYP121 Inhibitors.</a> Kavanagh, M.E., J.L. Gray, S.H. Gilbert, A.G. Coyne, K.J. McLean, H.J. Davis, A.W. Munro, and C. Abell. ChemMedChem, 2016. 11(17): p. 1924-1935. PMID[27432475]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27297488">Therapeutic Potential of the Mycobacterium tuberculosis Mycolic acid Transporter, MmpL3.</a> Li, W., A. Obregon-Henao, J.B. Wallach, E.J. North, R.E. Lee, M. Gonzalez-Juarrero, D. Schnappinger, and M. Jackson. Antimicrobial Agents and Chemotherapy, 2016. 60(9): p. 5198-5207. PMID[27297488]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27681720">The Structure-antimicrobial Activity Relationships of a Promising Class of the Compounds Containing the N-Arylpiperazine Scaffold.</a> Malik, I., J. Csollei, J. Jampilek, L. Stanzel, I. Zadrazilova, J. Hosek, S. Pospisilova, A. Cizek, A. Coffey, and J. O&#39;Mahony. Molecules, 2016. 21(10): 1274. PMID[27681720]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27639973">Isolation of alpha-Linolenic acid from Sutherlandia frutescens and Its Inhibition of Mycobacterium tuberculosis&#39; Shikimate Kinase Enzyme.</a> Masoko, P., I.H. Mabusa, and R.L. Howard. BMC Complementary Alternative Medicine, 2016. 16(366): 8pp. PMID[27639973]. PMCID[PMC5027073].<b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">17. <span class="memofmt2-1">Arrival of Imidazo[2,1</span><span class="memofmt2-1">&#8209;</span><span class="memofmt2-1">b]thiazole-5-carboxamides: Potent Antituberculosis Agents That Target QcrB.</span>Moraski, G.C., N. Seeger, P.A. Miller, A.G. Oliver, H.I. Boshoff, S. Cho, S. Mulugeta, J.R. Anderson, S.G. Franzblau, and M.J. Miller. ACS Infectious Diseases, 2016. 2(6): p. 393-398.</p>

    <p class="plaintext">TB_09_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27503657">In Vitro Activity of 3-Triazeneindoles against Mycobacterium tuberculosis and Mycobacterium avium.</a> Nikonenko, B.V., A. Kornienko, K. Majorov, P. Ivanov, T. Kondratieva, M. Korotetskaya, A.S. Apt, E. Salina, and V. Velezheva. Antimicrobial Agents and Chemotherapy, 2016. 60(10): p. 6422-6424. PMID[27503657]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27510258">Isoniazid Minimal Inhibitory Concentrations of Tuberculosis Strains with katG Mutation.</a> Otto-Knapp, R., S. Vesenbeckh, N. Schonfeld, G. Bettermann, A. Roth, T. Bauer, H. Russmann, and H. Mauch. International Journal of Tuberculosis and Lung Disease, 2016. 20(9): p. 1275-1276. PMID[27510258]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27503647">Discovery of Novel Oral Protein Synthesis Inhibitors of Mycobacterium tuberculosis That Target Leucyl-tRNA Synthetase.</a> Palencia, A., X. Li, W. Bu, W. Choi, C.Z. Ding, E.E. Easom, L. Feng, V. Hernandez, P. Houston, L. Liu, M. Meewan, M. Mohan, F.L. Rock, H. Sexton, S. Zhang, Y. Zhou, B. Wan, Y. Wang, S.G. Franzblau, L. Woolhiser, V. Gruppo, A.J. Lenaerts, T. O&#39;Malley, T. Parish, C.B. Cooper, M.G. Waters, Z. Ma, T.R. Ioerger, J.C. Sacchettini, J. Rullas, I. Angulo-Barturen, E. Perez-Herran, A. Mendoza, D. Barros, S. Cusack, J.J. Plattner, and M.R. Alley. Antimicrobial Agents Chemotherapy, 2016. 60(10): p. 6271-6280. PMID[27503647]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27503643">Effects of Lipid-lowering Drugs on Vancomycin Susceptibility of Mycobacteria.</a> Rens, C., F. Laval, M. Daffe, O. Denis, R. Frita, A. Baulard, R. Wattiez, P. Lefevre, and V. Fontaine. Antimicrobial Agents and Chemotherapy, 2016. 60(10): p. 6193-6199. PMID[27503643]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27477207">Design and Development of New Class of Mycobacterium tuberculosisl-alanine Dehydrogenase Inhibitors.</a> Reshma, R.S., S. Saxena, K.A. Bobesh, V.U. Jeankumar, S. Gunda, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2016. 24(18): p. 4499-4508. PMID[27477207]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27476139">A Convenient Synthesis and Screening of Benzosuberone Bearing 1,2,3-Triazoles against Mycobacterium tuberculosis.</a> Sajja, Y., S. Vanguru, L. Jilla, H.R. Vulupala, R. Bantu, P. Yogeswari, D. Sriram, and L. Nagarapu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(17): p. 4292-4295. PMID[27476139]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000381709100007">Synthesis and in Vitro Study of Some Fused 1,2,4-Triazole Derivatives as Antimycobacterial Agents.</a> Seelam, N., S.P. Shrivastava, S. Prasanthi, and S. Gupta. Journal of Saudi Chemical Society, 2016. 20(4): p. 411-418. ISI[000381709100007]. <b><br />
    [WOS]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27601302">Bis-Biguanide dihydrochloride Inhibits Intracellular Replication of M. tuberculosis and Controls Infection in Mice.</a> Shen, H., F. Wang, G. Zeng, L. Shen, H. Cheng, D. Huang, R. Wang, L. Rong, and Z.W. Chen. Scientific Reports, 2016. 6(32725): 10pp. PMID[27601302]. PMCID[PMC5013693].<b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27235189">Antimicrobial Peptides as Novel Anti-tuberculosis Therapeutics.</a> Silva, J.P., R. Appelberg, and F.M. Gama. Biotechnology Advances, 2016. 34(5): p. 924-940. PMID[27235189]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27469982">Bortezomib Inhibits Bacterial and Fungal Beta-carbonic Anhydrases.</a> Supuran, C.T. Bioorganic &amp; Medicinal Chemistry, 2016. 24(18): p. 4406-4409. PMID[27469982]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27618117">Inhibition of Bacterial RNase P RNA by Phenothiazine Derivatives.</a> Wu, S., G. Mao, and L.A. Kirsebom. Biomolecules, 2016. 6(38): 16pp. PMID[27618117]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27297390">A Subset of Protective gamma9delta2 T Cells Is Activated by Novel Mycobacterial Glycolipid Components.</a> Xia, M., D.C. Hesser, P. De, I.G. Sakala, C.T. Spencer, J.S. Kirkwood, G. Abate, D. Chatterjee, K.M. Dobos, and D.F. Hoft. Infection and Immunity, 2016. 84(9): p. 2449-2462. PMID[27297390]. PMCID[PMC4995917].<b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27681932">DC-159a Shows Inhibitory Activity against DNA Gyrases of Mycobacterium leprae.</a> Yamaguchi, T., K. Yokoyama, C. Nakajima, and Y. Suzuki. Plos Neglected Tropical Diseases, 2016. 10(9): p. e0005013. PMID[27681932]. <b><br />
    [PubMed]</b>. TB_09_2016.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27600955">In Vitro Antimicrobial Activities of Animal-used Quinoxaline 1,4-di-N-Oxides against Mycobacteria, Mycoplasma and Fungi.</a> Zhao, Y., G. Cheng, H. Hao, Y. Pan, Z. Liu, M. Dai, and Z. Yuan. BMC Veterinary Research, 2016. 12(186): 13pp. PMID[27600955]. PMCID[PMC5011961].<b><br />
    [PubMed]</b>. TB_09_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
