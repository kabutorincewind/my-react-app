

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TBDjW0OoGuLDAQPQ+8SXJmArE63fCk+RIqhXDIVqjR8nOCWO3wThbhei7MD9ve78uZYIikK9Yuy4DfgE/xI4HcMWeHUrk1C9gE6dwxLEAric6gKalywFxHLHC6g=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="893F35F7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: March, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000370875200012">Biochemical Potential of alpha-L-Arabinofuranosidase as Anti-tuberculosis Candidate</a>. Asmarani, O., M.Z. Fanani, and N.N.T. Puspaningsih. Procedia Chemistry, 2016. 18: p. 82-89. ISI[000370875200012].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26907951">Design and Synthesis of 1-((1,5-bis(4-Chlorophenyl)-2-methyl-1H-pyrrol-3-yl)methyl)-4-methylpiperazine (BM212) and N-Adamantan-2-yl-N&#39;-((E)-3,7-dimethylocta-2,6-dienyl)ethane-1,2-diamine (SQ109) pyrrole Hybrid Derivatives: Discovery of Potent Antitubercular Agents Effective against Multidrug-resistant Mycobacteria.</a> Bhakta, S., N. Scalacci, A. Maitra, A.K. Brown, S. Dasugari, D. Evangelopoulos, T.D. McHugh, P.N. Mortazavi, A. Twist, E. Petricci, F. Manetti, and D. Castagnolo. Journal of Medicinal Chemistry, 2016. 59(6): p. 2780-2793. PMID[26907951].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26930596">Antimycobacterial Activity of a New Peptide Polydim-I Isolated from Neotropical Social Wasp Polybia dimorpha.</a> das Neves, R.C., M.M. Trentini, E.S.J. de Castro, K.S. Simon, A.L. Bocca, L.P. Silva, M.R. Mortari, A. Kipnis, and A.P. Junqueira-Kipnis. Plos One, 2016. 11(3): e0149729. PMID[26930596]. PMCID[PMC4773228].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26875934">Synthesis and Pharmacological Evaluation of Nucleoside Prodrugs Designed to Target Siderophore Biosynthesis in Mycobacterium tuberculosis.</a> Dawadi, S., S. Kawamura, A. Rubenstein, R. Remmel, and C.C. Aldrich. Bioorganic &amp; Medicinal Chemistry, 2016. 24(6): p. 1314-1321. PMID[26875934]. PMCID[PMC4769951].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26920799">Synthesis, Biological Evaluation and Molecular Docking Study of Some Novel Indole and Pyridine Based 1,3,4-Oxadiazole Derivatives as Potential Antitubercular Agents.</a> Desai, N.C., H. Somani, A. Trivedi, K. Bhatt, L. Nawale, V.M. Khedkar, P.C. Jha, and D. Sarkar. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(7): p. 1776-1783. PMID[26920799].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000371304100002">Synthesis, Molecular Docking and Antimicrobial Evaluation of Novel Benzoxazole Derivatives.</a> Ertan-Bolelli, T., I. Yildiz, and S. Ozgen-Ozgacar. Medicinal Chemistry Research, 2016. 25(4): p. 553-567. ISI[000371304100002].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26988804">Investigation of 4-Amino-5-alkynylpyrimidine-2(1H)-ones as Anti-mycobacterial Agents.</a> Garg, G., M. Pande, A. Agrawal, J. Li, and R. Kumar. Bioorganic &amp; Medicinal Chemistry, 2016. 24(8): p. 1771-1777. PMID[26988804].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26822568">Discovery of Potent Anti-tuberculosis Agents Targeting Leucyl-tRNA Synthetase.</a> Gudzera, O.I., A.G. Golub, V.G. Bdzhola, G.P. Volynets, S.S. Lukashov, O.P. Kovalenko, I.A. Kriklivyi, A.D. Yaremchuk, S.A. Starosyla, S.M. Yarmoluk, and M.A. Tukalo. Bioorganic &amp; Medicinal Chemistry, 2016. 24(5): p. 1023-1031. PMID[26822568].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26805396">Structure-guided Discovery of Antitubercular Agents That Target the Gyrase ATPase Domain.</a> Jeankumar, V.U., S. Saxena, R. Vats, R.S. Reshma, R. Janupally, P. Kulkarni, P. Yogeeswari, and D. Sriram. ChemMedChem, 2016. 11(5): p. 539-548. PMID[26805396].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000370777800001">Synthesis and In Vitro Evaluation of New Thiosemicarbazone Derivatives as Potential Antimicrobial Agents.</a> Kaplancikli, Z.A., M.D. Altintop, B. Sever, Z. Canturk, and A. Ozdemir. Journal of Chemistry, 2016. 2016. ISI[000370777800001].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26948407">Development of 3,5-Dinitrobenzylsulfanyl-1,3,4-oxadiazoles and Thiadiazoles as Selective Antitubercular Agents Active against Replicating and Nonreplicating Mycobacterium tuberculosis.</a> Karabanovich, G., J. Zemanova, T. Smutny, R. Szekely, M. Sarkan, I. Centarova, A. Vocat, I. Pavkova, P. Conka, J. Nemecek, J. Stolarikova, M. Vejsova, K. Vavrova, V. Klimesova, A. Hrabalek, P. Pavek, S.T. Cole, K. Mikusova, and J. Roh. Journal of Medicinal Chemistry, 2016. 59(6): p. 2362-2380. PMID[26948407].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26586403">Anti-tuberculosis Lead Molecules from Natural Products Targeting Mycobacterium tuberculosis Clpc1.</a> Lee, H. and J.W. Suh. Journal of Industrial Microbiology &amp; Biotechnology, 2016. 43(2-3): p. 205-212. PMID[26586403].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26464012">Antimycobacterial Activity of Five Efflux Pump Inhibitors against Mycobacterium tuberculosis Clinical Isolates.</a> Li, G., J. Zhang, C. Li, Q. Guo, Y. Jiang, J. Wei, Y. Qiu, X. Zhao, L.L. Zhao, J. Lu, and K. Wan. The Journal of Antibiotics, 2016. 69(3): p. 173-175. PMID[26464012].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26980494">Identification of a Novel Inhibitor of Isocitrate Lyase as a Potent Antitubercular Agent against Both Active and Non-replicating Mycobacterium tuberculosis.</a> Liu, Y., S. Zhou, Q. Deng, X. Li, J. Meng, Y. Guan, C. Li, and C. Xiao. Tuberculosis, 2016. 97: p. 38-46. PMID[26980494].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26918106">Syntheses and Biological Evaluations of Highly Functionalized Hydroxamate Containing and N-Methylthio monobactams as Anti-tuberculosis and beta-Lactamase Inhibitory Agents.</a> Majewski, M.W., K.D. Watson, S.Y. Cho, P.A. Miller, S.G. Franzblau, and M.J. Miller. MedChemComm, 2016. 7(1): p. 141-147. PMID[26918106]. PMCID[PMC4762374].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26941723">Boromycin Kills Mycobacterial Persisters without Detectable Resistance.</a> Moreira, W., D.B. Aziz, and T. Dick. Frontiers in Microbiology, 2016. 7(199): 7pp. PMID[26941723]. PMCID[PMC4761863].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26985307">2-(Quinolin-4-yloxy)acetamides are Active against Drug-Susceptible and Drug-resistant Mycobacterium tuberculosis Strains.</a> Pissinate, K., A.D. Villela, V. Rodrigues-Junior, B.C. Giacobbo, E.S. Grams, B.L. Abbadi, R.V. Trindade, L. Roesler Nery, C.D. Bonan, D.F. Back, M.M. Campos, L.A. Basso, D.S. Santos, and P. Machado. ACS Medicinal Chemistry Letters, 2016. 7(3): p. 235-239. PMID[26985307]. PMCID[PMC4789679].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000370693900016">Ionic Liquid-promoted One-pot Synthesis of Thiazole-imidazo[2,1-b][1,3,4]thiadiazole Hybrids and Their Antitubercular Activity.</a> Ramprasad, J., N. Nayak, U. Dalimba, P. Yogeeswari, and D. Sriram. MedChemComm, 2016. 7(2): p. 338-344. ISI[000370693900016].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000371304100018">Synthesis, Biological Evaluation and Molecular Docking of Novel Coumarin Incorporated Triazoles as Antitubercular, Antioxidant and Antimicrobial Agents.</a> Shaikh, M.H., D.D. Subhedar, B.B. Shingate, F.A.K. Khan, J.N. Sangshetti, V.M. Khedkar, L. Nawale, D. Sarkar, G.R. Navale, and S.S. Shinde. Medicinal Chemistry Research, 2016. 25(4): p. 790-804. ISI[000371304100018].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26502828">Design, Synthesis, and Antimycobacterial Activity of Novel Theophylline-7-acetic acid Derivatives with Amino acid Moieties.</a> Stavrakov, G., V. Valcheva, Y. Voynikov, I. Philipova, M. Atanasova, S. Konstantinov, P. Peikov, and I. Doytchinova. Chemical Biology &amp; Drug Design, 2016. 87(3): p. 335-341. PMID[26502828].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26927426">Facile Synthesis of 1,3-Thiazolidin-4-ones as Antitubercular Agents.</a> Subhedar, D.D., M.H. Shaikh, M.A. Arkile, A. Yeware, D. Sarkar, and B.B. Shingate. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(7): p. 1704-1708. PMID[26927426].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26750606">Synthesis and Anti-mycobacterial Activity of Glycosyl sulfamides of Arabinofuranose.</a> Suthagar, K. and A.J. Fairbanks. Organic &amp; Biomolecular Chemistry, 2016. 14(5): p. 1748-1754. PMID[26750606].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26985313">Design, Syntheses, and Anti-Tb Activity of 1,3-Benzothiazinone azide and Click Chemistry Products Inspired by BTZ043.</a> Tiwari, R., P.A. Miller, L.R. Chiarelli, G. Mori, M. Sarkan, I. Centarova, S. Cho, K. Mikusova, S.G. Franzblau, A.G. Oliver, and M.J. Miller. ACS Medicinal Chemistry Letters, 2016. 7(3): p. 266-270. PMID[26985313]. PMCID[PMC4789662].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27013803">Nitric Oxide Production Inhibition and Anti-mycobacterial Activity of Extracts and Halogenated Sesquiterpenes from the Brazilian Red Alga Laurencia dendroidea J. Agardh.</a> Ventura, T.L.B., F.L.D. Machado, M.H. de Araujo, L.M.D. Gestinari, C.R. Kaiser, F.D. Esteves, E.B. Lasunskaia, A.R. Soares, and M.F. Muzitano. Pharmacognosy Magazine, 2015. 11(44): p. S611-S618. PMID[27013803]. PMCID[PMC4787097].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_03_2016.</p>

    <h2>Patents</h2>

    <p class="plaintext">25.   <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160324&amp;CC=WO&amp;NR=2016041972A1&amp;KC=A1">Benzothiophene, Benzyloxybenzylidene and Indoline Derivatives Useful for the Treatment of Tuberculosis.</a> Rybniker, J., S. Cole, G. Keri, L. Orfi, J. Pato, I. Szabadkai, P. Banhegyi, Z. Greff, and P. Marko. Patent. 2016. 2015-EP71110 2016041972: 103pp.</p>

    <p class="plaintext"><b>[Patent].</b> TB_03_2016.</p>

    <br />

    <p class="plaintext">26.   <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160310&amp;CC=WO&amp;NR=2016037072A2&amp;KC=A2">Preparation of Fragments of Cyclic Acyldepsipeptide Enopeptin Analogs, as Potentiators of Bactericides, Clpp Activators and Antibacterials.</a> Sello, J.K. and D.W. Carney. Patent. 2016. 2015-US48568 2016037072: 202pp.</p>

    <p class="plaintext"><b>[Patent].</b> TB_03_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160224&amp;CC=CZ&amp;NR=305738B6&amp;KC=B6">Substituted Derivative of Phosphorus Oxyacids, Use Thereof and Pharmaceutical Composition Containing It.</a> Vinsova, J., M. Kratky, and G. Paraskevopoulos. Patent. 2016. 2014-915 305738: 29pp.</p>

    <p class="plaintext"> <b>[Patent].</b> TB_03_2016.</p>
    
    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
