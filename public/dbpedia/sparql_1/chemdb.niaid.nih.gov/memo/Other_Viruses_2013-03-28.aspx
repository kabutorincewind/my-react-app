

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-03-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1cYyHkOaxKflQxsHSnT0Z1N9BtImx/s8YYom39Ft9SJrPhv9afyjSpd49RS628K80zogm5PaEVRt5zUg3dtFRKtwOT45Ri60hLY7P8eCqytgOeXMb4VuWWi8tMw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9584FE36" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 15 - March 28, 2013</h1>

    <h2>HCMV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23485093">Cytomegalovirus Protease Targeted Prodrug Development.</a> Sabit, H., A. Dahan, J. Sun, C.J. Provoda, K.D. Lee, J.H. Hilfinger, and G.L. Amidon. Molecular Pharmaceutics, 2013.  <b>[Epub ahead of print]</b>; PMID[23485093].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23529728">Phenothiazines Inhibit HCV Entry Likely by Increasing the Fluidity of Cholesterol-rich Membranes.</a> Chamoun-Emanuelli, A.M., E.I. Pecheur, R.L. Simeon, D. Huang, P.S. Cremer, and Z. Chen. Antimicrobial Agents and Chemotherapy, 2013.  <b>[Epub ahead of print]</b>; PMID[23529728].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23512276">A Novel Strategy to Inhibit the Reproduction and Translation of Hepatitis C Virus.</a> Duan, A., L. Ning, C. Li, Y. Hou, N. Yang, L. Sun, and G. Li. Science China. Life Sciences, 2013.  <b>[Epub ahead of print]</b>; PMID[23512276].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23519201">Synthesis and Characterization of Celecoxib Derivatives as Possible Anti-inflammatory, Analgesic, Antioxidant, Anticancer and anti-HCV Agents.</a> Kucukguzel, S.G., I. Coskun, S. Aydin, G. Aktay, S. Gursoy, O. Cevik, O.B. Ozakpinar, D. Ozsavci, A. Sener, N. Kaushik-Basu, A. Basu, and T.T. Talele. Molecules, 2013. 18(3): p. 3595-3614; PMID[23519201].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.  </p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23506530">The Discovery of Hepatitis C Virus NS3-4A Protease Inhibitors with Improved Barrier to Resistance and Favorable Liver Distribution.</a> Moreau, B., J.A. O&#39;Meara, J. Bordeleau, M. Garneau, C. Godbout, V. Gorys, M. Leblanc, E. Villemure, P.W. White, and M. Llinas-Brunet. Journal of Medicinal Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23506530].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23521684">Acridones as Antiviral Agents: Synthesis, Chemical and Biological Properties.</a> Sepulveda, C.S., M.L. Fascio, C.C. Garcia, D.A. N, and E.B. Damonte. Current Medicinal Chemistry, 2013.  <b>[Epub ahead of print]</b>; PMID[23521684].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.  </p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23512654">Synthesis and Hepatitis C Antiviral Activity of 1-Aminobenzyl-1H-indazole-3-carboxamide Analogues.</a> Shi, J.J., F.H. Ji, P.L. He, Y.X. Yang, W. Tang, J.P. Zuo, and Y.C. Li. ChemMedChem, 2013.  <b>[Epub ahead of print]</b>; PMID[23512654].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p>

    <h2> </h2>

    <h2>HSV-1</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23507287">In Vivo anti-HSV Activity of a Sulfated Derivative of Agaricus brasiliensis Mycelial Polysaccharide.</a> Cardozo, F.T., I.V. Larsen, E.V. Carballo, G. Jose, R.A. Stern, R.C. Brummel, C.M. Camelini, M.J. Rossi, C.M. Simoes, and C.R. Brandt. Antimicrobial Agents and Chemotherapy, 2013.  <b>[Epub ahead of print]</b>; PMID[23507287].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0315-032813.</p> 

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000314707700021">Monoclonal Antibodies against Hepatitis C Genotype 3A Virus Like Particle Inhibit Virus Entry in Cell Culture System</a><i>.</i> Das, S., R.K. Shetty, A. Kumar, R.N. Shridharan, R. Tatineni, G. Chi, A. Mukherjee, S. Das, S.M. Subbarao, and A.A. Karande. Plos One, 2013. 8(1); ISI[000314707700021].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0315-032813.  </p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
