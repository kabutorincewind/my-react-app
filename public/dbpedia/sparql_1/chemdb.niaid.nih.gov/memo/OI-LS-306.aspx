

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-306.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fm7ViIDfkyLVREHDX82HlZ6dpddXzeTVkmtT8Oykf1R0pmXN5GJH3kHX/L8BmoqKFm5+Psm6qXaOHvd5rOykXfESV1o7YreDTe5FNRVzvIBRHvvUNHAmu9n4i20=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CB0127B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-306-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44138   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Endosomal membrane traffic: convergence point targeted by Mycobacterium tuberculosis and HIV</p>

    <p>          Deretic, V, Vergne, I, Chua, J, Master, S, Singh, SB, Fazio, JA, and Kyei, G</p>

    <p>          Cell Microbiol  <b>2004</b>.  6(11): 999-1009</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469429&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469429&amp;dopt=abstract</a> </p><br />

    <p>2.         44139   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Screening of antimicrobial activity of diarylamines in the 2,3,5-trimethylbenzo[b]thiophene series: a structure-activity evaluation study</p>

    <p>          Ferreira, Isabel CFR, Calhelha, Ricardo C, Estevinho, Leticia M, and Queiroz, Maria-Joao RP</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DGD8GS-9/2/92ac687e8d296ac45ee47a61bcb9736d">http://www.sciencedirect.com/science/article/B6TF9-4DGD8GS-9/2/92ac687e8d296ac45ee47a61bcb9736d</a> </p><br />

    <p>3.         44140   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Chemotherapeutic efficacy of poly (DL-lactide-co-glycolide) nanoparticle encapsulated antitubercular drugs at sub-therapeutic dose against experimental tuberculosis</p>

    <p>          Sharma, Anjali, Pandey, Rajesh, Sharma, Sadhna, and Khuller, GK</p>

    <p>          International Journal of Antimicrobial Agents <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4DFT4GY-7/2/11dc1bc98fd02902e56066526e3ee8c7">http://www.sciencedirect.com/science/article/B6T7H-4DFT4GY-7/2/11dc1bc98fd02902e56066526e3ee8c7</a> </p><br />

    <p>4.         44141   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus nonstructural protein, helicase activity, and viral replication by a recombinant human antibody clone</p>

    <p>          Prabhu, R, Khalap, N, Burioni, R, Clementi, M, Garry, RF, and Dash, S</p>

    <p>          Am J Pathol <b>2004</b>.  165(4): 1163-73</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15466383&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15466383&amp;dopt=abstract</a> </p><br />

    <p>5.         44142   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Rapid, automated, nonradiometric susceptibility testing of Mycobacterium tuberculosis complex to four first-line antituberculous drugs used in standard short-course chemotherapy</p>

    <p>          Johansen, Isik Somuncu, Thomsen, Vibeke Ostergaard, Marjamaki, Merja, Sosnovskaja, Anaida, and Lundgren, Bettina</p>

    <p>          Diagnostic Microbiology and Infectious Disease <b>2004</b>.  50(2 ): 103-107</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T60-4DH19DJ-4/2/d72c644f0acb269929fe95a7e4be73af">http://www.sciencedirect.com/science/article/B6T60-4DH19DJ-4/2/d72c644f0acb269929fe95a7e4be73af</a> </p><br />
    <br clear="all">

    <p>6.         44143   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Synthesis and anti-tubercular activity of a series of 2-sulfonamido/trifluoromethyl-6-substituted imidazo[2,1-b]-1,3,4-thiadiazole derivatives</p>

    <p>          Gadad, AK, Noolvi, MN, and Karpoormath, RV</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(21): 5651-5659</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465343&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465343&amp;dopt=abstract</a> </p><br />

    <p>7.         44144   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Atovaquone - a novel broad-spectrum anti-infective drug</p>

    <p>          Hudson, AT</p>

    <p>          Parasitol Today <b>1993</b>.  9(2): 66-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15463712&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15463712&amp;dopt=abstract</a> </p><br />

    <p>8.         44145   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          In vitro and in vivo antimycobacterial activity of antiinflammatory drug, diclofenac sodium</p>

    <p>          Dutta, NK, Kumar, KA, Mazumdar, K, and Dastidar, SG</p>

    <p>          Indian J Exp Biol <b>2004</b>.  42(9): 922-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15462188&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15462188&amp;dopt=abstract</a> </p><br />

    <p>9.         44146   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluation of novel non-nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum chemotherapeutic properties</p>

    <p>          Sriram, Dharmarajan, Bal, Tanushree Ratan, and Yogeeswari, Perumal</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DDXKDK-1/2/07d9a5c917b2dc80d3310f3eb81b9255">http://www.sciencedirect.com/science/article/B6TF8-4DDXKDK-1/2/07d9a5c917b2dc80d3310f3eb81b9255</a> </p><br />

    <p>10.       44147   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Opportunistic infections in the age of highly active antiretroviral therapy</p>

    <p>          Currier, JS</p>

    <p>          AIDS Patient Care STDS <b>1998</b>.  12(7): 521-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15462002&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15462002&amp;dopt=abstract</a> </p><br />

    <p>11.       44148   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Carboxylic acid and phosphate ester derivatives of fluconazole: synthesis and antifungal activities</p>

    <p>          Nam, Nguyen-Hai, Sardari, Soroush, Selecky, Meredith, and Parang, Keykavous</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DCD86D-5/2/653824eb740627ea151536c7da2f8eda">http://www.sciencedirect.com/science/article/B6TF8-4DCD86D-5/2/653824eb740627ea151536c7da2f8eda</a> </p><br />

    <p>12.       44149   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Characterization of a new member of the flavoprotein disulfide reductase family of enzymes from mycobacterium tuberculosis</p>

    <p>          Argyrou, A, Vetting, MW, and Blanchard, JS</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456792&amp;dopt=abstract</a> </p><br />

    <p>13.       44150   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Structure-Activity Relationship of Heterobase-Modified 2&#39;-C-Methyl Ribonucleosides as Inhibitors of Hepatitis C Virus RNA Replication</p>

    <p>          Eldrup, AB, Prhavc, M, Brooks, J, Bhat, B, Prakash, TP, Song, Q, Bera, S, Bhat, N, Dande, P, Cook, PD, Bennett, CF, Carroll, SS, Ball, RG, Bosserman, M, Burlein, C, Colwell, LF, Fay, JF, Flores, OA, Getty, K, LaFemina, RL, Leone, J, MacCoss, M, McMasters, DR, Tomassini, JE, Von, Langen D, Wolanski, B, and Olsen, DB</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5284-5297</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456273&amp;dopt=abstract</a> </p><br />

    <p>14.       44151   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Azaanalogues of ebselen as antimicrobial and antiviral agents: synthesis and properties</p>

    <p>          Wojtowicz, H, Kloc, K, Maliszewska, I, Mlochowski, J, Pietka, M, and Piasecki, E</p>

    <p>          Il Farmaco <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VJ8-4D990H2-1/2/eaef8acf1d6416a3eb236e99e69994c3">http://www.sciencedirect.com/science/article/B6VJ8-4D990H2-1/2/eaef8acf1d6416a3eb236e99e69994c3</a> </p><br />

    <p>15.       44152   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Characterization of an antifungal glycolipid secreted by the yeast Sympodiomycopsis paphiopedili</p>

    <p>          Kulakovskaya, Tatiana V, Shashkov, Alexander S, Kulakovskaya, Ekaterina V, and Golubev, Wladyslav I</p>

    <p>          FEMS Yeast Research <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8C-4D8F5D8-1/2/2910127093bc5ee41d6403e52cdd00ca">http://www.sciencedirect.com/science/article/B6W8C-4D8F5D8-1/2/2910127093bc5ee41d6403e52cdd00ca</a> </p><br />

    <p>16.       44153   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Few amino acid positions in rpoB are associated with most of the rifampin resistance in Mycobacterium tuberculosis</p>

    <p>          Cummings, MP and Segal, MR</p>

    <p>          BMC Bioinformatics <b>2004</b>.  5(1): 137</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15453919&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15453919&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.       44154   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Mechanisms of mycobacterial persistence in tuberculosis</p>

    <p>          Kusner, David J</p>

    <p>          Clinical Immunology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WCJ-4D8VGJB-1/2/9747aadcfde0348b5e20f69976e7fa78">http://www.sciencedirect.com/science/article/B6WCJ-4D8VGJB-1/2/9747aadcfde0348b5e20f69976e7fa78</a> </p><br />

    <p>18.       44155   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Prospects for new antitubercular drugs</p>

    <p>          Duncan, K and Barry, CE 3rd</p>

    <p>          Curr Opin Microbiol <b>2004</b>.  7(5): 460-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15451500&amp;dopt=abstract</a> </p><br />

    <p>19.       44156   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Cloning, expression, purification and crystallization of a transcriptional regulatory protein (Rv3291c) from Mycobacterium tuberculosis H37Rv</p>

    <p>          Shrivastava, T, Kumar, S, and Ramachandran, R</p>

    <p>          Acta Crystallogr D Biol Crystallogr <b>2004</b>.  60(Pt 10): 1874-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388937&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388937&amp;dopt=abstract</a> </p><br />

    <p>20.       44157   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          A 7-deaza-adenosine analog is a potent and selective inhibitor of hepatitis C virus replication with excellent pharmacokinetic properties</p>

    <p>          Olsen, DB, Eldrup, AB, Bartholomew, L, Bhat, B, Bosserman, MR, Ceccacci, A, Colwell, LF, Fay, JF, Flores, OA, Getty, KL, Grobler, JA, LaFemina, RL, Markel, EJ, Migliaccio, G, Prhavc, M, Stahlhut, MW, Tomassini, JE, MacCoss, M, Hazuda, DJ, and Carroll, SS </p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3944-53</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388457&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388457&amp;dopt=abstract</a> </p><br />

    <p>21.       44158   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Inhibition of human cytomegalovirus replication by benzimidazole nucleosides involves three distinct mechanisms</p>

    <p>          Evers, DL, Komazin, G, Ptak, RG, Shin, D, Emmer, BT, Townsend, LB, and Drach, JC</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3918-27</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388453&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388453&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.       44159   OI-LS-306; EMBASE-OI-10/9/2004</p>

    <p class="memofmt1-2">          Inhibition of cytokines expression in human microglia infected by virulent and non-virulent mycobacteria</p>

    <p>          Curto, Monica, Reali, Camilla, Palmieri, Giuseppina, Scintu, Franca, Schivo, Maria Laura, Sogos, Valeria, Marcialis, Maria Antonietta, Ennas, Maria Grazia, Schwarz, Herbert, Pozzi, Gianni, and Gremo, Fulvia</p>

    <p>          Neurochemistry International <b>2004</b>.  44(6): 381-392</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T0B-4B3NKPC-1/2/4aea52d6927874218bd35e8a89d6f76a">http://www.sciencedirect.com/science/article/B6T0B-4B3NKPC-1/2/4aea52d6927874218bd35e8a89d6f76a</a> </p><br />

    <p>23.       44160   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Posaconazole is a potent inhibitor of sterol 14alpha-demethylation in yeasts and molds</p>

    <p>          Munayyer, HK, Mann, PA, Chau, AS, Yarosh-Tomaine, T, Greene, JR, Hare, RS, Heimark, L, Palermo, RE, Loebenberg, D, and McNicholas, PM</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3690-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388421&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388421&amp;dopt=abstract</a> </p><br />

    <p>24.       44161   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial activity and molecular modeling studies of halogenated 4-[1H-imidazol-1-yl(phenyl)methyl]-1,5-diphenyl-1H-pyrazoles</p>

    <p>          Menozzi, G, Merello, L, Fossa, P, Schenone, S, Ranise, A, Mosti, L, Bondavalli, F, Loddo, R, Murgioni, C, Mascia, V, La, Colla P, and Tamburini, E</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(20): 5465-83</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388173&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388173&amp;dopt=abstract</a> </p><br />

    <p>25.       44162   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Diaryloxy methano phenanthrenes: a new class of antituberculosis agents</p>

    <p>          Panda, G, Shagufta, Mishra, JK, Chaturvedi, V, Srivastava, AK, Srivastava, R, and Srivastava, BS</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(20): 5269-76</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388155&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388155&amp;dopt=abstract</a> </p><br />

    <p>26.       44163   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Modeling epidemics of multidrug-resistant M. tuberculosis of heterogeneous fitness</p>

    <p>          Cohen, T and Murray, M</p>

    <p>          Nat Med <b>2004</b>.  10(10): 1117-21</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15378056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15378056&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>27.       44164   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Optimizing electroporation conditions for intracellular delivery of morpholino antisense oligonucleotides directed against the hepatitis C virus internal ribosome entry site</p>

    <p>          Jubin, R</p>

    <p>          Methods Mol Med <b>2004</b>.  106: 309-22</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15375324&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15375324&amp;dopt=abstract</a> </p><br />

    <p>28.       44165   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Antivirals and antiviral strategies</p>

    <p>          De Clercq, E</p>

    <p>          Nat Rev Microbiol <b>2004</b>.  2(9): 704-720</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15372081&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15372081&amp;dopt=abstract</a> </p><br />

    <p>29.       44166   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          N- t-Boc-amino acid esters of isomannide Potential inhibitors of serine proteases</p>

    <p>          Muri, EM, Gomes, M Jr, Costa, JS, Alencar, FL, Sales, A Jr, Bastos, ML, Hernandez-Valdes, R, Albuquerque, MG, Da, Cunha EF, Alencastro, RB, Williamson, JS, and Antunes, OA</p>

    <p>          Amino Acids <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365909&amp;dopt=abstract</a> </p><br />

    <p>30.       44167   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Rapid detection of isoniazid and rifampin resistance mutations in Mycobacterium tuberculosis complex from cultures or smear-positive sputa by use of molecular beacons</p>

    <p>          Lin, SY, Probert, W, Lo, M, and Desmond, E</p>

    <p>          J Clin Microbiol <b>2004</b>.  42(9): 4204-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365012&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15365012&amp;dopt=abstract</a> </p><br />

    <p>31.       44168   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Detection of hepatitis C virus by a user-developed reverse transcriptase-PCR and use of amplification products for subsequent genotyping</p>

    <p>          Tang, YW, Li, H, Roberto, A, Warner, D, and Yen-Lieberman, B</p>

    <p>          J Clin Virol <b>2004</b>.  31(2): 148-52</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15364272&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15364272&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>32.       44169   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Involvement of p38 signaling pathway in interferon-alpha-mediated antiviral activity toward hepatitis C virus</p>

    <p>          Ishida, H, Ohkawa, K, Hosui, A, Hiramatsu, N, Kanto, T, Ueda, K, Takehara, T, and Hayashi, N</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  321(3): 722-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358166&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15358166&amp;dopt=abstract</a> </p><br />

    <p>33.       44170   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Practical synthesis of a potent hepatitis C virus RNA replication inhibitor</p>

    <p>          Bio, MM, Xu, F, Waters, M, Williams, JM, Savary, KA, Cowden, CJ, Yang, C, Buck, E, Song, ZJ, Tschaen, DM, Volante, RP, Reamer, RA, and Grabowski, EJ</p>

    <p>          J Org Chem <b>2004</b>.  69(19): 6257-66</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357584&amp;dopt=abstract</a> </p><br />

    <p>34.       44171   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Effect of hydrogen peroxide and other protease inhibitors on Cryptosporidium parvum excystation and in vitro development</p>

    <p>          Kniel, KE, Sumner, SS, Pierson, MD, Zajac, AM, Hackney, CR, Fayer, R, and Lindsay, DS</p>

    <p>          J Parasitol <b>2004</b>.  90(4): 885-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15357093&amp;dopt=abstract</a> </p><br />

    <p>35.       44172   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Dormancy phenotype displayed by extracellular Mycobacterium tuberculosis within artificial granulomas in mice</p>

    <p>          Karakousis, PC, Yoshimatsu, T, Lamichhane, G, Woolwine, SC, Nuermberger, EL, Grosset, J, and Bishai, WR</p>

    <p>          J Exp Med <b>2004</b>.  200(5): 647-57</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15353557&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15353557&amp;dopt=abstract</a> </p><br />

    <p>36.       44173   OI-LS-306; PUBMED-OI-10/8/2004</p>

    <p class="memofmt1-2">          Metal ion transport and regulation in mycobacterium tuberculosis</p>

    <p>          Agranoff, D and Krishna, S</p>

    <p>          Front Biosci <b>2004</b>.  9: 2996-3006</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15353332&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15353332&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>37.       44174   OI-LS-306; WOS-OI-9/26/2004</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis reprograms waves of phosphatidylinositol 3-phosphate on phagosomal organelles</p>

    <p>          Chua, J and Deretic, V</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2004</b>.  279(35): 36982-36992, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223453600101">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223453600101</a> </p><br />

    <p>38.       44175   OI-LS-306; WOS-OI-9/26/2004</p>

    <p class="memofmt1-2">          Tools for target identification and validation</p>

    <p>          Wang, SL, Sim, TB, and Chang, YT</p>

    <p>          CURRENT OPINION IN CHEMICAL BIOLOGY <b>2004</b>.  8(4): 371-377, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223504000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223504000005</a> </p><br />

    <p>39.       44176   OI-LS-306; WOS-OI-9/26/2004</p>

    <p class="memofmt1-2">          Nitric oxide synthase stimulates prostaglandin synthesis and barrier function in C-parvum-infected porcine ileum</p>

    <p>          Gookin, JL, Duckett, LL, Armstrong, MU, Stauffer, SH, Finnegan, CP, Murtaugh, MP, and Argenzio, RA</p>

    <p>          AMERICAN JOURNAL OF PHYSIOLOGY-GASTROINTESTINAL AND LIVER PHYSIOLOGY <b>2004</b>.  287(3): G571-G581, 11 </p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223446400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223446400011</a> </p><br />

    <p>40.       44177   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Seeing is believing: the impact of structural genomics on antimicrobial drug discovery</p>

    <p>          Schmid, MB</p>

    <p>          NATURE REVIEWS MICROBIOLOGY <b>2004</b>.  2(9): 739-746, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223627200018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223627200018</a> </p><br />

    <p>41.       44178   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Anti-Candida activity of YH-1715R, a new triazole derivative</p>

    <p>          Park, KS, Kang, HI, Lee, JW, and Paik, YK</p>

    <p>          JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY <b>2004</b>.  14(4): 693-697, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223598400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223598400008</a> </p><br />

    <p>42.       44179   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Anaerobic incubation conditions enhance pyrazinamide activity against Mycobacterium tuberculosis</p>

    <p>          Wade, MM and Zhang, Y</p>

    <p>          JOURNAL OF MEDICAL MICROBIOLOGY <b>2004</b>.  53(8): 769-773, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223528600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223528600009</a> </p><br />

    <p>43.       44180   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          DNA fingerprint changes in tuberculosis: Reinfection, evolution, or laboratory error?</p>

    <p>          Glynn, JR, Yates, MD, Crampin, AC, Ngwira, BM, Mwaungulu, FD, Black, GF, Chaguluka, SD, Mwafulirwa, DT, Floyd, S, Murphy, C, Drobniewski, FA, and Fine, PEM</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2004</b>.  190(6): 1158-1166, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223633400017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223633400017</a> </p><br />
    <br clear="all">

    <p>44.       44181   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Viral DNA polymerase mutations associated with drug resistance in human cytomegalovirus. (vol 188, pg 32, 2003)</p>

    <p>          Chou, S, Lurain, NS, Thompson, KD, Miner, RC, and Drew, WL</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2004</b>.  190(6): 1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223633400025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223633400025</a> </p><br />

    <p>45.       44182   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Enzyme-catalyzed mechanism of isoniazid activation in class I and class III peroxidases</p>

    <p>          Pierattelli, R, Banci, L, Eady, NAJ, Bodiguel, J, Jones, JN, Moody, PCE, Raven, EL, Jamart-Gregoire, B, and Brown, KA</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2004</b>.  279(37): 39000-39009, 10</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223684100114">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223684100114</a> </p><br />

    <p>46.       44183   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Macrolides and ketolides: azithromycin, clarithromycin, telithromycin</p>

    <p>          Zuckerman, JM</p>

    <p>          INFECTIOUS DISEASE CLINICS OF NORTH AMERICA <b>2004</b>.  18(3): 621-+, 30</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223677200011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223677200011</a> </p><br />

    <p>47.       44184   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          The novel azole R126638 is a selective inhibitor of ergosterol synthesis in Candida albicans, Trichophyton spp., and Microsporum canis</p>

    <p>          Bossche, HV, Ausma, J, Bohets, H, Vermuyten, K, Willemsens, G, Marichal, P, Meerpoel, L, and Borgers, M</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(9): 3272-3278, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800009</a> </p><br />

    <p>48.       44185   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Posaconazole and amphotericin B combination therapy against Cryptococcus neoformans infection</p>

    <p>          Barchiesi, F, Spreghini, E, Schimizzi, AM, Maracci, M, Giannini, D, Carle, F, and Scalise, G</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(9): 3312-3316, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800015</a> </p><br />

    <p>49.       44186   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          SRI-286, a thiosemicarbazole, in combination with mefloquine and moxifloxacin for treatment of murine Mycobacterium avium complex disease</p>

    <p>          Bermudez, LE, Kolonoski, P, Seitz, LE, Petrofsky, M, Reynolds, R, Wu, M, and Young, LS</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2004</b>.  48(9): 3556-3558, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223625800050</a> </p><br />

    <p>50.       44187   OI-LS-306; WOS-OI-10/3/2004</p>

    <p class="memofmt1-2">          Binding mode determination of benzimidazole inhibitors of the hepatitis C virus RNA polymerase by a structure and dynamics strategy</p>

    <p>          LaPlante, SR, Jakalian, A, Aubry, N, Bousquet, Y, Ferland, JM, Gillard, J, Lefebvre, S, Poirier, M, Tsantrizos, YS, Kukolj, G, and Beaulieu, PL</p>

    <p>          ANGEWANDTE CHEMIE-INTERNATIONAL EDITION <b>2004</b>.  43(33): 4306-4311, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223603600011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223603600011</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
