

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-02-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KRhoOEjmjApqjpSgK47Fq5oucS4eANtiXMQB3z9Mi1dkP+cBgVbNYkAFGtyi5Uqa4BVpZFSLYpd/VeOjkmkz9Ci8L4e0XM52Jhtj9asphaC1o7JlWxzBGbIbG1Y=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9D1C0347" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 30 - February 12, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25671213">Antimicrobial and Cytotoxic Activity of Ferula gummosa Plant Essential Oil Compared to NaOCl and CHX: A Preliminary in Vitro Study.</a> Abbaszadegan, A., A. Gholami, H. Mirhadi, M. Saliminasab, A. Kazemi, and M.R. Moein. Restorative Dentistry &amp; Endodontics, 2015. 40(1): p. 50-57. PMID[25671213].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25622093">Mohangamides A and B, New Dilactone-tethered Pseudo-dimeric Peptides Inhibiting Candida albicans Isocitrate Lyase.</a> Bae, M., H. Kim, K. Moon, S.J. Nam, J. Shin, K.B. Oh, and D.C. Oh. Organic Letters, 2015. 17(3): p. 712-715. PMID[25622093].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25435172">Antibacterial Compounds from Salvia adenophora Fernald (Lamiaceae).</a> Bisio, A., A.M. Schito, S.N. Ebrahimi, M. Hamburger, G. Mele, G. Piatti, G. Romussi, F. Dal Piaz, and N. De Tommasi. Phytochemistry, 2015. 110: p. 120-132. PMID[25435172].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25605775">In Vitro Activity of 23 Tea Extractions and Epigallocatechin gallate against Candida Species.</a> Chen, M., L. Zhai, and M.C. Arendrup. Medical Mycology, 2015. 53(2): p. 194-198. PMID[25605775].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25182365">Novel Triazole Alcohol Antifungals Derived from Fluconazole: Design, Synthesis, and Biological Activity.</a> Hashemi, S.M., H. Badali, M.A. Faramarzi, N. Samadi, M.H. Afsarian, H. Irannejad, and S. Emami. Molecular Diversity, 2015. 19(1): p. 15-27. PMID[25182365].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25288679">In Vitro Interactions between Farnesol and Fluconazole, Amphotericin B or Micafungin against Candida albicans Biofilms.</a> Katragkou, A., M. McCarthy, E.L. Alexander, C. Antachopoulos, J. Meletiadis, M.A. Jabra-Rizk, V. Petraitis, E. Roilides, and T.J. Walsh. Journal of Antimicrobial Chemotherapy, 2015. 70(2): p. 470-478. PMID[25288679].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25543205">Design, Synthesis, and Biological Evaluation of Aminothiazole Derivatives against the Fungal Pathogens Histoplasma capsulatum and Cryptococcus neoformans.</a> Khalil, A., J.A. Edwards, C.A. Rappleye, and W. Tjarks. Bioorganic &amp; Medicinal Chemistry, 2015. 23(3): p. 532-547. PMID[25543205].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25512426">Synergistic Activity of Chloroquine with Fluconazole against Fluconazole-resistant Isolates of Candida Species.</a> Li, Y., Z. Wan, W. Liu, and R. Li. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1365-1369. PMID[25512426].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25410525">9. Antibiofilm Potential of Flavonoids Extracted from Moringa oleifera Seed Coat against Staphylococcus aureus, Pseudomonas aeruginosa and Candida albicans.</a> Onsare, J.G. and D.S. Arora. Journal of Applied Microbiology, 2015. 118(2): p. 313-325. PMID[25410525].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25657164">Susceptibility Characterisation of Candida Spp. to Four Essential Oils.</a> Rath, C.C. and S. Mohapatra. Indian Journal of Medical Microbiology, 2015. 33 Suppl: p. S93-S96. PMID[25657164].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25648598">Anti-tuberculous Activity of Treponemycin Produced by a Streptomyces Strain MS-6-6 Isolated from Saudi Arabia.</a> Yassien, M.A., H.M. Abdallah, A.M. El-Halawany, and A.A. Jiman-Fatani. Molecules, 2015. 20(2): p. 2576-2590. PMID[25648598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25381141">Histone Deacetylase Inhibitors for Enhancing Activity of Antifungal Agent: A Patent Evaluation of WO2014041424(A1).</a> Zhang, L. and W. Xu. Expert Opinion on Therapeutic Patents, 2015. 25(2): p. 237-240. PMID[25381141].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25421469">Rapid Cytolysis of Mycobacterium tuberculosis by Faropenem, an Orally Bioavailable Beta-lactam Antibiotic.</a> Dhar, N., V. Dubee, L. Ballell, G. Cuinet, J.E. Hugonnet, F. Signorino-Gelo, D. Barros, M. Arthur, and J.D. McKinney. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1308-1319. PMID[25421469].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25569565">Enabling the (3 + 2) Cycloaddition Reaction in Assembling Newer Anti-tubercular Lead Acting through the Inhibition of the Gyrase ATPase Domain: Lead Optimization and Structure Activity Profiling.</a> Jeankumar, V.U., R.S. Reshma, R. Janupally, S. Saxena, J.P. Sridevi, B. Medapi, P. Kulkarni, P. Yogeeswari, and D. Sriram. Organic &amp; Biomolecular Chemistry, 2015. 13(8): p. 2423-2431. PMID[25569565].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25657144">In Vitro Activities of Linezolid against Clinical Isolates of Mycobacterium tuberculosis from Shenyang, North of China.</a> Liu, H., Y. Wang, N. Liu, and L. Zhao. Indian Journal of Medical Microbiology, 2015. 33 Suppl: p. S164-S165. PMID[25657144].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25431322">Total Synthesis of the Protected Aglycon of Fidaxomicin (Tiacumicin B, Lipiarmycin A3).</a> Miyatake-Ondozabal, H., E. Kaufmann, and K. Gademann. Angewandte Chemie, 2015. 54(6): p. 1933-1936. PMID[25431322].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">17.<a href="http://www.ncbi.nlm.nih.gov/pubmed/25541388">Pharmacophore Based Virtual Screening, Synthesis and SAR of Novel Inhibitors of Mycobacterium sulfotransferase.</a> Saha, R., O. Tanwar, M.M. Alam, M.S. Zaman, S.A. Khan, and M. Akhter. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 701-707. PMID[25541388].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25645397">Delamanid (OPC-67683) for Treatment of Multi-drug-resistant Tuberculosis.</a> Sotgiu, G., E. Pontali, R. Centis, L. D&#39;Ambrosio, and G.B. Migliori. Expert Review of Anti-infective Therapy, 2015. 13(3): p. 305-315. PMID[25645397].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25559743">Design and Synthesis of Novel Carbazole Tethered Pyrrole Derivatives as Potent Inhibitors of Mycobacterium tuberculosis.</a> Surineni, G., P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 485-491. PMID[25559743].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25659138">A Rho GDP Dissociation Inhibitor Produced by Apoptotic T-cells Inhibits Growth of Mycobacterium tuberculosis.</a> Venkatasubramanian, S., R. Dhiman, P. Paidipally, S.S. Cheekatla, D. Tripathi, E. Welch, A.R. Tvinnereim, B. Jones, D. Theodorescu, P.F. Barnes, and R. Vankayalapati. Plos Pathogens, 2015. 11(2): p. e1004617. PMID[25659138].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25556103">Synthesis, Antimycobacterial and Antifungal Evaluation of Some New 1-Ethyl-5-(hetero)aryl-6-styryl-1,6-dihydropyrazine-2,3-dicarbonitriles.</a> Verbitskiy, E.V., P.A. Slepukhin, M.A. Kravchenko, S.N. Skornyakov, N.P. Evstigneeva, N.V. Kungurov, N.V. Zil&#39;berberg, G.L. Rusinov, O.N. Chupakhin, and V.N. Charushin. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 524-528. PMID[25556103].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/24621166">22. The Effect of Lopinavir/Ritonavir on the Antimalarial Activity of Artemether or Artemether/Lumefantrine in a Mouse Model of Plasmodium berghei.</a> Abiodun, O.O., J. Akinbo, and O. Ojurongbe. Journal of Chemotherapy, 2015. 27(1): p. 25-28. PMID[24621166].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25547934">Exploring in Vitro and in Vivo Hsp90 Inhibitors Activity against Human Protozoan Parasites.</a> Giannini, G. and G. Battistuzzi. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 462-465. PMID[25547934].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25563890">Hydroxamic acid Based Histone Deacetylase Inhibitors with Confirmed Activity against the Malaria Parasite.</a> Giannini, G., G. Battistuzzi, and D. Vignola. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(3): p. 459-461. PMID[25563890].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25584395">Salinipostins A-K, Long-chain Bicyclic Phosphotriesters as a Potent and Selective Antimalarial Chemotype.</a> Schulze, C.J., G. Navarro, D. Ebert, J. DeRisi, and R.G. Linington. The Journal of Organic Chemistry, 2015. 80(3): p. 1312-1320. PMID[25584395].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25519040">Uncovering New Structural Insights for Antimalarial Activity from Cost-effective Aculeatin-like Derivatives.</a> Winkler, M., M. Maynadier, S. Wein, M.A. Lespinasse, G. Boumis, A.E. Miele, H. Vial, and Y.S. Wong. Organic &amp; Biomolecular Chemistry, 2015. 13(7): p. 2064-2077. PMID[25519040].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0130-021215.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347297200013">Substituted Quinolinones. Part 22. In Vitro Antimicrobial Evaluation of Some 4-Hydroxy-1-methyl-3-pyrazolinylquinolin-2(1H)-ones as Useful Antibiotic Intermediates.</a> Abass, M. and E.S. Othman. Research on Chemical Intermediates, 2015. 41(1): p. 117-125. ISI[000347297200013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347022700016">Antimycobacterial Activity of Selected Medicinal Plants Traditionally used in Sudan to Treat Infectious Diseases.</a> Abuzeid, N., S. Kalsum, R.J. Koshy, M. Larsson, M. Glader, H. Andersson, J. Raffetseder, E. Pienaar, D. Eklund, M.S. Alhassan, H.A. AlGadir, W.S. Koko, T. Schon, M.A. Mesaik, O.M. Abdalla, A. Khalid, and M. Lerm. Journal of Ethnopharmacology, 2014. 157: p. 134-139. ISI[000347022700016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347405700030">Synthesis of 3-Chloro-4-fluorophenyl-3,4-dichlorophenyl Substituted Thiocarbamide Derivatives as Potential Antitubercular Agents.</a> Arshad, M.F., S. Kumar, A.H. Al Rohaimi, A.A. Hassan, A. Elkerdasy, and N. Upmanyu. Medicinal Chemistry Research, 2015. 24(1): p. 334-343. ISI[000347405700030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347145200004">Synthesis and Pharmacological Activities of Some New 2-[1-Heptyl-3-(4-methoxybenzyl)-5-oxo-1,5-dihydro-4H-1,2,4-triazol-4-yl]acetohydrazide Derivatives.</a> Beldrcana, O., E. Mentese, and S. Ulkerc. Zeitschrift Fur Naturforschung Section B - A Journal of Chemical Sciences, 2014. 69(9-10): p. 969-981. ISI[000347145200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347702200014">In Vitro Antimicrobial Activity of Pistacia lentiscus L. Edible Oil and Phenolic Extract.</a> Mezni, F., C. Aouadhi, M.L. Khouja, A. Khaldi, and A. Maaroufi. Natural Product Research, 2015. 29(6): p. 565-570. ISI[000347702200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347269900083">DNA Cleavage, Antibacterial, Antifungal and Anthelmintic Studies of Co(II), Ni(II) and Cu(II) Complexes of Coumarin Schiff Bases: Synthesis and Spectral Approach.</a> Patil, S.A., C.T. Prabhakara, B.M. Halasangi, S.S. Toragalmath, and P.S. Badami. Spectrochimica Acta Part A - Molecular and Biomolecular Spectroscopy, 2015. 137: p. 641-651. ISI[000347269900083].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347384900035">Antimicrobial and Antioxidant Activities of Bauhinia racemosa Lam. and Chemical Content.</a> Rashed, K. and M. Butnariu. Iranian Journal of Pharmaceutical Research, 2014. 13(3): p. 1073-1080. ISI[000347384900035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346586100013">Chemical Composition, Antimicrobial and Antioxidant Activities of the Essential Oil of Psammogetoncanescens.</a> Raudone, L., R. Raudonis, K. Gaivelyte, A. Pukalskas, P. Viskelis, P.R. Venskutonis, and V. Janulis. Natural Product Research, 2015. 29(3): p. 281-285. ISI[000346586100013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346892700035">Chemical Compositions and Antimicrobial Activity of the Essential Oils of Piper abbreviatum, P. ereeticaule and P. lanatum (Piperaceae).</a> Salleh, W., F. Ahmad, and K.H. Yen. Natural Product Communications, 2014. 9(12): p. 1795-1798. ISI[000346892700035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347019000034">New Antimicrobial Pregnane Glycosides from the Stem of Ecdysanthera rosea.</a> Song, C.W., P.K. Lunga, X.J. Qin, G.G. Cheng, J.L. Gu, Y.P. Liu, and X.D. Luo. Fitoterapia, 2014. 99: p. 267-275. ISI[000347019000034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347405700014">Design and Applications of Bifunctional Cinnamide Derivatives as Potential Antimycobacterial Agents with Few Hepatotoxic Effects.</a> Wu, Z.R., D.J. Zhi, L.F. Zheng, J.Y. Li, Y. Li, Q.J. Xie, N. Feng, Y.F. Bao, Q.Y. Gao, Y. Song, and H.Y. Li. Medicinal Chemistry Research, 2015. 24(1): p. 161-170. ISI[000347405700014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347019000038">Synthesis and Antimycobacterial Evaluation of Natural Oridonin and Its Enmein-type Derivatives.</a> Xu, S.T., L.L. Pei, D.H. Li, H. Yao, H. Cai, H.Q. Yao, X.M. Wu, and J.Y. Xu. Fitoterapia, 2014. 99: p. 300-306. ISI[000347019000038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347405700016">Synthesis and Biological Evaluation of Novel D-Glucose-derived 1,2,3-Triazoles as Potential Antibacterial and Antifungal Agents.</a> Zhang, H.Z., J.J. Wei, K.V. Kumar, S. Rasheed, and C.H. Zhou. Medicinal Chemistry Research, 2015. 24(1): p. 182-196. ISI[000347405700016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0130-021215.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
