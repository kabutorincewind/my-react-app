

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-09-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7qVGHH3BZUOr1Pj5Xio3awaZkxRvseyR1bHDNYZ9ij99eOzWfPNgpg5bxs95oaXNKtFXtlN1r9aegeaMM8xU2nVHcFm2T6eqEvwREy1pB65xLXpgRRL4aO99UU4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E49654C6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List: September 16, 2011 - September 29, 2011</h1>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21899332">P1-Substituted Symmetry-Based Human Immunodeficiency Virus Protease Inhibitors with Potent Antiviral Activity against Drug-Resistant Viruses.</a> Degoey, D.A., D.J. Grampovnik, H.J. Chen, W.J. Flosi, L.L. Klein, T. Dekhtyar, V. Stoll, M. Mamo, A. Molla, and D.J. Kempf, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21899332].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>    

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21953456">Mechanism of HIV reverse transcriptase inhibition by zinc: formation of a highly stable enzyme-(primer-template) complex with profoundly diminished catalytic activity.</a> Fenstermacher, K.J. and J.J. Destefano, Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21953456].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21916489">Design, Synthesis, and X-ray Crystallographic Analysis of a Novel Class of HIV-1 Protease Inhibitors.</a> Ganguly, A.K., S.S. Alluri, D. Caroccia, D. Biswas, C.H. Wang, E. Kang, Y. Zhang, A.T. McPhail, S.S. Carroll, C. Burlein, V. Munshi, P. Orth, and C. Strickland, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21916489].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21923937">Use of silver nanoparticles increased inhibition of cell-associated HIV-1 infection by neutralizing antibodies developed against HIV-1 envelope proteins.</a> Lara, H.H., L. Ixtepan-Turrent, E.N. Garza Trevino, and D.K. Singh, Journal of Nanobiotechnology, 2011. 9(1): p. 38; PMID[21923937].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21951660">Structural and biochemical characterization of the inhibitor complexes of XMRV protease.</a> Li, M., A. Gustchina, K. Matuz, J. Tozser, S. Namwong, N.E. Goldfarb, B.M. Dunn, and A. Wlodawer, The FEBS Journal, 2011. <b>[Epub ahead of print]</b>; PMID[21951660].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21879735">Activity and Safety of Synthetic Lectins Based on Benzoboroxole-Functionalized Polymers for Inhibition of HIV Entry.</a> Mahalingam, A., A.R. Geonnotti, J. Balzarini, and P.F. Kiser, Molecular Pharmaceutics, 2011. <b>[Epub ahead of print]</b>; PMID[21879735].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21947448">Docking, molecular dynamics and quantitative structure-activity relationship studies for HEPTs and DABOs as HIV-1 reverse transcriptase inhibitors.</a> Mao, Y., Y. Li, M. Hao, S. Zhang, and C. Ai, Journal of Molecular Modeling, 2011. <b>[Epub ahead of print]</b>; PMID[21947448].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21936641">Anti-HIV-1 and cytotoxicity of the alkaloids of Erythrina abyssinica Lam. growing in Sudan.</a> Mohammed, M.M., N.A. Ibrahim, N.E. Awad, A.A. Matloub, A.G. Mohamed-Ali, E.E. Barakat, A.E. Mohamed, and P.L. Colla, Natural Product Research, 2011. <b>[Epub ahead of print]</b>; PMID[21936641].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21933753">Measurement of antiviral activity in drugs for HIV-1.</a> Siliciano, R.F., The Lancet Infectious Diseases 2011. <b>[Epub ahead of print]</b>; PMID[21933753].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21936715">The hematopoietic cell-specific Rho GTPase inhibitor ARHGDIB/D4GDI limits HIV-1 replication.</a> Watanabe, T., E. Urano, K. Miyauchi, R. Ichikawa, M. Hamatake, N. Misawa, K. Sato, H. Ebina, Y. Koyanagi, and J. Komano, AIDS Research and Human Retroviruses, 2011. <b>[Epub ahead of print]</b>; PMID[21936715].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21928824">Development of indole compounds as small molecule fusion inhibitors targeting HIV-1 glycoprotein-41.</a> Zhou, G., D. Wu, B. Snyder, R.G. Ptak, H. Kaur, and M. Gochin, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21928824].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876179">Zinc-finger antiviral protein inhibits HIV-1 infection by selectively targeting multiply spliced viral mRNAs for degradation.</a> Zhu, Y., G. Chen, F. Lv, X. Wang, X. Ji, Y. Xu, J. Sun, L. Wu, Y.T. Zheng, and G. Gao, Proceedings of the National Academy of Sciences of the United States of America, 2011. 108(38): p. 15834-9; PMID[21876179].
    <br />
    <b>[PubMed]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294385600010">Structure-Activity Relationship Studies of HIV-1 Integrase Oligonucleotide Inhibitors.</a> Agapkina, J., T. Zatsepin, E. Knyazhanskaya, J.F. Mouscadet, and M. Gottikh, Acs Medicinal Chemistry Letters, 2011. 2(7): p. 532-537; PMID[WOS:000294385600010].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294139000093">Analogues of a lupane-type triterpene as potential anti-HIV agents.</a> Callies, O., L. Bedoya, A. Munoz, I. Jimenez, J. Alcami, and I. Bazzocchi, Planta Medica, 2011. 77(12): p. 1255-1255; PMID[WOS:000294139000093].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294714400021">d(TGGGAG) with 5 &#39;-nucleobase-attached large hydrophobic groups as potent inhibitors for HIV-1 envelop proteins mediated cell-cell fusion.</a> Chen, W., L. Xu, L. Cai, B. Zheng, K. Wang, J. He, and K. Liu, Bioorganic &amp; Medicinal Chemistry Letters. 2011. 21(19): p. 5762-5764; PMID[WOS:000294714400021].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294375700067">Crystal structure of HIV-1 reverse transcriptase (RT)/dsDNA/non-nucleoside inhibitor complex for understanding inhibition and drug resistance.</a> Das, K., S.E. Martinez, and E. Arnold, Antiviral Therapy, 2011. 16(4): p. A56-A56; PMID[WOS:000294375700067].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294375700027">TMC310911, a novel HIV type-1 protease inhibitor with an improved resistance coverage and a higher genetic barrier compared to currently approved protease inhibitors.</a> Dierynck, I., H. Van Marck, M. Van Ginderen, T. Jonckers, A. Raoof, G. Kraus, and G. Picchio, Antiviral Therapy, 2011. 16(4): p. A11-A11; PMID[WOS:000294375700027].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294801900003">Dibenzocyclooctadiene lignans from Schisandra neglecta and their anti-HIV-1 activities.</a> Duan, Y.X., J.L. Cao, R.R. Wen, G.Y. Yang, J.X. Pu, H.D. Sun, W.L. Xiao, and G.P. Li, Journal of Asian Natural Products Research, 2011. 13(7): p. 592-598; PMID[WOS:000294801900003].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294249100054">Microwave Assisted Organic Synthesis (MAOS) of Small Molecules as Potential HIV-1 Integrase Inhibitors</a>. Ferro, S., S. De Grazia, L. De Luca, R. Gitto, C.E. Faliti, Z. Debyzer, and A. Chimirri, Molecules, 2011. 16(8): p. 6858-6870; PMID[WOS:000294249100054].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294460400015">Tetrahydrofuran, tetrahydropyran, triazoles and related heterocyclic derivatives as HIV protease inhibitors.</a> Ghosh, A.A., DD, Future Medicinal Chemistry, 2011. 3(9): p. 1181-1197; PMID[WOS:000294460400015].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294714600201">tert-Butyl N-[3-hydroxy-1-phenyl-4-(pyrimidin-2-ylsulfanyl)butan-2-yl]carbamate monohydrate.</a> Gomes, C.R.B., T.R.A. Vasconcelos, W.T. Vellasco, J.L. Wardell, S. Wardell, and E.R.T. Tiekink, Acta Crystallographica Section E-structure Reports Online, 2011. 67: p. O2313-U2440; PMID[WOS:000294714600201].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294711100027">Synthesis and biological evaluation of HQCAs with aryl or benzyl substituents on N-1 position as potential HIV-1 integrase inhibitors.</a> He, Q., X. Zhang, H. Wu, S. Gu, X. Ma, L. Yang, Y. Zheng, and F. Chen, Bioorganic &amp; Medicinal Chemistry, 2011. 19(18): p. 5553-5558; PMID[WOS:000294711100027]. <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294410600004">Anti HIV-1 Agents 7. Discovery of 1-Hydroxy-4-chloro-9,10-anthraquinone Derivatives as New HIV-1 Inhibitors in Vitro</a>. Huang, N., Q. Wang, L. Yang, H. Xu, and Y. Zheng, Letters in Drug Design &amp; Discovery, 2011. 8(7): p. 602-605; PMID[WOS:000294410600004].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294375700072">Structure-function studies of HIV-1 protease drug resistance to tipranavir and darunavir.</a> Kovari, L.C., Y. Wang, and J. Brunzelle, Antiviral Therapy, 2011. 16(4): p. A61-A61; PMID[WOS:000294375700072]. <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294704600003">Antiviral Activity of Substituted Salicylanilides - A Review.</a> Kratky, M.V., J, Mini-Reviews in Medicinal Chemistry, 2011. 11(11): p. 956-967; PMID[WOS:000294704600003].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294714400037">Anti-AIDS agents 87. New bio-isosteric dicamphanoyl-dihydropyranochromone (DCP) and dicamphanoyl-khellactone (DCK) analogues with potent anti-HIV activity</a>. Liu, H., S. Xu, M. Cheng, Y. Chen, P. Xia, K. Qian, Y. Xia, Z. Yang, C. Chen, S. Morris-Natschke, and K. Lee, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(19): p. 5831-5834; PMID[WOS:000294714400037]. <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294711100037">Synthesis and biological evaluation of 2 &#39;,4 &#39;- and 3 &#39;,4 &#39;-bridged nucleoside analogues.</a> Nicolaou, K.C., S.P. Ellery, F. Rivas, K. Saye, E. Rogers, T.J. Workinger, M. Schallenberger, R. Tawatao, A. Montero, A. Hessell, F. Romesberg, D. Carson, and D. Burton, Bioorganic &amp; Medicinal Chemistry, 2011. 19(18): p. 5648-5669; PMID[WOS:000294711100037].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294139000452">Anti-HIV activity of Delta 18-oleane triterpenoids from Cassine xylocarpa.</a> Osorio, A.A., D.F. Torres, L.M. Bedoya, A. Munoz, J. Alcami, and I.L. Bazzocchi, Planta Medica, 2011. 77(12): p. 1341-1341; PMID[WOS:000294139000452].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294714400061">New betulinic acid derivatives as potent proteasome inhibitors.</a> Qian, K., S. Kim, H. Hung, L. Huang, C. Chen, and K. Lee, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(19): p. 5944-5947; PMID[WOS:000294714400061].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294607900001">Subunit-specific mutational analysis of residue N348 in HIV-1 reverse transcriptase.</a> Radzio, J. and N. Sluis-Cremer, Retrovirology, 2011. 8; PMID[WOS:000294607900001].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294410600002">Classification Structure-Activity Relationship Study of Reverse Transcriptase Inhibitors.</a> Seyagh, M., E.L.M. Mazouz, A. Schmitzer, D. Villemin, A. Jarid, and D. Cherqaoui, Letters in Drug Design &amp; Discovery, 2011. 8(7): p. 585-595; PMID[WOS:000294410600002].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294393900022">Synthesis, in vitro evaluation, and docking studies of novel chromone derivatives as HIV-1 protease inhibitor</a>. Ungwitayatorn, J., C. Wiwat, W. Samee, P. Nunthanavanit, and N. Phosrithong, Journal of Molecular Structure, 2011. 1001(1-3): p. 152-161; PMID[WOS:000294393900022].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294506200018">Microwave assisted synthesis of 4-thiazolidinones and its 5-arylidene derivatives: Screening of antimicrobial activity with various microorganisms.</a> Upadhyay, A., S.K. Srivastava, S.D. Srivastava, and N. Upadhyay, Journal of the Indian Chemical Society, 2011. 88(7): p. 1025-1031; PMID[WOS:000294506200018].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293610200019">Synthesis and Reactions of 1-Amino-5-morpholin-4-yl-6,7,8,9-tetrahydrothieno[2,3-c]isoquinoline.</a> Zaki, R.M., S.M. Radwan, and A.M.K. El-Dean, Journal of the Chinese Chemical Society, 2011. 58(4): p. 544-554; PMID[WOS:000293610200019].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294375200002">Quantitative Structure-Activity Relationship of IOPY/ISPY Analogues as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors</a>. Zhu, R., F. Wang, Q. Liu, and T. Kang, Acta Chimica Sinica, 2011. 69(15): p. 1731-1736; PMID[WOS:000294375200002].
    <br />
    <b>[WOS]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="memofmt2-2">Patent citations:</p>

    <p class="plaintext">36. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011106445A1&amp;KC=A1">Preparation of betulin derivatives for treatment of HIV-1.</a> Gao, D., N. Han, Z. Jin, F. Ning, J. Tang, Y. Wu, and H. Yang, 2011, (GlaxoSmithKline LLC, USA). Application: WO WO. p. 52pp.
    <br />
    <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">37. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110915&amp;CC=US&amp;NR=2011224242A1&amp;KC=A1">Preparation of styrylquinolines and their therapeutic uses as integrase inhibitors and for the treatment and prevention of HIV.</a> Giethlen, B., M. Michaut, C. Monneret, E. Soma, L. Thibault, and C.G. Wermuth, 2011, (BioAlliance Pharma, Fr.). Application: US US. p. 18pp , Cont -in-part of U S Ser No 269,241. <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">38. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110909&amp;CC=WO&amp;NR=2011107572A1&amp;KC=A1">Ferrocenyl flavonoids.</a> Hillard, E., G. Chabot, J.-P. Monserrat, G. Jaouen, K.N. Tiwari, F. De Montigny, and N. Neamati, 2011, (Centre National de la Recherche Scientifique (CNRS), Fr.). Application: WO WO. p. 50pp.
    <br />
    <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">39. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011106445A1&amp;KC=A1">Preparation of oxazinonaphthyridine derivatives for use as anti-HIV agents.</a> Jin, H., C.U. Kim, and B.W. Phillips, 2011, (Gilead Sciences, Inc., USA). Application: WO WO. p. 56pp.
    <br />
     <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">40. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011106703A2&amp;KC=A2">CXCR4 receptor allosteric modulators. 2011, (Anchor Therapeutics, Inc., USA).</a> Looby, R.J. and B. Tchernychev, Application: WO WO. p. 142pp.
    <br />
    <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">41. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110901&amp;CC=WO&amp;NR=2011105590A1&amp;KC=A1">Preparation of 1,3,4,8-tetrahydro-2H-pyrido[1,2-a]pyrazine derivatives and their use as HIV integrase inhibitors.</a> Miyazaki, S., Y. Bessho, K. Adachi, S. Kawashita, H. Isoshima, K. Oshita, and S. Fukuda, 2011, (Japan Tobacco Inc., Japan). Application: WO p 278pp.
    <br />
    <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
    <p class="plaintext">42. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110825&amp;CC=WO&amp;NR=2011100838A1&amp;KC=A1">1-Phenyl-1,5-dihydrobenzo[b][1,4]diazepine-2,4-dione derivatives as HIV replication inhibitors and their preparation and use for the treatment of HIV infection. 2011, (Boehringer Ingelheim International GmbH, Germany).</a> Simoneau, B., P. Deroy, L. Fader, A.-M. Faucher, A. Gagnon, C. Grand-Maitre, S. Kawai, S. Landry, J.-F. Mercier, and J. Rancourt, Application: WO p.122pp.
    <br />
    <b>[Patent]</b>. HIV_0916-092911.</p>
    <p> </p>
    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
