

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-01-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ABudSoqdQ3IJBksjIZtbb8CXDO2TJGIf/IGreaFRd63UoTUml8dsip/XZUPq8sfpgEl8xbxCQNwSDFGfy6Lu0gKFRM+oGoA4tBnHsL4w3Y3+0uvFkEU5UCPkLsI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A8CE9A05" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV  Citation List:  December 11 - December 24, 2008</h1>

    <p class="memofmt2-2">Anti*HIV</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19102682?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Enhancing the Divergent Activities of Betulinic Acid via Neoglycosylation.</a></p>

    <p class="authors">Goff RD, Thorson JS.</p>

    <p class="source">Org Lett. 2008 Dec 22. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19102682 [PubMed - as supplied by publisher]    </p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19099395?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Peptide Inhibitors of HIV-1 Egress.</a></p>

    <p class="authors">Waheed AA, Freed EO.</p>

    <p class="source">ACS Chem Biol. 2008 Dec 19;3(12):745-7.</p>

    <p class="pmid">PMID: 19099395 [PubMed - in process]                 </p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19090525?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Structural Modifications of DAPY Analogues with Potent Anti-HIV-1 Activity.</a></p>

    <p class="authors">Feng XQ, Liang YH, Zeng ZS, Chen FE, Balzarini J, Pannecouque C, De Clercq E.</p>

    <p class="source">ChemMedChem. 2008 Dec 17. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19090525 [PubMed - as supplied by publisher]    </p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19089644?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Characterization of Cyclodextrin Inclusion Complexes of the Anti-HIV Non-Nucleoside Reverse Transcriptase Inhibitor UC781.</a></p>

    <p class="authors">Yang H, Parniak MA, Isaacs CE, Hillier SL, Rohan LC.</p>

    <p class="source">AAPS J. 2008 Dec 17. [Epub ahead of print]  </p>

    <p class="pmid">PMID: 19089644 [PubMed - as supplied by publisher]    </p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/18996020?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">C-2-aryl O-substituted HI-236 derivatives as non-nucleoside HIV-1 reverse-transcriptase inhibitors.</a></p>

    <p class="authors">Hunter R, Younis Y, Muhanji CI, Curtin TL, Naidoo KJ, Petersen M, Bailey CM, Basavapathruni A, Anderson KS.</p>

    <p class="source">Bioorg Med Chem. 2008 Dec 15;16(24):10270-80. Epub 2008 Nov 1.</p>

    <p class="pmid">PMID: 18996020 [PubMed - in process]                 </p>

    <p class="memofmt2-2">Therapeutic and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19053244?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of HIV Budding by a Genetically Selected Cyclic Peptide Targeting the Gag-TSG101 Interaction.</a></p>

    <p class="authors">Tavassoli A, Lu Q, Gam J, Pan H, Benkovic SJ, Cohen SN.</p>

    <p class="source">ACS Chem Biol. 2008 Dec 19;3(12):757-764.</p>

    <p class="pmid">PMID: 19053244 [PubMed - as supplied by publisher]    </p>


    <p class="memofmt2-2">Compound and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19104010?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Preclinical Evaluation of GS-9160, a Novel Inhibitor of HIV-1 Integrase.</a></p>

    <p class="authors">Jones GS, Yu F, Zeynalzadegan A, Hesselgesser J, Chen X, Chen J, Jin H, Kim CU, Wright M, Geleziunas R, Tsiang M.</p>

    <p class="source">Antimicrob Agents Chemother. 2008 Dec 22. [Epub ahead of print]        </p>

    <p class="pmid">PMID: 19104010 [PubMed - as supplied by publisher]</p>


    <p class="memofmt2-2">Inhibition and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19097000?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Prenylisoflavonoids from Erythrina senegalensis as Novel HIV-1 Protease Inhibitors.</a></p>

    <p class="authors">Lee J, Oh WK, Ahn JS, Kim YH, Mbafor JT, Wandji J, Fomum ZT.</p>

    <p class="source">Planta Med. 2008 Dec 18. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19097000 [PubMed - as supplied by publisher]    </p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19097993?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">CD4-anchoring HIV-1 fusion inhibitor with enhanced potency and in vivo stability.</a></p>

    <p class="authors">Ji C, Kopetzki E, Jekle A, Stubenrauch KG, Liu X, Zhang J, Rao E, Schlothauer T, Fischer S, Cammack N, Heilek G, Ries S, Sankuratri S.</p>

    <p class="source">J Biol Chem. 2008 Dec 19. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19097993 [PubMed - as supplied by publisher]    </p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18993071?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Orally bioavailable prodrugs of a BCS class 2 molecule, an inhibitor of HIV-1 reverse transcriptase.</a></p>

    <p class="authors">Elworthy TR, Dunn JP, Hogg JH, Lam G, Saito YD, Silva TM, Stefanidis D, Woroniecki W, Zhornisky E, Zhou AS, Klumpp K.</p>

    <p class="source">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6344-7. Epub 2008 Nov 1.</p>

    <p class="pmid">PMID: 18993071 [PubMed - in process]                 </p>

    <p class="memofmt2-2">Pharmaceutical and (HIV or AIDS or Human Immunodeficiency Virus)</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18952420?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of trans-caffeate analogues and their bioactivities against HIV-1 integrase and cancer cell lines.</a></p>

    <p class="authors">Xia CN, Li HB, Liu F, Hu WX.</p>

    <p class="source">Bioorg Med Chem Lett. 2008 Dec 15;18(24):6553-7. Epub 2008 Oct 15.</p>

    <p class="pmid">PMID: 18952420 [PubMed - in process]                 </p>

    <p class="memofmt2-2">Citations From The Forwarded &#39;Web of Knowledge&#39; listings</p>

    <p class="title">12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Highly potent, fully recombinant anti-HIV chemokines: Reengineering a low-cost microbicide</a></p>

    <p class="authors">Gaertner, H  Cerini, F Escola, JM Kuenzi, G Melotti, A Offord, R Rossitto-Borlat, I Nedellec, R  Salkowitz, J Gorochov, G Mosier, D Hartley, O</p>

    <p class="source">PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE USA.  2008 Nov 18;105(46): 17706-17711.</p>

    <p class="pmid">PMID: 19004761               </p>

    <p class="title">13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">The antiretroviral potency of APOBEC1 deaminase from small animal species</a></p>

    <p class="authors">Ikeda, T Ohsugi, T Kimura, T Matsushita, S Maeda, Y Harada, S Koito, A</p>

    <p class="source">NUCLEIC ACIDS RESEARCH. 2008 Dec; 36(21): 6859-6871.               </p>

    <p class="pmid">PMID: 18971252</p>

    <p class="title">14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Biochemical Characterization of a Recombinant TRIM5 alpha Protein That Restricts Human Immunodeficiency Virus Type 1 Replication</a></p>

    <p class="authors">Langelier, CR Sandrin, V Eckert, DM Christensen, DE Chandrasekaran, V Alam, SL Aiken, C Olsen, JC Kar, AK Sodroski, JG Sundquist, WI</p>

    <p class="source">JOURNAL OF VIROLOGY. 2008 DEC; 82(23): 11682-94.      </p>

    <p class="pmid">PMID: 18799573</p>

    <p class="title">15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Pestalazines and Pestalamides, Bioactive Metabolites from the Plant Pathogenic Fungus Pestalotiopsis theae</a></p>

    <p class="authors">Ding, G Jiang, LH Guo, LD Chen, XL Zhang, H Che, YS</p>

    <p class="source">JOURNAL OF NATURAL PRODUCTS. 2008 Nov; 71(11): 1861-1865.              </p>

    <p class="pmid">PMID: 18855443</p>

    <p class="title">16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Albumin-conjugated C34 Peptide HIV-1 Fusion Inhibitor EQUIPOTENT TO C34 AND T-20 IN VITRO WITH SUSTAINED ACTIVITY IN SCID-HU THY/LIV MICE</a></p>

    <p class="authors"> Stoddart, CA Nault, G Galkina, SA Thibaudeau, K Bakis, P Bousquet-Gagnon, N Robitaille, M  Bellomo, M Paradis, V Liscourt, P Lobach, A Rivard, ME Ptak, RG Mankowski, MK Bridon, D Quraishi, O</p>

    <p class="source">JOURNAL OF BIOLOGICAL CHEMISTRY.  2008 Dec 5; 283(49):34045-52.     </p>

    <p class="pmid">PMID: 18809675</p>

    <p class="title">17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Preparation and Biological Properties of a Melibiose Binding Lectin from Bauhinia variegata Seeds</a></p>

    <p class="authors">Lin, P Ng, TB</p>

    <p class="source">JOURNAL OF AGRICULTURAL AND FOOD CHEMISTRY. 2008 NOV 26; 56(22): 10481-10486.               </p>

    <p class="pmid">PMID: 18942841               </p>

    <p class="title">18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Structure of a new xanthone from Securidaca inappendiculata</a></p>

    <p class="authors">AU Kang, WY Xu, XJ</p>

    <p class="source">CHEMISTRY OF NATURAL COMPOUNDS. 2008 Jul; 44(4): 432-434.              </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
