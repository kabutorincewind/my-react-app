

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-02-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Y8xfWjck2LJK46fISjG0Dtg7x49g0Hnc+awmHuW70h77DhOJWhhS0QYQl1ypSB+MXBoEtjd/juNrBa18wrXOzYV6KitECnsLHggYZBr9bmFW+3N5dWmGeldy2Hs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="23599BE4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: January 30 - February 12, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25451058">Hydroxylated Tropolones Inhibit Hepatitis B Virus Replication by Blocking Viral Ribonuclease H Activity.</a> Lu, G., E. Lomonosova, X. Cheng, E.A. Moran, M.J. Meyers, S.F. Le Grice, C.J. Thomas, J.K. Jiang, C. Meck, D.R. Hirsch, M.P. D&#39;Erasmo, D.M. Suyabatmaz, R.P. Murelli, and J.E. Tavis. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1070-1079. PMID[25451058].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25305505">Alisporivir Inhibition of Hepatocyte Cyclophilins Reduces HBV Replication and Hepatitis B Surface Antigen Production.</a> Phillips, S., S. Chokshi, U. Chatterji, A. Riva, M. Bobardt, R. Williams, P. Gallay, and N.V. Naoumov. Gastroenterology, 2015. 148(2): p. 403-414. PMID[25305505].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25512416">STING Agonists Induce an Innate Antiviral Immune Response Against Hepatitis B Virus.</a> Guo, F., Y. Han, X. Zhao, J. Wang, F. Liu, C. Xu, L. Wei, J.D. Jiang, T.M. Block, J.T. Guo, and J. Chang. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1273-1281. PMID[25512416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25338920">Sustained Inhibition of Hepatitis B Virus Replication in Vivo Using RNAi-activating Lentiviruses.</a> Ivacik, D., A. Ely, N. Ferry, and P. Arbuthnot. Gene Therapy, 2015. 22(2): p. 163-171. PMID[25338920].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25512419">Postexposure Prophylactic Effect of Hepatitis B Virus (HBV)-active Antiretroviral Therapy Against HBV Infection.</a> Watanabe, T., S. Hamada-Tsutsumi, Y. Yokomaku, J. Imamura, W. Sugiura, and Y. Tanaka. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1292-1298. PMID[25512419].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25491197">Caffeine Inhibits Hepatitis C Virus Replication in Vitro.</a> Batista, M.N., B.M. Carneiro, A.C. Braga, and P. Rahal. Archives of Virology, 2015. 160(2): p. 399-407. PMID[25491197].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25658612">TnBP/Triton X-45 Treatment of Plasma for Transfusion Efficiently Inactivates Hepatitis C Virus.</a> Chou, M.L., T. Burnouf, S.P. Chang, T.C. Hung, C.C. Lin, C.D. Richardson, and L.T. Lin. Plos One, 2015. 10(2): p. e0117800. PMID[25658612].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25658101">The Inhibitory Effects of Anacardic Acid on Hepatitis C Virus Life Cycle.</a> Hundt, J., Z. Li, and Q. Liu. Plos One, 2015. 10(2): p. e0117514. PMID[25658101].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25621702">Synthesis of Novel 4&#39;alpha-Trifluoromethyl-2&#39;beta-C-methyl-carbodine Analogs as anti-Hepatitis C Virus Agents.</a> Kim, S., E. Kim, and J.H. Hong. Nucleosides, Nucleotides and Nucleic Acids, 2015. 34(2): p. 79-91. PMID[25621702].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25451055">In Vitro and in Vivo Antiviral Activity and Resistance Profile of Ombitasvir, an Inhibitor of Hepatitis C Virus NS5A.</a> Krishnan, P., J. Beyer, N. Mistry, G. Koev, T. Reisch, D. DeGoey, W. Kati, A. Campbell, L. Williams, W. Xie, C. Setze, A. Molla, C. Collins, and T. Pilot-Matias. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 979-987. PMID[25451055].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25451053">In Vitro and in Vivo Antiviral Activity and Resistance Profile of the Hepatitis C Virus NS3/4A Protease Inhibitor ABT-450.</a> Pilot-Matias, T., R. Tripathi, D. Cohen, I. Gaultier, T. Dekhtyar, L. Lu, T. Reisch, M. Irvin, T. Hopkins, R. Pithawalla, T. Middleton, T. Ng, K. McDaniel, Y.S. Or, R. Menon, D. Kempf, A. Molla, and C. Collins. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 988-997. PMID[25451053].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25542984">Solid Lipid Nanoparticles as Non-viral Vector for the Treatment of Chronic Hepatitis C by RNA Interference.</a> Torrecilla, J., A. Del Pozo-Rodriguez, P.S. Apaolaza, M.A. Solinis, and A. Rodriguez-Gascon. International Journal of Pharmaceutics, 2015. 479(1): p. 181-188. PMID[25542984].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25487800">Novel Dengue Virus NS2B/NS3 Protease Inhibitors.</a> Wu, H., S. Bock, M. Snitko, T. Berger, T. Weidner, S. Holloway, M. Kanitz, W.E. Diederich, H. Steuber, C. Walter, D. Hofmann, B. Weissbrich, R. Spannaus, E.G. Acosta, R. Bartenschlager, B. Engels, T. Schirmeister, and J. Bodem. Antimicrobial Agents and Chemotherapy, 2015. 59(2): p. 1100-1109. PMID[25487800].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25476751">Inhibition of Hepatitis E Virus Replication by Proteasome Inhibitor Is Nonspecific.</a> Xu, L., X. Zhou, M.P. Peppelenbosch, and Q. Pan. Archives of Virology, 2015. 160(2): p. 435-439. PMID[25476751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24782459">Antiviral Treatment of Feline Immunodeficiency Virus-infected Cats with (R)-9-(2-Phosphonylmethoxypropyl)-2,6-diaminopurine.</a> Taffin, E., D. Paepe, N. Goris, J. Auwerx, M. Debille, J. Neyts, I. Van de Maele, and S. Daminet. Journal of Feline Medicine and Surgery, 2015. 17(2): p. 79-86. PMID[24782459].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25306391">Small Molecule Growth Inhibitors of Human Oncogenic Gammaherpesvirus Infected B-Cells.</a> Dzeng, R.K., H.C. Jha, J. Lu, A. Saha, S. Banerjee, and E.S. Robertson. Molecular Oncology, 2015. 9(2): p. 365-376. PMID[25306391].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25626372">Genipin as a Novel Chemical Activator of EBV Lytic Cycle.</a> Son, M., M. Lee, E. Ryu, A. Moon, C.S. Jeong, Y.W. Jung, G.H. Park, G.H. Sung, H. Cho, and H. Kang. Journal of Microbiology, 2015. 53(2): p. 155-165. PMID[25626372].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25499125">Anti-cytomegalovirus Activity of the Anthraquinone Atanyl Blue PRL.</a> Alam, Z., Z. Al-Mahdi, Y. Zhu, Z. McKee, D.S. Parris, H.I. Parikh, G.E. Kellogg, A. Kuchta, and M.A. McVoy. Antiviral Research, 2015. 114: p. 86-95. PMID[25499125].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25376108">Beta-Amyloid Peptides Display Protective Activity against the Human Alzheimer&#39;s Disease-associated Herpes Simplex Virus-1.</a> Bourgade, K., H. Garneau, G. Giroux, A.Y. Le Page, C. Bocti, G. Dupuis, E.H. Frost, and T. Fulop, Jr. Biogerontology, 2015. 16(1): p. 85-98. PMID[25376108].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25643242">Houttuynia cordata Targets the Beginning Stage of Herpes Simplex Virus Infection.</a> Hung, P.Y., B.C. Ho, S.Y. Lee, S.Y. Chang, C.L. Kao, S.S. Lee, and C.N. Lee. Plos One, 2015. 10(2): p. e0115475. PMID[25643242].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25648883">Total Synthesis and Antiviral Activity of Indolosesquiterpenoids from the Xiamycin and Oridamycin Families.</a> Meng, Z., H. Yu, L. Li, W. Tao, H. Chen, M. Wan, P. Yang, D.J. Edmonds, J. Zhong, and A. Li. Nature Communications, 2015. 6(6096): 8pp. PMID[25648883].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0130-021215.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
