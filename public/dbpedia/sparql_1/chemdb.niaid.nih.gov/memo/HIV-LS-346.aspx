

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-346.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jWfK4AWfmPERCaZc9ncYkndLVHGCxWJj5FLzMg+vC0uAoJbDTn+nInTbKJLxVSgKJENnJGEDM7mTgH/w106OEoDeP336pacAo+aoUaTToY9+XGWR6t1Mlf5pQNU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C40476D3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-346-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48696   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          New antiretroviral drugs</p>

    <p>          Hanson, K and Hicks, C</p>

    <p>          Curr HIV/AIDS Rep <b>2006</b>.  3(2): 93-101</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16608666&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16608666&amp;dopt=abstract</a> </p><br />

    <p>2.     48697   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Reverse transcriptase mutations 118I, 208Y, and 215Y cause HIV-1 hypersusceptibility to non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Clark, SA, Shulman, NS, Bosch, RJ, and Mellors, JW</p>

    <p>          AIDS <b>2006</b>.  20 (7): 981-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603849&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603849&amp;dopt=abstract</a> </p><br />

    <p>3.     48698   HIV-LS-346; EMBASE-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Evaluation of mismatch-binding ligands as inhibitors for Rev-RRE interaction</p>

    <p>          Nakatani, Kazuhiko, Horie, Souta, Goto, Yuki, Kobori, Akio, and Hagihara, Shinya</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4JN2NR3-5/2/e78e8b6d200bcf8363b2dc8f014784e0">http://www.sciencedirect.com/science/article/B6TF8-4JN2NR3-5/2/e78e8b6d200bcf8363b2dc8f014784e0</a> </p><br />

    <p>4.     48699   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          A pyrimidine-pyrazolone nucleoside chimera with potent in vitro anti-orthopoxvirus activity</p>

    <p>          Fan, X, Zhang, X, Zhou, L, Keith, KA, Kern, ER, and Torrence, PF</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603351&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16603351&amp;dopt=abstract</a> </p><br />

    <p>5.     48700   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Anti-human immunodeficiency virus activity of 3,4,5-tricaffeoylquinic acid in cultured cells of lettuce leaves</p>

    <p>          Tamura, H, Akioka, T, Ueno, K, Chujyo, T, Okazaki, K, King, PJ, and Robinson, WE Jr</p>

    <p>          Mol Nutr Food Res <b>2006</b>.  50(4-5): 396-400</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16598806&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16598806&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48701   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Multidrug resistance-associated proteins 3, 4, and 5</p>

    <p>          Borst, P, de Wolf, C, and van de Wetering, K</p>

    <p>          Pflugers Arch <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16586096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16586096&amp;dopt=abstract</a> </p><br />

    <p>7.     48702   HIV-LS-346; EMBASE-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Discovery of TSAO derivatives with an unusual HIV-1 activity/resistance profile</p>

    <p>          de Castro, Sonia, Garcia-Aparicio, Carlos, Van Laethem, Kristel, Gago, Federico, Lobaton, Esther, De Clercq, Erik, Balzarini, Jan, Camarasa, Maria-Jose, and Velazquez, Sonsoles</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4JKHJ09-2/2/305658ad14c792e8ea7d0a0b855ad470">http://www.sciencedirect.com/science/article/B6T2H-4JKHJ09-2/2/305658ad14c792e8ea7d0a0b855ad470</a> </p><br />

    <p>8.     48703   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          LMP-420, a small-molecule inhibitor of TNF-alpha, reduces replication of HIV-1 and Mycobacterium tuberculosis in human cells</p>

    <p>          Haraguchi, S, Day, NK, Kamchaisatian, W, Beigier-Pompadre, M, Stenger, S, Tangsinmankong, N, Sleasman, JW, Pizzo, SV, and Cianciolo, GJ</p>

    <p>          AIDS Res Ther <b>2006</b>.  3(1): 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16573838&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16573838&amp;dopt=abstract</a> </p><br />

    <p>9.     48704   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Envelope conformational changes induced by human immunodeficiency virus type 1 attachment inhibitors prevent CD4 binding and downstream entry events</p>

    <p>          Ho, HT, Fan, L, Nowicka-Sans, B, McAuliffe, B, Li, CB, Yamanaka, G, Zhou, N, Fang, H, Dicker, I, Dalterio, R, Gong, YF, Wang, T, Yin, Z, Ueda, Y, Matiskella, J, Kadow, J, Clapham, P, Robinson, J, Colonno, R, and Lin, PF</p>

    <p>          J Virol <b>2006</b>.  80(8): 4017-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571818&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16571818&amp;dopt=abstract</a> </p><br />

    <p>10.   48705   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          HIV-1 coreceptors and their inhibitors</p>

    <p>          Ray, N and Doms, RW</p>

    <p>          Curr Top Microbiol Immunol <b>2006</b>.  303: 97-120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570858&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570858&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48706   HIV-LS-346; EMBASE-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Assembly of HIV-1 Vif-Cul5 E3 ubiquitin ligase through a novel zinc-binding domain-stabilized hydrophobic interface in Vif</p>

    <p>          Xiao, Zuoxiang, Ehrlich, Elana, Yu, Yunkai, Luo, Kun, Wang, Tao, Tian, Chunjuan, and Yu, Xiao-Fang</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JG5F83-1/2/4870947a61c5fb49722d430303d6886e">http://www.sciencedirect.com/science/article/B6WXR-4JG5F83-1/2/4870947a61c5fb49722d430303d6886e</a> </p><br />

    <p>12.   48707   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Potency of stampidine against multi-nucleoside reverse transcriptase inhibitor resistant human immunodeficiency viruses</p>

    <p>          Uckun, FM, Venkatachalam, TK, and Qazi, S</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(2a): 193-203</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570827&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570827&amp;dopt=abstract</a> </p><br />

    <p>13.   48708   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Large-scale synthesis and formulation of GMP-grade stampidine, a new anti-HIV agent</p>

    <p>          DuMez, D, Venkatachalam, TK, and Uckun, FM</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(2a): 136-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570822&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570822&amp;dopt=abstract</a> </p><br />

    <p>14.   48709   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Phosphorothioate oligonucleotides inhibit human immunodeficiency virus type 1 fusion by blocking gp41 core formation</p>

    <p>          Vaillant, A, Juteau, JM, Lu, H, Liu, S, Lackman-Smith, C, Ptak, R, and Jiang, S</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(4): 1393-401</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569857&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16569857&amp;dopt=abstract</a> </p><br />

    <p>15.   48710   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Development of Antiviral Fusion Inhibitors: Short Modified Peptides Derived from the Transmembrane Glycoprotein of Feline Immunodeficiency Virus</p>

    <p>          D&#39;Ursi, AM, Giannecchini, S, Esposito, C, Alcaro, MC, Sichi, O, Armenante, MR, Carotenuto, A, Papini, AM, Bendinelli, M, and Rovero, P</p>

    <p>          Chembiochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566046&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16566046&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   48711   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication in human primary cells by a dolabellane diterpene isolated from the marine algae Dictyota pfaffii</p>

    <p>          Cirne-Santos, CC, Teixeira, VL, Castello-Branco, LR, Frugulhetti, IC, and Bou-Habib, DC</p>

    <p>          Planta Med <b>2006</b>.  72(4): 295-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557468&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16557468&amp;dopt=abstract</a> </p><br />

    <p>17.   48712   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Triketoacid inhibitors of HIV-integrase: A new chemotype useful for probing the integrase pharmacophore</p>

    <p>          Walker, MA, Johnson, T, Ma, Z, Banville, J, Remillard, R, Kim, O, Zhang, Y, Staab, A, Wong, H, Torri, A, Samanta, H, Lin, Z, Deminie, C, Terry, B, Krystal, M, and Meanwell, N </p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546383&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16546383&amp;dopt=abstract</a> </p><br />

    <p>18.   48713   HIV-LS-346; PUBMED-HIV-4/17/2006</p>

    <p class="memofmt1-2">          Phenotypic drug resistance patterns in subtype A HIV-1 clones with nonnucleoside reverse transcriptase resistance mutations</p>

    <p>          Eshleman, SH, Jones, D, Galovich, J, Paxinos, EE, Petropoulos, CJ, Jackson, JB, and Parkin, N</p>

    <p>          AIDS Res Hum Retroviruses <b>2006</b>.  22(3): 289-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16545016&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16545016&amp;dopt=abstract</a> </p><br />

    <p>19.   48714   HIV-LS-346; WOS-HIV-4/9/2006</p>

    <p class="memofmt1-2">          Sulfated polymannuroguluronate, a novel anti-acquired immune deficiency syndrome drug candidate, blocks neuroinflammatory signalling by targeting the transactivator of transcription (Tat) protein</p>

    <p>          Hui, B, Xia, W, Li, J, Wang, LM, Ai, J, and Geng, MY</p>

    <p>          JOURNAL OF NEUROCHEMISTRY <b>2006</b>.  97(2): 334-344, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236328200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236328200003</a> </p><br />

    <p>20.   48715   HIV-LS-346; WOS-HIV-4/9/2006</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluations of novel oxindoles as HIV-1 non-nucleoside reverse transcriptase inhibitors. Part I</p>

    <p>          Jiang, T, Kuhen, KL, Wolff, K, Yin, H, Bieza, K, Caldwell, J, Bursulaya, B, Wu, TYH, and He, Y </p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(8): 2105-2108, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000009</a> </p><br />

    <p>21.   48716   HIV-LS-346; WOS-HIV-4/9/2006</p>

    <p class="memofmt1-2">          Abacavir prodrugs: Microwave-assisted synthesis and their evaluation of anti-HIV activities</p>

    <p>          Sriram, D, Yogeeswari, P, Myneedu, NS, and Saraswat, V</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(8): 2127-2129, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236319000014</a> </p><br />

    <p>22.   48717   HIV-LS-346; WOS-HIV-4/9/2006</p>

    <p class="memofmt1-2">          Toward a carbohydrate-based HIV-1 vaccine: Synthesis and immunological studies of oligomannose-containing glycoconjugates</p>

    <p>          Ni, JH, Song, HJ, Wang, YD, Stamatos, NM, and Wang, LX</p>

    <p>          BIOCONJUGATE CHEMISTRY <b>2006</b>.  17(2): 493-500, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236226200031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236226200031</a> </p><br />

    <p>23.   48718   HIV-LS-346; WOS-HIV-4/16/2006</p>

    <p class="memofmt1-2">          Identification of peptide ligands to the chemokine receptor CCR5 and their maturation by gene shuffling</p>

    <p>          Vyroubalova, EC, Hartley, O, Mermod, N, and Fisch, I</p>

    <p>          MOLECULAR IMMUNOLOGY <b>2006</b>.  43(10): 1573-1578, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236450900008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236450900008</a> </p><br />

    <p>24.   48719   HIV-LS-346; WOS-HIV-4/16/2006</p>

    <p class="memofmt1-2">          HIV-1 Nef protects human-monocyte-derived macrophages from HIV-1-induced apoptosis</p>

    <p>          Olivetta, E and Federico, M</p>

    <p>          EXPERIMENTAL CELL RESEARCH <b>2006</b>.  312(6): 890-900, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236459000018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236459000018</a> </p><br />

    <p>25.   48720   HIV-LS-346; WOS-HIV-4/16/2006</p>

    <p class="memofmt1-2">          Interaction between the HIV-1 protein Vpr and the adenine nucleotide translocator</p>

    <p>          Sabbah, EN, Druillennec, S, Morellet, N, Bouaziz, S, Kroemer, G, and Roques, BP</p>

    <p>          CHEMICAL BIOLOGY &amp; DRUG DESIGN <b>2006</b>.  67(2): 145-154, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236474300006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236474300006</a> </p><br />

    <p>26.   48721   HIV-LS-346; WOS-HIV-4/16/2006</p>

    <p class="memofmt1-2">          Pathogenic significance of alpha-N-acetylgalactosaminidase activity found in the envelope glycoprotein gp160 of human immunodeficiency virus type 1</p>

    <p>          Yamamoto, N</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2006</b>.  22(3): 262-271, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236520100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236520100007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
