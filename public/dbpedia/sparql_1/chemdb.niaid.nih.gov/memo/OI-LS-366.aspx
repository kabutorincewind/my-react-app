

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-366.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="F0gkZcjv7vU17BBABzGKYmx+mFw6atXbTpyNujx5bCWt4x3UXUwRaqShmjE/VRNMD4+GAenuvXOHJKyQOaEyMUOSGE85ST3xpTh1OjXT9DZB24kUQPT2t+tZSV8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E701E56" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-366-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59979   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p><b>          Antifungal activity of Brazilian medicinal plants involved in popular treatment of mycoses</b> </p>

    <p>          Cruz, MC, Santos, PO, Barbosa, AM Jr, de, Melo DL, Alviano, CS, Antoniolli, AR, Alviano, DS, and Trindade, RC</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17234376&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17234376&amp;dopt=abstract</a> </p><br />

    <p>2.     59980   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Antileishmanial and antifungal activity of plants used in traditional medicine in Brazil</p>

    <p>          Braga, FG, Bouzada, ML, Fabri, RL, de O, Matos M, Moreira, FO, Scio, E, and Coimbra, ES</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17234373&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17234373&amp;dopt=abstract</a> </p><br />

    <p>3.     59981   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Mechanism of thioamide drug action against tuberculosis and leprosy</p>

    <p>          Wang, F, Langley, R, Gulten, G, Dover, LG, Besra, GS, Jacobs, WR Jr, and Sacchettini, JC</p>

    <p>          J Exp Med <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17227913&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17227913&amp;dopt=abstract</a> </p><br />

    <p>4.     59982   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Comparison of the Proteome of Isoniazid-Resistant and -Susceptible Strains of Mycobacterium tuberculosis</p>

    <p>          Jiang, X, Zhang, W, Gao, F, Huang, Y, Lv, C, and Wang, H</p>

    <p>          Microb Drug Resist <b>2006</b>.  12(4): 231-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17227207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17227207&amp;dopt=abstract</a> </p><br />

    <p>5.     59983   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Structure and role of sialic acids on the surface of Aspergillus fumigatus conidiospores</p>

    <p>          Warwas, ML, Watson, JN, Bennet, AJ, and Moore, MM</p>

    <p>          Glycobiology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223648&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223648&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59984   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of new N-(2-hydroxy-4(or 5)-nitro/aminophenyl)benzamides and phenylacetamides as antimicrobial agents</p>

    <p>          Ertan, T, Yildiz, I, Ozkan, S, Temiz-Arpaci, O, Kaynak, F, Yalcin, I, Aki-Sener, E, and Abbasoglu, U</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223562&amp;dopt=abstract</a> </p><br />

    <p>7.     59985   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of new (2E,6E)-10-(dimethylamino)-3,7-dimethyl-2,6-decadien-1-ol ethers as inhibitors of human and Trypanosoma cruzi oxidosqualene cyclase</p>

    <p>          Galli, Ubaldina, Oliaro-Bosso, Simonetta, Taramino, Silvia, Venegoni, Serena, Pastore, Emanuele, Tron, Gian Cesare, Balliano, Gianni, Viola, Franca, and Sorba, Giovanni</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(1): 220-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M21SX1-F/2/60f8ef03f84fdf95749a8de8e768ffb5">http://www.sciencedirect.com/science/article/B6TF9-4M21SX1-F/2/60f8ef03f84fdf95749a8de8e768ffb5</a> </p><br />

    <p>8.     59986   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Studies on acyclic pyrimidines as inhibitors of mycobacteria</p>

    <p>          Srivastav, NC, Manning, T, Kunimoto, DY, and Kumar, R</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17218105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17218105&amp;dopt=abstract</a> </p><br />

    <p>9.     59987   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          The immunological selection of recombinant peptides from Cryptosporidium parvum reveals 14 proteins expressed at the sporozoite stage, 7 of which are conserved in other apicomplexa</p>

    <p>          Trasarti, Elisabetta, Pizzi, Elisabetta, Pozio, Edoardo, and Tosini, Fabio</p>

    <p>          Molecular and Biochemical Parasitology <b>2007</b>.  In Press, Uncorrected Proof: 781</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T29-4MS3J7C-1/2/a69076e76b6c7a70e8276f846094a6cb">http://www.sciencedirect.com/science/article/B6T29-4MS3J7C-1/2/a69076e76b6c7a70e8276f846094a6cb</a> </p><br />

    <p>10.   59988   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Structure-aided design of inhibitors of mycobacterium tuberculosis thymidylate kinase</p>

    <p>          Van Calenbergh, S</p>

    <p>          Verh K Acad Geneeskd Belg <b>2006</b>.  68(4): 223-248</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17214439&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17214439&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59989   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis secreted antigen (MTSA-10) modulates macrophage function by redox regulation of phosphatases</p>

    <p>          Basu, SK, Kumar, D, Singh, DK, Ganguly, N, Siddiqui, Z, Rao, KV, and Sharma, P</p>

    <p>          FEBS J <b>2006</b>.  273(24): 5517-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17212774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17212774&amp;dopt=abstract</a> </p><br />

    <p>12.   59990   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Peginterferon alfa-2b and ribavirin in patients with hepatitis C virus and decompensated cirrhosis: A controlled study</p>

    <p>          Iacobellis, Angelo, Siciliano, Massimo, Perri, Francesco, Annicchiarico, Brigida E, Leandro, Gioacchino, Caruso, Nazario, Accadia, Laura, Bombardieri, Giuseppe, and Andriulli, Angelo</p>

    <p>          Journal of Hepatology <b>2007</b>.  46(2): 206-212</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4M57P2H-3/2/85801f31e9b7cda8025ea6febc432afa">http://www.sciencedirect.com/science/article/B6W7C-4M57P2H-3/2/85801f31e9b7cda8025ea6febc432afa</a> </p><br />

    <p>13.   59991   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Peginterferon alfa-2b vs. peginterferon alfa-2a in the treatment of hepatitis C, is there any difference?</p>

    <p>          Sierra, Fernando</p>

    <p>          Journal of Hepatology <b>2007</b>.  46(2): 349-303</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4MFCNG9-B/2/c24176324b729f5d985a18bebe2423f0">http://www.sciencedirect.com/science/article/B6W7C-4MFCNG9-B/2/c24176324b729f5d985a18bebe2423f0</a> </p><br />

    <p>14.   59992   OI-LS-366; PUBMED-OI-1/22/2007</p>

    <p class="memofmt1-2">          Impact of Ribavirin Dose Reductions in Hepatitis C Virus Genotype 1 Patients Completing Peginterferon Alfa-2a/Ribavirin Treatment</p>

    <p>          Reddy, KR, Shiffman, ML, Morgan, TR, Zeuzem, S, Hadziyannis, S, Hamzeh, FM, Wright, TL, and Fried, M</p>

    <p>          Clin Gastroenterol Hepatol <b>2007</b>.  5(1): 124-129</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196435&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196435&amp;dopt=abstract</a> </p><br />

    <p>15.   59993   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Combination Therapy With Telaprevir and Pegylated Interferon Suppresses Both Wild-Type and Resistant Hepatitis C Virus</p>

    <p>          Lang, Les</p>

    <p>          Gastroenterology <b>2007</b>.  132(1): 5-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4MVVPPN-J/2/80283e7ff5d2f13a50116bfdbb35d071">http://www.sciencedirect.com/science/article/B6WFX-4MVVPPN-J/2/80283e7ff5d2f13a50116bfdbb35d071</a> </p><br />
    <br clear="all">

    <p>16.   59994   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Tolerability of Peg interferon-[alpha]2b and Ribavirin therapy in patients with chronic hepatitis C and glucose-6-phosphate dehydrogenase deficiency</p>

    <p>          Demelia, Luigi, Civolani, Alberto, Murgia, Debora, Murru, Alessandra, Sorbello, Orazio, and Rizzetto, Mario</p>

    <p>          Journal of Hepatology <b>2007</b>.  46(1): 171-173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4M9S95R-1/2/81b80420e939d7ce10dbf72fcdeb7ae2">http://www.sciencedirect.com/science/article/B6W7C-4M9S95R-1/2/81b80420e939d7ce10dbf72fcdeb7ae2</a> </p><br />

    <p>17.   59995   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Propagation of hepatitis C virus infection: Elucidating targets for therapeutic intervention</p>

    <p>          Kaplan, David E</p>

    <p>          Drug Discovery Today: Disease Mechanisms <b>2006</b>.  3(4): 471-477</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75D7-4M936NW-1/2/65dd669f0d90cbb9f1fc9a718af8d747">http://www.sciencedirect.com/science/article/B75D7-4M936NW-1/2/65dd669f0d90cbb9f1fc9a718af8d747</a> </p><br />

    <p>18.   59996   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Antiviral activity of berberine and related compounds against human cytomegalovirus</p>

    <p>          Hayashi, Kyoko, Minoda, Kazuki, Nagaoka, Yasuo, Hayashi, Toshimitsu, and Uesato, Shinichi</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof:  119</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MRG08K-C/2/0005b9ecec37db73eded851b4bfbc25c">http://www.sciencedirect.com/science/article/B6TF9-4MRG08K-C/2/0005b9ecec37db73eded851b4bfbc25c</a> </p><br />

    <p>19.   59997   OI-LS-366; WOS-OI-1/15/2007</p>

    <p class="memofmt1-2">          Gene expression profiling of monocyte-derived macrophages following infection with Mycobacterium avium subspecies avium and Mycobacterium avium subspecies paratuberculosis</p>

    <p>          Murphy, JT, Sommer, S, Kabara, EA, Verman, N, Kuelbs, MA, Saama, P, Halgren, R, and Coussens, PM</p>

    <p>          PHYSIOLOGICAL GENOMICS <b>2006</b>.  28(1): 67-75, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242832000009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242832000009</a> </p><br />

    <p>20.   59998   OI-LS-366; EMBASE-OI-1/21/2007</p>

    <p class="memofmt1-2">          Transfusion-Transmitted Cytomegalovirus: Lessons From a Murine Model</p>

    <p>          Roback, John D, Su, Leon, Zimring, James C, and Hillyer, Christopher D</p>

    <p>          Transfusion Medicine Reviews <b>2007</b>.  21(1): 26-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75B5-4MJSBRH-7/2/5d866bb5ea2654a2e11a44b769e67728">http://www.sciencedirect.com/science/article/B75B5-4MJSBRH-7/2/5d866bb5ea2654a2e11a44b769e67728</a> </p><br />

    <p>21.   59999   OI-LS-366; WOS-OI-1/15/2007</p>

    <p class="memofmt1-2">          TB therapy with new drugs</p>

    <p>          Duncan, K</p>

    <p>          DRUG METABOLISM REVIEWS <b>2006</b>.  38: 32-33, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239659200051">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239659200051</a> </p><br />
    <br clear="all">

    <p>22.   60000   OI-LS-366; WOS-OI-1/15/2007</p>

    <p class="memofmt1-2">          Drug-resistant tuberculosis</p>

    <p>          Nations, JA, Lazarus, AA, and Walsh, TE</p>

    <p>          DM DISEASE-A-MONTH <b>2006</b>.  52(11-12): 435-440, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243017800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243017800005</a> </p><br />

    <p>23.   60001   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Activity of 7-methyljuglone in combination with antituberculous drugs against Mycobacterium tuberculosis</p>

    <p>          Bapela, NB, Lall, N, Fourie, PB, Franzblau, SG, and Van Rensburg, CEJ</p>

    <p>          PHYTOMEDICINE <b>2006</b>.  13(9-10): 630-635, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242980800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242980800004</a> </p><br />

    <p>24.   60002   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          New derivatives of BM212: A class of antimycobacterial compounds based on the pyrrole ring as a scaffold</p>

    <p>          Biava, M, Porretta, GC, and Manetti, F</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY <b>2007</b>.  7(1): 65-78, 14</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243031200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243031200009</a> </p><br />

    <p>25.   60003   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Synthesis and microbiological evaluation of some N-methyl piperidone oxime ethers</p>

    <p>          Parthiban, P, Balasubramanian, S, Aridoss, G, and Kabilan, S</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH <b>2005</b>.  14(8-9): 523-538, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243027900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243027900006</a> </p><br />

    <p>26.   60004   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Synthesis and biological significance of 2-mercaptobenzoxazole derivatives</p>

    <p>          Srivastava, SK, Jain, A, and Srivastava, SD</p>

    <p>          JOURNAL OF THE INDIAN CHEMICAL SOCIETY <b>2006</b>.  83(11): 1118-1123, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243095200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243095200010</a> </p><br />

    <p>27.   60005   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Antitubercular nucleosides that inhibit siderophore biosynthesis: SAR of the glycosyl domain</p>

    <p>          Somu, RV, Wilson, DJ, Bennett, EM, Boshoff, HI, Celia, L, Beck, BJ, Barry, CE, and Aldrich, CC </p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(26): 7623-7635, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242974100010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242974100010</a> </p><br />

    <p>28.   60006   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Mycobacterium paratuberculosis, Mycobacterium smegmatis, and lipopolysaccharide induce different transcriptional and post-transcriptional regulation of the IRG1 gene in murine macrophages</p>

    <p>          Basler, T, Jeckstadt, S, Valentin-Weigand, P, and Goethe, R</p>

    <p>          JOURNAL OF LEUKOCYTE BIOLOGY <b>2006</b>.  79(3): 628-638, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243015000022</a> </p><br />
    <br clear="all">

    <p>29.   60007   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          In-vitro antibacterial, antifungal and cytotoxic activity of cobalt (II), copper (II), nickel (II) and zinc (II) complexes with furanylmethyl- and thienylmethyl-dithiolenes: [1, 3-dithiole-2-one and 1,3-dithiole-2-thione]</p>

    <p>          Chohan, ZH, Shaikh, AU, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(6): 733-740, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242937500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242937500013</a> </p><br />

    <p>30.   60008   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          In-vitro antibacterial, antifungal and cytotoxic properties of metal-based furanyl derived sulfonamides</p>

    <p>          Chohan, ZH, Shaikh, AU, Naseer, MM, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(6): 771-781, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242937500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242937500018</a> </p><br />

    <p>31.   60009   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          High-throughput screening of RNA polymerase inhibitors using a fluorescent UTP analog</p>

    <p>          Bhat, J, Rane, R, Solapure, SM, Sarkar, D, Sharma, U, Harish, MN, Lamb, S, Plant, D, Alcock, P, Peters, S, Barde, S, and Roy, RK</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2006</b>.  11(8): 968-976, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242940500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242940500008</a> </p><br />

    <p>32.   60010   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Development of a simple high-throughput screening protocol based on biosynthetic activity of Mycobacterium tuberculosis glutamine synthetase for the identification of novel inhibitors</p>

    <p>          Singh, U and Sarkar, D</p>

    <p>          JOURNAL OF BIOMOLECULAR SCREENING <b>2006</b>.  11(8): 1035-1042, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242940500015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242940500015</a> </p><br />

    <p>33.   60011   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          QSAR modeling of antifungal activity of some heterocyclic compounds</p>

    <p>          Ursu, O, Costescu, A, Diudea, MV, and Parv, B</p>

    <p>          CROATICA CHEMICA ACTA <b>2006</b>.  79(3): 483-488, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242869600020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242869600020</a> </p><br />

    <p>34.   60012   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          In vitro efficacy of Linezolid on clinical strains of Mycobacterium tuberculosis and other mycobacteria</p>

    <p>          Molicotti, P, Ortu, S, Bua, A, Cannas, S, Sechi, LA, and Zanetti, S</p>

    <p>          NEW MICROBIOLOGICA <b>2006</b>.  29(4): 275-280, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243029300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243029300007</a> </p><br />
    <br clear="all">

    <p>35.   60013   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Synthesis, characterization and antibacterial properties of rare earth (Ce3+, Pr3+, Nd3+, Sm3+, Er3+) complexes with L-aspartic acid and o-phenanthroline</p>

    <p>          Yu, H, He, QZ, Yang, J, and Zheng, WJ</p>

    <p>          JOURNAL OF RARE EARTHS <b>2006</b>.  24: 4-8, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242957500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242957500002</a> </p><br />

    <p>36.   60014   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Study of phenotypic and molecular characterization of Mycobacterium tuberculosis isolates resistant to both isoniazid and ethambutol</p>

    <p>          Zaki, MES and Goda, T</p>

    <p>          JOURNAL OF RAPID METHODS AND AUTOMATION IN MICROBIOLOGY <b>2006</b>.  14(4): 377-388, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243023700009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243023700009</a> </p><br />

    <p>37.   60015   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Combination nucleotide analogues in the treatment of acute fulminant hepatitis</p>

    <p>          Cope, A, Lo, W, and Kudesia, G</p>

    <p>          JOURNAL OF CLINICAL VIROLOGY <b>2006</b>.  36: S37-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240804100146">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000240804100146</a> </p><br />

    <p>38.   60016   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Determination of MICs of aminocandin for Candida spp. and filamentous fungi</p>

    <p>          Isham, N and Ghannoum, MA</p>

    <p>          JOURNAL OF CLINICAL MICROBIOLOGY <b>2006</b>.  44(12): 4342-4344, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242876300007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242876300007</a> </p><br />

    <p>39.   60017   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Building oligonucleotide therapeutics using non-natural chemistries</p>

    <p>          Wilson, C and Keefe, AD</p>

    <p>          CURRENT OPINION IN CHEMICAL BIOLOGY <b>2006</b>.  10(6): 607-614, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242919700013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242919700013</a> </p><br />

    <p>40.   60018   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Therapeutic potential of natural product signal transduction agents</p>

    <p>          Koehn, FE</p>

    <p>          CURRENT OPINION IN BIOTECHNOLOGY <b>2006</b>.  17(6): 631-637, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242936500012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242936500012</a> </p><br />

    <p>41.   60019   OI-LS-366; WOS-OI-1/21/2007</p>

    <p class="memofmt1-2">          Therapeutic peptides: technological advances driving peptides into development</p>

    <p>          Sato, AK, Viswanathan, M, Kent, RB, and Wood, CR</p>

    <p>          CURRENT OPINION IN BIOTECHNOLOGY <b>2006</b>.  17(6): 638-642, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242936500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242936500013</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
