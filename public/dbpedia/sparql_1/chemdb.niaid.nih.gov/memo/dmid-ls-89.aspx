

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-89.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="N6CSBxS9lvjHCeJH83bhdtrezjxlnkKsYlZnZFIjHk9UrOiRWQCRtlUP+zpav8JIi+ZA7k+3rJ6wjO1qvaqeJ9+MSNSmLh8HkfNpU2a5yeGB9/1t16+PjEDqpdw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="75107743" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-89-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     56277   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral properties of clinoptilolite</p>

    <p>          Grce, Magdalena and Pavelic, Kresimir</p>

    <p>          Microporous and Mesoporous Materials <b>2005</b>.  79(1-3): 165-169</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     56278   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design of translactam HCMV protease inhibitors as potent antivirals</p>

    <p>          Borthwick, AD</p>

    <p>          Med Res Rev <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15789440&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15789440&amp;dopt=abstract</a> </p><br />

    <p>3.     56279   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral cyclic D,L-a-peptides</p>

    <p>          Horne, WSeth,  Cui, Chunli, Wiethoff, Christopher M, Wilcoxen, Keith M, Amorin, Manuel, Nemerow, Glen R, and Ghadiri, MReza</p>

    <p>          229th ACS National Meeting <b>2005</b>.: ORGN-436</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     56280   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of cepharanthine against severe acute respiratory syndrome coronavirus in vitro</p>

    <p>          Zhang, CH, Wang, YF, Liu, XJ, Lu, JH, Qian, CW, Wan, ZY, Yan, XG, Zheng, HY, Zhang, MY, Xiong, S, Li, JX, and Qi, SY</p>

    <p>          Chin Med J (Engl) <b>2005</b>.  118(6): 493-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15788131&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15788131&amp;dopt=abstract</a> </p><br />

    <p>5.     56281   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Virally encoded RNAs as substrates, inhibitors, and delivery vehicles for RNAi and uses for antiviral therapy</p>

    <p>          Kowalik, Timothy F and Stadler, Bradford M</p>

    <p>          PATENT:  WO <b>2005019433</b>  ISSUE DATE:  20050303</p>

    <p>          APPLICATION: 2004  PP: 123 pp.</p>

    <p>          ASSIGNEE:  (University of Massachusetts, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     56282   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of indomethacin and indomethacin derivatives as broad-spectrum antiviral drugs, and corresponding pharmaceutical compositions</p>

    <p>          Santoro, Maria Gabriella</p>

    <p>          PATENT:  WO <b>2005013980</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 24 pp.</p>

    <p>          ASSIGNEE:  (Universita&#39; Degli Studi di Roma &#39;tor Vergata&#39;, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     56283   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Herpes simplex virus infection: part I--Biology, clinical presentation and latency</p>

    <p>          Yarom, N, Buchner, A, and Dayan, D</p>

    <p>          Refuat Hapeh Vehashinayim <b>2005</b>.  22(1): 7-15, 84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15786655&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15786655&amp;dopt=abstract</a> </p><br />

    <p>8.     56284   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus (HCV) NS5A protein downregulates HCV IRES-dependent translation</p>

    <p>          Kalliampakou, KI, Kalamvoki, M, and Mavromara, P</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 4): 1015-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784895&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15784895&amp;dopt=abstract</a> </p><br />

    <p>9.     56285   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ethanol extract of Polygonum cuspidatum inhibits hepatitis B virus in a stable HBV-producing cell line</p>

    <p>          Chang, JS, Liu, HW, Wang, KC, Chen, MC, Chiang, LC, Hua, YC, and Lin, CC</p>

    <p>          Antiviral Res <b>2005</b>.  66(1): 29-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781129&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781129&amp;dopt=abstract</a> </p><br />

    <p>10.   56286   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cranberry juice constituents affect influenza virus adhesion and infectivity</p>

    <p>          Weiss, EI, Houri-Haddad, Y, Greenbaum, E, Hochman, N, Ofek, I, and Zakay-Rones, Z</p>

    <p>          Antiviral Res <b>2005</b>.  66(1): 9-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781126&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781126&amp;dopt=abstract</a> </p><br />

    <p>11.   56287   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The efficacy of cidofovir treatment of mice infected with ectromelia (mousepox) virus encoding interleukin-4</p>

    <p>          Robbins, SJ, Jackson, RJ, Fenner, F, Beaton, S, Medveczky, J, Ramshaw, IA, and Ramsay, AJ</p>

    <p>          Antiviral Res <b>2005</b>.  66(1): 1-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781125&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15781125&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   56288   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Respiratory Syncytial Virus (RSV) Infection Induces Cyclooxygenase 2: A Potential Target for RSV Therapy</p>

    <p>          Richardson, JY, Ottolini, MG, Pletneva, L, Boukhvalova, M, Zhang, S, Vogel, SN, Prince, GA, and Blanco, JC</p>

    <p>          J Immunol <b>2005</b>.  174(7): 4356-64</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15778400&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15778400&amp;dopt=abstract</a> </p><br />

    <p>13.   56289   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Orthopoxvirus targets for the development of antiviral therapies</p>

    <p>          Prichard, MN and Kern, ER</p>

    <p>          Curr Drug Targets Infect Disord <b>2005</b>.  5(1): 17-28</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15777195&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15777195&amp;dopt=abstract</a> </p><br />

    <p>14.   56290   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Site-directed Mutagenesis and Kinetic Studies of the West Nile Virus NS3 Protease Identify Key Enzyme-Substrate Interactions</p>

    <p>          Chappell, Keith J, Nall, Tessa A, Stoermer, Martin J, Fang, Ning-Xia, Tyndall, Joel DA, Fairlie, David P, and Young, Paul R</p>

    <p>          Journal of Biological Chemistry <b>2005</b>.  280(4): 2896-2903</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   56291   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          siRNA targeting the Leader sequence of SARS-CoV inhibits virus replication</p>

    <p>          Li, T, Zhang, Y, Fu, L, Yu, C, Li, X, Li, Y, Zhang, X, Rong, Z, Wang, Y, Ning, H, Liang, R, Chen, W, Babiuk, LA, and Chang, Z</p>

    <p>          Gene Ther <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772689&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15772689&amp;dopt=abstract</a> </p><br />

    <p>16.   56292   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification of new flaviviruses in the Kokobera virus complex</p>

    <p>          Nisbet, Debra J, Lee, Katie J, van den Hurk, Andrew F, Johansen, Cheryl A, Kuno, Goro, Chang, Gwong-Jen J, Mackenzie, John S, Ritchie, Scott A, and Hall, Roy A</p>

    <p>          Journal of General Virology <b>2005</b>.  86(1): 121-124</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   56293   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of endoperoxides for the treatment of infections caused by flaviviridae, including hepatitis c, bovine viral diarrhea and classical swine fevervirus</p>

    <p>          Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, and Peys, Eric</p>

    <p>          PATENT:  US <b>20050059647</b>  ISSUE DATE: 20050317</p>

    <p>          APPLICATION: 2003-8648  PP: 6 pp.</p>

    <p>          ASSIGNEE:  (Belg.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   56294   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Topical Nmso3 Inhibits Adenovirus Replication in the Ad5/Nzw Rabbit Ocular Model.</p>

    <p>          Romanonski, E. <i>et al.</i></p>

    <p>          Investigative Ophthalmology &amp; Visual Science <b>2004</b>.  45: U597</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338001619">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223338001619</a> </p><br />

    <p>19.   56295   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Substituted 4H-pyrazolo[1,5-a]pyrimidin-7-ones as hepatitis C virus polymerase inhibitors</p>

    <p>          Deng, Yongqi,  Popovici-Muller, Janeta V, Shipps, Gerald W, Rosner, Kristin E, Wang, Tong, Curran, Patrick, Cooper, Alan B, Girijavallabhan, Viyyoor, Butkiewicz, Nancy, and Cable, Mickey</p>

    <p>          229th ACS National Meeting <b>2005</b>.: MEDI-340</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   56296   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pyrazolopyrimidines as inhibitors of HCV RNA polymerase</p>

    <p>          Popovici-Muller, Janeta V, Shipps, Gerald W, Rosner, Kristin E, Deng, Yongqi, Wang, Tong, Curran, Patrick, Brown, Meredith A, Cooper, Alan B, Cable, Mickey, Butkiewicz, Nancy, and Girijavallabhan, Viyyoor</p>

    <p>          229th ACS National Meeting <b>2005</b>.: MEDI-337</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   56297   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Biodefense - Unnoticed Amendment Bans Synthesis of Smallpox Virus</p>

    <p>          Enserink, M.</p>

    <p>          Science <b>2005</b>.  307(5715): 1540-1541</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227622400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227622400002</a> </p><br />

    <p>22.   56298   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Epitope mapping and biological function analysis of antibodies produced by immunization of mice with an inactivated Chinese isolate of severe acute respiratory syndrome-associated coronavirus (SARS-CoV)</p>

    <p>          Chou, TH, Wang, S, Sakhatskyy, PV, Mboudoudjeck, I, Lawrence, JM, Huang, S, Coley, S, Yang, B, Li, J, Zhu, Q, and Lu, S</p>

    <p>          Virology <b>2005</b>.  334(1): 134-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749129&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15749129&amp;dopt=abstract</a> </p><br />

    <p>23.   56299   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Acidic-Rearranged Compounds From Tingenone Derivatives and Their Biological Activity</p>

    <p>          Sotanaphun, U. <i>et al.</i></p>

    <p>          Pharmaceutical Biology <b>2005</b>.  43(1): 39-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227473400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227473400008</a> </p><br />
    <br clear="all">

    <p>24.   56300   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of bicyclic glycosides as antiviral agents for the treatment of infections caused by the Alphaherpesvirinae HSV-1 and HSV-2</p>

    <p>          Sas, Benedikt, Van Hemel, Johan, Vandenkerckhove, Jan, Peys, Eric, Van Der Eycken, Johan, and Van Hoof, Steven</p>

    <p>          PATENT:  US <b>2005059612</b>  ISSUE DATE:  20050317</p>

    <p>          APPLICATION: 2003-8602  PP: 15 pp.</p>

    <p>          ASSIGNEE:  (Belg.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   56301   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rethinking Smallpox</p>

    <p>          Weiss, M. <i>et al.</i></p>

    <p>          Clinical Infectious Diseases <b>2004</b>.  39(11): 1668-1673</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227492100019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227492100019</a> </p><br />

    <p>26.   56302   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p><b>          Method for prepn. of (3-R-1-adamantyl)-1-ethylamine hydrochlorides with antiviral activity</b> </p>

    <p>          Moiseev, IK, Makarova, NV, Pozdnyakov, VV, Galegov, GA, and Andronova, VL</p>

    <p>          PATENT:  RU <b>2247714</b>  ISSUE DATE: 20050310</p>

    <p>          APPLICATION: 2001-64252  PP: No pp. given</p>

    <p>          ASSIGNEE:  (Gosudarstvennoe Obrazovatel&#39;noe Uchrezhdenie Vysshego Professional&#39;nogo Obrazovaniya Samarskii Gosudarstvennyi Tekhnicheskii Universitet, Russia and NII Virusologii im. D. I. Ivanovskogo RAMN)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   56303   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro anti-HSV-2 activity and mechanism of action of proanthocyanidin A-1 from vaccinium vitis-idaea</p>

    <p>          Cheng, Hua-Yew, Lin, Ta-Chen, Yang, Chien-Min, Shieh, Den-En, and Lin, Chun-Ching</p>

    <p>          Journal of the Science of Food and Agriculture <b>2005</b>.  85(1 ): 10-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   56304   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Persistence of DNA in cell cultures may jeopardize the analysis of human herpesvirus 6 dynamics by means of real-time PCR</p>

    <p>          Bonnafous, P, Gautheret-Dejean, A, Boutolleau, D, Caiola, D, and Agut, H</p>

    <p>          J Virol Methods <b>2005</b>.  125(1): 95-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15737421&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15737421&amp;dopt=abstract</a> </p><br />

    <p>29.   56305   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Syntheses and anti-viral properties of N-6 substituted derivatives of L-like 4&#39;-deoxy-5&#39;-noraristeromycin and oxyamino and hydroxylamino carbanucleosides</p>

    <p>          Serbessa, Tesfaye, Roy, Atanu, Yang, Minmin, and Schneller, Stewart W</p>

    <p>          229th ACS National Meeting <b>2005</b>.: MEDI-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>30.   56306   DMID-LS-89; PUBMED-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of Some Pyrazoloquinolines as Inhibitors of Herpes Simplex Virus Type 1 Replication</p>

    <p>          Bekhit, AA, El-Sayed, OA, Aboul-Enein, HY, Siddiqui, YM, and Al-Ahdal, MN</p>

    <p>          Arch Pharm (Weinheim) <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736285&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15736285&amp;dopt=abstract</a> </p><br />

    <p>31.   56307   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of cisplatin derivatives of tilorone against reovirus ST3, vaccinia virus, varicella zoster virus (VZV), and herpes simplex virus (HSV-1)</p>

    <p>          Roner, Michael R, Carraher, Charles E Jr, and Dhanji, Salima</p>

    <p>          Polymeric Materials: Science and Engineering <b>2005</b>.  92: 499-501</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   56308   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiherpetic Activity of (Z)- and (E)-9-(3-Phosphonomethoxyprop-1-En-Yl)Adenines</p>

    <p>          Ivanov, A. <i>et al.</i></p>

    <p>          Russian Journal of Bioorganic Chemistry <b>2005</b>.  31(1): 58-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227321000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227321000007</a> </p><br />

    <p>33.   56309   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Purine and nonpurine pharmacological cyclin-dependent kinase inhibitors target initiation of viral transcription</p>

    <p>          Lacasse, Jonathan J, Provencher, Veronic MI, Urbanowski, Matthew D, and Schang, Luis M</p>

    <p>          Therapy <b>2005</b>.  2(1): 77-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   56310   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparison of the antiviral activities of alkoxyalkyl and alkyl esters of cidofovir against human and murine cytomegalovirus replication in vitro</p>

    <p>          Wan William B, Beadle, James R, Hartline, Caroll, Kern, Earl R, Ciesla, Stephanie L, Valiaeva, Nadejda, and Hostetler, Karl Y</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(2): 656-662</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   56311   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Replication-defective viral vectors derived from lymphotropic human herpesviruses 6 and 7 (HHV-6 or HHV-7), and vaccination uses</p>

    <p>          Frenkel, Niza</p>

    <p>          PATENT:  WO <b>2005012539</b>  ISSUE DATE:  20050210</p>

    <p>          APPLICATION: 2004  PP: 114 pp.</p>

    <p>          ASSIGNEE:  (Israel)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   56312   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Syntheses C-18 Dibenzoeyclooctadiene Lignan Derivatives as Anti-Hbsag and Anti-Hbeag Agents</p>

    <p>          Kuo, Y. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(5): 1555-1561</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227251300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227251300014</a> </p><br />
    <br clear="all">

    <p>37.   56313   DMID-LS-89; WOS-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Synthesis and Biological Evaluation of Two Analogues of the C-Riboside Showdomycin</p>

    <p>          Renner, J. <i>et al.</i></p>

    <p>          Australian Journal of Chemistry <b>2005</b>.  58(2): 86-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227184500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227184500003</a> </p><br />

    <p>38.   56314   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          C1 esterase inhibitor as a drug for treating viruses pathogenic to humans</p>

    <p>          Heimburger, Norbert and Gronski, Peter</p>

    <p>          PATENT:  WO <b>2005016375</b>  ISSUE DATE:  20050224</p>

    <p>          APPLICATION: 2004  PP: 27 pp.</p>

    <p>          ASSIGNEE:  (ZLB Behring G.m.b.H., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   56315   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The Adamantane-Derived Bananins Are Potent Inhibitors of the Helicase Activities and Replication of SARS Coronavirus</p>

    <p>          Tanner, Julian A, Zheng, Bo-Jian, Zhou, Jie, Watt, Rory M, Jiang, Jie-Qing, Wong, Kin-Ling, Lin, Yong-Ping, Lu, Lin-Yu, He, Ming-Liang, Kung, Hsiang-Fu, Kesel, Andreas J, and Huang, Jian-Dong</p>

    <p>          Chemistry &amp; Biology <b>2005</b>.  12(3): 303-311</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   56316   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sesquiterpene derivatives from Stachybotrys as antiviral medicines against HIV, BVDV, HCV, and CoV viruses</p>

    <p>          Sato, Akihiko and Kobayashi, Masanori</p>

    <p>          PATENT:  JP <b>2005060362</b>  ISSUE DATE:  20050310</p>

    <p>          APPLICATION: 2004-14136  PP: 17 pp.</p>

    <p>          ASSIGNEE:  (Shionogi and Co., Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   56317   DMID-LS-89; SCIFINDER-DMID-3/29/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of SARS-associated coronavirus (SCoV) infection and replication by RNA interference</p>

    <p>          Kung, Hsiang-Fu, He, Ming-Liang, Zheng, Bo-Jiang, Guan, Yi, Lin, Marie Chia-Mi, and Peng, Ying </p>

    <p>          PATENT:  US <b>2005004063</b>  ISSUE DATE:  20050106</p>

    <p>          APPLICATION: 2004-62305  PP: 31 pp.</p>

    <p>          ASSIGNEE:  (Peop. Rep. China)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
