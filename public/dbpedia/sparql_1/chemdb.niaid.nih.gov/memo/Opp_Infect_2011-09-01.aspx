

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-09-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dgmnEu+UVB6wLdqzeFhpW693TF9hqPX8dIlkkbT6bk5lZxVpplZlABTSbHKPd2m/uSEQkTGegHcQ/be0lW/qrYh/Q25pod4cnwd9MpFwVEWwXGOfxEClf5BaPPI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="60A9EFAB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">August 19 - September 1, 2011</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21849221">Synthesis of Novel 6-Phenyl-2,4-disubstituted pyrimidine-5-carbonitriles as Potential Antimicrobial Agents.</a> Al-Abdullah, E.S., A.R. Al-Obaid, O.A. Al-Deeb, E.E. Habib, and A.A. El-Emam, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4642-4647; PMID[21849221].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21712145">Synthesis and Antimicrobial Properties of Some New Thiazolyl coumarin Derivatives.</a> Arshad, A., H. Osman, M.C. Bagley, C.K. Lam, S. Mohamad, and A.S. Zahariluddin, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3788-3794; PMID[21712145].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21867554">Antimicrobial and Toxicological Activities of Five Medicinal Plant Species from Cameroon Traditional Medicine.</a> Assob, J.C., H.L. Kamga, D.S. Nsagha, A.L. Njunda, P.F. Nde, E.A. Asongalem, A.J. Njouendou, B. Sandjon, and V.B. Penlap, BMC Complementary and Alternative Medicine, 2011. 11(1): p. 70; PMID[21867554].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21872510">In Vitro Antimicrobial Activity of Maleic acid and Ethylenediaminetetraacetic acid on Endodontic Pathogens.</a> Ballal, N.V., P.P. Yegneswaran, K. Mala, and K.S. Bhat, Oral Surgery, Oral Medicine, Oral Pathology, Oral Radiology, and Endodontics, 2011. <b>[Epub ahead of print]</b>; PMID[21872510].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21770434">Cytotoxic Bipyridines from the Marine-Derived Actinomycete Actinoalloteichus cyanogriseus WH1-2216-6.</a> Fu, P., S. Wang, K. Hong, X. Li, P. Liu, Y. Wang, and W. Zhu, Journal of Natural Products, 2011. 74(8): p. 1751-1756; PMID[21770434].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21849051">Evaluation of Abelmoschus moschatus Extracts for Antioxidant, Free Radical Scavenging, Antimicrobial and Antiproliferative Activities Using in Vitro Assays.</a> Gul, M.Z., L.M. Bhakshu, F. Ahmad, A.K. Kondapi, I.A. Qureshi, and I.A. Ghazi, Bmc Complementary and Alternative Medicine, 2011. 11(1): p. 64; PMID[21849051]. <b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21741732">Synthesis and Identification of beta-Aryloxyquinolines and Their Pyrano[3,2-c]chromene Derivatives as a New Class of Antimicrobial and Antituberculosis agents.</a> Mungra, D.C., M.P. Patel, D.P. Rajani, and R.G. Patel, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4192-4200; PMID[21741732]. <b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21794959">Synthesis and studies of novel 2-(4-cyano-3-trifluoromethylphenyl amino)-4-(quinoline-4-yloxy)-6-(piperazinyl/piperidinyl)-s-triazines as potential antimicrobial, antimycobacterial and anticancer agents.</a> Patel, R.V., P. Kumari, D.P. Rajani, and K.H. Chikhalia, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4354-4365; PMID[21794959].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21761939">Isolation, Structure, and Biological Activities of Fellutamides C and D from an Undescribed Metulocladosporiella (Chaetothyriales) Using the Genome-Wide Candida albicans Fitness Test.</a> Xu, D., J. Ondeyka, G.H. Harris, D. Zink, J.N. Kahn, H. Wang, G. Bills, G. Platas, W. Wang, A.A. Szewczak, P. Liberator, T. Roemer, and S.B. Singh, Journal of Natural Products, 2011. 74(8): p. 1721-1730; PMID[21761939].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21641699">Design, Synthesis and Antimicrobial Activity of Chiral 2-(Substituted-hydroxyl)-3-(benzo[d]oxazol-5-yl)propanoic acid Derivatives.</a> Zhang, W., W. Liu, X. Jiang, F. Jiang, H. Zhuang, and L. Fu, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3639-3650; PMID[21641699].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21794961">Synthesis of Novel Fluconazoliums and Their Evaluation for Antibacterial and Antifungal Activities.</a> Zhang, Y.Y., J.L. Mi, C.H. Zhou, and X.D. Zhou, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4391-402; PMID[21794961].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OI_0819-090111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

     <p> </p>

     <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21839549">A Facile Four-component Sequential Protocol in the Expedient Synthesis of Novel 2-Aryl-5-methyl-2,3-dihydro-1H-3-pyrazolones in Water and Their Antitubercular Evaluation.</a> Gunasekaran, P., S. Perumal, P. Yogeeswari, and D. Sriram, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4530-4536; PMID[21839549].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21622020">Antitubercular and Fluorescence Studies of Copper(II) Complexes with Quinolone family Member, Ciprofloxacin.</a> Kharadi, G.J., Spectrochimica acta. Part A, Molecular and Biomolecular Spectroscopy, 2011. 79(5): p. 898-903; PMID[21622020].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21641695">Design, Synthesis and anti-Tubercular Evaluation of New 2-Acylated and 2-Alkylated amino-5-(4-(benzyloxy)phenyl)thiophene-3-carboxylic acid Derivatives. Part 1.</a> Lu, X., B. Wan, S.G. Franzblau, and Q. You, European Journal of Medicinal Chemistry, 2011. 46(9): p. 3551-3563; PMID[21641695].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21571059">Antimycobacterial Evaluation and Preliminary Phytochemical Investigation of Selected Medicinal Plants Traditionally Used in Mozambique.</a> Luo, X., D. Pires, J.A. Ainsa, B. Gracia, S. Mulhovo, A. Duarte, E. Anes, and M.J. Ferreira, Journal of Ethnopharmacology, 2011. 137(1): p. 114-120; PMID[21571059].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21764184">Novel Aryloxy azolyl chalcones with Potent Activity against Mycobacterium tuberculosis H37Rv.</a> Marrapu, V.K., V. Chaturvedi, S. Singh, S. Sinha, and K. Bhandari, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4302-4310; PMID[21764184].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21664731">Synthesis and Screening of Galactose-linked Nitroimidazoles and Triazoles against Mycobacterium tuberculosis.</a> Mugunthan, G., K. Ramakrishna, D. Sriram, P. Yogeeswari, and K.P. Ravindranathan Kartha, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4725-32; PMID[21664731].</p>

     <p class="plaintext"><b>PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876482">Antibacterial Activity of Aristolochia brevipes against Multidrug-Resistant Mycobacterium tuberculosis.</a> Navarro-Garcia, V.M., J. Luna-Herrera, M.G. Rojas-Bribiesca, P. Alvarez-Fitz, and M.Y. Rios, Molecules (Basel, Switzerland), 2011. 16(9): p. 7357-7364; PMID[21876482].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21875763">Ruthenium(II) phosphine/diimine/picolinate Complexes: Inorganic Compounds as Agents against Tuberculosis.</a> Pavan, F.R., G.V. Poelhsitz, M.I. Barbosa, S.R. Leite, A.A. Batista, J. Ellena, L.S. Sato, S.G. Franzblau, V. Moreno, D. Gambino, and C.Q. Leite, European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21875763].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21786803">Design, Synthesis, and Biological Evaluation of Triazolo-pyrimidine Derivatives as Novel Inhibitors of Hepatitis B Virus Surface Antigen (HBsAg) Secretion.</a> Yu, W., C. Goddard, E. Clearfield, C. Mills, T. Xiao, H. Guo, J.D. Morrey, N.E. Motter, K. Zhao, T.M. Block, A. Cuconati, and X. Xu, Journal of Medicinal Chemistry, 2011. 54(16): p. 5660-5670; PMID[21786803].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

     <p> </p>

     <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21824783">Synthesis and Antimalarial Evaluation of Novel Benzopyrano[4,3-b]benzopyran Derivatives.</a> Devakaram, R., D.S. Black, K.T. Andrews, G.M. Fisher, R.A. Davis, and N. Kumar, Bioorganic &amp; Medicinal Chemistry, 2011. 19(17): p. 5199-5206; PMID[21824783].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21854078">Structure-Activity Relationships of 4-Position Diamine Quinoline Methanols as Intermittent Preventative Treatment (IPT) against Plasmodium falciparum.</a> Milner, E., S. Gardner, J. Moon, K. Grauer, J. Auschwitz, I. Bathurst, D. Caridha, L. Gerena, M. Gettayacamin, J. Johnson, M. Kozar, P. Lee, S. Leed, Q. Li, W. McCalmont, V. Melendez, N. Roncal, R. Sciotti, B. Smith, J. Sousa, A. Tungtaeng, P. Wipf, and G. Dow, Journal of medicinal chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21854078].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21879088">Synthesis, Structures and anti-Malaria Activity of Some Gold(i) phosphine complexes Containing Seleno- and Thiosemicarbazonato Ligands.</a> Molter, A., J. Rust, C.W. Lehmann, G. Deepa, P. Chiba, and F. Mohr, Dalton Transactions (Cambridge, England : 2003), 2011. <b>[Epub ahead of print]</b>; PMID[21879088].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876053">In Vitro Activity against Plasmodium falciparum of Antiretroviral Drugs.</a> Nsanzabana, C. and P.J. Rosenthal, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21876053].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21501696">Antimalarial Activity of Endoperoxide Compound 6-(1,2,6,7-Tetraoxaspiro[7.11]nonadec-4-yl)hexan-1-ol.</a> Sato, A., A. Hiramoto, M. Morita, M. Matsumoto, Y. Komich, Y. Nakase, N. Tanigawa, O. Hiraoka, K. Hiramoto, H. Hayatsu, K. Higaki, S. Kawai, A. Masuyama, M. Nojima, Y. Wataya, and H.S. Kim, Parasitology international, 2011. 60(3): p. 270-273; PMID[21501696].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21848332">Antimalarial Activities of New Guanidylimidazole and Guanidylimidazoline Derivatives.</a> Zhang, L., R. Sathunuru, D. Caridha, B. Pybus, M.T. O&#39;Neil, M.P. Kozar, and A.J. Lin, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21848332].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p> </p>

     <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

     <p> </p>

     <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21670183">The 8-Aminoquinoline Analogue Sitamaquine Causes Oxidative Stress in Leishmania donovani Promastigotes by Targeting Succinate Dehydrogenase.</a> Carvalho, L., J.R. Luque-Ortega, C. Lopez-Martin, S. Castanys, L. Rivas, and F. Gamarro, Antimicrobial Agents and Chemotherapy, 2011. 55(9): p. 4204-4210; PMID[21670183].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21771652">Antileishmanial Sesquiterpene lactones from Pseudelephantopus spicatus, a Traditional Remedy from the Chayahuita Amerindians (Peru). Part III</a>. Odonne, G., G. Herbette, V. Eparvier, G. Bourdy, R. Rojas, M. Sauvain, and D. Stien, Journal of Ethnopharmacology, 2011. 137(1): p. 875-879; PMID[21771652].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21866394">Increasing the Activity of Copper(II) Complexes against Leishmania Through Lipophilicity and Pro-oxidant Ability.</a> Portas, A.D., D.C. Miguel, J.K. Yokoyama-Yasunaka, S.R. Uliana, and B.P. Esposito, Journal of biological inorganic chemistry : JBIC : A Publication of the Society of Biological Inorganic Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21866394].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21775030">Synthesis, Evaluation against Leishmania amazonensis and Cytotoxicity Assays in Macrophages of Sixteen New Congeners Morita-Baylis-Hillman Adducts.</a> Silva, F.P., P.A. de Assis, C.G. Junior, N.G. de Andrade, S.M. da Cunha, M.R. Oliveira, and M.L. Vasconcellos, European Journal of Medicinal Chemistry, 2011. 46(9): p. 4295-4301; PMID[21775030].</p>

     <p class="plaintext"><b>[PubMed]</b>. OI_0819-090111.</p>

     <p> </p>

     <p> </p>

     <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293632600012">Effects of Culture Filtrates of Endophytic Fungi Obtained from Piper aduncum L. on the Growth of Mycobacterium tuberculosis.</a> de Lima, A.M., J.I. Salem, J.V.B. de Souza, A.C.A. Cortez, C.M. Carvalho, F.C.M. Chaves, and V.F. da Veiga, Electronic Journal of Biotechnology, 2011. 14(4); ISI[000293632600012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293756600020">A New Acylated triterpene with Antimicrobial Activity from the Leaves of Rauvolfia vomitoria.</a> Fannang, S.V., V. Kuete, C.D. Mbazoa, J.I. Momo, T.V.D. Hanh, F. Tillequin, E. Seguin, E. Chosson, and J. Wandji, Chemistry of Natural Compounds, 2011. 47(3): p. 404-407; ISI[000293756600020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293695300024">Antimicrobial Potential of Xylaria polymorpha (Pers.) Grev.</a> Hacioglu, N., I. Akata, and B. Dulger, African Journal of Microbiology Research, 2011. 5(6): p. 728-730; ISI[000293695300024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293296500020">Synthesis, Spectral, Theoretical, and Antimicrobial Screening of Some Heterocyclic oximes.</a> Jayabharathi, J., A. Manimekalai, and M. Padmavathy, Medicinal Chemistry Research, 2011. 20(7): p. 981-995; ISI[ 000293296500020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293802900006">Investigation of the Antimicrobial Activity of Rhaponticum (Rhaponticum Carthamoides DC Iljin) and Shrubby Cinquefoil (Potentilla Fruticosa L.).</a> Jurkstiene, V., A. Pavilonis, D. Garsviene, A. Juozulynas, L. Samsoniene, D. Dauksiene, K. Jankauskiene, G. Simoniene-Kazlauskiene, and E. Stankevicius, Medicina-Lithuania, 2011. 47(3): p. 174-179; ISI[000293802900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293296500007">Synthesis and Evaluation of in Vitro Antitubercular Activity and Antimicrobial activity of Some Novel 4H-Chromeno 2,3-d pyrimidine via 2-amino-4-phenyl-4H-chromene-3-carbonitriles.</a> Kamdar, N.R., D.D. Haveliwala, P.T. Mistry, and S.K. Patel, Medicinal Chemistry Research, 2011. 20(7): p. 854-864; ISI[000293296500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293695300012">Screening of Pogostemon parviflorus Benth. for anti-Candida Activity.</a> Najafi, S. and B. Sadeghi-Nejad, African Journal of Microbiology Research, 2011. 5(6): p. 657-660; ISI[000293695300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293696900020">Non-steroidal Anti-inflammatory Drugs (NSAIDs) Inhibit Growth of Yeast Pathogens.</a> Ncango, D.M., C.H. Pohl, P.W.J. van Wyk, and J.L.F. Kock, African Journal of Microbiology Research, 2011. 5(9): p. 1123-1125; ISI[000293696900020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293696900014">Antimicrobial and Nematicidal Screening of Anamorphic Fungi Isolated from Plant Debris of Tropical Areas in Mexico.</a> Reyes-Estebanez, M., E. Herrera-Parra, J. Cristobal-Alejo, G. Heredia-Abarca, B. Canto-Canche, I. Medina-Baizabal, and M. Gamboa-Angulo, African Journal of Microbiology Research, 2011. 5(9): p. 1083-1089; ISI[000293696900014].</p>

    <p class="v"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293355000014">Potential Fungal Inhibition by Immobilized Hydrolytic Enzymes from Trichoderma asperellum.</a> Silva, B.D.S., C.J. Ulhoa, K.A. Batista, F. Yamashita, and K.F. Fernandes, Journal of agricultural and food chemistry, 2011. 59(15): p. 8148-8154; ISI[000293355000014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293298300013">Evaluation of Cinnamomum tamala Oil and Its Phenylpropanoid Eugenol for Their Antifungal and Antiaflatoxigenic Activity.</a> Srivastava, B., A. Sagar, and N.K. Dubey, Food Analytical Methods, 2011. 4(3): p. 347-356; ISI[000293298300013].</p>

    <p class="plaintext"> <b>[WOS]</b>. OI_0819-090111.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293630800013">Synthesis, Characterization and Antimicrobial Activity of 3,5-di-tert-Butylsalicylaldehyde-S-methylthiosemicarbazones and Their Ni(II) Complexes.</a> Turkkan, B., B. Sariboga, and N. Sariboga, Transition Metal Chemistry, 2011. 36(6): p. 679-684; ISI[000293630800013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0819-090111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
