

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-03-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gyl/mvdDr/ZB2RpkJSQzn0cxCB0JWjpoUhYTNATgsYlrG1Byq2N49Nw/bkQtvPSAtrCymNFuRYaZaRrHV5yNrP2HTVAngk6OK/ihr8tPhzyfvFjIA46dpx3dBGw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="41A1E5C1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 13 - March 26, 2015</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25704089">The JAK2 Inhibitor AZD1480 Inhibits Hepatitis A Virus Replication in Huh7 Cells.</a> Jiang, X., T. Kanda, S. Nakamoto, K. Saito, M. Nakamura, S. Wu, Y. Haga, R. Sasaki, N. Sakamoto, H. Shirasawa, H. Okamoto, and O. Yokosuka. Biochemical and Biophysical Research Communications, 2015. 458(4): p. 908-912. PMID[25704089].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0313-032615.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25754231">Synthesis of bis-Macrocyclic HCV Protease Inhibitor MK-6325 via Intramolecular sp(2)-sp(3) Suzuki-Miyaura Coupling and Ring Closing Metathesis.</a> Li, H., J.P. Scott, C.Y. Chen, M. Journet, K. Belyk, J. Balsells, B. Kosjek, C.A. Baxter, G.W. Stewart, C. Wise, M. Alam, Z.J. Song, and L. Tan. Organic Letters, 2015. 17(6): p. 1533-1536. PMID[25754231].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0313-032615.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25702849">Novel Fused Tetrathiocines as Antivirals That Target the Nucleocapsid Zinc Finger Containing Protein of the Feline Immunodeficiency Virus (FIV) as a Model of HIV Infection.</a> Asquith, C.R., M.L. Meli, L.S. Konstantinova, T. Laitinen, A. Poso, O.A. Rakitin, R. Hofmann-Lehmann, K. Allenspach, and S.T. Hilton. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1352-1355. PMID[25702849].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0313-032615.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25789502">Polyoxygenated Cembrane Diterpenoids from the Soft Coral Sarcophyton ehrenbergi.</a> Cheng, S.Y., S.K. Wang, M.K. Hsieh, and C.Y. Duh. International Journal of Molecular Sciences, 2015. 16(3): p. 6140-6152. PMID[25789502].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0313-032615.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25783941">Synergistic Effects of Acyclovir and 3, 19-Isopropylideneandrographolide on Herpes Simplex Virus Wild Types and Drug-resistant Strains.</a> Priengprom, T., T. Ekalaksananan, B. Kongyingyoes, S. Suebsasana, C. Aromdee, and C. Pientong. BMC Complementary and Alternative Medicine, 2015. 15(56):  8pp. PMID[25783941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0313-032615.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348730500064">Drug-drug Interactions of Carbamazepine with the HCV Direct Acting Antiviral (DAA) Combination of ABT-450/R, Ombitasvir and Dasabuvir.</a> Badri, P., S. Dutta, A. Asatryan, H. Wang, I. Podsadecki, W. Awni, and R. Menon. Clinical Pharmacology &amp; Therapeutics, 2015. 97: p. S23-S24. ISI[000348730500064].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348730500068">Drug-drug Interactions of Digoxin with the HCV Direct Acting Antiviral (DAA) Combination of ABT-450/R, Ombitasvir and Dasabuvir.</a> Badri, P., S. Dutta, L. Rodrigues, B. Ding, T. Podsadecki, W. Awni, and R. Menon. Clinical Pharmacology &amp; Therapeutics, 2015. 97: p. S25-S25. ISI[000348730500068].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167403198">Potent Hepatitis C Virus NS5A Inhibitors; Discovery of BMK-20113.</a> Bae, I.H. and B.M. Kim. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #613. ISI[000349167403198].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167402206">Novel Naphthalene Derivatives as Potent HCV NS5A Inhibitors: Design, Synthesis, SAR, and Optimization Studies.</a> Baskaran, S., W. Kazmierski, M.S. Duan, S. Dickerson, J. Cooper, R. Grimes, and J. Walker. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #407. ISI[000349167402206].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167402192">ProTides of N-(3-(5-(2 &#39;-Deoxyuridine))prop-2-ynyl)octanamide Phosphate, a Potent and Selective Inhibitor of Thymidylate Synthase X from Mycobacterium Tuberculosis, as Potential Anti-tubercular and Anti-viral Agents.</a> Ferrari, V., C. McGuigan, M. Derudas, B. Gonczy, K. Hinsinger, S. Kandil, F. Pertusati, M. Serpi, R. Snoeck, G. Andrei, J. Balzarini, T.D. McHugh, A. Maitra, E. Akorli, D. Evangelopoulos, and S. Bhakta. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #392. ISI[000349167402192].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167401719">Discovery of Inhibitors of the Hepatitis C Virus Using a Cell-based Infection Assay.</a> He, S.S., K.L. Li, Z.Y. Hu, M. Ferrer, N. Southall, B. Lin, J.B. Xiao, X. Hu, W. Zheng, J.J. Marugan, J. Aube, F.J. Schoenen, K.J. Frankowski, and T.J. Liang. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #13. ISI[000349167401719].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167402219">HCV NS5A Replication Complex Inhibitors: Effects of Substituted Pyrrolidines in Balancing Genotype 1a/1b Potency.</a> Henderson, J.A., B.C. Liu, S. Giroux, T.J. Reddy, M. Bubenik, C. Cadilhac, L. Vaillancourt, G. Falardeau, C. Poisson, O. Pereira, K.M. Cottrell, M. Morris, O. Nicolas, D. Bilimoria, N. Mani, N. Ewing, R. Shawgo, L. L&#39;Heureux, S. Selliah, A.L. Grillot, Y.L. Bennani, and J.P. Maxwell. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #421. ISI[000349167402219].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167401784">Discovery of a Novel Lactam-containing Non-nucleoside Class of HCV NS5B Inhibitors.</a> Li, P., J. Green, D. Lauffer, Q. Tang, L. Chan, C. Poisson, J. Court, N. Waal, S. Ronkin, S. Nanthakumar, S.K. Das, C. Yannopoulos, W. Dorsch, N. Mani, D. Bilimoria, R. Shawgo, G. Rao, O. Nicolas, N. Chauret, S. Selliah, and F. Denis. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #81. ISI[000349167401784].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348457602389">Antiviral Drug Discovery and Development: Progress of the HCV Inhibitors Asunaprevir and BMS-791325 and the HIV-1 Attachment Inhibitor BMS-663068.</a> Meanwell, N.A. Abstracts of Papers of the American Chemical Society, 2014. 247: Poster #224. ISI[000348457602389].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br />  

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348730500066">Pharmacokinetic Interaction of HCV NS3/4A Protease Inhibitor Vaniprevir and Rosuvastatin.</a> Orito, Y., T. Iwasa, N. Uemura, G. Fujimoto, S. Yama, W. Gao, L. Caro, C. Fandozzi, T. Prucksaritanont, M. Anderson, J. Butterton, and S. Hasegawa. Clinical Pharmacology &amp; Therapeutics, 2015. 97: p. S24-S25. ISI[000348730500066].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348730500240">Drug-drug Interactions of Omeprazole with the HCV Direct Acting Antiviral (DAA) Combination of ABT-450/R, Ombitasvir and Dasabuvir.</a> Polepally, A.R., S. Dutta, T. Baykal, B. Hu, T.J. Podsadecki, W.M. Awni, and R.M. Menon. Clinical Pharmacology &amp; Therapeutics, 2015. 97: p. S77-S78. ISI[000348730500240].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349167402478">Discovery of Novel Catalytic Technologies in the Process Development of HCV Polymerase Inhibitors.</a> Shekhar, S. Abstracts of Papers of the American Chemical Society, 2014. 248: Poster #9. ISI[000349167402478].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349979500029">17Beta-Estradiol Inhibits Hepatitis C (HCV) Infection via Binding to its Receptor.</a> Giarda, P., A. Magri, C.Z. Foglia, E. Boccato, E.M. Burlone, R. Minisini, E. Grossini, and M. Pirisi. Digestive and Liver Disease, 2015. 47: p. E14-E15. ISI[000349979500029].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350112600003">Feleucin-BO1: A Novel Antimicrobial Non-apeptide amide from the Skin Secretion of the Toad, Bombina orientalis, and Design of a Potent Broad-spectrum Synthetic Analogue, Feleucin-K3.</a> Hou, X.J., Q. Du, R.J. Li, M. Zhou, H. Wang, L. Wang, C. Guo, T.B. Chen, and C. Shaw. Chemical Biology &amp; Drug Design, 2015. 85(3): p. 259-267. ISI[000350112600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349881300005">Anti-Hepatitis B Virus Active Constituents from Swertia chirayita.</a> Zhou, N.J., C.A. Geng, X.Y. Huang, Y.B. Ma, X.M. Zhang, J.L. Wang, and J.J. Chen. Fitoterapia, 2015. 100: p. 27-34. ISI[000349881300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0313-032615.</p>

    <h2>Special Search Request Citations</h2>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24133297">Pharmacokinetic and Pharmacodynamic Properties of GS-9620, a Novel Toll-Like Receptor 7 Agonist, Demonstrate Interferon-stimulated Gene Induction without Detectable Serum Interferon at Low Oral Doses.</a> Fosdick, A., J. Zheng, S. Pflanz, C.R. Frey, J. Hesselgesser, R.L. Halcomb, G. Wolfgang, and D.B. Tumas. The Journal of Pharmacology and Experimental Therapeutics, 2014. 348(1): p. 96-105. PMID[24133297].</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23961878">Identification and Optimization of Pteridinone Toll-Like Receptor 7 (TLR7) Agonists for the Oral Treatment of Viral Hepatitis.</a> Roethle, P.A., R.M. McFadden, H. Yang, P. Hrvatin, H. Hui, M. Graupe, B. Gallagher, J. Chao, J. Hesselgesser, P. Duatschek, J. Zheng, B. Lu, D.B. Tumas, J. Perry, and R.L. Halcomb. Journal of Medicinal Chemistry, 2013. 56(18): p. 7324-7333. PMID[23961878].</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/">Pharmaceutical Combinations Comprising a Thionucleotide Analog and other Agents for Treating a Hepatitis C Virus Infection.</a> Blatt, L.M., L. Beigelman, H. Tan, J.A. Symons, D.B. Smith, M. Jiang, T.L. Kieffer, and C.A. Rijnbrand. Patent. 2013. 2013-US30594 2013142157: 406pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/">Preparation of Pyridazinylmethylimidazopyridine Derivatives and Analogs for Use in the Treatment of Hepatitis C Virus using Combination Chemotherapy.</a> Delaney, W.E., J.O. Link, H. Mo, D.W. Oldach, A.S. Ray, W.J. Watkins, C.Y. Yang, and W. Zhong. Patent. 2012. 2011-US64017 2012087596: 143pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/">Preparation of Fatty acid Antiviral Conjugates.</a> Milne, J.C., M.R. Jirousek, C.B. Vu, A. Wensley, and A. Ting. Patent. 2013. 2012-US69229 2013090420: 264pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/">Preparation of Pyridazinylmethylimidazopyridine Derivatives and Analogs for Use in the Treatment of Hepatitis C Virus Using Combination Chemotherapy.</a> Ray, A.S., W.J. Watkins, J.O. Link, D.W. Oldach, and W.E.I.V. Delaney. Patent. 2013. 2012-US55621 2013040492: 159pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/">Preparation of Substituted Nucleosides, Nucleotides and Analogs thereof as Protease and Polymerase Inhibitors for Treatment of HCV Infection.</a> Smith, D.B., L. Beigelman, G. Wang, and M.H. Welch. Patent. 2014. 2013-US76727 2014100498: 148pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/">Preparation of Substituted Phosphorothioate Nucleotide Analogs as Antiviral Agents.</a> Wang, G. and L. Beigelman. Patent. 2013. 2012-US71064 2013096680: 167 pp.</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25192394">Determinants of Activity at Human Toll-like Receptors 7 and 8: Quantitative Structure-Activity Relationship (QSAR) of Diverse Heterocyclic Scaffolds.</a> Yoo, E., D.B. Salunke, D. Sil, X. Guo, A.C. Salyer, A.R. Hermanson, M. Kumar, S.S. Malladi, R. Balakrishna, W.H. Thompson, H. Tanji, U. Ohto, T. Shimizu, and S.A. David. Journal of Medicinal Chemistry, 2014. 57(19): p. 7955-7970. PMID[25192394].</p>

    <p class="plaintext"><b>[Special Search]</b>. OV_0313-032615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
