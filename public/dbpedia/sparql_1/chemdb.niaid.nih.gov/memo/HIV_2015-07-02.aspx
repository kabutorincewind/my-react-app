

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2015-07-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7KjFgeCc22CudLTvM3ZC3Ua8el+wn1v8A5osiOr4AWtvRGeMbnwyW+wqF+tiikVOswwix5gD5sY5TjmySaholojsotWkkURBymHQHiFx9wGgCkze0bAtsv85WVo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="245A04AE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 19 - July 2, 2015</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25970729">Limonoids with anti-HIV Activity from Cipadessa cinerascens.</a> Yu, J.H., G.C. Wang, Y.S. Han, Y. Wu, M.A. Wainberg, and J.M. Yue. Journal of Natural Products, 2015. 78(6): p. 1243-1252. PMID[25970729].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=26056259">Toe1 Is an Inhibitor of HIV-1 Replication with Cell-penetrating Capability.</a> Sperandio, S., C. Barat, M.A. Cabrita, A. Gargaun, M.V. Berezovski, M.J. Tremblay, and I. de Belle. Proceedings of the National Academy of Sciences of the United States of America, 2015. 112(26): p. E3392-E3401. PMID[26056259].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25970561">Antiviral Activity of Diterpene esters on Chikungunya Virus and HIV Replication.</a> Nothias-Scaglia, L.F., C. Pannecouque, F. Renucci, L. Delang, J. Neyts, F. Roussi, J. Costa, P. Leyssen, M. Litaudon, and J. Paolini. Journal of Natural Products, 2015. 78(6): p. 1277-1283. PMID[25970561].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25907370">Anti-HIV Diarylpyrimidine-quinolone Hybrids and Their Mode of Action.</a></p>

    <p class="plaintext">Mao, T.Q., Q.Q. He, Z.Y. Wan, W.X. Chen, F.E. Chen, G.F. Tang, E. De Clercq, D. Daelemans, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry, 2015. 23(13): p. 3860-3868. PMID[25907370].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25359703">Design, Synthesis, and Biological Evaluation of Novel 4-Aminopiperidinyl-linked 3,5-Disubstituted-1,2,6-thiadiazine-1,1-dione Derivatives as HIV-1 NNRTIs.</a> Liu, T., B. Huang, Y. Tian, X. Liang, H. Liu, H. Liu, P. Zhan, E. De Clercq, C. Pannecouque, and X. Liu. Chemical Biology &amp; Drug Design, 2015. 86(1): p. 887-893. PMID[25359703].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=26085138">Discovery of Another anti-HIV Protein in the Search for the CD8+ Cell anti-HIV Factor.</a> Levy, J.A. Proceedings of the National Academy of Sciences of the United States of America, 2015. 112(26): p. 7888-7889. PMID[26085138].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=26132818">The Low-cost Compound Lignosulfonic acid (LA) Exhibits Broad-spectrum anti-HIV and anti-HSV Activity and Has Potential for Microbicidal Applications.</a> Gordts, S.C., G. Ferir, T. D&#39;Huys, M.I. Petrova, S. Lebeer, R. Snoeck, G. Andrei, and D. Schols. Plos One, 2015. 10(7): p. e0131219. PMID[26132818].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25358434">Design, Synthesis, and anti-HIV Evaluation of Novel Triazine Derivatives Targeting the Entrance Channel of the NNRTI Binding Pocket.</a> Chen, X., Q. Meng, L. Qiu, P. Zhan, H. Liu, E. De Clercq, C. Pannecouque, and X. Liu. Chemical Biology &amp; Drug Design, 2015. 86(1): p. 902-908. PMID[25358434].
    <br />
    <b>[PubMed]</b>. HIV_0619-070215.</p><b></b>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355843200007">Enhanced HIV-1 Reverse Transcriptase Inhibitory and Antibacterial Properties in Callus of Catha edulis forsk.</a> Kumari, A., P. Baskaran, and J. Van Staden. Phytotherapy Research, 2015. 29(6): p. 840-843. ISI[000355843200007].
    <br />
    <b>[WOS]</b>. HIV_0619-070215.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000355875800022">Inhibition of the DNA Polymerase and RNase H Activities of HIV-1 Reverse Transcriptase and HIV-1 Replication by Brasenia schreberi (Junsai) and Petasites japonicus (Fuki) Components.</a> Hisayoshi, T., M. Shinomura, K. Yokokawa, I. Kuze, A. Konishi, K. Kawaji, E.N. Kodama, K. Hata, S. Takahashi, S. Nirasawa, S. Sakuda, and K. Yasukawa. Journal of Natural Medicines, 2015. 69(3): p. 432-440. ISI[000355875800022].
    <br />
    <b>[WOS]</b>. HIV_0619-070215.</p> 

    <br />

    <br />

    <br />

    <br />

    <br />

    <p class="plaintext">                                      </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
