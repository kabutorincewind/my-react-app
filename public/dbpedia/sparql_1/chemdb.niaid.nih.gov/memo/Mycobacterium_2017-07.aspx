

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gCCr80xhcJ0jMPm5v6KCKsS4EFTVMZ0j32LxluPLGIvkoObzOybzW+kGKJof0RmgjTmKZOU/eCOM69ypyiSsmN/O/8K6R0yKixc8pMPkBCTlhVP3ZGxRV+puYAM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="931C8D94" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: July, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404312200049">Carrageenan-stabilized Chitosan Alginate Nanoparticles Loaded with Ethionamide for the Treatment of Tuberculosis.</a> Abdelghany, S., M. Alkhawaldeh, and H.S. AlKhatib. Journal of Drug Delivery Science and Technology, 2017. 39: p. 442-449. ISI[000404312200049].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28359706">Discovery of a New Mycobacterium tuberculosis Thymidylate Synthase X Inhibitor with a Unique Inhibition Profile.</a> Abu El Asrar, R., L. Margamuljana, H. Klaassen, M. Nijs, A. Marchand, P. Chaltin, H. Myllykallio, H.F. Becker, S. De Jonghe, P. Herdewijn, and E. Lescrinier. Biochemical Pharmacology, 2017. 135: p. 69-78. PMID[28359706].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28669536">Development of a Novel Lead That Targets M. tuberculosis Polyketide Synthase 13.</a> Aggarwal, A., M.K. Parai, N. Shetty, D. Wallis, L. Woolhiser, C. Hastings, N.K. Dutta, S. Galaviz, R.C. Dhakal, R. Shrestha, S. Wakabayashi, C. Walpole, D. Matthews, D. Floyd, P. Scullion, J. Riley, O. Epemolu, S. Norval, T. Snavely, G.T. Robertson, E.J. Rubin, T.R. Ioerger, F.A. Sirgel, R. van der Merwe, P.D. van Helden, P. Keller, E.C. Bottger, P.C. Karakousis, A.J. Lenaerts, and J.C. Sacchettini. Cell, 2017. 170(2): p. 249-259 e225. PMID[28669536]. PMCID[PMC5509550].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000403499200004">Antioxidant, Antigenotoxic, Antimicrobial Activities and Phytochemical Analysis of Dianthus carmelitarum.</a> Aliyazicioglu, R., S. Demir, M. Badem, S.O. Sener, N. Korkmaz, E.A. Demir, U. Ozgen, S.A. Karaoglu, and Y. Aliyazicioglu. Records of Natural Products, 2017. 11(3): p. 270-284. ISI[000403499200004].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28629306">Beta-CA-specific Inhibitor Dithiocarbamate Fc14-584B: A Novel Antimycobacterial Agent with Potential to Treat Drug-resistant Tuberculosis.</a> Aspatwar, A., M. Hammaren, S. Koskinen, B. Luukinen, H. Barker, F. Carta, C.T. Supuran, M. Parikka, and S. Parkkila. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 832-840. PMID[28629306].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28712709">Synthesis and Biological Evaluation of Novel 1,2,3-Triazole Derivatives as Anti-tubercular Agents.</a> Aziz Ali, A., D. Gogoi, A.K. Chaliha, A.K. Buragohain, P. Trivedi, P.J. Saikia, P.S. Gehlot, A. Kumar, V. Chaturvedi, and D. Sarma. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(16): p. 3698-3703. PMID[28712709].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28552337">One-pot Microwave Assisted Stereoselective Synthesis of Novel Dihydro-2&#39;H-spiro[indene-2,1&#39;-pyrrolo-[3,4-c]pyrrole]-tetraones and Evaluation of Their Antimycobacterial Activity and Inhibition of Ache.</a> Bharkavi, C., S. Vivek Kumar, M. Ashraf Ali, H. Osman, S. Muthusubramanian, and S. Perumal. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(14): p. 3071-3075. PMID[28552337].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27784238">Discovery and Optimization of NAD(+)-Dependent DNA Ligase Inhibitors as Novel Antibacterial Compounds.</a> Bi, F.C., R.X. Ma, and S.T. Ma. Current Pharmaceutical Design, 2017. 23(14): p. 2117-2130. PMID[27784238].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27908268">Synthesis of New Fluoro-benzimidazole Derivatives as an Approach towards the Discovery of Novel Intestinal Antiseptic Drug Candidates.</a> Cevik, U.A., B.N. Saglik, Y. Ozkay, Z. Canturk, J. Bueno, F. Demirci, and A.S. Koparal. Current Pharmaceutical Design, 2017. 23(15): p. 2276-2286. PMID[27908268].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28506901">New Isoflavonoids from the Extract of Rhynchosia precatoria (Humb. &amp; Bonpl. ex Willd.) DC. And Their Antimycobacterial Activity.</a> Coronado-Aceves, E.W., G. Gigliarelli, A. Garibay-Escobar, R.E.R. Zepeda, M. Curini, J. Lopez Cervantes, C.I. Ines Espitia-Pinzon, S. Superchi, S. Vergura, and M.C. Marcotullio. Journal of Ethnopharmacology, 2017. 206: p. 92-100. PMID[28506901].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28521188">Bis(Diphenylphosphino)amines-containing Ruthenium cymene Complexes as Potential anti-Mycobacterium tuberculosis Agents.</a> da Silva, J.P., I.C. Silva, F.R. Pavan, D.F. Back, and M.P. de Araujo. Journal of Inorganic Biochemistry, 2017. 173: p. 134-140. PMID[28521188].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000403668900018">Hydroquinone Derivatives from the Marine-derived Fungus Gliomastix sp.</a> Elnaggar, M.S., W. Ebrahim, A. Mandi, T. Kurtan, W.E.G. Muller, R. Kalscheuer, A. Singab, W.H. Lin, Z. Liu, and P. Proksch. RSC Advances, 2017. 7(49): p. 30640-30649. ISI[000403668900018].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405305900011">Synthesis of Carbohydrate-substituted Isoxazoles and Evaluation of Their Antitubercular Activity.</a> Hajlaoui, K., A. Guesmi, N. Ben Hamadi, and M. Msaddek. Heterocyclic Communications, 2017. 23(3): p. 225-229. ISI[000405305900011].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28475320">Structure-based Rational Design of Novel Inhibitors against Fructose-1,6-bisphosphate Aldolase from Candida albicans.</a> Han, X.Y., X.Y. Zhu, Z.Q. Hong, L. Wei, Y.L. Ren, F. Wan, S.H. Zhu, H. Peng, L. Guo, L. Rao, L.L. Feng, and J. Wan. Journal of Chemical Information and Modeling, 2017. 57(6): p. 1426-1438. PMID[28475320].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404984700008">Antitubercular and Antimicrobial Activity of NH4VO3 Promoted 1,4-Dihydropyridine Incorporated 1,3,4-Trisubstituted Pyrazole.</a> Harikrishna, N., A.M. Isloor, K. Ananda, T. Parish, J. Jamalis, H.A. Ghabbour, and H.K. Fun. Letters in Drug Design &amp; Discovery, 2017. 14(6): p. 699-711. ISI[000404984700008].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28465626">Synthesis and Biological Evaluation of Indole Core-based Derivatives with Potent Antibacterial Activity against Resistant Bacterial Pathogens.</a> Hong, W., J. Li, Z. Chang, X. Tan, H. Yang, Y. Ouyang, Y. Yang, S. Kaur, I.C. Paterson, Y.F. Ngeow, and H. Wang. The Journal of Antibiotics, 2017. 70(7): p. 832-844. PMID[28465626].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28109130">Identification of Promising DNA GyrB Inhibitors for Tuberculosis Using Pharmacophore-based Virtual Screening, Molecular Docking and Molecular Dynamics Studies.</a> Islam, M.A. and T.S. Pillay. Chemical Biology &amp; Drug Design, 2017. 90(2): p. 282-296. PMID[28109130].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28749949">2-Aminoimidazoles Potentiate beta-Lactam Antimicrobial Activity against Mycobacterium tuberculosis by reducing beta-Lactamase Secretion and Increasing Cell Envelope Permeability.</a> Jeon, A.B., A. Obregon-Henao, D.F. Ackart, B.K. Podell, J.M. Belardinelli, M. Jackson, T.V. Nguyen, M.S. Blackledge, R.J. Melander, C. Melander, B.K. Johnson, R.B. Abramovitch, and R.J. Basaraba. Plos One, 2017. 12(7): p. e0180925. PMID[28749949].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28744747">Structural and Inhibition Analysis of Novel Sulfur-rich 2-Mercaptobenzothiazole and 1,2,3-Triazole Ligands against Mycobacterium tuberculosis DprE1 Enzyme.</a> Karan, S., V.K. Kashyap, S. Shafi, and A.K. Saxena. Journal of Molecular Modeling, 2017. 23(8): p. 241. PMID[28744747].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28509874">Evaluation of the Mycobactericidal Effect of Thio-functionalized Carbohydrate Derivatives.</a> Korycka-Machala, M., A. Brzostek, B. Dziadek, M. Kawka, T. Poplawski, Z.J. Witczak, and J. Dziadek. Molecules, 2017. 22(5): E812. PMID[28509874].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28594172">Structure-based Optimization of Pyridoxal 5&#39;-Phosphate-dependent Transaminase Enzyme (BioA) Inhibitors That Target Biotin Biosynthesis in Mycobacterium tuberculosis.</a> Liu, F., S. Dawadi, K.M. Maize, R. Dai, S.W. Park, D. Schnappinger, B.C. Finzel, and C.C. Aldrich. Journal of Medicinal Chemistry, 2017. 60(13): p. 5507-5520. PMID[28594172].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404076200018">Benzylsulfanyl benzo-heterocycle amides and hydrazones as New Agents against Drug-susceptible and Resistant Mycobacterium tuberculosis.</a> Lu, X.Y., X.L. Hu, Z.Y. Liu, T.Y. Zhang, R.B. Wang, B.J. Wan, S.G. Franzblau, and Q.D. You. MedChemComm, 2017. 8(6): p. 1303-1306. ISI[000404076200018].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404985900004">In Vitro and in Silico Analysis of beta-Lactam Derivatives as Antimycobacterial Agents.</a> Luna-Herrera, J., E.E. Lara-Ramirez, A.R. Munoz-Duarte, F.E. Olazaran, M.J. Chan-Bacab, R. Moo-Puc, A.M. Perez-Vazquez, C.M. Morales-Reyes, and G. Rivera. Letters in Drug Design &amp; Discovery, 2017. 14(7): p. 782-786. ISI[000404985900004].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28654200">Preparation and Evaluation of Potent Pentafluorosulfanyl-substituted Anti-tuberculosis Compounds.</a> Moraski, G.C., R. Bristol, N. Seeger, H.I. Boshoff, P.S. Tsang, and M.J. Miller. ChemMedChem, 2017. 12(14): p. 1108-1115. PMID[28654200].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404966900035">Synthesis of Novel Fluorene bisamide Derivatives via Ugi Reaction and Evaluation Their Biological Activity against Mycobacterium Species.</a> Rezayan, A.H., S. Hariri, P. Azerang, G. Ghavami, I. Portugal, and S. Sardari. Iranian Journal of Pharmaceutical Research, 2017. 16(2): p. 734-744. ISI[000404966900035].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404913700001">Southeast Asian Medicinal Plants as a Potential Source of Antituberculosis Agent.</a> Sanusi, S.B., M.F. Abu Bakar, M. Mohamed, S.F. Sabran, and M.M. Mainasara. Evidence-Based Complementary and Alternative Medicine, 2017. 7185649: 39pp. ISI[000404913700001].
    <br />
    <b>[WOS]</b>. TB_07_2017.</p><br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28544982">An Evaluation of Minor Groove Binders as Anti-fungal and Anti-mycobacterial Therapeutics.</a> Scott, F.J., R.J.O. Nichol, A.I. Khalaf, F. Giordani, K. Gillingwater, S. Ramu, A. Elliott, J. Zuegg, P. Duffy, M.J. Rosslee, L. Hlaka, S. Kumar, M. Ozturk, F. Brombacher, M. Barrett, R. Guler, and C.J. Suckling. European Journal of Medicinal Chemistry, 2017. 136: p. 561-572. PMID[28544982].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28511780">Anti-Mycobacterium Avium Complex Activity of Clarithromycin, Rifampin, Rifabutin, and Ethambutol in Combination with Adenosine 5&#39;-Triphosphate.</a> Tatano, Y., S. Yamabe, C. Sano, and H. Tomioka. Diagnostic Microbiology and Infectious Disease, 2017. 88(3): p. 241-246. PMID[28511780].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28698545">A Novel Protein Kinase Inhibitor IMB-YH-8 with Anti-tuberculosis Activity.</a> Xu, J., J.X. Wang, J.M. Zhou, C.L. Xu, B. Huang, Y. Xing, B. Wang, R. Luo, Y.C. Wang, X.F. You, Y. Lu, and L.Y. Yu. Scientific Reports, 2017. 7(5093): 10pp. PMID[28698545]. PMCID[PMC5506005].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28703766">Synthesis and in Vitro Antimycobacterial and Antibacterial Activity of 8-OMe ciprofloxacin-hydrozone/azole Hybrids.</a> Xu, Z., S. Zhang, L.S. Feng, X.N. Li, G.C. Huang, Y. Chai, Z.S. Lv, H.Y. Guo, and M.L. Liu. Molecules, 2017. 22(7): E1171. PMID[28703766].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28720502">Design, Synthesis and in Vitro Anti-mycobacterial Evaluation of Gatifloxacin-1H-1,2,3-triazole-isatin Hybrids.</a> Xu, Z., S. Zhang, X. Song, M. Qiang, and Z. Lv. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(16): p. 3643-3646. PMID[28720502].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28561567">Clathrin-independent Killing of Intracellular Mycobacteria and Biofilm Disruptions Using Synthetic Antimicrobial Polymers.</a> Yavvari, P.S., S. Gupta, D. Arora, V.K. Nandicoori, A. Srivastava, and A. Bajaj. Biomacromolecules, 2017. 18(7): p. 2024-2033. PMID[28561567].
    <br />
    <b>[PubMed]</b>. TB_07_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">33. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170720&amp;CC=WO&amp;NR=2017123161A1&amp;KC=A1">Inhibition of Intracellular Growth of Mycobacterium Species and Its Applications Using a Compound That Enhances the Activity of an NAD+-Dependent Deacetylase Such as Sirtuins.</a> Singhal, A. and C. Cheng. Patent. 2017. 2017-SG50021 2017123161: 200pp.
    <br />
    <b>[Patent]</b>. TB_07_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
