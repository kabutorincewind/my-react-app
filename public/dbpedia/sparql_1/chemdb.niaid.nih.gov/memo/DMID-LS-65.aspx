

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-65.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OhgD47UQ0ylakb7FekXs69dMbA0j7Mo1xrA1txSaxsDgPLteu7g/r9H1lGZE4l8nlxw+sv54BwUQnZKenPw93pNnQ9gu4uNYzJ62mH5qwqYHFBHdFvfCM2FmzXk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4EAE24DE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-65-MEMO</b> </p>

<p>    1.    5897   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Antigenic Differences between H5N1 Human Influenza Viruses Isolated in 1997 and 2003</p>

<p>           Horimoto, T,  Fukuda, N, Iwatsuki-Horimoto, K, Guan, Y,  Lim, W, Peiris, M, Sugii, S, Odagiri, T, Tashiro, M, and Kawaoka, Y</p>

<p>           J Vet Med Sci 2004. 66(3): 303-305</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15107562&amp;dopt=abstract</a> </p><br />

<p>    2.    5898   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Inhibitory effect of bovine lactoferrin on infection with mouse adenovirus</p>

<p>           Shimizu, Keiko, Nukanobu, Noriaki, and Tazume, Seiki</p>

<p>           Igaku to Seibutsugaku 2004. 148(1):  30-34</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    5899   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Cowpox virus CrmA, Myxoma virus SERP2 and baculovirus P35 are not functionally interchangeable caspase inhibitors in poxvirus infections</p>

<p>           Nathaniel, R, MacNeill, AL, Wang, YX, Turner, PC, and Moyer, RW</p>

<p>           J Gen Virol 2004. 85(Pt 5): 1267-1278</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105544&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105544&amp;dopt=abstract</a> </p><br />

<p>    4.    5900   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Effects of Maribavir and Selected Indolocarbazoles on Epstein-Barr Virus Protein Kinase BGLF4 and on Viral Lytic Replication</p>

<p>           Gershburg, E, Hong, K, and Pagano, JS</p>

<p>           Antimicrob Agents Chemother 2004. 48(5): 1900-1903</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105156&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105156&amp;dopt=abstract</a> </p><br />

<p>    5.    5901   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Inhibitory Activity of Alkoxyalkyl and Alkyl Esters of Cidofovir and Cyclic Cidofovir against Orthopoxvirus Replication In Vitro</p>

<p>           Keith, KA, Wan, WB, Ciesla, SL, Beadle, JR, Hostetler, KY, and Kern, ER</p>

<p>           Antimicrob Agents Chemother 2004. 48(5): 1869-1871</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105146&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105146&amp;dopt=abstract</a> </p><br />

<p>    6.    5902   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Activities of benzimidazole d- and L-ribonucleosides in animal models of cytomegalovirus infections</p>

<p>           Kern, ER, Hartline, CB, Rybak, RJ, Drach, JC, Townsend, LB, Biron, KK, and Bidanset, DJ</p>

<p>           Antimicrob Agents Chemother 2004. 48(5): 1749-1755</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105130&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105130&amp;dopt=abstract</a> </p><br />

<p>    7.    5903   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Efficacy of Novel Hemagglutinin-Neuraminidase Inhibitors BCX 2798 and BCX 2855 against Human Parainfluenza Viruses In Vitro and In Vivo</p>

<p>           Alymova, IV, Taylor, G, Takimoto, T, Lin, TH, Chand, P, Babu, YS, Li, C, Xiong, X, and Portner, A</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1495-1502</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105096&amp;dopt=abstract</a> </p><br />

<p>    8.    5904   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Isolation of Salicin Derivatives from Homalium cochinchinensis and Their Antiviral Activities(1)</p>

<p>           Ishikawa, T,  Nishigaya, K, Takami, K, Uchikoshi, H, Chen, IS, and Tsai, IL</p>

<p>           J Nat Prod 2004. 67(4): 659-663</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104498&amp;dopt=abstract</a> </p><br />

<p>    9.    5905   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Antiviral Flavonoids from the Seeds of Aesculus chinensis</p>

<p>           Wei, F, Ma, SC, Ma, LY, But, PP, Lin, RC, and Khan, IA</p>

<p>           J Nat Prod 2004. 67(4): 650-653</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15104496&amp;dopt=abstract</a> </p><br />

<p>  10.    5906   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Nucleoside analogues and mitochondrial toxicity</p>

<p>           Fleischer, R, Boxwell, D, and Sherman, KE</p>

<p>           Clin Infect Dis 2004. 38(8): e79-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15095236&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15095236&amp;dopt=abstract</a> </p><br />

<p>  11.    5907   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Preparation of pyrrolidine derivatives useful in treatment of hepatitis C virus infection</b>  ((3 D Gene Pharma, Fr.)</p>

<p>           Halfon, Philippe, Sayada, Chalom, Feryn, Jean-Marc, Perbost, Regis, Garzino, Frederic, Camplo, Michel, Courcambeck, Jerome, and Pepe, Gerard</p>

<p>           PATENT: EP 1408031 A1;  ISSUE DATE: 20040414</p>

<p>           APPLICATION: 2002-30343; PP: 29 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    5908   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Treatment of viral diseases by 1,3,5-triazine nucleoside and nucleotide analogs, and preparation thereof</b> ((Koronis Pharmaceuticals, Incorporated USA)</p>

<p>           Daifuku, Richard, Gall, Alexander, and Sergueev, Dmitri</p>

<p>           PATENT: WO 2004028454 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 108 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  13.    5909   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Pegylated interferon alpha-2b plus ribavirin as therapy for chronic hepatitis C in HIV-infected patients</p>

<p>           Callens, S, Bottieau, E, Michielsen, P, and Colebunders, R</p>

<p>           AIDS 2004. 18(1): 131</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090841&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090841&amp;dopt=abstract</a> </p><br />

<p>  14.    5910   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Short- and long-term effects of highly active antiretroviral therapy on Kaposi sarcoma-associated herpesvirus immune responses and viraemia</p>

<p>           Bourboulia, D, Aldam, D, Lagos, D, Allen, E, Williams, I, Cornforth, D, Copas, A, and Boshoff, C</p>

<p>           AIDS 2004. 18(3): 485-493</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090801&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15090801&amp;dopt=abstract</a> </p><br />

<p>  15.    5911   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Inhibition of Sars Coronavirus Infection in Vitro With Clinically Approved Antiviral Drugs</b></p>

<p>           Tan, ELC, Ooi, EE, Lin, CY, Tan, HC, Ling, AE, Lim, B, and Stanton, LW</p>

<p>           Emerging Infectious Diseases 2004.  10(4): 581-586</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    5912   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Serpin Mechanism of Hepatitis C Virus Nonstructural 3 (NS3) Protease Inhibition: induced fit as a mechanism for narrow specificity</p>

<p>           Richer, Martin J, Juliano, Luiz, Hashimoto, Carl, and Jean, Francois</p>

<p>           Journal of Biological Chemistry 2004. 279(11): 10222-10227</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    5913   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Avian Influenza: a New Pandemic Threat?</b></p>

<p>           Trampuz, A, Prabhu, RM, Smith, TF, and Baddour, LM</p>

<p>           Mayo Clinic Proceedings 2004. 79(4): 523-530</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  18.    5914   DMID-LS-65; PUBMED-DMID-4/27/2004</p>

<p class="memofmt1-2">           Cofactor mimics as selective inhibitors of NAD-dependent inosine monophosphate dehydrogenase (IMPDH)--the major therapeutic target</p>

<p>           Pankiewicz, KW, Patterson, SE, Black, PL, Jayaram, HN, Risal, D, Goldstein, BM, Stuyver, LJ, and Schinazi, RF</p>

<p>           Curr Med Chem  2004. 11(7): 887-900</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15083807&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15083807&amp;dopt=abstract</a> </p><br />

<p>  19.    5915   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Randomized study of the efficacy and safety of oral elderberry extract in the treatment of influenza A and B virus infections</p>

<p>           Zakay-Rones, Z, Thom, E, Wollan, T, and Wadstein, J</p>

<p>           Journal of International Medical Research 2004. 32(2): 132-140</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  20.    5916   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Oral antivirals for the acute treatment of recurrent herpes labialis</p>

<p>           Jensen, Lori A, Hoehns, James D, and Squires, Cindy L</p>

<p>           Annals of Pharmacotherapy 2004. 38(4): 705-709</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    5917   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Inhibitors of S-adenosyl methionine decarboxylase and use for the treatment of a herpes simplex virus infection</b> ((Institut National de la Sante et de la Recherche Medicale (Inserm), Fr.)</p>

<p>           Diaz, Jean-Jacques and Greco, Anna</p>

<p>           PATENT: EP 1408028 A1;  ISSUE DATE: 20040414</p>

<p>           APPLICATION: 2002-30350; PP: 30 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    5918   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Phenyl phosphoramidate derivatives of stavudine as anti-HIV agents with potent and selective in-vitro antiviral activity against Adenovirus</p>

<p>           Uckun, Fatih M, Pendergrass, Sharon, Qazi, Sanjive, Samuel, P, and Venkatachalam, TK</p>

<p>           European Journal of Medicinal Chemistry 2004. 39(3): 225-234</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    5919   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Putranjivain A from Euphorbia jolkini inhibits both virus entry and late stage replication of herpes simplex virus type 2 in vitro</p>

<p>           Cheng, Hua-Yew, Lin, Ta-Chen, Yang, Chien-Min, Wang, Kuo-Chih, Lin, Liang-Tzung, and Lin, Chun-Ching</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(4): 577-583</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    5920   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Enhanced Resistance to Herpes Simplex Virus Type 1 Infection in Transgenic Mice Expressing a Soluble Form of Herpesvirus Entry Mediator</b></p>

<p>           Ono, E, Yoshino, S, Amagai, K, Taharaguchi, S, Kimura, C, Morimoto, J, Inobe, M, Uenishi, T, and Uede, T</p>

<p>           Virology 2004. 320(2): 267-275</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    5921   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Preparation of arylethanol(thienopyridinemethyl)amine derivatives as antiviral agents</b>((Pharmacia &amp; Upjohn Company, USA)</p>

<p>           Schnute, Mark E, Cudahy, Michele M, Ciske, Fred L, Genin, Michael J, Anderson, David J, Judge, Thomas M, Eggen, Marijean, and Collier, Sarah A</p>

<p>           PATENT: WO 2004022568 A1;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 59 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    5922   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Preparation of 4-oxo-4,7-dihydrothieno[2,3-b]pyridine-5-carboxamides as antiviral agents</b>((Pharmacia &amp; Upjohn Company, USA)</p>

<p>           Schnute, Mark E, Ciske, Fred L, Genin, Michael J, Strohbach, Joseph W, and Thaisrivongs, Suvit</p>

<p>           PATENT: WO 2004022566 A1;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2003; PP: 74 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  27.    5923   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Prophylactic and therapeutic antiviral compositions containing evening primrose (Oenothera biennis) extract</b> ((Morinaga Milk Industry Co., Ltd. Japan)</p>

<p>           Shiraki, Kimiyasu, Kurokawa, Masahiko, Tamura, Yoshitaka, Teraguchi, Susumu, Wakabayashi, Hiroyuki, Atarashi, Koichiro, and Kawakami, Tomomi</p>

<p>           PATENT: JP 2004083487 A2;  ISSUE DATE: 20040318</p>

<p>           APPLICATION: 2002-50341; PP: 16 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  28.    5924   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Efficacy of Tenofovir Disoproxil Fumarate in Antiretroviral Therapy-Naive and -Experienced Patients Coinfected With Hiv-1 and Hepatitis B Virus</b></p>

<p>           Dore, GJ, Cooper, DA, Pozniak, AL, Dejesus, E, Zhong, LJ, Miller, MD, Lu, B, and Cheng, AK</p>

<p>           Journal of Infectious Diseases 2004.  189(7): 1185-1192</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    5925   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Preparation of 1-arylnaphthalenes and related compounds as antiviral agents for the treatment of herpesviral infections</b>((Taiwan))</p>

<p>           Hsu, Tsu-an, Hsieh, Hsing-pang, Juan, Li-jung, Chang, Sui-yuan, and Kuo, Yueh-hsiung</p>

<p>           PATENT: US 2004044069 A1;  ISSUE DATE: 20040304</p>

<p>           APPLICATION: 2003-52052; PP: 10 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  30.    5926   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Mek Inhibition Impairs Influenza B Virus Propagation Without Emergence of Resistant Variants</b></p>

<p>           Ludwig, S, Wolff, T, Ehrhardt, C, Wurzer, WJ, Reinhardt, J, Planz, O, and Pleschka, S</p>

<p>           Febs Letters 2004. 561(1-3): 37-43</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    5927   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Adefovir Dipivoxil in the Treatment of Chronic Hepatitis B</b></p>

<p>           Rivkin, AM</p>

<p>           Annals of Pharmacotherapy 2004. 38(4): 625-633</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    5928   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           SARS virus inhibited by siRNA</p>

<p>           Elmen, Joacim, Wahlestedt, Claes, Brytting, Maria, Wahren, Britta, and Ljungberg, Karl</p>

<p>           Preclinica 2004. 2(2): 135-142</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    5929   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Identification of Novel Inhibitors of the SARS Coronavirus Main Protease 3CLpro</p>

<p>           Bacha, Usman, Barrila, Jennifer, Velazquez-Campoy, Adrian, Leavitt, Stephanie A, and Freire, Ernesto</p>

<p>           Biochemistry 2004. 43(17): 4906-4912</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    5930   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Coronaviruses. Cause of SARS and other infections</p>

<p>           Stock, Ingo</p>

<p>           Medizinische Monatsschrift fuer Pharmazeuten 2004. 27(1): 4-12</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  35.    5931   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Interaction between heptad repeat 1 and 2 regions in spike protein of SARS-associated coronavirus: implications for virus fusogenic mechanism and identification of fusion inhibitors</p>

<p>           Liu, Shuwen, Xiao, Gengfu, Chen, Yibang, He, Yuxian, Niu, Jinkui, Escalante, Carlos R, Xiong, Huabao, Farmar, James, Debnath, Asim K, Tien, Po, and Jiang, Shibo</p>

<p>           Lancet 2004. 363(9413): 938-947</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    5932   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Rapid Detection and Differentiation of Human Pathogenic Orthopox Viruses by a Fluorescence Resonance Energy Transfer Real-Time Pcr Assay</b></p>

<p>           Panning, M, Asper, M, Kramme, S, Schmitz, H, and Drosten, C</p>

<p>           Clinical Chemistry 2004. 50(4): 702-708</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  37.    5933   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>The Treatment of Hepatitis C: History, Presence and Future</b></p>

<p>           Vrolijk, JM, De Knegt, RJ, Veldt, BJ, Orlent, H, and Schalm, SW</p>

<p>           Netherlands Journal of Medicine 2004. 62(3): 76-82</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  38.    5934   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Separation and screening of compounds of biological origin using molecularly imprinted polymers</p>

<p>           Xu, Xiaojie, Zhu, Lili, and Chen, Lirong</p>

<p>           Journal of Chromatography, B: Analytical Technologies in the Biomedical and Life Sciences 2004. 804(1): 61-69</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  39.    5935   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Human Polyomavirus Bk Monitoring by Quantitative Pcr in Renal Transplant Recipients</b></p>

<p>           Merlino, C, Bergallo, M, Giacchino, F, Daniele, R, Bollero, C, Comune, L, Segoloni, GP, and Cavallo, R</p>

<p>           Intervirology  2004. 47(1): 41-47</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    5936   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Antiviral action of ribavirin in chronic hepatitis C</p>

<p>           Pawlotsky, Jean-Michel, Dahari, Harel, Neumann, Avidan U, Hezode, Christophe, Germanidis, Georgios, Lonjon, Isabelle, Castera, Laurent, and Dhumeaux, Daniel</p>

<p>           Gastroenterology 2004. 126(3): 703-714</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    5937   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Testing antivirals against hepatitis delta virus: Farnesyl transferase inhibitors</p>

<p>           Bordier, Bruno B and Glenn, Jeffrey S</p>

<p>           Methods in Molecular Medicine 2004.  96(Hepatitis B and D Protocols, Volume 2): 539-553</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.    5938   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Cost Effectiveness of Peginterferon Alpha-2a Plus Ribavirin Versus Interferon Alpha-2b Plus Ribavirin as Initial Therapy for Treatment-Naive Chronic Hepatitis C</b></p>

<p>           Sullivan, SD, Craxi, A, Alberti, A, Giuliani, G, De Carli, C, Wintfeld, N, Patel, KK, and Green, J</p>

<p>           Pharmacoeconomics 2004. 22(4): 257-265</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  43.    5939   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Rescue therapy for drug resistant hepatitis B: another argument for combination chemotherapy?</p>

<p>           Shaw, Tim, Bowden, Scott, and Locarnini, Stephen</p>

<p>           Gastroenterology 2004. 126(1): 343-347</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  44.    5940   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Novel 3&#39;-deoxy analogs of the anti-HBV agent entecavir: synthesis of enantiomers from a single chiral epoxide</p>

<p>           Ruediger, Edward, Martel, Alain, Meanwell, Nicholas, Solomon, Carola, and Turmel, Brigitte</p>

<p>           Tetrahedron Letters 2004. 45(4): 739-742</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  45.    5941   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Polio Endgame - Wanted: Drug for a Disappearing Disease</b></p>

<p>           Enserink, M</p>

<p>           Science 2004. 303(5666): 1971</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  46.    5942   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>Complementary and Alternative Therapies in the Treatment of Chronic Hepatitis C: a Systematic Review</b></p>

<p>           Coon, JT and Ernst, E</p>

<p>           Journal of Hepatology 2004. 40(3): 491-500</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  47.    5943   DMID-LS-65; WOS-DMID-4/27/2004</p>

<p>           <b>The Management of Oral Human Papillomavirus With Topical Cidofovir: a Case Report</b></p>

<p>           Derossi, SS and Laudenbach, J</p>

<p>           Cutis 2004. 73(3): 191-193</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  48.    5944   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Preparation of vinylpyrimidones as antivirals</b>((Axxima Pharmaceuticals Aktiengesellschaft, Germany and 4SC AG))</p>

<p>           Missio, Andrea, Herget, Thomas, Aschenbrenner, Andrea, Kramer, Bernd, Leban, Johann, and Wolf, Kristina</p>

<p>           PATENT: EP 1389461 A1;  ISSUE DATE: 20040218</p>

<p>           APPLICATION: 2002-18357; PP: 46 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  49.    5945   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Effect of topically applied resveratrol on cutaneous herpes simplex virus infections in hairless mice</p>

<p>           Docherty, John J, Smith, Jennifer S, Fu, Ming Ming, Stoner, Terri, and Booth, Tristan</p>

<p>           Antiviral Research 2004. 61(1): 19-26</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  50.    5946   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           siRNAs and hairpin RNAs as antiviral agents against human influenza virus</b>((Massachusetts Institute of Technology, USA)</p>

<p>           Chen, Jianzhu, Eisen, Herman N, and Ge, Qing</p>

<p>           PATENT: WO 2004028471 A2;  ISSUE DATE: 20040408</p>

<p>           APPLICATION: 2003; PP: 239 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  51.    5947   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p><b>           Antiviral agents containing phenethyl caffeate for controlling influenza virus</b>((U.C.C. Ueshima Coffee Co., Ltd. Japan)</p>

<p>           Kishimoto, Noriaki and Fujita, Tokio</p>

<p>           PATENT: JP 2004091446 A2;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2002-62097; PP: 12 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  52.    5948   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           VP1 sequencing of all human rhinovirus serotypes: insights into genus phylogeny and susceptibility to antiviral capsid-binding compounds</p>

<p>           Ledford, Rebecca M, Patel, Nitesh R, Demenczuk, Tina M, Watanyar, Adiba, Herbertz, Torsten, Collett, Marc S, and Pevear, Daniel C</p>

<p>           Journal of Virology 2004. 78(7): 3663-3674</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  53.    5949   DMID-LS-65; SCIFINDER-DMID-4/27/2004</p>

<p class="memofmt1-2">           Viral receptor blockage by multivalent recombinant antibody fusion proteins: inhibiting human rhinovirus (HRV) infection with CFY196</p>

<p>           Fang, Fang and Yu, Mang</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(1): 23-25</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
