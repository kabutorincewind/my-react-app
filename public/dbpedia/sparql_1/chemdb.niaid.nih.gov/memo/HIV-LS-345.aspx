

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-345.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="v4jbOxrEv6M1rG6F7GtRcVAvEs1tywptbg7019HTtZAH05SfoDOEtKTAUemUyGy84y7z23MAR2Kv0cm+T/9KI9ovYyW+WJL40tpJZowkWF75anDAedKlxvUoy90=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D6F145F4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-345-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48656   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          Synthesis, separation and anti-HIV activity of distereoisomers of N-[p-(4-bromophenyl) -2&#39;,3&#39;-didehydro-3&#39;-deoxy-5&#39;-thymidylyl]-L-alanine methyl ester (stampidine)</p>

    <p>          Venkatachalam, TK, Qazi, S, and Uckun, FM</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(2a): 152-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570823&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570823&amp;dopt=abstract</a> </p><br />

    <p>2.     48657   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          Stampidine as a novel nucleoside reverse transcriptase inhibit with potent anti-HIV activity</p>

    <p>          Uckun, FM</p>

    <p>          Arzneimittelforschung <b>2006</b>.  56(2a): 121-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16570821&amp;dopt=abstract</a> </p><br />

    <p>3.     48658   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          N-methylpurine DNA glycosylase and 8-oxoguanine DNA glycosylase metabolize the antiviral nucleoside 2-bromo-5,6-dichloro-1-({beta}-D-ribofuranosyl)benzimidazole</p>

    <p>          Lorenzi, PL, Landowski, CP, Brancale, A, Song, X, Townsend, LB, Drach, JC, and Amidon, GL</p>

    <p>          Drug Metab Dispos <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16565170&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16565170&amp;dopt=abstract</a> </p><br />

    <p>4.     48659   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Triterpenoids from Schisandra lancifolia with Anti-HIV-1 Activity</p>

    <p>          Xiao, Wei-Lie, Tian, Ren-Rong, Pu, Jian-Xin, Li, Xian, Wu, Li, Lu, Yang, Li, Sheng-Hong, Li, Rong-Tao, Zheng, Yong-Tang, Zheng, Qi-Tai, and Sun, Han-Dong</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(2): 277-279</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     48660   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          A series of 5-aminosubstituted 4-fluorobenzyl-8-hydroxy-[1,6]naphthyridine-7-carboxamide HIV-1 integrase inhibitors</p>

    <p>          Guare, JP, Wai, JS, Gomez, RP, Anthony, NJ, Jolly, SM, Cortes, AR, Vacca, JP, Felock, PJ, Stillmock, KA, Schleif, WA, Moyer, G, Gabryelski, LJ, Jin, L, Chen, IW, Hazuda, DJ, and Young, SD</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554152&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48661   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          The C-terminal 26-residue peptide of serpin A1 is an inhibitor of HIV-1</p>

    <p>          Congote, LF</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  343(2): 617-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554023&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16554023&amp;dopt=abstract</a> </p><br />

    <p>7.     48662   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Moronic acid derivatives as novel potent anti-HIV agents</p>

    <p>          Sakurai, Yojiro, Yu, Donglei, Chen, Chin-Ho, Chang, Fang-Rong, and Lee, Kuo-Hsiung</p>

    <p>          Abstracts of Papers, 231st ACS National Meeting, Atlanta, GA, United States, March 26-30, 2006  <b>2006</b>.: MEDI-376</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     48663   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluations of novel oxindoles as HIV-1 non-nucleoside reverse transcriptase inhibitors. Part 2</p>

    <p>          Jiang, Tao, Kuhen, Kelli L, Wolff, Karen, Yin, Hong, Bieza, Kimberly, Caldwell, Jeremy, Bursulaya, Badry, Tuntland, Tove, Zhang, Kanyin, Karanewsky, Donald, and He, Yun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(8): 2109-2112</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48664   HIV-LS-345; PUBMED-HIV-4/3/2006</p>

    <p class="memofmt1-2">          Advances in HIV-1 entry inhibitors: strategies to interfere with receptor and coreceptor engagement</p>

    <p>          Markovic, I</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(9): 1105-19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515489&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16515489&amp;dopt=abstract</a> </p><br />

    <p>10.   48665   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Discovery and structure-activity relationship studies of a unique class of HIV-1 integrase inhibitors</p>

    <p>          Dayam, Raveendra, Sanchez, Tino, and Neamati, Nouri</p>

    <p>          ChemMedChem <b>2006</b>.  1(2): 238-244</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   48666   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Preparation of (quinolinylaminoalkyl)-benzimidazole derivatives as chemokine receptor ligands with anti-HIV activity</p>

    <p>          Gudmundsson, Kristjan, Sebahar, Paul Richard, and Richardson, Leah D&#39;Aurora</p>

    <p>          PATENT:  WO <b>2006023400</b>  ISSUE DATE:  20060302</p>

    <p>          APPLICATION: 2005  PP: 303 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   48667   HIV-LS-345; WOS-HIV-3/26/2006</p>

    <p class="memofmt1-2">          A folding inhibitor of the HIV-1 protease</p>

    <p>          Broglia, RA, Provasi, D, Vasile, F, Ottolina, G, Longhi, R, and Tiana, G</p>

    <p>          PROTEINS-STRUCTURE FUNCTION AND BIOINFORMATICS <b>2006</b>.  62(4 ): 928-933, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235872700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235872700010</a> </p><br />

    <p>13.   48668   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          The receptor-binding region of human apolipoprotein E has direct anti-infective activity</p>

    <p>          Dobson, Curtis B, Sales, Sean D, Hoggard, Patrick, Wozniak, Matthew A, and Crutcher, Keith A</p>

    <p>          Journal of Infectious Diseases <b>2006</b>.  193(3): 442-450</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48669   HIV-LS-345; SCIFINDER-HIV-3/27/2006</p>

    <p class="memofmt1-2">          Preparation of 1,3,5-triazepine-2,4-dione nucleoside analogs as potential antiviral and antitumor agents</p>

    <p>          Clivio, Pascale and Peyrane, Frederic</p>

    <p>          PATENT:  FR <b>2875231</b>  ISSUE DATE: 20060317</p>

    <p>          APPLICATION: 2004-9758  PP: 20 pp.</p>

    <p>          ASSIGNEE:  (Centre National de la Recherche Scientifique CNRS, Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   48670   HIV-LS-345; WOS-HIV-3/26/2006</p>

    <p class="memofmt1-2">          Discovering novel chemical inhibitors of human cyclophilin A: Virtual screening, synthesis, and bioassay</p>

    <p>          Li, J, Chen, J, Gui, CS, Zhang, L, Qin, Y, Xu, Q, Zhang, J, Liu, H, Shen, X, and Jiang, HL</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2006</b>.  14(7): 2209-2224, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236015000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236015000010</a> </p><br />

    <p>16.   48671   HIV-LS-345; WOS-HIV-3/26/2006</p>

    <p class="memofmt1-2">          Broad neutralization and complement-mediated lysis of HIV-1 by (PE)HRG214, a novel caprine anti-HIV-1 polyclonal antibody</p>

    <p>          Verity, EE, Williams, LA, Haddad, DN, Choy, V, O&#39;Loughlin, C, Chatfield, C, Saksena, NK, Cunningham, A, Gelder, F, and McPhee, DA</p>

    <p>          AIDS <b>2006</b>.  20 (4): 505-515, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235873000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235873000003</a> </p><br />

    <p>17.   48672   HIV-LS-345; WOS-HIV-4/2/2006</p>

    <p class="memofmt1-2">          Synthesis of some heterocycle containing urea derivatives and their anti-viral activity</p>

    <p>          Verma, M, Singh, KN, and Clercq, ED</p>

    <p>          HETEROCYCLES <b>2006</b>.  68(1): 11-22, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236020400003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236020400003</a> </p><br />

    <p>18.   48673   HIV-LS-345; WOS-HIV-4/2/2006</p>

    <p class="memofmt1-2">          Next-generation HIV-1 non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Boone, LR</p>

    <p>          CURRENT OPINION IN INVESTIGATIONAL DRUGS <b>2006</b>.  7(2): 128-135, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000236125800004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
