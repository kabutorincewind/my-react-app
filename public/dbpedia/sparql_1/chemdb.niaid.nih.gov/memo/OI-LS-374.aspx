

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-374.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Hb6E+s/NtX8wv9nXXqO2RCBOzMXdiqZ5XxNXNVhM7qjPzL/dULWKi6x6Bzld9s9s/94d7aGCsNAgxc2vcKFb4QXiW/DWb8exbo0kxKIpczfYU3GeUDP5sEX+kDw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D4E9B25E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-374-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60837   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Host Genetics of Mycobacterial Diseases in Mice and Men: Forward Genetic Studies of BCG-osis and Tuberculosis</p>

    <p>          Fortin, A, Abel, L, Casanova, JL, and Gros, P</p>

    <p>          Annu Rev Genomics Hum Genet <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17492906&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17492906&amp;dopt=abstract</a> </p><br />

    <p>2.     60838   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Inhibition of Isolated Mycobacterium tuberculosis Fatty Acid Synthase I by Pyrazinamide Analogs</p>

    <p>          Ngo, SC, Zimhony, O, Chung, WJ, Sayahi, H, Jacobs, WR Jr, and Welch, JT</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17485499&amp;dopt=abstract</a> </p><br />

    <p>3.     60839   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Determination of ethambutol MICs for Mycobacterium tuberculosis and Mycobacterium avium isolates by resazurin microtitre assay</p>

    <p>          Jadaun, GP, Agarwal, C, Sharma, H, Ahmed, Z, Upadhyay, P, Faujdar, J, Gupta, AK, Das, R, Gupta, P, Chauhan, DS, Sharma, VD, and Katoch, VM</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17483147&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17483147&amp;dopt=abstract</a> </p><br />

    <p>4.     60840   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Inhibitors of hepatitis C virus NS3.4A protease. Effect of P4 capping groups on inhibitory potency and pharmacokinetics</p>

    <p>          Perni, RB, Chandorkar, G, Cottrell, KM, Gates, CA, Lin, C, Lin, K, Luong, YP, Maxwell, JP, Murcko, MA, Pitlik, J, Rao, G, Schairer, WC, Drie, JV, and Wei, Y</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482818&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482818&amp;dopt=abstract</a> </p><br />

    <p>5.     60841   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal activities of new fluconazole analogues with azaheterocycle moiety</p>

    <p>          Lebouvier, N, Pagniez, F, Duflos, M, Le, Pape P, Na, YM, Le, Baut G, and Le, Borgne M</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482460&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482460&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     60842   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Structure, function and drug targeting in Mycobacterium tuberculosis cytochrome P450 systems</p>

    <p>          McLean, KJ, Dunford, AJ, Neeli, R, Driscoll, MD, and Munro, AW</p>

    <p>          Arch Biochem Biophys <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482138&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17482138&amp;dopt=abstract</a> </p><br />

    <p>7.     60843   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Inosine monophosphate dehydrogenase (IMPDH) as a target in drug discovery</p>

    <p>          Shu, Q and Nair, V</p>

    <p>          Med Res Rev <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17480004&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17480004&amp;dopt=abstract</a> </p><br />

    <p>8.     60844   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Carbonic anhydrases as targets for medicinal chemistry</p>

    <p>          Supuran, CT and Scozzafava, A</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17475500&amp;dopt=abstract</a> </p><br />

    <p>9.     60845   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Treatment of hepatitis C virus infection</p>

    <p>          Weigand, K, Stremmel, W, and Encke, J</p>

    <p>          World J Gastroenterol <b>2007</b>.  13(13): 1897-905</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17461488&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17461488&amp;dopt=abstract</a> </p><br />

    <p>10.   60846   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Biochemical Mechanism of Hepatitis C Virus Inhibition by the Broad-Spectrum Antiviral Arbidol</p>

    <p>          Pecheur, EI, Lavillette, D, Alcaras, F, Molle, J, Boriskin, YS, Roberts, M, Cosset, FL, and Polyak, SJ</p>

    <p>          Biochemistry <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17455911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17455911&amp;dopt=abstract</a> </p><br />

    <p>11.   60847   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          XDR-TB--danger ahead</p>

    <p>          Singh, MM</p>

    <p>          Indian J Tuberc <b>2007</b>.  54(1): 1-2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17455416&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17455416&amp;dopt=abstract</a> </p><br />

    <p>12.   60848   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          Entry of hepatitis C virus and human immunodeficiency virus is selectively inhibited by carbohydrate-binding agents but not by polyanions</p>

    <p>          Bertaux, Claire, Daelemans, Dirk, Meertens, Laurent, Cormier, Emmanuel G, Reinus, John F, Peumans, Willy J, Van Damme, Els JM, Igarashi, Yasuhiro, Oki, Toshikazu, Schols, Dominique, Dragic, Tatjana, and Balzarini, Jan</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 2064</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4NPHMH0-4/2/d0018d1e2ac221673bebf716f0d2d288">http://www.sciencedirect.com/science/article/B6WXR-4NPHMH0-4/2/d0018d1e2ac221673bebf716f0d2d288</a> </p><br />

    <p>13.   60849   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Lumazine synthase from Candida albicans as an anti-fungal target enzyme: Structural and biochemical basis for drug design</p>

    <p>          Morgunova, E, Saller, S, Haase, I, Cushman, M, Bacher, A, Fischer, M, and Ladenstein, R</p>

    <p>          J Biol Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446177&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17446177&amp;dopt=abstract</a> </p><br />

    <p>14.   60850   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Aptamer from whole-bacterium SELEX as new therapeutic reagent against virulent Mycobacterium tuberculosis</p>

    <p>          Chen, F, Zhou, J, Luo, F, Mohammed, AB, and Zhang, XL</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  357(3): 743-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17442275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17442275&amp;dopt=abstract</a> </p><br />

    <p>15.   60851   OI-LS-374; PUBMED-OI-5/14/2007</p>

    <p class="memofmt1-2">          Tuberculosis: still a concern for all countries in Europe</p>

    <p>          Falzon, D and Belghiti, F</p>

    <p>          Euro Surveill <b>2007</b>.  12(3): E070322.1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17439790&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17439790&amp;dopt=abstract</a> </p><br />

    <p>16.   60852   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          Inhibition of T-Cell Inflammatory Cytokines, Hepatocyte NF-[kappa]B Signaling, and HCV Infection by Standardized Silymarin</p>

    <p>          Polyak, Stephen J, Morishima, Chihiro, Shuhart, Margaret C, Wang, Chia C, Liu, Yanze, and Lee, David Y-W</p>

    <p>          Gastroenterology <b>2007</b>.  132(5): 1925-1936</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4N3P02C-3/2/a1de6f3eeb52d0eb78d285656ee51cd6">http://www.sciencedirect.com/science/article/B6WFX-4N3P02C-3/2/a1de6f3eeb52d0eb78d285656ee51cd6</a> </p><br />
    <br clear="all">

    <p>17.   60853   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          The Hepatitis C Virus Life Cycle as a Target for New Antiviral Therapies</p>

    <p>          Pawlotsky, Jean-Michel, Chevaliez, Stephane, and McHutchison, John G</p>

    <p>          Gastroenterology <b>2007</b>.  132(5): 1979-1998</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4NN7Y57-S/2/3e87f421f22aaaade7493f318723a735">http://www.sciencedirect.com/science/article/B6WFX-4NN7Y57-S/2/3e87f421f22aaaade7493f318723a735</a> </p><br />

    <p>18.   60854   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          A method for in vitro assembly of hepatitis C virus core protein and for screening of inhibitors</p>

    <p>          Fromentin, Remi, Majeau, Nathalie, Gagne, Marie-Eve Laliberte, Boivin, Annie, Duvignaud, Jean-Baptiste, and Leclerc, Denis</p>

    <p>          Analytical Biochemistry <b>2007</b>.  In Press, Corrected Proof: 375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W9V-4ND60M3-5/2/dff5cbfe4f10056fe348583faa40f9d2">http://www.sciencedirect.com/science/article/B6W9V-4ND60M3-5/2/dff5cbfe4f10056fe348583faa40f9d2</a> </p><br />

    <p>19.   60855   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          Phosphorylation of hepatitis C virus NS5A nonstructural protein: A new paradigm for phosphorylation-dependent viral RNA replication?</p>

    <p>          Huang, Ying, Staschke, Kirk, De Francesco, Raffaele, and Tan, Seng-Lai</p>

    <p>          Virology <b>2007</b>.  In Press, Corrected Proof: 375</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4ND70B3-1/2/9bbac33ce873b9da3c04ff8a5a649577">http://www.sciencedirect.com/science/article/B6WXR-4ND70B3-1/2/9bbac33ce873b9da3c04ff8a5a649577</a> </p><br />

    <p>20.   60856   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          Synthesis, stereochemistry and antimicrobial evaluation of t(3)-benzyl-r(2),c(6)-diarylpiperidin-4-one and its derivatives</p>

    <p>          Jayabharathi, J, Manimekalai, A, Consalata Vani, T, and Padmavathy, M</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(5): 593-605</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MFSNYJ-3/2/6a0854d9176c7b1c431b14c7255a3a9e">http://www.sciencedirect.com/science/article/B6VKY-4MFSNYJ-3/2/6a0854d9176c7b1c431b14c7255a3a9e</a> </p><br />

    <p>21.   60857   OI-LS-374; EMBASE-OI-5/14/2007</p>

    <p class="memofmt1-2">          7-Oxo-4,7-dihydrothieno[3,2-b]pyridine-6-carboxamides: Synthesis and Biological Activity of a New Class of Highly Potent Inhibitors of Human Cytomegalovirus DNA Polymerase</p>

    <p>          Larsen, Scott D, Zhang, Zhijun, DiPaolo, Brian A, Manninen, Peter R, Rohrer, Douglas C, Hageman, Michael J, Hopkins, Todd A, Knechtel, Mary L, Oien, Nancee L, Rush, Bob D, Schwende, Francis J, Stefanski, Kevin J, Wieber, Janet L, Wilkinson, Karen F, Zamora, Kathyrn M, Wathen, Michael W, and Brideau, Roger J</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 927</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4NP980V-5/2/0533de43ab76b946aeccd13baca0eb37">http://www.sciencedirect.com/science/article/B6TF9-4NP980V-5/2/0533de43ab76b946aeccd13baca0eb37</a> </p><br />
    <br clear="all">

    <p>22.   60858   OI-LS-374; WOS-OI-5/4/2007</p>

    <p class="memofmt1-2">          Synthesis, critical micelle concentrations, and antimycobacterial properties of homologous, dendritic amphiphiles. Probing intrinsic activity and the &quot;cutoff&quot; effect</p>

    <p>          Sugandhi, EW, Macri, RV, Williams, AA, Kite, BL, Slebodnick, C, Falkinham, JO, Esker, AR, and Gandour, RD</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  50(7): 1645-1650</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245259000023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245259000023</a> </p><br />

    <p>23.   60859   OI-LS-374; WOS-OI-5/4/2007</p>

    <p><b>          The manzamines as an example of the unique structural classes available for the discovery and optimization of infectious disease controls based on marine natural products</b> </p>

    <p>          Hamann, MT</p>

    <p>          CURRENT PHARMACEUTICAL DESIGN <b>2007</b>.  13(6): 653-660</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245201200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245201200008</a> </p><br />

    <p>24.   60860   OI-LS-374; WOS-OI-5/4/2007</p>

    <p class="memofmt1-2">          P56 LCK inhibitor identification by pharmacophore modelling and molecular docking</p>

    <p>          Bharatham, N, Bharatham, K, and Lee, KW</p>

    <p>          BULLETIN OF THE KOREAN CHEMICAL SOCIETY <b>2007</b>.  28(2): 200-206</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245197800010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245197800010</a> </p><br />

    <p>25.   60861   OI-LS-374; WOS-OI-5/11/2007</p>

    <p class="memofmt1-2">          Dimethyldithioimidocarbonates-mediated heterocyclizations: Synthesis of imidazolidines and benzheterocycles as potent antitubercular agents</p>

    <p>          Hegde, VS, Kolavi, GD, Lamani, RS, and Khazi, IAM</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2007</b>.  182(4): 911-920</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245316700020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245316700020</a> </p><br />

    <p>26.   60862   OI-LS-374; WOS-OI-5/11/2007</p>

    <p class="memofmt1-2">          Combinatorial RNAi: A winning strategy for the race against evolving targets?</p>

    <p>          Grimm, D and Kay, MA</p>

    <p>          MOLECULAR THERAPY <b>2007</b>.  15(5): 878-888</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245999500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245999500006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
