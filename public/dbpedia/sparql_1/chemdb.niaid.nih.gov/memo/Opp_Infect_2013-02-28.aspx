

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-02-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xxo1kOJzPrT/BiqUcwyv1HjcKV23xIjRJ1JKH2YOTtkTy0UrnJ32dTSE1WUE+o+whR4rVGSGrnlcGSEx0vGyVRLv17+nVtmpd2jx/C8ycliCI8A1UPgbPH70yxM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E6F7B80" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:<br />February 15 - February 28, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23437896">The Mediterranean Red Alga Asparagopsis taxiformis has Antifungal Activity against Aspergillus Species.</a> Genovese, G., S. Leitner, S.A. Minicante, and C. Lass-Florl. Mycoses, 2013. <b>[Epub ahead of print]</b>. PMID[23437896].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/243429275">PG-2, a Potent Amp against Pathogenic Microbial Strains, from Potato (Solanum tuberosum L Cv. Gogu Valley) Tubers Not Cytotoxic against Human Cells.</a> Kim, J.Y., R. Gopal, S.Y. Kim, C.H. Seo, H.B. Lee, H. Cheong, and Y. Park. International Journal of Molecular Sciences, 2013. 14(2): p. 4349-4360. PMID[23429275].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23417281">Antimicrobial Activity of Calotropis procera Ait. (Asclepiadaceae) and Isolation of Four Flavonoid Glycosides as the Active Constituents.</a> Nenaah, G. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23417281].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23294829">Synthesis of Gatifloxacin Derivatives and Their Biological Activities against Mycobacterium leprae and Mycobacterium tuberculosis.</a> Gomez, C., P. Ponien, N. Serradji, A. Lamouri, A. Pantel, E. Capton, V. Jarlier, G. Anquetin, and A. Aubry. Bioorganic &amp; Medicinal Chemistry, 2013. 21(4): p. 948-956. PMID[23294829].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23360543">Asperterpenoid A, a New Sesterterpenoid as an Inhibitor of Mycobacterium tuberculosis Protein Tyrosine Phosphatase B from the Culture of Aspergillus sp. 16-5c.</a> Huang, X., H. Huang, H. Li, X. Sun, H. Huang, Y. Lu, Y. Lin, Y. Long, and Z. She. Organic Letters, 2013. 15(4): p. 721-723. PMID[23360543].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23275348">The Naphthoquinone Diospyrin Is an Inhibitor of DNA Gyrase with a Novel Mechanism of Action.</a> Karkare, S., T.T. Chung, F. Collin, L.A. Mitchenall, A.R. McKay, S.J. Greive, J.J. Meyer, N. Lall, and A. Maxwell. The Journal of Biological Chemistry, 2013. 288(7): p. 5149-5156. PMID[23275348].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23439641">Zafirlukast Inhibits Complexation of LSR2 with DNA and Growth of Mycobacterium tuberculosis.</a> Pinault, L., J.S. Han, C.M. Kang, J. Franco, and D.R. Ronning. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23439641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23437287">Improved BM212 MMPl3 Inhibitor Analogue Shows Efficacy in Acute Murine Model of Tuberculosis Infection.</a> Poce, G., R.H. Bates, S. Alfonso, M. Cocozza, G.C. Porretta, L. Ballell, J. Rullas, F. Ortega, A. De Logu, E. Agus, V. La Rosa, M.R. Pasca, E. De Rossi, B. Wae, S.G. Franzblau, F. Manetti, M. Botta, and M. Biava. Plos One, 2013. 8(2): p. e56980. PMID[23437287].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23440582">Novel 1-(4-Substituted benzylidene)-4-(1-(substituted methyl)-2,3-dioxoindolin-5-yl)semicarbazide Derivatives for Use against Mycobacterium tuberculosis H37rv (ATCC 27294) and MDR-Tb Strain.</a> Raja, S. and C.R. Prakash. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23440582].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23247255">Antimalarial Activity of 10-Alkyl/Aryl Esters and -Aminoethylethers of Artemisinin.</a> Cloete, T.T., H.J. Krebs, J.A. Clark, M.C. Connelly, A. Orcutt, M.S. Sigal, R. Kiplin Guy, and D. N&#39;Da D. Bioorganic Chemistry, 2013. 46: p. 10-16. PMID[23247255].</p>

    <p class="plaintext">[PubMed]. OI_0201-021413.</p> 
    
    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23439633">The Antimalarial Activities of Methylene Blue and the 1,4-Naphthoquinone 3-[4-(trifluoromethyl)benzyl]-menadione are Not Due to Inhibition of the Mitochondrial Electron Transport Chain.</a> Ehrhardt, K., E. Davioud-Charvet, H. Ke, A.B. Vaidya, M. Lanzer, and M. Deponte. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23439633].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23439562">Cytotoxic and Antimalarial Amaryllidaceae Alkaloids from the Bulbs of Lycoris radiata.</a> Hao, B., S.F. Shen, and Q.J. Zhao. Molecules, 2013. 18(3): p. 2458-2468. PMID[23439562].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23440579">Antimalarial Activity of Nepodin Isolated from Rumex crispus.</a> Lee, K.H. and K.H. Rhee. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23440579].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23433124">Potent in Vivo Anti-malarial Activity and Representative Snapshot Pharmacokinetic Evaluation of Artemisinin-quinoline Hybrids.</a> Lombard, M.C., N.D. DD, C. Tran Van Ba, S. Wein, J. Norman, L. Wiesner, and H. Vial. Malaria Journal, 2013. 12(1): p. 71. PMID[23433124].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23425037">Synthesis and Antimalarial Efficacy of Two-carbon Linked, Artemisinin-derived, Trioxane Dimers in Combination with Known Antimalarial Drugs.</a> Mott, B.T., A.K. Tripathi, M.A. Siegler, C.D. Moore, D.J. Sullivan Jr, and G.H. Posner. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23425037].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23252936">Discovery of Novel Antimalarial Compounds Enabled by QSAR-based Virtual Screening.</a> Zhang, L., D. Fourches, A. Sedykh, H. Zhu, A. Golbraikh, S. Ekins, J. Clark, M.C. Connelly, M. Sigal, D. Hodges, A. Guiguemde, R.K. Guy, and A. Tropsha. Journal of Chemical Information and Modeling, 2013. 53(2): p. 475-492. PMID[23252936].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.   </p>


    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23324406">Phthalazinone Inhibitors of Inosine-5&#39;-monophosphate dehydrogenase from Cryptosporidium parvum.</a> Johnson, C.R., S.K. Gorla, M. Kavitha, M. Zhang, X. Liu, B. Striepen, J.R. Mead, G.D. Cuny, and L. Hedstrom. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(4): p. 1004-1007. PMID[23324406].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23321561">Tryptanthrin Derivatives as Toxoplasma Gondii Inhibitors-structure-activity-relationship of the 6-Position.</a> Krivogorsky, B., A.C. Nelson, K.A. Douglas, and P. Grundt. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(4): p. 1032-1035. PMID[23321561].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0215-022813.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314080000010">Identification of Novel Imidazo 1,2-a pyridine Inhibitors Targeting M. tuberculosis QCRB Biocompatibility and Antimicrobial Activity of Zinc(II)-Doped Hydroxyapatite, Synthesized by a Hydrothermal Method.</a> Abrahams, K.A., J.A.G. Cox, V.L. Spivey, N.J. Loman, M.J. Pallen, C. Constantinidou, R. Fernandez, C. Alemparte, M.J. Remuinan, D. Barros, L. Ballell, G.S. Besra, Z. Radovanovic, D. Veljovic, B. Jokic, S. Dimitrijevic, G. Bogdanovic, V. Kojic, R. Petrovic, and D. Janackovic. Plos One, 2012. 7(12): p. 1798. ISI[000314080000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314032900071">Antioxidant and Antimicrobial Activities of 7-Hydroxycalamenene-rich Essential Oils from Croton cajucara Benth.</a> Azevedo, M.M.B., F.C.M. Chaves, C.A. Almeida, H.R. Bizzo, R.S. Duarte, G.M. Campos-Takaki, C.S. Alviano, and D.S. Alviano. Molecules, 2013. 18(1): p. 1128-1137. ISI[000314032900071].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313694200031">Inhibition of the beta-Carbonic anhydrases from Mycobacterium tuberculosis with C-Cinnamoyl glycosides: Identification of the First Inhibitor with Anti-mycobacterial Activity.</a> Buchieri, M.V., L.E. Riafrecha, O.M. Rodriguez, D. Vullo, H.R. Morbidoni, C.T. Supuran, and P.A. Colinas. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(3): p. 740-743. ISI[000313694200031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314165300004">A New Ascochlorin Derivative from Cylindrocarpon sp FKI-4602.</a> Kawaguchi, M., T. Fukuda, R. Uchida, K. Nonaka, R. Masuma, and H. Tomoda. Journal of Antibiotics, 2013. 66(1): p. 23-29. ISI[000314165300004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313700900011">In Vitro Antifungal Activity of Dehydrozingerone and Its Fungitoxic Properties.</a> Kubra, I.R., P.S. Murthy, and L.J.M. Rao. Journal of Food Science, 2013. 78(1): p. M64-M69. ISI[000313700900011].</p>

    <p class="plaintext"><b>WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313694200024">One Pot Solvent Free Synthesis and in Vitro Antitubercular Screening of 3-Aracylphthalides against Mycobacterium tuberculosis.</a> Limaye, R.A., V.B. Kumbhar, A.D. Natu, M.V. Paradkar, V.S. Honmore, R.R. Chauhan, S.P. Gample, and D. Sarkar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(3): p. 711-714. ISI[000313694200024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314080000008">Antimicrobial Oxidized Hemp Fibers with Incorporated Silver Particles.</a> Milanovic, J., T. Mihailovic, K. Popovic, and M. Kostic. Journal of the Serbian Chemical Society, 2012. 77(12): p. 1759-1773. ISI[000314080000008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00013773700004">Antifungal Properties of Canavalia ensiformis Urease and Derived Peptides.</a> Postal, M., A.H.S. Martinelli, A.B. Becker-Ritt, R. Ligabue-Braun, D.R. Demartini, S.F.F. Ribeiro, G. Pasquali, V.M. Gomes, and C.R. Carlini. Peptides, 2012. 38(1): p. 22-32. ISI[000313773700004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313896500059">Copper-boosting Compounds: A Novel Concept for Antimycobacterial Drug Discovery.</a> &lt; Speer, A., T.B. Shrestha, S.H. Bossmann, R.J. Basaraba, G.J. Harber, S.M. Michalek, M. Niederweis, O. Kutsch, and F. Wolschendorf. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 1089-1091. ISI[000313896500059].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313404200032">Synthesis and Antiplasmodial and Antimycobacterial Evaluation of New Nitroimidazole and Nitroimidazooxazine Derivatives.</a> Tukulula, M., R.K. Sharma, M. Meurillon, A. Mahajan, K. Naran, D. Warner, J.X. Huang, B. Mekonnen, and K. Chibale. ACS Medicinal Chemistry Letters, 2013. 4(1): p. 128-131. ISI[000313404200032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p> 
    
    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00013896500049">Inhaled Microparticles Containing Clofazimine are Efficacious in Treatment of Experimental Tuberculosis in Mice.</a> Verma, R.K., W.A. Germishuizen, M.P. Motheo, A.K. Agrawal, A.K. Singh, M. Mohan, P. Gupta, U.D. Gupta, M. Cholo, R. Anderson, P.B. Fourie, and A. Misra. Antimicrobial Agents and Chemotherapy, 2013. 57(2): p. 1050-1052. ISI[000313896500049].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0215-022813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
