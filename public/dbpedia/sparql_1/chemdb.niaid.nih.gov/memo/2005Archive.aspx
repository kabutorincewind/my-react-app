

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="2005Archive.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="W5i1wIoKkVXEbEwTRw9jzLfoiDdVVKRhw66OqhLYjWd71dp7LVUaBxepYHEC0HvsKnuo/oHHyLTSXu8pj8J0glMbonoM/wBNTshUvfJ1Qh3PVKoKJzNNNvy0/g8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1FCC4E97" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>Literature Surveillance Memos</h1>

        <p>This page contains links to bi-weekly literature surveillance memos that identify relevant published research therapies on pre-clinical experimental therapies for HIV, the opportunistic infections (OIs) associated with AIDS and other viral pathogens.</p>
        <p>Note: Where available, URL links to article abstracts have been included for the convenience of the user. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users.  Abstract links to Web of Science (publishorperish.nih.gov) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>
        
        <div id="memo">
            <table id="memotable">
                <tr><th class="MemoColHIVArchive">HIV</th><th class="MemoColOIArchive">Opportunistic Infections</th><th class="MemoColOVArchive">Other Viral Pathogens</th></tr>

			<tr>
			<td><a href="HIV-LS-338.aspx" target="_blank">HIV-LS-338</a>
              (12/29/2005)
			</td>
            <td><a href="OI-LS-338.aspx" target="_blank">OI-LS-338</a>
              (12/29/2005)
			</td>
			<td><a href="dmid-ls-108.aspx" target="_blank">DMID-LS-108</a>
              (12/22/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-337.aspx" target="_blank">HIV-LS-337</a>
              (12/15/2005)
			</td>
            <td><a href="OI-LS-337.aspx" target="_blank">OI-LS-337</a>
              (12/15/2005)
			</td>
			<td><a href="dmid-ls-107.aspx" target="_blank">DMID-LS-107</a>
              (12/07/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-336.aspx" target="_blank">HIV-LS-336</a>
              (12/01/2005)
			</td>
            <td><a href="OI-LS-336.aspx" target="_blank">OI-LS-336</a>
              (12/01/2005)
			</td>
			<td><a href="dmid-ls-106.aspx" target="_blank">DMID-LS-106</a>
              (11/24/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-335.aspx" target="_blank">HIV-LS-335</a>
              (11/17/2005)
			</td>
            <td><a href="OI-LS-335.aspx" target="_blank">OI-LS-335</a>
              (11/17/2005)
			</td>
			<td><a href="dmid-ls-105.aspx" target="_blank">DMID-LS-105</a>
              (11/10/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-334.aspx" target="_blank">HIV-LS-334</a>
              (11/03/2005)
			</td>
            <td><a href="OI-LS-334.aspx" target="_blank">OI-LS-334</a>
              (11/03/2005)
			</td>
			<td><a href="dmid-ls-104.aspx" target="_blank">DMID-LS-104</a>
              (10/27/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-333.aspx" target="_blank">HIV-LS-333</a>
              (10/20/2005)
			</td>
            <td><a href="OI-LS-333.aspx" target="_blank">OI-LS-333</a>
              (10/20/2005)
			</td>
			<td><a href="dmid-ls-103.aspx" target="_blank">DMID-LS-103</a>
              (10/13/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-332.aspx" target="_blank">HIV-LS-332</a>
              (10/06/2005)
			</td>
            <td><a href="OI-LS-332.aspx" target="_blank">OI-LS-332</a>
              (10/06/2005)
			</td>
			<td><a href="dmid-ls-102.aspx" target="_blank">DMID-LS-102</a>
              (09/29/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-331.aspx" target="_blank">HIV-LS-331</a>
              (09/22/2005)
			</td>
            <td><a href="OI-LS-331.aspx" target="_blank">OI-LS-331</a>
              (09/22/2005)
			</td>
			<td><a href="dmid-ls-101.aspx" target="_blank">DMID-LS-101</a>
              (09/15/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-330.aspx" target="_blank">HIV-LS-330</a>
              (09/08/2005)
			</td>
            <td><a href="OI-LS-330.aspx" target="_blank">OI-LS-330</a>
              (09/08/2005)
			</td>
			<td><a href="dmid-ls-100.aspx" target="_blank">DMID-LS-100</a>
              (09/01/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-329.aspx" target="_blank">HIV-LS-329</a>
              (08/25/2005)
			</td>
            <td><a href="OI-LS-329.aspx" target="_blank">OI-LS-329</a>
              (08/25/2005)
			</td>
			<td><a href="dmid-ls-99.aspx" target="_blank">DMID-LS-99</a>&nbsp;&nbsp;
              (08/18/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-328.aspx" target="_blank">HIV-LS-328</a>
              (08/11/2005)
			</td>
            <td><a href="OI-LS-328.aspx" target="_blank">OI-LS-328</a>
              (08/11/2005)
			</td>
			<td><a href="dmid-ls-98.aspx" target="_blank">DMID-LS-98</a>&nbsp;&nbsp;
              (08/04/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-327.aspx" target="_blank">HIV-LS-327</a>
              (07/28/2005)
			</td>
            <td><a href="OI-LS-327.aspx" target="_blank">OI-LS-327</a>
              (07/28/2005)
			</td>
			<td><a href="dmid-ls-97.aspx" target="_blank">DMID-LS-97</a>&nbsp;&nbsp;
              (07/21/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-326.aspx" target="_blank">HIV-LS-326</a>
              (07/14/2005)
			</td>
            <td><a href="OI-LS-326.aspx" target="_blank">OI-LS-326</a>
              (07/14/2005)
			</td>
			<td><a href="dmid-ls-96.aspx" target="_blank">DMID-LS-96</a>&nbsp;&nbsp;
              (07/07/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-325.aspx" target="_blank">HIV-LS-325</a>
              (06/30/2005)
			</td>
            <td><a href="OI-LS-325.aspx" target="_blank">OI-LS-325</a>
              (06/30/2005)
			</td>
			<td><a href="dmid-ls-95.aspx" target="_blank">DMID-LS-95</a>&nbsp;&nbsp;
              (06/23/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-324.aspx" target="_blank">HIV-LS-324</a>
              (06/16/2005)
			</td>
            <td><a href="OI-LS-324.aspx" target="_blank">OI-LS-324</a>
              (06/16/2005)
			</td>
			<td><a href="dmid-ls-94.aspx" target="_blank">DMID-LS-94</a>&nbsp;&nbsp;
              (06/09/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-323.aspx" target="_blank">HIV-LS-323</a>
              (06/02/2005)
			</td>
            <td><a href="OI-LS-323.aspx" target="_blank">OI-LS-323</a>
              (06/02/2005)
			</td>
			<td><a href="dmid-ls-93.aspx" target="_blank">DMID-LS-93</a>&nbsp;&nbsp;
              (05/26/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-322.aspx" target="_blank">HIV-LS-322</a>
              (05/19/2005)
			</td>
            <td><a href="OI-LS-322.aspx" target="_blank">OI-LS-322</a>
              (05/19/2005)
			</td>
			<td><a href="dmid-ls-92.aspx" target="_blank">DMID-LS-92</a>&nbsp;&nbsp;
              (05/12/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-321.aspx" target="_blank">HIV-LS-321</a>
              (05/05/2005)
			</td>
            <td><a href="OI-LS-321.aspx" target="_blank">OI-LS-321</a>
              (05/05/2005)
			</td>
			<td><a href="dmid-ls-91.aspx" target="_blank">DMID-LS-91</a>&nbsp;&nbsp;
              (04/28/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-320.aspx" target="_blank">HIV-LS-320</a>
              (04/21/2005)
			</td>
            <td><a href="OI-LS-320.aspx" target="_blank">OI-LS-320</a>
              (04/21/2005)
			</td>
			<td><a href="dmid-ls-90.aspx" target="_blank">DMID-LS-90</a>&nbsp;&nbsp;
              (04/14/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-319.aspx" target="_blank">HIV-LS-319</a>  
              (04/07/2005)
			</td>
            <td><a href="OI-LS-319.aspx" target="_blank">OI-LS-319</a>  
              (04/07/2005)
			</td>
			<td><a href="dmid-ls-89.aspx" target="_blank">DMID-LS-89</a>&nbsp;&nbsp;
              (03/31/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-318.aspx" target="_blank">HIV-LS-318</a>  
              (03/24/2005)
			</td>
            <td><a href="OI-LS-318.aspx" target="_blank">OI-LS-318</a>  
              (03/24/2005)
			</td>
			<td><a href="dmid-ls-88.aspx" target="_blank">DMID-LS-88</a>&nbsp;&nbsp;
              (03/17/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-317.aspx" target="_blank">HIV-LS-317</a>  
              (03/10/2005)
			</td>
            <td><a href="OI-LS-317.aspx" target="_blank">OI-LS-317</a>  
              (03/10/2005)
			</td>
			<td><a href="dmid-ls-87.aspx" target="_blank">DMID-LS-87</a>&nbsp;&nbsp;
              (03/03/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-316.aspx" target="_blank">HIV-LS-316</a>  
              (02/24/2005)
			</td>
            <td><a href="OI-LS-316.aspx" target="_blank">OI-LS-316</a>  
              (02/24/2005)
			</td>
            <td><a href="dmid-ls-86.aspx" target="_blank">DMID-LS-86</a>&nbsp;&nbsp;
              (02/17/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-315.aspx" target="_blank">HIV-LS-315</a>  
              (02/10/2005)
			</td>
            <td><a href="OI-LS-315.aspx" target="_blank">OI-LS-315</a>  
              (02/10/2005)
			</td>
			<td><a href="dmid-ls-85.aspx" target="_blank">DMID-LS-85</a>&nbsp;&nbsp;
              (02/03/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-314.aspx" target="_blank">HIV-LS-314</a>  (01/27/2005)
			</td>
            <td><a href="OI-LS-314.aspx" target="_blank">OI-LS-314</a>  (01/27/2005)
			</td>
			<td><a href="dmid-ls-84.aspx" target="_blank">DMID-LS-84</a>&nbsp;&nbsp;
              (01/20/2005)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-313.aspx" target="_blank">HIV-LS-313</a>  (01/13/2005)
			</td>
            <td><a href="OI-LS-313.aspx" target="_blank">OI-LS-313</a>  (01/13/2005)
			</td>
            <td><a href="dmid-ls-83.aspx" target="_blank">DMID-LS-83</a>&nbsp;&nbsp;
              (01/06/2005)
			</td>
            </tr>
                
            </table>
			<fieldset id="memobottom">
			    <br /><br /><br />			
			    <fieldset class="submitbuttons" id="memobottomrow">
                    <ul id="memobottomrowbar">
                        <li><a href="2004Archive.aspx">2004</a></li> 
	    		    	<li><a href="2006Archive.aspx">2006</a></li>
	    			    <li><a href="2007Archive.aspx">2007</a></li>
	    			    <li><a href="2009Archive.aspx">2008-2009</a></li>
	    			    <li><a href="2010Archive.aspx">2010</a></li>
	    			    <li><a href="2011Archive.aspx">2011</a></li>
  				        <li><a href="2012Archive.aspx">2012</a></li>
   				        <li><a href="2013Archive.aspx">2013</a></li>
   				        <li><a href="2014Archive.aspx">2014</a></li>    
   				        <li><a href="2015Archive.aspx">2015</a></li>    	    		    	
   				        <li><a href="2016Archive.aspx">2016</a></li>       				        
   				        <li><a href="memos.aspx">2017</a></li>    
                    </ul>                                      
    			    <br /><br />  				    			    
			    </fieldset>
            </fieldset>
            
        </div>

    </div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
