

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-05-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Bibrq8OCArcBz+Jks4nwSjVyLV5u5ttwg3sBYPscup3VUpC7LIH4q1rtipYHuDiIkzmc7ch7YFkTmJ3KJnxRJ9CT1UP1UFjSTgl+ZsEqvAVEuVLnBNmHz/hHyJ4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0DF4255B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  May 13, 2011 - May 26, 2011</h1>  

    <p class="memofmt2-2">Pubmed citations</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21599420">New First and Second Generation Inhibitors of Human Immunodeficiency Virus-1 Integrase</a>. Pendri, A., N.A. Meanwell, K.M. Peese, and M.A. Walker. Expert Opinion on Therapeutic Patents, 2011. <b>[Epub ahead of print]</b>; PMID[21599420].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21595586">An Evaluation of the Rnase H Inhibitory Effects of Vietnamese Medicinal Plant Extracts and Natural Compounds</a>. Tai, B.H., N.D. Nhut, N.X. Nhiem, T.H. Quang, N.T. Thanh Ngan, B.T. Thuy Luyen, T.T. Huong, J. Wilson, J.A. Beutler, N.K. Ban, N.M. Cuong, and Y.H. Kim. Pharmaceutical Biology, 2011. <b>[Epub ahead of print]</b>; PMID[21595586].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576910">A Short Hairpin Loop-Structured Oligodeoxynucleotide Targeting the Virion-Associated Rnase H of HIV Inhibits HIV Production in Cell Culture and in Hupbl-Scid Mice</a>. Heinrich, J., D. Schols, and K. Moelling. Intervirology, 2011. <b>[Epub ahead of print]</b>; PMID[21576910].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21576247">Decoding the Membrane Activity of the Cyclotide Kalata B1: The Importance of Phosphatidylethanolamine Phospholipids and Lipid Organization on Hemolytic and Anti-HIV Activities</a>. Henriques, S.T., Y.H. Huang, K.J. Rosengren, H.G. Franquelim, F.A. Carvalho, A. Johnson, S. Sonza, G. Tachedjian, M.A. Castanho, N.L. Daly, and D.J. Craik. The Journal of Biological Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21576247].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21569631">Inhibitory Activity of 9-Phenylcyclohepta[D]Pyrimidinedione Derivatives against Different Strains of HIV-1 as Non-Nucleoside Reverse Transcriptase Inhibitors</a>. Huang, Y., X. Wang, X. Yu, L. Yuan, Y. Guo, W. Xu, T. Liu, J. Liu, Y. Shao, and L. Ma. Virology Journal, 2011. 8(1): p. 230; PMID[21569631].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568930">Bitter Gourd (Momordica Charantia) Is a Cornucopia of Health: A Review of Its Credited Antidiabetic, Anti-HIV, and Antitumor Properties</a>. Fang, E.F. and T.B. Ng. Current Molecular Medicine, 2011. <b>[Epub ahead of print]</b>; PMID[21568930].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568919">Recent Advances in the Dabos Family as Potent HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors</a>. Yu, M., E. Fan, J. Wu, and X. Liu. Current Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21568919].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568877">Computer-Aided Design, Synthesis, and Biological Activity Evaluation of Potent Fusion Inhibitors Targeting HIV-1 Gp41</a>. Tan, J.J., B. Zhang, X.J. Cong, L.F. Yang, B. Liu, R. Kong, Z.Y. Kui, C.X. Wang, and L.M. Hu. Medicinal Chemistry (Shariqah (United Arab Emirates)), 2011. <b>[Epub ahead of print]</b>; PMID[21568877].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21568335">Synthesis, Activity and Structural Analysis of Novel Alpha-Hydroxytropolone Inhibitors of Human Immunodeficiency Virus Reverse Transcriptase-Associated Ribonuclease H</a>. Chung, S., D.M. Himmel, J.K. Jiang, K. Wojtak, J.D. Bauman, J.W. Rausch, J.A. Wilson, J.A. Beutler, C.J. Thomas, E. Arnold, and S.F. Le Grice. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21568335].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21539377">Novel 4,4-Disubstituted Piperidine-Based C-C Chemokine Receptor-5 Inhibitors with High Potency against Human Immunodeficiency Virus-1 and an Improved Human Ether-a-Go-Go Related Gene (Herg) Profile</a>. Kazmierski, W.M., D.L. Anderson, C. Aquino, B.A. Chauder, M. Duan, R. Ferris, T. Kenakin, C.S. Koble, D.G. Lang, M.S. McIntyre, J. Peckham, C. Watson, P. Wheelan, A. Spaltenstein, M.B. Wire, A. Svolto, and M. Youngman. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21539377].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21493066">Development of Tricyclic Hydroxy-1h-Pyrrolopyridine-Trione Containing HIV-1 Integrase Inhibitors</a>. Zhao, X.Z., K. Maddali, M. Metifiot, S.J. Smith, B.C. Vu, C. Marchand, S.H. Hughes, Y. Pommier, and T.R. Burke, Jr. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2986-90; PMID[21493066].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21489793">New Monoterpene Glycosides and Phenolic Compounds from Distylium Racemosum and Their Inhibitory Activity against Ribonuclease H</a>. Kim, J.A., S.Y. Yang, A. Wamiru, J.B. McMahon, S.F. Le Grice, J.A. Beutler, and Y.H. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(10): p. 2840-4; PMID[21489793].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21478402">Advanced Glycation End Products Inhibit Both Infection and Transmission in Trans of HIV-1 from Monocyte-Derived Dendritic Cells to Autologous T Cells</a>. Nasreddine, N., C. Borde, J. Gozlan, L. Belec, V. Marechal, and H. Hocini. Journal of Immunology (Baltimore, MD: 1950), 2011. 186(10): p. 5687-95; PMID[21478402].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21462931">Synthesis of (E)-3&#39;-Phosphonoalkenyl Modified Nucleoside Phosphonates Via a Highly Stereoselective Olefin Cross-Metathesis Reaction</a>. Huang, Q. and P. Herdewijn. The Journal of Organic Chemistry, 2011. 76(10): p. 3742-53; PMID[21462931].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21567967">Structural Investigation of the Naphthyridone Scaffold: Identification of a 1,6-Naphthyridone Derivative with Potent and Selective Anti-HIV Activity.</a> <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Tabarrini%20O%22%5BAuthor%5D">Tabarrini O</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Massari%20S%22%5BAuthor%5D">Massari S</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Sancineto%20L%22%5BAuthor%5D">Sancineto L</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Daelemans%20D%22%5BAuthor%5D">Daelemans D</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Sabatini%20S%22%5BAuthor%5D">Sabatini S</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Manfroni%20G%22%5BAuthor%5D">Manfroni G</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Cecchetti%20V%22%5BAuthor%5D">Cecchetti V</a>, <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=%22Pannecouque%20C%22%5BAuthor%5D">Pannecouque C</a>. ChemMedChem, 2011. <b>[Epub ahead of print]</b>; PMID[21567967].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290062400010">HIV-1 Entry Inhibition by Small-Molecule CCR5 Antagonists: A Combined Molecular Modeling and Mutant Study Using a High-Throughput Assay</a>. Labrecque, J., M. Metz, G. Lau, M.C. Darkes, R.S.Y. Wong, D. Bogucki, B. Carpenter, G. Chen, T.S. Li, S. Nan, D. Schols, G.J. Bridger, S.P. Pricker, and R.T. Skerlj. Virology, 2011. 413(2): p. 231-243; ISI[000290062400010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289934300020">In Vitro and in Vivo Cleavage of Hiv-1 Rna by New Sofa-Hdv Ribozymes and Their Potential to Inhibit Viral Replication</a>. Laine, S., R.J. Scarborough, D. Levesque, L. Didierlaurent, K.J. Soye, M. Mougel, J.P. Perreault, and A. Gatignol. RNA biology, 2011. 8(2): p. 343-353; ISI[000289934300020].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290046300002">Dibenzocyclooctadiene Lignans from the Fruits of Schisandra Rubriflora and Their Anti-HIV-1 Activities</a>. Mu, H.X., X.S. Li, P. Fan, G.Y. Yang, J.X. Pu, H.D. Sun, Q.F. Hu, and W.L. Xiao. Journal of Asian Natural Products Research, 2011. 13(5): p. 393-399; ISI[000290046300002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289956700005">Synthetic Studies toward 13-Oxyingenol: Construction of the Fully Substituted Tetracyclic Compound</a>. Ohyoshi, T., Y. Miyazawa, K. Aoki, S. Ohmura, Y. Asuma, I. Hayakawa, and H. Kigoshi. Organic Letters, 2011. 13(9): p. 2160-2163; ISI[000289956700005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290068100010">Stability-Indicating Micellar Liquid Chromatography Method for Three Novel Derivatives of Zidovudine in Aqueous and Simulated Gastric and Intestinal Fluids Matrices</a>. Raviolo, M.A., J. Esteve-Romero, and M.C. Brinon. Journal of Chromatography A, 2011. 1218(18): p. 2540-2545; ISI[000290068100010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290019200058">Formulation Development of Retrocyclin 1 Analog RC-101 as an Anti-HIV Vaginal Microbicide Product</a>. Sassi, A.B., M.R. Cost, A.L. Cole, A.M. Cole, D.L. Patton, P. Gupta, and L.C. Rohan. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2282-2289; ISI[000290019200058].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290019200030">Selection and Characterization of HIV-1 with a Novel S68 Deletion in Reverse Transcriptase</a>. Schinazi, R.F., I. Massud, K.L. Rapp, M. Cristiano, M.A. Detorio, R.A. Stanton, M.A. Bennett, M. Kierlin-Duncan, J. Lennerstrand, and J.H. Nettles. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2054-2060; ISI[000290019200030].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290019200070">4 &#39;-C-Methyl-2 &#39;-Deoxyadenosine and 4 &#39;-C-Ethyl-2 &#39;-Deoxyadenosine Inhibit HIV-1 Replication</a>. Vu, B.C., P.L. Boyer, M.A. Siddiqui, V.E. Marquez, and S.H. Hughes. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2379-2389; ISI[000290019200070].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000290073100010">Cordymin, an Antifungal Peptide from the Medicinal Fungus Cordyceps Militaris</a>. Wong, J.H., T.B. Ng, H.X. Wang, S.C.W. Sze, K.Y. Zhang, Q. Li, and X.X. Lu. Phytomedicine, 2011. 18(5): p. 387-392; ISI[000290073100010].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0513-052611.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
