

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-01-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="N5OdnVaYSzYM75A7DFOImZMDGiMWypJQqkvMI2fRwgzPYmN4SNuGv7w7s7fhhAYJ7h457O9qt18aLzla3cIg3kWdZZgo+UjvSZLkgT+P7pkgx+xLkNyXvHD+B0M=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8CD90DEE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: December 20, 2013 - January 2, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24375637">Cyclosporin A and Its Analogs Inhibit Hepatitis B Virus Entry into Cultured Hepatocytes through Targeting a Membrane Transporter NTCP<i>.</i></a> Watashi, K., A. Sluder, T. Daito, S. Matsunaga, A. Ryo, S. Nagamori, M. Iwamoto, S. Nakajima, S. Tsukuda, K. Borroto-Esoda, M. Sugiyama, Y. Tanaka, Y. Kanai, H. Kusuhara, M. Mizokami, and T. Wakita. Hepatology, 2013. <b>[Epub ahead of print]</b>; PMID[24375637].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24372792">2-Amino-N-(2,6-dichloropyridin-3-yl)acetamide Derivatives as a Novel Class of HBV Capsid Assembly Inhibitor<i>.</i></a> Cho, M.H., H. Jeong, Y.S. Kim, J.W. Kim, and G. Jung. Journal of Viral Hepatitis, 2013. <b>[Epub ahead of print]</b>; PMID[24372792].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24269476">Antiviral Activity of Chemical Compound Isolated from Artemisia morrisonensis against Hepatitis B Virus in Vitro<i>.</i></a> Huang, T.J., S.H. Liu, Y.C. Kuo, C.W. Chen, and S.C. Chou. Antiviral Research, 2014. 101: p. 97-104; PMID[24269476].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24358168">Inhibitory Effects of Caffeic acid Phenethyl Ester Derivatives on Replication of Hepatitis C Virus<i>.</i></a> Shen, H., A. Yamashita, M. Nakakoshi, H. Yokoe, M. Sudo, H. Kasai, T. Tanaka, Y. Fujimoto, M. Ikeda, N. Kato, N. Sakamoto, H. Shindo, S. Maekawa, N. Enomoto, M. Tsubuki, and K. Moriishi. Plos One, 2013. 8(12): p. e82299; PMID[24358168].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24356905">Inhibition of Hepatitis C Virus Replication in Vitro by Xanthohumol, a Natural Product Present in Hops<i>.</i></a> Lou, S., Y.M. Zheng, S.L. Liu, J. Qiu, Q. Han, N. Li, Q. Zhu, P. Zhang, C. Yang, and Z. Liu. Planta Medica, 2013. <b>[Epub ahead of print]</b>; PMID[24356905].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24345201">Nucleotide Prodrugs of 2&#39;-Deoxy-2&#39;-spirooxetane Ribonucleosides as Novel Inhibitors of the HCV NS5B Polymerase<i>.</i></a> Jonckers, T.H., K. Vandyck, L. Vandekerckhove, L. Hu, A. Tahri, S. Van Hoof, T.I. Lin, L. Vijgen, J.M. Berke, S. Lachau-Durand, S. Bart, L. Leclercq, G. Fanning, B. Samuelsson, M. Nilsson, A. Rosenquist, K. Simmen, and P. Raboisson. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24345201].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24318893">Discovery of Small Molecule Modifiers of MicroRNAs for the Treatment of HCV Infection<i>.</i></a> Tripp, V.T. and D.D. Young. Methods in Molecular Biology, 2014. 1103: p. 153-163; PMID[24318893].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24275348">Optimization of Potency and Pharmacokinetics of Tricyclic Indole Derived Inhibitors of HCV NS5B Polymerase. Identification of Ester Prodrugs with Improved Oral Pharmacokinetics<i>.</i></a> Venkatraman, S., F. Velazquez, S. Gavalas, W. Wu, K.X. Chen, A.G. Nair, F. Bennett, Y. Huang, P. Pinto, Y. Jiang, O. Selyutin, B. Vibulbhan, Q. Zeng, C. Lesburg, J. Duca, L. Heimark, H.C. Huang, S. Agrawal, C.K. Jiang, E. Ferrari, C. Li, J. Kozlowski, S. Rosenblum, N.Y. Shih, and F.G. Njoroge. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 447-458; PMID[24275348].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p>

    <h2>EBV</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24359277">Potential Antiviral Lignans from the Roots of Saururus chinensis with Activity against Epstein-Barr Virus Lytic Replication<i>.</i></a> Cui, H., B. Xu, T. Wu, J. Xu, Y. Yuan, and Q. Gu. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>; PMID[24359277].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p>

    <h2>HSV1</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24370835">Inhibition of Ataxia telangiectasia Mutated (ATM) Kinase Suppresses Herpes Simplex Virus Type 1 (HSV-1) Keratitis<i>.</i></a> Alekseev, O., K. Donovan, and J. Azizkhan-Clifford. Investigative Ophthalmology &amp; Visual Science, 2013. <b>[Epub ahead of print]</b>; PMID[24370835].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126587">Kay-2-41, a Novel Nucleoside Analogue Inhibitor of Orthopoxviruses in Vitro and in Vivo<i>.</i></a> Duraffour, S., R. Drillien, K. Haraguchi, J. Balzarini, D. Topalis, J.J. van den Oord, G. Andrei, and R. Snoeck. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 27-37; PMID[24126587].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24374267">Study of Interferon-Beta Antiviral Activity against Herpes Simplex Virus Type 1 in Neuron-enriched Trigeminal Ganglia Cultures<i>.</i></a> Low-Calle, A.M., J. Prada-Arismendy, and J.E. Castellanos. Virus Research, 2013. <b>[Epub ahead of print]</b>; PMID[24374267].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1220-010214.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000326902300003">Molecular Design and Synthesis of HCV Inhibitors Based on Thiazolone Scaffold<i>.</i></a> Al-Ansary, G.H., M.A.H. Ismail, D.A. Abou El Ella, S. Eid, and K.A.M. Abouzid. European Journal of Medicinal Chemistry, 2013. 68: p. 19-32; ISI[000326902300003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1220-010214.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS000328179300030">Anti-Hepatitis C Virus RdRp Activity and Replication of Novel Anilinobenzothiazole Derivatives<i>.</i></a> Peng, H.K., W.C. Chen, Y.T. Lin, C.K. Tseng, S.Y. Yang, C.C. Tzeng, J.C. Lee, and S.C. Yang. Antiviral Research, 2013. 100(1): p. 269-275; ISI[000328179300030].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1220-010214.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
