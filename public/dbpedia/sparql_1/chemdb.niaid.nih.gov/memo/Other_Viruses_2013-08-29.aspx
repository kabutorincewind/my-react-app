

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-08-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="TeRwWxgt10VhMPv/yXCTTD2cyvI3LYWuV5tZ5qFfRNJ5myCMBfq/wHRgQIAwzd2YjVjwW5M/xkzFfXhXHBny7ZdDva7g6eA4fPH4dVxX/vE+8zj1PphpL0O5s0c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C921D43C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 16 - August 29, 2013</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23973993">Synthesis and anti-HBV Activity of Novel 3&#39;-N-Phenylsulfonyl docetaxel Analogs<i>.</i></a> Chang, J., Y.P. Hao, X.D. Hao, H.F. Lu, J.M. Yu, and X. Sun. Molecules, 2013. 18(9): p. 10189-10212; PMID[23973993].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23964152">Effects of SAHA on Proliferation and Apoptosis of Hepatocellular Carcinoma Cells and Hepatitis B Virus Replication<i>.</i></a> Wang, Y.C., X. Yang, L.H. Xing, and W.Z. Kong. World Journal of Gastroenterology, 2013. 19(31): p. 5159-5164; PMID[23964152].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23959305">Preclinical Characterization of GLS4: An Inhibitor of Hepatitis B Virus Core Particle Assembly<i>.</i></a> Wu, G., B. Liu, Y. Zhang, J. Li, A. Arzumanyan, M.M. Clayton, R.F. Schinazi, Z. Wang, S. Goldmann, Q. Ren, F. Zhang, and M.A. Feitelson. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[23959305].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23981392">Arbidol Inhibits Viral Entry by Interfering with Clathrin-dependent Trafficking<i>.</i></a> Blaising, J., P.L. Levy, S.J. Polyak, M. Stanifer, S. Boulant, and E.I. Pecheur. Antiviral Research, 2013. <b>[Epub ahead of print]</b>; PMID[23981392].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23947785">Aurintricarboxylic acid Modulates the Affinity of Hepatitis C Virus NS3 Helicase for Both Nucleic acid and ATP<i>.</i></a> Shadrick, W.R., S. Mukherjee, A.M. Hanson, N.L. Sweeney, and D.N. Frick. Biochemistry, 2013. <b>[Epub ahead of print]</b>; PMID[23947785].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23965980">Design, Synthesis and Antiviral Activity Studies of Schizonepetin Derivatives<i>.</i></a> Bao, B., Z. Meng, N. Li, Z. Meng, L. Zhang, Y. Cao, W. Yao, M. Shan, and A. Ding. International Journal of Molecular Sciences, 2013. 14(8): p. 17193-17203; PMID[23965980].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23959197">Chemistry and Antiviral Activity of Arrabidaea pulchra (Bignoniaceae)<i>.</i></a> Brandao, G.C., E.G. Kroon, D.E. Souza, J.D. Filho, and A.B. Oliveira. Molecules, 2013. 18(8): p. 9919-9932; PMID[23959197].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23957446">Evaluation of Anti-infective Potential of a Tribal Folklore Odina wodier RoxB against Some Selected Microbes and Herpes Simplex Virus Associated with Skin Infection<i>.</i></a> Ojha, D., H. Mukherjee, S. Ghosh, P. Bag, S. Mondal, N.S. Chandra, K.C. Mondal, A. Samanta, S. Chakrabarti, and D. Chattopadhyay. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>; PMID[23957446].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0816-082913.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
