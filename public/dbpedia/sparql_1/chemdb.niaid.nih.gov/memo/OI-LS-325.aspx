

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-325.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="54hofxpWMEp26pPJVuEP6VT5je4wM8zbUGiNbcH2vdcN3xJqXcU6zeQCjF7yCuJvwj2BQ7VECd04DQyfvx6poH5VrqyLQROuIhvIe0L3re4PKVWiV+gxDqzQjeY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6AE22DD3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-325-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47554   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Intraocular properties of hexadecyloxypropyl-cyclic-cidofovir in Guinea pigs</p>

    <p>          Lu, S, Cheng, L, Hostetler, KY, Koh, HJ, Beadle, JR, Davidson, MC, and Freeman, WR</p>

    <p>          J Ocul Pharmacol Ther <b>2005</b>.  21(3): 205-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15969637&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15969637&amp;dopt=abstract</a> </p><br />

    <p>2.     47555   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Evaluation of hybridisation on oligonucleotide microarrays for analysis of drug-resistant Mycobacterium tuberculosis</p>

    <p>          Gryadunov, D, Mikhailovich, V, Lapa, S, Roudinskii, N, Donnikov, M, Pan&#39;kov, S, Markova, O, Kuz&#39;min, A, Chernousova, L, Skotnikova, O, Moroz, A, Zasedatelev, A, and Mirzabekov, A</p>

    <p>          Clin Microbiol Infect <b>2005</b>.  11(7): 531-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15966970&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15966970&amp;dopt=abstract</a> </p><br />

    <p>3.     47556   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Inhibitory effect of hexamethylene bisacetamide on replication of human cytomegalovirus</p>

    <p>          Kitagawa, R, Hagihara, K, Uhara, M, Matsutani, K, Kirita, A, and Tanaka, J</p>

    <p>          Arch Virol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15959837&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15959837&amp;dopt=abstract</a> </p><br />

    <p>4.     47557   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Inhibitory activity of human immunodeficiency virus aspartyl protease inhibitors against Encephalitozoon intestinalis evaluated by cell culture-quantitative PCR assay</p>

    <p>          Menotti, Jean, Santillana-Hayat, Maud, Cassinat, Bruno, Sarfati, Claudine, Derouin, Francis, and Molina, Jean-Michel</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2005</b>.  49(6): 2362-2366</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     47558   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Acquisition of rifabutin resistance by a rifampicin resistant mutant of Mycobacterium tuberculosis involves an unusual spectrum of mutations and elevated frequency</p>

    <p>          Anthony, RM, Schuitema, AR, Bergval, IL, Brown, TJ, Oskam, L, and Klatser, PR</p>

    <p>          Ann Clin Microbiol Antimicrob <b>2005</b>.  4(1): 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15958167&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15958167&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47559   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activities of bis-glycosylated diamino alcohols</p>

    <p>          Tripathi, RP, Tiwari, VK, Tewari, N, Katiyar, D, Saxena, N, Sinha, S, Gaikwad, A, Srivastava, A, Chaturvedi, V, Manju, YK, Srivastava, R, and Srivastava, BS</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955703&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955703&amp;dopt=abstract</a> </p><br />

    <p>7.     47560   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          In vitro antimycobacterial activity of newly synthesised S-alkylisothiosemicarbazone derivatives and synergistic interactions in combination with rifamycins against Mycobacterium avium</p>

    <p>          De Logu, A, Saddi, M, Onnis, V, Sanna, C, Congiu, C, Borgna, R, and Cocco, MT</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955675&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15955675&amp;dopt=abstract</a> </p><br />

    <p>8.     47561   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Synthesis and study of antibacterial and antifungal activities of novel 8-methyl-7,9-diaryl-1,2,4,8-tetraazaspiro[4.5]decan-3-thiones</p>

    <p>          Balasubramanian, S, Ramalingan, C, Aridoss, G, and Kabilan, S</p>

    <p>          European Journal of Medicinal Chemistry <b>2005</b>.  40(7): 694-700</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     47562   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          In vitro activity of linezolid against multidrug-resistant Mycobacterium tuberculosis isolates</p>

    <p>          Erturan, Z and Uzun, M</p>

    <p>          Int J Antimicrob Agents <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950439&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15950439&amp;dopt=abstract</a> </p><br />

    <p>10.   47563   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Synthesis of (1,4)-naphthoquinono-[3,2-c]-1H-pyrazoles and their (1,4)-naphthohydroquinone derivatives as antifungal, antibacterial, and anticancer agents</p>

    <p>          Tandon, Vishnu K, Yadav, Dharmendra B, Chaturvedi, Ashok K, and Shukla, Praveen K</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  15(13): 3288-3291</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   47564   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Multidrug-resistant tuberculosis: news from the front</p>

    <p>          Nettleman, MD</p>

    <p>          JAMA <b>2005</b>.  293(22): 2788-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15941810&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15941810&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   47565   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of copper(II) and silver(I) complexes of hydroxynitrocoumarins: X-ray crystal structures of [Cu(hnc)2(H2O)2].2H2O and [Ag(hnc)] (hncH=4-hydroxy-3-nitro-2H-chromen-2-one)</p>

    <p>          Creaven, Bernadette S, Egan, Denise A, Kavanagh, Kevin, McCann, Malachy, Mahon, Mary, Noble, Andy, Thati, Bhumika, and Walsh, Maureen</p>

    <p>          Polyhedron <b>2005</b>.  24(8): 949-957</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   47566   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Requirement of the mymA operon for appropriate cell wall ultrastructure and persistence of Mycobacterium tuberculosis in the spleens of guinea pigs</p>

    <p>          Singh, A, Gupta, R, Vishwakarma, RA, Narayanan, PR, Paramasivan, CN, Ramanathan, VD, and Tyagi, AK</p>

    <p>          J Bacteriol <b>2005</b>.  187(12): 4173-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15937179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15937179&amp;dopt=abstract</a> </p><br />

    <p>14.   47567   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          A fluoroquinolone resistance protein from Mycobacterium tuberculosis that mimics DNA</p>

    <p>          Hegde, SS, Vetting, MW, Roderick, SL, Mitchenall, LA, Maxwell, A, Takiff, HE, and Blanchard, JS</p>

    <p>          Science <b>2005</b>.  308(5727): 1480-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15933203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15933203&amp;dopt=abstract</a> </p><br />

    <p>15.   47568   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Preparation of bis(trialkylammonium)alkane dihalides or analogs thereof as antibacterial, antiviral, parasiticidal, and antifungal agents</p>

    <p>          Widmer, Alfred Werner, Jolliffe, Katrina Anne, Wright, Lesley Catherine, and Sorrell, Tania Christine</p>

    <p>          PATENT:  WO <b>2005047230</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 94 pp.</p>

    <p>          ASSIGNEE:  (The University of Sydney, Australia</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   47569   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Discovery of substituted isoxazolecarbaldehydes as potent spermicides, acrosin inhibitors and mild anti-fungal agents</p>

    <p>          Gupta, G, Jain, RK, Maikhuri, JP, Shukla, PK, Kumar, M, Roy, AK, Patra, A, Singh, V, and Batra, S</p>

    <p>          Hum Reprod <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15932909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15932909&amp;dopt=abstract</a> </p><br />

    <p>17.   47570   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          In vitro and in vivo antifungal activities of FX0685, a novel triazole antifungal agent with potent activity against fluconazole-resistant Candida albicans</p>

    <p>          Takahata, Sho, Okutomi, Takafumi, Ohtsuka, Keiko, Hoshiko, Shigeru, Uchida, Katsuhisa, and Yamaguchi, Hideyo</p>

    <p>          Medical Mycology <b>2005</b>.  43(3): 227-233</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   47571   OI-LS-325; PUBMED-OI-6/27/2005</p>

    <p class="memofmt1-2">          Microarray and allele specific PCR detection of point mutations in Mycobacterium tuberculosis genes associated with drug resistance</p>

    <p>          Tang, X, Morris, SL, Langone, JJ, and Bockstahler, LE</p>

    <p>          J Microbiol Methods <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15923050&amp;dopt=abstract</a> </p><br />

    <p>19.   47572   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          In vitro efficacy of azithromycin and roxithromycin against Toxoplasma gondii</p>

    <p>          Tanyuksel, M, Bas, AL, Araz, E, Ozyurt, M, Albay, A, and Babur, C</p>

    <p>          TURKISH JOURNAL OF VETERINARY &amp; ANIMAL SCIENCES <b>2005</b>.  29(2): 525-529, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229422000050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229422000050</a> </p><br />

    <p>20.   47573   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          In vitro antifungal properties, structure-activity relationships and studies on the mode of action of N-phenyl, N-aryl, N-phenylalkyl maleimides and related compounds</p>

    <p>          Lopez, Silvia N, Castelli, Maria V, De Campos, Fatima, Correa, Rogerio, Cechinel Filho, Valdir, Yunes, Rosendo A, Zamora, Miguel A, Enriz, Ricardo D, Ribas, Juan C, Furlan, Ricardo LE, and Zacchino, Susana A</p>

    <p>          Arzneimittel Forschung <b>2005</b>.  55(2): 123-132</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   47574   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Hirsutellones A-E, antimycobacterial alkaloids from the insect pathogenic fungus Hirsutella nivea BCC 2594</p>

    <p>          Isaka, M, Rugseree, N, Maithip, P, Kongsaeree, P, Prabpai, S, and Thebtaranonth, Y</p>

    <p>          TETRAHEDRON <b>2005</b>.  61(23): 5577-5583, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229422300016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229422300016</a> </p><br />

    <p>22.   47575   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p><b>          Methods of treating disease through the administration of a manzamine analog or derivative</b> </p>

    <p>          Hamann, Mark T, Rao, Karumanchi Venkateswara, and Peng, Jiangnan</p>

    <p>          PATENT:  US <b>2005085554</b>  ISSUE DATE:  20050421</p>

    <p>          APPLICATION: 2004-26734  PP: 21 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   47576   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Composite genome map and recombination parameters derived from three archetypal lineages of Toxoplasma gondii</p>

    <p>          Khan, A, Taylor, S, Su, C, Mackey, AJ, Boyle, J, Cole, R, Glover, D, Tang, K, Paulsen, IT, Berriman, M, Boothroyd, JC, Pfefferkorn, ER, Dubey, JP, Ajioka, JW, Roos, DS, Wootton, JC, and Sibley, LD</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2005</b>.  33(9): 2980-2992, 13</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229544600029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229544600029</a> </p><br />
    <br clear="all">

    <p>24.   47577   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Can cell systems biology rescue drug discovery?</p>

    <p>          Butcher, EC</p>

    <p>          NATURE REVIEWS DRUG DISCOVERY <b>2005</b>.  4(6): 461-467, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229578100016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229578100016</a> </p><br />

    <p>25.   47578   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          In vitro activity of new quinoxalin 1,4-dioxide derivatives against strains of Mycobacterium tuberculosis and other mycobacteria</p>

    <p>          Zanetti, S, Sechi, LA, Molicotti, P, Cannas, S, Carta, A, Bua, A, Deriu, A, and Paglietti, G</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  25(2): 179-181</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   47579   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Heterocyclic isosters of antimycobacterial salicylanilides</p>

    <p>          Matyk, Josef, Waisser, Karel, Drazkova, Katerina, Kunes, Jiri, Klimesova, Vera, Palat, Karel, and Kaustova, Jarmila</p>

    <p>          Farmaco <b>2005</b>.  60(5): 399-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   47580   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Structure-activity relationships for the design of small-molecule inhibitors</p>

    <p>          Andricopulo, AD and Montanari, CA</p>

    <p>          MINI-REVIEWS IN MEDICINAL CHEMISTRY <b>2005</b>.  5(6): 585-593, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229534100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229534100007</a> </p><br />

    <p>28.   47581   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Production of antibacterial and antifungal metabolites by Streptomyces violaceusniger and media optimization studies for the maximum metabolite production</p>

    <p>          Tripathi, CKM, Praveen, V, Singh, V, and Bihari, V</p>

    <p>          MEDICINAL CHEMISTRY RESEARCH <b>2004</b>.  13(8-9): 790-799, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229569600018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229569600018</a> </p><br />

    <p>29.   47582   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Hassallidin A, a glycosylated lipopeptide with antifungal activity from the cyanobacterium Hassallia sp</p>

    <p>          Neuhof, T, Schmieder, P, Preussel, K, Dieckmann, R, Pham, H, Bartl, F, and von Dohren, H</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2005</b>.  68(5): 695-700, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229596900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229596900011</a> </p><br />

    <p>30.   47583   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Design, synthesis and biological evaluation of 2,4-diamino-6-methyl-5-substitutedpyrrolo[2,3- ]pyrimidines as dihydrofolate reductase inhibitors</p>

    <p>          Gangjee, A, Jain, HD, and Queener, SF</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2005</b>.  42(4): 589-594, 6</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416400018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229416400018</a> </p><br />
    <br clear="all">

    <p>31.   47584   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Host control of Mycobacterium tuberculosis is regulated by 5-lipoxygenase-dependent lipoxin production</p>

    <p>          Bafica, A, Scanga, CA, Serhan, C, Machado, F, White, S, Sher, A, and Aliberti, J</p>

    <p>          JOURNAL OF CLINICAL INVESTIGATION <b>2005</b>.  115(6): 1601-1606, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229592300034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229592300034</a> </p><br />

    <p>32.   47585   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Differential localization of alternatively spliced hypoxanthine-xanthine-guanine phosphoribosyltransferase isoforms in Toxoplasma gondii</p>

    <p>          Chaudhary, K, Donald, RGK, Nishi, M, Carter, D, Ullman, B, and Roos, DS</p>

    <p>          JOURNAL OF BIOLOGICAL CHEMISTRY <b>2005</b>.  280(23): 22053-22059, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229557900049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229557900049</a> </p><br />

    <p>33.   47586   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Preparation of dihydroquinazolines as antiviral agents</p>

    <p>          Wunberg, Tobias, Baumeister, Judith, Jeske, Mario, Nell, Peter, Nikolic, Susanne, Suessmeier, Frank, Zimmermann, Holger, Grosser, Rolf, Hewlett, Guy, Keldenich, Joerg, Lang, Dieter, and Henninger, Kerstin</p>

    <p>          PATENT:  WO <b>2005047278</b>  ISSUE DATE:  20050526</p>

    <p>          APPLICATION: 2004  PP: 50 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   47587   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Phenyl(quinazolinyl)amines as UL 97-kinase inhibitors for the treatment of human cytomegaloviral and other herpesviral infections</p>

    <p>          Herget, Thomas </p>

    <p>          PATENT:  WO <b>2005040125</b>  ISSUE DATE:  20050506</p>

    <p>          APPLICATION: 2004  PP: 56 pp.</p>

    <p>          ASSIGNEE:  (Axxima Pharmaceuticals A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   47588   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Macrophage migration inhibitory factor reduces the growth of virulent Mycobacterium tuberculosis in human macrophages</p>

    <p>          Oddo, M, Calandra, T, Bucala, R, and Meylan, PRA</p>

    <p>          INFECTION AND IMMUNITY <b>2005</b>.  73(6): 3783-3786, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229485900069">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229485900069</a> </p><br />

    <p>36.   47589   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Progress toward the therapy of hepatitis with RNAi</p>

    <p>          Randall, G</p>

    <p>          HEPATOLOGY <b>2005</b>.  41(6): 1220-1222, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229517300002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229517300002</a> </p><br />
    <br clear="all">

    <p>37.   47590   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Antimicrobial peptides as microbicidal contraceptives: prophecies for prophylactics - a mini review</p>

    <p>          Yedery, RD and Reddy, KVR</p>

    <p>          EUROPEAN JOURNAL OF CONTRACEPTION AND REPRODUCTIVE HEALTH CARE <b>2005</b>.  10(1): 32-42, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229601000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229601000008</a> </p><br />

    <p>38.   47591   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Trends in linezolid susceptibility patterns in 2002: Report from the worldwide Zyvox annual appraisal of potency and spectrum program</p>

    <p>          Ross, JE, Anderegg, TR, Sader, HS, Fritsche, TR, and Jones, RN</p>

    <p>          DIAGNOSTIC MICROBIOLOGY AND INFECTIOUS DISEASE <b>2005</b>.  52(1 ): 53-58, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229444600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229444600009</a> </p><br />

    <p>39.   47592   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          The carboxy-terminal end of the peptide deformylase from Mycobacterium tuberculosis is indispensable for its enzymatic activity</p>

    <p>          Saxena, R and Chakraborti, PK</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2005</b>.  332(2): 418-425, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229572300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229572300017</a> </p><br />

    <p>40.   47593   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Fungicidal effect of resveratrol on human infectious fungi</p>

    <p>          Jung, HJ, Hwang, IA, Sung, WS, Kang, H, Kang, BS, Seu, YB, and Lee, DG</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2005</b>.  28(5): 557-560, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229448500008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229448500008</a> </p><br />

    <p>41.   47594   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Interaction between the HCV NS3 protein and the host TBK1 protein leads to inhibition of cellular antiviral responses</p>

    <p>          Otsuka, Motoyuki, Kato, Naoya, Moriyama, Masaru, Taniguchi, Hiroyoshi, Wang, Yue, Dharel, Narayan, Kawabe, Takao, and Omata, Masao</p>

    <p>          Hepatology (Hoboken, NJ, United States) <b>2005</b>.  41(5): 1004-1012</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   47595   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          IMP dehydrogenase from the protozoan parasite Toxoplasma gondii</p>

    <p>          Sullivan, WJ, Dixon, SE, Li, C, Striepen, B, and Queener, SF</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(6): 2172-2179, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229521600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229521600003</a> </p><br />

    <p>43.   47596   OI-LS-325; SCIFINDER-OI-6/20/2005</p>

    <p class="memofmt1-2">          Synthesis of 5&#39;,9-anhydro-3-(b-D-ribofuranosyl)xanthine, and 3,5&#39;-anhydro-xanthosine as potential anti-hepatitis C virus agents</p>

    <p>          Chun, Byoung-Kwon, Wang, Peiyuan, Hassan, Abdalla, Du, Jinfa, Tharnish, Phillip M, Stuyver, Lieven J, Otto, Michael J, Schinazi, Raymond F, and Watanabe, Kyoichi A</p>

    <p>          Tetrahedron Letters <b>2005</b>.  46(16): 2825-2827</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.   47597   OI-LS-325; WOS-OI-6/19/2005</p>

    <p class="memofmt1-2">          Preclinical testing of the nitroimidazopyran PA-824 for activity against Mycobacterium tuberculosis in a series of in vitro and in vivo models</p>

    <p>          Lenaerts, AJ, Gruppo, V, Marietta, KS, Johnson, CM, Driscoll, DK, Tompkins, NM, Rose, JD, Reynolds, RC, and Orme, IM</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(6): 2294-2301, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229521600017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229521600017</a> </p><br />

    <p>45.   47598   OI-LS-325; WOS-OI-6/26/2005</p>

    <p class="memofmt1-2">          The multitude of targets for the immune system and drug therapy in the fungal cell wall</p>

    <p>          Nimrichter, L, Rodrigues, ML, Rodrigues, EG, and Travassos, LR</p>

    <p>          MICROBES AND INFECTION <b>2005</b>.  7(4): 789-798, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229534600026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229534600026</a> </p><br />

    <p>46.   47599   OI-LS-325; WOS-OI-6/26/2005</p>

    <p class="memofmt1-2">          Inhibitors of HCVNS5B polymerase. Part 2: Evaluation of the northern region of (2Z)-2-benzoylamino-3-(4-phenoxy-phenyl)-acrylic acid</p>

    <p>          Pfefferkorn, JA, Nugent, R, Gross, RJ, Greene, M, Mitchell, MA, Reding, MT, Funk, LA, Anderson, R, Wells, PA, Shelly, JA, Anstadt, R, Finzel, BC, Harris, MS, Kilkuskie, RE, Kopta, LA, and Schwende, FJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(11): 2812-2818, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229665600024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000229665600024</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
