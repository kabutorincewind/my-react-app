

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-01-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tNrbwUf/P271X/6stXNSYdMzKlBQt4+lUmyoq5YJEEbb/42tizFWlZ1D2Dd/wsV/PZY+RKrMJxFqXzdVVDdGOwXCur+jki9SGo+z1U/8JBo0sa6ayvhjS8fFviY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="473A5911" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: December 20, 2013 - January 2, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24378967">Synthesis of Novel 2-(Substituted amino)alkylthiopyrimidin-4(3H)-ones as Potential Antimicrobial Agents.</a> Attia, M.I., A.A. El-Emam, A.A. Al-Turkistani, A.L. Kansoh, and N.R. El-Brollosy. Molecules, 2013. 19(1): p. 279-290. PMID[24378967].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24156913">In Vitro Analysis of Flufenamic acid Activity against Candida albicans Biofilms.</a> Chavez-Dozal, A.A., M. Jahng, H.S. Rane, K. Asare, V.V. Kulkarny, S.M. Bernardo, and S.A. Lee. International Journal of Antimicrobial Agents, 2014. 43(1): p. 86-91. PMID[24156913].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24366745">Synergistic Effect of the Flavonoids Catechin, Quercetin and Epigallocatechin Gallate with Fluconazole Induce Apoptosis in Candida tropicalis Resistant to Fluconazole.</a> da Silva, C.R., J.B. de Andrade Neto, R.D. Campos, N.S. Figueiredo, L.S. Sampaio, H.I. Ferreira Magalhaes, B.C. Cavalcanti, D.G. Macedo, G.M. de Andrade, I.S. Pampolha Lima, G.S. Viana, M.O. de Moraes, M.D. Pinto Lobo, T.B. Grangeiro, and H.V. Nobre Junior. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24366745].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24222512">Synthesis and Antimicrobial Activity of alpha-Aminoboronic-containing Peptidomimetics.</a> Gozhina, O.V., J.S. Svendsen, and T. Lejon. Journal of Peptide Science, 2014. 20(1): p. 20-24. PMID[24222512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24300736">Synthesis and Biological Evaluation of Novel N-Substituted 1H-dibenzo[a,c]carbazole Derivatives of Dehydroabietic acid as Potential Antimicrobial Agents.</a> Gu, W., C. Qiao, S.F. Wang, Y. Hao, and T.T. Miao. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 328-331. PMID[24300736].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24218046">Synthesis, Characterization, and Antimicrobial Activity of Silver(I) and Copper(II) Complexes of Phosphate Derivatives of Pyridine and Benzimidazole.</a> Kalinowska-Lis, U., E.M. Szewczyk, L. Checinska, J.M. Wojciechowski, W.M. Wolf, and J. Ochocki. ChemMedChem, 2014. 9(1): p. 169-176. PMID[24218046].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24372722">Susceptibility Testing in Aspergillus Species Complex.</a> Lass-Florl, C. Clinical Microbiology and Infection, 2013. <b>[Epub ahead of print]</b>. PMID[24372722].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24332489">Synthesis and Evaluation of Novel Azoles as Potent Antifungal Agents.</a> Li, L., H. Ding, B. Wang, S. Yu, Y. Zou, X. Chai, and Q. Wu. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 192-194. PMID[24332489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24378871">Hylaranins: Prototypes of a New Class of Amphibian Antimicrobial Peptide from the Skin Secretion of the Oriental Broad-folded Frog, Hylarana latouchii.</a> Lin, Y., N. Hu, P. Lyu, J. Ma, L. Wang, M. Zhou, S. Guo, T. Chen, and C. Shaw. Amino Acids, 2013. <b>[Epub ahead of print]</b>. PMID[24378871].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24376206">Structural Optimization of Berberine as a Synergist to Restore Antifungal Activity of Fluconazole against Drug-resistant Candida albicans.</a> Liu, H., L. Wang, Y. Li, J. Liu, M. An, S. Zhu, Y. Cao, Z. Jiang, M. Zhao, Z. Cai, L. Dai, T. Ni, W. Liu, S. Chen, C. Wei, C. Zang, S. Tian, J. Yang, C. Wu, D. Zhang, H. Liu, and Y. Jiang. ChemMedChem, 2014. 9(1): p. 207-216. PMID[24376206].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24364425">Synthesis, Characterization, and Antimicrobial Activity of Kojic acid Grafted Chitosan Oligosaccharide.</a> Liu, X., W. Xia, Q. Jiang, Y. Xu, and P. Yu. Journal of Agricultural and Food Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24364425].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24211081">Effect of Stereochemistry, Chain Length and Sequence Pattern on Antimicrobial Properties of Short Synthetic Beta-sheet Forming Peptide Amphiphiles.</a> Ong, Z.Y., J. Cheng, Y. Huang, K. Xu, Z. Ji, W. Fan, and Y.Y. Yang. Biomaterials, 2014. 35(4): p. 1315-1325. PMID[24211081].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23905682">Photodynamic Fungicidal Efficacy of Hypericin and Dimethyl Methylene Blue against Azole-resistant Candida albicans Strains.</a> Paz-Cristobal, M.P., D. Royo, A. Rezusta, E. Andres-Ciriano, M.C. Alejandre, J.F. Meis, M.J. Revillo, C. Aspiroz, S. Nonell, and Y. Gilaberte. Mycoses, 2014. 57(1): p. 35-42. PMID[23905682].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24373124">Estimation of Flavoniods, Antimicrobial, Antitumor and Anticancer Activity of Carissa opaca Fruits.</a> Sahreen, S., M.R. Khan, R.A. Khan, and N.A. Shah. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 372. PMID[24373124].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24376900">Purpurin Triggers Caspase-independent Apoptosis in Candida dubliniensis Biofilms.</a> Tsang, P.W., A.P. Wong, H.P. Yang, and N.F. Li. PLoS One, 2013. 8(12): p. e86032. PMID[24376900].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24239582">SpALF4: A Newly Identified Anti-lipopolysaccharide Factor from the Mud Crab Scylla paramamosain with Broad Spectrum Antimicrobial Activity.</a> Zhu, L., J.F. Lan, Y.Q. Huang, C. Zhang, J.F. Zhou, W.H. Fang, X.J. Yao, H. Wang, and X.C. Li. Fish and Shellfish Immunology, 2014. 36(1): p. 172-180. PMID[24239582].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24314670">Rational Design, Synthesis and Antitubercular Evaluation of Novel 2-(Trifluoromethyl)phenothiazine-[1,2,3]triazole Hybrids.</a> Addla, D., A. Jallapally, D. Gurram, P. Yogeeswari, D. Sriram, and S. Kantevari. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 233-236. PMID[24314670].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24359458">Preliminary Antimycobacterial Study on Selected Turkish Plants (Lamiaceae) against Mycobacterium tuberculosis and Search for Some Phenolic Constituents.</a> Askun, T., E.M. Tekwu, F. Satil, S. Modanlioglu, and H. Aydeniz. BMC Complementary and Alternative Medicine, 2013. 13: p. 365. PMID[24359458].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24189255">Bactericidal Activity and Mechanism of Action of AZD5847, a Novel Oxazolidinone for Treatment of Tuberculosis.</a> Balasubramanian, V., S. Solapure, H. Iyer, A. Ghosh, S. Sharma, P. Kaur, R. Deepthi, V. Subbulakshmi, V. Ramya, V. Ramachandran, M. Balganesh, L. Wright, D. Melnick, S.L. Butler, and V.K. Sambandamurthy. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 495-502. PMID[24189255].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24325645">A Noncompetitive Inhibitor for Mycobacterium tuberculosis&#39;s Class IIa Fructose 1,6-Bisphosphate aldolase.</a> Capodagli, G.C., W.G. Sedhom, M. Jackson, K.A. Ahrendt, and S.D. Pegan. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24325645].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24047344">Antibacterial Activity of and Resistance to Small Molecule Inhibitors of the ClpP Peptidase.</a> Compton, C.L., K.R. Schmitz, R.T. Sauer, and J.K. Sello. ACS Chemical Biology, 2013. 8(12): p. 2669-2677. PMID[24047344].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24341950">Synthesis, Characterization and in Vitro Extracellular and Intracellular Activity against Mycobacterium tuberculosis Infection of New Second-line Antitubercular Drug-palladium Complexes.</a> Giovagnoli, S., M.L. Marenzoni, M. Nocchetti, C. Santi, P. Blasi, A. Schoubben, and M. Ricci. Journal of Pharmacy and Pharmacology, 2014. 66(1): p. 106-121. PMID[24341950].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24314672">A Novel Indigoid anti-Tuberculosis Agent.</a> Klein, L.L., V. Petukhova, B. Wan, Y. Wang, B.D. Santasiero, D.C. Lankin, G.F. Pauli, and S.G. Franzblau. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 268-270. PMID[24314672].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24352433">Indoleamides are Active against Drug-resistant Mycobacterium tuberculosis.</a> Lun, S., H. Guo, O.K. Onajole, M. Pieroni, H. Gunosewoyo, G. Chen, S.K. Tipparaju, N.C. Ammerman, A.P. Kozikowski, and W.R. Bishai. Nature Communications, 2013. 4: p. 2907. PMID[24352433].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24315189">Synthesis of 3-(3-Aryl-pyrrolidin-1-yl)-5-aryl-1,2,4-triazines That Have Antibacterial Activity and Also Inhibit Inorganic Pyrophosphatase.</a> Lv, W., B. Banerjee, K.L. Molland, M.N. Seleem, A. Ghafoor, M.I. Hamed, B. Wan, S.G. Franzblau, A.D. Mesecar, and M. Cushman. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 406-418. PMID[24315189].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24047443">Antimycobacterial Activity of Cyclic Dipeptides Isolated from Bacillus sp. N Strain Associated with Entomopathogenic Nematode.</a> NISHANTH KUMAR, S. AND C. MOHANDAS. Pharmaceutical Biology, 2014. 52(1): p. 91-96. PMID[24047443].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126580">Optimization of Pyrrolamides as Mycobacterial GyrB ATPase Inhibitors: Structure-Activity Relationship and in Vivo Efficacy in a Mouse Model of Tuberculosis.</a> P, S.H., S. Solapure, K. Mukherjee, V. Nandi, D. Waterson, R. Shandil, M. Balganesh, V.K. Sambandamurthy, A.K. Raichurkar, A. Deshpande, A. Ghosh, D. Awasthy, G. Shanbhag, G. Sheikh, H. McMiken, J. Puttur, J. Reddy, J. Werngren, J. Read, M. Kumar, M. R, M. Chinnapattu, P. Madhavapeddi, P. Manjrekar, R. Basu, S. Gaonkar, S. Sharma, S. Hoffner, V. Humnabadkar, V. Subbulakshmi, and V. Panduga. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 61-70. PMID[24126580].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24139881">IQG-607 Abrogates the Synthesis of Mycolic acids and Displays Intracellular Activity against Mycobacterium tuberculosis in Infected Macrophages.</a> Rodrigues-Junior, V.S., A.A. Dos Santos Junior, A.D. Villela, J.M. Belardinelli, H.R. Morbidoni, L.A. Basso, M.M. Campos, and D.S. Santos. International Journal of Antimicrobial Agents, 2014. 43(1): p. 82-85. PMID[24139881].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126578">New 2-Thiopyridines as Potential Candidates for Killing Both Actively Growing and Dormant Mycobacterium tuberculosis Cells.</a> Salina, E., O. Ryabova, A. Kaprelyants, and V. Makarov. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 55-60. PMID[24126578].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24332625">Synthesis and Antimycobacterial Activity of Novel Camphane-based Agents.</a> Stavrakov, G., I. Philipova, V. Valcheva, and G. Momekov. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(1): p. 165-167. PMID[24332625].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24321011">Theoretical and Experimental Study of Inclusion Complexes Formed by Isoniazid and Modified beta-Cyclodextrins: 1H NMR Structural Determination and Antibacterial Activity Evaluation.</a> Teixeira, M.G., J.V. de Assis, C.G. Soares, M.F. Venancio, J.F. Lopes, C.S. Nascimento, Jr., C.P. Anconi, G.S. Carvalho, C.S. Lourenco, M.V. de Almeida, S.A. Fernandes, and W.B. de Almeida. The Journal of Physical Chemistry B, 2013. <b>[Epub ahead of print]</b>. PMID[24321011].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24100489">Plasmodium falciparum Polymorphisms Associated with Ex Vivo Drug Susceptibility and Clinical Effectiveness of Artemisinin-based Combination Therapies in Benin.</a> Dahlstrom, S., A. Aubouy, O. Maiga-Ascofare, J.F. Faucher, A. Wakpo, S. Ezinmegnon, A. Massougbodji, P. Houze, E. Kendjo, P. Deloron, J. Le Bras, and S. Houze. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 1-10. PMID[24100489].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24366746">Dihydroquinazolinone Inhibitors of Proliferation of Blood and Liver Stage Malaria Parasites.</a> Derbyshire, E.R., J. Min, W.A. Guiguemde, J.A. Clark, M.C. Connelly, A.D. Magalhaes, R.K. Guy, and J. Clardy. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24366746].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145526">Evidence for Pyronaridine as a Highly Effective Partner Drug for Treatment of Artemisinin-resistant Malaria in a Rodent Model.</a> Henrich, P.P., C. O&#39;Brien, F.E. Saenz, S. Cremers, D.E. Kyle, and D.A. Fidock. Antimicrobial Agents and Chemotherapy, 2014. 58(1): p. 183-195. PMID[24145526].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24369685">Quinine Dimers are Potent Inhibitors of the Plasmodium falciparum Chloroquine Resistance Transporter and are Active against Quinoline-resistant P. falciparum.</a> Hrycyna, C.A., R.L. Summers, A.M. Lehane, M.M. Pires, H. Namanja, K. Bohn, J. Kuriakose, M. Ferdig, P.P. Henrich, D.A. Fidock, K. Kirk, J. Chmielewski, and R.E. Martin. ACS Chemical Biology, 2013. <b>[Epub ahead of print]</b>. PMID[24369685].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24350818">Myristicyclins A and B: Antimalarial Procyanidins from Horsfieldia spicata from Papua New Guinea.</a> Lu, Z., R.M. Van Wagoner, C.D. Pond, A.R. Pole, J.B. Jensen, D. Blankenship, B.T. Grimberg, R. Kiapranis, T.K. Matainaho, L.R. Barrows, and C.M. Ireland. Organic Letters, 2013. <b>[Epub ahead of print]</b>. PMID[24350818].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24268543">Antiprotozoal Activity of Dicationic 3,5-Diphenylisoxazoles, Their Prodrugs and Aza-analogues.</a> Patrick, D.A., S.A. Bakunov, S.M. Bakunova, T. Wenzler, R. Brun, and R.R. Tidwell. Bioorganic &amp; Medicinal Chemistry, 2014. 22(1): p. 559-576. PMID[24268543].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24354322">Quinoline-Pyrimidine Hybrids: Synthesis, Antiplasmodial Activity, SAR and Mode of Action Studies.</a> Singh, K., H. Kaur, P.J. Smith, C. de Kock, K. Chibale, and J. Balzarini. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[24354322].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br />

    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23891618">Nanoparticle Formulations of Decoquinate Increase Antimalarial Efficacy against Liver Stage Plasmodium Infections in Mice.</a> Wang, H., Q. Li, S. Reyes, J. Zhang, Q. Zeng, P. Zhang, L. Xie, P.J. Lee, N. Roncal, V. Melendez, M. Hickman, and M.P. Kozar. Nanomedicine, 2014. 10(1): p. 57-65. PMID[23891618].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24295325">Discrimination of Potent Inhibitors of Toxoplasma gondii Enoyl-acyl Carrier Protein Reductase by a Thermal Shift Assay.</a> Afanador, G.A., S.P. Muench, M. McPhillie, A. Fomovska, A. Schon, Y. Zhou, G. Cheng, J. Stec, J.S. Freundlich, H.M. Shieh, J.W. Anderson, D.P. Jacobus, D.A. Fidock, A.P. Kozikowski, C.W. Fishwick, D.W. Rice, E. Freire, R. McLeod, and S.T. Prigge. Biochemistry, 2013. 52(51): p. 9155-9166. PMID[24295325].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">41. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24366743">Spiroindolone That Inhibits PfATPase4 Is a Potent, Cidal Inhibitor of Toxoplasma gondii Tachyzoites in Vitro and in Vivo (132 Characters, sp).</a> Zhou, Y., A. Fomovska, S. Muench, B.S. Lai, E. Mui, and R. McLeod. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[24366743].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1220-010214.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327716800006">Pyrimidine-5-carbonitriles - Part III: Synthesis and Antimicrobial Activity of Novel 6-(2-Substituted propyl)-2,4-disubstituted pyrimidine-5-carbonitriles.</a> Al-Deeb, O.A., A.A. Al-Turkistani, E.S. Al-Abdullah, N.R. El-Brollosy, E.E. Habib, and A.A. El-Emam. Heterocyclic Communications, 2013. 19(6): p. 411-419. ISI[000327716800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327603600001">1-Aryl-3-(1H-imidazol-1-yl)propan-1-ol Esters: Synthesis, anti-Candida Potential and Molecular Modeling Studies.</a> Attia, M.I., A.A. Radwan, A.S. Zakaria, M.S. Almutairi, and S.W. Ghoneim. Chemistry Central Journal, 2013. 1: p. 168. ISI[000327603600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327366000005">Study on the Synthesis and Antibacterial Properties of Silver-chitosan Composite.</a> Bao, Y., C.R. Wei, E.Q. Su, Q. Zhang, and Y.X. Bai. Polymers &amp; Polymer Composites, 2013. 21(8): p. 519-524. ISI[000327366000005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327727900010">Antifungal Activity of a Library of Cyclitols and Related Compounds.</a> Bellomo, A., A. Bertucci, V. de la Sovera, G. Carrau, M. Raimondi, S. Zacchino, H.A. Stefani, and D. Gonzalez. Letters in Drug Design &amp; Discovery, 2014. 11(1): p. 67-75. ISI[000327727900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327293700011">Synthesis and Biological Activity of New Chiral N-Acylsulfonamide bis-oxazolidin-2-ones.</a> Bouasla, R., H. Berredjem, M. Berredjem, M. Ibrahim-Oualid, A. Allaoui, M. Lecouvey, and N.E. Aouf. Journal of Heterocyclic Chemistry, 2013. 50(6): p. 1328-1332. ISI[000327293700011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000326902300011">Synthesis and Antimicrobial Activity of Novel 5-(1-Adamantyl)-2-aminomethyl-4-substituted-1,2,4-triazoline-3-thiones.</a> El-Emam, A.A., A.M.S. Al-Tamimi, M.A. Al-Omar, K.A. Alrashood, and E.E. Habib. European Journal of Medicinal Chemistry, 2013. 68: p. 96-102. ISI[000326902300011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327695900006">Synthesis and Biological Evaluation of bis-Imidazolidineiminothiones: A Comparative Study.</a> El-Sharief, M., Z. Moussa, and A.M.S. El-Sharief. Archiv der Pharmazie, 2013. 346(7): p. 542-555. ISI[000327695900006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327408500008">Role of Polyphenols in the Antimicrobial Activity of Ethanol Tamarindus indica L Leaves Fluid Extract.</a> Escalona-Arranz, J.C., R. Perez-Roses, I. Urdaneta-Laffita, H. Morris-Quevedo, M.I. Camacho-Pozo, J. Rodriguez-Amado, and G. Sierra Gonzalez. Boletin Latinoamericano y del Caribe de Plantas Medicinales y Aromaticas, 2013. 12(5): p. 516-522. ISI[000327408500008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327363500002">Identification of Bacillomycin D from Bacillus subtilis fmbJ and Its Inhibition Effects against Aspergillus flavus.</a> Gong, Q.W., C. Zhang, F.X. Lu, H.Z. Zhao, X.M. Bie, and Z.X. Lu. Food Control, 2014. 36(1): p. 8-14. ISI[000327363500002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br />  

    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327787700061">Anti-tubercular Agents. Part 8: Synthesis, Antibacterial and Antitubercular Activity of 5-Nitrofuran Based 1,2,3-Triazoles.</a> Kamal, A., S.M.A. Hussaini, S. Faazil, Y. Poornachandra, G.N. Reddy, C.G. Kumar, V.S. Rajput, C. Rani, R. Sharma, I.A. Khan, and N.J. Babu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6842-6846. ISI[000327787700061].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327257000003">Synthesis and 3D-QSAR Analysis of 2-Chloroquinoline Derivatives as H37RV MTB Inhibitors.</a> Khunt, R.C., V.M. Khedkar, and E.C. Coutinho. Chemical Biology &amp; Drug Design, 2013. 82(6): p. 669-684. ISI[000327257000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327697400017">Chemical Composition and Antimicrobial Activity of the Essential Oil of Stachys officinalis (L.) Trevis. (Lamiaceae).</a> Lazarevic, J.S., A.S. Dordevic, D.V. Kitic, B.K. Zlatkovic, and G.S. Stojanovic. Chemistry &amp; Biodiversity, 2013. 10(7): p. 1335-1349. ISI[000327697400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327461300003">Novel Benzothiazole Derivatives with a Broad Antifungal Spectrum: Design, Synthesis and Structure-Activity Relationships.</a> Liu, Y., Y. Wang, G.Q. Dong, Y.Q. Zhang, S.C. Wu, Z.Y. Miao, J.Z. Yao, W.N. Zhang, and C.Q. Sheng. MedChemComm, 2013. 4(12): p. 1551-1561. ISI[000327461300003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">55. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327787700053">Design, Synthesis and Evaluation of 6-(4-((Substituted-1H-1,2,3-triazol-4-yl)methyl)piperazin-1-yl)phenanthridine Analogues as Antimycobacterial Agents.</a> Nagesh, H.N., K.M. Naidu, D.H. Rao, J.P. Sridevi, D. Sriram, P. Yogeeswari, and K. Sekhar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(24): p. 6805-6810. ISI[000327787700053].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">56. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327465000003">In Vitro and in Vivo Evaluation of Antimicrobial Activities of Essential Oils Extracted from Some Indigenous Spices.</a> Naveed, R., I. Hussain, M.S. Mahmood, and M. Akhtar. Pakistan Veterinary Journal, 2013. 33(4): p. 413-417. ISI[000327465000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">57. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327291700001">Synthesis, Antibacterial and Antifungal Activity of 2-Amino-1,4-naphthoquinones Using Silica-supported Perchloric acid (HClO4-SiO2) as a Mild, Recyclable and Highly Efficient Heterogeneous Catalyst.</a> Sharma, U., D. Katoch, S. Sood, N. Kumar, B. Singh, A. Thakur, and A. Gulati. Indian Journal of Chemistry Section B - Organic Chemistry Including Medicinal Chemistry, 2013. 52(11): p. 1431-1440. ISI[000327291700001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br /> 

    <p class="plaintext">58. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000327682400017">Inhibition Studies on Mycobacterium tuberculosis N-Acetylglucosamine-1-phosphate uridyltransferase (GlmU).</a> Tran, A.T., D.Y. Wen, N.P. West, E.N. Baker, W.J. Britton, and R.J. Payne. Organic &amp; Biomolecular Chemistry, 2013. 11(46): p. 8113-8126. ISI[000327682400017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1220-010214.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
