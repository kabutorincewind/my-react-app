

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-02-28.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Nw+64xjNZaxshZEd7X7FDmHjGmvLdFMz+KG3NN++uOUUn53LXfo10xpz5tv2kOBJKFNzJLbGtplJRdz2oqykvR5DEosi/2mvLCJf8Qony+pAY2SoDeLTiZix6fg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="58C98F97" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: February 15 - February 28, 2013</h1>

    <h2>HCMV</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=23426791">Allitridin Inhibits Human Cytomegalovirus Replication in Vitro</a><i>.</i> Zhang, J., H. Wang, Z.D. Xiang, S.N. Shu, and F. Fang. Molecular Medicine Reports, 2013.  <b>[Epub ahead of print]</b>; PMID[23426791].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0215-022813.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=23422646">Oleanolic acid and Ursolic acid: Novel Hepatitis C Virus Antivirals That Inhibit NS5B Activity</a><i>.</i> Kong, L., S. Li, Q. Liao, Y. Zhang, R. Sun, X. Zhu, Q. Zhang, J. Wang, X. Wu, X. Fang, and Y. Zhu. Antiviral Research, 2013.  <b>[Epub ahead of print]</b>; PMID[23422646].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0215-022813.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=23422647">BST2/Tetherin Inhibits Hepatitis C Virus Production in Human Hepatoma Cells</a><i>.</i> Pan, X.B., X.W. Qu, D. Jiang, X.L. Zhao, J.C. Han, and L. Wei. Antiviral Research, 2013.  <b>[Epub ahead of print]</b>; PMID[23422647].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0215-022813.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/?term=23444170">Molecular Design, Synthesis and Cell Based HCV Replicon Assay of Novel Benzoxazole Derivatives<i>.</i></a> Ismail, M.A., M. Adel, N.S. Ismail, and K.A. Abouzid. Drug Research, 2013.  <b>[Epub ahead of print]</b>; PMID[23444170].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_0215-022813.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000314146500006">Selective Estrogen Receptor Modulators Inhibit Hepatitis C Virus Infection at Multiple Steps of the Virus Life Cycle</a><i>.</i> Murakami, Y., M. Fukasawa, Y. Kaneko, T. Suzuki, T. Wakita, and H. Fukazawa. Microbes and Infection, 2013. 15(1): p. 45-55; ISI[000314146500006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0215-022813.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000313393800020">Fluorescent Primuline Derivatives Inhibit Hepatitis C Virus NS3-Catalyzed RNA Unwinding, Peptide Hydrolysis and Viral Replicase Formation</a><i>.</i> Ndjomou, J., R. Kolli, S. Mukherjee, W.R. Shadrick, A.M. Hanson, N.L. Sweeney, D. Bartczak, K.L. Li, K.J. Frankowski, F.J. Schoenen, and D.N. Frick. Antiviral Research, 2012. 96(2): p. 245-255; ISI[000313393800020].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0215-022813.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%20000313547700006">CD40 Inhibits Replication of Hepatitis C Virus in Primary Human Hepatocytes by C-Jun N Terminal Kinase Activation Independent from the Interferon Pathway</a><i>.</i> Rau, S.J., E. Hildt, K. Himmelsbach, R. Thimme, T. Wakita, H.E. Blum, and R. Fischer. Hepatology, 2013. 57(1): p. 23-37; ISI[000313547700006].</p>

    <p class="plaintext"><b>[WOS]</b> OV_0215-022813.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
