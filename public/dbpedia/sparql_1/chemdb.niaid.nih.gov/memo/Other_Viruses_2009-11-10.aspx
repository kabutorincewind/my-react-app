

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="m4PJlrviOXT6/c7A2MT3wjJh2xvssoEnpON76y3BgCl/LX60ADRvCzV/FB6FrfK85ZzQw+Mhl+FLNYqC5+JFz1b9ierL+3RvwKZAq7ZNwvGOEiM4HxYo/X5YEd0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E89C4C98" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1"><b>Other Viruses Citation List</b><b>: </b></p>

    <p class="memofmt2-h1">October 28, 2009 -November 10, 2009</p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19901090?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=7">A novel class of meso-tetrakis-porphyrin derivatives exhibit potent activities against hepatitis C virus genotype 1b replicons in vitro.</a></p>

    <p class="NoSpacing">Cheng Y, Tsou LK, Cai J, Aya T, Dutschman GE, Gullen EA, Grill SP, Chen AP, Lindenbach BD, Hamilton AD, Cheng YC.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Nov 9. [Epub ahead of print]PMID: 19901090 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19894743?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=22">In Vitro and In Vivo Isotope Effects with Hepatitis C Protease Inhibitors: Enhanced Plasma Exposure of Deuterated Telaprevir versus Telaprevir in Rats.</a></p>

    <p class="NoSpacing">Maltais F, Jung YC, Chen M, Tanoury J, Perni RB, Mani N, Laitinen L, Huang H, Liao S, Gao H, Tsao H, Block E, Ma C, Shawgo RS, Town C, Brummel CL, Howe D, Pazhanisamy S, Raybuck S, Namchuk M, Bennani YL.</p>

    <p class="NoSpacing">J Med Chem. 2009 Nov 6. [Epub ahead of print]PMID: 19894743 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19821520?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=107">Cyclosporine A inhibits hepatitis C virus nonstructural protein 2 through cyclophilin A.</a></p>

    <p class="NoSpacing">Ciesek S, Steinmann E, Wedemeyer H, Manns MP, Neyts J, Tautz N, Madan V, Bartenschlager R, von Hahn T, Pietschmann T.</p>

    <p class="NoSpacing">Hepatology. 2009 Nov;50(5):1638-45.PMID: 19821520 [PubMed - in process]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19767736?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=130">Conformational inhibition of the hepatitis C virus internal ribosome entry site RNA.</a></p>

    <p class="NoSpacing">Parsons J, Castaldi MP, Dutta S, Dibrov SM, Wyles DL, Hermann T.</p>

    <p class="NoSpacing">Nat Chem Biol. 2009 Nov;5(11):823-5. Epub 2009 Sep 20.PMID: 19767736 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19664635?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=181">The anti-hepatitis C agent nitazoxanide induces phosphorylation of eukaryotic initiation factor 2alpha via protein kinase activated by double-stranded RNA activation.</a></p>

    <p class="NoSpacing">Elazar M, Liu M, McKenna SA, Liu P, Gehrig EA, Puglisi JD, Rossignol JF, Glenn JS.</p>

    <p class="NoSpacing">Gastroenterology. 2009 Nov;137(5):1827-35. Epub 2009 Aug 4.PMID: 19664635 [PubMed - in process]</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19015358?ordinalpos=6&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A 6-Aminoquinolone Compound, WC5, with Potent and Selective Anti-Human Cytomegalovirus Activity.</a></p>

    <p class="NoSpacing">Mercorelli B, Muratore G, Sinigalia E, Tabarrini O, Biasolo MA, Cecchetti V, Palù G, Loregian A.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2008 Nov 17. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19015358 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">Hepatitis b virus</p> 

    <p class="ListParagraph">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19853440?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=71">Anti-HBV agents. Part 3: preliminary structure-activity relationships of tetra-acylalisol A derivatives as potent hepatitis B virus inhibitors.</a></p>

    <p class="NoSpacing">Zhang Q, Jiang ZY, Luo J, Ma YB, Liu JF, Guo RH, Zhang XM, Zhou J, Niu W, Du FF, Li L, Li C, Chen JJ.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Dec 1;19(23):6659-65. Epub 2009 Oct 7.PMID: 19853440 [PubMed - in process]</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="ListParagraph">8. Popovici-Muller, Janeta, et al.</p>

    <p class="NoSpacing">Pyrazolo[1,5-a]pyrimidine-based inhibitors of HCV polymerase.</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 2009 19(22):6331-6.</p>

    <p class="ListParagraph">9. Ruebsam, Frank, et al.</p>

    <p class="NoSpacing">Discovery of tricyclic 5,6-dihydro-1H-pyridin-2-ones as novel, potent, and orally bioavailable inhibitors of HCV NS5B polymerase.</p>

    <p class="NoSpacing">BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 2009 19(22): 6404-12.</p>

    <p class="ListParagraph">10. Chen, Ke-Xian, et al.</p>

    <p class="NoSpacing">QSAR Analysis of 1,1-Dioxoisothiazole and Benzo[b]thiophene-1,1-dioxide Derivatives as Novel Inhibitors of Hepatitis C Virus NS5B Polymerase.</p>

    <p class="NoSpacing">ACTA CHIMICA SLOVENICA. 2009 56(3): 684-93.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
