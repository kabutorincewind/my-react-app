

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-149.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="j/3kAG1UD2upX4UXw96FeJwNs1jiXlvdsrFgmiZSMhppKJ5Q4tUxwMCuITPJSLOWqPboYFtS9Ea+tiULb1jeOtc5evl6THO61u2e6BzMUvjqriaQsWJMqDHAdKc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3DCFA080" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-149-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61258   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Microbial, viral and mammalian susceptibility to agents that affect cell growth and metabolism, and mode of action of compounds</p>

    <p>          Kjeldsen, Naja Joslin</p>

    <p>          PATENT:  EP <b>1785721</b>  ISSUE DATE: 20070516</p>

    <p>          APPLICATION: 2005-60363  PP: 53pp.</p>

    <p>          ASSIGNEE:  (Stratos Bio Ltd., Switz.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61259   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Diaryl ureas for treating virus infections</p>

    <p>          Weber, Olaf and Riedl, Bernd</p>

    <p>          PATENT:  WO <b>2007068383</b>  ISSUE DATE:  20070621</p>

    <p>          APPLICATION: 2006  PP: 114pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61260   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Unique Antiviral Mechanism Discovered in Anti-Hepatitis B Virus Research With a Natural Product Analogue</p>

    <p>          Ying, C. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2007</b>.  104(20): 8526-8531</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246599900060">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246599900060</a> </p><br />

    <p>4.     61261   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Arenaviruses and Filoviruses</p>

    <p>          Rollin, Pierre E, Nichol, Stuart T, Zaki, Sherif, and Ksiazek, Thomas G</p>

    <p>          Man. Clin. Microbiol. (9th Ed.) <b> 2007</b>.  2: 1510-1522</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61262   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          First Synthesis and Antimicrobial Activity of N- and S-Alpha-L-Arabinopyranosyl-1,2,4-Triazoles</p>

    <p>          Khalil, N.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2007</b>.  26(4): 361-377</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246536100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246536100004</a> </p><br />

    <p>6.     61263   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Virus-interacting layered phyllosilicates and methods of inactivating viruses</p>

    <p>          Hughes, John</p>

    <p>          PATENT:  US <b>2007031512</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2005-65018  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Amcol International Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     61264   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          N-heterocyclylpyrrolidine carboxamide derivatives, processes for preparing them, pharmaceutical compositions containing them, and their use as antiviral agents</p>

    <p>          Schmitz, Franz Ulrich, Roberts, Christopher Don, Abadi, Ali Dehghani Mohammad, Griffith, Ronald Conrad, Leivers, Martin Robert, Slobodov, Irina, and Rai, Roopa</p>

    <p>          PATENT:  WO <b>2007070600</b>  ISSUE DATE:  20070621</p>

    <p>          APPLICATION: 2006  PP: 159pp.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     61265   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of antiviral azanucleoside derivatives as inhibitors RNA-dependent viral polymerases</p>

    <p>          Chiacchio, Ugo, Mastino, Antonio, Merino, Pedro, and Romeo, Giovanni</p>

    <p>          PATENT:  WO <b>2007065883</b>  ISSUE DATE:  20070614</p>

    <p>          APPLICATION: 2006  PP: 45pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare P. Angeletti S.p.A., Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61266   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Polycyclic phenolic compounds and use in treating viral infections</p>

    <p>          Dugourd, Dominique</p>

    <p>          PATENT:  WO <b>2007062528</b>  ISSUE DATE:  20070607</p>

    <p>          APPLICATION: 2006  PP: 77pp.</p>

    <p>          ASSIGNEE:  (Migenix Corporation, Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61267   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of heteroaryl substituted pyrrolo[2,3-b]pyridines and pyrrolo[2,3-b]pyrimidines as Janus kinase inhibitors</p>

    <p>          Rodgers, James D, Shepard, Stacey, Maduskuie, Thomas P, Wang, Haisheng, Falahatpisheh, Nikoo, Rafalski, Maria, Arvanitis, Argyrios G, Storace, Louis, Jalluri, Ravi Kumar, Fridman, Jordan S, and Vaddi, Krishna</p>

    <p>          PATENT:  US <b>2007135461</b>  ISSUE DATE:  20070614</p>

    <p>          APPLICATION: 2006-47721  PP: 222pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61268   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Pentaazacyclopenta[b]fluoren-9-ones as novel inhibitors of cysteine proteases, their preparation, pharmaceutical compositions thereof and their therapeutic applications</p>

    <p>          Guedat, Philippe, Boissy, Guillaume, Borg-Capra, Catherine, Colland, Frederic, Daviet, Laurent, Formstecher, Etienne, Jacq, Xavier, Rain, Jean-Christophe, Delansorne, Remi, Peretto, Ilaria, and Vignando, Stefano</p>

    <p>          PATENT:  WO <b>2007066200</b>  ISSUE DATE:  20070614</p>

    <p>          APPLICATION: 2006  PP: 68pp.</p>

    <p>          ASSIGNEE:  (Hybrigenics S. A., Fr.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61269   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral triterpenoids from the medicinal plant Schefflera heptaphylla</p>

    <p>          Li, Yaolan, Jiang, Renwang, Ooi, Linda SM, But, Paul PH, and Ooi, Vincent EC</p>

    <p>          Phytother. Res. <b>2007</b>.  21(5): 466-470</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   61270   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Mechanisms of resistance to antiviral agents</p>

    <p>          Shafer, Robert W, Einav, Shirit, and Chou, Sunwen</p>

    <p>          Man. Clin. Microbiol. (9th Ed.) <b> 2007</b>.  2: 1689-1704</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61271   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral agents</p>

    <p>          Lurain, Nell S and Thompson, Kenneth D</p>

    <p>          Man. Clin. Microbiol. (9th Ed.) <b> 2007</b>.  2: 1669-1688</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   61272   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of pyridinylcarboxamide derivatives as cytokine inhibitors</p>

    <p>          Delaet, Nancy GL, Montalban, Antonio Garrido, Larson, Christopher, Lum, Christopher, Pei, Yazhong, Sebo, Lubomir, Urban, Jan, Wang, Zhijun, and Boman, Erik</p>

    <p>          PATENT:  WO <b>2007056016</b>  ISSUE DATE:  20070518</p>

    <p>          APPLICATION: 2006  PP: 188pp.</p>

    <p>          ASSIGNEE:  (Kemia, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   61273   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Preparation of bi-aryl 2,4-pyrimidinediamines as inhibitors of kinases</p>

    <p>          Cao, Jon Jianguo, Hood, John, Lohse, Dan, Mak, Chi Ching, McPherson, Andrew, Noronha, Glenn, Pathak, Ved, Renick, Joel, Soll, Richard M, and Zeng, Binqi</p>

    <p>          PATENT:  WO <b>2007053452</b>  ISSUE DATE:  20070510</p>

    <p>          APPLICATION: 2006  PP: 336pp., which</p>

    <p>          ASSIGNEE:  (Targegen, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   61274   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of C-fluoro-branched cyclopropyl nucleosides</p>

    <p>          Kim, Aihong and Hong, Joon Hee</p>

    <p>          Eur. J. Med. Chem. <b>2007</b>.  42(4): 487-493</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   61275   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Short synthesis and antiviral evaluation of C-fluoro-branched cyclopropyl nucleosides</p>

    <p>          Oh Chang Hyun and Hong Joon Hee</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(4): 403-11.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   61276   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral resistance of herpes simplex virus and varicella-zoster virus</p>

    <p>          Daikoku Tohru and Shiraki Kimiyasu</p>

    <p>          Nippon Rinsho <b>2007</b>.  65 Suppl 2 Pt. 1: 480-6.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   61277   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Tea catechins as a potential alternative anti-infectious agent</p>

    <p>          Song Jae Min and Seong Baik Lin</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(3): 497-506.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>21.   61278   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Screening for peptides of anti-rotavirus by phage-displayed technique</p>

    <p>          Yao Ning, Yao Lun-Guang, Zhang Xiang-Man, Guo Tai-Lin, and Kan Yun-Chao</p>

    <p>          Sheng Wu Gong Cheng Xue Bao <b>2007</b>.  23(3): 403-8.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   61279   DMID-LS-149; SCIFINDER-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Aniline derivatives as antiviral and anticancer agents, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          Jorgensen, William L, Ruiz-Caro, Juliana, and Hamilton, Andrew D</p>

    <p>          PATENT:  WO <b>2007038387</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 2006  PP: 93pp.</p>

    <p>          ASSIGNEE:  (Yale University, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   61280   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Entecavir (Etv) Is Superior to Lamivudine (Lvd) for the Treatment of Chronic Hepatitis B (Chb): Results of a Phase 3 Chinese Study (Etv-023) in Nucleoside-Naive Patients</p>

    <p>          Yao, G. <i>et al.</i></p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A126</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400175">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400175</a> </p><br />

    <p>24.   61281   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Management of Hepatitis C With Natural and Synthetic Medicines Poster Session-Treatment</p>

    <p>          Usmanghani, K., Iqbal, A., and Hannan, A.</p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A157</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400272">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400272</a> </p><br />

    <p>25.   61282   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          Efficient Asymmetric Synthesis of N-[(1r)-6-Chloro-2,3,4,9-Tetrahydro-1h-Carbazol-1-Yl]-2-Pyridinecarboxam Ide for Treatment of Human Papillomavirus Infections</p>

    <p>          Boggs, S. <i>et al.</i></p>

    <p>          Organic Process Research &amp; Development <b>2007</b>.  11(3): 539-545</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246570400035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246570400035</a> </p><br />

    <p>26.   61283   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          A Sars-Coronovirus 3cl Protease Inhibitor Isolated From the Marine Sponge Axinella Cf. Corrugata: Structure Elucidation and Synthesis</p>

    <p>          De Lira, S. <i>et al.</i></p>

    <p>          Journal of the Brazilian Chemical Society <b>2007</b>.  18(2): 440-443</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246443800029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246443800029</a> </p><br />

    <p>27.   61284   DMID-LS-149; WOS-DMID-7/13/2007</p>

    <p class="memofmt1-2">          The Chemopreventive Effect of Anti-Hepatitis Drug Bicyclol on Hepatocarcinogenesis in Vitro and in Vivo</p>

    <p>          Sun, H. and Liu, G.</p>

    <p>          Journal of Gastroenterology and Hepatology <b>2006</b>.  21: A177</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400343">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247320400343</a> </p><br />
    <br clear="all">

    <p>28.   61285   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Macrocyclic Inhibitors of HCV NS3-4A Protease: Design and Structure Activity Relationship</p>

    <p>          Venkatraman, S and Njoroge, FG</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(13): 1290-301</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627558&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627558&amp;dopt=abstract</a> </p><br />

    <p>29.   61286   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Structural features and antiviral activity of sulphated fucans from the brown seaweed Cystoseira indica</p>

    <p>          Mandal, P, Mateu, CG, Chattopadhyay, K, Pujol, CA, Damonte, EB, and Ray, B</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(3): 153-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626599&amp;dopt=abstract</a> </p><br />

    <p>30.   61287   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Anticytomegalovirus activity of pristimerin, a triterpenoid quinone methide isolated from Maytenus heterophylla (Eckl. &amp; Zeyh.)</p>

    <p>          Murayama, T, Eizuru, Y, Yamada, R, Sadanari, H, Matsubara, K, Rukung, G, Tolo, FM, Mungai, GM, and Kofi-Tsekpo, M</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(3): 133-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626597&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626597&amp;dopt=abstract</a> </p><br />

    <p>31.   61288   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Los Alamos hepatitis C virus sequence and human immunology databases: an expanding resource for antiviral research</p>

    <p>          Hraber, PT, Leach, RW, Reilly, LP, Thurmond, J, Yusim, K, and Kuiken, C</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(3): 113-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626595&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17626595&amp;dopt=abstract</a> </p><br />

    <p>32.   61289   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          H5N1 avian influenza virus: an overview</p>

    <p>          Proenca-Modena, JL, Macedo, IS, and Arruda, E</p>

    <p>          Braz J Infect Dis <b>2007</b>.  11(1): 125-33</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17625741&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   61290   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Activity of compounds from Chinese herbal medicine Rhodiola kirilowii (Regel) Maxim against HCV NS3 serine protease</p>

    <p>          Zuo, G, Li, Z, Chen, L, and Xu, X</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624450&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17624450&amp;dopt=abstract</a> </p><br />

    <p>34.   61291   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Yellow fever virus NS3 protease: peptide-inhibition studies</p>

    <p>          Lohr, K, Knox, JE, Phong, WY, Ma, NL, Yin, Z, Sampath, A, Patel, SJ, Wang, WL, Chan, WL, Rao, KR, Wang, G, Vasudevan, SG, Keller, TH, and Lim, SP</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 8): 2223-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622626&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622626&amp;dopt=abstract</a> </p><br />

    <p>35.   61292   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of 6-(Alkyl-heteroaryl)furo[2,3-d]pyrimidin-2(3H)-one Nucleosides and Analogues with Ethynyl, Ethenyl, and Ethyl Spacers at C6 of the Furopyrimidine Core(1)</p>

    <p>          Robins, MJ, Nowak, I, Rajwanshi, VK, Miranda, K, Cannon, JF, Peterson, MA, Andrei, G, Snoeck, R, Clercq, ED, and Balzarini, J</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622128&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17622128&amp;dopt=abstract</a> </p><br />

    <p>36.   61293   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Small Molecule and Novel Treatments for Chronic Hepatitis C Virus Infection</p>

    <p>          Harrison, SA</p>

    <p>          Am J Gastroenterol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617208&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17617208&amp;dopt=abstract</a> </p><br />

    <p>37.   61294   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Respiratory syncytial virus fusion inhibitors. Part 6: An examination of the effect of structural variation of the benzimidazol-2-one heterocycle moiety</p>

    <p>          Combrink, KD, Gulgeze, HB, Thuring, JW, Yu, KL, Civiello, RL, Zhang, Y, Pearce, BC, Yin, Z, Langley, DR, Kadow, KF, Cianci, CW, Li, Z, Clarke, J, Genovesi, EV, Medina, I, Lamb, L, Yang, Z, Zadjura, L, Krystal, M, and Meanwell, NA</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17616396&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17616396&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>38.   61295   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral and antimicrobial activities of three sesquiterpene lactones from Centaurea solstitialis L. ssp. solstitialis</p>

    <p>          Ozcelik, B, Gurbuz, I, Karaoglu, T, and Yesilada, E</p>

    <p>          Microbiol Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17614269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17614269&amp;dopt=abstract</a> </p><br />

    <p>39.   61296   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          In vitro antiviral activity of antimicrobial peptides against herpes simplex virus 1, adenovirus, and rotavirus</p>

    <p>          Carriel-Gomes, MC, Kratz, JM, Barracco, MA, Bachere, E, Barardi, CR, and Simoes, CM</p>

    <p>          Mem Inst Oswaldo Cruz <b>2007</b>.  102(4): 469-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17612767&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17612767&amp;dopt=abstract</a> </p><br />

    <p>40.   61297   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-influenza activities of carboxyl alkoxyalkyl esters of 4-guanidino-Neu5Ac2en (zanamivir)</p>

    <p>          Liu, ZY, Wang, B, Zhao, LX, Li, YH, Shao, HY, Yi, H, You, XF, and Li, ZR</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17611105&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17611105&amp;dopt=abstract</a> </p><br />

    <p>41.   61298   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          In vitro and in vivo activity of T-705 against arenavirus and bunyavirus infections</p>

    <p>          Gowen, BB, Wong, MH, Jung, KH, Sanders, AB, Mendenhall, M, Bailey, KW, Furuta, Y, and Sidwell, RW</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17606691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17606691&amp;dopt=abstract</a> </p><br />

    <p>42.   61299   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Antiviral Effect of Orally Administered (-)-{beta}-D-2-Aminopurine Dioxolane in Woodchucks with Chronic Woodchuck Hepatitis Virus Infection</p>

    <p>          Menne, S, Asif, G, Narayanasamy, J, Butler, SD, George, AL, Hurwitz, SJ, Schinazi, RF, Chu, CK, Cote, PJ, Gerin, JL, and Tennant, BC</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17606676&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17606676&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>43.   61300   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Substrate Specificity Profiling and Identification of a New Class of Inhibitor for the Major Protease of the SARS Coronavirus(,)</p>

    <p>          Goetz, DH, Choe, Y, Hansell, E, Chen, YT, McDowell, M, Jonsson, CB, Roush, WR, McKerrow, J, and Craik, CS</p>

    <p>          Biochemistry <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17605471&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17605471&amp;dopt=abstract</a> </p><br />

    <p>44.   61301   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Cyclic Depsipeptides, Ichthyopeptins A and B, from Microcystis ichthyoblabe</p>

    <p>          Zainuddin, EN, Mentel, R, Wray, V, Jansen, R, Nimtz, M, Lalk, M, and Mundt, S</p>

    <p>          J Nat Prod <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602586&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17602586&amp;dopt=abstract</a> </p><br />

    <p>45.   61302   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Design and synthesis of bioactive adamantane spiro heterocycles</p>

    <p>          Kolocouris, N, Zoidis, G, Foscolos, GB, Fytas, G, Prathalingham, SR, Kelly, JM, Naesens, L, and De, Clercq E</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.  17(15): 4358-62</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17588747&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17588747&amp;dopt=abstract</a> </p><br />

    <p>46.   61303   DMID-LS-149; PUBMED-DMID-7/16/2007</p>

    <p class="memofmt1-2">          Evaluation of hepatitis B virus replication and proteomic analysis of HepG2.2.15 cell line after cyclosporine A treatment</p>

    <p>          Xie, HY, Xia, WL, Zhang, CC, Wu, LM, Ji, HF, Cheng, Y, and Zheng, SS</p>

    <p>          Acta Pharmacol Sin <b>2007</b>.  28(7): 975-984</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17588333&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17588333&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
