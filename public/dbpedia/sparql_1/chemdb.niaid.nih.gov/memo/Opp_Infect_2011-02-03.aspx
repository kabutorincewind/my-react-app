

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-02-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WqqUpgiaue6Olz21E/taBMyjkdu54MvroXEbnau7nhP1DyoPIjNK6Prh5vLh7wQqYmccO7rfbZdtl7ZUD96vbG1jmie9moBcDgImn400raOIu3Q+hFi0hh02lbA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA19062B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">January 21 - February 3, 2011</p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21098240">The Antibiotic Monensin Causes Cell Cycle Disruption of Toxoplasma gondii Mediated through the DNA Repair Enzyme TgMSH-1.</a> Lavine, M.D. and G. Arrizabalaga. Antimicrobial Agents and Chemotherapy, 2011. 55(2): p. 745-755; PMID[21098240].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p> 

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21282457">Multi-laboratory Testing of Two-Drug Combinations of Antifungals against Candida albicans, Candida glabrata, and Candida parapsilosis.</a> Chaturvedi, V., R. Ramani, D. Andes, D.J. Diekema, M.A. Pfaller, M.A. Ghannoum, C. Knapp, S.R. Lockhart, L. Ostrosky-Zeichner, T.J. Walsh, K. Marchillo, S. Messer, A.R. Welshenbaugh, C. Bastulli, N. Iqbal, V.L. Paetznick, J. Rodriguez, and T. Sein. Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>. PMID[21282457].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21280103">Synthesis of 2-Mercaptobenzimidazole Derivatives as Potential Anti-microbial and Cytotoxic Agents.</a> Hosamani, K.M. and R.V. Shingalapur. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>. PMID[21280103].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21272859">In vitro synergism between berberine and miconazole against planktonic and biofilm Candida cultures.</a> Wei, G.X., X. Xu, and C.D. Wu. Archives of Oral Biology, 2011. <b>[Epub ahead of print]</b>. PMID[21272859]. <b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21272037">Low-level efficacy of cosmetic preservatives.</a> Lundov, M.D., J.D. Johansen, C. Zachariae, and L. Moesby. International Journal of Cosmetic Science, 2011. <b>[Epub ahead of print]</b>. PMID[21272037].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21269424">Antimicrobial activities of the methanol extract, fractions and compounds from Ficus polita Vahl. (Moraceae).</a> Kuete, V., J. Kamga, L.P. Sandjo, B. Ngameni, H.M. Poumale, P. Ambassa, and B.T. Ngadjui. BMC Complementary and Alternative Medicine, 2011. 11(1): p. 6; PMID[21269424].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21265996">Plant production of anti-beta-glucan antibodies for immunotherapy of fungal infections in humans.</a> Capodicasa, C., P. Chiani, C. Bromuro, F. De Bernardis, M. Catellani, A.S. Palma, Y. Liu, T. Feizi, A. Cassone, E. Benvenuto, and A. Torosantucci. Plant Biotechnology Journal, 2011. <b>[Epub ahead of print]</b>. PMID[21265996].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21264486">Two squalene synthase inhibitors, E5700 and ER-119884, interfere with cellular proliferation and induce ultrastructural and lipid profile alterations in a Candida tropicalis strain resistant to fluconazole, itraconazole, and amphotericin B.</a> Ishida, K., G. Visbal, J.C. Rodrigues, J.A. Urbina, W. de Souza, and S. Rozental. Journal of Infection and Chemotherapy, 2011. <b>[Epub ahead of print]</b>. PMID[21264486].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21259352">Antimicrobial Evaluation of the Polyisoprenylated Benzophenones Nemorosone and Guttiferone A.</a> Monzote, L., O. Cuesta-Rubio, A. Matheeussen, T. Van Assche, L. Maes, and P. Cos. Phytotherapy Research, 2011. <b>[Epub ahead of print]</b>. PMID[21259352].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21259045">A Novel Cationic Ribonuclease with Antimicrobial Activity from Rana dybowskii.</a> Tao, F., M. Fan, W. Zhao, Q. Lin, and R. Ma. Biochemical Genetics, 2011. <b>[Epub ahead of print]</b>. PMID[21259045].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21258865">Antifungal Properties of Crude Extracts of Five Egyptian Medicinal Plants Against Dermatophytes and Emerging Fungi.</a> Hashem, M. Mycopathologia, 2011. <b>[Epub ahead of print]</b>. PMID[21258865].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21257199">A photopolymerized antimicrobial hydrogel coating derived from epsilon-poly-l-lysine.</a> Zhou, C., P. Li, X. Qi, A.R. Sharif, Y.F. Poon, Y. Cao, M.W. Chang, S.S. Leong, and M.B. Chan-Park. Biomaterials, 2011. <b>[Epub ahead of print]</b>. PMID[21257199].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21254831">Interactions between various microbes and ginseng botanicals.</a> Tournas, V.H., J.S. Kohn, and E.J. Katsoudas. Critical Reviews in Microbiology, 2011. <b>[Epub ahead of print]</b>. PMID[21254831].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21255433">Synthetic arylquinuclidine derivatives exhibit antifungal activity against Candida albicans, Candida tropicalis and Candida parapsilopsis.</a> Ishida, K., J. Rodrigues, S. Cammerer, J. Urbina, I. Gilbert, W. de Souza, and S. Rozental. Annals of Clinical Microbiology and Antimicrobials, 2011. 10(1): p. 3; PMID[21255433].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21252684">Effects of Ionic and Surfactant Agents on the Antimicrobial Activity of Polyhexamethylene Biguanide.</a> Yanai, R., K. Ueda, T. Nishida, M. Toyohara, and O. Mori. Eye &amp; Contact Lens, 2011. <b>[Epub ahead of print]</b>. PMID[21252684].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21251840">Synthesis of egonol derivatives and their antimicrobial activities.</a> Emirdag-Ozturk, S., T. Karayildirim, and H. Anil. Bioorganic and Medicinal Chemistry, 2011. 19(3): p. 1179-1188; PMID[21251840].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21251230">2-amino-nonyl-6-methoxyl-tetralin muriate (10b) against Candida albicans augments endogenous reactive oxygen species: a microarray analysis study.</a> Liang, R., X. Yong, Y. Jiang, Y. Tan, B. Dai, S. Wang, T. Hu, X. Chen, N. Li, Z. Dong, and Y. Cao. The FEBS Journal, 2011. <b>[Epub ahead of print]</b>. PMID[21251230].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21248850">Reduction of disulphide bonds unmasks potent antimicrobial activity of human beta-defensin 1.</a> Schroeder, B.O., Z. Wu, S. Nuding, S. Groscurth, M. Marcinowski, J. Beisner, J. Buchner, M. Schaller, E.F. Stange, and J. Wehkamp. Nature, 2011. 469(7330): p. 419-423; PMID[21248850].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21216051">Synthesis and structure-activity relationship of 2-thiopyrimidine-4-one analogs as antimicrobial and anticancer agents.</a> Prachayasittikul, S., A. Worachartcheewan, C. Nantasenamat, M. Chinworrungsee, N. Sornsongkhram, S. Ruchirawat, and V. Prachayasittikul. European Journal of Medicinal Chemistry, 2011. 46(2): p. 738-742; PMID[21216051].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21215618">Synthesis and antifungal activity of furo[2,3-f]quinolin-5-ols.</a> Ryu, C.K., Y.H. Kim, J.H. Nho, J.A. Hong, J.H. Yoon, and A. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(3): p. 952-955; PMID[21215618].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21215620">Synthesis and evaluation of a class of new coumarin triazole derivatives as potential antimicrobial agents.</a> Shi, Y. and C.H. Zhou. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(3): p. 956-960; PMID[21215620].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21163555">Synthesis, antimicrobial activity and structure-activity relationship study of N,N-dibenzyl-cyclohexane-1,2-diamine derivatives.</a> Sharma, M., P. Joshi, N. Kumar, S. Joshi, R.K. Rohilla, N. Roy, and D.S. Rawat. European Journal of Medicinal Chemistry, 2011. 46(2): p. 480-487; PMID[21163555].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21194811">Synthesis, antidepressant and antifungal evaluation of novel 2-chloro-8-methylquinoline amine derivatives.</a> Kumar, S., S. Bawa, S. Drabu, H. Gupta, L. Machwal, and R. Kumar. European Journal of Medicinal Chemistry, 2011. 46(2): p. 670-675; PMID[21194811].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21093524">Diverse role of microbially bioconverted product of cabbage (Brassicaoleracea) by Pseudomonassyringe pv. T1 on inhibiting Candida species.</a> Bajpai, V.K., S.C. Kang, E. Park, W.T. Jeon, and K.H. Baek. Food and Chemical Toxicology, 2011. 49(2): p. 403-407; PMID[21093524].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20945514">Chemical composition and antioxidant and antimicrobial activities of essential oil of Allium sphaerocephalon L. subsp. sphaerocephalon (Liliaceae) inflorescences.</a> Lazarevic, J.S., A.S. Ethordevic, B.K. Zlatkovic, N.S. Radulovic, and R.M. Palic. Journal of the Science of Food and Agriculture, 2011. 91(2): p. 322-329; PMID[20945514].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20703942">Enzymatic activity, sensitivity to antifungal drugs and Baccharis dracunculifolia essential oil by Candida strains isolated from the oral cavities of breastfeeding infants and in their mothers&#39; mouths and nipples.</a> Pereira, C.A., A.C. da Costa, A.K. Machado, M. Beltrame Junior, M.S. Zollner, J.C. Junqueira, and A.O. Jorge. Mycopathologia, 2011. 171(2): p. 103-109; PMID[20703942].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20572887">Lavender, tea tree and lemon oils as antimicrobials in washing liquids and soft body balms.</a> Kunicka-Styczynska, A., M. Sikora, and D. Kalemba. International Journal of Cosmetic Science, 2011. 33(1): p. 53-61; PMID[20572887].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21257971">A Novel Human IgA Monoclonal Antibody Protects against Tuberculosis.</a> Balu, S., R. Reljic, M.J. Lewis, R.J. Pleass, R. McIntosh, C. van Kooten, M. van Egmond, S. Challacombe, J.M. Woof, and J. Ivanyi. Journal of Immunology, 2011. <b>[Epub ahead of print]</b>. PMID[21257971].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21232949">Synthesis and evaluation of anti-tubercular activity of new dithiocarbamate sugar derivatives.</a> Horita, Y., T. Takii, R. Kuroishi, T. Chiba, K. Ogawa, L. Kremer, Y. Sato, Y. Lee, T. Hasegawa, and K. Onozaki. Bioorganic and Medicinal Chemistry Letters, 2011. 21(3): p. 899-903; PMID[21232949].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21051548">The plant alkaloid piperine as a potential inhibitor of ethidium bromide efflux in Mycobacterium smegmatis.</a> Jin, J., J. Zhang, N. Guo, H. Feng, L. Li, J. Liang, K. Sun, X. Wu, X. Wang, M. Liu, X. Deng, and L. Yu. Journal of Medical Microbiology, 2011. 60(Pt 2): p. 223-239; PMID[21051548].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21275374">Design, Synthesis, and Study of a Mycobactin-Artemisinin Conjugate That Has Selective and Potent Activity against Tuberculosis and Malaria.</a> Miller, M.J., A.J. Walz, H. Zhu, C. Wu, G. Moraski, U. Mollmann, E.M. Tristani, A.L. Crumbliss, M.T. Ferdig, L. Checkley, R.L. Edwards, and H.I. Boshoff. Journal of the American Chemical Society, 2011. <b>[Epub ahead of print]</b>. PMID[21275374].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21259445">Pyrido[1,2-a]benzimidazole-Based Agents Active Against Tuberculosis (TB), Multidrug-Resistant (MDR) TB and Extensively Drug-Resistant (XDR) TB.</a> Pieroni, M., S.K. Tipparaju, S. Lun, Y. Song, A.W. Sturm, W.R. Bishai, and A.P. Kozikowski. ChemMedChem, 2011. 6(2): p. 334-342; PMID[21259445].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0119-020211.</p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="plaintext">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285553200017">Synthesis and Antimicrobial Screening of Pyrazolo-3-Aryl Quinazolin-4(3H)ones.</a> Deshmukh, M.B., S. Patil, S.S. Patil, and S.D. Jadhav. Indian Journal of Pharmaceutical Sciences, 2010. 72(4): p. 500-U104; ISI[000285553200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285485000066">Synthesis and antitubercular activity of new mefloquine-oxazolidine derivatives.</a> Goncalves, R.S.B., C.R. Kaiser, M.C.S. Lourenco, M.V.N. de Souza, J.L. Wardell, S. Wardell, and A.D. da Silva. European Journal of Medicinal Chemistry, 2010. 45(12): p. 6095-6100; ISI[000285485000066].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285553200020">Antibacterial and Antifungal Potential of some Arid Zone Plants.</a> Jain, S.C., B. Pancholi, R. Singh, and R. Jain. Indian Journal of Pharmaceutical Sciences, 2010. 72(4): p. 510-U115; ISI[000285553200020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0119-020211.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285553200009">Efficient Electrochemical Synthesis, Antimicrobial and Antiinflammatory Activity of 2 amino-5-substituted-1,3,4-oxadiazole Derivatives.</a> Kumar, S. and D.P. Srivastava. Indian Journal of Pharmaceutical Sciences, 2010. 72(4): p. 458-464; ISI[000285553200009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0119-020211.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
