

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-11-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6AGtNQuRIkwlK2A5bHVz1lOmoXcWtvjfaU3FOPeZrkOuczWEqg142v3SQt1AEEAeQ4BNSjusR/cgtSz9T7DQO2nf1PdguOc7Aw4oHRExtLaei1NW8Cn34mKjd+Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7750A2CB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: November 9 - November 20, 2012</h1>

    <h2>Epstein-Barr</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23067008">Ascorbic acid Kills Epstein-Barr Virus Positive Burkitt Lymphoma Cells and Epstein-Barr Virus Transformed B-Cells in Vitro, but Not in Vivo.</a><i>.</i> Shatzer, A.N., M.G. Espey, M. Chavez, H. Tu, M. Levine, and J.I. Cohen. Leukemia &amp; Lymphoma, 2012.  <b>[Epub ahead of print]</b>; PMID[23067008]. <b>[PubMed]</b> OV_1109-112012.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23158906">Anti-HBV Efficacy of Combined SiRNAs Targeting Viral Gene and Heat Shock Cognate 70</a><i>.</i> Bian, Z., A. Xiao, M. Cao, M. Liu, S. Liu, Y. Jiao, W. Yan, Z. Qi, and Z. Zheng. Virology Journal, 2012. 9(1): p. 275; PMID[23158906].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23140388">A New Lignan with anti-HBV Activity from the Roots of Bombax ceiba<i>.</i></a>Wang, G.K., B.B. Lin, R. Rao, K. Zhu, X.Y. Qin, G.Y. Xie, and M.J. Qin. Natural Product Research, 2012. <b>[Epub ahead of print]</b>; PMID[23140388].</p>

    <p class="plaintext"><b>[PubMed]</b> ov_1109-112012.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22981689">Preparation and Evaluation of Liposome-encapsulated Codrug LMX</a><i>.</i> Zhong, Y., J. Wang, Y. Wang, and B. Wu. International Journal of Pharmaceutics, 2012. 438(1-2): p. 240-248; PMID[22981689].</p>

    <p class="plaintext"><b>[PubMed]</b> ov_1109-112012.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23149997">Treatment with Silymarin for Hepatitis C Virus</a><i>.</i> Ferenci, P. The Journal of the American Medical Association, 2012. 308(18): p. 1856-1857; PMID[23149997].</p>

    <p class="plaintext"><b>[PubMed]</b> ov_1109-112012.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23139125">Bioactive Anthraquinones from Endophytic Fungus Aspergillus versicolor Isolated from Red Sea Algae</a><i>.</i> Hawas, U.W., A.A. El-Beih, and A.M. El-Halawany. Archives of Pharmacal Research, 2012. 35(10): p. 1749-1756; PMID[23139125].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=22965230">High Affinity Peptide Inhibitors of the Hepatitis C Virus NS3-4A Protease Refractory to Common Resistant Mutants</a><i>.</i> Kugler, J., S. Schmelz, J. Gentzsch, S. Haid, E. Pollmann, J. Van Den Heuvel, R. Franke, T. Pietschmann, D.W. Heinz, and J. Collins. The journal of Biological Chemistry, 2012. 287(46): P. 39224-39232; PMID[22965230].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <p> </p>     

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23036957">Synthesis and SAR of Geminal Substitutions at the C5&#39; Carbosugar Position of Pyrimidine-derived HCV Inhibitors</a><i>.</i> Verma, V.A., R. Rossman, F. Bennett, L. Chen, S. Gavalas, V. Girijavallabhan, Y. Huang, S.H. Kim, A. Kosinski, P. Pinto, R. Rizvi, B. Shankar, L. Tong, F. Velazquez, S. Venkatraman, J. Kozlowski, M. Maccoss, C.D. Kwong, N. Bansal, H.S. Kezar, 3rd, R.C. Reynolds, J.A. Maddry, S. Ananthan, J.A. Secrist, 3rd, C. Li, R. Chase, S. Curry, H.C. Huang, X. Tong, F.G. Njoroge, and A. Arasappan. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): P. 6967-6973; PMID[23036957].</p>
    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23131196">Enantiomers of an Indole Alkaloid Containing Unusual Dihydrothiopyran and 1,2,4-Thiadiazole Rings from the Root of Isatis indigotica</a><i>.</i> Chen, M., S. Lin, L. Li, C. Zhu, X. Wang, Y. Wang, B. Jiang, S. Wang, Y. Li, J. Jiang, and J. Shi. Organic Letters, 2012. 14(22): p. 5668-5671; PMID[23131196].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <h2>HSV-2</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23161023">In Vitro Antiviral Activity of Dermaseptin S(4) and Derivatives from Amphibian Skin against Herpes Simplex Virus Type 2</a><i>.</i> Bergaoui, I., A. Zairi, F. Tangy, M. Aouni, B. Selmi, and K. Hani. Journal of Medical Virology, 2012.  <b>[Epub ahead of print]</b>; PMID[23161023].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1109-112012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309513000003">Carbocyclic Nucleoside Analogues: Classification, Target Enzymes, Mechanisms of Action and Synthesis</a><i>.</i> Matyugina, E.S., A.L. Khandazhinskaya, and S.N. Kochetkov. Russian Chemical Reviews, 2012. 81(8): P. 729-746; ISI[000309513000003].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309464300044">Identification and Analysis of Hepatitis C Virus NS3 Helicase Inhibitors Using Nucleic Acid Binding Assays</a><i>.</i> Mukherjee, S., A.M. Hanson, W.R. Shadrick, J. Ndjomou, N.L. Sweeney, J.J. Hernandez, D. Bartczak, K.L. Li, K.J. Frankowski, J.A. Heck, L.A. Arnold, F.J. Schoenen, and D.N. Frick. Nucleic Acids Research, 2012. 40(17): p. 8607-8621; ISI[000309464300044].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309524600016">Synthesis, in Vitro and in Silico NS5B Polymerase Inhibitory Activity of Benzimidazole Derivatives</a><i>.</i> Patil, V.M., K.R. Gurukumar, M. Chudayeu, S.P. Gupta, S. Samanta, N. Masand, and N. Kaushik-Basu. Medicinal Chemistry, 2012. 8(4): P. 629-635; ISI[000309524600016].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1109-112012.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309657100018">Evaluation of Phosphatidylinositol-4-kinase III&#945; as a Hepatitis C Virus Drug Target.</a> Vaillancourt, F.H., M. Brault, L. Pilote, N. Uyttersprot, E.T. Gaillard, J.H. Stoltz, B.L. Knight, L. Pantages, M. McFarland, S. Breitfelder, T.T. Chiu, L. Mahrouche, A.M. Faucher, M. Cartier, M.G. Cordingley, R.C. Bethell, H.P. Jiang, P.W. White, and G. Kukolj. 232, 2012. 86(21): P. 11595-11607; ISI[000309657100018].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1109-112012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
