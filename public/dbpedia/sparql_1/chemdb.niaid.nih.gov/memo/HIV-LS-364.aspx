

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-364.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eliP1QBHsbWmfP/EfRaA1Nq7Qh3R5ZziPkj4LYoWmIaKmshxHNUbN5VKnDm1HS+ZWQwUGF40AUIYp11/BYG1gHhl/+hPRx39UuaiWm6YqXV4QqhKWyv0cjXIIIM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A2ECB8F3" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-364-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59743   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis of Novel Diarylpyrimidine Analogues of TMC278 and their Antiviral Activity against HIV-1 Wild-Type and Mutant Strains</p>

    <p>          Mordant, Celine, Schmitt, Benoit, Pasquier, Elisabeth, Demestre, Christophe, Queguiner, Laurence, Masungi, Chantal, Peeters, Anik, Smeulders, Liesbeth, Bettens, Eva, and Hertogs, Kurt</p>

    <p>          European Journal of Medicinal Chemistry <b>2006</b>.  In Press, Accepted Manuscript</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4MK72GX-1/2/2e9423a2983dca4490c83bdacf7795c5">http://www.sciencedirect.com/science/article/B6VKY-4MK72GX-1/2/2e9423a2983dca4490c83bdacf7795c5</a> </p><br />

    <p>2.     59744   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Simple criterion for selection of flavonoid compounds with anti-HIV activity</p>

    <p>          Veljkovic, Veljko, Mouscadet, Jean-Francois, Veljkovic, Nevena, Glisic, Sanja, and Debyser, Zeger</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4MJJH07-5/2/63859279a2ce2c95e3675c138a6f37b2">http://www.sciencedirect.com/science/article/B6TF9-4MJJH07-5/2/63859279a2ce2c95e3675c138a6f37b2</a> </p><br />

    <p>3.     59745   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Stoichiometry of the antiviral protein APOBEC3G in HIV-1 virions</p>

    <p>          Xu, Hongzhan, Chertova, Elena, Chen, Jianbo, Ott, David E, Roser, James D, Hu, Wei-Shau, and Pathak, Vinay K</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4MFJT6J-1/2/d1ba72cadbc177302e0c2c6ef46b19c7">http://www.sciencedirect.com/science/article/B6WXR-4MFJT6J-1/2/d1ba72cadbc177302e0c2c6ef46b19c7</a> </p><br />

    <p>4.     59746   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Identification of the benzodiazepines as a new class of antileishmanial agent</p>

    <p>          Clark, Rachel L, Carter, Katharine C, Mullen, Alexander B, Coxon, Geoffrey D, Owusu-Dapaah, George, McFarlane, Emma, Duong Thi, MDao, Grant, MHelen, Tettey, Justice NA, and Mackay, Simon P</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4M91NW9-B/2/faee33386db8a0b5c3c14e33b1601941">http://www.sciencedirect.com/science/article/B6TF9-4M91NW9-B/2/faee33386db8a0b5c3c14e33b1601941</a> </p><br />

    <p>5.     59747   HIV-LS-364; WOS-HIV-12/18/2006</p>

    <p class="memofmt1-2">          FEP-guided selection of bicyclic heterocycles in lead optimization for non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Kim, JT, Hamilton, AD, Bailey, CM, Domoal, RA, Wang, LG, Anderson, KS, and Jorgensen, WL</p>

    <p>          JOURNAL OF THE AMERICAN CHEMICAL SOCIETY <b>2006</b>.  128(48): 15372-15373, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242367000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242367000010</a> </p><br />
    <br clear="all">

    <p>6.     59748   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Nucleosides with self-complementary hydrogen-bonding motifs: Synthesis and base-pairing studies of two nucleosides containing the imidazo[4,5-d]pyridazine ring system: Tetrahedron Prize for Creativity in Organic Chemistry 2005: B. Giese</p>

    <p>          Ujjinamatada, Ravi K, Paulman, Robin L, Ptak, Roger G, and Hosmane, Ramachandra S</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(18): 6359-6367</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4K4WMP2-3/2/6e6f24ff22167002273630b733a9a1de">http://www.sciencedirect.com/science/article/B6TF8-4K4WMP2-3/2/6e6f24ff22167002273630b733a9a1de</a> </p><br />

    <p>7.     59749   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Cyclophilin A: An auxiliary but not necessary cofactor for TRIM5[alpha] restriction of HIV-1</p>

    <p>          Stremlau, Matthew, Song, Byeongwoon, Javanbakht, Hassan, Perron, Michel, and Sodroski, Joseph</p>

    <p>          Virology <b>2006</b>.  351(1): 112-120</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JTR8WP-2/2/e3f8c543f759633c4ec4b51db0b7abdf">http://www.sciencedirect.com/science/article/B6WXR-4JTR8WP-2/2/e3f8c543f759633c4ec4b51db0b7abdf</a> </p><br />

    <p>8.     59750   HIV-LS-364; WOS-HIV-12/18/2006</p>

    <p class="memofmt1-2">          Antiretroviral drug-resistant HIV-2 infection - a new therapeutic dilemma</p>

    <p>          Maniar, JK, Damond, F, Kamath, RR, Mandalia, S, and Surjushe, A</p>

    <p>          INTERNATIONAL JOURNAL OF STD &amp; AIDS <b>2006</b>.  17(11): 781-782, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242084300017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242084300017</a> </p><br />

    <p>9.     59751   HIV-LS-364; WOS-HIV-12/18/2006</p>

    <p class="memofmt1-2">          A historical sketch of the discovery and development of HIV-1 integrase inhibitors</p>

    <p>          Savarino, A</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2006</b>.  15(12): 1507-1522, 16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242273700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242273700004</a> </p><br />

    <p>10.   59752   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Interaction of small molecule inhibitors of HIV-1 entry with CCR5</p>

    <p>          Seibert, Christoph, Ying, Weiwen, Gavrilov, Svetlana, Tsamis, Fotini, Kuhmann, Shawn E, Palani, Anandan, Tagat, Jayaram R, Clader, John W, McCombie, Stuart W, and Baroudy, Bahige M</p>

    <p>          Virology <b>2006</b>.  349(1): 41-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4J9X2PK-1/2/a849fcb363bf5f291ce753669b6c2bda">http://www.sciencedirect.com/science/article/B6WXR-4J9X2PK-1/2/a849fcb363bf5f291ce753669b6c2bda</a> </p><br />

    <p>11.   59753   HIV-LS-364; WOS-HIV-12/18/2006</p>

    <p class="memofmt1-2">          P-glycoprotein effects of cyclic urea HIV protease inhibitor DMP 323 in competitional absorption studies</p>

    <p>          Richter, M, Gyemant, N, Molnar, J, and Hilgeroth, A</p>

    <p>          ARCHIV DER PHARMAZIE <b>2006</b>.  339(11): 625-628, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242385800006</a> </p><br />
    <br clear="all">

    <p>12.   59754   HIV-LS-364; EMBASE-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Co-evolution of nelfinavir-resistant HIV-1 protease and the p1-p6 substrate</p>

    <p>          Kolli, Madhavi, Lastere, Stephane, and Schiffer, Celia A</p>

    <p>          Virology <b>2006</b>.  347(2): 405-409</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4J32J9G-1/2/befda56d844df72db97f29491dbc16f4">http://www.sciencedirect.com/science/article/B6WXR-4J32J9G-1/2/befda56d844df72db97f29491dbc16f4</a> </p><br />

    <p>13.   59755   HIV-LS-364; WOS-HIV-12/24/2006</p>

    <p class="memofmt1-2">          Immunoglobulin A antibodies against internal HIV-1 proteins neutralize HIV-1 replication inside epithelial cells</p>

    <p>          Wright, A, Yan, HM, Lamm, ME, and Huang, YT</p>

    <p>          VIROLOGY <b>2006</b>.  356(1-2): 165-170, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800018</a> </p><br />

    <p>14.   59756   HIV-LS-364; WOS-HIV-12/24/2006</p>

    <p class="memofmt1-2">          Determinants of activity of the HIV-1 maturation inhibitor PA-457</p>

    <p>          Li, F, Zoumplis, D, Matallana, C, Kilgore, NR, Reddick, M, Yunus, AS, Adamson, CS, Salzwedel, K, Martin, DE, Allaway, GP, Freed, EO, and Wild, CT</p>

    <p>          VIROLOGY <b>2006</b>.  356(1-2): 217-224, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242424800024</a> </p><br />

    <p>15.   59757   HIV-LS-364; WOS-HIV-12/24/2006</p>

    <p class="memofmt1-2">          Carbanucleosides: synthesis of both enantiomers of 2-(6-chloro-purin-9-yl)-3,5-bishydroxymethyl cyclopentanol from D-glucose</p>

    <p>          Roy, BG, Maity, JK, Drew, MGB, Achari, B, and Mandal, SB</p>

    <p>          TETRAHEDRON LETTERS <b>2006</b>.  47(50): 8821-8825, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242483300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242483300004</a> </p><br />

    <p>16.   59758   HIV-LS-364; WOS-HIV-12/24/2006</p>

    <p class="memofmt1-2">          RNA as a target for host defense and anti-HIV drugs</p>

    <p>          Fanning, GC</p>

    <p>          CURRENT DRUG TARGETS <b>2006</b>.  7(12): 1607-1613, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242419400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242419400006</a> </p><br />

    <p>17.   59759   HIV-LS-364; WOS-HIV-12/24/2006</p>

    <p class="memofmt1-2">          Towards new methodologies for the synthesis of biologically interesting 6-substituted pyrimidines and 4(3H)-pyrimidinones</p>

    <p>          Petricci, E, Mugnaini, C, Radi, M, Togninelli, A, Bernardini, C, Manetti, F, Parlato, MC, Renzulli, ML, Alongi, M, Falciani, C, Corelli, F, and Botta, M</p>

    <p>          ARKIVOC <b>2006</b>.: 452-478, 27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000242573900032</a> </p><br />
    <br clear="all">

    <p>18.   59760   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          A novel diketo phosphonic acid that exhibits specific, strand-transfer inhibition of HIV integrase and anti-HIV activity</p>

    <p>          Chi, G, Nair, V, Semenova, E, and Pommier, Y</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188872&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17188872&amp;dopt=abstract</a> </p><br />

    <p>19.   59761   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and Anti-Human Immunodeficiency Virus Activity of 4&#39;-Branched (+/-)-4&#39;-Thiostavudines</p>

    <p>          Kumamoto, H, Nakai, T, Haraguchi, K, Nakamura, KT, Tanaka, H, Baba, M, and Cheng, YC</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7861-7867</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181169&amp;dopt=abstract</a> </p><br />

    <p>20.   59762   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis, in Vitro Antiviral Evaluation, and Stability Studies of Novel alpha-Borano-Nucleotide Analogues of 9-[2-(Phosphonomethoxy)ethyl]adenine and (R)-9-[2-(Phosphonomethoxy)propyl]adenine</p>

    <p>          Barral, K, Priet, S, Sire, J, Neyts, J, Balzarini, J, Canard, B, and Alvarez, K</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7799-7806</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181162&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181162&amp;dopt=abstract</a> </p><br />

    <p>21.   59763   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Rapid discovery and structure-activity profiling of novel inhibitors of human immunodeficiency virus type 1 protease enabled by the copper(i)-catalyzed synthesis of 1,2,3-triazoles and their further functionalization</p>

    <p>          Whiting, M, Tripp, JC, Lin, YC, Lindstrom, W, Olson, AJ, Elder, JH, Sharpless, KB, and Fokin, VV</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7697-710</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181152&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181152&amp;dopt=abstract</a> </p><br />

    <p>22.   59764   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Insights on Resistance to Reverse Transcriptase: The Different Patterns of Interaction of the Nucleoside Reverse Transcriptase Inhibitors in the Deoxyribonucleotide Triphosphate Binding Site Relative to the Normal Substrate</p>

    <p>          Carvalho, AT, Fernandes, PA, and Ramos, MJ</p>

    <p>          J Med Chem <b>2006</b>.  49(26): 7675-7682</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181150&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17181150&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   59765   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Mutations in the connection domain of HIV-1 reverse transcriptase increase 3&#39;-azido-3&#39;-deoxythymidine resistance</p>

    <p>          Nikolenko, GN, Delviks-Frankenberry, KA, Palmer, S, Maldarelli, F, Fivash, MJ Jr, Coffin, JM, and Pathak, VK</p>

    <p>          Proc Natl Acad Sci U S A <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17179211&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17179211&amp;dopt=abstract</a> </p><br />

    <p>24.   59766   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and evaluation of 2-aryl-3-heteroaryl-1,3-thiazolidin-4-ones as anti-HIV agents</p>

    <p>          Rawal, RK, Tripathi, R, Katti, SB, Pannecouque, C, and De Clercq, E</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178227&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17178227&amp;dopt=abstract</a> </p><br />

    <p>25.   59767   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Isolation and characterization of an antifungal peptide with antiproliferative activity from seeds of Phaseolus vulgaris cv. &#39;Spotted Bean&#39;</p>

    <p>          Wang, HX and Ng, TB</p>

    <p>          Appl Microbiol Biotechnol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17177050&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17177050&amp;dopt=abstract</a> </p><br />

    <p>26.   59768   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          A comparison of three highly active antiretroviral treatment strategies consisting of non-nucleoside reverse transcriptase inhibitors, protease inhibitors, or both in the presence of nucleoside reverse transcriptase inhibitors as initial therapy (CPCRA 058 FIRST Study): a long-term randomised trial</p>

    <p>          MacArthur, RD, Novak, RM, Peng, G, Chen, L, Xiang, Y, Hullsiek, KH, Kozal, MJ, van, den Berg-Wolf M, Henely, C, Schmetter, B, and Dehlinger, M</p>

    <p>          Lancet <b>2006</b>.  368(9553): 2125-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17174704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17174704&amp;dopt=abstract</a> </p><br />

    <p>27.   59769   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          HIV-1 reverse transcriptase plus-strand initiation exhibits preferential sensitivity to nonnucleoside reverse transcriptase inhibitors in vitro</p>

    <p>          Grobler, JA, Dornadula, G, Rice, MR, Simcoe, AL, Hazuda, DJ, and Miller, MD</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17172472&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17172472&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   59770   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Structure-function analysis of the ribosomal frameshifting signal of two human immunodeficiency virus type 1 isolates with increased resistance to viral protease inhibitors</p>

    <p>          Girnary, R, King, L, Robinson, L, Elston, R, and Brierley, I</p>

    <p>          J Gen Virol <b>2007</b>.  88(Pt 1): 226-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170455&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17170455&amp;dopt=abstract</a> </p><br />

    <p>29.   59771   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Protein methylation is required to maintain optimal HIV-1 infectivity</p>

    <p>          Willemsen, NM, Hitchen, EM, Bodetti, TJ, Apolloni, A, Warrilow, D, Piller, SC, and Harrich, D</p>

    <p>          Retrovirology <b>2006</b>.  3(1): 92</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169163&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17169163&amp;dopt=abstract</a> </p><br />

    <p>30.   59772   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Antiviral properties of combination peptides of HIV-1 Rev NLS and NES</p>

    <p>          Kobayashi, N, Sato, T, and Yoshida, T</p>

    <p>          Protein Pept Lett <b>2006</b>.  13(10): 1025-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168825&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168825&amp;dopt=abstract</a> </p><br />

    <p>31.   59773   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Attacking HIV Provirus: Therapeutic Strategies to Disrupt Persistent Infection</p>

    <p>          Margolis, DM and Archin, NM</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(4): 369-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168802&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17168802&amp;dopt=abstract</a> </p><br />

    <p>32.   59774   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          The therapeutic potential of the chemokine receptor CXCR4 antagonists as multi-functional agents</p>

    <p>          Tsutsumi, H, Tanaka, T, Ohashi, N, Masuno, H, Tamamura, H, Hiramatsu, K, Araki, T, Ueda, S, Oishi, S, and Fujii, N</p>

    <p>          Biopolymers <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17167792&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17167792&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>33.   59775   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Novel CCR5 monoclonal antibodies with potent and broad-spectrum anti-HIV activities</p>

    <p>          Ji, C, Brandt, M, Dioszegi, M, Jekle, A, Schwoerer, S, Challand, S, Zhang, J, Chen, Y, Zautke, L, Achhammer, G, Baehner, M, Kroetz, S, Heilek-Snyder, G, Schumacher, R, Cammack, N, and Sankuratri, S</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166600&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166600&amp;dopt=abstract</a> </p><br />

    <p>34.   59776   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          HIV-1 clones resistant to a small molecule CCR5 inhibitor use the inhibitor-bound form of CCR5 for entry</p>

    <p>          Pugach, P, Marozsan, AJ, Ketas, TJ, Landes, EL, Moore, JP, and Kuhmann, SE</p>

    <p>          Virology <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166540&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17166540&amp;dopt=abstract</a> </p><br />

    <p>35.   59777   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of 1,3,3,4-Tetrasubstituted Pyrrolidine CCR5 Receptor Antagonists. Discovery of a Potent and Orally Bioavailable Anti-HIV Agent</p>

    <p>          Ma, D, Yu, S, Li, B, Chen, L, Chen, R, Yu, K, Zhang, L, Chen, Z, Zhong, D, Gong, Z, Wang, R, Jiang, H, and Pei, G</p>

    <p>          ChemMedChem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17163560&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17163560&amp;dopt=abstract</a> </p><br />

    <p>36.   59778   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          AZT 5&#39;-Cholinephosphate as an Anti-HIV Agent: The Study of Biochemical Properties and Metabolic Transformations Using Its 32P-Labelled Counterpart</p>

    <p>          Yanvarev, DV, Shirokova, EA, Astapova, MV, and Skoblov, YS</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2007</b>.  26(1): 23-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17162584&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17162584&amp;dopt=abstract</a> </p><br />

    <p>37.   59779   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel 5(H)-phenanthridin-6-ones, 5(H)-phenanthridin-6-one diketo acid, and polycyclic aromatic diketo acid analogs as new HIV-1 integrase inhibitors</p>

    <p>          Patil, S, Kamath, S, Sanchez, T, Neamati, N, Schinazi, RF, and Buolamwini, JK</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17158051&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17158051&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>38.   59780   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Epigallocatechin gallate, the main polyphenol in green tea, binds to the T-cell receptor, CD4: Potential for HIV-1 therapy</p>

    <p>          Williamson, MP, McCormick, TG, Nance, CL, and Shearer, WT</p>

    <p>          J Allergy Clin Immunol <b>2006</b>.  118(6): 1369-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157668&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157668&amp;dopt=abstract</a> </p><br />

    <p>39.   59781   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Structure-activity relationship in the 3-iodo-4-phenoxypyridinone (IOPY) series: The nature of the C-3 substituent on anti-HIV activity</p>

    <p>          Benjahad, A, Oumouch, S, Guillemont, J, Pasquier, E, Mabire, D, Andries, K, Nguyen, CH, and Grierson, DS</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157017&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17157017&amp;dopt=abstract</a> </p><br />

    <p>40.   59782   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Novel inhibitors of the early steps of the HIV-1 life cycle</p>

    <p>          Citterio, P and Rusconi, S</p>

    <p>          Expert Opin Investig Drugs <b>2007</b>.  16(1): 11-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17155850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17155850&amp;dopt=abstract</a> </p><br />

    <p>41.   59783   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Kinetic studies of HIV-1 and HIV-2 envelope glycoprotein-mediated fusion</p>

    <p>          Gallo, SA, Reeves, JD, Garg, H, Foley, B, Doms, RW, and Blumenthal, R</p>

    <p>          Retrovirology <b>2006</b>.  3: 90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17144914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17144914&amp;dopt=abstract</a> </p><br />

    <p>42.   59784   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Biochemical differentiation of APOBEC3F and APOBEC3G proteins associated with HIV-1 life cycle</p>

    <p>          Wang, X, Dolan, PT, Dang, Y, and Zheng, YH</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17142455&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17142455&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>43.   59785   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Analysis of Amino Acids in the beta7-beta8 Loop of Human Immunodeficiency Virus Type 1 Reverse Transcriptase for their Role in Virus Replication</p>

    <p>          Mulky, A, Vu, BC, Conway, JA, Hughes, SH, and Kappes, JC</p>

    <p>          J Mol Biol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17141805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17141805&amp;dopt=abstract</a> </p><br />

    <p>44.   59786   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p><b>          Inhibition of HIV-1 replication by an improved hairpin ribozyme that includes an RNA decoy</b> </p>

    <p>          Barroso-DelJesus, A, Puerta-Fernandez, E, Tapia, N, Romero-Lopez, C, Sanchez-Luque, FJ, Martinez, MA, and Berzal-Herranz, A</p>

    <p>          RNA Biol <b>2005</b>.  2(2): 75-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132944&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132944&amp;dopt=abstract</a> </p><br />

    <p>45.   59787   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          {alpha}-defensins block the early steps of HIV-1 infection: interference with the binding of gp120 to CD4</p>

    <p>          Furci, L, Sironi, F, Tolazzi, M, Vassena, L, and Lusso, P</p>

    <p>          Blood <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132727&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17132727&amp;dopt=abstract</a> </p><br />

    <p>46.   59788   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Multi-drug resistant HIV-1 reverse transcriptase genotype in children treated with dual nucleoside reverse transcriptase inhibitors (NRTIs)</p>

    <p>          Engchanil, C, Kosalaraksa, P, Lulitanond, V, Lumbiganon, P, and Chantratita, W</p>

    <p>          J Med Assoc Thai <b>2006</b>.  89(10): 1713-20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17128848&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17128848&amp;dopt=abstract</a> </p><br />

    <p>47.   59789   HIV-LS-364; PUBMED-HIV-12/26/2006</p>

    <p class="memofmt1-2">          Bayesian network analysis of resistance pathways against protease inhibitors</p>

    <p>          Deforche, K, Camacho, R, Grossman, Z, Silander, T, Soares, MA, Moreau, Y, Shafer, RW, Van, Laethem K, Carvalho, AP, Wynhoven, B, Cane, P, Snoeck, J, Clarke, J, Sirivichayakul, S, Ariyoshi, K, Holguin, A, Rudich, H, Rodrigues, R, Bouzas, MB, Cahn, P, Brigido, LF, Soriano, V, Sugiura, W, Phanuphak, P, Morris, L, Weber, J, Pillay, D, Tanuri, A, Harrigan, PR, Shapiro, JM, Katzenstein, DA, Kantor, R, and Vandamme, AM</p>

    <p>          Infect Genet Evol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17127103&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17127103&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
