

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-10-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="M/uGYJ1SGJzAXTr5SKmSAvmKZZhN6KdOfLpUt8JDdzKRhL2GLElMoUEk1YWiYs6sSyt9WM6fQeLiSz/av+n4Nr2/1c/RWMN1royQJdoYNj4evvI2wS0wjjz0D4U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3A2C658F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">October 14 - October 27, 2011</h1>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22007627">Antifungal Effect of the Essential Oil of Thymus broussonetii Boiss Endogenous Species of Morocco.</a> Bellete, B., H. Raberin, P. Flori, S. El Akssi, R. Tran Manh Sung, M. Taourirte, and J. Hafid. Natural Product Research, 2011. <b>[Epub ahead of print]</b>; PMID[22007627].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21752697">Antioxidant, Electrochemical, Thermal, Antimicrobial and Alkane Oxidation Properties of Tridentate Schiff Base Ligands and Their Metal Complexes.</a> Ceyhan, G., C. Celik, S. Urus, I. Demirtas, M. Elmastas, and M. Tumer. Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2011. 81(1): p. 184-98; PMID[21752697].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21999633">Synthesis, Antimicrobial Activity and Cytotoxicity of Some New Carbazole Derivatives.</a> Kaplancikli, Z.A., L. Yurttas, G. Turan-Zitouni, A. Ozdemir, R. Ozic, and S. Ulusoylar-Yildirim. Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21999633].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723186">Novel Transition Metal Complexes of 4-Hydroxy-coumarin-3-thiocarbohydrazone: Pharmacodynamic of Co(III) on Rats and Antimicrobial Activity.</a> Mosa, A.I., A.A. Emara, J.M. Yousef, and A.A. Saddiq.  Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2011. 81(1): p. 35-43; PMID[21723186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21999582">Synthesis, Antimicrobial and Antiproliferative Activity of Novel Silver(I) Tris(pyrazolyl)methanesulfonate and 1,3,5-Triaza-7-phosphadamantane Complexes.</a> Pettinari, C., F. Marchetti, G. Lupidi, L. Quassinti, M. Bramucci, D. Petrelli, L.A. Vitali, M.F. Guedes da Silva, L.M. Martins, P. Smolenski, and A.J. Pombeiro. Inorganic Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21999582].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21752698">Spectroscopic and Mycological Studies of Co(II), Ni(II) and Cu(II) Complexes with 4-Aminoantipyrine Derivative.</a> Sharma, A.K. and S. Chandra, Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2011. 81(1): p. 424-430; PMID[21752698].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22008010">Phenol Esters and other Constituents from the Stem Barks of Stereospermum acuminatissimum.</a> Sob Tanemossu, S.V., H.K. Wabo, C.P. Tang, P. Tane, B.T. Ngadjui, and Y. Ye, Journal of Asian Natural Products Research, 2011. <b>[Epub ahead of print]</b>; PMID[22008010].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22016556">Correlation of the Chemical Composition of Essential Oils from Origanum vulgare subsp. virens with the in Vitro Activity against Clinical Yeast and Filamentous Fungi</a>. Vale-Silva, L.A., M.J. Silva, D. Oliveira, M.J. Goncalves, C. Cavaleiro, L. Salgueiro, and E. Pinto, Journal of Medical Microbiology, 2011.<b>[Epub ahead of print]</b>; PMID[22016556].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22010795">Synthesis, Crystal Structure, Spectroscopic, Magnetic, Theoretical, and Microbiological Studies of a Nickel(II) Complex of l-Tyrosine and Imidazole, [Ni(Im)(2)(l-tyr)(2)].4H(2)O.</a> Wojciechowska, A., M. Daszkiewicz, Z. Staszak, A. Trusz-Zdybek, A. Bienko, and A. Ozarowski, Inorganic Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22010795].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22020493">Antifungal Activity of Phenolic-rich Lavandula multifida L. Essential Oil.</a> Zuzarte, M., L. Vale-Silva, M.J. Goncalves, C. Cavaleiro, S. Vaz, J. Canhoto, E. Pinto, and L. Salgueiro, European Journal of Clinical Microbiology &amp; Infectious Diseases : Official Publication of the European Society of Clinical Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[22020493].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p>

      <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21767828">Synthesis of Quinoline Coupled [1,2,3]-Triazoles as a Promising Class of anti-Tuberculosis Agents.</a> Karthik Kumar, K., S. Prabu Seenivasan, V. Kumar, and T. Mohan Das. Carbohydrate Research, 2011. 346(14): p. 2084-2090; PMID[21767828].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22006792">Benzothiophene carboxamide Derivatives as Inhibitors of Plasmodium falciparum Enoyl-ACP Reductase.</a> Banerjee, T., S.K. Sharma, N. Kapoor, V. Dwivedi, N. Surolia, and A. Surolia. IUBMB life, 2011. <b>[Epub ahead of print]</b>; PMID[22006792].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22006835">Total Synthesis, Stereochemical Assignment, and Antimalarial Activity of Gallinamide A.</a> Conroy, T., J.T. Guo, R.G. Linington, N.H. Hunt, and R.J. Payne, Chemistry (Weinheim an der Bergstrasse, Germany). 2011. <b>[Epub ahead of print]</b>; PMID[22006835].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22008701">In Vitro and in Vivo Activity of Frenolicin B against Plasmodium falciparum and P berghei.</a> Fitzgerald, J.T., P.P. Henrich, C. O&#39;Brien, M. Krause, E.H. Ekland, C. Mattheis, J.M. Sa, D. Fidock, and C. Khosla. The Journal of Antibiotics, 2011. <b>[Epub ahead of print]</b>; PMID[22008701].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22004007">Cytotoxic and Antimalarial Azaphilones from Chaetomium longirostre.</a>Panthama, N., S. Kanokmedhakul, K. Kanokmedhakul, and K. Soytong. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[22004007].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21612900">Indole and Aminoimidazole Moieties appear as Key Structural Units in Antiplasmodial Molecules.</a> Passemar, C., M. Salery, P.N. Soh, M.D. Linas, A. Ahond, C. Poupat, and F. Benoit-Vical. Phytomedicine: International Journal of Phytotherapy and Phytopharmacology, 2011. 18(13): p. 1118-1125; PMID[21612900].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</p>

    <p class="plaintext">17.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/22014034">Identification of Novel Inhibitors of Dipeptidylcarboxypeptidase of Leishmania donovani via Ligand Based Virtual Screening and Biological Evaluation.</a> Gangwar, S., M.S. Baig, P. Shah, S. Biswas, S. Batra, M.I. Siddiqi, and N. Goyal. Chemical Biology &amp; Drug Design, 2011; PMID[22014034].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22024817">Discovery of Safe and Orally Effective 4-Aminoquinaldine Analogues as Apoptotic Inducers with Activity against Experimental Visceral Leishmaniasis.</a>  Palit, P., A. Hazra, A. Maity, R.S. Vijayan, P. Manoharan, S. Banerjee, N.B. Mondal, N. Ghoshal, and N. Ali. Antimicrobial Agents and Chemotherapy, 2011; PMID[22024817].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295211000050">Schiff Bases of Indoline-2,3-dione: Potential Novel Inhibitors of Mycobacterium Tuberculosis (Mtb) DNA Gyrase</a>. Aboul-Fadl, T., H.A. Abdel-Aziz, M.K. Abdel-Hamid, T. Elsaman, J. Thanassi, and M.J. Pucci, Molecules, 2011. 16(9): p. 7864-7879; ISI[000295211000050].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295798900017">Volatile Constituents and anti-Candida Activity of the Aerial Parts Essential Oil of Dittrichia graveolens (L.) Greuter Grown in Iran.</a> Aghel, N., A.Z. Mahmoudabadi, and L. Darvishi, African Journal of Pharmacy and Pharmacology, 2011. 5(6): p. 772-775; ISI[000295798900017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295494500038">Ligand Based Virtual Screening and Biological Evaluation of Inhibitors of Chorismate Mutase (Rv1885c) from Mycobacterium tuberculosis H37Rv (vol 17, pg 3053, 2007).</a> Agrawal, H., A. Kumar, N.C. Bal, M.I. Siddiqi, and A. Arora, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(20): p. 6206-6206; ISI[000295494500038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295802000002">Phytochemical Analysis and Antimicrobial Evaluation of Andrographis lineata Nees Leaves and Stem Extracts.</a> Alagesaboopathi, C., African Journal of Pharmacy and Pharmacology, 2011. 5(9): p. 1190-1195; ISI[000295802000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295211000045">Chemical, Antioxidant and Antimicrobial Investigations of Pinus cembra L. Bark and Needles</a>. Apetrei, C.L., C. Tuchilus, A.C. Aprotosoaie, A. Oprea, K.E. Malterud, and A. Miron, Molecules, 2011. 16(9): p. 7773-7788; ISI[000295211000045].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295577000034">Biological Evaluation of Novel 1,4-Dithiine Derivatives as Potential Antimicrobial Agents.</a> Bielenica, A., J. Kossakowski, M. Struga, I. Dybala, P. La Colla, E. Tamburini, and R. Loddo, Medicinal Chemistry Research, 2011. 20(8): p. 1411-1420; ISI[000295577000034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295113800029">Antimicrobial Activity of Delaminated Aminopropyl Functionalized Magnesium phyllosilicates.</a> Chandrasekaran, G., H.K. Han, G.J. Kim, and H.J. Shin, Applied Clay Science, 2011. 53(4): p. 729-736; ISI[000295113800029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295498600009">Liriodenine, Early Antimicrobial Defence in Annona diversifolia.</a> De la Cruz-Chacon, I., A.R. Gonzalez-Esquinca, P.G. Fefer, and L.F.J. Garcia, Zeitschrift Fur Naturforschung Section C-a Journal of Biosciences, 2011. 66(7-8): p. 377-384; ISI[000295498600009].</p>

    <p class="plaintext"><b>WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295430800019">Antimicrobial Activities of the Lichen Roccella belangeriana (Awasthi) from Mangroves of Gulf of Mannar.</a>  Devi, G.K., P. Anantharaman, K. Kathiresan, and T. Balasubramanian, INDIAN JOURNAL OF GEO-MARINE SCIENCES LA., 2011. 40(3): p. 449-453; ISI[000295430800019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295308600014">Anti-inflammatory and Antimicrobial Profiles of Scilla nervosa (Burch.) Jessop (Hyacinthaceae).</a>du Toit, K., A. Kweyama, and J. Bodenstein, South African Journal of Science, 2011. 107(5-6): p. 96-100; ISI[000295308600014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295391100013">Indole-3-carbinol Generates Reactive Oxygen Species and Induces Apoptosis</a>. Hwang, I.S., J. Lee, and D.G. Lee, Biological &amp; Pharmaceutical Bulletin, 2011. 34(10): p. 1602-1608; ISI[000295391100013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295444900018">Antimalarial and Antituberculosis Substances from Streptomyces sp BCC26924.</a> Intaraudom, C., P. Rachtawee, R. Suvannakad, and P. Pittayakhajonwut, Tetrahedron, 2011. 67(39): p. 7593-7597; ISI[000295444900018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295494500014">Synthesis and Structure-Activity Relationships of Novel Substituted 8-Amino, 8-Thio, and 1,8-Pyrazole Congeners of Antitubercular Rifamycin S and Rifampin.</a> Jin, Y.F., S.K. Gill, P.D. Kirchhoff, B.J. Wan, S.G. Franzblau, G.A. Garcia, and H.D.H. Showalter, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(20): p. 6094-6099; ISI[000295494500014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295689300005">Chemical Constituents and Antimicrobial Activity of the Essential Oil from Vicia dadianorum Extracted by Hydro and Microwave Distillations.</a> Kahriman, N., B. Yayli, M. Yucel, S.A. Karaoglu, and N. Yayli, Records of Natural Products, 2012. 6(1): p. 49-56; ISI[000295689300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295533100012">Synthesis and Antituberculosis Activity of Derivatives of Stevia rebaudiana Glycoside Steviolbioside and Diterpenoid Isosteviol Containing Hydrazone, Hydrazide, and Pyridinoyl Moieties.</a> Kataev, V.E., I.Y. Strobykina, O.V. Andreeva, B.F. Garifullin, R.R. Sharipova, V.F. Mironov, and R.V. Chestnova, Russian Journal of Bioorganic Chemistry, 2011. 37(4): p. 483-491; ISI[000295533100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295577000012">Synthesis and Antimicrobial Activity of Some Bisoctahydroxanthene-1,8-dione Derivatives.</a> Kaya, M., E. Basar, and F. Colak, Medicinal Chemistry Research, 2011. 20(8): p. 1214-1219; ISI[000295577000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295689300009">Gas Chromatographic Determination of Quinolizidine Alkaloids in Genista sandrasica and Their Antimicrobial Activity.</a> Kucukboyaci, N., S. Ozkan, and F. Tosun, Records of Natural Products, 2012. 6(1): p. 71-74; ISI[000295689300009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295577000026">Design and Synthesis of 2-Chloroquinoline Derivatives as Non-azoles Antimycotic Agents.</a> Kumar, S., S. Bawa, S. Drabu, and B.P. Panda, Medicinal Chemistry Research, 2011. 20(8): p. 1340-1348; ISI[000295577000026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295592700027">Synthesis, Crystal Structures, Antimicrobial Properties and Enzyme Inhibition Studies of Zinc(II) Complexes of Thiones.</a> Malik, M.R., V. Vasylyeva, K. Merz, N. Metzler-Nolte, M. Saleem, S. Ali, A.A. Isab, K.S. Munawar, and S. Ahmad, Inorganica Chimica Acta, 2011. 376(1): p. 207-211; ISI[000295592700027].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295659800006">Synthesis, Spectral, Thermal and Biological Studies of Transition Metal Complexes of 4-Hydroxy-3- 3-(4-hydroxyphenyl)-acryloyl -6-methyl-2H-pyran-2-one.</a> Patange, V.N. and B.R. Arbad, Journal of the Serbian Chemical Society, 2011. 76(9): p. 1237-1246; ISI[000295659800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295498600005">Fluorinated s-Triazinyl Piperazines as Antimicrobial Agents</a>. Patel, R.V., P. Kumari, and K.H. Chikhalia, Zeitschrift Fur Naturforschung Section C-a Journal of Biosciences, 2011. 66(7-8): p. 345-352; ISI[000295498600005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295320800002">Synthesis of Some NH-Derivatives of Ciprofloxacin as Antibacterial and Antifungal Agents.</a> Rabbani, M.G., M.R. Islam, M. Ahmad, and A.M.L. Hossion, Bangladesh Journal of Pharmacology, 2011. 6(1): p. 8-13; ISI[000295320800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295246900014">Chemical Composition and Biological Properties of the Leaf Essential Oil of Tagetes lucida Cav. from Cuba</a>. Regalado, E.L., M.D. Fernandez, J.A. Pino, J. Mendiola, and O.A. Echemendia, Journal of Essential Oil Research, 2011. 23(5): p. 63-67; ISI[000295246900014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295797700004">Antifungal, Cytotoxic Activities and Docking Studies of 2,5-Dimercapto-1,3,4-thiadiazole Derivatives.</a> Samee, W. and O. Vajragupta, African Journal of Pharmacy and Pharmacology, 2011. 5(4): p. 477-485; ISI[000295797700004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295533700005">In Vitro and in Situ Antiyeast Activity of Gracilaria changii Methanol Extract against Candida albicans.</a> Sasidharan, S., I. Darah, and K. Jain, European Review for Medical and Pharmacological Sciences, 2011. 15(9): p. 1020-1026; ISI[000295533700005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>

    <p> </p>

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000295494500007">Synthesis and Biological Activity of Desmethoxy Analogues of Coruscanone.</a>A. Tichotova, L., E. Matousova, M. Spulak, J. Kunes, I. Votruba, V. Buchta, and M. Pour, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(20): p. 6062-6066; ISI[000295494500007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1014-102711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
