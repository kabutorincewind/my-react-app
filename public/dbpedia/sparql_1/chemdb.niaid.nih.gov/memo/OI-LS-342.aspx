

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-342.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hWCJBnJs5g9iJGaC7Qlsp60AV6a3SrT+MRNAzEczQOZkoZFdJq+lgtBs0UWeRbfXfOq2LlXI12vPIgIfuh4CmPTHo8Tk3gbpuZxvU545t2OlIxqHbN6prk+rca0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6BB1B479" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-342-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48505   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Searching for a new anti-HCV therapy: Synthesis and properties of tropolone derivatives</p>

    <p>          Boguszewska-Chachulska, Anna M, Krawczyk, Mariusz, Najda, Andzelika, Kopanska, Katarzyna, Stankiewicz-Drogon, Anna, Zagorski-Ostoja, Wlodzimierz, and Bretner, Maria</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  341(2): 641-647</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4J2CMTG-6/2/391fe977bbb3542e84475ffa4243440e">http://www.sciencedirect.com/science/article/B6WBK-4J2CMTG-6/2/391fe977bbb3542e84475ffa4243440e</a> </p><br />

    <p>2.     48506   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Purification and Characterization of Mycobacterium tuberculosis Indole-3-glycerol Phosphate Synthase</p>

    <p>          Yang, Y, Zhang, M, Zhang, H, Lei, J, Jin, R, Xu, S, Bao, J, Zhang, L, and Wang, H</p>

    <p>          Biochemistry (Mosc) <b>2006</b>.  71 Suppl 1: S38-S43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16487066&amp;dopt=abstract</a> </p><br />

    <p>3.     48507   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Mycobacterium tuberculosis Cells Growing in Macrophages Are Filamentous and Deficient in FtsZ Rings</p>

    <p>          Chauhan, A, Madiraju, MV, Fol, M, Lofton, H, Maloney, E, Reynolds, R, and Rajagopalan, M</p>

    <p>          J Bacteriol <b>2006</b>.  188(5): 1856-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16484196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16484196&amp;dopt=abstract</a> </p><br />

    <p>4.     48508   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Screening for antimicrobial activity of ten medicinal plants used in Colombian folkloric medicine: A possible alternative in the treatment of non-nosocomial infections</p>

    <p>          Rojas, JJ, Ochoa, VJ, Ocampo, SA, and Munoz, JF</p>

    <p>          BMC Complement Altern Med <b>2006</b>.  6(1): 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16483385&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16483385&amp;dopt=abstract</a> </p><br />

    <p>5.     48509   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of some alkyl [5-(nitroaryl)-1,3,4-thiadiazol-2-ylthio]propionates</p>

    <p>          Foroumadi, Alireza, Kargar, Zahra, Sakhteman, Amirhossein, Sharifzadeh, Zahra, Feyzmohammadi, Robabeh, Kazemi, Mahnoush, and Shafiee, Abbas</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(5): 1164-1167</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HTBKW5-4/2/50a57caf803407414f0c7da18ddf365f">http://www.sciencedirect.com/science/article/B6TF9-4HTBKW5-4/2/50a57caf803407414f0c7da18ddf365f</a> </p><br />
    <br clear="all">

    <p>6.     48510   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Identification of Isoflavone Derivatives as Effective Anticryptosporidial Agents in Vitro and in Vivo</p>

    <p>          Stachulski, AV, Berry, NG, Lilian, Low AC, Moores, SL, Row, E, Warhurst, DC, Adagu, IS, and Rossignol, JF</p>

    <p>          J Med Chem <b>2006</b>.  49(4): 1450-1454</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480281&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16480281&amp;dopt=abstract</a> </p><br />

    <p>7.     48511   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Investigation of the mechanism of action of 3-(4-bromophenyl)-5-acyloxymethyl-2,5-dihydrofuran-2-one against Candida albicans by flow cytometry</p>

    <p>          Vale-Silva, Luis A, Buchta, Vladimir, Vokurkova, Doris, and Pour, Milan</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J8KVXN-3/2/d1833ea155377fc3c4effa5eda4ec269">http://www.sciencedirect.com/science/article/B6TF9-4J8KVXN-3/2/d1833ea155377fc3c4effa5eda4ec269</a> </p><br />

    <p>8.     48512   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Substituent effect of fluorine atoms in the 2,4-difluorophenyl group on antifungal activity of CS-758</p>

    <p>          Kagoshima, Yoshiko and Konosu, Toshiyuki</p>

    <p>          Journal of Fluorine Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TGD-4J7B186-1/2/202252d043fff8ad3d9552ecdeda157c">http://www.sciencedirect.com/science/article/B6TGD-4J7B186-1/2/202252d043fff8ad3d9552ecdeda157c</a> </p><br />

    <p>9.     48513   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Towards Species-specific Antifolates</p>

    <p>          Chan, DC and Anderson, AC</p>

    <p>          Curr Med Chem <b>2006</b>.  13(4): 377-98</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16475929&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16475929&amp;dopt=abstract</a> </p><br />

    <p>10.   48514   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Synthesis and potent antimicrobial activities of some novel retinoidal monocationic benzimidazoles</p>

    <p>          Ates-Alagoz, Z, Alp, M, Kus, C, Yildiz, S, Buyukbingol, E, and Goker, H</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.  339(2): 74-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16470650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16470650&amp;dopt=abstract</a> </p><br />

    <p>11.   48515   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          In vitro interactions of anidulafungin with azole antifungals, amphotericin B and 5-fluorocytosine against Candida species</p>

    <p>          Karlowsky, James A, Hoban, Daryl J, Zhanel, George G, and Goldstein, Beth P</p>

    <p>          International Journal of Antimicrobial Agents <b>2006</b>.  27(2): 174-177</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4J2M4C5-8/2/008b4fdf9bd8e8bd2530f6fc6b67abae">http://www.sciencedirect.com/science/article/B6T7H-4J2M4C5-8/2/008b4fdf9bd8e8bd2530f6fc6b67abae</a> </p><br />

    <p>12.   48516   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Steady-state kinetics and inhibitory action of antitubercular phenothiazines on mycobacterium tuberculosis type-II NADH-menaquinone oxidoreductase (NDH-2)</p>

    <p>          Yano, T, Li, LS, Weinstein, E, Teh, JS, and Rubin, H</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16469750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16469750&amp;dopt=abstract</a> </p><br />

    <p>13.   48517   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Serum amyloid P-component in murine tuberculosis: induction kinetics and intramacrophage Mycobacterium tuberculosis growth inhibition in vitro</p>

    <p>          Singh, Prati Pal and Kaur, Sukhraj</p>

    <p>          Microbes and Infection <b>2006</b>.  8(2): 541-551</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4HC15V5-4/2/68f2a9a06057002f67b92dd8ae940efa">http://www.sciencedirect.com/science/article/B6VPN-4HC15V5-4/2/68f2a9a06057002f67b92dd8ae940efa</a> </p><br />

    <p>14.   48518   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Structure of the Mycobacterium tuberculosis proteasome and mechanism of inhibition by a peptidyl boronate</p>

    <p>          Hu, G, Lin, G, Wang, M, Dick, L, Xu, RM, Nathan, C, and Li, H</p>

    <p>          Mol Microbiol <b>2006</b>.  59(5): 1417-28</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16468986&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16468986&amp;dopt=abstract</a> </p><br />

    <p>15.   48519   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Synthesis of pyrazinamide Mannich bases and its antitubercular properties</p>

    <p>          Sriram, D, Yogeeswari, P, and Reddy, SP</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464574&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16464574&amp;dopt=abstract</a> </p><br />

    <p>16.   48520   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Medicinal plants used by Tanzanian traditional healers in the management of Candida infections</p>

    <p>          Runyoro, DK, Ngassapa, OD, Matee, MI, Joseph, CC, and Moshi, MJ</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16458463&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   48521   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          SAR and mode of action of novel non-nucleoside inhibitors of Hepatitis C NS5b RNA polymerase</p>

    <p>          Powers, JP, Piper, DE, Li, Y, Mayorga, V, Anzola, J, Chen, JM, Jaen, JC, Lee, G, Liu, J, Peterson, MG, Tonn, GR, Ye, Q, Walker, NP, and Wang, Z</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 1034-46</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451069&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451069&amp;dopt=abstract</a> </p><br />

    <p>18.   48522   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Novel potent hepatitis C virus NS3 serine protease inhibitors derived from proline-based macrocycles</p>

    <p>          Chen, KX, Njoroge, FG, Arasappan, A, Venkatraman, S, Vibulbhan, B, Yang, W, Parekh, TN, Pichardo, J, Prongay, A, Cheng, KC, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 995-1005</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451065&amp;dopt=abstract</a> </p><br />

    <p>19.   48523   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          3-(1,1-dioxo-2H-(1,2,4)-benzothiadiazin-3-yl)-4-hydroxy-2(1H)-quinolinones , potent inhibitors of hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Tedesco, R, Shaw, AN, Bambal, R, Chai, D, Concha, NO, Darcy, MG, Dhanak, D, Fitch, DM, Gates, A, Gerhardt, WG, Halegoua, DL, Han, C, Hofmann, GA, Johnston, VK, Kaura, AC, Liu, N, Keenan, RM, Lin-Goerke, J, Sarisky, RT, Wiggall, KJ, Zimmerman, MN, and Duffy, KJ</p>

    <p>          J Med Chem <b>2006</b>.  49(3): 971-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451063&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16451063&amp;dopt=abstract</a> </p><br />

    <p>20.   48524   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Design and synthesis of 2,3,4,9-tetrahydro-1H-carbazole and 1,2,3,4-tetrahydro-cyclopenta[b]indole derivatives as non-nucleoside inhibitors of hepatitis C virus NS5B RNA-dependent RNA polymerase</p>

    <p>          Gopalsamy, Ariamala, Shi, Mengxiao, Ciszewski, Gregory, Park, Kaapjoo, Ellingboe, John W, Orlowski, Mark, Feld, Boris, and Howe, Anita YM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J8KVXN-5/2/64d632ff00cc74b8e443ebb9c197146c">http://www.sciencedirect.com/science/article/B6TF9-4J8KVXN-5/2/64d632ff00cc74b8e443ebb9c197146c</a> </p><br />

    <p>21.   48525   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Benzimidazole inhibitors of hepatitis C virus NS5B polymerase: Identification of 2-[(4-diarylmethoxy)phenyl]-benzimidazole</p>

    <p>          Ishida, Tomio, Suzuki, Takayoshi, Hirashima, Shintaro, Mizutani, Kenji, Yoshida, Atsuhito, Ando, Izuru, Ikeda, Satoru, Adachi, Tsuyoshi, and Hashimoto, Hiromasa</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J6W71K-9/2/325ffdf15276aa1e2037292f04302849">http://www.sciencedirect.com/science/article/B6TF9-4J6W71K-9/2/325ffdf15276aa1e2037292f04302849</a> </p><br />
    <br clear="all">

    <p>22.   48526   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          An efficient, asymmetric solid-phase synthesis of benzothiadiazine-substituted tetramic acids: Potent inhibitors of the hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Evans, Karen A, Chai, Deping, Graybill, Todd L, Burton, George, Sarisky, Robert T, Lin-Goerke, Juili, Johnston, Victor K, and Rivero, Ralph A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J6W71K-B/2/741f5b4d1e7cead93fe8a702f79341c4">http://www.sciencedirect.com/science/article/B6TF9-4J6W71K-B/2/741f5b4d1e7cead93fe8a702f79341c4</a> </p><br />

    <p>23.   48527   OI-LS-342; PUBMED-OI-2/21/2006</p>

    <p class="memofmt1-2">          Hexadecylphosphocholine (miltefosine) has broad-spectrum fungicidal activity and is efficacious in a mouse model of cryptococcosis</p>

    <p>          Widmer, F, Wright, LC, Obando, D, Handke, R, Ganendren, R, Ellis, DH, and Sorrell, TC</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(2): 414-21</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436691&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16436691&amp;dopt=abstract</a> </p><br />

    <p>24.   48528   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          The anti-malaria drug artesunate inhibits replication of cytomegalovirus in vitro and in vivo</p>

    <p>          Kaptein, Suzanne JF, Efferth, Thomas, Leis, Martina, Rechter, Sabine, Auerochs, Sabrina, Kalmer, Martina, Bruggeman, Cathrien A, Vink, Cornelis, Stamminger, Thomas, and Marschall, Manfred</p>

    <p>          Antiviral Research <b>2006</b>.  69(2): 60-69</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4HM7N1C-2/2/35e5ce6bd0a7a3a899cb3045446f175b">http://www.sciencedirect.com/science/article/B6T2H-4HM7N1C-2/2/35e5ce6bd0a7a3a899cb3045446f175b</a> </p><br />

    <p>25.   48529   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Parallel synthesis of 5-cyano-6-aryl-2-thiouracil derivatives as inhibitors for hepatitis C viral NS5B RNA-dependent RNA polymerase</p>

    <p>          Ding, Yili, Girardet, Jean-Luc, Smith, Kenneth L, Larson, Gary, Prigaro, Brett, Wu, Jim Z, and Yao, Nanhua</p>

    <p>          Bioorganic Chemistry <b>2006</b>.  34(1): 26-38</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBT-4HV74BV-1/2/68186fa1e4a674b6cb21642fc456b112">http://www.sciencedirect.com/science/article/B6WBT-4HV74BV-1/2/68186fa1e4a674b6cb21642fc456b112</a> </p><br />

    <p>26.   48530   OI-LS-342; EMBASE-OI-2/21/2006</p>

    <p class="memofmt1-2">          Novel Inhibitors of Hepatitis C Virus RNA-dependent RNA Polymerases</p>

    <p>          Lee, Gary, Piper, Derek E, Wang, Zhulun, Anzola, John, Powers, Jay, Walker, Nigel, and Li, Yang</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4J554RY-2/2/affee7a6de984abb455f78ee2f1d17d6">http://www.sciencedirect.com/science/article/B6WK7-4J554RY-2/2/affee7a6de984abb455f78ee2f1d17d6</a> </p><br />

    <p>27.   48531   OI-LS-342; WOS-OI-2/12/2006</p>

    <p class="memofmt1-2">          Mechanisms of action of ribavirin against distinct viruses</p>

    <p>          Graci, JD and Cameron, CE</p>

    <p>          REVIEWS IN MEDICAL VIROLOGY <b>2006</b>.  16(1): 37-48, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234869200003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234869200003</a> </p><br />

    <p>28.   48532   OI-LS-342; WOS-OI-2/12/2006</p>

    <p class="memofmt1-2">          Potent 7-hydroxy-1,2,3,4-tetrahydroisoquinoline-3-carboxylic acid-based macrocyclic inhibitors of hepatitis C virus NS3 protease</p>

    <p>          Chen, KX, Njoroge, FG, Pichardo, J, Prongay, A, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(2): 567-574, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234836200017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234836200017</a> </p><br />

    <p>29.   48533   OI-LS-342; WOS-OI-2/12/2006</p>

    <p class="memofmt1-2">          Correlation of antifungal activity with fungal phospholipase inhibition using a series of bisquaternary ammonium salts</p>

    <p>          Ng, CKL, Obando, D, Widmer, F, Wright, LC, Sorrell, TC, and Jolliffe, KA</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  49(2): 811-816, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234836200041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234836200041</a> </p><br />

    <p>30.   48534   OI-LS-342; WOS-OI-2/12/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro antitubercular activity of some 1-[(4-sub)phenyl]-3-(4-{1-[(pyridine-4-carbonyl) hydrazono]ethyl}phenyl)thiourea</p>

    <p>          Sriram, D, Yogeeswari, P, and Madhu, K</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2006</b>.  16(4): 876-878, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234946300024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234946300024</a> </p><br />

    <p>31.   48535   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          The role of an amphiphilic capping group in covalent and non-covalent dipeptide inhibitors of HCVNS3 serine protease</p>

    <p>          Colarusso, S, Gerlach, B, Giuliano, C, Koch, U, Matassa, VG, and Narjes, F</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(2): 113-117, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400005</a> </p><br />

    <p>32.   48536   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          P1 and P1 &#39; optimization of [3,4]-bicycloproline P2 incorporated tetrapeptidyl alpha-ketoamide based HCV protease inhibitors</p>

    <p>          Chen, SH, Lamar, J, Yip, Y, Victor, F, Johnson, RB, Wang, QM, Glass, JI, Heinz, B, Colacino, J, Guo, DQ, Tebbe, M, and Munroe, JE</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(2): 118-123, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235081400006</a> </p><br />

    <p>33.   48537   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          Phenyldihydroxypyrimidines as HCVNS5B RNA dependent RNA polymerase inhibitors. Part 1: Amides and ureas</p>

    <p>          Crescenzi, B, Poma, M, Ontoria, JM, Marchetti, A, Nizi, E, Matassa, VG, and Gardelli, C</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(6): 451-455, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900004</a> </p><br />
    <br clear="all">

    <p>34.   48538   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          Phenyldihydroxypyrimidines as HCVNS5B RNA dependent RNA polymerase inhibitors. Part II: Sulfonamides</p>

    <p>          Ponzi, S, Giuliano, C, Donghi, M, Poma, M, Matassa, VG, and Stansfield, I</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(6): 456-461, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235167900005</a> </p><br />

    <p>35.   48539   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          Inhibitors of hepatitis C virus NS3 center dot 4A protease: P-2 proline variants</p>

    <p>          Farmer, LJ, Britt, SD, Cottrell, KM, Court, JJ, Courtney, LF, Deininger, DD, Gates, CA, Harbeson, SL, Lin, K, Lin, C, Luong, YP, Maxwell, JP, Pitlik, J, Rao, BG, Schairer, WC, Thomson, JA, Tung, RD, Van, Drie JH, Wei, YY, and Perni, RB</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(7): 497-502, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000001</a> </p><br />

    <p>36.   48540   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-mycobacterial activity of N &#39;-[(E)-(disubstituted-phenyl)methylidene]isoni otino-hydrazide derivatives</p>

    <p>          Junior, IN, Lourenco, MCS, das Gracas, M, Henriques, MO,  Ferreira, B, Vasconcelos, TRA, Peralta, MA, de Oliveira, PSM, Wardell, SMSV, and de Souza, MVN</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2005</b>.  2(7): 563-566, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235168000012</a> </p><br />

    <p>37.   48541   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          A novel, highly selective inhibitor of pestivirus replication that targets the viral RNA-dependent RNA polymerase</p>

    <p>          Paeshuyse, J, Leyssen, P, Mabery, E, Boddeker, N, Vrancken, R, Froeyen, M, Ansari, IH, Dutartre, H, Rozenski, J, Gil, LHVG, Letellier, C, Lanford, R, Canard, B, Koenen, F, Kerkhofs, P, Donis, RO, Herdewijn, P, Watson, J, De, Clercq E, Puerstinger, G, and Neyts, J</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(1): 149-160, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235091700013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235091700013</a> </p><br />

    <p>38.   48542   OI-LS-342; WOS-OI-2/19/2006</p>

    <p class="memofmt1-2">          Hurghadolide A and swinholide I, potent actin-microfilament disrupters from the Red Sea sponge Theonella swinhoei</p>

    <p>          Youssef, DTA and Mooberry, SL</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(1): 154-157, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235106500035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000235106500035</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
