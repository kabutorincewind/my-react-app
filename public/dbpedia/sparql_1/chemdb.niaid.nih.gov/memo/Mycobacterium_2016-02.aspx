

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2016-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="sUeAHF5gSK9kmtF8TM7PnPMC0iU9kftzkdQlM5ELBuUYnLLGK2mzgGhdrudH1bHRcJWYOiFtoZzaYMbqth8GjbETdUVX99SAadUKp/LQPnADIbig9JoCYkgU2XI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C0259B2E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: February, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000369003300030">Fused Heterocycles: Synthesis and Antitubercular Activity of Novel 6-Substituted-2-(4-methyl-2-substituted phenylthiazol-5-yl)H-imidazo[1,2-a]Pyridine.</a> Abhale, Y.K., K.K. Deshmukh, A.V. Sasane, A.P. Chavan, and P.C. Mhaske. Journal of Heterocyclic Chemistry, 2016. 53(1): p. 229-233. ISI[000369003300030].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26792518">Fluoroquinolone Interactions with Mycobacterium tuberculosis Gyrase: Enhancing Drug Activity against Wild-type and Resistant Gyrase.</a> Aldred, K.J., T.R. Blower, R.J. Kerns, J.M. Berger, and N. Osheroff. Proceedings of the National Academy of Sciences of the United States of America, 2016. 113(7): p. 839-846. PMID[26792518]. PMCID[PMC4763725].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368464400007">Whole Cell Target Engagement Identifies Novel Inhibitors of Mycobacterium tuberculosis Decaprenylphosphoryl-beta-D-ribose Oxidase.</a> Batt, S.M., M.C. Izquierdo, J.C. Pichel, C.J. Stubbs, L.V.G. Del Peral, E. Perez-Herran, N. Dhar, B. Mouzon, M. Rees, J.P. Hutchinson, R.J. Young, J.D. McKinney, D.B. Aguirre, L. Ballell, G.S. Besra, and A. Argyrou. ACS  Infectious Diseases, 2015. 1(12): p. 615-626. ISI[000368464400007].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368548500008">Some Novel Schiff Bases of 1,2,4 Triazole Bearing Haloarene Moiety-synthesis and Evaluation of Antituberculosis Properties and Neutrophil Function Test.</a> Castelino, P.A., J.P. Dasappa, K.G. Bhat, S.A. Joshi, and S. Jalalpure. Medicinal Chemistry Research, 2016. 25(1): p. 83-93. ISI[000368548500008].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26919940">Evaluation of Anti-tubercular Activity of Linolenic acid and Conjugated-linoleic acid as Effective Inhibitors against Mycobacterium tuberculosis.</a> Choi, W.H. Asian Pacific Journal of Tropical Medicine, 2016. 9(2): p. 125-129. PMID[26919940].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26810833">Synthesis and Anti-tuberculosis Activity of Glycitylamines.</a> Corkran, H.M., E.M. Dangerfield, G.W. Haslett, B.L. Stocker, and M.S. Timmer. Bioorganic &amp; Medicinal Chemistry, 2016. 24(4): p. 693-702. PMID[26810833].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368548800012">Synthesis and Biological Evaluation of 1,3,4-Oxadiazole Bearing Dihydropyrimidines as Potential Antitubercular Agents.</a> Desai, N.C., A.R. Trivedi, H.V. Vaghani, H.C. Somani, and K.A. Bhatt. Medicinal Chemistry Research, 2016. 25(2): p. 329-338. ISI[000368548800012].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368959100011">Antimycobacterial Activity against Different Pathogens and Selectivity Index of Fourteen Medicinal Plants Used in Southern Africa to Treat Tuberculosis and Respiratory Ailments.</a> Dzoyem, J.P., A.O. Aro, L.J. McGaw, and J.N. Eloff. South African Journal of Botany, 2016. 102: p. 70-74. ISI[000368959100011].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26643335">Clofazimine Prevents the Regrowth of Mycobacterium abscessus and Mycobacterium avium Type Strains Exposed to Amikacin and Clarithromycin.</a> Ferro, B.E., J. Meletiadis, M. Wattenberg, A. de Jong, D. van Soolingen, J.W. Mouton, and J. van Ingen. Antimicrobial Agents and Chemotherapy, 2016. 60(2): p. 1097-1105. PMID[26643335]. PMCID[PMC4750661].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26735610">Nordihydroguaiaretic acid (NDGA) and alpha-Mangostin Inhibit the Growth of Mycobacterium tuberculosis by Inducing Autophagy.</a> Guzman-Beltran, S., M.A. Rubio-Badillo, E. Juarez, F. Hernandez-Sanchez, and M. Torres. International Immunopharmacology, 2016. 31: p. 149-157. PMID[26735610].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26675440">Identification of a Novel Class of Quinoline-Oxadiazole Hybrids as Anti-tuberculosis Agents.</a> Jain, P.P., M.S. Degani, A. Raju, A. Anantram, M. Seervi, S. Sathaye, M. Ray, and M.G.R. Rajan. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(2): p. 645-649. PMID[26675440].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25978136">New Pimarane Diterpenes and Other Antimycobacterial Metabolites from Anisochilus verticillatus.</a> Kulkarni, R.R., K. Shurpali, V.M. Khedkar, V.G. Puranik, D. Sarkar, and S.P. Joshi. Natural Product Research, 2016. 30(6): p. 675-681. PMID[25978136].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368464400006">A Focused Screen Identifies Antifolates with Activity on Mycobacterium tuberculosis.</a> Kumar, A., A. Guardia, G. Colmenarejo, E. Perez, R.R. Gonzalez, P. Torres, D. Calvo, R.M. Gomez, F. Ortega, E. Jimenez, R.C. Gabarro, J. Rullas, L. Ballell, and D.R. Sherman. ACS  Infectious Diseases, 2015. 1(12): p. 604-614. ISI[000368464400006].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368463600002">Inhibiting the beta-Lactamase of Mycobacterium tuberculosis (Mtb) with Novel Boronic acid Transition-state Inhibitors (BATSIs).</a> Kurz, S.G., S. Hazra, C.R. Bethel, C. Romagnoli, E. Caselli, F. Prati, J.S. Blanchard, and R.A. Bonomo. ACS  Infectious Diseases, 2015. 1(6): p. 234-242. ISI[000368463600002].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000369003300028">Convenient Synthesis of Novel Quinazoline Congeners via Copper Catalyzed C-N/C-S Coupling and Their Biological Evaluation.</a> Lakum, H.P., D.R. Shah, and K.H. Chikhalia. Journal of Heterocyclic Chemistry, 2016. 53(1): p. 209-219. ISI[000369003300028].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26751718">Nitroarenes as Antitubercular Agents: Stereoelectronic Modulation to Mitigate Mutagenicity.</a> Landge, S., V. Ramachandran, A. Kumar, J. Neres, K. Murugan, C. Sadler, M.D. Fellows, V. Humnabadkar, P. Vachaspati, A. Raichurkar, S. Sharma, S. Ravishankar, S. Guptha, V.K. Sambandamurthy, T.S. Balganesh, B.G. Ugarkar, V. Balasubramanian, B.S. Bandodkar, and M. Panda. ChemMedChem, 2016. 11(3): p. 331-339. PMID[26751718].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368463400003">Diaza-anthracene Antibiotics from a Freshwater-derived Actinomycete with Selective Antibacterial Activity toward Mycobacterium tuberculosis.</a> Mullowney, M.W., C.H. Hwang, A.G. Newsome, X.W. Wei, U. Tanouye, B.J. Wan, S. Carlson, N.J. Barranis, E.O. hAinmhire, W.L. Chen, K. Krishnamoorthy, J. White, R. Blair, H. Lee, J.E. Burdette, P.K. Rathod, T. Parish, S. Cho, S.G. Franzblau, and B.T. Murphy. ACS  Infectious Diseases, 2015. 1(4): p. 168-174. ISI[000368463400003].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26755393">Synthesis, in Vitro Antimycobacterial Evaluation and Docking Studies of Some New 5,6,7,8-Tetrahydropyrido[4&#39;,3&#39;:4,5]thieno[2,3-d]pyrimidin-4(3H)-one Schiff Bases.</a> Narender, M., S.B. Jaswanth, K. Umasankar, J. Malathi, A. Raghuram Reddy, K.R. Umadevi, A.V. Dusthackeer, K. Venkat Rao, and R.A. Raghuram. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(3): p. 836-840. PMID[26755393].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26306817">Structure and Activity Relationships of the Anti-mycobacterium Antibiotics Resorcinomycin and Pheganomycin.</a> Ogasawara, Y., K. Ooya, M. Fujimori, M. Noike, and T. Dairi. Journal of Antibiotics, 2016. 69(2): p. 119-120. PMID[26306817].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26759836">Rational Design and Synthesis of Substrate-product Analogue Inhibitors of alpha-Methylacyl-coenzyme A Racemase from Mycobacterium tuberculosis.</a> Pal, M., M. Khanal, R. Marko, S. Thirumalairajan, and S.L. Bearne. Chemical Communications, 2016. 52(13): p. 2740-2743. PMID[26759836].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26348876">Design and Development of Mycobacterium tuberculosis Lysine epsilon-Aminotransferase Inhibitors for Latent Tuberculosis Infection.</a> Parthiban, B.D., S. Saxena, M. Chandran, P.S. Jonnalagadda, R. Yadav, R.R. Srilakshmi, Y. Perumal, and S. Dharmarajan. Chemical Biology &amp; Drug Design, 2016. 87(2): p. 265-274. PMID[26348876].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26645944">Synthesis and Bioactivity of Antitubercular Peptides and Peptidomimetics: An Update.</a> Rodriguez, L.M.D., H. Kaur, and M.A. Brimble. Organic &amp; Biomolecular Chemistry, 2016. 14(4): p. 1177-1187. PMID[26645944].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26763748">Synthesis and Anti-tubercular Activity of N(2)-Arylbenzo[g]isoquinoline-5,10-dione-3-iminium bromides.</a> Rotthier, G., D. Cappoen, Q.T. Nguyen, T.A. Dang Thi, V. Mathys, V.T. Nguyen, K. Huygen, L. Maes, P. Cos, and K. Abbaspour Tehrani. Organic &amp; Biomolecular Chemistry, 2016. 14(6): p. 2041-2051. PMID[26763748].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368967800001">Synthesis of New Spirooxindole Derivatives through 1,3-Dipolar Cycloaddition of Azomethine ylides and Their Antitubercular Activity.</a> Rouatbi, F., M. Askri, F. Nana, G. Kirsch, D. Sriram, and P. Yogeeswari. Tetrahedron Letters, 2016. 57(2): p. 163-167. ISI[000368967800001].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26867485">Design, Synthesis and Biological Evaluation of Imidazo[2,1-b]thiazole and Benzo[d]imidazo[2,1-b]thiazole Derivatives as Mycobacterium tuberculosis Pantothenate Synthetase Inhibitors</a>. Samala, G., P.B. Devi, S. Saxena, N. Meda, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry, 2016. 24(6): p. 1298-1307. PMID[26867485].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000369515700073">Novel Benzimidazole-oxadiazole Hybrid Molecules as Promising Antimicrobial Agents.</a> Shruthi, N., B. Poojary, V. Kumar, M.M. Hussain, V.M. Rai, V.R. Pai, M. Bhat, and B.C. Revannasiddappa. RSC Advances, 2016. 6(10): p. 8312-8325. ISI[000369515700073].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000369154600083">Development of an Intracellular Screen for New Compounds Able to Inhibit Mycobacterium tuberculosis Growth in Human Macrophages.</a> Sorrentino, F., R.G. del Rio, X. Zheng, J.P. Matilla, P.T. Gomez, M.M. Hoyos, M.E.P. Herran, A.M. Losana, and Y. Av-Gay. Antimicrobial Agents and Chemotherapy, 2016. 60(1): p. 640-645. ISI[000369154600083].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26762852">Comparative Analyses of the Proteins from Mycobacterium tuberculosis and Human Genomes: Identification of Potential Tuberculosis Drug Targets.</a> Sridhar, S., P. Dash, and K. Guruprasad. Gene, 2016. 579(1): p. 69-74. PMID[26762852].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26725953">Synthesis and Antituberculosis Activity of Indole-pyridine Derived Hydrazides, Hydrazide-hydrazones, and Thiosemicarbazones.</a> Velezheva, V., P. Brennan, P. Ivanov, A. Kornienko, S. Lyubimov, K. Kazarian, B. Nikonenko, K. Majorov, and A. Apt. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(3): p. 978-985. PMID[26725953].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000368464400003">Identification of Novel Anti-mycobacterial Compounds by Screening a Pharmaceutical Small-molecule Library against Nonreplicating Mycobacterium tuberculosis.</a> Warrier, T., M. Martinez-Hoyos, M. Marin-Amieva, G. Colmenarejo, E. Porras-De Francisco, A.I. Alvarez-Pedraglio, M.T. Fraile-Gabaldon, P.A. Torres-Gomez, L. Lopez-Quezada, B. Gold, J. Roberts, Y. Ling, S. Somersan-Karakaya, D. Little, N. Cammack, C. Nathan, and A. Mendoza-Losana. ACS  Infectious Diseases, 2015. 1(12): p. 580-585. ISI[000368464400003].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26525785">In Vitro Activity of beta-Lactams in Combination with beta-Lactamase Inhibitors against Multidrug-resistant Mycobacterium tuberculosis Isolates.</a> Zhang, D., Y.F. Wang, J. Lu, and Y. Pang. Antimicrobial Agents and Chemotherapy, 2016. 60(1): p. 393-399. PMID[26525785]. PMCID[PMC4704149].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_02_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160218&amp;CC=WO&amp;NR=2016024096A1&amp;KC=A1">Preparation of Tricycles as Antibacterial Agents.</a> Ratcliffe, A., I. Cooper, M. Pichowicz, N. Stokes, and C. Charrier. Patent. 2016. 2015-GB52303 2016024096: 143pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_02_2016.</p>

    <br />

    <p class="plaintext">33. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160120&amp;CC=CN&amp;NR=105250270A&amp;KC=A">Application of Cyanogramide in Preparation of anti-TB Drugs.</a> Tian, L. Patent. 2016. 2015-10761826 105250270: 6pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_02_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
