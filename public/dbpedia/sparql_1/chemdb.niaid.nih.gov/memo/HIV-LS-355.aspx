

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-355.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X7ApobPHNuuHTxQBMZiFVrZ1d4bTSc82yVhTFxoKiJaVQhp/GUVEnknDxg5TWx3/2y8SqI1nIElWej5+IT8zwubzlzrb32XwyfIz0npTv5UtA/7nBsjbrsNewTU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="80CCCC5A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-355-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58734   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Design, Synthesis, Biological Evaluation, and Molecular Modeling Studies of TIBO-Like Cyclic Sulfones as Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors</p>

    <p>          Di Santo, R, Costi, R, Artico, M, Ragno, R, Lavecchia, A, Novellino, E, Gavuzzo, E, La Torre, F, Cirilli, R, Cancio, R, and Maga, G</p>

    <p>          ChemMedChem <b>2006</b>.  1(1): 82-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892340&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16892340&amp;dopt=abstract</a> </p><br />

    <p>2.     58735   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Nucleoside analogues with 6-membered carbon ring structure, synthetic method and use in antivirus thereof</p>

    <p>          Lou, Hongxiang, Zhan, Tianrong, Ma, Yudao, and Fan, Peihong</p>

    <p>          PATENT:  CN <b>1803819</b>  ISSUE DATE: 20060719</p>

    <p>          APPLICATION: 1004-2184  PP: 18 pp.</p>

    <p>          ASSIGNEE:  (Shandong University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     58736   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Design and Discovery of a Novel Dipeptidyl-peptidase IV (CD26)-Based Prodrug Approach</p>

    <p>          Garcia-Aparicio, C, Bonache, MC, De, Meester I, San-Felix, A, Balzarini, J, Camarasa, MJ, and Velazquez, S</p>

    <p>          J Med Chem <b>2006</b>.  49(17): 5339-5351</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913724&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913724&amp;dopt=abstract</a> </p><br />

    <p>4.     58737   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of N-[(imidazo[1,2-a]pyridin-2-yl)methyl]-3,4-dihydro-2H-pyrano[3,2-b]pyridin-4-amines that bind to chemokine receptors for use against HIV and other disorders</p>

    <p>          Gudmundsson, Kristjan</p>

    <p>          PATENT:  WO <b>2006076131</b>  ISSUE DATE:  20060720</p>

    <p>          APPLICATION: 2005  PP: 63 pp.</p>

    <p>          ASSIGNEE:  (Smithkline Beecham Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     58738   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Structure-Based Design of Novel HIV-1 Protease Inhibitors To Combat Drug Resistance</p>

    <p>          Ghosh, AK, Sridhar, PR, Leshchenko, S, Hussain, AK, Li, J, Kovalevsky, AY, Walters, DE, Wedekind, JE, Grum-Tokars, V, Das, D, Koh, Y, Maeda, K, Gatanaga, H, Weber, IT, and Mitsuya, H</p>

    <p>          J Med Chem <b>2006</b>.  49(17): 5252-5261</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913714&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16913714&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     58739   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of 2-oxo-3-cyano-1,6a-diaza-tetrahydro-fluoranthenes as anti-HIV agents</p>

    <p>          Kesteleyn, Bart Rudolf Romanie, Raboisson, Pierre Jean-Marie, Van De Vreken, Wim, and Canard, Maxime Francis Jean-Marie Ghislain</p>

    <p>          PATENT:  WO <b>2006072636</b>  ISSUE DATE:  20060713</p>

    <p>          APPLICATION: 2006  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     58740   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Structure-activity relationships in platelet-activating factor. Part 14: Synthesis and biological evaluation of piperazine derivatives with dual anti-PAF and anti-HIV-1 activity</p>

    <p>          Sallem, W, Serradji, N, Dereuddre-Bosquet, N, Dive, G, Clayette, P, and Heymans, F</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908170&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908170&amp;dopt=abstract</a> </p><br />

    <p>8.     58741   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV-1 activity of a novel series of 1,4,2-benzodithiazine-dioxides</p>

    <p>          Brzozowski, Z, Saczewski, F, and Neamati, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908143&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908143&amp;dopt=abstract</a> </p><br />

    <p>9.     58742   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Synthesis and HIV-integrase strand transfer inhibition activity of 7-hydroxy[1,3]thiazolo[5,4-b]pyridin-5(4H)-ones</p>

    <p>          Boros, EE, Johns, BA, Garvey, EP, Koble, CS, and Miller, WH</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908139&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16908139&amp;dopt=abstract</a> </p><br />

    <p>10.   58743   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          2&#39;,3&#39;-Dideoxy-3&#39;-hydroxymethylcytosine or a prodrug for the treatment of HIV</p>

    <p>          Xhou, Xiao-Xiong and Zhang, Hong</p>

    <p>          PATENT:  WO <b>2006070004</b>  ISSUE DATE:  20060706</p>

    <p>          APPLICATION: 2005  PP: 73 pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   58744   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Diketo acids derivatives as integrase inhibitors: the war against the acquired immunodeficiency syndrome</p>

    <p>          Henao-Mejia, Jorge, Goez, Yenny, Patino, Pablo, and Rugeles, Maria T</p>

    <p>          Recent Patents on Anti-Infective Drug Discovery <b>2006</b>.  1(2 ): 255-265</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   58745   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          New, potent P1/P2-morpholinone-based HIV-protease inhibitors</p>

    <p>          Kazmierski, WM, Furfine, E, Spaltenstein, A, and Wright, LL</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16904316&amp;dopt=abstract</a> </p><br />

    <p>13.   58746   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p><b>          Simple synthesis of novel acyclic (e)-bromovinyl nucleosides as potential antiviral agents</b> </p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(8): 879-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901820&amp;dopt=abstract</a> </p><br />

    <p>14.   58747   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of aminium salts of 1,2,3-triazoles as prodrugs of drugs including antiviral agents</p>

    <p>          Kadow, John F and Regueiro-Ren, Alicia</p>

    <p>          PATENT:  US <b>2006142298</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005-38902  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   58748   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          8-Azabicyclo[3.2.1]octane derivatives with an activity on chemokine CCR5 receptors and their preparation, pharmaceutical compositions and use for treatment of inflammatory diseases and infection caused by HIV and genetically related retroviruses</p>

    <p>          Pryde, David Cameron and Stupple, Paul Anthony</p>

    <p>          PATENT:  WO <b>2006067584</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 67 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58749   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Towards discovering dual functional inhibitors against both wild type and K103N mutant HIV-1 reverse transcriptases: molecular docking and QSAR studies on 4,1-benzoxazepinone analogues</p>

    <p>          Zhang, Z, Zheng, M, Du, L, Shen, J, Luo, X, Zhu, W, and Jiang, H</p>

    <p>          J Comput Aided Mol Des <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16897578&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16897578&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   58750   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Structure-activity relationships in platelet-activating factor. Part 13: Synthesis and biological evaluation of piperazine derivatives with dual anti-PAF and anti-HIV-1 or pure antiretroviral activity</p>

    <p>          Serradji, N, Bensaid, O, Martin, M, Sallem, W, Dereuddre-Bosquet, N, Benmehdi, H, Redeuilh, C, Lamouri, A, Dive, G, Clayette, P, and Heymans, F</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16893650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16893650&amp;dopt=abstract</a> </p><br />

    <p>18.   58751   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Application of 2-(n-methyl-guanidino)-ethanesulfonic acid in anti-hiv virus</p>

    <p>          Jin, Yan, Zheng, Yongtang, Wang, Jianhua, and Zhang, Wei</p>

    <p>          PATENT:  CN <b>1785168</b>  ISSUE DATE: 20060614</p>

    <p>          APPLICATION: 1008-2873  PP: 5 pp.</p>

    <p>          ASSIGNEE:  (Dalian Institute of Chemical Physics, Chinese Academy of Sciences Peop. Rep. China and Kunming Institute of Zoology, Chinese Academy of Sciences</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   58752   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Structure-activity relationship study of flavone compounds with anti-HIV-1 integrase activity: A density functional theory study</p>

    <p>          Lameira, J, Medeiros, IG, Reis, M, Santos, AS, and Alves, CN</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16890444&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16890444&amp;dopt=abstract</a> </p><br />

    <p>20.   58753   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Modified azidothymidine 5&#39;-phosphonate for potential antiviral preparations</p>

    <p>          Kukhanova, Marina Konstantinovna, Khankajinskaya, Anastasiya Lvovna, Yasko, Maxim Vladimirovich, Shirokova, Elena Anatolievna, Shipitsyn, Alexander Valerievich, and Pokrovsky, Andrey Georgievich</p>

    <p>          PATENT:  WO <b>2006062434</b>  ISSUE DATE:  20060615</p>

    <p>          APPLICATION: 2005  PP: 19 pp.</p>

    <p>          ASSIGNEE:  (Russia)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   58754   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Antiviral activity of Arthrospira-derived spirulan-like substances</p>

    <p>          Rechter, S, Konig, T, Auerochs, S, Thulke, S, Walter, H, Dornenburg, H, Walter, C, and Marschall, M</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884788&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   58755   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 replication by amphotericin B methyl ester: selection for resistant variants</p>

    <p>          Waheed, AA, Ablan, SD, Mankowski, MK, Cummins, JE, Ptak, RG, Schaffner, CP, and Freed, EO</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16882663&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16882663&amp;dopt=abstract</a> </p><br />

    <p>23.   58756   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 integrase activity by synthetic peptides derived from the HIV-1 HXB2 Pol region of the viral genome</p>

    <p>          Zawahir, Z and Neamati, N</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16879966&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16879966&amp;dopt=abstract</a> </p><br />

    <p>24.   58757   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis and antiviral evaluation of novel pyrimidine thioapionucleosides</p>

    <p>          Lee, Rae Sang, Hong, Joon Hee, and Ko, Ok Hyun</p>

    <p>          Yakhak Hoechi <b>2006</b>.  50(2): 65-69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>25.   58758   HIV-LS-355; PUBMED-HIV-8/21/2006</p>

    <p class="memofmt1-2">          Contribution of Vpu, Env, and Nef to CD4 down-modulation and resistance of human immunodeficiency virus type 1-infected T cells to superinfection</p>

    <p>          Wildum, S, Schindler, M, Munch, J, and Kirchhoff, F</p>

    <p>          J Virol <b>2006</b>.  80(16): 8047-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16873261&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16873261&amp;dopt=abstract</a> </p><br />

    <p>26.   58759   HIV-LS-355; SCIFINDER-HIV-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of HIV inhibiting bicyclic pyrimidine derivatives</p>

    <p>          Janssen, Paul Adriaan Jan, Guillemont, Jerome Emile Georges, Paugam, Mikaeel, Delest, Bruno Francois Marie, Heeres, Jan, and Lewi, Paulus Joannes</p>

    <p>          PATENT:  WO <b>2006045828</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Tibotec Pharmaceuticals Ltd., Ire. and Arts, Frank Xavier Jozef Herwig</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   58760   HIV-LS-355; WOS-HIV-8/20/2006</p>

    <p><b>          cis- and trans-decahydro-1,6-naphthyridines. Stereoselective synthesis and stereochemistry</b> </p>

    <p>          Esipova, TV, Borisenko, AA, and Grishina, GV</p>

    <p>          RUSSIAN JOURNAL OF ORGANIC CHEMISTRY <b>2006</b>.  42(6): 922-928, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239375600019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239375600019</a> </p><br />
    <br clear="all">

    <p>28.   58761   HIV-LS-355; WOS-HIV-8/20/2006</p>

    <p class="memofmt1-2">          Chemokine receptor CCR-5 inhibitors produced by Chaetomium globosum</p>

    <p>          Yang, SW, Mierzwa, R, Terracciano, J, Patel, M, Gullo, V, Wagner, N, Baroudy, B, Puar, M, Chan, TM, McPhail, AT, and Chu, M</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2006</b>.  69(7): 1025-1028, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239336000010</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
