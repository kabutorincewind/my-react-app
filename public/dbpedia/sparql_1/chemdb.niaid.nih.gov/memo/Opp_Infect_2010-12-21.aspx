

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-12-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9pBynbdodnqollssBLyveJ01pkXjHyB46B3fxp7NcJsS3x5eJ+22P3wPuhflXAsV8YJ/mKDkVnTi4W6jKiL0xs3cmVTE2i7qF9UrceWW3gORj+uXVH35sl6yNWc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1BA927B4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">December 10 - December 21, 2010</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21141979">Antifungal Activity of Amphotericin B Conjugated to Carbon Nanotubes.</a> Benincasa, M., S. Pacor, W. Wu, M. Prato, A. Bianco, and R. Gennaro. American Chemical Society Nano, 2010. <b>[Epub ahead of print]</b>; PMID[21141979].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21149624">In vitro activity and in vivo efficacy of anidulafungin in murine infections by Aspergillus flavus.</a> Calvo, E., F.J. Pastor, E. Mayayo, V. Salas, and J. Guarro. Antimicrobial Agents and Chemotherapy, 2010. <b>[Epub ahead of print]</b>; PMID[21149624].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21163626">Antimicrobial efficacy of silver ions in combination with tea tree oil against Pseudomonas aeruginosa, Staphylococcus aureus and Candida albicans.</a> Low, W.L., C. Martin, D.J. Hill, and M.A. Kenward. International Journal of Antimicrobial Agents, 2010. <b>[Epub ahead of print]</b>; PMID[21163626].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21158423">N-Hydroxypyridones, Phenylhydrazones, and a Quinazolinone from Isaria farinosa.</a> Ma, C., Y. Li, S. Niu, H. Zhang, X. Liu, and Y. Che. Journal of Natural Products, 2010. <b>[Epub ahead of print</b>]; PMID[21158423].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21159940">Wild-type MIC distributions and epidemiological cutoff values (ECVs) for posaconazole and voriconazole and Candida spp. as determined by 24-h CLSI broth microdilution.</a> Pfaller, M.A., L. Boyken, R.J. Hollis, J. Kroeger, S.A. Messer, S. Tendolkar, and D.J. Diekema. Journal of Clinical Microbiology, 2010. <b>[Epub ahead of print]</b>; PMID[21159940].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21168911">Synthetic cationic amphiphilic alpha-helical peptides as antimicrobial agents.</a> Wiradharma, N., U. Khoe, C.A. Hauser, S.V. Seow, S. Zhang, and Y.Y. Yang. Biomaterials, 2010. <b>[Epub ahead of print]</b>; PMID[21168911].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21167915">Purification, biochemical characterization and antifungal activity of a new lipid transfer protein (LTP) from Coffea canephora seeds with alpha-amylase inhibitor properties.</a> Zottich, U., M. Da Cunha, A.O. Carvalho, G.B. Dias, N.C. Silva, I.S. Santos, V.V. do Nacimento, E.C. Miguel, O.L. Machado, and V.M. Gomes. Biochimica et Biophysica Acta, 2010. <b>[Epub ahead of print]</b>; PMID[21167915].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21071220">A facile 1,3-dipolar cycloaddition of azomethine ylides to 2-arylidene-1,3-indanediones: synthesis of dispiro-oxindolylpyrrolothiazoles and their antimycobacterial evaluation.</a> Maheswari, S.U., K. Balamurugan, S. Perumal, P. Yogeeswari, and D. Sriram. Bioorganic and Medicinal Chemistry Letters, 2010. 20(24): p. 7278-7282; PMID[21071220].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20927480">Synthesis and structure of azole-fused indeno[2,1-c]quinolines and their anti-mycobacterial properties.</a> Upadhayaya, R.S., P.D. Shinde, A.Y. Sayyed, S.A. Kadam, A.N. Bawane, A. Poddar, O. Plashkevych, A. Foldesi, and J. Chattopadhyaya. Organic &amp; Biomolecular Chemistry, 2010. 8(24): p. 5661-5673; PMID[20927480].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="memofmt2-3">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284617200007">Preliminary phytochemical and antimicrobial investigations of leaf extracts of Ochna schweinfurthiana (Ochnaceae).</a> Abdullahi, M.I., I. Iliya, A.K. Haruna, M.I. Sule, A.M. Musa, and M.S. Abdullahi. African Journal of Pharmacy and Pharmacology, 2010. 4(2): p. 83-86; ISI[000284617200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284520500008">A regio- and stereo-controlled approach to triazoloquinoxalinyl C-nucleosides.</a> Amer, A., M.S. Ayoup, S.N. Khattab, S.Y. Hassan, V. Langer, S. Senior, and A.M. El Massry. Carbohydrate Research, 2010. 345(17): p. 2474-2484; ISI[000284520500008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284481400007">Antimicrobial activity of Rhaponticum acaule and Scorzonera undulata growing wild in Tunisia.</a> Ben Abdelkader, H., K.B.H. Salah, K. Liouane, O. Boussaada, K. Gafsi, M.A. Mahjoub, M. Aouni, A.N. Hellal, and Z. Mighri. African Journal of Microbiology Research, 2010. 4(19): p. 1954-1958; ISI[000284481400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282986000012">Quality assessment and antimicrobial activity of various honey types of Pakistan.</a> Gulfraz, M., F. Iftikhar, S. Raja, S. Asif, S. Mehmood, Z. Anwar, and G. Kaukob. African Journal of Biotechnology, 2010. 9(41): p. 6902-6906; ISI[000282986000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284281300023">Antimicrobial and antioxidant activities of the plant Heliotropium strigosum.</a> Hussain, S., M. Jamil, F. Ullah, A. Khan, M. Arfan, S. Ahmad, and L. Khatoon. African Journal of Biotechnology, 2010. 9(45): p. 7738-7743; ISI[000284281300023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284474400013">Synthesis, characterization, potentiometry, and antimicrobial studies of transition metal complexes of a tridentate ligand.</a> Jadhav, S.M., V.A. Shelke, A.S. Munde, S.G. Shankarwar, V.R. Patharkar, and T.K. Chondhekar. Journal of Coordination Chemistry, 2010. 63(23): p. 4153-4164; ISI[000284474400013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284582100014">Farnesol, a Potential Efflux Pump Inhibitor in Mycobacterium smegmatis.</a> Jin, J., J.Y. Zhang, N. Guo, H. Sheng, L. Li, J.C. Liang, X.L. Wang, Y. Li, M.Y. Liu, X.P. Wu, and L. Yu. Molecules, 2010. 15(11): p. 7750-7762; ISI[000284582100014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284738400021">Synthesis and Structure-Activity Relationships of Aza- and Diazabiphenyl Analogues of the Antitubercular Drug (6S)-2-Nitro-6-{ 4-(trifluoromethoxy)benzyl oxy}-6,7-dihydro-5H-imidazo 2,1-b 1,3 oxazine (PA-824).</a> Kmentova, I., H.S. Sutherland, B.D. Palmer, A. Blaser, S.G. Franzblau, B.J. Wan, Y.H. Wang, Z.K. Ma, W.A. Denny, and A.M. Thompson. Journal of Medicinal Chemistry, 2010. 53(23): p. 8421-8439; ISI[000284738400021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284247000029">Antifungal Activity and Chemical Composition of the Essential Oils of Lippia alba (Miller) N.E Brown Grown in Different Regions of Colombia.</a> Mesa-Arango, A.C., L. Betancur-Galvis, J. Montiel, J.G. Bueno, A. Baena, D.C. Duran, J.R. Martinez, and E.E. Stashenko. Journal of Essential Oil Research, 2010. 22(6): p. 568-574; ISI[000284247000029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284664500019">Facile three-component domino reactions in the regioselective synthesis and antimycobacterial evaluation of novel indolizines and pyrrolo 2,1-a isoquinolines.</a> Muthusaravanan, S., S. Perumal, P. Yogeeswari, and D. Sriram. Tetrahedron Letters, 2010. 51(49): p. 6439-6443; ISI[000284664500019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000282052200004">Phytochemical and antimicrobial screening of the aqueous extract of Cassia arereh Del. stem-bark.</a> Ngulde, S.I., S. Sanni, U.K. Sandabe, and D. Sani. African Journal of Pharmacy and Pharmacology, 2010. 4(8): p. 530-534; ISI[000282052200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284716900009">Antimycobacterial and antimicrobial study of new 1,2,4-triazoles with benzothiazoles.</a> Patel, N.B., I.H. Khan, and S.D. Rajani. Archiv Der Pharmazie, 2010. 343(11-12): p. 692-699; ISI[000284716900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284877400001">Liquid and vapour-phase antifungal activities of selected essential oils against candida albicans: microscopic observations and chemical characterization of cymbopogon citratus.</a> Tyagi, A.K. and A. Malik. Bmc Complementary and Alternative Medicine, 2010. 10; ISI[000284877400001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000284281300019">Antibacterial activity of water-phase extracts from bamboo shavings against food spoilage microorganisms.</a> Zhang, J.Y., J.Y. Gong, Y.T. Ding, B.Y. Lu, X.Q. Wu, and Y. Zhang. African Journal of Biotechnology, 2010. 9(45): p. 7710-7717; ISI[000284281300019.</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1209-122110.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
