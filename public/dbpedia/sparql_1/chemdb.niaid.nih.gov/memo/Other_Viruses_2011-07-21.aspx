

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-07-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4cSE26etO/bD2HawYKfrApoh/Y5UfSm68uc5N5b0a9qqH66Os+7N6TiAVff0u5GafhOLfOkj43wy7awaxQnv2jLF/TMpaQW5u4kGYEI0mBacjUt6Z2I5FWMSAyM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA3DBC7C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">July 8 - July 21, 2011</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p> </p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21745500">Inhibitory Effects of Tricin Derivative from Sasa albo-marginata on Replication of Human Cytomegalovirus</a>. Akuzawa, K., R. Yamada, Z. Li, Y. Li, H. Sadanari, K. Matsubara, K. Watanabe, M. Koketsu, Y. Tuchida, and T. Murayama. Antiviral research, 2011. <b>[Epub ahead of print]</b>; PMID[21745500].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21738555">Ethanol Extract from Ampelopsis sinica Root Exerts Anti-Hepatitis B Virus Activity via Inhibition of p53 Pathway In Vitro.</a> Pang, R., J.Y. Tao, S.L. Zhang, K.L. Chen, L. Zhao, X. Yue, Y.F. Wang, P. Ye, Y. Zhu, and J.G. Wu. Evidence-Based Complementary and Alternative Medicine : eCAM, 2011. 2011: p. 939205; PMID[21738555].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21757347">Synthesis, Structure-Activity Relationship and Biological Evaluation of Novel N-substituted matrinic acid Derivatives as Host Heat-stress Cognate 70 (Hsc70) Down-regulators.</a> Du, N.N., X. Li, Y.P. Wang, F. Liu, Y.X. Liu, C.X. Li, Z.G. Peng, L.M. Gao, J.D. Jiang, and D.Q. Song. Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>; PMID[21757347].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21766884">Oleanane-Type Triterpenoids from the Leaves and Twigs of Fatsia polycarpa.</a> Cheng, S.Y., C.M. Wang, Y.M. Hsu, T.J. Huang, S.C. Chou, E.H. Lin, and C.H. Chou. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21766884].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="desc"> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21761853">Quinoxalin-2(1H)-One Derivatives as Inhibitors Against Hepatitis C Virus.</a> Liu, G. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21761853].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21762538">Glycyrrhizin as Antiviral Agent against Hepatitis C Virus.</a> Ashfaq, U.A., M.S. Masoud, Z. Nawaz, and S. Riazuddin. Journal of Translational Medicine, 2011. 9(1): p. 112; PMID[21762538].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Herpes Simplex virus</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21772234">Benzylidene-bis-(4-Hydroxycoumarin) and Benzopyrano-Coumarin Derivatives: Synthesis, 1H/13C-NMR Conformational and X-ray Crystal Structure Studies and In vitro Antiviral Activity Evaluations.</a> Zavrsnik, D., S. Muratovic, D. Makuc, J. Plavec, M. Cetina, A. Nagl, E.D. Clercq, J. Balzarini, and M. Mintas. Molecules (Basel, Switzerland), 2011. 16(7): p. 6023-40; PMID[21772234]. <b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21745746">Acyclic Nucleoside Phosphonates with a Branched 2-(2-Phosphonoethoxy)ethyl Chain: Efficient Synthesis and Antiviral Activity</a>. Hockova, D., A. Holy, G. Andrei, R. Snoeck, and J. Balzarini. Bioorganic &amp; Medicinal Chemistry, 2011. 19(15): p. 4445-53; PMID[21745746].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21773062">Synthesis and Biological Evaluation of New 3-Phenyl-1-[(4-arylpiperazin-1-yl)alkyl]-piperidine-2,6-diones.</a> Bielenica, A., J. Kossakowski, M. Struga, I. Dybala, R. Loddo, C. Ibba, and P. La Colla. Scientia Pharmaceutica, 2011. 79(2): p. 225-238; PMID[21773062].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763352">Antiherpes Activity of Glucoevatromonoside, a Cardenolide Isolated from a Brazilian Cultivar of Digitalis lanata.</a> Bertol, J.W., C. Rigotto, R.M. de Padua, W. Kreis, C.R. Barardi, F.C. Braga, and C.M. Simoes. Antiviral Research, 2011. <b>[Epub ahead of print]</b>; PMID[21763352].</p>

    <p class="NoSpacing"><b>[PubMed]</b>. OV_0708-072111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.V.</p>

    <p class="memofmt2-2"> </p>

    <p class="NoSpacing">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Inhibition of Hepatitis B Virus (HBV) by LNA-mediated Nuclear Interference with HBV DNA Transcription</a>. Sun, Z., W.Q. Xiang, Y.J. Guo, Z. Chen, W. Liu, and D.R. Lu. Biochemical and Biophysical Research Communications, 2011. 409(3): p. 430-435; ISI[000292059500013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Design and Optimization of a Series of Potent and Novel HCV NS3 Protease Inhibitors Leading to the Discovery of BMS-650032</a>. Sun, L.Q., A.X. Wang, J. Chen, S.Y. Sit, Y. Chen, P. Hewawasam, Y. Tu, M. Ding, S.V. D&#39;Andrea, Z.Z. Zheng, N. Sin, B.L. Venables, A. Cocuzza, D. Bilder, D. Carini, B. Johnson, F. Yu, D. Hernandez, G.Z. Zhai, A. Sheaffer, D. Barry, H. Mulherin, M. Lee, J. Friborg, S. Levine, C.Q. Chen, J.O. Knipe, K. Mosure, A. Good, H. Klei, R. Rajamani, Y.Z. Shu, T. Phillip, V.K. Arora, J. Loy, S. Adams, R. Schartman, M. Browning, P.C. Levesque, D. Li, J.L. Zhu, H.B. Sun, G. Pilcher, D. Bounous, R.W. Lange, C. Pasquinelli, T. Eley, R. Colonno, N.A. Meanwell, F. McPhee, and P.M. Scola. Abstracts of Papers of the American Chemical Society, 2011. 241; ISI[000291982806072].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Inhibition of hepatitis C virus replication by herbal extract: Phyllanthus amarus as potent natural source</a>. Ravikumar, Y.S., U. Ray, M. Nandhitha, A. Perween, H.R. Naika, N. Khanna, and S. Das. Virus Research, 2011. 158(1-2): p. 89-97; ISI[000292077300012].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Down-regulation of IRES Containing 5 &#39; UTR of HCV Genotype 3a using siRNAs</a>. Khaliq, S., S. Jahan, A. Pervaiz, U.A. Ashfaq, and S. Hassan. Virology Journal, 2011. 8; ISI[000291884100001].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0708-072111.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Ale">Synthesis and SAR around the R6 Position of 2(1H)-Pyrazinone Based HCV NS3 Protease Inhibitors</a>. Benfrage, A.K., J. Gising, P. Ortqvist, H. Alogheli, A. Ehrenberg, M. Larhed, U.H. Danielson, and A. Sandstrom. Biopolymers, 2011. 96(4): p. 457; ISI[000291763400195].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OV_0708-072111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
