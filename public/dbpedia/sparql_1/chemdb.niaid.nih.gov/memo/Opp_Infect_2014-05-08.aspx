

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-05-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4oq7YuW17+lIdJ6/Vk7/bQBvZT9SKkMGwtZJDbYiaep4KrWP94082MMA29QaoMucGehLVUIQw7uUtOkB2P2H0KOd0xVON8w2EUOZk8uPHjKTPsplbuQSGNUC+kQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C9CB3E5F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 25 - May 8, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24603122">Volatile Compounds of Salvadora persica Inhibit the Growth of Oral Candida Species.</a> Alili, N., J.C. Turp, E.M. Kulik, and T. Waltimo. Archives of Oral Biology, 2014. 59(5): p. 441-447. PMID[24603122].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24287625">Antimicrobial Drugs Encapsulated in Fibrin Nanoparticles for Treating Microbial Infested Wounds.</a> Alphonsa, B.M., P.T. Sudheesh Kumar, G. Praveen, R. Biswas, K.P. Chennazhi, and R. Jayakumar. Pharmaceutical Research, 2014. 31(5): p. 1338-1351. PMID[24287625].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24781056">Inhibitors of the Glyoxylate Cycle Enzyme ICL1 in Candida albicans for Potential Use as Antifungal Agents.</a> Cheah, H.L., V. Lim, and D. Sandai. Plos One, 2014. 9(4): p. e95951. PMID[24781056].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24523158">A Novel Nerolidol-rich Essential Oil from Piper claussenianum Modulates Candida albicans Biofilm.</a> Curvelo, J.A., A.M. Marques, A.L. Barreto, M.T. Romanos, M.B. Portela, M.A. Kaplan, and R.M. Soares. Journal of Medical Microbiology, 2014. 63(Pt 5): p. 697-702. PMID[24523158].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24514090">Synthesis and Antifungal Activity in Vitro of Isoniazid Derivatives against Histoplasma capsulatum var. Capsulatum.</a> de Aguiar Cordeiro, R., F.J. de Farias Marques, R. de Aguiar Cordeiro, M.R. da Silva, A. Donato Maia Malaquias, C.V. Silva de Melo, J. Mafezoli, C. Ferreira de Oliveira Mda, R.S. Nogueira Brilhante, M.F. Gadelha Rocha, J. Pinheiro Gomes Bandeira Tde, and J.J. Costa Sidrim. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2504-2511. PMID[24514090].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24566179">Plant-derived Decapeptide OSIP108 Interferes with Candida albicans Biofilm Formation without Affecting Cell Viability.</a>Delattin, N., K. De Brucker, D.J. Craik, O. Cheneval, M. Frohlich, M. Veber, L. Girandon, T.R. Davis, A.E. Weeks, C.A. Kumamoto, P. Cos, T. Coenye, B. De Coninck, B.P. Cammue, and K. Thevissen. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2647-2656. PMID[24566179].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24716724">Synthesis, Antimicrobial Evaluation, and Structure-Activity Relationship of alpha-Pinene Derivatives.</a> Dhar, P., P. Chan, D.T. Cohen, F. Khawam, S. Gibbons, T. Snyder-Leiby, E. Dickstein, P.K. Rai, and G. Watal. Journal of Agricultural and Food Chemistry, 2014. 62(16): p. 3548-3552. PMID[24716724].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24625282">Fumigatosides A-D, Four New Glucosidated pyrazinoquinazoline indole Alkaloids from a Jellyfish-derived Fungus Aspergillus fumigatus.</a> Liu, J., X. Wei, E.L. Kim, X. Lin, X.W. Yang, X. Zhou, B. Yang, J.H. Jung, and Y. Liu. Organic Letters, 2014. 16(9): p. 2574. PMID[24625282].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24503221">Combination of Fluconazole with Non-antifungal Agents: A Promising Approach to Cope with Resistant Candida albicans Infections and Insight into New Antifungal Agent Discovery.</a> Liu, S., Y. Hou, X. Chen, Y. Gao, H. Li, and S. Sun. International Journal of Antimicrobial Agents, 2014. 43(5): p. 395-402. PMID[24503221].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24566186">Activities of Fluconazole, Caspofungin, Anidulafungin, and Amphotericin B on Planktonic and Biofilm Candida Species Determined by Microcalorimetry.</a> Maiolo, E.M., U. Furustrand Tafin, O. Borens, and A. Trampuz. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2709-2717. PMID[24566186].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24681068">Synthesis, Structure-Activity Relationships, and in Vitro Antibacterial and Antifungal Activity Evaluations of Novel Pyrazole Carboxylic and Dicarboxylic acid Derivatives.</a> Mert, S., R. Kasimogullari, T. Ica, F. Colak, A. Altun, and S. Ok. European Journal of Medicinal Chemistry, 2014. 78: p. 86-96. PMID[24681068].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24685541">Environmentally Benign Synthesis and Antimicrobial Study of Novel Chalcogenophosphates.</a> Mitra, S., S. Mukherjee, S.K. Sen, and A. Hajra. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(9): p. 2198-2201. PMID[24685541].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24623107">Lipopeptides from Bacillus Strain AR2 Inhibits Biofilm Formation by Candida albicans.</a> Rautela, R., A.K. Singh, A. Shukla, and S.S. Cameotra. Antonie van Leeuwenhoek International Journal of General and Molecular Microbiology, 2014. 105(5): p. 809-821. PMID[24623107].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">14.<a href="http://www.ncbi.nlm.nih.gov/pubmed/24548812">Synthesis, Spectral Characterization and Antimicrobial Studies of Nano-sized Oxovanadium(IV) Complexes with Schiff Bases Derived from 5-(Phenyl/Substituted Phenyl)-2-hydrazino-1,3,4-thiadiazole and Indoline-2,3-dione.</a> Sahani, M.K., U. Yadava, O.P. Pandey, and S.K. Sengupta. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 125: p. 189-194. PMID[24548812].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24648138">Highly Potential Antifungal Activity of Quantum-sized Silver Nanoparticles against Candida albicans.</a> Selvaraj, M., P. Pandurangan, N. Ramasami, S.B. Rajendran, S.N. Sangilimuthu, and P. Perumal. Applied Biochemistry and Biotechnology, 2014. 173(1): p. 55-66. PMID[24648138].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24686017">Searching for New Derivatives of Neocryptolepine: Synthesis, Antiproliferative, Antimicrobial and Antifungal Activities.</a>Sidoryk, K., A. Jaromin, J.A. Edward, M. Switalska, J. Stefanska, P. Cmoch, J. Zagrodzka, W. Szczepek, W. Peczynska-Czoch, J. Wietrzyk, A. Kozubek, R. Zarnowski, D.R. Andes, and L. Kaczmarek. European Journal of Medicinal Chemistry, 2014. 78: p. 304-313. PMID[24686017].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24272547">Antifungal Properties of the Human Metschnikowia Strain IHEM 25107.</a> Sisti, M. and V. Savini. Folia Microbiologica, 2014. 59(3): p. 263-266. PMID[24272547].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24550336">Mechanism of Action of 5-Nitrothiophenes against Mycobacterium tuberculosis.</a> Hartkoorn, R.C., O.B. Ryabova, L.R. Chiarelli, G. Riccardi, V. Makarov, and S.T. Cole. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2944-2947. PMID[24550336].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24597756">Peniphenones A-D from the Mangrove Fungus Penicillium dipodomyicola HN4-3A as Inhibitors of Mycobacterium tuberculosis Phosphatase MptpB.</a> Li, H., J. Jiang, Z. Liu, S. Lin, G. Xia, X. Xia, B. Ding, L. He, Y. Lu, and Z. She. Journal of Natural Products, 2014. 77(4): p. 800-806. PMID[24597756].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24576406">Antimycobacterial, Anti-inflammatory and Genotoxicity Evaluation of Plants Used for the Treatment of Tuberculosis and Related Symptoms in South Africa.</a> Madikizela, B., A.R. Ndhlala, J.F. Finnie, and J. Van Staden. Journal of Ethnopharmacology, 2014. 153(2): p. 386-391. PMID[24576406].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24514091">Moxifloxacin Retains Antimycobacterial Activity in the Presence of GyrA Mutations.</a> McGrath, M., N.C. Gey van Pittius, F.A. Sirgel, P.D. Van Helden, and R.M. Warren. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2912-2915. PMID[24514091].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24690527">ProTides of N-(3-(5-(2&#39;-Deoxyuridine))prop-2-ynyl)octanamide as Potential Anti-tubercular and Anti-viral Agents.</a> McGuigan, C., M. Derudas, B. Gonczy, K. Hinsinger, S. Kandil, F. Pertusati, M. Serpi, R. Snoeck, G. Andrei, J. Balzarini, T.D. McHugh, A. Maitra, E. Akorli, D. Evangelopoulos, and S. Bhakta. Bioorganic &amp; Medicinal Chemistry, 2014. 22(9): p. 2816-2824. PMID[24690527].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24726304">Design, Synthesis and Evaluation of Novel 2,5,6-Trisubstituted Benzimidazoles Targeting FtsZ as Antitubercular Agents.</a>Park, B., D. Awasthi, S.R. Chowdhury, E.H. Melief, K. Kumar, S.E. Knudson, R.A. Slayden, and I. Ojima. Bioorganic &amp; Medicinal Chemistry, 2014. 22(9): p. 2602-2612. PMID[24726304].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24713182">Activity of 4,5-Dihydro-1H-pyrazoles against Mycobacterium tuberculosis and Nontuberculous Mycobacteria.</a>Ramos, D.F., G. Fiss, C.P. Frizzo, M.A. Martins, H.G. Bonacorso, N. Zanatta, and P.E. Almeida da Silva. International Journal of Antimicrobial Agents, 2014. 43(5): p. 481-483. PMID[24713182].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24561382">In Vivo Antimalarial Activities of Enantia polycarpa Stem Bark against Plasmodium berghei berghei in Mice.</a> Anosa, G.N., R.I. Udegbunam, J.O. Okoro, and O.N. Okoroafor. Journal of Ethnopharmacology, 2014. 153(2): p. 531-534. PMID[24561382].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24673206">Antiparasitic Chaiyaphumines from Entomopathogenic Xenorhabdus sp. PB61.4.</a> Grundmann, F., M. Kaiser, M. Schiell, A. Batzer, M. Kurz, A. Thanwisai, N. Chantratita, and H.B. Bode. Journal of Natural Products, 2014. 77(4): p. 779-783. PMID[24673206].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24681985">Synthesis, Characterization and Pharmacological Screening of Some Novel 5-Imidazopyrazole Incorporated Polyhydroquinoline Derivatives.</a> Kalaria, P.N., S.P. Satasia, and D.K. Raval. European Journal of Medicinal Chemistry, 2014. 78: p. 207-216. PMID[24681985].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24686013">2-Aryl-3H-indol-3-ones: Synthesis, Electrochemical Behaviour and Antiplasmodial Activities.</a> Najahi, E., A. Valentin, P.L. Fabre, K. Reybier, and F. Nepveu. European Journal of Medicinal Chemistry, 2014. 78: p. 269-274. PMID[24686013].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24673163">Bis-Alkylamine indolo[3,2-b]quinolines as Hemozoin Ligands: Implications for Antimalarial Cytostatic and Cytocidal Activities.</a> Paulo, A., M. Figueiras, M. Machado, C. Charneira, J. Lavrado, S.A. Santos, D. Lopes, J. Gut, P.J. Rosenthal, F. Nogueira, and R. Moreira. Journal of Medicinal Chemistry, 2014. 57(8): p. 3295-3313. PMID[24673163].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24699367">Synthetic Indole and Melatonin Derivatives Exhibit Antimalarial Activity on the Cell Cycle of the Human Malaria Parasite Plasmodium falciparum.</a> Schuck, D.C., A.K. Jordao, M. Nakabashi, A.C. Cunha, V.F. Ferreira, and C.R. Garcia. European Journal of Medicinal Chemistry, 2014. 78: p. 375-382. PMID[24699367].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24550329">Discovery of Compounds Blocking the Proliferation of Toxoplasma gondii and Plasmodium falciparum in a Chemical Space based on Piperidinyl-benzimidazolone Analogs.</a> Saidani, N., C.Y. Botte, M. Deligny, A.L. Bonneau, J. Reader, R. Lasselin, G. Merer, A. Niepceron, F. Brossier, J.C. Cintrat, B. Rousseau, L.M. Birkholtz, M.F. Cesbron-Delauw, J.F. Dubremetz, C. Mercier, H. Vial, R. Lopez, and E. Marechal. Antimicrobial Agents and Chemotherapy, 2014. 58(5): p. 2586-2597. PMID[24550329].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0425-050814.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333545200001">Inhibition of Conidiophore Development in Aspergillus fumigatus by an Escherichia coli DH5 Alpha Strain, a Promising Antifungal Candidate against Aspergillosis.</a> Balhara, M., S. Ruhil, M. Kumar, S. Dhankhar, and A.K. Chhillar. Journal de Mycologie Medicale, 2014. 24(1): p. 1-12. ISI[000333545200001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333047500017">Phenolic Compounds and Antioxidant, Antimicrobial and Antimycobacterial Activities of Serjania erecta Radlk. (Sapindaceae).</a> Cardoso, C.A.L., R.G. Coelho, N.K. Honda, A. Pott, F.R. Pavan, and C.Q.F. Leite. Brazilian Journal of Pharmaceutical Sciences, 2013. 49(4): p. 775-782. ISI[000333047500017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333575600015">Antimicrobial Activity of Honeys from Two Stingless Honeybee Species and Apis mellifera (Hymenoptera: Apidae) against Pathogenic Microorganisms.</a> da Cruz, C.B.N., F.A. Pieri, G.A. Carvalho-Zilse, P.P. Orlandi, C.G. Nunes-Silva, and L. Leomil. Acta Amazonica, 2014. 44(2): p. 287-290. ISI[000333575600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333605600021">Multilaboratory Study of Epidemiological Cutoff Values for Detection of Resistance in Eight Candida Species to Fluconazole, Posaconazole, and Voriconazole.</a> Espinel-Ingroff, A., M.A. Pfaller, B. Bustamante, E. Canton, A. Fothergill, J. Fuller, G.M. Gonzalez, C. Lass-Florl, S.R. Lockhart, E. Martin-Mazuelos, J.F. Meis, M.S.C. Melhem, L. Ostrosky-Zeichner, T. Pelaez, M.W. Szeszs, G. St-Germain, L.X. Bonefietti, J. Guarro, and J. Turnidge. Antimicrobial Agents and Chemotherapy, 2014. 58(4): p. 2006-2012. ISI[000333605600021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333620600022">Isolation and Characterization of Soil Streptomyces Species as Potential Biological Control Agents against Fungal Plant Pathogens.</a>Evangelista-Martinez, Z. World Journal of Microbiology &amp; Biotechnology, 2014. 30(5): p. 1639-1647. ISI[000333620600022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333525500001">Gold Complexes as Antimicrobial Agents: An Overview of Different Biological Activities in Relation to the Oxidation State of the Gold Ion and the Ligand Structure.</a> Glisic, B.D. and M.I. Djuran. Dalton Transactions, 2014. 43(16): p. 5950-5969. ISI[000333525500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333519100002">Isolation, Characterization and Antimicrobial Evaluation of a Novel Compound N-Octacosan 7beta ol, from Fumaria parviflora Lam.</a>Jameel, M., M. Islamuddin, A. Ali, F. Afrin, and M. Ali. BMC Complementary and Alternative Medicine, 2014. 14: p. 98. ISI[000333519100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333727000001">Polyaniline/CoFe2O4 Nanocomposite Inhibits the Growth of Candida albicans 077 by ROS Production.</a> Khan, J.A., M. Qasim, B.R. Singh, W. Khan, D. Das, and A.H. Naqvi. Comptes Rendus Chimie, 2014. 17(2): p. 91-102. ISI[000333727000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333580000013">Synthesis and Synergistic Antimycobacterial Screening of Chlorpromazine and its Metabolites.</a> Kigondu, E.M., M. Njoroge, K. Singh, N. Njuguna, D.F. Warner, and K. Chibale. MedChemComm, 2014. 5(4): p. 502-506. ISI[000333580000013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333580000016">Synthesis and Biological Evaluation of Substituted N-Alkylphenyl-3,5-dinitrobenzamide Analogs as anti-TB Agents.</a> Munagala, G., K.R. Yempalla, S.K. Aithagani, N.P. Kalia, F. Ali, I. Ali, V.S. Rajput, C. Rani, R. Chib, R. Mehra, A. Nargotra, I.A. Khan, R.A. Vishwakarma, and P.P. Singh. MedChemComm, 2014. 5(4): p. 521-527. ISI[000333580000016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333545200002">Analysis and in Vitro Anti-candida Antifungal Activity of Cuminum cyminum and Salvadora persica Herbs Extracts against Pathogenic Candida Strains.</a> Naeini, A., N.J. Naderi, and H. Shokri. Journal de Mycologie Medicale, 2014. 24(1): p. 13-18. ISI[000333545200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333518500001">Evaluation of Antibacterial and Cytotoxic Activity of Artemisia nilagirica and Murraya koenigii Leaf Extracts against Mycobacteria and Macrophages.</a> Naik, S.K., S. Mohanty, A. Padhi, R. Pati, and A. Sonawane. BMC Complementary and Alternative Medicine, 2014. 14: p. 87. ISI[000333518500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334282800002">Investigation on Antimicrobial Activity of Root Extracts of Thespesia populnea Linn.</a> Senthil-Rajan, D., M. Rajkumar, R. Srinivasan, C. Kumarappan, K. Arunkumar, K.L. Senthilkumar, and M.V. Srikanth. Tropical Biomedicine, 2013. 30(4): p. 570-578. ISI[000334282800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333574600006">Lithium Inhibits Growth of Intracellular Mycobacterium kansasii through Enhancement of Macrophage Apoptosis.</a>Sohn, H., K. Kim, K.S. Lee, H.G. Choi, K.I. Lee, A.R. Shin, J.S. Kim, S.J. Shin, C.H. Song, J.K. Park, and H.J. Kim. Journal of Microbiology, 2014. 52(4): p. 299-306. ISI[000333574600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333341500005">Synthesis and Antifungal Activity of Substituted Nitrotetrazole-5-carbaldehyde Hydrazones.</a> Tyrkov, A.G., M.A. Abdel&#39;rakhim, L.T. Sukhenko, and O.V. Degtyarev. Pharmaceutical Chemistry Journal, 2014. 47(11): p. 589-592. ISI[000333341500005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>

    <br />

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333770600001">Synthesis and Biological Evaluation of 5-Substituted Derivatives of Benzimidazole.</a> Vasic, V.P., J.Z. Penjisevic, I.T. Novakovic, V.V. Sukalovic, D.B. Andric, and S.V. Kostic-Rajacic. Journal of the Serbian Chemical Society, 2014. 79(3): p. 277-282. ISI[000333770600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0425-050814.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
