

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-02-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="uO9hg7gLFZHPkrdM0Ezjiq0UsnVERZjP92RFAmsItUaRVsR4T8VCX1Z8zs6mc002oTQMd9NRl796wLnWD/FT5Gh5SOKhXvdjfvlGKFuzH2ndq+O6PvqfrOOHv1I=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1DB2F178" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: February 13 - February 26, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25693196">Validation of Cross-genotype Neutralization by Hepatitis B Virus-specific Monoclonal Antibodies by in Vitro and in Vivo Infection.</a> Hamada-Tsutsumi, S., E. Iio, T. Watanabe, S. Murakami, M. Isogawa, S. Iijima, T. Inoue, K. Matsunami, K. Tajiri, T. Ozawa, H. Kishi, A. Muraguchi, T. Joh, and Y. Tanaka. Plos One, 2015. 10(2): p. e0118062. PMID[25693196].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25600408">Synthesis and anti-Hepatitis B Virus Activity of C4 Amide-substituted Isosteviol Derivatives.</a> Huang, T.J., C.L. Yang, Y.C. Kuo, Y.C. Chang, L.M. Yang, B.H. Chou, and S.J. Lin. Bioorganic &amp; Medicinal Chemistry, 2015. 23(4): p. 720-728. PMID[25600408].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25613678">6-(Azaindol-2-yl)pyridine-3-sulfonamides as Potent and Selective Inhibitors Targeting Hepatitis C Virus NS4B.</a> Chen, G., H. Ren, N. Zhang, W. Lennox, A. Turpoff, S. Paget, C. Li, N. Almstead, F.G. Njoroge, Z. Gu, J. Graci, S.P. Jung, J. Colacino, F. Lahser, X. Zhao, M. Weetall, A. Nomeir, and G.M. Karp. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 781-786. PMID[25613678].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25615008">Synthesis of 2,3-Dideoxy-2-fluoro-2,3-endo-methylene- and 2,3-Dideoxy-2-fluoro-3-C-hydroxymethyl-2,3-endo-methylene-pentofuranoses and Their Use in the Preparation of Conformationally Locked Bicyclic Nucleosides.</a> Clarkson, R., Z. Komsta, B.A. Mayes, A. Moussa, M. Shelbourne, A. Stewart, A.J. Tyrrell, L.L. Wallis, and A.C. Weymouth-Wilson. Journal of Organic Chemistry, 2015. 80(4): p. 2198-2215. PMID[25615008].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25550074">Supercritical Fluid Extraction of Heather (Calluna vulgaris) and Evaluation of anti-Hepatitis C Virus Activity of the Extracts.</a> Garcia-Risco, M.R., E. Vazquez, J. Sheldon, E. Steinmann, N. Riebesehl, T. Fornari, and G. Reglero. Virus Research, 2015. 198: p. 9-14. PMID[25550074].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25595681">Discovery of Thienoimidazole-based HCV NS5A Inhibitors. Part 1: C2-Symmetric Inhibitors with Diyne and Biphenyl Linkers.</a> Giroux, S., D. Bilimoria, C. Cadilhac, K.M. Cottrell, F. Denis, E. Dietrich, N. Ewing, J.A. Henderson, L. L&#39;Heureux, N. Mani, M. Morris, O. Nicolas, T.J. Reddy, S. Selliah, R.S. Shawgo, J. Xu, N. Chauret, F. Berlioz-Seux, L.C. Chan, S.K. Das, A.L. Grillot, Y.L. Bennani, and J.P. Maxwell. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 936-939. PMID[25595681].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25597006">Discovery of Thienoimidazole-based HCV NS5A Inhibitors. Part 2: Non-Symmetric Inhibitors with Potent Activity against Genotype 1a and 1b.</a> Giroux, S., D. Bilimoria, C. Cadilhac, K.M. Cottrell, F. Denis, E. Dietrich, N. Ewing, J.A. Henderson, L. L&#39;Heureux, N. Mani, M. Morris, O. Nicolas, T.J. Reddy, S. Selliah, R.S. Shawgo, J. Xu, N. Chauret, F. Berlioz-Seux, L.C. Chan, S.K. Das, A.L. Grillot, Y.L. Bennani, and J.P. Maxwell. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 940-943. PMID[25597006].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25619934">Bioengineering and Semisynthesis of an Optimized Cyclophilin Inhibitor for Treatment of Chronic Viral Infection.</a> Hansson, M.J., S.J. Moss, M. Bobardt, U. Chatterji, N. Coates, J.A. Garcia-Rivera, E. Elmer, S. Kendrew, P. Leyssen, J. Neyts, E.A.M. Nur, T. Warneck, B. Wilkinson, P. Gallay, and M.A. Gregory. Chemistry &amp; Biology, 2015. 22(2): p. 285-292. PMID[25619934].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25577039">Synthesis and Evaluation of NS5A Inhibitors Containing Diverse Heteroaromatic Cores.</a> Henderson, J.A., D. Bilimoria, M. Bubenik, C. Cadilhac, K.M. Cottrell, F. Denis, E. Dietrich, N. Ewing, G. Falardeau, S. Giroux, L. L&#39;Heureux, B. Liu, N. Mani, M. Morris, O. Nicolas, O.Z. Pereira, C. Poisson, T.J. Reddy, S. Selliah, R.S. Shawgo, L. Vaillancourt, J. Wang, J. Xu, N. Chauret, F. Berlioz-Seux, L.C. Chan, S.K. Das, A.L. Grillot, Y.L. Bennani, and J.P. Maxwell. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 948-951. PMID[25577039].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25577041">Benzimidazole-containing HCV NS5A Inhibitors: Effect of 4-Substituted Pyrrolidines in Balancing Genotype 1a and 1b Potency.</a> Henderson, J.A., D. Bilimoria, M. Bubenik, C. Cadilhac, K.M. Cottrell, E. Dietrich, F. Denis, N. Ewing, G. Falardeau, S. Giroux, R. Grey, Jr., L. L&#39;Heureux, B. Liu, N. Mani, M. Morris, O. Nicolas, O.Z. Pereira, C. Poisson, B. Govinda Rao, T.J. Reddy, S. Selliah, R.S. Shawgo, L. Vaillancourt, J. Wang, C.G. Yannopoulos, N. Chauret, F. Berlioz-Seux, L.C. Chan, S.K. Das, A.L. Grillot, Y.L. Bennani, and J.P. Maxwell. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 944-947. PMID[25577041].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25671292">Enantioselective Synthesis, Stereochemical Correction, and Biological Investigation of the Rodgersinine Family of 1,4-Benzodioxane Neolignans.</a> Pilkington, L.I., J. Wagoner, S.J. Polyak, and D. Barker. Organic Letters, 2015. 17(4): p. 1046-1049. PMID[25671292].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-02261.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25473062">Targeting Cellular Squalene Synthase, an Enzyme Essential for Cholesterol Biosynthesis, Is a Potential Antiviral Strategy against Hepatitis C Virus.</a> Saito, K., Y. Shirasago, T. Suzuki, H. Aizaki, K. Hanada, T. Wakita, M. Nishijima, and M. Fukasawa. Journal of Virology, 2015. 89(4): p. 2220-2232. PMID[25473062].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25592710">Prodrugs of Imidazotriazine and Pyrrolotriazine C-Nucleosides Can Increase anti-HCV Activity and Enhance Nucleotide Triphosphate Concentrations in Vitro.</a> Tyndall, E.M., A.G. Draffan, B. Frey, B. Pool, R. Halim, S. Jahangiri, S. Bond, V. Wirth, A. Luttick, D. Tilmanis, J. Thomas, K. Porter, and S.P. Tucker. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(4): p. 869-873. PMID[25592710].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25458285">Characterization and Biological Effects of Two Polysaccharides Isolated from Acanthopanax sciadophylloides.</a> Lee, J.B., T. Tanikawa, K. Hayashi, M. Asagi, Y. Kasahara, and T. Hayashi. Carbohydrate Polymers, 2015. 116: p. 159-166. PMID[25458285].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0213-022615.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348486000002">Antiviral Drug Discovery: Broad-spectrum Drugs from Nature.</a> Martinez, J.P., F. Sasse, M. Bronstrup, J. Diez, and A. Meyerhans. Natural Product Reports, 2015. 32(1): p. 29-48. ISI[000348486000002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000347710600003">Novel 4-Thiazolidinones as Non-nucleoside Inhibitors of Hepatitis C Virus NS5B RNA-dependent RNA Polymerase.</a> Cakir, G., I. Kucukguzel, R. Guhamazumder, E. Tatar, D. Manvar, A. Basu, B.A. Patel, J. Zia, T.T. Talele, and N. Kaushik-Basu. Archiv der Pharmazie, 2015. 348(1): p. 10-22. ISI[000347710600003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0213-022615.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348455200351">Benzhydrylpiperazine Compounds Inhibit Cholesterol-dependent HCV Entry.</a> Chamoun-Emanuelli, A.M. and Z.L. Chen. Abstracts of Papers of the American Chemical Society, 2014. 247: Poster #22. ISI[000348455200351].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0213-022615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
