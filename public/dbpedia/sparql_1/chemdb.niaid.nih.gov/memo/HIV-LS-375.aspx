

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-375.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6MS3kP/Pgh0i4I2J+aPjkl7tTLrj1GTC+SgGtxCXMaKvfF/He4jvUWBKDimgcaAGpy8DUiaIff4IwXgjTGxBHxfVaKwf7fvni39ql33CKBnXnvDVm+12fZHZPpQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BB154CDC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-375-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60898   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Tmaz as an antiviral agent and use thereof</p>

    <p>          Lelas, Tihomir </p>

    <p>          PATENT:  WO <b>2007054085</b>  ISSUE DATE:  20070518</p>

    <p>          APPLICATION: 2006</p>

    <p>          ASSIGNEE:  (Ljubicic, Mijo Germany and Ivkovic, Slavko</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60899   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Agaritine and its derivatives are potential inhibitors against HIV proteases</p>

    <p>          Gao, Wei-Na, Wei, Dong-Qing, Li, Yun, Gao, Hui, Xu, Wei-Ren, Li, Ai-Xiu, and Chou, Kuo-Chen</p>

    <p>          Med. Chem. <b>2007</b>.  3(3): 221-226</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60900   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          Human immunodeficiency virus type 1 Vif inhibits packaging and antiviral activity of a degradation-resistant APOBEC3G variant</p>

    <p>          Opi, S, Kao, S, Goila-Gaur, R, Khan, MA, Miyagi, E, Takeuchi, H, and Strebel, K</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17522211&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17522211&amp;dopt=abstract</a> </p><br />

    <p>4.     60901   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          An alternative strategy for inhibiting multidrug-resistant mutants of the dimeric HIV-1 protease by targeting the subunit interface</p>

    <p>          Bannwarth, L and Reboud-Ravaux, M</p>

    <p>          Biochem Soc Trans <b>2007</b>.  35(Pt 3): 551-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17511649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17511649&amp;dopt=abstract</a> </p><br />

    <p>5.     60902   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          A Naturally Occurring Splice Variant of CXCL12/Stromal Cell-Derived Factor 1 is a potent HIV-1 Inhibitor with Weak Chemotaxis and Cell Survival Activities</p>

    <p>          Altenburg, JD, Broxmeyer, HE, Jin, Q, Cooper, S, Basu, S, and Alkhatib, G</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507482&amp;dopt=abstract</a> </p><br />

    <p>6.     60903   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Small molecule inhibitors of HIV-1 capsid assembly</p>

    <p>          Prevelige, Peter Jr</p>

    <p>          PATENT:  WO <b>2007048042</b>  ISSUE DATE:  20070426</p>

    <p>          APPLICATION: 2006  PP: 69pp.</p>

    <p>          ASSIGNEE:  (University of Alabama at Birmingham, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     60904   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Use of bivalent or polyvalent trisaccharides as fusion inhibitors in all HIV types, subtypes, groups, strains, and circulating recombinant forms, as well as for the treatment of other viral infections and cancer</p>

    <p>          Farquharson, Mercedes</p>

    <p>          PATENT:  US <b>2007093452</b>  ISSUE DATE:  20070426</p>

    <p>          APPLICATION: 2005-57377  PP: 4pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     60905   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          QSAR Study on Pyridinone Derivatives as HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor: A Mixed Approach</p>

    <p>          Vasanthanathan, P, Lakshmi, M, and Rao, AR</p>

    <p>          Med Chem <b>2007</b>.  3(3): 227-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17504193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17504193&amp;dopt=abstract</a> </p><br />

    <p>9.     60906   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          N-Phenyl-phenylacetamide derivatives as non-nucleoside reverse transcriptase inhibitors, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          Mirzadegan, Taraneh and Silva, Tania</p>

    <p>          PATENT:  US <b>2007088053</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006-59058  PP: 27pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60907   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          N-Phenyl-phenylacetamide derivatives as non-nucleoside reverse transcriptase inhibitors, their preparation, pharmaceutical compositions, and use in therapy</p>

    <p>          Silva, Tania and Sweeney, Zachary Kevin</p>

    <p>          PATENT:  US <b>2007088015</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006-59057  PP: 32pp.</p>

    <p>          ASSIGNEE:  (Roche Palo Alto LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60908   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          A phase I/IIa study with succinylated human serum albumin (Suc-HSA), a candidate HIV-1 fusion inhibitor</p>

    <p>          Vermeulen, JN, Meijer, DK, Over, J, Lange, J, Proost, JH, Bakker, HI, Beljaars, L, Wit, FW, and Prins, JM</p>

    <p>          Antivir Ther <b>2007</b>.  12(2): 273-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17503670&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17503670&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   60909   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of pyrimidine and pyrimidinone derivatives and their use for the treatment of HIV infections</p>

    <p>          Botta, Maurizio, Corelli, Federico, Petricci, Elena, Radi, Marco, Maga, Giovanni, Este&#39;a, Jose, and Mai, Antonello</p>

    <p>          PATENT:  WO <b>2007043094</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006  PP: 53pp.</p>

    <p>          ASSIGNEE:  (Universita degli Studi di Siena, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60910   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          Antiretroviral drug resistance, HIV-1 tropism, and HIV-1 subtype among men who have sex with men with recent HIV-1 infection</p>

    <p>          Eshleman, SH, Husnik, M, Hudelson, S, Donnell, D, Huang, Y, Huang, W, Hart, S, Jackson, B, Coates, T, Chesney, M, and Koblin, B</p>

    <p>          AIDS <b>2007</b>.  21 (9): 1165-1174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17502727&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17502727&amp;dopt=abstract</a> </p><br />

    <p>14.   60911   HIV-LS-375; SCIFINDER-HIV-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of heterocyclic Non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Saito, Yoshihito David, Smith, Mark, and Sweeney, Zachary Kevin</p>

    <p>          PATENT:  US <b>2007078128</b>  ISSUE DATE:  20070405</p>

    <p>          APPLICATION: 54  PP: 31pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60912   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          Structure-Activity Relationships of Synthetic Coumarins as HIV-1 Inhibitors</p>

    <p>          Kostova, I, Raleva, S, Genova, P, and Argirova, R</p>

    <p>          Bioinorg Chem Appl <b>2006</b>.: 68274</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497014&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497014&amp;dopt=abstract</a> </p><br />

    <p>16.   60913   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          Evaluation of the substrate envelope hypothesis for inhibitors of HIV-1 protease</p>

    <p>          Chellappan, S, Kairys, V, Fernandes, MX, Schiffer, C, and Gilson, MK</p>

    <p>          Proteins <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17474129&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17474129&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   60914   HIV-LS-375; PUBMED-HIV-5/29/2007</p>

    <p class="memofmt1-2">          Improved oral bioavailability of anti-HIV agent N&#39;-[2-(2-thiophene)ethyl]-N&#39;-[2-(5-bromopyridyl)]-thiourea (HI-443) in a novel lipophilic formulation</p>

    <p>          Uckun, FM, Erbeck, D, Tibbles, H, Qazi, S, and Venkatachalam, TK</p>

    <p>          Arzneimittelforschung <b>2007</b>.  57(3): 164-70</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17469651&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17469651&amp;dopt=abstract</a> </p><br />

    <p>18.   60915   HIV-LS-375; WOS-HIV-5/18/2007</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity of 4-chloro-2-mercapto-N-(1,4,5,6-tetrahydro-1,2,4-triazin-3-yl)benzenesulf onamide derivatives</p>

    <p>          Pomarnacka, E</p>

    <p>          POLISH JOURNAL OF CHEMISTRY <b>2007</b>.  81(3): 345-352</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245413300005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245413300005</a> </p><br />

    <p>19.   60916   HIV-LS-375; WOS-HIV-5/18/2007</p>

    <p class="memofmt1-2">          Design and synthesis of BACE1 inhibitors containing a novel norstatine derivative (2R,3R)-3-amino-2-hydroxy-4(phenylthio)butyric acid</p>

    <p>          Ziora, Z, Kasai, S, Hidaka, K, Nagamine, A, Kimura, T, Hayashi, Y, and Kiso, Y</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2007</b>.  17(6): 1629-1633</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245492400029">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245492400029</a> </p><br />

    <p>20.   60917   HIV-LS-375; WOS-HIV-5/18/2007</p>

    <p class="memofmt1-2">          Novel in vivo model for the study of human immunodeficiency virus type 1 transcription inhibitors: Evaluation of new 6-desfluoroquinolone derivatives</p>

    <p>          Stevens, M, Pollicita, M, Pannecouque, C, Verbeken, E, Tabarrini, O, Cecchetti, V, Aquaro, S, Perno, CF, Fravolini, A, De Clercq, E, Schols, D, and Balzarini, J</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(4): 1407-1413</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245416500040</a> </p><br />

    <p>21.   60918   HIV-LS-375; WOS-HIV-5/25/2007</p>

    <p class="memofmt1-2">          Resistance of HIV-1 to the broadly HIV-1-neutralizing, anti-carbohydrate antibody 2G12</p>

    <p>          Huskens, D, Van Laethem, K, Vermeire, K, Balzarini, J, and Schols, D</p>

    <p>          VIROLOGY <b>2007</b>.  360(2): 294-304</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245566100006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245566100006</a> </p><br />

    <p>22.   60919   HIV-LS-375; WOS-HIV-5/25/2007</p>

    <p class="memofmt1-2">          Anti-HIV activity of a cardiac glycoside isolated from Elaeodendron croceum</p>

    <p>          Prinsloo, G, Meyer, JJM, and Hussein, AA</p>

    <p>          SOUTH AFRICAN JOURNAL OF BOTANY <b>2007</b>.  73(2): 308</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246312700120">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246312700120</a> </p><br />
    <br clear="all">

    <p>23.   60920   HIV-LS-375; WOS-HIV-5/25/2007</p>

    <p class="memofmt1-2">          Synthesis of ursolic phosphonate derivatives as potential Anti-HIV agents</p>

    <p>          Deng, SL, Baglin, I, Nour, M, Flekhter, O, Vita, C, and Cave, C</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2007</b>.  182(5): 951-967</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245591600003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245591600003</a> </p><br />

    <p>24.   60921   HIV-LS-375; WOS-HIV-5/25/2007</p>

    <p class="memofmt1-2">          Mechanism of drug resistance due to N88S in CRF01_AE HIV-1 protease, analyzed by molecular dynamics simulations</p>

    <p>          Ode, H, Matsuyama, S, Hata, M, Hoshino, T, Kakizawa, J, and Sugiura, W</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  50(8): 1768-1777</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245634500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245634500007</a> </p><br />

    <p>25.   60922   HIV-LS-375; WOS-HIV-5/25/2007</p>

    <p class="memofmt1-2">          D- and L-2 &#39;,3 &#39;-didehydro-2 &#39;,3 &#39;-dideoxy-3 &#39;-fluoro-carbocyclic nucleosides: Synthesis, anti-HIV activity and mechanism of resistance</p>

    <p>          Wang, JN, Jin, YH, Rapp, KL, Schinazi, RF, and Chu, CK</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2007</b>.  50(8): 1828-1839</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245634500012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245634500012</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
