

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-06-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RTxY6CDGqz+56L66aKk4pEUvef1kq0HdbSzp3tLg2kZV0nxkArs1gMmOHcEsNjoPZ5NvFEtQRhJzzVrSvyKqf3wN25oPAM2arhp44GfPciMsvBFvvD76g0D97VQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="40CB962A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List:  May 25 - June 7, 2012</h1>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22633687">Finger Loop Inhibitors of the HCV NS5b Polymerase. Part II. Optimization of Tetracyclic Indole-based Macrocycle Leading to the Discovery of TMC647055.</a> Vendeville, S., T.I. Lin, L. Hu, A. Tahri, D. McGowan, M.D. Cummings, K. Amssoms, M. Canard, S. Last, I. Van den Steen, B. Devogelaere, M.C. Rouan, L. Vijgen, J.M. Berke, P. Dehertogh, E. Fransen, E. Cleiren, L. van der Helm, G. Fanning, K. Van Emelen, O. Nyanguile, K. Simmen, and P. Raboisson, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22633687].
    <br />
    <b>[PubMed]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22664976">First-in-human Pharmacokinetics and Antiviral Activity of IDX375, a Novel Non-nucleoside Hepatitis C Virus Polymerase Inhibitor.</a> de Bruijne, J., V.D.W.d.R. J, A.A. van Vliet, X.J. Zhou, M.F. Temam, J. Molles, J. Chen, K. Pietropaolo, J.Z. Sullivan-Bolyai, D. Mayers, and H.W. Reesink, Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22664976].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22664130">Tri-substituted Acylhydrazines as Tertiary Amide Bioisosteres: HCV NS5B Polymerase Inhibitors.</a> Canales, E., J.S. Carlson, T. Appleby, M. Fenaux, J. Lee, Y. Tian, N. Tirunagari, M. Wong, and W.J. Watkins, Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22664130].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0525-060712.</p>

    <h2>HSV-1</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22654513">Cascade Search for HSV-1 Combinatorial Drugs with High Antiviral Efficacy and Low Toxicity.</a> Ding, X., D.J. Sanchez, A. Shahangian, I. Al-Shyoukh, G. Cheng, and C.M. Ho, International Journal of Nanomedicine, 2012. 7: p. 2281-2292; PMID[22654513].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22624581">Anti-herpes Virus Activities of Bioactive Fraction and Isolated Pure Constituent of Mallotus peltatus: An Ethnomedicine from Andaman Islands.</a> Bag, P., D. Chattopadhyay, H. Mukherjee, D. Ojha, N. Mandal, M.C. Sarkar, T. Chatterjee, G. Das, and S. Chakraborti, Virology Journal, 2012. 9(1): p. 98; PMID[22624581].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0525-060712.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303579100002">Synthesis and Biological Evaluation of (3 &#39; S)-3 &#39;-Deoxy-3 &#39;-fluoro-3 &#39;-C-ethynlcytidine.</a> Clark, J.L., C.B. Clark, and J.C. Mason, Nucleosides Nucleotides &amp; Nucleic Acids, 2012. 31(4): p. 286-292; ISI[000303579100002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303484600660">In Vitro Characterization of the Anti-viral Activity of TT-034, a RNAi-Therapeutic against the Hepatitis C Virus.</a> Lavender, H., K. Brady, F. Burden, O. Delpuech-Adams, H. Denise, A. Palmer, H. Perkins, B. Savic, S. Scott, C. Smith-Burchnell, S. Moschos, P. Troke, F. Wright, R. Corbau, and D. Suhy, Molecular Therapy, 2012. 20: p. S256-S256; ISI[000303484600660].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303484600327">HCV Inhibition by a Multicistronic anti-HCV miRNA AAV Vector in Vitro and in Vivo.</a> Marcucci, K.T., X. Yang, S.Z. Zhou, G.X. Luo, and L.B. Couto, Molecular Therapy, 2012. 20: p. S128-S129; ISI[000303484600327].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303692000003">An Engineered Inhibitor RNA that Efficiently Interferes with Hepatitis C Virus Translation and Replication</a><i>.</i> Romero-Lopez, C., B. Berzal-Herranz, J. Gomez, and A. Berzal-Herranz, Antiviral Research, 2012. 94(2): p. 131-138; ISI[000303692000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0525-060712.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303681400006">A QSAR Study on Some Thiophene Derivatives Acting as anti-HCV Agents</a><i>.</i> Varshney, J., A. Sharma, and S.P. Gupta, Letters in Drug Design &amp; Discovery, 2012. 9(4): p. 389-396; ISI[000303681400006].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0525-060712.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
