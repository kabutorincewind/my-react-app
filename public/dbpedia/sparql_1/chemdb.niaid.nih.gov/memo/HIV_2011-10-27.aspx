

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-10-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dFTkVkO2Lw8Ei6J6/hsElB5KonvXRe20HSzmOCoBlRW6cUXKjgEdljxtBZkb7Paj66u2SfOiKeCf5Ya5csCp4Z2yfsgpxdIQiZixgL3VSKwnoCWsq+d91xQvJHM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="246E4511" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 14, 2011 - October 27, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21793507">Structure-Based Identification and Neutralization Mechanism of Tyrosine Sulfate Mimetics That Inhibit HIV-1 Entry.</a> Acharya, P., C. Dogo-Isonagie, J.M. Lalonde, S.N. Lam, G.J. Leslie, M.K. Louder, L.L. Frye, A.K. Debnath, J.R. Greenwood, T.S. Luongo, L. Martin, K.S. Watts, J.A. Hoxie, J.R. Mascola, C.A. Bewley, and P.D. Kwong, ACS Chemical Biology, 2011. 6(10): p. 1069-1077; PMID[21793507].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21888425">Discovery of Novel Promising Targets for Anti-AIDS Drug Developments by Computer Modeling: Application to the HIV-1 gp120 V3 Loop.</a> Andrianov, A.M., I.V. Anishchenko, and A.V. Tuzikov, Journal of Chemical Information and Modeling 2011. 51(10): p. 2760-2767; PMID[21888425].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22017513">Difluoromethylbenzoxazole Pyrimidine Thioether Derivatives: A Novel Class of Potent Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Boyer, J., E. Arnoult, M. Medebielle, J. Guillemont, J. Unge, and D. Jochmans, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22017513].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22017299">The Antiretroviral Protease Inhibitors Indinavir and Nelfinavir Stimulate Mrp1-mediated GSH Export from Cultured Brain Astrocytes.</a> Brandmann, M., K. Tulpule, M.M. Schmidt, and R. Dringen, Journal of Neurochemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22017299].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21878060">A Single Step Assay for Rapid Evaluation of Inhibitors Targeting HIV Type 1 Tat-Mediated Long Terminal Repeat Transactivation.</a> Chande, A.G., M. Baba, and R. Mukhopadhyaya, AIDS Research and Human Retroviruses, 2011. <b>[Epub ahead of print]</b>; PMID[21878060].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21859137">Evaluation of the Synthesis of Sialic Acid-PAMAM Glycodendrimers Without the Use of Sugar Protecting Groups, and the Anti-HIV-1 Properties of These Compounds.</a> Clayton, R., J. Hardman, C.C. Labranche, and K.D. McReynolds, Bioconjugate Chemistry, 2011. 22(10): p. 2186-2197; PMID[21859137].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22013044">Stimulation of hRAD51 Nucleofilament Restricts HIV-1 Integration in Vitro and in Infected Cells.</a> Cosnefroy, O., A. Tocco, P. Lesbats, S. Thierry, C. Calmels, T. Wiktorowicz, S. Reigadas, Y. Kwon, A. De Cian, S. Desfarges, P. Bonot, J. San Filippo, S. Litvak, E. Le Cam, A. Rethwilm, H. Fleury, P.P. Connell, P. Sung, O. Delelis, M.L. Andreola, and V. Parissi, Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[22013044].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21999803">Classification of anti-HIV Compounds Using Counterpropagation Artificial Neural Networks and Decision Trees.</a> Jalali-Heravi, M., A. Mani-Varnosfaderani, P.E. Jahromi, M.M. Mahmoodi, and D. Taherinia, SAR and QSAR in Environmental Research, 2011. <b>[Epub ahead of print]</b>; PMID[21999803].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22026578">Synthesis of Conformationally North-Locked Pyrimidine Nucleosides Built on an Oxa-bicyclo[3.1.0]hexane Scaffold</a>. Ludek, O.R. and V.E. Marquez, The Journal of Organic Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22026578].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p> </p>

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21889342">Ferrocenyl chalcone difluoridoborates inhibit HIV-1 integrase and display low activity towards cancer and endothelial cells.</a> Monserrat, J.P., R.I. Al-Safi, K.N. Tiwari, L. Quentin, G.G. Chabot, A. Vessieres, G. Jaouen, N. Neamati, and E.A. Hillard, Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(20): p. 6195-7; PMID[21889342].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21999609">Development of Anti-HIV Activity Models of Lysine Sulfonamide Analogs: A QSAR Perspective.</a> Muthukumaran, R., B. Sangeetha, R. Amutha, and P.P. Mathur, Current Computer Aided Drug Design, 2011. <b>[Epub ahead of print]</b>; PMID[21999609].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21905195">Biophysical Investigations of GBV-C E1 Peptides as Potential Inhibitors of HIV-1 Fusion Peptide.</a> Sanchez-Martin, M.J., P. Urban, M. Pujol, I. Haro, M.A. Alsina, and M.A. Busquets, ChemPhysChem, 2011. 12(15): p. 2816-2822; PMID[21905195].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22020812">In Silico Design of Multi-target Inhibitors for C-C Chemokine Receptors Using Substructural Descriptors.</a> Speck-Planche, A. and V.V. Kleandrova, Molecular Diversity, 2011. <b>[Epub ahead of print]</b>; PMID[22020812].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21930388">Novel Diarylpyridinones, Diarylpyridazinones and Diarylphthalazinones as Potential HIV-1 Nonnucleoside Reverse Transcriptase Inhibitors (NNRTIs).</a> Venkatraj, M., K.K. Arien, J. Heeres, B. Dirie, J. Joossens, S. Van Goethem, P. Van der Veken, J. Michiels, C.M. Vande Velde, G. Vanham, P.J. Lewi, and K. Augustyns, Bioorganic &amp; Medicinal Chemistry, 2011. 19(20): p. 5924-5934; PMID[21930388].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22014786">Cordysobin, a Novel Alkaline Serine Protease with HIV-1 Reverse Transcriptase Inhibitory Activity from the Medicinal Mushroom Cordyceps sobolifera.</a> Wang, S.X., Y. Liu, G.Q. Zhang, S. Zhao, F. Xu, X.L. Geng, and H.X. Wang, Journal of Bioscience and Bioengineering 2011. <b>[Epub ahead of print]</b>; PMID[22014786].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21945613">Molecules from Apoptotic Pathways Modulate HIV-1 Replication in Jurkat Cells.</a> Wang, X., V. Ragupathy, J. Zhao, and I. Hewlett, Biochemical and Biophysical Research Communications, 2011. 414(1): p. 20-24; PMID[21945613].
    <br />
    <b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22013046">Single Chain Fv-based anti-HIV Proteins: Potential and Limitations.</a> West, A.P., Jr., R.P. Galimidi, P.N. Gnanapragasam, and P.J. Bjorkman, Journal of Virology, 2011. <b>[Epub ahead of print]</b>; PMID[22013046].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p> 

    <p class="NoSpacing">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295533200002">Polyanionic Inhibitors of HIV Adsorption.</a> Baranova, E.O., N.S. Shastina, and V.I. Shvets, Russian Journal of Bioorganic Chemistry, 2011. 37(5): p. 527-542; ISI[000295533200002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295297800014">Comprehensive Analysis of the Intracellular Metabolism of Antiretroviral Nucleosides and Nucleotides Using Liquid Chromatography-tandem Mass Spectrometry and Method Improvement by Using Ultra Performance Liquid Chromatography.</a> Coulier, L., H. Gerritsen, J.J.A. van Kampen, M.L. Reedijk, T.M. Luider, A. Osterhaus, R.A. Gruters, and L. Brull, Journal of Chromatography B-Analytical Technologies in the Biomedical and Life Sciences, 2011. 879(26): p. 2772-2782; ISI[000295297800014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295422500002">The Effects of RNase H Inhibitors and Nevirapine on the Susceptibility of HIV-1 to AZT and 3TC.</a> Davis, C.A., M.A. Parniak, and S.H. Hughes, Virology, 2011. 419(2): p. 64-71; ISI[000295422500002].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295218600018">Strategies to Improve Efficacy and Safety of a Novel Class of Antiviral Hyper-activation-limiting Therapeutic Agents: The VS411 Model in HIV/AIDS</a>. De Forni, F., M.R. Stevens, and F. Lori,  British Journal of Pharmacology, 2011. 164(4): p. 1392-1392; ISI[000295218600018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295160600416">The HIV Protease Inhibitor Nelfinavir has Proteasome-inhibiting Activity in Vivo Distinct from Bortezomib.</a> Kraus, M., J. Bader, D. Hess, S. Haile, H. Overkleeft, and C. Driessen, Onkologie, 2011. 34: p. 159-159; ISI[000295160600416].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295454200068">Specificity of RSG-1.2 Peptide Binding to RRE-IIB RNA Element of HIV-1 over Rev Peptide is Mainly Enthalpic in Origin.</a> Kumar, S., D. Bose, H. Suryawanshi, H. Sabharwal, K. Mapa, and S. Maiti, Plos One, 2011. 6(8); ISI[000295454200068].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295068100003">HIV-1 Protease Inhibitors with a Tertiary Alcohol Containing Transition-state Mimic and Various P2 and P1 &#39; Substituents</a>. Ohrngren, P.W., XY Persson, M Ekegren, JK Wallberg, H Vrang, L Rosenquist, A Samuelsson, B Unge, T Larhed, M, Medchemcomm, 2011. 2(8): p. 701-709; ISI[000295068100003].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294990900014">Design, Synthesis, and Evaluation of Thiazolidinone Derivatives as Antimicrobial and Anti-viral Agents.</a> Ravichandran, V., A. Jain, K.S. Kumar, H. Rajak, and R.K. Agrawal, Chemical Biology &amp; Drug Design, 2011. 78(3): p. 464-470; ISI[000294990900014].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.
    <br />
    <br />
    26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295211500022">Diastereoselective Synthesis of (Aryloxy)phosphoramidate Prodrugs</a>. Roman, C.A., P. Wasserthal, J. Balzarini, and C. Meier, European Journal of Organic Chemistry, 2011(25): p. 4899-4909; ISI[000295211500022].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295084400008">An Evaluation of the RNase H Inhibitory Effects of Vietnamese Medicinal Plant Extracts and Natural Compounds.</a> Tai, B.H., N.D. Nhut, N.X. Nhiem, T.H. Quang, T.T.N. Nguyen, T.T.L. Bui, T.T. Huong, J. Wilson, J.A. Beutler, N.K. Ban, N.M. Cuong, and Y.H. Kim, Pharmaceutical Biology, 2011. 49(10): p. 1046-1051; ISI[000295084400008].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295294700010">Evaluation of Cynanchum otophyllum glucan sulfate against Human Immunodeficiency Virus and Herpes Simplex Virus as a Microbicide Agent.</a> Tao, J., J. Yang, C.Y. Chen, X.M. Cao, S.L. Zhao, and K.L. Ben, Indian Journal of Pharmacology, 2011. 43(5): p. 536-540; ISI[000295294700010].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295109400012">Product and Process Understanding of a Novel Pediatric anti-HIV Tenofovir Niosomes with a High-pressure Homogenizer.</a> Zidan, A.S., Z. Rahman, and M.A. Khan, European Journal of Pharmaceutical Sciences, 2011. 44(1-2): p. 93-102; ISI[000295109400012].
    <br />
    <b>[WOS]</b>. HIV_1014-102711.</p>

    <p class="memofmt2-2">Meeting Abstract citations</p>

    <p class="NoSpacing">30. <a href="http://www.natap.org/2011/ICAAC/ICAAC_34.htm">BI 224436, a Non-Catalytic Site Integrase Inhibitor, is a Potent Inhibitor of the Replication of Treatment-naïve and Raltegravir-resistant Clinical Isolates of HIV-1.</a> Fenwick, C., R. Bethell, M. Cordingley, P. Edwards, A-M. Quinson, P. Robinson, B. Simoneau. C. Yoakim, 51<sup>st</sup> ICAAC, 2011.</p>

    <p class="NoSpacing">HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://www.natap.org/2011/ICAAC/ICAAC_33.htm">Pharmacodynamics of BI 224436 for HIV-1 in an in Vitro Hollow Fiber Infection Model System.</a> Brown, A.N., J. McSharry, R. Kulawy, C. Fenwick, R. Bethell, J. Duan, J. Kort, P. Robinson, B. Simoneau, C. Yoakim, G. Drusano, 51<sup>st</sup> ICAAC, 2011.</p>

    <p class="NoSpacing">HIV_1014-102711.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://www.natap.org/2011/ICAAC/ICAAC_32.htm">Preclinical Profile of BI 224436, a Novel HIV-1 Non-Catalytic Site Integrase Inhibitor.</a> Yoakim, C., M. Amad, M.D. Bailey, R. Bethell, M. Bos, P. Bonneau, M. Cordingley, R. Coulombe, J. Duan, P. Edwards, L. Fader, A-M Faucher, M. Garneau, A. Jakalian, S. Kawai, L. Lamorte, S. LaPlante, L. Luo, S. Mason, M-A Poupart, N. Rioux, B. Simoneau, Y. Tsantrizos, M. Witvrouw, C.F. Boehringer, 51<sup>st</sup> ICAAC, 2011.</p>

    <p class="NoSpacing">HIV_1014-102711.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
