

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2008-11-03.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zuua0RpZuOVyjtkW8g+lZQsSjcv0tQRqTNtUSbqmc/WvzL0xO5wgK7Vg2nPM2+LiO2plCcqmf1YJ3FgazAa5RM29+xLjrIoVwSvmxglln7DuqQ8y8u1fvUyhN18=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E11958CA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 15 - October 29, 2008</h1>

    <h2>Anti-HIV</h2>

    <p class="ListParagraph">1.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18925934?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Yu H, Tudor D, Alfsen A, Labrosse B, Clavel F, Bomsel M.</a></p>

    <p class="plaintext">Peptide P5 (residues 628-683), comprising the entire membrane proximal region of HIV-1 gp41 and its calcium-binding site, is a potent inhibitor of HIV-1 infection.</p>

    <p class="plaintext">Retrovirology. 2008 Oct 16;5(1):93. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18925934 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">2.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18852475?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">He Y, Cheng J, Lu H, Li J, Hu J, Qi Z, Liu Z, Jiang S, Dai Q.</a></p>

    <p class="plaintext">Potent HIV fusion inhibitors against Enfuvirtide-resistant HIV-1 strains.</p>

    <p class="plaintext">Proc Natl Acad Sci U S A. 2008 Oct 21;105(42):16332-7. Epub 2008 Oct 13.</p>

    <p class="plaintext">PMID: 18852475 [PubMed - in process]</p> 

    <p class="ListParagraph">3.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18835162?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Chen KX, Xie HY, Li ZG, Gao JR.</a></p>

    <p class="plaintext">Quantitative structure-activity relationship studies on 1-aryl-tetrahydroisoquinoline analogs as active anti-HIV agents.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Oct 15;18(20):5381-6. Epub 2008 Sep 18.</p>

    <p class="plaintext">PMID: 18835162 [PubMed - in process]</p>

    <p class="memofmt2-1">&nbsp;</p>

    <h2>HIV or AIDS and Compound or Therapeutic</h2>

    <p class="ListParagraph">4.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18823110?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Giffin MJ, Heaslet H, Brik A, Lin YC, Cauvi G, Wong CH, McRee DE, Elder JH, Stout CD, Torbett BE.</a></p>

    <p class="plaintext">A copper(I)-catalyzed 1,2,3-triazole azide-alkyne click compound is a potent inhibitor of a multidrug-resistant HIV-1 protease variant.</p>

    <p class="plaintext">J Med Chem. 2008 Oct 23;51(20):6263-70. Epub 2008 Sep 30.</p>

    <p class="plaintext">PMID: 18823110 [PubMed - in process]</p>

    <h2>HIV or AIDS and Drug</h2>

    <p class="ListParagraph">5.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18843400?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ghosh AK, Gemma S, Takayama J, Baldridge A, Leshchenko-Yashchuk S, Miller HB, Wang YF, Kovalevsky AY, Koh Y, Weber IT, Mitsuya H.</a></p>

    <p class="plaintext">Potent HIV-1 protease inhibitors incorporating meso-bicyclic urethanes as P2-ligands: structure-based design, synthesis, biological evaluation and protein-ligand X-ray studies.</p>

    <p class="plaintext">Org Biomol Chem. 2008 Oct 21;6(20):3703-13. Epub 2008 Aug 11.</p>

    <p class="plaintext">PMID: 18843400 [PubMed - in process]</p> 

    <p class="ListParagraph">6.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18831589?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Wendeler M, Lee HF, Bermingham A, Miller JT, Chertov O, Bona MK, Baichoo NS, Ehteshami M, Beutler J, O&#39;Keefe BR, Götte M, Kvaratskhelia M, Le Grice S.</a></p>

    <p class="plaintext">Vinylogous ureas as a novel class of inhibitors of reverse transcriptase-associated ribonuclease H activity.</p>

    <p class="plaintext">ACS Chem Biol. 2008 Oct 17;3(10):635-44. Epub 2008 Oct 3.</p>

    <p class="plaintext">PMID: 18831589 [PubMed - in process] <b>           </b></p> 

    <p class="ListParagraph">7.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18811134?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Kazmierski WM, Aquino C, Chauder BA, Deanda F, Ferris R, Jones-Hertzog DK, Kenakin T, Koble CS, Watson C, Wheelan P, Yang H, Youngman M.</a></p>

    <p class="plaintext">Discovery of bioavailable 4,4-disubstituted piperidines as potent ligands of the chemokine receptor 5 and inhibitors of the human immunodeficiency virus-1.    </p>

    <p class="plaintext">J Med Chem. 2008 Oct 23;51(20):6538-46. Epub 2008 Sep 24.</p>

    <p class="plaintext">PMID: 18811134 [PubMed - in process]</p> 

    <h2>HIV or AIDS and Inhibition</h2>

    <p class="ListParagraph">8.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18829317?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Raza A, Sham YY, Vince R.</a></p>

    <p class="plaintext">Design and synthesis of sulfoximine based inhibitors for HIV-1 protease.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Oct 15;18(20):5406-10. Epub 2008 Sep 13.</p>

    <p class="plaintext">PMID: 18829317 [PubMed - in process]</p> 

    <h2>HIV or AIDS and class</h2>

    <p class="ListParagraph">9.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18826203?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Jessen HJ, Balzarini J, Meier C.</a></p>

    <p class="plaintext">Intracellular Trapping of cycloSal-Pronucleotides: modification of prodrugs with amino acid esters.</p>

    <p class="plaintext">J Med Chem. 2008 Oct 23;51(20):6592-8. Epub 2008 Oct 1.</p>

    <p class="plaintext">PMID: 18826203 [PubMed - in process]</p> 

    <h2>Journal Check</h2>

    <p class="ListParagraph">10.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18783203?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ghosh AK, Gemma S, Baldridge A, Wang YF, Kovalevsky AY, Koh Y, Weber IT, Mitsuya H.</a></p>

    <p class="plaintext">Flexible cyclic ethers/polyethers as novel P2-ligands for HIV-1 protease inhibitors: design, synthesis, biological evaluation, and protein-ligand X-ray studies.</p>

    <p class="plaintext">J Med Chem. 2008 Oct 9;51(19):6021-33. Epub 2008 Sep 11.</p>

    <p class="plaintext">PMID: 18783203 [PubMed - in process]</p>  

    <p class="ListParagraph">11.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18824350?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Zhan P, Liu X, Cao Y, Wang Y, Pannecouque C, De Clercq E.</a></p>

    <p class="plaintext">1,2,3-Thiadiazole thioacetanilides as a novel class of potent HIV-1 non-nucleoside reverse transcriptase inhibitors.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Oct 15;18(20):5368-71. Epub 2008 Sep 18.</p>

    <p class="plaintext">PMID: 18824350 [PubMed - in process]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
