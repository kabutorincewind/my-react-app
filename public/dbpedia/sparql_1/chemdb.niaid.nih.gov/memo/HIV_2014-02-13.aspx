

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-02-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="98rn85WthmgqeEnhV3qqbyacvAS0vHyopT1Z9aZEilCdrdD3zcGBlCniT+EACKrjeXE6SVRrzmHqkWwJaHJFAP26wUgCSqvJXCs03CfrHjpDfpVmfxJKj38hExk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="85F84F58" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: January 31 - February 13, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24378711">Specific Features of HIV-1 Integrase Inhibition by Bisphosphonate Derivatives.</a>Agapkina, J., D. Yanvarev, A. Anisenko, S. Korolev, J. Vepsalainen, S. Kochetkov, and M. Gottikh. European Journal of Medicinal Chemistry, 2014. 73: p. 73-82. PMID[24378711].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23957390">Discovery of HIV-1 Integrase Inhibitors: Pharmacophore Mapping, Virtual Screening, Molecular Docking, Synthesis, and Biological Evaluation.</a>Bhatt, H., P. Patel, and C. Pannecouque. Chemical Biology &amp; Drug Design, 2014. 83(2): p. 154-166. PMID[23957390].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24217696">Evaluation of PD 404,182 as an anti-HIV and anti-Herpes Simplex Virus Microbicide.</a>Chamoun-Emanuelli, A.M., M. Bobardt, B. Moncla, M.K. Mankowski, R.G. Ptak, P. Gallay, and Z. Chen. Antimicrobial Agents and Chemotherapy, 2014. 58(2): p. 687-697. PMID[24217696].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24412110">Discovery of MK-1439, an Orally Bioavailable Non-Nucleoside Reverse Transcriptase Inhibitor Potent against a Wide Range of Resistant Mutant HIV Viruses.</a>Cote, B., J.D. Burch, E. Asante-Appiah, C. Bayly, L. Bedard, M. Blouin, L.C. Campeau, E. Cauchon, M. Chan, A. Chefson, N. Coulombe, W. Cromlish, S. Debnath, D. Deschenes, K. Dupont-Gaudet, J.P. Falgueyret, R. Forget, S. Gagne, D. Gauvreau, M. Girardin, S. Guiral, E. Langlois, C.S. Li, N. Nguyen, R. Papp, S. Plamondon, A. Roy, S. Roy, R. Seliniotakis, M. St-Onge, S. Ouellet, P. Tawa, J.F. Truchon, J. Vacca, M. Wrona, Y. Yan, and Y. Ducharme. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 917-922. PMID[24412110].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24117013">A Combination Microbicide Gel Protects Macaques against Vaginal Simian Human Immunodeficiency Virus-Reverse Transcriptase Infection, but Only Partially Reduces Herpes Simplex Virus-2 Infection after a Single High-dose Cochallenge.</a>Hsu, M., M. Aravantinou, R. Menon, S. Seidor, D. Goldman, J. Kenney, N. Derby, A. Gettie, J. Blanchard, M. Piatak, Jr., J.D. Lifson, J.A. Fernandez-Romero, T.M. Zydowsky, and M. Robbiani. AIDS Research and Human Retroviruses, 2014. 30(2): p. 174-183. PMID[24117013].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24431237">Artificial Nucleobase-amino acid Conjugates: A New Class of TAR RNA Binding Agents.</a>Joly, J.P., G. Mata, P. Eldin, L. Briant, F. Fontaine-Vive, M. Duca, and R. Benhida. Chemistry, 2014. 20(7): p. 2071-2079. PMID[24431237].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24342709">Design and Synthesis of Novel Distamycin-modified Nucleoside Analogues as HIV-1 Reverse Transcriptase Inhibitors.</a> Li, C., C. Ma, J. Zhang, N. Qian, J. Ding, R. Qiao, and Y. Zhao. Antiviral research, 2014. 102: p. 54-60. PMID[24342709].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24411125">Structure-Activity Relationships of Diamine Inhibitors of Cytochrome P450 (CYP) 3A as Novel Pharmacoenhancers, Part I: Core Region.</a> Liu, H., L. Xu, H. Hui, R. Vivian, C. Callebaut, B.P. Murray, A. Hong, M.S. Lee, L.K. Tsai, J.K. Chau, K.M. Stray, C. Cannizzaro, Y.C. Choi, G.R. Rhodes, and M.C. Desai. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 989-994. PMID[24411125].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23327639">Design, Synthesis, Antimicrobial Activity and anti-HIV Activity Evaluation of Novel Hybrid Quinazoline-triazine Derivatives.</a> Modh, R.P., E. De Clercq, C. Pannecouque, and K.H. Chikhalia. Journal of Enzyme Inhibition and Medicinal Chemistry, 2014. 29(1): p. 100-108. PMID[23327639]. <b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24503884">Purified Human Breast Milk MUC1 and MUC4 Inhibit Human Immunodeficiency Virus.</a>Mthembu, Y., Z. Lotz, M. Tyler, C. de Beer, J. Rodrigues, L. Schoeman, and A.S. Mall. Neonatology, 2014. 105(3): p. 211-217. PMID[24503884].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24433967">Synthesis, Biophysical Characterization and anti-HIV Activity of d(TG3AG) Quadruplexes Bearing Hydrophobic Tails at the 5&#39;-End.</a> Romanucci, V., D. Milardi, T. Campagna, M. Gaglione, A. Messere, A. D&#39;Urso, E. Crisafi, C. La Rosa, A. Zarrelli, J. Balzarini, and G. Di Fabio. Bioorganic &amp; Medicinal Chemistry, 2014. 22(3): p. 960-966. PMID[24433967].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24412072">Structure-Activity Relationships of Diamine Inhibitors of Cytochrome P450 (CYP) 3A as Novel Pharmacoenhancers. Part II: P2/P3 Region and Discovery of Cobicistat (GS-9350).</a>Xu, L., H. Liu, A. Hong, R. Vivian, B.P. Murray, C. Callebaut, Y.C. Choi, M.S. Lee, J. Chau, L.K. Tsai, K.M. Stray, R.G. Strickley, J. Wang, L. Tong, S. Swaminathan, G.R. Rhodes, and M.C. Desai. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(3): p. 995-999. PMID[24412072].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0131-021314.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329194900034">Exceptionally Potent and Broadly Cross-reactive, Bispecific Multivalent HIV-1 Inhibitors Based on Single Human CD4 and Antibody Domains.</a>Chen, W.Z., Y. Feng, P. Prabakaran, T.L. Ying, Y.P. Wang, J.P. Sun, C.D.S. Macedo, Z.Y. Zhu, Y.X. He, V.R. Polonis, and D.S. Dimitrov. Journal of Virology, 2014. 88(2): p. 1125-1139. ISI[000329194900034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329161300005">A Candidate anti-HIV Reservoir Compound, Auranofin, Exerts a Selective &#39;Anti-memory&#39; Effect by Exploiting the Baseline Oxidative Status of Lymphocytes.</a> Chirullo, B., R. Sgarbanti, D. Limongi, I.L. Shytaj, D. Alvarez, B. Das, A. Boe, S. DaFonseca, N. Chomont, L. Liotta, E. Petricoin, S. Norelli, E. Pelosi, E. Garaci, A. Savarino, and A.T. Palamara. Cell Death &amp; Disease, 2013. 4; p. e944. ISI[000329161300005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329551100021">Diverse Modifications of the 4-Methylphenyl Moiety of TAK-779 by Late-stage Suzuki-Miyaura Cross-coupling.</a> Junker, A., D. Schepmann, J. Yamaguchi, K. Itami, A. Faust, K. Kopka, S. Wagner, and B. Wunsch. Organic &amp; Biomolecular Chemistry, 2014. 12(1): p. 177-186. ISI[000329551100021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329445700270">Substrate-specific Inactivation of CYP3A by the HIV Protease Inhibitors Ritonavir, Saquinavir and Amprenavir.</a> Kazmi, F., P. Yerino, K. Goodell, B. Kirby, B.W. Ogilvie, D.B. Buckley, and J. Unadkat. Drug Metabolism Reviews, 2014. 45: p. 133-134. ISI[000329445700270].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329331200024">Kadcotriones A-C: Tricyclic Triterpenoids from Kadsura coccinea.</a>Liang, C.Q., Y.M. Shi, X.Y. Li, R.H. Luo, Y. Li, Y.T. Zheng, H.B. Zhang, W.L. Xiao, and H.D. Sun. Journal of Natural Products, 2013. 76(12): p. 2350-2354. ISI[000329331200024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329188800003">Exosomes from Breast Milk Inhibit HIV-1 Infection of Dendritic Cells and Subsequent Viral Transfer to CD4(+) T Cells.</a> Naslund, T.I., D. Paquin-Proulx, P.T. Paredes, H. Vallhov, J.K. Sandberg, and S. Gabrielsson. AIDS, 2014. 28(2): p. 171-180. ISI[000329188800003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329607500036">Anti-HIV-1 Screening of (2E)-3-(2-Chloro-6-methyl/methoxyquinolin-3-yl)-1-(aryl)prop-2-en-1-ones.</a> Rizvi, S.U.F., M. Ahmad, M.H. Bukhari, C. Montero, P. Chatterjee, M. Detorio, and R. Schinazi. Medicinal Chemistry Research, 2014. 23(1): p. 402-407. ISI[000329607500036].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329144600018">Synthesis of Novel 2&#39;-Fluoro-5&#39;-deoxyphosphonic acids and bis(SATE) Adenine Analogue as Potent Antiviral Agents.</a> Shen, G.H. and J.H. Hong. Bulletin of the Korean Chemical Society, 2013. 34(12): p. 3621-3628. ISI[000329144600018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329579400004">The RIAD Peptidomimetic Inhibits HIV-1 Replication in Humanized NSG Mice.</a> Singh, M., P. Singh, D. Vaira, E.A. Torheim, S. Rahmouni, K. Tasken, and M. Moutschen. European Journal of Clinical Investigation, 2014. 44(2): p. 146-152. ISI[000329579400004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000329373400027">Synthesis and Studies of New 6-Halo(diphenyl)methyl- and 6-(Thiophen-2-ylmethyl)pyrimidin-4(3H)-ones as Possible HIV-1 Reverse Transcriptase Inhibitors.</a> Valuev-Elliston, V.T., A.V. Ivanov, B.S. Orlinson, E.N. Gerasimov, L.L. Brunilina, E.K. Zakharova, S.N. Kochetkov, I.A. Novakov, and M.B. Navrotskii. Russian Chemical Bulletin, 2013. 62(3): p. 797-801. ISI[000329373400027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0131-021314.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140116&amp;CC=WO&amp;NR=2014009794A1&amp;KC=A1">Preparation of Pyrrolo[2,3-B]pyridine Compounds as Antiviral Agents and Methods for Treating HIV.</a> De la Rosa, M.A., B.A. Johns, W.M. Kazmierski, V. Samano, L. Suwandi, D. Temelkoff, E. Velthuisen, and J.G. Weatherhead. Patent. 2014. 2013-IB15012014009794: 213pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140101&amp;CC=CN&amp;NR=103483272A&amp;KC=A">M-Diarene-pyrimidine Derivatives as HIV-1 Reverse Transcriptase Inhibitors and Their Preparation, Pharmaceutical Compositions and Use in the Treatment of HIV Infection.</a> Liu, X., X. Li, and P. Zhan. Patent. 2014. 2013-10456775103483272: 25pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140108&amp;CC=CN&amp;NR=103497146A&amp;KC=A">Preparation of 2-(N-Arylmethylpiperidine-4-ylamino)-4-(substituted-phenoxy)benzene Derivatives as HIV-1 Inhibitors.</a>Liu, X. and L. Zhang. Patent. 2014. 2013-10452018103497146: 29pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140116&amp;CC=WO&amp;NR=2014011769A1&amp;KC=A1">Macrocyclic Compounds as HIV Integrase Inhibitors.</a>Reger, T., A. Walji, J. Sanders, J. Wai, and L. Wang. Patent. 2014. 2013-US499222014011769: 84pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140116&amp;CC=WO&amp;NR=2014008636A1&amp;KC=A1">Preparation of Macrocyclic Compounds as HIV Integrase Inhibitors.</a>Reger, T., A.M. Walji, J.M. Sanders, J.S. Wai, L. Hu, S. Fang, L. Wang, P. Yang, Z. Ma, and L. Sun. Patent. 2014. 2012-CN784652014008636: 90pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140116&amp;CC=US&amp;NR=2014018288A1&amp;KC=A1">The Extracellular DING Protein from CD4-Positive T Cells for Use in the Treatment of Infection.</a> Simm, M. and R. Sachdeva. Patent. 2014. 2013-1394333720140018288: 46pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0131-021314.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
