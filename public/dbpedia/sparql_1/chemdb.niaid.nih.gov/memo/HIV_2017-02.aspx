

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-02.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eX1MRHjkngoOlv2zKAkEnFCEX6GOefUjdCrYIagoI+fWSuWy1YjZZqg11F/h/j87cFiS1U4tuOqBoz++8VRfumLJDGcXBtJjZ1w1nzZqzMCA+f/8U9bCGrQujhY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F8DE7B44" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: February, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27774687">Human CaaX Protease ZMPSTE24 Expressed in Yeast: Structure and Inhibition by HIV Protease Inhibitors.</a> Clark, K.M., J.L. Jenkins, N. Fedoriw, and M.E. Dumont. Protein Science, 2017. 26(2): p. 242-257. PMID[27774687]. PMCID[PMC5275749].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">2. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28151023">Emtricitabine + Tenofovir Alafenamide for the Treatment of HIV.</a> Corado, K.C. and E.S. Daar. Expert Opinion on Pharmacotherapy, 2017. 18(4): p. 427-432. PMID[28151023].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">3. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27815461">Antiviral and Antitumor Activity of Licorice Root Extracts.</a> Fukuchi, K., N. Okudaira, K. Adachi, R. Odai-Ide, S. Watanabe, H. Ohno, M. Yamamoto, T. Kanamoto, S. Terakubo, H. Nakashima, Y. Uesawa, H. Kagaya, and H. Sakagami. In Vivo, 2016. 30(6): p. 777-785. PMID[27815461].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000393045600016">Synthesis of New Derivatives of 5-(3,4-Dihydro-2H-pyrrol-5-yl)-pyrimidine.</a> Gasparyan, S.P., M.V. Alexanyan, G.K. Arutyunyan, S.L. Kocharov, A.H. Martirosyan, R.A. Tamazyan, A.G. Ayvazyan, H.A. Panosyan, and G.G. Danagulyan. Russian Journal of Organic Chemistry, 2016. 52(11): p. 1646-1653. ISI[000393045600016].
    <br />
    <b>[WOS]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">5. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28117606">Tenofovir Alafenamide, Emtricitabine, Elvitegravir, and Cobicistat Combination Therapy for the Treatment of HIV.</a> Imaz, A. and D. Podzamczer. Expert Review of Anti-Infective Therapy, 2017. 15(3): p. 195-209. PMID[28117606].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">6. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28218680">Application of the Triazolization Reaction to Afford Dihydroartemisinin Derivatives with anti-HIV Activity.</a> Jana, S., S. Iram, J. Thomas, M.Q. Hayat, C. Pannecouque, and W. Dehaen. Molecules, 2017. 22(E303): 13pp. PMID[28218680].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">7. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28167742">Structural and Preclinical Studies of Computationally Designed Non-Nucleoside Reverse Transcriptase Inhibitors for Treating HIV Infection.</a> Kudalkar, S.N., J. Beloor, A.H. Chan, W.G. Lee, W.L. Jorgensen, P. Kumar, and K.S. Anderson. Molecular Pharmacology, 2017. 91(4): p. 383-391. PMID[28167742].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000392864100012">Structural Modifications of Diarylpyrimidine-quinolone Hybrids as Potent HIV-1 NNRTIs with an Improved Drug Resistance Profile.</a> Mao, T.Q., Q.Q. He, W.X. Chen, G.F. Tang, F.E. Chen, E. De Clercq, D. Daelemans, and C. Pannecouque. Current Pharmaceutical Design, 2016. 22(46): p. 6982-6987. ISI[000392864100012].
    <br />
    <b>[WOS]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">9. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27992102">Natural Product Kuwanon-L Inhibits HIV-1 Replication through Multiple Target Binding.</a> Martini, R., F. Esposito, A. Corona, R. Ferrarese, E.R. Ceresola, L. Visconti, C. Tintori, A. Barbieri, A. Calcaterra, V. Iovine, F. Canducci, E. Tramontano, and M. Botta. ChemBioChem, 2017. 18(4): p. 374-377. PMID[27992102].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000392064500010">Anti-human Immunodeficiency Activity of Novel 2-Arylpyrrolidine Analogs.</a> Martirosyan, A.H., S.P. Gasparyan, M.V. Alexanyan, G.K. Harutyunyan, H.A. Panosyan, and R.F. Schinazi. Medicinal Chemistry Research, 2017. 26(1): p. 101-108. ISI[000392064500010].
    <br />
    <b>[WOS]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">11. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27766892">Isatin Thiazoline Hybrids as Dual Inhibitors of HIV-1 Reverse Transcriptase.</a> Meleddu, R., S. Distinto, A. Corona, E. Tramontano, G. Bianco, C. Melis, F. Cottiglia, and E. Maccioni. Journal of Enzyme Inhibition and Medicinal Chemistry, 2017. 32(1): p. 130-136. PMID[27766892].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">12. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28145455">M48U1 and Tenofovir Combination Synergistically Inhibits HIV Infection in Activated PBMCs and Human Cervicovaginal Histocultures.</a> Musumeci, G., I. Bon, D. Lembo, V. Cagno, M.C. Re, C. Signoretto, E. Diani, L. Lopalco, C. Pastori, L. Martin, G. Ponchel, D. Gibellini, and K. Bouchemal. Scientific Reports, 2017. 7(41018): 12pp. PMID[28145455]. PMCID[PMC5286506].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">13. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28218730">Neoflavonoids as Inhibitors of HIV-1 Replication by Targeting the Tat and NF-kappaB Pathways.</a> Olmedo, D.A., J.L. Lopez-Perez, E. Del Olmo, L.M. Bedoya, R. Sancho, J. Alcami, E. Munoz, A.S. Feliciano, and M.P. Gupta. Molecules, 2017. 22(321): 13pp. PMID[28218730].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">14. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28220857">SAMHD1 Enhances Nucleoside-analogue Efficacy against HIV-1 in Myeloid Cells.</a> Ordonez, P., S. Kunzelmann, H.C. Groom, M.W. Yap, S. Weising, C. Meier, K.N. Bishop, I.A. Taylor, and J.P. Stoye. Scientific Reports, 2017. 7(42824): 14pp. PMID[28220857]. PMCID[PMC5318888].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">15. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27763826">Anti-HIV Activities of Intramolecular G4 and Non-G4 Oligonucleotides.</a> Prokofjeva, M., V. Tsvetkov, D. Basmanov, A. Varizhuk, M. Lagarkova, I. Smirnov, K. Prusakov, D. Klinov, V. Prassolov, G. Pozmogova, and S.N. Mikhailov. Nucleic Acid Therapeutics, 2017. 27(1): p. 56-66. PMID[27763826].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">16. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28010138">Synthesis, Molecular Modeling and Biological Evaluation of Two New Chicoric acid Analogs.</a> Righi, G., R. Pelagalli, V. Isoni, I. Tirotta, R. Dallocchio, A. Dessi, B. Macchi, C. Frezza, I. Rossetti, and P. Bovicelli. Natural Product Research, 2017. 31(4): p. 397-403. PMID[28010138].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">17. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28112280">Tenofovir Alafenamide fumarate for the Treatment of HIV Infection.</a> Sampath, R., J. Zeuli, S. Rizza, and Z. Temesgen. Drugs of Today, 2016. 52(11): p. 617-625. PMID[28112280].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">18. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28163339">Synthesis and Antiviral Evaluation of 2&#39;,2&#39;,3&#39;,3&#39;-Tetrafluoro nucleoside Analogs.</a> Sari, O., L. Bassit, C. Gavegnano, T.R. McBrayer, L. McCormick, B. Cox, S.J. Coats, F. Amblard, and R.F. Schinazi. Tetrahedron Letters, 2017. 58(7): p. 642-644. PMID[28163339]. PMCID[PMC5289701].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">19. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27858001">Au(III) Compounds as HIV Nucleocapsid Protein (NCp7)-Nucleic acid Antagonists.</a> Spell, S.R., J.B. Mangrum, E.J. Peterson, D. Fabris, R. Ptak, and N.P. Farrell. Chemical Communications, 2017. 53(1): p. 91-94. PMID[27858001].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">20. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28027315">Non-human Primate Schlafen11 Inhibits Production of Both Host and Viral Proteins.</a> Stabell, A.C., J. Hawkins, M.Q. Li, X. Gao, M. David, W.H. Press, and S.L. Sawyer. Plos Pathogens, 2016. 12(12): e1006066. PMID[28027315]. PMCID[PMC5189954].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">21. <a href="https://www.ncbi.nlm.nih.gov/pubmed/28082070">Design, Synthesis and Biological Evaluation of (E)-3,4-Dihydroxystyryl-4-acylaminophenethyl sulfone, sulfoxide Derivatives as Dual Inhibitors of HIV-1 CCR5 and Integrase.</a> Sun, Y., W. Xu, N. Fan, X. Sun, X. Ning, L. Ma, J. Liu, and X. Wang. Bioorganic &amp; Medicinal Chemistry, 2017. 25(3): p. 1076-1084. PMID[28082070].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000392038900008">Chiral Phosphoric acid Catalyzed Enantioselective Addition of Thiols to in Situ Generated Ketimines: Synthesis of N,S-Ketals.</a> Unhale, R.A., N. Molleti, N.K. Rana, S. Dhanasekaran, S. Bhandary, and V.K. Singh. Tetrahedron Letters, 2017. 58(2): p. 145-151. ISI[000392038900008].
    <br />
    <b>[WOS]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">23. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27928921">Discovery of Natural Mouse Serum Derived HIV-1 Entry Inhibitor(s).</a> Wei, M., Y. Chen, J. Xi, S. Ru, M. Ji, D. Zhang, Q. Fang, and B. Tang. Acta Virologica, 2016. 60(4): p. 404-409. PMID[27928921].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">24. <a href="https://www.ncbi.nlm.nih.gov/pubmed/27993846">Nonnucleoside Reverse Transcriptase Inhibitors Reduce HIV-1 Production from Latently Infected Resting CD4+ T Cells Following Latency Reversal.</a> Zerbato, J.M., G. Tachedjian, and N. Sluis-Cremer. Antimicrobial Agents and Chemotherapy, 2017. 61(3): e01736-16. PMID[27993846].
    <br />
    <b>[PubMed]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000391451500024">Supramolecular Combinations of Humic Polyanions as Potent Microbicides with Polymodal anti-HIV-activities.</a> Zhernov, Y.V., S. Kremb, M. Helfer, M. Schindler, M. Harir, C. Mueller, N. Hertkorn, N.P. Avvakumova, A.I. Konstantinov, R. Brack-Werner, P. Schmitt-Kopplin, and I.V. Perminova. New Journal of Chemistry, 2017. 41(1): p. 212-224. ISI[000391451500024].
    <br />
    <b>[WOS]</b>. HIV_02_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170202&amp;CC=WO&amp;NR=2017017630A1&amp;KC=A1">Preparation of Novel Substituted Betulinic amide Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, P.R. Adulla, D.K. Gazula Levi, V. Mukkera, S. Neela, and S.V.L. Lanka. Patent. 2017. 2016-IB54505 2017017630: 170pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170209&amp;CC=WO&amp;NR=2017021922A1&amp;KC=A1">Preparation of Triterpene-amide Compounds as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, D.K. Gazula Levi, P.R. Adulla, S. Neela, and N. Mogili. Patent. 2017. 2016-IB54711 2017021922: 87pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025915A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Eastman, K.J., J.F. Kadow, B.N. Naidu, K.E. Parcella, M. Patel, P. Sivaprakasam, and Y. Tu. Patent. 2017. 2016-IB54830 2017025915: 275pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">29. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017006280A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Eastman, K.J., J.F. Kadow, B.N. Naidu, K.E. Parcella, M. Patel, and Y. Tu. Patent. 2017. 2016-IB54089 2017006280: 85pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">30. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170202&amp;CC=WO&amp;NR=2017017609A1&amp;KC=A1">Preparation of Betulin Derivatives for Preventing or Treating HIV Infections.</a> Johns, B.A. Patent. 2017. 2016-IB54458 2017017609: 105pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">31. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170112&amp;CC=WO&amp;NR=2017006281A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, M. Patel, J.L. Romine, D.R. St. Laurent, T. Wang, and Z. Zhang. Patent. 2017. 2016-IB54090 2017006281: 171pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">32. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017025864A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, J.L. Romine, P. Sivaprakasam, and D.R. St. Laurent. Patent. 2017. 2016-IB54688 2017025864: 204pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">33. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170126&amp;CC=US&amp;NR=2017020940A1&amp;KC=A1">Methods for Inhibiting HIV-1 Activity by Inhibitory Mechanisms of Extracts of Guaiacum officinale L. (Zygophyllaceae).</a> Lowe, H., N.J. Toyang, and J. Bryant. Patent. 2017. 2015-14805324 20170020940: 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p>

    <br />

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170216&amp;CC=WO&amp;NR=2017027434A1&amp;KC=A1">Preparation of Acyclic Nucleotide &#914;-amino acid ester phosphodiamides as Antiviral Agents.</a> Vachal, P., I. Raheem, Z. Guo, and T.J. Hartingh. Patent. 2017. 2016-US45946 2017027434: 76pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_02_2017.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
