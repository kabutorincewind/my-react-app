

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-311.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+G9Qn9kB+C5P7WrL6+t8ylDG1KCPtnSKeXKLxpJPl8/f6QM+742kbnK75xdRUWbtvBINUdFHyIyMW06sEZqjuZRkOS1QO3UqWSRF5cjao82WwWGlWmUrsQUYDuI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F01D713A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-311-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44470   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          New 2-bromomethyl-8-substituted-benzo[c]chromen-6-ones. Synthesis and biological properties</p>

    <p>          Garino, C, Bihel, F, Pietrancosta, N, Laras, Y, Quelever, G, Woo, I, Klein, P, Bain, J, Boucher, JL, and Kraus, JL</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(1): 135-138</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582426&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582426&amp;dopt=abstract</a> </p><br />

    <p>2.         44471   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Optimization of pyrrolidinone based HIV protease inhibitors</p>

    <p>          Sherrill, RG,  Andrews, CW, Bock, WJ, Davis-Ward, RG, Furfine, ES, Hazen, RJ, Rutkowske, RD, Spaltenstein, A, and Wright, LL</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(1): 81-84</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582415&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582415&amp;dopt=abstract</a> </p><br />

    <p>3.         44472   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          &quot;Virostatics&quot; as a Potential New Class of HIV Drugs</p>

    <p>          Kelly, LM, Lisziewicz, J, and Lori, F</p>

    <p>          Curr Pharm Des <b>2004</b>.  10(32): 4103-4120</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579091&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579091&amp;dopt=abstract</a> </p><br />

    <p>4.         44473   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Antiviral Drugs that Target Cellular Proteins May Play Major Roles in Combating HIV Resistance</p>

    <p>          Provencher, VM, Coccaro, E, Lacasse, JJ, and Schang, LM</p>

    <p>          Curr Pharm Des <b>2004</b>.  10(32): 4081-4101</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579090&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579090&amp;dopt=abstract</a> </p><br />

    <p>5.         44474   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Action of Anti-HIV Drugs and Resistance: Reverse Transcriptase Inhibitors and Protease Inhibitors</p>

    <p>          Imamichi, T</p>

    <p>          Curr Pharm Des  <b>2004</b>.  10(32): 4039-4053</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579086&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579086&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44475   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          CADA, a novel CD4-targeted HIV inhibitor, is synergistic with various anti-HIV drugs in vitro</p>

    <p>          Vermeire, K, Princen, K, Hatse, S, De Clercq, E, Dey, K, Bell, TW, and Schols, D</p>

    <p>          AIDS <b>2004</b>.  18(16): 2115-2125</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577644&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577644&amp;dopt=abstract</a> </p><br />

    <p>7.         44476   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Multiple mutations in human immunodeficiency virus-1 integrase confer resistance to the clinical trial drug S-1360</p>

    <p>          Fikkert, V, Hombrouck, A, Van Remoortel, B, De Maeyer, M, Pannecouque, C, De Clercq, E, Debyser, Z, and Witvrouw, M</p>

    <p>          AIDS <b>2004</b>.  18(15): 2019-2028</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577623&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577623&amp;dopt=abstract</a> </p><br />

    <p>8.         44477   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Correlations between factors determining the pharmacokinetics and antiviral activity of HIV-1 non-nucleoside reverse transcriptase inhibitors of the diaryltriazine and diarylpyrimidine classes of compounds</p>

    <p>          Lewi, Paul, Arnold, Eddy, Andries, Koen, Bohets, Hilde, Borghys, Herman, Clark, Arthur, Daeyaert, Frits, Das, Kalyan, de Bethune, Marie-Pierre, de Jonge, Marc, Heeres, Jan, Koymans, Luc, Leempoels, Jos, Peeters, Jef, Timmerman, Philip, Van den Broeck, Walter, Vanhoutte, Frederic, van&#39;t Klooster, Gerben, Vinkers, Maarten, Volovik, Yulia, and Janssen, Paul AJ</p>

    <p>          Drugs in R&amp;D <b>2004</b>.  5(5): 245-257</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.         44478   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Design of inhibitors against HIV, HTLV-I, and Plasmodium falciparum aspartic proteases</p>

    <p>          Abdel-Rahman, HM, Kimura, T, Hidaka, K, Kiso, A, Nezami, A, Freire, E, Hayashi, Y, and Kiso, Y</p>

    <p>          Biol Chem <b>2004</b>.  385(11): 1035-1039</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15576323&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15576323&amp;dopt=abstract</a> </p><br />

    <p>10.       44479   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Persulfated Molecular Umbrellas as Anti-HIV and Anti-HSV Agents</p>

    <p>          Jing, Bingwen, Janout, Vaclav, Herold, Betsy C, Klotman, Mary E, Heald, Taylor, and Regen, Steven L</p>

    <p>          Journal of the American Chemical Society <b>2004</b>.  126(49): 15930-15931</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>11.       44480   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p><b>          Structure-Activity Relationships in Platelet-Activating Factor. 12. Synthesis and Biological Evaluation of Platelet-Activating Factor Antagonists with Anti-HIV-1 Activity</b> </p>

    <p>          Serradji, Nawal, Martin, Marc, Bensaid, Okkacha, Cisternino, Salvatore, Rousselle, Christophe, Dereuddre-Bosquet, Nathalie, Huet, Jack, Redeuilh, Catherine, Lamouri, Aazdine, Dong, Chang-Zhi, Clayette, Pascal, Scherrmann, Jean-Michel, Dormont, Dominique, and Heymans, Francoise</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(25): 6410-6419</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>12.       44481   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Coccinin, an antifungal peptide with antiproliferative and HIV-1 reverse transcriptase inhibitory activities from large scarlet runner beans</p>

    <p>          Ngai, PH and Ng, TB</p>

    <p>          Peptides <b>2004</b>.  25(12): 2063-2068</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15572193&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15572193&amp;dopt=abstract</a> </p><br />

    <p>13.       44482   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          New constrained &quot;molecular tongs&quot; designed to dissociate HIV-1 protease dimer</p>

    <p>          Merabet, N, Dumond, J, Collinet, B, Van, Baelinghem L, Boggetto, N, Ongeri, S, Ressad, F, Reboud-Ravaux, M, and Sicsic, S</p>

    <p>          J Med Chem <b>2004</b>.  47(25): 6392-6400</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566308&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566308&amp;dopt=abstract</a> </p><br />

    <p>14.       44483   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Resveratrol glucuronides as the metabolites of resveratrol in humans: Characterization, synthesis, and anti-HIV activity</p>

    <p>          Wang, Lai-Xi, Heredia, Alonso, Song, Haijing, Zhang, Zhaojun, Yu, Biao, Davis, Charles, and Redfield, Robert</p>

    <p>          Journal of Pharmaceutical Sciences <b>2004</b>.  93(10): 2448-2457</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.       44484   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Antiviral effects of human immunodeficiency virus type 1-specific small interfering RNAs against targets conserved in select neurotropic viral strains</p>

    <p>          Dave, RS and Pomerantz, RJ</p>

    <p>          J Virol <b>2004</b> .  78(24): 13687-13696</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15564478&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15564478&amp;dopt=abstract</a> </p><br />

    <p>16.       44485   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Atazanavir: A new protease inhibitor to treat HIV infection</p>

    <p>          Musial, Bogdan L, Chojnacki, Joanna K, and Coleman, Craig I</p>

    <p>          American Journal of Health-System Pharmacy <b>2004</b>.  61(13): 1365-1374</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       44486   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Active site binding modes of the beta-diketoacids: a multi-active site approach in HIV-1 integrase inhibitor design</p>

    <p>          Dayam, R and Neamati, N</p>

    <p>          Bioorg Med Chem <b>2004</b>.  12(24): 6371-6381</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15556755&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15556755&amp;dopt=abstract</a> </p><br />

    <p>18.       44487   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          CoMFA 3D-QSAR Analysis of HIV-1 RT Nonnucleoside Inhibitors, TIBO Derivatives Based on Docking Conformation and Alignment</p>

    <p>          Zhou, Z and Madura, JD</p>

    <p>          J Chem Inf Comput Sci <b>2004</b>.  44(6): 2167-78</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15554687&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15554687&amp;dopt=abstract</a> </p><br />

    <p>19.       44488   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis and antiviral activity of P1&#39; arylsulfonamide azacyclic urea HIV protease inhibitors</p>

    <p>          Huang, Peggy P, Randolph, John T, Klein, Larry L, Vasavanonda, Sudthida, Dekhtyar, Tatyana, Stoll, Vincent S, and Kempf, Dale J</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(15): 4075-4078</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       44489   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Molecular Determinants of Multi-nucleoside Analogue Resistance in HIV-1 Reverse Transcriptases Containing a Dipeptide Insertion in the Fingers Subdomain: Effect of mutations D67N and T215Y on removal of thymidine nucleotide analogues from blocked DNA primers</p>

    <p>          Matamoros, Tania, Franco, Sandra, Vazquez-Alvarez, Blanca M, Mas, Antonio, Martinez, Miguel Angel, and Menendez-Arias, Luis</p>

    <p>          Journal of Biological Chemistry <b>2004</b>.  279(23): 24569-24577</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>21.       44490   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Secretory leukocyte protease inhibitor binds to annexin II, a cofactor for macrophage HIV-1 infection</p>

    <p>          Ma, G, Greenwell-Wild, T, Lei, K, Jin, W, Swisher, J, Hardegen, N, Wild, CT, and Wahl, SM</p>

    <p>          J Exp Med <b>2004</b>.  200(10): 1337-1346</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15545357&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15545357&amp;dopt=abstract</a> </p><br />

    <p>22.       44491   HIV-LS-311; PUBMED-HIV-12/14/2004</p>

    <p class="memofmt1-2">          Insights into saquinavir resistance in the G48V HIV-1 protease: quantum calculations and molecular dynamic simulations</p>

    <p>          Wittayanarakul, K, Aruksakunwong, O, Saen-Oon, S, Chantratita, W, Parasuk, V, Sompornpisut, P, and Hannongbua, S</p>

    <p>          Biophys J <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542562&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542562&amp;dopt=abstract</a> </p><br />

    <p>23.       44492   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Phosphoramidate Protides of Carbocyclic 2&#39;,3&#39;-Dideoxy-2&#39;,3&#39;-Didehydro-7-Deazaadenosine with Potent Activity Against HIV and HBV</p>

    <p>          Gudmundsson, Kristjan S, Wang, Zhicheng, Daluge, Susan M, Johnson, Lance C, Hazen, Richard, Condreay, Lynn D, and McGuigan, Christopher</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2004</b>.  23(12): 1929-1937</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       44493   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Properties of Arabino and Ribonucleosides of 1,3-Dideazaadenine, 4-Nitro-1, 3-dideazaadenine and Diketopiperazine</p>

    <p>          Sinha, Sarika, Srivastava, Richa, De Clercq, Erik, and Singh, Ramendra K</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2004</b>.  23(12): 1815-1824</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       44494   HIV-LS-311; WOS-HIV-12/05/2004</p>

    <p class="memofmt1-2">          New sesterterpenes from Madagascan Lendenfeldia sponges</p>

    <p>          Chill, L, Rudi, A, Aknin, M, Loya, S, Hizi, A, and Kashman, Y</p>

    <p>          TETRAHEDRON <b>2004</b>.  60(47): 10619-10626, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224715100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224715100003</a> </p><br />

    <p>26.       44495   HIV-LS-311; SCIFINDER-HIV-12/8/2004</p>

    <p class="memofmt1-2">          Effect of increasing thiolation of the polycytidylic acid strand of poly I:poly C on the a, b and g interferon-inducing properties, antiviral and antiproliferative activities</p>

    <p>          Chadha, Kailash C, Dembinski, Wlodzimierz E, Dunn, Christopher B, Aradi, Janos, Bardos, Thomas J, Dunn, Joseph A, and Ambrus, Julian L</p>

    <p>          Antiviral Research <b>2004</b>.  64(3): 171-177</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       44496   HIV-LS-311; WOS-HIV-12/05/2004</p>

    <p class="memofmt1-2">          Ceramide, a target for antiretroviral therapy</p>

    <p>          Finnegan, CM, Rawat, SS, Puri, A, Wang, JM, Ruscetti, FW, and Blumenthal, R</p>

    <p>          PROCEEDINGS OF THE NATIONAL ACADEMY OF SCIENCES OF THE UNITED STATES OF AMERICA <b>2004</b>.  101(43): 15452-15457, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224782400033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224782400033</a> </p><br />

    <p>28.       44497   HIV-LS-311; WOS-HIV-12/05/2004</p>

    <p class="memofmt1-2">          Docking strategies in drug design and it applications to HIV-1 Tat protein inhibitors</p>

    <p>          Kanyalkar, M, Coutinho, E, and Saran, A</p>

    <p>          NATIONAL ACADEMY SCIENCE LETTERS-INDIA <b>2004</b>.  27(9-10): 315-327, 15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224768500004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224768500004</a> </p><br />

    <p>29.       44498   HIV-LS-311; WOS-HIV-12/05/2004</p>

    <p class="memofmt1-2">          CXCR4-dependent infection of CD8(+), but not CD4(+), lymphocytes by a primary human immunodeficiency virus type 1 isolate</p>

    <p>          Zerhouni, B, Nelson, JAE, and Saha, K</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(22): 12288-12296, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224781400021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224781400021</a> </p><br />

    <p>30.       44499   HIV-LS-311; WOS-HIV-12/05/2004</p>

    <p class="memofmt1-2">          Inhibition of the human chemokine receptor CCR5 by variecolin and variecolol and isolation of four new variecolin analogues, emericolins A-D, from Emericella aurantiobrunnea</p>

    <p>          Yoganathan, K, Rossant, C, Glover, RP, Cao, SG, Vittal, JJ, Ng, S, Huang, YC, Buss, AD, and Butler, MS</p>

    <p>          JOURNAL OF NATURAL PRODUCTS <b>2004</b>.  67(10): 1681-1684, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224707700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224707700008</a> </p><br />
    <br clear="all">

    <p>31.       44500   HIV-LS-311; WOS-HIV-12/12/2004</p>

    <p class="memofmt1-2">          3-O-glutaryl-dihydrobetulin and related monoacyl derivatives as potent anti-HIV agents</p>

    <p>          Kashiwada, Y, Sekiya, M, Ikeshiro, Y, Fujioka, T, Kilgore, NR, Wild, CT, Allaway, GP, and Lee, KH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(23): 5851-5853, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224953300026">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224953300026</a> </p><br />

    <p>32.       44501   HIV-LS-311; WOS-HIV-12/12/2004</p>

    <p class="memofmt1-2">          Anti-AIDS agents. Part 62: Anti-HIV activity of 2 &#39;-substituted 4-methyl-3 &#39;,4 &#39;-di-O-(-)-camphanoyl-(+)-cis-khellactone (4-methyl DCK) analogs</p>

    <p>          Zhang, Q, Chen, Y, Xia, P, Xia, Y, Yang, ZY, Yu, DL, Morris-Natschke, SL, and Lee, KH</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2004</b>.  14(23): 5855-5857, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224953300027">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224953300027</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
