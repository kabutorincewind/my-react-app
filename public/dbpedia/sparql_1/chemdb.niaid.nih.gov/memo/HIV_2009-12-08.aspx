

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-12-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="L7F9QVy3kZ3pFxzyxQghdPrjnMLZPO3dFUQUuGPB/MoIAw7ByY/iAFrpq/dlMFEGwHSpIhTN+BWgITXlbsM9QsY/FCll/QmcMea6BdxtZIVfwl0gi57uPSRcDNg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="06E5C52A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 25 - December 8, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19962871?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Dammarenolic acid, a secodammarane triterpenoid from Aglaia sp. shows potent anti-retroviral activity in vitro.</a> Esimone CO, Eck G, Nworu CS, Hoffmann D, Uberla K, Proksch P. Phytomedicine. 2009 Dec 3. [Epub ahead of print]PMID: 19962871</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19962287?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=10">A dimeric high-molecular-weight chymotrypsin inhibitor with antitumor and HIV-1 reverse transcriptase inhibitory activities from seeds of Acacia confusa.</a> Lam SK, Ng TB. Phytomedicine. 2009 Dec 2. [Epub ahead of print]PMID: 19962287</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19961222?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=13">HIV-1 Protease Inhibitors with a Transition-State Mimic Comprising a Tertiary Alcohol: Improved Antiviral Activity in Cells.</a>Mahalingam AK, Axelsson L, Ekegren JK, Wannberg J, Kihlstro&#776;m J, Unge T, Wallberg H, Samuelsson B, Larhed M, Hallberg A. J Med Chem. 2009 Dec 4. [Epub ahead of print]PMID: 19961222</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19958026?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=16">A 1,8-Naphthyridone Derivative Targets the HIV-1 Tat-Mediated Transcription and Potently Inhibits the HIV-1 Replication.</a>Massari S, Daelemans D, Barreca ML, Knezevich A, Sabatini S, Cecchetti V, Marcello A, Pannecouque C, Tabarrini O. J Med Chem. 2009 Dec 3. [Epub ahead of print]PMID: 19958026</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19956941?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=17">A novel lectin with highly potent antiproliferative and HIV-1 reverse transcriptase inhibitory activities from cicada (Cicada flammata).</a> Ye XJ, Ng TB. Appl Microbiol Biotechnol. 2009 Dec 3. [Epub ahead of print]PMID: 19956941</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19956805?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=18">Novel galactonic acid-binding hexameric lectin from Hibiscus mutabilis seeds with antiproliferative and potent HIV-1 reverse transcriptase inhibitory activities.</a> Lam SK, Ng TB. Acta Biochim Pol. 2009 Dec 3. [Epub ahead of print]PMID: 19956805</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19954143?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=29">A General Approach to Chiral Building Blocks via Direct Amino Acid-Catalyzed Cascade Three-Component Reductive Alkylations: Formal Total Synthesis of HIV-1 Protease Inhibitors, Antibiotic Agglomerins, Brefeldin A, and (R)-gamma-Hexanolide.</a> Ramachary DB, Vijayendar Reddy Y. J Org Chem. 2009 Dec 2. [Epub ahead of print]PMID: 19954143</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19949058?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=44">BLOCKADE OF X4 TROPIC HIV-1 CELLULAR ENTRY BY GSK812397, A POTENT NON-COMPETITIVE CXCR4 RECEPTOR ANTAGONIST.</a>Jenkinson S, Thomson M, McCoy D, Edelstein M, Danehower S, Lawrence W, Wheelan P, Spaltenstein A, Gudmundsson K. Antimicrob Agents Chemother. 2009 Nov 30. [Epub ahead of print]PMID: 19949058</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19875507?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=82">In vitro synergistic activity against CCR5-tropic HIV-1 with combinations of potential candidate microbicide molecules HHA, KRV2110 and enfuvirtide (T20).</a> Jenabian MA, Saïdi H, Charpentier C, Van Herrewege Y, Son JC, Schols D, Balzarini J, Vanham G, Bélec L; ANRS Multi-Micro Project Study Group. J Antimicrob Chemother. 2009 Dec;64(6):1192-5. Epub 2009 Oct 29.PMID: 19875507</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19874035?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=84">Design of HIV protease inhibitors based on inorganic polyhedral metallacarboranes.</a> Rezácová P, Pokorná J, Brynda J, Kozísek M, Cígler P, Lepsík M, Fanfrlík J, Rezác J, Grantz Sasková K, Sieglová I, Plesek J, Sícha V, Grüner B, Oberwinkler H, Sedlácek&#39; J, Kräusslich HG, Hobza P, Král V, Konvalinka J. J Med Chem. 2009 Nov 26;52(22):7132-41.PMID: 19874035</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19800369?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=106">Antiviral evaluation of octadecyloxyethyl esters of (S)-3-hydroxy-2-(phosphonomethoxy)propyl nucleosides against herpesviruses and orthopoxviruses.</a> Valiaeva N, Prichard MN, Buller RM, Beadle JR, Hartline CB, Keith KA, Schriewer J, Trahan J, Hostetler KY. Antiviral Res. 2009 Dec;84(3):254-9. Epub 2009 Oct 1.PMID: 19800369</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19796851?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=108">Synthesis and anti-HIV-1 integrase activity of modified dinucleotides.</a> Aubert Y, Chassignol M, Roig V, Mbemba G, Weiss J, Meudal H, Mouscadet JF, Asseline U. Eur J Med Chem. 2009 Dec;44(12):5029-44. Epub 2009 Sep 9.PMID: 19796851</p>

    <p class="title">13.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19781821?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=114">Synthesis, pharmacological and antiviral activity of 1,3-thiazepine derivatives.</a> Struga M, Kossakowski J, Koziol AE, Kedzierska E, Fidecka S, La Colla P, Ibba C, Collu G, Sanna G, Secci B, Loddo R. Eur J Med Chem. 2009 Dec;44(12):4960-9. Epub 2009 Sep 6.PMID: 19781821</p>

    <p class="title">14.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19775780?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=123">Activity and molecular modeling of a new small molecule active against NNRTI-resistant HIV-1 mutants.</a> Carta A, Pricl S, Piras S, Fermeglia M, La Colla P, Loddo R. Eur J Med Chem. 2009 Dec;44(12):5117-22. Epub 2009 Sep 4.PMID: 19775780</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271950400033">Revised Structure of the Alkaloid Drymaritin.</a>  Wetzel, I; Allmendinger, L; Bracher, F.  JOURNAL OF NATURAL PRODUCTS, 2009; 72(10): 1908-1910.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271644800006">Influence of elastin-like peptide fusions on the quantity and quality of a tobacco-derived human immunodeficiency virus-neutralizing antibody.</a>  Floss, DM; Sack, M; Arcalis, E; Stadlmann, J; Quendler, H; Rademacher, T; Stoger, E; Scheller, J; Fischer, R; Conrad, U.  PLANT BIOTECHNOLOGY JOURNAL, 2009; 7(9): 899-913. </p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271644700004">Synthesis of new hetero- and carbocyclic aromatic amides of glycyrrhizic acid as potential anti-HIV agents.</a>  Kondratenko, RM; Baltina, LA; Baltina, LA; Plyasunova, OA; Pokrovskii, AG; Tolstikov, GA.  PHARMACEUTICAL CHEMISTRY JOURNAL, 2009; 43(7); 383-388.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271838100005">Structural investigation of HIV-1 nonnucleoside reverse transcriptase inhibitors: 2-Aryl-substituted benzimidazoles.</a>  Ziolkowska, NE; Michejda, CJ; Bujacz, GD.  JOURNAL OF MOLECULAR STRUCTURE, 2009; 937(1-3): 34-38.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
