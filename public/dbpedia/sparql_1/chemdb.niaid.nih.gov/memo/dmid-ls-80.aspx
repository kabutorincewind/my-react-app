

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-80.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="eK1Ac55jdr+zYye5syBR+hlOMVznZsxaa+YqC1zZCJxDVv5NSJhwlWUnke/j1N99h5xFXLrNXlMvzalBvAp/idLu40ZMcPhnFg+ZATy9nEHXoDthd53ro453CH8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F12DD370" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-80-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6581   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Evaluation of the antiherpetic activity of standardized extracts of Achyrocline satureioides</p>

    <p>          Bettega, JM, Teixeira, H, Bassani, VL, Barardi, CR, and Simoes, CM</p>

    <p>          Phytother Res <b>2004</b>.  18(10): 819-823</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15551398&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15551398&amp;dopt=abstract</a> </p><br />

    <p>2.         6582   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Inhibition of bovine viral diarrhea virus (BVDV) by mizoribine: synergistic effect of combination with interferon-alpha</p>

    <p>          Yanagida, K, Baba, C, and Baba, M</p>

    <p>          Antiviral Res <b>2004</b>.  64(3): 195-201</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550273&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15550273&amp;dopt=abstract</a> </p><br />

    <p>3.         6583   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Reactivation of Latent Epstein-Barr Virus by Methotrexate: A Potential Contributor to Methotrexate-Associated Lymphomas</p>

    <p>          Feng, WH, Cohen, JI, Fischer, S, Li, L, Sneller, M, Goldbach-Mansky, R, Raab-Traub, N, Delecluse, HJ, and Kenney, SC</p>

    <p>          J Natl Cancer Inst <b>2004</b>.  96(22): 1691-1702</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15547182&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15547182&amp;dopt=abstract</a> </p><br />

    <p>4.         6584   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Antiviral compounds from traditional Chinese medicines Galla Chinese as inhibitors of HCV NS3 protease</p>

    <p>          Duan, D, Li, Z, Luo, H, Zhang, W, Chen, L, and Xu, X</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(24): 6041-4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546725&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15546725&amp;dopt=abstract</a> </p><br />

    <p>5.         6585   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Inhibition of SARS-CoV replication by siRNA</p>

    <p>          Wu, Chang-Jer, Huang, Hui-Wen, Liu, Chiu-Yi, Hong, Cheng-Fong, and Chan, Yi-Lin</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DVBBK3-1/2/fa83a0ab4508c30101818c51888f9e9c">http://www.sciencedirect.com/science/article/B6T2H-4DVBBK3-1/2/fa83a0ab4508c30101818c51888f9e9c</a> </p><br />
    <br clear="all">

    <p>6.         6586   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Enhancement of antiviral activity against hepatitis C virus in vitro by interferon combination therapy</p>

    <p>          Okuse, Chiaki, Rinaudo, Jo Ann, Farrar, Kristine, Wells, Frances, and Korba, Brent E</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DS7NWT-1/2/2cfcb1e93455f408979d6ef6c847338e">http://www.sciencedirect.com/science/article/B6T2H-4DS7NWT-1/2/2cfcb1e93455f408979d6ef6c847338e</a> </p><br />

    <p>7.         6587   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Reduction of Hepatitis C Virus NS5A Hyperphosphorylation by Selective Inhibition of Cellular Kinases Activates Viral RNA Replication in Cell Culture</p>

    <p>          Neddermann, P, Quintavalle, M, Di, Pietro C, Clementi, A, Cerretani, M, Altamura, S, Bartholomew, L, and De, Francesco R</p>

    <p>          J Virol <b>2004</b>.  78(23): 13306-14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542681&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542681&amp;dopt=abstract</a> </p><br />

    <p>8.         6588   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A novel dry powder influenza vaccine and intranasal delivery technology: induction of systemic and mucosal immune responses in rats</p>

    <p>          Huang, J, Garmise, RJ, Crowder, TM, Mar, K, Hwang, CR, Hickey, AJ, Mikszta, JA, and Sullivan, VJ</p>

    <p>          Vaccine <b>2004</b>.  23(6): 794-801</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542204&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15542204&amp;dopt=abstract</a> </p><br />

    <p>9.         6589   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          In vitro anti-herpetic activity of sulfated polysaccharide fractions from Caulerpa racemosa</p>

    <p>          Ghosh, P, Adhikari, U, Ghosal, PK, Pujol, CA, Carlucci, MJ, Damonte, EB, and Ray, B</p>

    <p>          Phytochemistry  <b>2004</b>.  65(23): 3151-3157</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541745&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541745&amp;dopt=abstract</a> </p><br />

    <p>10.       6590   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          An RNA ligand inhibits hepatitis C virus NS3 protease and helicase activities</p>

    <p>          Fukuda, K, Umehara, T, Sekiya, S, Kunio, K, Hasegawa, T, and Nishikawa, S</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  325(3): 670-675</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541341&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       6591   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of novel 2&#39;, 3&#39;, 4&#39;-triply branched carbocyclic nucleosides as potential antiviral agents</p>

    <p>          Ko, OH and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2004</b>.  337(11): 579-586</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15540223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15540223&amp;dopt=abstract</a> </p><br />

    <p>12.       6592   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Synthesis of N(3),5&#39;-Cyclo-4-(beta-d-ribofuranosyl)-vic-triazolo[4,5-b]pyridin-5-one, a Novel Compound with Anti-Hepatitis C Virus Activity</p>

    <p>          Wang, P, Hollecker, L, Pankiewicz, KW, Patterson, SE, Whitaker, T, McBrayer, TR, Tharnish, PM, Sidwell, RW, Stuyver, LJ, Otto, MJ, Schinazi, RF, and Watanabe, KA</p>

    <p>          J Med Chem <b>2004</b>.  47(24): 6100-3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537363&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15537363&amp;dopt=abstract</a> </p><br />

    <p>13.       6593   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Synthesis, physicochemical and pharmacokinetic studies of potential prodrugs of beta-L-2&#39;-deoxycytidine, a selective and specific anti-HBV agent</p>

    <p>          Pierra, C, Benzaria, S, Dukhan, D, Loi, AG, La, Colla P, Bridges, E, Mao, J, Standring, D, Sommadossi, JP, and Gosselin, G</p>

    <p>          Antivir Chem Chemother <b>2004</b>.  15(5): 269-79</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15535049&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15535049&amp;dopt=abstract</a> </p><br />

    <p>14.       6594   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Treatment of mannan-enhanced influenza B virus infections in mice with oseltamivir, ribavirin and viramidine</p>

    <p>          Smee, DF, Wandersee, MK, Wong, MH, Bailey, KW, and Sidwell, RW</p>

    <p>          Antivir Chem Chemother <b>2004</b>.  15(5): 261-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15535048&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15535048&amp;dopt=abstract</a> </p><br />

    <p>15.       6595   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A Nucleotide Binding Motif in Hepatitis C Virus (Hcv) Ns4b Mediates Hcv Rna Replication</p>

    <p>          Einav, S. <i>et al.</i></p>

    <p>          Journal of Virology <b>2004</b>.  78(20): 11288-11295</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>16.       6596   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Prospects for the therapy and prevention of dengue virus infections</p>

    <p>          Damonte, EB, Pujol, CA, and Coto, CE</p>

    <p>          Adv Virus Res <b>2004</b>.  63: 239-85</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15530563&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15530563&amp;dopt=abstract</a> </p><br />

    <p>17.       6597   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          An N-Terminal Amphipathic Helix in Hepatitis C Virus (Hcv) Ns4b Mediates Membrane Association, Correct Localization of Replication Complex Proteins, and Hcv Rna Replication</p>

    <p>          Elazar, M. <i>et al.</i></p>

    <p>          Journal of Virology <b>2004</b>.  78(20): 11393-11400</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>18.       6598   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Adefovir dipivoxil: review of a novel acyclic nucleoside analogue</p>

    <p>          Danta, M and Dusheiko, G</p>

    <p>          Int J Clin Pract <b>2004</b>.  58(9): 877-86</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15529522&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15529522&amp;dopt=abstract</a> </p><br />

    <p>19.       6599   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Mapping of antigenic sites on the nucleocapsid protein of the severe acute respiratory syndrome coronavirus</p>

    <p>          He, Y, Zhou, Y, Wu, H, Kou, Z, Liu, S, and Jiang, S</p>

    <p>          J Clin Microbiol <b>2004</b>.  42(11): 5309-14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15528730&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15528730&amp;dopt=abstract</a> </p><br />

    <p>20.       6600   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          The non-nucleoside antiviral, BAY 38-4766, protects against cytomegalovirus (CMV) disease and mortality in immunocompromised guinea pigs</p>

    <p>          Schleiss, Mark R, Bernstein, David I, McVoy, Michael A, Stroup, Greg, Bravo, Fernando, Creasy, Blaine, McGregor, Alistair, Henninger, Kristin, and Hallenberger, Sabine</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DT2HJ9-1/2/c46250f0a975030af89eacd6ce45e7a3">http://www.sciencedirect.com/science/article/B6T2H-4DT2HJ9-1/2/c46250f0a975030af89eacd6ce45e7a3</a> </p><br />

    <p>21.       6601   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          The SARS-CoV S glycoprotein</p>

    <p>          Xiao, X and Dimitrov, DS</p>

    <p>          Cell Mol Life Sci <b>2004</b>.  61(19-20): 2428-30</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15526150&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15526150&amp;dopt=abstract</a> </p><br />

    <p>22.       6602   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Design, synthesis, and structural analysis of inhibitors of influenza neuraminidase containing a 2,3-disubstituted tetrahydrofuran-5-carboxylic acid core</p>

    <p>          Wang, Gary T, Wang, Sheldon, Gentles, Robert, Sowin, Thomas, Maring, Clarence J, Kempf, Dale J, Kati, Warren M, Stoll, Vincent, Stewart, Kent D, and Laver, Graeme</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DN9H40-7/2/f5ed83d1b43b4315e7586739adba2b8b">http://www.sciencedirect.com/science/article/B6TF9-4DN9H40-7/2/f5ed83d1b43b4315e7586739adba2b8b</a> </p><br />

    <p>23.       6603   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Herpes zoster virus infection</p>

    <p>          Liesegang, TJ</p>

    <p>          Curr Opin Ophthalmol <b>2004</b>.  15(6): 531-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523199&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523199&amp;dopt=abstract</a> </p><br />

    <p>24.       6604   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Dual-Action Mechanism of Viramidine Functioning as a Prodrug and as a Catabolic Inhibitor for Ribavirin</p>

    <p>          Wu, J., Larson, G., and Hong, Z.</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(10): 4006-4008</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       6605   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Gene transfer into hepatocytes mediated by herpes simplex virus-Epstein-Barr virus hybrid amplicons</p>

    <p>          Muller, Lars, Saydam, Okay, Saeki, Yoshinaga, Heid, Irma, and Fraefel, Cornel</p>

    <p>          Journal of Virological Methods <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T96-4DK6CVX-2/2/637796be75c29d387359945db9b19fc9">http://www.sciencedirect.com/science/article/B6T96-4DK6CVX-2/2/637796be75c29d387359945db9b19fc9</a> </p><br />

    <p>26.       6606   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Assembly and budding of influenza virus</p>

    <p>          Nayak, Debi P, Hui, Eric Ka-Wai, and Barman, Subrata</p>

    <p>          Virus Research  <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T32-4DGMY7T-1/2/58180a49ee2b248ba27dfbb0a8904197">http://www.sciencedirect.com/science/article/B6T32-4DGMY7T-1/2/58180a49ee2b248ba27dfbb0a8904197</a> </p><br />

    <p>27.       6607   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Safety and efficacy of alamifovir in patients with chronic hepatitis B virus infection</p>

    <p>          Soon, DK, Lowe, SL, Teng, CH, Yeo, KP, McGill, J, and Wise, SD</p>

    <p>          J Hepatol <b>2004</b>.  41(5): 852-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519660&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15519660&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.       6608   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          In vitro EBV-infected subline of KMH2, derived from Hodgkin lymphoma, expresses only EBNA-1, while CD40 ligand and IL-4 induce LMP-1 but not EBNA-2</p>

    <p>          Kis, LL, Nishikawa, J, Takahara, M, Nagy, N, Matskova, L, Takada, K, Elmberger, PG, Ohlsson, A, Klein, G, and Klein, E</p>

    <p>          Int J Cancer <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15514968&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15514968&amp;dopt=abstract</a> </p><br />

    <p>29.       6609   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Neuraminidase is important for the initiation of influenza virus infection in human airway epithelium</p>

    <p>          Matrosovich, MN, Matrosovich, TY, Gray, T, Roberts, NA, and Klenk, HD</p>

    <p>          J Virol <b>2004</b>.  78(22): 12665-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507653&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507653&amp;dopt=abstract</a> </p><br />

    <p>30.       6610   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Pyrimidine containing RSV fusion inhibitors</p>

    <p>          Nikitenko, Antonia, Raifeld, Yuri, Mitsner, Boris, and Newman, Howard</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DR87SW-7/2/811bd0f80fc2dd741b39b76edf59fda4">http://www.sciencedirect.com/science/article/B6TF9-4DR87SW-7/2/811bd0f80fc2dd741b39b76edf59fda4</a> </p><br />

    <p>31.       6611   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          SARS coronavirus 3CLpro N-terminus is indispensable for proteolytic activity but not for enzyme dimerization: Biochemical and thermodynamic investigation in conjunction with molecular dynamics simulations</p>

    <p>          Chen, S, Chen, L, Tan, J, Chen, J, Du, L, Sun, T, Shen, J, Chen, K, Jiang, H, and Shen, X</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15507456&amp;dopt=abstract</a> </p><br />

    <p>32.       6612   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          5&#39;-Homoaristeromycin. Synthesis and antiviral activity against orthopox viruses</p>

    <p>          Yang, Minmin and Schneller, Stewart W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DN9T3K-3/2/6214176b5ddcd912cf2260f5cd0357ca">http://www.sciencedirect.com/science/article/B6TF9-4DN9T3K-3/2/6214176b5ddcd912cf2260f5cd0357ca</a> </p><br />
    <br clear="all">

    <p>33.       6613   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A new seco-abietane-type diterpene from the stem bark of Picea glehni</p>

    <p>          Tanaka, R, Wada, S, Kinouchi, Y, Tokuda, H, and Matsunaga, S</p>

    <p>          Planta Med <b>2004</b>.  70(9): 877-80</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503357&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503357&amp;dopt=abstract</a> </p><br />

    <p>34.       6614   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          In vitro antiviral diterpenes from the Brazilian brown alga Dictyota pfaffii</p>

    <p>          Barbosa, JP, Pereira, RC, Abrantes, JL, Cirne, dos Santos CC, Rebello, MA, Frugulhetti, IC, and Texeira, VL</p>

    <p>          Planta Med <b>2004</b>.  70(9): 856-60</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503355&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15503355&amp;dopt=abstract</a> </p><br />

    <p>35.       6615   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Inhibition of subgenomic hepatitis C virus RNA in Huh-7 cells: ribavirin induces mutagenesis in HCV RNA</p>

    <p>          Kanda, T, Yokosuka, O, Imazeki, F, Tanaka, M, Shino, Y, Shimada, H, Tomonaga, T, Nomura, F, Nagao, K, Ochiai, T, and Saisho, H</p>

    <p>          J Viral Hepat <b>2004</b>.  11(6): 479-487</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500548&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500548&amp;dopt=abstract</a> </p><br />

    <p>36.       6616   DMID-LS-80; PUBMED-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Adefovir dipivoxil in chronic hepatitis B infection</p>

    <p>          Yuen, MF and Lai, CL</p>

    <p>          Expert Opin Pharmacother <b>2004</b>.  5(11): 2361-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500383&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500383&amp;dopt=abstract</a> </p><br />

    <p>37.       6617   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Novel imidazole substituted 6-methylidene-penems as broad-spectrum [beta]-lactamase inhibitors</p>

    <p>          Venkatesan, Aranapakam M, Agarwal, Atul, Abe, Takao, Ushirogochi, Hideki, Yamamura, Itsuki, Kumagai, Toshio, Petersen, Peter J, Weiss, William J, Lenoy, Eileen, and Yang, Youjun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2004</b>.  12(22): 5807-5817</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4DCD86D-2/2/8005931b90ba82bee590bcc6bf18fd0e">http://www.sciencedirect.com/science/article/B6TF8-4DCD86D-2/2/8005931b90ba82bee590bcc6bf18fd0e</a> </p><br />
    <br clear="all">

    <p>38.       6618   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A novel auto-cleavage assay for studying mutational effects on the active site of severe acute respiratory syndrome coronavirus 3C-like protease</p>

    <p>          Shan, Yu-Fei, Li, Shou-Feng, and Xu, Gen-Jun</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  324(2): 579-583</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4DFT24B-9/2/6978fa98bf18cf532e6fc6783d34aef9">http://www.sciencedirect.com/science/article/B6WBK-4DFT24B-9/2/6978fa98bf18cf532e6fc6783d34aef9</a> </p><br />

    <p>39.       6619   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Synthetic studies towards anti-SARS agents: application of an indium-mediated allylation of [alpha]-aminoaldehydes as the key step towards an intermediate</p>

    <p>          Chng, Shu-Sin, Hoang, Truong-Giang, Wayne Lee, Wei-Woon, Tham, Mun-Pun, Ling, Hui Yvonne, and Loh, Teck-Peng</p>

    <p>          Tetrahedron Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4DS7TCX-G/2/3de713af81624063a9b524f28327aae9">http://www.sciencedirect.com/science/article/B6THS-4DS7TCX-G/2/3de713af81624063a9b524f28327aae9</a> </p><br />

    <p>40.       6620   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Generation of predictive pharmacophore model for SARS-coronavirus main proteinase</p>

    <p>          Zhang, Xue Wu, Yap, Yee Leng, and Altmeyer, Ralf M</p>

    <p>          European Journal of Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4DR02BG-5/2/c2b04d9537e26ef029a63763dc826f4c">http://www.sciencedirect.com/science/article/B6VKY-4DR02BG-5/2/c2b04d9537e26ef029a63763dc826f4c</a> </p><br />

    <p>41.       6621   DMID-LS-80; EMBASE-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Cellular entry of the SARS coronavirus</p>

    <p>          Hofmann, Heike and Pohlmann, Stefan</p>

    <p>          Trends in Microbiology <b>2004</b>.  12(10): 466-472</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TD0-4D98JJG-1/2/ab00e1aac37b8c790728831e3b4cca70">http://www.sciencedirect.com/science/article/B6TD0-4D98JJG-1/2/ab00e1aac37b8c790728831e3b4cca70</a> </p><br />

    <p>42.       6622   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          From the Great Barrier Reef to a &quot;Cure&quot; for the Flu - Tall Tales, but True</p>

    <p>          Laver, G.</p>

    <p>          Perspectives in Biology and Medicine <b>2004</b>.  47(4): 590-596</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>43.       6623   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p><b>          Human Herpesvirus 6 Dna Polymerase: Enzymatic Parameters, Sensitivity to Ganciclovir and Determination of the Role of the a(961)V Mutation in Hhv-6 Ganciclovir Resistance</b> </p>

    <p>          De Bolle, L. <i>et al.</i></p>

    <p>          Antiviral Research <b>2004</b>.  64(1): 17-25</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>44.       6624   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Varicella-Zoster Virus Transfer to Skin by T Cells and Modulation of Viral Replication by Epidermal Cell Interferon-Alpha</p>

    <p>          Ku, C. <i>et al.</i></p>

    <p>          Journal of Experimental Medicine <b>2004</b>.  200(7): 917-925</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>45.       6625   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Herpes Simplex Virus Infections</p>

    <p>          Gross, G.</p>

    <p>          Hautarzt <b>2004</b>.  55(9): 818-+</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>46.       6626   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Varicella-Zoster Virus Infections</p>

    <p>          Lilie, H. and Wassilew, S.</p>

    <p>          Hautarzt <b>2004</b>.  55(9): 831-+</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>47.       6627   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Papillomavirus Diseases</p>

    <p>          Hengge, U.</p>

    <p>          Hautarzt <b>2004</b>.  55(9): 841-854</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>48.       6628   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          The Synthetic Development of the Anti-Influenza Neuraminidase Inhibitor Oseltamivir Phosphate (Tamiflu((R))): a Challenge for Synthesis &amp; Process Research</p>

    <p>          Abrecht, S. <i>et al.</i></p>

    <p>          Chimia <b>2004</b>.  58 (9): 621-629</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>49.       6629   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A Human in Vitro Model System for Investigating Genome-Wide Host Responses to Sars Coronavirus Infection</p>

    <p>          Ng, L. <i>et al.</i></p>

    <p>          Bmc Infectious Diseases <b>2004</b>.  4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>50.       6630   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Antiviral Therapy for Influenza - a Clinical and Economic Comparative Review</p>

    <p>          Schmidt, A.</p>

    <p>          Drugs <b>2004</b>.  64 (18): 2031-2046</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>51.       6631   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          Characterization of Protein-Protein Interactions Between the Nucleocapsid Protein and Membrane Protein of the Sars Coronavirus</p>

    <p>          He, R. <i>et al.</i></p>

    <p>          Virus Research  <b>2004</b>.  105(2): 121-125</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>52.       6632   DMID-LS-80; WOS-DMID-11/22/2004</p>

    <p class="memofmt1-2">          A Rapid Detection Method for Vaccinia Virus, the Surrogate for Smallpox Virus</p>

    <p>          Donaldson, K., Kramer, M., and Lim, D.</p>

    <p>          Biosensors &amp; Bioelectronics <b>2004</b>.  20(2): 322-327</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
