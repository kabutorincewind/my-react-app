

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-06-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9epX9TC9AfnuNcyO9vxqo25Upzt//mgWKYciorMlzf4CXUqOco3ZAK5aSBa3YUD2s03fgVTexcot0J8NV1iQMM1COIB8CwhjwAVdn6R08YycmteAlsMbfopwPTU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="998F88C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: June 7 - June 20, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23758814">Mutations in Variable Domains of the HIV-1 Envelope Gene Can Have a Significant Impact on Maraviroc and Vicriviroc Resistance.</a> Asin-Milan, O., A. Chamberland, Y. Wei, A. Haidara, M. Sylla, and C.L. Tremblay. AIDS Research and Therapy, 2013. 10(1): p. 15. PMID[23758814].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23755966">Identification and Antiviral Activity of Common Polymorphisms in the APOBEC3 Locus in Human Populations.</a> Duggal, N.K., W. Fu, J.M. Akey, and M. Emerman. Virology, 2013. <b>[Epub ahead of print]</b>. PMID[23755966].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23751259">A Novel Bivalent HIV-1 Entry Inhibitor Reveals Fundamental Differences in CCR5 - mu- opioid Receptor Interactions in Human Glia.</a> El-Hage, N., S.M. Dever, E.M. Podhaizer, C.K. Arnatt, Y. Zhang, and K.F. Hauser. AIDS, 2013. <b>[Epub ahead of print]</b>. PMID[23751259].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23749955">Identification of a Rare Mutation at Reverse Transcriptase Lys65 (K65e) in HIV-1-Infected Patients Failing on Nucleos(t)ide Reverse Transcriptase Inhibitors.</a> Fourati, S., B. Visseaux, D. Armenia, L. Morand-Joubert, A. Artese, C. Charpentier, P. Van Der Eede, G. Costa, S. Alcaro, M. Wirden, C.F. Perno, F. Ceccherini Silberstein, D. Descamps, V. Calvez, and A.G. Marcelin. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23749955].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23485336">Discovery and Structural Characterization of a New Inhibitor Series of HIV-1 Nucleocapsid Function: NMR Solution Structure Determination of a Ternary Complex Involving a 2:1 Inhibitor/NC Stoichiometry.</a> Goudreau, N., O. Hucke, A.M. Faucher, C. Grand-Maitre, O. Lepage, P.R. Bonneau, S.W. Mason, and S. Titolo. Journal of Molecular Biology, 2013. 425(11): p. 1982-1998. PMID[23485336].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23744599">Recent Findings on the Mechanisms Involved in Tenofovir Resistance.</a> Iyidogan, P. and K.S. Anderson. Antiviral Chemistry &amp; Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23744599].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23757338">Interferon Alpha Partially Inhibits HIV Replication in Hepatocytes in Vitro.</a> Kong, L. and J.T. Blackard. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23757338].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23762317">Identifying Chemicals with Potential Therapy of HIV Based on Protein-protein and Protein-chemical Interaction Network.</a> Li, B.Q., B. Niu, L. Chen, Z.J. Wei, T. Huang, M. Jiang, J. Lu, M.Y. Zheng, X.Y. Kong, and Y.D. Cai. PloS one, 2013. 8(6): p. e65207. PMID[23762317].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23758584">Design of Cell-permeable Stapled-peptides as HIV-1 Integrase Inhibitors.</a> Long, Y.Q., S.X. Huang, Z. Zawahir, Z.L. Xu, H.Y. Li, T.W. Sanchez, Y. Zhi, S. De Houwer, F. Christ, Z. Debyser, and N. Neamati. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23758584].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23742639">Dissecting the Pharmacophore of Curcumin. Which Structural Element Is Critical for Which Action?</a> Minassi, A., G. Sanchez-Duffhues, J.A. Collado, E. Munoz, and G. Appendino. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23742639].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23746300">Dissecting Cytochrome P450 3a4-ligand Interactions Using Ritonavir Analogues.</a> Sevrioukova, I.F. and T.L. Poulos. Biochemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23746300].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23744645">RIG-I Activation Inhibits HIV Replication in Macrophages.</a> Wang, Y., X. Wang, J. Li, Y. Zhou, and W. Ho. Journal of Leukocyte Biology, 2013. <b>[Epub ahead of print]</b>. PMID[23744645].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23665328">Heme Oxygenase-1 Induction Alters Chemokine Regulation and Ameliorates Human Immunodeficiency Virus-type-1 Infection in Lipopolysaccharide-stimulated Macrophages.</a> Zhou, Z.H., N. Kumari, S. Nekhai, K.A. Clouse, L.M. Wahl, K.M. Yamada, and S. Dhawan. Biochemical and Biophysical Research Communications, 2013. 435(3): p. 373-377. PMID[23665328].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23777356">Three New Isopimarane diterpenoids from Excoecaria acerifolia.</a> Huang, S.Z., Q.Y. Ma, W.W. Fang, F.Q. Xu, H. Peng, H.F. Dai, J. Zhou, and Y.X. Zhao. Journal of Asian Natural Products Research, 2013. <b>Epub ahead of print</b>. PMID[23777356].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17696512">Design and Synthesis of HIV-1 Protease Inhibitors Incorporating Oxazolidinones as P2/P2&#39; Ligands in Pseudosymmetric Dipeptide Isosteres.</a> Reddy, G.S., A. Ali, M.N. Nalam, S.G. Anjum, H. Cao, R.S. Nathans, C.A. Schiffer, and T.M. Rana. Journal of Medicinal Chemistry, 2007. 50(18): p. 4316-4328. PMID[17696512].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0607-062013.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317911500033">Drug Synergy of Tenofovir and Nanoparticle-based Antiretrovirals for HIV Prophylaxis.</a> Chaowanachan, T., E. Krogstad, C. Ball, and K.A. Woodrow. PloS One, 2013. 8(4): p. e61416. ISI[000317911500033].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000318404700017">Attenuation of HIV-1 Replication in Macrophages by Cannabinoid Receptor 2 Agonists.</a> Ramirez, S.H., N.L. Reichenbach, S.S. Fan, S. Rom, S.F. Merkel, X. Wang, W.Z. Ho, and Y. Persidsky. Journal of Leukocyte Biology, 2013. 93(5): p. 801-810. ISI[ 000318404700017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000317907200119">Estradiol Reduces Susceptibility of Cd4(+) T Cells and Macrophages to HIV-infection.</a> Rodriguez-Garcia, M., N. Biswas, M.V. Patel, F.D. Barr, S.G. Crist, C. Ochsenbauer, J.V. Fahey, and C.R. Wira. PloS one, 2013. 8(4): p. e62069. ISI[ 000317907200119].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318757800001">A Monoclonal Antibody against Lymphocyte Function-associated Antigen-1 Decreases HIV-1 Replication by Inducing the Secretion of an Antiviral Soluble Factor.</a> Rychert, J., L. Jones, G. McGrath, S. Bazner, and E.S. Rosenberg. Virology Journal, 2013. 10: p. 120. ISI[000318757800001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000316951900005">Transient Swelling, Spreading, and Drug Delivery by a Dissolved anti-HIV Microbicide-bearing Film.</a> Tasoglu, S., L.C. Rohan, D.F. Katz, and A.J. Szeri. Physics of Fluids, 2013. 25(3). ISI[ 000316951900005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>

    <br />

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318388300007">How Conformational Changes Can Affect Catalysis, Inhibition and Drug Resistance of Enzymes with Induced-fit Binding Mechanism Such as the HIV-1 Protease.</a> Weikl, T.R. and B. Hemmateenejad. Biochimica et Biophysica Acta-Proteins and Proteomics, 2013. 1834(5): p. 867-873. ISI[000318388300007].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0607-062013.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
