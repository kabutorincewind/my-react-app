

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-09-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jSNpwBeZX+q+NAYMIWZqWpYo+cttIAeBtHH8ZI90ByOtY73m3wQo+wgRq8T/Hgb2f0BJ2iJjaFopMe9UCciaEPB8UB6gB6MHN09imsEQ6vVILycOX8tvDMMhRSU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B4DCDFF5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  August 19, 2011 - September 1, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21859137">Evaluation of the Synthesis of Sialic Acid-PAMAM Glycodendrimers without the Use of Sugar Protecting Groups, and the Anti-HIV-1 Properties of These Compounds.</a> Clayton, R., J. Hardman, C.C. Labranche, and K.D. McReynolds. Bioconjugate Chemistry. 2011. <b>[Epub ahead of print]</b>; PMID[21859137].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21824782">Synthesis and Structure-Activity Relationship of Novel Diarylpyrimidines with Hydromethyl Linker (CH(OH)-DAPYs) as HIV-1 NNRTIs.</a> Gu, S.X., Q.Q. He, S.Q. Yang, X.D. Ma, F.E. Chen, E.D. Clercq, J. Balzarini, and C. Pannecouque. Bioorganic &amp; Medicinal Chemistry. 2011. 19(17): p. 5117-5124; PMID[21824782].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21766363">Synthesis of the Anti-HIV Agent (-)-Hyperolactone C by Using Oxonium Ylide Formation-Rearrangement.</a> Hodgson, D.M. and S. Man. Chemistry. 2011. 17(35): p. 9731-9737; PMID[21766363].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21860071">Potent anti-HIV-1 Activity of N-HR-derived Peptides Including a Deep Pocket-forming Region without Antagonistic Effects on T-20.</a> Izumi, K., K. Watanabe, S. Oishi, N. Fujii, M. Matsuoka, S.G. Sarafianos, and E.N. Kodama. Antiviral Chemistry &amp; Chemotherapy. 2011. 22(1): p. 51-55; PMID[21860071].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21664011">A Novel and Efficient One-pot Synthesis of Symmetrical Diamide (Bis-amidate) Prodrugs of Acyclic Nucleoside Phosphonates and Evaluation of Their Biological Activities.</a> Jansa, P., O. Baszczynski, M. Dracinsky, I. Votruba, Z. Zidek, G. Bahador, G. Stepan, T. Cihlar, R. Mackman, A. Holy, and Z. Janeba. European Journal of Medicinal Chemistry. 2011. 46(9): p. 3748-3754; PMID[21664011].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21853995">Efficient Discovery of Potent Anti-HIV Agents Targeting the Tyr181Cys Variant of HIV Reverse Transcriptase.</a> Jorgensen, W.L., M. Bollini, V.V. Thakur, R.A. Domaoal, K.A. Spasov, and K.S. Anderson. Journal of the American Chemical Society. 2011. <b>[Epub ahead of print]</b>; PMID[21853995].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21715497">Potent and Broad Anti-HIV-1 Activity Exhibited by a Glycosyl-Phosphatidylinositol-Anchored Peptide Derived from the CDR H3 of Broadly Neutralizing Antibody PG16.</a> Liu, L., M. Wen, W. Wang, S. Wang, L. Yang, Y. Liu, M. Qian, L. Zhang, Y. Shao, J.T. Kimata, and P. Zhou. Journal of Virology. 2011. 85(17): p. 8467-8476; PMID[21715497].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21741133">Synthesis and Structural Studies of Pentacycloundecane-based HIV-1 PR Inhibitors: A Hybrid 2D NMR and Docking/QM/MM/MD Approach.</a> Makatini, M.M., K. Petzold, S.N. Sriharsha, N. Ndlovu, M.E. Soliman, B. Honarparvar, R. Parboosing, A. Naidoo, P.I. Arvidsson, Y. Sayed, P. Govender, G.E. Maguire, H.G. Kruger, and T. Govender, European Journal of Medicinal Chemistry. 2011. 46(9): p. 3976-3985; PMID[21741133].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876054">MK-0536 Inhibits HIV-1 Integrases Resistant to Raltegravir.</a> Metifiot, M., B. Johnson, S. Smith, X.Z. Zhao, C. Marchand, T. Burke, S. Hughes, and Y. Pommier. Antimicrobial Agents and Chemotherapy. 2011. <b>[Epub ahead of print]</b>; PMID[21876054].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21876053">In Vitro Activity against Plasmodium falciparum of Antiretroviral Drugs.</a> Nsanzabana, C. and P.J. Rosenthal. Antimicrobial Agents and Chemotherapy. 2011. <b>[Epub ahead of print]</b>; PMID[21876053].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21746942">Novel Postentry Inhibitor of Human Immunodeficiency Virus Type 1 Replication Screened by Yeast Membrane-associated Two-hybrid System.</a> Urano, E., N. Kuramochi, R. Ichikawa, S.Y. Murayama, K. Miyauchi, H. Tomoda, Y. Takebe, M. Nermut, J. Komano, and Y. Morikawa. Antimicrobial Agents and Chemotherapy. 2011. 55(9): p. 4251-4260; PMID[21746942].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21700368">Synthesis, Antiviral Activity, Cytotoxicity and Cellular Pharmacology of l-3&#39;-Azido-2&#39;,3&#39;-dideoxypurine nucleosides.</a> Zhang, H.W., M. Detorio, B.D. Herman, S. Solomon, L. Bassit, J.H. Nettles, A. Obikhod, S.J. Tao, J.W. Mellors, N. Sluis-Cremer, S.J. Coats, and R.F. Schinazi. European Journal of Medicinal Chemistry. 2011. 46(9): p. 3832-3844; PMID[21700368].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

    <p> </p>

     <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21763726">The New and Less Toxic Protease Inhibitor Saquinavir-NO Maintains anti-HIV-1 Properties in Vitro Indistinguishable from Those of the Parental Compound Saquinavir.</a> Canducci, F., E.R. Ceresola, D. Saita, Y. Al-Abed, G. Garotta, M. Clementi, and F. Nicoletti. Antiviral Research. 2011. 91(3): p. 292-295; PMID[21763726].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21709091">Novel Trichomonacidal Spermicides.</a> Jain, A., N. Lal, L. Kumar, V. Verma, R. Kumar, V. Singh, R.K. Mishra, A. Sarswat, S.K. Jain, J.P. Maikhuri, V.L. Sharma, and G. Gupta. Antimicrobial Agents and Chemotherapy. 2011. 55(9): p. 4343-4351; PMID[21709091].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21804651">An Efficient Microwave Assisted Synthesis of Novel Class of Rhodanine Derivatives as Potential HIV-1 and JSP-1 Inhibitors.</a> Kamila, S., H. Ankati, and E.R. Biehl. Tetrahedron Letters. 2011. 52(34): p. 4375-4377; PMID[21804651].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21864279">Recent Developments of Peptidomimetic HIV-1 Protease Inhibitors.</a> Qiu, X. and Z.P. Liu. Curr Med Chem. 2011. <b>[Epub ahead of print]</b>; PMID[21864279].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21745701">Synthesis of New 2&#39;-Deoxy-2&#39;-fluoro-4&#39;-azido nucleoside Analogues as Potent anti-HIV Agents.</a> Wang, Q., W. Hu, S. Wang, Z. Pan, L. Tao, X. Guo, K. Qian, C.H. Chen, K.H. Lee, and J. Chang. European Journal of Medicinal Chemistry. 2011. 46(9): p. 4178-4183; PMID[21745701].</p>

     <p class="plaintext"><b>[Pubmed]</b>. HIV_0819-090111.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p> </p>

     <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293460700008">Synthesis and antimicrobial activity of new 1-[(tetrazol-5-yl)methyl] indole derivatives, their 1,2,4-triazole thioglycosides and acyclic analogs.</a> El-Sayed, W.A., R.E.A. Megeid, and H.A.S. Abbas. Archives of Pharmacal Research. 34(7): p. 1085-1096; ISI[000293460700008].</p>

     <p class="plaintext"><b>[WOS]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000293557800053">Highly Potent Chimeric Inhibitors Targeting Two Steps of HIV Cell Entry.</a> Zhao, B., M.K. Mankowski, B.A. Snyder, R.G. Ptak, and P.J. LiWang. Journal of Biological Chemistry. 286(32): p. 28370-28381; ISI[000293557800053].</p>

    <p class="NoSpacing"><b>[WOS]</b>. HIV_0819-090111.</p>

    <p> </p>

     <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000293503000031">Ethyl malonate amides: A diketo acid offspring fragment for HIV integrase inhibition.</a> Serafin, K., P. Mazur, A. Bak, E. Laine, L. Tchertanov, J.F. Mouscadet, and J. Polanski. Bioorganic &amp; Medicinal Chemistry. 19(16): p. 5000-5005; ISI[000293503000031].</p>

     <p class="plaintext"><b>[WOS]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p> </p>

     <p class="memofmt2-2">Patent citations:</p>

     <p> </p>

     <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110721&amp;CC=US&amp;NR=2011178092A1&amp;KC=A1">Preparation of sulfonamide compounds as HIV-1 protease inhibitors.</a> Ali, A., M.D. Altman, S.G. Anjum, H. Cao, S. Chellappan, M.X. Fernandes, M.K. Gilson, V. Kairys, N. King, E. Nalivaika, M. Prabu, T.M. Rana, K.K. Sai, C.A. Schiffer, and B. Tidor. Patent. 2011. 2007-960120 20110178092.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110804&amp;CC=US&amp;NR=2011190385A1&amp;KC=A1">Methods and products for reawakening retrocyclins.</a> Cole, A.M. Patent. 2011. 2010-699389 20110190385.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110804&amp;CC=US&amp;NR=2011190343A1&amp;KC=A1">Preparation of indole, benzimidazole, indoline, and isoindoline compounds as HIV-1 fusion inhibitors.</a> Gochin, M. and G. Zhou. Patent. 2011. 2011-6716 20110190343.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110728&amp;CC=WO&amp;NR=2011088549A1&amp;KC=A1">Derivatives of pyridoxine for inhibiting HIV integrase.</a> Stranix, B., J.-E. Bouchard, and G. Milot. Patent. 2011. 2011-CA41 2011088549.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110804&amp;CC=WO&amp;NR=2011094150A1&amp;KC=A1">Antiviral therapy for treatment of HIV infection.</a> Underwood, M.R. Patent. 2011. 2011-US22219 2011094150.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>

     <p> </p>

     <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;adjacent=true&amp;locale=en_gb&amp;FT=D&amp;date=20110728&amp;CC=WO&amp;NR=2011088561A1&amp;KC=A1">Anti-viral compounds and compositions.</a> Yao, X. and Z. Ao. Patent. 2011. 2011-CA70 2011088561.</p>

     <p class="plaintext"><b>[Patent]</b>. HIV_0819-090111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
