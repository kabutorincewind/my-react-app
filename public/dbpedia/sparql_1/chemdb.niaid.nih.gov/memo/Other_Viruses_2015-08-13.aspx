

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2015-08-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0fUz6AeqUzzvK5yI7bxP8iv8yG8AplPFUSJQUFva3O9myJYkRY0dZsam62Az1agl3c3NXJJ9KZeKPCD7JqttARa7dxPH2xXO6FuCZHhcE+KRFZzy41SqwqvFfOI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9D0D1410" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: July 31 - August 13, 2015</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26095852">Development of Novel Hepatitis B Virus Capsid Inhibitor Using in Silico Screening.</a> Hayakawa, M., H. Umeyama, M. Iwadate, T. Tanahashi, Y. Yano, M. Enomoto, A. Tamori, N. Kawada, and Y. Murakami. Biochemical and Biophysical Research Communications, 2015. 463(4): p. 1165-1175. PMID[26095852].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0731-081315.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26051832">Bioactivity-guided Isolation of anti-Hepatitis B Virus Active Sesquiterpenoids from the Traditional Chinese Medicine: Rhizomes of Cyperus rotundus.</a> Xu, H.B., Y.B. Ma, X.Y. Huang, C.A. Geng, H. Wang, Y. Zhao, T.H. Yang, X.L. Chen, C.Y. Yang, X.M. Zhang, and J.J. Chen. Journal of Ethnopharmacology, 2015. 171: p. 131-140. PMID[26051832].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0731-081315.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25882501">Isolation and anti-Hepatitis B Virus Activity of Dibenzocyclooctadiene ignans from the Fruits of Schisandra chinensis.</a> Xue, Y., X. Li, X. Du, X. Li, W. Wang, J. Yang, J. Chen, J. Pu, and H. Sun. Phytochemistry, 2015. 116: p. 253-261. PMID[25882501].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0731-081315.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26262613">Identification of Hydroxyanthraquinones as Novel Inhibitors of Hepatitis C Virus NS3 Helicase.</a> Furuta, A., M. Tsubuki, M. Endoh, T. Miyamoto, J. Tanaka, K.A. Salam, N. Akimitsu, H. Tani, A. Yamashita, K. Moriishi, M. Nakakoshi, Y. Sekiguchi, S. Tsuneda, and N. Noda. International Journal of Molecular Sciences, 2015. 16(8): p. 18439-18453. PMID[26262613].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0731-081315.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%25%20000357139000021">Structure-Activity Relationships of Neplanocin A Analogues as S-Adenosylhomocysteine hydrolase Inhibitors and their Antiviral and Antitumor Activities.</a> Chandra, G., Y.W. Moon, Y. Lee, J.Y. Jang, J. Song, A. Nayak, K. Oh, V.A. Mulamoottil, P.K. Sahu, G. Kim, T.S. Chang, M. Noh, S.K. Lee, S. Choi, and L.S. Jeong. Journal of Medicinal Chemistry, 2015. 58(12): p. 5108-5120. ISI[000357139000021].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0731-081315.</p><br /> 

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:%25%20000358099000019">Irbesartan, an FDA Approved Drug for Hypertension and Diabetic Nephropathy, is a Potent Inhibitor for Hepatitis B Virus Entry by Disturbing Na+-dependent Taurocholate Cotransporting Polypeptide Activity.</a> Wang, X.J., W. Hu, T.Y. Zhang, Y.Y. Mao, N.N. Liu, and S.Q. Wang. Antiviral Research, 2015. 120: p. 140-146. ISI[000358099000019].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0731-081315.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
