

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-06-21.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="62Se9IbAqrTxvXWqh0FQcw/GcjxMiGhwcDJoH5uVbzMnVpvx6KqZXre84bTl1g4YACq+gBOYbZ/2paV9YZqvlcMO+ZGiZe5wiVUjUe2ZbnWHnZBgf12bUDeW0ME=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D5AD7AC8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  June 8 - June 21, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22703590">Synthesis and Biological Evaluation of Tricyclic Guanidine Analogues of Batzelladine K for Antimalarial, Antileishmanial, Antibacterial, Antifungal and anti-HIV Activities.</a> Ahmed, N., K.G. Brahmbhatt, S.I. Khan, M. Jacob, B.L. Tekwani, S. Sabde, D. Mitra, I.P. Singh, I.A. Khan, and K.K. Bhutani. Chemical Biology &amp; Drug design, 2012. <b>[Epub ahead of print]</b>; PMID[22703590].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22687516">Amino-acid Derived 1,2-Benzisothiazolinone Derivatives as Novel Small Molecule Antifungal Inhibitors: Identification of Potential Genetic Targets.</a> Alex, D., F. Gay-Andrieu, J. May, L. Thampi, D. Dou, A. Mooney, W. Groutas, and R. Calderone. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22687516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22712717">Replacement of the Methylene of Dihydrochalcones with Oxygen: Synthesis and Biological Evaluation of 2-Phenoxyacetophenones.</a> Ansari, M., S. Emami, M.A. Khalilzadeh, M.T. Maghsoodlou, A. Foroumadi, M.A. Faramarzi, N. Samadi, and S.K. Ardestani. Chemical Biology &amp; Drug design, 2012. <b>[Epub ahead of print]</b>; PMID[22712717].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22698715">Synthesis and Antimicrobial Activity of Geranyloxy- and Farnesyloxy-acetophenone Derivatives against Oral Pathogens.</a> Bonifait, L., A. Marquis, S. Genovese, F. Epifano, and D. Grenier. Fitoterapia, 2012. <b>[Epub ahead of print]</b>; PMID[22698715].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22683494">Chemical Composition and Antimicrobial Activity of Phyllanthus muellerianus (Kuntze) Excel Essential Oil.</a> Brusotti, G., I. Cesari, G. Gilardoni, S. Tosi, P. Grisoli, A.M. Picco, and G. Caccialanza. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22683494].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22693023">The Antimicrobial Activity of the Alcohols from Musca domestica</a>. Golebiowski, M., M. Dawgul, W. Kamysz, M.I. Bogus, W. Wieloch, E. Wloka, M. Paszkiewicz, E. Przybysz, and P. Stepnowski. The Journal of Experimental Biology, 2012. <b>[Epub ahead of print]</b>; PMID[22693023].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22690956">An Isoquinoline Alkaloid, Berberine, Can Inhibit Fungal Alpha Amylase: Enzyme Kinetic and Molecular Modeling Studies.</a> Tintu, I., K.V. Dileep, A. Augustine, and C. Sadasivan. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22690956].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22691039">A New Bromobenzyl Methyl Sulphoxide from Marine Red Alga Symphyocladia latiuscula.</a> Xu, X., L. Yin, Y. Wang, S. Wang, and F. Song. Natural Product Research, 2012. <b>[Epub ahead of print]</b>; PMID[22691039].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p>
    <p> </p>
    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22691154">Discovery and Optimization of Benzotriazine di-N-Oxides Targeting Replicating and Non-Replicating Mycobacterium tuberculosis.</a> Chopra, S., G.A. Koolpe, A.A. Tambo-Ong, K.N. Matsuyama, K.J. Ryan, T.B. Tran, R.S. Doppalapudi, E.S. Riccio, L.V. Iyer, C.E. Green, B. Wan, S.G. Franzblau, and P.B. Madrid. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22691154].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704594">Antimicrobial Activity of Selected South African Medicinal Plants.</a> Nielsen, T.R.M., V.D. Kuete, A.K.P. Jager, J.J.P. Marion Meyer, and N.P. Lall. BMC Complementary and Alternative Medicine, 2012. 12(1): p. 74; PMID[22704594].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22691726">In Vitro and in Vivo Activity of Clofazimine against Mycobacterium tuberculosis Persisters.</a> Xu, J., Y. Lu, L. Fu, H. Zhu, B. Wang, K. Mdluli, A.M. Upton, H. Jin, M. Zheng, W. Zhao, and P. Li. The International Journal of Tuberculosis and Lung Disease : the Official Journal of the International Union against Tuberculosis and Lung Disease, 2012. <b>[Epub ahead of print]</b>; PMID[22691726].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p>
    <p> </p>
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22686608">Antimalarial Activity of Pyrroloiminoquinones from the Australian Marine Sponge Zyzzya sp.</a> Davis, R.A., M.S. Buchanan, S. Duffy, V.M. Avery, S.A. Charman, W.N. Charman, K.L. White, D.M. Shackleford, M.D. Edstein, K.T. Andrews, D. Camp, and R.J. Quinn. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22686608].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22704625">Selective and Specific Inhibition of the Plasmodium falciparum Lysyl-tRNA Synthetase by the Fungal Secondary Metabolite Cladosporin.</a> Hoepfner, D., C.W. McNamara, C.S. Lim, C. Studer, R. Riedl, T. Aust, S.L. McCormack, D.M. Plouffe, S. Meister, S. Schuierer, U. Plikat, N. Hartmann, F. Staedtler, S. Cotesta, E.K. Schmitt, F. Petersen, F. Supek, R.J. Glynne, J.A. Tallarico, J.A. Porter, M.C. Fishman, C. Bodenreider, T.T. Diagana, N.R. Movva, and E.A. Winzeler. Cell Host &amp; Microbe, 2012. 11(6): p. 654-663; PMID[22704625].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22708838">Lead Optimization of Anti-malarial Propafenone Analogs.</a> Lowes, D., A. Pradhan, L.V. Iyer, T. Parman, J. Gow, F. Zhu, A. Furimsky, A. Lemoff, W.A. Guiguemde, M. Sigal, J.A. Clark, E. Wilson, L. Tang, M.C. Connelly, J.L. Derisi, D.E. Kyle, J. Mirsalis, and R.K. Guy. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22708838].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22710117">Novel 4-Aminoquinoline Analogs Highly Active against the Blood and Sexual Stages of Plasmodium in Vivo and in Vitro.</a> Saenz, F.E., T. Mutka, K. Udenze, A.M. Oduola, and D.E. Kyle. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22710117].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p>
    <p> </p>
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22281151">Enrofloxacin is Able to Control Toxoplasma gondii Infection in Both in Vitro and in Vivo Experimental Models.</a> Barbosa, B.F., A.O. Gomes, E.A. Ferro, D.R. Napolitano, J.R. Mineo, and N.M. Silva. Veterinary Parasitology, 2012. 187(1-2): p. 44-52; PMID[22281151].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0608-062112.</p>
    <p> </p>
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304088100010">4H-1,4-Benzothiazine, dihydro-1,4-benzothiazinones and 2-Amino-5-fluorobenzenethiol Derivatives: Design, Synthesis and in Vitro Antimicrobial Screening.</a> Armenise, D., M. Muraglia, M.A. Florio, N. De Laurentis, A. Rosato, A. Carrieri, F. Corbo, and C. Franchini. Archiv der Pharmazie, 2012. 345(5): p. 407-416; ISI[000304088100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304291600006">2-Aminothiophenes as Building Blocks in Heterocyclic Synthesis: Synthesis and Antimicrobial Evaluation of a New Class of Pyrido[1,2-a]thieno[3,2-e]pyrimidine, Quinoline and Pyridin-2-one Derivatives.</a> Behbehani, H., H.M. Ibrahim, S. Makhseed, M.H. Elnagdi, and H. Mahmoud. European Journal of Medicinal Chemistry, 2012. 52: p. 51-65; ISI[000304291600006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304291900008">Synthesis of Novel Spirooxindole Derivatives by One Pot Multicomponent Reaction and Their Antimicrobial Activity.</a> Bhaskar, G., Y. Arun, C. Balachandran, C. Saikumar, and P.T. Perumal. European Journal of Medicinal Chemistry, 2012. 51: p. 79-91; ISI[000304291900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304135200017">Resveratrol Lacks Antifungal Activity against Candida albicans.</a> Collado-Gonzalez, M., J.P. Guirao-Abad, R. Sanchez-Fresneda, S. Belchi-Navarro, and J.C. Arguelles. World Journal of Microbiology &amp; Biotechnology, 2012. 28(6): p. 2441-2446; ISI[000304135200017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303980600021">Design, Synthesis and Biological Evaluation of Potent NAD(+)-dependent DNA Ligase Inhibitors as Potential Antibacterial Agents. Part I: Aminoalkoxypyrimidine Carboxamides.</a> Gu, W.X., T.S. Wang, F. Maltais, B. Ledford, J. Kennedy, Y.Y. Wei, C.H. Gross, J. Parsons, L. Duncan, S.J.R. Arends, C. Moody, E. Perola, J. Green, and P.S. Charifson. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(11): p. 3693-3698; ISI[000303980600021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304027500002">Enhanced Cellular Uptake of a New, in Silico Identified Antitubercular Candidate by Peptide Conjugation.</a> Horvati, K., B. Bacsa, N. Szabo, S. David, G. Mezo, V. Grolmusz, B. Vertessy, F. Hudecz, and S. Bosze. Bioconjugate Chemistry, 2012. 23(5): p. 900-907; ISI[000304027500002]. <b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304169300007">Pyrimidino piperazinyl acetamides: Innovative Class of Hybrid Acetamide Drugs as Potent Antimicrobial and Antimycobacterial Agents.</a> Kanagarajan, V. and M. Gopalakrishnan. Pharmaceutical Chemistry Journal, 2012. 46(1): p. 26-34; ISI[000304169300007].</p>
    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p>  
    <p> </p>
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304323300002">Synthesis, Characterization, Molecular Modeling, and Thermal Analyses of Bioactive Co(II) and Cu(II) Complexes with Diacetylmonoxime and Different Amines.</a> Khedr, A.M., M. Gaber, and H.A. Diab. Journal of Coordination Chemistry, 2012. 65(10): p. 1672-1684; ISI[000304323300002]. <b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303776600060">Screening and Isolation of Associated Bioactive Microorganisms from Fasciospongia cavernosa from of Visakhapatnam Coast, Bay of Bengal.</a> Kumar, P.S., E.R. Krishna, P. Sujatha, and B.V. Kumar. E-Journal of Chemistry, 2012. 9(4): p. 2166-2176; ISI[000303776600060].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303776600059">Synthesis of 2-(3-Methyl-2-oxoquinoxalin-1(2H)-yl)acetamide Based Thiazolidinone Derivatives as Potent Antibacterial and Antifungal Agents.</a> Kumar, S., N. Kumar, S. Drabu, S.A. Khan, O. Alam, M. Malhotra, and M.A. Minhaj. E-Journal of Chemistry, 2012. 9(4): p. 2155-2165; ISI[000303776600059].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304353200004">Antifungal Activity of Bivittoside-D from Bohadschia vitiensis (Semper).</a> Lakshmi, V., S. Srivastava, S.K. Mishra, and P.K. Shukla. Natural Product Research, 2012. 26(10): p. 913-918; ISI[000304353200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304322800012">Binuclear Cyclopalladated Compounds with Antitubercular Activity: Synthesis and Characterization of [{Pd(C<sup>2</sup>,N-dmba)(X)}<sub>2</sub>(mu-bpp) (X = Cl, Br, NCO, N<sub>3</sub>; bpp=1,3-bis(4-pyridyl)propane).</a> Moro, A.C., A.C. Urbaczek, E.T. De Almeida, F.R. Pavan, C.Q.F. Leite, A.V.G. Netto, and A.E. Mauro. Journal of Coordination Chemistry, 2012. 65(8): p. 1434-1442; ISI[000304322800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304387300013">Synthesis, Biological Evaluation and X-Ray Crystallographic Studies of Imidazo[1,2-a]pyridine-Based Mycobacterium tuberculosis Glutamine Synthetase Inhibitors.</a> Nordqvist, A., M.T. Nilsson, O. Lagerlunda, D. Muthas, J. Gising, S. Yahiaoui, L.R. Odell, B.R. Srinivasa, M. Larhed, S.L. Mowbray, and A. Karlen. MedChemComm, 2012. 3(5): p. 620-626; ISI[000304387300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304234200039">A Broad Study of Two New Promising Antimycobacterial Drugs: Ag(I) and Au(I) Complexes with 2-(2-Thienyl)benzothiazole.</a> Pereira, G.A., A.C. Massabni, E.E. Castellano, L.A.S. Costa, C.Q.F. Leite, F.R. Pavan, and A. Cuin. Polyhedron, 2012. 38(1): p. 291-296; ISI[000304234200039].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304083200023">Evaluation of Antimicrobial Efficacy of Flavonoids of Withania somnifera L.</a> Singh, G. and P. Kumar. Indian Journal of Pharmaceutical Sciences, 2011. 73(4): p. 473-477; ISI[000304083200023].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303776600021">Magnetic and Spectroscopic Studies of the Synthesized Metal Complexes of bis(Pyridine-2-carbo) hydrazide and Their Antimicrobial Studies.</a> Singh, N.P., Anu, and J. Singh. E-Journal of Chemistry, 2012. 9(4): p. 1835-1842; ISI[000303776600021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304083200016">Phytochemical and Antifungal Profiles of the Seeds of Carica papaya L.</a> Singh, O. and M. Ali. Indian Journal of Pharmaceutical Sciences, 2011. 73(4): p. 447-450; ISI[000304083200016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p> 
    <p> </p>
    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000303697400005">Enhancement of Antimicrobial Activity of Antibiotics and Antifungals by the Use of Natural Products from Pityrogramma calomelanos (L.)</a> Link. Souza, T.M., M.F.B. Morais-Braga, J.G.M. Costa, A.A.F. Saraiva, and H.D.M. Coutinho. Archives of Biological Sciences, 2012. 64(1): p. 43-48; ISI[000303697400005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0608-062112.</p>
        <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
