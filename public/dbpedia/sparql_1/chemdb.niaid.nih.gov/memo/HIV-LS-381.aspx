

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-381.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="H1nutJ7+62nKz0hNtoAZp2Fo2jq48FWsgqToEGd+npqugVxm7oSiUFGX8sNFYlloI0UhdyN+KRBIWzvu9471NMCQuRnE8mShTnmr4mKnYvKEXYHZZO/Wy1Tc/4A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF8A6DE9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-381-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61485   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          HIV fusion inhibitor peptides derived from HIV gp41 with improved pharmacological properties, solubility and stability, and anti-AIDS uses</p>

    <p>          Dwyer, John J, Bray, Brian L, Schneider, Stephen E, Zhang, Huyi, Tvermoes, Nicolai A, Johnston, Barbara E, and Friedrich, Paul E</p>

    <p>          PATENT:  US <b>2007179278</b>  ISSUE DATE:  20070802</p>

    <p>          APPLICATION: 2007-46550  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Trimeris, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61486   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Pyrimidine mononucleotide and mononucleoside compounds for use in therapy, particularly as antitumor and antiviral agents</p>

    <p>          Aradi, Janos, Fesues, Laszlo, and Beck, Zoltan</p>

    <p>          PATENT:  WO <b>2007083173</b>  ISSUE DATE:  20070726</p>

    <p>          APPLICATION: 2007  PP: 37pp.</p>

    <p>          ASSIGNEE:  (University of Debrecen, Hung.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61487   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Preparation of oxazole derivatives as antiviral agents</p>

    <p>          Bajji, Ashok C, Kim, Se-Ho, Trovato, Richard, Mchugh, Robert J, Markovitz, Benjamin, and Anderson, Mark B</p>

    <p>          PATENT:  WO <b>2007076161</b>  ISSUE DATE:  20070705</p>

    <p>          APPLICATION: 2006  PP: 116pp.</p>

    <p>          ASSIGNEE:  (Myriad Genetics, Inc USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61488   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Bis(trihydroxystyrenesulfonyl)methanes as viral entry inhibitors and preparation of related compounds as HIV integrase inhibitors</p>

    <p>          Gervay-Hague, Jacquelyn, Meadows, Christopher D, North, Thomas W, and Duong, Yen T</p>

    <p>          PATENT:  WO <b>2007065032</b>  ISSUE DATE:  20070607</p>

    <p>          APPLICATION: 2006  PP: 61pp.</p>

    <p>          ASSIGNEE:  (The Regents of the University of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61489   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Discovery and optimization of a natural HIV-1 entry inhibitor targeting the gp41 fusion peptide</p>

    <p>          Muench, Jan, Staendker, Ludger, Adermann, Knut, Schulz, Axel, Schindler, Michael, Chinnadurai, Raghavan, Poehlmann, Stefan, Chaipan, Chawaree, Biet, Thorsten, Peters, Thomas, Meyer, Bernd, Wilhelm, Dennis, Lu, Hong, Jing, Weiguo, Jiang, Shibo, Forssmann, Wolf-Georg, and Kirchhoff, Frank</p>

    <p>          Cell (Cambridge, MA, U. S.) <b>2007</b>.  129(2): 263-275</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61490   HIV-LS-381; WOS-HIV-8/10/2007</p>

    <p class="memofmt1-2">          Anti-Stress, Anti-Hiv and Vitamin C-Synergized Radical Scavenging Activity of Mulberry Juice Fractions</p>

    <p>          Sakagami, H. <i>et al.</i></p>

    <p>          In Vivo <b>2007</b>.  21(3): 499-505</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247165300009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247165300009</a> </p><br />

    <p>7.     61491   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          The a(1,2)-mannosidase I inhibitor 1-deoxymannojirimycin potentiates the antiviral activity of carbohydrate-binding agents against wild-type and mutant HIV-1 strains containing glycan deletions in gp120</p>

    <p>          Balzarini, Jan </p>

    <p>          FEBS Lett. <b>2007</b>.  581(10): 2060-2064</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     61492   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Molecular umbrellas: a novel class of candidate topical microbicides to prevent human immunodeficiency virus and herpes simplex virus infections</p>

    <p>          Madan, Rebecca Pellett, Mesquita, Pedro MM, Cheshenko, Natalia, Jing, Bingwen, Shende, Vikas, Guzman, Esmeralda, Heald, Taylor, Keller, Marla J, Regen, Steven L, Shattock, Robin J, and Herold, Betsy C</p>

    <p>          J. Virol. <b>2007</b>.  81(14): 7636-7646</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61493   HIV-LS-381; SCIFINDER-HIV-8/13/2007</p>

    <p class="memofmt1-2">          Preparation of amino acid hydrazide derivatives as HIV protease inhibitors</p>

    <p>          Sund, Christian, Lindborg, Bjoern, Kalayanov, Genadiy, Samuelsson, Bertil, Wallberg, Hans, and Hallberg, Anders</p>

    <p>          PATENT:  WO <b>2007048557</b>  ISSUE DATE:  20070503</p>

    <p>          APPLICATION: 2006  PP: 85pp.</p>

    <p>          ASSIGNEE:  (Medivir AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61494   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          Darunavir (TMC114): a new HIV-1 protease inhibitor</p>

    <p>          Molina, JM and Hill, A</p>

    <p>          Expert Opin Pharmacother <b>2007</b>.  8(12): 1951-1964</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696796&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696796&amp;dopt=abstract</a> </p><br />

    <p>11.   61495   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          Potent New Antiviral Compound Shows Similar Inhibition and Structural Interactions with Drug Resistant Mutants and Wild Type HIV-1 Protease</p>

    <p>          Wang, YF, Tie, Y, Boross, PI, Tozser, J, Ghosh, AK, Harrison, RW, and Weber, IT</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696515&amp;dopt=abstract</a> </p><br />

    <p>12.   61496   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          Design and Synthesis of HIV-1 Protease Inhibitors Incorporating Oxazolidinones as P2/P2&#39; Ligands in Pseudosymmetric Dipeptide Isosteres</p>

    <p>          Reddy, GS, Ali, A, Nalam, MN, Anjum, SG, Cao, H, Nathans, RS, Schiffer, CA, and Rana, TM</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696512&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696512&amp;dopt=abstract</a> </p><br />

    <p>13.   61497   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          HIV-1 protease and HIV-1 integrase inhibitory substances from Eclipta prostrata</p>

    <p>          Tewtrakul, S, Subhadhirasakul, S, Cheenpracha, S, and Karalai, C</p>

    <p>          Phytother Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696192&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17696192&amp;dopt=abstract</a> </p><br />

    <p>14.   61498   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          Characterization and Structural Analysis of Novel Mutations in HIV-1 Reverse Transcriptase Involved in the Regulation of Resistance to Non-Nucleoside Inhibitors</p>

    <p>          Ceccherini-Silberstein, F, Svicher, V, Sing, T, Artese, A, Santoro, MM, Forbici, F, Bertoli, A, Alcaro, S, Palamara, G, d&#39;Arminio, Monforte A, Balzarini, J, Antinori, A, Lengauer, T, and Perno, CF</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17686836&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17686836&amp;dopt=abstract</a> </p><br />

    <p>15.   61499   HIV-LS-381; PUBMED-HIV-8/20/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV replication: A powerful antiviral strategy by IFN-beta gene delivery in CD4(+) cells</p>

    <p>          Brule, F, Khatissian, E, Benani, A, Bodeux, A, Montagnier, L, Piette, J, Lauret, E, and Ravet, E</p>

    <p>          Biochem Pharmacol <b>2007</b>.  74(6): 898-910</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662695&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662695&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
