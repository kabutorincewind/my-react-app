

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-07-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="IVasGhA4G+1+1q79N07FQFsiUTBkNljLMw3av2fSWlfnsYdawsZmvbLMvAQmwOzfCX1pLp8Ko0dCuGIzfh+a8WagOSD11XwZa2FbLXvk2t/aJv72ryyxa8PdDGk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8D81ACBA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 6 - July 19, 2012</h1>

    <p class="memofmt2-2">PubMed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22786810">An Efficient Synthesis of a Hydroxyethylamine (HEA) Isostere and Its Alpha-aminophosphonate and Phosphoramidate Derivatives as Potential anti-HIV Agents</a><i>.</i> Bhattacharya, A.K., K.C. Rana, C. Pannecouque, and E. De Clercq, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22786810].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22804107">Ligand-free Suzuki Coupling of Arylboronic acids with Methyl (E)-4-bromobut-2-enoate: Synthesis of Unconventional Cores of HIV-1 Protease Inhibitors</a><i>.</i> Chiummiento, L., M. Funicello, P. Lupattelli, and F. Tramutola, Organic Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22804107].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22803661">Fragment Hopping Approach Directed at Design of HIV IN-LEDGF/p75 Interaction Inhibitors</a><i>.</i> De Luca, L., S. Ferro, F. Morreale, F. Christ, Z. Debyser, A. Chimirri, and R. Gitto, Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22803661].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22800535">Carbonylhydrazide-based Molecular Tongs Inhibit Wild-type and Mutated HIV-1 Protease Dimerization</a><i>.</i> Dufau, L., A.S. Marques Ressurreicao, R. Fanelli, N. Kihal, A. Vidu, T. Milcent, J.L. Soulier, J. Rodrigo De Losada, A. Desvergne, K. Leblanc, G. Bernadat, B. Crousse, M. Reboud-Ravaux, and S. Ongeri, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22800535].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22804908">NX43Inhibition of XMRV and HIV-1 Proteases by Pepstatin a and Acetyl-pepstatin</a><i>.</i> Matuz, K., J. Motyan, M. Li, A. Wlodawer, and J. Tozser, FEBS Journal, 2012. <b>[Epub ahead of print]</b>; PMID[22804908].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22771113">Use of ATP Analogs to Inhibit HIV-1 Transcription</a><i>.</i> Narayanan, A., G. Sampey, R. Van Duyne, I. Guendel, K. Kehn-Hall, J. Roman, R. Currer, H. Galons, N. Oumata, B. Joseph, L. Meijer, M. Caputi, S. Nekhai, and F. Kashanchi, Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22771113].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22708897">Design, Synthesis, and Biological and Structural Evaluations of Novel HIV-1 Protease Inhibitors to Combat Drug Resistance</a><i>.</i> Parai, M.K., D.J. Huggins, H. Cao, M.N. Nalam, A. Ali, C.A. Schiffer, B. Tidor, and T.M. Rana, Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22708897].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22808106">Small Molecule Inhibitors of the LEDGF Site of Human Immunodeficiency Virus Integrase Identified by Fragment Screening and Structure Based Design</a><i>.</i> Peat, T.S., D.I. Rhodes, N. Vandegraaff, G. Le, J.A. Smith, L.J. Clark, E.D. Jones, J.A. Coates, N. Thienthong, J. Newman, O. Dolezal, R. Mulder, J.H. Ryan, G.P. Savage, C.L. Francis, and J.J. Deadman, PLoS One, 2012. 7(7): p. e40147; PMID[22808106].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <h2> </h2>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305604100010">Synthesis and Structure-Activity Relationship Studies of HIV-1 Virion Infectivity Factor (vif) Inhibitors That Block Viral Replication</a><i>.</i> Ali, A., J.H. Wang, R.S. Nathans, H. Cao, N. Sharova, M. Stevenson, and T.M. Rana, ChemMedChem, 2012. 7(7): p. 1217-1229; ISI[000305604100010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305202300009">Amino Acid Derivatives of the (-) Enantiomer of Gossypol Are Effective Fusion Inhibitors of Human Immunodeficiency Virus Type 1</a><i>.</i> An, T., W.J. Ouyang, W. Pan, D.Y. Guo, J.R. Li, L.L. Li, G. Chen, J. Yang, S.W. Wu, and P. Tien, Antiviral Research, 2012. 94(3): p. 276-287; ISI[000305202300009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305356400006">Identification of HIV-1 Inhibitors Targeting the Nucleocapsid Protein</a><i>.</i> Breuer, S., M.W. Chang, J.Y. Yuan, and B.E. Torbett, Journal of Medicinal Chemistry, 2012. 55(11): p. 4968-4977; ISI[000305356400006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305356400062">Design, Synthesis, and X-ray Crystallographic Analysis of a Novel Class of HIV-1 Protease Inhibitors</a><i>.</i> Ganguly, A.K., S.S. Alluri, D. Caroccia, D. Biswas, C.H. Wang, E.H. Kang, Y. Zhang, A.T. McPhail, S.S. Carroll, C. Burlein, V. Munshi, P. Orth, and C. Strickland, Journal of Medicinal Chemistry, 2012. 55(11): p. 5647-5647; ISI[000305356400062].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305222000017">Vaginal Film Drug Delivery of the Pyrimidinedione IQP-0528 for the Prevention of HIV Infection</a><i>.</i> Ham, A.S., L.C. Rohan, A. Boczar, L. Yang, K.W. Buckheit, and R.W. Buckheit, Pharmaceutical Research, 2012. 29(7): p. 1897-1907; ISI[000305222000017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305295000014">Design, Synthesis and Evaluation of Unique 2,4,5-triaryl Imidazole Derivatives as Novel Potent Aspartic Protease Inhibitors</a><i>.</i> Khan, M.S., S. Akhtar, S.A. Siddiqui, M.S. Siddiqui, K.V. Srinivasan, and J.M. Arif, Medicinal Chemistry, 2012. 8(3): p. 428-435; ISI[000305295000014].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304894100025">Distinct Effects of Two HIV-1 Capsid Assembly Inhibitor Families That Bind the Same Site within the N-terminal Domain of the Viral CA Protein</a><i>.</i> Lemke, C.T., S. Titolo, U. von Schwedler, N. Goudreau, J.F. Mercier, E. Wardrop, A.M. Faucher, R. Coulombe, S.S.R. Banik, L. Fader, A. Gagnon, S.H. Kawai, J. Rancourt, M. Tremblay, C. Yoakim, B. Simoneau, J. Archambault, W.I. Sundquist, and S.W. Mason, Journal of Virology, 2012. 86(12): p. 6643-6655; ISI[000304894100025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304892700002">Isolation and Identification of a Novel Polysaccharide-peptide Complex with Antioxidant, Anti-proliferative and Hypoglycaemic Activities from the Abalone Mushroom</a><i>.</i> Li, N., L. Li, J.C. Fang, J.H. Wong, T.B. Ng, Y. Jiang, C.R. Wang, N.Y. Zhang, T.Y. Wen, L.Y. Qu, P.Y. Lv, R.L. Zhao, B. Shi, Y.P. Wang, X.Y. Wang, and F. Liu, Bioscience Reports, 2012. 32(3): p. 221-228; ISI[000304892700002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305086600025">Intravaginal Ring Delivery of Tenofovir Disoproxil Fumarate for Prevention of HIV and Herpes Simplex Virus Infection</a><i>.</i> Mesquita, P.M.M., R. Rastogi, T.J. Segarra, R.S. Teller, N.M. Torres, A.M. Huber, P.F. Kiser, and B.C. Herold, Journal of Antimicrobial Chemotherapy, 2012. 67(7): p. 1730-1738; ISI[000305086600025].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305381000006">Characterization of a Peptide Domain within the GB Virus C Envelope Glycoprotein (E2) That Inhibits HIV Replication</a><i>.</i> Xiang, J.H., J.H. McLinden, T.M. Kaufman, E.L. Mohr, N. Bhattarai, Q. Chang, and J.T. Stapleton, Virology, 2012. 430(1): p. 53-62; ISI[000305381000006].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="memofmt2-2">Latent HIV Reactivation</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22802445">BET Bromodomain Inhibition as a Novel Strategy for Reactivation of HIV-1</a><i>.</i> Banerjee, C., N. Archin, D. Michaels, A.C. Belkina, G.V. Denis, J. Bradner, P. Sebastiani, D.M. Margolis, and M. Montano, Journal of Leukocyte Biology, 2012. <b>[Epub ahead of print]</b>; PMID[22802445].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>

    <p> </p>

    <p class="memofmt2-2">Animal Studies</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22805180">Prolonged Tenofovir Treatment of Macaques Infected with K65R Reverse Transcriptase Mutants of SIV Results in the Development of Antiviral Immune Responses That Control Virus Replication after Drug Withdrawal</a><i>.</i> Van Rompay, K.K., K.A. Trott, K. Jayashankar, Y. Geng, C.C. Labranche, J.A. Johnson, G. Landucci, J. Lipscomb, R.P. Tarara, D.R. Canfield, W. Heneine, D.N. Forthal, D. Montefiori, and K. Abel, Retrovirology, 2012. 9(1): p. 57; PMID[22805180].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0706-071912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
