

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-01-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Gx6chn49k2d7r1fllMl0qEkvUrg+hOEW61nNry65dZGzA32TfMH0FrcCa+HOMuk3d2vqX9uHKzVWqTRbGn7jf0vYXNcRlAxS66eHa3Dw25Y5ioWIs1IomAbUQmA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D84558E6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:<br />January 4 - January 17, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23303813">Proangiogenic Growth Factors Potentiate in Situ Angiogenesis and Enhance Antifungal Drug Activity in Murine Invasive Aspergillosis.</a> Ben-Ami, R., N.D. Albert, R.E. Lewis, and D.P. Kontoyiannis. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>. PMID[23303813].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 

    <br />
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23292344">Antifungal Activity of Azole Compounds CPA18 and CPA109 against Azole-susceptible and -resistant Strains of Candida albicans.</a> Calabrese, E.C., S. Castellano, M. Santoriello, C. Sgherri, M.F. Quartacci, L. Calucci, A.G. Warrilow, D.C. Lamb, S.L. Kelly, C. Milite, I. Granata, G. Sbardella, G. Stefancich, B. Maresca, and A. Porta. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23292344].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 

    <br />
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23306152">Synthesis, Structure-Activity Relationships (SAR) and in Silico Studies of Coumarin Derivatives with Antifungal Activity.</a> de Araujo, R.S., F.Q. Guerra, O.L.E. de, C.A. de Simone, J.F. Tavares, L. Scotti, M.T. Scotti, T.M. de Aquino, R.O. de Moura, F.J. Mendonca, and J.M. Barbosa-Filho. International Journal of Molecular Sciences, 2013. 14(1): p. 1293-1309. PMID[23306152].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23313397">Evaluation of the Anticryptococcal Activity of the Antibiotic Polymyxin B in Vitro and in Vivo.</a> Zhai, B. and X. Lin. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>.PMID[23313397].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23305394">Current Prospects of Synthetic Curcumin Analogues and Chalcone Derivatives against Mycobacterium tuberculosis.</a> Abbas Bukhari, S.N., S.G. Franzblau, I. Jantan, and M. Jasamai. Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23305394].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23307663">Fueling Open-source Drug Discovery: 177 Small-molecule Leads against Tuberculosis.</a> Ballell, L., R.H. Bates, R.J. Young, D. Alvarez-Gomez, E. Alvarez-Ruiz, V. Barroso, D. Blanco, B. Crespo, J. Escribano, R. Gonzalez, S. Lozano, S. Huss, A. Santos-Villarejo, J.J. Martin-Plaza, A. Mendoza, M.J. Rebollo-Lopez, M. Remuinan-Blanco, J.L. Lavandera, E. Perez-Herran, F.J. Gamo-Benito, J.F. Garcia-Bustos, D. Barros, J.P. Castro, and N. Cammack. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23307663].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23301038">Synthesis, Antitubercular Activity and Mechanism of Resistance of Highly Effective Thiacetazone Analogues.</a> Coxon, G.D., D. Craig, R.M. Corrales, E. Vialla, L. Gannoun-Zaki, and L. Kremer. PloS One, 2013. 8(1): p. e53162. PMID[23301038].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23296917">Production of 3-Nitropropionic acid by Endophytic Fungus Phomopsis longicolla Isolated from Trichilia elegans A. JUSS ssp. elegans and Evaluation of Biological Activity.</a> Flores, A.C., J.A. Pamphile, M.H. Sarragiotto, and E. Clemente. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23296917].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23305444">Discovery and Evaluation of Novel Inhibitors of Mycobacterium Protein Tyrosine Phosphatase B from the 6-Hydroxy-benzofuran-5-carboxylic acid Scaffold.</a> He, Y., J. Xu, Z.H. Yu, A. Gunawan, L.L. Wu, L. Wang, and Z.Y. Zhang. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23305444].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23312605">Synergistic Activities of Tigecycline with Clarithromycin or Amikacin against Rapidly Growing Mycobacteria in Taiwan.</a> Huang, C.W., J.H. Chen, S.T. Hu, W.C. Huang, Y.C. Lee, C.C. Huang, and G.H. Shen. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23312605].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23306195">Thiolactomycin-based &#946;-Ketoacyl-AcpM Synthase A (KasA) Inhibitors: Fragment-based Inhibitor Discovery Using Transient 1D NOE NMR Spectroscopy.</a> Kapilashrami, K., G.R. Bommineni, C.A. Machutta, P. Kim, C.T. Lai, C. Simmerling, F. Picart, and P.J. Tonge. The Journal of Biological Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23306195].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23315266">Modification of Proportion Sensitivity Testing Method for Ethionamide.</a> Lakshmi, R., R. Ramachandran, K. Devika, G. Radhika, A. Syam Sundar, F. Rahman, N. Selvakumar, and V. Kumar. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23315266].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23291382">Antitubercular Activity and Subacute Toxicity of (-)-Licarin A in BALB/c Mice: A Neolignan Isolated from Aristolochia taliscana.</a> Leon-Diaz, R., M. Meckes-Fischer, L. Valdovinos-Martinez, H.R. Hernandez-Pando, M.G. Campos, and M.A. Jimenez-Arellanes. Archives of Medical Research, 2013. <b>[Epub ahead of print]</b>. PMID[23291382].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23295931">Activity of Drug Combinations against Mycobacterium tuberculosis Grown in Aerobic and Hypoxic Acidic Conditions.</a> Piccaro, G., F. Giannoni, P. Filippini, A. Mustazzolu, and L. Fattorini. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23295931].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23286234">Kinetic Mechanism and Inhibition of Mycobacterium tuberculosis D-Alanine:D-Alanine Ligase by the Antibiotic D-Cycloserine.</a> Prosser, G.A. and L.P. de Carvalho. The FEBS Journal, 2013. <b>[Epub ahead of print]</b>. PMID[23286234].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23312604">In Vitro Effect of Three-drug Combinations of Antituberculous Agents against Multidrug-resistant Mycobacterium tuberculosis Isolates.</a> Rey-Jurado, E., G. Tudo, J.P. de la Bellacasa, M. Espasa, and J. Gonzalez-Martin. International Journal of Antimicrobial Agents, 2013. <b>[Epub ahead of print]</b>. PMID[23312604].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23316686">Discovery of Novel Acetohydroxyacid Synthase Inhibitors as Active Agents against Mycobacterium tuberculosis by Virtual Screening and Bioassay.</a> Wang, D., X. Zhu, C. Cui, M. Dong, H. Jiang, Z. Li, Z. Liu, W. Zhu, and J.G. Wang. Journal of Chemical Information and Modeling, 2013. <b>[Epub ahead of print]</b>. PMID[23316686].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23237840">Synthesis and Antimycobacterial Evaluation of Pyrazinamide Derivatives with Benzylamino Substitution.</a> Zitko, J., P. Paterova, V. Kubicek, J. Mandikova, F. Trejtnar, J. Kunes, and M. Dolezal. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(2): p. 476-479. PMID[23237840].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23307699">Synthesis of a Potent Antimalarial Agent through Natural Products Conjugation.</a> Bruno, M., B. Trucchi, D. Monti, S. Romeo, M. Kaiser, and L. Verotta. ChemMedChem, 2013. <b>[Epub ahead of print]</b>. PMID[23307699].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23289711">New bis-Thiazolium Analogues as Potential Antimalarial Agents: Design, Synthesis, and Biological Evaluation.</a> Caldarelli, S.A., S. El Fangour, S. Wein, C. Tran van Ba, C. Perigaud, A. Pellet, H.J. Vial, and S. Peyrottes. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23289711].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23265884">Discovery and Preliminary Structure-Activity Relationship Analysis of 1,14-Sperminediphenylacetamides as Potent and Selective Antimalarial Lead Compounds.</a> Liew, L.P., M. Kaiser, and B.R. Copp. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(2): p. 452-454. PMID[23265884].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23225895">Plasmodium falciparum Drug Resistance Phenotype as Assessed by Patient Antimalarial Drug Levels and Its Association with pfmdr1 Polymorphisms.</a> Malmberg, M., P.E. Ferreira, J. Tarning, J. Ursing, B. Ngasala, A. Bjorkman, A. Martensson, and J.P. Gil. The Journal of Infectious Diseases, 2013. <b>[Epub ahead of print]</b>.PMID[23225895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23292347">Antimalarial Pharmacology and Therapeutics of Atovaquone.</a> Nixon, G.L., D.M. Moss, A.E. Shone, D.G. Lalloo, N. Fisher, P.M. O&#39;Neill, S.A. Ward, and G.A. Biagini. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23292347].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23228469">From a Cytotoxic Agent to the Discovery of a Novel Antimalarial Agent.</a> Singh, R.S., U. Das, J.M. Auschwitz, S.E. Leed, M.R. Hickman, J.R. Dimmock, and J. Alcorn. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(2): p. 584-587. PMID[23228469].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23320609">Antibacterial and Antiplasmodial Constituents of Beilschmiedia cryptocaryoides.</a> Talontsi, F.M., M. Lamshoft, J.O. Bauer, A.A. Razakarivony, B. Andriamihaja, C. Strohmann, and M. Spiteller. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23320609].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23215035">Alpha-heteroatom Derivatized Analogues of 3-(Acetylhydroxyamino)propyl phosphonic acid (FR900098) as Antimalarials.</a> Verbrugghen, T., P. Vandurm, J. Pouyez, L. Maes, J. Wouters, and S. Van Calenbergh. Journal of Medicinal Chemistry, 2013. 56(1): p. 376-380. PMID[23215035].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0104-011713.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23308231">All-trans Retinoic acid in Combination with Primaquine Clears Pneumocystis Infection.</a> Lei, G.S., C. Zhang, S. Shao, H.W. Jung, P.J. Durant, and C.H. Lee. PloS One, 2013. 8(1): p. e53479. PMID[23308231].
    <br />
    <b>[PubMed]</b>. OI_0104-011713.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311588100026">The Added Effect of Thioridazine in the Treatment of Drug-resistant Tuberculosis.</a> Amaral, L., Z. Udwadia, E. Abbate, and D. van Soolingen. International Journal of Tuberculosis and Lung Disease, 2012. 16(12): p. 1706-1708. ISI[000311588100026].
    <br />
    <b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312298300006">Synthesis and Antitubercular Evaluation of N-Arylpyrazine and N,N&#39;-Alkyl-diylpyrazine-2-carboxamide Derivatives.</a> Bispo, M.D.F., R.S.B. Goncalves, C.H.D. Lima, L.N.D. Cardoso, M.C.S. Lourenco, and M.V.N. de Souza. Journal of Heterocyclic Chemistry, 2012. 49(6): p. 1317-1322. ISI[000312298300006].
    <br />
    <b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312447100004">Computational Screening for New Inhibitors of M. tuberculosis Mycolyltransferases Antigen 85 Group of Proteins as Potential Drug Targets.</a> Gahoi, S., R.S. Mandal, N. Ivanisenko, P. Shrivastava, S. Jain, A.K. Singh, M.V. Raghunandanan, S. Kanchan, B. Taneja, C. Mandal, V.A. Ivanisenko, A. Kumar, R. Kumar, S. Ramachandran, and Open Source Drug Discovery Consortium. Journal of Biomolecular Structure &amp; Dynamics, 2013. 31(1): p. 30-43. ISI[000312447100004].
    <br />
    <b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312386800012">Azurophil Granule Proteins Constitute the Major Mycobactericidal Proteins in Human Neutrophils and Enhance the Killing of Mycobacteria in Macrophages.</a> Jena, P., S. Mohanty, T. Mohanty, S. Kallert, M. Morgelin, T. Lindstrom, N. Borregaard, S. Stenger, A. Sonawane, and O.E. Sorensen. PloS One, 2012. 7(12) : p. e50345. ISI[000312386800012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312265800003">Synthesis and Antimicrobial Activity of 3-Aryl-4,5,6,7-tetrahydroindazoles.</a> Pantyukhin, A.A., A.G. Mikhailovskii, G.A. Aleksandrova, R.R. Makhmudov, N.N. Pershina, and M.I. Vakhrin. Pharmaceutical Chemistry Journal, 2012. 46(8): p. 470-472. ISI[000312265800003].
    <br />
    <b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312298300020">Synthesis and Evaluation of Antimicrobial Activity of Some New Hetaryl-azoles Derivatives Obtained from 2-Aryl-4-methylthiazol-5-carbohydrazides and Isonicotinic acid hydrazide.</a> Tiperciuc, B., V. Zaharia, I. Colosi, C. Moldovan, O. Crisan, A. Pirnau, L. Vlase, M. Duma, and O. Oniga. Journal of Heterocyclic Chemistry, 2012. 49(6): p. 1407-1414. ISI[000312298300020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000312075.00013">Influence of Substituent Effects on Spectroscopic Properties and Antimicrobial Activity of 5-(4&#39;-Substituted phenylazo)-2-thioxothiazolidinone Derivatives.</a>  Abou-Dobara, M.I., A.Z. El-Sonbati, and S.M. Morgan. World Journal of Microbiology &amp; Biotechnology, 2013. 29(1): p. 119-126. ISI[000312075300013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311425000006">Enhancement of Commercial Antifungal Agents by Kojic acid.</a> Kim, J.H., P.K. Chang, K.L. Chan, N.C.G. Faria, N. Mahoney, Y.K. Kim, M.D. Martins, and B.C. Campbell. International Journal of Molecular Sciences, 2012. 13(11): p. 13867-13880. ISI[000311425000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311873600010">Synthesis and in Vitro Antimycobacterial and Isocitrate Lyase Inhibition Properties of Novel 2-Methoxy-2 &#39;-hydroxybenzanilides, Their Thioxo Analogues and Benzoxazoles.</a> Kozic, J., E. Novotna, M. Volkova, J. Stolarikova, F. Trejtnar, V. Wsol, and J. Vinsova. European Journal of Medicinal Chemistry, 2012. 56: p. 108-119. ISI[000311873600010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p> 
    
    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000311859100028">Microbial Transformation of Antimalarial Terpenoids</a>. Parshikov, I.A., A.I. Netrusov, and J.B. Sutherland. Biotechnology Advances, 2012. 30(6): p. 1516-1523. ISI[000311859100028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0104-011713.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
