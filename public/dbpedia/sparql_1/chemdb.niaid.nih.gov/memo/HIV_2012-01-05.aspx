

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-01-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="y/Ze8/D5aQBM/Q7T9EA4wfR97uJK323oBLLRKBscXmQ+p9g+pkLel7/yYGa5u9tT489/egPOROUVae2R0qQgxaOEgaSRFmixFRBOIPhjLW2I7M7Fn5h3o9faOis=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3ED2F999" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  December 9, 2011 - December 22, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22180752">Structural Basis for the Antiviral Activity of BST-2/Tetherin and Its Viral Antagonism.</a> Arias, J.F., Y. Iwabu, and K. Tokunaga, Frontiers in Microbiology, 2011. 2: p. 250; PMID[22180752].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22179573">Synthesis of the Pyridinyl Analogues of Dibenzylideneacetone (pyr-dba) via an Improved Claisen-Schmidt Condensation, Displaying Diverse Biological Activities as Curcumin Analogues.</a> Cao, B., Y. Wang, K. Ding, N. Neamati, and Y.Q. Long, Organic &amp; Biomolecular Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22179573].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22071527">Synthesis, Structure-Activity Relationships, and Mechanism of Action of anti-HIV-1 Lamellarin alpha 20-sulfate Analogues.</a> Kamiyama, H., Y. Kubo, H. Sato, N. Yamamoto, T. Fukuda, F. Ishibashi, and M. Iwao, Bioorganic &amp; Medicinal Chemistry, 2011. 19(24): p. 7541-7550; PMID[22071527].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22160935">Ribosome Inactivating Proteins from Plants Inhibiting Viruses.</a> Kaur, I., R.C. Gupta, and M. Puri, Virologica Sinica, 2011. 26(6): p. 357-365; PMID[22160935].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21990112">Synthesis and Anti-HIV Activity of Aryl-2-[(4-cyanophenyl)amino]-4-pyrimidinone hydrazones as Potent Non-nucleoside Reverse Transcriptase Inhibitors.</a> Ma, X.D., S.Q. Yang, S.X. Gu, Q.Q. He, F.E. Chen, E. De Clercq, J. Balzarini, and C. Pannecouque, ChemMedChem, 2011. 6(12): p. 2225-2232; PMID[21990112].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22173432">Interfacial Inhibitors: Targeting Macromolecular Complexes.</a> Pommier, Y. and C. Marchand, Nature Reviews. Drug Discovery, 2011. <b>[Epub ahead of print]</b>; PMID[22173432].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21986114">Surface Modified Nevirapine Nanosuspensions for Viral Reservoir Targeting: In Vitro and in Vivo Evaluation.</a> Shegokar, R. and K.K. Singh, International Journal of Pharmaceutics, 2011. 421(2): p. 341-352; PMID[21986114].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22155823">The Mechanism of Interaction of Human Mitochondrial DNA Polymerase gamma with the Novel Nucleoside Reverse Transcriptase Inhibitor 4&#39; -Ethynyl-2-fluoro-2&#39; -deoxyadenosine Indicates a Low Potential for Host Toxicity.</a> Sohl, C.D., K. Singh, R. Kasiviswanathan, W.C. Copeland, H. Mitsuya, S.G. Sarafianos, and K.S. Anderson, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22155823].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22123961">Rapid Development of Glycan-specific, Broad, and Potent anti-HIV-1 gp120 Neutralizing Antibodies in an R5 SIV/HIV Chimeric Virus Infected Macaque.</a> Walker, L.M., D. Sok, Y. Nishimura, O. Donau, R. Sadjadpour, R. Gautam, M. Shingai, R. Pejchal, A. Ramos, M.D. Simek, Y. Geng, I.A. Wilson, P. Poignard, M.A. Martin, and D.R. Burton, Proceedings of the  National Academy of Sciences of the United States of America 2011. 108(50): p. 20125-20129; PMID[22123961].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19546363">Potent Activity of a Nucleoside Reverse Transcriptase Inhibitor, 4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine, against Human Immunodeficiency Virus Type 1 Infection in a Model Using Human Peripheral Blood Mononuclear Cell-transplanted NOD/SCID Janus Kinase 3 knockout Mice.</a> Hattori, S., K. Ide, H. Nakata, H. Harada, S. Suzu, N. Ashida, S. Kohgo, H. Hayakawa, H. Mitsuya, and S. Okada, Antimicrobial Agents and Chemotherapy, 2009. 53(9): p. 3887-3893; PMID[19546363].
    <br />
    <b>[PubMed]</b>. HIV_1209-122211.</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297136200003">Synthesis and Transformations of Novel Formyl-substituted Quinolines.</a> Aleksanyan, I.S., K Panosyan, H, Heterocyclic Communications, 2011. 17(3-4): p. 105-110; ISI[000297136200003].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297151800007">Anti-HIV Activity of Natural Triterpenoids and Hemisynthetic Derivatives 2004-2009.</a> Cassels, B.K. and M. Asencio, Phytochemistry Reviews, 2011. 10(4): p. 545-564; ISI[000297151800007].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296973000051">Silver Nanoparticles as Potential Antiviral Agents.</a> Galdiero, S.F., A Vitiello, M Cantisani, M Marra, V Galdiero, M, Molecules, 2011. 16(10): p. 8894-8918; ISI[000296973000051].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297151800006">Advances in the Synthesis and Pharmacological Activity of Lupane-type Triterpenoid Saponins.</a></p>

    <p class="plaintext">Gauthier, C., J. Legault, M. Piochon-Gauthier, and A. Pichette, Phytochemistry Reviews, 2011. 10(4): p. 521-544; ISI[000297151800006].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296852800001">Investigation of Furo[2,3-h]- and Pyridazino[3,4-f]cinnolin-3-ol Scaffolds as Substrates for the Development of Novel HIV-1 Integrase Inhibitors.</a> Gomaa, M.F.Y., N. Pala, M. Derudas, J.A. Hasanen, E.H. El-Tamany, P. Casule, A. Mariani, and M. Sechi, Arkivoc, 2011: p. 1-14; ISI[000296852800001].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297150600021">Design and Synthesis of Pyridone Inhibitors of Non-Nucleoside Reverse Transcriptase.</a> Gomez, R., S. Jolly, T. Williams, T. Tucker, R. Tynebor, J. Vacca, G. McGaughey, M.T. Lai, P. Felock, V. Munshi, D. DeStefano, S. Touch, M. Miller, Y.W. Yan, R. Sanchez, Y.X. Liang, B. Paton, B.L. Wan, and N. Anthony, Bioorganic &amp; Medicinal Chemistry Letters 2011. 21(24): p. 7344-7350; ISI[000297150600021].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297197000035">Characterization of a Novel Type of HIV-1 Particle Assembly Inhibitor Using a Quantitative Luciferase-Vpr Packaging-Based Assay.</a> Gonzalez, G.D., S Errazuriz, E Coric, P Souquet, F Turcaud, S Boulanger, P Bouaziz, S Hong, SS, Plos One, 2011. 6(11); ISI[000297197000035].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297056400373">Effects of Antiretroviral Protease Inhibitors, Lopinavir and Ritonavir, on the Hepatobiliary Disposition of Bile Acids in Sandwich-cultured Rat Hepatocytes.</a> Griffin, L.M. and K.L.R. Brouwer, Drug Metabolism Reviews, 2011. 43: p. 197-197; ISI[000297056400373].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297160500026">Evaluation of a Fluorescent Derivative of AMD3100 and Its Interaction with the CXCR4 Chemokine Receptor.</a> Knight, J.C., A.J. Hallett, A. Brancale, S.J. Paisey, R.W.E. Clarkson, and P.G. Edwards, ChemBioChem, 2011. 12(17): p. 2692-2698; ISI[000297160500026].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p> </p>

    <p> </p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297095000009">Regioselective Synthesis of Novel 3-Thiazolidine Acetic Acid Derivatives from Glycosido Ureides.</a> Li, Y.X., W. Chen, X.P. Yang, G.P. Yu, M.Z. Mao, Y.Y. Zhou, T.W. Liu, and Z.M. Li, Chemical Biology &amp; Drug Design, 2011. 78(6): p. 969-978; ISI[000297095000009].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296852800022">Synthesis of Linear Dibenzo[1,8]naphthyridines using 2-chloro-4-methylquinolines.</a> Manoj, M. and K.J.R. Prasad, Arkivoc. 2011: p. 289-307; ISI[000296852800022].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297161800016">Advances in HIV Microbicide Development.</a> Olsen, J.E., D Dewhurst, S, Future Medicinal Chemistry, 2011. 3(16): p. 2101-2116; ISI[000297161800016].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296859900013">Crystal Structures of Novel Allosteric Peptide Inhibitors of HIV Integrase Identify New Interactions at the LEDGF Binding Site.</a> Rhodes, D.P., TS Vandegraaff, N Jeevarajah, D Newman, J Martyn, J Coates, JAV Ede, NJ Rea, P Deadman, JJ, ChemBioChem, 2011. 12(15): p. 2311-2315; ISI[000296859900013].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297465300028">Anti-HIV-1 Reverse Transcriptase Activities of Hexane Extracts from some Asian Medicinal Plants.</a> Silprasit, K., S. Seetaha, P. Pongsanarakul, S. Hannongbua, and K. Choowongkomon, Journal of Medicinal Plants Research, 2011. 5(17): p. 4194-4201; ISI[000297465300028].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000297189900004">Effects of New Quinizarin Derivatives on both HCV NS5B RNA Polymerase and HIV-1 Reverse Transcriptase Associated Ribonuclease H Activities.</a> Tramontano, E.K., T Zinzula, L Esposito, F, Journal of Chemotherapy, 2011. 23(5): p. 273-276; ISI[000297189900004].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000296548400033">Syntheses of Indolo[3,2,1-d,e]phenanthridines and Isochromeno[3,4-a] carbazoles: Palladium Catalyzed Intramolecular Arylation via C-H Functionalization.</a> Yamuna, E., M. Zeller, and K.J.R. Prasad, Tetrahedron Letters 2011. 52(45): p. 6030-6034; ISI[000296548400033].
    <br />
    <b>[WOS]</b>. HIV_1209-122211.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
