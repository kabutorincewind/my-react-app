

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-04-23.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fhlJYxTgtk9S45Q+HiF5kAXXjy4Gpc/Tocjken5cXLqLHfVvYemYSJRtb5I0WkWeWkwa73CLrpaj8G6EEQV2WQVVlEqvcLbamagoWB5v1iou5lMOq6QPi1o39LI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5A322FBB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 10 - April 23, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25747495">Clerodane Type Diterpene as a Novel Antifungal Agent from Polyalthia longifolia Var. pendula.</a> Bhattacharya, A.K., H.R. Chand, J. John, and M.V. Deshpande. European Journal of Medicinal Chemistry, 2015. 94: p. 1-7. PMID[25747495].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25764341">Broad-spectrum Antimicrobial Polycarbonate Hydrogels with Fast Degradability.</a> Pascual, A., J.P. Tan, A. Yuen, J.M. Chan, D.J. Coady, D. Mecerreyes, J.L. Hedrick, Y.Y. Yang, and H. Sardon. Biomacromolecules, 2015. 16(4): p. 1169-1178. PMID[25764341].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25898416">Phytochemical Compositions and Biological Activities of Essential Oil from Xanthium strumarium L.</a> Sharifi-Rad, J., S.M. Hoseini-Alfatemi, M. Sharifi-Rad, M. Sharifi-Rad, M. Iriti, M. Sharifi-Rad, R. Sharifi-Rad, and S. Raeisi. Molecules, 2015. 20(4): p. 7034-7047. PMID[25898416].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25871371">A Facile Synthesis and Antimicrobial Activity Evaluation of Sydnonyl-substituted Thiazolidine Derivatives.</a> Shih, M.H., Y.Y. Xu, Y.S. Yang, and G.L. Lin. Molecules, 2015. 20(4): p. 6520-6532. PMID[25871371].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25702854">In Vitro Production and Antifungal Activity of Peptide ABP-dHC-cecropin A</a>. Zhang, J., A. Movahedi, J. Xu, M. Wang, X. Wu, C. Xu, T. Yin, and Q. Zhuge. Journal of Biotechnology, 2015. 199: p. 47-54. PMID[25702854].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25791454">Studies on Antimicrobial Effects of Four Ligands and their Transition Metal Complexes with 8-Mercaptoquinoline and Pyridine Terminal Groups.</a> Zhang, L.J., J.A. Zhang, X.Z. Zou, Y.J. Liu, N. Li, Z.J. Zhang, and Y. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(8): p. 1778-1781. PMID[25791454].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25778990">Synthesis and Biological Screening of 2&#39;-Aryl/Benzyl-2-aryl-4-methyl-4&#39;,5-bithiazolyls as Possible Anti-tubercular and Antimicrobial Agents.</a> Abhale, Y.K., A.V. Sasane, A.P. Chavan, K.K. Deshmukh, S.S. Kotapalli, R. Ummanni, S.F. Sayyad, and P.C. Mhaske. European Journal of Medicinal Chemistry, 2015. 94: p. 340-347. PMID[25778990].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25736994">Synthesis of Novel Pleuromutilin Derivatives. Part 1: Preliminary Studies of Antituberculosis Activity.</a> Dong, Y.J., Z.H. Meng, Y.Q. Mi, C. Zhang, Z.H. Cui, P. Wang, and Z.B. Xu. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(8): p. 1799-1803. PMID[25736994].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25778993">Discovery of InhA Inhibitors with Anti-mycobacterial Activity through a Matched Molecular Pair Approach.</a> Kanetaka, H., Y. Koseki, J. Taira, T. Umei, H. Komatsu, H. Sakamoto, G. Gulten, J.C. Sacchettini, M. Kitamura, and S. Aoki. European Journal of Medicinal Chemistry, 2015. 94: p. 378-385. PMID[25778993].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25771108">Synthesis, Crystal Structure and Biological Evaluation of Substituted Quinazolinone Benzoates as Novel Antituberculosis Agents Targeting Acetohydroxyacid Synthase.</a> Lu, W., I.A. Baig, H.J. Sun, C.J. Cui, R. Guo, I.P. Jung, D. Wang, M. Dong, M.Y. Yoon, and J.G. Wang. European Journal of Medicinal Chemistry, 2015. 94: p. 298-305. PMID[25771108].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25781074">Synthesis and Structure-Activity Relationships for Extended Side Chain Analogues of the Antitubercular Drug (6S)-2-Nitro-6-{[4-(trifluoromethoxy)benzyl]oxy}-6,7-dihydro-5H-imidazo[2,1-b][1,3]oxazine (PA-824).</a> Palmer, B.D., H.S. Sutherland, A. Blaser, I. Kmentova, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, W.A. Denny, and A.M. Thompson. Journal of Medicinal Chemistry, 2015. 58(7): p. 3036-3059. PMID[25781074].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/25747499">12. Synthesis of Isocryptolepine Analogues and their Structure-Activity Relationship Studies as Antiplasmodial and Antiproliferative Agents.</a> Aroonkit, P., C. Thongsornkleeb, J. Tummatorn, S. Krajangsri, M. Mungthin, and S. Ruchirawat. European Journal of Medicinal Chemistry, 2015. 94: p. 56-62. PMID[25747499].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25768697">New Heterocyclic Hybrids of Pyrazole and its Bioisosteres: Design, Synthesis and Biological Evaluation as Dual Acting Antimalarial-Antileishmanial Agents.</a> Bekhit, A.A., A.M. Hassan, H.A. Abd El Razik, M.M. El-Miligy, E.J. El-Agroudy, and D. Bekhit Ael. European Journal of Medicinal Chemistry, 2015. 94: p. 30-44. PMID[25768697].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25781377">Synthesis and Bioactivity of Beta-substituted Fosmidomycin Analogues Targeting 1-Deoxy-D-xylulose-5-phosphate reductoisomerase.</a> Chofor, R., S. Sooriyaarachchi, M.D. Risseeuw, T. Bergfors, J. Pouyez, C. Johny, A. Haymond, A. Everaert, C.S. Dowd, L. Maes, T. Coenye, A. Alex, R.D. Couch, T.A. Jones, J. Wouters, S.L. Mowbray, and S. Van Calenbergh. Journal of Medicinal Chemistry, 2015. 58(7): p. 2988-3001. PMID[25781377].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25555060">Identification of Potent Phosphodiesterase Inhibitors that Demonstrate Cyclic Nucleotide-dependent Functions in Apicomplexan Parasites.</a> Howard, B.L., K.L. Harvey, R.J. Stewart, M.F. Azevedo, B.S. Crabb, I.G. Jennings, P.R. Sanders, D.T. Manallack, P.E. Thompson, C.J. Tonkin, and P.R. Gilson. ACS Chemical Biology, 2015. 10(4): p. 1145-1154. PMID[25555060].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25894322">Identification of Selective Inhibitors of the Plasmodium falciparum Hexose Transporter PfHT by Screening Focused Libraries of Anti-malarial Compounds.</a> Ortiz, D., W.A. Guiguemde, A. Johnson, C. Elya, J. Anderson, J. Clark, M. Connelly, L. Yang, J. Min, Y. Sato, R.K. Guy, and S.M. Landfear. Plos One, 2015. 10(4): p. e0123598. PMID[25894322].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25766631">Design, Synthesis and Biological Evaluation of Functionalized Phthalimides: A New Class of Antimalarials and Inhibitors of Falcipain-2, a Major Hemoglobinase of Malaria Parasite.</a> Singh, A.K., V. Rajendran, A. Pant, P.C. Ghosh, N. Singh, N. Latha, S. Garg, K.C. Pandey, B.K. Singh, and B. Rathi. Bioorganic &amp; Medicinal Chemistry, 2015. 23(8): p. 1817-1827. PMID[25766631].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25785478">Inhibitors of Plasmodial Serine Hydroxymethyltransferase (SHMT): Cocrystal Structures of Pyrazolopyrans with Potent Blood- and Liver-stage Activities.</a> Witschel, M.C., M. Rottmann, A. Schwab, U. Leartsakulpanich, P. Chitnumsub, M. Seet, S. Tonazzi, G. Schwertz, F. Stelzer, T. Mietzner, C. McNamara, F. Thater, C. Freymond, A. Jaruwat, C. Pinthong, P. Riangrungroj, M. Oufir, M. Hamburger, P. Maser, L.M. Sanz-Alonso, S. Charman, S. Wittlin, Y. Yuthavong, P. Chaiyen, and F. Diederich. Journal of Medicinal Chemistry, 2015. 58(7): p. 3117-3130. PMID[25785478].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0410-042315.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350919100056">Monocarbonyl Analogs of Curcumin Inhibit Growth of Antibiotic Sensitive and Resistant Strains of Mycobacterium tuberculosis.</a> Baldwin, P.R., A.Z. Reeves, K.R. Powell, R.J. Napier, A.I. Swimm, A.M. Sun, K. Giesler, B. Bommarius, T.M. Shinnick, J.P. Snyder, D.C. Liotta, and D. Kalman. European Journal of Medicinal Chemistry, 2015. 92: p. 693-699. ISI[000350919100056].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350696000035">Inhibition of M. tuberculosis beta-Ketoacyl CoA Reductase FabG4 (Rv0242c) by Triazole Linked Polyphenol-aminobenzene Hybrids: Comparison with the Corresponding Gallate Counterparts.</a> Banerjee, D.R., K. Senapati, R. Biswas, A.K. Das, and A. Basak. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1343-1347. ISI[000350696000035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350365100021">Development of 8-Benzyloxy-substituted quinoline ethers and Evaluation of their Antimicrobial Activities.</a> Chung, P.Y., R. Gambari, Y.X. Chen, C.H. Cheng, Z.X. Bian, A.S.C. Chan, J.C.O. Tang, P.H.M. Leung, C.H. Chui, and K.H. Lam. Medicinal Chemistry Research, 2015. 24(4): p. 1568-1577. ISI[000350365100021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350973300009">Evaluation of Antioxidant, Cholinesterase Inhibitory and Antimicrobial Properties of Mentha longifolia Subsp noeana and its Secondary Metabolites.</a> Ertas, A., A.C. Goren, N. Hasimi, V. Tolan, and U. Kolak. Records of Natural Products, 2015. 9(1): p. 105-115. ISI[000350973300009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351562200003">Characterization and Antitubercular Activity of Synthesized Pyrimidine Derivatives via Chalcones.</a> Faldu, V.J., V.K. Gothalia, and V.H. Shah. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2015. 54(3): p. 391-398. ISI[000351562200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351193100009">Synthesis of a New Peptide-coumarin Conjugate: A Potential Agent against Cryptococcosis.</a> Ferreira, S.Z., H.C. Carneiro, H.A. Lara, R.B. Alves, J.M. Resende, H.M. Oliveira, L.M. Silva, D.A. Santos, and R.P. Freitas. ACS Medicinal Chemistry Letters, 2015. 6(3): p. 271-275. ISI[000351193100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350879300012">Chemical Composition and Antibacterial Activity of Essential Oils Extracted from Plants Cultivated in Mexico.</a> Flores, C.R., A. Pennec, C. Nugier-Chauvin, R. Daniellou, L. Herrera-Estrella, and A.L. Chauvin. Journal of the Mexican Chemical Society, 2014. 58(4): p. 452-455. ISI[000350879300012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350365100022">Synthesis, Spectroscopic Characterization, Analgesic, and Antimicrobial Activities of Co(II), Ni(II), and Cu(II) Complexes of 2-[N,N-bis-(3,5-Dimethyl-pyrazolyl-1-methyl)]aminothiazole.</a> Kalanithi, M., M. Rajarajan, P. Tharmaraj, and S.J. Raja. Medicinal Chemistry Research, 2015. 24(4): p. 1578-1585. ISI[000350365100022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351097000007">Synthesis, Characterization, and in Vitro Antimicrobial and Antifungal Activity of Novel Acridines.</a> Kaya, M., Y. Yildirir, and G.Y. Celik. Pharmaceutical Chemistry Journal, 2015. 48(11): p. 724-728. ISI[000351097000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350895900032">A Convenient Synthesis of Some New Fused Pyridine and Pyrimidine Derivatives of Antimicrobial Profiles.</a> Khalifa, N.M., A.A.H. Abdel-Rahman, S.I. Abd-Elmoez, O.A. Fathalla, and A.A. Abd El-Gwaad. Research on Chemical Intermediates, 2015. 41(4): p. 2295-2305. ISI[000350895900032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351328000008">Influence of Plant Extracts on the Growth of Oral Pathogens Streptococcus mutans and Candida albicans in Vitro.</a> Krumina, G., L. Ratkevicha, V. Nikolajeva, A. Babarikina, and D. Babarykin. Proceedings of the Estonian Academy of Sciences, 2015. 64(1): p. 62-67. ISI[000351328000008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350919100033">Novel Amide and Sulphonamide Derivatives of 6-(Piperazin-1-yl)phenanthridine as Potent Mycobacterium tuberculosis H37Rv Inhibitors.</a> Naidu, K.M., H.N. Nagesh, M. Singh, D. Sriram, P. Yogeeswari, and K. Sekhar. European Journal of Medicinal Chemistry, 2015. 92: p. 415-426. ISI[000350919100033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351148000010">Structural and in Vitro Anti-tubercular Activity Study of (E)-N&#39;-(2,6- Dihydroxybenzylidene)nicotinohydrazide and some Transition Metal Complexes</a>. Ogunniran, K.O., M.A. Mesubi, K. Raju, and T. Narender. Journal of the Iranian Chemical Society, 2015. 12(5): p. 815-829. ISI[000351148000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350974000008">Chemical Composition, Antioxidant and Antimicrobial Activities of the Essential Oil of Nepeta hindostana (Roth) Haines from India.</a> Pandey, A.K., M. Mohan, P. Singh, and N.N. Tripathi. Records of Natural Products, 2015. 9(2): p. 224-233. ISI[000350974000008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351320900008">Pyridine-3,4-dicarboximide as Starting Material for the Total Synthesis of the Natural Product Eupolauramine and its Isomer Iso-eupolauramine Endowed with Anti-tubercular Activities.</a> Perdigao, G., C. Deraeve, G. Mori, M.R. Pasca, G. Pratviel, and V. Bernardes-Genisson. Tetrahedron, 2015. 71(10): p. 1555-1559. ISI[000351320900008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext"><a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350075100019">34. Synthesis, Crystal Structure, Theoretical Calculations and Antimicrobial Properties of [Pt(Tetramethylthiourea)(4)] [Pt(CN)(4) 4H2O].</a> Sadaf, H., A.A. Isab, S. Ahmad, A. Espinosa, M. Mas-Montoya, I.U. Khan, Ejaz, S.U. Rehman, M.A.J. Ali, M. Saleem, J. Ruiz, and C. Janiak. Journal of Molecular Structure, 2015. 1085: p. 155-161. ISI[000350075100019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350365100033">Synthesis and Biological Evaluation of Novel Indolo[2,3-c]isoquinoline Derivatives.</a> Saundane, A.R. and R. Kalpana. Medicinal Chemistry Research, 2015. 24(4): p. 1681-1695. ISI[000350365100033].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351580500012">Synthesis and in Vitro Antimicrobial Evaluation of Piperazine Substituted Quinazoline-based Thiourea/Thiazolidinone/Chalcone Hybrids.</a> Shah, D.R., H.P. Lakum, and K.H. Chikhalia. Russian Journal of Bioorganic Chemistry, 2015. 41(2): p. 209-222. ISI[000351580500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350250700003">Electro-organic Mediated Synthesis of bis-1,3,4-Oxadiazoles and Evaluation of their Antifungal Activity.</a> Sharma, L.K., A. Saraswat, S. Singh, M.K. Srivastav, and R.K.P. Singh. Proceedings of the National Academy of Sciences, India Section A: Physical Sciences, 2015. 85(1): p. 29-34. ISI[000350250700003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350913100005">Synthetic Cinnamates as Potential Antimicrobial Agents.</a> Stefanovic, O.D., I.D. Radojevic, and L.R. Comic. Hemijska Industrija, 2015. 69(1): p. 37-42. ISI[000350913100005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351097000004">Synthesis and Antimicrobial and Antifungal Activities of 3-Substituted 1-cyanomethyl-3,4-dihydroisoquinolinium chlorides.</a> Surikova, O.V., A.G. Mikhailovskii, and T.F. Odegova. Pharmaceutical Chemistry Journal, 2015. 48(11): p. 713-715. ISI[000351097000004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000351641300007">Synthesis and Antimycobacterial Activity of some Triazole Derivatives-New Route to Functionalized Triazolopyridazines</a>. Tehrani, K., V. Mashayekhi, P. Azerang, S. Minaei, S. Sardari, and F. Kobarfard. Iranian Journal of Pharmaceutical Research, 2015. 14: p. 59-68. ISI[000351641300007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350974000010">Volatiles and Antimicrobial Activity of the Essential Oils of the Mosses Pseudoscleropodium purum, Eurhynchium striatum, and Eurhynchium angustirete Grown in Turkey.</a> Tosun, G., B. Yayli, T. Ozdemir, N. Batan, A. Bozdeveci, and N. Yayli. Records of Natural Products, 2015. 9(2): p. 237-242. ISI[000350974000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350919100006">Synthesis of Functionalized 3-, 5-, 6- and 8-Aminoquinolines via Intermediate (3-Pyrrolin-1-yl)- and (2-Oxopyrrolidin-1-yl)quinolines and Evaluation of their Antiplasmodial and Antifungal Activity.</a> Vandekerckhove, S., S. Van Herreweghe, J. Willems, B. Danneels, T. Desmet, C. de Kock, P.J. Smith, K. Chibale, and M. D&#39;Hooghe. European Journal of Medicinal Chemistry, 2015. 92: p. 91-102. ISI[000350919100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0410-042315.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
