

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-292.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="l/0uocQ4pNElYOrKjAd6v3nr1csse7nil8YRH/j1+zpTN2ynVvbhJcVOyxjEKPTNARSnFySgFL0EBKgOIgUUTjbsvZCmVEJeQcvcc+XWLXlALrLjPnP8lU8zAzY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0EE69E5C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - OI-LS-292-MEMO</b> </p>

<p>    1.    43074   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Total synthesis and in vitro-antifungal activity of (+/-)-2-methoxytetradecanoic Acid</p>

<p>           Carballeira, NM, Ortiz, D, Parang, K, and Sardari, S</p>

<p>           Arch Pharm (Weinheim) 2004. 337(3):  152-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038060&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15038060&amp;dopt=abstract</a> </p><br />

<p>    2.    43075   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Molecular characterization of Mycobacterium tuberculosis isolates presenting various drug susceptibility profiles from Greece using three DNA typing methods</p>

<p>           Vrioni, Georgia, Levidiotou, Stamatina, Matsiota-Bernard, Peggy, and Marinis, Evangelos</p>

<p>           Journal of Infection 2004. 48(3): 253-262</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WJT-4B4PT0H-2/2/022b41579bc52a6c4299d1554ccc6bc8">http://www.sciencedirect.com/science/article/B6WJT-4B4PT0H-2/2/022b41579bc52a6c4299d1554ccc6bc8</a> </p><br />

<p>    3.    43076   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Synergistic Inhibition of Intracellular Hepatitis C Virus Replication by Combination of Ribavirin and Interferon- alpha</p>

<p>           Tanabe, Y, Sakamoto, N, Enomoto, N, Kurosaki, M, Ueda, E, Maekawa, S, Yamashiro, T, Nakagawa, M, Chen, CH, Kanazawa, N, Kakinuma, S, and Watanabe, M</p>

<p>           J Infect Dis 2004. 189(7): 1129-39</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15031779&amp;dopt=abstract</a> </p><br />

<p>    4.    43077   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Preliminary in vitro studies on two potent, water-soluble trimethoprim analogues with exceptional species selectivity against dihydrofolate reductase from Pneumocystis carinii and Mycobacterium avium</p>

<p>           Forsch, Ronald A, Queener, Sherry F, and Rosowsky, Andre</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(7): 1811-1815</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BSVNHJ-2/2/644eb5ecc1d3cb1a6b2ace3075712e45">http://www.sciencedirect.com/science/article/B6TF9-4BSVNHJ-2/2/644eb5ecc1d3cb1a6b2ace3075712e45</a> </p><br />

<p>    5.    43078   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Discovery of a New Family of Inhibitors of Human Cytomegalovirus (HCMV) Based upon Lipophilic Alkyl Furano Pyrimidine Dideoxy Nucleosides: Action via a Novel Non-Nucleosidic Mechanism</p>

<p>           McGuigan, C, Pathirana, RN, Snoeck, R, Andrei, G, DeClercq, E, and Balzarini, J</p>

<p>           J Med Chem 2004. 47(7): 1847-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027877&amp;dopt=abstract</a> </p><br />

<p>    6.    43079   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Structure-Activity Study on a Novel Series of Macrocyclic Inhibitors of the Hepatitis C Virus NS3 Protease Leading to the Discovery of BILN 2061</p>

<p>           Llinas-Brunet, M, Bailey, MD, Bolger, G, Brochu, C, Faucher, AM, Ferland, JM, Garneau, M, Ghiro, E, Gorys, V, Grand-Maitre, C, Halmos, T, Lapeyre-Paquette, N, Liard, F, Poirier, M, Rheaume, M, Tsantrizos, YS, and Lamarre, D</p>

<p>           J Med Chem 2004. 47(7): 1605-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15027850&amp;dopt=abstract</a> </p><br />

<p>    7.    43080   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Oligonucleotide-based strategies to inhibit human hepatitis C virus</p>

<p>           Martinand-Mari, C, Lebleu, B, and Robbins, I</p>

<p>           Oligonucleotides 2003. 13(6): 539-48</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025918&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15025918&amp;dopt=abstract</a> </p><br />

<p>    8.    43081   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Synthesis and preliminary evaluation of some pyrazine containing thiazolines and thiazolidinones as antimicrobial agents</p>

<p>           Bonde, Chandrakant G and Gaikwad, Naresh J</p>

<p>           Bioorganic &amp; Medicinal Chemistry  2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BY3SF4-4/2/bf14f2ec7aaa6eca49feb7da5f4b3e86">http://www.sciencedirect.com/science/article/B6TF8-4BY3SF4-4/2/bf14f2ec7aaa6eca49feb7da5f4b3e86</a> </p><br />

<p>    9.    43082   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           The vitro efficacy of [beta]-lactam and [beta]-lactamase inhibitors against multidrug resistant clinical strains of Mycobacterium tuberculosis</p>

<p>           Dincer, Irem, Ergin, Alper, and Kocagoz, Tanil</p>

<p>           International Journal of Antimicrobial Agents 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T7H-4BY3YK8-D/2/9590c4d1e1539e03baab9d58680a66d9">http://www.sciencedirect.com/science/article/B6T7H-4BY3YK8-D/2/9590c4d1e1539e03baab9d58680a66d9</a> </p><br />

<p>  10.    43083   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Optically active antifungal azoles: synthesis and antifungal activity of (2R,3S)-2-(2,4-difluorophenyl)-3-(5-{2-[4-aryl-piperazin-1-yl]-ethyl}-tetrazol-2-yl/1-yl)-1-[1,2,4]-triazol-1-yl-butan-2-ol*1</p>

<p>           Upadhayaya, Ram Shankar, Sinha, Neelima, Jain, Sanjay, Kishore, Nawal, Chandra, Ramesh, and Arora, Sudershan K</p>

<p>           Bioorganic &amp; Medicinal Chemistry  2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF8-4BX77WS-6/2/e75b6c753ddf49753cdaad67120384ed">http://www.sciencedirect.com/science/article/B6TF8-4BX77WS-6/2/e75b6c753ddf49753cdaad67120384ed</a> </p><br />

<p>  11.    43084   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Protein kinases as targets for anti-parasitic chemotherapy</p>

<p>           Doerig, C</p>

<p>           Biochim Biophys Acta 2004. 1697(1-2): 155-68</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023358&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15023358&amp;dopt=abstract</a> </p><br />

<p>  12.    43085   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Design, synthesis and evaluation of novel 1,4-naphthoquinone derivatives as antifungal and anticancer agents</p>

<p>           Tandon, Vishnu K, Chhor, Rakeshwar B, Singh, Ravindra V, Rai, Sanjay, and Yadav, Dharmendra B </p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(5): 1079-1083</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-2/2/53bddcc59999342fd7bb09105540361b">http://www.sciencedirect.com/science/article/B6TF9-4BRJ576-2/2/53bddcc59999342fd7bb09105540361b</a> </p><br />

<p>  13.    43086   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Cloning, expression, and characterization of Mycobacterium tuberculosis dihydrofolate reductase</p>

<p>           White, EL, Ross, LJ, Cunningham, A, and Escuyer, V</p>

<p>           FEMS Microbiol Lett 2004. 232(1):  101-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019741&amp;dopt=abstract</a> </p><br />

<p>  14.    43087   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           In vitro and in vivo antimicrobial activity of two alpha-helical cathelicidin peptides and of their synthetic analogs</p>

<p>           Benincasa, M, Skerlavaj, B, Gennaro, R, Pellegrini, A, and Zanetti, M</p>

<p>           Peptides 2003. 24(11): 1723-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019203&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15019203&amp;dopt=abstract</a> </p><br />

<p>  15.    43088   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Alternative approaches for efficient inhibition of hepatitis C virus RNA replication by small interfering RNAs</p>

<p>           Kronke, J, Kittler, R, Buchholz, F, Windisch, MP, Pietschmann, T, Bartenschlager, R, and Frese, M</p>

<p>           J Virol 2004. 78(7): 3436-46</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15016866&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15016866&amp;dopt=abstract</a> </p><br />

<p>  16.    43089   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Multiple thioredoxin-mediated routes to detoxify hydroperoxides in Mycobacterium tuberculosis</p>

<p>           Jaeger, Timo, Budde, Heike, Flohe, Leopold, Menge, Ulrich, Singh, Mahavir, Trujillo, Madia, and Radi, Rafael</p>

<p>           Archives of Biochemistry and Biophysics 2004. 423(1): 182-191</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6WB5-4BD5NF3-1/2/e880d9d0f675c517bf758570e2666298">http://www.sciencedirect.com/science/article/B6WB5-4BD5NF3-1/2/e880d9d0f675c517bf758570e2666298</a> </p><br />

<p>  17.    43090   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           The 500-base-pair fragment of the putative gene RvD1-Rv2031c is also present in the genome of Mycobacterium tuberculosis</p>

<p>           Metaxa-Mariatou, V, Vakalis, N, Gazouli, M, and Nasioulas, G</p>

<p>           In Vivo 2004. 18(1): 33-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15011748&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15011748&amp;dopt=abstract</a> </p><br />

<p>  18.    43091   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           [Study on isoniazid-resistant Mycobacterium tuberculosis isolates by multiple-polymerase chain reaction-single strand conformation polymorphism]</p>

<p>           Cheng, XD, Yu, WB, Bie, LF, Su, MQ, Zhang, R, and Hao, XK</p>

<p>           Zhonghua Jie He He Hu Xi Za Zhi 2004. 27(1): 23-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14989821&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14989821&amp;dopt=abstract</a> </p><br />

<p>  19.    43092   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Antifungal and antiviral chemotherapy</p>

<p>           Suresh, L and Radfar, L</p>

<p>           Alpha Omegan 2003. 96(4): 35-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14983728&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14983728&amp;dopt=abstract</a> </p><br />

<p>  20.    43093   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Inhibitors of hepatitis C virus NS3.4A protease. Part 3: P2 proline variants</p>

<p>           Perni, Robert B, Farmer, Luc J, Cottrell, Kevin M, Court, John J, Courtney, Lawrence F, Deininger, David D, Gates, Cynthia A, Harbeson, Scott L, Kim, Joseph L, and Lin, Chao</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BV4T73-3/2/fe43af8572584802442ec7f4e082610d">http://www.sciencedirect.com/science/article/B6TF9-4BV4T73-3/2/fe43af8572584802442ec7f4e082610d</a> </p><br />

<p>  21.    43094   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Treatment of murine systemic aspergillosis with polyene-intralipid admixtures</p>

<p>           Sionov, E and Segal, E</p>

<p>           Med Mycol 2004. 42(1): 73-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982116&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982116&amp;dopt=abstract</a> </p><br />

<p>  22.    43095   OI-LS-292; PUBMED-OI-3/24/2004</p>

<p class="memofmt1-2">           Posttranscriptional inhibition of gene expression by Mycobacterium tuberculosis offsets transcriptional synergism with IFN-gamma and posttranscriptional up-regulation by IFN-gamma</p>

<p>           Qiao, Y, Prabhakar, S, Canova, A, Hoshino, Y, WeiDen M, and Pine, R</p>

<p>           J Immunol 2004. 172(5): 2935-2943</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978096&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978096&amp;dopt=abstract</a> </p><br />

<p>  23.    43096   OI-LS-292; EMBASE-OI-3/24/2004</p>

<p class="memofmt1-2">           Scaling-up treatment for HIV/AIDS: lessons learned from multidrug-resistant tuberculosis</p>

<p>           Gupta, Rajesh, Irwin, Alexander, Raviglione, Mario C, and Yong Kim, Jim</p>

<p>           The Lancet 2004. 363(9405): 320-324</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T1B-4BHSMV4-Y/2/cdad80a2b0c807132ed1f2045558a6a7">http://www.sciencedirect.com/science/article/B6T1B-4BHSMV4-Y/2/cdad80a2b0c807132ed1f2045558a6a7</a> </p><br />

<p>  24.    43097   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           Intracellular expression of engineered RNase P ribozymes effectively blocks gene expression and replication of human cytomegalovirus</p>

<p>           Kim, K, Umamoto, S, Trang, P, Hai, R, and Liu, FY</p>

<p>           RNA 2004. 10(3): 438-447</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189115400011">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189115400011</a> </p><br />

<p>  25.    43098   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           Prevention of lethal murine candidiasis using HP (2-20), an antimicrobial peptide derived from the N-terminus of Helicobacter pylori ribosomal protein L1</p>

<p>           Ribeiro, PD and Medina-Acosta, E</p>

<p>           PEPTIDES 2003. 24(11): 1807-1814</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189110700019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189110700019</a> </p><br />

<p>  26.    43099   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           Replication of hepatitis C virus RNA occurs in a membrane-bound replication complex containing nonstructural viral proteins and RNA</p>

<p>           El-Hage, N and Luo, GX</p>

<p>           J GEN VIROL 2003. 84: 2761-2769</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189155700017">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189155700017</a> </p><br />

<p>  27.    43100   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           A conserved basic loop in hepatitis C virus p7 protein is required for amantadine-sensitive ion channel activity in mammalian cells but is dispensable for localization to mitochondria</p>

<p>           Griffin, SDC, Harvey, R, Clarke, DS, Barclay, WS, Harris, M, and Rowlands, DJ</p>

<p>           J GEN VIROL 2004. 85: 451-461</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189152500019">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189152500019</a> </p><br />

<p>  28.    43101   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           New beauvericins, potentiators of antifungal miconazole activity, produced by Beauveria sp FKI-1366 - I. Taxonomy, fermentation, isolation and biological properties</p>

<p>           Fukuda, T, Arai, M, Yamaguchi, Y, Masuma, R, Tomoda, H, and Omura, S</p>

<p>           J ANTIBIOT 2004. 57(2): 110-116</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189285000005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189285000005</a> </p><br />

<p>  29.    43102   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           New beauvericins, potentiators of antifungal miconazole activity, produced by Beauveria sp FKI-1366 - II. Structure elucidation.</p>

<p>           Fukuda, T, Arai, M, Tomoda, H, and Omura, S</p>

<p>           J ANTIBIOT 2004. 57(2): 117-124</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189285000006">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189285000006</a> </p><br />

<p>  30.    43103   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           The effects of rifampicin and fluoroquinolones on tubercle bacilli within human macrophages</p>

<p>           Duman, N, Cevikbas, A, and Johansson, C</p>

<p>           INT J ANTIMICROB AG 2004. 23(1): 84-87</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189096900014">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189096900014</a> </p><br />

<p>  31.    43104   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           TFIIH transcription factor, a target for the Rift Valley hemorrhagic fever virus</p>

<p>           Le May, N, Dubaele, S, De Santis, LP, Billecocq, A, Bouloy, M, and Egly, JM</p>

<p>           CELL 2004. 116(4): 541-550</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189184200008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189184200008</a> </p><br />

<p>  32.    43105   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           Synthesis of mannopyranose disaccharides as photoaffinity probes for mannosyltransferases in Mycobacterium tuberculosis</p>

<p>           Pathak, AK, Pathak, V, Riordan, JM, Gurcha, SS, Besra, GS, and Reynolds, RC</p>

<p>           CARBOHYD RES 2004. 339(3): 683-691</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189085600026">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189085600026</a> </p><br />

<p>  33.    43106   OI-LS-292; WOS-OI-3/14/2004</p>

<p class="memofmt1-2">           Structural characterization and antimicrobial activity of chitosan/betaine derivative complex</p>

<p>           Liu, H, Du, YM, Yang, JH, and Zhu, HY</p>

<p>           CARBOHYD POLYM 2004. 55(3): 291-297</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189137200008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189137200008</a> </p><br />

<p>  34.    43107   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Pegylated interferon-alpha protects type 1 pneumocytes against SARS coronavirus infection in macaques</p>

<p>           Haagmans, BL, Kuiken, T, Martina, BE, Fouchier, RAM, Rimmelzwaan, GF, van, Amerongen G, van, Riel D, de, Jong T, Itamura, S, Chan, KH, Tashiro, M, and Osterhaus, ADME</p>

<p>           NAT MED 2004. 10(3): 290-293</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189297700041">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189297700041</a> </p><br />

<p>  35.    43108   OI-LS-292; WOS-OI-3/21/2004</p>

<p><b>           Antibacterial and antifungal activity of some thiosemicarbazones and 1,3,4-thiadiazolines</b> </p>

<p>           Brousse, BN, Massa, R, Moglioni, AG, Alho, MM, D&#39;Accorso, N, Gutkind, G, and Moltrasio, GY</p>

<p>           J CHIL CHEM SOC 2004. 49(1): 45-49</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189308600009">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189308600009</a> </p><br />

<p>  36.    43109   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Persistent and dormant tubercle bacilli and latent tuberculosis</p>

<p>           Zhang, Y</p>

<p>           FRONT BIOSCI 2004. 9: 1136-1156</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189333700010">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189333700010</a> </p><br />

<p>  37.    43110   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           A tale of two lipids: Mycobacterium tuberculosis phagosome maturation arrest</p>

<p>           Chua, J, Vergne, I, Master, S, and Deretic, V</p>

<p>           CURR OPIN MICROBIOL 2004. 7(1): 71-77</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189358700013">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189358700013</a> </p><br />

<p>  38.    43111   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Spirobipyridopyrans, spirobinaphthopyrans, indolinospiropyridopyrans, indolinospironaphthopyrans and indolinospironaphtho-1,4-oxazines: synthesis, study of X-ray crystal structure, antitumoral and antiviral evaluation</p>

<p>           Raic-Malic, S, Tomaskovic, L, Mrvos-Sermek, D, Prugovecki, B, Cetina, M, Grdisa, M, Pavelic, K, Mannschreck, A, Balzarini, J, De, Clercq E, and Mintas, M</p>

<p>           BIOORGAN MED CHEM 2004. 12(5): 1037-1045</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189314700023">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189314700023</a> </p><br />

<p>  39.    43112   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Antimicrobial activities of N-(2-hydroxy-1-naphthalidene)-amino acid(glycine, alanine, phenylalanine, histidine, tryptophane) Schiff bases and their manganese(III) complexes</p>

<p>           Sakiyan, I, Logoglu, E, Arslan, S, Sari, N, and Sakiyan, N</p>

<p>           BIOMETALS 2004. 17(2): 115-120</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189394800004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189394800004</a> </p><br />

<p>  40.    43113   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Geldanamycin, a ligand of heat shock protein 90, inhibits the replication of herpes simplex virus type 1 in vitro</p>

<p>           Li, YH, Tao, PZ, Liu, YZ, and Jiang, JD</p>

<p>           ANTIMICROB AGENTS CH 2004. 48(3): 867-872</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189351700024">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000189351700024</a> </p><br />

<p>  41.    43114   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Identification of a novel class of inhibitors of hepatitis CNS5B polymerase.</p>

<p>           Proulx, M, Chan, L, Reddy, TJ, Das, SK, Pereira, SZ, Wang, WY, Siddiqui, A, Turcotte, N, Drouin, A, Yannopoulos, C, Poisson, C, Alaoui, HMA, Bethell, R, Hamel, M, Bilimoria, D, Nguyen-Ba, N, and L&#39;Heureux, L</p>

<p>           ABSTR PAP AM CHEM S 2003. 225: U180</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918000966">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918000966</a> </p><br />

<p>  42.    43115   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           2,4-diamino-5-arylthiosubstituted furo[2,3-d]pyrimidines as potential Pneumocystis carinii and Toxoplasma gondii dihydrofolate reductase inhibitors.</p>

<p>           Gangjee, A, Sinha, S, Zeng, YB, Lin, X, and Queener, SF</p>

<p>           ABSTR PAP AM CHEM S 2003. 225: U198</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001058">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001058</a> </p><br />

<p>  43.    43116   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Discovery of BILN 2061 - A small molecule inhibitor of the hepatitis C virus serine protease.</p>

<p>           Llinas-Brunet, M, Bailey, M, Bolger, G, Cameron, D, Cartier, M, Faucher, AM, Goudreau, N, Kukolj, G, Lagace, L, Pause, A, Rancourt, J, Thibeault, D, Tsantrizos, Y, and Lamarre, D</p>

<p>           ABSTR PAP AM CHEM S 2003. 225: U231</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001242">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187918001242</a> </p><br />

<p>  44.    43117   OI-LS-292; WOS-OI-3/21/2004</p>

<p class="memofmt1-2">           Novel trehalose-based compounds against Mycobacterium, synthesis and antibacterial studies.</p>

<p>           Chang, CWT, Elchert, B, Hui, Y, Wang, JH, Wennergren, J, Rai, R, Takemoto, J, and Bensaci, M</p>

<p>           ABSTR PAP AM CHEM S 2003. 226: U212</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062400920">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187062400920</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
