

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-122.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XOhGQXm6Jh8hJKham8XkRtg77IhpySZ2LUZbQawLeUXFoKn2DYfUQyA8n0QPii3xMraScNgkZBWd3JLyoB4eolm6X4h3FBh76O3jRkfbocJXMRozYomUNbc5WmA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AA57F614" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-122-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58576   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Selective inhibitors of hepatitis C virus replication</p>

    <p>          Neyts, Johan</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K7X85V-2/2/a374f7b3e0fffabd6e58d743759153d7">http://www.sciencedirect.com/science/article/B6T2H-4K7X85V-2/2/a374f7b3e0fffabd6e58d743759153d7</a> </p><br />

    <p>2.     58577   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Comparative mechanistic studies of de novo RNA synthesis by flavivirus RNA-dependent RNA polymerases</p>

    <p>          Selisko, Barbara, Dutartre, Helene, Guillemot, Jean-Claude, Debarnot, Claire, Benarroch, Delphine, Khromykh, Alexander, Despres, Philippe, Egloff, Marie-Pierre, and Canard, Bruno</p>

    <p>          Virology <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4JSFV8R-3/2/401c4bac6db8e0b1c774003264f01b23">http://www.sciencedirect.com/science/article/B6WXR-4JSFV8R-3/2/401c4bac6db8e0b1c774003264f01b23</a> </p><br />

    <p>3.     58578   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral activity of Arthrospira-derived spirulan-like substances</p>

    <p>          Rechter, Sabine, Konig, Tanja, Auerochs, Sabrina, Thulke, Stefanie, Walter, Hauke, Dornenburg, Heike, Walter, Christian, and Marschall, Manfred</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K9C39C-1/2/9528bad96ab7270f9c055253600fe037">http://www.sciencedirect.com/science/article/B6T2H-4K9C39C-1/2/9528bad96ab7270f9c055253600fe037</a> </p><br />

    <p>4.     58579   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Perspectives on the development of acyclic nucleotide analogs as antiviral drugs</p>

    <p>          Lee, William A and Martin, John C</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K714X4-1/2/8fd0eb4b6ff8e0005e4dd710dd0026dc">http://www.sciencedirect.com/science/article/B6T2H-4K714X4-1/2/8fd0eb4b6ff8e0005e4dd710dd0026dc</a> </p><br />

    <p>5.     58580   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus by D-fraction from Grifola frondosa: Synergistic effect of combination with interferon-[alpha] in HepG2 2.2.15</p>

    <p>          Gu, Chang-Qing, Li, Jun-Wen, and Chao, Fu-Huan</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K4269B-1/2/5104988fd2b57c2b7512df3be8bc44e3">http://www.sciencedirect.com/science/article/B6T2H-4K4269B-1/2/5104988fd2b57c2b7512df3be8bc44e3</a> </p><br />

    <p>6.     58581   DMID-LS-122; EMBASE-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antivirals for influenza: Historical perspectives and lessons learned</p>

    <p>          Hayden, Frederick G</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K4WG22-3/2/ec9c443294556bd9507a524efb08ae3d">http://www.sciencedirect.com/science/article/B6T2H-4K4WG22-3/2/ec9c443294556bd9507a524efb08ae3d</a> </p><br />

    <p>7.     58582   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Selection and characterization of replicon variants dually resistant to thumb- and palm-binding nonnucleoside polymerase inhibitors of the hepatitis C virus</p>

    <p>          Le Pogam, S, Kang, H, Harris, SF, Leveque, V, Giannetti, AM, Ali, S, Jiang, WR, Rajyaguru, S, Tavares, G, Oshiro, C, Hendricks, T, Klumpp, K, Symons, J, Browner, MF, Cammack, N, and Najera, I</p>

    <p>          JOURNAL OF VIROLOGY: J. Virol <b>2006</b>.  80(12): 6146-6154, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238103900050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238103900050</a> </p><br />

    <p>8.     58583   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nitroimidazoles - Part 2 - Synthesis, antiviral and antitumor activity of new 4-nitroimidazoles</p>

    <p>          Al-Masoudi, NA, Al-Soud, YA, Kalogerakis, A, Pannecouque, C, and De Clercq, E</p>

    <p>          CHEMISTRY &amp; BIODIVERSITY: Chem. Biodivers <b>2006</b>.  3(5): 515-526, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238084000004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238084000004</a> </p><br />

    <p>9.     58584   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitors of HCVNS5B polymerase: Synthesis and structure-activity relationships of N-alkyl-4-hydroxyquinolon-3-yl-benzothiadiazine sulfamides</p>

    <p>          Krueger, AC, Madigan, DL, Jiang, WW, Kati, WM, Liu, DC, Liu, Y, Maring, CJ, Masse, S, McDaniel, KF, Middleton, T, Mo, HM, Molla, A, Montgomery, D, Pratt, JK, Rockway, TW, Zhang, R, and Kempf, DJ</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS: Bioorg. Med. Chem. Lett <b>2006</b>.  16(13): 3367-3370, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238182700003</a> </p><br />

    <p>10.   58585   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleoside analogs as anti-HBV agents</p>

    <p>          Zhou, XX and Littler, E</p>

    <p>          CURRENT TOPICS IN MEDICINAL CHEMISTRY: Curr. Top. Med. Chem <b>2006</b>.  6(9): 851-865, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238037500002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238037500002</a> </p><br />

    <p>11.   58586   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of viruses by RNA interference</p>

    <p>          Stram, Y and Kuzntzova, L</p>

    <p>          VIRUS GENES: Virus Genes <b>2006</b>.  32(3): 299-306, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237866400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237866400009</a> </p><br />

    <p>12.   58587   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 6-arylthio analogs of 2 &#39;,3 &#39;-dideoxy-3 &#39;-fluoroguanosine and their effect against hepatitis B virus replication</p>

    <p>          Torii, T, Onishi, T, Izawa, K, Maruyama, T, Demizu, Y, Neyts, J, and De Clercq, E</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS: Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(4-6): 655-665, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237847700024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237847700024</a> </p><br />
    <br clear="all">

    <p>13.   58588   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human papillomavirus: The burden of infection</p>

    <p>          Wiley, D and Masongsong, E</p>

    <p>          OBSTETRICAL &amp; GYNECOLOGICAL SURVEY: Obstet. Gynecol. Surv <b>2006</b>.  61: S3-S14, 12</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238228300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238228300001</a> </p><br />

    <p>14.   58589   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          IFN-lambda: Novel antiviral cytokines</p>

    <p>          Ank, N, West, H, and Paludan, SR</p>

    <p>          JOURNAL OF INTERFERON AND CYTOKINE RESEARCH: J. Interferon Cytokine Res <b>2006</b>.  26(6): 373-379, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238205700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238205700001</a> </p><br />

    <p>15.   58590   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral and immunomodulatory properties of new pro-glutathione (GSH) molecules</p>

    <p>          Fraternale, A, Paoletti, MF, Casabianca, A, Oiry, J, Clayette, P, Vogel, JU, Cinatl, JJ, Palamara, AT, Sgarbanti, R, Garaci, E, Millo, E, Benatti, U, and Magnani, M</p>

    <p>          CURRENT MEDICINAL CHEMISTRY: Curr. Med. Chem <b>2006</b>.  13(15): 1749-1755, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238206200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238206200004</a> </p><br />

    <p>16.   58591   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inability of kaolin treatment to remove nonspecific inhibitors from equine serum for the hemagglutination inhibition test against equine H7N7 influenza virus</p>

    <p>          Boliar, S, Stanislawek, W, and Chambers, TM</p>

    <p>          JOURNAL OF VETERINARY DIAGNOSTIC INVESTIGATION: J. Vet. Diagn. Invest <b>2006</b>.  18(3): 264-267, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238031400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238031400005</a> </p><br />

    <p>17.   58592   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          siRNA targeting Vaccinia virus double-stranded RNA binding protein [E3L] exerts potent antiviral effects</p>

    <p>          Dave, RS, McGettigan, JP, Qureshi, T, Schnell, MJ, Nunnari, G, and Pomerantz, RJ</p>

    <p>          VIROLOGY: Virology <b>2006</b>.  348(2): 489-497, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237819200021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237819200021</a> </p><br />

    <p>18.   58593   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian influenza</p>

    <p>          Saeed, AA and Hussein, MF</p>

    <p>          SAUDI MEDICAL JOURNAL: Saudi Med. J <b>2006</b>.  27(5): 585-595, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237854300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237854300001</a> </p><br />

    <p>19.   58594   DMID-LS-122; WOS-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mutations distal to the substrate site can affect varicella zoster virus thymidine kinase activity: Implications for drug design</p>

    <p>          El Omari, K, Liekens, S, Bird, LE, Balzarini, J, and Stammers, DK</p>

    <p>          MOLECULAR PHARMACOLOGY: Mol. Pharmacol <b>2006</b>.  69(6): 1891-1896, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237688100014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237688100014</a> </p><br />

    <p>20.   58595   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          In vitro screening of traditionally used medicinal plants in China against Enteroviruses</p>

    <p>          Guo, JP, Pang, J, Wang, XW, Shen, ZQ, Jin, M, and Li, JW</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(25): 4078-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16810764&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16810764&amp;dopt=abstract</a> </p><br />

    <p>21.   58596   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral treatment of hepatitis B virus-transgenic mice by a marine organism, Styela plicata</p>

    <p>          Wang, R, Du, ZL, Duan, WJ, Zhang, X, Zeng, FL, and Wan, XX</p>

    <p>          World J Gastroenterol <b>2006</b>.  12(25): 4038-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16810755&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16810755&amp;dopt=abstract</a> </p><br />

    <p>22.   58597   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cyanovirin-N inhibits hepatitis C virus entry by binding to envelope protein glycans</p>

    <p>          Helle, F, Wychowski, C, Vu-Dac, N, Gustafson, KR, Voisset, C, and Dubuisson, J</p>

    <p>          J Biol Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809348&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809348&amp;dopt=abstract</a> </p><br />

    <p>23.   58598   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Hepatitis C virus entry depends on clathrin-mediated endocytosis</p>

    <p>          Blanchard, E,  Belouzard, S, Goueslain, L, Wakita, T, Dubuisson, J, Wychowski, C, and Rouille, Y</p>

    <p>          J Virol <b>2006</b> .  80(14): 6964-6972</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809302&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16809302&amp;dopt=abstract</a> </p><br />

    <p>24.   58599   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Mechanisms of establishment of persistent SARS-CoV-infected cells</p>

    <p>          Mizutani, T, Fukushi, S, Ishii, K, Sasaki, Y, Kenri, T, Saijo, M, Kanaji, Y, Shirota, K, Kurane, I, and Morikawa, S</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16808902&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16808902&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>25.   58600   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Respiratory syncytial virus infections: Recent prospects for control</p>

    <p>          Sidwell, RW and Barnard, DL</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16806515&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16806515&amp;dopt=abstract</a> </p><br />

    <p>26.   58601   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome (SARS): development of diagnostics and antivirals</p>

    <p>          Sorensen, MD, Sorensen, B, Gonzalez-Dosal, R, Melchjorsen, CJ, Weibel, J, Wang, J, Jun, CW, Huanming, Y, and Kristensen, P</p>

    <p>          Ann N Y Acad Sci <b>2006</b>.  1067: 500-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16804033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16804033&amp;dopt=abstract</a> </p><br />

    <p>27.   58602   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense Oligonucleotide Inhibition of Hepatitis C Virus Genotype 4 Replication in HepG2 Cells</p>

    <p>          El Awady, MK,  Badr, El Din NG, El, Garf WT, Youssef, SS, Omran, MH, Elabd, J, and Goueli, SA</p>

    <p>          Cancer Cell Int <b>2006</b>.  6(1): 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16803625&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16803625&amp;dopt=abstract</a> </p><br />

    <p>28.   58603   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of cell culture conditions on the anticytomegalovirus activity of maribavir</p>

    <p>          Chou, S, Van Wechel, LC, and Marousek, GI</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(7): 2557-2559</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801445&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801445&amp;dopt=abstract</a> </p><br />

    <p>29.   58604   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Intracellular Metabolism and In Vitro Activity of Tenofovir against Hepatitis B Virus</p>

    <p>          Delaney, WE 4th, Ray, AS, Yang, H, Qi, X, Xiong, S, Zhu, Y, and Miller, MD</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(7): 2471-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801428&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801428&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>30.   58605   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Detection of Influenza Viruses Resistant to Neuraminidase Inhibitors in Global Surveillance during the First 3 Years of Their Use</p>

    <p>          Monto, AS, McKimm-Breschkin, JL, Macken, C, Hampson, AW, Hay, A, Klimov, A, Tashiro, M, Webster, RG, Aymard, M, Hayden, FG, and Zambon, M</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(7): 2395-402</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801417&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16801417&amp;dopt=abstract</a> </p><br />

    <p>31.   58606   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Different anti-HCV profiles of statins and their potential for combination therapy with interferon</p>

    <p>          Ikeda, M, Abe, KI, Yamada, M, Dansako, H, Naka, K, and Kato, N</p>

    <p>          Hepatology <b>2006</b>.  44(1): 117-125</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16799963&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16799963&amp;dopt=abstract</a> </p><br />

    <p>32.   58607   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synergistic antiviral activity of acyclovir and vidarabine against herpes simplex virus types 1 and 2 and varicella-zoster virus</p>

    <p>          Suzuki, M, Okuda, T, and Shiraki, K</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16797734&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16797734&amp;dopt=abstract</a> </p><br />

    <p>33.   58608   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Novel Cyclopropyl Nucleosides, Phosphonate Nucleosides and Phosphonic Acid Nucleosides</p>

    <p>          Hyun, Oh C and Hong, JH</p>

    <p>          Arch Pharm (Weinheim) <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795106&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16795106&amp;dopt=abstract</a> </p><br />

    <p>34.   58609   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effects of rutin and quercetin on monooxygenase activities in experimental influenza virus infection</p>

    <p>          Savov, VM, Galabov, AS, Tantcheva, LP, Mileva, MM, Pavlova, EL, Stoeva, ES, and Braykova, AA</p>

    <p>          Exp Toxicol Pathol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16793246&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16793246&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>35.   58610   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of Japanese Encephalitis Virus NS1 Protein Expression in Cell by Small Interfering RNAs</p>

    <p>          Liu, X, Cao, S, Zhou, R, Xu, G, Xiao, S, Yang, Y, Sun, M, Li, Y, and Chen, H</p>

    <p>          Virus Genes <b>2006</b>.  33(1): 69-75</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16791421&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16791421&amp;dopt=abstract</a> </p><br />

    <p>36.   58611   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral effects of saikosaponins on human coronavirus 229e in vitro</p>

    <p>          Cheng, PW, Ng, LT, Chiang, LC, and Lin, CC</p>

    <p>          Clin Exp Pharmacol Physiol <b>2006</b>.  33(7): 612-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789928&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789928&amp;dopt=abstract</a> </p><br />

    <p>37.   58612   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Targets and Tools: Recent Advances in the Development of Anti-HCV Nucleic Acids</p>

    <p>          Romero-Lopez, C, Sanchez-Luque, FJ, and Berzal-Herranz, A</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(2): 121-45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789875&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16789875&amp;dopt=abstract</a> </p><br />

    <p>38.   58613   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Application of bioinformatics in search for cleavable peptides of SARS-CoV M(pro) and chemical modification of octapeptides</p>

    <p>          Du, Q, Wang, S, Jiang, Z, Gao, W, Li, Y, Wei, D, and Chou, KC</p>

    <p>          Med Chem <b>2005</b>.  1(3): 209-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787316&amp;dopt=abstract</a> </p><br />

    <p>39.   58614   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nucleoside analog inhibitors of hepatitis C virus replication</p>

    <p>          Carroll, SS and Olsen, DB</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(1): 17-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787301&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787301&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>40.   58615   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Discovery and development of VX-950, a novel, covalent, and reversible inhibitor of hepatitis C virus NS3.4A serine protease</p>

    <p>          Lin, C, Kwong, AD, and Perni, RB</p>

    <p>          Infect Disord Drug Targets <b>2006</b>.  6(1): 3-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787300&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787300&amp;dopt=abstract</a> </p><br />

    <p>41.   58616   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Recombinant human polyclonal antibodies: A new class of therapeutic antibodies against viral infections</p>

    <p>          Bregenholt, S, Jensen, A, Lantto, J, Hyldig, S, and Haurum, JS</p>

    <p>          Curr Pharm Des  <b>2006</b>.  12(16): 2007-15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787244&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16787244&amp;dopt=abstract</a> </p><br />

    <p>42.   58617   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Brefeldin A inhibits pestivirus release from infected cells, without affecting its assembly and infectivity</p>

    <p>          Macovei, A, Zitzmann, N, Lazar, C, Dwek, RA, and Branza-Nichita, N</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  346(3): 1083-90</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16782064&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16782064&amp;dopt=abstract</a> </p><br />

    <p>43.   58618   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 2-C-hydroxymethylribofuranosylpurines as potent anti-hepatitis C virus (HCV) agents</p>

    <p>          Yoo, BN, Kim, HO, Moon, HR, Seol, SK, Jang, SK, Lee, KM, and Jeong, LS</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16781148&amp;dopt=abstract</a> </p><br />

    <p>44.   58619   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pandemic influenza: a potential role for statins in treatment and prophylaxis</p>

    <p>          Fedson, DS</p>

    <p>          Clin Infect Dis <b>2006</b>.  43(2): 199-205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16779747&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16779747&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>45.   58620   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Amantadine inhibits the function of an ion channel encoded by GB virus B, but fails to inhibit virus replication</p>

    <p>          Premkumar, A, Dong, X, Haqshenas, G, Gage, PW, and Gowans, EJ</p>

    <p>          Antivir Ther <b>2006</b>.  11(3): 289-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759044&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16759044&amp;dopt=abstract</a> </p><br />

    <p>46.   58621   DMID-LS-122; PUBMED-DMID-7/3/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral Effect of Guazuma ulmifolia and Stryphnodendron adstringens on Poliovirus and Bovine Herpesvirus</p>

    <p>          Felipe, AM, Rincao, VP, Benati, FJ, Linhares, RE, Galina, KJ, de, Toledo CE, Lopes, GC, de, Mello JC, and Nozawa, C</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(6): 1092-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16754999&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16754999&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
