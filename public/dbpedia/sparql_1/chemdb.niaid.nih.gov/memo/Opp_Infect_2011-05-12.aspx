

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2011-05-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vR9YoLrYL4TbdyNpDJdXPfvzRiU9GuHpH8HvpyffigdDaCo2yt2NT7f+llcLI6HiqtenO6l0spWCH1+dTcl7oB4dnXsdNxmiTjAdizN09HPhRU/TLGA5oVEeh4A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F07AC301" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections Citation List: </p>

    <p class="memofmt2-h1">April 29 - May 12, 2011</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p> 

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21547917">Exploration of Islamic Medicine Plant Extracts as Powerful Antifungals for the Prevention of Mycotoxigenic Aspergilli Growth in Organic Silage.</a> Tayel, A.A., M.F. Salem, W.F. El-Tras, and L. Brimer. Journal of the Science of Food and Agriculture, 2011. <b>[Epub ahead of print]</b>; PMID[21547917].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21547455">Root Cultures of Hypericum perforatum subsp. angustifolium Elicited with Chitosan and Production of Xanthone-Rich Extracts with Antifungal Activity.</a> Tocci, N., G. Simonetti, F.D. D&#39;Auria, S. Panella, A.T. Palamara, A. Valletta, and G. Pasqua. Applied Microbiology and Biotechnology, 2011. <b>[Epub ahead of print]</b>; PMID[21547455].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21543360">Cryptococcus and Trichosporon spp. are Susceptible In Vitro to Branched Histidine- and Lysine-Rich Peptides (BHKPs).</a> Verwer, P.E., M.C. Woodle, T. Boekhout, F. Hagen, I.A. Bakker-Woudenberg, and W.W. van de Sande. Journal of Antimicrobial Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21543360].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21528493">Antitumor and Antimicrobial Activities of Endophytic Fungi from Medicinal Parts of Aquilaria sinensis.</a> Cui, J.L., S.X. Guo, and P.G. Xiao. Journal of Zhejiang University Science. B., 2011. 12(5): p. 385-392; PMID[21528493].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p> 

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p> 

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21542597">Antitubercular Chromones and Flavonoids from Pisonia aculeata.</a> Wu, M.C., C.F. Peng, I.S. Chen, and I.L. Tsai. Journal of Natural Products, 2011. <b>[Epub ahead of print]</b>; PMID[21542597].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21539505">Curcumin Acts Synergistically with Fluconazole to Sensitize a Clinical Isolate of Candida albicans Showing a MDR Phenotype.</a> Garcia-Gomes, A.S., J.A. Curvelo, R.M. Soares, and A. Ferreira-Pereira. Medical Mycology, 2011. <b>[Epub ahead of print]</b>; PMID[21539505].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">7. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21560236">Secondary Metabolites from the Root Wood of Zanthoxylum wutaiense and Their Antitubercular Activity.</a> Huang, H.Y., T. Ishikawa, C.F. Peng, S. Chen, and I.S. Chen. Chemistry and Biodiversity, 2011. 8(5): p. 880-886; PMID[21560236].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">8. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21486206">Synthesis and Antimycobacterial Evaluation of N&#39;-(E)-heteroaromaticpyrazine-2-carbohydrazide Derivatives.</a> Lima, C.H., M.G. Henriques, A.L. Candea, M.C. Lourenco, F.A. Bezerra, M.L. Ferreira, C.R. Kaiser, and M.V. de Souza. Medicinal Chemistry, 2011. 7(3): p. 245-249; PMID[21486206].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">9. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21454647">Potent Inhibitors of a Shikimate Pathway Enzyme from Mycobacterium tuberculosis: Combining Mechanism - and Modeling-Based Design.</a> Reichau, S., W. Jiao, S.R. Walker, R.D. Hutton, E.N. Baker, and E.J. Parker. Journal of Biological Chemistry, 2011. 286(18): p. 16197-16207; PMID[21454647].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</p> 

    <p class="NoSpacing">10. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21520472">Evaluation of Turkish Seaweeds for Antiprotozoal, Antimycobacterial and Cytotoxic Activities.</a> Suzgec-Selcuk, S., A.H. Mericli, K.C. Guven, M. Kaiser, R. Casey, S. Hingley-Wilson, A. Lalvani, and D. Tasdemir. Phytotherapy Research, 2011. 25(5): p. 778-783; PMID[21520472].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">11. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21535894">In Vitro and In Vivo Assessment of the Anti-Malarial Activity of Caesalpinia pluviosa and Analysis of its Active Fractions.</a> Kayano, A.C., S.C. Lopes, F.G. Bueno, E.C. Cabral, W.C. Neiras, L.M. Yamauchi, M.A. Foglio, M.N. Eberlin, J.C. Mello, and F.T. Costa. Malaria Journal, 2011. 10(1): p. 112; PMID[21535894].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">12. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21474310">Potent Antiprotozoal Activity of a Novel Semi-Synthetic Berberine Derivative.</a> Bahar, M., Y. Deng, X. Zhu, S. He, T. Pandharkar, M.E. Drew, A. Navarro-Vazquez, C. Anklin, R.R. Gil, R.W. Doskotch, K.A. Werbovetz, and A.D. Kinghorn. Bioorganic &amp; Medicinal Chemistry Letters, 2011. 21(9): p. 2606-2610; PMID[21474310].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">13. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21392942">In Vitro Activity of New Artemisinin Derivatives Against Plasmodium falciparum Clinical Isolates from Gabon.</a> Held, J., S.A. Soomro, P.G. Kremsner, F.H. Jansen, and B. Mordmuller. International Journal of Antimicrobial Agents, 2011. 37(5): p. 485-488; PMID[21392942].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">14. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21357292">In Vitro and In Vivo Antiplasmodial Activities of Risedronate and Its Interference with Protein Prenylation in Plasmodium falciparum.</a> Jordao, F.M., A.Y. Saito, D.C. Miguel, V. de Jesus Peres, E.A. Kimura, and A.M. Katzin. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2026-2031; PMID[21357292].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">15. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21343466">In Vitro Activity of Proveblue (Methylene Blue) on Plasmodium falciparum Strains Resistant to Standard Antimalarial Drugs.</a> Pascual, A., M. Henry, S. Briolant, S. Charras, E. Baret, R. Amalvict, E. Huyghues des Etages, M. Feraud, C. Rogier, and B. Pradines. Antimicrobial Agents and Chemotherapy, 2011. 55(5): p. 2472-2474; PMID[21343466].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">16. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21329950">An Unusual Dimeric Guaianolide with Antiprotozoal Activity and Further Sesquiterpene Lactones from Eupatoriumperfoliatum.</a> Maas, M., A. Hensel, F.B. Costa, R. Brun, M. Kaiser, and T.J. Schmidt. Phytochemistry, 2011. 72(7): p. 635-644; PMID[21329950].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">17. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21107862">Antiplasmodial Volatile Extracts from Cleistopholis patens Engler &amp; Diels and Uvariastrum pierreanum Engl. (Engl. &amp; Diels) (Annonaceae) Growing in Cameroon.</a> Boyom, F.F., V. Ngouana, E.A. Kemgne, P.H. Zollo, C. Menut, J.M. Bessiere, J. Gut, and P.J. Rosenthal. Parasitology Research, 2011. 108(5): p. 1211-1217; PMID[21107862].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">18. <a href="http://www.ncbi.nlm.nih.gov/Pubmed/21079994">Antiplasmodial Activity of Botanical Extracts Against Plasmodium falciparum.</a> Bagavan, A., A.A. Rahuman, C. Kamaraj, N.K. Kaushik, D. Mohanakrishnan, and D. Sahal. Parasitology Research, 2011. 108(5): p. 1099-1109; PMID[21079994].</p>

    <p class="NoSpacing"><b>[Pubmed]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="memofmt2-h1"> </p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="NoSpacing">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289546300064">Biochemical Characterization of Anti-microbial Activity of Glycolipids Produced by Rhodococcus Erythropolis.</a> Abdel-Megeed, A., A.N. Al-Rahma, A.A. Mostafa, and K.H.C. Baser. Pakistan Journal of Botany, 2011. 43(2): p. 1323-1334; ISI[000289546300064].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289655100037">Optimization of 3-Pphenylthio)quinolinium Compounds Against Opportunistic Fungal Pathogens.</a> Boateng, C.A., X.Y. Zhu, M.R. Jacob, S.I. Khan, L.A. Walker, and S.Y. Ablordeppey. European Journal of Medicinal Chemistry, 2011. 46(5): p. 1789-1797; ISI[000289655100037].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">21. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289697800034">Ethionamide Boosters: Synthesis, Biological Activity, and Structure-Activity Relationships of a Series of 1,2,4-Oxadiazole EthR Inhibitors.</a> Flipo, M., M. Desroses, N. Lecat-Guillet, B. Dirie, X. Carette, F. Leroux, C. Piveteau, F. Demirkaya, Z. Lens, P. Rucktooa, V. Villeret, T. Christophe, H.K. Jeon, C. Locht, P. Brodin, B. Deprez, A.R. Baulard, and N. Willand. Journal of Medicinal Chemistry, 2011. 54(8): p. 2994-3010; ISI[000289697800034].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">22. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289338000002">Design of Anti-Parasitic and Anti-Fungal Hydroxy-naphthoquinones that are Less Susceptible to Drug Resistance.</a> Hughes, L.M., C.A. Lanteri, M.T. O&#39;Neil, J.D. Johnson, G.W. Gribble, and B.L. Trumpower. Molecular and Biochemical Parasitology, 2011. 177(1): p. 12-19; ISI[000289338000002].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">23. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289707300006">Identification and Characterization of Antimicrobial Peptides from the Skin of the Endangered Frog Odorrana ishikawae.</a> Iwakoshi-Ukena, E., K. Ukena, A. Okimoto, M. Soga, G. Okada, N. Sano, T. Fujii, Y. Sugawara, and M. Sumida. Peptides, 2011. 32(4): p. 670-676; ISI[000289707300006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">24. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289655100045">Identification of Novel Potential Antibiotics for Tuberculosis by In Silico Structure-Based Drug Screening.</a> Izumizono, Y., S. Arevalo, Y. Koseki, M. Kuroki, and S. Aoki. European Journal of Medicinal Chemistry, 2011. 46(5): p. 1849-1856; ISI[000289655100045].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">25. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289355700013">Identification of Mannich Base as a Novel Inhibitor of Mycobacterium Tuberculosis Isocitrate by High-Throughput Screening.</a> Ji, L., Q.X. Long, D.C. Yang, and J.P. Xie. International Journal of Biological Sciences, 2011. 7(3): p. 376-382; ISI[000289355700013].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">26. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289572500011">Spectral, Biological Screening, and DNA Studies of Metal Chelates of 4- N,N-bis-(3,5-Dimethyl-pyrazolyl-1-methyl) aminoantipyrine.</a> Kalanithi, M., M. Rajarajan, and P. Tharmaraj. Journal of Coordination Chemistry, 2011. 64(8): p. 1436-1445; ISI[000289572500011].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">27. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289270000006">Antimicrobial Activity Screening of Isolated Flavonoids from Azadirachta indica Leaves.</a> Kanwal, Q., I. Hussain, H.L. Siddiqui, and A. Javaid. Journal of the Serbian Chemical Society, 2011. 76(3): p. 375-384; ISI[000289270000006].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289542800018">Synthesis of Some New Chalcones, Pyrazolines and Acetyl Pyrazolines Derived from Piperonal and Halogenohydroxy Acetophenones as Antimicrobial Agents.</a> Karamunge, K.G., M.A. Sayyed, A.Y. Vibhute, and Y.B. Vibhute. Journal of the Indian Chemical Society, 2011. 88(3): p. 443-450; ISI[000289542800018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289520000028">Antifungal Activity of Essential Oils and Their Synergy with Fluconazole Against Drug-Resistant Strains of Aspergillus fumigatus and Trichophyton rubrum.</a>  Khan, M.S.A. and I. Ahmad. Applied Microbiology and Biotechnology, 2011. 90(3): p. 1083-1094; ISI[000289520000028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289572500004">Synthesis, Spectral Analysis, and Molecular Modeling of Bioactive Sn(II)-Complexes with Oxadiazole Schiff Bases.</a> Khedr, A.M., S. Jadon, and V. Kumar. Journal of Coordination Chemistry, 2011. 64(8): p. 1351-1359; ISI[000289572500004].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289697800018">Chemistry and Biology of Macrolide Antiparasitic Agents.</a> Lee, Y., J.Y. Choi, H. Fu, C. Harvey, S. Ravindran, W.R. Roush, J.C. Boothroyd, and C. Khosla. Journal of Medicinal Chemistry, 2011. 54(8): p. 2792-2804; ISI[000289697800018].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289684200003">The Effect of Baccharis Glutinosa Extract on the Growth of Mycotoxigenic Fungi and Fumonisin B-1 and Aflatoxin B-1 Production.</a> Rosas-Burgos, E.C., M.O. Cortez-Rocha, M. Plascencia-Jatomea, F.J. Cinco-Moroyoqui, R.E. Robles-Zepeda, J. Lopez-Cervantes, D.I. Sanchez-Machado, and F. Lares-Villa. World Journal of Microbiology &amp; Biotechnology, 2011. 27(5): p. 1025-1033; ISI[000289684200003].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">33. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289655100028">Design and Synthesis of Antifungal Benzoheterocyclic Derivatives by Scaffold Hopping.</a> Sheng, C.Q., X.Y. Che, W.Y. Wang, S.Z. Wang, Y.B. Cao, J.Z. Yao, Z.Y. Miao, and W.N. Zhang. European Journal of Medicinal Chemistry, 2011. 46(5): p. 1706-1712; ISI[000289655100028].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">34. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289330400014">Chemical Composition and Antifungal Activity of Essential Oil from Cicuta virosa L. var. latisecta Celak.</a> Tian, J., X.Q. Ban, H. Zeng, J.S. He, B. Huang, and Y.W. Wang. International Journal of Food Microbiology, 2011. 145(2-3): p. 464-470; ISI[000289330400014].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>

    <p> </p>

    <p class="NoSpacing">35. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000289336300005">Design Synthesis and Biological Evaluation of 3-Substituted Triazole Derivatives.</a> Wang, B.G., S.C. Yu, X.Y. Chai, Y.Z. Yan, H.G. Hu, and Q.Y. Wu. Chinese Chemical Letters, 2011. 22(5): p. 519-522; ISI[000289336300005].</p>

    <p class="NoSpacing"><b>[WOS]</b>. OI_0427-051111.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
