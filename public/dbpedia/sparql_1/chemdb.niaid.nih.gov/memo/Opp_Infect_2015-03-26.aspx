

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-03-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bD51Pf+bCyJbOsuKPf82KMbQeYBdusrMTBj3ODhZy3J7hdTXWL2UWyq0z+gdvDBo+0aWZowWcXvRJj+HytqZvfmN+CV/y0Ch0R8Vdw4lkZwqbt739r8tFrR3EqU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="81278FAA" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 13 - March 26, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25576936">Ni(II), Pd(II) and Pt(II) Complexes of (1H-1,2,4-Triazole-3-ylimino)methyl]naphthalene-2-ol. Structural, Spectroscopic, Biological, Cytotoxicity, Antioxidant and DNA Binding.</a> Gaber, M., H.A. El-Ghamry, and S.K. Fathalla. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 139: p. 396-404. PMID[25576936].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25542127">Synthesis, Anti-oxidant Activity, and Biodegradability of a Novel Recombinant Polysaccharide Derived from Chitosan and Lactose.</a> Guo, M., Y. Ma, C. Wang, H. Liu, Q. Li, and M. Fei. Carbohydrate Polymers, 2015. 118: p. 218-223. PMID[25542127].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25308646">Chemical Composition and Antimicrobial Activity of Polish Herbhoneys.</a> Isidorov, V.A., R. Bagan, S. Bakier, and I. Swiecicka. Food Chemistry, 2015. 171: p. 84-88. PMID[25308646].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25785690">Human beta-Defensin 4 with Non-native Disulfide Bridges Exhibit Antimicrobial Activity.</a> Sharma, H. and R. Nagaraj. Plos One, 2015. 10(3): p. e0119525. PMID[25785690].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25781988">Comparative Study of Antimicrobial Activity of AgBr and Ag Nanoparticles (NPs).</a> Suchomel, P., L. Kvitek, A. Panacek, R. Prucek, J. Hrbac, R. Vecerova, and R. Zboril. Plos One, 2015. 10(3): p. e0119202. PMID[25781988].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25782003">Methyl-hydroxylamine as an Efficacious Antibacterial Agent that Targets the Ribonucleotide Reductase Enzyme.</a> Julian, E., A. Baelo, J. Gavalda, and E. Torrents. Plos One, 2015. 10(3): p. e0122049. PMID[25782003].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25681226">Modified Calanolides Incorporated with Furan-2-nitro Mimics against Mycobacterium tuberculosis.</a> Liu, Z., X. Guo, and G. Liu. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1297-1300. PMID[25681226].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25799414">Mycobacterial Dihydrofolate Reductase Inhibitors Identified Using Chemogenomic Methods and in Vitro Validation.</a> Mugumbate, G., K.A. Abrahams, J.A. Cox, G. Papadatos, G. van Westen, J. Lelievre, S.T. Calus, N.J. Loman, L. Ballell, D. Barros, J.P. Overington, and G.S. Besra. Plos One, 2015. 10(3): p. e0121492. PMID[25799414].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25670502">Synthesis of New Generation Triazolyl- and Isoxazolyl-containing 6-Nitro-2,3-dihydroimidazooxazoles as anti-TB Agents: In Vitro, Structure-Activity Relationship, Pharmacokinetics and in Vivo Evaluation.</a> Munagala, G., K.R. Yempalla, S. Singh, S. Sharma, N.P. Kalia, V.S. Rajput, S. Kumar, S.D. Sawant, I.A. Khan, R.A. Vishwakarma, and P.P. Singh. Organic &amp; Biomolecular Chemistry, 2015. 13(12): p. 3610-3624. PMID[25670502].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25677656">A Novel Molecule with Notable Activity against Multi-drug Resistant Tuberculosis.</a> Nair, V., M.O. Okello, N.K. Mangu, B.I. Seo, and M.G. Gund. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(6): p. 1269-1273. PMID[25677656].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25427196">2-Carboxyquinoxalines Kill Mycobacterium tuberculosis through Noncovalent Inhibition of DprE1.</a> Neres, J., R.C. Hartkoorn, L.R. Chiarelli, R. Gadupudi, M.R. Pasca, G. Mori, A. Venturelli, S. Savina, V. Makarov, G.S. Kolly, E. Molteni, C. Binda, N. Dhar, S. Ferrari, P. Brodin, V. Delorme, V. Landry, A.L. de Jesus Lopes Ribeiro, D. Farina, P. Saxena, F. Pojer, A. Carta, R. Luciani, A. Porta, G. Zanoni, E. De Rossi, M.P. Costi, G. Riccardi, and S.T. Cole. ACS Chemical Biology, 2015. 10(3): p. 705-714. PMID[25427196].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25636146">A Virtual Screen Discovers Novel, Fragment-sized Inhibitors of Mycobacterium tuberculosis InhA.</a> Perryman, A.L., W. Yu, X. Wang, S. Ekins, S. Forli, S.G. Li, J.S. Freundlich, P.J. Tonge, and A.J. Olson. Journal of Chemical Information and Modeling, 2015. 55(3): p. 645-659. PMID[25636146].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25802510">Antimycobacterial and Cytotoxicity Activity of Microcystins.</a> Ramos, D.F., A. Matthiensen, W. Colvara, A.P. de Votto, G.S. Trindade, P.E. da Silva, and J.S. Yunes. Journal of Venomous Animals and Toxins including Tropical Diseases, 2015. 21: p. 9. PMID[25802510].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25631047">Mycobacterial Nicotinate Mononucleotide Adenylyltransferase: Structure, Mechanism, and Implications for Drug Discovery.</a> Rodionova, I.A., H.J. Zuccola, L. Sorci, A.E. Aleshin, M.D. Kazanov, C.T. Ma, E. Sergienko, E.J. Rubin, C.P. Locher, and A.L. Osterman. The Journal of Biological Chemistry, 2015. 290(12): p. 7693-7706. PMID[25631047].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25574649">A Novel One-dimensional Manganese(II) Coordination Polymer Containing Both Dicyanamide and Pyrazinamide Ligands: Synthesis, Spectroscopic Investigations, X-ray Studies and Evaluation of Biological Activities.</a> Tabrizi, L., H. Chiniforoshan, and P. McArdle. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2015. 139: p. 307-312. PMID[25574649].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25535086">Antiplasmodial Compounds from the Stem Bark of Neoboutonia macrocalyx Pax.</a> Namukobe, J., B.T. Kiremire, R. Byamukama, J.M. Kasenene, H.M. Akala, E. Kamau, and V. Dumontet. Journal of Ethnopharmacology, 2015. 162: p. 317-322. PMID[25535086].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0313-032615.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350330000007">Synthesis, Spectral Characterization, Thermal and Antimicrobial Studies of Neodymium(III) and Samarium(III) Complexes with 1,2,4-Triazole Schiff Bases.</a> Ain, Q., O.P. Pandey, and S.K. Sengupta. Journal of the Indian Chemical Society, 2014. 91(10): p. 1899-1907. ISI[000350330000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000348343900050">Synthesis and Bio-evaluation of Novel 7-Hydroxy Coumarin Derivatives via Knoevenagel Reaction.</a> Darla, M.M., B.S. Krishna, K.U. Rao, N.B. Reddy, M.K. Srivash, K. Adeppa, C.S. Sundar, C.S. Reddy, and K. Misra. Research on Chemical Intermediates, 2015. 41(2): p. 1115-1133. ISI[000348343900050].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350177300021">Antimycobacterial and Antifungal Activities of Melaleuca alternifolia Oil Nanoparticles.</a> de Souza, M.E., L.Q.S. Lopes, R.D. Vaucher, D.N. Mario, S.H. Alves, V.A. Agertt, B.V. Bianchini, S.I. Felicidade, M.M.A. de Campus, A.A. Boligon, M.L. Athayde, C.G. Santos, R.P. Baffin, P. Gomes, and R.C.V. Santos. Journal of Drug Delivery Science and Technology, 2014. 24(5): p. 559-560. ISI[000350177300021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349388300005">Syntheses, Characterizations, Crystal Structures, and Biological Activities of Two New Mixed Ligand Ni( II) and Cu(II) Schiff Base Complexes.</a> Ebrahimipour, S.Y., M. Mohamadi, J. Castro, N. Mollania, H.A. Rudbari, and A. Sacca. Journal of Coordination Chemistry, 2015. 68(4): p. 632-649. ISI[000349388300005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349407100016">Green Synthesis and in Silico Investigation of Dihydro-2H-benzo[1,3]oxazine Derivatives as Inhibitors of Mycobacterium tuberculosis.</a> Kamble, R.D., S.V. Hese, R.J. Meshram, J.R. Kote, R.N. Gacche, and B.S. Dawane. Medicinal Chemistry Research, 2015. 24(3): p. 1077-1088. ISI[000349407100016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349700400013">Novel Isoniazid-amidoether Derivatives: Synthesis, Characterization and Antimycobacterial Activity Evaluation.</a> Kumar, D., G. Khare, Beena, S. Kidwai, A.K. Tyagi, R. Singh, and D.S. Rawat. MedChemComm, 2015. 6(1): p. 131-137. ISI[000349700400013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349456900010">The Novel Derivatives of 3-(Iminomethyl)-2H-chromen-2-one with Thiourea and Piperazine Structural Motive: Rationale, Synthesis, Antimicrobial and anti-TB Evaluation.</a> Lakum, H.P., D.R. Shah, and K.H. Chikhalia. Letters in Drug Design &amp; Discovery, 2015. 12(4): p. 324-341. ISI[000349456900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000350330000004">Synthesis, Spectral Characterization, Thermal and Biocidal Properties of Metal Complexes with N-Substituted sulfonamides.</a> Rama, I. and S. Ramaswami. Journal of the Indian Chemical Society, 2014. 91(10): p. 1877-1886. ISI[000350330000004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349407100007">Synthesis and Antimicrobial Activity of Pyrazolyl Benzoxazoles, Benzothiazoles and Benzimidazoles.</a> Reddy, L.M., T.B. Prakash, A. Padmaja, and V. Padmavathi. Medicinal Chemistry Research, 2015. 24(3): p. 970-979. ISI[000349407100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349407100035">Rational Design, Synthesis and Evaluation of Novel-substituted 1,2,3-Triazolylmethyl carbazoles as Potent Inhibitors of Mycobacterium tuberculosis.</a> Surineni, G., P. Yogeeswari, D. Sriram, and S. Kantevari. Medicinal Chemistry Research, 2015. 24(3): p. 1298-1309. ISI[000349407100035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000349430000046">Daucus carota Subsp Gummifer Essential Oil as a Natural Source of Antifungal and Anti-inflammatory Drugs.</a> Valente, J., M. Zuzarte, R. Resende, M.J. Goncalves, C. Cavaleiro, C.F. Pereira, M.T. Cruz, and L. Salgueiro. Industrial Crops and Products, 2015. 65: p. 361-366. ISI[000349430000046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0313-032615.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
