

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2012-01-19.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="NY5KnNlZCJsHog7jqJvXl/31tUb28lljPOabCgrGOaJImNVy2WM0TF9p2zcBzOa1noKL3s5Eu0zNNktQJF71wsblmXTnmO/i22KcvscdWxQZmqAeVkkAZ9na08A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="014BF305" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 6 - January 19, 2012</h1>

    <p class="memofmt2-2">Pubmed citations
    <br />
    <br /></p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22105071">Identification of Molecular Determinants from Moloney Leukemia Virus 10 Homolog (MOV10) Protein for Virion Packaging and anti-HIV-1 Activity.</a> Abudu, A., X. Wang, Y. Dang, T. Zhou, S.H. Xiang, and Y.H. Zheng, Journal of Biological Chemistry, 2012. 287(2): p. 1220-1228; PMID[22105071].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22045343">A Glycomimetic Compound Inhibits DC-SIGN-mediated HIV Infection in Cellular and Cervical Explant Models.</a>. Berzi, A., J.J. Reina, R. Ottria, I. Sutkeviciute, P. Antonazzo, M. Sanchez-Navarro, E. Chabrol, M. Biasin, D. Trabattoni, I. Cetin, J. Rojo, F. Fieschi, A. Bernardi, and M. Clerici, AIDS, 2012. 26(2): p. 127-137; PMID[22045343].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22236118">Recent Progress in Small Molecule CCR5 Antagonists as Potential HIV-1 Entry Inhibitors.</a> Chen, W., P. Zhan, E. De Clercq, and X. Liu, Current Pharmaceutical Design, 2012. <b>[Epub ahead of print]</b>; PMID[22236118].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238306">The Viral Protein Tat Can Inhibit the Establishment of HIV-1 Latency.</a> Donahue, D.A., B.D. Kuhl, R.D. Sloan, and M.A. Wainberg, Journal of Virology, 2012. <b>[Epub ahead of print]</b>; PMID[22238306].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227602">Microwave-assisted Synthesis of Small Molecules Targeting the Infectious Diseases Tuberculosis, HIV/AIDS, Malaria and Hepatitis C.</a> Gising, J., L.R. Odell, and M. Larhed, Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22227602].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext"> 6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233531">Identification of Novel HIV-1 Integrase Inhibitors Using Shape-based Screening, QSAR and Docking Approach.</a> Gupta, P., P. Garg, and N. Roy, Chemical Biology and Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22233531].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22227864">Development of a Combination Microbicide Gel Formulation Containing IQP-0528 and Tenofovir for the Prevention of HIV Infection.</a> Ham, A.S., S.R. Ugaonkar, L. Shi, K.W. Buckheit, H. Lakougna, U. Nagaraja, G. Gwozdz, L. Goldman, P.F. Kiser, and R.W. Buckheit, Jr., Journal of Pharmaceutical Sciences, 2012. <b>[Epub ahead of print]</b>; PMID[22227864].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22252501">Effect of Efavirenz on UDP-glucuronosyltransferase 1A1, 1A4, 1A6, and 1A9 Activities in Human Liver Microsomes.</a> Ji, H.Y., H. Lee, S.R. Lim, J.H. Kim, and H.S. Lee, Molecules, 2012. 17: p. 851-860; PMID[22252501].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238232">Cyclophillin a (CyPA) Is Required for Efficient HCMV DNA Replication and Reactivation.</a> Keyes, L.R., M.G. Bego, M. Soland, and S. St Jeor, Journal of General Virology, 2012.<b>[Epub ahead of print]</b>; PMID[22238232].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22242796">Computational Modeling Methods for QSAR Studies on HIV-1 Integrase Inhibitors (2005-2010).</a> Ko, G.M., A.S. Reddy, R. Garg, S. Kumar, and A.R. Hadaegh, Current Computer-Aided Drug Design, 2012 <b>[Epub ahead of print]</b>; PMID[22242796].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22173724">Influence of the Nucleobase and Anchimeric Assistance of the Carboxyl Acid Groups in the Hydrolysis of Amino Acid Nucleoside Phosphoramidates.</a> Maiti, M., S. Michielssens, N. Dyubankova, E. Lescrinier, A. Ceulemans, and P. Herdewijn, Chemistry, 2012. 18(3): p. 857-868; PMID[22173724].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22234680">HIV-1 Envelope-Dependent Restriction of CXCR4-Using Viruses in Children but Not Adult Untransformed CD4+ T Lymphocyte Lines.</a> Mariani, S.A., I. Brigida, A. Kajaste-Rudnitski, S. Ghezzi, A. Rocchi, A. Plebani, E. Vicenzi, A. Aiuti, and G. Poli, Blood, 2012.<b>[Epub ahead of print]</b>; PMID[22234680].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22239286">Interaction of I50V Mutant and I50L/A71V Double Mutant HIV-Protease with Inhibitor TMC114 (Darunavir): Molecular Dynamics Simulation and Binding Free Energy Studies.</a> Meher, B.R. and Y. Wang, Journal of Physical Chemistry B, 2012. <b>[Epub ahead of print]</b>; PMID[22239286].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22182762">Novel HIV-1 Fusion Inhibition Peptides: Designing the Next Generation of Drugs.</a> Miyamoto, F. and E.N. Kodama, Antiviral Chemistry and Chemotherapy, 2012. 22(4): p. 151-158; PMID[22182762].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22100453">Altered Strand Transfer Activity of a Multiple-drug-resistant Human Immunodeficiency Virus Type 1 Reverse Transcriptase Mutant with a Dipeptide Fingers Domain Insertion.</a> Nguyen, L.A., W. Daddacha, S. Rigby, R.A. Bambara, and B. Kim, Journal of Molecular Biology, 2012. 415(2): p. 248-262; PMID[22100453].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22247043">A Synthetic C34 Trimer of HIV-1 gp41 Shows Significant Increase in Inhibition Potency.</a> Nomura, W., C. Hashimoto, A. Ohya, K. Miyauchi, E. Urano, T. Tanaka, T. Narumi, T. Nakahara, J.A. Komano, N. Yamamoto, and H. Tamamura, ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22247043].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22244351">Cleavage of Pyrene-Stabilized RNA Bulge Loops by trans-(+/-)-Cyclohexane-1,2-diamine.</a> Patel, S., J. Rana, J. Roy, and H. Huang, Chemistry Central Journal, 2012. 6(1): p. 3; PMID[22244351].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238126">Critical Differences in HIV-1 and HIV-2 Protease Specificity for Clinical Inhibitors.</a> Tie, Y., Y.F. Wang, P.I. Boross, T.Y. Chiu, A.K. Ghosh, J. Tozser, J.M. Louis, R.W. Harrison, and I.T. Weber, Protein Science, 2012. <b>[Epub ahead of print]</b>; PMID[22238126].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21930169">Pegylated poly(ethylene imine) Copolymer-delivered siRNA Inhibits HIV Replication in Vitro.</a> Weber, N.D., O.M. Merkel, T. Kissel, and M.A. Munoz-Fernandez, Journal of Controlled Release, 2012. 157(1): p. 55-63; PMID[21930169].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22228771">Broad Antiviral Activity and Crystal Structure of HIV-1 Fusion Inhibitor Sifuvirtide.</a> Yao, X., H. Chong, C. Zhang, S. Waltersperger, M. Wang, S. Cui, and Y. He, Journal of Biological Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22228771].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22233539">Nitrogen-containing Polyhydroxylated Aromatics as HIV-1 Integrase Inhibitors: Synthesis, Structure-Activity Relationship Analysis, and Biological Activity.</a> Yu, S., L. Zhang, S. Yan, P. Wang, T. Sanchez, F. Christ, Z. Debyser, N. Neamati, and G. Zhao, Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22233539].</p>

    <p class="plaintext"><b>[PubMed]</b>.HIV_0106-011912.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations</p> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000298114300005">Synthesis of New Derivatives of Inositol-containing Phospholipid Dimer Analogs as Potential Inhibitors of Virus Adsorption.</a> Baranova, E.O., T.P.L. Dang, S.V. Eremin, D.S. Esipov, N.S. Shastina, and V.I. Shvets, Pharmaceutical Chemistry Journal, 2011. 45(6): p. 344-350; ISI[000298114300005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297960100013">The R-Diastereomer of 6 &#39;-o-Toluoyl-carba-LNA Modification in the Core Region of siRNA Leads to 24-Times Improved RNA Silencing Potency against the HIV-1 compared to its S-counterpart.</a> Dutta, S., N. Bhaduri, R.S. Upadhayaya, N. Rastogi, S.G. Chandel, J.K. Vandavasi, O. Plashkevych, R.A. Kardile, and J. Chattopadhyaya, MedChemComm, 2011. 2(11): p. 1110-1119; ISI[000297960100013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">24.<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297960100010">Discovery of gs-8374, a Potent Human Immunodeficiency Virus Type 1 Protease Inhibitor with a Superior Resistance Profile.</a> He, G.X., Z.-Y. Yang, M. Williams, C. Callebaut, T. Cihlar, B.P. Murray, C. Yang, M.L. Mitchell, H. Liu, J. Wang, M. Arimilli, E. Eisenberg, K.M. Stray, L.K. Tsai, M. Hatada, X. Chen, J.M. Chen, Y. Wang, M.S. Lee, R.G. Strickley, Q. Iwata, X. Zheng, C.U. Kim, S. Swaminathan, M.C. Desai, W.A. Lee, and L. Xu, MedChemComm, 2011. 2(11): p. 1093-1098; ISI[000297960100010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">25.<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297860100026">Synthesis, Structure-Activity Relationships, and Mechanism of Action of anti-HIV-1 Lamellarin Alpha 20-sulfate Analogues.</a> Kamiyama, H., Y. Kubo, H. Sato, N. Yamamoto, T. Fukuda, F. Ishibashi, and M. Iwao, Bioorganic and Medicinal Chemistry, 2011. 19(24): p. 7541-7550; ISI[000297860100026].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0106-011912.</p>

    <p> </p>

    <p class="plaintext">26.<a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297860100010">A Specific Protein Disorder Catalyzer of HIV-1 Nef.</a> Lugari, A., S. Breuer, T. Coursindel, S. Opi, A. Restouin, X. Shi, A. Nazabal, B.E. Torbett, J. Martinez, Y. Collette, I. Parrot, S.T. Arold, and X. Morelli, Bioorganic and Medicinal Chemistry, 2011. 19(24): p. 7401-7406; ISI[000297860100010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0106-011912.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
