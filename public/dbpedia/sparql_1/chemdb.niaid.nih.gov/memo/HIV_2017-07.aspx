

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="stzbGYPK/JYeZ+au/WA8OiMKsgxbVwvQaGRbFbzisKYQW9CTrbKzr6R/cSOiU4oDdFFMq/P2QVReYNdb+4CJDPIP7iRwOU/mKjUUHdChwE63tczCHqfNGve2pU0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FBD2B2DF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: July, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28645727">Inhibition of Non Canonical HIV-1 Tat Secretion through the Cellular Na+,K+-ATPase Blocks HIV-1 Infection.</a>Agostini, S., H. Ali, C. Vardabasso, A. Fittipaldi, E. Tasciotti, A. Cereseto, A. Bugatti, M. Rusnati, M. Lusic, and M. Giacca. EBioMedicine, 2017. 21: p. 170-181. PMID[28645727]. PMCID[PMC5514404].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000403665700008">Synthesis, X-Ray Structure, in Vitro HIV and Kinesin Eg5 Inhibition Activities of New Arene ruthenium Complexes of Pyrimidine Analogs.</a> Al-Masoudi, W.A., N.A. Al-Masoudi, B. Weibert, and R. Winter. Journal of Coordination Chemistry, 2017. 70(12): p. 2061-2073. ISI[000403665700008].
    <br />
    <b>[WOS]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28521261">Novel Anti-inflammatory Agents Targeting CXCR4: Design, Synthesis, Biological Evaluation and Preliminary Pharmacokinetic Study.</a> Bai, R., Z. Liang, Y. Yoon, E. Salgado, A. Feng, S. Gurbani, and H. Shim. European Journal of Medicinal Chemistry, 2017. 136: p. 360-371. PMID[28521261]. PMCID[PMC5521218].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404984700001">Synthesis, Structural Studies and Biological Evaluation of Halogen Derivatives of 1,3-Disubstituted thiourea.</a>Bielenica, A., K. Stepien, A. Sawczenko, T. Lis, A.E. Koziol, S. Madeddu, D. Collu, F. Iuliano, A. Kosmider, and M. Struga. Letters in Drug Design &amp; Discovery, 2017. 14(6): p. 636-646. ISI[000404984700001].
    <br />
    <b>[WOS]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28675217">G2-S16 Dendrimer as a Candidate for a Microbicide to Prevent HIV-1 Infection in Women.</a> Cena-Diez, R., P. Garcia-Broncano, F. Javier de la Mata, R. Gomez, S. Resino, and M. Munoz-Fernandez. Nanoscale, 2017. 9(27): p. 9732-9742. PMID[28675217].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000405247900001">Structure-Activity Relationships of N-Cinnamoyl and Hydroxycinnamoyl amides on alpha-Glucosidase Inhibition.</a> Chochkova, M.G., P.P. Petrova, B.M. Stoykova, G.I. Ivanova, M. Sticha, G. Dibo, and T.S. Milkova. Journal of Chemistry, 2017. 6080129: 5pp. ISI[000405247900001].
    <br />
    <b>[WOS]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28689087">The Histone Deacetylase Inhibitor SAHA Simultaneously Reactivates HIV-1 from Latency and Up-regulates NKG2D Ligands Sensitizing for Natural Killer Cell Cytotoxicity.</a> Desimio, M.G., E. Giuliani, and M. Doria. Virology, 2017. 510: p. 9-21. PMID[28689087].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28559060">Searching for Novel N1-Substituted benzimidazol-2-ones as Non-Nucleoside HIV-1 RT Inhibitors.</a> Ferro, S., M.R. Buemi, L. De Luca, F.E. Agharbaoui, C. Pannecouque, and A.M. Monforte. Bioorganic &amp; Medicinal Chemistry, 2017. 25(14): p. 3861-3870. PMID[28559060].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28471000">Encapsulation of Ritonavir in Solid Lipid Nanoparticles: In-Vitro anti-HIV-1 Activity Using Lentiviral Particles.</a>Javan, F., A. Vatanara, K. Azadmanesh, M. Nabi-Meibodi, and M. Shakouri. Journal of Pharmacy and Pharmacology, 2017. 69(8): p. 1002-1009. PMID[28471000].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28543773">Degradation of Heme oxygenase-1 by the Immunoproteasome in Astrocytes: A Potential Interferon-Gamma-dependent Mechanism Contributing to HIV Neuropathogenesis.</a> Kovacsics, C.E., A.J. Gill, S.S. Ambegaokar, B.B. Gelman, and D.L. Kolson. GLIA, 2017. 65(8): p. 1264-1277. PMID[28543773].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28539449">Anti-HIV-1 ADCC Antibodies Following Latency Reversal and Treatment Interruption.</a> Lee, W.S., A.B. Kristensen, T.A. Rasmussen, M. Tolstrup, L. Ostergaard, O.S. Sogaard, B.D. Wines, P.M. Hogarth, A. Reynaldi, M.P. Davenport, S. Emery, J. Amin, D.A. Cooper, V.L. Kan, J. Fox, H. Gruell, M.S. Parsons, and S.J. Kent. Journal of Virology, 2017. 91(15): e00603-17. PMID[28539449]. PMCID[PMC5512246].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28628181">Synthesis and Antiviral Evaluation of Base-modified Deoxythreosyl nucleoside phosphonates.</a> Liu, C., S.G. Dumbre, C. Pannecouque, B. Korba, S. De Jonghe, and P. Herdewijn. Organic &amp; Biomolecular Chemistry, 2017. 15(26): p. 5513-5528. PMID[28628181].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28682067">Expanding the Antiviral Spectrum of 3-Fluoro-2-(phosphonomethoxy)propyl acyclic nucleoside phosphonates: Diamyl aspartate amidate Prodrugs.</a> Luo, M., E. Groaz, G. Andrei, R. Snoeck, R. Kalkeri, R.G. Ptak, T. Hartman, R.W. Buckheit, Jr., D. Schols, S. De Jonghe, and P. Herdewijn. Journal of Medicinal Chemistry, 2017. 60(14): p. 6220-6238. PMID[28682067].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28558972">Structure-based Identification of Inhibitors Targeting Obstruction of the HIVgp41 N-Heptad Repeat Trimer.</a>McGee, T.D., Jr., H.A. Yi, W.J. Allen, A. Jacobs, and R.C. Rizzo. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(14): p. 3177-3184. PMID[28558972].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000404864100065">Eutypellazines A-M, Thiodiketopiperazine-type Alkaloids from Deep Sea Derived Fungus Eutypella sp. MCCC 3A00281.</a>Niu, S., D. Liu, Z. Shao, P. Proksch, and W. Lin. RSC Advances, 2017. 7(53): p. 33580-33590. ISI[000404864100065].
    <br />
    <b>[WOS]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28461981">Antiretroviral Activity of a Novel Pyrimidyl-di(diazaspiroalkane) Derivative.</a> Novoselova, E.A., O.B. Riabova, I.A. Leneva, V.G. Nesterenko, R.N. Bolgarin, and V.A. Makarov. Acta Naturae, 2017. 9(1): p. 105-107. PMID[28461981]. PMCID[PMC5406667].<b><br />
    [PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28322889">Anti-HIV Antibody and Drug Combinations Exhibit Synergistic Activity against Drug-resistant HIV-1 Strains.</a>Qi, Q., Q. Wang, W. Chen, F. Yu, L. Du, D.S. Dimitrov, L. Lu, and S. Jiang. The Journal of Infection, 2017. 75(1): p. 68-71. PMID[28322889].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000403705700012">Synthesis and Structural Characterization of New Bioactive Ligands and Their Bi(III) Derivatives.</a>Raheel, A., Imtiaz-ud-Din, S. Andleeb, S. Ramadan, and M.N. Tahir. Applied Organometallic Chemistry, 2017. 31(6): e3632. ISI[000403705700012].
    <br />
    <b>[WOS]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28638104">Anti-proliferative Therapy for HIV Cure: A Compound Interest Approach.</a> Reeves, D.B., E.R. Duke, S.M. Hughes, M. Prlic, F. Hladik, and J.T. Schiffer. Scientific Reports, 2017. 7(4011): 10pp. PMID[28638104]. PMCID[PMC5479830].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28748719">Synthesis of Potential HIV Integrase Inhibitors Inspired by Natural Polyphenol Structures.</a> Righi, G., R. Pelagalli, V. Isoni, I. Tirotta, M. Marini, M. Palagri, R. Dallocchio, A. Dessi, B. Macchi, C. Frezza, G. Forte, A. Dalla Cort, G. Portalone, and P. Bovicelli. Natural Product Research, 2017: p. 1-9. PMID[28748719].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28652425">Evaluation of Biological Activity of Mastic Extracts Based on Chemotherapeutic Indices.</a> Suzuki, R., H. Sakagami, S. Amano, K. Fukuchi, K. Sunaga, T. Kanamoto, S. Terakubo, H. Nakashima, Y. Shirataki, M. Tomomura, Y. Masuda, S. Yokose, A. Tomomura, H. Watanabe, M. Okawara, and Y. Matahira. In Vivo, 2017. 31(4): p. 591-598. PMID[28652425].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28463783">Synthesis and Biological Evaluation of Chemokine Receptor Ligands with 2-Benzazepine Scaffold.</a>Thum, S., A.K. Kokornaczyk, T. Seki, M. De Maria, N.V.O. Zacarias, H. de Vries, C. Weiss, M. Koch, D. Schepmann, M. Kitamura, N. Tschammer, L.H. Heitman, A. Junker, and B. Wuensch. European Journal of Medicinal Chemistry, 2017. 135: p. 401-413. PMID[28463783].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28584151">Discovery and Characterization of a Novel CD4-Binding Adnectin with Potent anti-HIV Activity.</a> Wensel, D., Y. Sun, Z. Li, S. Zhang, C. Picarillo, T. McDonagh, D. Fabrizio, M. Cockett, M. Krystal, and J. Davis. Antimicrobial Agents and Chemotherapy, 2017. 61(8): e00508-17. PMID[28584151].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28559249">MK-8591 (4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine) Exhibits Potent Activity against HIV-2 Isolates and Drug-resistant HIV-2 Mutants in Culture.</a>Wu, V.H., R.A. Smith, S. Masoum, D.N. Raugi, S. Ba, M. Seydi, J.A. Grobler, and G.S. Gottlieb. Antimicrobial Agents and Chemotherapy, 2017. 61(8): e00744-17. PMID[28559249].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28381571">Discovery of Novel Small-molecule Inhibitors of Lim Domain Kinase for Inhibiting HIV-1.</a> Yi, F., J. Guo, D. Dabbagh, M. Spear, S. He, K. Kehn-Hall, J. Fontenot, Y. Yin, M. Bibian, C.M. Park, K. Zheng, H.J. Park, V. Soloveva, D. Gharaibeh, C. Retterer, R. Zamani, M.L. Pitt, J. Naughton, Y. Jiang, H. Shang, R.M. Hakami, B. Ling, J.A.T. Young, S. Bavari, X. Xu, Y. Feng, and Y. Wu. Journal of Virology, 2017. 91(13): e02418-16. PMID[28381571]. PMCID[PMC5469273].
    <br />
    <b>[PubMed]</b>. HIV_07_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170706&amp;CC=WO&amp;NR=2017115329A1&amp;KC=A1">Preparation of C-3 Triterpenone Derivatives as HIV Inhibitors.</a> Bandi, P.R., R.R. Kura, D.K. Gazula Levi, P.R. Adulla, and B.R. Kasireddy. Patent. 2017. 2016-IB58104 2017115329: 315pp.
    <br />
    <b>[Patent]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170713&amp;CC=WO&amp;NR=2017120549A1&amp;KC=A1">D-Peptides Compositions for Inhibiting HIV Entry into Host Cells.</a> Welch, B.D., J.N. Francis, and M.S. Kay. Patent. 2017. 2017-US12640 2017120549: 91pp.
    <br />
    <b>[Patent]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170706&amp;CC=WO&amp;NR=2017113288A1&amp;KC=A1">Preparation of Fused Tricyclic Heterocycles as HIV Integrase Inhibitors for the Treatment and Prophylaxis of HIV Infection.</a> Yu, T., S.T. Waddell, T.H. Graham, J.A. McCauley, A.W. Stamford, J. Cai, and Z. Qi. Patent. 2017. 2015-CN100106 2017113288: 70pp.
    <br />
    <b>[Patent]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">29. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170706&amp;CC=WO&amp;NR=2017116928A1&amp;KC=A1">Preparation of Fused Tricyclic Heterocycles as HIV Integrase Inhibitors for the Treatment and Prophylaxis of HIV Infection.</a> Yu, T., S.T. Waddell, T.H. Graham, Y. Zhang, J.A. McCauley, A.W. Stamford, J. Cai, and Z. Qi. Patent. 2017. 2016-US68173 2017116928: 83pp.
    <br />
    <b>[Patent]</b>. HIV_07_2017.</p><br />

    <p class="plaintext">30. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170622&amp;CC=WO&amp;NR=2017106071A1&amp;KC=A1">Spirocyclic Quinolizine Derivatives Useful as HIV Integrase Inhibitors.</a> Zhang, Y., S.T. Waddell, T. Yu, J.A. McCauley, and A. Stamford. Patent. 2017. 2016-US66078 2017106071: 68pp.
    <br />
    <b>[Patent]</b>. HIV_07_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
