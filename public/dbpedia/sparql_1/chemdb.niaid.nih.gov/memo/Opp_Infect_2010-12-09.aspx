

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-12-09.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BRYl9GjeGmLYmhPjK+almvpETsYUhBdmdcKzfs7mMSTGmVlkSjtJUuQ6RBQTfJ3qsscDsm1mKWB2n/l8jQZp/P2nTV2RkJ4SgXuwy/FHZVpFf6vvSXAFMI0WU8s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5A25C185" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">November 24 - December 9, 2010</p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21044846">Acetazolamide-based fungal chitinase inhibitors.</a> Schuttelkopf, A.W., L. Gros, D.E. Blair, J.A. Frearson, D.M. van Aalten, and I.H. Gilbert. Bioorganic &amp;  Medicinal Chemistry, 2010. 18(23): p. 8334-40; PMID[21044846].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21040515">Activity against biofilms of Candida albicans.</a> Hua, J., R. Yamarthy, S. Felsenstein, R.W. Scott, K. Markowitz, and G. Diamond. Activity of antimicrobial peptide mimetics in the oral cavity: I. Molecular Oral Microbiology, 2010. 25(6): p. 418-25; PMID[21040515].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20950895">Novel conformationally restricted triazole derivatives with potent antifungal activity.</a> Wang, W., S. Wang, Y. Liu, G. Dong, Y. Cao, Z. Miao, J. Yao, W. Zhang, and C. Sheng. European Journal of Medicinal Chemistry, 2010. 45(12): p. 6020-6; PMID[20950895].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20933306">Synthesis and in vitro antimicrobial evaluation of novel fluoroquinolone derivatives.</a> Srinivasan, S., R.M. Beema Shafreen, P. Nithyanand, P. Manisankar, and S.K. Pandian. European Journal of Medicinal Chemistry, 2010. 45(12): p. 6101-5; PMID[20933306].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21050767">Design, synthesis and inhibition activity of novel cyclic peptides against protein tyrosine phosphatase A from Mycobacterium tuberculosis.</a> Chandra, K., D. Dutta, A.K. Das, and A. Basak. Bioorganic &amp;  Medicinal Chemistry, 2010. 18(23): p. 8365-73; PMID[21050767].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21044844">Highly active antimycobacterial derivatives of benzoxazine.</a> Petrlikova, E., K. Waisser, H. Divisova, P. Husakova, P. Vrabcova, J. Kunes, K. Kolar, and J. Stolarikova. Bioorganic and Medicinal Chemistry, 2010. 18(23): p. 8178-87; PMID[21044844].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21041091">Synthesis and bio-evaluation of alkylaminoaryl phenyl cyclopropyl methanones as antitubercular and antimalarial agents.</a> Ajay, A., V. Singh, S. Singh, S. Pandey, S. Gunjan, D. Dubey, S.K. Sinha, B.N. Singh, V. Chaturvedi, R. Tripathi, R. Ramchandran, and R.P. Tripathi. Bioorganic &amp;  Medicinal Chemistry, 2010. 18(23): p. 8289-301; PMID[21041091].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20961671">Synthesis, biological activity, and evaluation of the mode of action of novel antitubercular benzofurobenzopyrans substituted on A ring.</a> Termentzi, A., I. Khouri, T. Gaslonde, S. Prado, B. Saint-Joanis, F. Bardou, E.P. Amanatiadou, I.S. Vizirianakis, J. Kordulakova, M. Jackson, R. Brosch, Y.L. Janin, M. Daffe, F. Tillequin, and S. Michel. European Journal of Medicinal Chemistry, 2010. 45(12): p. 5833-47; PMID[20961671].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20951473">Design, synthesis and inhibitory activity against Mycobacterium tuberculosis thymidine monophosphate kinase of acyclic nucleoside analogues with a distal imidazoquinolinone.</a> Familiar, O., H. Munier-Lehmann, J.A. Ainsa, M.J. Camarasa, and M.J. Perez-Perez. European Journal of Medicinal Chemistry, 2010. 45(12): p. 5910-8; PMID[20951473].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20937542">New amino acid esters of salicylanilides active against MDR-TB and other microbes.</a> Kratky, M., J. Vinsova, V. Buchta, K. Horvati, S. Bosze, and J. Stolarikova. European Journal of Medicinal Chemistry, 2010. 45(12): p. 6106-13; PMID[20937542].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20932607">A regio- and stereoselective 1,3-dipolar cycloaddition for the synthesis of novel spiro-pyrrolothiazolyloxindoles and their antitubercular evaluation.</a> Prasanna, P., K. Balamurugan, S. Perumal, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2010. 45(12): p. 5653-61; PMID[20932607].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20828886">Synthesis and antitubercular activities of substituted benzoic acid N&#39;-(substituted benzylidene/furan-2-ylmethylene)-N-(pyridine-3-carbonyl)-hydrazides.</a> Kumar, P., B. Narasimhan, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2010. 45(12): p. 6085-9; PMID[20828886].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1124-120810.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283906100020">Spectroscopic, magnetic and thermal studies of Co(II), Ni(II), Cu(II) and Zn(II) complexes of 3-acetylcoumarin-isonicotinoylhydrazone and their antimicrobial and anti-tubercular activity evaluation.</a> Hunoor, R.S., B.R. Patil, D.S. Badiger, R.S. Vadavi, K.B. Gudasi, V.M. Chandrashekhar, and I.S. Muchchandi. Spectrochimica Acta Part a-Molecular and Biomolecular Spectroscopy, 2010. 77(4): p. 838-844; ISI[000283906100020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283977900013">Antituberculosis, antifungal and thermal activity of mixed ligand transition metal complexes.</a> Kharadi, G.J., J.R. Patel, and B.Z. Dholakiya. Applied Organometallic Chemistry, 2010. 24(11): p. 821-827; ISI[000283977900013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1124-120810.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000283911300052">Synthesis and pharmacological evaluation of clubbed isopropylthiazole derived triazolothiadiazoles, triazolothiadiazines and mannich bases as potential antimicrobial and antitubercular agents.</a> Kumar, G.V.S., Y.R. Prasad, B.P. Mallikarjuna, and S.M. Chandrashekar. European Journal of Medicinal Chemistry, 2010. 45(11): p. 5120-5129; ISI[000283911300052].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1124-120810.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
