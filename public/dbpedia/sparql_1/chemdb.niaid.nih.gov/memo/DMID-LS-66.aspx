

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-66.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="oKrYT0YlLIOHrdQfNVFkujEGWunCuQ2GlrGXMcJ7B9cblDR5+Usy0rO96gZDKanahKc+BaGUnis6KgV6+s7ZFnAUtC0wDfAN8RinQ4+xPTgO1Vwi4/u3GWRnNpU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D2DC70F5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-66-MEMO</b> </p>

<p>    1.    5950   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Probing the structure of the SARS coronavirus using scanning electron microscopy</p>

<p>           Lin, Y, Yan, X, Cao, W, Wang, C, Feng, J, Duan, J, and Xie, S</p>

<p>           Antivir Ther 2004. 9(2): 287-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134191&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15134191&amp;dopt=abstract</a> </p><br />

<p>    2.    5951   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Interfering with hepatitis C virus RNA replication</p>

<p>           Randall, Glenn and Rice, Charles M</p>

<p>           Virus Research 2004. 102(1): 19-25</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4BVP8BS-1/2/8147c77ae24eb54ce22b9c2daed7c0b5">http://www.sciencedirect.com/science/article/B6T32-4BVP8BS-1/2/8147c77ae24eb54ce22b9c2daed7c0b5</a> </p><br />

<p>    3.    5952   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Targeting internal ribosome entry site (IRES)-mediated translation to block hepatitis C and other RNA viruses*1</p>

<p>           Dasgupta, Asim, Das, Saumitra, Izumi, Raquel, Venkatesan, Arun, and Barat, Bhaswati</p>

<p>           FEMS Microbiology Letters 2004. 234(2):  189-199</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2W-4C4BWTG-3/2/b34feebe780da1a8a137b3d2e57e355a">http://www.sciencedirect.com/science/article/B6T2W-4C4BWTG-3/2/b34feebe780da1a8a137b3d2e57e355a</a> </p><br />

<p>    4.    5953   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Evaluation of Homology Modeling of the Severe Acute Respiratory Syndrome (SARS) Coronavirus Main Protease for Structure Based Drug Design</p>

<p>           Takeda-Shitaka, M, Nojima, H, Takaya, D, Kanou, K, Iwadate, M, and Umeyama, H</p>

<p>           Chem Pharm Bull (Tokyo) 2004. 52(5): 643-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133227&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15133227&amp;dopt=abstract</a> </p><br />

<p>    5.    5954   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Structural and functional features of the polycationic peptide required for inhibition of herpes simplex virus invasion of cells</p>

<p>           Trybala, E, Olofsson, S, Mardberg, K, Svennerholm, B, Umemoto, K, Glorioso, JC, and Bergstrom, T</p>

<p>           Antiviral Res  2004. 62(3): 125-34</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130536&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130536&amp;dopt=abstract</a> </p><br />

<p>    6.    5955   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Evaluation of antiviral activity against human herpesvirus 8 (HHV-8) and Epstein-Barr virus (EBV) by a quantitative real-time PCR assay</p>

<p>           Friedrichs, C, Neyts, J, Gaspar, G, Clercq, ED, and Wutzler, P</p>

<p>           Antiviral Res  2004. 62(3): 121-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130535&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15130535&amp;dopt=abstract</a> </p><br />

<p>    7.    5956   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Optimizing PEG-interferon and ribavirin combination therapy for patients infected with HCV-2 or HCV-3: is the puzzle completed?</p>

<p>           Alberti, Alfredo</p>

<p>           Journal of Hepatology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5VKTW-3/2/a1b3828b76808995ddd7f6b80410bbb1">http://www.sciencedirect.com/science/article/B6W7C-4C5VKTW-3/2/a1b3828b76808995ddd7f6b80410bbb1</a> </p><br />

<p>    8.    5957   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Cellular and Humoral Immunity against Vaccinia Virus Infection of Mice</p>

<p>           Xu, R, Johnson, AJ, Liggitt, D, and Bevan, MJ</p>

<p>           J Immunol 2004. 172(10): 6265-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128815&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128815&amp;dopt=abstract</a> </p><br />

<p>    9.    5958   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           CC-chemokine receptor 5 (CCR5) in hepatitis C--at the crossroads of the antiviral immune response?</p>

<p>           Ahlenstiel, G, Woitas, RP, Rockstroh, J, and Spengler, U</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128728&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15128728&amp;dopt=abstract</a> </p><br />

<p>  10.    5959   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Modeling hamsters for evaluating West Nile virus therapies</p>

<p>           Morrey, John D, Day, Craig W, Julander, Justin G, Olsen, Aaron L, Sidwell, Robert W, Cheney, Carl D, and Blatt, Lawrence M</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4C56JPD-1/2/8c1ed6a25618df30c0374e0bf483d393">http://www.sciencedirect.com/science/article/B6T2H-4C56JPD-1/2/8c1ed6a25618df30c0374e0bf483d393</a> </p><br />

<p>  11.    5960   DMID-LS-66; WOS-DMID-5/12/2004</p>

<p class="memofmt1-2">           In Vitro Evaluation of Cyanovirin-N Antiviral Activity, by Use of Lentiviral Vectors Pseudotyped With Filovirus Envelope Glycoproteins</p>

<p>           Barrientos, LG, Lasala, F, Otero, JR, Sanchez, A, and Delgado, R</p>

<p>           Journal of Infectious Diseases 2004. 189(8): 1440-1443</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    5961   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Synthesis and evaluation of novel 1,4-naphthoquinone derivatives as antiviral, antifungal and anticancer agents</p>

<p>           Tandon, VK, Singh, RV, and Yadav, DB</p>

<p>           Bioorg Med Chem Lett 2004. 14(11):  2901-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15125956&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15125956&amp;dopt=abstract</a> </p><br />

<p>  13.    5962   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           New drugs and treatment for respiratory syncytial virus</p>

<p>           Maggon, K and Barik, S</p>

<p>           Rev Med Virol  2004. 14(3): 149-68</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124232&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15124232&amp;dopt=abstract</a> </p><br />

<p>  14.    5963   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Complementary and alternative therapies in the treatment of chronic hepatitis C: a systematic review</p>

<p>           Coon, Joanna Thompson and Ernst, Edzard</p>

<p>           Journal of Hepatology 2004. 40(3): 491-500</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4BRRC3V-R/2/060a2ffb94b6644d9fd159a9a1f09f3e">http://www.sciencedirect.com/science/article/B6W7C-4BRRC3V-R/2/060a2ffb94b6644d9fd159a9a1f09f3e</a> </p><br />

<p>  15.    5964   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Inhibition of respiratory syncytial virus infection with the CC chemokine RANTES (CCL5)</p>

<p>           Elliott, MB, Tebbey, PW, Pryharski, KS, Scheuer, CA, Laughlin, TS, and Hancock, GE</p>

<p>           J Med Virol 2004. 73(2): 300-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15122808&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15122808&amp;dopt=abstract</a> </p><br />

<p>  16.    5965   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Antiherpetic mode of action of (22S,23S)-3beta-bromo-5alpha,22,23-trihydroxystigmastan-6-one in vitro</p>

<p>           Wachsman, MB, Castilla, V, Talarico, LB, Ramirez, JA, Galagovsky, LR, and Coto, CE</p>

<p>           Int J Antimicrob Agents 2004. 23(5): 524-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15120737&amp;dopt=abstract</a> </p><br />

<p>  17.    5966   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Stability and inactivation of SARS coronavirus</p>

<p>           Rabenau, HF, Cinatl, J, Morgenstern, B, Bauer, G, Preiser, W, and Doerr, HW</p>

<p>           Med Microbiol Immunol (Berl) 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15118911&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15118911&amp;dopt=abstract</a> </p><br />

<p>  18.    5967   DMID-LS-66; WOS-DMID-5/12/2004</p>

<p class="memofmt1-2">           West Nile Virus: Pathogenesis and Therapeutic Options</p>

<p>           Gea-Banacloche, J, Johnson, RT, Bagic, A, Butman, JA, Murray, PR, and Agrawal, AG</p>

<p>           Annals of Internal Medicine 2004. 140(7): 545-553</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  19.    5968   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           New therapies on the horizon for hepatitis C</p>

<p>           Sookoian, SC</p>

<p>           Ann Hepatol 2003. 2(4): 164-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115955&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115955&amp;dopt=abstract</a> </p><br />

<p>  20.    5969   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Peptide-based inhibitors of the hepatitis C virus NS3 protease: structure-activity relationship at the C-terminal position</p>

<p>           Rancourt, J, Cameron, DR, Gorys, V, Lamarre, D, Poirier, M, Thibeault, D, and Llinas-Brunet, M</p>

<p>           J Med Chem 2004. 47(10): 2511-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115394&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115394&amp;dopt=abstract</a> </p><br />

<p>  21.    5970   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Non-peptidic small-molecule inhibitors of the single-chain hepatitis C virus NS3 protease/NS4A cofactor complex discovered by structure-based NMR screening</p>

<p>           Wyss, DF, Arasappan, A, Senior, MM, Wang, YS, Beyer, BM, Njoroge, FG, and McCoy, MA</p>

<p>           J Med Chem 2004. 47(10): 2486-98</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115392&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15115392&amp;dopt=abstract</a> </p><br />

<p>  22.    5971   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Mouse models for studying orthopoxvirus respiratory infections</p>

<p>           Schriewer, J, Buller, RM, and Owens, G</p>

<p>           Methods Mol Biol 2004. 269: 289-308</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15114022&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15114022&amp;dopt=abstract</a> </p><br />

<p>  23.    5972   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           RhoA-derived peptide dimers share mechanistic properties with other polyanionic inhibitors of respiratory syncytial virus (RSV), including disruption of viral attachment and dependence on RSV G</p>

<p>           Budge, PJ, Li, Y, Beeler, JA, and Graham, BS</p>

<p>           J Virol 2004. 78(10): 5015-22</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113882&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15113882&amp;dopt=abstract</a> </p><br />

<p>  24.    5973   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Molecular mechanisms of influenza virus resistance to neuraminidase inhibitors</p>

<p>           Gubareva, Larisa V</p>

<p>           Virus Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4CBVP8P-1/2/4867771459d26fd7cd4de63203563e49">http://www.sciencedirect.com/science/article/B6T32-4CBVP8P-1/2/4867771459d26fd7cd4de63203563e49</a> </p><br />

<p>  25.    5974   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Anti-HCV activities of selective polyunsaturated fatty acids</p>

<p>           Leu, GZ, Lin, TY, and Hsu, JT</p>

<p>           Biochem Biophys Res Commun 2004. 318(1):  275-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110784&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15110784&amp;dopt=abstract</a> </p><br />

<p>  26.    5975   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           A polysaccharide fraction from medicinal herb Prunella vulgaris downregulates the expression of herpes simplex virus antigen in Vero cells</p>

<p>           Chi-Ming Chiu, Lawrence, Zhu, Wen, and Eng-Choon Ooi, Vincent</p>

<p>           Journal of Ethnopharmacology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T8D-4C8NR55-4/2/8a43b510cdd84ca51a366220531f45ce">http://www.sciencedirect.com/science/article/B6T8D-4C8NR55-4/2/8a43b510cdd84ca51a366220531f45ce</a> </p><br />

<p>  27.    5976   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Identification of a human influenza type B strain with reduced sensitivity to neuraminidase inhibitor drugs</p>

<p>           Hurt, Aeron C, McKimm-Breschkin, Jennifer L, McDonald, Mandy, Barr, Ian G, Komadina, Naomi, and Hampson, Alan W</p>

<p>           Virus Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4C83CF8-1/2/1919adfb34bff668d57501a0b86f31ff">http://www.sciencedirect.com/science/article/B6T32-4C83CF8-1/2/1919adfb34bff668d57501a0b86f31ff</a> </p><br />

<p>  28.    5977   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           A novel means of identifying the neuraminidase type of currently circulating human A(H1) influenza viruses</p>

<p>           Hurt, Aeron C, Barr, Ian G, Komadina, Naomi, and Hampson, Alan W</p>

<p>           Virus Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4C47KNC-5/2/faeeb8d973b41845902c8be271760e02">http://www.sciencedirect.com/science/article/B6T32-4C47KNC-5/2/faeeb8d973b41845902c8be271760e02</a> </p><br />

<p>  29.    5978   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           In vitro activity of expanded-spectrum pyridazinyl oxime ethers related to pirodavir: novel capsid-binding inhibitors with potent antipicornavirus activity</p>

<p>           Barnard, DL,  Hubbard, VD, Smee, DF, Sidwell, RW, Watson, KG, Tucker, SP, and Reece, PA</p>

<p>           Antimicrob Agents Chemother 2004. 48(5): 1766-1772</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105133&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105133&amp;dopt=abstract</a> </p><br />

<p>  30.    5979   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Highly potent and long-acting trimeric and tetrameric inhibitors of influenza virus neuraminidase</p>

<p>           Watson, Keith G, Cameron, Rachel, Fenton, Rob J, Gower, David, Hamilton, Stephanie, Jin, Betty, Krippner, Guy Y, Luttick, Angela, McConnell, Darryl, and MacDonald, Simon JF</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(6): 1589-1592</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-F/2/8e046d1712bd314855bf3b89831dce9a">http://www.sciencedirect.com/science/article/B6TF9-4BT8F6G-F/2/8e046d1712bd314855bf3b89831dce9a</a> </p><br />

<p>  31.    5980   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Mechanism of action of the ribopyranoside benzimidazole GW275175X against human cytomegalovirus</p>

<p>           Underwood, MR, Ferris, RG, Selleseth, DW, Davis, MG, Drach, JC, Townsend, LB, Biron, KK, and Boyd, FL</p>

<p>           Antimicrob Agents Chemother 2004.  48(5): 1647-51</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105116&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15105116&amp;dopt=abstract</a> </p><br />

<p>  32.    5981   DMID-LS-66; EMBASE-DMID-5/12/2004</p>

<p class="memofmt1-2">           Structural proteomics of the poxvirus family</p>

<p>           Randall, Arlo Z, Baldi, Pierre, and Villarreal, Luis P</p>

<p>           Artificial Intelligence in Medicine 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T4K-4C60743-2/2/7070cc7eb5333d72fc2a6e68c2d63910">http://www.sciencedirect.com/science/article/B6T4K-4C60743-2/2/7070cc7eb5333d72fc2a6e68c2d63910</a> </p><br />

<p>  33.    5982   DMID-LS-66; WOS-DMID-5/12/2004</p>

<p class="memofmt1-2">           Design and Syntheses of 1,6-Naphthalene Derivatives as Selective Hcmv Protease Inhibitors</p>

<p>           Gopalsamy, A, Lim, K, Ellingboe, JW, Mitsner, B, Nikitenko, A, Upeslacis, J, Mansour, TS, Olson, MW, Bebernitz, GA, Grinberg, D, Feld, B, Moy, FJ, and O&#39;connell, J</p>

<p>           Journal of Medicinal Chemistry 2004. 47(8): 1893-1899</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    5983   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           West Nile virus: a growing concern?</p>

<p>           Gould, LH and Fikrig, E</p>

<p>           J Clin Invest  2004. 113(8): 1102-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15085186&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15085186&amp;dopt=abstract</a> </p><br />

<p>  35.    5984   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Structure-activity relationship of purine ribonucleosides for inhibition of hepatitis C virus RNA-dependent RNA polymerase</p>

<p>           Eldrup, AB, Allerson, CR, Bennett, CF, Bera, S, Bhat, B, Bhat, N, Bosserman, MR, Brooks, J, Burlein, C, Carroll, SS, Cook, PD, Getty, KL, MacCoss, M, McMasters, DR, Olsen, DB, Prakash, TP, Prhavc, M, Song, Q, Tomassini, JE, and Xia, J</p>

<p>           J Med Chem 2004. 47(9): 2283-95</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084127&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15084127&amp;dopt=abstract</a> </p><br />

<p>  36.    5985   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Antiviral activity of antimicrobial cationic peptides against Junin virus and herpes simplex virus</p>

<p>           Albiol, Matanic VC and Castilla, V</p>

<p>           Int J Antimicrob Agents 2004. 23(4): 382-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081088&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15081088&amp;dopt=abstract</a> </p><br />

<p>  37.    5986   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Hepatitis C drug being developed</p>

<p>           Anon</p>

<p>           AIDS Patient Care STDS 2004. 18(1): 62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080102&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15080102&amp;dopt=abstract</a> </p><br />

<p>  38.    5987   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           Synthesis of (+/-)-hamigeran B, (-)-hamigeran B, and (+/-)-1-epi-hamigeran B: use of bulky silyl groups to protect a benzylic carbon-oxygen bond from hydrogenolysis</p>

<p>           Clive, DL and Wang, J</p>

<p>           J Org Chem 2004. 69(8): 2773-84</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074927&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074927&amp;dopt=abstract</a> </p><br />

<p>  39.    5988   DMID-LS-66; PUBMED-DMID-5/12/2004</p>

<p class="memofmt1-2">           The substrate activity of (S)-9-[3-hydroxy-(2-phosphonomethoxy)propyl]adenine diphosphate toward DNA polymerases alpha, delta and epsilon</p>

<p>           Birkus, G, Rejman, D, Otmar, M, Votruba, I, Rosenberg, I, and Holy, A</p>

<p>           Antivir Chem Chemother 2004. 15(1): 23-33</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074712&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15074712&amp;dopt=abstract</a> </p><br />

<p>  40.    5989   DMID-LS-66; WOS-DMID-5/12/2004</p>

<p class="memofmt1-2">           Human Herpesvirus 8: an Update</p>

<p>           De Paoli, P</p>

<p>           Microbes and Infection 2004. 6(3): 328-335</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  41.    5990   DMID-LS-66; WOS-DMID-5/12/2004</p>

<p class="memofmt1-2">           Development of Potent Inhibitors of the Sars Associated Coronavirus Protease 3cl(Pro)</p>

<p>           Bacha, UM, Barrila, JA, Velazquez-Campoy, A, Leavitt, S, and Freire, E</p>

<p>           Biophysical Journal 2004. 86(1): 97A</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
