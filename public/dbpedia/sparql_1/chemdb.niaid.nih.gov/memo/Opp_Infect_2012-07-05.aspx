

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-07-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/aeCbd8KFujpPNhSVKs5NaRYYzCu5aGEmrxNNG4u9nSS1Faw6PEN9731xUM9tbsKhGKUDYGvZh0QQTRgXG8ZwUaoAQ8oR7rxy44gRgmPjPXqtmRt8scR7Vf4N7A=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="189618A2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  June 22 - July 5, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22749988">Purification and Characterization of Hb 98-114: A Novel Hemoglobin-derived Antimicrobial Peptide from the Midgut of Rhipicephalus (Boophilus) microplus.</a> Belmonte, R., C.E. Cruz, J.R. Pires, and S. Daffre. Peptides, 2012. <b>[Epub ahead of print]</b>; PMID[22749988].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22520151">Investigation on the Pharmacological Profile of 2,6-Diacetylpyridine bis(benzoylhydrazone) Derivatives and Their Antimony(III) and Bismuth(III) Complexes.</a> Ferraz, K.S., N.F. Silva, J.G. da Silva, L.F. de Miranda, C.F. Romeiro, E.M. Souza-Fagundes, I.C. Mendes, and H. Beraldo. European Journal of Medicinal Chemistry, 2012. 53C: p. 98-106; PMID[22520151].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22735612">Chemoenzymatic Synthesis, Structural Study and Biological Activity of Novel Indolizidine and Quinolizidine Iminocyclitols.</a> Gomez, L., X. Garrabou, J. Joglar, J. Bujons, T. Parella, C. Vilaplana, P.J. Cardona, and P. Clapes. Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22735612].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22727093">Antifungal Activity of Coronarin D against Candida albicans.</a> Kaomongkolgit, R., K. Jamdee, S. Wongnoi, N. Chimnoi, and S. Techasakul. Oral Surgery, Oral Medicine, Oral Pathology and Oral Radiology, 2012. 114(1): p. 61-66; PMID[22727093].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22546149">Phytochemical and Antimicrobial Investigations of Stilbenoids and Flavonoids Isolated from Three Species of Combretaceae.</a> Katerere, D.R., A.I. Gray, R.J. Nash, and R.D. Waigh. Fitoterapia, 2012. 83(5): p. 932-940; PMID[22546149].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22747736">Antimicrobial Activities of the Methanol Extract and Compounds from the Twigs of Dorstenia mannii (Moraceae).</a> Mbaveng, A.T.D., V.D. Kuete, B.D. Ngameni, V.P.P. Beng, B.T.P. Ngadjui, J.J.P. Marion Meyer, and L.P. Namrita. BMC Complementary and Alternative Medicine, 2012. 12(1): p. 83; PMID[22747736].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22732728">Pharmacological Properties and Protein Binding Capacity of Phenolic Extracts of Some Venda Medicinal Plants Used against Cough and Fever.</a> Mulaudzi, R.B., A.R. Ndhlala, M.G. Kulkarni, and J. Van Staden. Journal of Ethnopharmacology, 2012. <b>[Epub ahead of print]</b>; PMID[22732728].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22747978">Identification of Antimicrobial Compound, Diketopiperazines, from a Bacillus sp. N Strain Associated with a Rhabditid Entomopathogenic Nematode against Major Plant-pathogenic Fungi.</a> Nishanth Kumar, S., C. Mohandas, J.V. Siji, K.N. Rajasekharan, and B. Nambisan. Journal of Applied Microbiology, 2012. <b>[Epub ahead of print]</b>; PMID[22747978].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22516426">Synthesis of Benzimidazolyl-1,3,4-oxadiazol-2ylthio-N-phenyl (Benzothiazolyl) acetamides as Antibacterial, Antifungal and Antituberculosis Agents.</a> Patel, R.V., P.K. Patel, P. Kumari, D.P. Rajani, and K.H. Chikhalia. European Journal of Medicinal Chemistry, 2012. 53C: p. 41-51; PMID[22516426]. <b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22543169">Evaluation of Antimicrobial Activity of Ethanol Extract and Compounds Isolated from Trichodesma indicum (Linn.) R. Br. Root.</a> Perianayagam, J.B., S.K. Sharma, K.K. Pillai, A. Pandurangan, and D. Kesavan. Journal of Ethnopharmacology, 2012. 142(1): p. 283-286; PMID[22543169].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22516425">Synthesis and Characterization of Some New Complexes of Cu(II), Ni(II) and V(IV) with Schiff Base Derived from Indole-3-carboxaldehyde.</a> Biological Activity on Prokaryotes and Eukaryotes. Rosu, T., E. Pahontu, D.C. Ilies, R. Georgescu, M. Mocanu, M. Leabu, S. Shova, and A. Gulea. European Journal of Medicinal Chemistry, 2012. 53C: p. 380-389; PMID[22516425].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22560629">Synthesis, Anti-candida Activity, and Cytotoxicity of New (4-(4-Iodophenyl)thiazol-2-yl)hydrazine Derivatives.</a> Secci, D., B. Bizzarri, A. Bolasco, S. Carradori, M. D&#39;Ascenzio, D. Rivanera, E. Mari, L. Polletta, and A. Zicari. European Journal of Medicinal Chemistry, 2012. 53C: p. 246-253; PMID[22560629].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22716223">Viral Protease Inhibitors Affect the Production of Virulence Factors in Cryptococcus neoformans.</a> Sidrim, J.J., L.V. Perdigao-Neto, R.A. Cordeiro, R.S. Brilhante, J.J. Leite, C.E. Teixeira, A.J. Monteiro, R.M. Freitas, J.F. Ribeiro, J.R. Mesquita, M.V. Goncalves, and M.F. Rocha. Canadian Journal of Microbiology, 2012. 58(7): p. 932-936; PMID[22716223].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22735601">Triterpenoids of Euphorbia kansuensis Proch.</a> Zhang, B.B., X.L. Han, Q. Jiang, Z.X. Liao, C. Liu, and Y.B. Qu. Fitoterapia, 2012. <b>[Epub ahead of print]</b>; PMID[22735601].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22633120">Synthesis and Antimycobacterial Activity of Some Phthalimide Derivatives.</a> Akgun, H., I. Karamelekoglu, B. Berk, I. Kurnaz, G. Saribiyik, S. Oktem, and T. Kocagoz. Bioorganic &amp; Medicinal Chemistry, 2012. 20(13): p. 4149-4154; PMID[22633120].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22575534">A Solvent Free, Four-Component Synthesis and 1,3-Dipolar Cycloaddition of 4(H)-Pyrans with Nitrile Oxides: Synthesis and Discovery of Antimycobacterial Activity of Enantiomerically Pure 1,2,4-Oxadiazoles.</a> Almansour, A.I., R. Suresh Kumar, N. Arumugam, and D. Sriram. European Journal of Medicinal Chemistry, 2012. 53C: p. 416-423; PMID[22575534].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p>    
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22738293">Discovery of Novel N-phenyl-phenoxyacetamide Derivatives as EthR Inhibitors and Ethionamide Boosters by Combining High-throughput Screening and Synthesis.</a> Flipo, M., N. Willand, N. Lecat-Guillet, C. Hounsou, D. Matthieu, F. Leroux, Z. Lens, V. Villeret, W. Alexandre, R. Wintjens, T. Christophe, H.K. Jeon, C. Locht, P. Brodin, A. Baulard, and B. Deprez. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22738293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22730136">Antimycobacterial Activity of Pyrimido[4,5-B]diazepine Derivatives.</a> Insuasty, B., A. Garcia, J. Bueno, J. Quiroga, R. Abonia, and A. Ortiz. Archiv Der Pharmazie, 2012. <b>[Epub ahead of print]</b>; PMID[22730136].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22717728">Synthesis, Characterisation and Antitubercular Activities of a Series of Pyruvate-containing Aroylhydrazones and Their Cu-complexes.</a> Jamadar, A., A.K. Duhme-Klair, K. Vemuri, M. Sritharan, P. Dandawate, and S. Padhye. Dalton Transactions (Cambridge, England : 2003), 2012. <b>[Epub ahead of print]</b>; PMID[22717728].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22620987">Synthesis and Biological Evaluation of Purpurealidin E-derived Marine Sponge Metabolites: Aplysamine-2, Aplyzanzine a, and Suberedamines a and B.</a> Kottakota, S.K., D. Evangelopoulos, A. Alnimr, S. Bhakta, T.D. McHugh, M. Gray, P.W. Groundwater, E.C. Marrs, J.D. Perry, C.D. Spilling, and J.J. Harburn. Journal of Natural Products, 2012. 75(6): p. 1090-1101; PMID[22620987].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22737986">Click Chemistry Approach for bis-Chromenyl-triazole Hybrids and Their Antitubercular Activity.</a> Naik, R.J., M.V. Kulkarni, K.S. Pai, and P.G. Nayak. Chemical Biology &amp; Drug design, 2012. <b>[Epub ahead of print]</b>; PMID[22737986].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22626551">Synthesis of Leubethanol Derivatives and Evaluation against Mycobacterium tuberculosis.</a> Perez-Meseguer, J., E. Del Olmo, B. Alanis-Garza, R. Escarcena, E. Garza-Gonzalez, R. Salazar-Aranda, A.S. Feliciano, and N.W. de Torres. Bioorganic &amp; Medicinal Chemistry, 2012. 20(13): p. 4155-4163; PMID[22626551].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22735598">Phenylpropanoids of Alpinia galanga as Efflux Pump Inhibitors in Mycobacterium smegmatis mc(2) 155.</a> Roy, S.K., S. Pahwa, H. Nandanwar, and S.M. Jachak. Fitoterapia, 2012. <b>[Epub ahead of print]</b>; PMID[22735598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22569076">Effect of Antimicrobial Peptides on ATPase Activity and Proton Pumping in Plasma Membrane Vesicles Obtained from Mycobacteria.</a> Santos, P., A. Gordillo, L. Osses, L.M. Salazar, and C.Y. Soto. Peptides, 2012. 36(1): p. 121-128; PMID[22569076].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22664188">Discovery of Novel 5-(Ethyl or Hydroxymethyl) Analogs of 2&#39;-&#39;up&#39; Fluoro (or Hydroxyl) pyrimidine Nucleosides as a New Class of Mycobacterium tuberculosis, Mycobacterium bovis and Mycobacterium avium Inhibitors.</a> Shakya, N., N.C. Srivastav, S. Bhavanam, C. Tse, N. Desroches, B. Agrawal, D.Y. Kunimoto, and R. Kumar. Bioorganic &amp; Medicinal Chemistry, 2012. 20(13): p. 4088-4097; PMID[22664188].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2278509">Synthesis and Antimycobacterial Activity of Novel 1,3-Dimethylisocyanurate Derivatives.</a> Shulaeva, M.M., S.G. Fattakhov, L.F. Saifina, R.V. Chestnova, R. Valijev, D.N. Mingaleev, A.D. Voloshina, and V.S. Reznik. European Journal of Medicinal Chemistry, 2012. 53C: p. 300-307; PMID[22578509]. <b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22726534">Evaluation of Structurally Diverse Benzoazepines Clubbed with Coumarins as Mycobacterium tuberculosis Agents.</a> Upadhyay, K., A. Manvar, K. Rawal, S. Joshi, Y. Naliapara, J. Trivedi, R. Chaniyara, and A. Shah. Chemical Biology &amp; Drug design, 2012. <b>[Epub ahead of print]</b>; PMID[22726534].</p>
   
    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22749825">Antimycobacterial Activity: A Facile Three-component [3+2]-Cycloaddition for the Regioselective Synthesis of Highly Functionalised Dispiropyrrolidines.</a> Wei, A.C., M.A. Ali, Y.K. Yoon, R. Ismail, T.S. Choon, R.S. Kumar, N. Arumugam, A.I. Almansour, and H. Osman. Bioorganic &amp; Medicinal Chemistry Letters, 2012. <b>[Epub ahead of print]</b>; PMID[22749825].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22621375">Factors Influencing the Specificity of Inhibitor Binding to the Human and Malaria Parasite Dihydroorotate Dehydrogenases.</a> Bedingfield, P.T., D. Cowen, P. Acklam, F. Cunningham, M.R. Parsons, G.A. McConkey, C.W. Fishwick, and A.P. Johnson. Journal of Medicinal Chemistry, 2012. 55(12): p. 5841-5850; PMID[22621375].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22731758">Alpha-Substituted Beta-oxa Isosteres of Fosmidomycin: Synthesis and Biological Evaluation.</a> Brucher, K., B. Illarionov, J. Held, S. Tschan, A. Kunfermann, M. Pein, A. Bacher, T. Grawert, L. Maes, B. Mordmueller, M. Fischer, and T. Kurz. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22731758].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22732921">Identification of a New Chemical Class of Antimalarials.</a> Brunner, R., H. Aissaoui, C. Boss, Z. Bozdech, R. Brun, O. Corminboeuf, S. Delahaye, C. Fischli, B. Heidmann, M. Kaiser, J. Kamber, S. Meyer, P. Papastogiannidis, R. Siegrist, T. Voss, R. Welford, S. Wittlin, and C. Binkert. The Journal of infectious diseases, 2012. <b>[Epub ahead of print]</b>; PMID[22732921].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/227532258">Synthesis and Molecular Modeling Studies of Derivatives of a Highly Potent Peptidomimetic Vinyl Ester as Falcipain-2 Inhibitors.</a> Ettari, R., N. Micale, G. Grazioso, F. Bova, T. Schirmeister, S. Grasso, and M. Zappala. ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22753258].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22588006">Aryl Aryl Methyl Thio Arenes Prevent Multidrug-Resistant Malaria in Mouse by Promoting Oxidative Stress in Parasites.</a> Goyal, M., P. Singh, A. Alam, S. Kumar Das, M. Shameel Iqbal, S. Dey, S. Bindu, C. Pal, G. Panda, and U. Bandyopadhyay. Free Radical Biology &amp; Medicine, 2012. 53(1): p. 129-142; PMID[22588006].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22726686">Acyclic Immucillin Phosphonates: Second-generation Inhibitors of Plasmodium falciparum Hypoxanthine- guanine-xanthine Phosphoribosyltransferase.</a> Hazleton, K.Z., M.C. Ho, M.B. Cassera, K. Clinch, D.R. Crump, I. Rosario, Jr., E.F. Merino, S.C. Almo, P.C. Tyler, and V.L. Schramm. Chemistry &amp; Biology, 2012. 19(6): p. 721-730; PMID[22726686].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22725979">Synthesis of Novel N-branched Acyclic Nucleoside Phosphonates as Potent and Selective Inhibitors of Human, Plasmodium falciparum and Plasmodium vivax 6-Oxopurine Phosphoribosyltransferases.</a> Hockova, D., D.T. Keough, Z. Janeba, T.H. Wang, J. de Jersey, and L.W. Guddat. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22725979].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22753234">Cinnamic Acid/Chloroquinoline Conjugates as Potent Agents against Chloroquine-resistant Plasmodium falciparum.</a> Perez, B., C. Teixeira, J. Gut, P.J. Rosenthal, J.R. Gomes, and P. Gomes. ChemMedChem, 2012. <b>[Epub ahead of print]</b>; PMID[22753234].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22496096">High-throughput Screening for Small-Molecule Inhibitors of Plasmodium falciparum Glucose-6-phosphate Dehydrogenase 6-Phosphogluconolactonase.</a> Preuss, J., M. Hedrick, E. Sergienko, A. Pinkerton, A. Mangravita-Novo, L. Smith, C. Marx, E. Fischer, E. Jortzik, S. Rahlfs, K. Becker, and L. Bode. Journal of Biomolecular Screening, 2012. 17(6): p. 738-751; PMID[22496096].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22547613">HDQ, a Potent Inhibitor of Plasmodium falciparum Proliferation, Binds to the Quinone Reduction Site of the Cytochrome bc1 Complex.</a> Vallieres, C., N. Fisher, T. Antoine, M. Al-Helal, P. Stocks, N.G. Berry, A.S. Lawrenson, S.A. Ward, P.M. O&#39;Neill, G.A. Biagini, and B. Meunier. Antimicrobial Agents and Chemotherapy, 2012. 56(7): p. 3739-3747; PMID[22547613].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p>
    
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">40. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22278729">In Vitro Determination of Anticryptosporidial Activity of Phytogenic Extracts and Compounds.</a> Teichmann, K., M. Kuliberda, G. Schatzmayr, F. Hadacek, and A. Joachim. Parasitology Research, 2012. 111(1): p. 231-240; PMID[22278729].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0622-070512.</p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600021">Dimeric 2-(2-Chlorophenyl)-quinazolin-4-ones as Potential Antimicrobial Agents.</a> Desai, N.C., A. Dodiya, N. Bhatt, and M. Kumar. Medicinal Chemistry Research, 2012. 21(7): p. 1127-1135; ISI[000304463600021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304801400012">Antimicrobial Screening of Tropical Microfungi Isolated from Sinkholes Located in the Yucatan Peninsula, Mexico.</a> Gamboa-Angulo, M., S.D. De la Rosa-Garcia, G. Heredia-Abarca, and I.L. Medina-Baizabal. African Journal of Microbiology Research, 2012. 6(10): p. 2305-2312; ISI[000304801400012]. <b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304801400011">In Vitro and Intracellular Antimycobacterial Activity of a Bacillus pumilus Strain.</a> Hassi, M., S. David, A. Haggoud, S. El Guendouzi, A. El Ouarti, S. Ibn Souda, and M. Iraqui. African Journal of Microbiology Research, 2012. 6(10): p. 2299-2304; ISI[000304801400011].</p>
    
    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>    
    
    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600060">Isonicotinic Acid Hydrazide Derivatives: Synthesis, Antimicrobial Activity, and QSAR Studies.</a> Judge, V., B. Narasimhan, M. Ahuja, D. Sriram, P. Yogeeswari, E. De Clercq, C. Pannecouque, and J. Balzarini. Medicinal Chemistry Research, 2012. 21(7): p. 1451-1470; ISI[000304463600060].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304587600001">Synthesis of Potent Inhibitors of Beta-ketoacyl-acyl Carrier Protein Synthase III as Potential Antimicrobial Agents.</a> Liu, Y., W. Zhong, R.J. Li, and S. Li. Molecules, 2012. 17(5): p. 4770-4781; ISI[000304587600001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600032">Synthesis, Characterization and Antimicrobial Evaluation of Novel Derivatives of Isoniazid.</a> Malhotra, M., S. Sharma, and A. Deep. Medicinal Chemistry Research, 2012. 21(7): p. 1237-1244; ISI[000304463600032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304983800001">Antimicrobial Constituents of Artemisia afra Jacq. ex Willd. Against Periodontal Pathogens.</a> More, G., N. Lall, A. Hussein, and T.E. Tshikalange. Evidence-Based Complementary and Alternative Medicine, 2012; ISI[000304983800001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000305061300001">Antimicrobial Effects of a Lipophilic Fraction and Kaurenoic acid Isolated from the Root Bark Extracts of Annona senegalensis.</a> Okoye, T.C., P.A. Akah, C.O. Okoli, A.C. Ezike, E.O. Omeje, and U.E. Odoh. Evidence-Based Complementary and Alternative Medicine, 2012; ISI[000305061300001]. <b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304463600042">Synthesis, Spectral Studies and Biological Evaluation of a Novel Series of 2-Substituted-5,6-diarylsubstituted Imidazo(2,1-B)-1,3,4-thiadiazole Derivatives as Possible Anti-tubercular Agents.</a> Palkar, M.B., M.N. Noolvi, V.S. Maddi, M. Ghatole, and L.G. Nargund. Medicinal Chemistry Research, 2012. 21(7): p. 1313-1321; ISI[000304463600042].</p>
    
    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304895200004">Synthesis of 2-Oxoazetidine Derivatives of 2-Aminothiazole and Their Biological Activity.</a> Samadhiya, P., R. Sharma, S.K. Srivastava, and S.D. Srivastava. Journal of the Serbian Chemical Society, 2012. 77(5): p. 599-605; ISI[000304895200004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">51. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304727000013">Synthesis of Certain 2-Substituted-1H-benzimidazole Derivatives as Antimicrobial and Cytotoxic Agents.</a> Taher, A.T., N.A. Khalil, E.M. Ahmed, and Y.M. Ragab. Chemical &amp; Pharmaceutical Bulletin, 2012. 60(6): p. 778-784; ISI[000304727000013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">52. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304438900009">Elucidation of Mycobacterium tuberculosis Type II Dehydroquinase Inhibitors Using a Fragment Elaboration Strategy.</a> Tran, A.T., N.P. West, W.J. Britton, and R.J. Payne. ChemMedChem, 2012. 7(6): p. 1031-1043; ISI[000304438900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">53. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304523400042">Identification of Novel Mt-GuaB2 Inhibitor Series Active against M. tuberculosis.</a> Usha, V., J.V. Hobrath, S.S. Gurcha, R.C. Reynolds, and G.S. Besra. PLOS One, 2012. 7(3); ISI[000304523400042]. <b>[WOS]</b>. OI_0622-070512.</p> 
    <p> </p>
    
    <p class="plaintext">54. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000304783200017">NXL104 Irreversibly Inhibits the Beta-lactamase from Mycobacterium tuberculosis.</a> Xu, H., S. Hazra, and J.S. Blanchard. Biochemistry, 2012. 51(22): p. 4551-4557; ISI[000304783200017].</p>
   
    <p class="plaintext"><b>[WOS]</b>. OI_0622-070512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
