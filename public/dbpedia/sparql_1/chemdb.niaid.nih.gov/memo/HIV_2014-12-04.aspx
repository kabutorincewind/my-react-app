

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-12-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="lzVlODJMc5Bq2WotiNHfF6+5ytPpopOiZQX3nbMhgd8SRynyTag7CZEhsyd2Hgr36sJr8mskGba5n5FsBqHmT7ucFder9xtrCqnmXs1abeHMMD66k1ZCtJhiIWw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="97D17CE6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 21 - December 4, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25103489">Anti-HIV-1 Activity of the G-quadruplex Ligand BRACO-19.</a> Perrone, R., E. Butovskaya, D. Daelemans, G. Palu, C. Pannecouque, and S.N. Richter. The Journal of Antimicrobial Chemotherapy, 2014. 69(12): p. 3248-3258. PMID[25103489].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25159595">Novel Furocoumarins as Potential HIV-1 Integrase Inhibitors.</a> Olomola, T.O., S. Mosebi, R. Klein, T. Traut-Johnstone, J. Coates, R. Hewer, and P.T. Kaye. Bioorganic Chemistry, 2014. 57: p. 1-4. PMID[25159595].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25310595">Sulfonation Pathway Inhibitors Block Reactivation of Latent HIV-1.</a> Murry, J.P., J. Godoy, A. Mukim, J. Swann, J.W. Bruce, P. Ahlquist, A. Bosque, V. Planelles, C.A. Spina, and J.A. Young. Virology, 2014. 471-473C: p. 1-12. PMID[25310595].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25240095">Design, Synthesis and anti-HIV Evaluation of Novel Diarylnicotinamide Derivatives (DANAs) Targeting the Entrance Channel of the NNRTI Binding Pocket through Structure-guided Molecular Hybridization.</a> Liu, Z., W. Chen, P. Zhan, E. De Clercq, C. Pannecouque, and X. Liu. European Journal of Medicinal Chemistry, 2014. 87: p. 52-62. PMID[25240095].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25372991">Synthesis and Antiviral Evaluation of Novel 4&#39;-Trifluoromethylated 5&#39;-deoxyapiosyl Nucleoside Phosphonic acids.</a> Kim, S., E. Kim, W. Lee, and J. Hee Hong. Nucleosides, Nucleotides &amp; Nucleic Acids, 2014. 33(12): p. 747-766. PMID[25372991].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25419997">Five New Secondary Metabolites Produced by a Marine-associated Fungus, Daldinia eschscholzii.</a> Hu, Z.X., Y.B. Xue, X.B. Bi, J.W. Zhang, Z.W. Luo, X.N. Li, G.M. Yao, J.P. Wang, and Y.H. Zhang. Marine Drugs, 2014. 12(11): p. 5563-5575. PMID[25419997].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25224013">Abasic Phosphorothioate Oligomers Inhibit HIV-1 Reverse Transcription and Block Virus Transmission across Polarized Ectocervical Organ Cultures.</a> Fraietta, J.A., Y.M. Mueller, K.L. Lozenski, D. Ratner, A.C. Boesteanu, A.S. Hancock, C. Lackman-Smith, I.J. Zentner, I.M. Chaiken, S. Chung, S.F. LeGrice, B.A. Snyder, M.K. Mankowski, N.M. Jones, J.L. Hope, P. Gupta, S.H. Anderson, B. Wigdahl, and P.D. Katsikis. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7056-7071. PMID[25224013].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25103850">Novel Inhibitors of Human Immunodeficiency Virus Type 2 Infectivity.</a> Beach, L.B., J.M. Rawson, B. Kim, S.E. Patterson, and L.M. Mansky. Journal of GeneralVirology, 2014. 95(Pt 12): p. 2778-2783. PMID[25103850].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25267674">Synergistic Combinations of the CCR5 Inhibitor VCH-286 with Other Classes of HIV-1 Inhibitors.</a> Asin-Milan, O., M. Sylla, M. El-Far, G. Belanger-Jasmin, A. Haidara, J. Blackburn, A. Chamberland, and C.L. Tremblay. Antimicrobial Agents and Chemotherapy, 2014. 58(12): p. 7565-7569. PMID[25267674].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25251696">New CYP17 Hydroxylase Inhibitors: Synthesis, Biological Evaluation, QSAR, and Molecular Docking Study of New Pregnenolone Analogs.</a> Al-Masoudi, N.A., D.S. Ali, B. Saeed, R.W. Hartmann, M. Engel, S. Rashid, and A. Saeed. Archiv der Pharmazie, 2014. 347(12): p. 896-907. PMID[25251696].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_1121-120414.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344434400056">Cellular HIV-1 Inhibition by Truncated Old World Primate APOBEC3a Proteins Lacking a Complete Deaminase Domain.</a> Katuwal, M., Y.Q. Wang, K. Schmitt, K.J. Guo, K. Halemano, M.L. Santiago, and E.B. Stephens. Virology, 2014. 468: p. 532-544. ISI[000344434400056].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344320700010">Novel anti-HIV-1 NNRTIs Based on a Pyrazolo[4,3-d]isoxazole Backbone Scaffold: Design, Synthesis and Insights into the Molecular Basis of Action.</a> Gomha, S.M., M.G. Badrey, M.M. Abdalla, and R.K. Arafa. MedChemComm, 2014. 5(11): p. 1685-1692. ISI[000344320700010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1121-120414.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344569900001">Triptolide Inhibits Human Immunodeficiency Virus Type 1 Replication by Promoting Proteasomal Degradation of Tat Protein.</a> Wan, Z.T. and X.L. Chen. Retrovirology, 2014. 11(88): 13 pp. ISI[000344569900001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1121-120414.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
