

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-07-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Sdac3qJ06lwfrijPiDR4NQ9lieGXxyTm2unAvwJEPPQxbxp0yIAomuTvetHmSkVjgWrC7fNiehC5OZ6rgNvP8kVbcfc5JVB6zuo1Jw+CZgTsWcNHmh6LtTAjsTk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1D40192F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: July 4 - July 17, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24760893">Host and Viral Determinants of Mx2 Antiretroviral Activity.</a> Busnadiego, I., M. Kane, S.J. Rihn, H.F. Preugschas, J. Hughes, D. Blanco-Melo, V.P. Strouvelle, T.M. Zang, B.J. Willett, C. Boutell, P.D. Bieniasz, and S.J. Wilson. Journal of Virology, 2014. 88(14): p. 7738-7752. PMID[24760893].</p>
    
    <p class="plaintext"><b>[PubMed]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25016576">Characterization of a Monoclonal Antibody to a Novel Glycan-Dependent Epitope in the V1/V2 Domain of the HIV-1 Envelope Protein, gp120.</a> Doran, R.C., J.F. Morales, B. To, T.J. Morin, R. Theolis Jr, S.M. O&#39;Rourke, B. Yu, K.A. Mesa, and P.W. Berman. Molecular Immunology, 2014. 62(1): p. 219-226. PMID[25016576].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24855645">Targeting Spare CC Chemokine Receptor 5 (CCR5) as a Principle to Inhibit HIV-1 Entry.</a> Jin, J., P. Colin, I. Staropoli, E. Lima-Fernandes, C. Ferret, A. Demir, S. Rogee, O. Hartley, C. Randriamampita, M.G. Scott, S. Marullo, N. Sauvonnet, F. Arenzana-Seisdedos, B. Lagane, and A. Brelot. Journal ofBiologicalChemistry, 2014. 289(27): p. 19042-19052. PMID[24855645].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25010042">Antiviral Lectins as Potential HIV Microbicides.</a> Koharudin, L.M. and A.M. Gronenborn. Current Opinion in Virology, 2014. 7C: p. 95-100. PMID[25010042].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0704-071714.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336870800013">Rapid Production of HIV-1 Neutralizing Antibodies in Baculovirus Infected Insect Cells.</a> Liu, B.Q., R.Q. Wang, F. Wu, X.D. Xu, and H.Y. Chen. Protein Expression and Purification, 2014. 99: p. 87-93. ISI[000336870800013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337171800019">M-CSF Inhibits anti-HIV-1 Activity of IL-32, but They Enhance M2-like Phenotypes of Macrophages.</a> Osman, A., F. Bhuyan, M. Hashimoto, H. Nasser, T. Maekawa, and S. Suzu.Journal of Immunology, 2014. 192(11): p. 5083-5089. ISI[000337171800019].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337301100010">Total Synthesis and Structure Revision of Didemnaketal B</a>. Fuwa, H., T. Muto, K. Sekine, and M. Sasaki. Chemistry, 2014. 20(7): p. 1848-1860. ISI[000337301100010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">8<a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337140300005">. Biologically Active Diterpenes Containing a gem-Dimethylcyclopropane Subunit: An Intriguing Source of PKC Modulators.</a> Duran-Pena, M.J., J.M.B. Ares, I.G. Collado, and R. Hernandez-Galan. Natural Product Reports, 2014. 31(7): p. 940-952. ISI[000337140300005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337324200001">Snake Venom L-Amino acid oxidases: An Overview on Their Antitumor Effects.</a> Costa, T.R., S.M. Burin, D.L. Menaldo, C.F.A. de, and S.V. Sampaio. Journal of Venomous Animals and Toxins Including Tropical Diseases, 2014. 20(23): p. 7. ISI[000337324200001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000336913200012">Crystal Structure of (E)-(2-((2,3-Dihydrobenzo[b][1,4]dioxin-6-yl)methylene)-N-phenylhydrazinecarbothioamide, C16H15N3O2S.</a> Chen, S.Y., F.M. Guo, Y.Y. Huang, J. Ma, N. Li, and J. Sun. Zeitschrift fur Kristallographie, 2014. 229(2): p. 99-100. ISI[000336913200012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337231300774">Cross-clade Inhibition of HIV on Primary Cells by CXCR4 or CCR5 Fused to the C34 Peptide from gp41 HR2.</a> Wang, J.B., G.J. Leslie, J. De Clercq, M.W. Richardson, A.P.O. Jordon, P.D. Gregory, J.L. Riley, J.A. Hoxie, and M.C. Holmes. Molecular Therapy, 2014. 22: p. S302-S303. ISI[000337231300774].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337685800012">C-SRC and PYK2 Protein Tyrosine Kinases Play Protective Roles in Early HIV-1 Infection of CD4(+) T-Cell Lines.</a> McCarthy, S.D.S., D. Jung, D. Sakac, and D.R. Branch. Journal of Acquired Immune Deficiency Syndromes, 2014. 66(2): p. 118-126. ISI[000337685800012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337600500016">The HIV-1 gp41 Ectodomain Is Cleaved by Matriptase to Produce a Chemotactic Peptide that Acts through FPR2.</a> Wood, M.P., A.L. Cole, C.R. Eade, L.M. Chen, K.X. Chai, and A.M. Cole. Immunology, 2014. 142(3): p. 474-483. ISI[000337600500016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000337609400009">H-Phosphonate-Mediated Amination of Quinoline N-Oxides with Tertiary Amines: A Mild and Metal-Free Synthesis of 2-Dialkylaminoquinolines.</a> Chen, X.L., X. Li, Z.B. Qu, D.D. Ke, L.B. Qu, L.K. Duan, W.P. Mai, J.W. Yuan, J.Y. Chen, and Y.F. Zhao. Advanced Synthesis &amp; Catalysis, 2014. 356(9): p. 1979-1985. ISI[000337609400009].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0704-071714.</p>

    <br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
