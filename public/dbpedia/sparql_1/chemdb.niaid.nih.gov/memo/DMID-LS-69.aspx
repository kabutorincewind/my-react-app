

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-69.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pKU8jbuKw5ihVRhzyTM/RPjlCeY9sW/L3i8O8YsgNgkba5LjuDKy3iBFbVgy9IKfI7y6pYueDXZBh3bOZ3CmFtFGTnzJyf+OL+tyqyjkA+RV4xN8VdPraqYsV3E=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="244D1D08" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-69-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    6091   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Antiviral activity of lactoferrin towards naked viruses</p>

<p>           Seganti, Lucilla, Di Biase, Assunta Maria, Marchetti, Magda, Pietrantoni, Agostina, Tinari, Antonella, and Superti, Fabiana</p>

<p>           BioMetals 2004. 17(3): 295-299</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     2.    6092   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Antiviral Activity on Hantavirus and Apoptosis of Vero Cells of Natural and Semi-Synthetic Compounds From Heliotropium Filifolium Resin.</b></p>

<p>           Modak, B, Galeno, H, and Torres, R</p>

<p>           Journal of the Chilean Chemical Society 2004. 49(2): 143-145</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     3.    6093   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Impact of Novel Histone Deacetylase Inhibitors, Chap31 and Fr901228 (Fk228), on Adenovirus-Mediated Transgene Expression</b></p>

<p>           Taura, K, Yamamoto, Y, Nakajima, A, Hata, K, Uchinami, H, Yonezawa, K, Hatano, E, Nishino, N, and Yamaoka, Y</p>

<p>           Journal of Gene Medicine 2004. 6(5): 526-536</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>     4.    6094   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Structural variations in keto-glutamines for improved inhibition against hepatitis A virus 3C proteinase</p>

<p>           Jain, RP and Vederas, JC</p>

<p>           Bioorg Med Chem Lett 2004. 14(14):  3655-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15203137&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15203137&amp;dopt=abstract</a> </p><br />

<p>     5.    6095   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Mutations in the human cytomegalovirus UL27 gene that confer resistance to maribavir</p>

<p>           Chou, S, Marousek, GI, Senters, AE, Davis, MG, and Biron, KK</p>

<p>           J Virol 2004. 78(13): 7124-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194788&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15194788&amp;dopt=abstract</a> </p><br />

<p>     6.    6096   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Non-nucleoside inhibitors of the HCV polymerase</p>

<p>           Sarisky, RT</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15190019&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15190019&amp;dopt=abstract</a> </p><br />

<p>     7.    6097   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           In vitro and in vivo inhibitory effects of disodium cromoglycate on influenza virus infection</p>

<p>           Hidari, KI, Tsujii, E, Hiroi, J, Mano, E, Miyatake, A, Miyamoto, D, Suzuki, T, and Suzuki, Y</p>

<p>           Biol Pharm Bull 2004. 27(6): 825-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187427&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15187427&amp;dopt=abstract</a> </p><br />

<p>     8.    6098   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           New synthetic glutathione derivatives with increased antiviral activities</p>

<p>           Palamara, AT, Brandi, G, Rossi, L, Millo, E, Benatti, U, Nencioni, L, Iuvara, A, Garaci, E, and Magnani, M</p>

<p>           Antivir Chem Chemother 2004. 15(2): 83-91</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185726&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15185726&amp;dopt=abstract</a> </p><br />

<p>     9.    6099   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Attenuation of SARS coronavirus by a short hairpin RNA expression plasmid targeting RNA-dependent RNA polymerase</p>

<p>           Lu, A, Zhang, H, Zhang, X, Wang, H, Hu, Q, Shen, L, Schaffhausen, BS, Hou, W, and Li, L</p>

<p>           Virology 2004. 324(1): 84-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15183056&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15183056&amp;dopt=abstract</a> </p><br />

<p>   10.    6100   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Evaluation of Cytotoxicity and Antiviral Actvity of Recombinant Human Interferon Alfa-2a and Recombinant Human Interferon Alfa-B/D Hybrid Against Bovine Viral Diarrhea Virus, Infectious Bovine Rhinotracheitis Virus, and Vesicular Stomatitis Virus in Vitro</b></p>

<p>           Peek, SF, Bonds, MD, Gangemi, DG, Thomas, CB, and Schultz, RD</p>

<p>           American Journal of Veterinary Research 2004. 65(6): 871-874</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   11.    6101   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Bovine aortic endothelial cells are susceptible to hantavirus infection; a new aspect in hantavirus ecology</p>

<p>           Muranyi, W, Kehm, R, Bahr, U, Muller, S, Handermann, M, Darai, G, and Zeier, M</p>

<p>           Virology 2004. 318(1): 112-122</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   12.    6102   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           AM3 inhibits HBV replication through activation of peripheral blood mononuclear cells</p>

<p>           Majano, P, Roda-Navarro, P, Alonso-Lebrero, JL, Brieva, A, Casal, C, Pivel, JP, Lopez-Cabrera, M, and Moreno-Otero, R</p>

<p>           Int Immunopharmacol 2004. 4(7): 921-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15182731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15182731&amp;dopt=abstract</a> </p><br />

<p>   13.    6103   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>The Reactions of 2-(Bromoseleno)Benzenesulfonyl Chloride With Primary Amines Toward 2,2 &#39;-Diselenobis(Benzenesulfonamides) and 1,3,2-Benzothiaselenazole 1,1-Dioxides: New Oxygen-Transfer Agents, Antimicrobials and Virucides</b></p>

<p>           Potaczek, P,  Giurg, M, Kloc, K, Maliszewska, I, Piasecki, E, Pietka, M, and Mlochowski, J</p>

<p>           Polish Journal of Chemistry 2004. 78(5): 687-697</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   14.    6104   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Pharmacogenomic Perspectives of Chronic Hepatitis C Virus (Hcv) Infection</b></p>

<p>           Tang, J and Kaslow, RA</p>

<p>           Pharmacogenomics Journal 2004. 4(3): 171-174</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.     6105   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           TFIIH transcription factor, a target for the Rift valley hemorrhagic fever virus</p>

<p>           Le May, Nicolas, Dubaele, Sandy, Proietti De Santis, Luca, Billecocq, Agnes, Bouloy, Michele, and Egly, Jean-Marc</p>

<p>           Cell (Cambridge, MA, United States) 2004. 116(4): 541-550</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   16.    6106   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Design and Characterization of Helical Peptides that Inhibit the E6 Protein of Papillomavirus(,)</p>

<p>           Liu, Y, Liu, Z, Androphy, E, Chen, J, and Baleja, JD</p>

<p>           Biochemistry 2004. 43(23): 7421-31</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15182185&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15182185&amp;dopt=abstract</a> </p><br />

<p>   17.    6107   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Coronavirus 3CL-pro proteinase cleavage sites: Possible relevance to SARS virus pathology</p>

<p>           Kiemer, L, Lund, O, Brunak, S, and Blom, N</p>

<p>           BMC Bioinformatics 2004. 5(1): 72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180906&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180906&amp;dopt=abstract</a> </p><br />

<p>   18.    6108   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Assessment of New Chemical Disinfectants for Hbv Virucidal Activity in a Cell Culture Model</b></p>

<p>           Payan, C, Pivert, A, Kampf, G, Ramont, C, Cottin, J, and Lemarie, C</p>

<p>           Journal of Hospital Infection 2004. 56: S58-S63</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   19.    6109   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Antiviral Properties of Quinolone-based Drugs</p>

<p>           Richter, S, Parolin, C, Palumbo, M, and Palu, G</p>

<p>           Curr Drug Targets Infect Disord 2004. 4(2): 111-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180459&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15180459&amp;dopt=abstract</a> </p><br />

<p>   20.    6110   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Structural and functional polymorphism of the glycoproteins of filoviruses</p>

<p>           Volchkov, Viktor E, Volchkova, Valentina A, Dolnik, Olga, Feldmann, Heinz, and Klenk, Hans-Dieter</p>

<p>           CONFERENCE: Ebola and Marburg Viruses 2004: 59-89</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   21.    6111   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Positively charged synthetic peptides from structural proteins of papillomaviruses abrogate human papillomavirus infectivity</p>

<p>           Bousarghin, L, Touze, A, Yvonnet, B, and Coursaget, P</p>

<p>           J Med Virol 2004. 73(3): 474-80</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170645&amp;dopt=abstract</a> </p><br />

<p>  22.     6112   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           The E glycoproteins of flaviviruses as fusion proteins and their use as targets for prevention of infection</b> ((The Administrators of the Tulane Educational Fund, USA and The Rockefeller University))</p>

<p>           Garry, Robert F, Dash, Srikanta, Coy, David H, and McKeating, Jane A</p>

<p>           PATENT: WO 2004044220 A2;  ISSUE DATE: 20040527</p>

<p>           APPLICATION: 2003; PP: 73 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   23.    6113   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           The use of endoperoxides for the treatment of infections caused by Flaviviridae, including hepatitis C, bovine viral diarrhea, and classical swine fever viruses</b>((Kemin Pharma Europe B.V.B.A., USA)</p>

<p>           Vandenkerckhove, Jan, Sas, Benedikt, Peys, Eric, and Van Hemel, Johan</p>

<p>           PATENT: WO 2004041176 A2;  ISSUE DATE: 20040521</p>

<p>           APPLICATION: 2003; PP: 14 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   24.    6114   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Virus spread, tissue inflammation and antiviral response in brains of flavivirus susceptible and resistant mice acutely infected with Murray Valley encephalitis virus</p>

<p>           Silvia, OJ, Pantelic, L, Mackenzie, JS, Shellam, GR, Papadimitriou, J, and Urosevic, N</p>

<p>           Archives of Virology 2004. 149(3):  447-464</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   25.    6115   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activity of monofluorinated cyclopropanoid nucleosides</p>

<p>           Rosen, Thomas C, De Clercq, Erik, Balzarini, Jan, and Haufe, Guenter</p>

<p>           Organic &amp; Biomolecular Chemistry 2004. 2(2): 229-237</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   26.    6116   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Inhibition of the Epstein-Barr virus lytic cycle by Zta-targeted RNA interference</p>

<p>           Chang, Y, Chang, SS, Lee, HH, Doong, SL, Takada, K, and Tsai, CH</p>

<p>           J Gen Virol 2004. 85(Pt 6): 1371-1379</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15166418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15166418&amp;dopt=abstract</a> </p><br />

<p>   27.    6117   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Thiadiazolyl quinazolones as potential antiviral and antihypertensive agents</p>

<p>           Pandey, VK, Tusi, Sarah, Tusi, Zehra, Raghubir, R, Dixit, M, Joshi, MN, and Bajpai, SK</p>

<p>           Indian Journal of Chemistry, Section B: Organic Chemistry Including Medicinal Chemistry 2004. 43B(1): 180-183</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   28.    6118   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Mutational analysis of the influenza virus cRNA promoter and identification of nucleotides critical for replication</p>

<p>           Crow, M, Deng, T, Addley, M, and Brownlee, GG</p>

<p>           J Virol 2004. 78(12): 6263-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163719&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163719&amp;dopt=abstract</a> </p><br />

<p>  29.     6119   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Biochemical and structural characterization of South methanocarba-thymidine that specifically inhibits growth of herpes simplex virus type 1 thymidine kinase transduced osteosarcoma cells</p>

<p>           Schelling, P, Claus, MT, Johner, R, Marquez, VE, Schulz, GE, and Scapozza, L</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163659&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163659&amp;dopt=abstract</a> </p><br />

<p>   30.    6120   DMID-LS-69; PUBMED-DMID-6/23/2004</p>

<p class="memofmt1-3">           Identification of a human influenza type B strain with reduced sensitivity to neuraminidase inhibitor drugs</p>

<p>           Hurt, AC, McKimm-Breschkin, JL, McDonald, M, Barr, IG, Komadina, N, and Hampson, AW</p>

<p>           Virus Res 2004. 103(1-2): 205-11</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163511&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163511&amp;dopt=abstract</a> </p><br />

<p>   31.    6121   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           Aromatic polyketide intermediates as selective anticancer and antiviral agents</b>((Galilaeus OY, Finland)</p>

<p>           Kunnari, Tero and Vuento, Matti</p>

<p>           PATENT: WO 2004045600 A1;  ISSUE DATE: 20040603</p>

<p>           APPLICATION: 2003; PP: 18 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   32.    6122   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Inhibition of Hepatitis C Virus Rna Replication by Benzimidazoles With High Affinity for the 5 &#39;-Untranslated Region of the Genomic Rna</b></p>

<p>           Swayze, EE, Seth, P, Jefferson, EA, Miyaji, A, Osgood, S, Ranken, R, Propp, S, Lowery, K, and Griffey, RH</p>

<p>           Antiviral Research 2004. 62(2): A82</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   33.    6123   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Synthesis and biological activity of substituted 2,4,6-s-triazines</p>

<p>           Pandey, Vinod Kumar, Tusi, Sarah, Tusi, Zehra, Joshi, Madhawanand, and Bajpai, Shashikala</p>

<p>           Acta Pharmaceutica (Zagreb, Croatia) 2004. 54(1): 1-12</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   34.    6124   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           Crystals of 2-amino-6-(4-methoxyphenylthio)-9-[2-(phosphonomethoxy)ethyl]purine bis(2,2,2-trifluoroethyl) ester</b>((Mitsubishi Pharma Corporation, Japan)</p>

<p>           Yoshizawa, Katsuyoshi, Yamada, Hiroyuki, Masunaga, Toshio, Inokawa, Haruki, Sekiya, Kouichi, Shirasaka, Tadashi, and Ubasawa, Masaru</p>

<p>           PATENT: WO 2004024743 A1;  ISSUE DATE: 20040325</p>

<p>           APPLICATION: 2003; PP: 108 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   35.    6125   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Antimicrobial and Antiviral Activities of an Actinomycete (Streptomyces Sp.) Isolated From a Brazilian Tropical Forest Soil</b></p>

<p>           Sacramento, DR, Coelho, RRR, Wigg, MD, Linhares, L, Dos Santos, MGM, Samedo, L , and Da Silva, AJR</p>

<p>           World Journal of Microbiology &amp; Biotechnology 2004. 20(3): 225-229</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.     6126   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Changes in in vitro susceptibility of influenza A H3N2 viruses to a neuraminidase inhibitor drug during evolution in the human host</p>

<p>           Thompson, Catherine I, Barclay, Wendy S, and Zambon, Maria C</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(5): 759-765</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   37.    6127   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           Anti-influenza virus agents containing tranexamic acid</b>((Daiichi Pharmaceutical Co., Ltd. Japan)</p>

<p>           Tsunoda, Kenji</p>

<p>           PATENT: WO 2004032915 A1;  ISSUE DATE: 20040422</p>

<p>           APPLICATION: 2003; PP: 23 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   38.    6128   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Ribozymes that cleave reovirus genome segment S1 also protect cells from pathogenesis caused by reovirus infection</p>

<p>           Shahi, Shweta, Shanmugasundaram, Ganapathy K, and Banerjea, Akhil C</p>

<p>           Proceedings of the National Academy of Sciences of the United States of America 2001. 98(7): 4101-4106</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   39.    6129   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Inactivation of S-Adenosyl-L-homocysteine Hydrolase and Antiviral Activity with 5&#39;,5&#39;,6&#39;,6&#39;-Tetradehydro-6&#39;-deoxy-6&#39;-halohomoadenosine Analogs (4&#39;-Haloacetylene Analogs Derived from Adenosine)</p>

<p>           Robins, Morris J, Wnuk, Stanislaw F, Yang, Xiaoda, Yuan, Chong-Sheng, Borchardt, Ronald T, Balzarini, Jan, and De Clercq, Erik</p>

<p>           Journal of Medicinal Chemistry 98. 41(20): 3857-3864</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   40.    6130   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Antiviral activity of galangin isolated from the aerial parts of Helichrysum aureonitens</p>

<p>           Meyer, JJM, Afolayan, AJ, Taylor, MB, and Erasmus, D</p>

<p>           Journal of Ethnopharmacology 97. 56(2): 165-169</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   41.    6131   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Polyanion Inhibitors of Human Immunodeficiency Virus and Other Viruses. Part 2. Polymerized Anionic Surfactants Derived from Amino Acids and Dipeptides</p>

<p>           Leydet, A, Hachemi, HEl, Boyer, B, Lamaty, G, Roque, JP, Schols, D, Snoeck, R, Andrei, G, Ikeda, S, and et al</p>

<p>           Journal of Medicinal Chemistry 96. 39(8): 1626-34</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   42.    6132   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Antiviral activity of phosphonoformate on rotavirus transcription and replication</p>

<p>           Rios, Maritza, Munoz, Marianne, and Spencer, Eugenio</p>

<p>           Antiviral Research 95. 27(1-2): 71-83</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   43.    6133   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Synthesis and antiviral activity of some new 1H-1,2,4-triazole derivatives</p>

<p>           Todoulou, OG, Papadaki-Valiraki, AE, Ikeda, S, and De Clercq, E</p>

<p>           European Journal of Medicinal Chemistry 94. 29(7-8): 611-20</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   44.    6134   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           The synthesis and antiviral properties of (+-)-5&#39;-noraristeromycin and related purine carbocyclic nucleosides.  A new lead for anti-human cytomegalovirus agent design</p>

<p>           Patil, Sharadbala D, Schneller, Stewart W, Hosoya, Mitsuaki, Snoeck, Robert, Andrei, Graciela, Balzarini, Jan, and De Clercq, Erik</p>

<p>           Journal of Medicinal Chemistry 92. 35(18): 3372-7</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   45.    6135   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Inhibition in vitro of the replication of murine cytomegalovirus or reovirus type 3 by the glutamine analog acivicin</p>

<p>           Keast, D and Vasquez, AR</p>

<p>           Archives of Virology 92. 124(3-4):  235-44</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   46.    6136   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           Preparation of 3&#39;-deoxy-3&#39;-fluoroadenosine as antiviral agent</b>((Asahi Glass Co., Ltd. Japan)</p>

<p>           Morisawa, Yoshitomi, Yasuda, Arata, and De Clercq, Erik</p>

<p>           PATENT: JP 03284691 A2;  ISSUE DATE: 19911216</p>

<p>           APPLICATION: 90-15786; PP: 5 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   47.    6137   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Sulfated polymers are potent and selective inhibitors of various enveloped viruses, including herpes simplex virus, cytomegalovirus, vesicular stomatitis virus, respiratory syncytial virus, and toga-, arena- and retroviruses</p>

<p>           Schols, D, De Clercq, E, Balzarini, J, Baba, M, Witvrouw, M, Hosoya, M, Andrei, G, Snoeck, R, Neyts, J, and et al</p>

<p>           Antiviral Chemistry &amp; Chemotherapy 90. 1(4): 233-40</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   48.    6138   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p><b>           Preparation of nucleosides useful as antiviral, antineoplastic, antibacterial, and antitumor agents</b> ((USA))</p>

<p>           Sartorelli, Alan C, Cheng, Yung-Chi, and Liu, Mao-Chin</p>

<p>           PATENT: US 20040116362 A1;  ISSUE DATE: 20040617</p>

<p>           APPLICATION: 2002-58989; PP: 28 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   49.    6139   DMID-LS-69; SCIFINDER-DMID-6/23/2004</p>

<p class="memofmt1-3">           Designing clinical development programs for anti-hepatitis B virus drugs</p>

<p>           Brown, Nathaniel A</p>

<p>           Methods in Molecular Medicine 2004.  96(Hepatitis B and D Protocols, Volume 2): 499-537</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>   50.    6140   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>A Colorimetric Cell Culture Assay for the Identification of Sars Coronavirus Inhibitors</b></p>

<p>           Keyaerts, E,  Vijgen, L, Neyts, J, De Clercq, E, Balzarini, J, and Van Ranst, M</p>

<p>           Antiviral Research 2004. 62(2): A75</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  51.     6141   DMID-LS-69; WOS-DMID-6/23/2004</p>

<p>           <b>Possibilities to Inhibit the Replication of a Severe Acute Respiratory Syndrome (Sars) Associated Coronavirus</b></p>

<p>           Schmidtke, M, Meier, C, Schacke, M, Helbig, B, Makarov, V, and Wutzler, P</p>

<p>           Antiviral Research 2004. 62(2): A76-A77</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
