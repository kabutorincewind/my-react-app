

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2015-01-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="tsQsDh90qIkwEiFUnxHMrgtJzaMW0ccKcbrHbtsdITg29HJjkXnTiE9YzrYSUTEmTpGHbGwo+wvo55P/Lxn4++6Zgo3tE8sfarlAo3U3BjKeXqmJMF1cfG4tUOA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E52C8B38" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: January 16 - January 29, 2015</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25439896">Synthesis, Characterization and Antimicrobial Properties of Grafted Sugarcane Bagasse/Silver Nanocomposites.</a> Abdelwahab, N.A. and N. Shukry. Carbohydrate Polymers, 2015. 115: p. 276-284. PMID[25439896].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25537192">HPLC-SPE-NMR Characterization of Major Metabolites in Salvia fruticosa Mill. Extract with Antifungal Potential: Relevance of Carnosic acid, Carnosol, and Hispidulin.</a> Exarchou, V., L. Kanetis, Z. Charalambous, S. Apers, L. Pieters, V. Gekas, and V. Goulas. Journal of Agricultural and Food Chemistry, 2015. 63(2): p. 457-463. PMID[25537192].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25497131">Synthesis, Anticandidal Activity of N(3)-(4-Methoxyfumaroyl)-(S)-2,3-diaminopropanoic amide Derivatives - Novel Inhibitors of Glucosamine-6-phosphate Synthase.</a> Pawlak, D., M. Stolarska, M. Wojciechowski, and R. Andruszkiewicz. European Journal of Medicinal Chemistry, 2015. 90: p. 577-582. PMID[25497131].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25531979">Syntheses, Structures, and Antimicrobial Activity of New Remarkably Light-stable and Water-soluble tris(Pyrazolyl)methanesulfonate silver(I) Derivatives of N-Methyl-1,3,5-triaza-7-phosphaadamantane salt - [mPTA]BF4.</a> Smolenski, P., C. Pettinari, F. Marchetti, M.F. Guedes da Silva, G. Lupidi, G.V. Badillo Patzmay, D. Petrelli, L.A. Vitali, and A.J. Pombeiro. Inorganic Chemistry, 2015. 54(2): p. 434-440. PMID[25531979].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25515956">New Synthetic Sulfone Derivatives Inhibit Growth, Adhesion and the Leucine Arylamidase APE2 Gene Expression of Candida albicans in Vitro.</a> Staniszewska, M., M. Bondaryk, and Z. Ochal. Bioorganic &amp; Medicinal Chemistry, 2015. 23(2): p. 314-321. PMID[25515956].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25561178">A New Antibiotic Kills Pathogens without Detectable Resistance.</a> Ling, L.L., T. Schneider, A.J. Peoples, A.L. Spoering, I. Engels, B.P. Conlon, A. Mueller, T.F. Schaberle, D.E. Hughes, S. Epstein, M. Jones, L. Lazarides, V.A. Steadman, D.R. Cohen, C.R. Felix, K.A. Fetterman, W.P. Millett, A.G. Nitti, A.M. Zullo, C. Chen, and K. Lewis. Nature, 2015. 517(7535): p. 455-459. PMID[25561178].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25486447">Structure Guided Lead Generation for M. tuberculosis Thymidylate Kinase (MTB TMK): Discovery of 3-Cyanopyridone and 1,6-Naphthyridin-2-one as Potent Inhibitors.</a> Naik, M., A. Raichurkar, B.S. Bandodkar, B.V. Varun, S. Bhat, R. Kalkhambkar, K. Murugan, R. Menon, J. Bhat, B. Paul, H. Iyer, S. Hussein, J.A. Tucker, M. Vogtherr, K.J. Embrey, H. McMiken, S. Prasad, A. Gill, B.G. Ugarkar, J. Venkatraman, J. Read, and M. Panda. Journal of Medicinal Chemistry, 2015. 58(2): p. 753-766. PMID[25486447].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25461892">Piperazine Derivatives: Synthesis, Inhibition of the Mycobacterium tuberculosis Enoyl-acyl Carrier Protein Reductase and SAR Studies.</a> Rotta, M., K. Pissinate, A.D. Villela, D.F. Back, L.F. Timmers, J.F. Bachega, O.N. de Souza, D.S. Santos, L.A. Basso, and P. Machado. European Journal of Medicinal Chemistry, 2015. 90: p. 436-447. PMID[25461892].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25517015">Design, Synthesis, and Evaluation of New Thiadiazole-based Direct Inhibitors of Enoyl Acyl Carrier Protein Reductase (InhA) for the Treatment of Tuberculosis.</a> Sink, R., I. Sosic, M. Zivec, R. Fernandez-Menendez, S. Turk, S. Pajk, D. Alvarez-Gomez, E.M. Lopez-Roman, C. Gonzales-Cortez, J. Rullas-Triconado, I. Angulo-Barturen, D. Barros, L. Ballell-Pages, R.J. Young, L. Encinas, and S. Gobec. Journal of Medicinal Chemistry, 2015. 58(2): p. 613-624. PMID[25517015].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25481079">Antimalarial Chemotherapy: Orally Curative Artemisinin-derived Trioxane Dimer Esters.</a> Conyers, R.C., J.R. Mazzone, A.K. Tripathi, D.J. Sullivan, and G.H. Posner. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(2): p. 245-248. PMID[25481079].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25561559">Dried Whole-plant Artemisia annua Slows Evolution of Malaria Drug Resistance and Overcomes Resistance to Artemisinin.</a> Elfawal, M.A., M.J. Towler, N.G. Reich, P.J. Weathers, and S.M. Rich. Proceedings of the National Academy of Sciences of the United States of America, 2015. 112(3): p. 821-826. PMID[25561559].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25488841">The Synthesis, Antimalarial Activity and COMFA Analysis of Novel Aminoalkylated Quercetin Analogs.</a> Helgren, T.R., R.J. Sciotti, P. Lee, S. Duffy, V.M. Avery, O. Igbinoba, M. Akoto, and T.J. Hagen. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(2): p. 327-332. PMID[25488841].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25494538">Aza-acyclic Nucleoside Phosphonates Containing a Second Phosphonate Group as Inhibitors of the Human, Plasmodium falciparum and Vivax 6-Oxopurine Phosphoribosyltransferases and their Prodrugs as Antimalarial Agents.</a> Keough, D.T., D. Hockova, Z. Janeba, T.H. Wang, L. Naesens, M.D. Edstein, M. Chavchich, and L.W. Guddat. Journal of Medicinal Chemistry, 2015. 58(2): p. 827-846. PMID[25494538].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25486423">Synthesis and in Vitro Antiplasmodial Activity of Ferrocenyl Aminoquinoline Derivatives.</a> Mwande Maguene, G., J.B. Lekana-Douki, E. Mouray, T. Bousquet, P. Grellier, S. Pellegrini, F.S. Toure Ndouo, J. Lebibi, and L. Pelinski. European Journal of Medicinal Chemistry, 2015. 90: p. 519-525. PMID[25486423].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25486422">Synthesis and Structure-Activity-Relationship Studies of Thiazolidinediones as Antiplasmodial Inhibitors of the Plasmodium falciparum Cysteine Protease Falcipain-2.</a> Sharma, R.K., Y. Younis, G. Mugumbate, M. Njoroge, J. Gut, P.J. Rosenthal, and K. Chibale. European Journal of Medicinal Chemistry, 2015. 90: p. 507-518. PMID[25486422].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25461309">Synthesis and in Vitro Biological Evaluation of Dihydroartemisinyl-chalcone esters.</a> Smit, F.J., R.A. van Biljon, L.M. Birkholtz, and D.D. N&#39;Da. European Journal of Medicinal Chemistry, 2015. 90: p. 33-44. PMID[25461309].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0116-012915.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346676000006">Action of Essential Oils from Brazilian Native and Exotic Medicinal Species on Oral Biofilms.</a> Bersan, S.M.F., L.C.C. Galvao, V.F.F. Goes, A. Sartoratto, G.M. Figueira, V.L.G. Rehder, S.M. Alencar, R.M.T. Duarte, P.L. Rosalen, and M.C.T. Duarte. BMC Complementary and Alternative Medicine, 2014. 14(451): 12pp. ISI[000346676000006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346796500022">Anti-mycobacterial Nucleoside Antibiotics from a Marine-derived Streptomyces Sp TPU1236A.</a> Bu, Y.Y., H. Yamazaki, K. Ukai, and M. Namikoshi. Marine Drugs, 2014. 12(12): p. 6102-6112. ISI[000346796500022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346899500005">Design and Antimicrobial Evaluation of 1-Methylimidazole Derivatives as New Antifungal and Antibacterial Agents.</a> Iman, M., A. Davood, B.K. Gebbink, P. Azerang, M. Alibolandi, and S. Sardari. Pharmaceutical Chemistry Journal, 2014. 48(8): p. 513-519. ISI[000346899500005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346682200006">Chemical Compounds, in Vitro Antioxidant and Antifungal Activities of Some Plant Essential Oils Belonging to Rosaceae Family.</a> Mileva, M., E. Krumova, J. Miteva-Staleva, N. Kostadinova, A. Dobreva, and A.S. Galabov. Comptes Rendus de l&#39;Academie Bulgare des Sciences, 2014. 67(10): p. 1363-1368. ISI[000346682200006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346846100024">Inhibitory Effect of Helichrysum arenarium Essential Oil on the Growth of Food Contaminated Microorganisms.</a> Moghadam, H.D., A.M. Sani, and M.M. Sangatash. Journal of Essential Oil Bearing Plants, 2014. 17(5): p. 911-921. ISI[000346846100024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346138400025">Green Synthesis of Silver Nanoparticles Using Fruit Extract of Malus domestica and Study of its Antimicrobial Activity.</a> Roy, K., C.K. Sarkar, and C.K. Ghosh. Digest Journal of Nanomaterials and Biostructures, 2014. 9(3): p. 1137-1146. ISI[000346138400025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346216500012">Synthesis of Zinc Oxide Nanoparticles by Homogeneous Precipitation Method and its Application in Antifungal Activity against Candida albicans.</a> Sharma, R.K. and R. Ghose. Ceramics International, 2015. 41(1): p. 967-975. ISI[000346216500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346459800002">In Vitro and in Vivo Antifungal Activity of Cassia surattensis Flower against Aspergillus niger.</a> Sumathy, V., Z. Zakaria, S.L. Jothy, S. Gothai, S. Vijayarathna, L.Y. Latha, Y. Chen, and S. Sasidharan. Microbial Pathogenesis, 2014. 77: p. 7-12. ISI[000346459800002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346679900003">Identification of Plant-derived Natural Products as Potential Inhibitors of the Mycobacterium tuberculosis Proteasome.</a> Zheng, Y.J., X. Jiang, F. Gao, J.X. Song, J.X. Sun, L.X. Wang, X.X. Sun, Z.H. Lu, and H.Y. Zhang. BMC Complementary and Alternative Medicine, 2014. 14(400): 7pp. ISI[000346679900003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000346900000003">Computerized Prediction, Synthesis, and Antimicrobial Activity of New Amino-acid Derivatives of 2-Chloro-N-(9,10-dioxo-9,10-dihydroanthracen-1-yl)acetamide.</a> Zvarich, V.I., M.V. Stasevich, O.V. Stan&#39;ko, E.Z. Komarovskaya-Porokhnyavets, V.V. Poroikov, A.V. Rudik, A.A. Lagunin, M.V. Vovk, and V.P. Novikov. Pharmaceutical Chemistry Journal, 2014. 48(9): p. 582-586. ISI[000346900000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0116-012915.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
