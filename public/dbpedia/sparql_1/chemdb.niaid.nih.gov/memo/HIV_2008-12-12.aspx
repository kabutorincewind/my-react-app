

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2008-12-12.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xksvFhy0H5aC5Kcs8tqVCJ8WZQhzdgXJ9IQLofqyNINnQlYSizT8cxI+O38yAKuwXXjwPzEuhi0XbO08crN26vs68Fags6saYEhaTXEyy2COZk8JDYSuanIL7HU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8B130971" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  November 27 - December 10, 2008</h1>

    <h2>Anti*HIV</h2>

    <p class="title">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19041990?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Kuguacins F-S, cucurbitane triterpenoids from Momordica charantia.</a></p>

    <p class="authors">Chen JC, Liu WQ, Lu L, Qiu MH, Zheng YT, Yang LM, Zhang XM, Zhou L, Li ZR.</p>

    <p class="source">Phytochemistry. 2008 Nov 28. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19041990 [PubMed - as supplied by publisher]           </p> 

    <p class="title">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19009092?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A palladium-mediated cascade cyclisation approach to the CDE cores of rubriflordilactone A and lancifodilactone G.</a></p>

    <p class="authors">Cordonnier MC, Jennifer Kan SB, Anderson EA.</p>

    <p class="source">Chem Commun (Camb). 2008 Nov 30;(44):5818-20. Epub 2008 Oct 2.</p>

    <p class="pmid">PMID: 19009092 [PubMed - in process]        </p> 

    <p class="title">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19003569?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel synthesis and anti-HIV activity of 4&#39;-branched exomethylene carbocyclic nucleosides using a ring-closing metathesis of triene.</a></p>

    <p class="authors">Li H, Yoo JC, Hong JH.</p>

    <p class="source">Nucleosides Nucleotides Nucleic Acids. 2008 Dec;27(12):1238-49.</p>

    <p class="pmid">PMID: 19003569 [PubMed - in process]        </p> 

    <p class="title">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18977147?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and synthesis of novel 2&#39;,3&#39;-dideoxy-4&#39;-selenonucleosides as potential antiviral agents.</a></p>

    <p class="authors">Jeong LS, Choi YN, Tosh DK, Choi WJ, Kim HO, Choi J.</p>

    <p class="source">Bioorg Med Chem. 2008 Dec 1;16(23):9891-7. Epub 2008 Oct 17.</p>

    <p class="pmid">PMID: 18977147 [PubMed - in process]        </p> 

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18789977?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel targets for HIV therapy.</a></p>

    <p class="authors">Greene WC, Debyser Z, Ikeda Y, Freed EO, Stephens E, Yonemoto W, Buckheit RW, Esté JA, Cihlar T.</p>

    <p class="source">Antiviral Res. 2008 Dec;80(3):251-65. Epub 2008 Sep 30.</p>

    <p class="pmid">PMID: 18789977 [PubMed - in process]        </p> 

    <p class="title">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18654860?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Neurokinin-1 receptor antagonist (aprepitant) suppresses HIV-1 infection of microglia/macrophages.</a></p>

    <p class="authors">Wang X, Douglas SD, Song L, Wang YJ, Ho WZ.</p>

    <p class="source">J Neuroimmune Pharmacol. 2008 Dec;3(4):257-64. Epub 2008 Jul 25.</p>

    <p class="pmid">PMID: 18654860 [PubMed - in process]        </p> 

    <p class="title">7.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18618318?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Oleanolic acid and related derivatives as medicinally important compounds.</a></p>

    <p class="authors">Sultana N, Ata A.</p>

    <p class="source">J Enzyme Inhib Med Chem. 2008 Dec;23(6):739-56.</p>

    <p class="pmid">PMID: 18618318 [PubMed - in process]        </p> 

    <p class="title">8.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18608761?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Development of linear and nonlinear predictive QSAR models and their external validation using molecular similarity principle for anti-HIV indolyl aryl sulfones.</a></p>

    <p class="authors">Roy K, Mandal AS.</p>

    <p class="source">J Enzyme Inhib Med Chem. 2008 Dec;23(6):980-95.</p>

    <p class="pmid">PMID: 18608761 [PubMed - in process]        </p> 

    <p class="title">9.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18242784?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design and synthesis of 2-(2,6-dibromophenyl)-3-heteroaryl-1,3-thiazolidin-4-ones as anti-HIV agents.</a></p>

    <p class="authors">Rawal RK, Tripathi R, Katti SB, Pannecouque C, De Clercq E.</p>

    <p class="source">Eur J Med Chem. 2008 Dec;43(12):2800-6. Epub 2007 Dec 27.</p>

    <p class="pmid">PMID: 18242784 [PubMed - in process]        </p> 

    <p class="title">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19057930?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Computer-based design of novel HIV-1 entry inhibitors: neomycin conjugated to arginine peptides at two specific sites.</a></p>

    <p class="authors">Berchanski A, Lapidot A.</p>

    <p class="source">J Mol Model. 2008 Dec 5. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19057930 [PubMed - as supplied by publisher]           </p>

    <h2>(Drug or Compound or Therapeutic) and (HIV or AIDS or Human Immunodeficiency Virus)</h2>

    <p class="title">11.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19064629?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">The Relative Activity of &#39;Function Sparing&#39; HIV-1 Entry Inhibitors on Viral Entry and CCR5 Internalization: Is Allosteric Functional Selectivity a Valuable Therapeutic Property?</a></p>

    <p class="authors">Muniz-Medina V, Jones S, Maglich J, Galardi C, Hollingsworth R, Kazmierski W, Ferris R, Edelstein M, Chiswell K, Kenakin T.</p>

    <p class="source">Mol Pharmacol. 2008 Dec 8. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19064629 [PubMed - as supplied by publisher]           </p> 

    <p class="title">12.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19047650?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Specific interactions between the viral co-receptor CXCR4 and the biguanide-based compound NB325 mediate inhibition of human immunodeficiency virus type 1 infection.</a></p>

    <p class="authors">Thakkar N, Pirrone V, Passic S, Zhu W, Kholodovych V, Welsh W, Rando RF, Labib ME, Wigdahl B, Krebs FC.</p>

    <p class="source">Antimicrob Agents Chemother. 2008 Dec 1. [Epub ahead of print]</p>

    <p class="pmid">PMID: 19047650 [PubMed - as supplied by publisher]           </p> 

    <p class="title">13.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19040279?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">PL-100, a novel HIV-1 protease inhibitor displaying a high genetic barrier to resistance: an in vitro selection study.</a></p>

    <p class="authors">Dandache S, Coburn CA, Oliveira M, Allison TJ, Holloway MK, Wu JJ, Stranix BR, Panchal C, Wainberg MA, Vacca JP.</p>

    <p class="source">J Med Virol. 2008 Dec;80(12):2053-63.</p>

    <p class="pmid">PMID: 19040279 [PubMed - in process]        </p>    

    <h2>(HIV or Aids or &quot;human immunodeficiency virus&quot;) and (inhibit*)</h2> 

    <p class="title">14.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18842738?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Llama antibody fragments with cross-subtype human immunodeficiency virus type 1 (HIV-1)-neutralizing properties and high affinity for HIV-1 gp120.</a></p>

    <p class="authors">Forsman A, Beirnaert E, Aasa-Chapman MM, Hoorelbeke B, Hijazi K, Koh W, Tack V, Szynol A, Kelly C, McKnight A, Verrips T, de Haard H, Weiss RA.</p>

    <p class="source">J Virol. 2008 Dec;82(24):12069-81. Epub 2008 Oct 8.</p>

    <p class="pmid">PMID: 18842738 [PubMed - in process]        </p> 

    <p class="title">15.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18809675?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Albumin-conjugated C34 Peptide HIV-1 Fusion Inhibitor: EQUIPOTENT TO C34 AND T-20 IN VITRO WITH SUSTAINED ACTIVITY IN SCID-HU THY/LIV MICE.</a></p>

    <p class="authors">Stoddart CA, Nault G, Galkina SA, Thibaudeau K, Bakis P, Bousquet-Gagnon N, Robitaille M, Bellomo M, Paradis V, Liscourt P, Lobach A, Rivard ME, Ptak RG, Mankowski MK, Bridon D, Quraishi O.</p>

    <p class="source">J Biol Chem. 2008 Dec 5;283(49):34045-52. Epub 2008 Sep 22.</p>

    <p class="pmid">PMID: 18809675 [PubMed - in process]        </p>  

    <p class="pmid memofmt2-1">(HIV or Aids or &quot;human immunodeficiency virus&quot;) and (protease or proteinase)</p>

    <p class="pmid memofmt2-1">&nbsp;</p>

    <p class="title">16.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19009075?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Target-selective photo-degradation of HIV-1 protease by a fullerene-sugar hybrid.</a></p>

    <p class="authors">Tanimoto S, Sakai S, Matsumura S, Takahashi D, Toshima K.</p>

    <p class="source">Chem Commun (Camb). 2008 Nov 30;(44):5767-9. Epub 2008 Oct 2.</p>

    <p class="pmid">PMID: 19009075 [PubMed - in process]        </p> 

    <p class="title">17.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18799572?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Biochemical and biophysical characterization of a chimeric TRIM21-TRIM5alpha protein.</a></p>

    <p class="authors">Kar AK, Diaz-Griffero F, Li Y, Li X, Sodroski J.</p>

    <p class="source">J Virol. 2008 Dec;82(23):11669-81. Epub 2008 Sep 17.</p>

    <p class="pmid">PMID: 18799572 [PubMed - in process]        </p> 

    <p class="title">18.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18751697?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Marmorin, a new ribosome inactivating protein with antiproliferative and HIV-1 reverse transcriptase inhibitory activities from the mushroom Hypsizigus marmoreus.</a></p>

    <p class="authors">Wong JH, Wang HX, Ng TB.</p>

    <p class="source">Appl Microbiol Biotechnol. 2008 Dec;81(4):669-74. Epub 2008 Aug 27.</p>

    <p class="pmid">PMID: 18751697 [PubMed - in process]        </p>

    <h2>Journal Check</h2> 

    <p class="title">19.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19024630?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Binding modes of two novel non-nucleoside reverse transcriptase inhibitors, YM-215389 and YM-228855, to HIV type-1 reverse transcriptase.</a></p>

    <p class="authors">Kodama E, Orita M, Masuda N, Yamomoto O, Fujii M, Ohgami T, Kageyama S, Ohta M, Hatta T, Inoue H, Suzuki H, Sudo K, Shimizu Y, Matsuoka M.</p>

    <p class="source">Antivir Chem Chemother. 2008;19(3):133-41.</p>

    <p class="pmid">PMID: 19024630 [PubMed - in process]        </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
