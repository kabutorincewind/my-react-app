

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-06-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yID+WwH/xZo2H+TbBle4PqAwNm0ZoF4xuwxKZB7mVkNCFeXHBIPXMUqS6tgcYH8L03dpvyFDL/sm1D/yspRNEFNIVOXx/+nohyaFBbf5/j3ou/zLpVhPuue3On4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DB30E878" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: May 23 - June 5, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24691882">A Novel Antifungal Protein with Lysozyme-like Activity from Seeds of Clitoria ternatea.</a> K, A. and S. K. Applied Biochemistry and Biotechnology, 2014. 173(3): p. 682-693. PMID[24691882].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24674450">A Novel Echinocandin MIG0310 with Anticandida Activity from Newly Isolated Fusarium Sp. Strain MS-R1.</a> Masaphy, S. Journal of Applied Microbiology, 2014. 116(6): p. 1458-1464. PMID[24674450].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24559136">Chemical Composition, Antibacterial, Antifungal and Antioxidant Activities of Algerian Eryngium tricuspidatum L. Essential Oil.</a> Merghache, D., Z. Boucherit-Otmani, S. Merghache, I. Chikhi, C. Selles, and K. Boucherit. Journal of Natural Products, 2014. 28(11): p. 795-807. PMID[24559136].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24867320">A Novel Antifungal is Active against Candida albicans Biofilms and Inhibits Mutagenic Acetaldehyde Production in Vitro.</a>Nieminen, M.T., L. Novak-Frazer, V. Rautemaa, R. Rajendran, T. Sorsa, G. Ramage, P. Bowyer, and R. Rautemaa. Plos One, 2014. 9(5): p. e97864. PMID[24867320].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24535279">Pyrrolo[1,2-alpha][1,4]benzodiazepines Show Potent in Vitro Antifungal Activity and Significant in Vivo Efficacy in a Microsporum canis Dermatitis Model in Guinea Pigs.</a> Paulussen, C., K. de Wit, G. Boulet, P. Cos, L. Meerpoel, and L. Maes. Journal of Antimicrobial Chemotherapy, 2014. 69(6): p. 1608-1610. PMID[24535279].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24613800">Five New Sesquiterpenoids from Dalbergia odorifera.</a>Wang, H., W.H. Dong, W.J. Zuo, S. Liu, H.M. Zhong, W.L. Mei, and H.F. Dai. Fitoterapia, 2014. 95: p. 16-21. PMID[24613800].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24642357">Dual Antifungal Properties of Cationic Antimicrobial Peptides Polybia-MPI: Membrane Integrity Disruption and Inhibition of Biofilm Formation.</a> Wang, K., J. Yan, W. Dang, J. Xie, B. Yan, W. Yan, M. Sun, B. Zhang, M. Ma, Y. Zhao, F. Jia, R. Zhu, W. Chen, and R. Wang. Peptides, 2014. 56: p. 22-29. PMID[24642357].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24685500">Flavonoids and Stilbenoids from Derris eriocarpa.</a>Zhang, H.X., P.K. Lunga, Z.J. Li, Q. Dai, and Z.Z. Du. Fitoterapia, 2014. 95: p. 147-153. PMID[24685500].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24740074">Profiling of rpoB Mutations and MICs for Rifampin and Rifabutin in Mycobacterium tuberculosis.</a> Jamieson, F.B., J.L. Guthrie, A. Neemuchwala, O. Lastovetska, R.G. Melano, and C. Mehaffy. Journal of Clinical Microbiology, 2014. 52(6): p. 2157-2162. PMID[24740074].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24735645">Synthesis, Crystal Studies, Anti-tuberculosis and Cytotoxic Studies of 1-[(2E)-3-Phenylprop-2-enoyl]-1H-benzimidazole Derivatives.</a>Kalalbandi, V.K., J. Seetharamappa, U. Katrahalli, and K.G. Bhat. European Journal of Medicinal Chemistry, 2014. 79: p. 194-202. PMID[24735645].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24687500">Deoxysugars as Antituberculars and alpha-Mannosidase Inhibitors.</a>Kashyap, V., S. Sharma, V. Singh, S.K. Sharma, M. Saquib, R. Srivastava, and A.K. Shaw. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3530-3532. PMID[24687500].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24690454">Antimicrobial Prenylated Benzoylphloroglucinol Derivatives and Xanthones from the Leaves of Garcinia goudotiana.</a>Mahamodo, S., C. Riviere, C. Neut, A. Abedini, H. Ranarivelo, N. Duhal, V. Roumy, T. Hennebelle, S. Sahpaz, A. Lemoine, D. Razafimahefa, B. Razanamahefa, F. Bailleul, and B. Andriamihaja. Phytochemistry, 2014. 102: p. 162-168. PMID[24690454].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24721314">Synthesis, Crystal Structures and Characterization of Late First Row Transition Metal Complexes Derived from Benzothiazole Core: Anti-tuberculosis Activity and Special Emphasis on DNA Binding and Cleavage Property.</a> Netalkar, P.P., S.P. Netalkar, S. Budagumpi, and V.K. Revankar. European Journal of Medicinal Chemistry, 2014. 79: p. 47-56. PMID[24721314].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24721315">Facile Synthesis of Benzonitrile/Nicotinonitrile Based S-triazines as New Potential Antimycobacterial Agents.</a> Patel, A.B., K.H. Chikhalia, and P. Kumari. European Journal of Medicinal Chemistry, 2014. 79: p. 57-65. PMID[24721315].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24709266">Killing of Mycobacterium avium by Lactoferricin Peptides: Improved Activity of Arginine- and D-amino-acid-containing Molecules.</a> Silva, T., B. Magalhaes, S. Maia, P. Gomes, K. Nazmi, J.G. Bolscher, P.N. Rodrigues, M. Bastos, and M.S. Gomes. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3461-3467. PMID[24709266].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24886941">Diethyl 2-(phenylcarbamoyl)phenyl phosphorothioates: Synthesis, Antimycobacterial Activity and Cholinesterase Inhibition.</a>Vinsova, J., M. Kratky, M. Komloova, E. Dadapeer, S. Stepankova, K. Vorcakova, and J. Stolarikova. Molecules, 2014. 19(6): p. 7152-7168. PMID[24886941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24526515">In Vitro and Intracellular Activity of 4-Substituted piperazinyl phenyl oxazolidinone Analogues against Mycobacterium tuberculosis.</a> Wang, D., Y. Zhao, Z. Liu, H. Lei, M. Dong, and P. Gong. Journal of Antimicrobial Chemotherapy, 2014. 69(6): p. 1711-1714. PMID[24526515].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24663022">In Vitro and in Vivo Activities of Three Oxazolidinones against Nonreplicating Mycobacterium tuberculosis.</a>Zhang, M., C. Sala, N. Dhar, A. Vocat, V.K. Sambandamurthy, S. Sharma, G. Marriner, V. Balasubramanian, and S.T. Cole. Antimicrobial Agents and Chemotherapy, 2014. 58(6): p. 3217-3223. PMID[24663022].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24560941">Synthesis and in Vitro Biological Evaluation of Aminoacridines and Artemisinin-acridine Hybrids.</a> Joubert, J.P., F.J. Smit, L. du Plessis, P.J. Smith, and D.D. N&#39;Da. European Journal of Pharmaceutical Sciences, 2014. 56: p. 16-27. PMID[24560941].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24763263">Synthesis and Biological Evaluation of some Novel Pyrido[1,2-a]pyrimidin-4-ones as Antimalarial Agents.</a> Mane, U.R., D. Mohanakrishnan, D. Sahal, P.R. Murumkar, R. Giridhar, and M.R. Yadav. European Journal of Medicinal Chemistry, 2014. 79: p. 422-435. PMID[24763263].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24834447">Synthesis and Antimalarial Activity of Prodigiosenes.</a>Marchal, E., D.A. Smithen, M.I. Uddin, A.W. Robertson, D.L. Jakeman, V. Mollard, C.D. Goodman, K.S. MacDougall, S.A. McFarland, G.I. McFadden, and A. Thompson. Organic &amp; Biomolecular Chemistry, 2014. 12(24): p. 4132-4142. PMID[24834447].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24775306">Antimalarial Chemotherapy: Artemisinin-derived Dimer Carbonates and Thiocarbonates.</a> Mazzone, J.R., R.C. Conyers, A.K. Tripathi, D.J. Sullivan, and G.H. Posner. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(11): p. 2440-2443. PMID[24775306].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24808642">Antiplasmodial Activity of Certain Medicinal Plants against Chloroquine Resistant Plasmodium berghei Infected White Albino BALB/c Mice.</a> Rajendran, C., M. Begam, D. Kumar, I. Baruah, H.K. Gogoi, R.B. Srivastava, and V. Veer. Journal of Parasitic Diseases, 2014. 38(2): p. 148-152. PMID[24808642].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24747290">Design, Economical Synthesis and Antiplasmodial Evaluation of Vanillin Derived Allylated Chalcones and their Marked Synergism with Artemisinin against Chloroquine Resistant Strains of Plasmodium falciparum.</a>Sharma, N., D. Mohanakrishnan, U.K. Sharma, R. Kumar, Richa, A.K. Sinha, and D. Sahal. European Journal of Medicinal Chemistry, 2014. 79: p. 350-368. PMID[24747290].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24742203">Second Generation Steroidal 4-Aminoquinolines are Potent, Dual-target Inhibitors of the Botulinum Neurotoxin Serotype A Metalloprotease and P. falciparum Malaria.</a> Videnovic, M., D.M. Opsenica, J.C. Burnett, L. Gomba, J.E. Nuss, Z. Selakovic, J. Konstantinovic, M. Krstic, S. Segan, M. Zlatovic, R.J. Sciotti, S. Bavari, and B.A. Solaja. Journal of Medicinal Chemistry, 2014. 57(10): p. 4134-4153. PMID[24742203].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24879541">Investigation of Indolglyoxamide and Indolacetamide Analogues of Polyamines as Antimalarial and Antitrypanosomal Agents.</a>Wang, J., M. Kaiser, and B.R. Copp. Marine Drugs, 2014. 12(6): p. 3138-3160. PMID[24879541].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0523-060514.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334906700009">Pharmacological Evaluation of Crude Methanolic Extract of Kalanchoe pinnata Leaves.</a> Ahmed, M., Z.U. Wazir, R.A. Khan, M.I. Khan, S. Waqas, and A. Iqbal. Toxicological and Environmental Chemistry, 2013. 95(9): p. 1539-1545. ISI[000334906700009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335206800010">Anti-tubercular, Antioxidant and in Vitro Anti-inflammatory Activity of Some Newly Synthesized Chalcones.</a> Alam, S., R. Panda, and M. Kachroo. Indian Journal of Chemistry, 2014. 53(4): p. 440-443. ISI[000335206800010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334587000008">Antifungal Activity of Essential Oils of Three Aromatic Plants from Western Algeria against Five Fungal Pathogens of Tomato (Lycopersicon esculentum Mill).</a> Alam, S.B., N.G. Benyelles, M.E.A. Dib, N. Djabou, L. Tabti, J. Paolini, A. Muselli, and J. Costa. Journal of Applied Botany and Food Quality, 2014. 87: p. 56-61. ISI[000334587000008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335113900030">Optimization of an Imidazopyridazine Series of Inhibitors of Plasmodium falciparum Calcium-dependent Protein Kinase 1 (PfCDPK1).</a> Chapman, T.M., S.A. Osborne, C. Wallace, K. Birchall, N. Bouloc, H.M. Jones, K.H. Ansell, D.L. Taylor, B. Clough, J.L. Green, and A.A. Holder. Journal of Medicinal Chemistry, 2014. 57(8): p. 3570-3587. ISI[000335113900030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334493100038">The Preliminary Assessment of Anti-microbial Activity of HPLC Separated Components of Kirkia wilmsii.</a> Chigayo, K., P.E.L. Mojapelo, P. Bessong, and J.R. Gumbo. African Journal of Traditional Complementary and Alternative Medicines, 2014. 11(3): p. 275-281. ISI[000334493100038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335273800006">Synthesis and Antimicrobial Evaluation of Some New 1,2-bis-(2-(N-Arylimino)-1,3-thiazolidin-3-yl)ethane Derivatives.</a> Dawood, K.M. and H.K.A. Abu-Deif. Chemical &amp; Pharmaceutical Bulletin, 2014. 62(5): p. 439-445. ISI[000335273800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334682700018">Antimicrobial and SEM Studies of Sophorolipids Synthesized using Lauryl Alcohol.</a> Dengle-Pulate, V., P. Chandorkar, S. Bhagwat, and A.A. Prabhune. Journal of Surfactants and Detergents, 2014. 17(3): p. 543-552. ISI[000334682700018].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334802400011">Biosynthetic Genes and Activity Spectrum of Antifungal Polyynes from Collimonas fungivorans Ter331.</a> Fritsche, K., M. van den Berg, W. de Boer, T.A. van Beek, J.M. Raaijmakers, J.A. van Veen, and J.H.J. Leveau. Environmental Microbiology, 2014. 16(5): p. 1334-1345. ISI[000334802400011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334402600013">Antimicrobial, Cytotoxic and Phytotoxic Potency of Ethyl Acetate Extract of Rhizopus stolonifer Culture.</a> Iqbal, M., M. Amin, Z. Iqbal, H. Bibi, A. Iqbal, Z.U. Din, M. Suleman, and H.U. Shah. Tropical Journal of Pharmaceutical Research, 2014. 13(1): p. 87-92. ISI[000334402600013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334420300040">N-Substituted 5-amino-6-methylpyrazine-2,3-dicarbonitriles: Microwave-assisted Synthesis and Biological Properties.</a> Jandourek, O., M. Dolezal, P. Paterova, V. Kubicek, M. Pesko, J. Kunes, A. Coffey, J.H. Guo, and K. Kralova. Molecules, 2014. 19(1): p. 651-671. ISI[000334420300040].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334184400029">Synthesis and Biological Evaluation of Amino acid-linked 1,2,3-Bistriazole Conjugates as Potential Antimicrobial Agents.</a> Kaushik, C.P., K. Lal, A. Kumar, and S. Kumar. Medicinal Chemistry Research, 2014. 23(6): p. 2995-3004. ISI[000334184400029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334444700101">Synthesis and Biological Evaluation of 2-Aminobenzamide Derivatives as Antimicrobial Agents: Opening/Closing Pharmacophore Site.</a> Mabkhot, Y.N., A.M. Al-Majid, A. Barakat, S.S. Al-Showiman, M.S. Al-Har, S. Radi, M.M. Naseer, and T.B. Hadda. International Journal of Molecular Sciences, 2014. 15(3): p. 5115-5127. ISI[000334444700101].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334184400020">Synthesis and Antimicrobial Screening of Some Novel Chalcones and Flavanones Substituted with Higher Alkyl Chains.</a> Mallavadhani, U.V., L. Sahoo, K.P. Kumar, and U.S. Murty. Medicinal Chemistry Research, 2014. 23(6): p. 2900-2908. ISI[000334184400020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334562700035">In Vitro Evaluation of Antimicrobial Effect of Miswak against Common Oral Pathogens.</a> Naseem, S., K. Hashmi, F. Fasih, S. Sharafat, and R. Khanani. Pakistan Journal of Medical Sciences, 2014. 30(2): p. 398-403. ISI[000334562700035].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334627900001">Chemical Characterization by GC-MS and in Vitro Activity against Candida albicans of Volatile Fractions Prepared from Artemisia dracunculus, Artemisia abrotanum, Artemisia absinthium and Artemisia vulgaris.</a> Obistioiu, D., R.T. Cristina, I. Schmerold, R. Chizzola, K. Stolze, I. Nichita, and V. Chiurciu. Chemistry Central Journal, 2014. 8. ISI[000334627900001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333749200014">Time-dependent Diaryl Ether Inhibitors of InhA: Structure-activity Relationship Studies of Enzyme Inhibition, Antibacterial Activity, and in Vivo Efficacy.</a> Pan, P., S.E. Knudson, G.R. Bommineni, H.J. Li, C.T. Lai, N.N. Liu, M. Garcia-Diaz, C. Simmerling, S.S. Patil, R.A. Slayden, and P.J. Tonge. ChemMedChem, 2014. 9(4): p. 776-791. ISI[000333749200014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334337100038">Design and Synthesis of Triazolopyrimidine acylsulfonamides as Novel Anti-mycobacterial Leads Acting through Inhibition of Acetohydroxyacid Synthase.</a> Patil, V., M. Kale, A. Raichurkar, B. Bhaskar, D. Prahlad, M. Balganesh, S. Nandan, and P.S. Hameed. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(9): p. 2222-2225. ISI[000334337100038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333569500001">Chemical and Biological Study on the Essential Oil of Artemisia caerulescens L. ssp densiflora (Viv).</a> Petretto, G.L., M. Chessa, A. Piana, M.D. Masia, M. Foddai, G. Mangano, N. Culeddu, F.U. Afifi, and G. Pintore. Natural Product Research, 2013. 27(19): p. 1709-1715. ISI[000333569500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333780800020">Microwave Induced Three-component Synthesis and Antimycobacterial Activity of Benzopyrazolo[3,4-b]quinolindiones.</a> Quiroga, J., Y. Diaz, J. Bueno, B. Insuasty, R. Abonia, A. Ortiz, M. Nogueras, and J. Cobo. European Journal of Medicinal Chemistry, 2014. 74: p. 216-224. ISI[000333780800020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000334656200002">Synthesis, Characterization, Antibacterial, Antifungal and Anthelmintic Activities of a New 5-Nitroisatin Schiff Base and its Metal Complexes.</a> Rao, R., K.R. Reddy, and K.N. Mahendra. Bulgarian Chemical Communications, 2014. 46(1): p. 11-17. ISI[000334656200002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>

    <br />

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000335230500001">Evaluation of Anti-candida Activity of Vitis vinifera L. Seed Extracts Obtained from Wine and Table Cultivars.</a> Simonetti, G., A.R. Santamaria, F.D. D&#39;Auria, N. Mulinacci, M. Innocenti, F. Cecchini, E. Pericolini, E. Gabrielli, S. Panella, D. Antonacci, A.T. Palamara, A. Vecchiarelli, and G. Pasqua. BioMed Research International, 2014. 127021: p. 11. ISI[000335230500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0523-060514.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
