

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-09-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FL4fFejeB4z8tAyDrFwYJOzCHyJwPIY21J6siRTWBTB6CChNNJF2DmgQ4D+pe8TpINUOtYgwyfeTosajagao73noD5EC2lcCa2IPBe8x+x/mhLMIJI+wkxJvqmg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="953077D8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 29 - September 11, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25165817">Heat-stable Molecule Derived from Streptococcus cristatus Induces APOBEC3 Expression and Inhibits HIV-1 Replication.</a> Wang, Z., Y. Luo, Q. Shao, B.L. Kinlock, C. Wang, J.E. Hildreth, H. Xie, and B. Liu. PloS One, 2014. 9(8): p. e106078. PMID[25165817].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24942573">Comparative Analysis of the Capacity of Elite Suppressor CD4+ and CD8+ T Cells to Inhibit HIV-1 Replication in Monocyte-derived Macrophages.</a> Walker-Sperling, V.E., R.W. Buckheit, 3rd, and J.N. Blankson. Journal of Virology, 2014. 88(17): p. 9789-9798. PMID[24942573].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25105490">HIV Pre-exposure Prophylaxis: Mucosal Tissue Drug Distribution of RT Inhibitor Tenofovir and Entry Inhibitor Maraviroc in a Humanized Mouse Model.</a> Veselinovic, M., K.H. Yang, J. LeCureux, C. Sykes, L. Remling-Mulder, A.D. Kashuba, and R. Akkina. Virology, 2014. 464-465: p. 253-263. PMID[25105490].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25010891">A New Antiviral: Chimeric 3TC-AZT Phosphonate Efficiently Inhibits HIV-1 in Human Tissues ex Vivo.</a> Vanpouille, C., A. Khandazhinskaya, I. Karpenko, S. Zicari, V. Barreto-de-Souza, S. Frolova, L. Margolis, and S. Kochetkov. Antiviral Research, 2014. 109: p. 125-131. PMID[25010891].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25064352">Marine Actinobacteria Associated with Marine Organisms and Their Potentials in Producing Pharmaceutical Natural Products.</a> Valliappan, K., W. Sun, and Z. Li. Applied Microbiology and Biotechnology, 2014. 98(17): p. 7365-7377. PMID[25064352].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25175101">Efficient HIV-1 Inhibition by a 16 nt-long RNA Aptamer Designed by combining in Vitro Selection and in Silico Optimisation Strategies.</a> Sanchez-Luque, F.J., M. Stich, S. Manrubia, C. Briones, and A. Berzal-Herranz. Scientific Reports, 2014. 4: p. 6242. PMID[25175101].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24973878">Zinc Finger Nuclease: A New Approach for Excising HIV-1 Proviral DNA from Infected Human T Cells.</a> Qu, X., P. Wang, D. Ding, X. Wang, G. Zhang, X. Zhou, L. Liu, X. Zhu, H. Zeng, and H. Zhu. Molecular Biology Reports, 2014. 41(9): p. 5819-5827. PMID[24973878].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24970894">4&#39;-Ethynyl-2-fluoro-2&#39;-deoxyadenosine (EFdA) Inhibits HIV-1 Reverse Transcriptase with Multiple Mechanisms.</a> Michailidis, E., A.D. Huber, E.M. Ryan, Y.T. Ong, M.D. Leslie, K.B. Matzek, K. Singh, B. Marchand, A.N. Hagedorn, K.A. Kirby, L.C. Rohan, E.N. Kodama, H. Mitsuya, M.A. Parniak, and S.G. Sarafianos. Journal of Biological Chemistry, 2014. 289(35): p. 24533-24548. PMID[24970894].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25136083">TIM-family Proteins Inhibit HIV-1 Release.</a> Li, M., S.D. Ablan, C. Miao, Y.M. Zheng, M.S. Fuller, P.D. Rennert, W. Maury, M.C. Johnson, E.O. Freed, and S.L. Liu. Proceedings of the National Academy Sciences of the United States of America, 2014. 111(35): p. E3699-E3707. PMID[25136083].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25182330">Dolutegravir: A New Integrase Strand Transfer Inhibitor for the Treatment of HIV - an Alternative Viewpoint.</a> Gillette, M.A., B.M. Shah, J.J. Schafer, and J.A. DeSimone, Jr. Pharmacotherapy, 2014. 34(9): p. e173-e174. PMID[25182330].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25074848">Synthesis and Biological Evaluation of New Simple Indolic Non Peptidic HIV Protease Inhibitors: The Effect of Different Substitution Patterns.</a> Bonini, C., L. Chiummiento, N. Di Blasio, M. Funicello, P. Lupattelli, F. Tramutola, F. Berti, A. Ostric, S. Miertus, V. Frecer, and D.X. Kong. Bioorganic &amp; Medicinal Chemistry, 2014. 22(17): p. 4792-4802. PMID[25074848].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0829-091114.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340223400018">Synthesis and anti-HIV-1 Activity of the Conjugates of Gossypol with Oligopeptides and D-Glucosamine.</a> Yang, J., J.R. Li, J.X. Yang, L.L. Li, W.J. Ouyang, S.W. Wu, and F. Zhang. Chinese Chemical Letters, 2014. 25(7): p. 1052-1056. ISI[000340223400018].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340071200017">Synthesis, in-Vitro Reverse Transcriptase Inhibitory Activity and Docking Study of Some New Imidazol-5-one Analogs.</a> Mokale, S.N., D.K. Lokwani, and D.B. Shinde. Medicinal Chemistry Research, 2014. 23(8): p. 3752-3764. ISI[000340071200017].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340301500039">Design and Synthesis of a New Series of Cyclopropylamino-linking Diarylpyrimidines as HIV Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Liu, Y., G. Meng, A. Zheng, F. Chen, W.X. Chen, C.E. De, C. Pannecouque, and J. Balzarini. European Journal of Pharmaceutical Sciences, 2014. 62: p. 334-341. ISI[000340301500039].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340209900008">Anti-HIV-1 Activity of Phlorotannin Derivative 8,4&#39;&#39;&#39;-Dieckol from Korean Brown Alga Ecklonia cava.</a> Karadeniz, F., K.H. Kang, J.W. Park, S.J. Park, and S.K. Kim. Bioscience Biotechnology and Biochemistry, 2014. 78(7): p. 1151-1158. ISI[000340209900008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0829-091114.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000340225100035">Reactivation of Latent HIV-1 by New Semi-synthetic Ingenol Esters.</a> Jose, D.P., K. Bartholomeeusen, C.R.D. da, C.M. Abreu, J. Glinski, C.T.B.F. da, A. Rabay, L.F. Pianowski, L.W. Dudycz, U. Ranga, B.M. Peterlin, L.F. Pianowski, A. Tanuri, and R.S. Aguiar. Virology, 2014. 462: p. 328-339. ISI[000340225100035].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0829-091114.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
