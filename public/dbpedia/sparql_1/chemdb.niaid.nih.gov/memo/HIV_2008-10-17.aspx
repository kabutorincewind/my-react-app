

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2008-10-17.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vV9qtUx22qNHzpY5pNyKahmzNrNU8322tzGLUZo6fPtxopYL/QKYpwiv4SOiv3HvFWXgN+RcORQWRglCf7kA7J2SBLVYnxaPtVdH+9k2SCPgIiNefokW5FuQEGo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B0148F9C" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  October 1 - October 14, 2008</h1>

    <h2>Anti-HIV</h2> 

    <p class="ListParagraph">1.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18842358?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Maneglier B, Rogez-Kreuz C, Dereuddre-Bosquet N, Martal J, Devillier P, Dormont D, Clayette P.</a></p>

    <p class="plaintext">            [Anti-HIV effects of IFN-tau in human macrophages: Role of cellular antiviral factors and interleukin-6.]</p>

    <p class="plaintext">            Pathol Biol (Paris). 2008 Oct 6. [Epub ahead of print] French.</p>

    <p class="plaintext">            PMID: 18842358 [PubMed - as supplied by publisher]              </p> 

    <p class="ListParagraph">2.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18817633?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Du L, Zhao YX, Yang LM, Zheng YT, Tang Y, Shen X, Jiang HL.</a></p>

    <p class="plaintext">            Symmetrical 1-pyrrolidineacetamide showing anti-HIV activity through a new binding site on HIV-1 integrase.</p>

    <p class="plaintext">            Acta Pharmacol Sin. 2008 Oct;29(10):1261-7.</p>

    <p class="plaintext">            PMID: 18817633 [PubMed - in process]                                    </p> 

    <p class="ListParagraph">3.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18788044?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Oh CH, Liu LJ, Hong JH.</a></p>

    <p class="plaintext">            First synthesis and anti-HIV evaluation of 4&#39;-methyl-cyclopentanyl 9-deazaadenosine.</p>

    <p class="plaintext">            Nucleosides Nucleotides Nucleic Acids. 2008 Oct;27(10):1144-52. </p>

    <p class="plaintext">            PMID: 18788044 [PubMed - in process]                                    </p> 

    <p class="ListParagraph">4.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18753929?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Pollock S, Dwek RA, Burton DR, Zitzmann N.</a></p>

    <p class="plaintext">N-Butyldeoxynojirimycin is a broadly effective anti-HIV therapy significantly enhanced by targeted liposome delivery.                                                           </p>

    <p class="plaintext">AIDS. 2008 Oct 1;22(15):1961-9.</p>

    <p class="plaintext">PMID: 18753929 [PubMed - in process]                                    </p> 

    <p class="ListParagraph">5.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18832250?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Dolin R.</a></p>

    <p class="plaintext">A new class of anti-HIV therapy and new challenges.</p>

    <p class="plaintext">N Engl J Med. 2008 Oct 2;359(14):1509-11. No abstract available.  </p>

    <p class="plaintext">PMID: 18832250 [PubMed - indexed for MEDLINE]                </p> 

    <p class="ListParagraph">6.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18691555?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Du L, Zhao Y, Chen J, Yang L, Zheng Y, Tang Y, Shen X, Jiang H.</a></p>

    <p class="plaintext">            D77, one benzoic acid derivative, functions as a novel anti-HIV-1 inhibitor targeting the interaction between integrase and cellular LEDGF/p75.</p>

    <p class="plaintext">            Biochem Biophys Res Commun. 2008 Oct 10;375(1):139-44. Epub 2008 Aug 6.   </p>

    <p class="plaintext">            PMID: 18691555 [PubMed - indexed for MEDLINE]                </p> 

    <p class="ListParagraph">7.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18682248?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Shi W, Qi Z, Pan C, Xue N, Debnath AK, Qie J, Jiang S, Liu K.</a></p>

    <p class="plaintext">            Novel anti-HIV peptides containing multiple copies of artificially designed heptad repeat motifs.</p>

    <p class="plaintext">            Biochem Biophys Res Commun. 2008 Oct 3;374(4):767-72. Epub 2008 Aug 3.    </p>

    <p class="plaintext">            PMID: 18682248 [PubMed - indexed for MEDLINE]                </p> 

    <p class="ListParagraph">8.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18644965?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Abdurahman S, Végvári A, Youssefi M, Levi M, Höglund S, Andersson E, Horal P, Svennerholm B, Balzarini J, Vahlne A.</a></p>

    <p class="plaintext">Activity of the small modified amino acid alpha-hydroxy glycineamide on in vitro and in vivo human immunodeficiency virus type 1 capsid assembly and infectivity.</p>

    <p class="plaintext">Antimicrob Agents Chemother. 2008 Oct;52(10):3737-44. Epub 2008 Jul 21.        </p>

    <p class="plaintext">PMID: 18644965 [PubMed - in process]                                    </p> 

    <p class="ListParagraph">9.                   <a href="http://www.ncbi.nlm.nih.gov/pubmed/18538869?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Garg H, Francella N, Tony KA, Augustine LA, Barchi JJ Jr, Fantini J, Puri A, Mootoo DR, Blumenthal R.</a></p>

    <p class="plaintext">            Glycoside analogs of beta-galactosylceramide, a novel class of small molecule antiviral agents that inhibit HIV-1 entry.</p>

    <p class="plaintext">            Antiviral Res. 2008 Oct;80(1):54-61. Epub 2008 May 19.            </p>

    <p class="plaintext">            PMID: 18538869 [PubMed - in process]            </p>

    <h2>Anti-AIDS</h2>

    <p class="ListParagraph">10.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18783210?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ma CM, Hattori M, Daneshtalab M, Wang L.</a></p>

    <p class="plaintext">            Chlorogenic acid derivatives with alkyl chains of different lengths and orientations: potent alpha-glucosidase inhibitors.</p>

    <p class="plaintext">            J Med Chem. 2008 Oct 9;51(19):6188-94. Epub 2008 Sep 11.     </p>

    <p class="plaintext">            PMID: 18783210 [PubMed - in process]            </p>

    <h2>Anti-viral and HIV</h2>

    <p class="ListParagraph">11.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18850049?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Wang S, Han J, Wang Y, Lu W, Chi C.</a></p>

    <p class="plaintext">Design of peptide inhibitors for furin based on the C-terminal fragment of histone H1.2. Acta Biochim Biophys Sin (Shanghai). 2008 Oct;40(10):848-54.</p>

    <p class="plaintext">PMID: 18850049 [PubMed - in process]            </p> 

    <h2>(HIV or AIDS) and Antiretroviral</h2>

    <p class="ListParagraph">12.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18852475?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">He Y, Cheng J, Lu H, Li J, Hu J, Qi Z, Liu Z, Jiang S, Dai Q.</a>       </p>

    <p class="plaintext">Potent HIV fusion inhibitors against Enfuvirtide-resistant HIV-1 strains.              </p>

    <p class="plaintext">Proc Natl Acad Sci U S A. 2008 Oct 13. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18852475 [PubMed - as supplied by publisher]                </p> 

    <p class="ListParagraph">13.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18852272?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Prichard MN, Hartline CB, Harden EA, Daily SL, Beadle JR, Valiaeva N, Kern ER, Hostetler KY.</a></p>

    <p class="plaintext">Inhibition of Herpesvirus Replication by Hexadecyloxypropyl Esters of Purine and Pyrimidine Based Phosphonomethoxyethyl Nucleoside Phosphonates.</p>

    <p class="plaintext">Antimicrob Agents Chemother. 2008 Oct 13. [Epub ahead of print]         </p>

    <p class="plaintext">PMID: 18852272 [PubMed - as supplied by publisher]                </p> 

    <p class="ListParagraph">14.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18826204?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Tucker TJ, Sisko JT, Tynebor RM, Williams TM, Felock PJ, Flynn JA, Lai MT, Liang Y, McGaughey G, Liu M, Miller M, Moyer G, Munshi V, Perlow-Poehnelt R, Prasad S, Reid JC, Sanchez R, Torrent M, Vacca JP, Wan BL, Yan Y.</a></p>

    <p class="plaintext">Discovery of 3-{5-[(6-Amino-1H-pyrazolo[3,4-b]pyridine-3-yl)methoxy]-2-chlorophenoxy}-5-chlorobenzonitrile (MK-4965): A Potent, Orally Bioavailable HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitor with Improved Potency against Key Mutant Viruses.     </p>

    <p class="plaintext">J Med Chem. 2008 Oct 1. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18826204 [PubMed - as supplied by publisher]    </p>   

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/18817633?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Du L, Zhao YX, Yang LM, Zheng YT, Tang Y, Shen X, Jiang HL.</a></p>

    <p class="plaintext">Symmetrical 1-pyrrolidineacetamide showing anti-HIV activity through a new binding site on HIV-1 integrase.</p>

    <p class="plaintext">Acta Pharmacol Sin. 2008 Oct;29(10):1261-7.                </p>

    <p class="plaintext">PMID: 18817633 [PubMed - in process]                        </p>

    <h2>(HIV or AIDS) and (Therapeutic or Compound)</h2>

    <p class="ListParagraph">15.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18834110?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Maga G, Falchi F, Garbelli A, Belfiore A, Witvrouw M, Manetti F, Botta M.</a></p>

    <p class="plaintext">Pharmacophore Modeling and Molecular Docking Led to the Discovery of Inhibitors of Human Immunodeficiency Virus-1 Replication Targeting the Human Cellular Aspartic Acid-Glutamic Acid-Alanine-Aspartic Acid Box Polypeptide 3.              </p>

    <p class="plaintext">J Med Chem. 2008 Oct 4. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18834110 [PubMed - as supplied by publisher]                </p> 

    <p class="ListParagraph">16.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18830166?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Sechi M, Azzena U, Delussu MP, Dallocchio R, Dessì A, Cosseddu A, Pala N, Neamati N.</a></p>

    <p class="plaintext">Design and Synthesis of Bis-amide and Hydrazide-containing Derivatives of Malonic Acid as Potential HIV-1 Integrase Inhibitors.</p>

    <p class="plaintext">Molecules. 2008 Oct 1;13(10):2442-61.              </p>

    <p class="plaintext">PMID: 18830166 [PubMed - in process]            </p> 

    <p class="ListParagraph">17.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18830165?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Liu Y, Zhou E, Yu K, Zhu J, Zhang Y, Xie X, Li J, Jiang H.</a></p>

    <p class="plaintext">Discovery of a Novel CCR5 Antagonist Lead Compound Through Fragment Assembly.</p>

    <p class="plaintext">Molecules. 2008 Oct 1;13(10):2426-41.</p>

    <p class="plaintext">PMID: 18830165 [PubMed - in process]            </p>

    <h2>(HIV or AIDS) and Drug and Novel</h2>

    <p class="ListParagraph">18.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18831589?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Wendeler M, Lee HF, Bermingham A, Miller JT, Chertov O, Bona MK, Baichoo NS, Ehteshami M, Beutler J, O&#39;Keefe BR, Go&#776;tte M, Kvaratskhelia M, Le Grice S.</a></p>

    <p class="plaintext">Vinylogous Ureas as a Novel Class of Inhibitors of Reverse Transcriptase-Associated Ribonuclease H Activity.</p>

    <p class="plaintext">ACS Chem Biol. 2008 Oct 3. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18831589 [PubMed - as supplied by publisher]    </p>

    <h2>(HIV or AIDS) and Drug</h2>

    <p class="ListParagraph">19.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18653459?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Waheed AA, Ablan SD, Soheilian F, Nagashima K, Ono A, Schaffner CP, Freed EO.</a></p>

    <p class="plaintext">Inhibition of human immunodeficiency virus type 1 assembly and release by the cholesterol-binding compound amphotericin B methyl ester: evidence for Vpu dependence.</p>

    <p class="plaintext">J Virol. 2008 Oct;82(19):9776-81. Epub 2008 Jul 23.</p>

    <p class="plaintext">PMID: 18653459 [PubMed - indexed for MEDLINE]    </p>

    <h2>(HIV or AIDS) and Inhibition</h2>

    <p class="ListParagraph">20.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18806783?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Nathans R, Cao H, Sharova N, Ali A, Sharkey M, Stranska R, Stevenson M, Rana TM.</a></p>

    <p class="plaintext">Small-molecule inhibition of HIV-1 Vif.</p>

    <p class="plaintext">Nat Biotechnol. 2008 Oct;26(10):1187-92. Epub 2008 Sep 21.</p>

    <p class="plaintext">PMID: 18806783 [PubMed - in process]            </p>

    <h2>(HIV or AIDS) and Inhibits</h2>

    <p class="ListParagraph">21.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18774711?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Melamed JY, Egbertson MS, Varga S, Vacca JP, Moyer G, Gabryelski L, Felock PJ, Stillmock KA, Witmer MV, Schleif W, Hazuda DJ, Leonard Y, Jin L, Ellis JD, Young SD.</a></p>

    <p class="plaintext">Synthesis of 5-(1-H or 1-alkyl-5-oxopyrrolidin-3-yl)-8-hydroxy-[1,6]-naphthyridine-7-carboxamide inhibitors of HIV-1 integrase.</p>

    <p class="plaintext">Bioorg Med Chem Lett. 2008 Oct 1;18(19):5307-10. Epub 2008 Aug 14.</p>

    <p class="plaintext">PMID: 18774711 [PubMed - in process]            </p>

    <h2>(HIV or AIDS) and Inhibitor</h2>

    <p class="ListParagraph">22.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18850604?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">De Zotti M, Formaggio F, Kaptein B, Broxterman QB, Felock PJ, Hazuda DJ, Singh SB, Brückner H, Toniolo C.</a></p>

    <p class="plaintext">Complete Absolute Configuration of Integramide A, a Natural, 16-mer Peptide Inhibitor of HIV-1 Integrase, Elucidated by Total Synthesis.</p>

    <p class="plaintext">Chembiochem. 2008 Oct 10. [Epub ahead of print] No abstract available.</p>

    <p class="plaintext">PMID: 18850604 [PubMed - as supplied by publisher]                </p>

    <h2>(HIV or AIDS) and Class</h2>

    <p class="ListParagraph">23.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18826203?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Jessen HJ, Balzarini J, Meier C.</a></p>

    <p class="plaintext">Intracellular Trapping of cycloSal-Pronucleotides: Modification of Prodrugs with Amino Acid Esters.</p>

    <p class="plaintext">J Med Chem. 2008 Oct 1. [Epub ahead of print]</p>

    <p class="plaintext">PMID: 18826203 [PubMed - as supplied by publisher]                </p>

    <h2>Journal Check</h2>

    <p class="ListParagraph">24.               <a href="http://www.ncbi.nlm.nih.gov/pubmed/18783203?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Ghosh AK, Gemma S, Baldridge A, Wang YF, Kovalevsky AY, Koh Y, Weber IT, Mitsuya H.</a></p>

    <p class="plaintext">Flexible cyclic ethers/polyethers as novel P2-ligands for HIV-1 protease inhibitors: design, synthesis, biological evaluation, and protein-ligand X-ray studies.</p>

    <p class="plaintext">J Med Chem. 2008 Oct 9;51(19):6021-33. Epub 2008 Sep 11.</p>

    <p class="plaintext">PMID: 18783203 [PubMed - in process]            </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
