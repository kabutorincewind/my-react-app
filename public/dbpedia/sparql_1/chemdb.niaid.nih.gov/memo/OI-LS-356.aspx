

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-356.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MQuyqeQjvSKRPPo3fplCMbop2woLSE51TdXrkkkVE3wCEUJ3pxUqsvrWliPdnJWC9yCDrr2BaO2D/NjZGw1zLwbF1tgSibsFDEbAFITA+gpbp5jTPlfcET4lrWE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D991E1D5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-356-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58871   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Synthesis, tuberculosis inhibitory activity, and SAR study of N-substituted-phenyl-1,2,3-triazole derivatives</p>

    <p>          Costa, MS, Boechat, N, Rangel, EA, da, Silva FD, de Souza, AM, Rodrigues, CR, Castro, HC, Junior, IN, Lourenco, MC, Wardell, SM, and Ferreira, VF</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949290&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949290&amp;dopt=abstract</a> </p><br />

    <p>2.     58872   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          The history of tuberculosis</p>

    <p>          Daniel, Thomas M</p>

    <p>          Respiratory Medicine <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WWS-4KSVFY9-1/2/2e28aab732dfb775641606b64c8a0e9f">http://www.sciencedirect.com/science/article/B6WWS-4KSVFY9-1/2/2e28aab732dfb775641606b64c8a0e9f</a> </p><br />

    <p>3.     58873   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Comparison of susceptibility and transcription profile of the new antifungal hassallidin A with caspofungin</p>

    <p>          Neuhof, T, Seibold, M, Thewes, S, Laue, M, Han, CO, Hube, B, and von, Dohren H</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16949033&amp;dopt=abstract</a> </p><br />

    <p>4.     58874   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          In vitro anti-mycobacterial activities of various 2&#39;-deoxyuridine, 2&#39;-arabinouridine and 2&#39;-arabinofluoro-2&#39;-deoxyuridine analogues: synthesis and biological studies</p>

    <p>          Srivastav, NC, Manning, T, Kunimoto, DY, and Kumar, R</p>

    <p>          Med Chem <b>2006</b>.  2(3): 287-93</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948475&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16948475&amp;dopt=abstract</a> </p><br />

    <p>5.     58875   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Synthesis, antimycobacterial and antitumor activities of new (1,1-dioxido-3-oxo-1,2-benzisothiazol-2(3H)-yl)methyl N,N-disubstituted dithiocarbamate/O-alkyldithiocarbonate derivatives</p>

    <p>          Guzel, Ozlen and Salman, Aydin</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4KPP44T-1/2/ce26b71bbfbfeee900b9cb16f8cff1ac">http://www.sciencedirect.com/science/article/B6TF8-4KPP44T-1/2/ce26b71bbfbfeee900b9cb16f8cff1ac</a> </p><br />
    <br clear="all">

    <p>6.     58876   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Antiviral Targets of a Chromene Derivative from Sargassum micracanthum in the Replication of Human Cytomegalovirus</p>

    <p>          Hayashi, K, Mori, J, Saito, H, and Hayashi, T</p>

    <p>          Biol Pharm Bull <b>2006</b>.  29(9): 1843-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946496&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946496&amp;dopt=abstract</a> </p><br />

    <p>7.     58877   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Identification of a diacylglycerol acyltransferase gene involved in accumulation of triacylglycerol in Mycobacterium tuberculosis under stress</p>

    <p>          Sirakova, TD, Dubey, VS, Deb, C, Daniel, J, Korotkova, TA, Abomoelak, B, and Kolattukudy, PE</p>

    <p>          Microbiology <b>2006</b>.  152(Pt 9): 2717-25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946266&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16946266&amp;dopt=abstract</a> </p><br />

    <p>8.     58878   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Antiviral interactions of an HCV polymerase inhibitor with an HCV protease inhibitor or interferon in vitro</p>

    <p>          Koev, G, Dekhtyar, T, Han, L, Yan, P, Ng, TI, Lin, CT, Mo, H, and Molla, A</p>

    <p>          Antiviral Res <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945431&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945431&amp;dopt=abstract</a> </p><br />

    <p>9.     58879   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Synthesis of pyrrolo[2,3-d]pyrimidine nucleoside derivatives as potential anti-HCV agents</p>

    <p>          Varaprasad, CV, Ramasamy, KS, Girardet, JL, Gunic, E, Lai, V, Zhong, W, An, H, and Hong, Z</p>

    <p>          Bioorg Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16945403&amp;dopt=abstract</a> </p><br />

    <p>10.   58880   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p><b>          Interference of Mycobacterium tuberculosis cell division by Rv2719c, a cell wall hydrolase</b> </p>

    <p>          Chauhan, A, Lofton, H, Maloney, E, Moore, J, Fol, M, Madiraju, MV, and Rajagopalan, M</p>

    <p>          Mol Microbiol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16942606&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16942606&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   58881   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Functional and phenotypic changes in monocytes from patients with tuberculosis are reversed with treatment</p>

    <p>          Sanchez, Maria D, Garcia, Yoenis, Montes, Carlos, Paris, Sara C, Rojas, Mauricio, Barrera, Luis F, Arias, Mauricio A, and Garcia, Luis F</p>

    <p>          Microbes and Infection <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VPN-4KD584D-2/2/a975028fb711d01fc4fc54b57653a695">http://www.sciencedirect.com/science/article/B6VPN-4KD584D-2/2/a975028fb711d01fc4fc54b57653a695</a> </p><br />

    <p>12.   58882   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          2,6-hexadecadiynoic acid and 2,6-nonadecadiynoic acid: novel synthesized acetylenic fatty acids as potent antifungal agents</p>

    <p>          Carballeira, NM, Sanabria, D, Cruz, C, Parang, K, Wan, B, and Franzblau, S</p>

    <p>          Lipids <b>2006</b>.  41 (5): 507-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16933795&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16933795&amp;dopt=abstract</a> </p><br />

    <p>13.   58883   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          A double-blind randomized study of aminoglycoside infusion with combined therapy for pulmonary Mycobacterium avium complex disease</p>

    <p>          Kobashi, Yoshihiro, Matsushima, Toshiharu, and Oka, Mikio</p>

    <p>          Respiratory Medicine <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WWS-4K42BN9-1/2/34e245a344204a429d09717f731cb62b">http://www.sciencedirect.com/science/article/B6WWS-4K42BN9-1/2/34e245a344204a429d09717f731cb62b</a> </p><br />

    <p>14.   58884   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          A Mycothiol Synthase Mutant of Mycobacterium tuberculosis Has an Altered Thiol-Disulfide Content and Limited Tolerance to Stress</p>

    <p>          Buchmeier, NA, Newton, GL, and Fahey, RC</p>

    <p>          J Bacteriol <b>2006</b>.  188(17): 6245-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923891&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16923891&amp;dopt=abstract</a> </p><br />

    <p>15.   58885   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Bicyclic Nucleosides as Inhibitors of M. tuberculosis Thymidylate Kinase</p>

    <p>          Van Daele, I,  Munier-Lehmann, H, Hendrickx, PM, Marchal, G, Chavarot, P, Froeyen, M, Qing, L, Martins, JC, and Van Calenbergh, S</p>

    <p>          ChemMedChem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16921580&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16921580&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   58886   OI-LS-356; PUBMED-OI-9/5/2006</p>

    <p class="memofmt1-2">          Use of a codon alteration strategy in a novel approach to cloning the Mycobacterium tuberculosis diaminopimelic acid epimerase</p>

    <p>          Usha, V, Dover, LG, Roper, DL, Lloyd, AJ, and Besra, GS</p>

    <p>          FEMS Microbiol Lett <b>2006</b>.  262(1): 39-47</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16907737&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16907737&amp;dopt=abstract</a> </p><br />

    <p>17.   58887   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          New drug candidates and therapeutic targets for tuberculosis therapy</p>

    <p>          Zhang, Ying, Post-Martens, Katrin, and Denkin, Steven</p>

    <p>          Drug Discovery Today <b>2006</b>.  11(1-2): 21-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T64-4J853YK-4/2/2d71e2eb6a692bf973e2fcb53713a1cd">http://www.sciencedirect.com/science/article/B6T64-4J853YK-4/2/2d71e2eb6a692bf973e2fcb53713a1cd</a> </p><br />

    <p>18.   58888   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Origin and Development of RUTI, a New Therapeutic Vaccine Against Mycobacterium tuberculosis Infection</p>

    <p>          Cardona, PJ and Amat, I</p>

    <p>          Archivos de Bronconeumologia <b>2006</b>.  42(1): 25-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B8CWC-4KR39BH-6/2/03a15d57785a4ec6e28b748d8d0dde00">http://www.sciencedirect.com/science/article/B8CWC-4KR39BH-6/2/03a15d57785a4ec6e28b748d8d0dde00</a> </p><br />

    <p>19.   58889   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Hydrazones of 2-aryl-quinoline-4-carboxylic acid hydrazides: Synthesis and preliminary evaluation as antimicrobial agents</p>

    <p>          Metwally, Kamel A, Abdel-Aziz, Lobna M, Lashine, El-Sayed M, Husseiny, Mohamed I, and Badawy, Rania H</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4KST3FD-4/2/389b17dadb639016764cfcbc6340032e">http://www.sciencedirect.com/science/article/B6TF8-4KST3FD-4/2/389b17dadb639016764cfcbc6340032e</a> </p><br />

    <p>20.   58890   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Antimicrobial activities of cinnamyl rifamycin derivatives, T-9 and T-11, against Mycobacterium tuberculosis and Mycobacterium avium complex (MAC) with special reference to the activities against intracellular MAC</p>

    <p>          Sato, K, Shimizu, T, Dimova, V, and Tomioka, H</p>

    <p>          MICROBIOLOGY AND IMMUNOLOGY <b>2006</b>.  50(8): 621-623, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239522400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239522400007</a> </p><br />

    <p>21.   58891   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Relationships between hepatitis C virus replication and CXCL-8 production in vitro</p>

    <p>          Koo, BCA, McPoland, P, Wagoner, JP, Kane, OJ, Lohmann, V, and Polyak, SJ</p>

    <p>          JOURNAL OF VIROLOGY <b>2006</b>.  80(16): 7885-7893, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239557700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239557700010</a> </p><br />
    <br clear="all">

    <p>22.   58892   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Biologically active polymers: VII. Synthesis and antimicrobial activity of some crosslinked copolymers with quaternary ammonium and phosphonium groups</p>

    <p>          Kenawy, El-Refaie, Abdel-Hay, FImam, El-Magd, Ahmed Abou, and Mahmoud, Yehia</p>

    <p>          Reactive and Functional Polymers <b>2006</b>.  66(4): 419-429</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TY0-4HKCYXH-1/2/918bcb61e7a3a51882fe3464b77e69b0">http://www.sciencedirect.com/science/article/B6TY0-4HKCYXH-1/2/918bcb61e7a3a51882fe3464b77e69b0</a> </p><br />

    <p>23.   58893   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          3-Farnesyl-2-hydroxybenzoic acid is a new anti-Helicobacter pylori compound from Piper multiplinervium</p>

    <p>          Ruegg, T, Calderon, AI, Queiroz, EF, Solis, PN, Marston, A, Rivas, F, Ortega-Barria, E, Hostettmann, K, and Gupta, MP</p>

    <p>          Journal of Ethnopharmacology <b>2006</b>.  103(3): 461-467</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4HG6FVG-1/2/05c384116fb493161785d5477e4c7912">http://www.sciencedirect.com/science/article/B6T8D-4HG6FVG-1/2/05c384116fb493161785d5477e4c7912</a> </p><br />

    <p>24.   58894   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Debio-025 is a promising new drug for the treatment of chronic hepatitis C</p>

    <p>          Inoue, K, Watanabe, T, Yoshiba, M, Dumont, JM, Scalfaro, P, and Kohara, M</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2006</b>.  44: S153-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100406">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100406</a> </p><br />

    <p>25.   58895   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Subgenomic hepatitis c replicon deriving from a clinical isolate with virologic non-response to IFN-based therapy and IFN resistance in vitro</p>

    <p>          Schonberger, B, Appel, N, Lohmann, V, Goebel, R, Bartenschlager, R, Zeuzem, S, and Sarrazin, C </p>

    <p>          JOURNAL OF HEPATOLOGY <b>2006</b>.  44: S157-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100417">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237328100417</a> </p><br />

    <p>26.   58896   OI-LS-356; EMBASE-OI-9/5/2006</p>

    <p class="memofmt1-2">          Antiviral drugs for cytomegalovirus diseases: Special Issue To Honour Professor Erik De Clercq</p>

    <p>          Biron, Karen K </p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 154-163</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-2/2/ce0b69aeadb891e3e0ca293086c8a5d7">http://www.sciencedirect.com/science/article/B6T2H-4K18TKC-2/2/ce0b69aeadb891e3e0ca293086c8a5d7</a> </p><br />

    <p>27.   58897   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Changing patterns in the risk for cytomegalovirus infection and disease and treatment-related outcomes in the era of preemptive antiviral therapy</p>

    <p>          Boeckh, M, Kirby, K, Norasetthada, L, Sandmaier, B, Maloney, D, Maris, M, Storb, R, and Corey, L</p>

    <p>          INTERNATIONAL JOURNAL OF INFECTIOUS DISEASES <b>2006</b>.  10: S31-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239428800056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239428800056</a> </p><br />
    <br clear="all">

    <p>28.   58898   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Synthesis of N-3(4-(4-chlorophenyl thiazole-2-yl)-(2-(amino)methyl)-quinzoline-4(3H)-one and their derivatives for antitubercular</p>

    <p>          Pattan, SR, Reddy, VVK, Manvi, FV, Desai, BG, and Bhat, AR</p>

    <p>          INDIAN JOURNAL OF CHEMISTRY SECTION B-ORGANIC CHEMISTRY INCLUDING MEDICINAL CHEMISTRY <b>2006</b>.  45 (7): 1778-1781, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239475200020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239475200020</a> </p><br />

    <p>29.   58899   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Synthesis of 2 &#39;-C-methylcytidine and 2 &#39;-C-methyluridine derivatives modified in the 3 &#39;-position as potential antiviral agents</p>

    <p>          Pierra, C, Amador, A, Badaroux, E, Storer, R, and Gosselin, G</p>

    <p>          COLLECTION OF CZECHOSLOVAK CHEMICAL COMMUNICATIONS <b>2006</b>.  71(7): 991-1010, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239593900006</a> </p><br />

    <p>30.   58900   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Tuberculosis chemotherapy: the influence of bacillary stress and damage response pathways on drug efficacy</p>

    <p>          Warner, DF and Mizrahi, V</p>

    <p>          CLINICAL MICROBIOLOGY REVIEWS <b>2006</b>.  19(3): 558-+, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239419600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239419600006</a> </p><br />

    <p>31.   58901   OI-LS-356; WOS-OI-8/27/2006</p>

    <p class="memofmt1-2">          Antibacterial, antifungal and anticonvulsant evaluation of novel newly synthesized 1-[2-(1H-tetrazol-5-yl)eth l]-1H-benzo[d][1,2,3]triazoles</p>

    <p>          Rajasekaran, A, Murugesan, S, and AnandaRajagopal, K</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2006</b>.  29(7): 535-540, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239411500001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239411500001</a> </p><br />

    <p>32.   58902   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          In vitro selected Con1 subgenomic replicons resistant to 2 &#39;-C-methyl-cytidine or to R1479 show lack of cross resistance</p>

    <p>          Le Pogam, S, Jiang, WR, Leveque, V, Rajyaguru, S, Ma, H, Kang, H, Jiang, S, Singer, M , Ali, S, Klumpp, K, Smith, D, Symons, J, Cammack, N, and Najera, I</p>

    <p>          VIROLOGY <b>2006</b>.  351(2): 349-359, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239649500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239649500010</a> </p><br />

    <p>33.   58903   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of new 2-hydroxy-N-(3-oxo-1-thia-4-azaspiro[4.4]non-4- l)/(3-oxo-1-thia-4-azaspiro[4.5]dec-4-yl)-2,2-d phenylacetamide derivatives</p>

    <p>          Guzel, O, Ithan, E, and Salman, A</p>

    <p>          MONATSHEFTE FUR CHEMIE <b>2006</b>.  137(6): 795-801, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239791700011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239791700011</a> </p><br />
    <br clear="all">

    <p>34.   58904   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Utilization of ribozymes as antiviral agents</p>

    <p>          Benitez-Hess, ML and Alvarez-Salas, LM</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(6): 390-404, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400005</a> </p><br />

    <p>35.   58905   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Antitubercular properties of substituted hydroxycyclohexadienones</p>

    <p>          Shah, M, Wells, G, Bradshaw, TD, Laughton, CA, Stevens, MFG, and Westwell, AD</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(6): 419-423, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400008</a> </p><br />

    <p>36.   58906   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-tubercular activity of a series of N &#39;-(disubstitutedbenzoyl)isoniazid derivatives</p>

    <p>          Junior, IN, Lourenco, MCS, de, Mirandal GDB, Vasconcelos, TRA, Pais, KC, Junior, JDD, Wardell, SMSV, Wardell, JL, and de, Souza MVN</p>

    <p>          LETTERS IN DRUG DESIGN &amp; DISCOVERY <b>2006</b>.  3(6): 424-428, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239768400009</a> </p><br />

    <p>37.   58907   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Evaluation of a modified antimycobacterial susceptibility test using Middlebrook 7H10 agar containing 2,3-diphenyl-5-thienyl-(2)tetrazolium chloride</p>

    <p>          Lee, S, Kong, DH, Yun, SH, Lee, KR, Lee, KP, Franzblau, SG, Lee, EY, and Chang, CL</p>

    <p>          JOURNAL OF MICROBIOLOGICAL METHODS <b>2006</b>.  66(3): 548-551, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239671600018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239671600018</a> </p><br />

    <p>38.   58908   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Antifungal activity of some coumarins obtained from species of Pterocaulon (Asteraceae)</p>

    <p>          Stein, AC, Alvarez, S, Avancini, C, Zacchino, S, and von, Poser G</p>

    <p>          JOURNAL OF ETHNOPHARMACOLOGY <b>2006</b>.  107(1): 95-98, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239676000016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239676000016</a> </p><br />

    <p>39.   58909   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Synthesis and antifungal activity of new thienyl and aryl conazoles</p>

    <p>          Chevreuil, F, Landreau, A, Seraphin, D, Larcher, G, Bouchara, JP, and Richomme, P</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2006</b>.  21(3): 293-303, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239088900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239088900007</a> </p><br />

    <p>40.   58910   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Identification of an antimicrobial entity from the cyanobacterium Fischerella sp isolated from bark of Azadirachta indica (Neem) tree</p>

    <p>          Asthana, RK, Srivastava, A, Singh, AP, Deepali, Singh, SP, Nath, G, Srivastava, R, and Srivastava, BS</p>

    <p>          JOURNAL OF APPLIED PHYCOLOGY <b>2006</b>.  18(1): 33-39, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239590800005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239590800005</a> </p><br />
    <br clear="all">

    <p>41.   58911   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Antimicrobial activity of coupled hydroxyanthracenones isolated from plants of the genus Karwinskia</p>

    <p>          Salazar, R, Rivas, V, Gonzalez, G, and Waksman, N</p>

    <p>          FITOTERAPIA <b>2006</b>.  77(5): 398-400, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239668200013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239668200013</a> </p><br />

    <p>42.   58912   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Toxoplasma gondii scavenges host-derived lipoic acid despite its de novo synthesis in the apicoplast</p>

    <p>          Crawford, MJ, Thomsen-Zieger, N, Ray, M, Schachtner, J, Roos, DS, and Seeber, F</p>

    <p>          EMBO JOURNAL <b>2006</b>.  25(13): 3214-3222, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239625800024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239625800024</a> </p><br />

    <p>43.   58913   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Anti-RNA virus activity of polyoxometalates</p>

    <p>          Shigeta, S, Mori, S, Yamase, T, Yamamoto, N, and Yamamoto, N</p>

    <p>          BIOMEDICINE &amp; PHARMACOTHERAPY <b>2006</b>.  60(5): 211-219, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239697600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239697600001</a> </p><br />

    <p>44.   58914   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Population genetics study of isoniazid resistance mutations and evolution of multidrug-resistant Mycobactetium tuberculosis</p>

    <p>          Hazbon, MH, Brimacombe, M, del, Valle MB, Cavatore, M, Guerrero, MI, Varma-Basil, M, Billman-Jacobe, H, Lavender, C, Fyfe, J, Garcia-Garcia, L, Leon, CI, Bose, M, Chaves, F, Murray, M, Eisenach, KD, Sifuentes-Osornio, J, Cave, MD, de, Leon AP, and Alland, D</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(8): 2640-2649, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400007</a> </p><br />

    <p>45.   58915   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Differential gene expression in response to exposure to antimycobacterial agents and other stress conditions among seven Mycobacterium tuberculosis whiB-like genes</p>

    <p>          Geiman, DE, Raghunand, TR, Agarwal, N, and Bishai, WR</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(8): 2836-2841, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400035">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400035</a> </p><br />

    <p>46.   58916   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Genetic basis for natural and acquired resistance to the diarylquinoline R207910 in mycobacteria</p>

    <p>          Petrella, S, Cambau, E, Chauffour, A, Andries, K, Jarlier, V, and Sougakoff, W</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(8): 2853-2856, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400039">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400039</a> </p><br />

    <p>47.   58917   OI-LS-356; WOS-OI-9/3/2006</p>

    <p class="memofmt1-2">          Resistance levels and rpoB gene mutations among in vitro-selected rifampin-resistant Mycobacterium tuberculosis mutants</p>

    <p>          Huitric, E, Werngren, J, Jureen, P, and Hoffner, S</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(8): 2860-2862, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239640400041</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
