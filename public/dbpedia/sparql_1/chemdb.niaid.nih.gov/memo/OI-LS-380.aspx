

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-380.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jjPM1QFWBTlNdrWy6EXeIdI6Ip+CMHRCqG8cXj+kahi7dQBKy+9fu52t4zubHJWio8cCfk2trkdPGMyS/p2P4RFlehEVaTlauMjA1nJSdCIFNxDfF2B/JIlvhuE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="87C3769B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-380-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61425   OI-LS-380; WOS-OI-7/27/2007</p>

    <p class="memofmt1-2">          Diarylquinolines Target Subunit C of Mycobacterial Atp Synthase</p>

    <p>          Koul, A. <i>et al.</i></p>

    <p>          Nature Chemical Biology <b>2007</b>.  3(6): 323-324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246816400010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246816400010</a> </p><br />

    <p>2.     61426   OI-LS-380; EMBASE-OI-8/6/2007</p>

    <p class="memofmt1-2">          Chemotherapeutic evaluation of alginate nanoparticle-encapsulated azole antifungal and antitubercular drugs against murine tuberculosis</p>

    <p>          Ahmad, Zahoor, Sharma, Sadhna, and Khuller, Gopal K</p>

    <p>          Nanomedicine: Nanotechnology, Biology and Medicine <b>2007</b>.  In Press, Corrected Proof: 1116</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7MDB-4P8996C-1/2/5b82f488b44cea388d10947feba30589">http://www.sciencedirect.com/science/article/B7MDB-4P8996C-1/2/5b82f488b44cea388d10947feba30589</a> </p><br />

    <p>3.     61427   OI-LS-380; WOS-OI-7/27/2007</p>

    <p class="memofmt1-2">          Drug Resistance in Tuberculosis</p>

    <p>          Ebrahim, G.</p>

    <p>          Journal of Tropical Pediatrics <b>2007</b>.  53(3): 147-149</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247426200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247426200001</a> </p><br />

    <p>4.     61428   OI-LS-380; WOS-OI-7/27/2007</p>

    <p class="memofmt1-2">          Antimicrobial and Cytotoxic Activity of Agelasine and Agelasimine Analogs</p>

    <p>          Vik, A. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2007</b>.  15(12): 4016-4037</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246870400007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246870400007</a> </p><br />

    <p>5.     61429   OI-LS-380; WOS-OI-7/27/2007</p>

    <p class="memofmt1-2">          The Oriented Development of Antituberculotics (Part Ii): Halogenated 3-(4-Alkylphenyl)-1,3-Benzoxazine-2,4-(3h)-Diones</p>

    <p>          Waisser, K. <i>et al.</i></p>

    <p>          Archiv Der Pharmazie <b>2007</b>.  340(5): 264-267</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246835600006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246835600006</a> </p><br />

    <p>6.     61430   OI-LS-380; EMBASE-OI-8/6/2007</p>

    <p class="memofmt1-2">          The methylerythritol phosphate pathway for isoprenoid biosynthesis in coccidia: Presence and sensitivity to fosmidomycin</p>

    <p>          Clastre, Marc, Goubard, Armelle, Prel, Anne, Mincheva, Zoia, Viaud-Massuart, Marie-Claude, Bout, Daniel, Rideau, Marc, Velge-Roussel, Florence, and Laurent, Fabrice</p>

    <p>          Experimental Parasitology <b>2007</b>.  116(4): 375-384</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFH-4N3P08F-1/2/9598fa67ed1fc9c0bc82150b642a3e4a">http://www.sciencedirect.com/science/article/B6WFH-4N3P08F-1/2/9598fa67ed1fc9c0bc82150b642a3e4a</a> </p><br />
    <br clear="all">

    <p>7.     61431   OI-LS-380; EMBASE-OI-8/6/2007</p>

    <p class="memofmt1-2">          Broad-spectrum antimicrobial activity of the reactive compounds generated in vitro by Manduca sexta phenoloxidase</p>

    <p>          Zhao, Picheng, Li, Jiajing, Wang, Yang, and Jiang, Haobo</p>

    <p>          Insect Biochemistry and Molecular Biology <b>2007</b>.  37(9): 952-959</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T79-4NSWYY5-2/2/208e8ea01190e75829e0209eb2487333">http://www.sciencedirect.com/science/article/B6T79-4NSWYY5-2/2/208e8ea01190e75829e0209eb2487333</a> </p><br />

    <p>8.     61432   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Antimycobacterial Benzofuro[3,2-F]Chromenes From a 5-Bromochromen-6-Ol</p>

    <p>          Prado, S. <i>et al.</i></p>

    <p>          Synthesis-Stuttgart <b>2007</b>.(10): 1566-1570</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247095700023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247095700023</a> </p><br />

    <p>9.     61433   OI-LS-380; EMBASE-OI-8/6/2007</p>

    <p class="memofmt1-2">          Synthesis, crystal structure and antimicrobial activity of deoxybenzoin derivatives from genistein</p>

    <p>          Li, Huan-Qiu, Xue, Jia-Yu, Shi, Lei, Gui, Shan-Ying, and Zhu, Hai-Liang</p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  In Press, Corrected Proof: 153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4NXHC50-1/2/a6ab434bc4e471781b4e4e5a2469a314">http://www.sciencedirect.com/science/article/B6VKY-4NXHC50-1/2/a6ab434bc4e471781b4e4e5a2469a314</a> </p><br />

    <p>10.   61434   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          The Structure of the Regulatory Domain of the Adenylyl Cyclase Rv1264 From Mycobacterium Tuberculosis With Bound Oleic Acid</p>

    <p>          Findeisen, F. <i> et al.</i></p>

    <p>          Journal of Molecular Biology <b>2007</b>.  369(5): 1282-1295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247096100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247096100012</a> </p><br />

    <p>11.   61435   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Advanced Applications of Counter-Current Chromatography in the Isolation of Anti-Tuberculosis Constituents From Dracaena Angustifolia</p>

    <p>          Case, R. <i>et al.</i></p>

    <p>          Journal of Chromatography a <b>2007</b>.  1151(1-2): 169-174</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247049400028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247049400028</a> </p><br />

    <p>12.   61436   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Identification of Host Genes Involved in Hepatitis C Virus Replication by Small Interfering Rna Technology</p>

    <p>          Ng, T. <i>et al.</i></p>

    <p>          Hepatology <b>2007</b>.  45(6): 1413-1421</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247011500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247011500013</a> </p><br />

    <p>13.   61437   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Gsmn-Tb: a Web-Based Genome Scale Network Model of Mycobacterium Tuberculosis Metabolism</p>

    <p>          Beste, D. <i>et al.</i></p>

    <p>          Genome Biology  <b>2007</b>.  8(5)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246983100019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246983100019</a> </p><br />

    <p>14.   61438   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Acid-Catalyzed Synthesis of Oxathiolone Fused Chalcones. Comparison of Their Activity Toward Various Microorganisms and Human Cancer Cells Line</p>

    <p>          Konieczny, M. <i> et al.</i></p>

    <p>          European Journal of Medicinal Chemistry <b>2007</b>.  42(5): 729-733</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247107400019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247107400019</a> </p><br />

    <p>15.   61439   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Synthesis of Isomeric, Oxathiolone Fused Chalcones, and Comparison of Their Activity Toward Various Microorganisms and Human Cancer Cells Line</p>

    <p>          Konieczny, M. <i> et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2007</b>.  55(5): 817-820</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247078000022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247078000022</a> </p><br />

    <p>16.   61440   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          The Gene Expression Data of Mycobacterium Tuberculosis Based on Affymetrix Gene Chips Provide Insight Into Regulatory and Hypothetical Genes</p>

    <p>          Fu, L. and Fu-Liu, C.</p>

    <p>          Bmc Microbiology <b>2007</b>.  7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247016700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247016700001</a> </p><br />

    <p>17.   61441   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Synthesis and Antitubercular Activity of Phenothiazines With Reduced Binding to Dopamine and Serotonin Receptors</p>

    <p>          Madrid, P. <i>et al.</i></p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(11): 3014-3017</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247052900012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247052900012</a> </p><br />

    <p>18.   61442   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of Some 4-Substituted Quinoxalinones</p>

    <p>          Ingale, S. <i>et al.</i></p>

    <p>          Asian Journal of Chemistry <b>2007</b>.  19(5): 3797-3802</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246955400076">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246955400076</a> </p><br />

    <p>19.   61443   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Serum Differentially Alters the Antifungal Properties of Echinocandin Drugs</p>

    <p>          Paderu, P. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(6): 2253-2256</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246991400062">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000246991400062</a> </p><br />

    <p>20.   61444   OI-LS-380; WOS-OI-8/3/2007</p>

    <p class="memofmt1-2">          Complementary and Alternative Medicine (Cam) for the Treatment of Chronic Hepatitis B and C: a Review</p>

    <p>          Modi, A., Wright, E., and Seeff, L.</p>

    <p>          Antiviral Therapy <b>2007</b>.  12(3): 285-295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247111600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247111600001</a> </p><br />

    <p>21.   61445   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          Synthesis of novel substituted pyrazolyl-2-toluidinomethanethione and pyrazolyl-2-methoxyanilinomethanethione as potential antitubercular agents</p>

    <p>          Ali, MA and Yar, MS</p>

    <p>          Acta Pol Pharm <b>2007</b>.  64(2): 139-146</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17665863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17665863&amp;dopt=abstract</a> </p><br />

    <p>22.   61446   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          Synthesis and antifungal properties of alpha-methoxy and alpha-hydroxyl substituted 4-thiatetradecanoic acids</p>

    <p>          Carballeira, NM, O&#39;neill, R, and Parang, K</p>

    <p>          Chem Phys Lipids <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17662704&amp;dopt=abstract</a> </p><br />

    <p>23.   61447   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          Gomesin, a peptide produced by the spider Acanthoscurria gomesiana, is a potent anticryptococcal agent that acts in synergism with fluconazole</p>

    <p>          Barbosa, FM, Daffre, S, Maldonado, RA, Miranda, A, Nimrichter, L, and Rodrigues, ML</p>

    <p>          FEMS Microbiol Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17645524&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17645524&amp;dopt=abstract</a> </p><br />

    <p>24.   61448   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          Activity of ketoconazole against Mycobacterium tuberculosis in vitro and in the mouse model</p>

    <p>          Byrne, ST, Denkin, SM, Gu, P, Nuermberger, E, and Zhang, Y</p>

    <p>          J Med Microbiol <b>2007</b>.  56(Pt 8): 1047-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17644711&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17644711&amp;dopt=abstract</a> </p><br />

    <p>25.   61449   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          Recent progress in the development of inhibitors of the hepatitis C virus RNA-dependent RNA polymerase</p>

    <p>          Koch, U and Narjes, F</p>

    <p>          Curr Top Med Chem <b>2007</b>.  7(13): 1302-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627559&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17627559&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>26.   61450   OI-LS-380; PUBMED-OI-8/6/2007</p>

    <p class="memofmt1-2">          From the Cover: Structural basis for the specific inhibition of protein kinase G, a virulence factor of Mycobacterium tuberculosis</p>

    <p>          Scherr, N, Honnappa, S, Kunz, G, Mueller, P, Jayachandran, R, Winkler, F, Pieters, J, and Steinmetz, MO</p>

    <p>          Proc Natl Acad Sci U S A <b>2007</b>.  104(29): 12151-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17616581&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17616581&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
