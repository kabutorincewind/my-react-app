

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-07-07.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ytCGINLt5sbRq9fsEzI18NED6HpENdn26GAqZuhUJMAJDIHeqVsUM7EZGpO9wQOd54gyKvk78zB8PpAgOEcFGK1gbnbQo6UwjBtJEDQmq3aB7tvlRt6LYtFOXfo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B4A65255" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">June  24, 2009 - July 7, 2009</p>

    <p class="memofmt2-2">Opportunistic Fungi (Cryptococcus, Aspergillus, Candida, Histoplasma)</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19528168?ordinalpos=44&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antifungal activity of thymol against clinical isolates of fluconazole-sensitive and -resistant Candida albicans.</a></p>

    <p class="NoSpacing">Guo N, Liu J, Wu X, Bi X, Meng R, Wang X, Xiang H, Deng X, Yu L.</p>

    <p class="NoSpacing">J Med Microbiol. 2009 Aug;58(Pt 8):1074-9. Epub 2009 Jun 15.</p>

    <p class="NoSpacing">PMID: 19528168 [PubMed - in process]</p> 

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19583202?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Edaxadiene: A New Bioactive Diterpene from Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Mann FM, Xu M, Chen X, Fulton DB, Russell DG, Peters RJ.</p>

    <p class="NoSpacing">J Am Chem Soc. 2009 Jul 7. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19583202 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19564358?ordinalpos=102&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Tuberculosis and Trimethoprim-Sulfamethoxazole.</a></p>

    <p class="NoSpacing">Forgacs P, Wengenack NL, Hall L, Zimmerman SK, Silverman ML, Roberts GD.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Jun 29. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19564358 [PubMed - as supplied by publisher]</p> 

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19481935?ordinalpos=227&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Optimization of the central linker of dicationic bis-benzimidazole anti-MRSA and anti-VRE agents.</a></p>

    <p class="NoSpacing">Hu L, Kully ML, Boykin DW, Abood N.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jul 1;19(13):3374-7. Epub 2009 May 20.</p>

    <p class="NoSpacing">PMID: 19481935 [PubMed - in process]</p> 

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19467603?ordinalpos=238&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Antimycobacterial activity of new 3,5-disubstituted 1,3,4-oxadiazol-2(3H)-one derivatives. Molecular modeling investigations.</a></p>

    <p class="NoSpacing">Zampieri D, Mamolo MG, Laurini E, Fermeglia M, Posocco P, Pricl S, Banfi E, Scialino G, Vio L.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jul 1;17(13):4693-707. Epub 2009 May 3.</p>

    <p class="NoSpacing">PMID: 19467603 [PubMed - in process]</p> 

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19457676?ordinalpos=247&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis and biological evaluation of novel triazole, urea and thiourea derivatives of quinoline against Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Upadhayaya RS, Kulkarni GM, Vasireddy NR, Vandavasi JK, Dixit SS, Sharma V, Chattopadhyaya J.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 Jul 1;17(13):4681-92. Epub 2009 May 5.</p>

    <p class="NoSpacing">PMID: 19457676 [PubMed - in process]</p> 

    <p class="ListParagraph">7.<a href="http://www.ncbi.nlm.nih.gov/pubmed/19345718?ordinalpos=325&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Inhibition of 7,8-diaminopelargonic acid aminotransferase from Mycobacterium tuberculosis by chiral and achiral anologs of its substrate: biological implications.</a></p>

    <p class="NoSpacing">Mann S, Colliandre L, Labesse G, Ploux O.</p>

    <p class="NoSpacing">Biochimie. 2009 Jul;91(7):826-34. Epub 2009 Apr 2.</p>

    <p class="NoSpacing">PMID: 19345718 [PubMed - in process]</p>


    <h2>Antimicrobials, Antifungals (General)</h2> 

    <p class="ListParagraph">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19464891?ordinalpos=69&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of novel 3-(1-(1-substituted piperidin-4-yl)-1H-1,2,3-triazol-4-yl)-1,2,4-oxadiazol-5(4H)-one as antifungal agents.</a></p>

    <p class="NoSpacing">Sangshetti JN, Nagawade RR, Shinde DB.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Jul 1;19(13):3564-7. Epub 2009 May 3.</p>

    <p class="NoSpacing">PMID: 19464891 [PubMed - in process]</p> 

    <p class="ListParagraph">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19349095?ordinalpos=105&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis and biological evaluation of novel nitrogen and sulfur containing hetero-1,4-naphthoquinones as potent antifungal and antibacterial agents.</a></p>

    <p class="NoSpacing">Tandon VK, Maurya HK, Mishra NN, Shukla PK.</p>

    <p class="NoSpacing">Eur J Med Chem. 2009 Aug;44(8):3130-7. Epub 2009 Mar 21.</p>

    <p class="NoSpacing">PMID: 19349095 [PubMed - in process]</p>    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
