

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-06-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="R2Macej4pOomDF6p5FIwODNoMtz4k54OR6RvpJIZdYH6kk17SsvlqA+GgxkQsHSff86iWjesqWeM0OX5eqJM1vj9u9/6f2S720AhAj6TNNpdkNlOE19/QkZBpzI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4AEF4BAF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">May 28 - June 10, 2010</p> 

    <p class="memofmt2-2">Hepatitis C Virus</p> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20516278">PSI-7851, a Pronucleotide of {beta}-D-2&#39;-Deoxy-2&#39;-Fluoro-2&#39;-C-Methyluridine Monophosphate: A Potent and Pan-Genotype Inhibitor of Hepatitis C Virus Replication.</a> Lam, A.M., E. Murakami, C. Espiritu, H.M. Micolochick Steuer, C. Niu, M. Keilman, H. Bao, V. Zennou, N. Bourne, J.G. Julander, J.D. Morrey, D.F. Smee, D.N. Frick, J.A. Heck, P. Wang, D. Nagarathnam, B.S. Ross, M.J. Sofia, M.J. Otto, and P.A. Furman. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20516278].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20527890">Phosphoramidate ProTides of 2&#39;-C-Methylguanosine as Highly Potent Inhibitors of Hepatitis C Virus. Study of Their in Vitro and in Vivo Properties.</a> McGuigan, C., A. Gilles, K. Madela, M. Aljarah, S. Holl, S. Jones, J. Vernachio, J. Hutchins, B. Ames, K.D. Bryant, E. Gorovits, B. Ganguly, D. Hunley, A. Hall, A. Kolykhalov, Y. Liu, J. Muhammad, N. Raja, R. Walters, J. Wang, S. Chamberlain, and G. Henson. J Med Chem. <b>[Epub ahead of print]</b>; PMID[20527890].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20534349">The Efficacy of siRNAs against Hepatitis C Virus Is Strongly Influenced by Structure and Target Site Accessibility.</a> Sagan, S.M., N. Nasheri, C. Luebbert, and J.P. Pezacki. Chem Biol. 17(5): p. 515-527; PMID[20534349].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20308381">Preclinical characterization of the antiviral activity of SCH 900518 (Narlaprevir), a novel mechanism-based inhibitor of hepatitis C virus NS3 protease.</a> Tong, X., A. Arasappan, F. Bennett, R. Chase, B. Feld, Z. Guo, A. Hart, V. Madison, B. Malcolm, J. Pichardo, A. Prongay, R. Ralston, A. Skelton, E. Xia, R. Zhang, and F.G. Njoroge. Antimicrob Agents Chemother. 54(6): p. 2365-70; PMID[20308381].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20512985">Multiple effects of silymarin on the hepatitis C virus lifecycle.</a> Wagoner, J., A. Negash, O.J. Kane, L.E. Martinez, Y. Nahmias, N. Bourne, D.M. Owen, J. Grove, C. Brimacombe, J.A. McKeating, E.I. Pecheur, T.N. Graf, N.H. Oberlies, V. Lohmann, F. Cao, J.E. Tavis, and S.J. Polyak. Hepatology. 51(6): p. 1912-21; PMID[20512985].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p />

    <p />

    <p class="memofmt2-2">Hepatitis B Virus</p> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19775597">Bioactive flavones and biflavones from Selaginella moellendorffii Hieron.</a> Cao, Y., N.H. Tan, J.J. Chen, G.Z. Zeng, Y.B. Ma, Y.P. Wu, H. Yan, J. Yang, L.F. Lu, and Q. Wang. Fitoterapia. 81(4): p. 253-8; PMID[19775597].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p> 

    <p class="memofmt2-2">Human Cytomegalovirus</p> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20194528">Novel anticytomegalovirus activity of immunosuppressant mizoribine and its synergism with ganciclovir.</a> Kuramoto, T., T. Daikoku, Y. Yoshida, M. Takemoto, K. Oshima, Y. Eizuru, Y. Kanda, T. Miyawaki, and K. Shiraki. J Pharmacol Exp Ther. 333(3): p. 816-21; PMID[20194528].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p>

    <p> </p>

    <p class="memofmt2-2">HSV-1 Virus</p> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20534624">ASP2151, a novel helicase-primase inhibitor, possesses antiviral activity against varicella-zoster virus and herpes simplex virus types 1 and 2.</a> Chono, K., K. Katsumata, T. Kontani, M. Kobayashi, K. Sudo, T. Yokota, K. Konno, Y. Shimizu, and H. Suzuki. J Antimicrob Chemother. [<b>Epub ahead of print]</b>; PMID[20534624].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0528-061010.</p> 

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p> 

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277720400024">Human Cytomegalovirus Protein pUL117 Targets the Mini-Chromosome Maintenance Complex and Suppresses Cellular DNA Synthesis.</a> Qian, Z.K., V. Leung-Pineda, B.Q. Xuan, H. Piwnica-Worms, and D. Yu. PLOS Pathogens. 6(3); ISI[000277720400024].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0528-061010.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
