

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-367.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mTznmSdDjMM6b+jzFbzmhcnXWJosEWnWivzP8GJr2p1jGj/WKoZs0dgww+YDN9JoGOkqgJOje2yZ3I3ilxWW6TbioGhEl0T4mWddcfEH+BXMkSbG70REMpRPZO8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6AB2DE37" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-367-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60096   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          1,25(OH)2D3 inhibits in vitro and in vivo intracellular growth of apicomplexan parasite Toxoplasma gondii</p>

    <p>          Rajapakse, R, Uring-Lambert, B, Andarawewa, KL, Rajapakse, RP, Abou-Bacar, A, Marcellin, L, and Candolfi, E</p>

    <p>          J Steroid Biochem Mol Biol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270431&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17270431&amp;dopt=abstract</a> </p><br />

    <p>2.     60097   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          Design and synthesis of substrate-mimic inhibitors of mycothiol-S-conjugate amidase from Mycobacterium tuberculosis</p>

    <p>          Metaferia, Belhu B, Ray, Satyajit, Smith, Jeremy A, and Bewley, Carole A</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  17(2): 444-447</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60098   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Highly Efficient Ligands for Dihydrofolate Reductase from Cryptosporidium hominis and Toxoplasma gondii Inspired by Structural Analysis</p>

    <p>          Pelphrey, PM, Popov, VM, Joska, TM, Beierlein, JM, Bolstad, ES, Fillingham, YA, Wright, DL, and Anderson, AC</p>

    <p>          J Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17269758&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17269758&amp;dopt=abstract</a> </p><br />

    <p>4.     60099   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Anidulafungin: a new echinocandin for candidal infections</p>

    <p>          de la Torre, P and Reboli, AC</p>

    <p>          Expert Rev Anti Infect Ther <b>2007</b>.  5(1): 45-52</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266452&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17266452&amp;dopt=abstract</a> </p><br />

    <p>5.     60100   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of oxazolidinone derivatives as antibacterial agents</p>

    <p>          Josyula, Vara Prasad Venkata Nagendra, Boyer, Frederick Earl Jr, and Kim, Ji-Young</p>

    <p>          PATENT:  WO <b>2007000644</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 38pp.</p>

    <p>          ASSIGNEE:  (Pharmacia &amp; Upjohn Company LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     60101   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Differential drug susceptibility of intracellular and extracellular tuberculosis, and the impact of P-glycoprotein</p>

    <p>          Hartkoorn, RC, Chandler, B, Owen, A, Ward, SA, Bertel, Squire S, Back, DJ, and Khoo, SH</p>

    <p>          Tuberculosis (Edinb) <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258938&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17258938&amp;dopt=abstract</a> </p><br />

    <p>7.     60102   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Isoniazid prophylaxis differently modulates T-cell responses to RD1-epitopes in contacts recently exposed to Mycobacterium tuberculosis: a pilot study</p>

    <p>          Goletti, D, Parracino, MP, Butera, O, Bizzoni, F, Casetti, R, Dainotto, D, Anzidei, G, Nisii, C, Ippolito, G, Poccia, F, and Girardi, E</p>

    <p>          Respir Res <b>2007</b>.  8(1): 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257436&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257436&amp;dopt=abstract</a> </p><br />

    <p>8.     60103   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Association of the Rv0679c protein with lipids and carbohydrates in Mycobacterium tuberculosis/Mycobacterium bovis BCG</p>

    <p>          Matsuba, T, Suzuki, Y, and Tanaka, Y</p>

    <p>          Arch Microbiol  <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252234&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17252234&amp;dopt=abstract</a> </p><br />

    <p>9.     60104   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Nanomolar Competitive Inhibitors of Mycobacterium tuberculosis and Streptomyces coelicolor Type II Dehydroquinase</p>

    <p>          Prazeres, VF, Sanchez-Sixto, C, Castedo, L, Lamb, H, Hawkins, AR, Riboldi-Tunnicliffe, A, Coggins, JR, Lapthorn, AJ, and Gonzalez-Bello, C</p>

    <p>          ChemMedChem <b>2007</b>.  2(2): 194-207</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17245805&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17245805&amp;dopt=abstract</a> </p><br />

    <p>10.   60105   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Inhibition of HCV subgenomic replicons by siRNAs derived from plasmids with opposing U6 and H1 promoters</p>

    <p>          Korf, M, Meyer, A, Jarczak, D, Beger, C, Manns, MP, and Kruger, M</p>

    <p>          J Viral Hepat <b>2007</b>.  14(2): 122-32</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17244252&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17244252&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   60106   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Growth inhibition of Toxoplasma gondii and Plasmodium falciparum by Nanomolar Concentrations of HDQ (1-hydroxy-2-dodecyl-4(1H)quinolone): a High Affinity Inhibitor of Alternative (type II) NADH Dehydrogenases</p>

    <p>          Saleh, A, Friesen, J, Baumeister, S, Gross, U, and Bohne, W</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242151&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242151&amp;dopt=abstract</a> </p><br />

    <p>12.   60107   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Antimicrobial activity of long-chain, water-soluble, dendritic tricarboxylato amphiphiles</p>

    <p>          Williams, AA, Sugandhi, EW, Macri, RV, Falkinham, JO 3rd, and Gandour, RD</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242037&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242037&amp;dopt=abstract</a> </p><br />

    <p>13.   60108   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p><b>          On the Action of Methotrexate and 6-Mercaptopurine on M. avium Subspecies paratuberculosis</b> </p>

    <p>          Greenstein Robert J, Su Liya, Haroutunian Vahram, Shahidi Azra, and Brown Sheldon T</p>

    <p>          PLoS ONE <b>2007</b>.  2: e161.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60109   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Contribution of the Rv2333c efflux pump (the Stp protein) from Mycobacterium tuberculosis to intrinsic antibiotic resistance in Mycobacterium bovis BCG</p>

    <p>          Ramon-Garcia, S, Martin, C, De, Rossi E, and Ainsa, JA</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242035&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242035&amp;dopt=abstract</a> </p><br />

    <p>15.   60110   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          Uptake of inhalable microparticles affects defence responses of macrophages infected with Mycobacterium tuberculosis H37Ra</p>

    <p>          Sharma, R, Muttil, P, Yadav, AB, Rath, SK, Bajpai, VK, Mani, U, and Misra, A</p>

    <p>          J Antimicrob Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242031&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17242031&amp;dopt=abstract</a> </p><br />

    <p>16.   60111   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p><b>          Cysteine Protease Inhibitors Block Toxoplasma gondii Microneme Secretion and Cell Invasion</b> </p>

    <p>          Teo Chin Fen, Zhou Xing Wang, Bogyo Matthew, and Carruthers Vern B</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.  51(2): 679-88.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>17.   60112   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          2,5-dimethyl-4-hydroxy-3(2H)-furanone (DMHF); antimicrobial compound with cell cycle arrest in nosocomial pathogens</p>

    <p>          Sung, Woo Sang, Jung, Hyun Jun, Park, Keunnam, Kim, Hyun Soo, Lee, In-Seon, and Lee, Dong Gun</p>

    <p>          Life Sciences <b>2007</b>.  80(6): 586-591</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60113   OI-LS-367; PUBMED-OI-2/5/2007</p>

    <p class="memofmt1-2">          AccD6, a member of the Fas II locus, is a functional carboxyltransferase subunit of the acyl-coenzyme A carboxylase in Mycobacterium tuberculosis</p>

    <p>          Daniel, J, Oh, TJ, Lee, CM, and Kolattukudy, PE</p>

    <p>          J Bacteriol <b>2007</b>.  189(3): 911-7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17114269&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17114269&amp;dopt=abstract</a> </p><br />

    <p>19.   60114   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          In vitro-clinical correlations for amphotericin B susceptibility in AIDS-associated cryptococcal meningitis</p>

    <p>          Larsen, RA, Bauer, M, Brouwer, AE, Sanchez, A, Thomas, AM, Rajanuwong, A, Chierakul, W, Peacock, SJ, Day, N, White, NJ, Rinaldi, MG, and Harrison, TS</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(1): 343-345</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   60115   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          Cell Wall Targeting of Laccase of Cryptococcus neoformans during Infection of Mice</p>

    <p>          Waterman Scott R, Hacham Moshe, Panepinto John, Hu Guowu, Shin Soowan, and Williamson Peter R</p>

    <p>          Infect Immun <b>2007</b>.  75(2): 714-22.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   60116   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          Antiviral compounds</p>

    <p>          Cho, Aesop, Kim, Choung U, and Sheng, Xiaoning C</p>

    <p>          PATENT:  WO <b>2007009109</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006  PP: 197pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   60117   OI-LS-367; WOS-OI-1/28/2007</p>

    <p class="memofmt1-2">          Synthesis, magnetic, spectral, and antimicrobial studies of Cu(II), Ni(II) Co(II), Fe(III), and UO2(II) complexes of a new Schiff base hydrazone derived from 7-chloro-4-hydrazinoquinoline</p>

    <p>          El-Behery, M and El-Twigry, H</p>

    <p>          SPECTROCHIMICA ACTA PART A-MOLECULAR AND BIOMOLECULAR SPECTROSCOPY <b>2007</b>.  66(1): 28-36, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243159300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243159300004</a> </p><br />

    <p>23.   60118   OI-LS-367; SCIFINDER-OI-1/29/2007</p>

    <p class="memofmt1-2">          Tandem repeats of lactoferrin-derived anti-hepatitis C virus Peptide enhance antiviral activity in cultured human hepatocytes</p>

    <p>          Abe Ken-Ichi, Nozaki Akito, Tamura Kazushi, Ikeda Masanori, Naka Kazuhito, Dansako Hiromichi, Hoshino Hiro-O, Tanaka Katsuaki, and Kato Nobuyuki</p>

    <p>          Microbiol Immunol <b>2007</b>.  51(1): 117-25.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   60119   OI-LS-367; WOS-OI-1/28/2007</p>

    <p class="memofmt1-2">          Moxifloxacin and gatifloxacin in an acid model of persistent Mycobacterium tuberculosis</p>

    <p>          Kubendiran, G, Paramasivan, CN, Sulochana, S, and Mitchison, DA</p>

    <p>          JOURNAL OF CHEMOTHERAPY <b>2006</b>.  18(6): 617-623, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243185500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243185500006</a> </p><br />

    <p>25.   60120   OI-LS-367; WOS-OI-1/28/2007</p>

    <p class="memofmt1-2">          In vitro and in vivo (animal models) antifungal activity of posaconazole</p>

    <p>          Mallie, M</p>

    <p>          JOURNAL DE MYCOLOGIE MEDICALE <b>2006</b>.  16: S1-S8, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243198400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243198400001</a> </p><br />

    <p>26.   60121   OI-LS-367; WOS-OI-1/28/2007</p>

    <p class="memofmt1-2">          Fungal citridone D having a novel phenylfuropyridine skeleton</p>

    <p>          Fukuda, T, Sakabe, Y, Tomoda, H, and Omura, S</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2006</b>.  54(12): 1659-1661, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243143000008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243143000008</a> </p><br />

    <p>27.   60122   OI-LS-367; WOS-OI-1/28/2007</p>

    <p class="memofmt1-2">          Combination of amphotericin B with flucytosine is active in vitro against flucytosine-resistant isolates of Cryptococcus neoformans</p>

    <p>          Schwarz, P, Janbon, G, Dromer, F, Lortholary, O, and Dannaoui, E</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2007</b>.  51(1): 383-385, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243214200056">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243214200056</a> </p><br />

    <p>28.   60123   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Synthesis, crystal structure and antifungal/antibacterial activity of some novel highly functionalized benzoylaminocarbothioyl pyrrolidines</p>

    <p>          Dondas, HA, Nural, Y, Duran, N, and Kilner, C</p>

    <p>          TURKISH JOURNAL OF CHEMISTRY <b>2006</b>.  30(5): 573-583, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243366000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243366000005</a> </p><br />

    <p>29.   60124   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Mathematical modeling of subgenomic hepatitis C virus replication in Huh-7 cells</p>

    <p>          Dahari, H, Ribeiro, RM, Rice, CM, and Perelson, AS</p>

    <p>          JOURNAL OF VIROLOGY <b>2007</b>.  81(2): 750-760, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243350100032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243350100032</a> </p><br />

    <p>30.   60125   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          N(4)-tolyl-2-benzoylpyridine thiosemicarbazones and their copper(II) complexes with significant antifungal activity. Crystal structure of N(4)-para-tolyl-2-benzoylpyridine thiosemicarbazone</p>

    <p>          Mendes, IC, Moreira, JP, Speziali, NL, Mangrich, AS, Takahashi, JA, and Beraldo, H</p>

    <p>          JOURNAL OF THE BRAZILIAN CHEMICAL SOCIETY <b>2006</b>.  17(8): 1571-1577, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243386100013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243386100013</a> </p><br />
    <br clear="all">

    <p>31.   60126   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Pathogenesis of Aspergillus fumigatus and the kinetics of galactomannan in an in vitro model of early invasive pulmonary aspergillosis: Implications for antifungal therapy</p>

    <p>          Hope, WW, Kruhlak, MJ, Lyman, CA, Petraitiene, R, Petraitis, V, Francesconi, A, Kasai, M, Mickiene, D, Sein, T, Peter, J, Kelaher, AM, Hughes, JE, Cotton, MP, Cotten, CJ, Bacher, J, Tripathi, S, Bermudez, L, Maugel, TK, Zerfas, PM, Wingard, JR, Drusano, GL, and Walsh, TJ</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2007</b>.  195(3): 455-466, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243237700021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243237700021</a> </p><br />

    <p>32.   60127   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          The hepatitis C virus NS3 protein: A model RNA helicase and potential drug target</p>

    <p>          Frick, DN</p>

    <p>          CURRENT ISSUES IN MOLECULAR BIOLOGY <b>2007</b>.  9: 1-20, 20 </p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243237000001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243237000001</a> </p><br />

    <p>33.   60128   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Antiplasmodial, antimycobacterial, and cytotoxic principles from Camchaya calcarea</p>

    <p>          Vongvanich, N, Kittakoop, P, Charoenchai, P, Intamas, S, Sriklung, K, and Thebtaranonth, Y</p>

    <p>          PLANTA MEDICA <b>2006</b>.  72(15): 1427-1430, 4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243285400015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243285400015</a> </p><br />

    <p>34.   60129   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of sulfonamides and 4-(p-nitro benzoyl) piperazine incorporated fluoro quinolones</p>

    <p>          Patel, NB and Bhagat, PR</p>

    <p>          INDIAN JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2006</b>.  16(2): 205-206, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243578100031">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243578100031</a> </p><br />

    <p>35.   60130   OI-LS-367; WOS-OI-2/4/2007</p>

    <p class="memofmt1-2">          Synthesis, antimicrobial activity and QSARs of new benzoxazine-3-ones</p>

    <p>          Alper-Hayta, S, Aki-Sener, E, Tekiner-Gulbas, B, Yildiz, I, Temiz-Arpaci, O, Yalcin, I, and Altanlar, N</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(12): 1398-1404, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243360300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243360300004</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
