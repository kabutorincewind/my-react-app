

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-01-05.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="oPzmXfnqeN/Yep8b07t9PwVuPGtfnRxHExUFDItdPahTedVZPTt7zy/vQT4iZYjBQQpSX9lQtyGqNzG/33DQjBF6mtZ/JCiv3Pv/aZDTxDeXirzfWnwCnBe41q4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CF4264B7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: </h1>

    <h1 class="memofmt2-h1">December 23 - January 5, 2012</h1>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22061661">A New 9-nor-Atractylodin from Atractylodes lancea and the Antibacterial Activity of the Atractylodin Derivatives.</a> Chen, Y., Y. Wu, H. Wang, and K. Gao, Fitoterapia, 2012. 83(1): p. 199-203; PMID[22061661].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22212016">Ctbt (7-Chlorotetrazolo[5,1-C]benzo[1,2,4]triazine) Producing ROS Affects Growth and Viability of Filamentous Fungi.</a> Culakova, H., V. Dzugasova, Y. Gbelska, and J. Subik, Fems Microbiology Letters, 2011. <b>[Epub ahead of print]</b>; PMID[22212016].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22006003">In Vitro Effect of Malachite Green on Candida albicans Involves Multiple Pathways and Transcriptional Regulators UPC2 and STP2.</a> Dhamgaye, S., F. Devaux, R. Manoharlal, P. Vandeputte, A.H. Shah, A. Singh, C. Blugeon, D. Sanglard, and R. Prasad, Antimicrobial Agents and Chemotherapy, 2012. 56(1): p. 495-506; PMID[22006003].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22203607">The Monoamine Oxidase A Inhibitor Clorgyline is a Broad-spectrum Inhibitor of Fungal ABC and MFS Transporter Efflux Pump Activities Which Reverses the Azole Resistance of Candida albicans and Candida glabrata Clinical Isolates.</a> Holmes, A.R., M.V. Keniya, I. Ivnitski-Steele, B.C. Monk, E. Lamping, L.A. Sklar, and R.D. Cannon, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[22203607].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22088496">A New Antifungal Coumarin from Clausena excavata.</a> Kumar, R., A. Saha, and D. Saha, Fitoterapia, 2012. 83(1): p. 230-233; PMID[22088496].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22186331">Antimicrobial Activities of Three Species of Family Mimosaceae.</a> Mahmood, A. and R.A. Qureshi, Pakistan Journal of Pharmaceutical Sciences, 2012. 25(1): p. 203-206; PMID[22186331].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22113112">Synthesis and Antifungal Activity of 6,7-bis(Arylthio)-quinazoline-5,8-diones and Furo[2,3-F]quinazolin-5-ols.</a> Ryu, C.K., Y.H. Kim, H.A. Im, J.Y. Kim, J.H. Yoon, and A. Kim, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(1): p. 500-503; PMID[22113112].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22208440">Comparison of the Effect of Non-antifungal and Antifungal Agents on Candida Isolates from the Gastrointestinal Tract.</a> Siavoshi, F., A. Tavakolian, A. Foroumadi, N. Mohammad Hosseini, S. Massarrat, S. Pedramnia, and P. Saniee, Archives of Iranian Medicine, 2012. 15(1): p. 27-31; PMID[22208440].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22029966">Synthesis, Spectral Characterization and Biological Activity of Zinc(Ii) Complexes with 3-Substituted phenyl-4-amino-5-hydrazino-1, 2, 4-triazole Schiff Bases.</a> Singh, A.K., O.P. Pandey, and S.K. Sengupta, Spectrochimica Acta. Part A, Molecular and Biomolecular Spectroscopy, 2012. 85(1): p. 1-6; PMID[22029966].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22024829">Antifungal Susceptibilities of Aspergillus fumigatus Clinical Isolates Obtained in Nagasaki, Japan.</a>  Tashiro, M., K. Izumikawa, A. Minematsu, K. Hirano, N. Iwanaga, S. Ide, T. Mihara, N. Hosogaya, T. Takazono, Y. Morinaga, S. Nakamura, S. Kurihara, Y. Imamura, T. Miyazaki, T. Nishino, M. Tsukamoto, H. Kakeya, Y. Yamamoto, K. Yanagihara, A. Yasuoka, T. Tashiro, and S. Kohno, Antimicrobial Agents and Chemotherapy, 2012. 56(1): p. 584-587; PMID[22024829].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22061662">Polyketides with Antimicrobial Activity from the Solid Culture of an Endolichenic Fungus Ulocladium Sp.</a>Wang, Q.X., L. Bao, X.L. Yang, H. Guo, R.N. Yang, B. Ren, L.X. Zhang, H.Q. Dai, L.D. Guo, and H.W. Liu, Fitoterapia, 2012. 83(1): p. 209-214; PMID[22061662].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22148391">Structure-Activity Relationships for Amide-, Carbamate-, and Urea-Linked Analogues of the Tuberculosis Drug (6s)-2-Nitro-6-{[4-(trifluoromethoxy)benzyl]oxy}-6,7-dihydro-5h-imidazo[2,1-B][1, 3]oxazine (Pa-824).</a>Blaser, A., B.D. Palmer, H.S. Sutherland, I. Kmentova, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, A.M. Thompson, and W.A. Denny, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[22148391].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22209468">Antibacterial Activities of Dendritic Amphiphiles against Nontuberculous Mycobacteria.</a> Falkinham, J.O., 3rd, R.V. Macri, B.B. Maisuria, M.L. Actis, E.W. Sugandhi, A.A. Williams, A.V. Snyder, F.R. Jackson, M.A. Poppe, L. Chen, K. Ganesh, and R.D. Gandour, Tuberculosis (Edinburgh, Scotland), 2011. <b>[Epub ahead of print]</b>; PMID[22209468].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22153872">Synthesis of Novel 3-Cyclohexylpropanoic acid-derived Nitrogen Heterocyclic Compounds and Their Evaluation for Tuberculostatic Activity.</a> Gobis, K., H. Foks, K. Bojanowski, E. Augustynowicz-Kopec, and A. Napiorkowska, Bioorganic &amp; Medicinal Chemistry, 2012. 20(1): p. 137-144; PMID[22153872].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22104148">Synthesis, Antitubercular Evaluation and 3D-Qsar Study of N-Phenyl-3-(4-fluorophenyl)-4-substituted pyrazole Derivatives.</a> Khunt, R.C., V.M. Khedkar, R.S. Chawda, N.A. Chauhan, A.R. Parikh, and E.C. Coutinho, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(1): p. 666-678; PMID[22104148].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22214209">Dihalogenated Sulfanilamides and Benzolamides Are Effective Inhibitors of the Three Beta-class Carbonic Anhydrases from Mycobacterium tuberculosis.</a> Maresca, A., A. Scozzafava, D. Vullo, and C.T. Supuran, Journal of Enzyme Inhibition and Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22214209].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22215188">Antiplasmodial Activity of Flavonol quercetin and Its Analogues in Plasmodium falciparum: Evidence from Clinical Isolates in Bangladesh and Standardized Parasite Clones.</a> Ganesh, D., H.P. Fuehrer, P. Starzengruber, P. Swoboda, W.A. Khan, J.A. Reismann, M.S. Mueller, P. Chiba, and H. Noedl, Parasitology Research, 2012. <b>[Epub ahead of print]</b>; PMID[22215188].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22213131">Antiplasmodial Decarboxyportentol acetate and 3,4-Dehydrotheaspirone from Laumoniera bruceadelpha.</a>Morita, H., R. Mori, J. Deguchi, S. Oshimi, Y. Hirasawa, W. Ekasari, A. Widyawaruyanti, and A.H. Hadi, Journal of Natural Medicines, 2012. <b>[Epub ahead of print]</b>; PMID[22213131].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22195562">Identification and Validation of Tetracyclic benzothiazepines as Plasmodium falciparum Cytochrome Bc(1) Inhibitors.</a>Dong, C.K., S. Urgaonkar, J.F. Cortese, F.J. Gamo, J.F. Garcia-Bustos, M.J. Lafuente, V. Patel, L. Ross, B.I. Coleman, E.R. Derbyshire, C.B. Clish, A.E. Serrano, M. Cromwell, R.H. Barker, Jr., J.D. Dvorin, M.T. Duraisingh, D.F. Wirth, J. Clardy, and R. Mazitschek, Chemistry &amp; Biology, 2011. 18(12): p. 1602-10; PMID[22195562].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22095896">Identification of 1,3-Diiminoisoindoline carbohydrazides as Potential Antimalarial Candidates.</a> Mombelli, P., M.C. Witschel, A.W. van Zijl, J.G. Geist, M. Rottmann, C. Freymond, F. Rohl, M. Kaiser, V. Illarionova, M. Fischer, I. Siepe, W.B. Schweizer, R. Brun, and F. Diederich, ChemMedChem, 2012. 7(1): p. 151-158; PMID[22095896].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22033013">Indole Alkaloids from Muntafara sessilifolia with Antiplasmodial and Cytotoxic Activities.</a> Girardot, M., C. Deregnaucourt, A. Deville, L. Dubost, R. Joyeau, L. Allorge, P. Rasoanaivo, and L. Mambu, Phytochemistry, 2012. 73(1): p. 65-73; PMID[22033013].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22082562">Methylenebissantin: A Rare Methylene-bridged Bisflavonoid from Dodonaea Viscosa Which Inhibits Plasmodium falciparum Enoyl-acp reductase.</a> Muhammad, A., I. Anis, Z. Ali, S. Awadelkarim, A. Khan, A. Khalid, M.R. Shah, M. Galal, I.A. Khan, and M. Iqbal Choudhary, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(1): p. 610-612; PMID[22082562].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22172698">Synthesis, Docking and in Vitro Antimalarial Evaluation of Bifunctional Hybrids Derived from Beta-Lactams and 7-Chloroquinoline Using Click Chemistry.</a> Singh, P., M. Kumar, J. Gut, P.J. Rosenthal, K. Kumar, V. Kumar, M.P. Mahajan, and K. Bisetty, Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(1): p. 57-61; PMID[22172698].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p> </p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21717278">Evaluation of Plumbagin and Its Derivative as Potential Modulators of Redox Thiol Metabolism of Leishmania Parasite.</a> Sharma, N., A.K. Shukla, M. Das, and V.K. Dubey, Parasitology Research, 2012. 110(1): p. 341-348; PMID[21717278].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22072308">Leishmanicidal and Antitumoral Activities of Endophytic Fungi Associated with the Antarctic Angiosperms Deschampsia antarctica Desv. and Colobanthus quitensis (Kunth) Bartl.</a>Santiago, I.F., T.M. Alves, A. Rabello, P.A. Sales Junior, A.J. Romanha, C.L. Zani, C.A. Rosa, and L.H. Rosa, Extremophiles: Life Under Extreme Conditions, 2012. 16(1): p. 95-103; PMID[22072308].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTI-PROTOZOAL COMPOUNDS</h2>

    <p> </p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22215028">1-(Fluoroalkylidene)-1,1-bisphosphonic acids Are Potent and Selective Inhibitors of the Enzymatic Activity of Toxoplasma gondii Farnesyl pyrophosphate synthase.</a> Szajnman, S.H., V.S. Rosso, L. Malayil, A. Smith, S.N. Moreno, R. Docampo, and J.B. Rodriguez, Organic &amp; Biomolecular Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22215028].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1223-010512.</p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p> </p>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297458700019">Chemical Composition and Antifungal Activity of Aleppo Pine Essential Oil.</a> Abi-Ayad, M., F.Z. Abi-Ayad, H.A. Lazzouni, S.A. Rebiahi, C.Z. Cherif, and Bessiere, Journal of Medicinal Plants Research, 2011. 5(22): p. 5433-5436; ISI[000297458700019].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297384800009">Cytotoxic and Antimicrobial Properties of Furoflavones and Furochalcones.</a> Alam, M.S. and D.U. Lee, Journal of the Korean Society for Applied Biological Chemistry, 2011. 54(5): p. 725-730; ISI[000297384800009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297791000014">Glucosinolate Profiling and Antimicrobial Screening of Aurinia leucadea (Brassicaceae).</a> Blazevic, I., A. Radonic, M. Skocibusic, G.R. De Nicola, S. Montaut, R. Iori, P. Rollin, J. Mastelic, M. Zekic, and A. Maravic, Chemistry &amp; Biodiversity, 2011. 8(12): p. 2310-2321; ISI[000297791000014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297789400007">Antimycobacterial Activity of the Fractions and Compounds from Scutia buxifolia.</a> Boligon, A.A., V. Agertt, V. Janovik, R.C. Cruz, M.M.A. Campos, D. Guillaume, M.L. Athayde, and A.R.S. dos Santos, Revista Brasileira De Farmacognosia-Brazilian Journal of Pharmacognosy, 2011. 22(1): p. 45-52; ISI[000297789400007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297594400030">Antimicrobial Activity of Crude Extracts from Mangrove Fungal Endophytes.</a> Buatong, J., S. Phongpaichit, V. Rukachaisirikul, and J. Sakayaroj, World Journal of Microbiology &amp; Biotechnology, 2011. 27(12): p. 3005-3008; ISI[000297594400030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297603600015">Bisubstrate Adenylation Inhibitors of Biotin Protein Ligase from Mycobacterium tuberculosis.</a> Duckworth, B.P., T.W. Geders, D. Tiwari, H.I. Boshoff, P.A. Sibbald, C.E. Barry, D. Schnappinger, B.C. Finzel, and C.C. Aldrich, Chemistry &amp; Biology, 2011. 18(11): p. 1432-1441; ISI[000297603600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297463900002">Crude Sample Preparation, Extraction and in Vitro Screening for Antimicrobial Activity of Selected Wound Healing Medicinal Plants in Kwazulu-Natal, South Africa: A Review.</a> Ghuman, S. and R.M. Coopoosamy, Journal of Medicinal Plants Research, 2011. 5(16): p. 3572-3576; ISI[000297463900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297836100010">Triclosan Antagonizes Fluconazole Activity against Candida albicans.</a> Higgins, J., E. Pinjon, H.N. Oltean, T.C. White, S.L. Kelly, C.M. Martel, D.J. Sullivan, D.C. Coleman, and G.P. Moran, Journal of Dental Research, 2012. 91(1): p. 65-70; ISI[000297836100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297785800008">Synthesis and Biological Evaluation of Some Novel Phenyl thiazole Derivatives as Possible Antimicrobial Agents.</a>Kittur, B.S., B.S. Sastry, Y.R. Prasad, S.R. Pattan, and Y. Srinivas, Indian Journal of Pharmaceutical Education and Research, 2011. 45(2): p. 157-163; ISI[000297785800008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297756800021">Antifungal Activity of Different Neem Leaf Extracts and the Nimonol against Some Important Human Pathogens.</a>Mahmoud, D.A., N.M. Hassanein, K.A. Youssef, and M.A. Abou Zeid, Brazilian Journal of Microbiology, 2011. 42(3): p. 1006-1015; ISI[000297756800021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297619600015">Synthetic, Spectral, Structural and Antimicrobial Studies of Some Schiff Bases 3-D Metal Complexes.</a> Mishra, A.P., R.K. Mishra, and M.D. Pandey, Russian Journal of Inorganic Chemistry, 2011. 56(11): p. 1757-1764; ISI[000297619600015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297659100028">Inhibitory Activity of Lactic and Acetic Acid on Aspergillus flavus Growth for Food Preservation.</a> Pelaez, A.M.L., C.A.S. Catano, E.A.Q. Yepes, R.R.G. Villarroel, G.L. De Antoni, and L. Giannuzzi, Food Control, 2012. 24(1-2): p. 177-183; ISI[000297659100028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297697200036">Antimicrobial Prospect of Newly Synthesized 1,3-Thiazole Derivatives.</a> Sadek, B., M.M. Al-Tabakha, and K.M.S. Fahelelbom, Molecules, 2011. 16(11): p. 9386-9396; ISI[000297697200036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>

    <p> </p>

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000297458800004">In Vitro Antimicrobial Activity of Some Phytochemical Fractions of Euphorbia pulcherima L. (Poinsettia).</a>Yakubu, A.I. and M.D. Mukhtar, Journal of Medicinal Plants Research, 2011. 5(12): p. 2470-2475; ISI[000297458800004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1223-010512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
