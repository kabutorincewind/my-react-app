

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-153.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ylew071SytEu6Hto3eiRtMPzUXpQObHOW/n8g6qGQllJ+MG9fJ3fUrNq6XTV9LQuD7rKtIZmnWfNkne00497LVbznZ/XutJrZmgfqMNYw8L+z9/JqFZA+Dnz6Dk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="645F0BC0" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-153-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61603   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Effect of 6-azacytidine on the course of experimental adenoviral infection in newborn syrian hamsters</p>

    <p>          Zarubaev, VV, Slita, AV, Sukhinin, VP, Nosach, LN, Dyachenko, NS, Povnitsa, OY, Zhovnovataya, VL, Alexeeva, IV, and Palchikovskaya, LI</p>

    <p>          J. Chemother. (Firenze, Italy) <b>2007</b>.  19(1): 44-51</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     61604   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Antiviral drugs for treatment of arenavirus infection</p>

    <p>          Hruby, Dennis E, Bolken, Tove, Amberg, Sean, and Dai, Dongcheng</p>

    <p>          PATENT:  WO <b>2007100888</b>  ISSUE DATE:  20070907</p>

    <p>          APPLICATION: 2007</p>

    <p>          ASSIGNEE:  (Siga Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     61605   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          In Vitro and In Vivo Activities of T-705 against Arenavirus and Bunyavirus Infections</p>

    <p>          Gowen Brian B, Wong Min-Hui, Jung Kie-Hoon, Sanders Andrew B, Mendenhall Michelle, Bailey Kevin W, Furuta Yousuke, and Sidwell Robert W</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.  51(9): 3168-76.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     61606   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Amino Acid Derivatives, IV [1]: Synthesis and Antiviral Evaluation of New a-Amino Acid Esters Bearing Methyl b-d-Ribofuranoside Side Chain</p>

    <p>          Ali, Ibrahim AI, Ali, Omar M, and Abdel-Rahman, Adel A-H</p>

    <p>          Monatsh. Chem.  <b>2007</b>.  138(9): 909-915</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>5.     61607   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Antibacterial and antiviral naphthazarins from Maharanga bicolor</p>

    <p>          Rajbhandari, M, Schoepke, Th, Mentel, R, and Lindequist, U</p>

    <p>          Pharmazie <b>2007</b>.  62(8): 633-635</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     61608   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Pyrimidine nucleoside derivatives, pharmaceutical compositions and antiviral agents containing them, and therapy of viral infection with them</p>

    <p>          Machida, Haruhiko and Yamamoto, Masakazu</p>

    <p>          PATENT:  JP <b>2007210963</b>  ISSUE DATE:  20070823</p>

    <p>          APPLICATION: 2006-33854  PP: 17pp.</p>

    <p>          ASSIGNEE:  (Arigen, Inc. Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>7.     61609   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Antineoplastic and antiviral activities of pyrrolizidine alkaloids from Heliotropium marifolum Koen. ex Retz</p>

    <p>          Singh, B and Sharma, RA</p>

    <p>          Proc. Natl. Acad. Sci., India, Sect. B <b>2007</b>.  77(2): 197-205</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>8.     61610   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of quinolones as antiviral agents</p>

    <p>          Fuerstner, Chantal, Thede, Kai, Zimmermann, Holger, Brueckner, David, Henninger, Kerstin, Lang, Dieter, and Schohe-Loop, Rudolf</p>

    <p>          PATENT:  WO <b>2007090579</b>  ISSUE DATE:  20070816</p>

    <p>          APPLICATION: 2007  PP: 134pp.</p>

    <p>          ASSIGNEE:  (Aicuris G.m.b.H. &amp; Co. K.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     61611   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of indoloquinoxaline ammonium compounds as antiviral agents</p>

    <p>          Homman, Mohammed, Engqvist, Robert, Soederberg-Naucler, Cecilia, and Bergman, Jan</p>

    <p>          PATENT:  WO <b>2007084073</b>  ISSUE DATE:  20070726</p>

    <p>          APPLICATION: 2007  PP: 23pp.</p>

    <p>          ASSIGNEE:  (Vironova AB, Swed.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   61612   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of TDA derivatives as antiviral agents</p>

    <p>          Zhao, Jinghua, Zhao, Xiangui, and Pan, Xiandao</p>

    <p>          PATENT:  CN <b>1990470</b>  ISSUE DATE: 20070704</p>

    <p>          APPLICATION: 1013-7408  PP: 18pp.</p>

    <p>          ASSIGNEE:  (Beijing Union Pharmaceutical Factory, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   61613   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Chemical properties, mode of action, and in vivo anti-herpes activities of a lignin-carbohydrate complex from Prunella vulgaris</p>

    <p>          Zhang, Yongwen, But, Paul Pui-Hay, Ooi, Vincent Eng-Choon, Xu, Hong-Xi, Delaney, Gillian D, Lee, Spencer HS, and Lee, Song F</p>

    <p>          Antiviral Res.  <b>2007</b>.  75(3): 242-249</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   61614   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Preparation of cubane nucleoside analogs as antiviral and antitumor agents</p>

    <p>          Trampota, Miroslav and Murphy, Randall B</p>

    <p>          PATENT:  WO <b>2007059330</b>  ISSUE DATE:  20070524</p>

    <p>          APPLICATION: 2006  PP: 79pp.</p>

    <p>          ASSIGNEE:  (Medkura Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   61615   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Differential antiviral activity of benzastatin C and its dechlorinated derivative from Streptomyces nitrosporeus</p>

    <p>          Lee, Jong-Gyo, Yoo, Ick-Dong, and Kim, Won-Gon</p>

    <p>          Biol. Pharm. Bull. <b>2007</b>.  30(4): 795-797</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   61616   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Facile synthesis and evaluation of antitumor and antiviral activities of [1,2,5]thiadiazolo[3,4-d]pyrimidines (8-thiapurines) and 4-b-D-ribofuranosyl-[1,2,5]thiadiazolo[3,4-d]pyrimidines</p>

    <p>          Nagamatsu, Tomohisa, Islam, Rafiqul, and Ashida, Noriyuki</p>

    <p>          Heterocycles <b>2007</b>.  72: 573-588</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>15.   61617   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Selective phosphorylation of antiviral drugs by vaccinia virus thymidine kinase</p>

    <p>          Prichard, Mark N, Keith, Kathy A, Johnson, Mary P, Harden, Emma A, McBrayer, Alexis, Luo, Ming, Qiu, Shihong, Chattopadhyay, Debasish, Fan, Xuesen, Torrence, Paul F, and Kern, Earl R</p>

    <p>          Antimicrob. Agents Chemother. <b>2007</b>.  51(5): 1795-1803</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   61618   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p><b>          Synthesis and antiviral activities of new acyclic and &quot;double-headed&quot; nucleoside analogues</b> </p>

    <p>          Zhang, Xinying, Amer, Adel, Fan, Xuesen, Balzarini, Jan, Neyts, Johan, De Clercq, Erik, Prichard, Mark, Kern, Earl, and Torrence, Paul F</p>

    <p>          Bioorg. Chem. <b>2007</b>.  35(3): 221-232</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   61619   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Peptides interacting with a-helical coiled-coil structures for treatment of HIV, influenza virus, and Mycobacterium tuberculosis infections</p>

    <p>          Mahrenholz, Carsten and Portwich, Michael</p>

    <p>          PATENT:  WO <b>2007068240</b>  ISSUE DATE:  20070621</p>

    <p>          APPLICATION: 2006  PP: 74pp.</p>

    <p>          ASSIGNEE:  (Charite-Universitaetsmedizin Berlin, Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   61620   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Cell line dependency for antiviral activity and in vivo efficacy of N-methanocarbathymidine against orthopoxvirus infections in mice</p>

    <p>          Smee, Donald F, Wandersee, Miles K, Bailey, Kevin W, Wong, Min-Hui, Chu, Chung K, Gadthula, Srinivas, and Sidwell, Robert W</p>

    <p>          Antiviral Res.  <b>2007</b>.  73(1): 69-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   61621   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Severe acute respiratory syndrome coronavirus entry as a target of antiviral therapies</p>

    <p>          Kuhn, Jens H, Li, Wenhui, Radoshitzky, Sheli R, Choe, Hyeryun, and Farzan, Michael</p>

    <p>          Antiviral Ther. <b>2007</b>.  12(4, Pt. B): 639-650</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   61622   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Antiviral effect of glycyrrhizin on porcine respiratory coronavirus (PRCV)</p>

    <p>          Cao, Min-jie, Wu, Guo-ping, Weng, Ling, and Su, Wen-jin</p>

    <p>          Tianran Chanwu Yanjiu Yu Kaifa <b>2007</b>.  19(2): 221-224</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>21.   61623   DMID-LS-153; SCIFINDER-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Flavonoid compound having an antiviral activity</p>

    <p>          Kwon, Dur Han, Choi, Wha Jeong, Lee, Choong Hwan, Kim, Jin Hee, and Kim, Man Bae</p>

    <p>          PATENT:  WO <b>2007069823</b>  ISSUE DATE:  20070621</p>

    <p>          APPLICATION: 2006  PP: 28pp.</p>

    <p>          ASSIGNEE:  (Korea Research Institute of Bioscience and Biotechnology, S. Korea</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>22.   61624   DMID-LS-153; WOS-DMID-8/31/2007</p>

    <p class="memofmt1-2">          Hepatitis a, B, C, D, E, G: an Update</p>

    <p>          Hall, G.</p>

    <p>          Ethnicity &amp; Disease <b>2007</b>.  17(2): S40-S45</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248009700041">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000248009700041</a> </p><br />

    <p>23.   61625   DMID-LS-153; WOS-DMID-8/31/2007</p>

    <p class="memofmt1-2">          Two New Sesquiterpenes From Alisma Orientalis</p>

    <p>          Jiang, Z. <i>et al.</i></p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2007</b>.  55(6): 905-907</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247512600012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247512600012</a> </p><br />

    <p>24.   61626   DMID-LS-153; WOS-DMID-8/31/2007</p>

    <p class="memofmt1-2">          In Vitro Resistance Selection and in Vivo Efficacy of Morpholino Oligomers Against West Nile Virus</p>

    <p>          Deas, T. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(7): 2470-2482</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800025</a> </p><br />

    <p>25.   61627   DMID-LS-153; WOS-DMID-9/7/2007</p>

    <p class="memofmt1-2">          Sultam Thiourea Inhibition of West Nile Virus</p>

    <p>          Barklis, E. <i>et al.</i></p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2007</b>.  51(7): 2642-2645</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800055">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247665800055</a> </p><br />

    <p>26.   61628   DMID-LS-153; WOS-DMID-9/7/2007</p>

    <p class="memofmt1-2">          Bird Flu: Lessons From Sars</p>

    <p>          Wong, G. and Leung, T.</p>

    <p>          Paediatric Respiratory Reviews <b>2007</b>.  8(2): 171-176</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247926800011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247926800011</a> </p><br />

    <p>27.   61629   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Inhibition of respiratory syncytial virus of subgroups A and B using deoxyribozyme DZ1133 in mice</p>

    <p>          Zhou, J, Yang, XQ, Xie, YY, Zhao, XD, Jiang, LP, Wang, LJ, and Cui, YX</p>

    <p>          Virus Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804108&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17804108&amp;dopt=abstract</a> </p><br />

    <p>28.   61630   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Identification of Natural Compounds with Anti-Hepatitis B Virus Activity from Rheum palmatum L. Ethanol Extract</p>

    <p>          Li, Z, Li, LJ, Sun, Y, and Li, J</p>

    <p>          Chemotherapy <b>2007</b>.  53(5): 320-326</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17785969&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17785969&amp;dopt=abstract</a> </p><br />

    <p>29.   61631   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Discovery and development of antiviral drugs for biodefense: Experience of a small biotechnology company</p>

    <p>          Bolken, TC and Hruby, DE</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17765333&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17765333&amp;dopt=abstract</a> </p><br />

    <p>30.   61632   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Characterization of Low Pathogenicity H5N1 Avian Influenza Viruses from North America</p>

    <p>          Spackman, E, Swayne, DE, Suarez, DL, Senne, DA, Pedersen, JC, Killian, ML, Pasick, J, Handel, K, Somanathan, Pillai SP, Lee, CW, Stallknecht, D, Slemons, R, Ip, HS, and Deliberto, T</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17728231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17728231&amp;dopt=abstract</a> </p><br />

    <p>31.   61633   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          New therapeutic opportunities for Hepatitis C based on small RNA</p>

    <p>          Pan, QW, Henry, SD, Scholte, BJ, Tilanus, HW, and Janssen, HL</p>

    <p>          World J Gastroenterol <b>2007</b>.  13(33): 4431-4436</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724797&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724797&amp;dopt=abstract</a> </p><br />

    <p>32.   61634   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Synergistic Efficacy of the Combination ST-246 with CMX001 against Orthopoxviruses</p>

    <p>          Quenelle, DC, Prichard, MN, Keith, KA, Hruby, DE, Jordan, R, Painter, GR, Robertson, A, and Kern, ER</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724153&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17724153&amp;dopt=abstract</a> </p><br />

    <p>33.   61635   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Structure-Based Design and Synthesis of Highly Potent SARS-CoV 3CL Protease Inhibitors</p>

    <p>          Shao, YM, Yang, WB, Peng, HP, Hsu, MF, Tsai, KC, Kuo, TH, Wang, AH, Liang, PH, Lin, CH, Yang, AS, and Wong, CH</p>

    <p>          Chembiochem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17722121&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>34.   61636   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          Evaluation systems for anti-HCV drugs</p>

    <p>          Moriishi, K and Matsuura, Y</p>

    <p>          Adv Drug Deliv Rev <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17720275&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17720275&amp;dopt=abstract</a> </p><br />

    <p>35.   61637   DMID-LS-153; PUBMED-DMID-9/10/2007</p>

    <p class="memofmt1-2">          The phenylpropenamide derivative AT-130 blocks HBV replication at the level of viral RNA packaging</p>

    <p>          Feld, JJ, Colledge, D, Sozzi, V, Edwards, R, Littlejohn, M, and Locarnini, SA</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709147&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709147&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
