

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2010-05-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="IYyO+daywhq6sBTjFfYp91od19jLDivhQqOyR1uCuxCfoh5tv69m5Ar2bmZVbOJWGt81tAFlMEjvT1N99dnyID5QMVcMy7j3ZB2XjsU7wXrZK8ZlXrAJmhjtDF4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0D07F3A2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">May 14- May 27, 2010</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">OPPORTUNISTIC PROTOZOA COMPOUNDS (including pneumocystis, cryptosporidium, toxoplasma, microsporidia [encephalitozoon, enterocytozoon, vittaforma])</h2> 

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20456959">Structure-activity relationships of carbocyclic 6-benzylthioinosine analogues as subversive substrates of Toxoplasma gondii adenosine kinase.</a> Kim, Y.A., R.K. Rawal, J. Yoo, A. Sharon, A.K. Jha, C.K. Chu, R.H. Rais, O.N. Al Safarjalani, F.N. Naguib, and M.H. El Kouni. Bioorg Med Chem. 18(10): p. 3403-12; PMID[20456959].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p> </p>

    <h2 class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="memofmt2-1"> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20482450">Antifungal activity of low molecular weight chitosan against clinical isolates of Candida spp.</a> Alburquenque, C., S.A. Bucarey, A. Neira-Carrillo, B. Urzua, G. Hermosilla, and C.V. Tapia. Med Mycol. <b>[Epub ahead of print]</b>; PMID[20482450].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20499922">Dihydro-beta-agarofuran Sesquiterpenes and Pentacyclic Triterpenoids from the Root Bark of Osyris lanceolata.</a> Yeboah, E.M., R.R. Majinda, A. Kadziola, and A. Muller. J Nat Prod. <b>[Epub ahead of print]</b>; PMID[20499922].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p class="memofmt2-h1"> </p>

    <h2 class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</h2>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20491506">Triazole-Linked Inhibitors of Inosine Monophosphate Dehydrogenase from Human and Mycobacterium tuberculosis.</a> Chen, L., D.J. Wilson, Y. Xu, C.C. Aldrich, K. Felczak, Y.Y. Sham, and K.W. Pankiewicz. J Med Chem. <b>[Epub ahead of print]</b>; PMID[20491506].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20480490">Thiazolopyrimidine Inhibitors of 2-Methylerythritol 2,4-Cyclodiphosphate Synthase (IspF) from Mycobacterium tuberculosis and Plasmodium falciparum.</a> Geist, J.G., S. Lauw, V. Illarionova, B. Illarionov, M. Fischer, T. Grawert, F. Rohdich, W. Eisenreich, J. Kaiser, M. Groll, C. Scheurer, S. Wittlin, J.L. Alonso-Gomez, W.B. Schweizer, A. Bacher, and F. Diederich. ChemMedChem. <b>[Epub ahead of print]</b>; PMID[20480490].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20498327">Berberine-INF55 Hybrid Antimicrobials: Effects of Varying the Relative Orientation of the Berberine and INF55 Components.</a> Tomkiewicz, D., G. Casadei, J. Larkins-Ford, T.I. Moy, J. Garner, J.B. Bremner, F.M. Ausubel, K. Lewis, and M.J. Kelso. Antimicrob Agents Chemother. <b>[Epub ahead of print]</b>; PMID[20498327].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OI_0514-052710.</p>

    <p> </p>

    <p> </p>

    <h2 class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</h2>

    <p> </p>

    <p class="plaintext">7. <a href="http://apps.isiknowledge.com/InboundService.do?Func=Frame&amp;product=WOS&amp;action=retrieve&amp;SrcApp=Alerting&amp;UT=000271802801023&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;Init=Yes&amp;SrcAuth=Alerting&amp;mode=FullRecord&amp;customersID=Alerting&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">PRES 38-Design of bioavailable InhA inhibitors with activity against drug-resistant strains of Mycobacterium tuberculosis.</a> Khanna, A., C.W. AmEnde, N.N. Liu, S. Knudson, R.A. Slayden, and P.J. Tonge. SO ABSTRACTS OF PAPERS OF THE AMERICAN CHEMICAL SOCIETY LA English SN 0065-7727 PD APR 6 PY 2008 VL 235 GA 519XJ UT ISI:000271802801023 ER.</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277325500009&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis and antimicrobial activities of sulfonohydrazide-substituted 8-hydroxyquinoline derivative and its oxinates.</a> Dixit, R.B., S.F. Vanparia, T.S. Patel, C.L. Jagani, H.V. Doshi, and B.C. Dixit. Applied Organometallic Chemistry. 24(5): p. 408-413; ISI[000277325500009].</p>

    <p class="plaintext">[WOS]. OI_0514-052710.</p>

    <p> </p>

    <p>9. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277339000006&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Macrocyclic complexes: synthesis and characterization.</a> Singh, D. and K. Kumar. Journal of the Serbian Chemical Society. 75(4): p. 475-482; ISI[000277339000006].</p>

    <p class="plaintext">[WOS]. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277147400038&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Synthesis, Antimicrobial, and Anti-inflammatory Activities of Novel 5-(1-Adamantyl)-4-arylideneamino-3-mercapto-1,2,4-triazoles and Related Derivatives.</a> Al-Omar, M.A., E.S. Al-Abdullah, I.A. Shehata, E.E. Habib, T.M. Ibrahim, and A.A. El-Emam. Molecules. 15(4): p. 2526-2550; ISI[000277147400038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277157200007&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">The antifungal effect of six commercial extracts of Chilean propolis on Candida spp.</a> Herrera, C.L., M. Alvear, L. Barrientos, G. Montenegro, and L.A. Salazar. Ciencia E Investigacion Agraria. 37(1): p. 75-84; ISI[000277157200007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277250200011&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Antimycobacterial Metabolites from Plectranthus: Royleanone Derivatives against Mycobacterium tuberculosis Strains.</a> Rijo, P., M.F. Simoes, A.P. Francisco, R. Rojas, R.H. Gilman, A.J. Vaisberg, B. Rodriguez, and C. Moiteiro. Chemistry &amp; Biodiversity. 7(4): p. 922-932; ISI[000277250200011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277257400014&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Monooxovanadium(V) 2-phenylphenoxides: synthesis, characterization, and antimicrobial potential.</a> Sharma, N., M. Thakur, and S.C. Chaudhry. Journal of Coordination Chemistry. 63(6): p. 1071-1079; ISI[000277257400014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277246300034&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Novel 1,3,5-triphenyl-2-pyrazolines as anti-infective agents.</a> Sivakumar, P.M., S.P. Seenivasan, V. Kumar, and M. Doble. Bioorganic &amp; Medicinal Chemistry Letters. 20(10): p. 3169-3172; ISI[000277246300034].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://apps.isiknowledge.com/InboundService.do?product=WOS&amp;action=retrieve&amp;SrcApp=EndNote&amp;UT=000277208900021&amp;SID=4Fh3IEmGMjHkmNcgflk&amp;SrcAuth=ResearchSoft&amp;mode=FullRecord&amp;customersID=ResearchSoft&amp;DestFail=http%3A%2F%2Faccess.isiproducts.com%2Fcustom_images%2Fwok_failed_auth.html">Binding and Inhibition of Copper Ions to RecA Inteins from Mycobacterium tuberculosis.</a> Zhang, L.Y., N. Xiao, Y. Pan, Y.C. Zheng, Z.Y. Pan, Z.F. Luo, X.L. Xu, and Y.Z. Liu. Chemistry-a European Journal. 16(14): p. 4297-4306; ISI[000277208900021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0514-052710.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
