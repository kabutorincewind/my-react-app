

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-152.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Aaxt0wve8g6nE1hpG7R+HBKBS1Q7hmZ+I/I0UjoBoUxuekc6M2b3upBsRSo9Uo2DvNKc0Op8SSgz0IWSCz9BHca1wVeVAhfSP/hdRAk7UTI3JvvajxK6UqxF+9Q=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="ED5234E1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-152-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     61529   DMID-LS-152; WOS-DMID-8/17/2007</p>

    <p class="memofmt1-2">          Mxa-Independent Inhibition of Hantaan Virus Replication Induced by Type I and Type Ii Interferon in Vitro</p>

    <p>          Oelschlegel, R., Kruger, D., and Rang, A.</p>

    <p>          Virus Research  <b>2007</b>.  127(1): 100-105</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247300900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247300900013</a> </p><br />

    <p>2.     61530   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Two new alkaloids and active anti-hepatitis B virus constituents from Hypserpa nitida</p>

    <p>          Cheng, Pi, Ma, Yun-bao, Yao, Shu-ying, Zhang, Quan, Wang, En-jun, Yan, Meng-hong, Zhang, Xue-mei, Zhang, Feng-xue, and Chen, Ji-jun</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PF6B40-4/2/15c72d5a568c6f4c577cc20e4b22c0de">http://www.sciencedirect.com/science/article/B6TF9-4PF6B40-4/2/15c72d5a568c6f4c577cc20e4b22c0de</a> </p><br />

    <p>3.     61531   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p><b>          Antiviral activity and hepatoprotection by heme oxygenase-1 in hepatitis B virus infection</b> </p>

    <p>          Protzer, Ulrike, Seyfried, Stefan, Quasdorff, Maria, Sass, Gabriele, Svorcova, Miriam, Webb, Dennis, Bohne, Felix, Hosel, Marianna, Schirmacher, Peter, and Tiegs, Gisa</p>

    <p>          Gastroenterology <b>2007</b>.  In Press, Accepted Manuscript: 202</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WFX-4P8GWVT-4/2/5ff0e31e5a2971680ace3d0ac516670e">http://www.sciencedirect.com/science/article/B6WFX-4P8GWVT-4/2/5ff0e31e5a2971680ace3d0ac516670e</a> </p><br />

    <p>4.     61532   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Current research on drugs and vaccines for fighting bird flu</p>

    <p>          Wiwanitkit, Viroj</p>

    <p>          Transactions of the Royal Society of Tropical Medicine and Hygiene <b>2007</b>.  In Press, Corrected Proof: 5652</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B75GP-4P8H8CS-3/2/4d4bbf4829361a0e334cc7f9d7f14c07">http://www.sciencedirect.com/science/article/B75GP-4P8H8CS-3/2/4d4bbf4829361a0e334cc7f9d7f14c07</a> </p><br />

    <p>5.     61533   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Practical and efficient method for amino acid derivatives containing [beta]-quaternary center: application toward synthesis of hepatitis C virus NS3 serine protease inhibitors</p>

    <p>          Arasappan, Ashok, Venkatraman, Srikanth, Padilla, Angela I, Wu, Wanli, Meng, Tao, Jin, Yan, Wong, Jesse, Prongay, Andrew, Girijavallabhan, Viyyoor, and George Njoroge, F</p>

    <p>          Tetrahedron Letters <b>2007</b>.  48(36): 6343-6347</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4P4NPF6-6/2/c22b4e16602879acba4a13e745a25e89">http://www.sciencedirect.com/science/article/B6THS-4P4NPF6-6/2/c22b4e16602879acba4a13e745a25e89</a> </p><br />
    <br clear="all">

    <p>6.     61534   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Structure-based Design, Synthesis and Biological Evaluation of Peptidomimetic SARS-CoV 3CLpro Inhibitors</p>

    <p>          Ghosh, Arun K, Xi, Kai, Grum-Tokars, Valerie, Xu, Xiaoming, Ratia, Kiira, Fu, Wentao, Houser, Katherine V, Baker, Susan C, Johnson, Michael E, and Mesecar, Andrew D</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2007</b>.  In Press, Accepted Manuscript: 1295</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4PG117F-F/2/d08a67439c48ef29f5e232a427a869d7">http://www.sciencedirect.com/science/article/B6TF9-4PG117F-F/2/d08a67439c48ef29f5e232a427a869d7</a> </p><br />

    <p>7.     61535   DMID-LS-152; EMBASE-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Towards our understanding of SARS-CoV, an emerging and devastating but quickly conquered virus</p>

    <p>          Feng, Youjun and Gao, George F</p>

    <p>          Comparative Immunology, Microbiology and Infectious Diseases <b>2007</b>.  In Press, Corrected Proof: 179</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T5H-4P77G8Y-1/2/58ce82e31a88bf92876806f0abba019e">http://www.sciencedirect.com/science/article/B6T5H-4P77G8Y-1/2/58ce82e31a88bf92876806f0abba019e</a> </p><br />

    <p>8.     61536   DMID-LS-152; WOS-DMID-8/17/2007</p>

    <p class="memofmt1-2">          Influenza Virus a (H5n1): a Pandemic Risk?</p>

    <p>          Babakir-Mina, M. <i>et al.</i></p>

    <p>          New Microbiologica <b>2007</b>.  30(2): 65-78</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247405600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247405600001</a> </p><br />

    <p>9.     61537   DMID-LS-152; WOS-DMID-8/17/2007</p>

    <p class="memofmt1-2">          Acetylsalicylic Acid (Asa) Blocks Influenza Virus Propagation Via Its Nf-Kappa B-Inhibiting Activity</p>

    <p>          Mazur, I. <i>et al.</i></p>

    <p>          Cellular Microbiology <b>2007</b>.  9(7): 1683-1694</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247319000007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000247319000007</a> </p><br />

    <p>10.   61538   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          The Identification of Novel Small Molecule Inhibitors of West Nile Virus Infection</p>

    <p>          Noueiry, AO, Olivo, PD, Slomczynska, U, Zhou, Y, Buscher, B, Geiss, B, Engle, M, Roth, RM, Chung, KM, Samuel, M, and Diamond, MS</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17715228&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17715228&amp;dopt=abstract</a> </p><br />

    <p>11.   61539   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Cyclophilin inhibitors in hepatitis C viral infection</p>

    <p>          Flisiak, R, Dumont, JM, and Crabbe, R</p>

    <p>          Expert Opin Investig Drugs <b>2007</b>.  16(9): 1345-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17714021&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17714021&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   61540   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Effect of ribavirin on the hepatitis C virus (JFH-1) and its correlation with interferon sensitivity</p>

    <p>          Brochot, E, Duverlie, G, Castelain, S, Morel, V, Wychowski, C, Dubuisson, J, and Francois, C</p>

    <p>          Antivir Ther <b>2007</b>.  12(5): 805-13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713164&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713164&amp;dopt=abstract</a> </p><br />

    <p>13.   61541   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Resistance profiling of hepatitis C virus protease inhibitors using full-length NS3</p>

    <p>          Dahl, G, Sandstrom, A, Akerblom, E, and Danielson, UH</p>

    <p>          Antivir Ther <b>2007</b>.  12(5): 733-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713156&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713156&amp;dopt=abstract</a> </p><br />

    <p>14.   61542   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Herpes simplex virus: treatment with antimicrobial peptides</p>

    <p>          Kovalchuk, LV, Gankovskaya, LV, Gankovskaya, OA, and Lavrov, VF</p>

    <p>          Adv Exp Med Biol <b>2007</b>.  601: 369-76</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713025&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17713025&amp;dopt=abstract</a> </p><br />

    <p>15.   61543   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Biological evaluation of anti-influenza viral activity of semi-synthetic catechin derivatives</p>

    <p>          Song, JM, Park, KD, Lee, KH, Byun, YH, Park, JH, Kim, SH, Kim, JH, and Seong, BL</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709148&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17709148&amp;dopt=abstract</a> </p><br />

    <p>16.   61544   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Analogue inhibitors by modifying oseltamivir based on the crystal neuraminidase structure for treating drug-resistant H5N1 virus</p>

    <p>          Du, QS, Wang, SQ, and Chou, KC</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17707775&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17707775&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>17.   61545   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Neuraminidase inhibitor resistance in influenza viruses</p>

    <p>          Reece, PA</p>

    <p>          J Med Virol <b>2007</b>.  79(10): 1577-86</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17705169&amp;dopt=abstract</a> </p><br />

    <p>18.   61546   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Physicochemical properties of the potent influenza neuraminidase inhibitor, peramivir (BCX-1812)</p>

    <p>          Viegas, TX, Goodin, RR, Winkle, LV, and Hawthorne, RB</p>

    <p>          J Pharm Biomed Anal <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703908&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703908&amp;dopt=abstract</a> </p><br />

    <p>19.   61547   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          alpha -Defensin Inhibits Influenza Virus Replication by Cell-Mediated Mechanism(s)</p>

    <p>          Salvatore, M, Garcia-Sastre, A, Ruchala, P, Lehrer, RI, Chang, T, and Klotman, ME</p>

    <p>          J Infect Dis <b>2007</b>.  196(6): 835-43</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703413&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17703413&amp;dopt=abstract</a> </p><br />

    <p>20.   61548   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus infection and expression in vitro and in vivo by recombinant adenovirus expressing short hairpin RNA</p>

    <p>          Sakamoto, N, Tanabe, Y, Yokota, T, Satoh, K, Sekine-Osajima, Y, Nakagawa, M, Itsui, Y, Tasaka, M, Sakurai, Y, Cheng-Hsin, C, Yano, M, Ohkoshi, S, Aoyagi, Y, Maekawa, S, Enomoto, N, Kohara, M, and Watanabe, M</p>

    <p>          J Gastroenterol Hepatol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17683479&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17683479&amp;dopt=abstract</a> </p><br />

    <p>21.   61549   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Structural proteomics of the SARS coronavirus: a model response to emerging infectious diseases</p>

    <p>          Bartlam, M, Xu, Y, and Rao, Z</p>

    <p>          J Struct Funct Genomics <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17680348&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17680348&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>22.   61550   DMID-LS-152; PUBMED-DMID-8/27/2007</p>

    <p class="memofmt1-2">          Hippomanin a from acetone extract of Phyllanthus urinaria inhibited HSV-2 but not HSV-1 infection in vitro</p>

    <p>          Yang, CM, Cheng, HY, Lin, TC, Chiang, LC, and Lin, CC</p>

    <p>          Phytother Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17661333&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17661333&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
