

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-04-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1D79pG3E9AOKRxIAhpvsWOCVkGDGXBN9SGTWaG93g6FoADJlzjFzuOzKdqdGRkuw9CMNMQOZGtiTS0Tyfgex/9LySKcWU6ZfkRk0XRgD1WOEwvg9tH/oN4/jU7k=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B1A5C9E4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: April 11 - April 24, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23636868">Retinazone Inhibits Certain Blood-borne Human Viruses Including Ebola Virus Zaire<i>.</i></a> Kesel, A.J., Z. Huang, M.G. Murray, M.N. Prichard, L. Caboni, D.K. Nevin, D. Fayne, D.G. Lloyd, M.A. Detorio, and R.F. Schinazi. Antiviral Chemistry &amp; Chemotherapy, 2014. 23(5): p. 197-215; PMID[23636868].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.  </p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24742271">Inhibitory Effect of Kaolin Minerals Compound against Hepatitis C Virus in Huh-7 Cell Lines<i>.</i></a> Ali, L., M. Idrees, M. Ali, A. Hussain, I.U. Rehman, A. Ali, S.A. Iqbal, and E.H. Kamel. BMC Research Notes, 2014. 7(1): p. 247; PMID[24742271].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.  </p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24656612">Identification of a Novel Series of Potent HCV NS5B Site I Inhibitors<i>.</i></a> Eastman, K.J., Z. Yang, J.A. Bender, K. Mosure, J.A. Lemm, N.A. Meanwell, S.B. Roberts, J. Knipe, and J.F. Kadow. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(8): p. 1993-1997; PMID[24656612].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.  </p> <br />

    <p class="plaintext">4. <a href="file:///C:/AppData/Local/Temp/24732793">The Acyclic Retinoid Peretinoin Inhibits Hepatitis C Virus Replication and Infectious Virus Release in Vitro<i>.</i></a> Shimakami, T., M. Honda, T. Shirasaki, R. Takabatake, F. Liu, K. Murai, T. Shiomoto, M. Funaki, D. Yamane, S. Murakami, S.M. Lemon, and S. Kaneko. Scientific Reports, 2014. 4: p. 4688; PMID[24732793].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.  </p> <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24650713">Development of Bivalent Oleanane-type Triterpenes as Potent HCV Entry Inhibitors<i>.</i></a> Yu, F., Y. Peng, Q. Wang, Y. Shi, L. Si, H. Wang, Y. Zheng, E. Lee, S. Xiao, M. Yu, Y. Li, C. Zhang, H. Tang, C. Wang, L. Zhang, and D. Zhou. European Journal of Medicinal Chemistry, 2014. 77: p. 258-268; PMID[24650713].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24740100">A Potent Combination Microbicide That Targets SHIV-RT, HSV-2 and HPV<i>.</i></a> Kizima, L., A. Rodriguez, J. Kenney, N. Derby, O. Mizenina, R. Menon, S. Seidor, S. Zhang, K. Levendosky, N. Jean-Pierre, P. Pugach, G. Villegas, B.E. Ford, A. Gettie, J. Blanchard, M. Piatak, Jr., J.D. Lifson, G. Paglini, N. Teleshova, T.M. Zydowsky, M. Robbiani, and J.A. Fernandez-Romero. Plos One, 2014. 9(4): p. e94547; PMID[24740100].</p>

    <p class="plaintext"><b>[PubMed].</b> OV_0411-042414.  </p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333006200006">Potent Hepatitis C Virus NS5A Inhibitors Containing a Benzidine Core<i>.</i></a> Bae, I.H., J.K. Choi, C. Chough, S.J. Keum, H. Kim, S.K. Jang, and B.M. Kim. ACS Medicinal Chemistry Letters, 2014. 5(3): p. 255-258; ISI[000333006200006].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0411-042414.</p> <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333006200004">Discovery of SCH 900188: A Potent Hepatitis C Virus NS5B Polymerase Inhibitor Prodrug as a Development Candidate<i>.</i></a> Chen, K.X., S. Venkatraman, G.N. Anilkumar, Q.B. Zeng, C.A. Lesburg, B. Vibulbhan, F. Velazquez, T.Y. Chan, F. Bennet, Y.H. Jiang, P. Pinto, Y.H. Huang, O. Selyutin, S. Agrawal, H.C. Huang, C. Li, K.C. Cheng, N.Y. Shih, J.A. Kozlowski, S.B. Rosenblum, and F.G. Njoroge. ACS Medicinal Chemistry Letters, 2014. 5(3): p. 244-248; ISI[000333006200004].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0411-042414.</p> <br />

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333006200003">Discovery of Thienoimidazole-based HCV NS5A Genotype 1a and 1b Inhibitors<i>.</i></a> Giroux, S., J.W. Xu, T.J. Reddy, M. Morris, K.M. Cottrell, C. Cadilhac, J.A. Henderson, O. Nicolas, D. Bilimoria, F. Denis, N. Mani, N. Ewing, R. Shawgo, L. L&#39;Heureux, S. Selliah, L. Chan, N. Chauret, F. Berlioz-Seux, M.N. Namchuk, A.L. Grillot, Y.L. Bennani, S.K. Das, and J.P. Maxwell. ACS Medicinal Chemistry Letters, 2014. 5(3): p. 240-243; ISI[000333006200003].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0411-042414.</p> <br />

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333006200005">Novel Peptidomimetic Hepatitis C Virus NS3/4A Protease Inhibitors Spanning the P2-P1 &#39; Region<i>.</i></a> Lampa, A.K., S.M. Bergman, S.S. Gustafsson, H. Alogheli, E.B. Akerblom, G.G. Lindeberg, R.M. Svensson, P. Artursson, U.H. Danielson, A. Karlen, and A. Sandstrom. ACS Medicinal Chemistry Letters, 2014. 5(3): p. 249-254; ISI[000333006200005].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0411-042414.</p> <br />

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000333006200008">Novel Quinoline-based P2-P4 Macrocyclic Derivatives as Pan-genotypic HCV NS3/4A Protease Inhibitors<i>.</i></a> Shah, U., C. Jayne, S. Chackalamannil, F. Velaazquez, Z.Y. Guo, A. Buevich, J.A. Howe, R. Chase, A. Soriano, S. Agrawal, M.T. Rudd, J.A. McCauley, N.J. Liverton, J. Romano, K. Bush, P.J. Coleman, C. Grise-Bard, M.C. Brochu, S. Charron, V. Aulakh, B. Bachand, P. Beaulieu, H. Zaghdane, S. Bhat, Y.X. Han, J.P. Vacca, I.W. Davies, A.E. Weber, and S. Venkatraman. ACS Medicinal Chemistry Letters, 2014. 5(3): p. 264-269; ISI[000333006200008].</p>

    <p class="plaintext"><b>[WOS].</b> OV_0411-042414.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
