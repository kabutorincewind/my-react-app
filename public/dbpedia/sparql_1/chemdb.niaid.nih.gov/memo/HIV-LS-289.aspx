

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-289.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="q9L57oZ2zEWfb9eJGRqY7QZ+DaD98RVxxLz5wYWInb/yZPhnVvUNYp0Ciq3G/yg5K0C5Yw6Ssp5gGJbpNmnNq72K54QpBIMhGcTLRWS/ZW4LGFN2t1xbljexErU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="15CDEE89" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - HIV-LS-289-MEMO</b>  </p>

<p>    1.    42773   HIV-LS-289; PUBMED-HIV-2/11/2004 </p>

<p class="memofmt1-2">           Purification of a novel low-molecular-mass laccase with HIV-1 reverse transcriptase inhibitory activity from the mushroom Tricholoma giganteum</p>

<p>           Wang, HX and Ng, TB</p>

<p>           Biochem Biophys Res Commun 2004. 315(2):  450-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766229&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766229&amp;dopt=abstract</a> </p><br />

<p>    2.    42774   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p class="memofmt1-2">           Chemokine receptor antagonists as HIV entry inhibitors</p>

<p>           Anon</p>

<p>           Expert Opinion on Therapeutic Patents 2004. 14(2): 251-255</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    3.    42775   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Computer-Aided Design, Synthesis, and Anti-HIV-1 Activity in Vitro of 2-Alkylamino-6-[1-(2,6-difluorophenyl)alkyl]-3,4-dihydro-5-alkylpyrimidin- 4(3H)- ones as Novel Potent Non-Nucleoside Reverse Transcriptase Inhibitors, Also Active Against the Y181C Variant</p>

<p>           Ragno, R, Mai, A, Sbardella, G, Artico, M, Massa, S, Musiu, C, Mura, M, Marturana, F, Cadeddu, A, and La, Colla P</p>

<p>           J Med Chem 2004. 47(4): 928-934</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761194&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14761194&amp;dopt=abstract</a> </p><br />

<p>    4.    42776   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Anti-HIV inhibitors based on nucleic acids: emergence of aptamers as potent antivirals</p>

<p>           Joshi, PJ, Fisher, TS, and Prasad, VR</p>

<p>           Curr Drug Targets Infect Disord 2003. 3(4): 383-400</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754437&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754437&amp;dopt=abstract</a> </p><br />

<p>    5.    42777   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Structural and thermodynamic basis of resistance to HIV-1 protease inhibition: implications for inhibitor design</p>

<p>           Velazquez-Campoy, A, Muzammil, S, Ohtaka, H, Schon, A, Vega, S, and Freire, E</p>

<p>           Curr Drug Targets Infect Disord 2003. 3(4): 311-28</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754432&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754432&amp;dopt=abstract</a> </p><br />

<p>    6.    42778   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Preparation of 5-substituted 1,2,4-triazole-3-carboxylic acid esters as intermediates for HIV integrase inhibitors via acylamidrazones</b>((Shionogi and Co., Ltd. Japan)</p>

<p>           Nagai, Masahiko, Noguchi, Koichi, and Uenaka, Masaaki</p>

<p>           PATENT: JP 2004018480 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2002-46750; PP: 30 pp.</p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>    7.    42779   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           A review of HIV-1 resistance to the nucleoside and nucleotide inhibitors</p>

<p>           Shulman, N and Winters, M</p>

<p>           Curr Drug Targets Infect Disord 2003. 3(4): 273-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754429&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14754429&amp;dopt=abstract</a> </p><br />

<p>    8.    42780   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Synthesis of new 2,3-diaryl-1,3-thiazolidin-4-ones as anti-HIV agents</p>

<p>           Rao, A, Balzarini, J, Carbone, A, Chimirri, A, De Clercq, E, Monforte, AM, Monforte, P, Pannecouque, C, and Zappala, M</p>

<p>           Farmaco 2004. 59(1): 33-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14751314&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14751314&amp;dopt=abstract</a> </p><br />

<p>    9.    42781   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p class="memofmt1-2">           Novel nevirapine-like inhibitors with improved activity against NNRTI-resistant HIV: 8-heteroarylthiomethyldipyridodiazepinone derivatives</p>

<p>           Yoakim, C, Bonneau, PR, Deziel, R, Doyon, L, Duan, J, Guse, I, Landry, S, Malenfant, E, Naud, J, Ogilvie, WW, O&#39;Meara, JA, Plante, R, Simoneau, B, Thavonekham, B, Bos, M, and Cordingley, MG</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(3): 739-742</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  10.    42782   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Anti-HIV-1 Protostane Triterpenes and Digeranylbenzophenone from Trunk Bark and Stems of Garcinia speciosa</p>

<p>           Rukachaisirikul, V, Pailee, P, Hiranrat, A, Tuchinda, P, Yoosook, C, Kasisit, J, Taylor, WC, and Reutrakul, V</p>

<p>           Planta Med 2003. 69(12): 1141-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14750032&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14750032&amp;dopt=abstract</a> </p><br />

<p>  11.    42783   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Use of chloroquine, hydroxychloroquine, and 4 aminoquinolinic derivatives to obtain a drug for the anti retroviral therapy, active towards HIV sensitive strains and towards HIV strains which are resistant to both nucleosidic and non-nucleosidic reverse transcriptase inhibitors and to proteases inhibitors</b>((Valpharma S. A., San Marino)</p>

<p>           Valducci, Roberto, Alighieri, Tiziano, and Avanessian, Serozh</p>

<p>           PATENT: EP 1374867 A1;  ISSUE DATE: 20040102</p>

<p>           APPLICATION: 2003-13823; PP: 17 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  12.    42784   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Prodrugs of HIV protease inhibitors-saquinavir, indinavir and nelfinavir-derived from diglycerides or amino acids: synthesis, stability and anti-HIV activity</p>

<p>           Gaucher, B, Rouquayrol, M, Roche, D, Greiner, J, Aubertin, AM, and Vierling, P</p>

<p>           Org Biomol Chem 2004. 2(3): 345-57</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747863&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747863&amp;dopt=abstract</a>  </p><br />

<p>  13.    42785   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           Influence of primate lentiviral Vif and proteasome inhibitors on human immunodeficiency virus type 1 virion packaging of APOBEC3G</p>

<p>           Liu, B, Yu, X, Luo, K, Yu, Y, and Yu, XF</p>

<p>           J Virol 2004. 78(4): 2072-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747572&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14747572&amp;dopt=abstract</a> </p><br />

<p>  14.    42786   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           New anti-HIV protease inhibitors provide more treatment options</p>

<p>           Nadler, J</p>

<p>           AIDS Patient Care STDS 2003. 17(11):  551-64</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14746663&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14746663&amp;dopt=abstract</a> </p><br />

<p>  15.    42787   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Preparation of steroid-containing macrolide glycopeptides as carriers for steroid/non-steroid anti-inflammatory, antineoplastic, and antiviral active molecules</b> ((Pliva D.D., Croatia)</p>

<p>           Mercep, Mladen, Mesic, Milan, Tomaskovic, Linda, and Markovic, Stribor</p>

<p>           PATENT: WO 2004005313 A2;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 96 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    42788   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           APOBEC-mediated DNA deamination as a mechanism of innate immunity to retroviral infection</b>  ((UK))</p>

<p>           Malim, Michael H, Sheehy, Ann M, Harris, Reuben S, Bishop, Kate N, Neuberger, Michael S, Gaddis, Nathan C, and Simon, James HM</p>

<p>           PATENT: US 20040009951 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 46; PP: 35 pp., which</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    42789   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Vaccines comprising inactivated HIV-pulsed antigen presenting cells and HIV protease inhibitor for treatment of AIDS</b>((Fr.))</p>

<p>           Andrieu, Jean-Marie and Lu, Louis</p>

<p>           PATENT: US 20040009194 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003-10537; PP: 29 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  18.    42790   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Method for the treatment or prevention of virus infection using polybiguanide-based compounds</b> ((USA))</p>

<p>           Labib, Mohamed E and Stockel, Richard F</p>

<p>           PATENT: US 20040009144 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003-42540; PP: 30 pp.</p>

<p>&nbsp;           <!--HYPERLNK:-->  </p>

<p>  19.    42791   HIV-LS-289; PUBMED-HIV-2/11/2004</p>

<p class="memofmt1-2">           In vivo imaging of HIV protease activity in amplicon vector-transduced gliomas</p>

<p>           Shah, K, Tung, CH, Chang, CH, Slootweg, E, O&#39;Loughlin, T, Breakefield, XO, and Weissleder, R</p>

<p>           Cancer Res 2004. 64(1): 273-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14729634&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14729634&amp;dopt=abstract</a> </p><br />

<p>  20.    42792   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Preparation of acyclic nucleosides and nucleotides as antiviral agents</b>((Taiwan))</p>

<p>           Hakimelahi, Gholam Hossein</p>

<p>           PATENT: US 2004002475 A1;  ISSUE DATE: 20040101</p>

<p>           APPLICATION: 2002-52806; PP: 21 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    42793   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Inhibition of virus anchorage by RGG domain of a human cell surface-expressed protein, nucleolin, and uses for antiviral therapy</b>((Fr.))</p>

<p>           Hovanessian, Ara V and Briand, Jean-paul</p>

<p>           PATENT: US 2004002457 A1;  ISSUE DATE: 20040101</p>

<p>           APPLICATION: 2003-56889; PP: 48 pp., which</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  22.    42794   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Preparation of ring-expanded nucleosides and nucleotides as virucides and bactericides</b>((Nabi, USA and University of Maryland Baltimore County))</p>

<p>           Hosmane, Ramachandra S and Sood, Ramesh K</p>

<p>           PATENT: US 6677310 B1;  ISSUE DATE: 20040113</p>

<p>           APPLICATION: 99-33159; PP: 51 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    42795   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Determining the replication capacity of viruses resistant to non-nucleoside inhibitors of reverse transcriptase in selection of drug therapy</b>((Virologic, Inc. USA)</p>

<p>           Huang, W, Wrin, Mary T, Gamarnik, Andrea, Beauchaine, J, Whitcomb, JM, Petropoulos, Christos J, and Parkin, Neil T</p>

<p>           PATENT: WO 2004003513 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 54 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    42796   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           New mutational profiles in HIV-1 reverse transcriptase correlated with phenotypic drug resistance</b> ((Tibotec Pharmaceuticals, Ltd. Ire.)</p>

<p>           Azijn, Hilde, De Bethune, Marie-Pierre, and Vingerhoets, Johan Hendrika Jozef</p>

<p>           PATENT: WO 2004003227 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 18 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    42797   HIV-LS-289; SCIFINDER-HIV-2/4/2004</p>

<p><b>           Preparation of 2,4-disubstituted pyridine N-oxides useful as HIV reverse transcriptase inhibitors</b> ((Bristol-Myers Squibb Company, USA)</p>

<p>           Rodgers, James D and Wang, Haisheng</p>

<p>           PATENT: WO 2004002410 A2;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 71 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    42798   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Discovery of a Tat HIV-1 inhibitor through computer-aided drug design</p>

<p>           Esquieu, D, Peloponese, JM, Opi, S, Gregoire, C, De Mareuil, J, Watkins, J, Campbell, G, Dunot, JP, Sturgis, J, Witvrouw, M, Pannecouque, C, De Clercq, E, Montembault, M, Giang, VT, Villieras, M, Fargeas, V, Lebreton, J, and Loret, EP</p>

<p>           SPECTROSC-INT J 2003. 17(4): 639-645</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188061500001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188061500001</a> </p><br />

<p>  27.    42799   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Monoamine oxidase inhibition and CNS immunodeficiency infection</p>

<p>           Koutsilieri, E, Scheller, C, ter, Meulen V, and Riederer, P</p>

<p>           NEUROTOXICOLOGY 2004. 25(1-2): 267-270</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187910600029">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187910600029</a> </p><br />

<p>  28.    42800   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Simian virus 40-based replication of catalytically inactive human immunodeficiency virus type 1 integrase mutants in nonpermissive T cells and monocyte-derived macrophages</p>

<p>           Lu, R, Nakajima, N, Hofmann, W, Benkirane, M, Teh-Jeang, K, Sodroski, J, and Engelman, A</p>

<p>           J VIROL 2004. 78(2): 658-668</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187957700012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187957700012</a> </p><br />

<p>  29.    42801   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           The chemokine receptor CXCR4 and not the N-methyl-D-aspartate receptor mediates gp120 neurotoxicity in cerebellar granule cells</p>

<p>           Bachis, A and Mocchetti, I</p>

<p>           J NEUROSCI RES 2004. 75(1): 75-82</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187942100008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187942100008</a> </p><br />

<p>  30.    42802   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Improved breadth and potency of an HIV-1-neutralizing human single-chain antibody by random mutagenesis and sequential antigen panning</p>

<p>           Zhang, MY, Shu, YU, Rudolph, D, Prabakaran, P, Labrijn, AF, Zwick, MB, Lal, RB, and Dimitrov, DS</p>

<p>           J MOL BIOL 2004. 335(1): 209-219</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187879600016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187879600016</a> </p><br />

<p>  31.    42803   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           High attenuation and immunogenicity of a simian immunodeficiency virus expressing a proteolysis-resistant inhibitor of NF-kappa B</p>

<p>           Quinto, I, Puca, A, Greenhouse, J, Silvera, P, Yalley-Ogunro, J, Lewis, MG, Palmieri, C, Trimboli, F, Byrum, R, Adelsberger, J, Venzon, D, Chen, XN, and Scala, G</p>

<p>           J BIOL CHEM 2004. 279(3): 1720-1728</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188005700021">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188005700021</a> </p><br />

<p>  32.    42804   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Pyridoxal 5 &#39;-phosphateas a novel weapon against autoimmunity and transplant rejection</p>

<p>           Namazi, MR</p>

<p>           FASEB J 2003. 17(15): 2184-2186</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188067500038">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188067500038</a> </p><br />

<p>  33.    42805   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Fullerene derivatives: an attractive tool for biological applications</p>

<p>           Bosi, S, da Ros, T, Spalluto, G, and Prato, M</p>

<p>           EUR J MED CHEM 2003. 38(11-12): 913-923</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188000400001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188000400001</a>  </p><br />

<p>  34.    42806   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Mechanisms of internalization and recycling of the chemokine receptor, CCR5</p>

<p>           Mueller, A and Strange, PG</p>

<p>           EUR J BIOCHEM  2004. 271(2): 243-252</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187958900002">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187958900002</a> </p><br />

<p>  35.    42807   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Surface nucleolin participates in both the binding and endocytosis of lactoferrin in target cells</p>

<p>           Legrand, D, Vigie, K, Said, EA, Elass, E, Masson, M, Slomianny, MC, Carpentier, M, Briand, JP, Mazurier, J, and Hovanessian, AG</p>

<p>           EUR J BIOCHEM  2004. 271(2): 303-317</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187958900008">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187958900008</a> </p><br />

<p>  36.    42808   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Evolving therapeutic paradigms for HIV and HCV</p>

<p>           Molla, A and Kohlbrenner, W</p>

<p>           CURR OPIN BIOTECH 2003. 14(6): 634-640</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187924100012">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187924100012</a> </p><br />

<p>  37.    42809   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Resistance towards exonucleases of dinucleotides with stereochemically altered internucleotide phosphate bonds</p>

<p>           Nair, V and Pal, S</p>

<p>           BIOORG MED CHEM LETT 2004. 14(1): 289-291</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000057">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187862000057</a> </p><br />

<p>  38.    42810   HIV-LS-289; WOS-HIV-2/1/2004</p>

<p class="memofmt1-2">           Purification of glysojanin, an antifungal protein, from the black soybean Glycine soja</p>

<p>           Ngai, PHK and Ng, TB</p>

<p>           BIOCHEM CELL BIOL 2003. 81(6): 387-394</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187992500005">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187992500005</a> </p><br />

<p>  39.    42811   HIV-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           The tat antagonist neomycin B hexa-arginine conjugate inhibits gp120-induced death of human neuroblastoma cells (1)</p>

<p>           Lapidot, A, Catani, MV, Corasaniti, MT, Ranalli, M, Amantea, D, Litovchick, A, and Melino, G</p>

<p>           J NEUROCHEM 2003. 87: 55</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187240200183">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000187240200183</a> </p><br />

<p>  40.    42812   HIV-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Antiretroviral activity of the anti-CD4 monoclonal antibody TNX-355 in patients infected with HIV type 1</p>

<p>           Kuritzkes, DR, Jacobson, J, Powderly, WG, Godofsky, E, DeJesus, E, Haas, F, Reimann, KA, Larson, JL, Yarbough, PO, Curt, V, and Shanahan, WR</p>

<p>           J INFECT DIS 2004. 189(2): 286-291</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188097900016">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188097900016</a> </p><br />

<p>  41.    42813   HIV-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           A novel approach to develop anti-HIV drugs: adapting non-nucleoside anticancer chemotherapeutics</p>

<p>           Sadaie, AR, Mayner, R, and Doniger, J</p>

<p>           ANTIVIR RES 2004. 61(1): 1-18</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188114900001">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188114900001</a>  </p><br />

<p>  42.    42814   HIV-LS-289; WOS-HIV-2/8/2004</p>

<p class="memofmt1-2">           Interaction of sulfonated anionic porphyrins with HIV glycoprotein gp120: photodamages revealed by inhibition of antibody binding to V3 and C5 domains</p>

<p>           Dairou, J, Vever-Bizet, C, and Brault, D</p>

<p>           ANTIVIR RES 2004. 61(1): 37-47</p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188114900004">http://publishorperish.nih.gov/isicgi/gateway/Gateway.cgi?KeyUT=000188114900004</a>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
