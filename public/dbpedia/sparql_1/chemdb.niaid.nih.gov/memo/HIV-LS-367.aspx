

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-367.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4EYpFvX11wd6rQ7bs+/3OLOZop5Lu/tpYpLJEJ4iDos7Go1sbMDHtsp9ewXG3X8Wsy/xsUB5XoA3eXbXB4t2/xlWHRQAjQssQbMWF0nniZOs6Kmb9hutjZVLxzw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="24ED6B5B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-367-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60069   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Hiv reverse transcriptase inhibitors</p>

    <p>          Saggar, Sandeep A, Sisko, John T, Tucker, Thomas J, Tynebor, Robert M, Su, Dai-Shi, and Anthony, Neville J</p>

    <p>          PATENT:  US <b>2007021442</b>  ISSUE DATE:  20070125</p>

    <p>          APPLICATION: 2006-29133  PP: 72pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60070   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          A Mutation in Alpha Helix 3 of CA Renders Human Immunodeficiency Virus Type 1 Cyclosporin A-Resistant and Dependent: Rescue by a Second-Site Substitution in a Distal Region of CA</p>

    <p>          Yang, R and Aiken, C</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267487&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267487&amp;dopt=abstract</a> </p><br />

    <p>3.     60071   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Simultaneous determination of 1,5-dicaffeoylquinic acid and its active metabolites in human plasma by liquid chromatography-tandem mass spectrometry for pharmacokinetic studies</p>

    <p>          Gu, R, Dou, G, Wang, J, Dong, J, and Meng, Z</p>

    <p>          J Chromatogr B Analyt Technol Biomed Life Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267301&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17267301&amp;dopt=abstract</a> </p><br />

    <p>4.     60072   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Safety and Pharmacokinetics of Brecanavir, a Novel HIV-1 Protease Inhibitor, Following Repeat Administration with and without Ritonavir in Healthy Adult Subjects</p>

    <p>          Reddy, SY, Ford, SL, Anderson, MT, Murray, SC, Ng-Cashin, J, and Johnson, MA</p>

    <p>          Antimicrob Agents Chemother <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17261626&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17261626&amp;dopt=abstract</a> </p><br />

    <p>5.     60073   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of aryl phosphate derivatives of d4T having activity against resistant HIV strains</p>

    <p>          Uckun, Fatih M </p>

    <p>          PATENT:  US <b>2007015733</b>  ISSUE DATE:  20070118</p>

    <p>          APPLICATION: 2006-27757  PP: 42pp., Cont.-in-part of U.S. Ser. No. 275,102.</p>

    <p>          ASSIGNEE:  (Parker Hughes Institute, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>6.     60074   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          HIV-1 integration is inhibited by stimulation of the VPAC2 neuroendocrine receptor</p>

    <p>          Bokaei, PB, Ma, XZ, Sakac, D, and Branch, DR</p>

    <p>          Virology <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257640&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17257640&amp;dopt=abstract</a> </p><br />

    <p>7.     60075   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Alkenyldiarylmethanes, fused analogs and syntheses thereof</p>

    <p>          Cushman, Mark S and Deng, Bo-Liang</p>

    <p>          PATENT:  WO <b>2007005531</b>  ISSUE DATE:  20070111</p>

    <p>          APPLICATION: 2006  PP: 72pp.</p>

    <p>          ASSIGNEE:  (Purdue Research Foundation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     60076   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 Infection of Human CD4+ T Cells by Microbial HSP70 and the Peptide Epitope 407-426</p>

    <p>          Babaahmady, K, Oehlmann, W, Singh, M, and Lehner, T</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251296&amp;dopt=abstract</a> </p><br />

    <p>9.     60077   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Carbohydrate-binding agents efficiently prevent dendritic cell-specific intercellular adhesion molecule-3-grabbing nonintegrin (DC-SIGN)-directed HIV-1 transmission to T lymphocytes</p>

    <p>          Balzarini, Jan, Van Herrewege, Yven, Vermeire, Kurt, Vanham, Guido, and Schols, Dominique</p>

    <p>          Molecular Pharmacology <b>2007</b>.  71(1): 3-11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60078   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Broad-spectrum anti-HIV Potential of a Peptide HIV-1 Entry Inhibitor</p>

    <p>          Cocklin, S, Gopi, H, Querido, B, Nimmagadda, M, Kuriakose, S, Cicala, C, Ajith, S, Baxter, S, Arthos, J, Martin-Garcia, J, and Chaiken, IM</p>

    <p>          J Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251295&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17251295&amp;dopt=abstract</a> </p><br />

    <p>11.   60079   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p><b>          Oxazolidinecarboxamides as HIV-1 protease inhibitors, and methods of making and using them</b> </p>

    <p>          Rana, Tariq M, Ali, Akbar, Cao, Hong, Sai, Kiran Kumar Reddy Ga, and Anjum, Saima Ghafoor</p>

    <p>          PATENT:  WO <b>2007002173</b>  ISSUE DATE:  20070104</p>

    <p>          APPLICATION: 2006  PP: 194pp., which which</p>

    <p>          ASSIGNEE:  (University of Massachusetts, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   60080   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Use of modified U1 snRNAs to inhibit HIV-1 replication</p>

    <p>          Sajic R, Lee K, Asai K, Sakac D, Branch D R, Upton C, and Cochrane A</p>

    <p>          Nucleic Acids Res <b>2007</b>.  35(1): 247-55.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60081   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          3&#39;-Azido-3&#39;-deoxythymidine-(5&#39;)-tetraphospho-(5&#39;)-adenosine, the product of ATP-mediated excision of chain-terminating AZTMP, is a potent chain-terminating substrate for HIV-1 reverse transcriptase</p>

    <p>          Dharmasena, S, Pongracz, Z, Arnold, E, Sarafianos, SG, and Parniak, MA</p>

    <p>          Biochemistry <b>2007</b>.  46(3): 828-36</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17223704&amp;dopt=abstract</a> </p><br />

    <p>14.   60082   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p><b>          Lysis of human immunodeficiency virus type 1 by a specific secreted human phospholipase A2</b> </p>

    <p>          Kim Jae-Ouk, Chakrabarti Bimal K, Guha-Niyogi Anuradha, Louder Mark K, Mascola John R, Ganesh Lakshmanan, and Nabel Gary J</p>

    <p>          J Virol <b>2007</b>.  81(3): 1444-50.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60083   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Susceptibility of HIV-1 non-B subtypes and recombinant variants to Enfuvirtide</p>

    <p>          Holguin, A, Faudon, JL, Labernardiere, JL, and Soriano, V</p>

    <p>          J Clin Virol <b>2007</b>.  38(2): 176-80</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196877&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17196877&amp;dopt=abstract</a> </p><br />

    <p>16.   60084   HIV-LS-367; PUBMED-HIV-2/5/2007</p>

    <p class="memofmt1-2">          Plasma adenosine deaminase activity among HIV1 Clade C seropositives: Relation to CD4 T cell population and antiretroviral therapy</p>

    <p>          Chittiprol, S, Satishchandra, P, Bhimasenarao, RS, Rangaswamy, GR, Sureshbabu, SV, Subbakrishna, DK, Satish, KS, Desai, A, Ravi, V, and Shetty, KT</p>

    <p>          Clin Chim Acta <b>2007</b>.  377(1-2): 133-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049340&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17049340&amp;dopt=abstract</a> </p><br />

    <p>17.   60085   HIV-LS-367; SCIFINDER-HIV-1/29/2007</p>

    <p class="memofmt1-2">          Preparation of phosphono-pent-2-en-1-yl nucleosides and analogs as antiviral and antitumor agents</p>

    <p>          Hostetler, Karl Y, Beadle, James R, and Choo, Hyunah</p>

    <p>          PATENT:  WO <b>2006137953</b>  ISSUE DATE:  20061228</p>

    <p>          APPLICATION: 2006  PP: 67pp.</p>

    <p>          ASSIGNEE:  (The Regents of the Univerisity of California, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   60086   HIV-LS-367; WOS-HIV-1/28/2007</p>

    <p class="memofmt1-2">          Design of anti-HIV Ligands by means of minimal topological difference (MTD) method</p>

    <p>          Duda-Seiman, C, Duda-Seiman, D, Dragos, D, Medeleanu, M, Careja, V, Putz, MV, Lacrama, AM, Chiriac, A, Nutiu, R, and Ciubotariu, D</p>

    <p>          INTERNATIONAL JOURNAL OF MOLECULAR SCIENCES <b>2006</b>.  7(11): 537-555, 19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243258800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243258800006</a> </p><br />

    <p>19.   60087   HIV-LS-367; WOS-HIV-1/28/2007</p>

    <p class="memofmt1-2">          Virostatics: A new class of anti-HIV drugs</p>

    <p>          Lori, F, Foli, A, Kelly, LM, and Lisziewicz, J</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2007</b>.  14(2): 233-241, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248700010</a> </p><br />

    <p>20.   60088   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Hyperbranched molecular structures with potential antiviral activity: Derivatives of 5,6-dihydroxyindole-2-carboxylic acid</p>

    <p>          Sechi, M, Casu, F, Campesi, I, Fiori, S, and Mariani, A</p>

    <p>          MOLECULES <b>2006</b>.  11(12): 968-977, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243402400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243402400004</a> </p><br />

    <p>21.   60089   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p><b>          Syntheses and anti-AIDS activities of polyrotaxane-3 &#39;-azido-3 &#39;-deoxythymidine conjugates</b> </p>

    <p>          Chung, I, Ha, CS, Lee, JK, Lee, CK, and Xie, D</p>

    <p>          MACROMOLECULAR RESEARCH <b>2006</b>.  14(6): 668-672, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243353700016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243353700016</a> </p><br />

    <p>22.   60090   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Synthesis of 2,3,4,7-tetrahydro-1H-azepines as privileged ligand scaffolds for the design of aspartic protease inhibitors via a ring-closing metathesis approach</p>

    <p>          Brass, S, Chan, NS, Gerlach, C, Luksch, T, Bottcher, J, and Diederich, WE</p>

    <p>          JOURNAL OF ORGANOMETALLIC CHEMISTRY <b>2006</b>.  691(24-25): 5406-5422, 17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243267200034">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243267200034</a> </p><br />

    <p>23.   60091   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Development of low molecular weight CXCR4 antagonists by exploratory structural tuning of cyclic tetra- and pentapeptide-scaffolds towards the treatment of HIV infection, cancer metastasis and rheumatoid arthritis</p>

    <p>          Tamamura, H, Tsutsumi, H, Masuno, H, and Fujii, N</p>

    <p>          CURRENT MEDICINAL CHEMISTRY <b>2007</b>.  14(1): 93-102, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248500006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243248500006</a> </p><br />

    <p>24.   60092   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Screening and selecting for optimized antiretroviral drugs: rising to the challenge of drug resistance</p>

    <p>          de Bethune, MP and Hertogs, K</p>

    <p>          CURRENT MEDICAL RESEARCH AND OPINION <b>2006</b>.  22(12): 2603-2612, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243238900030">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243238900030</a> </p><br />

    <p>25.   60093   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Short partially double-stranded oligodeoxynucleotide induces reverse transcriptase/RNase H-mediated cleavage of HIV RNA and contributes to abrogation of infectivity of virions</p>

    <p>          Matskevich, AA, Ziogas, A, Heinrich, J, Quast, SA, and Moelling, K</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2006</b>.  22(12): 1220-1230, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243398800003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243398800003</a> </p><br />

    <p>26.   60094   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Resistance mutational analysis of HIV type 1 subtype C among rural South African drug-naive patients prior to large-scale availability of antiretrovirals</p>

    <p>          Bessong, PO, Mphahlele, J, Choge, IA, Obi, LC, Morris, L, Hammarskjold, ML, and Rekosh, DM</p>

    <p>          AIDS RESEARCH AND HUMAN RETROVIRUSES <b>2006</b>.  22(12): 1306-1312, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243398800015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243398800015</a> </p><br />

    <p>27.   60095   HIV-LS-367; WOS-HIV-2/4/2007</p>

    <p class="memofmt1-2">          Structural studies of algal lectins with anti-HIV activity</p>

    <p>          Ziolkowska, NE and Wlodawer, A</p>

    <p>          ACTA BIOCHIMICA POLONICA <b>2006</b>.  53(4): 617-626, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243239400001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000243239400001</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
