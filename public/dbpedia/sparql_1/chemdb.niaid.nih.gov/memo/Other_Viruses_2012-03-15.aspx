

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2012-03-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="zlJmZvScAB3BdEdm94P/r1/IaHk647mBc4TtuqJxWBi3/ikbLHYpmsQDoAEZWqApp6uyYNPoFm5rBVa9eiQkSmUBypXovhU1WJlNcArWdCsVZS0lu1Avv/iYjCY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EDC19FE5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

    <h1 class="memofmt2-h1">March 2 - March 15, 2012</h1>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p> </p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22404728">Inhibition Effect Produced by Dominant Negative Mutant Fusion Protein PreS2-TLM-ScFv-HBcDN on HBV Replication in Vitro.</a> Liu, W.X., H.H. Zhu, W. Wu, J.L. He, and Z. Chen, Journal of Viral Hepatitis, 2012. 19(4): p. 295-300; PMID[22404728].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22387093">In Vitro Activity of Cepharanthine Hydrochloride against Clinical Wild-type and lamivudine-resistant Hepatitis B Virus Isolates.</a> Zhou, Y., Y. Wang, Y. Zhang, L. Zheng, X. Yang, N. Wang, J. Jiang, F. Ma, D. Yin, C. Sun, and Q. Wang, European Journal of Pharmacology, 2012, <b>[Epub ahead of print]</b>; PMID[22387093].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22414763">Novel Human SR-BI Antibodies Prevent Infection and Dissemination of HCV in Vitro and in Humanized Mice.</a> Lacek, K., K. Vercauteren, K. Grzyb, M. Naddeo, L. Verhoye, M.P. Slowikowski, S. Fafi-Kremer, A.H. Patel, T.F. Baumert, A. Folgori, G. Leroux-Roels, R. Cortese, P. Meuleman, and A. Nicosia, Journal of Hepatology, 2012, <b>[Epub ahead of print]</b>; PMID[22414763].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22412376">Metabolism of Phosphatidylinositol 4-Kinase IIIalpha-dependent PI4P is Subverted by HCV and is Targeted by a 4-Anilino quinazoline with Antiviral Activity.</a> Bianco, A., V. Reghellin, L. Donnici, S. Fenu, R. Alvarez, C. Baruffa, F. Peri, M. Pagani, S. Abrignani, P. Neddermann, and R. De Francesco, PLoS Pathogens, 2012. 8(3): p. e1002576; PMID[22412376].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22409723">Optimization of Potent Hepatitis C Virus NS3 Helicase Inhibitors Isolated from the Yellow Dyes Thioflavine S and Primuline.</a> Li, K., K.J. Frankowski, C.A. Belon, B. Neunswander, J. Ndjomou, A.M. Hanson, M.A. Shanahan, F.J. Schoenen, B.S. Blagg, J. Aube, and D.N. Frick, Journal of Medicinal Chemistry, 2012, <b>[Epub ahead of print]</b>; PMID[22409723].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22394195">Inhibition of Hepatitis C Virus NS3 Helicase by Manoalide.</a> Salam, K.A., A. Furuta, N. Noda, S. Tsuneda, Y. Sekiguchi, A. Yamashita, K. Moriishi, M. Nakakoshi, M. Tsubuki, H. Tani, J. Tanaka, and N. Akimitsu, Journal of Natural Products, 2012, <b>[Epub ahead of print]</b>; PMID[22394195].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22391672">Let-7b is a Novel regulator of Hepatitis C Virus Replication.</a> Cheng, J.C., Y.J. Yeh, C.P. Tseng, S.D. Hsu, Y.L. Chang, N. Sakamoto, and H.D. Huang, Cellular and Molecular Life Sciences : CMLS, 2012, <b>[Epub ahead of print]</b>; PMID[22391672].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22386565">Integrated Structure-based Activity Prediction Model of Benzothiadiazines on Various Genotypes of HCV NS5B Polymerase (1a, 1b and 4) and Its Application in the Discovery of New Derivatives.</a> Ismail, M.A., D.A. Abou El Ella, K.A. Abouzid, and A.H. Mahmoud, Bioorganic &amp; Medicinal Chemistry, 2012. 20(7): p. 2455-2478; PMID[22386565].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22383290">Discovery of N-Arylalkyl-3-hydroxy-4-oxo-3,4-dihydroquinazolin-2-carboxamide Derivatives as HCV NS5B Polymerase Inhibitors.</a> Deore, R.R., G.S. Chen, P.T. Chang, T.R. Chern, S.Y. Lai, M.H. Chuang, J.H. Lin, F.L. Kung, C.S. Chen, C.T. Chiou, and J.W. Chern, ChemMedChem, 2012, <b>[Epub ahead of print]</b>; PMID[22383290].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22378192">A Human Claudin-1 Derived Peptide Inhibits Hepatitis C Virus Entry.</a> Si, Y., S. Liu, X. Liu, J.L. Jacobs, M. Cheng, Y. Niu, Q. Jin, T. Wang, and W. Yang, Hepatology (Baltimore, Md.), 2012, <b>[Epub ahead of print]</b>; PMID[22378192].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">HSV-1</p>

    <p> </p>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22408623">Bilirubin: An Endogenous Molecule with Antiviral Activity in Vitro.</a> Santangelo, R., C. Mancuso, S. Marchetti, E. Di Stasio, G. Pani, and G. Fadda, Frontiers in pharmacology, 2012. 3: p. 36; PMID[22408623].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0302-031512.</p>

    <p> </p>

    <p> </p>

    <p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

    <p> </p>

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300028800038">Stereoselective Synthesis and anti-HCV Activity of Conformationally Restricted 2 &#39;-C-Substituted carbanucleosides.</a> Choi, W.J., Y.J. Ko, G. Chandra, H.W. Lee, H.O. Kim, H.J. Koh, H.R. Moon, Y.H. Jung, and L.S. Jeong, Tetrahedron, 2012. 68(4): p. 1253-1261; ISI[000300028800038].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300410700009">2-Hydroxy-1-oxo-1,2-dihydroisoquinoline-3-carboxylic acid with Inbuilt beta-N-Hydroxy-gamma-keto-acid Pharmacophore as HCV NS5B Polymerase Inhibitors.</a> Deore, R.R., G.S. Chen, C.S. Chen, P.T. Chang, M.H. Chuang, T.R. Chern, H.C. Wang, and J.W. Chern, Current medicinal chemistry, 2012. 19(4): p. 613-624; ISI[000300410700009].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300028800043">Synthesis of Novel Azanorbornylpurine Derivatives.</a> Hrebabecky, H., M. Dejmek, M. Dracinsky, M. Sala, P. Leyssen, J. Neyts, M. Kaniakova, J. Krusek, and R. Nencka, Tetrahedron, 2012. 68(4): p. 1286-1298; ISI[000300028800043].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000299759000007">Synthesis of Novel Anthracene Derivatives of Isoxazolino-carbocyclic nucleoside Analogues.</a> Moggio, Y., L. Legnani, B. Bovio, M.G. Memeo, and P. Quadrelli, Tetrahedron, 2012. 68(5): p. 1384-1392; ISI[000299759000007].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0302-031512.</p>

    <p> </p>

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000300140300043">Identification of the Niemann-Pick C1-like 1 Cholesterol Absorption Receptor as a New Hepatitis C Virus Entry Factor.</a> Sainz, B., N. Barretto, D.N. Martin, N. Hiraga, M. Imamura, S. Hussain, K.A. Marsh, X.M. Yu, K. Chayama, W.A. Alrefai, and S.L. Uprichard, Nature Medicine, 2012. 18(2): p. 281-285; ISI[000300140300043].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0302-031512.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
