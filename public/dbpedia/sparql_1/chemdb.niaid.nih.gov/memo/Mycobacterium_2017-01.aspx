

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-01.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="V+kO8GDVFGSsTCCbdn7QI6s5lJN45MMIrg7MqQiNd0bhSiGtnL67pW34s4CyK/q/9qfkvMKbPeeezhjixahuZjbF5TE9wyqM1RfA0z2pxht+sM/UVst48hTPZF8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B07026C9" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: January, 2017</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27914798">Antimycobacterial Activity of Novel Hydrazide-hydrazone Derivatives with 2H-Chromene and Coumarin Scaffold.</a> Angelova, V.T., V. Valcheva, N.G. Vassilev, R. Buyukliev, G. Momekov, I. Dimitrov, L. Saso, M. Djukic, and B. Shivachev. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(2): p. 223-227. PMID[27914798]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389167100035">Lanthanide Benzoates with 2,6-di-tert-Butylphenol Moiety: Synthesis, Luminescent and Antioxidant Properties.</a> Antonenko, T.A., D.B. Shpakovsky, Y.A. Gracheva, T.V. Balashova, A.P. Pushkarev, M.N. Bochkarev, and E.R. Milaeva. Inorganica Chimica Acta, 2017. 455: p. 276-282. ISI[000389167100035]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27894872">Synthesis, Screening and Docking Analysis of Hispolon Analogs as Potential Antitubercular Agents.</a> Balaji, N.V., B. Hari Babu, G.V. Subbaraju, K. Purna Nagasree, and M. Murali Krishna Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(1): p. 11-15. PMID[27894872]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000390631100003">Synthesis and Antimicrobial Activity of Novel 5-Substituted-N-(substituted-2H-[1,3]oxazino[6,5-b]quinolin-3(4H)-y1)-3-phenyl-1H-indole-2-carboxamides.</a> Basavarajaiah, S.M. and B.H.M. Mruthyunjayaswamy. Indian Journal of Chemistry, 2016. 55(12): p. 1511-1519. ISI[000390631100003]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27872065">In Vitro Susceptibility Testing of Bedaquiline against Mycobacterium avium Complex.</a> Brown-Elliott, B.A., J.V. Philley, D.E. Griffith, F. Thakkar, and R.J. Wallace, Jr. Antimicrobial Agents and Chemotherapy, 2017. 61(2): e01798-16. PMID[27872065]. PMCID[PMC5278735].<b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27168314">DNA Sequence-selective C8-linked Pyrrolobenzodiazepine Heterocyclic Polyamide Conjugates Show Anti-tubercular-specific Activities.</a> Brucoli, F., J.D. Guzman, M.A. Basher, D. Evangelopoulos, E. McMahon, T. Munshi, T.D. McHugh, K.R. Fox, and S. Bhakta. Journal of Antibiotics, 2016. 69(12): p. 843-849. PMID[27168314]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000391146000001">Synthesis, Antitubercular and Leishmanicidal Evaluation of Resveratrol Analogues.</a> Coimbra, E.S., J.A. Santos, L.L. Lima, P.A. Machado, D.L. Campos, F.R. Pavan, and A.D. Silva. Journal of the Brazilian Chemical Society, 2016. 27(12): p. 2161-2169. ISI[000391146000001]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389464700004">Structure-based Virtual Screening to Get New Scaffold Inhibitors of the Ser/Thr Protein Kinase PknB from Mycobacterium tuberculosis.</a> Coluccia, A., G. La Regina, N. Barilone, M.N. Lisa, A. Brancale, G. Andre-Leroux, P.M. Alzari, and R. Silvestri. Letters in Drug Design &amp; Discovery, 2016. 13(10): p. 1012-1018. ISI[000389464700004]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27978658">Stereoselective Synthesis, Docking, and Biological Evaluation of Difluoroindanediol-based Mene Inhibitors as Antibiotics.</a> Evans, C.E., J.S. Matarlo, P.J. Tonge, and D.S. Tan. Organic Letters, 2016. 18(24): p. 6384-6387. PMID[27978658]. PMCID[PMC5171203].<b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27750201">Antitubercular Activity of 1,2,3-Triazolyl Fatty acid Derivatives.</a> Ghiano, D.G., A. de la Iglesia, N. Liu, P.J. Tonge, H.R. Morbidoni, and G.R. Labadie. European Journal Medicinal Chemistry, 2017. 125: p. 842-852. PMID[27750201]. PMCID[PMC5148633].<b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27241561">Identification of Mycobacterium tuberculosis Leucyl-tRNA Synthetase (LeuRS) Inhibitors among the Derivatives of 5-Phenylamino-2H-[1,2,4]triazin-3-one.</a> Gudzera, O.I., A.G. Golub, V.G. Bdzhola, G.P. Volynets, O.P. Kovalenko, K.S. Boyarshin, A.D. Yaremchuk, M.V. Protopopov, S.M. Yarmoluk, and M.A. Tukalo. Journal of Enzyme Inhibition and Medicinal Chemistry, 2016. 31(sup2): p. 201-207. PMID[27241561]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27838484">Biofabrication of Polyphenols Stabilized Reduced Graphene oxide and Its Anti-tuberculosis Activity.</a> Han, W., W.Y. Niu, B. Sun, G.C. Shi, and X.Q. Cui. Journal of Photochemistry and Photobiology B-Biology, 2016. 165: p. 305-309. PMID[27838484]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389463600014">Design, Synthesis and Evaluation of Diphenyl ether Analogues as Antitubercular Agents.</a> Inturi, B., G.V. Pujar, M.N. Purohit, V.B. Iyer, G.S. Sowmya, and M. Kulkarni. RSC Advances, 2016. 6(112): p. 110571-110582. ISI[000389463600014]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27889632">Synthesis, Biological Evaluation and in Silico Molecular Modeling of Pyrrolyl Benzohydrazide Derivatives as Enoyl ACP Reductase Inhibitors.</a> Joshi, S.D., S.R. Dixit, V.H. Kulkarni, C. Lherbet, M.N. Nadagouda, and T.M. Aminabhavi. European Journal of Medicinal Chemistry, 2017. 126: p. 286-297. PMID[27889632]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27750198">Synthesis and Structure-activity Studies of Side Chain Analogues of the Anti-tubercular Agent, Q203.</a> Kang, S., Y.M. Kim, R.Y. Kim, M.J. Seo, Z. No, K. Nam, S. Kim, and J. Kim. European Journal of Medicinal Chemistry, 2017. 125: p. 807-815. PMID[27750198]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27907875">S-Substituted 3,5-dinitrophenyl 1,3,4-oxadiazole-2-thiols and tetrazole-5-thiols as Highly Efficient Antitubercular Agents.</a> Karabanovich, G., J. Nemecek, L. Valaskova, A. Carazo, K. Konecna, J. Stolarikova, A. Hrabalek, O. Pavlis, P. Pavek, K. Vavrova, J. Roh, and V. Klimesova. European Journal of Medicinal Chemistry, 2017. 126: p. 369-383. PMID[27907875]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389462100002">Biological Evaluation of Small Molecule Inhibitors of Mtb-ASADH Enzyme.</a> Kumar, R., R. Sethi, P. Shah, I. Roy, I.P. Singh, P.V. Bharatam, R. Tewari, and P. Garg. Letters in Drug Design &amp; Discovery, 2016. 13(7): p. 587-590. ISI[000389462100002]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27654393">Pyrazolo[1,5-a]pyridine-3-carboxamide Hybrids: Design, Synthesis and Evaluation of Anti-tubercular Activity.</a> Lu, X.Y., J. Tang, S.Y. Cui, B.J. Wan, S.G. Franzblauc, T.Y. Zhang, X.T. Zhang, and K. Ding. European Journal of Medicinal Chemistry, 2017. 125: p. 41-48. PMID[27654393]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27639087">Integration of Ligand and Structure Based Approaches for Identification of Novel MbtI Inhibitors in Mycobacterium tuberculosis and Molecular Dynamics Simulation Studies.</a> Maganti, L., P. Grandhi, and N. Ghoshal. Journal of Molecular Graphics &amp; Modelling, 2016. 70: p. 14-22. PMID[27639087]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27955925">Synthesis and Biological Evaluation of Phosphate Isosters of Fosmidomycin and Analogs as Inhibitors of Escherichia coli and Mycobacterium smegmatis 1-Deoxyxylulose 5-phosphate Reductoisomerases.</a> Munier, M., D. Tritsch, F. Krebs, J. Esque, A. Hemmerlin, M. Rohmer, R.H. Stote, and C. Grosdemange-Billiard. Bioorganic &amp; Medicinal Chemistry, 2017. 25(2): p. 684-689. PMID[27955925]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27307092">Lauric acid and Myristic acid from Allium sativum inhibit the Growth of Mycobacterium tuberculosis H37Ra: In Silico Analysis Reveals Possible Binding to Protein Kinase B.</a> Muniyan, R. and J. Gurunathan. Pharmaceutical Biology, 2016. 54(12): p. 2814-2821. PMID[27307092]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389966600006">In Search of Tuberculosis Drug Design: An in Silico Approach to Azoimidazolyl Derivatives as Antagonist for Cytochrome P450.</a> Pradhan, S., S. Mondal, and C. Sinha. Journal of the Indian Chemical Society, 2016. 93(9): p. 1067-1084. ISI[000389966600006]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389342800066">Synthesis, Characterization and Biological Activity of Fluorescently Labeled Bedaquiline Analogues.</a> Rombouts, J.A., R.M.P. Veenboer, C. Villellas, P. Lu, A.W. Ehlers, K. Andries, A. Koul, H. Lill, E. Ruijter, R.V.A. Orru, K. Lammertsma, D. Bald, and J.C. Slootweg. RSC Advances, 2016. 6(110): p. 108708-108716. ISI[000389342800066]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27688192">Quinolidene-Rhodanine Conjugates: Facile Synthesis and Biological Evaluation.</a> Subhedar, D.D., M.H. Shaikh, B.B. Shingate, L. Nawale, D. Sarkar, V.M. Khedkar, F.A.K. Khan, and J.N. Sangshetti. European Journal of Medicinal Chemistry, 2017. 125: p. 385-399. PMID[27688192]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27690385">Small-molecule Probes Reveal Esterases with Persistent Activity in Dormant and Reactivating Mycobacterium tuberculosis.</a> Tallman, K.R., S.R. Levine, and K.E. Beatty. ACS Infectious Diseases, 2016. 2(12): p. 48-56. PMID[27690385]. <b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27905500">Targeting Intracellular P-Aminobenzoic acid Production Potentiates the Anti-tubercular Action of Antifolates.</a> Thiede, J.M., S.L. Kordus, B.J. Turman, J.A. Buonomo, C.C. Aldrich, Y. Minato, and A.D. Baughn. Scientific Reports, 2016. 6(38083): 8pp. PMID[27905500]. PMCID[PMC5131483].<b><br />
    [PubMed]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000389330900007">Ecofriendly Synthesis and Biological Evaluation of 4-(4-Nitro-phenyl)-2-phenyl-1,4-dihydro-benzo[4,5]imidazo[1,2-a]pyrimidine-3-carboxylic acid ethyl ester Derivatives as an Antitubercular Agents.</a> Warekar, P.P., P.T. Patil, K.T. Patil, D.K. Jamale, G.B. Kolekar, and P.V. Anbhule. Synthetic Communications, 2016. 46(24): p. 2022-2030. ISI[000389330900007]. <b><br />
    [WOS]</b>. TB_01_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170126&amp;CC=WO&amp;NR=2017015106A1&amp;KC=A1">Substituted Phenyloxazolidinones for Antimicrobial Therapy.</a> Cooper, C.B., H. Huang, D. Zhang, N. Fotouhi, and T. Kaneko. Patent. 2017. 2016-US42486 2017015106: 227pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">29. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170105&amp;CC=WO&amp;NR=2017001661A1&amp;KC=A1">Preparation of Heterocyclic Compounds as Antibacterial Agents.</a> Guillemont, J.E.G., M.M.S. Motte, P.J.-M.B. Raboisson, and A. Tahri. Patent. 2017. 2016-EP65503 2017001661: 84pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">30. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170105&amp;CC=WO&amp;NR=2017001660A1&amp;KC=A1">Preparation of Heterocyclic Compounds as Antibacterial Agents.</a> Guillemont, J.E.G., M.M.S. Motte, P.J.-M.B. Raboisson, and A. Tahri. Patent. 2017. 2016-EP65499 2017001660: 194pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_01_2017.</p>

    <br />

    <p class="plaintext">31. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170119&amp;CC=WO&amp;NR=2017011730A2&amp;KC=A2">Phenazine Derivatives as Antimicrobial Agents.</a> Huigens, R.W., III, A. Grarrison, and Y. Abouelhassan. Patent. 2017. 2016-US42439</p>

    <p class="plaintext">2017011730: 251pp.</p>

    <p class="plaintext"><b>[Patent]</b>. TB_01_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
