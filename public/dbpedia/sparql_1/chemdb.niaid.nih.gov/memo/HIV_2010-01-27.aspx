

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-01-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="d/wd9bmmaC2R6uykmoKtH4tL9bqvIpFd4973HVnXuntKh1j3L/5x32loCI/R7UcmNcKTLKfZXKs8AeRTviejwgZ4I8FjycoSRB26DTqpN5MsKQR3t5HtD5KNn3o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A80C1727" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  January 6 - January 19, 2010</h1>

    <p class="memofmt2-1"> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20080975?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">A lectin isolated from bananas is a potent inhibitor of HIV replication.</a>  Swanson MD, Winter HC, Goldstein IJ, Markovitz DM.  J Biol Chem. 2010 Jan 15. [Epub ahead of print]PMID: 20080975</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20067983?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Different selection patterns of resistance and cross-resistance to HIV-1 agents targeting CCR5.</a>  Armand-Ugón M, Moncunill G, Gonzalez E, Mena M, Ballana E, Clotet B, Esté JA.  J Antimicrob Chemother. 2010 Jan 12. [Epub ahead of print]PMID: 20067983</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20066561?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">A novel lectin with highly potent antiproliferative and HIV-1 reverse transcriptase inhibitory activities from the edible wild mushroom Russula delica.</a>  Zhao S, Zhao Y, Li S, Zhao J, Zhang G, Wang H, Ng TB.  Glycoconj J. 2010 Jan 12. [Epub ahead of print]PMID: 20066561</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20054825?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Progranulin (granulin/epithelin precursor) and its constituent granulin repeats repress transcription from cellular promoters.</a>  Hoque M, Mathews MB, Pe&#39;ery T.  J Cell Physiol. 2010 Jan 6. [Epub ahead of print]PMID: 20054825</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19715533?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Biochemical and Functional Properties of a Lectin Purified from Korean Large Black Soybeans - A Cultivar of Glycine Max.</a>  Fang EF, Wong JH, Lin P, Ng TB.  Protein Pept Lett. 2010 Jan 10. [Epub ahead of print]PMID: 19715533</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>6.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272938300001">Inhibition of HIV-1 integrase nuclear import and replication by a peptide bearing integrase putative nuclear localization signal.</a>  Levin, A; Armon-Omer, A; Rosenbluh, J; Melamed-Book, N; Graessmann, A; Waigmann, E; Loyter, A.  RETROVIROLOGY, 2009; 6.</p>

    <p>7.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273042200012">3-O-Methylfunicone, a Selective Inhibitor of Mammalian Y-Family DNA Polymerases from an Australian Sea Salt Fungal Strain.</a>  Mizushina, Y; Motoshima, H; Yamaguchi, Y; Takeuchi, T; Hirano, K; Sugawara, F; Yoshida, H. MARINE DRUGS, 2009; 7(4): 624-639.</p>

    <p>8.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272878500055">Synthesis of New Pyridazine Derivatives as Potential Anti-HIV-1 Agents.</a>  Ferro, S; Agnello, S; Barreca, ML; De Luca, L; Christ, F; Gitto, R.  JOURNAL OF HETEROCYCLIC CHEMISTRY, 2009; 46(6): 1420-1424.</p>

    <p>9.       <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272927500004">A Convenient Synthesis and Biological Evaluation of Novel Pseudonucleosides Bearing a Thiazolidin-4-one Moiety by Tandem Staudinger/Aza-Wittig/Cyclization.</a>  Chen, H; Zhang, HZ; Feng, JN; Li, XL; Jiao, LL; Qin, ZB; Yin, QM; Zhang, JC.  EUROPEAN JOURNAL OF ORGANIC CHEMISTRY, 2009; 35: 6100-6103.</p>

    <p>10.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272928600040">Activity and molecular modeling of a new small molecule active against NNRTI-resistant HIV-1 mutants.</a>  Carta, A; Pricl, S; Piras, S; Fermeglia, M; La Colla, P; Loddo, R.  EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY, 2009; 44(12): 5117-5122.</p>

    <p>11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272832000013">Synthesis and Biological Activities of Quinoline Derivatives as HIV-1 Integrase Inhibitors.</a>  Luo, ZG; Zeng, CC; Wang, F; He, HQ; Wang, CX; Du, HG; Hu, LM.  CHEMICAL RESEARCH IN CHINESE UNIVERSITIES, 2009; 25(6): 841-845.</p>

    <p>12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272832000016">Design and Synthesis of New Anti-AIDS Drugs-Introducing an Immunomodulator into Delavirdine Derivative.</a>  Lu, HB; Mi, HY; Cui, LL; Yu, XH; Liang, T; Wang, ES.  CHEMICAL RESEARCH IN CHINESE UNIVERSITIES, 2009; 25(6): 856-860.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272992700030">Unique metabolites of Pestalotiopsis fici suggest a biosynthetic; hypothesis involving a Diels-Alder reaction and then mechanistic diversification.</a>  Liu, L; Niu, SB; Lu, XH; Chen, XL; Zhang, H; Guo, LD; Che, YS.  CHEMICAL COMMUNICATIONS, 2010; 46(3): 460-462.</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272935600009">Synthesis and evaluation of 3 &#39;-azido-2 &#39;,3 &#39;-dideoxypurine nucleosides as inhibitors of human immunodeficiency virus.</a>  Zhang, HW; Coats, SJ; Bondada, L; Amblard, F; Detorio, M; Asif, G; Fromentin, E; Solomon, S; Obikhod, A; Whitaker, T; Sluis-Cremer, N; Mellors, JW; Schinazi, RF.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(1): 60-64.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272935600039">Structure-based design, synthesis and biological evaluation of new N-carboxyphenylpyrrole derivatives as HIV fusion inhibitors targeting gp41.</a>  ang, Y; Lu, H; Zhu, Q; Jiang, SB; Liao, Y.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(1): 189-192.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272936000038">Non-peptide entry inhibitors of HIV-1 that target the gp41 coiled coil pocket.</a> Stewart, KD; Huth, JR; Ng, TI; McDaniel, K; Hutchinson, RN; Stoll, VS; Mendoza, RR; Matayoshi, ED; Carrick, R; Mo, HM; Severin, J; Walter, K; Richardson, PL; Barrett, LW; Meadows, R; Anderson, S; Kohlbrenner, W; Maring, C; Kempf, DJ; Molla, A; Olejniczak, ET.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(2): 612-617.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272936000070">Spirodiketopiperazine-based CCR5 antagonists: Improvement of their pharmacokinetic profiles.</a>  Nishizawa, R; Nishiyama, T; Hisaichi, K; Hirai, K; Habashita, H; Takaoka, Y; Tada, H; Sagawa, K; Shibayama, S; Maeda, K; Mitsuya, H; Nakai, H; Fukushima, D; Toda, M.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(2): 763-766.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000272892100013">Synthesis, nanosizing and in vitro drug release of a novel anti-HIV polymeric prodrug: Chitosan-O-isopropyl-5 &#39;-O-d4T monophosphate conjugate.</a>  Yang, L; Chen, LQ; Zeng, R; Li, C; Qiao, RZ; Hu, LM; Li, ZL.  BIOORGANIC &amp; MEDICINAL CHEMISTRY, 2010; 18(1): 117-123.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263288200044">Characterization of a Peptide Domain within the GB Virus C NS5A Phosphoprotein that Inhibits HIV Replication.</a>  Xiang, JH; McLinden, JH; Chang, Q; Jordan, EL; Stapleton, JT.  PLOS ONE, 2008; 3(7).</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000273153800004">Facile Synthesis of Hydroxy Wilfordic Acid, A Esterifying Unit of Anti-HIV Sesquiterpene Pyridine Alkaloids.</a>  Eun, JS; Seo, SY.  ARCHIVES OF PHARMACAL RESEARCH. 2009; 32(12): 1673-1679.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
