

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-82.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Jy6O40j/st+/MyVxAhJ4D7upIVENXlwxptiNDPOVP3p5NeBOaJo+s5FrPsV+c0hm4NVSxoCVeYZwf4jaWVZTzGFwLpfhyil+iJrggmsPMmyDnRlkP4Ep0UdsjTk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6714B020" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-82-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6670   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          6-[2-Phosphonomethoxy)Alkoxy]-2,4-Diaminopyrimidines: a New Class of Acyclic Pyrimidine Nucleoside Phosphonates With Antiviral Activity</p>

    <p>          Balzarini, J. <i> et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2004</b>.  23(8-9): 1321-1327</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         6671   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Role of the protein kinase PKR in the inhibition of varicella-zoster virus replication by beta interferon and gamma interferon</p>

    <p>          Desloges, N, Rahaus, M, and Wolff, MH</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 1): 1-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15604425&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15604425&amp;dopt=abstract</a> </p><br />

    <p>3.         6672   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Pyrimidine containing RSV fusion inhibitors</p>

    <p>          Nikitenko, A, Raifeld, Y, Mitsner, B, and Newman, H</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(2): 427-30</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15603966&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15603966&amp;dopt=abstract</a> </p><br />

    <p>4.         6673   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Inhibition of native hepatitis C virus replicase by nucleotide and non-nucleoside inhibitors</p>

    <p>          Ma, Han, Leveque, Vincent, De Witte, Anniek, Li, Weixing, Hendricks, Than, Clausen, Saundra M, Cammack, Nick, and Klumpp, Klaus</p>

    <p>          Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4F1J8M9-3/2/7d0382a0e9cc853ed219a714db6c2379">http://www.sciencedirect.com/science/article/B6WXR-4F1J8M9-3/2/7d0382a0e9cc853ed219a714db6c2379</a> </p><br />

    <p>5.         6674   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Comparative efficacy of acyclovir and vidarabine on the replication of varicella-zoster virus</p>

    <p>          Miwa, Naoko, Kurosaki, Kunikazu, Yoshida, Yoshihiro, Kurokawa, Masahiko, Saito, Shigeru, and Shiraki, Kimiyasu</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DWGS7V-1/2/dd6ae2ce656f444da658b0aaabccae60">http://www.sciencedirect.com/science/article/B6T2H-4DWGS7V-1/2/dd6ae2ce656f444da658b0aaabccae60</a> </p><br />

    <p>6.         6675   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Drugs for the management of respiratory syncytial virus infection</p>

    <p>          Broughton, S and Greenough, A</p>

    <p>          Curr Opin Investig Drugs <b>2004</b>.  5(8): 862-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15600242&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15600242&amp;dopt=abstract</a> </p><br />

    <p>7.         6676   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Inhibition of herpesvirus replication by a series of 4-oxo-dihydroquinolines with viral polymerase activity</p>

    <p>          Hartline, Caroll B, Harden, Emma A, Williams-Aziz, Stephanie L, Kushner, Nicole L, Brideau, Roger J, and Kern, Earl R</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DYVTS1-1/2/ebaa56c03a881bbae746693f0df85c41">http://www.sciencedirect.com/science/article/B6T2H-4DYVTS1-1/2/ebaa56c03a881bbae746693f0df85c41</a> </p><br />

    <p>8.         6677   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Inhibitors of the HCV NS5B polymerase: new hope for the treatment of hepatitis C infections</p>

    <p>          Beaulieu, PL and Tsantrizos, YS</p>

    <p>          Curr Opin Investig Drugs <b>2004</b>.  5(8): 838-50</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15600240&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15600240&amp;dopt=abstract</a> </p><br />

    <p>9.         6678   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Parallel synthesis of pteridine derivatives as potent inhibitors for hepatitis C virus NS5B RNA-dependent RNA polymerase</p>

    <p>          Ding, Yili, Girardet, Jean-Luc, Smith, Kenneth L, Larson, Gary, Prigaro, Brett, Lai, Vicky CH, Zhong, Weidong, and Wu, Jim Z</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DXT7R7-9/2/1249a8386743330f18623e5ba00a505a">http://www.sciencedirect.com/science/article/B6TF9-4DXT7R7-9/2/1249a8386743330f18623e5ba00a505a</a> </p><br />

    <p>10.       6679   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          A novel therapy for acute hepatitis utilizing dehydroepiandrosterone in the murine model of hepatitis</p>

    <p>          Yoneda, Masato, Wada, Koichiro, Katayama, Kazufumi, Nakajima, Noriko, Iwasaki, Tomoyuki, Osawa, Emi, Mukasa, Koji, Yamada, Yasuhiko, Blumberg, Richard S, Sekihara, Hisahiko, and Nakajima, Atsushi</p>

    <p>          Biochemical Pharmacology <b>2004</b>.  68(11): 2283-2289</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T4P-4DDRBF1-5/2/e2e67a609ce71cf8a32e0a7a2a157c7b">http://www.sciencedirect.com/science/article/B6T4P-4DDRBF1-5/2/e2e67a609ce71cf8a32e0a7a2a157c7b</a> </p><br />

    <p>11.       6680   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Pegylated interferon alfa-2b vs standard interferon alfa-2b, plus ribavirin, for chronic hepatitis C in HIV-infected patients: a randomized controlled trial</p>

    <p>          Carrat, F, Bani-Sadr, F, Pol, S, Rosenthal, E, Lunel-Fabiani, F, Benzekri, A, Morand, P, Goujard, C, Pialoux, G, Piroth, L, Salmon-Ceron, D, Degott, C, Cacoub, P, and Perronne, C</p>

    <p>          JAMA <b>2004</b>.  292(23): 2839-48</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598915&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15598915&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.       6681   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Synthesis of 9-(2-[beta]-C-methyl-[beta]-D-ribofuranosyl)-6-substituted purine derivatives as inhibitors of HCV RNA replication</p>

    <p>          Ding, Yili, Girardet, Jean-Luc, Hong, Zhi, Lai, Vicky CH, An, Haoyun, Koh, Yung-hyo, Shaw, Stephanie Z, and Zhong, Weidong</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DXC1JJ-1/2/60dfa3f60e120969ac073510c016bed3">http://www.sciencedirect.com/science/article/B6TF9-4DXC1JJ-1/2/60dfa3f60e120969ac073510c016bed3</a> </p><br />

    <p>13.       6682   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Synthesis of 2&#39;-[beta]-C-methyl toyocamycin and sangivamycin analogues as potential HCV inhibitors</p>

    <p>          Ding, Yili, An, Haoyun, Hong, Zhi, and Girardet, Jean-Luc</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DWGYTJ-3/2/d2f5872985425b2cbafbd3f11434a3e0">http://www.sciencedirect.com/science/article/B6TF9-4DWGYTJ-3/2/d2f5872985425b2cbafbd3f11434a3e0</a> </p><br />

    <p>14.       6683   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          A Novel Oral Vehicle for Poorly Soluble Hsv-Helicase Inhibitors: Pk/Pd Validations</p>

    <p>          Duan, J. <i>et al.</i></p>

    <p>          Pharmaceutical Research <b>2004</b>.  21(11): 2079-2084</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>15.       6684   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Inhibitors of virus replication: recent developments and prospects</p>

    <p>          Magden, J, Kaariainen, L, and Ahola, T</p>

    <p>          Appl Microbiol Biotechnol <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592828&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592828&amp;dopt=abstract</a> </p><br />

    <p>16.       6685   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Detection of influenza virus: traditional approaches and development of biosensors</p>

    <p>          Amano, Y and Cheng, Q</p>

    <p>          Anal Bioanal Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15592819&amp;dopt=abstract</a> </p><br />

    <p>17.       6686   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Studies on Synthesis and Antiviral Activity of Ribavirin and Its Derivatives</p>

    <p>          Li, Q. <i>et al.</i></p>

    <p>          Chinese Journal of Organic Chemistry <b>2004</b>.  24(11): 1432-1435</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.       6687   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Ethnomedicinal uses of Momordicacharantia (Cucurbitaceae) in Togo and relation to its phytochemistry and biological activity</p>

    <p>          Beloin, N, Gbeassor, M, Akpagana, K, Hudson, J, de, Soussa K, Koumaglo, K, and Arnason, JT</p>

    <p>          J Ethnopharmacol <b>2005</b>.  96(1-2): 49-55</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588650&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588650&amp;dopt=abstract</a> </p><br />

    <p>19.       6688   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          A Substrate Mimetic Approach for Influenza Neuraminidase Inhibitors</p>

    <p>          Jeong, J. <i>et al.</i></p>

    <p>          Bulletin of the Korean Chemical Society <b>2004</b>.  25(10): 1575-1577</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>20.       6689   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Discovery of pyrano[3,4-b]indoles as potent and selective HCV NS5B polymerase inhibitors</p>

    <p>          Gopalsamy, A, Lim, K, Ciszewski, G, Park, K, Ellingboe, JW, Bloom, J, Insaf, S, Upeslacis, J, Mansour, TS, Krishnamurthy, G, Damarla, M, Pyatski, Y, Ho, D, Howe, AY, Orlowski, M, Feld, B, and O&#39;Connell, J</p>

    <p>          J Med Chem <b>2004</b>.  47(26): 6603-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588095&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588095&amp;dopt=abstract</a> </p><br />

    <p>21.       6690   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          A systematic approach to the optimization of substrate-based inhibitors of the hepatitis C virus NS3 protease: discovery of potent and specific tripeptide inhibitors</p>

    <p>          Llinas-Brunet, M, Bailey, MD, Ghiro, E, Gorys, V, Halmos, T, Poirier, M, Rancourt, J, and Goudreau, N</p>

    <p>          J Med Chem <b>2004</b>.  47(26): 6584-94</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588093&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588093&amp;dopt=abstract</a> </p><br />

    <p>22.       6691   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Peginterferon alfa-2b plus ribavirin for naIve patients with genotype 1 chronic hepatitis C: a randomized controlled trial[star, open]</p>

    <p>          Bruno, Savino, Camma, Calogero, Di Marco, Vito, Rumi, Mariagrazia, Vinci, Maria, Camozzi, Mario, Rebucci, Chiara, Di Bona, Danilo, Colombo, Massimo, and Craxi, Antonio</p>

    <p>          Journal of Hepatology <b>2004</b>.  41(3): 474-481</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7C-4CTJ31K-5/2/dfeba24a8f0553bdf1162a84c746c8c7">http://www.sciencedirect.com/science/article/B6W7C-4CTJ31K-5/2/dfeba24a8f0553bdf1162a84c746c8c7</a> </p><br />
    <br clear="all">

    <p>23.       6692   DMID-LS-82; PUBMED-DMID-12/20/2004</p>

    <p class="memofmt1-2">          The design and enzyme-bound crystal structure of indoline based peptidomimetic inhibitors of hepatitis C virus NS3 protease</p>

    <p>          Ontoria, JM, Di Marco, S, Conte, I, Di Francesco, ME, Gardelli, C, Koch, U, Matassa, VG, Poma, M, Steinkuhler, C, Volpari, C, and Harper, S</p>

    <p>          J Med Chem <b>2004</b>.  47(26): 6443-6446</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588076&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15588076&amp;dopt=abstract</a> </p><br />

    <p>24.       6693   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Identification of [(naphthalene-1-carbonyl)-amino]-acetic acid derivatives as nonnucleoside inhibitors of HCV NS5B RNA dependent RNA polymerase</p>

    <p>          Gopalsamy, Ariamala, Lim, Kitae, Ellingboe, John W, Krishnamurthy, Girija, Orlowski, Mark, Feld, Boris, van Zeijl, Marja, and Howe, Anita YM</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(16): 4221-4224</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4CP68CK-6/2/f280480a47fdd49a2358646c9703c0de">http://www.sciencedirect.com/science/article/B6TF9-4CP68CK-6/2/f280480a47fdd49a2358646c9703c0de</a> </p><br />

    <p>25.       6694   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          A novel oral vehicle for poorly soluble HSV-helicase inhibitors: PK/PD validations</p>

    <p>          Duan, J, Liard, F, Paris, W, and Lambert, M</p>

    <p>          Pharm Res <b>2004</b>.  21(11): 2079-84</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15587931&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15587931&amp;dopt=abstract</a> </p><br />

    <p>26.       6695   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Antiviral Activities of Medicinal Herbs Traditionally Used in Southern Mainland China</p>

    <p>          Li, Y. <i>et al.</i></p>

    <p>          Phytotherapy Research <b>2004</b>.  18(9): 718-722</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       6696   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Persulfated Molecular Umbrellas as Anti-HIV and Anti-HSV Agents</p>

    <p>          Jing, B, Janout, V, Herold, BC, Klotman, ME, Heald, T, and Regen, SL</p>

    <p>          J Am Chem Soc <b>2004</b>.  126(49): 15930-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15584704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15584704&amp;dopt=abstract</a> </p><br />

    <p>28.       6697   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Influenza: old and new threats</p>

    <p>          Palese, P</p>

    <p>          Nat Med <b>2004</b>.  10(12 Suppl): S82-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15577936&amp;dopt=abstract</a> </p><br />

    <p>29.       6698   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Interferon-Beta and Interferon-Gamma Synergistically Inhibit the Replication of Severe Acute Respiratory Syndrome-Associated Coronavirus (Sars-Cov)</p>

    <p>          Sainz, B. <i>et al.</i></p>

    <p>          Virology <b>2004</b>.  329(1): 11-17</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>30.       6699   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Virus Ion Channels: a Kind of New Target for Antivirus Drug Action</p>

    <p>          Yuan, Y. <i>et al.</i></p>

    <p>          Progress in Biochemistry and Biophysics <b>2004</b>.  31(10): 861-864</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       6700   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Novel 3&#39;-deoxy analogs of the anti-HBV agent entecavir: synthesis of enantiomers from a single chiral epoxide</p>

    <p>          Ruediger, Edward, Martel, Alain, Meanwell, Nicholas, Solomon, Carola, and Turmel, Brigitte</p>

    <p>          Tetrahedron Letters <b>2004</b>.  45(4): 739-742</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THS-4B5B922-C/2/7e6a24e68dd996d0df8f218e90aa4c76">http://www.sciencedirect.com/science/article/B6THS-4B5B922-C/2/7e6a24e68dd996d0df8f218e90aa4c76</a> </p><br />

    <p>32.       6701   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Emerging therapies for herpes viral infections (types 1 - 8)</p>

    <p>          Chakrabarty, A, Pang, KR, Wu, JJ, Narvaez, J, Rauser, M, Huang, DB, Beutner, KR, and Tyring, SK</p>

    <p>          Expert Opin Emerg Drugs <b>2004</b>.  9(2): 237-56</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15571482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15571482&amp;dopt=abstract</a> </p><br />

    <p>33.       6702   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Exploring the Potential of Variola Virus Infection of Cynomolgus Macaques as a Model for Human Smallpox</p>

    <p>          Jahrling, P. <i>et al.</i></p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2004</b>.  101(42): 15196-15200</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       6703   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Antiviral Drugs for Prophylaxis and Treatment of Influenza</p>

    <p>          Anon</p>

    <p>          Medical Letter on Drugs and Therapeutics <b>2004</b>.  46(1194): 85-87</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>35.       6704   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Identification and enzymatic characterization of NS2B-NS3 protease of Alkhurma virus, a class-4 flavivirus</p>

    <p>          Bessaud, M, Grard, G, Peyrefitte, CN, Pastorino, B, Rolland, D, Charrel, RN, Lamballerie, X, and Tolou, HJ</p>

    <p>          Virus Res <b>2005</b>.  107(1): 57-62</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15567034&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15567034&amp;dopt=abstract</a> </p><br />

    <p>36.       6705   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          A comparative evaluation of three methods of antiviral susceptibility testing of clinical herpes simplex virus isolates</p>

    <p>          Safrin, S, Phan, L, and Elbeik, T</p>

    <p>          Clin Diagn Virol <b>1995</b>.  4(1): 81-91</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566830&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566830&amp;dopt=abstract</a> </p><br />

    <p>37.       6706   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Detection of yellow fever virus by polymerase chain reaction</p>

    <p>          Brown, TM, Chang, GJ, Cropp, CB, Robbins, KE, and Tsai, TF</p>

    <p>          Clin Diagn Virol <b>1994</b>.  2(1): 41-51</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566752&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566752&amp;dopt=abstract</a> </p><br />

    <p>38.       6707   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p><b>          Randomized trial of three different regimens for 24 weeks for re-treatment of chronic hepatitis C patients who failed to respond to interferon-alpha monotherapy in Taiwan</b> </p>

    <p>          Chuang, WL, Dai, CY, Chen, SC, Lee, LP, Lin, ZY, Hsieh, MY, Wang, LY, Yu, ML, and Chang, WY</p>

    <p>          Liver Int <b>2004</b>.  24(6): 595-602</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566510&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566510&amp;dopt=abstract</a> </p><br />

    <p>39.       6708   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Entecavir for the treatment of chronic hepatitis B</p>

    <p>          Shaw, T and Locarnini, S</p>

    <p>          Expert Rev Anti Infect Ther <b>2004</b>.  2(6): 853-71</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566330&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566330&amp;dopt=abstract</a> </p><br />

    <p>40.       6709   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Adefovir dipivoxil for treatment of breakthrough hepatitis caused by lamivudine-resistant mutants of hepatitis B virus</p>

    <p>          Hosaka, T, Suzuki, F, Suzuki, Y, Saitoh, S, Kobayashi, M, Someya, T, Sezaki, H, Akuta, N, Tsubota, A, Arase, Y, Ikeda, K, and Kumada, H</p>

    <p>          Intervirology <b>2004</b>.  47(6): 362-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15564749&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15564749&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>41.       6710   DMID-LS-82; PUBMED-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Virucidal activity presence in Trichilia glabra leaves</p>

    <p>          Cella, M, Riva, DA, Coulombie, FC, and Mersich, SE</p>

    <p>          Rev Argent Microbiol <b>2004</b>.  36(3): 136-8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15559196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15559196&amp;dopt=abstract</a> </p><br />

    <p>42.       6711   DMID-LS-82; WOS-DMID-12/20/2004</p>

    <p class="memofmt1-2">          Cytomegalovirus</p>

    <p>          Anon</p>

    <p>          American Journal of Transplantation <b>2004</b>.  4: 51-58 </p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>43.       6712   DMID-LS-82; EMBASE-DMID-12/20/2004 </p>

    <p class="memofmt1-2">          Polyprotein cleavage mechanism of SARS CoV Mpro and chemical modification of the octapeptide</p>

    <p>          Du, Qi-Shi, Wang, Shu-Qing, Zhu, Yu, Wei, Dong-Qing, Guo, Hong, Sirois, Suzanne, and Chou, Kuo-Chen</p>

    <p>          Peptides <b>2004</b>.  25(11): 1857-1864</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T0M-4D09MM9-1/2/4eb895b3fd535107ddc13912d5d791db">http://www.sciencedirect.com/science/article/B6T0M-4D09MM9-1/2/4eb895b3fd535107ddc13912d5d791db</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
