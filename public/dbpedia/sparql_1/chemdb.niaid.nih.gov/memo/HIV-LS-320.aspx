

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-320.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ao+wfX7jeM6THOa4uKkF7HL32AUc99qyhyvnDeqnKyGF3QC/zTICEVR6IWa3OFNou09hWizejxAfSunaPK6iZJMOqNEEg8KkCJnMP4AXmhN+OZ00wGoTGyluJSc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BE70AE4A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-320-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     45161   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Differential Inhibition of HIV-1 and SIV Envelope-Mediated Cell Fusion by C34 Peptides Derived from the C-Terminal Heptad Repeat of gp41 from Diverse Strains of HIV-1, HIV-2, and SIV</p>

    <p>          Gustchina, E, Hummer, G, Bewley, CA, and Clore, GM</p>

    <p>          J Med Chem <b>2005</b>.  48(8): 3036-3044</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828842&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15828842&amp;dopt=abstract</a> </p><br />

    <p>2.     45162   HIV-LS-320; EMBASE-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Suppression of Multidrug-resistant HIV-1 Reverse Transcriptase Primer Unblocking Activity by [alpha]-Phosphate-modified Thymidine Analogues</p>

    <p>          Matamoros, Tania, Deval, Jerome, Guerreiro, Catherine, Mulard, Laurence, Canard, Bruno, and Menendez-Arias, Luis</p>

    <p>          Journal of Molecular Biology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4G05H5S-4/2/68f2ac6a8fdd4d91ef6e41010529f28f">http://www.sciencedirect.com/science/article/B6WK7-4G05H5S-4/2/68f2ac6a8fdd4d91ef6e41010529f28f</a> </p><br />

    <p>3.     45163   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Inhibition of lysosome and proteasome function enhances human immunodeficiency virus type 1 infection</p>

    <p>          Wei, BL, Denton, PW, O&#39;neill, E, Luo, T, Foster, JL, and Garcia, JV</p>

    <p>          J Virol <b>2005</b>.  79(9): 5705-12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15827185&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15827185&amp;dopt=abstract</a> </p><br />

    <p>4.     45164   HIV-LS-320; EMBASE-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Targeting HIV-1 integrase with aptamers selected against the purified RNase H domain of HIV-1 RT</p>

    <p>          Metifiot, Mathieu, Leon, Oscar, Tarrago-Litvak, Laura, Litvak, Simon, and Andreola, Marie-Line </p>

    <p>          Biochimie <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VRJ-4FXWT6S-3/2/951bbc8a0f3db8d2fd07b0af0df23741">http://www.sciencedirect.com/science/article/B6VRJ-4FXWT6S-3/2/951bbc8a0f3db8d2fd07b0af0df23741</a> </p><br />

    <p>5.     45165   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Isolation and characterisation of novel cyclotides from viola hederaceae: Solution structure and anti-HIV activity of VHL-1, A leaf-specific-expressed cyclotide</p>

    <p>          Chen, B, Colgrave, ML, Daly, NL, Rosengren, KJ, Gustafson, KR, and Craik, DJ</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15824119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15824119&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     45166   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Theaflavin derivatives in black tea and catechin derivatives in green tea inhibit HIV-1 entry by targeting gp41</p>

    <p>          Liu, S, Lu, H, Zhao, Q, He, Y, Niu, J, Debnath, AK, Wu, S, and Jiang, S</p>

    <p>          Biochim Biophys Acta <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15823507&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15823507&amp;dopt=abstract</a> </p><br />

    <p>7.     45167   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Concise synthesis and antiviral activity of novel unsaturated acyclic pyrimidine nucleosides</p>

    <p>          Oh, CH, Baek, TR, and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(2): 153-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822621&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822621&amp;dopt=abstract</a> </p><br />

    <p>8.     45168   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Synthesis of (+/-)-4&#39;-ethynyl and 4&#39;-cyano carbocyclic analogues of stavudine (d4T)</p>

    <p>          Kumamoto, H, Haraguchi, K, Tanaka, H, Nitanda, T, Baba, M, Dutschman, GE, Cheng, YC, and Kato, K</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(2): 73-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822615&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15822615&amp;dopt=abstract</a> </p><br />

    <p>9.     45169   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          HIV-1 resistance conferred by siRNA cosuppression of CXCR4 and CCR5 coreceptors by a bispecific lentiviral vector</p>

    <p>          Anderson, J and Akkina, R</p>

    <p>          AIDS Res Ther <b>2005</b>.  2(1): 1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15813990&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15813990&amp;dopt=abstract</a> </p><br />

    <p>10.   45170   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Deoxythreosyl Phosphonate Nucleosides as Selective Anti-HIV Agents</p>

    <p>          Wu, T, Froeyen, M, Kempeneers, V, Pannecouque, C, Wang, J, Busson, R, De, Clercq E, and Herdewijn, P</p>

    <p>          J Am Chem Soc <b>2005</b>.  127(14): 5056-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15810840&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15810840&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   45171   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Potent 1,3,4-trisubstituted pyrrolidine CCR5 receptor antagonists: effects of fused heterocycles on antiviral activity and pharmacokinetic properties</p>

    <p>          Kim, D, Wang, L, Hale, JJ, Lynch, CL, Budhu, RJ, Maccoss, M, Mills, SG, Malkowitz, L, Gould, SL, Demartino, JA, Springer, MS, Hazuda, D, Miller, M, Kessler, J, Hrin, RC, Carver, G, Carella, A, Henry, K, Lineberger, J, Schleif, WA, and Emini, EA</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(8): 2129-34</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15808483&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15808483&amp;dopt=abstract</a> </p><br />

    <p>12.   45172   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Rare mutations at codon 103 of HIV-1 reverse transcriptase can confer resistance to non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Harrigan, PR, Mo, T, Wynhoven, B, Hirsch, J, Brumme, Z, McKenna, P, Pattery, T, Vingerhoets, J, and Bacheler, LT</p>

    <p>          AIDS <b>2005</b>.  19 (6): 549-54</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15802972&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15802972&amp;dopt=abstract</a> </p><br />

    <p>13.   45173   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Enfuvirtide resistance mutations: impact on human immunodeficiency virus envelope function, entry inhibitor sensitivity, and virus neutralization</p>

    <p>          Reeves, JD, Lee, FH, Miamidian, JL, Jabara, CB, Juntilla, MM, and Doms, RW</p>

    <p>          J Virol <b>2005</b>.  79(8): 4991-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795284&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15795284&amp;dopt=abstract</a> </p><br />

    <p>14.   45174   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Evaluation of an oligonucleotide ligation assay for detection of mutations in HIV-1 subtype C individuals who have high level resistance to nucleoside reverse transcriptase inhibitors and non-nucleoside reverse transcriptase inhibitors</p>

    <p>          Wallis, CL, Mahomed, I, Morris, L, Chidarikire, T, Stevens, G, Rekhviashvili, N, and Stevens, W</p>

    <p>          J Virol Methods <b>2005</b>.  125(2): 99-109</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15794978&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15794978&amp;dopt=abstract</a> </p><br />

    <p>15.   45175   HIV-LS-320; PUBMED-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Lancifodilactone F: a novel nortriterpenoid possessing a unique skeleton from Schisandra lancifolia and its anti-HIV activity</p>

    <p>          Xiao, WL, Li, RT, Li, SH, Li, XL, Sun, HD, Zheng, YT, Wang, RR, Lu, Y, Wang, C, and Zheng, QT</p>

    <p>          Org Lett <b>2005</b>.  7(7): 1263-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15787482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15787482&amp;dopt=abstract</a> </p><br />

    <p>16.   45176   HIV-LS-320; EMBASE-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Antiviral activity in vitro of Urtica dioica L., Parietaria diffusa M. et K. and Sambucus nigra L.</p>

    <p>          Uncini Manganelli, RE, Zaccaro, L, and Tomei, PE</p>

    <p>          Journal of Ethnopharmacology <b>2005</b>.  98(3): 323-327</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T8D-4FK42K8-3/2/ba778eb59f0783537cf156ea902ee51c">http://www.sciencedirect.com/science/article/B6T8D-4FK42K8-3/2/ba778eb59f0783537cf156ea902ee51c</a> </p><br />

    <p>17.   45177   HIV-LS-320; EMBASE-HIV-4/19/2005</p>

    <p class="memofmt1-2">          Spectrum of antiviral activity of o-(acetoxyphenyl)hept-2-ynyl sulphide (APHS)</p>

    <p>          Pereira, Candida F, Rutten, Karla, Stranska, Ruzena, Huigen, Marleen CDG, Aerts, Piet C, de Groot, Raoul J, Egberink, Herman F, Schuurman, Rob, and Nottet, Hans SLM</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4FX23FV-1/2/28b9812c3725d3ddc542f22baedd31d1">http://www.sciencedirect.com/science/article/B6T7H-4FX23FV-1/2/28b9812c3725d3ddc542f22baedd31d1</a> </p><br />

    <p>18.   45178   HIV-LS-320; WOS-HIV-4/10/2005</p>

    <p class="memofmt1-2">          The CCR5 receptor-based mechanism of action of 873140, a potent allosteric noncompetitive HIV entry inhibitor</p>

    <p>          Watson, C, Jenkinson, S, Kazmierski, W, and Kenakin, T</p>

    <p>          MOLECULAR PHARMACOLOGY <b>2005</b>.  67(4): 1268-1282, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227805800033">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227805800033</a> </p><br />

    <p>19.   45179   HIV-LS-320; WOS-HIV-4/10/2005</p>

    <p class="memofmt1-2">          The conserved glycine-rich segment linking the N-terminal fusion peptide to the coiled coil of human T-cell leukemia virus type 1 transmembrane glycoprotein gp21 is a determinant of membrane fusion function</p>

    <p>          Wilson, KA, Bar, S, Maerz, AL, Alizon, M, and Poumbourios, P</p>

    <p>          JOURNAL OF VIROLOGY <b>2005</b>.  79(7): 4533-4539, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227743700064">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227743700064</a> </p><br />

    <p>20.   45180   HIV-LS-320; WOS-HIV-4/10/2005</p>

    <p class="memofmt1-2">          Aspartic proteases of Plasmodium falciparum as the target of HIV-1 protease inhibitors</p>

    <p>          Savarino, A, Cauda, R, and Cassone, A</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2005</b>.  191(8): 1381-1382, 2</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227804500028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227804500028</a> </p><br />

    <p>21.   45181   HIV-LS-320; WOS-HIV-4/10/2005</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of branched and conformationally restricted analogs of the anticancer compounds 3&#39;-C-ethynyluridine (EUrd) and 3&#39;-C-ethynylcytidine (ECyd)</p>

    <p>          Hrdlicka, PJ, Andersen, NK, Jepsen, JS, Hansen, FG, Haselmann, KF, Nielsen, C, and Wengel, J</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2005</b>.  13(7): 2597-2621, 25</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227891000025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227891000025</a> </p><br />
    <br clear="all">

    <p>22.   45182   HIV-LS-320; WOS-HIV-4/17/2005</p>

    <p class="memofmt1-2">          In silico studies toward the discovery of new anti-HIV nucleoside compounds through the use of TOPS-MODE and 2D/3D connectivity indices. 2. Purine derivatives</p>

    <p>          Vilar, S, Estrada, E, Uriarte, E, Santana, L, and Gutierrez, Y</p>

    <p>          JOURNAL OF CHEMICAL INFORMATION AND MODELING <b>2005</b>.  45(2): 502-514, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228018000036">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228018000036</a> </p><br />

    <p>23.   45183   HIV-LS-320; WOS-HIV-4/17/2005</p>

    <p class="memofmt1-2">          Marked structural and functional heterogeneity in CXCR4: Separation of HIV-1 and SDF-I alpha responses</p>

    <p>          Sloane, AJ, Raso, V, Dimitrov, DS, Xiao, XD, Deo, S, Muljadi, N, Restuccia, D, Turville, S, Kearney, C, Broder, CC, Zoellner, H, Cunningham, AL, Bendall, L, and Lynch, GW</p>

    <p>          IMMUNOLOGY AND CELL BIOLOGY <b>2005</b>.  83(2): 129-143, 15</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227953300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000227953300004</a> </p><br />

    <p>24.   45184   HIV-LS-320; WOS-HIV-4/17/2005</p>

    <p class="memofmt1-2">          Reduced azole susceptibility in genotype 3 Candida dubliniensis isolates associated with increased CdCDR1 and CdCDR2 expression</p>

    <p>          Pinjon, E, Jackson, CJ, Kelly, SL, Sanglard, D, Moran, G, Coleman, DC, and Sullivan, DJ</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2005</b>.  49(4): 1312-1318, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000228082500007</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
