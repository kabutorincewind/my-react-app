

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-04-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dF3EC16D6wX/wWUdWxgnqyLvbwGHXytP6eqwbSxheBBdqmv+natmj99FqEq+n8wCtiL7ezlEG1BoglUVhONqFXWSTtlThxomWwJooLE1inTnjSbVkJ8tO+wj9K8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="FD16C641" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: March 28 - April 10, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24481875">Taraxacum mongolicum Extract Exhibits a Protective Effect on Hepatocytes and an Antiviral Effect against Hepatitis B Virus in Animal and Human Cells<i>.</i></a> Jia, Y.Y., R.F. Guan, Y.H. Wu, X.P. Yu, W.Y. Lin, Y.Y. Zhang, T. Liu, J. Zhao, S.Y. Shi, and Y. Zhao. Molecular Medicine Reports, 2014. 9(4): p. 1381-1387; PMID[24481875].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24509240">Inhibition of Hepatitis B Virus Replication by Ligand-mediated Activation of RNase L<i>.</i></a> Park, I.H., Y.C. Kwon, W.S. Ryu, and B.Y. Ahn. Antiviral Research, 2014. 104: p. 118-127; PMID[24509240].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24517400">New Antiviral Agents for the Treatment of Hepatitis C: ABT-450<i>.</i></a> Carrion, A.F., J. Gutierrez, and P. Martin. Expert Opinion on Pharmacotherapy, 2014. 15(5): p. 711-716; PMID[24517400].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24652513">New Direct-acting Antiviral Agents for the Treatment of Chronic Hepatitis C in 2014<i>.</i></a> Cornberg, M., C. Honer Zu Siederdissen, B. Maasoumy, and M.P. Manns. Der Internist, 2014. 55(4): p. 390-400; PMID[24652513].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23432541">Cholesterol Sulfate as a Potential Inhibitor of Hepatitis C Virus NS3 Helicase<i>.</i></a> Furuta, A., K.A. Salam, N. Akimitsu, J. Tanaka, H. Tani, A. Yamashita, K. Moriishi, M. Nakakoshi, M. Tsubuki, Y. Sekiguchi, S. Tsuneda, and N. Noda. Journal of Enzyme Inhibition and Medicinal Chemistry, 2014. 29(2): p. 223-229; PMID[23432541].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24593285">Ledipasvir : A Novel Synthetic Antiviral for the Treatment of HCV Infection<i>.</i></a> Gentile, I., A.R. Buonomo, F. Borgia, G. Castaldo, and G. Borgia. Expert Opinion on Investigational Drugs, 2014. 23(4): p. 561-571; PMID[24593285].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24699145">PBDE: Structure-activity Studies for the Inhibition of Hepatitis C Virus NS3 Helicase<i>.</i></a> Salam, K.A., A. Furuta, N. Noda, S. Tsuneda, Y. Sekiguchi, A. Yamashita, K. Moriishi, M. Nakakoshi, H. Tani, S.R. Roy, J. Tanaka, M. Tsubuki, and N. Akimitsu. Molecules, 2014. 19(4): p. 4006-4020; PMID[24699145].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p>

    <h2>Epstein-Barr Virus</h2>

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24486954">Small Molecule Inhibition of Epstein-Barr Virus Nuclear Antigen-1 DNA Binding Activity Interferes with Replication and Persistence of the Viral Genome<i>.</i></a> Lee, E.K., S.Y. Kim, K.W. Noh, E.H. Joo, B. Zhao, E. Kieff, and M.S. Kang. Antiviral Research, 2014. 104: p. 73-83; PMID[24486954].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0328-041014.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000332175100075">Cytomegalovirus Mutants Resistant to Ganciclovir and Cidofovir Differ in Susceptibilities to Synguanol and Its 6-Ether and 6-Thioether Derivatives<i>.</i></a> Chou, S.W., G. Komazin-Meredith, J.D. Williams, and T.L. Bowlin. Antimicrobial Agents and Chemotherapy, 2014. 58(3): p. 1809-1812; ISI[000332175100075].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0328-041014.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
