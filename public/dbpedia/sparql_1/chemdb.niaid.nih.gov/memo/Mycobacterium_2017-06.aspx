

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2017-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1z8aenjaL4xwZ//ASMBmKvBw5C/7O+PDsUBb2CBUGD9XwgbISWMVY1SNcdjeS7u9tbWTdhqKaMpaC4qfzPqw3/BQq+mqLLpzOTxYvXOuF36yFHIY2XStA/OdZ9o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="29A2ACFD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800"><i>Mycobacterium</i> Citations List: June, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28364227">An Anti-mycobacterial Bisfunctionalized Sphingolipid and New Bromopyrrole Alkaloid from the Indonesian Marine Sponge Agelas sp.</a> Abdjul, D.B., H. Yamazaki, S. Kanno, A. Tomizawa, H. Rotinsulu, D.S. Wewengkang, D.A. Sumilat, K. Ukai, M.M. Kapojos, and M. Namikoshi. Journal of Natural Medicines, 2017. 71(3): p. 531-536. PMID[28364227]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28411559">Synthesis and Antimycobacterial Screening of New Thiazolyl-oxazole Derivatives.</a> Abhale, Y.K., A.V. Sasane, A.P. Chavan, S.H. Shekh, K.K. Deshmukh, S. Bhansali, L. Nawale, D. Sarkar, and P.C. Mhaske. European Journal of Medicinal Chemistry, 2017. 132: p. 333-340. PMID[28411559]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28545436">Thymoquinone (TQ) Inhibits the Replication of Intracellular Mycobacterium tuberculosis in Macrophages and Modulates Nitric Oxide Production.</a> Al Mahmud, H., H. Seo, S. Kim, M.I. Islam, K.W. Nam, H.D. Cho, and H.Y. Song. BMC Complementary and Alternative Medicine, 2017. 17(279): 8pp. PMID[28545436]. PMCID[PMC5445392].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28653928">Synthesis, Characterization and Enzyme Inhibitory Activity of New Pyrazinamide Iron Complexes.</a> Ali, M., S.A.U. Qader, F. Shahid, M.S. Arayne, and M. Mumtaz. Pakistan Journal of Pharmaceutical Sciences, 2017. 30(3): p. 825-831. PMID[28653928]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28512022">Synthesis, Antimycobacterial Activity and Docking Study of 2-Aroyl-[1]benzopyrano[4,3-C]pyrazol-4(1H)-one Derivatives and Related Hydrazide-hydrazones.</a> Angelova, V.T., V. Valcheva, T. Pencheva, Y. Voynikov, N. Vassilev, R. Mihaylova, G. Momekov, and B. Shivachev. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(13): p. 2996-3002. PMID[28512022]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28661444">Glycosylation of Recombinant Antigenic Proteins from Mycobacterium tuberculosis: In Silico Prediction of Protein Epitopes and ex Vivo Biological Evaluation of New Semi-synthetic Glycoconjugates.</a> Bavaro, T., S. Tengattini, L. Piubelli, F. Mangione, R. Bernardini, V. Monzillo, S. Calarota, P. Marone, M. Amicosante, L. Pollegioni, C. Temporini, and M. Terreni. Molecules, 2017. 22(7): E1081. PMID[28661444]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28558683">Inhibition of Biofilm Formation in Mycobacterium smegmatis by Parinari curatellifolia Leaf Extracts.</a> Bhunu, B., R. Mautsa, and S. Mukanganyama. BMC Complementary and Alternative Medicine, 2017. 17(285): 10pp. PMID[28558683]. PMCID[PMC5450307].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28598408">Anti-mycobacterial Evaluation of 7-Chloro-4-aminoquinolines and Hologram Quantitative Structure-Activity Relationship (HQSAR) Modeling of Amino-imino Tautomers.</a> Bispo, M.L.F., C.H.S. Lima, L.N.F. Cardoso, A.L.P. Candea, F. Bezerra, M.C.S. Lourenco, M. Henriques, R.B. Alencastro, C.R. Kaiser, M.V.N. Souza, and M.G. Albuquerque. Pharmaceuticals, 2017. 10(2): E52. PMID[28598408]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28330892">In Vitro Susceptibility Testing of Tedizolid against Nontuberculous Mycobacteria.</a> Brown-Elliott, B.A. and R.J. Wallace, Jr. Journal of Clinical Microbiology, 2017. 55(6): p. 1747-1754. PMID[28330892]. PMCID[PMC5442531].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28542623">Gallium Nanoparticles Facilitate Phagosome Maturation and Inhibit Growth of Virulent Mycobacterium tuberculosis in Macrophages.</a> Choi, S.R., B.E. Britigan, D.M. Moran, and P. Narayanasamy. Plos One, 2017. 12(5): e0177987. PMID[28542623]. PMCID[PMC5436895].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28629354">Acetone Leaf Extracts of Some South African Trees with High Activity against Escherichia coli Also Have Good Antimycobacterial Activity and Selectivity Index.</a> Elisha, I.L., F.S. Botha, B. Madikizela, L.J. McGaw, and J.N. Eloff. BMC Complementary and Alternative Medicine, 2017. 17(327): 5pp. PMID[28629354]. PMCID[PMC5477271].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28475832">A Phenotypic Based Target Screening Approach Delivers New Antitubercular CTP Synthetase Inhibitors.</a> Esposito, M., S. Szadocka, G. Degiacomi, B.S. Orena, G. Mori, V. Piano, F. Boldrin, J. Zemanova, S. Huszar, D. Barros, S. Ekins, J. Lelievre, R. Manganelli, A. Mattevi, M.R. Pasca, G. Riccardi, L. Ballel, K. Mikusova, and L.R. Chiarelli. ACS Infectious Diseases, 2017. 3(6): p. 428-437. PMID[28475832]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28000551">Antimicrobial Evaluation of 5-Substituted aryl 1H-tetrazoles.</a> Feinn, L., J. Dudley, A. Coca, and E.L. Roberts. Medicinal Chemistry, 2017. 13(4): p. 359-364. PMID[28000551]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402352500012">Pt(II) and Ni(II) Complexes of Octahydropyrrolo[3,4-C]pyrrole N-benzoylthiourea Derivatives: Synthesis, Characterization, Physical Parameters and Biological Activity.</a> Gemili, M., H. Sari, M. Ulger, E. Sahin, and Y. Nural. Inorganica Chimica Acta, 2017. 463: p. 88-96. ISI[000402352500012]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402441900002">Synthesis, Docking Study and Biological Evaluation of Some New Thiourea Derivatives Bearing Benzenesulfonamide Moiety.</a> Ghorab, M.M., M.S.A. El-Gaby, A.M. Soliman, M.S. Alsaid, M.M. Abdel-Aziz, and M.M. Elaasser. Chemistry Central Journal, 2017. 11(42): 12pp. ISI[000402441900002]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28285521">Exploring Covalent Allosteric Inhibition of Antigen 85C from Mycobacterium tuberculosis by Ebselen Derivatives.</a> Goins, C.M., S. Dajnowicz, S. Thanna, S.J. Sucheck, J.M. Parks, and D.R. Ronning. ACS Infectious Diseases, 2017. 3(5): p. 378-387. PMID[28285521]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28415385">Antitubercular Activity of ZnO Nanoparticles Prepared by Solution Combustion Synthesis Using Lemon Juice as Bio-fuel.</a> Gopala Krishna, P., P. Paduvarahalli Ananthaswamy, P. Trivedi, V. Chaturvedi, N. Bhangi Mutta, A. Sannaiah, A. Erra, and T. Yadavalli. Materials Science &amp; Engineering. C, Materials for Biological Applications, 2017. 75: p. 1026-1033. PMID[28415385]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28504879">Antitubercular Activity of Mycelium-associated Ganoderma Lanostanoids.</a> Isaka, M., P. Chinthanom, M. Sappan, S. Supothina, V. Vichai, K. Danwisetkanjana, T. Boonpratuang, K.D. Hyde, and R. Choeyklin. Journal of Natural Products, 2017. 80(5): p. 1361-1369. PMID[28504879]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28392458">Anti-inflammatory, Antimycobacterial and Genotoxic Evaluation of Doliocarpus dentatus.</a> Ishikawa, R.B., M.M. Leitao, R.M. Kassuya, L.F. Macorini, F.M.F. Moreira, C.A.L. Cardoso, R.G. Coelho, A. Pott, G.M. Gelfuso, J. Croda, R.J. Oliveira, and C.A.L. Kassuya. Journal of Ethnopharmacology, 2017. 204: p. 18-25. PMID[28392458]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28429168">Bedaquiline Susceptibility Test for Totally Drug-resistant Tuberculosis Mycobacterium tuberculosis.</a> Jang, J.C., Y.G. Jung, J. Choi, H. Jung, and S. Ryoo. Journal of Microbiology, 2017. 55(6): p. 483-487. PMID[28429168]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28566651">A New Pyranonaphtoquinone Derivative, 4-Oxo-rhinacanthin A, from Roots of Indonesian Rhinacanthus nasutus.</a> Maarisit, W., H. Yamazaki, D.B. Abdjul, O. Takahashi, R. Kirikoshi, and M. Namikoshi. Chemical &amp; Pharmaceutical Bulletin, 2017. 65(6): p. 586-588. PMID[28566651]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400806100036">The Synthesis and in Vitro Screening of the 2-/3-Alkoxyphenylcarbamic acid Derivatives Containing a 4&#39;-(2&#39;-Fluorophenyl)piperazin-1&#39;-yl Moiety against Some Non-tuberculous Mycobacterial Strains.</a> Malik, I., J. Curillova, J. Csollei, J. Jampilek, I. Zadrazilova, R. Govender, J. O&#39;Mahony, A. Coffey, and P. Mikus. Fresenius Environmental Bulletin, 2017. 26(4): p. 2759-2770. ISI[000400806100036]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28438933">Pyrazinoic acid Inhibits a Bifunctional Enzyme in Mycobacterium tuberculosis.</a> Njire, M., N. Wang, B. Wang, Y. Tan, X. Cai, Y. Liu, J. Mugweru, J. Guo, H.M.A. Hameed, S. Tan, J. Liu, W.W. Yew, E. Nuermberger, G. Lamichhane, J. Liu, and T. Zhang. Antimicrobial Agents and Chemotherapy, 2017. 61(7): e00070-17. PMID[28438933]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28222400">Antiprotozoal, Antimycobacterial, and Anti-inflammatory Evaluation of Cnidoscolus chayamansa (Mc Vaugh) Extract and the Isolated Compounds.</a> Perez-Gonzalez, M.Z., G.A. Gutierrez-Rebolledo, L. Yepez-Mulia, I.S. Rojas-Tome, J. Luna-Herrera, and M.A. Jimenez-Arellanes. Biomedicine &amp; Pharmacotherapy, 2017. 89: p. 89-97. PMID[28222400]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28365981">Evaluation of the Antimicrobial Activity of Cationic Polymers against Mycobacteria: Toward Antitubercular Macromolecules.</a> Phillips, D.J., J. Harrison, S.J. Richards, D.E. Mitchell, E. Tichauer, A.T.M. Hubbard, C. Guy, I. Hands-Portman, E. Fullam, and M.I. Gibson. Biomacromolecules, 2017. 18(5): p. 1592-1599. PMID[28365981]. PMCID[PMC5435458].<b><br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000402994200009">Virtual Screening of Selective Inhibitors of Phosphopantetheine adenylyltransferase from Mycobacterium tuberculosis.</a> Podshivalov, D.D., V.I. Timofeev, D.D. Sidorov-Biryukov, and I.P. Kuranova. Crystallography Reports, 2017. 62(3): p. 405-410. ISI[000402994200009]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000401540200021">Antitubercular Activity Increase in Labdane Diterpenes from Copaifera Oleoresin through Structural Modification.</a> Silva, A.N., A.C.F. Soares, M.M.W. Cabral, A.P. de Andrade, M.B.M. da Silva, C.H.G. Martins, R.C.S. Veneziani, S. Ambrosio, J.K. Bastos, and V.C.G. Heleno. Journal of the Brazilian Chemical Society, 2017. 28(6): p. 1106-1112. ISI[000401540200021]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28528545">Discovery and Biosynthesis of Gladiolin: A Burkholderia gladioli Antibiotic with Promising Activity against Mycobacterium tuberculosis.</a> Song, L., M. Jenner, J. Masschelein, C. Jones, M.J. Bull, S.R. Harris, R.C. Hartkoorn, A. Vocat, I. Romero-Canelon, P. Coupland, G. Webster, M. Dunn, R. Weiser, C. Paisey, S.T. Cole, J. Parkhill, E. Mahenthiralingam, and G.L. Challis. Journal of the American Chemical Society, 2017. 139(23): p. 7974-7981. PMID[28528545]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28462832">6-Nitro-2,3-dihydroimidazo[2,1-b][1,3]thiazoles: Facile Synthesis and Comparative Appraisal against Tuberculosis and Neglected Tropical Diseases.</a> Thompson, A.M., A. Blaser, B.D. Palmer, R.F. Anderson, S.S. Shinde, D. Launay, E. Chatelain, L. Maes, S.G. Franzblau, B. Wan, Y. Wang, Z. Ma, and W.A. Denny. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(11): p. 2583-2589. PMID[28462832]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28621733">Discovery of Indeno[1,2-c]quinoline Derivatives as Potent Dual Antituberculosis and Anti-inflammatory Agents.</a> Tseng, C.H., C.W. Tung, C.H. Wu, C.C. Tzeng, Y.H. Chen, T.L. Hwang, and Y.L. Chen. Molecules, 2017. 22(6): E1001. PMID[28621733]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28383891">Ent-Abietane and Tigliane Diterpenoids from the Roots of Euphorbia fischeriana and Their Inhibitory Effects against Mycobacterium smegmatis.</a> Wang, C.J., Q.L. Yan, Y.F. Ma, C.P. Sun, C.M. Chen, X.G. Tian, X.Y. Han, C. Wang, S. Deng, and X.C. Ma. Journal of Natural Products, 2017. 80(5): p. 1248-1254. PMID[28383891]. <b> 
    <br />
    [PubMed]</b>. TB_06_2017.</p><br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000400372500181">A Comeback of the Antifolates: Developing a Novel and Potent Mycobacterium tuberculosis Dihydrofolate Reductase Inhibitor.</a> Zha, B.S., R. Liao, L. Ono, M. Pollastri, D. Lorimer, W.S. Weiner, S. Santhakumar, C. Walpole, and D.R. Sherman. American Journal of Respiratory and Critical Care Medicine, 2017. 195. ISI[000400372500181]. <b><br />
    [WOS]</b>. TB_06_2017.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28523106">Discovery of Fluorine-containing Benzoxazinyl-oxazolidinones for the Treatment of Multidrug Resistant Tuberculosis.</a> Zhao, H.Y., Y. Lu, L. Sheng, Z.S. Yuan, B. Wang, W.P. Wang, Y. Li, C. Ma, X.L. Wang, D.F. Zhang, and H.H. Huang. ACS Medicinal Chemistry Letters, 2017. 8(5): p. 533-537. PMID[28523106]. PMCID[PMC5430409].<b><br />
    [PubMed]</b>. TB_06_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">34. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170622&amp;CC=WO&amp;NR=2017103615A1&amp;KC=A1">Combination Product.</a> Biagini, G.A., S.A. Ward, G.L. Nixon, and P.M. O&#39;Neill. Patent. 2017. 2016-GB53972 2017103615: 102pp.
    <br />
    <b>[Patent]</b>. TB_06_2017.</p><br />

    <p class="plaintext">35. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170622&amp;CC=WO&amp;NR=2017100849A1&amp;KC=A1">Preparation of Pyrrolidine Nucleoside and Nucleotide Analogs as 6-Oxopurine phosphoribosyl Transferase Inhibitors.</a> De Jersey, J., L.W. Guddat, D. Hockova, D.T. Keough, R. Pohl, and D. Rejman. Patent. 2017. 2016-AU51238 2017100849: 85pp.
    <br />
    <b>[Patent]</b>. TB_06_2017.</p><br />

    <p class="plaintext">36. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20170622&amp;CC=US&amp;NR=2017174639A1&amp;KC=A1">Preparation of Sulfonamide Compounds Useful as Eis Inhibitors.</a> Garneau-Tsodikova, S., O.V. Tsodikov, and J.E. Posey. Patent. 2017. 2016-15381901 20170174639: 44pp.
    <br />
    <b>[Patent]</b>. TB_06_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
