

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="2004Archive.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="z5aQYLbtVcEbKX6rVAdpzn4+/zEm0UithT0F/VdxRzT/9m+JtctqbFTl80I5isdEH3L4ECmDZyI/ItQ5zgsZDWwr4Sro1sjTfqQS37QA0fvWNt4iRPQFW1JnMu0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="D581ED31" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>Literature Surveillance Memos</h1>

        <p>This page contains links to bi-weekly literature surveillance memos that identify relevant published research therapies on pre-clinical experimental therapies for HIV, the opportunistic infections (OIs) associated with AIDS and other viral pathogens.</p>
        <p>Note: Where available, URL links to article abstracts have been included for the convenience of the user. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users.  Abstract links to Web of Science (publishorperish.nih.gov) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>
        
        <div id="memo">
            <table id="memotable">
                <tr><th class="MemoColHIVArchive">HIV</th><th class="MemoColOIArchive">Opportunistic Infections</th><th class="MemoColOVArchive">Other Viral Pathogens</th></tr>
               	<tr><td><a href="HIV-LS-312.aspx" target="_blank">HIV-LS-312</a></td><td><a href="OI-LS-312.aspx" target="_blank">OI-LS-312</a></td><td><a href="dmid-ls-82.aspx" target="_blank">DMID-LS-82</a></td></tr>
                <tr><td><a href="HIV-LS-311.aspx" target="_blank">HIV-LS-311</a></td><td><a href="OI-LS-311.aspx" target="_blank">OI-LS-311</a></td><td><a href="dmid-ls-81.aspx" target="_blank">DMID-LS-81</a></td></tr>
                <tr><td><a href="HIV-LS-310.aspx" target="_blank">HIV-LS-310</a></td><td><a href="OI-LS-310.aspx" target="_blank">OI-LS-310</a></td><td><a href="dmid-ls-80.aspx" target="_blank">DMID-LS-80</a></td></tr>
                <tr><td><a href="HIV-LS-309.aspx" target="_blank">HIV-LS-309</a></td><td><a href="OI-LS-310.aspx" target="_blank">OI-LS-309</a></td><td><a href="dmid-ls-79.aspx" target="_blank">DMID-LS-79</a></td></tr>
                <tr><td><a href="HIV-LS-308.aspx" target="_blank">HIV-LS-308</a></td><td><a href="OI-LS-308.aspx" target="_blank">OI-LS-308</a></td><td><a href="dmid-ls-78.aspx" target="_blank">DMID-LS-78</a></td></tr>	
                <tr><td><a href="HIV-LS-307.aspx" target="_blank">HIV-LS-307</a></td><td><a href="OI-LS-307.aspx" target="_blank">OI-LS-307</a></td><td><a href="dmid-ls-77.aspx" target="_blank">DMID-LS-77</a></td></tr>
                <tr><td><a href="HIV-LS-306.aspx" target="_blank">HIV-LS-306</a></td><td><a href="OI-LS-306.aspx" target="_blank">OI-LS-306</a></td><td><a href="dmid-ls-76.aspx" target="_blank">DMID-LS-76</a></td></tr>
                <tr><td><a href="HIV-LS-305.aspx" target="_blank">HIV-LS-305</a></td><td><a href="OI-LS-305.aspx" target="_blank">OI-LS-305</a></td><td><a href="dmid-ls-75.aspx" target="_blank">DMID-LS-75</a></td></tr>
                <tr><td><a href="HIV-LS-304.aspx" target="_blank">HIV-LS-304</a></td><td><a href="OI-LS-304.aspx" target="_blank">OI-LS-304</a></td><td><a href="dmid-ls-74.aspx" target="_blank">DMID-LS-74</a></td></tr>
                <tr><td><a href="HIV-LS-303.aspx" target="_blank">HIV-LS-303</a></td><td><a href="OI-LS-303.aspx" target="_blank">OI-LS-303</a></td><td><a href="dmid-ls-73.aspx" target="_blank">DMID-LS-73</a></td></tr>
                <tr><td><a href="HIV-LS-302.aspx" target="_blank">HIV-LS-302</a></td><td><a href="OI-LS-302.aspx" target="_blank">OI-LS-302</a></td><td><a href="dmid-ls-72.aspx" target="_blank">DMID-LS-72</a></td></tr>
                <tr><td><a href="HIV-LS-301.aspx" target="_blank">HIV-LS-301</a></td><td><a href="OI-LS-301.aspx" target="_blank">OI-LS-301</a></td><td><a href="dmid-ls-71.aspx" target="_blank">DMID-LS-71</a></td></tr>
                <tr><td><a href="HIV-LS-300.aspx" target="_blank">HIV-LS-300</a></td><td><a href="OI-LS-300.aspx" target="_blank">OI-LS-300</a></td><td><a href="dmid-ls-70.aspx" target="_blank">DMID-LS-70</a></td></tr>
                <tr><td><a href="HIV-LS-299.aspx" target="_blank">HIV-LS-299</a></td><td><a href="OI-LS-299.aspx" target="_blank">OI-LS-299</a></td><td><a href="DMID-LS-69.aspx" target="_blank">DMID-LS-69</a></td></tr>
                <tr><td><a href="HIV-LS-298.aspx" target="_blank">HIV-LS-298</a></td><td><a href="OI-LS-298.aspx" target="_blank">OI-LS-298</a></td><td><a href="DMID-LS-68.aspx" target="_blank">DMID-LS-68</a></td></tr>
		        <tr><td><a href="HIV-LS-297.aspx" target="_blank">HIV-LS-297</a></td><td><a href="OI-LS-297.aspx" target="_blank">OI-LS-297</a></td><td><a href="DMID-LS-67.aspx" target="_blank">DMID-LS-67</a></td></tr>
                <tr><td><a href="HIV-LS-296.aspx" target="_blank">HIV-LS-296</a></td><td><a href="OI-LS-296.aspx" target="_blank">OI-LS-296</a></td><td><a href="DMID-LS-66.aspx" target="_blank">DMID-LS-66</a></td></tr>
                <tr><td><a href="HIV-LS-295.aspx" target="_blank">HIV-LS-295</a></td><td><a href="OI-LS-295.aspx" target="_blank">OI-LS-295</a></td><td><a href="DMID-LS-65.aspx" target="_blank">DMID-LS-65</a></td></tr>
                <tr><td><a href="HIV-LS-294.aspx" target="_blank">HIV-LS-294</a></td><td><a href="OI-LS-294.aspx" target="_blank">OI-LS-294</a></td><td><a href="DMID-LS-64.aspx" target="_blank">DMID-LS-64</a></td></tr>
                <tr><td><a href="HIV-LS-293.aspx" target="_blank">HIV-LS-293</a></td><td><a href="OI-LS-293.aspx" target="_blank">OI-LS-293</a></td><td><a href="DMID-LS-63.aspx" target="_blank">DMID-LS-63</a></td></tr>
                <tr><td><a href="HIV-LS-292.aspx" target="_blank">HIV-LS-292</a></td><td><a href="OI-LS-292.aspx" target="_blank">OI-LS-292</a></td><td><a href="DMID-LS-62.aspx" target="_blank">DMID-LS-62</a></td></tr>
                <tr><td><a href="HIV-LS-291.aspx" target="_blank">HIV-LS-291</a></td><td><a href="OI-LS-291.aspx" target="_blank">OI-LS-291</a></td><td><a href="DMID-LS-61.aspx" target="_blank">DMID-LS-61</a></td></tr>
                <tr><td><a href="HIV-LS-290.aspx" target="_blank">HIV-LS-290</a></td><td><a href="OI-LS-290.aspx" target="_blank">OI-LS-290</a></td><td></td></tr>
                <tr><td><a href="HIV-LS-289.aspx" target="_blank">HIV-LS-289</a></td><td><a href="OI-LS-289.aspx" target="_blank">OI-LS-289</a></td><td></td></tr>
		        <tr><td><a href="HIV-LS-288.aspx" target="_blank">HIV-LS-288</a></td><td><a href="OI-LS-288.aspx" target="_blank">OI-LS-288</a></td><td></td></tr>
                <tr><td><a href="HIV-LS-287.aspx" target="_blank">HIV-LS-287</a></td><td><a href="OI-LS-287.aspx" target="_blank">OI-LS-287</a></td><td></td></tr>
            </table>
			<fieldset id="memobottom">
			    <br /><br /><br />			
			    <fieldset class="submitbuttons" id="memobottomrow">
                    <ul id="memobottomrowbar">
	    		    	<li><a href="2005Archive.aspx">2005</a></li>
	    		    	<li><a href="2006Archive.aspx">2006</a></li>
	    			    <li><a href="2007Archive.aspx">2007</a></li>
	    			    <li><a href="2009Archive.aspx">2008-2009</a></li>
	    			    <li><a href="2010Archive.aspx">2010</a></li>
	    			    <li><a href="2011Archive.aspx">2011</a></li>
  				        <li><a href="2012Archive.aspx">2012</a></li>
   				        <li><a href="2013Archive.aspx">2013</a></li>
   				        <li><a href="2014Archive.aspx">2014</a></li>    
   				        <li><a href="2015Archive.aspx">2015</a></li>    
   				        <li><a href="2016Archive.aspx">2016</a></li>       				        
   				        <li><a href="memos.aspx">2017</a></li>    
                    </ul>                                      
    			    <br /><br />  				    			    
			    </fieldset>
            </fieldset>
            
        </div>

    </div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
