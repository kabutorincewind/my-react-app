

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-340.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="g/oWcz270+0QasPjEsObPwAGWiCB4bGfnQNvo48QswdE/Tb5yb+QqdufTWRIZRgs7SaQ2NbDu72x/eHiVKPQxCkMKWvI1yxoSqVPex7pIkls8YAVnwmgMEgrqCM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="5F48D481" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH HIV-LS-340-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48365   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Inhibitors of HIV-1 Protease by Using In Situ Click Chemistry</p>

    <p>          Whiting, M, Muldoon, J, Lin, YC, Silverman, SM, Lindstrom, W, Olson, AJ, Kolb, HC, Finn, MG, Sharpless, KB, Elder, JH, and Fokin, VV</p>

    <p>          Angew Chem Int Ed Engl <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16425339&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16425339&amp;dopt=abstract</a> </p><br />

    <p>2.     48366   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Tenofovir DF, emtricitabine, and efavirenz vs. zidovudine, lamivudine, and efavirenz for HIV</p>

    <p>          Gallant, JE, DeJesus, E, Arribas, JR, Pozniak, AL, Gazzard, B, Campo, RE, Lu, B, McColl, D, Chuck, S, Enejosa, J, Toole, JJ, and Cheng, AK</p>

    <p>          N Engl J Med <b>2006</b>.  354(3): 251-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421366&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16421366&amp;dopt=abstract</a> </p><br />

    <p>3.     48367   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Structure-Activity Relationship Studies of Novel Benzophenones Leading to the Discovery of a Potent, Next Generation HIV Nonnucleoside Reverse Transcriptase Inhibitor</p>

    <p>          Romines, KR, Freeman, GA, Schaller, LT, Cowan, JR, Gonzales, SS, Tidwell, JH, Andrews, CW 3rd, Stammers, DK, Hazen, RJ, Ferris, RG, Short, SA, Chan, JH, and Boone, LR</p>

    <p>          J Med Chem <b>2006</b>.  49(2): 727-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420058&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420058&amp;dopt=abstract</a> </p><br />

    <p>4.     48368   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          HIV Integrase Inhibitors with Nucleobase Scaffolds: Discovery of a Highly Potent Anti-HIV Agent</p>

    <p>          Nair, V, Chi, G, Ptak, R, and Neamati, N</p>

    <p>          J Med Chem <b>2006</b>.  49(2): 445-447</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420027&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16420027&amp;dopt=abstract</a> </p><br />

    <p>5.     48369   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Preferential Inhibition of the Magnesium-Dependent Strand Transfer Reaction of HIV-1 Integrase by {alpha}-Hydroxytropolones</p>

    <p>          Semenova, EA, Johnson, AA, Marchand, C, Davis, DA, Yarchoan, R, and Pommier, Y</p>

    <p>          Mol Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16418335&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16418335&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     48370   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Antiviral activity and cross-resistance profile of P-1946, a novel human immunodeficiency virus type 1 protease inhibitor</p>

    <p>          Sevigny, G, Stranix, B, Tian, B, Dubois, A, Sauve, G, Petropoulos, C, Lie, Y, Hellmann, N, Conway, B, and Yelle, J</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4J32H1X-2/2/84a20ee29617b8c623aa20df7790046f">http://www.sciencedirect.com/science/article/B6T2H-4J32H1X-2/2/84a20ee29617b8c623aa20df7790046f</a> </p><br />

    <p>7.     48371   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Neamine derivatives having a nucleobase with a lysine or an arginine as a linker, their synthesis and evaluation as potential inhibitors for HIV TAR-Tat</p>

    <p>          Yajima, S, Shionoya, H, Akagi, T, and Hamasaki, K</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16413787&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16413787&amp;dopt=abstract</a> </p><br />

    <p>8.     48372   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and anti-integrase activity of catechol-DKA hybrids</p>

    <p>          Maurin, C, Bailly, F, Mbemba, G, Mouscadet, JF, and Cotelle, P</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16412645&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16412645&amp;dopt=abstract</a> </p><br />

    <p>9.     48373   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Synthesis, antitumor and anti-HIV activities of benzodithiazine-dioxides</p>

    <p>          Brzozowski, Z, Saczewski, F, and Neamati, N</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16406643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16406643&amp;dopt=abstract</a> </p><br />

    <p>10.   48374   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Anti-HIV-1 protease- and HIV-1 integrase activities of Thai medicinal plants known as Hua-Khao-Yen</p>

    <p>          Tewtrakul, S, Itharat, A, and Rattanasuwan, P</p>

    <p>          J Ethnopharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16406414&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16406414&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   48375   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Anti-Tat and anti-HIV activities of trimers of n-alkylglycines</p>

    <p>          Marquez, N, Sancho, R, Macho, A, Moure, A, Masip, I, Messeguer, A, and Munoz, E</p>

    <p>          Biochem Pharmacol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16405868&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16405868&amp;dopt=abstract</a> </p><br />

    <p>12.   48376   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Structural Insights into the Mechanisms of Drug Resistance in HIV-1 Protease NL4-3</p>

    <p>          Heaslet, H, Kutilek, V, Morris, GM, Lin, YC, Elder, JH, Torbett, BE, and Stout, CD</p>

    <p>          J Mol Biol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16403521&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16403521&amp;dopt=abstract</a> </p><br />

    <p>13.   48377   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Design, synthesis, and biological evaluation of monopyrrolinone-based HIV-1 protease inhibitors possessing augmented P2&#39; side chains</p>

    <p>          Smith III, Amos B, Charnley, Adam K, Harada, Hironori, Beiger, Jason J, Cantin, Louis-David, Kenesky, Craig S, Hirschmann, Ralph, Munshi, Sanjeev, Olsen, David B, and Stahlhut, Mark W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(4): 859-863</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HKMPP6-5/2/fde2a4c769b5d49d0744245042179c3a">http://www.sciencedirect.com/science/article/B6TF9-4HKMPP6-5/2/fde2a4c769b5d49d0744245042179c3a</a> </p><br />

    <p>14.   48378   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Aminopyrimidinimino isatin analogues: design of novel non- nucleoside HIV-1 reverse transcriptase inhibitors with broad-spectrum chemotherapeutic properties</p>

    <p>          Sriram, D, Bal, TR, and Yogeeswari, P</p>

    <p>          J Pharm Pharm Sci <b>2005</b>.  8(3): 565-77</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16401403&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16401403&amp;dopt=abstract</a> </p><br />

    <p>15.   48379   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Human immunodeficiency virus type 1 gp120 inhibits long-term potentiation via chemokine receptor CXCR4 in rat hippocampal slices</p>

    <p>          Dong, J and Xiong, H</p>

    <p>          J Neurosci Res  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16400660&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16400660&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   48380   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Norepinephrine inhibits human immunodeficiency virus type-1 infection through the NF-[kappa]B inactivation</p>

    <p>          Moriuchi, Masako, Yoshimine, Hiroyuki, Oishi, Kazunori, and Moriuchi, Hiroyuki</p>

    <p>          Virology <b>2006</b>.  345(1): 167-173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4HGM73D-3/2/9e133a28b35343316263be16e4bb9875">http://www.sciencedirect.com/science/article/B6WXR-4HGM73D-3/2/9e133a28b35343316263be16e4bb9875</a> </p><br />

    <p>17.   48381   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Optimization of diarylamines as non-nucleoside inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Ruiz-Caro, Juliana, Basavapathruni, Aravind, Kim, Joseph T, Bailey, Christopher M, Wang, Ligong, Anderson, Karen S, Hamilton, Andrew D, and Jorgensen, William L</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(3): 668-671</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HKCYSV-1/2/ce87db516a8490685f5de13f308064a7">http://www.sciencedirect.com/science/article/B6TF9-4HKCYSV-1/2/ce87db516a8490685f5de13f308064a7</a> </p><br />

    <p>18.   48382   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          In vivo dynamics of the K103N mutation following the withdrawal of non-nucleoside reverse transcriptase inhibitors in Human Immunodeficiency Virus-infected patients</p>

    <p>          Gianotti, N, Galli, L, Boeri, E, Maillard, M, Serra, G, Ratti, D, Gallotta, G, Vacchini, D, Tremolada, Y, Lazzarin, A, Clementi, M, and Castagna, A</p>

    <p>          New Microbiol <b>2005</b>.  28(4): 319-26</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16386016&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16386016&amp;dopt=abstract</a> </p><br />

    <p>19.   48383   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          R5 variants of human immunodeficiency virus type 1 preferentially infect CD62L- CD4+ T cells and are potentially resistant to nucleoside reverse transcriptase inhibitors</p>

    <p>          Gondois-Rey, F, Biancotto, A, Fernandez, MA, Bettendroffer, L, Blazkova, J, Trejbalova, K, Pion, M, and Hirsch, I</p>

    <p>          J Virol <b>2006</b>.  80(2): 854-65</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378987&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378987&amp;dopt=abstract</a> </p><br />

    <p>20.   48384   HIV-LS-340; PUBMED-HIV-1/23/2006</p>

    <p class="memofmt1-2">          Discovery of Next Generation Inhibitors of HIV Protease</p>

    <p>          Spaltenstein, A, Kazmierski, WM, Miller, JF, and Samano, V</p>

    <p>          Curr Top Med Chem <b>2005</b>.  5(16): 1589-607</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375744&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16375744&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   48385   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Conformationally restrained carbazolone-containing [alpha],[gamma]-diketo acids as inhibitors of HIV integrase</p>

    <p>          Li, Xingnan and Vince, Robert</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4HYMW0Y-1/2/6bb52e518d534eb3fdb08a5f8ca2b7b6">http://www.sciencedirect.com/science/article/B6TF8-4HYMW0Y-1/2/6bb52e518d534eb3fdb08a5f8ca2b7b6</a> </p><br />

    <p>22.   48386   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Bleomycin has antiviral properties against drug-resistant HIV strains and sensitises virus to currently used antiviral agents</p>

    <p>          Georgiou, Niki A, van der Bruggen, Tjomme, Healy, Derek MC, van Tienen, Carla, de Bie, Prim, Oudshoorn, Maroeska, Marx, Joannes JM, and van Asbeck, BSweder</p>

    <p>          International Journal of Antimicrobial Agents <b>2006</b>.  27(1): 63-68</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4HR75KH-1/2/189ef187d2c7118ab04b119ddcba35aa">http://www.sciencedirect.com/science/article/B6T7H-4HR75KH-1/2/189ef187d2c7118ab04b119ddcba35aa</a> </p><br />

    <p>23.   48387   HIV-LS-340; EMBASE-HIV-1/24/2006</p>

    <p class="memofmt1-2">          Modeling of activity of cyclic urea HIV-1 protease inhibitors using regularized-artificial neural networks</p>

    <p>          Fernandez, Michael and Caballero, Julio</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(1): 280-294</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H7T0J0-3/2/4614f155a6b59734916ea15c034c0d34">http://www.sciencedirect.com/science/article/B6TF8-4H7T0J0-3/2/4614f155a6b59734916ea15c034c0d34</a> </p><br />

    <p>24.   48388   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Synthetic approaches to nuclease-resistant, nonnatural dinucleotides of anti-HIV integrase interest</p>

    <p>          Chi, GC and Nair, V</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2005</b>.  24(10-12): 1449-1468, 20</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900005</a> </p><br />

    <p>25.   48389   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Identification and characterization of a potent, selective nonpeptide agonist of the CC chemokine receptor CCR8</p>

    <p>          Haskell, CA, Horuk, R, Liang, M, Rosser, M, Dunning, L, Islam, I, Kremer, L, Gutierrez, J, Marquez, G, Martinez, C, Biscone, MJ, Doms, RW, and Ribeiro, S</p>

    <p>          MOLECULAR PHARMACOLOGY <b>2006</b>.  69(1): 309-316, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234252000032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234252000032</a> </p><br />

    <p>26.   48390   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Characterization of human immunodeficiency virus (HIV)-2 vector mobilization by HIV-1</p>

    <p>          Morris, KV and Looney, DJ</p>

    <p>          HUMAN GENE THERAPY <b>2005</b>.  16(12): 1463-1472, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234253000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234253000011</a> </p><br />
    <br clear="all">

    <p>27.   48391   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Human immunodeficiency virus (HIV) entry inhibitors (CCR5 specific blockers) in development: Are they the next novel therapies?</p>

    <p>          Idemyor, V</p>

    <p>          HIV CLINICAL TRIALS <b>2005</b>.  6(5): 272-277, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234165600005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234165600005</a> </p><br />

    <p>28.   48392   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Requirement of an additional Sam68 domain for inhibition of human immunodeficiency virus type 1 replication by Sam68 dominant negative mutants lacking the nuclear localization signal</p>

    <p>          Zhang, JZ, Liu, Y, Henao, J, Rugeles, MT, Li, JL, Chen, T, and He, JJ</p>

    <p>          GENE <b>2005</b>.  363: 67-76, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234166700008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234166700008</a> </p><br />

    <p>29.   48393   HIV-LS-340; WOS-HIV-1/15/2006</p>

    <p class="memofmt1-2">          Second-generation cycloSal-d4TMP pronucleotides bearing esterase-cleavable sites - the &quot;trapping&quot; concept</p>

    <p>          Meier, C, Ducho, C, Jessen, H, Vukadinovic-Tenter, D, and Balzarini, J</p>

    <p>          EUROPEAN JOURNAL OF ORGANIC CHEMISTRY <b>2005</b>.(1): 197-206, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234237500019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234237500019</a> </p><br />

    <p>30.   48394   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          Phenolics with anti-HSV and anti-HIV activities from Artocarpus gomezianus , Mallotus pallidus , and Triphasia trifolia</p>

    <p>          Likhitwitayawuid, K, Supudompol, B, Sritularak, B, Lipipun, V, Rapp, K, and Schinazi, RF</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2005</b>.  43(8): 651-657, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234398600002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234398600002</a> </p><br />

    <p>31.   48395   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          Simple synthesis and anti-HIV activity of novel cyclopentene phosphonate nucleosides</p>

    <p>          Kim, A and Hong, JH</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2006</b>.  25(1): 1-8, 8</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200001</a> </p><br />

    <p>32.   48396   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          New AZT conjugates as potent anti-HIV agents</p>

    <p>          You, ZQ and Lee, HJ</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2006</b>.  25(1): 37-54, 18</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200005</a> </p><br />

    <p>33.   48397   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          Virologic and enzymatic studies revealing the mechanism of K65R- and Q151M-associated HIV-1 drug resistance towards emtricitabine and lamivudine</p>

    <p>          Feng, JY, Myrick, FT, Margot, NA, Mulamba, GB, Rimsky, L, Borroto-Esoda, K, Selmi, B, and Canard, B</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2006</b>.  25(1): 89-107, 19</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200009</a> </p><br />

    <p>34.   48398   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p><b>          Synthesis and antiviral evaluation of novel 4 &#39;-hydroxymethyl branched apiosyl nucleosides</b> </p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS <b>2006</b>.  25(1): 109-117, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234388200010</a> </p><br />

    <p>35.   48399   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          CCR5 Delta 32, CCR2-64I, and SDF1-3 &#39; A polymorphisms related to resistance to HIV-1 infection and disease in the Ecuadorian population</p>

    <p>          Paz-y-Mino, C, Morillo, SA, Celi, AP, Witte, T, Guijarro, MJM, Collantes, JC, and Leone, PE</p>

    <p>          HUMAN BIOLOGY <b>2005</b>.  77(4): 521-526, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234289000010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234289000010</a> </p><br />

    <p>36.   48400   HIV-LS-340; WOS-HIV-1/22/2006</p>

    <p class="memofmt1-2">          Tipranavir: a protease inhibitor for multi-drug resistant HIV-1</p>

    <p>          Best, B and Haubrich, R</p>

    <p>          EXPERT OPINION ON INVESTIGATIONAL DRUGS <b>2006</b>.  15(1): 59-70, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234421000006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234421000006</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
