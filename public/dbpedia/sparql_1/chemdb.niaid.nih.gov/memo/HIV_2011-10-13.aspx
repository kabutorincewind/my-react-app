

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2011-10-13.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="AMMks/qUMJuQ9I6ENb5OoscyZmPiKZTnj/hvDgiDpvjOvFvwtlLk4rJlCaeuDed90MUWtyc5BlwI3qeg+jmals+q7cPOwWGKDK1CogWc4xRI+7PvrPb2mRoVbOQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2B17DF25" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">

<h1 class="memofmt2-h1">HIV Citation List: September 30, 2011 - October 13, 2011</h1>

    <p class="memofmt2-2">Pubmed citations</p>

<p class="NoSpacing"> 1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21600933">Selective inhibition of Human Immunodeficiency Virus type 1 (HIV-1) by a novel family of tricyclic nucleosides.</a> Bonache, M.C., A. Cordeiro, E. Quesada, E. Vanstreels, D. Daelemans, M.J. Camarasa, J. Balzarini, and A. San-Felix, Antiviral Research, 2011. 92(1): p. 37-44; PMID[ 21600933]. <br /><b>[PubMed]</b>. HIV_0930-101311. </p>
<p> </p>
<p class="NoSpacing"> 2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21723379">Inhibition of HIV-1 Reverse Transcriptase, Toxicological and Chemical Profile of Calophyllum brasiliense Extracts from Chiapas, Mexico.</a> Cesar, G.Z., M.G. Alfonso, M.M. Marius, E.M. Elizabeth, C.B. Angel, H.R. Maira, C.L. Guadalupe, J.E. Manuel, and R.C. Ricardo, Fitoterapia, 2011. 82(7): p. 1027-1034; PMID[ 21723379]. <br /> <b>[PubMed]</b>. HIV_0930-101311. </p>
<p> </p>
<p class="NoSpacing"> 3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21864952">Anti-AIDS Agents 86. Synthesis and anti-HIV Evaluation of 2',3' -Seco-3'-nor DCP and DCK Analogues. </a> Chen, Y., M. Cheng, F.Q. Liu, P. Xia, K. Qian, D. Yu, Y. Xia, Z.Y. Yang, C.H. Chen, S.L. Morris- Natschke, and K.H. Lee, European Journal of Medicinal Chemistry, 2011. 46(10): p. 4924-4936; PMID[ 21864952].<br /><b>[PubMed]</b>. HIV_0930-101311. </p> 
<p> </p>
<p class="NoSpacing"> 4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21974685">3-Hydroxy-6,7-dihydropyrimido[2,1-c][1,4]oxazin-4(9H)-ones as New HIV-1 Integrase Inhibitors WO2011046873 A1. </a> Cotelle, P., Expert Opinion on Therapeutic Patents, 2011. <b>[Epub ahead of print]</b>; PMID[ 21974685].<br /><b>[PubMed]</b>. HIV_0930-101311. </p>
<p> </p>
<p class="NoSpacing"> 5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21976775">Dimethyl Fumarate , an Immune Modulator and Inducer of the Antioxidant Response, Suppresses HIV Replication and Macrophage-Mediated Neurotoxicity: A Novel Candidate for HIV  Neuroprotection . </a> &nbsp; Cross, S.A., D.R. Cook, A.W. Chi, P.J. Vance, L.L. Kolson , B.J. Wong, K.L. Jordan- Sciutto , and D.L. Kolson , Journal of Immunology, 2011. <b>[Epub ahead of print]</b>; PMID[ 21976775]. <br /><b>[PubMed]</b>.  HIV_0930-101311. </p> 
<p> </p>
<p class="NoSpacing"> 6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21767569">4-[1-(4-Fluorobenzyl)-4-hydroxy-1H-indol-3-yl]-2-hydroxy-4-oxobut-2-enoic acid as a Prototype to Develop Dual Inhibitors of HIV-1 Integration Process.</a> De Luca, L., R. Gitto , F. Christ, S. Ferro, S. De Grazia , F. Morreale , Z. Debyser , and A. Chimirri , Antiviral Research, 2011. 92(1): p. 102-107; PMID[ 21767569].<br /><b>[PubMed]</b>. HIV_0930-101311. </p>
<p> </p>
<p class="NoSpacing"> 7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21727088">Broad-spectrum Aptamer Inhibitors of HIV Reverse Transcriptase Closely Mimic Natural Substrates.</a> Ditzler, M.A., D. Bose, N. Shkriabai, B. Marchand, S.G. Sarafianos, M. Kvaratskhelia, and D.H. Burke, Nucleic Acids Research, 2011. 39(18): p. 8237-8247; PMID[ 21727088].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p>
<p> </p>
<p class="NoSpacing"> 8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21725586">An anti-HIV-1 Compound that Increases Steady-state Expression of Apoplipoprotein B mRNA-editing Enzyme-catalytic Polypeptide-like 3G.</a> Ejima, T., M. Hirota, T. Mizukami, M. Otsuka, and M. Fujita, International Journal of Molecular Medicine, 2011. 28(4): p. 613-616; PMID[ 21725586].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21902586">Susceptibility of HIV Type 2 Primary Isolates to CCR5 and CXCR4 Monoclonal Antibodies, Ligands, and Small Molecule Inhibitors.</a> Espirito-Santo, M., Q. Santos-Costa, M. Calado, P. Dorr, and J.M. Azevedo -Pereira, AIDS Research and Human Retroviruses, 2011. <b>[Epub ahead of print]</b>; PMID[ 21902586].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985673">Design and Synthesis of Conformationally Constrained Inhibitors of Non-Nucleoside Reverse Transcriptase.</a> Gomez, R., S. Jolly, T.M. Williams, J.P. Vacca, M. Torrent, G. McGaughey, M.T. Lai, P.J. Felock, V. Munshi, D. Distefano, J.A. Flynn, M.D. Miller, Y. Yan, J.C. Reid, R. Sanchez, Y. Liang, B. Paton, B.L. Wan, and N.J. Anthony, Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[ 21985673].<br /><b>[PubMed]</b>. HIV_0930-101311.</p>
<p> </p>
<p class="NoSpacing"> 11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21849441">Nuclear Receptor Signaling Inhibits HIV-1 Replication in Macrophages through Multiple trans-Repression Mechanisms.</a> Hanley, T.M. and G.A. Viglianti, Journal of Virology, 2011. 85(20): p. 10834-10850; PMID[ 21849441].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21984685">Synthesis of 1-Benzyl-3-(3,5-dimethylbenzyl)uracil Derivatives with Potential anti-HIV Activity. </a> Isono, Y., N. Sakakibara, P. Ordonez, T. Hamasaki, M. Baba, M. Ikejiri, and T. Maruyama, Antiviral Chemistry &amp; Chemotherapy, 2011. 22(2): p. 57-65; PMID[ 21984685].<br /><b>[PubMed]</b>. HIV_0930-101311.</p>
<p> </p>
<p class="NoSpacing"> 13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21853995">Efficient Discovery of Potent Anti-HIV Agents Targeting the Tyr181Cys Variant of HIV Reverse Transcriptase.</a> Jorgensen, W.L., M. Bollini, V.V. Thakur, R.A. Domaoal, K.A. Spasov, and K.S. Anderson, Journal of the American Chemical Society, 2011. 133(39): p. 15686-15696; PMID[ 21853995].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21888325">Enantioselective Total Synthesis of the Potent anti-HIV Nucleoside EFdA.</a> Kageyama, M., T. Nagasawa, M. Yoshida, H. Ohrui, and S. Kuwahara, Organic Letters, 2011. 13(19): p. 5264-5266; PMID[ 21888325].<br /><b>[PubMed]</b>. HIV_0930-101311.</p>
<p> </p>
<p class="NoSpacing"> 15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21967290">Design and Synthesis of Carbocyclic Versions of Furanoid Nucleoside phosphonic acid Analogues as Potential anti-HIV Agents. </a>Kim, E., G.H. Shen, and J.H. Hong, Nucleosides, Nucleotides &amp; Nucleic Acids, 2011. 30(10): p. 798-813; PMID[ 21967290].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21950642">Telaprevir: Novel NS3/4 Protease Inhibitor for the Treatment of Hepatitis C.</a> Klibanov, O.M., S.H. Williams, L.S. Smith, J.L. Olin, and S.B. Vickery, Pharmacotherapy, 2011. 31(10): p. 951-974; PMID[ 21950642].<br /><b>[PubMed]</b>. HIV_0930-101311. </p> 
<p> </p>
<p class="NoSpacing"> 17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21967289">Synthesis of Novel 6'-Spirocyclopropyl-5'-norcarbocyclic adenosine phosphonic acid Analogues as Potent anti-HIV Agents. </a> Liu, L.J., E. Kim, and J.H. Hong, Nucleosides, Nucleotides &amp; Nucleic Acids, 2011. 30(10): p. 784-797; PMID[ 21967289].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21683097">Thiated Derivatives of 2',3'-Dideoxy-3'-fluorothymidine: Synthesis, in Vitro anti-HIV-1 Activity and Interaction with Recombinant Drug Resistant HIV-1 Reverse Transcriptase Forms. </a> Miazga, A., F. Hamy, S. Louvel, T. Klimkait, Z. Pietrusiewicz, A. Kurzynska-Kokorniak, M. Figlerowicz, P. Winska, and T. Kulikowski, Antiviral Research, 2011. 92(1): p. 57-63; PMID[ 21683097].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21880398">Synthesis and in Vitro Activity of Novel N-3 Acylated TSAO-T Compounds Against HIV-1 and HCV.</a> Moura, M., S. Josse, A.N. Van Nhien, C. Fournier, G. Duverlie, S. Castelain, E. Soriano, J. Marco- Contelles, J. Balzarini, and D. Postel, European Journal of Medicinal Chemistry, 2011. 46(10): p. 5046-5056; PMID[ 21880398].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21903401">1-[2-(2-Benzoyl- and 2-Benzylphenoxy)ethyl]uracils as Potent anti-HIV-1 Agents.</a> Novikov, M.S., O.N. Ivanova, A.V. Ivanov, A.A. Ozerov, V.T. Valuev -Elliston, K. Temburnikar, G.V. Gurskaya, S.N. Kochetkov, C. Pannecouque, J. Balzarini, and K.L. Seley-Radtke, Bioorganic &amp; Medicinal Chemistry, 2011. 19(19): p. 5794-5802; PMID[ 21903401].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21915419">Design, Synthesis and Biological Evaluation of Optically Pure Functionalized spiro [ 5,5]Undecane-1,5,9-triones as HIV-1 Inhibitors.</a> Ramachary, D.B., Y.V. Reddy, A. Banerjee, and S. Banerjee, Organic &amp; Biomolecular Chemistry, 2011. 9(21): p. 7282-7286; PMID[ 21915419].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21971360">Phenotypic Susceptibility of HIV-2 to Raltegravir: Integrase Mutations Q148R and N155H Confer Raltegravir Resistance. </a> Smith, R.A., D.N. Raugi, N.B. Kiviat, S.E. Hawes, J.I. Mullins, P.S. Sow, and G.S. Gottlieb, AIDS, 2011. <b>[Epub ahead of print]</b>; PMID[ 21971360].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21969102">Binding of Novel Fullerene Inhibitors to HIV-1 Protease: Insight through Molecular Dynamics and Molecular Mechanics Poisson-Boltzmann Surface Area Calculations. </a> Tzoupis, H., G. Leonis, S. Durdagi, V. Mouchlis, T. Mavromoustakos, and M.G. Papadopoulos, Journal of Computer-Aided Molecular Design, 2011. <b>[Epub ahead of print]</b>; PMID[ 21969102].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21961709">Designing Hypothesis of Substituted Benzoxazinones as HIV-1 Reverse Transcriptase Inhibitors: QSAR Approach. </a> Veerasamy, R., D.K. Subramaniam, O.C. Chean, and N.M. Ying, Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[ 21961709].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21872971">Arylazolylthioacetanilide. Part 8(): Design, Synthesis and Biological Evaluation of Novel 2-(2-(2,4 -Dichlorophenyl)-2H-1,2,4-triazol-3-ylthio)-N- arylacetamides as Potent HIV-1 Inhibitors. </a> Zhan, P., X. Chen, X. Li, D. Li, Y. Tian, W. Chen, C. Pannecouque, E. De Clercq, and X. Liu, European Journal of Medicinal Chemistry, 2011. 46(10): p. 5039-5045; PMID[ 21872971].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21985312">Ionic Derivatives of Betulinic acid as Novel HIV-1 Protease Inhibitors.</a> Zhao, H., S.S. Holmes, G.A. Baker, S. Challa, H.S. Bose, and Z. Song, Journal of Enzyme Inhibition and Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[ 21985312].<br /><b>[PubMed]</b>.  HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21970838">Metabolism of Novel anti-HIV Agent 3-Cyanomethyl-4-methyl-DCK by Human liver Microsomes and Recombinant CYP Enzymes.</a> Zhuang, X.M., J.T. Deng, H. Li, W.L. Kong, J.X. Ruan, and L. Xie, Acta Pharmacologica Sinica 2011. 32(10): p. 1276-1284; PMID[ 21970838].<br /><b>[PubMed]</b>. HIV_0930-101311.</p> 

<p class="memofmt2-2">ISI Web of Knowledge citations:</p>

<p class="NoSpacing"> 28. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294802800013">Rationally Designed Interfacial Peptides Are Efficient In Vitro Inhibitors of HIV-1 Capsid Assembly with Antiviral Activity</a>. Bocanegra, R.N., M Domenech, R Lopez, I Abian, O Rodriguez- Huete, A Cavasotto, CN Velazquez- Campoy, A Gomez, J Martinez, MA Neira, JL Mateu, MG, Plos One, 2011. 6(9); ISI[ 000294802800013]. <br /> <b>[WOS]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 29. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295001100024">Synthesis of the Anti-HIV Agent (-)- Hyperolactone C by Using Oxonium Ylide Formation-Rearrangement.</a> Hodgson, D.M. and S. Man, Chemistry-a European Journal, 2011. 17(35): p. 9731-9737; ISI[ 000295001100024]. <br /> <b>[WOS]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 30. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294790400010">Multisite lambda Dynamics for Simulated Structure-Activity Relationship Studies.</a> Knight, J.L. and Brooks, Journal of Chemical Theory and Computation, 2011. 7(9): p. 2728-2739; ISI[ 000294790400010]. <br /> <b>[WOS]</b>. HIV_0930-101311.</p> 
<p> </p>
<p class="NoSpacing"> 31. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000294936800014">High-throughput Assay to Identify Inhibitors of Vpu -mediated down-regulation of Cell Surface BST-2.</a> Zhang, Q.L., ZL Mi, ZY Li, XY Jia, PP Zhou, JM Yin, X You, XF Yu, LY Guo, F Ma, J Liang, C Cen, S, Antiviral Research, 2011. 91(3): p. 321-329; ISI[ 000294936800014]. <br /> <b>[WOS]</b>. HIV_0930-101311.</p>
<p> </p>
<p class="NoSpacing"> 32. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000295030000038">Zinc-finger Antiviral Protein Inhibits HIV-1 Infection by Selectively Targeting Multiply Spliced Viral mRNAs for Degradation</a>. Zhu, Y.C., GF Lv, FX Wang, XL Ji, X Xu, YH Sun, J Wu, L Zheng, YT Gao, GX, Proceedings of the National Academy of Sciences of the United States of America, 2011. 108(38): p. 15834-15839; ISI[ 000295030000038]. <br /> <b>[WOS]</b>. HIV_0930-101311.</p>

</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
