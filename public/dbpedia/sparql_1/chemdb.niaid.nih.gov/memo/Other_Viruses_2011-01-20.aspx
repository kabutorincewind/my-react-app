

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-01-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4NGFP4GlopZBY4LA2G9A6KiNazySw2w0jiOoBalBsVaQdiMMagkhzZPkzM9KyvJyL/6ouybyT/r2VfaOZgWMVfWxZFgPNG8gunUWh1nYgZHXVQKgC9BI0p2ys7U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA030E3B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <p class="memofmt2-h1">Other Viruses Citation List: </p>

    <p class="memofmt2-h1">January 7 - January 20, 2011</p>

    <p class="memofmt2-2">Human Cytomegalovirus</p>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21232828">Synthesis of New C5-(1-Substituted-1,2,3-Triazol-4 or 5-Yl)-2&#39;-Deoxyuridines and Their Antiviral Evaluation</a>. Montagu, A., V. Roy, J. Balzarini, R. Snoeck, G. Andrei, and L.A. Agrofoglio. European Journal of Medicinal Chemistry, 2010. <b>[Epub ahead of print]</b>; PMID[21232828].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p class="memofmt2-2">Herpes simplex virus</p>

     <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21243614">Novel Benzimidazo[2,1-C][1,4]Thiazinone Derivatives with Potent Activity against Hsv-1</a>. Galal, S.A., S.I. Naem, A.O. Nezhawy, M.A. Ali, and H.I. Diwani. Archiv Der Pharmazie, 2011. <b>[Epub ahead of print]</b>; PMID[21243614].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21225264">Oxoquinoline Derivatives: Identification and Structure-Activity Relationship (Sar) Analysis of New Anti-Hsv-1 Agents</a>. Abreu, P.A., V.A. da Silva, F.C. Santos, H.C. Castro, C.S. Riscado, M.T. de Souza, C.P. Ribeiro, J.E. Barbosa, C.C. Dos Santos, C.R. Rodrigues, V. Lione, B.A. Correa, A.C. Cunha, V.F. Ferreira, M.C. de Souza, and I.C. Paixao. Current Microbiology, 2011. <b>[Epub ahead of print]</b>; PMID[21225264].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21221922">Antiviral Activity of Recombinant Cyanovirin-N against Hsv-1</a>. Yu, H., Z.T. Liu, R. Lv, and W.Q. Zhang. Virologica Sinica, 2010. 25(6): p. 432-9; PMID[21221922].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p class="memofmt2-2">Hepatitis B virus</p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21218816">Design and Synthesis of Oxymatrine Analogues Overcoming Drug Resistance in Hepatitis B Virus through Targeting Host Heat Stress Cognate 70</a>. Gao, L.M., Y.X. Han, Y.P. Wang, Y.H. Li, Y.Q. Shan, X. Li, Z.G. Peng, C.W. Bi, T. Zhang, N.N. Du, J.D. Jiang, and D.Q. Song. Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21218816].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p class="memofmt2-2">Hepatitis C virus</p>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21235805">Characterization of Thiobarbituric Acid Derivatives as Inhibitors of Hepatitis C Virus Ns5b Polymerase</a>. Lee, J.H., S. Lee, M.Y. Park, and H. Myung. Virology Journal, 2011. 8(1): p. 18; PMID[21235805].</p>

    <p class="plaintext"><b>[Pubmed]</b>. OV_0107-012011.</p>

    <p class="memofmt2-2"> </p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p class="memofmt2-2"> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285237000003">Pyridine Carboxamides: Potent Palm Site Inhibitors of Hcv Ns5b Polymerase</a>. Cheng, C.C., X.H. Huang, G.W. Shipps, Y.S. Wang, D.F. Wyss, K.A. Soucy, C.K. Jiang, S. Agrawal, E. Ferrari, Z.Q. He, and H.C. Huang. ACS Medicinal Chemistry Letters, 2010. 1(9): p. 466-471; ISI[000285237000003].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0107-012011.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000285383400008">Selective Irreversible Inhibition of a Protease by Targeting a Noncatalytic Cysteine</a>. Hagel, M., D.Q. Niu, T. St Martin, M.P. Sheets, L.X. Qiao, H. Bernard, R.M. Karp, Z.D. Zhu, M.T. Labenski, P. Chaturvedi, M. Nacht, W.F. Westlin, R.C. Petter, and J. Singh. Nature Chemical Biology, 2011. 7(1): p. 22-24; ISI[000285383400008].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0107-012011.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
