

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-05-22.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bmxXuTOgT+OjFrxZWArIPxGo+AJn7I8U3rtq26IQTgqKAqVR1pm1iHimVQVsXMEKdwaH2Wmj9AUv6SpoYKhZymnWvy8CmCGygf9kLKszhHxkt5dF9QkhZdj/vyk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="70320F66" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: May 9 - May 22, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24751439">Synthesis, anti-HIV and Cytostatic Evaluation of 3&#39;-Deoxy-3&#39;-fluorothymidine (FLT) Pro-nucleotides.</a> Velanguparackel, W., N. Hamon, J. Balzarini, C. McGuigan, and A.D. Westwell. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(10): p. 2240-2243. PMID[24751439].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=24827152">Dual Role of Novel Ingenol Derivatives from Euphorbia tirucalli in HIV Replication: Inhibition of de Novo Infection and Activation of Viral LTR.</a> Abreu, C.M., S.L. Price, E.N. Shirk, R.D. Cunha, L.F. Pianowski, J.E. Clements, A. Tanuri, and L. Gama. PloS One, 2014. 9(5): p. e97257. PMID[24827152].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0509-052214.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334008100012">ADS-J1 Inhibits HIV-1 Infection and Membrane Fusion by Targeting the Highly Conserved Pocket in the gp41 NHR-Trimer.</a> Yu, F., L. Lu, Q. Liu, X.W. Yu, L.L. Wang, E. He, P. Zou, L.Y. Du, R.W. Sanders, S.W. Liu, and S.B. Jiang. Biochimica et Biophysica Acta-Biomembranes, 2014. 1838(5): p. 1296-1305. ISI[000334008100012].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334060900002">New Dinucleoside phosphonate Derivatives as Prodrugs of 3&#39;-Azido-3&#39;-deoxythymidine and beta-L-2&#39;,3&#39;-Dideoxy-3&#39;-thiacytidine: Synthesis and anti-HIV Properties.</a> Solyev, P.N., M.V. Jasko, I.L. Karpenko, Y.A. Sharkin, A.V. Shipitsyn, and M.K. Kukhanova. Nucleosides Nucleotides &amp; Nucleic Acids, 2014. 33(2): p. 64-79. ISI[000334060900002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334467200002">Medicinal Attributes of Solanum xanthocarpum Fruit Consumed by Several Tribal Communities as Food: An in Vitro Antioxidant, Anticancer and Anti HIV Perspective.</a> Kumar, S. and A.K. Pandey. BMC Complementary and Alternative Medicine, 2014. 14. ISI[000334467200002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334060900004">Synthesis of Novel 2&#39;-Fluoro-3&#39;-hydroxymethyl-5&#39;-deoxythreosyl phosphonic acid Nucleoside Analogues as Antiviral Agents.</a> Kim, K.M. and J.H. Hong. Nucleosides Nucleotides &amp; Nucleic Acids, 2014. 33(2): p. 92-109. ISI[000334060900004].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334282700011">New 5-Ylidene rhodanine Derivatives Based on the Dispacamide A Model.</a> Guiheneuf, S., L. Paquin, F. Carreaux, E. Durieu, T. Roisnel, L. Meijer, and J.P. Bazureau. Molecular Diversity, 2014. 18(2): p. 375-388. ISI[000334282700011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000334061300003">Synthesis of Potential Pyrimidine Derivatives via Suzuki Cross-coupling Reaction as HIV and Kinesin Eg5 Inhibitors.</a> Al-Masoudi, N.A., A.G. Kassim, and N.A. Abdul-Reda. Nucleosides Nucleotides &amp; Nucleic Acids, 2014. 33(3): p. 141-161. ISI[000334061300003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0509-052214.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
