

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2017-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Tyxvmg7W70VdZ1ePVgSCKt7EkOhZQFVWMsbrkAwfRuHixiQk0+MYJ8aWnys0nWSDX544m7QQxJTuFMZitlR9LBJf6eKyvlEjfj6ZI4ijrj+OLmIHeG5zoaHOY8U=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F8E22E54" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1"><a name="_Toc479675800">HIV Citations List: November, 2017</a></h1>

    <h2><a name="_Toc479675801">Literature Citations</a></h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29121950">The HIV-1 Integrase-LEDGF Allosteric Inhibitor MUT-A: Resistance Profile, Impairment of Virus Maturation and Infectivity but without Influence on RNA Packaging or Virus Immunoreactivity.</a> Amadori, C., Y.U. van der Velden, D. Bonnard, I. Orlov, N. van Bel, E. Le Rouzic, L. Miralles, J. Brias, F. Chevreuil, D. Spehner, S. Chasset, B. Ledoussal, L. Mayr, F. Moreau, F. Garcia, J. Gatell, A. Zamborlini, S. Emiliani, M. Ruff, B.P. Klaholz, C. Moog, B. Berkhout, M. Plana, and R. Benarous. Retrovirology, 2017. 14(1): p. 50. PMID[29121950]. PMCID[PMC5680779].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28434186">New Thiourea and 1,3-Thiazolidin-4-one Derivatives Effective on the HIV-1 Virus.</a> Bielenica, A., G. Sanna, S. Madeddu, M. Struga, M. Jozwiak, A.E. Koziol, A. Sawczenko, I.B. Materek, A. Serra, and G. Giliberti. Chemical Biology &amp; Drug Design, 2017. 90(5): p. 883-891. PMID[28434186].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28904199">Calcitonin Gene-related Peptide Induces HIV-1 Proteasomal Degradation in Mucosal Langerhans Cells.</a> Bomsel, M. and Y. Ganor. Journal of Virology, 2017. 91(23): e01205-01217. PMID[28904199]. PMCID[PMC5686740].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29024587">Discovery of a Distinct Chemical and Mechanistic Class of Allosteric HIV-1 Integrase Inhibitors with Antiretroviral Activity.</a> Burlein, C., C. Wang, M. Xu, T. Bhatt, M. Stahlhut, Y. Ou, G.C. Adam, J. Heath, D.J. Klein, J. Sanders, K. Narayan, P. Abeywickrema, M.R. Heo, S.S. Carroll, J.A. Grobler, S. Sharma, T.L. Diamond, A. Converso, and D.J. Krosky. ACS Chemical Biology, 2017. 12(11): p. 2858-2865. PMID[29024587].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29152054">Structure Optimization of Aloperine Derivatives as HIV-1 Entry Inhibitors.</a> Dang, Z., H. Xie, L. Zhu, Q.Y. Zhang, Z.J. Li, L. Huang, and C.H. Chen. ACSMedicinal Chemistry Letters, 2017. 8(11): p. 1199-1203. PMID[29152054]. PMCID[PMC5683702].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28874382">Evaluation of the Innate Immune Modulator Acitretin as a Strategy to Clear the HIV Reservoir.</a>Garcia-Vidal, E., M. Castellvi, M. Pujantell, R. Badia, A. Jou, L. Gomez, T. Puig, B. Clotet, E. Ballana, E. Riveira-Munoz, and J.A. Este. Antimicrobial Agents and Chemotherapy, 2017. 61(11): e01368-01317. PMID[28874382]. PMCID[PMC5655051].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28958624">Design, Synthesis, X-ray Studies, and Biological Evaluation of Novel Macrocyclic HIV-1 Protease Inhibitors Involving the P1&#39;-P2&#39; Ligands.</a> Ghosh, A.K., W. Sean Fyvie, M. Brindisi, M. Steffey, J. Agniswamy, Y.F. Wang, M. Aoki, M. Amano, I.T. Weber, and H. Mitsuya. Bioorganic &amp; Medicinal Chemistry Letters, 2017. 27(21): p. 4925-4931. PMID[28958624]. PMCID[PMC5647257].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29090302">Carbosilane Dendrons with Fatty acids at the Core as a New Potential Microbicide against HSV-2/HIV-1 Co-infection.</a>Guerrero-Beltran, C., R. Cena-Diez, D. Sepulveda-Crespo, J. De la Mata, R. Gomez, M. Leal, M.A. Munoz-Fernandez, and J.L. Jimenez. Nanoscale, 2017. 9(44): p. 17263-17273. PMID[29090302].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29180782">Alpha-helicoidal HEAT-like Repeat Proteins (&#945;Rep) Selected as Interactors of HIV-1 Nucleocapsid Negatively Interfere with Viral Genome Packaging and Virus Maturation.</a> Hadpech, S., S. Nangola, K. Chupradit, K. Fanhchaksai, W. Furnon, A. Urvoas, M. Valerio-Lepiniec, P. Minard, P. Boulanger, S.S. Hong, and C. Tayapiwatana. Scientific Reports, 2017. 7(16335): 19pp. PMID[29180782]. PMCID[PMC5703948].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28923862">Antiviral Activity of Bictegravir and Cabotegravir against Integrase Inhibitor-resistant SIVmac239 and HIV-1.</a>Hassounah, S.A., A. Alikhani, M. Oliveira, S. Bharaj, R.I. Ibanescu, N. Osman, H.T. Xu, B.G. Brenner, T. Mesplede, and M.A. Wainberg. Antimicrobial Agents and Chemotherapy, 2017. 61(12): e01695-01617. PMID[28923862]. PMCID[PMC5700299].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29142313">Effects of Exosome on the Activation of CD4+ T Cells in Rhesus Macaques: A Potential Application for HIV Latency Reactivation.</a> Hong, X.W., B. Schouest, and H.B. Xu. Scientific Reports, 2017. 7(15611): 9pp. PMID[29142313]. PMCID[PMC5688118].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29152052">Discovery of Thiophene[3,2-d]pyrimidine Derivatives as Potent HIV-1 NNRTIs Targeting the Tolerant Region I of NNIBP.</a> Kang, D.W., X. Ding, G.C. Wu, Z.P. Huo, Z.X. Zhou, T. Zhao, D. Feng, Z. Wang, Y. Tian, D. Daelemans, C.E. De, C. Pannecouque, P. Zhan, and X.Y. Liu. ACSMedicinal Chemistry Letters, 2017. 8(11): p. 1188-1193. PMID[29152052]. PMCID[PMC5683699].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29113139">Anti-HIV Activity of Ocimum labiatum Extract and Isolated Pheophytin-A.</a>Kapewangolo, P., M. Kandawa-Schulz, and D. Meyer. Molecules, 2017. 22(11): E1763. PMID[29113139].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29093555">Exosomes Derived from HIV-1 Infected DCs Mediate Viral Trans-infection via Fibronectin and Galectin-3.</a> Kulkarni, R. and A. Prasad. Scientific Reports, 2017. 7(14787): 14pp. PMID[29093555]. PMCID[PMC5665889].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28987601">Design, Synthesis and anti-HIV Evaluation of Novel Diarylpyridine Derivatives as Potent HIV-1 NNRTIs.</a> Liu, Z., Y. Tian, J. Liu, B. Huang, D. Kang, E. De Clercq, D. Daelemans, C. Pannecouque, P. Zhan, and X. Liu. European Journal of Medicinal Chemistry, 2017. 140: p. 383-391. PMID[28987601].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414227700151">Emodin Inhibits HIV-1 Infection of Macrophages through the JAK-STAT Pathway.</a> Luo, F., X.K. Wang, H. Wang, J.C. Shi, W.J. Zheng, H.R. Xiong, Y. Feng, X. Wang, W.Z. Ho, and W. Hou. Journal of Neuroimmune Pharmacology, 2017. 12: p. S63-S63. ISI[000414227700151].
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414227700157">SDG Mediated HO-1 Induction and Partial Inhibition of Viral Replication in HIV-infected Human Macrophages.</a>Nieves-Rosado, H., K.S. Williams, S. Pu, and K. Jordan-Sciutto. Journal of Neuroimmune Pharmacology, 2017. 12: p. S65-S66. ISI[000414227700157].
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29212213">The Bromodomain and Extraterminal Domain Inhibitor Bromosporine Synergistically Reactivates Latent HIV-1 in Latently Infected Cells.</a> Pan, H.Y., P.P. Lu, Y.Z. Shen, Y.N. Wang, Z.T. Jiang, X.Y. Yang, Y.C. Zhong, H. Yang, I.U. Khan, M.Y. Zhou, B.K. Li, Z.Y. Zhang, J.Q. Xu, H.Z. Lu, and H.Z. Zhu. Oncotarget, 2017. 8(55): p. 94104-94116. PMID[29212213]. PMCID[PMC5706859].
    <br />
    <b>[</b><b>PubMed</b><b>]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29091918">Software-aided Approach to Investigate Peptide Structure and Metabolic Susceptibility of Amide Bonds in Peptide Drugs Based on High Resolution Mass Spectrometry.</a>Radchenko, T., A. Brink, Y. Siegrist, C. Kochansky, A. Bateman, F. Fontaine, L. Morettoni, and I. Zamora. Plos One, 2017. 12(11): e0186461. PMID[29091918]. PMCID[PMC5665424].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000413763600003">Design, Synthesis, and Docking Studies of New 2-Benzoxazolinone Derivatives as anti-HIV-1 Agents.</a>Safakish, M., Z. Hajimahdi, R. Zabihollahi, M.R. Aghasadeghi, R. Vahabpour, and A. Zarghi. Medicinal Chemistry Research, 2017. 26(11): p. 2718-2726. ISI[000413763600003]. 
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/29058549">Immunomodulatory Activities of Curcumin-stabilized Silver Nanoparticles: Efficacy as an Antiretroviral Therapeutic.</a> Sharma, R.K., K. Cwiklinski, R. Aalinkeel, J.L. Reynolds, D.E. Sykes, E. Quaye, J. Oh, S.D. Mahajan, and S.A. Schwartz. Immunological Investigations, 2017. 46(8): p. 833-846. PMID[29058549].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/28931685">p21 Restricts HIV-1 in Monocyte-derived Dendritic Cells through the Reduction of Deoxynucleoside Triphosphate Biosynthesis and Regulation of SAMHD1 Antiviral Activity.</a> Valle-Casuso, J.C., A. Allouch, A. David, G.M. Lenzi, L. Studdard, F. Barre-Sinoussi, M. Muller-Trutwin, B. Kim, G. Pancino, and A. Saez-Cirion. Journal of Virology, 2017. 91(23): e01324-01317. PMID[28931685]. PMCID[PMC5686764].
    <br />
    <b>[PubMed]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414227700105">Human Cervical Epithelial Cells Inhibit HIV Replication in Macrophages.</a> Xu, X.Q., L. Guo, J.L. Li, X. Wang, J.B. Liu, K. Zhuang, J. Gu, H. Liu, W.Z. Ho, and L. Zhou. Journal of Neuroimmune Pharmacology, 2017. 12: p. S46-S46. ISI[000414227700105]. 
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414227700107">Small Molecule ONC201/TIC10 Inhibits HIV-1 Replication in Reservoir Cell Types in Vitro and in Mouse Brains Xenotransplanted with Human Macrophages.</a> Zhao, R.Z., B.W. Wu, J.C.Z. Zheng, and Y.H. Huang. Journal of Neuroimmune Pharmacology, 2017. 12: p. S47-S47. ISI[000414227700107].
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p><br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000414227700108">Soybean Lectin Inhibits HIV Infection of Macrophages.</a> Zhou, R.H., X. Wang, T.C. Ma, H. Wang, W.Z. Ho, and J.L. Li. Journal of Neuroimmune Pharmacology, 2017. 12: p. S47-S47. ISI[000414227700108]. 
    <br />
    <b>[WOS]</b>. HIV_11_2017.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">26. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171123&amp;CC=WO&amp;NR=2017201187A1&amp;KC=A1">Preparation of 7H-Pyrrolo[2,3-d]pyrimidine Derivatives as Lim Kinase Inhibitors.</a> Feng, Y. and Y. Wu. Patent. 2017. 2017-US33144 2017201187: 122pp.
    <br />
    <b>[Patent]</b>. HIV_11_2017.</p>

    <br />

    <p class="plaintext">27. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171116&amp;CC=WO&amp;NR=2017195113A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, M. Patel, M.A. Walker, T. Wang, Z. Yin, Z. Zhang, and Z.B. Zheng. Patent. 2017. 2017-IB52701 2017195113: 117pp.
    <br />
    <b>[Patent]</b>. HIV_11_2017.</p>

    <br />

    <p class="plaintext">28. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171116&amp;CC=WO&amp;NR=2017195112A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, T. Wang, Z. Yin, and Z. Zhang. Patent. 2017. 2017-IB52700 2017195112: 95pp.
    <br />
    <b>[Patent]</b>. HIV_11_2017.</p>

    <br />

    <p class="plaintext">29. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171116&amp;CC=WO&amp;NR=2017195111A1&amp;KC=A1">Preparation of Pyridin-3-yl acetic acid Derivatives as Inhibitors of Human Immunodeficiency Virus Replication.</a> Kadow, J.F., B.N. Naidu, T. Wang, and Z. Zhang. Patent. 2017. 2017-IB52699 2017195111: 78pp.
    <br />
    <b>[Patent]</b>. HIV_11_2017.</p>

    <br />

    <p class="plaintext">30. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20171124&amp;CC=CN&amp;NR=107383048A&amp;KC=A">Artemisinin Derivative, Synthesis Thereof and Application [Machine Translation].</a> Li, J., D. Ba, and Z. Yang. Patent. 2017. 2017-10687057 107383048: 21pp.
    <br />
    <b>[Patent]</b>. HIV_11_2017.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
