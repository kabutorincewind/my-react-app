

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2013-10-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Ggc8SESoWewGipaaBkdgsDhFZEPwe1Y0KoglbUop2Z0YIyQLo4k/IOdl+exuAZO37ZbPigg6IAkKuP1hY+FgKGL32h/279I4Isps6v8Mh/wZn7kZTuZgrONGIVw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B6AF98DF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: September 27 - October 24, 2013</h1>

    <h2>Hepatitis A Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23906424">Comparison of the Virucidal Efficiency of Peracetic acid, Potassium monopersulfate and Sodium hypochlorite on Hepatitis A and Enteric Cytopathogenic Bovine Orphan Virus<i>.</i></a> Martin, H., C. Soumet, R. Fresnel, T. Morin, S. Lamaudiere, A.L. Le Sauvage, K. Deleurme, and P. Maris. Journal of Applied Microbiology, 2013. 115(4): p. 955-968; PMID[23906424].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24145793">Biological Activity of Oleanane Triterpene Derivatives Obtained by Chemical Derivatization<i>.</i></a> Cheng, S.Y., C.M. Wang, H.L. Cheng, H.J. Chen, Y.M. Hsu, Y.C. Lin, and C.H. Chou. Molecules, 2013. 18(10): p. 13003-13019; PMID[24145793].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099962">5&#39;-Triphosphate-siRNA Activates RIG-I-dependent Type I Interferon Production and Enhances Inhibition of Hepatitis B Virus Replication in HepG2.2.15 Cells<i>.</i></a> Chen, X., Y. Qian, F. Yan, J. Tu, X. Yang, Y. Xing, and Z. Chen. European Journal of Pharmacology, 2013. <b>[Epub ahead of print]</b>; PMID[24099962].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24099774">Specific Inhibition of Hepatitis C Virus Entry into Host Hepatocytes by Fungi-derived Sulochrin and Its Derivatives<i>.</i></a> Nakajima, S., K. Watashi, S. Kamisuki, S. Tsukuda, K. Takemoto, M. Matsuda, R. Suzuki, H. Aizaki, F. Sugawara, and T. Wakita. Biochemical and Biophysical Research Communications, 2013. <b>[Epub ahead of print]</b>; PMID[24099774].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23743442">Inhibition of Hepatitis B Virus Replication in Cultured Cells and in Vivo Using 2&#39;-O-Guanidinopropyl Modified SiRNAs<i>.</i></a> Marimani, M.D., A. Ely, M.C. Buff, S. Bernhardt, J.W. Engels, and P. Arbuthnot. Bioorganic &amp; Medicinal Chemistry, 2013. 21(20): p. 6145-6155; PMID[23743442].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24144360">Discovery and Early Development of TMC647055, a Non-nucleoside Inhibitor of the Hepatitis C Virus NS5B Polymerase<i>.</i></a> Cummings, M.D., T.I. Lin, L. Hu, A. Tahri, D. McGowan, K. Amssoms, S. Last, B. Devogelaere, M.C. Rouan, L. Vijgen, J.M. Berke, P. Dehertogh, E. Fransen, E. Cleiren, L. van der Helm, G. Fanning, O. Nyanguile, K. Simmen, P. Van Remoortere, P. Raboisson, and S. Vendeville. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24144360].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24144213">The Discovery of GS-9669, a Thumb Site II Non-nucleoside Inhibitor of NS5B for the Treatment of Genotype 1 Chronic Hepatitis C Infection<i>.</i></a> Lazerwith, S.E., W. Lew, J. Zhang, P. Morganelli, Q. Liu, E. Canales, M.O. Clarke, E. Doerffler, D. Byun, M. Mertzman, H. Ye, L. Chong, L. Xu, T. Appleby, X. Chen, M. Fenaux, A. Hashash, S. Leavitt, E. Maybery, M. Matles, J.W. Mwangi, Y. Tian, Y.J. Lee, J. Zhang, C. Zhu, B.P. Murray, and W.J. Watkins. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24144213].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24138284">Hepatitis C Virus Translation Inhibitors Targeting the Internal Ribosomal Entry Site<i>.</i></a> Dibrov, S.M., J. Parsons, M. Carnevali, S. Zhou, K.D. Rynearson, K. Ding, E. Garcia Sega, N.D. Brunn, M.A. Boerneke, M.P. Castaldi, and T. Hermann. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24138284].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24135727">Azetidines and Spiro Azetidines as Novel P2 Units in Hepatitis C Virus NS3 Protease Inhibitors<i>.</i></a> Bondada, L., R. Rondla, U. Pradere, P. Liu, C. Li, D. Bobeck, T. McBrayer, P. Tharnish, J. Courcambeck, P. Halfon, T. Whitaker, F. Amblard, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. <b>[Epub ahead of print]</b>; PMID[24135727].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24131104">The Versatile Nature of the 6-Aminoquinolone Scaffold: Identification of Submicromolar Hepatitis C Virus NS5B Inhibitors<i>.</i></a> Manfroni, G., R. Cannalire, M.L. Barreca, N. Kaushik-Basu, P. Leyssen, J. Winquist, N. Iraci, D. Manvar, J. Paeshuyse, R. Guhamazumder, A. Basu, S. Sabatini, O. Tabarrini, U.H. Danielson, J. Neyts, and V. Cecchetti. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24131104].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24130685">High-Throughput Screening (HTS) and Hit Validation to Identify Small Molecule Inhibitors with Activity against NS3/4A Proteases from Multiple Hepatitis C Virus Genotypes<i>.</i></a> Lee, H., T. Zhu, K. Patel, Y.Y. Zhang, L. Truong, K.E. Hevener, J.L. Gatuz, G. Subramanya, H.Y. Jeong, S.L. Uprichard, and M.E. Johnson. Plos One, 2013. 8(10): p. e75144; PMID[24130685].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24127258">Discovery of MK-8742: An HCV NS5A Inhibitor with Broad Genotype Activity<i>.</i></a> Coburn, C.A., P.T. Meinke, W. Chang, C.M. Fandozzi, D.J. Graham, B. Hu, Q. Huang, S. Kargman, J. Kozlowski, R. Liu, J.A. McCauley, A.A. Nomeir, R.M. Soll, J.P. Vacca, D. Wang, H. Wu, B. Zhong, D.B. Olsen, and S.W. Ludmerer. ChemMedChem, 2013. <b>[Epub ahead of print]</b>; PMID[24127258].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24126581">Preclinical Characterization of GSK2336805, a Novel Inhibitor of Hepatitis C Virus Replication That Selects for Resistance in NS5A<i>.</i></a> Walker, J., R. Crosby, A. Wang, E. Woldu, J. Vamathevan, C. Voitenleitner, S. You, K. Remlinger, M. Duan, W. Kazmierski, and R. Hamatake. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>; PMID[24126581].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24092864">Curcumin against Hepatitis C Virus Infection: Spicing up Antiviral Therapies with &#39;Nutraceuticals&#39;?</a> Pecheur, E.I. Gut, 2013. <b>[Epub ahead of print]</b>; PMID[24092864].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24079820">Use of 2&#39;-Spirocyclic Ethers in HCV Nucleoside Design<i>.</i></a> Du, J., B.K. Chun, R.T. Mosley, S. Bansal, H. Bao, C. Espiritu, A.M. Lam, E. Murakami, C. Niu, H.M. Micolochick Steuer, P.A. Furman, and M.J. Sofia. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24079820].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24069953">Discovery of a Novel Series of Potent Non-nucleoside Inhibitors of Hepatitis C Virus NS5B<i>.</i></a> Schoenfeld, R.C., D.L. Bourdet, K.A. Brameld, E. Chin, J. de Vicente, A. Fung, S.F. Harris, E.K. Lee, S. Le Pogam, V. Leveque, J. Li, A.S. Lui, I. Najera, S. Rajyaguru, M. Sangi, S. Steiner, F.X. Talamas, J.P. Taygerly, and J. Zhao. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24069953].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24024973">Enantiomeric Atropisomers Inhibit HCV Polymerase and/or HIV Matrix: Characterizing Hindered Bond Rotations and Target Selectivity<i>.</i></a> Laplante, S.R., P. Forgione, C. Boucher, R. Coulombe, J. Gillard, O. Hucke, A. Jakalian, M.A. Joly, G. Kukolj, C. Lemke, R. McCollum, S. Titolo, P.L. Beaulieu, and T. Stammers. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24024973].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23708123">A Fluorescence-based High-Throughput Screen to Identify Small Compound Inhibitors of the Genotype 3a Hepatitis C Virus RNA Polymerase<i>.</i></a> Eltahla, A.A., K. Lackovic, C. Marquis, J.S. Eden, and P.A. White. Journal of Biomolecular Screening, 2013. 18(9): p. 1027-1034; PMID[23708123].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23602522">Screening for Inhibitors of the Hepatitis C Virus Internal Ribosome Entry Site RNA<i>.</i></a> Zhou, S., K.D. Rynearson, K. Ding, N.D. Brunn, and T. Hermann. Bioorganic &amp; Medicinal Chemistry, 2013. 21(20): p. 6139-44; PMID[23602522].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24038507">Synthesis of Some New Benzisothiazolone and Benzenesulfonamide Derivatives of Biological Interest Starting from Saccharin sodium<i>.</i></a> El-Sabbagh, O.I. Archiv der Pharmazie, 2013. 346(10): p. 733-742; PMID[24038507].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23885069">Modulation of the Cellular Distribution of Human Cytomegalovirus Helicase by Cellular Factor Snapin<i>.</i></a> Luo, J., J. Chen, E. Yang, A. Shen, H. Gong, Z. Pei, G. Xiao, S. Lu, and F. Liu. Journal of Virology, 2013. 87(19): p. 10628-10640; PMID[23885069].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24118020">Antibacterial and Antiviral Evaluation of Sulfonoquinovosyldiacylglyceride (SQDG): A Glycolipid Isolated from Azadirachta indica Leaves<i>.</i></a> Bharitkar, Y.P., S. Bathini, D. Ojha, S. Ghosh, H. Mukherjee, K. Kuotsu, D. Chattopadhyay, and N.B. Mondal. Letters in Applied Microbiology, 2013. <b>[Epub ahead of print]</b>; PMID[24118020].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24095857">The Antiviral Activities of ISG15<i>.</i></a> Morales, D.J. and D.J. Lenschow. Journal of Molecular Biology, 2013. <b>[Epub ahead of print]</b>; PMID[24095857].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23999044">Sulfonyl-polyol N,N-dichloroamines with Rapid, Broad-spectrum Antimicrobial Activity<i>.</i></a> Shiau, T.P., E. Low, B. Kim, E.D. Turtle, C. Francavilla, D.J. O&#39;Mahony, L. Friedman, L. D&#39;Lima, A. Jekle, D. Debabov, M. Zuck, N.J. Alvarez, M. Anderson, R.R. Najafi, and R.K. Jain. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(20): p. 5650-5653; PMID[23999044].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Herpes Simplex Virus 2</h2>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24131916">In Vitro anti-Herpes Simplex Virus Activity of Crude Extract of the Roots of Nauclea latifolia Smith (Rubiaceae)<i>.</i></a> Donalisio, M., H.M. Nana, R.A. Ngono Ngane, D. Gatsing, A. Tiabou Tchinda, R. Rovito, V. Cagno, C. Cagliero, F.F. Boyom, P. Rubiolo, C. Bicchi, and D. Lembo. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 266; PMID[24131916].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24079703">Analysis of Compounds That Interfere with Herpes Simplex Virus-host Receptor Interactions Using Surface Plasmon Resonance<i>.</i></a> Gopinath, S.C., K. Hayashi, J.B. Lee, A. Kamori, C.X. Dong, T. Hayashi, and P.K. Kumar. Analytical Chemistry, 2013. <b>[Epub ahead of print]</b>; PMID[24079703].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <p class="memofmt2-1">Kaposi</p>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23580777">O-GlcNAc Transferase Inhibits Kshv Propagation and Modifies Replication Relevant Viral Proteins as Detected by Systematic O-GlcNAcylation Analysis<i>.</i></a> Jochmann, R., J. Pfannstiel, P. Chudasama, E. Kuhn, A. Konrad, and M. Sturzl. Glycobiology, 2013. 23(10): p. 1114-1130; PMID[23580777].</p>

    <p class="plaintext"><b>[PubMed]</b> OV_1011-102413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400073">Design, Synthesis, and Biological Evaluation of New 2 &#39;-Deoxy-2 &#39;-fluoro-4 &#39;-triazole Cytidine Nucleosides as Potent Antiviral Agents<i>.</i></a> Wu, J., W.Q. Yu, L.X. Fu, W. He, Y. Wang, B.S. Chai, C.J. Song, and J.B. Chang. European Journal of Medicinal Chemistry, 2013. 63: p. 739-745; ISI[000322855400073].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324005200041">Perovskatone A: A Novel C-23 Terpenoid from Perovskia Atriplicifolia<i>.</i></a> Jiang, Z.Y., C.G. Huang, H.B. Xiong, K. Tian, W.X. Liu, Q.F. Hu, H.B. Wang, G.Y. Yang, and X.Z. Huang. Tetrahedron Letters, 2013. 54(29): p. 3886-3888; ISI[000324005200041].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324474600032">A Serine Palmitoyltransferase Inhibitor Blocks Hepatitis C Virus Replication in Human Hepatocytes<i>.</i></a> Katsume, A., Y. Tokunaga, Y. Hirata, T. Munakata, M. Saito, H. Hayashi, K. Okamoto, Y. Ohmori, I. Kusanagi, S. Fujiwara, T. Tsukuda, Y. Aoki, K. Klumpp, K. Tsukiyama-Kohara, A. El-Gohary, M. Sudoh, and M. Kohara. Gastroenterology, 2013. 145(4): p. 865-873; ISI[000324474600032].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324403200032">Interferon and Ribavirin Combination Treatment Synergistically Inhibit HCV Internal Ribosome Entry Site Mediated Translation at the Level of Polyribosome Formation<i>.</i></a> Panigrahi, R., S. Hazari, S. Chandra, P.K. Chandra, S. Datta, R. Kurt, C.E. Cameron, Z.H. Huang, H.T. Zhang, R.F. Garry, L.A. Balart, and S. Dash. Plos One, 2013. 8(8): p. e72791; ISI[000324403200032].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000324547300089">Inhibition of HCV Replication by Oxysterol-binding Protein-related Protein 4 (Orp4) through Interaction with HCV NS5B and Alteration of Lipid Droplet Formation<i>.</i></a> Park, I.W., J. Ndjomou, Y.H. Wen, Z.Q. Liu, N.D. Ridgway, C.C. Kao, and J.J. He. Plos One, 2013. 8(9) : p.  e75648; ISI[000324547300089] .</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400085">Ester Prodrugs of Acyclic Nucleoside Thiophosphonates Compared to Phosphonates: Synthesis, Antiviral Activity and Decomposition Study<i>.</i></a> Roux, L., S. Priet, N. Payrot, C. Weck, M. Fournier, F. Zoulim, J. Balzarini, B. Canard, and K. Alvarez. European Journal of Medicinal Chemistry, 2013. 63: p. 869-881; ISI[000322855400085].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p>

    <br />

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322855400029">Coumarins Hinged Directly on Benzimidazoles and Their Ribofuranosides to Inhibit Hepatitis C Virus<i>.</i></a> Tsay, S.C., J.R. Hwu, R. Singha, W.C. Huang, Y.H. Chang, M.H. Hsu, F.K. Shieh, C.C. Lin, K.C. Hwang, J.C. Horng, E. De Clercq, I. Vliegen, and J. Neyts. European Journal of Medicinal Chemistry, 2013. 63: p. 290-298; ISI[000322855400029].</p>

    <p class="plaintext"><b>[WOS]</b> OV_1011-102413.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
