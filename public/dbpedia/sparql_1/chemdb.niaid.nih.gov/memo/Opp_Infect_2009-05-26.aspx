

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-05-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="OJR0EQiR3fY4ocZDYKh9FDSt1t4OOjuDnWjWKXFcycm3imFoEbXYcQ81L2+ONKZ5UHp4zQlBekP9W+VpFeIsvttKwWtiUuo95b/AFBRqZOe0CSJLADCXDKp7fcs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F25F3D3A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">May 13, 2009 - May 26, 2009</p>

    <h2>TB (Tuberculosis, Mycobacterium, Avium, Drug Resistant, MAC, MDR)</h2>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19471882?ordinalpos=15&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A new acyclic diterpene acid and bioactive compounds from Knema glauca.</a></p>

    <p class="NoSpacing">Rangkaew N, Suttisri R, Moriyasu M, Kawanishi K.</p>

    <p class="NoSpacing">Arch Pharm Res. 2009 May;32(5):685-92. Epub 2009 May 27.</p>

    <p class="NoSpacing">PMID: 19471882 [PubMed - in process]</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19454523?ordinalpos=77&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Comparative in vitro activities of the new quinolone nemonoxacin (TG-873870), gemifloxacin and other quinolones against clinical isolates of Mycobacterium tuberculosis.</a></p>

    <p class="NoSpacing">Tan CK, Lai CC, Liao CH, Chou CH, Hsu HL, Huang YT, Hsueh PR.</p>

    <p class="NoSpacing">J Antimicrob Chemother. 2009 May 20. [Epub ahead of print] No abstract available.</p>

    <p class="NoSpacing">PMID: 19454523 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19440303?ordinalpos=109&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Identification of 2-aminothiazole-4-carboxylate derivatives active against Mycobacterium tuberculosis H37Rv and the beta-ketoacyl-ACP synthase mtFabH.</a></p>

    <p class="NoSpacing">Al-Balas Q, Anthony NG, Al-Jaidi B, Alnimr A, Abbott G, Brown AK, Taylor RC, Besra GS, McHugh TD, Gillespie SH, Johnston BF, Mackay SP, Coxon GD.</p>

    <p class="NoSpacing">PLoS ONE. 2009;4(5):e5617. Epub 2009 May 19.</p>

    <p class="NoSpacing">PMID: 19440303 [PubMed - in process]</p>

    <p class="ListParagraph">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19403314?ordinalpos=121&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">New antituberculotics originated from salicylanilides with promising in vitro activity against atypical mycobacterial strains.</a></p>

    <p class="NoSpacing">Imramovský A, Vinsová J, Férriz JM, Dolezal R, Jampílek J, Kaustová J, Kunc F.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 May 15;17(10):3572-9. Epub 2009 Apr 11.</p>

    <p class="NoSpacing">PMID: 19403314 [PubMed - in process]</p>

    <p class="ListParagraph">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19386501?ordinalpos=123&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Discovery, synthesis, and biological evaluation of piperidinol analogs with anti-tuberculosis activity.</a></p>

    <p class="NoSpacing">Sun D, Scherman MS, Jones V, Hurdle JG, Woolhiser LK, Knudson SE, Lenaerts AJ, Slayden RA, McNeil MR, Lee RE.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2009 May 15;17(10):3588-94. Epub 2009 Apr 9.</p>

    <p class="NoSpacing">PMID: 19386501 [PubMed - in process]</p>

    <p class="ListParagraph">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19200841?ordinalpos=142&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Novel prophylactic and therapeutic vaccine against tuberculosis.</a></p>

    <p class="NoSpacing">Okada M, Kita Y, Nakajima T, Kanamaru N, Hashimoto S, Nagasawa T, Kaneda Y, Yoshida S, Nishida Y, Nakatani H, Takao K, Kishigami C, Inoue Y, Matsumoto M, McMurray DN, Dela Cruz EC, Tan EV, Abalos RM, Burgos JA, Saunderson P, Sakatani M.</p>

    <p class="NoSpacing">Vaccine. 2009 May 26;27(25-26):3267-70. Epub 2009 Feb 5.</p>

    <p class="NoSpacing">PMID: 19200841 [PubMed - in process]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
