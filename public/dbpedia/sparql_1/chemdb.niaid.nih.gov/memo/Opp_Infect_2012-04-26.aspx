

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-04-26.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WdrXAyU1CfsYYloSt/Wub+hNVQ90WJAlLbJHlvtaNl198iobTFaDDK7wgpIxOWKxKwOLVNuOjc5719k5TFscI5MzXjsJpSXo+0Vhno5ssoohbAdmGWMOw6l2MJY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2504ADE5" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  April 13 - April 26, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22528740">Effects of Antifungal Agents Alone and in Combination against Candida glabrata Strains Susceptible or Resistant to Fluconazole.</a> Alves, I.A., L.A. Bandeira, D.A. Mario, L.B. Denardi, L.V. Neves, J.M. Santurio, and S.H. Alves. Mycopathologia, 2012. <b>[Epub ahead of print]</b>; PMID[22528740].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22401914">Semisynthesis and Antimicrobial Activity of Novel Guttiferone-A Derivatives.</a> Dias, K.S., J.P. Januario, J.L. Dego, A.L. Dias, M.H. Dos Santos, I. Camps, L.F. Coelho, and C. Viegas, Jr. Bioorganic &amp; Medicinal Chemistry, 2012. 20(8): p. 2713-2720; PMID[22401914].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22521700">The Antifungal Properties of Chlorhexidine digluconate and Cetylpyrinidinium chloride on Oral Candida.</a> Fathilah, A.R., W.H. Himratul-Aznita, A.R. Fatheen, and K.R. Suriani. Journal of Dentistry, 2012. <b>[Epub ahead of print]</b>; PMID[22521700].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22528741">Antifungal Activities of Different Extracts of Marine Macroalgae against Dermatophytes and Candida Species.</a> Guedes, E.A., M.A. Dos Santos Araujo, A.K. Souza, L.I. de Souza, L.D. de Barros, F.C. de Albuquerque Maranhao, and A.E. Sant&#39;ana. Mycopathologia, 2012. <b>[Epub ahead of print]</b>; PMID[22528741].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22528077">Antimicrobial Activities of Recombinant Mouse beta-Defensin 3 and Its Synergy with Antibiotics.</a> Jiang, Y., X. Yi, M. Li, T. Wang, T. Qi, and X. She. Journal of Materials Science. Materials in Medicine, 2012; PMID[22528077].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22500738">Structure-Activity Relationship of Citrus Polymethoxylated Flavones and Their Inhibitory Effects on Aspergillus niger.</a> Liu, L., X. Xu, D. Cheng, X. Yao, and S. Pan. Journal of Agricultural and Food Chemistry, 2012. <b>[Epub ahead of print]</b>; PMID[22500738].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22524636">Phthalide and Isocoumarin Derivatives Produced by an Acremonium Sp. Isolated from a Mangrove Rhizophora apiculata.</a> Rukachaisirikul, V., A. Rodglin, Y. Sukpondma, S. Phongpaichit, J. Buatong, and J. Sakayaroj. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>; PMID[22524636].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22514262">In Vitro Activity of Phenylmercuric acetate against Ocular Pathogenic Fungi.</a> Xu, Y., D. Zhao, C. Gao, L. Zhou, G. Pang, and S. Sun. The Journal of Antimicrobial Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22514262].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508310">The Antidepressant Sertraline Provides a Promising Therapeutic Option for Neurotropic Cryptococcal Infections.</a> Zhai, B., C. Wu, L. Wang, M.S. Sachs, and X. Lin. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22508310].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p>
    <p> </p>
    
    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22459211">Compounds of Alpinia katsumadai as Potential Efflux Inhibitors in Mycobacterium smegmatis.</a> Groblacher, B., O. Kunert, and F. Bucar. Bioorganic &amp; Medicinal Chemistry, 2012. 20(8): p. 2701-2706; PMID[22459211].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22515497">Design, Synthesis and Antimycobacterial Property of Peg-bis(Inh) Conjugates.</a> Kakkar, D., A.K. Tiwari, K. Chuttani, A. Khanna, D. Upadhya, A. Datta, H. Singh, and A.K. Mishra. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>; PMID[22515497].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22510605">Systematic Evaluation of Structure-Activity Relationships of the Riminophenazine Class and Discovery of a C2 Pyridylamino Series for the Treatment of Multidrug-resistant Tuberculosis.</a> Liu, B., K. Liu, Y. Lu, D. Zhang, T. Yang, X. Li, C. Ma, M. Zheng, B. Wang, G. Zhang, F. Wang, Z. Ma, C. Li, H. Huang, and D. Yin. Molecules (Basel, Switzerland), 2012. 17(4): p. 4545-4559; PMID[22510605]. <b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22440626">Synthesis of Highly Potent Novel Anti-tubercular Isoniazid Analogues with Preliminary Pharmacokinetic Evaluation.</a> Ramani, A.V., A. Monika, V.L. Indira, G. Karyavardhi, J. Venkatesh, V.U. Jeankumar, T.H. Manjashetty, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2764-2767; PMID[22440626].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22418278">Antimycobacterial Activity of Bisbenzylisoquinoline Alkaloids from Tiliacora triandra against Multidrug-resistant Isolates of Mycobacterium tuberculosis.</a> Sureram, S., S.P. Senadeera, P. Hongmanee, C. Mahidol, S. Ruchirawat, and P. Kittakoop. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2902-2905; PMID[22418278].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22437116">Synthesis and in Vitro Anticancer and Antitubercular Activity of Diarylpyrazole Ligated Dihydropyrimidines Possessing Lipophilic Carbamoyl Group.</a> Yadlapalli, R.K., O.P. Chourasia, K. Vemuri, M. Sritharan, and R.S. Perali. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2708-2711; PMID[22437116].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p>
    <p> </p>
    
    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22414614">Phosphonium Lipocations as Antiparasitic Agents.</a> Long, T.E., X. Lu, M. Galizzi, R. Docampo, J. Gut, and P.J. Rosenthal. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2976-2979; PMID[22414614].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22524250">Imidazolopiperazines: Lead Optimization of the Second-generation Antimalarial Agents.</a> Nagle, A., T. Wu, K. Kuhen, K. Gagaring, R. Borboa, C. Francek, Z. Chen, D. Plouffe, X. Lin, C. Caldwell, J. Ek, S. Skolnik, F. Liu, J. Wang, J. Chang, C. Li, B. Liu, T. Hollenbeck, T. Tuntland, J. Isbell, T. Chuan, P.B. Alper, C. Fischli, R. Brun, S.B. Lakshminarayana, M. Rottmann, T.T. Diagana, E.A. Winzeler, R. Glynne, D.C. Tully, and A.K. Chatterjee. Journal of Medicinal Chemistry, 2012.<b>[Epub ahead of print]</b>; PMID[22524250].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508305">Sontochin as a Guide to Development of Drugs against Chloroquine Resistant Malaria.</a> Pou, S., R.W. Winter, A. Nilsen, J.X. Kelly, Y. Li, J.S. Doggett, E.W. Riscoe, K.W. Wegmann, D.J. Hinrichs, and M.K. Riscoe. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22508305].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508312">Anti-malarial Activity of the Anti-cancer HDAC Inhibitor SB939.</a> Sumanadasa, S.D., C.D. Goodman, A.J. Lucke, T. Skinner-Adams, I. Sahama, A. Haque, T.A. Do, G.I. McFadden, D.P. Fairlie, and K.T. Andrews. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22508312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p>
    <p> </p>
    
    <h2>ANTILEISHMANIAL COMPOUNDS (Leishmania spp.)</h2>

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22508306">Evaluation of Arylimidamides DB1955 and DB1960 as Candidates against Visceral Leishmaniasis and Chagas Disease - In Vivo Efficacy, Acute Toxicity, Pharmacokinetics and Toxicology Studies.</a> Zhu, X., Q. Liu, S. Yang, T. Parman, C.E. Green, J.C. Mirsalis, M.D. Soeiro, E.M. Souza, C.F. Silva, D.D. Batista, C.E. Stephens, M. Banerjee, A.A. Farahat, M. Munde, W.D. Wilson, D.W. Boykin, M.Z. Wang, and K.A. Werbovetz. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>; PMID[22508306].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p>
    <p> </p>
    
    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22425568">Structure-Activity Studies of Some Berberine Analogs as Inhibitors of Toxoplasma gondii.</a> Krivogorsky, B., J.A. Pernat, K.A. Douglas, N.J. Czerniecki, and P. Grundt. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(8): p. 2980-2982; PMID[22425568].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22527273">Comfa/Comsia 3D-QSAR of Pyrimidine Inhibitors of Pneumocystis carinii Dihydrofolate Reductase.</a> Santos-Filho, O.A., D. Forge, L.V. Hoelz, G.B. de Freitas, T.O. Marinho, J.Q. Araujo, M.G. Albuquerque, R.B. de Alencastro, and N. Boechat. Journal of Molecular Modeling, 2012. <b>[Epub ahead of print]</b>; PMID[22527273].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0413-042612.</p>
    <p> </p>
    
    <h2>Citations from the ISI Web of  Knowledge Listings for O.I.</h2>

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301893900005">Synthesis, Characterization and Antimicrobial Screening of Novel Quinoline-thiazole Derivatives.</a> Desai, N.C., N. Shihory, K. Rajpara, and A. Dodiya. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2012. 51(3): p. 508-513; ISI[000301893900005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p>  
    <p> </p>
    
    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301400800010">Chlorinated chromone and Diphenyl ether Derivatives from the Mangrove-derived Fungus Pestalotiopsis sp PSU-MA69.</a> Klaiklay, S., V. Rukachaisirikul, K. Tadpetch, Y. Sukpondma, S. Phongpaichit, J. Buatong, and J. Sakayaroj. Tetrahedron, 2012. 68(10): p. 2299-2305; ISI[000301400800010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301304100012">Antibacterial Activity of Some African Medicinal Plants Used Traditionally against Infectious Diseases.</a> Madureira, A.M., C. Ramalhete, S. Mulhovo, A. Duarte, and M.J.U. Ferreira. Pharmaceutical Biology, 2012. 50(4): p. 481-489; ISI[000301304100012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301522300020">Synthesis and Characterization of Thermally Stable and Biologically Active Metal-based Schiff Base Polymer.</a> Nishat, N., S.A. Khan, R. Rasool, and S. Parveen. Journal of Inorganic and Organometallic Polymers and Materials, 2012. 22(2): p. 455-463; ISI[000301522300020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301998300017">Antimicrobial Activity of Ulopterol Isolated from Toddalia asiatica (L.) Lam.: A Traditional Medicinal Plant.</a> Raj, M.K., C. Balachandran, V. Duraipandiyan, P. Agastian, and S. Ignacimuthu. Journal of Ethnopharmacology, 2012. 140(1): p. 161-165; ISI[000301998300017].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p> 
    <p> </p>
    
    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000301998300020">In Vivo Antioxidative Property, Antimicrobial and Wound Healing Activity of Flower Extracts of Pyrostegia venusta (Ker Gawl) Miers.</a> Roy, P., S. Amdekar, A. Kumar, R. Singh, P. Sharma, and V. Singh. Journal of Ethnopharmacology, 2012. 140(1): p. 186-192; ISI[000301998300020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0413-042612.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
