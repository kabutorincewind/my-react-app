

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-12-18.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="kAXP4QrUCHR1DWXZ04K/J1Vs7VEn7bEyqTNLP13PR8pzkRH7/jWDlKEiHPVg77Lab9ZjjWgwpO8VGuHlwK0I+xCunV9+22a6fhwnOhCoBiV9TF0YkFywRs3jgHU=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7FD7188E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: December 5 - December 18, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23985753">Myristoylated Derivatives of 2&#39;,3&#39;-Didehydro-2&#39;,3&#39;-dideoxythymidine (Stavudine) Bi-functional Prodrugs with Potent anti-HIV-1 Activity and Low Cytotoxicity.</a> Singh, R.K., A. Miazga, A. Dabrowska, A. Lipniacki, A. Piasek, T. Kulikowski, and D. Shugar. Antiviral Chemistry &amp; Chemotherapy, 2014. 23(6): p. 231-235. PMID[23985753]. 
    <br />
    <b>[PubMed]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25418038">Indolylarylsulfones Carrying a Heterocyclic Tail as Very Potent and Broad Spectrum HIV-1 Non-nucleoside Reverse Transcriptase Inhibitors.</a> Famiglini, V., G. La Regina, A. Coluccia, S. Pelliccia, A. Brancale, G. Maga, E. Crespan, R. Badia, E. Riveira-Munoz, J.A. Este, R. Ferretti, R. Cirilli, C. Zamperini, M. Botta, D. Schols, V. Limongelli, B. Agostino, E. Novellino, and R. Silvestri. Journal of Medicinal Chemistry, 2014. 57(23): p. 9945-9957. PMID[25418038]. 
    <br />
    <b>[PubMed]</b>. HIV_1205-121814.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344471800039">Altertoxins with Potent anti-HIV Activity from Alternaria tenuissima QUE1Se, a Fungal Endophyte of Quercus emoryi.</a> Bashyal, B.P., B.P. Wellensiek, R. Ramakrishnan, S.H. Faeth, N. Ahmad, and A.A.L. Gunatilaka. Bioorganic &amp; Medicinal Chemistry, 2014. 22(21): p. 6112-6116. ISI[000344471800039].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345039600001">A Novel Laccase with Potent Antiproliferative and HIV-1 Reverse Transcriptase Inhibitory Activities from Mycelia of Mushroom Coprinus comatus.</a> Zhao, S., C.B. Rong, C. Kong, Y. Liu, F. Xu, Q.J. Miao, S.X. Wang, H.X. Wang, and G.Q. Zhang. Biomed Research International, 2014. ISI[000345039600001]. 
    <br />
    <b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344157500068">Combination of the CCL5-derived Peptide R4.0 with Different HIV-1 Blockers Reveals Wide Target Compatibility and Synergic Cobinding to CCR5.</a> Secchi, M., L. Vassena, S. Morin, D. Schols, and L. Vangelista. Antimicrobial Agents and Chemotherapy, 2014. 58(10): p. 6215-6223. ISI[000344157500068]. 
    <br />
    <b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344833200008">Design, Synthesis and Activity Prescreening of Small Molecule-peptide Conjugates as HIV-1 Fusion Inhibitors Targeting gp41.</a> Liang, G.D., C. Wang, W.G. Shi, K. Wang, X.F. Jiang, X.Y. Xu, and K.L. Liu. Chemical Journal of Chinese Universities, 2014. 35(10): p. 2100-2103. ISI[000344833200008]. 
    <br />
    <b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344483804489">CPI-431-32, a Novel Cyclophilin A Inhibitor, Simultaneously Blocks Replication of HCV and HIV-1 Viruses in a Novel in Vitro Co-infection Model.</a> Gallay, P., M. Bobardt, D. Trepanier, D. Ure, C. Ordonez, and R.T. Foster. Hepatology, 2014. 60: p. 1128A-1128A. ISI[000344483804489]. 
    <br />
    <b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345125500027">Serendipitous Discovery of 2-((Phenylsulfonyl) methyl)-thieno[3,2-d]pyrimidine Derivatives as Novel HIV-1 Replication Inhibitors.</a> Kim, J., J. Kwon, D. Lee, S. Jo, D.S. Park, J. Choi, E. Park, J.Y. Hwang, Y. Ko, I. Choi, M.K. Ju, J. Ahn, J. Kim, S.J. Han, T.H. Kim, J. Cechetto, J. Nam, S. Ahn, P. Sommer, M. Liuzzi, and J. Lee. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(23): p. 5473-5477. ISI[000345125500027].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345287300013">Synthesis and Antiviral Evaluation of 2-Amino-6-carbamoylpurine Dioxolane Nucleoside Derivatives and Their Phosphoramidates Prodrugs.</a> Cho, J.H., L. Bondana, M.A. Detorio, C. Montero, L.C. Bassit, F. Amblard, S.J. Coats, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry, 2014. 22(23): p. 6665-6671. ISI[000345287300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000344977100020">Picomolar Inhibitors of HIV-1 Reverse Transcriptase: Design and Crystallography of Naphthyl phenyl ethers.</a> Lee, W.G., K.M. Frey, R. Gallardo-Macias, K.A. Spasov, M. Bollini, K.S. Anderson, and W.L. Jorgensen. ACS Medicinal Chemistry Letters, 2014. 5(11): p. 1259-1262. ISI[000344977100020].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000345317700005">Synthesis and anti-HIV-1 Activity of Olean-9(11),12(13)-dien-30-oic acid 3[beta]-(2-O-[beta]-D-Glucuronopyranosyl-[beta]-D-glucuronopyranoside).</a> Baltina, L.A., O.V. Stolyarova, R.M. Kondratenko, T.M. Gabbasov, L.A. Baltina, O.A. Plyasunova, and T.V. Il&#39;ina. Pharmaceutical Chemistry Journal, 2014. 48(7): p. 439-443. ISI[000345317700005].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1205-121814.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">12. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20140626&amp;CC=ES&amp;NR=2471670A1&amp;KC=A1">Aptamers that Bind HIV-1 5&#39;UTR, Methods and Compositions for Production and Therapeutic Uses.</a> Berzal Herranz, A., C. Briones Llorente, F.J. Sanchez-Luque, S. Cuevas Manrubia, and M. Stich. Patent. 2014. 2012-31819 2471670: 64pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">13. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141120&amp;CC=WO&amp;NR=2014183532A1&amp;KC=A1">Fused Tricyclic Heterocyclic Compounds as HIV Integrase Inhibitors and Their Preparation and Use for the Treatment of HIV Infection.</a> Coleman, P.J., T.J. Hartingh, I.T. Raheem, J. Schreier, J.T. Sisko, J. Wai, T. Graham, L. Hu, and X. Peng. Patent. 2014. 2014-CN75685 2014183532: 115pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">14. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141117&amp;CC=JP&amp;NR=2014214118A&amp;KC=A">Preparation of Pyrazole Derivatives for the Treatment of HIV Virus Infection.</a> Kato, T., A. Hirai, M. Matsuoka, and K. Shimura. Patent. 2014. 2013-92023 2014214118: 64pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">15. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141120&amp;CC=WO&amp;NR=2014186423A1&amp;KC=A1">Combination Therapy Using Vif Inhibitor and an Apobec3g (A3g) Activator for Treating HIV Infection and AIDS Disease.</a> Smith, H.C., R.P. Bennett, and K. Prohaska. Patent. 2014. 2014-US37935 2014186423: 74pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1205-121814.</p>

    <br />

    <p class="plaintext">16. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141120&amp;CC=WO&amp;NR=2014186398A1&amp;KC=A1">Compounds for Inhibiting Drug-resistant Strains of HIV-1 Integrase.</a> Zhao, X.Z., S. Smith, M.A. Metifiot, B. Johnson, C. Marchand, S. Hughes, Y. Pommier, and T.R. Burke. Patent. 2014. 2014-US37905 2014186398: 107pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1205-121814.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
