

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2010-05-27.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="VjocoHx3+Ndx4gzKogvBZh4FD8rtwOsITMwW287xvmUMFY0GhqkUbAC+Kd6pRnRX9tWzAsyyi2eOQ+8BajNtWhbCPutBTgxax7jUHiZiif+mUwN61RwMNJwr6Yc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CC18FDAE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  May 14 - May 27, 2010</h1>

    <p> </p>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20498311">Actinohivin, a broadly neutralizing prokaryotic lectin, inhibits HIV-1 infection by specifically targeting high-mannose type glycans on the gp120 envelope.</a>  Hoorelbeke B, Huskens D, Férir G, François KO, Takahashi A, Van Laethem K, Schols D, Tanaka H, Balzarini J.  Antimicrob Agents Chemother. 2010 May 24. [Epub ahead of print]PMID: 20498311</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20497098">Microparticulated anti-HIV vaginal gel: In vitro-in vivo drug release and vaginal irritation study.</a>  Chatterjee A, Kumar L, Bhowmik BB, Gupta A.  Pharm Dev Technol. 2010 May 25. [Epub ahead of print]PMID: 20497098</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20495861">A Novel Ribonuclease with Antiproliferative Activity from Fresh Fruiting Bodies of the Edible Mushroom Lyophyllum shimeiji.</a>  Zhang RY, Zhang GQ, Hu DD, Wang HX, Ng TB.  Biochem Genet. 2010 May 21. [Epub ahead of print]PMID: 20495861</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20491589">Inhibition of Human Immunodeficiency Virus Type 1 by Lactic Acid Bacteria from Human Breastmilk.</a>  Martín V, Maldonado A, Fernández L, Rodríguez JM, Connor RI.  Breastfeed Med. 2010 May 22. [Epub ahead of print]PMID: 20491589</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20488700">Anti HIV-1 agents 5: Synthesis and anti-HIV-1 activity of some N-arylsulfonyl-3-acetylindoles in vitro.</a>  Ran JQ, Huang N, Xu H, Yang LM, Lv M, Zheng YT.  Bioorg Med Chem Lett. 2010 May 18. [Epub ahead of print]PMID: 20488700</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20484498">Structural basis for the inhibition of RNaseH activity of HIV-1 Reverse Transcriptase by RNaseH active site-directed inhibitors.</a>  Su HP, Yan Y, Prasad GS, Smith RF, Daniels CL, Abeywickrema PD, Reid JC, Loughran HM, Kornienko M, Sharma S, Grobler JA, Xu B, Sardana V, Allison TJ, Williams PD, Darke PL, Hazuda DJ, Munshi S.   J Virol. 2010 May 19. [Epub ahead of print]PMID: 20484498</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20479208">Engineering of Lactobacillus jensenii to secrete RANTES and a CCR5 antagonist analogue as live HIV-1 blockers.</a>  Vangelista L, Secchi M, Liu X, Bachi A, Jia L, Xu Q, Lusso P.  Antimicrob Agents Chemother. 2010 May 17. [Epub ahead of print]PMID: 20479208</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20473303">Rational design of small-molecule inhibitors of the LEDGF/p75-integrase interaction and HIV replication.</a>  Christ F, Voet A, Marchand A, Nicolet S, Desimmie BA, Marchand D, Bardiot D, Van der Veken NJ, Van Remoortel B, Strelkov SV, De Maeyer M, Chaltin P, Debyser Z.  Nat Chem Biol. 2010 Jun;6(6):442-8. Epub 2010 May 16.PMID: 20473303</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/20462233">Dibenzocyclooctadiene Lignans from Schisandra wilsoniana and Their Anti-HIV-1 Activities.</a>  Yang GY, Li YK, Wang RR, Li XN, Xiao WL, Yang LM, Pu JX, Zheng YT, Sun HD.   J Nat Prod. 2010 May 12. [Epub ahead of print]PMID: 20462233</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20443225">Synthesis and SAR of novel isoquinoline CXCR4 antagonists with potent anti-HIV activity.</a>  Miller JF, Gudmundsson KS, D&#39;Aurora Richardson L, Jenkinson S, Spaltenstein A, Thomson M, Wheelan P.  Bioorg Med Chem Lett. 2010 May 15;20(10):3026-30.PMID: 20443225</p>

    <p class="title">11.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20409721">Discovery of GS-9131: Design, synthesis and optimization of amidate prodrugs of the novel nucleoside phosphonate HIV reverse transcriptase (RT) inhibitor GS-9148.</a>  Mackman RL, Ray AS, Hui HC, Zhang L, Birkus G, Boojamra CG, Desai MC, Douglas JL, Gao Y, Grant D, Laflamme G, Lin KY, Markevitch DY, Mishra R, McDermott M, Pakdaman R, Petrakovsky OV, Vela JE, Cihlar T.  Bioorg Med Chem. 2010 May 15;18(10):3606-17. Epub 2010 Mar 27.PMID: 20409721</p>

    <p class="title">12.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/20405850">A new DNA building block, 4&#39;-selenothymidine: synthesis and modification to 4&#39;-seleno-AZT as a potential anti-HIV agent.</a>  Alexander V, Choi WJ, Chun J, Kim HO, Jeon JH, Tosh DK, Lee HW, Chandra G, Choi J, Jeong LS.  Org Lett. 2010 May 21;12(10):2242-5.PMID: 20405850</p>

    <p> </p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277154200008">Development of an HIV-1 Specific Microbicide Using Caulobacter crescentus S-Layer Mediated Display of CD4 and MIP1 alpha.</a>  Nomellini, JF; Li, C; Lavallee, D; Shanina, I; Cavacini, LA; Horwitz, MS; Smit, J.  PLOS ONE, 2010; 5(4).</p>

    <p>14.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277147400055">p-Aminoacetophenonic Acids Produced by a Mangrove Endophyte Streptomyces sp (strain HK10552).</a>  Wang, FF; Xu, MJ; Li, QS; Sattler, I; Lin, WH.  MOLECULES, 2010; 15(4): 2782-2790.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277156100007">Progress of Bis(heteroaryl)piperazines (BHAPs) as Non-nucleoside Reverse Transcriptase Inhibitors (NNRTIs) against Human Immunodeficiency Virus Type 1 (HIV-1).</a>  Xu, H.  MINI-REVIEWS IN MEDICINAL CHEMISTRY, 2010; 1(62): 62-72.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277144200007">An Antiproliferative Ribonuclease from Fruiting Bodies of the Wild Mushroom Russula delica.</a>  Zhao, S; Zhao, YC; Li, SH; Zhang, GQ; Wang, HX; Ng, TB.  JOURNAL OF MICROBIOLOGY AND BIOTECHNOLOGY, 2010; 20(4): 693-699.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277167900004">A defensin with highly potent antipathogenic activities from the seeds of purple pole bean.</a>  Lin, P; Wong, JH; Ng, TB.  BIOSCIENCE REPORTS, 2010; 30(2): 101-109.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277246300003">Synthesis and SAR of novel isoquinoline CXCR4 antagonists with potent anti-HIV activity.</a>  Miller, JF; Gudmundsson, KS; Richardson, LD; Jenkinson, S; Spaltenstein, A; Thomson, M; Wheelan, P. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(10): 3026-3030.</p>

    <p>19.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277246300021">Novel hexahydropyrrolo[3,4-c]pyrrole CCR5 antagonists.</a>  Rotstein, DM; Melville, CR; Padilla, F; Cournoyer, D; Lee, EK; Lemoine, R; Petersen, AC; Setti, LQ; Wanner, J; Chen, LJ; Filonova, L; Loughhead, DG; Manka, J; Lin, XF; Gleason, S; Sankuratri, S; Ji, CH; deRosier, A; Dioszegi, M; Heilek, G; Jekle, A; Berry, P; Mau, CI; Weller, P. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2010; 20(10): 3116-3119.</p>

    <p>20.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277399200010">Synthesis and biological evaluation of inosine phosphonates.</a>  Abramov, M; Herdewijn, P. NEW JOURNAL OF CHEMISTRY, 2010; 34(5): 875-876.</p>

    <p>21.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000277335200002">First report of an antifungal amidase from Peltophorum ptercoarpum.</a>  Lam, SK; Ng, TB. BIOMEDICAL CHROMATOGRAPHY, 2010; 24(5): 458-464.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
