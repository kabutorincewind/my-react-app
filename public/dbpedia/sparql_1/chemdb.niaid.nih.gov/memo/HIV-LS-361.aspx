

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-361.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="t9VI9hy+hH2W9nmmEeDMIk/8GAJ5VEj8dhV/9M/gBm4+5m6Tw8Sr2Gf8EqIYZuxnLrNYOl6McnwA0qhianqL4cRfsCms79ghd2x5a30lBZLQQfXAV09WYnZNdcc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EAA310E6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-361-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     59375   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Synthesis and anti-HIV activity evaluation of 1-[(alkenyl or alkynyl or alkyloxy)methyl]-5-alkyl-6-(1-naphthoyl)-2,4-pyrimidinediones as novel non-nucleoside HIV-1 reverse transcriptase inhibitors</p>

    <p>          Ji, L, Chen, FE, Xie, B, De, Clercq E, Balzarini, J, and Pannecouque, C</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17095124&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17095124&amp;dopt=abstract</a> </p><br />

    <p>2.     59376   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Preparation of bicyclic heterocycles, particularly pyrimido[2,1-c][1,4]oxazine-2-carboxamides, as HIV integrase inhibitors</p>

    <p>          Naidu, BNarasimhulu, Banville, Jacques, Beaulieu, Francis, Connolly, Timothy P, Krystal, Mark R, Matiskella, John D, Ouellet, Carl, Plamondon, Serge, Remillard, Roger, Sorenson, Margaret E, Ueda, Yasutsugu, and Walker, Michael A</p>

    <p>          PATENT:  US <b>2006199956</b>  ISSUE DATE:  20060907</p>

    <p>          APPLICATION: 2005-26389  PP: 182pp., Cont.-in-part of U.S. Ser. No. 126,891.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     59377   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Anti-HIV quinuclidine compounds</p>

    <p>          Lecanu, Laurent, Greeson, Janet, and Papadopoulos, Vassilios</p>

    <p>          PATENT:  WO <b>2006085890</b>  ISSUE DATE:  20060817</p>

    <p>          APPLICATION: 2005  PP: 28pp.</p>

    <p>          ASSIGNEE:  (Samaritan Pharmaceuticals, Inc. USA and Georgetown University)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     59378   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Anti-HIV-1 Constituents from Leaves and Twigs of Cratoxylum arborescens</p>

    <p>          Reutrakul, V, Chanakul, W, Pohmakotr, M, Jaipetch, T, Yoosook, C, Kasisit, J, Napaswat, C, Santisuk, T, Prabpai, S, Kongsaeree, P, and Tuchinda, P</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091434&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17091434&amp;dopt=abstract</a> </p><br />

    <p>5.     59379   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Arylthiopyrrole (AThP) Derivatives as Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors: Synthesis, Structure-Activity Relationships, and Docking Studies (Part 2)</p>

    <p>          Lavecchia, A, Costi, R, Artico, M, Miele, G, Novellino, E, Bergamini, A, Crespan, E, Maga, G, and Di, Santo R</p>

    <p>          ChemMedChem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17089434&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17089434&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     59380   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Arylthiopyrrole (AThP) Derivatives as Non-Nucleoside HIV-1 Reverse Transcriptase Inhibitors: Synthesis, Structure-Activity Relationships, and Docking Studies (Part 1)</p>

    <p>          Di Santo, R, Costi, R, Artico, M, Miele, G, Lavecchia, A, Novellino, E, Bergamini, A, Cancio, R, and Maga, G</p>

    <p>          ChemMedChem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17089433&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17089433&amp;dopt=abstract</a> </p><br />

    <p>7.     59381   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Molecular Mechanism by which K70E in HIV-1 Reverse Transcriptase Confers Resistance to Nucleoside Reverse Transcriptase Inhibitors</p>

    <p>          Sluis-Cremer, N, Sheen, CW, Zelina, S, Argoti, Torres PS, Parikh, UM, and Mellors, JW</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088490&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17088490&amp;dopt=abstract</a> </p><br />

    <p>8.     59382   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Critical role of the N-loop and beta1-strand hydrophobic clusters of RANTES-derived peptides in anti-HIV activity</p>

    <p>          Vangelista, L, Longhi, R, Sironi, F, Pavone, V, and Lusso, P</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17083916&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17083916&amp;dopt=abstract</a> </p><br />

    <p>9.     59383   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          The Role of V1V2 and other HIV-1 Envelope Domains in Resistance to Autologous Neutralization during Clade C Infection</p>

    <p>          Rong, R, Bibollet-Ruche, F, Mulenga, J, Allen, S, Blackwell, JL, and Derdeyn, CA</p>

    <p>          J Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079307&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17079307&amp;dopt=abstract</a> </p><br />

    <p>10.   59384   HIV-LS-361; PUBMED-HIV-11/13/2006</p>

    <p class="memofmt1-2">          Ring substituent effects on biological activity of vinyl sulfones as inhibitors of HIV-1</p>

    <p>          Meadows, DC, Sanchez, T, Neamati, N, North, TW, and Gervay-Hague, J</p>

    <p>          Bioorg Med Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17074494&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17074494&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   59385   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Discovery, synthesis and optimization of a new series of selective HIV integrase inhibitors leading to MK-0518 currently in Phase III clinical trial for treatment of HIV/AIDS</p>

    <p>          Summa, Vincenzo</p>

    <p>          Abstracts of Papers, 232nd ACS National Meeting, San Francisco, CA, United States, Sept. 10-14, 2006 <b>2006</b>.: MEDI-298</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   59386   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Short-term antiviral activity of TMC278 - a novel NNRTI - in treatment-naive HIV-1-infected subjects</p>

    <p>          Goebel, Frank, Yakovlev, Alexy, Pozniak, Anton L, Vinogradova, Elena, Boogaerts, Griet, Hoetelmans, Richard, de Bethune, Marie-Pierre P, Peeters, Monika, and Woodfall, Brian</p>

    <p>          AIDS (Hagerstown, MD, United States) <b>2006</b>.  20(13): 1721-1726</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   59387   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Rational design of polymerase inhibitors as antiviral drugs</p>

    <p>          Oeberg, Bo</p>

    <p>          Antiviral Research <b>2006</b>.  71(2-3): 90-95</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   59388   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          CoMFA study on quinolones as novel inhibitors of HIV-1 reverse transcriptase</p>

    <p>          Yi, Ping and Qiu, Minghua</p>

    <p>          Jisuanji Yu Yingyong Huaxue <b>2006</b>.  23(5): 399-402</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   59389   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Synthesis, antiviral and cytotoxicity of some new sulphonamides</p>

    <p>          Selvam, P, Abileshini, B, Alagarsamy, V, Pannecouque, C, and De Clercq, E</p>

    <p>          Indian Journal of Heterocyclic Chemistry <b>2006</b>.  16(1): 73-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   59390   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Phosphonate analogs of hiv inhibitor compounds</p>

    <p>          Boojamra, Constantine G, Lin, Kuei-Ying, Mackman, Richard L, Markevitch, David Y, Petrakovsky, Oleg V, Ray, Adrian S, and Zhang, Lijun</p>

    <p>          PATENT:  WO <b>2006110157</b>  ISSUE DATE:  20061019</p>

    <p>          APPLICATION: 2005  PP: 511pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   59391   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Suppression of HIV-1 Protease Inhibitor Resistance by Phosphonate-mediated Solvent Anchoring</p>

    <p>          Cihlar, Tomas, He, Gong-Xin, Liu, Xiaohong, Chen, James M, Hatada, Marcos, Swaminathan, Swami, McDermott, Martin J, Yang, Zheng-Yu, Mulato, Andrew S, Chen, Xiaowu, Leavitt, Stephanie A, Stray, Kirsten M, and Lee, William A</p>

    <p>          Journal of Molecular Biology <b>2006</b>.  363(3): 635-647</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>18.   59392   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Fused pyrimidinones as HIV integrase inhibitors and their preparation and use in the treatment of HIV infection</p>

    <p>          Cecere, Giuseppe, Ferreira, Maria Del Rosario Rico, Jones, Philip, Pace, Paola, Petrocchi, Alessia, and Summa, Vincenzo</p>

    <p>          PATENT:  WO <b>2006103399</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2006  PP: 163pp.</p>

    <p>          ASSIGNEE:  (Istituto De Ricerche Di Biologia Molecolare P Angeletti SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   59393   HIV-LS-361; SCIFINDER-HIV-11/7/2006</p>

    <p class="memofmt1-2">          Fatty acid conjugates with HIV gp41-derived peptide for treatment of HIV infections</p>

    <p>          Wring, Stephen, Frick, Lloyd, Schneider, Stephen, Zhang, Huyi, Di, Jie, and Heilman, David</p>

    <p>          PATENT:  WO <b>2006105201</b>  ISSUE DATE:  20061005</p>

    <p>          APPLICATION: 2006  PP: 118pp.</p>

    <p>          ASSIGNEE:  (Trimeris, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   59394   HIV-LS-361; WOS-HIV-11/6/2006</p>

    <p class="memofmt1-2">          Solution structures and characterization of human immunodeficiency virus Rev responsive element IIB RNA targeting zinc finger proteins</p>

    <p>          Mishra, SH, Shelley, CM, Barrow, DJ, Darby, MK, and Germann, MW</p>

    <p>          BIOPOLYMERS <b>2006</b>.  83(4): 352-364, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241344000003</a> </p><br />

    <p>21.   59395   HIV-LS-361; WOS-HIV-11/13/2006</p>

    <p class="memofmt1-2">          The engagement of activating Fc gamma Rs inhibits primate lentivirus replication in human macrophages</p>

    <p>          David, A, Saez-Cirion, A, Versmisse, P, Malbec, O, Iannascoli, B, Herschke, F, Lucas, M, Barre-Sinoussi, F, Mouscadet, JF, Daeron, M, and Pancino, G</p>

    <p>          JOURNAL OF IMMUNOLOGY <b>2006</b>.  177(9): 6291-6300, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241477400063">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000241477400063</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
