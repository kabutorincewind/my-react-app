

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2014-11-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fTF0Wavk4ENJBCJr+Nuf+Cl495kouayMh9Hq0e0vjnsvbhsdOSozQYZKJPf2eS7gQmQJH1fklIjpByNQT6+iw5+Wz2W5bUu3avQU8Ay9/nDXUWq7pLle6SEeGEY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B3F988A7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: November 7 - November 20, 2014</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24858350">Spectroscopic and Biological Studies of New Binuclear Metal Complexes of a Tridentate ONS Hydrazone Ligand Derived from 4-Amino-6-methyl-3-thioxo-3,4-dihydro-1,2,4-triazin-5(2H)-one and 4,6-Diacetylresorcinol.</a> Adly, O.M. and A.A. Emara. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 132: p. 91-101. PMID[24858350].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25384372">Oxidative Stress, Glutathione Level and Antioxidant Response to Heavy Metals in Multi-resistant Pathogen, Candida tropicalis.</a> Ilyas, S. and A. Rehman. Environmental Monitoring and Assessment, 2015. 187(4115):  7 pp. PMID[25384372].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24887503">Synthesis, Spectroscopic Studies, Antimicrobial Activities and Antitumor of a New Monodentate V-shaped Schiff Base and its Transition Metal Complexes.</a> Ramadan, R.M., A.K. Abu Al-Nasr, and A.F. Noureldeen. Spectrochimica Acta Part A: Molecular and Biomolecular Spectroscopy, 2014. 132: p. 417-422. PMID[24887503].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25280369">Design, Synthesis, and Antibacterial Properties of Dual-ligand Inhibitors of Acetyl-CoA Carboxylase.</a> Silvers, M.A., G.T. Robertson, C.M. Taylor, and G.L. Waldrop. Journal of Medicinal Chemistry, 2014. 57(21): p. 8947-8959. PMID[25280369].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25148516">Orally Bioavailable 6-Chloro-7-methoxy-4(1H)-quinolones Efficacious against Multiple Stages of Plasmodium.</a> Cross, R.M., D.L. Flanigan, A. Monastyrskyi, A.N. LaCrue, F.E. Saenz, J.R. Maignan, T.S. Mutka, K.L. White, D.M. Shackleford, I. Bathurst, F.R. Fronczek, L. Wojtas, W.C. Guida, S.A. Charman, J.N. Burrows, D.E. Kyle, and R. Manetsch. Journal of Medicinal Chemistry, 2014. 57(21): p. 8860-8879. PMID[25148516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25254502">Binding Modes of Reverse Fosmidomycin Analogs toward the Antimalarial Target IspC.</a> Konzuch, S., T. Umeda, J. Held, S. Hahn, K. Brucher, C. Lienau, C.T. Behrendt, T. Grawert, A. Bacher, B. Illarionov, M. Fischer, B. Mordmuller, N. Tanaka, and T. Kurz. Journal of Medicinal Chemistry, 2014. 57(21): p. 8827-8838. PMID[25254502].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p><br />   

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25299353">Two-pronged Attack: Dual Inhibition of Plasmodium falciparum M1 and M17 Metalloaminopeptidases by a Novel Series of Hydroxamic acid-based Inhibitors.</a> Mistry, S.N., N. Drinkwater, C. Ruggeri, K.K. Sivaraman, S. Loganathan, S. Fletcher, M. Drag, A. Paiardini, V.M. Avery, P.J. Scammells, and S. McGowan. Journal of Medicinal Chemistry, 2014. 57(21): p. 9168-9183. PMID[25299353].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_1107-112014.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342991800006">Antioxidant Activity via DPPH, Gram-positive and Gram-negative Antimicrobial Potential in Edible Mushrooms.</a> Ahmad, N., F. Mahmood, S.A. Khalil, R. Zamir, H. Fazal, and B.H. Abbasi. Toxicology and Industrial Health, 2014. 30(9): p. 826-834. ISI[000342991800006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342963200010">Synthesis, Characterization and Biological Evaluation of New Benzimidazoles.</a> Ahmadi, A. Bulgarian Chemical Communications, 2014. 46(3): p. 503-507. ISI[000342963200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">10. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343035800089">Biocompatible Hyperbranched Epoxy/Silver-reduced Graphene oxide-curcumin Nanocomposite as an Advanced Antimicrobial Material.</a> Barua, S., P. Chattopadhyay, M.M. Phukan, B.K. Konwar, J. Islam, and N. Karak. RSC Advances, 2014. 4(88): p. 47797-47805. ISI[000343035800089].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">11. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342527200010">Antimicrobial Properties of Substituted Quino[3,2-b]benzo[1,4]thiazines.</a> Czarny, A., E. Zaczynska, M. Jelen, M. Zimecki, K. Pluta, B. Morak-Mlodawska, J. Artym, and M. Kocieba. Polish Journal of Microbiology, 2014. 63(3): p. 335-339. ISI[000342527200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">12. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343362500036">Synthesis and Antimycobacterial Activity of New Pyrazolate-bridged Dinuclear Complexes of the Type [Pd(u-L)(N3)(PPh3)]2 (PPh3 = Triphenylphosphine; L = Pyrazolates).</a> da Silva, C., J.G. Ferreira, A.E. Mauro, R.C.G. Frem, R.H.A. Santos, P.B. da Silva, F.R. Pavan, L.B. Marino, C.Q.F. Leite, and A.V.G. Netto. Inorganic Chemistry Communications, 2014. 48: p. 153-155. ISI[000343362500036].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342971900010">Determination of Phenolic Content, Antiradical, Antioxidant and Antimicrobial Activities of Turkish Pine Honey.</a> Ekici, L., O. Sagdic, S. Silici, and I. Ozturk. Quality Assurance and Safety of Crops &amp; Foods, 2014. 6(4): p. 439-444. ISI[000342971900010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342746900028">A &quot;One Pot,&quot; Environmentally Friendly, Multicomponent Synthesis of 2-Amino-5-cyano-4-[(2-aryl)]-1H-indol-3-yl-6-hydroxypyrimidines and their Antimicrobial Activity.</a> Gupta, R., A. Jain, Y. Madan, and E. Menghani. Journal of Heterocyclic Chemistry, 2014. 51(5): p. 1395-1403. ISI[000342746900028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343573100001">Synthesis and Antimycobacterial and Photosynthesis-inhibiting Evaluation of 2-(E)-2-substituted-ethenyl-1,3-benzoxazoles.</a> Imramovsky, A., J. Kozic, M. Pesko, J. Stolarikova, J. Vinsova, K. Kralova, and J. Jampilek. Scientific World Journal, 2014. 705973: 11 pp. ISI[000343573100001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342625500008">Synthesis, Characterization, and Antibacterial Activity of Structurally Complex 2-Acylated 2,3,1-benzodiazaborines and Related Compounds.</a> Kanichar, D., L. Roppiyakuda, E. Kosmowska, M.A. Faust, K.P. Tran, F. Chow, E. Buglo, M.P. Groziak, E.A. Sarina, M.M. Olmstead, I. Silva, and H.H. Xu. Chemistry &amp; Biodiversity, 2014. 11(9): p. 1381-1397. ISI[000342625500008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343459000001">Diversity and Antimicrobial Activities of Actinobacteria Isolated from Tropical Mangrove Sediments in Malaysia.</a> Lee, L.H., N. Zainal, A.S. Azman, S.K. Eng, B.H. Goh, W.F. Yin, N.S. Ab Mutalib, and K.G. Chan. Scientific World Journal, 2014. 698178: 14 pp. ISI[000343459000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342935300001">Evaluation of Antifungal Activity and Mechanism of Action of Citral against Candida albicans.</a> Leite, M.C.A., A.P.D. Bezerra, J.P. de Sousa, F.Q.S. Guerra, and E.D. Lima. Evidence-Based Complementary and Alternative Medicine, 2014. 378280: 9 pp. ISI[000342935300001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343113000010">Identification of Anti-tuberculosis Agents That Target the Cell-division Protein FtsZ.</a> Lin, Y., N.Y. Zhu, Y.X. Han, J.D. Jiang, and S.Y. Si. Journal of Antibiotics, 2014. 67(9): p. 671-676. ISI[000343113000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342920900012">Antimicrobial Polyurethane Thermosets based on Undecylenic acid: Synthesis and Evaluation.</a> Lluch, C., B. Esteve-Zarzoso, A. Bordons, G. Lligadas, J.C. Ronda, M. Galia, and V. Cadiz. Macromolecular Bioscience, 2014. 14(8): p. 1170-1180. ISI[000342920900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342831500012">Synthesis, Characterization, Structural Investigation, and Antimicrobial Studies of Mononuclear Zn(II), Cd(II), and Ag(I) Complexes of an N3O Schiff Base.</a> Pal, P.K., A. Banerjee, R. Bhadra, A.D. Jana, and G.K. Patra. Journal of Coordination Chemistry, 2014. 67(18): p. 3107-3120. ISI[000342831500012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342746900002">Polyethylene glycol Mediated, One-pot, Three-component Synthetic Protocol for Novel 3-[3-Substituted-5-mercapto-1,2,4-triazol-4-yl]-spiro-(indan-1&#39;,2-thiazolidin)-4-ones as New Class of Potential Antimicrobial and Antitubercular Agents.</a> Pandey, S.K., A. Ahamd, O.P. Pandey, and K. Nizamuddin. Journal of Heterocyclic Chemistry, 2014. 51(5): p. 1233-1239. ISI[000342746900002].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343262900009">One-pot Synthesis of 3,4-Dihydro-3-hydroxyisochroman-1-one and Evaluation of Acetal Derivatives as Antibacterial and Antifungal Agents.</a> Patel, G.M. and P.T. Deota. Heterocyclic Communications, 2014. 20(5): p. 299-303. ISI[000343262900009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342980500003">Discovery of Novel Mycobacterial DNA Gyrase B Inhibitors: In Silico and in Vitro Biological Evaluation</a>. Saxena, S., J. Renuka, P. Yogeeswari, and D. Sriram. Molecular Informatics, 2014. 33(9): p. 597-609. ISI[000342980500003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br />  

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000343023100006">N-Arylalkylbenzo[d]thiazole-2-carboxamides as Anti-mycobacterial Agents: Design, New Methods of Synthesis and Biological Evaluation.</a> Shah, P., T.M. Dhameliya, R. Bansal, M. Nautiyal, D.N. Kommi, P.S. Jadhavar, J.P. Sridevi, P. Yogeeswari, D. Sriram, and A.K. Chakraborti. MedChemComm, 2014. 5(10): p. 1489-1495. ISI[000343023100006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000342882000012">Antimicrobial Sesquiterpenes from the Chinese Medicinal Plant, Chloranthus angustifolius.</a> Yang, X.Z., C. Wang, J. Yang, D.R. Wan, Q.X. Lin, G.Z. Yang, Z.N. Mei, and Y.J. Feng. Tetrahedron Letters, 2014. 55(41): p. 5632-5634. ISI[000342882000012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_1107-112014.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
