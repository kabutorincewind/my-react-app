

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2012-11-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="899RoTI1yC/bkraf/XDqqGmasv4BGCSko5mv87xK0CAyaaRbAByHb2aOvZoMSiqjoro+hE/dSKUCfrbPGLcEqGPJ8M4YjIMDz3cM5QloGOvIo5DhOzIAoMU8Nf4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BF9876F4" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List:  November 9 - November 20, 2012</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext"><a href="http://www.ncbi.nlm.nih.gov/pubmed/23058887">1. Synthesis and Characterization of Some New Quinoline Based Derivatives Endowed with Broad Spectrum Antimicrobial Potency.</a> Desai, N.C., K.M. Rajpara, and V.V. Joshi. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): p. 6871-6875. PMID[23058887].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23139125">Bioactive Anthraquinones from Endophytic Fungus Aspergillus versicolor Isolated from Red Sea Algae.</a> Hawas, U.W., A.A. El-Beih, and A.M. El-Halawany. Archives of Pharmacal Research, 2012. 35(10): p. 1749-1756. PMID[23139125].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147740">Posaconazole Pharmacodynamic Target Determination against Wild-type and Cyp51 Mutant Isolates of Aspergillus fumigatus in an in Vivo Model of Invasive Pulmonary Aspergillosis.</a> Lepak, A.J., K. Marchillo, J. Vanhecker, and D.R. Andes. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23147740].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23148568">Biocidal Activity of Metalloacid-coated Surfaces against Multidrug-resistant Microorganisms.</a> Tetault, N., H. Gbaguidi-Haore, X. Bertrand, R. Quentin, and N. van der Mee-Marquet. Antimicrobial Resistance and Infection Control, 2012. 1(1): p. 35. PMID[23148568].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23152548">Diagnostic Accuracy and Reproducibility of WHO-endorsed Phenotypic Drug Susceptibility Testing Methods for First-line and Second-line Anti-tuberculosis Drugs: A Systematic Review and Meta-analysis.</a> Horne, D.J., L.M. Pinto, M. Arentz, S.Y. Lin, E. Desmond, L.L. Flores, K.R. Steingart, and J. Minion. Journal of Clinical Microbiology, 2012. <b>[Epub ahead of print]</b>. PMID[23152548].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23046964">Synthesis and Antituberculosis Activity of Novel Unfolded and Macrocyclic Derivatives of ent-Kaurane steviol.</a> Khaybullin, R.N., I.Y. Strobykina, A.B. Dobrynin, A.T. Gubaydullin, R.V. Chestnova, V.M. Babaev, and V.E. Kataev. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): p. 6909-6913. PMID[23046964].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23058885">Design and Synthesis of 1H-1,2,3-Triazoles Derived from Econazole as Antitubercular Agents.</a> Kim, S., S.N. Cho, T. Oh, and P. Kim. Bioorganic &amp; Medicinal Chemistry Letters, 2012. 22(22): p. 6844-6847. PMID[23058885].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p>  
    <p> </p>
    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23062712">The Synthesis and Antituberculosis Activity of 5&#39;-nor Carbocyclic uracil Derivatives.</a> Matyugina, E., A. Khandazhinskaya, L. Chernousova, S. Andreevskaya, T. Smirnova, A. Chizhov, I. Karpenko, S. Kochetkov, and L. Alexandrova. Bioorganic &amp; Medicinal Chemistry, 2012. 20(22): p. 6680-6686. PMID[23062712].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23136919">Antibacterial Activity of Long-chain Fatty Alcohols against Mycobacteria.</a> Mukherjee, K., P. Tribedi, B. Mukhopadhyay, and A.K. Sil. FEMS Microbiology Letters, 2012. <b>[Epub ahead of print]</b>. PMID[23136919].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23150983">Synthesis and Antitubercular Activity of 2-(Substituted phenyl/benzyl-amino)-6-(4-chlorophenyl)-5-(methoxycarbonyl)-4-methyl-3,6-dihydropyrimidin-1-ium chlorides.</a> Narayanaswamy, V.K., S.K. Nayak, M. Pillay, R. Prasanna, Y.M. Coovadia, and B. Odhav. Chemical Biology &amp; Drug Design, 2012. <b>[Epub ahead of print]</b>. PMID[23150983].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23149517">Bis-Imidazolinylindoles are Active against Methicillin-resistant Staphylococcus aureus and Multidrug-resistant Mycobacterium tuberculosis.</a> Panchal, R.G., D. Lane, H.I. Boshoff, M.M. Butler, D.T. Moir, T.L. Bowlin, and S. Bavari. The journal of Antibiotics, 2012. <b>[Epub ahead of print]</b>. PMID[23149517].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23150066">Potent Activity against Multidrug-resistant Mycobacterium tuberculosis of alpha-Mangostin Analogs.</a> Sudta, P., P. Jiarawapi, A. Suksamrarn, P. Hongmanee, and S. Suksamrarn. Chemical &amp; Pharmaceutical Bulletin, 2012. <b>[Epub ahead of print]</b>. PMID[23150066].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23145884">Antibacterial Butenolides from the Korean Tunicate pseudodistoma Antinboja.</a> Wang, W., H. Kim, S.J. Nam, B.J. Rho, and H. Kang. Journal of Natural Products, 2012. <b>[Epub ahead of print]</b>. PMID[23145884].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23145816">Mimicking the Intramolecular Hydrogen Bond: Synthesis, Biological Evaluation, and MOLECULAR Modeling of Benzoxazines and Quinazolines as Potential Antimalarial Agents.</a> Gemma, S., C. Camodeca, M. Brindisi, S. Brogi, G. Kukreja, S. Kunjir, E. Gabellieri, L. Lucantoni, A. Habluetzel, D. Taramelli, N. Basilico, R. Gualdani, F. Tadini-Buoninsegni, G. Bartolommei, M.R. Moncelli, R.E. Martin, R.L. Summers, S. Lamponi, L. Savini, I. Fiorini, M. Valoti, E. Novellino, G. Campiani, and S. Butini. Journal of Medicinal Chemistry, 2012. <b>[Epub ahead of print]</b>. PMID[23145816].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23137860">Antimalarial Interaction of Quinine and Quinidine with Clarithromycin.</a> Pandey, S.K., H. Dwivedi, S. Singh, W.A. Siddiqui, and R. Tripathi. Parasitology, 2012. <b>[Epub ahead of print]</b>: p. 1-8. PMID[23137860].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p>  
    <p> </p>
    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23147722">Mechanism-based Model of Parasite Growth and Dihydroartemisinin Pharmacodynamics in Murine Malaria.</a> Patel, K., K.T. Batty, B.R. Moore, P.L. Gibbons, J.B. Bulitta, and C.M. Kirkpatrick. Antimicrobial Agents and Chemotherapy, 2012. <b>[Epub ahead of print]</b>. PMID[23147722].
    <br />
    <b>[PubMed]</b>. OI_1109-112012.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309872600002">Endophytes and Associated Marine Derived Fungi-ecological and Chemical Perspectives.</a> Debbab, A., A.H. Aly, and P. Proksch. Fungal Diversity, 2012. 57(1): p. 45-83. ISI[000309872600002].
    <br />
    <b>[WOS]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309611500010">Candida parapsilosis as a Potent Biocontrol Agent against Growth and Aflatoxin Production by Aspergillus Species.</a> Niknejad, F., F. Zaini, M.A. Faramarzi, M. Amini, P. Kordbacheh, M. Mahmoudi, and M. Safara. Iranian Journal of Public Health, 2012. 41(10): p. 72-80. ISI[000309611500010].
    <br />
    <b>[WOS]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309649100012">Antifungal Spectrum, in Vivo Efficacy, and Structure-activity Relationship of Ilicicolin H.</a> Singh, S.B., W.G. Liu, X.H. Li, T. Chen, A. Shafiee, D. Card, G. Abruzzo, A. Flattery, C. Gill, J.R. Thompson, M. Rosenbach, S. Dreikorn, V. Hornak, M. Meinz, M. Kurtz, R. Kelly, and J.C. Onishi. Acs Medicinal Chemistry Letters, 2012. 3(10): p. 814-817. ISI[000309649100012].
    <br />
    <b>[WOS]</b>. OI_1109-112012.</p> 
    <p> </p>
    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000309615600009">Screening and Evaluation of Antimicrobial Activity of Tannery Fleshing Utilizing Fish Gut Bacteria.</a> Sumathi, C., S. Jayashree, and G. Sekaran. International Journal of Pharmacology, 2012. 8(5): p. 373-381. ISI[000309615600009].
    <br />
    <b>[WOS]</b>. OI_1109-112012.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
