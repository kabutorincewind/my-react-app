

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2009-03-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="LeC18wm08ay89Jl4yJfzyKzmDA7Ti2vb6QjPoFGEPqRvqhYhRlWl1XzAWDTtIdrgu2oI1/JNsfb8fSUlb4bqNwhSC09yFQ3/sH8ki2Pb/jBdVH9iwxVTcZNzcmc=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="54862E8B" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">Other Viruses Citation List:  March 4, 2009 - March 17, 2009</h1>

    <p class="memofmt2-2">Cytomegalovirus</p>

    <p class="ListParagraph">1.      <a href="http://www.ncbi.nlm.nih.gov.proxy-um.researchport.umd.edu/pubmed/19285980?ordinalpos=13&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Brefeldin A inhibits expression of DNA packaging proteins and nucleocapsid formation of human cytomegalovirus.</a></p>

    <p class="NoSpacing">Kregler O, Schilf R, Lander A, Bannert N, Bogner E.</p>

    <p class="NoSpacing">FEBS Lett. 2009 Mar 13. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19285980 [PubMed - as supplied by publisher]</p>

    <p class="memofmt2-2">Hepatitis Virus</p>

    <p class="ListParagraph">2.      <a href="http://www.ncbi.nlm.nih.gov.proxy-um.researchport.umd.edu/pubmed/19289518?ordinalpos=3&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">ODE-(S)-HPMPA IS A POTENT AND SELECTIVE INHIBITOR OF HEPATITIS C VIRUS REPLICATION IN GENOTYPE 1A, 1B AND 2A REPLICONS.</a></p>

    <p class="NoSpacing">Wyles DL, Kaihara KA, Korba BE, Schooley RT, Beadle JR, Hostetler KY.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2009 Mar 16. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19289518 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">3.      <a href="http://www.ncbi.nlm.nih.gov.proxy-um.researchport.umd.edu/pubmed/19289281?ordinalpos=4&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Anti-HBV agents. Part 2: Synthesis and in vitro anti-hepatitis B virus activities of alisol A derivatives.</a></p>

    <p class="NoSpacing">Zhang Q, Jiang ZY, Luo J, Liu JF, Ma YB, Guo RH, Zhang XM, Zhou J, Chen JJ.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Mar 4. [Epub ahead of print]</p>

    <p class="NoSpacing">PMID: 19289281 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">4.      <a href="http://www.ncbi.nlm.nih.gov.proxy-um.researchport.umd.edu/pubmed/19216075?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum">Synthesis and SAR of piperazinyl-N-phenylbenzamides as inhibitors of hepatitis C virus RNA replication in cell culture.</a></p>

    <p class="NoSpacing">Conte I, Giuliano C, Ercolani C, Narjes F, Koch U, Rowley M, Altamura S, De Francesco R, Neddermann P, Migliaccio G, Stansfield I.</p>

    <p class="NoSpacing">Bioorg Med Chem Lett. 2009 Mar 15;19(6):1779-83. Epub 2009 Jan 27.</p>

    <p class="NoSpacing">PMID: 19216075 [PubMed - in process]</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
