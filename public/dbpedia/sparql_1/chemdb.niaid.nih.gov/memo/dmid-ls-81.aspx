

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-81.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fvk7QLODaIyxrevGG0EaRkaDspUrW9qZ3adMOHZs903LBaQAV/3OrHsGA3nTB0RsFv5y3RGEZQm567IGkKUcs8GyGo2oeJFLP0AbWHWPR67fcqgusgfuMmEi1M0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="807E535A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-81-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         6633   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Use of combinations of reverse transcriptase inhibitors and viral DNA polymerase inhibitors for the treatment of viral diseases</p>

    <p>          Jahn, Gerhard, Schott, Herbert, Hamprecht, Klaus, and Mikeler, Elfriede</p>

    <p>          PATENT:  WO <b>2004098640</b>  ISSUE DATE:  20041118</p>

    <p>          APPLICATION: 2004  PP: 42 pp.</p>

    <p>          ASSIGNEE:  (Eberhard-Karls-Universitat Tubingen Universitatsklinikum, Germany</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>2.         6634   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Aminothiazole inhibitors of HCV RNA polymerase</p>

    <p>          Shipps, GW Jr, Deng, Y, Wang, T, Popovici-Muller, J, Curran, PJ, Rosner, KE, Cooper, AB, Girijavallabhan, V, Butkiewicz, N, and Cable, M</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(1): 115-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582422&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15582422&amp;dopt=abstract</a> </p><br />

    <p>3.         6635   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral Drugs that Target Cellular Proteins May Play Major Roles in Combating HIV Resistance</p>

    <p>          Provencher, VM, Coccaro, E, Lacasse, JJ, and Schang, LM</p>

    <p>          Curr Pharm Des  <b>2004</b>.  10(32): 4081-101</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579090&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579090&amp;dopt=abstract</a> </p><br />

    <p>4.         6636   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Virus attachment and entry offer numerous targets for antiviral therapy</p>

    <p>          Altmeyer, R</p>

    <p>          Curr Pharm Des  <b>2004</b>.  10(30): 3701-12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579065&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15579065&amp;dopt=abstract</a> </p><br />

    <p>5.         6637   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          New acridone inhibitors of human herpes virus replication</p>

    <p>          Bastow, KF</p>

    <p>          Curr Drug Targets Infect Disord <b>2004</b>.  4(4): 323-30</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578973&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15578973&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         6638   DMID-LS-81; WOS-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Advances in Hepatitis B and C</p>

    <p>          Thomson, E. and Main, J.</p>

    <p>          Current Opinion in Infectious Diseases <b>2004</b>.  17(5): 449-459</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>7.         6639   DMID-LS-81; WOS-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Adenovirus</p>

    <p>          Anon</p>

    <p>          American Journal of Transplantation <b>2004</b>.  4: 101-104</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>8.         6640   DMID-LS-81; WOS-DMID-12/8/2004</p>

    <p class="memofmt1-2">          The Human Polyomavirus, Jcv, Uses Serotonin Receptors to Infect Cells</p>

    <p>          Elphick, G. <i>et al.</i></p>

    <p>          Science <b>2004</b> .  306(5700): 1380-1383</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>9.         6641   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Role of viral and host factors in HCV persistence: which lesson for therapeutic and preventive strategies?</p>

    <p>          Missale, G, Cariani, E, and Ferrari, C</p>

    <p>          Dig Liver Dis <b>2004</b>.  36(11): 703-11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570998&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570998&amp;dopt=abstract</a> </p><br />

    <p>10.       6642   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral research - 15th international conference. The herpesviruses</p>

    <p>          Hodge, AV</p>

    <p>          IDrugs <b>2002</b>.  5(5): 397-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570452&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570452&amp;dopt=abstract</a> </p><br />

    <p>11.       6643   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral Research - 15th International Conference. HBV and HCV therapies</p>

    <p>          Smee, DF</p>

    <p>          IDrugs <b>2002</b>.  5(5): 392-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570451&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15570451&amp;dopt=abstract</a> </p><br />

    <p>12.       6644   DMID-LS-81; WOS-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Inhibition of Epstein-Barr Virus by the Triterpenoid Betulin Diphosphate and Uvaol</p>

    <p>          Muhammad, A. <i>et al.</i></p>

    <p>          Journal of Microbiology and Biotechnology <b>2004</b>.  14(5): 1086-1088</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.       6645   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          A pilot, open-labelled, phase II study using oral ribavirin in the treatment of patients with chronic active hepatitis B</p>

    <p>          Tong, MJ, Hwang, SJ, Lefkowitz, M, Lee, SD, Co, RL, and Lo, KJ</p>

    <p>          Clin Diagn Virol <b>1995</b>.  3(4): 377-85</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566819&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566819&amp;dopt=abstract</a> </p><br />

    <p>14.       6646   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral susceptibility testing of cytomegalovirus: criteria for detecting resistance to antivirals</p>

    <p>          Lawrence, Drew W, Miner, R, and Saleh, E</p>

    <p>          Clin Diagn Virol <b>1993</b>.  1(3): 179-85</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566731&amp;dopt=abstract</a> </p><br />

    <p>15.       6647   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis and evaluation of keto-glutamine analogues as potent inhibitors of severe acute respiratory syndrome 3CLpro</p>

    <p>          Jain, RP, Pettersson, HI, Zhang, J, Aull, KD, Fortin, PD, Huitema, C, Eltis, LD, Parrish, JC, James, MN, Wishart, DS, and Vederas, JC</p>

    <p>          J Med Chem <b>2004</b>.  47(25): 6113-6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566280&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566280&amp;dopt=abstract</a> </p><br />

    <p>16.       6648   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Preparation of 5-aza-7-deazapurine nucleosides as antiviral agents for treating flaviviridae</p>

    <p>          La Colla, Paolo, Gosselin, Gilles, Seela, Frank, Dukhan, David, and Leroy, Frederic</p>

    <p>          PATENT:  WO <b>2004096197</b>  ISSUE DATE:  20041111</p>

    <p>          APPLICATION: 2004  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (Universita Degli Studi di Cagliari, Italy, Centre National de la Recherche Scientifique, and Universitat Osnabruck Laboratorium fur Organic and Biorganic Chemie)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>17.       6649   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Neuraminidase Inhibitor-Rimantadine Combinations Exert Additive and Synergistic Anti-Influenza Virus Effects in MDCK Cells</p>

    <p>          Govorkova, EA, Fang, HB, Tan, M, and Webster, RG</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4855-63</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561867&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561867&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.       6650   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Novel Nonnucleoside Inhibitor of Hepatitis C Virus RNA-Dependent RNA Polymerase</p>

    <p>          Howe, AY, Bloom, J, Baldick, CJ, Benetatos, CA, Cheng, H, Christensen, JS, Chunduru, SK, Coburn, GA, Feld, B, Gopalsamy, A, Gorczyca, WP, Herrmann, S, Johann, S, Jiang, X, Kimberland, ML, Krisnamurthy, G, Olson, M, Orlowski, M, Swanberg, S, Thompson, I, Thorn, M, Del, Vecchio A, Young, DC, van, Zeijl M, Ellingboe, JW, Upeslacis, J, Collett, M, Mansour, TS, and O&#39;connell, JF</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4813-21</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561861&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561861&amp;dopt=abstract</a> </p><br />

    <p>19.       6651   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Combination of a Hepatitis C Virus NS3-NS4A Protease Inhibitor and Alpha Interferon Synergistically Inhibits Viral RNA Replication and Facilitates Viral RNA Clearance in Replicon Cells</p>

    <p>          Lin, K, Kwong, AD, and Lin, C</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4784-92</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561857&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561857&amp;dopt=abstract</a> </p><br />

    <p>20.       6652   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Potent and long-acting dimeric inhibitors of influenza virus neuraminidase are effective at a once-weekly dosing regimen</p>

    <p>          Macdonald, SJ, Watson, KG, Cameron, R, Chalmers, DK, Demaine, DA, Fenton, RJ, Gower, D, Hamblin, JN, Hamilton, S, Hart, GJ, Inglis, GG, Jin, B, Jones, HT, McConnell, DB, Mason, AM, Nguyen, V, Owens, IJ, Parry, N, Reece, PA, Shanahan, SE, Smith, D, Wu, WY, and Tucker, SP</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4542-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561823&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561823&amp;dopt=abstract</a> </p><br />

    <p>21.       6653   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Effect of antimetabolite immunosuppressants on Flaviviridae, including hepatitis C virus</p>

    <p>          Stangl, Jason R, Carroll, Kathleen L, Illichmann, Mitchell, and Striker, Robert</p>

    <p>          Transplantation <b>2004</b>.  77(4): 562-567</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>22.       6654   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antimicrobial peptides: premises and promises</p>

    <p>          Reddy, KV, Yedery, RD, and Aranha, C</p>

    <p>          Int J Antimicrob Agents <b>2004</b>.  24(6): 536-47</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15555874&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15555874&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.       6655   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Antiviral oligonucleotides targeting RSV (respiratory syncytial virus)</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  US <b>20040229828</b>  ISSUE DATE: 20041118</p>

    <p>          APPLICATION: 2003-6055  PP: 101 pp., Cont.-in-part of Appl. No. PCT/IB03-04573.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>24.       6656   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          &lt;04 Article Title&gt;</p>

    <p>          Allen, Scott H, Johns, Brian A, and Gudmundsson, Kristjan</p>

    <p>          &lt;10 Journal Title&gt; <b>2004</b>.(&lt;24 Issue ID&gt;): &lt;25 Page(s)&gt;</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>25.       6657   DMID-LS-81; PUBMED-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Early viral kinetics and treatment outcome in combination of high-dose interferon induction vs. pegylated interferon plus ribavirin for naive patients infected with hepatitis C virus of genotype 1b and high viral load</p>

    <p>          Tsubota, A, Arase, Y, Someya, T, Suzuki, Y, Suzuki, F, Saitoh, S, Ikeda, K, Akuta, N, Hosaka, T, Kobayashi, M, and Kumada, H</p>

    <p>          J Med Virol <b>2005</b>.  75(1): 27-34</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15543591&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15543591&amp;dopt=abstract</a> </p><br />

    <p>26.       6658   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Heterocyclic compounds, specifically 3,6-disubstituted 3H-furo[2,3-d]pyrimidin-2-ones and 2,6-disubstituted furo[2,3-d]pyrimidines, for use as novel nucleoside analogs and antivirals in the treatment of viral infections, particularly cytomegalovirus</p>

    <p>          McGuigan, Christopher, Balzarini, Jan, and De Clercq, Erik</p>

    <p>          PATENT:  WO <b>2004096813</b>  ISSUE DATE:  20041111</p>

    <p>          APPLICATION: 2004  PP: 62 pp.</p>

    <p>          ASSIGNEE:  (University College Cardiff Consultants Limited, UK and Rega Foundation)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>27.       6659   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Activity of Certain 3-Substituted 2,5,6-Trichloroindole Nucleosides</p>

    <p>          Williams, John D, Chen, Jiong J, Drach, John C, and Townsend, Leroy B</p>

    <p>          Journal of Medicinal Chemistry <b>2004</b>.  47(23): 5753-5765</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>28.       6660   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p><b>          Preparation of pyrrolylureas as antivirals, particularly for use against cytomegaloviruses</b> </p>

    <p>          Zimmermann, Holger, Brueckner, David, Heimbach, Dirk, Henninger, Kerstin, Hewlett, Guy, Rosentreter, Ulrich, Schohe-Loop, Rudolf, Baumeister, Judith, Schmidt, Thorsten, Reefschlaeger, Juergen, Lang, Dieter, Lin, Tse-i, and Radtke, Martin</p>

    <p>          PATENT:  WO <b>2004052852</b>  ISSUE DATE:  20040624</p>

    <p>          APPLICATION: 2003  PP: 108 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare Ag, Germany</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>29.       6661   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          The Synthesis and Antiherpetic Activity of Acyclovir Phosphonate Esters</p>

    <p>          Jasko, MV, Ulanova, NYu, Andronova, VL, Ivanov, AV, Karpenko, IL, Kukhanova, MK, Galegov, GA, and Skoblov, YuS</p>

    <p>          Russian Journal of Bioorganic Chemistry (Translation of Bioorganicheskaya Khimiya) <b>2004</b>.  30 (6): 539-546</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>30.       6662   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis of new 6-substituted purinyl-5&#39;-nor-1&#39;-homo-carba-nucleosides based on indanol</p>

    <p>          Fernandez, Franco, Garcia-Mera, Xerardo, Morales, Melvin, Vilarino, Leonardo, Caamano, Olga, and De Clercq, Eric</p>

    <p>          Tetrahedron <b>2004</b>.  60(41): 9245-9253</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>31.       6663   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Properties of Arabino and Ribonucleosides of 1,3-Dideazaadenine, 4-Nitro-1, 3-dideazaadenine and Diketopiperazine</p>

    <p>          Sinha, Sarika, Srivastava, Richa, De Clercq, Erik, and Singh, Ramendra K</p>

    <p>          Nucleosides, Nucleotides &amp; Nucleic Acids <b>2004</b>.  23(12): 1815-1824</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>32.       6664   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Method of inhibiting human metapneumovirus and human coronavirus in the prevention and treatment of severe acute respiratory syndrome (SARS)</p>

    <p>          Gallaher, William R and Garry, Robert F</p>

    <p>          PATENT:  US <b>20040229219</b>  ISSUE DATE: 20041118</p>

    <p>          APPLICATION: 2004-48234  PP: 54 pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>33.       6665   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Methods and compositions for inhibiting SARS-related coronavirus replication by using inhibitors of SARS related coronavirus 3C proteinase</p>

    <p>          Fuhrman, Shella Ann, Matthews, David Allan, Patick, Amy Karen, and Rejto, Paul Abraham</p>

    <p>          PATENT:  WO <b>2004093860</b>  ISSUE DATE:  20041104</p>

    <p>          APPLICATION: 2004  PP: 84 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>34.       6666   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Identification of Novel Small-Molecule Inhibitors of Severe Acute Respiratory Syndrome-Associated Coronavirus by Chemical Genetics</p>

    <p>          Kao, Richard Y, Tsui, Wayne HW, Lee, Terri SW, Tanner, Julian A, Watt, Rory M, Huang, Jian-Dong, Hu, Lihong, Chen, Guanhua, Chen, Zhiwei, Zhang, Linqi, He, Tian, Chan, Kwok-Hung, Tse, Herman, To, Amanda PC, Ng, Louisa WY, Wong, Bonnie CW, Tsoi, Hoi-Wah, Yang, Dan, Ho, David D, and Yuen, Kwok-Yung</p>

    <p>          Chemistry &amp; Biology <b>2004</b>.  11(9): 1293-1299</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
    <br clear="all">

    <p>35.       6667   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Small molecules targeting severe acute respiratory syndrome human coronavirus</p>

    <p>          Wu, Chung-Yi, Jan, Jia-Tsrong, Ma, Shiou-Hwa, Kuo, Chih-Jung, Juan, Hsueh-Fen, Cheng, Yih-Shyun E, Hsu, Hsien-Hua, Huang, Hsuan-Cheng, Wu, Douglass, Brik, Ashraf, Liang, Fu-Sen, Liu, Rai-Shung, Fang, Jim-Min, Chen, Shui-Tein, Liang, Po-Huang, and Wong, Chi-Huey</p>

    <p>          Proceedings of the National Academy of Sciences of the United States of America <b>2004</b>.  101(27): 10012-10017</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>36.       6668   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p><b>          Potent and selective inhibition of SARS coronavirus replication by aurintricarboxylic acid</b> </p>

    <p>          He, Runtao, Adonov, Anton, Traykova-Adonova, Maya, Cao, Jingxin, Cutts, Todd, Grudesky, Elsie, Deschambaul, Yvon, Berry, Jody, Drebot, Michael, and Li, Xuguang</p>

    <p>          Biochemical and Biophysical Research Communications <b>2004</b>.  320(4): 1199-1203</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>37.       6669   DMID-LS-81; SCIFINDER-DMID-12/8/2004</p>

    <p class="memofmt1-2">          Inhibition of severe acute respiratory syndrome coronavirus replication by niclosamide</p>

    <p>          Wu, Chang-Jer, Jan, Jia-Tsrong, Chen, Chi-Min, Hsieh, Hsing-Pang, Hwang, Der-Ren, Liu, Hwan-Wun, Liu, Chiu-Yi, Huang, Hui-Wen, Chen, Su-Chin, Hong, Cheng-Fong, Lin, Ren-Kuo, Chao, Yu-Sheng, and Hsu, John TA</p>

    <p>          Antimicrobial Agents and Chemotherapy <b>2004</b>.  48(7): 2693-2696</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
