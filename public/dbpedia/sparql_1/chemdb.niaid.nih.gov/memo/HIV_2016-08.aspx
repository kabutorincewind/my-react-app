

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-08.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="4baAVKsJtkDSGKgdKM9iRF+bIqrd2kvqsG2ygx+mB0n19QbM/3y1rWMX42gJAVmv6uYzaqjoJLGUiZOneP/Fnb3R31IicjFWGouX69NiXRQzNAPL1/OupWJ7BYk=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="61EC21B8" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: August, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27351927">Combination Antiretroviral Therapy and Indoleamine 2,3-Dioxygenase in HIV Infections: Challenges and New Opportunities.</a> Ahmad, A., V. Mehraj, M.A. Jenabian, S. Samarani, C. Tremblay, and J.P. Routy. AIDS, 2016. 30(11): p. 1839-1841. PMID[27351927].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">2. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379684700007">Rational Design, Synthesis, anti-HIV-1 RT and Antimicrobial Activity of Novel 3-(6-Methoxy-3,4-dihydroquinolin-1(2h)-yl)-1-(piperazin-1-yl)propan-1-on E Derivatives.</a> Chander, S., P. Wang, P. Ashok, L.M. Yang, Y.T. Zheng, and S. Murugesan. Bioorganic Chemistry, 2016. 67: p. 75-83. ISI[000379684700007].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000380076900011">Design, Synthesis and Biological Evaluation of Polyphenol-alpha-helical Peptide Conjugates as Potent HIV-1 Fusion Inhibitors.</a> Cheng, S.Q., G.D. Liang, X.F. Jiang, C. Wang, and K.L. Liu. Chemical Journal of Chinese Universities, 2016. 37(7): p. 1287-1292. ISI[000380076900011].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27389109">Tyrosine-Sulfated V2 Peptides Inhibit HIV-1 Infection via Coreceptor Mimicry.</a> Cimbro, R., F.C. Peterson, Q. Liu, C. Guzzo, P. Zhang, H. Miao, D. Van Ryk, X. Ambroggio, D.E. Hurt, L. De Gioia, B.F. Volkman, M.A. Dolan, and P. Lusso. EBioMedicine, 2016. 10: p. 45-54. PMID[27389109].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26990134">Studies on Cycloheptathiophene-3-carboxamide Derivatives as Allosteric HIV-1 Ribonuclease H Inhibitors.</a> Corona, A., J. Desantis, S. Massari, S. Distinto, T. Masaoka, S. Sabatini, F. Esposito, G. Manfroni, E. Maccioni, V. Cecchetti, C. Pannecouque, S.F. Le Grice, E. Tramontano, and O. Tabarrini. ChemMedChem, 2016. 11(16): p. 1709-1720. PMID[26990134].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27339173">Discovery and Structure-based Optimization of 2-Ureidothiophene-3-carboxylic acids as Dual Bacterial RNA Polymerase and Viral Reverse Transcriptase Inhibitors.</a> Elgaher, W.A., K.K. Sharma, J. Haupenthal, F. Saladini, M. Pires, E. Real, Y. Mely, and R.W. Hartmann. Journal of Medicinal Chemistry, 2016. 59(15): p. 7212-7222. PMID[27339173].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27317645">Synthesis and Evaluation of Substituted 4-(N-Benzylamino)cinnamate Esters as Potential Anti-cancer Agents and HIV-1 Integrase Inhibitors.</a> Faridoon, A.L. Edkins, M. Isaacs, D. Mnkandhla, H.C. Hoppe, and P.T. Kaye. Bioorganic &amp; Medicinal Chemistry, 2016. 26(15): p. 3810-3812. PMID[27317645].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27540531">The Effect of Root, Shoot and Seed Extracts of the Iranian Thymus L. (Family: Lamiaceae) Species on HIV-1 Replication and CD4 Expression.</a> Farsani, M.S., M. Behbahani, and H.Z. Isfahani. Cell Journal, 2016. 18(2): p. 255-261. PMID[27540531]. PMCID[PMC4988425].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27405794">Reduction Sensitive Lipid Conjugates of Tenofovir: Synthesis, Stability, and Antiviral Activity.</a> Giesler, K.E., J. Marengo, and D.C. Liotta. Journal of Medicinal Chemistry, 2016. 59(15): p. 7097-7110. PMID[27405794].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26727904">Toxicity and in Vitro Activity of HIV-1 Latency-reversing Agents in Primary CNS Cells.</a> Gray, L.R., H. On, E. Roberts, H.K. Lu, M.A. Moso, J.A. Raison, C. Papaioannou, W.J. Cheng, A.M. Ellett, J.C. Jacobson, D.F. Purcell, S.L. Wesselingh, P.R. Gorry, S.R. Lewin, and M.J. Churchill. Journal of Neurovirology, 2016. 22(4): p. 455-463. PMID[26727904].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26851503">The Janus Kinase Inhibitor Ruxolitinib Reduces HIV Replication in Human Macrophages and Ameliorates HIV Encephalitis in a Murine Model.</a> Haile, W.B., C. Gavegnano, S. Tao, Y. Jiang, R.F. Schinazi, and W.R. Tyor. Neurobiology of Disease, 2016. 92(Pt B): p. 137-143. PMID[26851503]. PMCID[PMC4907871].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27534123">A New Neolignan, and the Cytotoxic and anti-HIV-1 Activities of Constituents from the Roots of Dasymaschalon sootepense.</a> Hongthong, S., C. Kuhakarn, T. Jaipetch, P. Piyachaturawat, S. Jariyawat, K. Suksen, J. Limthongkul, N. Nuntasaen, and V. Reutrakul. Natural Product Communications, 2016. 11(6): p. 809-813. PMID[27534123].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27353536">Synthesis and Evaluation of 2-Pyridinylpyrimidines as Inhibitors of HIV-1 Structural Protein Assembly.</a> Kozisek, M., O. Stepanek, K. Parkan, C. Berenguer Albinana, M. Pavova, J. Weber, H.G. Krusslich, J. Konvalinka, and A. Machara. Bioorganic &amp; Medicinal Chemistry, 2016. 26(15): p. 3487-3490. PMID[27353536].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27191975">Extracellular Histones Identified in Crocodile Blood Inhibit in-Vitro HIV-1 Infection.</a> Kozlowski, H.N., E.T. Lai, P.C. Havugimana, C. White, A. Emili, D. Sakac, B. Binnington, A. Neschadim, S.D. McCarthy, and D.R. Branch. AIDS, 2016. 30(13): p. 2043-2052. PMID[27191975].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26914186">Arylazolyl(azinyl)thioacetanilides: Part 19: Discovery of Novel Substituted Imidazo[4,5-b]pyridin-2-ylthioacetanilides as Potent HIV NNRTIs via a Structure-based Bioisosterism Approach.</a> Li, X., B. Huang, Z. Zhou, P. Gao, C. Pannecouque, D. Daelemans, E. De Clercq, P. Zhan, and X. Liu. Chemical Biology &amp; Drug Design, 2016. 88(2): p. 241-253. PMID[26914186].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27454320">An Effective Conjugation Strategy for Designing Short Peptide-based HIV-1 Fusion Inhibitors.</a> Liang, G., H. Wang, H. Chong, S. Cheng, X. Jiang, Y. He, C. Wang, and K. Liu. Organic &amp; Biomolecular Chemistry, 2016. 14(33): p. 7875-7882. PMID[27454320].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27226574">Gallic acid is an Antagonist of Semen Amyloid Fibrils That Enhance HIV-1 Infection.</a> LoRicco, J.G., C.S. Xu, J. Neidleman, M. Bergkvist, W.C. Greene, N.R. Roan, and G.I. Makhatadze. Journal of Biological Chemistry, 2016. 291(27): p. 14045-14055. PMID[27226574]. PMCID[PMC4933164].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27456816">Discovery and Characterization of Novel Small-molecule CXCR4 Receptor Agonists and Antagonists.</a> Mishra, R.K., A.K. Shum, L.C. Platanias, R.J. Miller, and G.E. Schiltz. Scientific Reports, 2016. 6(30155): 9pp. PMID[27456816]. PMCID[PMC4960487].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27509004">1,2,3-Triazoles as Amide Bioisosteres: Discovery of a New Class of Potent HIV-1 Vif Antagonists.</a> Mohammed, I., I.R. Kummetha, G. Singh, N. Sharova, G. Lichinchi, J. Dang, M. Stevenson, and T.M. Rana. Journal of Medicinal Chemistry, 2016. 59(16): p. 7677-7682. PMID[27509004].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27384665">Structure-Activity Relationships of the Human Immunodeficiency Virus Type 1 Maturation Inhibitor PF-46396.</a> Murgatroyd, C., L. Pirrie, F. Tran, T.K. Smith, N.J. Westwood, and C.S. Adamson. Journal of Virology, 2016. 90(18): p. 8181-8197. PMID[27384665].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27529554">Histone Deacetylase Inhibitors Enhance CD4 T Cell Susceptibility to NK Cell Killing but Reduce NK Cell Function.</a> Pace, M., J. Williams, A. Kurioka, A.B. Gerry, B. Jakobsen, P. Klenerman, N. Nwokolo, J. Fox, S. Fidler, and J. Frater. Plos Pathogens, 2016. 12(8): p. e1005782. PMID[27529554]. PMCID[PMC4986965].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27556447">Inhibitory Effect of 2,3,5,6-Tetrafluoro-4-[4-(aryl)-1H-1,2,3-triazol-1-yl]benzenesulfonamide Derivatives on HIV Reverse Transcriptase Associated RNase H Activities.</a> Pala, N., F. Esposito, D. Rogolino, M. Carcelli, V. Sanna, M. Palomba, L. Naesens, A. Corona, N. Grandi, E. Tramontano, and M. Sechi. International Journal of Molecular Sciences, 2016. 17(8): 1371. PMID[27556447]. PMCID[PMC5000766].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000379609100013">Platelet Factor 4 Inhibits and Enhances HIV-1 Infection in a Concentration-dependent Manner by Modulating Viral Attachment.</a> Parker, Z.F., A.H. Rux, A.M. Riblett, F.H. Lee, L. Rauova, D.B. Cines, M. Poncz, B.S. Sachais, and R.W. Doms. AIDS Research and Human Retroviruses, 2016. 32(7): p. 705-717. ISI[000379609100013].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">24.<a href="http://www.ncbi.nlm.nih.gov/pubmed/27287366">Application of the Huisgen Cycloaddition and &#39;Click&#39; Reaction Toward Various 1,2,3-Triazoles as HIV Non-Nucleoside Reverse Transcriptase Inhibitors.</a> Pribut, N., C.G. Veale, A.E. Basson, W.A. van Otterlo, and S.C. Pelly. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(15): p. 3700-3704. PMID[27287366].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27387828">Isolation and Characterization of a Novel Neutralizing Antibody Targeting the CD4-binding Site of HIV-1 gp120.</a> Qiao, Y., L. Man, Z. Qiu, L. Yang, Y. Sun, and Y. He. Antiviral Research, 2016. 132: p. 252-261. PMID[27387828].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000380260200004">Synthesis and anti-HIV Activity of 5&#39;-Homo-2&#39;,3&#39;-dideoxy-2&#39;,3&#39;-didehydro-4&#39;-selenonucleosides (5&#39;-Homo-4&#39;-Se-d4Ns).</a> Qu, S., G. Kim, J. Yu, P.K. Sahu, Y. Choi, S.D. Naik, and L.S. Jeong. Asian Journal of Organic Chemistry, 2016. 5(6): p. 735-741. ISI[000380260200004].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000380288300252">Anti-nuclear Antibodies in Autoimmune Prone Mice Are Protective against HIV.</a> Schroeder, K.M., P. Strauch, M. Harper, R. Pelanda, M.L. Santiago, and R.M. Torres. Journal of Immunology, 2016. 196(1 Sup.): 47.3. ISI[000380288300252].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27390064">Discovery, Characterization, and Lead Optimization of 7-Azaindole Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Stanton, R.A., X. Lu, M. Detorio, C. Montero, E.T. Hammond, M. Ehteshami, R.A. Domaoal, J.H. Nettles, M. Feraud, and R.F. Schinazi. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(16): p. 4101-4105. PMID[27390064].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27251574">G-quadruplexes with Tetra(ethylene glycol)-modified deoxythymidines Are Resistant to Nucleases and Inhibit HIV-1 Reverse Transcriptase.</a> Tateishi-Karimata, H., T. Muraoka, K. Kinbara, and N. Sugimoto. ChemBioChem, 2016. 17(15): p. 1399-1402. PMID[27251574].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26800650">Rice Endosperm Is Cost-effective for the Production of Recombinant Griffithsin with Potent Activity against HIV.</a> Vamvaka, E., E. Arcalis, K. Ramessar, A. Evans, B.R. O&#39;Keefe, R.J. Shattock, V. Medina, E. Stoger, P. Christou, and T. Capell. Plant Biotechnology Journal, 2016. 14(6): p. 1427-1437. PMID[26800650]. PMCID[PMC4865440].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000380288301214">Anti-alpha(4)beta(7) mAb Prevents MAdCAM-1-mediated Activation and HIV Replication in CD4(+) T Cells.</a> Waliszewski, M., F. Nawaz, C. Cicala, J. Ray, R. Olowojesiku, I. Perrone, D.L. Wei, K. Jelicic, D. van Ryk, A.A. Ansari, A.S. Fauci, and J. Arthos. Journal of Immunology, 2016. 196(1 Sup.): 208.15. ISI[000380288301214].
    <br />
    <b>[WOS]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27022750">Bovine Lactoferrampin, Human Lactoferricin, and Lactoferrin 1-11 Inhibit Nuclear Translocation of HIV Integrase.</a> Wang, W.Y., J.H. Wong, D.T. Ip, D.C. Wan, R.C. Cheung, and T.B. Ng. Applied Biochemistry and Biotechnology, 2016. 179(7): p. 1202-1212. PMID[27022750].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27453773">Effect of Intercalator and Lewis Acid-base Branched Peptide Complex Formation: Boosting Affinity Towards HIV-1 RRE RNA.</a> Wynn, J.E., W.Y. Zhang, D.M. Tebit, L.R. Gray, M.L. Hammarskjold, D. Rekosh, and W.L. Santos. MedChemComm, 2016. 7(7): p. 1436-1440. PMID[27453773]. PMCID[PMC4957400].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27091070">Characterization and in Vitro Activity of a Branched Peptide Boronic acid That Interacts with HIV-1 RRE RNA.</a> Wynn, J.E., W.Y. Zhang, D.M. Tebit, L.R. Gray, M.L. Hammarskjold, D. Rekosh, and W.L. Santos. Bioorganic &amp; Medicinal Chemistry, 2016. 24(17): p. 3947-3952. PMID[27091070]. PMCID[PMC4972714].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27230835">Methylene bisphosphonates as the Inhibitors of HIV RT Phosphorolytic Activity.</a> Yanvarev, D.V., A.N. Korovina, N.N. Usanov, O.A. Khomich, J. Vepsalainen, E. Puljula, M.K. Kukhanova, and S.N. Kochetkov. Biochimie, 2016. 127: p. 153-162. PMID[27230835].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27473095">Alpha-1-antitrypsin Interacts with gp41 to Block HIV-1 Entry into CD4+T Lymphocytes.</a> Zhou, X.Y., Z. Liu, J. Zhang, J.W. Adelsberger, J. Yang, and G.F. Burton. BMC Microbiology, 2016. 16(1): 172. PMID[27473095]. PMCID[PMC4966588].
    <br />
    <b>[PubMed]</b>. HIV_08_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">37. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016128541A1&amp;KC=A1">Preparation of Triazole and Tetrazole Inhibitors of Human DDX3 Helicase as Antiviral Therapeutic Agents.</a> Meyerhans, A., M.-A. Martinez de la Sierra, A. Brai, R. Fazi, C. Tintori, M. Botta, J.-E. Araque, and J. Martinez. Patent. 2016. 2016-EP52990 2016128541: 174pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">38. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=WO&amp;NR=2016130628A1&amp;KC=A1">Griffithsin Mutants.</a> O&#39;Keefe, B.R., T. Moulaei, K.E. Palmer, L.C. Rohan, and J.L. Fuqua. Patent. 2016. 2016-US17267 2016130628: 32pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">39. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160824&amp;CC=EP&amp;NR=3058940A1&amp;KC=A1">Quinoline Derivatives for Use in the Treatment or Prevention of Viral Infection, in Particular HIV Infection.</a> Scherrer, D., A. Garcel, N. Campos, J. Tazi, A. Vautrin, F. Mahuteau-Betzer, R. Najman, and P. Fornarelli. Patent. 2016. 2015-305277 3058940: 39pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">40. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160728&amp;CC=US&amp;NR=2016213647A1&amp;KC=A1">Compositions and Methods for Inhibiting Viral Infection by Administering AR-12 and Its Analogs.</a> Schlesinger, L., J. Yount, A. Zukiwski, S. Proniuk, M.J. Tunon, and K. Zandi. Patent. 2016. 2016-15008306 20160213647: 43pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_08_2016.</p>

    <br />

    <p class="plaintext">41. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160818&amp;CC=US&amp;NR=2016235750A1&amp;KC=A1">Small Molecules Targeting HIV-1 Nef.</a> Smithgall, T.E. Patent. 2016. 2016-15042069 20160235750: 16pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_08_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
