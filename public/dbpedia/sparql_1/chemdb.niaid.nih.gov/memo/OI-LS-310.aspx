

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-310.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SH0q7qD8foyqH0ViQsNK0n2PdMWCadK7HpvbCTRbRAlMnbaOERDiHRAon+bKd7FqiIym4ctrNvC4XVSdVcMBWLDUd7deO9uHxtyL3iSc/qVckr/f88K3UwWcN4s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="73685DAC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-310-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44444   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          Antiviral susceptibility testing of cytomegalovirus: criteria for detecting resistance to antivirals</p>

    <p>          Lawrence, Drew W, Miner, R, and Saleh, E</p>

    <p>          Clin Diagn Virol <b>1993</b>.  1(3): 179-85</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566731&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566731&amp;dopt=abstract</a> </p><br />

    <p>2.         44445   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          A primer on the molecular virology of hepatitis C</p>

    <p>          Moradpour, D and Blum, HE</p>

    <p>          Liver Int <b>2004</b>.  24(6): 519-25</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566499&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15566499&amp;dopt=abstract</a> </p><br />

    <p>3.         44446   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          RpoB gene mutations in rifampin-resistant Mycobacterium tuberculosis strains isolated in the Aegean region of Turkey</p>

    <p>          Avkan, Oguz V, Eroglu, C, Guneri, S, Yapar, N, Oztop, A, Sanic, A, and Yuce, A</p>

    <p>          J Chemother <b>2004</b>.  16(5): 442-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15565909&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15565909&amp;dopt=abstract</a> </p><br />

    <p>4.         44447   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          Novel Nonnucleoside Inhibitor of Hepatitis C Virus RNA-Dependent RNA Polymerase</p>

    <p>          Howe, AY, Bloom, J, Baldick, CJ, Benetatos, CA, Cheng, H, Christensen, JS, Chunduru, SK, Coburn, GA, Feld, B, Gopalsamy, A, Gorczyca, WP, Herrmann, S, Johann, S, Jiang, X, Kimberland, ML, Krisnamurthy, G, Olson, M, Orlowski, M, Swanberg, S, Thompson, I, Thorn, M, Del, Vecchio A, Young, DC, van, Zeijl M, Ellingboe, JW, Upeslacis, J, Collett, M, Mansour, TS, and O&#39;connell, JF</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4813-21</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561861&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561861&amp;dopt=abstract</a> </p><br />

    <p>5.         44448   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          Rapid, Simple In Vivo Screen for New Drugs Active against Mycobacterium tuberculosis</p>

    <p>          Nikonenko, BV, Samala, R, Einck, L, and Nacy, CA</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(12): 4550-5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561824&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15561824&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44449   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          Synthesis of coumarin derivatives with cytotoxic, antibacterial and antifungal activity</p>

    <p>          Khan, KM, Saify, ZS, Khan, MZ, Zia-Ullah, Choudhary, IM, Atta-Ur-Rahman, Perveen, S, Chohan, ZH, and Supuran, CT</p>

    <p>          J Enzyme Inhib Med Chem <b>2004</b>.  19(4): 373-9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15558956&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15558956&amp;dopt=abstract</a> </p><br />

    <p>7.         44450   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          An RNA ligand inhibits hepatitis C virus NS3 protease and helicase activities</p>

    <p>          Fukuda, K, Umehara, T, Sekiya, S, Kunio, K, Hasegawa, T, and Nishikawa, S</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  325(3): 670-675</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541341&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15541341&amp;dopt=abstract</a> </p><br />

    <p>8.         44451   OI-LS-310; PUBMED-OI-11/30/2004</p>

    <p class="memofmt1-2">          Laschiatrion, a new antifungal agent from a Favolaschia species (Basidiomycetes) active against human pathogens</p>

    <p>          Anke, T, Werle, A, Kappe, R, and Sterner, O</p>

    <p>          J Antibiot (Tokyo) <b>2004</b>.  57(8): 496-501</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15515886&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15515886&amp;dopt=abstract</a> </p><br />

    <p>9.         44452   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Phenolic compounds from Baseonema acuminatum leaves: Isolation and antimicrobial activity</p>

    <p>          De Leo, M, Braca, A, De Tommasi, N, Norscia, I, Morelli, L, Battinelli, L, and Mazzanti, G</p>

    <p>          PLANTA MEDICA <b>2004</b>.  70(9): 841-846, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224440700012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224440700012</a> </p><br />

    <p>10.       44453   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Application of the trak-C (TM) HCV core assay for monitoring antiviral activity in HCV replication systems</p>

    <p>          Cagnon, L, Wagaman, P, Bartenschlager, R, Pietschmann, T, Gao, TJ, Kneteman, NM, Tyrrell, DLJ, Bahl, C, Niven, P, Lee, S, and Simmen, KA</p>

    <p>          JOURNAL OF VIROLOGICAL METHODS <b>2004</b>.  118(1): 23-31, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224496700004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224496700004</a> </p><br />

    <p>11.       44454   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          Well diffusion for antifungal susceptibility testing</p>

    <p>          Magaldi, S, Mata-Essayag, S, Hartung de Capriles, C, Perez, C, Colella, MT, Olaizola, Carolina, and Ontiveros, Yudith</p>

    <p>          International Journal of Infectious Diseases <b>2004</b>.  8(1): 39-45</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B7CPT-4B1XS0Y-4/2/aca6f19797576734c715bb97c1467b9e">http://www.sciencedirect.com/science/article/B7CPT-4B1XS0Y-4/2/aca6f19797576734c715bb97c1467b9e</a> </p><br />

    <p>12.       44455   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          HCVNS5b RNA-dependent RNA polymerase inhibitors: From alpha,gamma-diketoacids to 4,5-dihydroxypyrimidine- or 3-methyl-5-hydroxypyrimidinonecarboxylic acids. Design and synthesis</p>

    <p>          Summa, V, Petrocchi, A, Matassa, VG, Taliani, M, Laufer, R, De, Francesco R, Altamura, S, and Pace, P</p>

    <p>          JOURNAL OF MEDICINAL CHEMISTRY <b>2004</b>.  47(22): 5336-5339, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224495700002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224495700002</a> </p><br />

    <p>13.       44456   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Echinocandin antifungal drugs</p>

    <p>          Hospenthal, DR </p>

    <p>          INFECTIONS IN MEDICINE <b>2004</b>.  21(10): 476-478, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224479400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224479400006</a> </p><br />

    <p>14.       44457   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Inhibition of hepatitis viral replication by siRNA</p>

    <p>          Wu, J and Nandamuri, KM</p>

    <p>          EXPERT OPINION ON BIOLOGICAL THERAPY <b>2004</b>.  4(10): 1649-1659, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224515300010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224515300010</a> </p><br />

    <p>15.       44458   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          Antiviral compounds from traditional Chinese medicines Galla Chinese as inhibitors of HCV NS3 protease</p>

    <p>          Duan, Deliang, Li, Zhengquan, Luo, Hongpeng, Zhang, Wei, Chen, Lirong, and Xu, Xiaojie</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(24): 6041-6044</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DSW2KD-6/2/c4243af54c5abf8986b6b85fd48146bb">http://www.sciencedirect.com/science/article/B6TF9-4DSW2KD-6/2/c4243af54c5abf8986b6b85fd48146bb</a> </p><br />

    <p>16.       44459   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Quality, not quantity: The role of natural products and chemical proteomics in modern drug discovery</p>

    <p>          Piggott, AM and Karuso, P</p>

    <p>          COMBINATORIAL CHEMISTRY &amp; HIGH THROUGHPUT SCREENING <b>2004</b>.  7(7): 607-630, 24</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224454100002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224454100002</a> </p><br />

    <p>17.       44460   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Tuberculosis and opportunistic infections: Relevance to biologic agents</p>

    <p>          Bieber, J and Kavanaugh, A</p>

    <p>          CLINICAL AND EXPERIMENTAL RHEUMATOLOGY <b>2004</b>.  22(5): S126-S133, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224406600020">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224406600020</a> </p><br />

    <p>18.       44461   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          A new phenanthrene glycoside and other constituents from Dioscorea opposita</p>

    <p>          Sautour, M, Mitaine-Offer, AC, Miyamoto, T, Wagner, H, and Lacaille-Dubois, MA</p>

    <p>          CHEMICAL &amp; PHARMACEUTICAL BULLETIN <b>2004</b>.  52(10): 1235-1237, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224515000019">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224515000019</a> </p><br />
    <br clear="all">

    <p>19.       44462   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          Synthesis and antimycobacterial activity of 2-substituted halogenobenzimidazoles</p>

    <p>          Kazimierczuk, Z, Andrzejewska, M, Kaustova, J, and Klimesova, V</p>

    <p>          European Journal of Medicinal Chemistry <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6VKY-4DW90N7-2/2/cfea75f679fef0f83a2e5646b1fc8090">http://www.sciencedirect.com/science/article/B6VKY-4DW90N7-2/2/cfea75f679fef0f83a2e5646b1fc8090</a> </p><br />

    <p>20.       44463   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          The non-nucleoside antiviral, BAY 38-4766, protects against cytomegalovirus (CMV) disease and mortality in immunocompromised guinea pigs</p>

    <p>          Schleiss, Mark R, Bernstein, David I, McVoy, Michael A, Stroup, Greg, Bravo, Fernando, Creasy, Blaine, McGregor, Alistair, Henninger, Kristin, and Hallenberger, Sabine</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DT2HJ9-1/2/c46250f0a975030af89eacd6ce45e7a3">http://www.sciencedirect.com/science/article/B6T2H-4DT2HJ9-1/2/c46250f0a975030af89eacd6ce45e7a3</a> </p><br />

    <p>21.       44464   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          Enhancement of antiviral activity against hepatitis C virus in vitro by interferon combination therapy</p>

    <p>          Okuse, Chiaki, Rinaudo, Jo Ann, Farrar, Kristine, Wells, Frances, and Korba, Brent E</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DS7NWT-1/2/2cfcb1e93455f408979d6ef6c847338e">http://www.sciencedirect.com/science/article/B6T2H-4DS7NWT-1/2/2cfcb1e93455f408979d6ef6c847338e</a> </p><br />

    <p>22.       44465   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Sulfone and phosphinic acid analogs of decaprenolphosphoarabinose as potential anti-tuberculosis agents</p>

    <p>          Centrone, CA and Lowary, TL</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY <b>2004</b>.  12(21): 5495-5503, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224522700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224522700001</a> </p><br />

    <p>23.       44466   OI-LS-310; EMBASE-OI-11/30/2004</p>

    <p class="memofmt1-2">          Mycobacterium avium complex in patients with HIV infection in the era of highly active antiretroviral therapy</p>

    <p>          Karakousis, Petros C, Moore, Richard D, and Chaisson, Richard E</p>

    <p>          The Lancet Infectious Diseases <b>2004</b>.  4(9): 557-565</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W8X-4D5MS93-S/2/a929216326f45a7a1820d37d0363c453">http://www.sciencedirect.com/science/article/B6W8X-4D5MS93-S/2/a929216326f45a7a1820d37d0363c453</a> </p><br />

    <p>24.       44467   OI-LS-310; WOS-OI-11/21/2004</p>

    <p class="memofmt1-2">          Crystallization and preliminary X-ray crystallographic analysis of chorismate synthase from Mycobacterium tuberculosis</p>

    <p>          Dias, MVB, Ely, F, Canduri, F, Pereira, JF, Basso, LA, Palma, MS, de, Azevedo WF, and Santos, DS</p>

    <p>          ACTA CRYSTALLOGRAPHICA SECTION D-BIOLOGICAL CRYSTALLOGRAPHY <b>2004</b>.  60: 2003-2005, 3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224595200012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224595200012</a> </p><br />
    <br clear="all">

    <p>25.       44468   OI-LS-310; WOS-OI-11/28/2004</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of some netropsin analogues</p>

    <p>          Khalaf, AI, Ebrahimabadi, AH, Drummond, AJ, Anthony, NG, Mackay, SP, Suckling, CJ, and Waigh, RD</p>

    <p>          ORGANIC &amp; BIOMOLECULAR CHEMISTRY <b>2004</b>.  2(21): 3119-3127, 9</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224703400013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224703400013</a> </p><br />

    <p>26.       44469   OI-LS-310; WOS-OI-11/28/2004</p>

    <p class="memofmt1-2">          Viral evolution and interferon resistance of hepatitis C virus RNA replication in a cell culture model</p>

    <p>          Sumpter, R, Wang, CF, Foy, E, Loo, YM, and Gale, M</p>

    <p>          JOURNAL OF VIROLOGY <b>2004</b>.  78(21): 11591-11604, 14</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224540900013</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
