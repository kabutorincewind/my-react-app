

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-04-25.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="xmo3IIw1BO3xrrT1m6tQHU3ATQo7fpxPjgLqeE/m7uw9DrY+a9a2OQcTJbzBZUATCRjCUU6dJsd3W1JuS8qtEKmNCZ7oKU7ohyNEpPcfo2pSa93U11nnWK5eLTY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0ED486FB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: April 12 - April 25, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23489634">Synthesis and Antimicrobial Activity of Polyhalo isophthalonitrile Derivatives.</a> Huang, C., S.J. Yan, N.Q. He, Y.J. Tang, X.H. Wang, and J. Lin. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(8): p. 2399-2403. PMID[23489634].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23594040">Hydroxyaldimines as Potent in Vitro Anticryptococcal Agents.</a> Magalhaes, T.F., C.M. da Silva, A. de Fatima, D.L. da Silva, V.M. L, B.M. CV, R.B. Alves, A.L. Ruiz, G.B. Longato, J.E. de Carvalho, and M.A. de Resende-Stoianoff. Letters in Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23594040].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23595615">Otomycosis in the North of Iran: Common Pathogens and Resistance to Antifungal Agents.</a> Nemati, S., R. Hassanzadeh, S. Khajeh Jahromi, and A. Delkhosh Nasrollah Abadi. European Archives of Oto-Rhino-Laryngology, 2013. <b>[Epub ahead of print]</b>. PMID[23595615].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23603258">Antimicrobial Activity of Pleurocidin Is Retained in Plc-2, a C-Terminal 12-Aminoacid Fragment.</a> Souza, A.L., P. Diaz-Dellavalle, A. Cabrera, P. Larranaga, M. Dalla-Rizza, and S.G. De-Simone. Peptides, 2013. <b>[Epub ahead of print]</b>. PMID[23603258].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23534408">l-Tyrosinatonickel(II) Complex: Synthesis and Structural, Spectroscopic, Magnetic, and Biological Properties of 2{[Ni(l-Tyr)2(bpy)]}.3H2O.CH3OH.</a> Wojciechowska, A., A. Gagor, M. Duczmal, Z. Staszak, and A. Ozarowski. Inorganic Chemistry, 2013. 52(8): p. 4360-4371. PMID[23534408].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23593945">Contribution of Moxifloxacin or Levofloxacin in Second-Line Regimens with or without Continuation of Pyrazinamide in Murine Tuberculosis.</a> Ahmad, Z., S. Tyagi, A. Minkowski, C.A. Peloquin, J.H. Grosset, and E.L. Nuermberger. American Journal of Respiratory and Critical Care Medicine, 2013. <b>[Epub ahead of print]</b>. PMID[23593945].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22574627">Antibacterial Constituents of Three Cameroonian Medicinal Plants: Garcinia nobilis, Oricia suaveolens and Balsamocitrus camerunensis.</a> Fouotsa, H., A.T. Mbaveng, C.D. Mbazoa, A.E. Nkengfack, S. Farzana, C.M. Iqbal, J.J. Marion Meyer, N. Lall, and V. Kuete. BMC Complementary and Alternative Medicine, 2013. 13(1): p. 81. PMID[23574627].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />  

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23600706">Identification of Compounds with Potential Antibacterial Activity against Mycobacterium through Structure-Based Drug Screening.</a> Kinjo, T., Y. Koseki, M. Kobayashi, A. Yamada, K. Morita, K. Yamaguchi, R. Tsurusawa, G. Gulten, H. Komatsu, H. Sakamoto, J.C. Sacchettini, M. Kitamura, and S. Aoki. Journal of Chemical Information and Modeling, 2013. <b>[Epub ahead of print]</b>. PMID[23600706].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23474387">Synthesis and Biological Evaluation of Trans 6-Methoxy-1,1-dimethyl-2-phenyl-3-aryl-2,3-dihydro-1H-inden-4-yloxyalkylamine Derivatives against Drug Susceptible, Non-Replicating M. tuberculosis H37Rv and Clinical Multidrug Resistant Strains.</a> Kumar, S., A.P. Dwivedi, V.K. Kashyap, A.K. Saxena, A.K. Dwivedi, R. Srivastava, and D.P. Sahu. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(8): p. 2404-2407. PMID[23474387].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23591156">Mycobacterium tuberculosis Is Resistant to Streptolydigin.</a> Speer, A., J.L. Rowland, and M. Niederweis. Tuberculosis, 2013. <b>[Epub ahead of print]</b>. PMID[23591156].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23603838">A Mycobacteriophage-Derived Trehalose-6,6&#39;-dimycolate-binding Peptide Containing Both Antimycobacterial and Anti-inflammatory Abilities.</a> Wei, L., J. Wu, H. Liu, H. Yang, M. Rong, D. Li, P. Zhang, J. Han, and R. Lai. FASEB Journal, 2013. <b>[Epub ahead of print]</b>. PMID[23603838].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23609622">New Amide Derivatives of Quinoxaline 1,4-di-N-oxide with Leishmanicidal and Antiplasmodial Activities.</a> Barea, C., A. Pabon, S. Perez-Silanes, S. Galiano, G. Gonzalez, A. Monge, E. Deharo, and I. Aldana. Molecules, 2013. 18(4): p. 4718-4727. PMID[23609622].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23595058">Antimalarial Activity of Axidjiferosides, New beta-Galactosylceramides from the African Sponge Axinyssa Djiferi.</a> Farokhi, F., P. Grellier, M. Clement, C. Roussakis, P.M. Loiseau, E. Genin-Seward, J.M. Kornprobst, G. Barnathan, and G. Wielgosz-Collin. Marine Drugs, 2013. 11(4): p. 1304-1315. PMID[23595058].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23612203">Ex Vivo Responses of Plasmodium falciparum Clinical Isolates to Conventional and New Antimalarial Drugs in Niger.</a> Issaka, M., A. Salissou, I. Arzika, J. Guillebaud, A. Maazou, S. Specht, H. Zamanka, and T. Fandeur. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23612203].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23612422">Antiprotozoal Screening and Cytotoxicity of Extracts and Fractions from the Leaves, Stem Bark and Root Bark of Alstonia congensis.</a> Lumpu, S.L., C.M. Kikueta, M.E. Tshodi, A.P. Mbenza, O.K. Kambu, B.M. Mbamu, P. Cos, L. Maes, S. Apers, L. Pieters, and R.K. Cimanga. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23612422].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />   

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23499502">Borrelidin Analogues with Antimalarial Activity: Design, Synthesis and Biological Evaluation against Plasmodium falciparum Parasites.</a> Sugawara, A., T. Tanaka, T. Hirose, A. Ishiyama, M. Iwatsuki, Y. Takahashi, K. Otoguro, S. Omura, and T. Sunazuka. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(8): p. 2302-2305. PMID[23499502].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23587962">Modulation of PF10_0355 (MSPDBL2) Alters Plasmodium falciparum Response to Antimalarial Drugs.</a> Van Tyne, D., A.D. Uboldi, J. Healer, A.F. Cowman, and D.F. Wirth. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23587962].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23591483">Evaluation of Protective Effect of Multiantigenic DNA Vaccine Encoding MIC3 and ROP18 Antigen Segments of Toxoplasma Gondii in Mice.</a> Qu, D., J. Han, and A. Du. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23591483].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0412-042513.</p><br />

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316242700051">Synthesis and Antifungal Activity of Some s-Mercaptotriazolobenzothiazolyl Amino Acid Derivatives.</a> Aboelmagd, A., I.A.I. Ali, E.M.S. Salem, and M. Abdel-Razik. European Journal of Medicinal Chemistry, 2013. 60: p. 503-511. ISI[000316242700051].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316385200003">Chalcone-Derived Thiosemicarbazones and Their Zinc(II) and Gallium(III) Complexes: Spectral Studies and Antimicrobial Activity.</a> Da Silva, J.G., C.C.H. Perdigao, N.L. Speziali, and H. Beraldo. Journal of Coordination Chemistry, 2013. 66(3): p. 385-401. ISI[000316385200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316119100010">Inhibitory and Fungicidal Effects of Antifungal Drugs against Aspergillus Species in the Presence of Serum.</a> Elefanti, A., J.W. Mouton, K. Krompa, R. Al-Saigh, P.E. Verweij, L. Zerva, and J. Meletiadis. Antimicrobial Agents and Chemotherapy, 2013. 57(4): p. 1625-1631. ISI[000316119100010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316008300025">Synthesis, Characterization, and Biological Evaluation of New Oxime-Phosphazenes.</a> Koran, K., A. Ozkaya, F. Ozen, E. Cil, and M. Arslan. Research on Chemical Intermediates, 2013. 39(3): p. 1109-1124. ISI[000316008300025].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316172000001">In Vitro Antimicrobial Activity of Extracts from Plants Used Traditionally in South Africa to Treat Tuberculosis and Related Symptoms.</a> Madikizela, B., A.R. Ndhlala, J.F. Finnie, and J. Van Staden. Evidence-Based Complementary and Alternative Medicine, 2013. ISI[000316172000001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316064700015">Synthesis, Photophysical Property Study of Novel Fluorescent 4-(1,3-Benzoxazol-2-yl)-2-phenylnaphtho 1,2-d 1,3 Oxazole Derivatives and Their Antimicrobial Activity.</a> Phatangare, K.R., B.N. Borse, V.S. Padalkar, V.S. Patil, V.D. Gupta, P.G. Umape, and N. Sekar. Journal of Chemical Sciences, 2013. 125(1): p. 141-151. ISI[000316064700015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316546400022">Ability of Innate Defence Regulator Peptides IDR-1002, IDR-HH2 and IDR-1018 to Protect against Mycobacterium tuberculosis Infections in Animal Models.</a> Rivas-Santiago, B., J.E. Castaneda-Delgado, C.E.R. Santiago, M. Waldbrook, I. Gonzalez-Curiel, J.C. Leon-Contreras, J.A. Enciso-Moreno, V. del Villar, J. Mendez-Ramos, R.E.W. Hancock, and R. Hernandez-Pando. Plos One, 2013. 8(3). ISI[000316546400022].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:00031637220005">Solvent Free Microwave Assisted Synthesis and Evaluation of Potent Antimicrobial Activity of 1,11H-Pyrimido 4,5-a carbazol-2-ones, 1,11H-pyrimido 4,5-a carbazol-2-thiones and Pyrazolo 3,4-a carbazoles.</a> Velmurugan, R., A.V. Vijayasankar, and M. Sekar. Indian Journal of Chemistry Section B-Organic Chemistry Including Medicinal Chemistry, 2013. 52(3): p. 414-421. ISI[000316372200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000316064700013">New Bispyrazoline Derivatives Built around Aliphatic Chains: Synthesis, Characterization and Antimicrobial Studies.</a> Yusuf, M. and P. Jain. Journal of Chemical Sciences, 2013. 125(1): p. 117-127. ISI[000316064700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0412-042513.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
