

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-125.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ceGLsyfZosMu14APzdHJRUbIowdtAbVacNsc+DMRtOA1Van6q66IBS3AnH9rw50GY600p7I6UrMsw4+r6sd3YJPSSWEP4F7R184+0ocd9dBb9eOIzr7uUChpv2w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2A6D8601" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-125-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58709   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Antiviral Effect of Artemisinin from Artemisia annua against a Model Member of the Flaviviridae Family, the Bovine Viral Diarrhoea Virus (BVDV)</p>

    <p>          Romero, MR, Serrano, MA, Vallejo, M, Efferth, T, Alvarez, M, and Marin, JJ</p>

    <p>          Planta Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16902856&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16902856&amp;dopt=abstract</a> </p><br />

    <p>2.     58710   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis of 2-bromomethyl-3-hydroxy-2-hydroxymethyl-propyl pyrimidine and theophylline nucleosides under microwave irradiation. Evaluation of their activity against hepatitis B virus</p>

    <p>          Ashry, ES, Rashed, N, Abdel-Rahman, A, Awad, LF, and Rasheed, HA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(8): 925-39</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901823&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901823&amp;dopt=abstract</a> </p><br />

    <p>3.     58711   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          3&#39;-Fluoro-3&#39;-deoxy-5&#39;-noraristeromycin derivatives: Synthesis and antiviral analysis</p>

    <p>          Roy, Atanu, Serbessa, Tesfaye, and Schneller, Stewart W</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(14): 4980-4986</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     58712   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p><b>          Simple synthesis of novel acyclic (e)-bromovinyl nucleosides as potential antiviral agents</b> </p>

    <p>          Kim, JW and Hong, JH</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(8): 879-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901820&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16901820&amp;dopt=abstract</a> </p><br />

    <p>5.     58713   DMID-LS-125; WOS-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Development of a Synthetic Process Towards a Hepatitis C Polymerase Inhibitor</p>

    <p>          Camp, D. <i>et al.</i></p>

    <p>          Organic Process Research &amp; Development <b>2006</b>.  10(4): 814-821</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239167500018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239167500018</a> </p><br />

    <p>6.     58714   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Inhibition of hcv replication in HCV replicon by shRNAs</p>

    <p>          Hamazaki, H, Takahashi, H, Shimotohno, K, Miyano-Kurosaki, N, and Takaku, H</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2006</b>.  25(7): 801-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16898418&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16898418&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     58715   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Efficacy of topical cobalt chelate CTC-96 against adenovirus in a cell culture model and against adenovirus keratoconjunctivitis in a rabbit model</p>

    <p>          Epstein Seth P, Pashinsky Yevgenia Y, Gershon David, Winicov Irene, Srivilasa Charlie, Kristic Katarina J, and Asbell Penny A</p>

    <p>          BMC ophthalmology [electronic resource] <b>2006</b>.  6: 22.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     58716   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Development of a red-shifted fluorescence-based assay for SARS-coronavirus 3CL protease: identification of a novel class of anti-SARS agents from the tropical marine sponge Axinella corrugata</p>

    <p>          Hamill, P, Hudson, D, Kao, RY, Chow, P, Raj, M, Xu, H, Richer, MJ, and Jean, F</p>

    <p>          Biol Chem <b>2006</b>.  387(8): 1063-74</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895476&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16895476&amp;dopt=abstract</a> </p><br />

    <p>9.     58717   DMID-LS-125; WOS-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Multifaceted Antiviral Actions of Apobec3 Cytidine Deaminases</p>

    <p>          Chiu, Y. and Greene, W.</p>

    <p>          Trends in Immunology <b>2006</b>.  27(6): 291-297</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238794100008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238794100008</a> </p><br />

    <p>10.   58718   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Bleomycin is a Potent Small-Molecule Inhibitor of Hepatitis C Virus Replication</p>

    <p>          Rakic, B, Brulotte, M, Rouleau, Y, Belanger, S, and Pezacki, JP</p>

    <p>          Chembiochem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888741&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16888741&amp;dopt=abstract</a> </p><br />

    <p>11.   58719   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis, Crystal Structure, Structure-Activity Relationships, and Antiviral Activity of a Potent SARS Coronavirus 3CL Protease Inhibitor</p>

    <p>          Yang, S, Chen, SJ, Hsu, MF, Wu, JD, Tseng, CT, Liu, YF, Chen, HC, Kuo, CW, Wu, CS, Chang, LW, Chen, WC, Liao, SY, Chang, TY, Hung, HH, Shr, HL, Liu, CY, Huang, YA, Chang, LY, Hsu, JC, Peters, CJ, Wang, AH, and Hsu, MC</p>

    <p>          J Med Chem <b>2006</b>.  49(16): 4971-4980</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16884309&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   58720   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis, conformational characteristics and anti-influenza virus A activity of some 2-adamantylsubstituted azacycles</p>

    <p>          Setaki, D, Tataridis, D, Stamatiou, G, Kolocouris, A, Foscolos, GB, Fytas, G, Kolocouris, N, Padalko, E, Neyts, J, and Clercq, ED</p>

    <p>          Bioorg Chem <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16879857&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16879857&amp;dopt=abstract</a> </p><br />

    <p>13.   58721   DMID-LS-125; WOS-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Future Therapies for Hepatitis C</p>

    <p>          Pawlotsky, J. and Gish, R.</p>

    <p>          Antiviral Therapy <b>2006</b>.  11(4): 397-408</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238744600001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000238744600001</a> </p><br />

    <p>14.   58722   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Quinazoline derivatives as antiviral agents</p>

    <p>          Cockerill, George Stuart, Flack, Stephen Sean, Mathews, Neil, and Salter, James Iain</p>

    <p>          PATENT:  WO <b>2006079833</b>  ISSUE DATE:  20060803</p>

    <p>          APPLICATION: 2006  PP: 27pp.</p>

    <p>          ASSIGNEE:  (Arrow Therapeutics Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   58723   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Valopicitabine: anti-hepatitis C virus drug RNA-directed RNA polymerase (NS5B) inhibitor</p>

    <p>          Sorbera, LA, Castaner, J, and Leeson, PA</p>

    <p>          Drugs of the Future <b>2006</b>.  31(4): 320-324</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   58724   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          The molecular biology of coronaviruses</p>

    <p>          Masters, PS</p>

    <p>          Adv Virus Res <b>2006</b>.  66: 193-292</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16877062&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16877062&amp;dopt=abstract</a> </p><br />

    <p>17.   58725   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis of stilbene derivatives with inhibition of SARS coronavirus replication</p>

    <p>          Li, YQ, Li, ZL, Zhao, WJ, Wen, RX, Meng, QW, and Zeng, Y</p>

    <p>          Eur J Med Chem  <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875760&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58726   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p><b>          Hemin potentiates the anti-hepatitis C virus activity of the antimalarial drug artemisinin</b> </p>

    <p>          Paeshuyse, J, Coelmont, L, Vliegen, I, Hemel, JV, Vandenkerckhove, J, Peys, E, Sas, B, Clercq, ED, and Neyts, J</p>

    <p>          Biochem Biophys Res Commun <b>2006</b>.  348(1): 139-44</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875675&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16875675&amp;dopt=abstract</a> </p><br />

    <p>19.   58727   DMID-LS-125; WOS-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Current Therapy of Hepatitis C</p>

    <p>          De Knegt, R.</p>

    <p>          Scandinavian Journal of Gastroenterology <b>2006</b>.  41: 65-69</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239145100012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239145100012</a> </p><br />

    <p>20.   58728   DMID-LS-125; PUBMED-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Prevention and Control of Influenza: recommendations of the Advisory Committee on Immunization Practices (ACIP)</p>

    <p>          Smith, NM, Bresee, JS, Shay, DK, Uyeki, TM, Cox, NJ, and Strikas, RA</p>

    <p>          MMWR Recomm Rep <b>2006</b>.  55(RR-10): 1-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16874296&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16874296&amp;dopt=abstract</a> </p><br />

    <p>21.   58729   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Identification of Compounds with Anti-West Nile Virus Activity</p>

    <p>          Goodell, John R, Puig-Basagoiti, Francesc, Forshey, Brett M, Shi, Pei-Yong, and Ferguson, David M</p>

    <p>          Journal of Medicinal Chemistry <b>2006</b>.  49(6): 2127-2137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>22.   58730   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Synthesis and antiherpetic activity of N-(3-amino-1-adamantyl)calix[4]arenes</p>

    <p>          Motornaya, AE, Alimbarova, LM, Shokova, EA, and Kovalev, VV</p>

    <p>          Pharmaceutical Chemistry Journal <b>2006</b>.  40(2): 68-72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   58731   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of pyrimidinyl based heterocycles and their use in the treatment of inflammation, and as antiviral and therapeutic agents</p>

    <p>          Hong, Fang-Tsao, Liao, Hongyu, Lopez, Patricia, Tadesse, Seifu, and Tamayo, Nuria A</p>

    <p>          PATENT:  WO <b>2006069258</b>  ISSUE DATE:  20060629</p>

    <p>          APPLICATION: 2005  PP: 125 pp.</p>

    <p>          ASSIGNEE:  (Amgen Inc, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>24.   58732   DMID-LS-125; WOS-DMID-8/14/2006</p>

    <p class="memofmt1-2">          Preparation of a Fluorous Protecting Group and Its Application to the Chemoenzymatic Synthesis of Sialidase Inhibitor</p>

    <p>          Ikeda, K., Mori, H., and Sato, M.</p>

    <p>          Chemical Communications <b>2006</b>.(29): 3093-3094</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239137500012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000239137500012</a> </p><br />

    <p>25.   58733   DMID-LS-125; SCIFINDER-DMID-8/14/2006</p>

    <p class="memofmt1-2">          anti-coronavirus compounds</p>

    <p>          Wong, Chi-Huey, Wu, Chung-Yi, and Jan, Jia-Tsrong</p>

    <p>          PATENT:  WO <b>2006073456</b>  ISSUE DATE:  20060713</p>

    <p>          APPLICATION: 2005  PP: 33 pp.</p>

    <p>          ASSIGNEE:  (Academia Sinica, Taiwan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
