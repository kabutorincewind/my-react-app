

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-09-11.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X0ug9stk+V9KVy08t0s3PhWkzSEZr3sKxGD2gx/Ik4uLsnVFsMNmLmtS/LIl/E9+pIHcIB1U392Euu4tzBB8DWJ3xfUbqy3jd1FKEF/3oKr7nyC+WkGBUFxO6OY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AFFF4EDF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 29 - September 11, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24835118">Human microRNA hsa-miR-1231 Suppresses Hepatitis B Virus Replication by Targeting Core mRNA.</a> Kohno, T., M. Tsuge, E. Murakami, N. Hiraga, H. Abe, D. Miki, M. Imamura, H. Ochi, C.N. Hayes, and K. Chayama. Journal of Viral Hepatitis, 2014. 21(9): p. e89-e97. PMID[24835118].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25009077">In Vitro and in Vivo anti-Hepatitis B Virus Activities of the Lignan Niranthin Isolated from Phyllanthus niruri L.</a> Liu, S., W. Wei, K. Shi, X. Cao, M. Zhou, and Z. Liu. Journal of Ethnopharmacology, 2014. 155(2): p. 1061-1067. PMID[25009077].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25156438">Successful Treatment of Hepatitis B Virus-associated Membranous Nephropathy with Entecavir and Immunosuppressive Agents.</a> Ochi, A., E. Ishimura, M. Ichii, Y. Ohno, S. Nakatani, I. Kobayashi, H. Shima, A. Tsuda, K. Shidara, K. Mori, A. Tamori, and M. Inaba. Nephrology, 2014. 19(9): p. 595-596. PMID[25156438].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25180507">Differential Binding of Tenofovir and Adefovir to Reverse Transcriptase of Hepatitis B Virus.</a> van Hemert, F.J., B. Berkhout, and H.L. Zaaijer. PLoS One, 2014. 9(9): p. e106324. PMID[25180507].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25127104">A New Series of HAPs as anti-HBV Agents Targeting at Capsid Assembly.</a> Yang, X.Y., X.Q. Xu, H. Guan, L.L. Wang, Q. Wu, G.M. Zhao, and S. Li. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4247-4249. PMID[25127104].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">6<a href="http://www.ncbi.nlm.nih.gov/pubmed/25103601">. Structure-activity Studies of (-)-Epigallocatechin Gallate Derivatives as HCV Entry Inhibitors.</a> Bhat, R., A.T. Adam, J.J. Lee, G. Deloison, Y. Rouille, K. Seron, and D.P. Rotella. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4162-4165. PMID[25103601].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25101523">Cyclohexane-fused Octahydroquinolizine Alkaloids from Myrioneuron faberi with Activity against Hepatitis C Virus.</a> Cao, M.M., Y. Zhang, X.H. Li, Z.G. Peng, J.D. Jiang, Y.C. Gu, Y.T. Di, X.N. Li, D.Z. Chen, C.F. Xia, H.P. He, S.L. Li, and X.J. Hao. Journal of Organic Chemistry, 2014. 79(17): p. 7945-7950. PMID[25101523].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25019406">Benzhydrylpiperazine Compounds Inhibit Cholesterol-dependent Cellular Entry of Hepatitis C Virus.</a> Chamoun-Emanuelli, A.M., E.I. Pecheur, and Z. Chen. Antiviral Research, 2014. 109: p. 141-148. PMID[25019406].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25074011">Ombitasvir: A Potent Pan-genotypic Inhibitor of NS5A for the Treatment of Hepatitis C Virus Infection.</a> Gentile, I., A.R. Buonomo, and G. Borgia. Expert Review of Anti-infective Therapy, 2014. 12(9): p. 1033-1043. PMID[25074011].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25086684">Synthesis and Inhibitory Activity on Hepatitis C Virus RNA Replication of 4-(1,1,1,3,3,3-Hexafluoro-2-hydroxy-2-propyl)aniline Analogs.</a> Matsuno, K., Y. Ueda, M. Fukuda, K. Onoda, M. Waki, M. Ikeda, N. Kato, and H. Miyachi. Bioorganic &amp; Medicinal Chemistry Letters, 2014. 24(17): p. 4276-4280. PMID[25086684].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25079672">Activity of Core-modified 10-23 DNAzymes against HCV.</a> Robaldo, L., A. Berzal-Herranz, J.M. Montserrat, and A.M. Iribarren. ChemMedChem, 2014. 9(9): p. 2172-2177. PMID[25079672].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24982066">Hepatitis C Virus Genotype 5a Subgenomic Replicons for Evaluation of Direct-acting Antiviral Agents.</a> Wose Kinge, C.N., C. Espiritu, N. Prabdial-Sing, N.P. Sithebe, M. Saeed, and C.M. Rice. Antimicrobial Agents and Chemotherapy, 2014. 58(9): p. 5386-5394. PMID[24982066].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p>

    <h2>Herpes Simplex Virus 1      </h2>

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24995383">Oligonucleotides Designed to Inhibit TLR9 Block Herpes Simplex Virus Type 1 Infection at Multiple Steps.</a> Sauter, M.M., J.J. Gauger, and C.R. Brandt. Antiviral Research, 2014. 109: p. 83-96. PMID[24995383].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0829-091114.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000339820500002">A Novel Human Radixin Peptide Inhibits Hepatitis C Virus Infection at the Level of Cell Entry.</a> Bukong, T.N., K. Kodys, and G. Szabo. International Journal of Peptide Research and Therapeutics, 2014. 20(3): p. 269-276. ISI[000339820500002].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0809-091114.</p><br /> 

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340345900012">Recombinant Human L-Ficolin Directly Neutralizes Hepatitis C Virus Entry.</a> Hamed, M.R., R.J.P. Brown, C. Zothner, R.A. Urbanowicz, C.P. Mason, A. Krarup, C.P. McClure, W.L. Irving, J.K. Ball, M. Harris, T.P. Hickling, and A.W. Tarr. Journal of Innate Immunity, 2014. 6(5): p. 676-684. ISI[000340345900012].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0809-091114.</p><br /> 

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000340553200005">Selective Inhibitor of Histone Deacetylase 6 (Tubastatin A) Suppresses Proliferation of Hepatitis C Virus Replicon in Culture of Human Hepatocytes.</a> Kozlov, M.V., A.A. Kleymenova, K.A. Konduktorov, A.Z. Malikova, and S.N. Kochetkov. Biochemistry (Moscow), 2014. 79(7): p. 637-642. ISI[000340553200005].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0829-091114.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
