

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Mycobacterium_2015-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pt1JoY8GPgp1OZLfVDRguedI6DF6TkB9Ma3G+QdnRb0xqr1wsvVllCXGupcZP1Rs8dJBJNQGJ4nn/jSozP9//6AoDH70IeCrpyqJaftqIvkNk28cEujLqXUMW7w=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B517B2A7" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Mycobacterium Citations List: October, 2015</h1>

    <h2>PubMed Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26400221" target="_blank">Quantitative Structure-Activity Relationship of Molecules Constituent of Different Essential Oils with Antimycobacterial Activity against Mycobacterium tuberculosis and Mycobacterium bovis.</a> Andrade-Ochoa, S., G. Nevárez-Moorillón, L. Sánchez-Torres, M. Villanueva-García, B. Sánchez-Ramírez, L. Rodríguez-Valdez, and B. Rivera-Chavira. BMC Complementary and Alternative Medicine, 2015. 15(1): p. 332. PMID[26400221]. PMC[4579641]. 
    <br />
    <b>[PubMed].</b> TB_10_2015. 
    <br />
    <br />
    2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26470029">A Nano-MgO and Ionic Liquid-catalyzed &#39;Green&#39; Synthesis Protocol for the Development of Adamantyl-imidazolo-thiadiazoles as Anti-tuberculosis Agents Targeting Sterol 14alpha-Demethylase (Cyp51).</a> Anusha, S., B. Cp, C.D. Mohan, J. Mathai, S. Rangappa, S. Mohan, Chandra, S. Paricharak, L. Mervin, J.E. Fuchs, M. M, A. Bender, Basappa, and K.S. Rangappa. PlosOne, 2015. 10(10): p. e0139798. PMID[26470029].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26430796">Stable Tricyclic Antitubercular Ozonides Derived from Artemisinin.</a> Chaudhary, S., V. Sharma, P.K. Jaiswal, A.N. Gaikwad, S.K. Sinha, S.K. Puri, A. Sharon, P.R. Maulik, and V. Chaturvedi. Organic Letters, 2015. 17(20): p. 4948-4951. PMID[26430796].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26506338">Antimycobacterial Activities of Endolysins Derived from a Mycobacteriophage, BTCU-1.</a> Lai, M.J., C.C. Liu, S.J. Jiang, P.C. Soo, M.H. Tu, J.J. Lee, Y.H. Chen, and K.C. Chang. Molecules, 2015. 20(10): p. 19277-19290. PMID[26506338].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26440283">Mycobacterium Tuberculosis IMPDH in Complexes with Substrates, Products and Antitubercular Compounds.</a> Makowska-Grzyska, M., Y. Kim, S.K. Gorla, Y. Wei, K. Mandapati, M. Zhang, N. Maltseva, G. Modi, H.I. Boshoff, M. Gu, C. Aldrich, G.D. Cuny, L. Hedstrom, and A. Joachimiak. Plos One, 2015. 10(10): p. e0138976. PMID[26440283].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26457701">New Non-toxic Semi-synthetic Derivatives from Natural Diterpenes Displaying Anti-tuberculosis Activity.</a> Matos, P.M., B. Mahoney, Y. Chan, D.P. Day, M.M. Cabral, C.H. Martins, R.A. Santos, J.K. Bastos, P.C. Page, and V.C. Heleno. Molecules, 2015. 20(10): p. 18264-18278. PMID[26457701].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26318054">4-Aminoquinoline Derivatives as Novel Mycobacterium tuberculosis GYRB Inhibitors: Structural Optimization, Synthesis and Biological Evaluation.</a> Medapi, B., P. Suryadevara, J. Renuka, J.P. Sridevi, P. Yogeeswari, and D. Sriram. European Journal of Medicinal Chemistry, 2015. 103: p. 1-16. PMID[26318054].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26437401" target="_blank">Identification and Validation of Aspartic acid semialdehyde dehydrogenase as a New anti-Mycobacterium tuberculosis Target.</a> Meng, J., Y. Yang, C. Xiao, Y. Guan, X. Hao, Q. Deng, and Z. Lu. International Journal of Molecular Sciences. 16(10): p. 23572-23586. PMID[26437401]. 
    <br />
    <b>[PubMed].</b> TB_10_2015. </p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26498570">Design, Synthesis and Evaluation of Diarylpiperazine Derivatives as Potent Anti-tubercular Agents.</a> Penta, A., S. Franzblau, B. Wan, and S. Murugesan. European Journal of Medicinal Chemistry, 2015. 105: p. 238-244. PMID[26498570].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26487912">Synthesis and Biological Evaluation of Polar Functionalities Containing Nitrodihydroimidazooxazoles as anti-TB Agents.</a> Yempalla, K.R., G. Munagala, S. Singh, G. Kour, S. Sharma, R. Chib, S. Kumar, P. Wazir, G.D. Singh, S. Raina, S.S. Bharate, I.A. Khan, R.A. Vishwakarma, and P.P. Singh. ACS Medicinal Chemistry Letters, 2015. 6(10): p. 1059-1064. PMID[26487912].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26487909">Nitrofuranyl Methyl Piperazines as New Anti-TB Agents: Identification, Validation, Medicinal Chemistry, and PK Studies.</a> Yempalla, K.R., G. Munagala, S. Singh, A. Magotra, S. Kumar, V.S. Rajput, S.S. Bharate, M. Tikoo, G.D. Singh, I.A. Khan, R.A. Vishwakarma, and P.P. Singh. ACS Medicinal Chemistry Letters, 2015. 6(10): p. 1041-1046. PMID[26487909].</p>

    <p class="plaintext"><b>[PubMed]</b>. TB_10_2015.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26155871" target="_blank">Synthesis, in Vitro and in Silico Studies of Some Novel 5-Nitrofuran-2-yl hydrazones as Antimicrobial and Antitubercular Agents.</a> Abdel-Aziz, H., W. Eldehna, M. Fares, T. Elsaman, M. Abdel-Aziz, and D. Soliman. Biological &amp; Pharmaceutical Bulletin, 2015. 38(10): p. 1617-1630. PMID[26155871]. 
    <br />
    <b>[WOS].</b> TB_10_2015.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000361453500013" target="_blank">Antimicrobial Activity of TiO2:Ag Nanocrystalline Heterostructures: Experimental and Theoretical Insights.</a> Andre, R., C. Zamperini, E. Mima, V. Longo, A. Albuquerque, J. Sambrano, A. Machado, C. Vergani, A. Hernandes, J. Varela, and E. Longo. Chemical Physics, 2015. 459: p. 87-95. ISI[000361453500013]. 
    <br />
    <b>[WOS].</b> TB_10_2015. </p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362307500011">Development of ssDNA Aptamers as Potent Inhibitors of Mycobacterium tuberculosis Acetohydroxyacid Synthase.</a> Baig, I.A., J.Y. Moon, S.C. Lee, S.W. Ryoo, and M.Y. Yoon. Biochimica Et Biophysica Acta-Proteins and Proteomics, 2015. 1854(10): p. 1338-1350. ISI[000362307500011].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26210507" target="_blank">Combating Highly Resistant Emerging Pathogen Mycobacterium abscessus and Mycobacterium tuberculosis with Novel Salicylanilide Esters and Carbamates.</a> Baranyai, Z., M. Kratky, J. Vinsova, N. Szabo, Z. Senoner, K. Horvati, J. Stolarikova, S. David, and S. Bosze. European Journal of Medicinal Chemistry, 2015. 101: p. 692-704. PMID[26210507]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26295286" target="_blank">Identification of Phenoxyalkylbenzimidazoles with Antitubercular Activity.</a> Chandrasekera, N., T. Alling, M. Bailey, M. Files, J. Early, J. Ollinger, Y. Ovechkina, T. Masquelin, P. Desai, J. Cramer, P. Hipskind, J. Odingo, and T. Parish. Journal of Medicinal Chemistry, 2015. 58(18): p. 7273-7285. PMID[26295286]. 
    <br />
    <b>[WOS].</b> TB_10_2015.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26218806" target="_blank">The ATPase Activity of the Mycobacterial Plasma Membrane is inhibited by the LL37-analogous Peptide LLAP.</a> Chingate, S., G. Delgado, L. Salazar, and C. Soto. Peptides, 2015. 71: p. 222-228. PMID[26218806]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26162339" target="_blank">Evaluation of Substituted methyl cyclohexanone hybrids for Anti-tubercular, Anti-bacterial and Anti-fungal Activity: Facile Syntheses under Catalysis by Ionic Liquids.</a> Ghatole, A., K. Lanjewar, M. Gaidhane, and K. Hatzade. Spectrochimica acta Part A-Molecular and Biomolecular Spectroscopy, 2015. 151: p. 515-524. PMID[26162339]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">19. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000361758000013" target="_blank">Design, Synthesis of Quinolinyl Schiff Bases and Azetidinones as Enoyl ACP-reductase Inhibitors.</a> Joshi, S., U. More, D. Parkale, T. Aminabhavi, A. Gadad, M. Nadagouda, and R. Jawarkar. Medicinal Chemistry Research, 2015. 24(11): p. 3892-3911. ISI[000361758000013]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">20. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000361530500020" target="_blank">Chemical Composition and Biological Activity of Essential Oils of Sempervivum brevipilum Muirhead.</a> Kahriman, N., Z. Senyurek, V. Serdaroglu, A. Kahriman, and N. Yayli. Records of Natural Products, 2015. 9(4): p. 603-608. ISI[000361530500020]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25643871" target="_blank">Triazole: A Promising Antitubercular Agent.</a> Keri, R., S. Patil, S. Budagumpi, and B. Nagaraja. Chemical Biology &amp; Drug Design, 2015. 86(4): p. 410-423. PMID[25643871]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26341133" target="_blank">Development of a Three Component Complex to Increase Isoniazid Efficacy against Isoniazid Resistant and Nonresistant Mycobacterium tuberculosis.</a> Manning, T., S. Plummer, T. Baker, G. Wylie, A. Clingenpeel, and D. Phillips. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(20): p. 4621-4627. PMID[26341133]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26271585" target="_blank">Design of Novel Dispirooxindolopyrrolidine and Dispirooxindolopyrrolothiazole Derivatives as Potential Antitubercular Agents.</a> Mhiri, C., S. Boudriga, M. Askri, M. Knorr, D. Sriram, P. Yogeeswari, F. Nana, C. Golz, and C. Strohmann. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(19): p. 4308-4313. PMID[26271585]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24946795" target="_blank">Permeability Enhancing Lipid-based Co-solvent and SEDDS Formulations of SQ641, an Antimycobacterial Agent.</a> Mutyam, S., N. Bejugam, H. Parish, V. Reddy, E. Bogatcheva, and G. Shankar. Pharmaceutical Development and Technology, 2015. 20(5): p. 598-607. PMID[24946795]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26245695" target="_blank">Andrographolide: A Potent Antituberculosis Compound that Targets Aminoglycoside 2&#39;-N-acetyltransferase in Mycobacterium tuberculosis.</a> Prabu, A., S. Hassan, Prabuseenivasan, A. Shainaba, L. Hanna, and V. Kumar. Journal of Molecular Graphics &amp; Modelling, 2015. 61: p. 133-140. PMID[26245695]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362792900027">Antifolate Activity of Plant Polyphenols against Mycobacterium tuberculosis.</a> Raju, A., M.S. Degani, M.P. Khambete, M.K. Ray, and M.G.R. Rajan. Phytotherapy Research, 2015. 29(10): p. 1646-1651. ISI[000362792900027].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_10_2015.</p>

    <br />

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26298500" target="_blank">One-pot Synthesis of new Triazole-imidazo[2,1-b][1,3,4]thiadiazole hybrids via Click Chemistry and Evaluation of Their Antitubercular Activity.</a> Ramprasad, J., N. Nayak, U. Dalimba, P. Yogeeswari, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2015. 25(19): p. 4169-4173. PMID[26298500]. 
    <br />
    <b>[WOS].</b> TB_10_2015. 
    <br />
    <br /></p>

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26210508" target="_blank">Design, Synthesis of Benzocoumarin-pyrimidine Hybrids as Novel Class of Antitubercular Agents, their DNA Cleavage and X-ray Studies.</a> Reddy, D., K. Hosamani, and H. Devarajegowda. European Journal of Medicinal Chemistry, 2015. 101: p. 705-715. PMID[26210508]. 
    <br />
    <b>[WOS].</b> TB_10_2015. </p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26218841" target="_blank">Phenylbutyrate Induces LL-37-dependent Autophagy and Intracellular Killing of Mycobacterium tuberculosis in Human Macrophages.</a> Rekha, R., S. Muvva, M. Wan, R. Raqib, P. Bergman, S. Brighenti, G. Gudmundsson, and B. Agerberth. Autophagy, 2015. 11(9): p. 1688-1699. PMID[26218841]. 
    <br />
    <b>[WOS].</b> TB_10_2015. </p>

    <br />

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000362620900036">Piperazine Derivatives: Synthesis, Inhibition of the Mycobacterium tuberculosis Enoyl-acyl Carrier Protein Reductase and SAR Studies.</a> Rotta, M., K. Pissinate, A.D. Villela, D.F. Back, L. Timmers, J.F.R. Bachega, O.N. de Souza, D.S. Santos, L.A. Basso, and P. Machado. European Journal of Medicinal Chemistry, 2014. 87: p. 436-447. ISI[000362620900036].</p>

    <p class="plaintext"><b>[WOS]</b>. TB_10_2015.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">31. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151029&amp;CC=WO&amp;NR=2015164482A1&amp;KC=A1">Inhibitors of Drug-resistant Mycobacterium tuberculosis.</a> Bishai, W.R., S. Lun, and H. Guo. Patent. 2015. 2015-US27053 2015164482: 74pp.</p>

    <p class="plaintext"><b>[Patent].</b> TB_10_2015.</p>

    <br />

    <p class="plaintext">32. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20151015&amp;CC=WO&amp;NR=2015155549A1&amp;KC=A1">Preparation of Oxazoloquinoline Derivatives as Antibacterial Compounds.</a> Ratcliffe, A., A. Huxley, D. Lyth, G. Noonan, R. Kirk, M. Uosis-Martin, and N. Stokes. Patent. 2015. 2015-GB51107 2015155549: 183pp.</p>

    <p class="plaintext"><b>[Patent].</b> TB_10_2015.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
