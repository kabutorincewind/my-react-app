

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="2006Archive.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="QoqQD5gM7lLVSF4vcDpckrwWVAEHRXqO87n/gXGo/zX/0L9sKcNuD4tlIzzPQjOXufLuVk2Jc0HUwxmMhXLMgt6m/HbUw9ykMHyt31ZfFXn9Nf14FbHqV+hQ06s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="1B864018" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">
	<div id="description">
		<h1>Literature Surveillance Memos</h1>

        <p>This page contains links to bi-weekly literature surveillance memos that identify relevant published research therapies on pre-clinical experimental therapies for HIV, the opportunistic infections (OIs) associated with AIDS and other viral pathogens.</p>
        <p>Note: Where available, URL links to article abstracts have been included for the convenience of the user. Abstract links to PubMed (ncbi.nlm.nih.gov) are available to all users.  Abstract links to Web of Science (publishorperish.nih.gov) or Science Direct (sciencedirect.com) are only available to NIH staff inside the NIH firewall or to other registered users of these Websites.</p>
        
        <div id="memo">
            <table id="memotable">
                <tr><th class="MemoColHIVArchive">HIV</th><th class="MemoColOIArchive">Opportunistic Infections</th><th class="MemoColOVArchive">Other Viral Pathogens</th></tr>
            <tr>
			<td><a href="HIV-LS-364.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-364.aspx" target="_blank">64</a>
              (12/28/2006)
			</td>
            <td><a href="OI-LS-364.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-364.aspx" target="_blank">64</a>
              (12/28/2006)
			</td>
			<td><a href="dmid-ls-134.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-134.aspx" target="_blank">134</a>
              (12/21/2006)
			</td>
			</tr>

			<tr>
			<td><a href="HIV-LS-363.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-363.aspx" target="_blank">63</a>
              (12/14/2006)
			</td>
            <td><a href="OI-LS-363.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-363.aspx" target="_blank">63</a>
              (12/14/2006)
			</td>
			<td><a href="dmid-ls-133.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-133.aspx" target="_blank">133</a>
              (12/07/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-362.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-362.aspx" target="_blank">62</a>
              (11/30/2006)
			</td>
            <td><a href="OI-LS-362.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-362.aspx" target="_blank">62</a>
              (11/30/2006)
			</td>
			<td><a href="dmid-ls-132.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-132.aspx" target="_blank">132</a>
              (11/23/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-361.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-361.aspx" target="_blank">61</a>
              (11/16/2006)
			</td>
            <td><a href="OI-LS-361.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-361.aspx" target="_blank">61</a>
              (11/16/2006)
			</td>
			<td><a href="dmid-ls-131.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-131.aspx" target="_blank">131</a>
              (11/09/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-360.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-360.aspx" target="_blank">60</a>
              (11/02/2006)
			</td>
            <td><a href="OI-LS-360.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-360.aspx" target="_blank">60</a>
              (11/02/2006)
			</td>
			<td><a href="dmid-ls-130.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-130.aspx" target="_blank">130</a>
              (10/26/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-359.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-359.aspx" target="_blank">59</a>
              (10/19/2006)
			</td>
            <td><a href="OI-LS-359.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-359.aspx" target="_blank">59</a>
              (10/19/2006)
			</td>
			<td><a href="dmid-ls-129.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-129.aspx" target="_blank">129</a>
              (10/12/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-358.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-358.aspx" target="_blank">58</a>
              (10/05/2006)
			</td>
            <td><a href="OI-LS-358.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-358.aspx" target="_blank">58</a>
              (10/05/2006)
			</td>
			<td><a href="dmid-ls-128.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-128.aspx" target="_blank">128</a>
              (09/28/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-357.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-357.aspx" target="_blank">57</a>
              (09/21/2006)
			</td>
            <td><a href="OI-LS-357.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-357.aspx" target="_blank">57</a>
              (09/21/2006)
			</td>
			<td><a href="dmid-ls-127.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-127.aspx" target="_blank">127</a>
              (09/14/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-356.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-356.aspx" target="_blank">56</a>
              (09/07/2006)
			</td>
            <td><a href="OI-LS-356.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-356.aspx" target="_blank">56</a>
              (09/07/2006)
			</td>
			<td><a href="dmid-ls-126.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-126.aspx" target="_blank">126</a>
              (08/31/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-355.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-355.aspx" target="_blank">55</a>
              (08/24/2006)
			</td>
            <td><a href="OI-LS-355.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-355.aspx" target="_blank">55</a>
              (08/24/2006)
			</td>
			<td><a href="dmid-ls-125.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-125.aspx" target="_blank">125</a>
              (08/17/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-354.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-354.aspx" target="_blank">54</a>
              (08/10/2006)
			</td>
            <td><a href="OI-LS-354.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-354.aspx" target="_blank">54</a>
              (08/10/2006)
			</td>
			<td><a href="dmid-ls-124.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-124.aspx" target="_blank">124</a>
              (08/03/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-353.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-353.aspx" target="_blank">53</a>
              (07/27/2006)
			</td>
            <td><a href="OI-LS-353.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-353.aspx" target="_blank">53</a>
              (07/27/2006)
			</td>
			<td><a href="dmid-ls-123.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-123.aspx" target="_blank">123</a>
              (07/20/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-352.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-352.aspx" target="_blank">52</a>
              (07/13/2006)
			</td>
            <td><a href="OI-LS-352.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-352.aspx" target="_blank">52</a>
              (07/13/2006)
			</td>
			<td><a href="dmid-ls-122.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-122.aspx" target="_blank">122</a>
              (07/06/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-351.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-351.aspx" target="_blank">51</a>
              (06/29/2006)
			</td>
            <td><a href="OI-LS-351.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-351.aspx" target="_blank">51</a>
              (06/29/2006)
			</td>
			<td><a href="dmid-ls-121.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-121.aspx" target="_blank">121</a>
              (06/22/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-350.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-350.aspx" target="_blank">50</a>
              (06/15/2006)
			</td>
            <td><a href="OI-LS-350.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-350.aspx" target="_blank">50</a>
              (06/15/2006)
			</td>
			<td><a href="dmid-ls-120.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-120.aspx" target="_blank">120</a>
              (06/08/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-349.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-349.aspx" target="_blank">49</a>
              (06/01/2006)
			</td>
            <td><a href="OI-LS-349.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-349.aspx" target="_blank">49</a>
              (06/01/2006)
			</td>
			<td><a href="dmid-ls-119.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-119.aspx" target="_blank">119</a>
              (05/25/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-348.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-348.aspx" target="_blank">48</a>
              (05/18/2006)
			</td>
            <td><a href="OI-LS-348.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-348.aspx" target="_blank">48</a>
              (05/18/2006)
			</td>
			<td><a href="dmid-ls-118.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-118.aspx" target="_blank">118</a>
              (05/11/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-347.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-347.aspx" target="_blank">47</a>
              (05/04/2006)
			</td>
            <td><a href="OI-LS-347.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-347.aspx" target="_blank">47</a>
              (05/04/2006)
			</td>
			<td><a href="dmid-ls-117.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-117.aspx" target="_blank">117</a>
              (04/27/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-346.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-346.aspx" target="_blank">46</a>
              (04/20/2006)
			</td>
            <td><a href="OI-LS-346.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-346.aspx" target="_blank">46</a>
              (04/20/2006)
			</td>
			<td><a href="dmid-ls-116.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-116.aspx" target="_blank">116</a>
              (04/13/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-345.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-345.aspx" target="_blank">45</a>
              (04/06/2006)
			</td>
            <td><a href="OI-LS-345.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-345.aspx" target="_blank">45</a>
              (04/06/2006)
			</td>
			<td><a href="dmid-ls-115.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-115.aspx" target="_blank">115</a>
              (03/30/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-344.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-344.aspx" target="_blank">44</a>
              (03/23/2006)
			</td>
            <td><a href="OI-LS-344.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-344.aspx" target="_blank">44</a>
              (03/23/2006)
			</td>
			<td><a href="dmid-ls-114.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-114.aspx" target="_blank">114</a>
              (03/16/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-343.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-343.aspx" target="_blank">43</a>
              (03/09/2006)
			</td>
            <td><a href="OI-LS-343.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-343.aspx" target="_blank">43</a>
              (03/09/2006)
			</td>
			<td><a href="dmid-ls-113.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-113.aspx" target="_blank">113</a>
              (03/02/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-342.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-342.aspx" target="_blank">42</a>
              (02/23/2006)
			</td>
            <td><a href="OI-LS-342.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-342.aspx" target="_blank">42</a>
              (02/23/2006)
			</td>
			<td><a href="dmid-ls-112.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-112.aspx" target="_blank">112</a>
              (02/16/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-341.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-341.aspx" target="_blank">41</a>
              (02/09/2006)
			</td>
            <td><a href="OI-LS-341.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-341.aspx" target="_blank">41</a>
              (02/09/2006)
			</td>
			<td><a href="dmid-ls-111.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-111.aspx" target="_blank">111</a>
              (02/02/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-340.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-340.aspx" target="_blank">40</a>
              (01/26/2006)
			</td>
            <td><a href="OI-LS-340.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-340.aspx" target="_blank">40</a>
              (01/26/2006)
			</td>
			<td><a href="dmid-ls-110.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-110.aspx" target="_blank">110</a>
              (01/19/2006)
			</td>
            </tr>

			<tr>
			<td><a href="HIV-LS-339.aspx" target="_blank">HIV-LS-3</a><a href="HIV-LS-339.aspx" target="_blank">39</a>
              (01/12/2006)
			</td>
            <td><a href="OI-LS-339.aspx" target="_blank">OI-LS-3</a><a href="OI-LS-339.aspx" target="_blank">39</a>
              (01/12/2006)
			</td>
			<td><a href="dmid-ls-109.aspx" target="_blank">DMID-LS-</a><a href="dmid-ls-109.aspx" target="_blank">109</a>
              (01/05/2006)
			</td>
            </tr>
                
            </table>
			<fieldset id="memobottom">			
			    <br /><br /><br />			
			    <fieldset class="submitbuttons" id="memobottomrow">
                    <ul id="memobottomrowbar">
                        <li><a href="2004Archive.aspx">2004</a></li> 
	    		    	<li><a href="2005Archive.aspx">2005</a></li>
	    			    <li><a href="2007Archive.aspx">2007</a></li>
	    			    <li><a href="2009Archive.aspx">2008-2009</a></li>
	    			    <li><a href="2010Archive.aspx">2010</a></li>
	    			    <li><a href="2011Archive.aspx">2011</a></li>
  				        <li><a href="2012Archive.aspx">2012</a></li>
   				        <li><a href="2013Archive.aspx">2013</a></li>
   				        <li><a href="2014Archive.aspx">2014</a></li>    
   				        <li><a href="2015Archive.aspx">2015</a></li>    
   				        <li><a href="2016Archive.aspx">2016</a></li>       				        
   				        <li><a href="memos.aspx">2017</a></li>    
                    </ul>                                      
    			    <br /><br />  				    			    
			    </fieldset>
            </fieldset>
			                
        </div>

    </div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
