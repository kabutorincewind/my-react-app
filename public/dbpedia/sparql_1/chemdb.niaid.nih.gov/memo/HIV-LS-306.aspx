

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-306.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a9cVs8CkKjJUKLIF7SId5vGFAvu7aw8ycQJhoq+iYiZTFp861Yq8MPaFwfSlqOJvuTTzgWstS0z/cQC0j0dZ9MQgf7/MD7efohbTJEb0vu143sj4jC/NqRlZ6So=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="32873050" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-306-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44109   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          The effect of clarithromycin, fluconazole, and rifabutin on sulfamethoxazole hydroxylamine formation in individuals with human immunodeficiency virus infection (AACTG 283)</p>

    <p>          Winter, HR, Trapnell, CB, Slattery, JT, Jacobson, M, Greenspan, DL, Hooton, TM, and Unadkat, JD</p>

    <p>          Clin Pharmacol Ther <b>2004</b>.  76(4): 313-22</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15470330&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15470330&amp;dopt=abstract</a> </p><br />

    <p>2.         44110   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Structural determinants of the anti-HIV activity of a CCR5 antagonist derived from toxoplasma gondii</p>

    <p>          Yarovinsky, F, Andersen, JF, King, LR, Caspar, P, Aliberti, J, Golding, H, and Sher, A</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15469936&amp;dopt=abstract</a> </p><br />

    <p>3.         44111   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          APOBEC3B and APOBEC3C are potent inhibitors of simian immunodeficiency virus replication</p>

    <p>          Yu, Q, Chen, D, Konig, R, Mariani, R, Unutmaz, D, and Landau, NR</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15466872&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15466872&amp;dopt=abstract</a> </p><br />

    <p>4.         44112   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Impact of frequent natural polymorphisms at the protease gene on the in vitro susceptibility to protease inhibitors in HIV-1 non-B subtypes</p>

    <p>          Holguin, A, Paxinos, E, Hertogs, K, Womac, C, and Soriano, V</p>

    <p>          J Clin Virol <b>2004</b>.  31(3): 215-20</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465415&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465415&amp;dopt=abstract</a> </p><br />

    <p>5.         44113   HIV-LS-306; EMBASE-HIV-10/8/2004</p>

    <p class="memofmt1-2">          New non-nucleoside reverse transcriptase inhibitors (NNRTIs) in development for the treatment of HIV infections</p>

    <p>          Pauwels, Rudi</p>

    <p>          Current Opinion in Pharmacology <b>2004</b>.  4(5): 437-446</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6W7F-4D588NY-1/2/2d77dc7b8c2f99673a831a719a9cd416">http://www.sciencedirect.com/science/article/B6W7F-4D588NY-1/2/2d77dc7b8c2f99673a831a719a9cd416</a> </p><br />
    <br clear="all">

    <p>6.         44114   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Inhibition of HIV-1 infection in cells expressing an artificial complementary peptide</p>

    <p>          Hosokawa, M, Imai, M, Okada, H, and Okada, N</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  324(1): 236-40</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465008&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15465008&amp;dopt=abstract</a> </p><br />

    <p>7.         44115   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Hinnuliquinone, a C(2)-symmetric dimeric non-peptide fungal metabolite inhibitor of HIV-1 protease</p>

    <p>          Singh, SB, Ondeyka, JG, Tsipouras, N, Ruby, C, Sardana, V, Schulman, M, Sanchez, M, Pelaez, F, Stahlhut, MW, Munshi, S, Olsen, DB, and Lingham, RB</p>

    <p>          Biochem Biophys Res Commun <b>2004</b>.  324(1): 108-113</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15464989&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15464989&amp;dopt=abstract</a> </p><br />

    <p>8.         44116   HIV-LS-306; EMBASE-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Promising Phase I results against new HIV target</p>

    <p>          Whelan, Jo</p>

    <p>          Drug Discovery Today <b>2004</b>.  9(19): 823</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T64-4DBSDJ7-4/2/e0c9b2f6cf07c58868bf925c6d263d79">http://www.sciencedirect.com/science/article/B6T64-4DBSDJ7-4/2/e0c9b2f6cf07c58868bf925c6d263d79</a> </p><br />

    <p>9.         44117   HIV-LS-306; EMBASE-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Potency of HIV-1 envelope glycoprotein gp120 antibodies to inhibit the interaction of DC-SIGN with HIV-1 gp120</p>

    <p>          Lekkerkerker, Annemarie N, Ludwig, Irene S, van Vliet, Sandra J, van Kooyk, Yvette, and Geijtenbeek, Teunis BH</p>

    <p>          Virology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WXR-4DCMGYR-1/2/c5552ae56912d148c5efcc69d445a89a">http://www.sciencedirect.com/science/article/B6WXR-4DCMGYR-1/2/c5552ae56912d148c5efcc69d445a89a</a> </p><br />

    <p>10.       44118   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Design and Synthesis of Novel Indole beta-Diketo Acid Derivatives as HIV-1 Integrase Inhibitors</p>

    <p>          Sechi, M, Derudas, M, Dallocchio, R, Dessi, A, Bacchi, A, Sannia, L, Carta, F, Palomba, M, Ragab, O, Chan, C, Shoemaker, R, Sei, S, Dayam, R, and Neamati, N</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5298-5310</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456274&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456274&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44119   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          A 4&#39;-C-Ethynyl-2&#39;,3&#39;-Dideoxynucleoside Analogue Highlights the Role of the 3&#39;-OH in Anti-HIV Active 4&#39;-C-Ethynyl-2&#39;-deoxy Nucleosides</p>

    <p>          Siddiqui, MA, Hughes, SH, Boyer, PL, Mitsuya, H, Van, QN, George, C, Sarafinanos, SG, and Marquez, VE</p>

    <p>          J Med Chem <b>2004</b>.  47(21): 5041-5048</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456247&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456247&amp;dopt=abstract</a> </p><br />

    <p>12.       44120   HIV-LS-306; EMBASE-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Macrophages and HIV-1: dangerous liaisons</p>

    <p>          Verani, Alessia, Gras, Gabriel, and Pancino, Gianfranco</p>

    <p>          Molecular Immunology <b>2004</b>.  In Press, Corrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T9R-4CY0FB7-4/2/d350b9a98698d34ab97c3dd1e22134b5">http://www.sciencedirect.com/science/article/B6T9R-4CY0FB7-4/2/d350b9a98698d34ab97c3dd1e22134b5</a> </p><br />

    <p>13.       44121   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          ATP binding cassette multidrug transporters limit the anti-HIV activity of zidovudine and indinavir in infected human macrophages</p>

    <p>          Jorajuria, S, Dereuddre-Bosquet, N, Becher, F, Martin, S, Porcheray, F, Garrigues, A, Mabondzo, A, Benech, H, Grassi, J, Orlowski, S, Dormont, D, and Clayette, P</p>

    <p>          Antivir Ther <b>2004</b>.  9(4): 519-28</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456083&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15456083&amp;dopt=abstract</a> </p><br />

    <p>14.       44122   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Molecular dynamics simulations of 14 HIV protease mutants in complexes with indinavir</p>

    <p>          Chen, X, Weber, IT, and Harrison, RW</p>

    <p>          J Mol Model (Online) <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452774&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15452774&amp;dopt=abstract</a> </p><br />

    <p>15.       44123   HIV-LS-306; EMBASE-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Distinct effects of protease and reverse transcriptase inhibition in an immunological model of HIV-1 infection with impulsive drug effects</p>

    <p>          Smith, RJ and Wahl, LM</p>

    <p>          Bulletin of Mathematical Biology <b>2004</b>.  66(5): 1259-1283</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WC7-4BRP6TR-2/2/767f7da62926bd1f55137bcf68873aff">http://www.sciencedirect.com/science/article/B6WC7-4BRP6TR-2/2/767f7da62926bd1f55137bcf68873aff</a> </p><br />
    <br clear="all">

    <p>16.       44124   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          A series of diaryltriazines and diarylpyrimidines are highly potent nonnucleoside reverse transcriptase inhibitors with possible applications as microbicides</p>

    <p>          Van Herrewege, Y, Vanham, G, Michiels, J, Fransen, K, Kestens, L, Andries, K, Janssen, P, and Lewi, P</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(10): 3684-3689</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388420&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15388420&amp;dopt=abstract</a> </p><br />

    <p>17.       44125   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Alkaloids from Leucojum vernum and Antiretroviral Activity of Amaryllidaceae Alkaloids</p>

    <p>          Szlavik, L, Gyuris, A, Minarovits, J, Forgo, P, Molnar, J, and Hohmann, J</p>

    <p>          Planta Med <b>2004</b>.  70(9): 871-3</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386196&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386196&amp;dopt=abstract</a> </p><br />

    <p>18.       44126   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Anti-HIV-1 Activity of the Iboga Alkaloid Congener 18-Methoxycoronaridine</p>

    <p>          Silva, EM, Cirne-Santos, CC, Frugulhetti, IC, Galvao-Castro, B, Saraiva, EM, Kuehne, ME, and Bou-Habib, DC</p>

    <p>          Planta Med <b>2004</b>.  70(9): 808-12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15386189&amp;dopt=abstract</a> </p><br />

    <p>19.       44127   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          A novel action of minocycline: Inhibition of human immunodeficiency virus type 1 infection in microglia</p>

    <p>          Si, Q, Cosenza, M, Kim, MO, Zhao, ML, Brownlee, M, Goldstein, H, and Lee, S</p>

    <p>          J Neurovirol <b>2004</b>.  10(5): 284-292</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15385251&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15385251&amp;dopt=abstract</a> </p><br />

    <p>20.       44128   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          A nonneutralizing anti-HIV-1 antibody turns into a neutralizing antibody when expressed on the surface of HIV-1-susceptible cells: a new way to fight HIV</p>

    <p>          Lee, SJ, Garza, L, Yao, J, Notkins, AL, and Zhou, P</p>

    <p>          J Immunol <b>2004</b>.  173(7): 4618-26</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383596&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383596&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.       44129   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Curcumin, a novel p300/CBP specific inhibitor of acetyltransferase, represses the acetylation of histones/nonhistone proteins and HAT dependent chromatin transcription</p>

    <p>          Balasubramanyam, K, Varier, RA, Altaf, M, Swaminathan, V, Siddappa, NB, Ranga, U, and Kundu, TK</p>

    <p>          J Biol Chem <b>2004</b>.</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383533&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15383533&amp;dopt=abstract</a> </p><br />

    <p>22.       44130   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Facile incorporation of urea pseudopeptides into protease substrate analogue inhibitors</p>

    <p>          Myers, AC, Kowalski, JA, and Lipton, MA</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(20): 5219-22</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380231&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15380231&amp;dopt=abstract</a> </p><br />

    <p>23.       44131   HIV-LS-306; PUBMED-HIV-10/8/2004</p>

    <p class="memofmt1-2">          Dual role of prostratin in inhibition of infection and reactivation of human immunodeficiency virus from latency in primary blood lymphocytes and lymphoid tissue</p>

    <p>          Biancotto, A, Grivel, JC, Gondois-Rey, F, Bettendroffer, L, Vigne, R, Brown, S, Margolis, LB, and Hirsch, I</p>

    <p>          J Virol <b>2004</b>.  78(19): 10507-15</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15367617&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15367617&amp;dopt=abstract</a> </p><br />

    <p>24.       44132   HIV-LS-306; WOS-HIV-9/26/2004</p>

    <p class="memofmt1-2">          Synthesis of some new 4-amino-1,2,4-triazole derivatives as potential anti-HIV and anti-HBV</p>

    <p>          El-Barbary, AA, Abou-El-Ezz, AZ, Abdel-Kader, AA, El-Daly, M, and Nielsen, C</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2004</b>.  179(8): 1497-1508, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223476100007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223476100007</a> </p><br />

    <p>25.       44133   HIV-LS-306; WOS-HIV-9/26/2004</p>

    <p class="memofmt1-2">          Double cyclization of bis(alpha-hetarylmethyl)amino esters to optically active bridged N-heterocycles of HIV-inhibiting activity</p>

    <p>          Faltz, H, Bender, C, Wohrl, BM, Vogel-Bachmayr, K, Hubscher, U, Ramadan, K, and Liebscher, J</p>

    <p>          EUROPEAN JOURNAL OF ORGANIC CHEMISTRY <b>2004</b>.(16): 3484-3496, 13</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223457500013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223457500013</a> </p><br />

    <p>26.       44134   HIV-LS-306; WOS-HIV-10/3/2004</p>

    <p class="memofmt1-2">          Synthesis and biological activities of 5 &#39;-ethylenic and acetylenic modified L-nucleosides and isonucleosides</p>

    <p>          Wang, JF, Yang, XD, Zhang, LR, Yang, ZJ, and Zhang, LH</p>

    <p>          TETRAHEDRON <b>2004</b>.  60(38): 8535-8546, 12</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223654700022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223654700022</a> </p><br />
    <br clear="all">

    <p>27.       44135   HIV-LS-306; WOS-HIV-10/3/2004</p>

    <p class="memofmt1-2">          Synthesis of 3-fluoro-2 &#39;,3 &#39;-dideoxy-2 &#39;,3 &#39;-didehydro-4 &#39;-ethynyl-D- and -L-furanosyl nucleosides</p>

    <p>          Chen, X, Zhou, W, Schinazi, RF, and Chu, CK</p>

    <p>          JOURNAL OF ORGANIC CHEMISTRY <b>2004</b>.  69(18): 6034-6041, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223573100021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223573100021</a> </p><br />

    <p>28.       44136   HIV-LS-306; WOS-HIV-10/3/2004</p>

    <p class="memofmt1-2">          A new laccase from dried fruiting bodies of the monkey head mushroom Hericium erinaceum</p>

    <p>          Wang, H and Ng, TB</p>

    <p>          BIOCHEMICAL AND BIOPHYSICAL RESEARCH COMMUNICATIONS <b>2004</b>.  322(1): 17-21, 5</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223581000003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223581000003</a> </p><br />

    <p>29.       44137   HIV-LS-306; WOS-HIV-10/3/2004</p>

    <p class="memofmt1-2">          Intracellular metabolism and pharmacokinetics of 5 &#39;-hydrogenphosphonate of 3 &#39;-azido-2 &#39;,3 &#39;-dideoxythymidine, a prodrug of 3 &#39;-azido-2 &#39;,3 &#39;-dideoxythymidine</p>

    <p>          Skoblov, Y, Karpenko, I, Shirokova, E, Popov, K, Andronova, V, Galegov, G, and Kukhanova, M</p>

    <p>          ANTIVIRAL RESEARCH <b>2004</b>.  63(2): 107-113, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223558100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000223558100005</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
