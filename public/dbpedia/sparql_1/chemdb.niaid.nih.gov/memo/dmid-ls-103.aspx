

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-103.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="fOPQp+O9GkdwPxqN5iv4RuHVkbNQoJvqzlhtwnyW7tjIbSyVYjvRE3yPVbDUzXXLXBr8xrgz2ym70l0xZQFVYVQSzhnlkQdyAJSHSApUDGdPMo2l97qBVwenR54=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DD1874ED" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-103-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57888   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral cyclic D, L-a-peptides: Targeting a general biochemical pathway in virus infections</p>

    <p>          Horne, WSeth, Wiethoff, Christopher M, Cui, Chunli, Wilcoxen, Keith M, Amorin, Manuel, Ghadiri, MReza, and Nemerow, Glen R</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  13(17): 5145-5153</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     57889   DMID-LS-103; WOS-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Development of a Homogeneous Screening Assay for Automated Detection of Antiviral Agents Active Against Severe Acute Respiratory Syndrome-Associated Coronavirus</p>

    <p>          Ivens, T. <i>et al.</i></p>

    <p>          Journal of Virological Methods <b>2005</b>.  129(1): 56-63</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231936500007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231936500007</a> </p><br />

    <p>3.     57890   DMID-LS-103; WOS-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antisense Phosphorothioate Oligonucleotide Inhibition of Hepatitis C Virus Genotype 4 Replication in Hepg2 Cells</p>

    <p>          El Awady, M.</p>

    <p>          Journal of Biotechnology <b>2005</b>.  118: S79</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231195200268">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231195200268</a> </p><br />

    <p>4.     57891   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Broad-spectrum inhibitor of viruses in the Flaviviridae family</p>

    <p>          Ojwang, JO, Ali, S, Smee, DF, Morrey, JD, Shimasaki, CD, and Sidwell, RW</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16199098&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16199098&amp;dopt=abstract</a> </p><br />

    <p>5.     57892   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Incidence of adamantane resistance among influenza A (H3N2) viruses isolated worldwide from 1994 to 2005: a cause for concern</p>

    <p>          Bright, RA, Medina, MJ, Xu, X, Perez-Oronoz, G, Wallis, TR, Davis, XM, Povinelli, L, Cox, NJ, and Klimov, AI</p>

    <p>          Lancet <b>2005</b>.  366(9492): 1175-81</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198766&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16198766&amp;dopt=abstract</a> </p><br />

    <p>6.     57893   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian influenza A (H5N1) infection in humans</p>

    <p>          Beigel, JH, Farrar, J, Han, AM, Hayden, FG, Hyer, R, de, Jong MD, Lochindarat, S, Nguyen, TK, Nguyen, TH, Tran, TH, Nicoll, A, Touch, S, and Yuen, KY</p>

    <p>          N Engl J Med <b>2005</b>.  353(13): 1374-85</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16192482&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16192482&amp;dopt=abstract</a> </p><br />

    <p>7.     57894   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase inhibitors for influenza</p>

    <p>          Moscona, A</p>

    <p>          N Engl J Med <b>2005</b>.  353(13): 1363-73</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16192481&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16192481&amp;dopt=abstract</a> </p><br />

    <p>8.     57895   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Structure-Activity Relationship of the First Nonpeptidergic Inverse Agonists for the Human Cytomegalovirus Encoded Chemokine Receptor US28</p>

    <p>          Hulshof, JW, Casarosa, P, Menge, WM, Kuusisto, LM, van, der Goot H, Smit, MJ, de, Esch IJ, and Leurs, R</p>

    <p>          J Med Chem <b>2005</b>.  48(20): 6461-71</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190772&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190772&amp;dopt=abstract</a> </p><br />

    <p>9.     57896   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral random sequence oligonucleotides targeting viral families</p>

    <p>          Vaillant, Andrew and Juteau, Jean-Marc</p>

    <p>          PATENT:  US <b>2005196382</b>  ISSUE DATE:  20050908</p>

    <p>          APPLICATION: 2004-52308  PP: 100 pp., Cont.-in-part of U.S. Ser. No. 661,402.</p>

    <p>          ASSIGNEE:  (Replicor, Inc. Can.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   57897   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Structure-Activity Relationships of Novel Anti-hepatitis C Agents: N(3),5&#39;-Cyclo-4-(beta-d-ribofuranosyl)-vic-triazolo[4,5-b]pyridin-5-one Derivatives</p>

    <p>          Wang, P, Du, J, Rachakonda, S, Chun, BK, Tharnish, PM, Stuyver, LJ, Otto, MJ, Schinazi, RF, and Watanabe, KA</p>

    <p>          J Med Chem <b>2005</b>.  48(20): 6454-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190771&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190771&amp;dopt=abstract</a> </p><br />

    <p>11.   57898   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Simple but Highly Effective Three-Dimensional Chemical-Feature-Based Pharmacophore Model for Diketo Acid Derivatives as Hepatitis C Virus RNA-Dependent RNA Polymerase Inhibitors</p>

    <p>          Di Santo, R, Fermeglia, M, Ferrone, M, Paneni, MS, Costi, R, Artico, M, Roux, A, Gabriele, M, Tardif, KD, Siddiqui, A, and Pricl, S</p>

    <p>          J Med Chem <b>2005</b>.  48(20): 6304-6314</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190757&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190757&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   57899   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design, Synthesis, and Biological Activity of m-Tyrosine-Based 16- and 17-Membered Macrocyclic Inhibitors of Hepatitis C Virus NS3 Serine Protease</p>

    <p>          Chen, KX, Njoroge, FG, Pichardo, J, Prongay, A, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          J Med Chem <b>2005</b>.  48(20): 6229-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190750&amp;dopt=abstract</a> </p><br />

    <p>13.   57900   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Clevudine inhibits hepatitis delta virus viremia: a pilot study of chronically infected woodchucks</p>

    <p>          Casey, J, Cote, PJ, Toshkov, IA, Chu, CK, Gerin, JL, Hornbuckle, WE, Tennant, BC, and Korba, BE</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4396-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189132&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189132&amp;dopt=abstract</a> </p><br />

    <p>14.   57901   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus replication by antimonial compounds</p>

    <p>          Hwang, DR, Lin, RK, Leu, GZ, Lin, TY, Lien, TW, Yu, MC, Yeh, CT, and Hsu, JT</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4197-202</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189098&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189098&amp;dopt=abstract</a> </p><br />

    <p>15.   57902   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase inhibitor-resistant influenza viruses may differ substantially in fitness and transmissibility</p>

    <p>          Yen, HL, Herlocher, LM, Hoffmann, E, Matrosovich, MN, Monto, AS, Webster, RG, and Govorkova, EA</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4075-84</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189083&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189083&amp;dopt=abstract</a> </p><br />

    <p>16.   57903   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of nucleosides as prodrugs and antiviral agents</p>

    <p>          Roberts, Christopher D, Keicher, Jesse D, and Dyatkina, Natalia B</p>

    <p>          PATENT:  US <b>20050215511</b>  ISSUE DATE: 20050929</p>

    <p>          APPLICATION: 2004-53973  PP: 58 pp., Cont.-in-part of U.S. Ser. No. 861,311.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   57904   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Methods for preparing 7-(2&#39;-substituted-b-D-ribofuranosyl)-4-(NR2R3)-5-(substituted ethyn-1-yl)-pyrrolo[2,3-d]pyrimidine derivatives as antiviral agents</p>

    <p>          Roberts, Christopher D, Keicher, Jesse D, and Dyatkina, Natalia B</p>

    <p>          PATENT:  US <b>20050215510</b>  ISSUE DATE: 20050929</p>

    <p>          APPLICATION: 2004-53137  PP: 28 pp., Cont.-in-part of U.S. Ser. No. 861,311.</p>

    <p>          ASSIGNEE:  (Genelabs Technologies, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   57905   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          An orally bioavailable antipoxvirus compound (ST-246) inhibits extracellular virus formation and protects mice from lethal orthopoxvirus Challenge</p>

    <p>          Yang, G, Pevear, DC, Davies, MH, Collett, MS, Bailey, T, Rippen, S, Barone, L, Burns, C, Rhodes, G, Tohan, S, Huggins, JW, Baker, RO, Buller, RL, Touchette, E, Waller, K, Schriewer, J, Neyts, J, DeClercq, E, Jones, K, Hruby, D, and Jordan, R</p>

    <p>          J Virol <b>2005</b>.  79(20): 13139-49</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189015&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189015&amp;dopt=abstract</a> </p><br />

    <p>19.   57906   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of heterogeneous nuclear ribonucleoprotein K to inhibit the replication of hepatitis viruses B and C by binding to cis-regulatory elements</p>

    <p>          Poh, Lisa Ng Fong and Ren, Ee Chee</p>

    <p>          PATENT:  WO <b>2005059138</b>  ISSUE DATE:  20050630</p>

    <p>          APPLICATION: 2004  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Agency for Science, Technology and Research Singapore</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   57907   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Binding-site characterization and resistance to a class of non-nucleoside inhibitors of the HCV NS5B polymerase</p>

    <p>          Kukolj, G, McGibbon, GA, McKercher, G, Marquis, M, Lefebvre, S, Thauvette, L, Gauthier, J, Goulet, S, Poupart, MA, and Beaulieu, PL</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188890&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188890&amp;dopt=abstract</a> </p><br />

    <p>21.   57908   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Human cytomegalovirus-inhibitory flavonoids: Studies on antiviral activity and mechanism of action</p>

    <p>          Evers, DL, Chao, CF, Wang, X, Zhang, Z, Huong, SM, and Huang, ES</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188329&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188329&amp;dopt=abstract</a> </p><br />

    <p>22.   57909   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Ectromelia virus: the causative agent of mousepox</p>

    <p>          Esteban, DJ and Buller, RM</p>

    <p>          J Gen Virol <b>2005</b>.  86(Pt 10): 2645-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16186218&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16186218&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>23.   57910   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of phosphonated isoxazolidinyl nucleoside analogs with antiviral activity</p>

    <p>          Chiacchio, Ugo, Corsaro, Antonino, Iannazzo, Daniela, Macchi, Beatrice, Mastino, Antonio, Piperno, Anna, Rescifina, Antonio, Romeo, Giovanni, and Romeo, Roberto</p>

    <p>          PATENT:  WO <b>2005082896</b>  ISSUE DATE:  20050909</p>

    <p>          APPLICATION: 2005  PP: 29 pp.</p>

    <p>          ASSIGNEE:  (Universita&#39; Degli Studi di Catania, Italy and Universita&#39; Degli Studi di Messina)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>24.   57911   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          High-Throughput Cell-Based Screening for Hepatitis C Virus NS3/4A Protease Inhibitors</p>

    <p>          Lee, JC, Yu, MC, Lien, TW, Chang, CF, and Hsu, JT</p>

    <p>          Assay Drug Dev Technol <b>2005</b>.  3(4): 385-392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180993&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180993&amp;dopt=abstract</a> </p><br />

    <p>25.   57912   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New approaches and therapeutic modalities for the treatment of patients with chronic hepatitis C</p>

    <p>          Cornberg, M and Manns, MP</p>

    <p>          Ann Hepatol <b>2005</b>.  4(3): 144-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16177653&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16177653&amp;dopt=abstract</a> </p><br />

    <p>26.   57913   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          4-Oxo-4,7-dihydrothieno[2,3-b]pyridines as Non-Nucleoside Inhibitors of Human Cytomegalovirus and Related Herpesvirus Polymerases</p>

    <p>          Schnute, Mark E, Cudahy, Michele M, Brideau, Roger J, Homa, Fred L, Hopkins, Todd A, Knechtel, Mary L, Oien, Nancee L, Pitts, Thomas W, Poorman, Roger A, Wathen, Michael W, and Wieber, Janet L</p>

    <p>          Journal of Medicinal Chemistry <b>2005</b>.  48(18): 5794-5804</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   57914   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Both antigen optimization and lysosomal targeting are required for enhanced anti-tumour protective immunity in a human papillomavirus E7-expressing animal tumour model</p>

    <p>          Kim, MS and Sin, JI</p>

    <p>          Immunology <b>2005</b>.  116(2): 255-66</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162274&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16162274&amp;dopt=abstract</a> </p><br />

    <p>28.   57915   DMID-LS-103; PUBMED-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Capsid-targeted viral inactivation can destroy dengue 2 virus from within in vitro</p>

    <p>          Qin, CF and Qin, ED</p>

    <p>          Arch Virol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16155726&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16155726&amp;dopt=abstract</a> </p><br />

    <p>29.   57916   DMID-LS-103; WOS-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Potential Impact of Antiviral Drug Use During Influenza Pandemic</p>

    <p>          Gani, R. <i>et al.</i></p>

    <p>          Emerging Infectious Diseases <b>2005</b>.  11(9): 1355-1362</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231591400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231591400004</a> </p><br />

    <p>30.   57917   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Cytotoxicity and antiviral activity of a lignan extracted from Larrea divaricata</p>

    <p>          Konigheim, BS, Goleniowski, ME, and Contigiani, MS</p>

    <p>          Drug Design Reviews--Online <b>2005</b>.  2(1): 81-83</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>31.   57918   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Studies on the antiviral and cytotoxic activity of Schiff bases derived from 1,2-bis-(o- and p-aminophenoxy)ethane and salicylaldehyde</p>

    <p>          Bulut, Hakan, Karatepe, Mustafa, Temel, Hamdi, Sekerc, Memet, and Koparir, Metin</p>

    <p>          Asian Journal of Chemistry <b>2005</b>.  17(4): 2793-2796</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   57919   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening of antiviral drugs, and pharmaceutical compositions containing 2-thioxothiazolidinone derivatives</p>

    <p>          Gregor, Paul, Harris, Nicholas, and Zhuk, Regina</p>

    <p>          PATENT:  WO <b>2005089067</b>  ISSUE DATE:  20050929</p>

    <p>          APPLICATION: 2005  PP: 52 pp.</p>

    <p>          ASSIGNEE:  (Rimonyx Pharmaceuticals Ltd., Israel</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>33.   57920   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          CycloSal-pronucleotides of brivudine monophosphate - highly active antiviral agents</p>

    <p>          Meier, Chris, Meerbach, Astrid, and Wutzler, Peter</p>

    <p>          Current Medicinal Chemistry: Anti-Infective Agents <b>2005</b>.  4(4): 317-335</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   57921   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral agents from traditional Chinese medicine against herpes simplex virus</p>

    <p>          Cheng, Chuen-Lung and Xu, Hong-Xi</p>

    <p>          Journal of Traditional Medicines <b>2005</b>.  22(Suppl. 1): 133-137</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   57922   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral compositions comprising CCL5 (RANTES) fragments or mimetics, or CC chemokine receptor antagonists for inhibiting paramyxovirus infection</p>

    <p>          Hancock, Gerald Ervin and Tebbey, Paul William</p>

    <p>          PATENT:  WO <b>2005066205</b>  ISSUE DATE:  20050721</p>

    <p>          APPLICATION: 2004  PP: 82 pp.</p>

    <p>          ASSIGNEE:  (Wyeth, John and Brother Ltd. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>36.   57923   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of N4-substituted derivatives of (S)-1-[3-hydroxy-2-(phosphonomethoxy)propyl]cytosine (HPMPC, cidofovir, vistide)</p>

    <p>          Chalupova, Sarka and Holy, Antonin</p>

    <p>          Collection Symposium Series <b>2005</b>.  7(Chemistry of Nucleic Acid Components): 387-388</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   57924   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phosphonates, monophosphonamidates, bisphosphonamidates for the treatment of viral diseases</p>

    <p>          Cheng, Xiaqin, Cook, Gary P, Desai, Manoj C, Doerffler, Edward, He, Gong-Xin, Kim, Choung U, Lee, William A, Rohloff, John C, Wang, Jianying, and Yang, Zheng-Yu</p>

    <p>          PATENT:  WO <b>2005066189</b>  ISSUE DATE:  20050721</p>

    <p>          APPLICATION: 2004  PP: 161 pp.</p>

    <p>          ASSIGNEE:  (Gilead Sciences, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>38.   57925   DMID-LS-103; SCIFINDER-DMID-10/11/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Murine polyomavirus-VP1 virus-like particles immunize against some polyomavirus-induced tumors</p>

    <p>          Franzen, Andrea Vlastos, Tegersted, Karin, Hollaenderova, Dana, Forstova, Jitka, Ramqvist, Torbjoern, and Dalianis, Tina</p>

    <p>          In Vivo <b>2005</b>.  19(2): 323-326</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
