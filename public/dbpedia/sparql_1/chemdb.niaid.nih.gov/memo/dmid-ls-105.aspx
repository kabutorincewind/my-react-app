

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-105.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+QhLP+HjURoAlChWnw2xUmNlMgh8zFLBRwLzVQgHNdoh/0IJZjwNdUTY8uAavubK1E0OhDs4EsaUMr/agXmIdDILWCiGM59CnC2wnHBJ2uspZJrhbX3HSsONuTE=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4E6E9551" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-105-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     57952   DMID-LS-105; WOS-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          N-4-Hydroxycytosine Dioxolane Nucleosides and Their Activity Against Hepatitis B Virus</p>

    <p>          Du, J. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(8): 1209-1214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232691900005</a> </p><br />

    <p>2.     57953   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Pharmacological enhancement and manufacturing method of antiviral compound</p>

    <p>          Gong, Jiao and Jiang, Wei</p>

    <p>          PATENT:  US <b>20050181075</b>  ISSUE DATE: 20050818</p>

    <p>          APPLICATION: 2003-62990</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     57954   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          NOVEL AND SPECIFIC INHIBITORS OF A POXVIRUS TYPE I TOPOISOMERASE</p>

    <p>          Bond, A, Reichert, Z, and Stivers, JT</p>

    <p>          Mol Pharmacol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16267207&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16267207&amp;dopt=abstract</a> </p><br />

    <p>4.     57955   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Structural insights into SARS coronavirus proteins</p>

    <p>          Bartlam, M, Yang, H, and Rao, Z</p>

    <p>          Curr Opin Struct Biol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263266&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16263266&amp;dopt=abstract</a> </p><br />

    <p>5.     57956   DMID-LS-105; WOS-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          New Direct Antivirals Against Hepatitis C</p>

    <p>          Manns, M.</p>

    <p>          Antiviral Therapy <b>2004</b>.  9(6): H11-H12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000171">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231616000171</a> </p><br />

    <p>6.     57957   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Anti herpes simplex virus activity of lactoferrin/lactoferricin - an example of antiviral activity of antimicrobial protein/peptide</p>

    <p>          Jenssen, H</p>

    <p>          Cell Mol Life Sci <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16261265&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16261265&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>7.     57958   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Tetrahydrobenzothiophene inhibitors of hepatitis C virus NS5B polymerase</p>

    <p>          Laporte, MG, Lessen, TA, Leister, L, Cebzanov, D, Amparo, E, Faust, C, Ortlip, D, Bailey, TR, Nitz, TJ, Chunduru, SK, Young, DC, and Burns, CJ</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16260131&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16260131&amp;dopt=abstract</a> </p><br />

    <p>8.     57959   DMID-LS-105; WOS-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Study of Conformationally Restricted 3 &#39;-Deoxy-3 &#39;,4 &#39;-Exo-Methylene Nucleoside Analogues</p>

    <p>          Gagneron, J., Gosselin, G., and Mathe, C.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(5-7): 383-385</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000232473500010</a> </p><br />

    <p>9.     57960   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p><b>          An N-Linked Glycoprotein with {alpha}(2,3)-Linked Sialic Acid Is a Receptor for BK Virus</b> </p>

    <p>          Dugan, AS, Eash, S, and Atwood, WJ</p>

    <p>          J Virol <b>2005</b>.  79(22): 14442-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254379&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254379&amp;dopt=abstract</a> </p><br />

    <p>10.   57961   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of polyprotein processing and RNA replication of human rhinovirus by pyrrolidine dithiocarbamate involves metal ions</p>

    <p>          Krenn, BM, Holzer, B, Gaudernak, E, Triendl, A, van Kuppeveld, FJ, and Seipelt, J</p>

    <p>          J Virol <b>2005</b> .  79(22): 13892-13899</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254325&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254325&amp;dopt=abstract</a> </p><br />

    <p>11.   57962   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A cell-based high-throughput assay for screening inhibitors of human papillomavirus-16 long control region activity</p>

    <p>          Lembo, D, Donalisio, M, De, Andrea M, Cornaglia, M, Scutera, S, Musso, T, and Landolfo, S</p>

    <p>          FASEB J <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254045&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16254045&amp;dopt=abstract</a> </p><br />

    <p>12.   57963   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A survey of antiviral drugs for bioweapons</p>

    <p>          Goff, Arthur J and Paragas, Jason</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2005</b>.  16(5): 283-294</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>13.   57964   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Randomized, placebo-controlled trial of nonpegylated and pegylated forms of recombinant human alpha interferon 2a for suppression of dengue virus viremia in rhesus monkeys</p>

    <p>          Ajariyakhajorn, C, Mammen, MP Jr, Endy, TP, Gettayacamin, M, Nisalak, A, Nimmannitya, S, and Libraty, DH</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(11): 4508-4514</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251289&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16251289&amp;dopt=abstract</a> </p><br />

    <p>14.   57965   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Rift valley fever virus NSs mRNA is transcribed from an incoming anti-viral-sense S RNA segment</p>

    <p>          Ikegami, Tetsuro, Won, Sungyong, Peters, CJ, and Makino, Shinji</p>

    <p>          Journal of Virology <b>2005</b>.  79(18): 12106-12111</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   57966   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A New Lead for Nonpeptidic Active-Site-Directed Inhibitors of the Severe Acute Respiratory Syndrome Coronavirus Main Protease Discovered by a Combination of Screening and Docking Methods</p>

    <p>          Kaeppler, U, Stiefl, N, Schiller, M, Vicik, R, Breuning, A, Schmitz, W, Rupprecht, D, Schmuck, C, Baumann, K, Ziebuhr, J, and Schirmeister, T</p>

    <p>          J Med Chem <b>2005</b>.  48(22): 6832-6842</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250642&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250642&amp;dopt=abstract</a> </p><br />

    <p>16.   57967   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Design and Synthesis of Peptidomimetic Severe Acute Respiratory Syndrome Chymotrypsin-like Protease Inhibitors</p>

    <p>          Ghosh, AK, Xi, K, Ratia, K, Santarsiero, BD, Fu, W, Harcourt, BH, Rota, PA, Baker, SC, Johnson, ME, and Mesecar, AD</p>

    <p>          J Med Chem <b>2005</b>.  48(22): 6767-6771</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16250632&amp;dopt=abstract</a> </p><br />

    <p>17.   57968   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and anti-hepatitis C virus activity of nucleoside derivatives of N3, 5&#39;-anhydro-4-(beta-D-ribofuranosyl)-8-aza-purin-2-ones</p>

    <p>          Hassan, AE, Wang, P, McBrayer, TR, Tharnish, PM, Stuyver, LJ, Schinazi, RF, Otto, MJ, and Watanabe, KA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 961-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248072&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   57969   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of N3,5&#39;-cyclo-4-(beta-D-ribofuranosyl)-vic-triazolo[4,5-b]pyridin-5-one and its 3&#39;-deoxysugar analogue as potential anti-hepatitis C virus agents</p>

    <p>          Wang, P, Hollecker, L, Pankiewicz, KW, Patterson, SE, Whitaker, T, McBrayer, TR, Tharnish, PM, Stuyver, LJ, Schinazi, RF, Otto, MJ, and Watanabe, KA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 957-60</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248071&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248071&amp;dopt=abstract</a> </p><br />

    <p>19.   57970   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and evaluation of anti-HSV activity of new 5-alkynyl-2&#39;-deoxyuridines</p>

    <p>          Pchelintseva, AA, Skorobogatyj, MV, Petrunina, AL, Andronova, VL, Galegov, GA, Astakhova, IV, Ustinov, AV, Malakhov, AD, and Korshun, VA</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 923-926</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248063&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248063&amp;dopt=abstract</a> </p><br />

    <p>20.   57971   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and in vitro anti-hcv activity of beta-D- and 1-2&#39;-deoxy-2&#39;-fluororibonucleosides</p>

    <p>          Shi, J, Du, J, Ma, T, Pankiewicz, KW, Patterson, SE, Hassan, AE, Tharnish, PM, McBrayer, TR, Lostia, S, Stuyver, LJ, Watanabe, KA, Chu, CK, Schinazi, RF, and Otto, MJ</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 875-9</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248053&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248053&amp;dopt=abstract</a> </p><br />

    <p>21.   57972   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Nm 283, an efficient prodrug of the potent anti-HCV agent 2&#39;-C-methylcytidine</p>

    <p>          Pierra, C, Benzaria, S, Amador, A, Moussa, A, Mathieu, S, Storer, R, and Gosselin, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 767-770</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248033&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248033&amp;dopt=abstract</a> </p><br />

    <p>22.   57973   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Sense antiviral oligonucleotide analogs and method for treating ssRNA viral infection</p>

    <p>          Iversen, Patrick L</p>

    <p>          PATENT:  WO <b>2005013905</b>  ISSUE DATE:  20050217</p>

    <p>          APPLICATION: 2004  PP: 81 pp.</p>

    <p>          ASSIGNEE:  (Avi Biopharma, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>23.   57974   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 5-aza-7-deazaguanine nucleoside derivatives as potential anti-flavivirus agents</p>

    <p>          Dukhan, D, Leroy, F, Peyronnet, J, Bosc, E, Chaves, D, Durka, M, Storer, R, La, Colla P, Seela, F, and Gosselin, G</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 671-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248011&amp;dopt=abstract</a> </p><br />

    <p>24.   57975   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of N- and O-alkylated bicyclic furanopyrimidines as non-nucleosidic inhibitors of human cytomegalovirus</p>

    <p>          Kelleher, MR,  McGuigan, C, Andrei, G, Snoeck, R, De Clercq, E, and Balzarini, J</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 639-641</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248003&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16248003&amp;dopt=abstract</a> </p><br />

    <p>25.   57976   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Targeted therapy of respiratory syncytial virus by 2-5A antisense</p>

    <p>          Cramer, H, Okicki, JR, Kuang, M, and Xu, Z</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 497-501</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247978&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247978&amp;dopt=abstract</a> </p><br />

    <p>26.   57977   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          West Nile virus replication interferes with both poly(I:C)-induced interferon gene transcription and response to interferon treatment</p>

    <p>          Scholle, Frank and Mason, Peter W</p>

    <p>          Virology <b>2005</b>.  342(1): 77-87</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>27.   57978   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral potential of a new generation of acyclic nucleoside phosphonates, the 6-[2-(phosphonomethoxy)alkoxy]-2,4-diaminopyrimidines</p>

    <p>          De Clercq, E,  Andrei, G, Balzarini, J, Leyssen, P, Naesens, L, Neyts, J, Pannecouque, C, Snoeck, R, Ying, C, Hockova, D, and Holy, A</p>

    <p>          Nucleosides Nucleotides Nucleic Acids <b>2005</b>.  24(5-7): 331-341</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247948&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16247948&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>28.   57979   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide inhibitors of dengue virus NS3 protease. Part 2: SAR study of tetrapeptide aldehyde inhibitors</p>

    <p>          Yin, Z, Patel, SJ, Wang, WL, Chan, WL, Ranga, Rao KR, Wang, G, Ngew, X, Patel, V, Beer, D, Knox, JE, Ma, NL, Ehrhardt, C, Lim, SP, Vasudevan, SG, and Keller, TH</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16246563&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16246563&amp;dopt=abstract</a> </p><br />

    <p>29.   57980   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Peptide inhibitors of dengue virus NS3 protease. Part 1: Warhead</p>

    <p>          Yin, Z, Patel, SJ, Wang, WL, Wang, G, Chan, WL, Rao, KR, Alam, J, Jeyaraj, DA, Ngew, X, Patel, V, Beer, D, Lim, SP, Vasudevan, SG, and Keller, TH</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16246553&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16246553&amp;dopt=abstract</a> </p><br />

    <p>30.   57981   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and biological activity of 1H-benzotriazole and 1H-benzimidazole analogues--inhibitors of the NTpase/helicase of HCV and of some related Flaviviridae</p>

    <p>          Bretner, M, Baier, A, Kopanska, K, Najda, A, Schoof, A, Reinholz, M, Lipniacki, A, Piasek, A, Kulikowski, T, and Borowski, P</p>

    <p>          Antivir Chem Chemother <b>2005</b>.  16(5): 315-326</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245647&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16245647&amp;dopt=abstract</a> </p><br />

    <p>31.   57982   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Use of antiviral agents and other measures in an influenza pandemic</p>

    <p>          Groeneveld, K and van der Noordaa, J</p>

    <p>          Neth J Med <b>2005</b>.  63(9): 339-343</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16244380&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16244380&amp;dopt=abstract</a> </p><br />

    <p>32.   57983   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Case history: Acyclic nucleoside phosphonates: a key class of antiviral drugs</p>

    <p>          De Clercq, Erik and Holy, Antonin</p>

    <p>          Nature Reviews Drug Discovery <b>2005</b>.  4(11): 928-940</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>33.   57984   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of 9-(2-fatty acid ester ethoxymethyl)guanines as antiviral agents for treatment of hepatitis B virus and herpes virus</p>

    <p>          Zhou, Pei, Cai, Qinsheng, Huang, Hai, Li, Jiyang, and Zhou, Wei</p>

    <p>          PATENT:  CN <b>1563007</b>  ISSUE DATE: 20050112</p>

    <p>          APPLICATION: 1001-7500  PP: 14 pp.</p>

    <p>          ASSIGNEE:  (Fudan University, Peop. Rep. China</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>34.   57985   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Preparation of imidazol-4-yl-N&#39;-phenylureas as antiviral agents</p>

    <p>          Zimmermann, Holger, Brueckner, David, Heimbach, Dirk, Hendrix, Martin, Henninger, Kerstin, Hewlett, Guy, Rosentreter, Ulrich, Keldenich, Joerg, Lang, Dieter, and Radtke, Martin</p>

    <p>          PATENT:  WO <b>2005092865</b>  ISSUE DATE:  20051006</p>

    <p>          APPLICATION: 2005  PP: 124 pp.</p>

    <p>          ASSIGNEE:  (Bayer Healthcare A.-G., Germany</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   57986   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Screening of electrophilic compounds yields an aziridinyl peptide as new active-site directed SARS-CoV main protease inhibitor</p>

    <p>          Martina, E, Stiefl, N, Degel, B, Schulz, F, Breuning, A, Schiller, M, Vicik, R, Baumann, K, Ziebuhr, J, and Schirmeister, T</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.  15(24): 5365-9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216498&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16216498&amp;dopt=abstract</a> </p><br />

    <p>36.   57987   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Antiviral protein OVT102 derived from cyanovirin with improved properties and methods therefor</p>

    <p>          Levine, Howard L and Kerns, William</p>

    <p>          PATENT:  WO <b>2005058229</b>  ISSUE DATE:  20050630</p>

    <p>          APPLICATION: 2004  PP: 32 pp.</p>

    <p>          ASSIGNEE:  (Omniviral Therapeutics LLC, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   57988   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of dengue virus translation and RNA synthesis by a morpholino oligomer targeted to the top of the terminal 3&#39; stem-loop structure</p>

    <p>          Holden, KL, Stein, DA, Pierson, TC, Ahmed, AA, Clyde, K, Iversen, PL, and Harris, E</p>

    <p>          Virology <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214197&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16214197&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>38.   57989   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          A QSAR study on influenza neuraminidase inhibitors</p>

    <p>          Verma, RP and Hansch, C</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213733&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213733&amp;dopt=abstract</a> </p><br />

    <p>39.   57990   DMID-LS-105; PUBMED-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of C-6 substituted pyrazolo[1,5-a]pyridines with potent activity against herpesviruses</p>

    <p>          Allen, SH, Johns, BA, Gudmundsson, KS, Freeman, GA, Boyd, FL Jr, Sexton, CH, Selleseth, DW, Creech, KL, and Moniri, KR</p>

    <p>          Bioorg Med Chem <b>2005</b>.  14: 944-954</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213142&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16213142&amp;dopt=abstract</a> </p><br />

    <p>40.   57991   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Murine interferon lambdas (type III interferons) exhibit potent antiviral activity in vivo in a poxvirus infection model</p>

    <p>          Bartlett, Nathan W, Buttigieg, Karen, Kotenko, Sergei V, and Smith, Geoffrey L</p>

    <p>          Journal of General Virology <b>2005</b>.  86(6): 1589-1596</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   57992   DMID-LS-105; SCIFINDER-DMID-11/7/2005 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Method of regulating phosphorylation of sr protein and antiviral agents comprising sr protein activity regulator as the active ingredient</p>

    <p>          Hagiwara, Masatoshi, Fukuhara, Takeshi, Suzuki, Masaaki, and Hosoya, Takamitsu</p>

    <p>          PATENT:  WO <b>2005063293</b>  ISSUE DATE:  20050714</p>

    <p>          APPLICATION: 2004  PP: 122 pp.</p>

    <p>          ASSIGNEE:  (Japan)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
