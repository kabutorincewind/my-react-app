

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2011-09-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="67Vo8bjavwOyZ3C0mvd/igri6RKz1fJxLYyD1KvZKdHvKUvzmVDp1I+VGLWPaeA5gXWpG1ohoC5FcT6hlEgxxRif4wseFWVuB19tytBcIm5d2sxHngfEql/2eHo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="52C2BFF1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">

<div class="Section1">

<h1 class="memofmt2-h1">Other Viruses Citation List: </h1>

<h1 class="memofmt2-h1">September 16 – September 29, 2011</h1>

<p class="memofmt2-2">Hepatitis B virus</p>

<p class="NoSpacing">1.&nbsp; <a href="http://www.ncbi.nlm.nih.gov/pubmed/21930377">Antiviral Activity of Novel 2'-Fluoro-6'-methylene-carbocyclic adenosine against Wild-type and Drug-resistant Hepatitis B Virus Mutants.</a> Wang, J., U.S. Singh, R.K. Rawal, M. Sugiyama, J. Yoo, A.K. Jha, M. Scroggin, Z. Huang, M.G. Murray, R. Govindarajan, Y. Tanaka, B. Korba, and C.K. Chu,&nbsp; Bioorganic &amp; Medicinal Chemistry Letters, 2011. <b>[Epub ahead of print]</b>;PMID[21930377]. <br /><b>[PubMed]</b>. OV_0916-092911.</p>

<p class="memofmt2-2">Hepatitis C virus</p>

<p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21893371">Synthesis and SAR Optimization of Diketo acid Pharmacophore for HCV NS5B Polymerase Inhibition.</a> Bhatt, A., K.R. Gurukumar, A. Basu, M.R. Patel, N. Kaushik-Basu, and T.T. Talele, European Journal of Medicinal Chemistry, 2011. 46(10): p. 5138-5145; PMID[21893371]. <br />
<b>[PubMed]</b>. OV_0916-092911.</p>

<p class="NoSpacing">&nbsp;</p>

<p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21925774">2,6-bis-Arylmethyloxy-5-hydroxychromones with Antiviral Activity against both Hepatitis C Virus (HCV) and SARS-associated Coronavirus (SCV).</a> Kim, M.K., M.S. Yu, H.R. Park, K.B. Kim, C. Lee, S.Y. Cho, J. Kang, H. Yoon, D.E. Kim, H. Choo, Y.J. Jeong, and Y. Chong, European Journal of Medicinal Chemistry, 2011. <b>[Epub ahead of print]</b>; PMID[21925774]. <br /> <b>[PubMed]</b>. OV_0916-092911.</p>

<p class="memofmt2-2">HSV-1</p>

<p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21947401">Digallate Dimers of (-)-Epigallocatechin Gallate Inactivate Herpes Simplex Virus</a>. Isaacs, C.E., W. Xu, G. Merz, S. Hillier, L. Rohan, and G.Y. Wen, Antimicrobial Agents and Chemotherapy, 2011. <b>[Epub ahead of print]</b>; PMID[21947401]. <br /> <b>[PubMed]</b>. OV_0916-092911.</p>

<p class="memofmt2-2">Citations from the ISI Web of Knowledge Listings for O.V.</p>

<p class="NoSpacing">5. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294554300004">Design and Synthesis of Novel Carbocyclic Versions of 2 '-Spirocyclopropyl ribonucleosides as Potent anti-HCV Agents.</a> Oh, C.H., E. Kim, and J.H. Hong, Nucleosides Nucleotides &amp; Nucleic Acids, 2011. 30(6): p. 423-439; ISI[000294554300004]. <br /> <b>[WOS]</b>. OV_0916-092911. </p>
<p class="NoSpacing">&nbsp;</p>

<p class="NoSpacing">6. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294711100007"> Synthesis and Biological Evaluation of Matijing-Su Derivatives as Potent anti-HBV Agents.</a> Qiu, J.Y., B.X. Xu, Z.M. Huang, W.D. Pan, P.X. Cao, C.X. Liu, X.J. Hao, B.A. Song, and G.Y. Liang,&nbsp; Bioorganic &amp; Medicinal Chemistry, 2011. 19(18): p. 5352-5360; ISI[000294711100007]. <br /> <b>[WOS]</b>. OV_0916-092911.</p>
<p class="NoSpacing">&nbsp;</p>

<p class="NoSpacing">7. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294375700038"> Synthesis and Characterization of Picomolar Inhibitors of HCV NS5A in Hepatocytes.</a> Schinazi, R.F., H.W. Zhang, L. Zhou, F. Amblard, D.R. Bobeck, J. Shi, T. Whitaker, T.R. McBrayer, P.M. Tharnish, and S.J. Coats, Antiviral Therapy, 2011. 16(4): p. A22-A22; ISI[000294375700038].<br /> <b>[WOS]</b>. OV_0916-092911. </p>  
<p class="NoSpacing">&nbsp;</p>

<p class="NoSpacing">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294375800014"> Argonaute-2 Enhances Suppression of Human Cytomegalovirus Replication by Polycistronic Short Hairpin RNAs Targeting UL46, UL70 and UL122.</a> Shao, P.L., M.Y. Lu, Y.J. Liau, M.F. Chao, L.Y. Chang, C.Y. Lu, C.L. Kao, S.Y. Chang, Y.H. Chi, and L.M. Huang, Antiviral therapy, 2011. 16(5): p. 741-749; ISI[000294375800014].</p>
<p class="NoSpacing"><b>[WOS]</b>. OV_0916-092911. </p>
<p class="NoSpacing">&nbsp;</p>

<p class="NoSpacing">9. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000294356000002"> IL-29/IL-28A Suppress HSV-1 Infection of Human NT2-N Neurons.</a>. Zhou, L., J.L. Li, X. Wang, L. Ye, W. Hou, J. Ho, H. Li, and W.Z. Ho, Journal of Neurovirology, 2011. 17(3): p. 212-219; ISI[000294356000002]. <br /> <b>[WOS]</b>. OV_0916-092911.</p> 
    
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
