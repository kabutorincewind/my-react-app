

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2016-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jZ3+64a0JctJ91zBnbF+wGIfY0U/Io3xasJp/YzT4YjgJ2PCv55hDXv42sao8bpdl3BMPuGdUA1fSK8mHOigzzp1/iztIBgoxc8e3D5ulvZjhhc61doowekPHwo=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7C86A27F" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citations List: June, 2016</h1>

    <h2>Literature Citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27283735">Curcumin Inhibits HIV-1 by Promoting Tat Protein Degradation.</a> Ali, A. and A.C. Banerjea. Scientific Reports, 2016. 6(27539): 9pp. PMID[27283735]. PMCID [PMC4901322].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27104111">Synthesis, Anti-inflammatory, Bactericidal Activities and Docking Studies of Novel 1,2,3-Triazoles Derived from Ibuprofen Using Click Chemistry.</a>  Angajala, K.K., S. Vianala, R. Macha, M. Raghavender, M.K. Thupurani, and P.J. Pathi. SpringerPlus, 2016. 5(423): 15pp. PMID[27104111]. PMCID[PMC4828371].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">3. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000377595200011">Microwave Assisted Synthesis of (E)-1-(2-((1-Benzyl-1H-1,2,3-triazol-4-yl)methoxy)phenyl)-3-(9-ethyl-9H-carbazol-3-yl)prop-2-en-1-ones and Their Antimicrobial Activity.</a> Ashok, D., S. Ravi, B.V. Lakshmi, A. Ganesh, and S. Adam. Russian Journal of Bioorganic Chemistry, 2016. 42(3): p. 323-331. ISI[000377595200011].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27105029">2-Hydroxyisoquinoline-1,3(2H,4H)-diones (HIDs) as Human Immunodeficiency Virus Type 1 Integrase Inhibitors: Influence of the Alkylcarboxamide Substitution of Position 4.</a>Billamboz, M., V. Suchaud, F. Bailly, C. Lion, M.-L. Andreola, F. Christ, Z. Debyser, and P. Cotelle. European Journal of Medicinal Chemistry, 2016. 117: p. 256-268. PMID[27105029].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27315478">Bispecific anti-HIV-1 Antibodies with Enhanced Breadth and Potency.</a>Bournazos, S., A. Gazumyan, M.S. Seaman, M.C. Nussenzweig, and J.V. Ravetch. Cell, 2016. 165(7): p. 1609-1620. PMID[27315478]. </p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27159886">Synthesis and Properties of Hemostatic and Bacteria-Responsive In Situ Hydrogels for Emergency Treatment in Critical Situations.</a> Bu, Y., L. Zhang, J. Liu, L. Zhang, T. Li, H. Shen, X. Wang, F. Yang, P. Tang, and D. Wu. ACSApplied Materials &amp; Interfaces, 2016. 8(20): p. 12674-12683. PMID[ 27159886].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27074418">HIV Capsid Assembly, Mechanism, and Structure.</a> Chen, B. Biochemistry, 2016. 55(18): p. 2539-2552. PMID[27074418].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375637600009">Design, Synthesis and anti-HIV-Reverse Transcriptase Activity of Novel Diaryl Thiazolindin-4-one Derivatives Possessing Amide Linkage on N-3 Position.</a> Chen Hua, Shao Jie, Zhu Mo, and Li Xiaoliu. Chinese Journal of Organic Chemistry, 2016. 36(3): p. 527-532. ISI[000375637600009]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27161874">Synthesis and Bioevaluation of Substituted Chalcones, Coumaranones and Other Flavonoids as anti-HIV Agents.</a>Cole, A.L., S. Hossain, A.M. Cole, and O.t. Phanstiel. Bioorganic &amp; Medicinal Chemistry, 2016. 24(12): p. 2768-2776. PMID[27161874].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27314381">Identification of Vimentin as a Potential Therapeutic Target against HIV Infection.</a>Fernandez-Ortega, C., A. Ramirez, D. Casillas, T. Paneque, R. Ubieta, M. Dubed, L. Navea, L. Castellanos-Serra, C. Duarte, V. Falcon, O. Reyes, H. Garay, E. Silva, E. Noa, Y. Ramos, V. Besada, and L. Betancourt. Viruses, 2016. 8(98): 19pp. PMID[27314381].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26922346">Gold(I) Complex of 1,1&#39;-bis(Diphenylphosphino) Ferrocene-quinoline Conjugate: A Virostatic Agent against HIV-1.</a>Gama, N., K. Kumar, E. Ekengard, M. Haukka, J. Darkwa, E. Nordlander, and D. Meyer. Biometals, 2016. 29(3): p. 389-397. PMID[26922346].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26905802">Definition of an 18-Mer Synthetic Peptide Derived from the GB Virus C E1 Protein as a New HIV-1 Entry Inhibitor</a>.Gomara, M.J., V. Sanchez-Merino, A. Paus, A. Merino-Mansilla, J.M. Gatell, E. Yuste, and I. Haro. Biochimica et Biophysica Acta, 2016. 1860(6): p. 1139-1148. PMID[26905802].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27234889">Systematic Evaluation of Methyl Ester Bioisosteres in the Context of Developing Alkenyldiarylmethanes (ADAMs) as Non-Nucleoside Reverse Transcriptase Inhibitors (NNRTIs) for anti-HIV-1 Chemotherapy.</a> Hoshi, A., T. Sakamoto, J. Takayama, M. Xuan, M. Okazaki, T.L. Hartman, R.W. Buckheit, Jr., C. Pannecouque, and M. Cushman. Bioorganic &amp; Medicinal Chemistry, 2016. 24(13): p. 3006-3022. PMID[27234889].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27017974">Synthesis of Seven-membered Iminosugars Fused Thiazolidin-4-one and Benzthiazinan-4-one and Their HIV-RT Inhibitory Activity.</a> Hou, Y., S. Xing, J. Shao, Z. Yin, L. Hao, T. Yang, H. Zhang, M. Zhu, H. Chen, and X. Li. Carbohydrate Research, 2016. 429: p. 105-112. PMID[27017974].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27315479">Engineered Bispecific Antibodies with Exquisite HIV-1-Neutralizing Activity.</a> Huang, Y., J. Yu, A. Lanzi, X. Yao, C.D. Andrews, L. Tsai, M.R. Gajjar, M. Sun, M.S. Seaman, N.N. Padte, and D.D. Ho. Cell, 2016. 165(7): p. 1621-1631. PMID[27315479].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375968200016">Design and Discovery of Novel Thiazole Acetamide Derivatives as anti-HIV Agent Towards AIDS Treatment via Inhibition of Reverse Transcriptase.</a>Jiang, Y.-K., X.-Y. Zhang, and W.-M. Jiang. Latin American Journal of Pharmacy, 2016. 35(3): p. 537-544. ISI[000375968200016]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26860867">New Approach for Inhibition of HIV Entry: Modifying CD4 Binding Sites by Thiolated Pyrimidine Derivatives.</a>Kanizsai, S., J. Ongradi, J. Aradi, and K. Nagy. Pathology &amp; Oncology Research, 2016. 22(3): p. 617-623. PMID[26860867].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26831929">Expression of HIV-encoded microRNA-Tar and Its Inhibitory Effect on Viral Replication in Human Primary Macrophages.</a> Li, L., H. Feng, Q. Da, H. Jiang, L. Chen, L. Xie, Q. Huang, H. Xiong, F. Luo, L. Kang, Y. Zeng, H. Hu, W. Hou, and Y. Feng. Archives of Virology, 2016. 161(5): p. 1115-1123. PMID[26831929].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000376482400012">Design, Synthesis and Activity Screening of Isopeptide Bond-tethered N Peptides as HIV-1 Fusion Inhibitors.</a> Li Xue, Lai Wenqing, Jiang Xifeng, Wang Chao, and Liu Keliang. Chemical Journal of Chinese Universities, 2016. 37(5): p. 881-885. ISI[000376482400012]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375563900004">Synthesis and Biological Evaluation of Phenylpropanoid Derivatives.</a>Liu, S., Y. Li, W. Wei, and J. Wei. Medicinal Chemistry Research, 2016. 25(6): p. 1074-1086. ISI[000375563900004]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27090171">Identification and Characterization of BMS-955176, a Second-generation HIV-1 Maturation Inhibitor with Improved Potency, Antiviral Spectrum, and Gag Polymorphic Coverage.</a>Nowicka-Sans, B., T. Protack, Z. Lin, Z. Li, S. Zhang, Y. Sun, H. Samanta, B. Terry, Z. Liu, Y. Chen, N. Sin, S.Y. Sit, J.J. Swidorski, J. Chen, B.L. Venables, M. Healy, N.A. Meanwell, M. Cockett, U. Hanumegowda, A. Regueiro-Ren, M. Krystal, and I.B. Dicker. Antimicrobial Agents and Chemotherapy, 2016. 60(7): p. 3956-3969. PMID[27090171]. PMCID[PMC4914680]. </p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26666935">Pharmacokinetic and Pharmacodynamic Evaluation Following Vaginal Application of IQB3002, a Dual-chamber Microbicide Gel Containing the Nonnucleoside Reverse Transcriptase Inhibitor IQP-0528 in Rhesus Macaques.</a> Pereira, L.E., P.M.M. Mesquita, A. Ham, T. Singletary, F. Deyounks, A. Martin, J. McNicholl, K.W. Buckheit, R.W. Buckheit, Jr., and J.M. Smith. Antimicrobial Agents and Chemotherapy, 2016. 60(3): p. 1393-1400. PMID[26666935]. PMCID[PMC4775942].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27326328">Discovery of BMS-955176, a Second Generation HIV-1 Maturation Inhibitor with Broad Spectrum Antiviral Activity.</a>Regueiro-Ren, A., Z. Liu, Y. Chen, N. Sin, S.Y. Sit, J.J. Swidorski, J. Chen, B.L. Venables, J. Zhu, B. Nowicka-Sans, T. Protack, Z. Lin, B. Terry, H. Samanta, S. Zhang, Z. Li, B.R. Beno, X.S. Huang, S. Rahematpura, D.D. Parker, R. Haskell, S. Jenkins, K.S. Santone, M.I. Cockett, M. Krystal, N.A. Meanwell, U. Hanumegowda, and I.B. Dicker. ACS Medicinal Chemistry Letters, 2016. 7(6): p. 568-572. PMID[27326328]. PMCID[PMC4904268].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000377597200001">Development and Identification of a Novel anti-HIV-1 Peptide Derived by Modification of the N-terminal Domain of HIV-1 Integrase.</a>Sala, M., A. Spensiero, F. Esposito, M.C. Scala, E. Vernieri, A. Bertamino, M. Manfra, A. Carotenuto, P. Grieco, E. Novellino, M. Cadeddu, E. Tramontano, D. Schols, P. Campiglia, and I.M. Gomez-Monterrey. Frontiers in Microbiology, 2016. 7(845): 9pp. ISI[000377597200001]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375518900002">Remarkable in Vitro anti-HIV Activity of New Silver(I)- and Gold(I)-n-heterocyclic Carbene Complexes. Synthesis, DNA Binding and Biological Evaluation</a>.Sanchez, O., S. Gonzalez, A.R. Higuera-Padilla, Y. Leon, D. Coll, M. Fernandez, P. Taylor, I. Urdanibia, H.R. Rangel, J.T. Ortega, W. Castro, and M. Cristina Goite. Polyhedron, 2016. 110: p. 14-23. ISI[000375518900002].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27149363">Synthesis, Molecular Docking and alpha-Glucosidase Inhibition of 5-Aryl-2-(6&#39;-nitrobenzofuran-2&#39;-yl)-1,3,4-Oxadiazoles.</a>Taha, M., N.H. Ismail, S. Imran, A. Wadood, F. Rahim, S.M. Saad, K.M. Khan, and A. Nasir. Bioorganic Chemistry, 2016. 66: p. 117-123. PMID[27149363].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000376305300013">Synthesis, and Prediction of Molecular Properties and Antimicrobial Activity of Some Acylhydrazones Derived from N-(Arylsulfonyl)methionine.</a>Tatar, E., S. Senkardes, H.E. Sellitepe, S.G. Kucukguzel, S.A. Karaoglu, A. Bozdeveci, E. De Clercq, C. Pannecouque, T. Ben Hadda, and I. Kucukguzel. Turkish Journal of Chemistry, 2016. 40(3): p. 510-534. ISI[000376305300013].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26810656">Ebselen, a Small-molecule Capsid Inhibitor of HIV-1 Replication.</a>Thenin-Houssier, S., I.M.S. de Vera, L. Pedro-Rosa, A. Brady, A. Richard, B. Konnick, S. Opp, C. Buffone, J. Fuhrmann, S. Kota, B. Billack, M. Pietka-Ottlik, T. Tellinghuisen, H. Choe, T. Spicer, L. Scampavia, F. Diaz-Griffero, D.J. Kojetin, and S.T. Valente. Antimicrobial Agents and Chemotherapy, 2016. 60(4): p. 2195-2208. PMID[26810656]. PMCID[PMC4808204].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27264714">Identification of Potent Maturation Inhibitors against HIV-1 Clade C.</a>Timilsina, U., D. Ghimire, B. Timalsina, T.J. Nitz, C.T. Wild, E.O. Freed, and R. Gaur. Scientific Reports, 2016. 6(27403): 6pp. PMID[27264714]. PMCID[PMC4893694]. </p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27092412">Progress and Prospects on DENV Protease Inhibitors.</a> Timiri, A.K., B.N. Sinha, and V. Jayaprakash. European Journal of Medicinal Chemistry, 2016. 117: p. 125-143. PMID[27092412].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27250526">Thiazolides Elicit Anti-viral Innate Immunity and Reduce HIV Replication.</a> Trabattoni, D., F. Gnudi, S.V. Ibba, I. Saulle, S. Agostini, M. Masetti, M. Biasin, J.F. Rossignol, and M. Clerici. Scientific Reports, 2016. 6(27148): 10pp. PMID[27250526]. PMCID[PMC4890011].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27007716">Cyanovirin-N Produced in Rice Endosperm Offers Effective Pre-exposure Prophylaxis against HIV-1(BaL) Infection In Vitro.</a> Vamvaka, E., A. Evans, K. Ramessar, L.R.H. Krumpe, R.J. Shattock, B.R. O&#39;Keefe, P. Christou, and T. Capell. Plant Cell Reports, 2016. 35(6): p. 1309-1319. PMID[27007716].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27257969">Nonhuman Primate IFITM Proteins are Potent Inhibitors of HIV and SIV.</a> Wilkins, J., Y.M. Zheng, J. Yu, C. Liang, and S.L. Liu. Plos One, 2016. 11(6): p. e0156739. PMID[27257969]. PMCID[PMC4892622]. </p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27275831">A Designed &quot;Nested&quot; Dimer of Cyanovirin-N Increases Antiviral Activity.</a> Woodrum, B.W., J. Maxwell, D.M. Allen, J. Wilson, L.R. Krumpe, A.A. Bobkov, R.B. Hill, K.V. Kibler, B.R. O&#39;Keefe, and G. Ghirlanda. Viruses, 2016. 8(6): 15pp. PMID[27275831].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/27158142">Development of Novel Proteasome Inhibitors Based on Phthalazinone Scaffold.</a> Yang, L., W. Wang, Q. Sun, F. Xu, Y. Niu, C. Wang, L. Liang, and P. Xu. Bioorganic &amp; Medicinal Chemistry Letters, 2016. 26(12): p. 2801-2805. PMID[27158142].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000375621800001">Application of Ligand- and Receptor-based Approaches for Prediction of the HIV-RT Inhibitory Activity of Fullerene Derivatives.</a> Yilmaz, H., L. Ahmed, B. Rasulev, and J. Leszczynski. Journal of Nanoparticle Research, 2016. 18(123): 12pp. ISI[000375621800001].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;amp;SrcAuth=Alerting&amp;amp;SrcApp=Alerting&amp;amp;DestApp=WOS&amp;amp;DestLinkType=FullRecord;UT=WOS:000377566000134">Small Molecule ONC201/TIC10 Inhibits HIV-1 Replication and Integration.</a>Zhao, R., B. Wu, Y. Huang, and J.C. Zheng. Journal of Neuroimmune Pharmacology, 2016. 11: p. S52-S52. ISI[000377566000134]. </p>

    <p class="plaintext"><b>[WOS]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26808478">HIV-1 Integrase Strand Transfer Inhibitors with Reduced Susceptibility to Drug Resistant Mutant Integrases.</a> Zhao, X.Z., S.J. Smith, D.P. Maskell, M. Metifiot, V.E. Pye, K. Fesen, C. Marchand, Y. Pommier, P. Cherepanov, S.H. Hughes, and T.R. Burke, Jr. ACS Chemical Biology, 2016. 11(4): p. 1074-1081. PMID[26808478]. PMCID[PMC4836387].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_06_2016.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">39. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160616&amp;CC=WO&amp;NR=2016094518A2&amp;KC=A2">Novel Cyclic Peptides and Methods Using Same.</a> Chaiken, I.M. and A.A.R. Ahmed. Patent. 2016. 2015-US64708 2016094518: 84pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">40. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160616&amp;CC=WO&amp;NR=2016090545A1&amp;KC=A1">Preparation of Spirocyclic Heterocycle Compounds Useful as HIV Integrase Inhibitors.</a> Embrey, M.W., T.H. Graham, I.T. Raheem, J.D. Schreier, S.T. Waddell, L. Hu, X. Peng, and J.S. Wai. Patent. 2016. 2014-CN93335 2016090545: 75pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">41. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160616&amp;CC=WO&amp;NR=2016094197A1&amp;KC=A1">Preparation of Spirocyclic Heterocycle Compounds Useful as HIV Integrase Inhibitors.</a> Embrey, M.W., T.H. Graham, I.T. Raheem, J.D. Schreier, S.T. Waddell, J.S. Wai, L. Hu, and X. Peng. Patent. 2016. 2015-US63850 2016094197: 96pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">42. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160616&amp;CC=WO&amp;NR=2016094198A1&amp;KC=A1">Preparation of Spirocyclic Heterocycle Compounds Useful as HIV Integrase Inhibitors.</a> Graham, T.H., M.W. Embrey, A. Walji, and S.T. Waddell. Patent. 2016. 2015-US63851 2016094198: 68pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">43. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160602&amp;CC=WO&amp;NR=2016082403A1&amp;KC=A1">Preparation of Triazole Derivatives as anti-AIDS Agents.</a> Lu, S., H. Zhang, J. Yu, C. Shi, and X. Yu. Patent. 2016. 2015-CN75463 2016082403: 20pp ; Chemical Indexing Equivalent to 162:425301 (CN).</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">44. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160526&amp;CC=US&amp;NR=2016143884A1&amp;KC=A1">Use of Tetrahydroindazolylbenzamide and Tetrahydroindolylbenzamide Derivatives for the Treatment of Human Immunodeficiency Virus (HIV) and Acquired Immune Deficiency Syndrome (AIDS).</a> Orlemans, E.O.M., B.F. Haynes, G. Ferrari, T. Haystead, and J.J. Kwiek. Patent. 2016. 2015-14952421 20160143884: 62pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>

    <br />

    <p class="plaintext">45. <a href="https://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20160519&amp;CC=WO&amp;NR=2016077561A1&amp;KC=A1">Preparation of Oxolupene Derivatives and Use as anti-HIV Agents.</a> Regueiro-Ren, A., Z. Liu, J. Swidorski, and N.A. Meanwell. Patent. 2016. 2015-US60344 2016077561: 130pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_06_2016.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
