

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2014-11-20.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="H4a0Dd3TjgklwmRsvh9DzMHnCj47MJ2D642yoyEQhrWbSLSML7d9L6eiSM9AYFOaldiQW1+oVAKI4QGbB5ndkIHbD0VHe+huxqKH/7Hvf3AGHUISD1wTJ7MP3T8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="813BF069" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: November 7 - November 20, 2014</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25397597">Synthesis and Biological Evaluation of 2-Thioxopyrimidin-4(1H)-one Derivatives as Potential Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Khalifa, N.M. and M.A. Al-Omar. International Journal of Molecular Sciences, 2014. 15(11): p. 20723-20735. PMID[25397597].
    <br />
    <b>[PubMed]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25186731">Broad</a> <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=25186731">and Potent HIV-1 Neutralization by a Human Antibody That Binds the gp41-gp120 Interface.</a>Huang, J., B.H. Kang, M. Pancera, J.H. Lee, T. Tong, Y. Feng, H. Imamichi, I.S. Georgiev, G.Y. Chuang, A. Druz, N.A. Doria-Rose, L. Laub, K. Sliepen, M.J. van Gils, A.T. de la Pena, R. Derking, P.J. Klasse, S.A. Migueles, R.T. Bailer, M. Alam, P. Pugach, B.F. Haynes, R.T. Wyatt, R.W. Sanders, J.M. Binley, A.B. Ward, J.R. Mascola, P.D. Kwong, and M. Connors. Nature, 2014. 515(7525): p. 138-142. PMID[25186731].
    <br />
    <b>[PubMed]</b>. HIV_1107-112014.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">3. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000343094200008">Broadly Neutralizing anti-HIV-1 Antibodies Require Fc Effector Functions for in Vivo Activity.</a> Bournazos, S., F. Klein, J. Pietzsch, M.S. Seaman, M.C. Nussenzweig, and J.V. Ravetch. Cell, 2014. 158(6): p. 1243-1253. ISI[000343094200008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1107-112014.</p><br /> 

    <p class="plaintext">4. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000343641000034">Oxaliplatin Antagonizes HIV-1 Latency by Activating NF-kappa B without Causing Global T Cell Activation.</a> Zhu, X.L., S.J. Liu, P.F. Wang, X.Y. Qu, X.H. Wang, H.X. Zeng, H.B. Chen, and H.Z. Zhu. Biochemical and Biophysical Research Communications, 2014. 450(1): p. 202-207. ISI[000343641000034].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000343237900092">Design of Biodegradable Poly Lactic-co-glycolic acid (PLGA) Nanoparticles for Co-delivery of anti-HIV Protein Griffithsin and Dapivirine in the Vagina.</a> Yang, H.T., J. Li, C.S. Dezzutti, K. Palmer, and L.C. Rohan. Journal of Women&#39;s Health, 2014. 23(10): p. 873-873. ISI[000343237900092].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000343720200010">Novel Theoretically Designed HIV-1 Non-Nucleoside Reverse Transcriptase Inhibitors Derived from Nevirapine.</a> Liu, J.F., X. He, and J.Z.H. Zhang. Journal of Molecular Modeling, 2014. 20(10): 2451. ISI[000343720200010].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_1107-112014.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">7. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141023&amp;CC=WO&amp;NR=2014172188A2&amp;KC=A2">Preparation of 4-Pyridone Derivative Compounds as HIV Integrase Inhibitors.</a> Graham, T.H., J.S. Wai, A. Stamford, and W. Liu. Patent. 2014. 2014-US33720 2014172188: 81pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">8. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141002&amp;CC=WO&amp;NR=2014159959A1&amp;KC=A1">Preparation of Pyridoindazole Compounds as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N., M. Patel, and T.P. Connolly. Patent. 2014. 2014-US25525 2014159959: 124pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">9. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141002&amp;CC=WO&amp;NR=2014159076A1&amp;KC=A1">Preparation of Pyrazolopyrimidine Compounds as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N., M. Patel, S. D&#39;Andrea, and Z.B. Zheng. Patent. 2014. 2014-US21867 2014159076: 78pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">10. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141009&amp;CC=WO&amp;NR=2014164467A1&amp;KC=A1">Preparation of Pyrazolo[1,5-a]pyrimidinylacetic acid as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N., M. Patel, K. Peese, and Z. Wang. Patent. 2014. 2014-US22501 2014164467: 101pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">11. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141009&amp;CC=WO&amp;NR=2014164428A1&amp;KC=A1">Preparation of Macrocyclic Acetic acid Compounds as Inhibitors of Human Immunodeficiency Virus Replication.</a> Naidu, B.N., M. Patel, K. Peese, and Z. Wang. Patent. 2014. 2014-US22405 2014164428: 148pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">12. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141009&amp;CC=WO&amp;NR=2014164409A1&amp;KC=A1">Preparation of Imidazo[1,2-a]pyridinylacetic acid Compounds as Inhibitors of Human Immunodeficiency Virus Replication.</a> Peese, K., Z. Wang, J.F. Kadow, and B.N. Naidu. Patent. 2014. 2014-US22354 2014164409: 49pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">13. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141002&amp;CC=WO&amp;NR=2014160692A1&amp;KC=A1">Preparation of 2-Keto amide Derivatives as HIV Attachment Inhibitors.</a> Wang, T., Z. Yin, Z. Zhang, J.A. Bender, B.L. Johnson, J.F. Kadow, and N.A. Meanwell. Patent. 2014. 2014-US31697 2014160692: 96pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p>

    <br />

    <p class="plaintext">14. <a href="mailto:http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20141002&amp;CC=WO&amp;NR=2014160689A1&amp;KC=A1">Preparation of Piperazine and Homopiperazine Derivatives as HIV Attachment Inhibitors.</a> Wang, T., Z. Yin, Z. Zhang, B.L. Johnson, J.F. Kadow, and N.A. Meanwell. Patent. 2014. 2014-US31693 2014160689: 100pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_1107-112014.</p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
