

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2010-04-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ap7Odp6hl6zTI+w4RFau/H8zybqxMOOiYwPOdyfSaJMZa86+tUulAtc5grwHexTMI7lA0yKgIM3fc0gS+Phr9vhBkU/8g5Ui8BNLxSAxH1dlatwz7ugnLFlWA6o=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AC2D39FB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Other Viruses  Citation List: </p>

    <p class="memofmt2-h1">April 1 - April 15, 2010</p>

    <p> </p>

    <p class="memofmt2-2">Hepatitis C Virus</p>

    <p />

    <p class="NoSpacing">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20372020">Evaluation of Inhibitory effects of Thiobarbituric Acid Derivatives Targeting HCV NS5B Polymerase.</a></p>

    <p class="NoSpacing">Lee JH, Lee S, Park MY, Ha HJ, Myung H.</p>

    <p class="NoSpacing">J Microbiol Biotechnol. 2010 Mar;20(3):510-512.PMID: 20372020 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20371471">Identification of a class of HCV inhibitors directed against the nonstructural protein NS4B.</a></p>

    <p class="NoSpacing">Cho NJ, Dvory-Sobol H, Lee C, Cho SJ, Bryson P, Masek M, Elazar M, Frank CW, Glenn JS.</p>

    <p class="NoSpacing">Sci Transl Med. 2010 Jan 20;2(15):15ra6.PMID: 20371471 [PubMed - in process]</p>

    <p />

    <p class="NoSpacing">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20368394">haracterization of v36c, a novel amino acid substitution conferring hepatitis c virus (hcv) resistance to telaprevir, a potent peptidomimetic inhibitor of hcv protease.</a></p>

    <p class="NoSpacing">Barbotte L, Ahmed-Belkacem A, Chevaliez S, Soulier A, Hézode C, Wajcman H, Bartels DJ, Zhou Y, Ardzinski A, Mani N, Rao BG, George S, Kwong A, Pawlotsky JM.</p>

    <p class="NoSpacing">Antimicrob Agents Chemother. 2010 Apr 5. [Epub ahead of print]PMID: 20368394 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20363257">A small molecule inhibits HCV replication and alters NS4B&#39;s subcellular distribution.</a></p>

    <p class="NoSpacing">Bryson PD, Cho NJ, Einav S, Lee C, Tai V, Bechtel J, Sivaraja M, Roberts C, Schmitz U, Glenn JS.</p>

    <p class="NoSpacing">Antiviral Res. 2010 Apr 2. [Epub ahead of print]PMID: 20363257 [PubMed - as supplied by publisher]</p>

    <p />

    <p class="NoSpacing">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20363140">Allosteric inhibitors of hepatitis C virus NS5B polymerase thumb domain site II: structure-based design and synthesis of new templates.</a></p>

    <p class="NoSpacing">Malancona S, Donghi M, Ferrara M, Martin Hernando JI, Pompei M, Pesci S, Ontoria JM, Koch U, Rowley M, Summa V.</p>

    <p class="NoSpacing">Bioorg Med Chem. 2010 Apr 15;18(8):2836-48. Epub 2010 Mar 15.PMID: 20363140 [PubMed - in process]</p> 

    <h2 class="memofmt2-2">Herpes Simplex Virus</h2> 

    <p class="NoSpacing">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20377249">&quot;Viologen&quot; Dendrimers as Antiviral Agents: The Effect of Charge Number and Distance (dagger).</a></p>

    <p class="NoSpacing">Asaftei S, De Clercq E.</p>

    <p class="NoSpacing">J Med Chem. 2010 Apr 8. [Epub ahead of print]PMID: 20377249 [PubMed - as supplied by publisher]</p>

    <p />

    <p />

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>

    <p> </p>

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900013">Inhibitors of hepatitis C virus polymerase: Synthesis and characterization of novel 2-oxy-6-fluoro-N-((S)-1-hydroxy-3-phenylpropan-2-yl)-benzamides.</a> Cheng, Cliff C., et al.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(7): 2010. P. 2119-24.</p>

    <p> </p>

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000275805900017">TI Discovery of a synthetic dual inhibitor of HIV and HCV infection based on a tetrabutoxy-calix[4] arene scaffold.</a>  Tsou, Lun K., et al. BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS. 20(7): 2010. P. 2137-9.</p>

    <p> </p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
