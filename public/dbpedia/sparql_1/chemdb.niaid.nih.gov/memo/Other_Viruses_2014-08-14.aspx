

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Other_Viruses_2014-08-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="SWzt46hzQPdi5gFH1yFvCJthKBdZ6b3EX6J+b8zYDD49sQVRQUo/gmsMLPYdgVeIwjad2TISAI//gz9nljqPnS3iiINa2kH28IL6OW6LlKOqDqLQVYajNJY7SM0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A0708ADC" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Other Viruses Citation List: August 1 - August 14, 2014</h1>

    <h2>Hepatitis B Virus</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24971706">Nitazoxanide, an Antiviral Thiazolide, Depletes ATP-sensitive Intracellular Ca(2+) Stores.</a> Ashiru, O., J.D. Howe, and T.D. Butters. Virology, 2014. 462-463: p. 135-148. PMID[24971706].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24118725">HBV Clinical Isolates Expressing Adefovir Resistance Mutations Show Similar Tenofovir Susceptibilities Across Genotypes B, C and D.</a> Liu, Y., M.D. Miller, and K.M. Kitrinos. Liver International, 2014. 34(7): p. 1025-1032. PMID[24118725].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24858512">Hepatitis B Virus Replication is Blocked by a 2-Hydroxyisoquinoline-1,3(2H,4H)-dione (HID) Inhibitor of the Viral Ribonuclease H Activity.</a> Cai, C.W., E. Lomonosova, E.A. Moran, X. Cheng, K.B. Patel, F. Bailly, P. Cotelle, M.J. Meyers, and J.E. Tavis. Antiviral Research, 2014. 108: p. 48-55. PMID[24858512].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p>

    <h2>Hepatitis C Virus</h2>

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24867983">In vitro Activity and Resistance Profile of Samatasvir, a Novel NS5A Replication Inhibitor of Hepatitis C Virus.</a> Bilello, J.P., L.B. Lallos, J.F. McCarville, M. La Colla, I. Serra, C. Chapron, J.M. Gillum, C. Pierra, D.N. Standring, and M. Seifer. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4431-4442. PMID[24867983].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24786893">Cyclophilin Inhibitors Reduce Phosphorylation of RNA-dependent Protein Kinase to Restore Expression of IFN-stimulated Genes in HCV-infected Cells.</a> Daito, T., K. Watashi, A. Sluder, H. Ohashi, S. Nakajima, K. Borroto-Esoda, T. Fujita, and T. Wakita. Gastroenterology, 2014. 147(2): p. 463-472. PMID[24786893].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24768676">Kinetic Analyses Reveal Potent and Early Blockade of Hepatitis C Virus Assembly by NS5A Inhibitors.</a> McGivern, D.R., T. Masaki, S. Williford, P. Ingravallo, Z. Feng, F. Lahser, E. Asante-Appiah, P. Neddermann, R. De Francesco, A.Y. Howe, and S.M. Lemon. Gastroenterology, 2014. 147(2): p. 453-462.e7. PMID[24768676].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25092460">The Flavonoid Apigenin Inhibits Hepatitis C Virus Replication by Decreasing Mature microRNA122 Levels.</a> Shibata, C., M. Ohno, M. Otsuka, T. Kishikawa, K. Goto, R. Muroyama, N. Kato, T. Yoshikawa, A. Takata, and K. Koike. Virology, 2014. 462-463: p. 42-48. PMID[25092460].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25066323">Discovering Novel anti-HCV Compounds with Inhibitory Activities toward HCV NS3/4A Protease.</a> Yu, Y., J.F. Jing, X.K. Tong, P.L. He, Y.C. Li, Y.H. Hu, W. Tang, and J.P. Zuo. Acta Pharmacologica Sinica, 2014. 35(8): p. 1074-1081. PMID[25066323].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p>

    <h2>Feline Immunodeficiency Virus</h2>

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24954265">Treatment of Chronically FIV-infected Cats with Suberoylanilide hydroxamic acid.</a> McDonnel, S.J., M.L. Liepnieks, and B.G. Murphy. Antiviral Research, 2014. 108: p. 74-78. PMID[24954265].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p>

    <h2>Human Cytomegalovirus</h2>

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25003190">CDK9 Inhibitor FIT-039 Prevents Replication of Multiple DNA Viruses.</a> Yamamoto, M., H. Onogi, I. Kii, S. Yoshida, K. Iida, H. Sakai, M. Abe, T. Tsubota, N. Ito, T. Hosoya, and M. Hagiwara. Journal of Clinical Investigation, 2014. 124(8): p. 3479-3488. PMID[25003190].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p>

    <h2>Herpes Simplex Virus 1</h2>

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24820089">Antiherpesvirus Activities of Two Novel 4&#39;-Thiothymidine Derivatives, KAY-2-41 and KAY-39-149, are Dependent on Viral and Cellular Thymidine Kinases.</a> Coen, N., S. Duraffour, K. Haraguchi, J. Balzarini, J.J. van den Oord, R. Snoeck, and G. Andrei. Antimicrobial Agents and Chemotherapy, 2014. 58(8): p. 4328-4340. PMID[24820089].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24682316">Effect of the N-Butanoyl glutathione (GSH) Derivative and Acyclovir on HSV-1 Replication and Th1 Cytokine Expression in Human Macrophages.</a> Fraternale, A., G.F. Schiavano, M.F. Paoletti, L. Palma, M. Magnani, and G. Brandi. Medical Microbiology and Immunology, 2014. 203(4): p. 283-289. PMID[24682316].</p>

    <p class="plaintext"><b>[PubMed]</b>. OV_0801-081414.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.V.</h2>

    <p class="plaintext">13. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000338775200010">4 &#39;-Modified Pyrimidine Nucleosides as Potential anti-Hepatitis C Virus (HCV) Agents.</a> Shakya, N., S. Vedi, C. Liang, B. Agrawal, and R. Kumar. Letters in Drug Design &amp; Discovery, 2014. 11(7): p. 917-921. ISI[000338775200010].</p>

    <p class="plaintext"><b>[WOS]</b>. OV_0801-081414.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
