

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-11-10.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MBOspkrzpm6TO1/Q5005ZPi1ogk4Oaq1z6KDGHswJjFmvZ2KTFgoFY7GDeHGaUpOwE546RnOv1m+j5Uyyf3fXB0gvBUTHtQ8ZF0B/4pAsacM8r/E5v2uPuuqNXs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="94836B55" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1"> </h1>

    <h1 class="memofmt2-h1">HIV Citation List:  October 28 - November 10, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p>

    <p class="title">1.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19903162?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=1">Polyisoprenylated Benzophenones from Clusiaceae: Potential Drugs and Lead Compounds.</a>  Acuña UM, Jancovski N, Kennelly EJ.  Curr Top Med Chem. 2009 Nov 11. [Epub ahead of print]PMID: 19903162</p>

    <p class="title">2.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19902958?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=2">Solid Phase Synthesis of Novel Pyrrolidinedione Analogs as Potent HIV-1 Integrase Inhibitors.</a>  Pendri A, Troyer TL, Sofia MJ, Walker MA, Naidu BN, Banville J, Meanwell NA, Dicker I, Lin Z, Krystal M, Gerritz SW.  J Comb Chem. 2009 Nov 10. [Epub ahead of print]PMID: 19902958</p>

    <p class="title">3.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19901984?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=3">Potent inhibition of HIV-1 replication by a Tat mutant.</a>  Meredith LW, Sivakumaran H, Major L, Suhrbier A, Harrich D.  PLoS One. 2009 Nov 10;4(11):e7769.PMID: 19901984</p>

    <p class="title">4.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19900481?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=4">Neolignan, sesquineolignans and dineolignans, novel HIV-1-protease and HIV-1-induced cytopathic effect inhibitors purified from the rhizomes of Saururus chinensis.</a>  Lee J, Huh MS, Kim YC, Hattori M, Otake T.  Antiviral Res. 2009 Nov 6. [Epub ahead of print]PMID: 19900481</p>

    <p class="title">5.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19899101?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=5">Synthesis and Anti-HIV-1 Integrase Activitiy of Cyano Pyrimidinones.</a>  Ramajayam R, Mahera NB, Neamati N, Yadav MR, Giridhar R.  Arch Pharm (Weinheim). 2009 Nov 6. [Epub ahead of print]PMID: 19899101</p>

    <p class="title">6.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19895691?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=6">Antiretroviral activity of the aminothiol WR1065 against Human Immunodeficiency virus (HIV-1) in vitro and Simian Immunodeficiency virus (SIV) ex vivo.</a>  Poirier MC, Olivero OA, Hardy AW, Franchini G, Borojerdi JP, Walker VE, Walker DM, Shearer GM.  AIDS Res Ther. 2009 Nov 6;6(1):24. [Epub ahead of print]PMID: 19895691</p>

    <p class="title">7.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19893585?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=8">Beauvericin and enniatins H, I and MK1688 are new potent inhibitors of human immunodeficiency virus type-1 integrase.</a>  Shin CG, An DG, Song HH, Lee C.  J Antibiot (Tokyo). 2009 Nov 6. [Epub ahead of print]PMID: 19893585</p>

    <p class="title">8.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19878736?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=9">Cyclotides as templates in drug design.</a>  Henriques ST, Craik DJ.  Drug Discov Today. 2009 Oct 28. [Epub ahead of print]PMID: 19878736</p>

    <p class="title">9.       <a href="http://www.ncbi.nlm.nih.gov/pubmed/19873974?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=10">Azaindole Hydroxamic Acids are Potent HIV-1 Integrase Inhibitors.</a>  Plewe MB, Butler SL, R Dress K, Hu Q, Johnson TW, Kuehler JE, Kuki A, Lam H, Liu W, Nowlin D, Peng Q, Rahavendran SV, Tanis SP, Tran KT, Wang H, Yang A, Zhang J.  J Med Chem. 2009 Oct 29. [Epub ahead of print]PMID: 19873974</p>

    <p class="title">10.   <a href="http://www.ncbi.nlm.nih.gov/pubmed/19628308?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=11">Synthesis and anti-HIV activity evaluation of 2-(4-(naphthalen-2-yl)-1,2,3-thiadiazol-5-ylthio)-N-acetamides as novel non-nucleoside HIV-1 reverse transcriptase inhibitors.</a>  Zhan P, Liu X, Fang Z, Li Z, Pannecouque C, De Clercq E.  Eur J Med Chem. 2009 Nov;44(11):4648-53. Epub 2009 Jul 4.PMID: 19628308</p>

    <p class="memofmt2-2">ISI Web of Knowledge citations:</p>

    <p class="ListParagraphCxSpFirst">11.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271095100013">Antiviral activity of Spirulina - An Overview III.</a>  Srivastava, P.  VEGETOS, 2009; 22(1): 87-90. </p>

    <p class="ListParagraphCxSpLast">12.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerrting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271094000039">Regioselective synthesis of 3-deazacarbovir and its 3-deaza-adenosine analogues.</a>  Jha, AK; Sharon, A; Rondla, R; Chu, CK.  TETRAHEDRON, 2009; 65(45): 9362-9367.</p>

    <p>13.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerrting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271098300009">HIV-1-Inhibiting Activity of the Essential Oil of Ridolfia segetum and Oenanthe crocata.</a>  Bicchi, C; Rubiolo, P; Ballero, M; Sanna, C; Matteodo, M; Esposito, F; Zinzula, L; Tramontano, E.  PLANTA MEDICA, 2009; 75(12): 1331-1335.</p>

    <p>14.   <a href=":/gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271168400003">Flavonoids as Potential Inhibitors of Retroviral Enzymes.</a>  Ko, YJ; Oh, HJ; Ahn, HM; Kang, HJ; Kim, JH; Ko, YH.  JOURNAL OF THE KOREAN SOCIETY FOR APPLIED BIOLOGICAL CHEMISTRY, 2009; 52(4): 321-326.</p>

    <p>15.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271089500009">Synthesis of phenacyl derivatives of frangula-emodin and their HIV-1 RNase H activity.</a>  Kharlamova, TV.  CHEMISTRY OF NATURAL COMPOUNDS, 2009; 45(4): 500-502.</p>

    <p>16.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271137600004">Discovery, structure and biological activities of cyclotides.</a>  Daly, NL; Rosengren, KJ; Craik, DJ.  ADVANCED DRUG DELIVERY REVIEWS, 2009; 61(11): 918-930.</p>

    <p>17.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000270962500002">Anti-MV Diterpenes from Coleus forskohlii.</a>  Bodiwala, HS; Sabde, S; Mitra, D; Bhutani, KK; Singh, IP.  NATURAL PRODUCT COMMUNICATIONS, 2009; 4(9): 1173-1175.</p>

    <p>18.   <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000271029600032">Imidazopyridine-5,6,7,8-tetrahydro-8-quinolinamine derivatives with potent activity against HIV-1.</a>  Gudmundsson, KS; Boggs, SD; Catalano, JG; Svolto, A; Spaltenstein, A; Thomson, M; Wheelan, P; Jenkinson, S.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS, 2009; 19(22): 6399-6403.</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <p>&nbsp;</p>     

    <p class="memofmt2-2">&nbsp;</p>   
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
