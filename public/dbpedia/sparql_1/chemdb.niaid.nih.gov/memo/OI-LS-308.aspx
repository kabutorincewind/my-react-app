

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-308.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6rAEv1SnXHW1d//TT7BArtQ3489Ed2x+MXW7rFqGntc7SLtDHBxEobwoUaa2xoZklZZJwnL5xkLevhXAU103uV3BTa4rzkPLB38pS4eogHlOT9vVGnizI9kJugA=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="54B924FB" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-308-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.         44313   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Action of Fluoroquinolones and Linezolid on Logarithmic- and Stationary-Phase Culture of Mycobacterium tuberculosis</p>

    <p>          Garcia-Tapia, A, Rodriguez, JC, Ruiz, M, and Royo, G</p>

    <p>          Chemotherapy <b>2004</b>.  50(5): 211-213</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523179&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15523179&amp;dopt=abstract</a> </p><br />

    <p>2.         44314   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          In vitro advanced antimycobacterial screening of cobalt(II) and copper(II) complexes of fluorinated isonicotinoylhydrazones</p>

    <p>          Maccari, Rosanna, Ottana, Rosaria, Bottari, Bruno, Rotondo, Enrico, and Vigorita, Maria Gabriella</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2004</b>.  14(23): 5731-5733</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4DGW3C5-2/2/8e438223a852baf50917b24546eac13c">http://www.sciencedirect.com/science/article/B6TF9-4DGW3C5-2/2/8e438223a852baf50917b24546eac13c</a> </p><br />

    <p>3.         44315   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          Interaction between saquinavir and antimycotic drugs on C. albicans and C. neoformans strains</p>

    <p>          Casolari, Chiara, Rossi, Tiziana, Baggio, Giosue, Coppi, Andrea, Zandomeneghi, Ginevra, Ruberto, Antonio Ippazio, Farina, Claudio, Fabio, Giuliana, Zanca, Andrea, and Castelli, Mario</p>

    <p>          Pharmacological Research <b>2004</b>.  50(6): 605-610</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WP9-4DB54JK-1/2/69f2d54ac742197ab63ebd21c95b09a0">http://www.sciencedirect.com/science/article/B6WP9-4DB54JK-1/2/69f2d54ac742197ab63ebd21c95b09a0</a> </p><br />

    <p>4.         44316   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Hepatitis C: It&#39;s a long way to new therapy, it&#39;s a long way to go</p>

    <p>          Pawlotsky, JM</p>

    <p>          Gastroenterology <b>2004</b>.  127(5): 1629-32</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15521029&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15521029&amp;dopt=abstract</a> </p><br />

    <p>5.         44317   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Current trends in chemotherapy of tuberculosis</p>

    <p>          Jawahar, MS</p>

    <p>          Indian J Med Res <b>2004</b>.  120(4): 398-417</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15520489&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15520489&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.         44318   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Design, Synthesis, and Antiviral Activity of Certain 3-Substituted 2,5,6-Trichloroindole Nucleosides</p>

    <p>          Williams, JD, Chen, JJ, Drach, JC, and Townsend, LB</p>

    <p>          J Med Chem <b>2004</b>.  47(23): 5753-5765</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509174&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15509174&amp;dopt=abstract</a> </p><br />

    <p>7.         44319   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Inhibition of Adherence and Killing of Candida albicans with a 23-Mer Peptide (Fn/23) with Dual Antifungal Properties</p>

    <p>          Klotz, SA, Gaur, NK, Rauceo, J, Lake, DF, Park, Y, Hahm, KS, and Lipke, PN</p>

    <p>          Antimicrob Agents Chemother <b>2004</b>.  48(11): 4337-4341</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504862&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15504862&amp;dopt=abstract</a> </p><br />

    <p>8.         44320   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Antimycobacterial calixarenes enhance innate defense mechanisms in murine macrophages and induce control of Mycobacterium tuberculosis infection in mice</p>

    <p>          Colston, MJ, Hailes, HC, Stavropoulos, E, Herve, AC, Herve, G, Goodworth, KJ, Hill, AM, Jenner, P, Hart, PD, and Tascon, RE</p>

    <p>          Infect Immun <b>2004</b>.  72(11): 6318-23</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15501760&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15501760&amp;dopt=abstract</a> </p><br />

    <p>9.         44321   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Novel 2-oxoimidazolidine-4-carboxylic acid derivatives as Hepatitis C virus NS3-4A serine protease inhibitors: synthesis, activity, and X-ray crystal structure of an enzyme inhibitor complex</p>

    <p>          Arasappan, A, Njoroge, FG, Parekh, TN, Yang, X, Pichardo, J, Butkiewicz, N, Prongay, A, Yao, N, and Girijavallabhan, V</p>

    <p>          Bioorg Med Chem Lett <b>2004</b>.  14(23): 5751-5755</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15501035&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15501035&amp;dopt=abstract</a> </p><br />

    <p>10.       44322   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Inhibition of subgenomic hepatitis C virus RNA in Huh-7 cells: ribavirin induces mutagenesis in HCV RNA</p>

    <p>          Kanda, T, Yokosuka, O, Imazeki, F, Tanaka, M, Shino, Y, Shimada, H, Tomonaga, T, Nomura, F, Nagao, K, Ochiai, T, and Saisho, H</p>

    <p>          J Viral Hepat <b>2004</b>.  11(6): 479-487</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500548&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15500548&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.       44323   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          A Novel Antimicrobial Indolizinium Alkaloid from Aniba panurensis</p>

    <p>          Klausmeyer, P, Chmurny, GN, McCloud, TG, Tucker, KD, and Shoemaker, RH</p>

    <p>          J Nat Prod <b>2004</b>.  67(10): 1732-1735</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15497951&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15497951&amp;dopt=abstract</a> </p><br />

    <p>12.       44324   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          In vitro susceptibility testing of Mycobacterium tuberculosis complex strains isolated from seals to antituberculosis drugs</p>

    <p>          Bernardelli, A, Morcillo, N, Loureiro, J, Quse, V, and Davenport, S</p>

    <p>          Biomedica <b>2004</b>.  24 Supp 1: 85-91</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15495576&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15495576&amp;dopt=abstract</a> </p><br />

    <p>13.       44325   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Structure of EthR in a Ligand Bound Conformation Reveals Therapeutic Perspectives against Tuberculosis</p>

    <p>          Frenois, F, Engohang-Ndong, J, Locht, C, Baulard, AR, and Villeret, V</p>

    <p>          Mol Cell <b>2004</b>.  16(2): 301-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494316&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15494316&amp;dopt=abstract</a> </p><br />

    <p>14.       44326   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Differential effects on the hepatitis C virus (HCV) internal ribosome entry site by vitamin B12 and the HCV core protein</p>

    <p>          Li, D, Lott, WB, Martyn, J, Haqshenas, G, and Gowans, EJ</p>

    <p>          J Virol <b>2004</b>.  78(21): 12075-81</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15479850&amp;dopt=abstract</a> </p><br />

    <p>15.       44327   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          Prp8 intein in fungal pathogens: target for potential antifungal drugs</p>

    <p>          Liu, Xiang-Qin and Yang, Jing</p>

    <p>          FEBS Letters <b>2004</b>.  572(1-3): 46-50</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T36-4CXKN87-1/2/0e61332f28b4c76f6956a7958391027f">http://www.sciencedirect.com/science/article/B6T36-4CXKN87-1/2/0e61332f28b4c76f6956a7958391027f</a> </p><br />
    <br clear="all">

    <p>16.       44328   OI-LS-308; PUBMED-OI-11/4/2004</p>

    <p class="memofmt1-2">          Rapid, automated, nonradiometric susceptibility testing of Mycobacterium tuberculosis complex to four first-line antituberculous drugs used in standard short-course chemotherapy</p>

    <p>          Johansen, IS, Thomsen, VO, Marjamaki, M, Sosnovskaja, A, and Lundgren, B</p>

    <p>          Diagn Microbiol Infect Dis <b>2004</b>.  50(2): 103-7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474318&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15474318&amp;dopt=abstract</a> </p><br />

    <p>17.       44329   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          Inhibition of bovine viral diarrhea virus (BVDV) by mizoribine: synergistic effect of combination with interferon-[alpha]</p>

    <p>          Yanagida, Koichiro, Baba, Chiaki, and Baba, Masanori</p>

    <p>          Antiviral Research <b>2004</b>.  In Press, Uncorrected Proof</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4DJ3YFB-1/2/753d5ff6f857d116e599c17f8f493201">http://www.sciencedirect.com/science/article/B6T2H-4DJ3YFB-1/2/753d5ff6f857d116e599c17f8f493201</a> </p><br />

    <p>18.       44330   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          What is disrupting IFN-[alpha]&#39;s antiviral activity?</p>

    <p>          Mbow, MLamine and Sarisky, Robert T</p>

    <p>          Trends in Biotechnology <b>2004</b>.  22(8): 395-399</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TCW-4CPVMT4-3/2/c15f5d6f7b90c7ebf2677417f418ab80">http://www.sciencedirect.com/science/article/B6TCW-4CPVMT4-3/2/c15f5d6f7b90c7ebf2677417f418ab80</a> </p><br />

    <p>19.       44331   OI-LS-308; EMBASE-OI-11/4/2004</p>

    <p class="memofmt1-2">          Clinical and biologic aspects of human cytomegalovirus resistance to antiviral drugs</p>

    <p>          Baldanti, Fausto, Lurain, Nell, and Gerna, Giuseppe</p>

    <p>          Human Immunology <b>2004</b>.  65(5): 403-409</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T3B-4CXM1Y2-4/2/6ff4b3450bfdf74c076bd5807c2744bf">http://www.sciencedirect.com/science/article/B6T3B-4CXM1Y2-4/2/6ff4b3450bfdf74c076bd5807c2744bf</a> </p><br />

    <p>20.       44332   OI-LS-308; WOS-OI-10/25/2004</p>

    <p class="memofmt1-2">          Synthesis of new tetrazolyldienylphenothiazines as potential multidrug resistance inhibitory compounds</p>

    <p>          Nagy, I, Riedl, Z, Hajos, G, Messmer, A, Gyemant, N, and Molnar, J</p>

    <p>          ARKIVOC <b>2004</b>.: 177-182, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224153300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224153300014</a> </p><br />

    <p>21.       44333   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Antifungal flavonoids from Ballota glandulosissima</p>

    <p>          Citoglu, GS, Sever, B, Antus, S, Baitz-Gacs, E, and Altanlar, N</p>

    <p>          PHARMACEUTICAL BIOLOGY <b>2003</b>.  41(7): 483-486, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224247500003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224247500003</a> </p><br />
    <br clear="all">

    <p>22.       44334   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Intracellular inhibition of hepatitis C virus (HCV) internal ribosomal entry site (IRES)-dependent translation by peptide nucleic acids (PNAs) and locked nucleic acids (LNAs) (vol 32, pg 3792, 2004)</p>

    <p>          Nulf, CJ and Corey, D</p>

    <p>          NUCLEIC ACIDS RESEARCH <b>2004</b>.  32(16): 4954-1</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224207500025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224207500025</a> </p><br />

    <p>23.       44335   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Modeling the emergence of the &#39;hot zones&#39;: tuberculosis and the amplification dynamics of drug resistance</p>

    <p>          Blower, SM and Chou, T</p>

    <p>          NATURE MEDICINE <b>2004</b>.  10(10): 1111-1116, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224245800042">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224245800042</a> </p><br />

    <p>24.       44336   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Antibiotics at the crossroads</p>

    <p>          Nathan, C</p>

    <p>          NATURE <b>2004</b>.  431(7011): 899-902, 4</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224585600016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224585600016</a> </p><br />

    <p>25.       44337   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Resistance to macrolide, lincosamide, streptogramin, ketolide, and oxazolidinone antibiotics</p>

    <p>          Roberts, MC</p>

    <p>          MOLECULAR BIOTECHNOLOGY <b>2004</b>.  28(1): 47-62, 16</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224012700006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224012700006</a> </p><br />

    <p>26.       44338   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Cryptococcus neoformans methionine synthase: expression analysis and requirement for virulence</p>

    <p>          Pascon, RC, Ganous, TM, Kingsbury, JM, Cox, GM, and McCusker, JH</p>

    <p>          MICROBIOLOGY-SGM <b>2004</b>.  150: 3013-3023, 11</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224151100022">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224151100022</a> </p><br />

    <p>27.       44339   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          In vitro antimicrobial properties of aqueous garlic extract against multidrug-resistant bacteria and Candida species from Nigeria</p>

    <p>          Iwalokun, BA, Ogunledun, A, Ogbolu, DO, Bamiro, SB, and Jimi-Omojola, J</p>

    <p>          JOURNAL OF MEDICINAL FOOD <b>2004</b>.  7(3): 327-333, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224137700010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224137700010</a> </p><br />

    <p>28.       44340   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Does tuberculosis increase HIV load?</p>

    <p>          Day, JH, Grant, AD, Fielding, KL, Morris, L, Moloi, V, Charalambous, S, Puren, AJ, Chaisson, RE, De, Cock KM, Hayes, RJ, and Churchyard, GJ</p>

    <p>          JOURNAL OF INFECTIOUS DISEASES <b>2004</b>.  190(9): 1677-1684, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224303100021">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224303100021</a> </p><br />
    <br clear="all">

    <p>29.       44341   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Effect of n-octanesulphonylacetamide (OSA) on ATP and protein expression in Mycobacterium bovis BCG</p>

    <p>          Parrish, NM, Ko, CG, Hughes, MA, Townsend, CA, and Dick, JD</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2004</b>.  54(4): 722-729, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224207100004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224207100004</a> </p><br />

    <p>30.       44342   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Host cell tropism underlies species restriction of human and bovine Cryptosporidium parvum genotypes</p>

    <p>          Hashim, A, Clyne, M, Mulcahy, G, Akiyoshi, D, Chalmers, R, and Bourke, B</p>

    <p>          INFECTION AND IMMUNITY <b>2004</b>.  72(10): 6125-6131, 7</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224134000068">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224134000068</a> </p><br />

    <p>31.       44343   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial activity of novel tetrahydrobenzothienopyrimidines</p>

    <p>          Haleem, AA, Eissa, M, and Moneer, AA</p>

    <p>          ARCHIVES OF PHARMACAL RESEARCH <b>2004</b>.  27(9): 885-892, 8</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224106700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224106700001</a> </p><br />

    <p>32.       44344   OI-LS-308; WOS-OI-10/31/2004</p>

    <p class="memofmt1-2">          Antibacterial activities of neolignans isolated from the seed endotheliums of Trewia nudiflora</p>

    <p>          Li, GH, Zhao, PJ, Shen, YM, and Zhang, KQ</p>

    <p>          ACTA BOTANICA SINICA <b>2004</b>.  46(9): 1122-1127, 6</p>

    <p>&nbsp;          <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224034800016">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000224034800016</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
