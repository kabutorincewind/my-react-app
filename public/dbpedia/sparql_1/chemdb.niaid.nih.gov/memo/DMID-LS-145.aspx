

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-145.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dI9NNfUx8Od3yckya+SesyS3thha701QInBv0pDO8Vo4P/+e2Fd819x7f8q4F1oTAErMmNsd11DzFvrACFsnB0vq36gQLoF62/hEiHVnUe6qhifl9o4JEib0WC8=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DF89A5AE" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-145-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     60863   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Cyclopentenol nucleoside compounds, intermediates for their synthesis and methods of treating viral infections</p>

    <p>          Chu, David CK, Cho, Jong Hyun, and Kim, Hyo-Joon</p>

    <p>          PATENT:  WO <b>2007047793</b>  ISSUE DATE:  20070426</p>

    <p>          APPLICATION: 2006  PP: 121pp.</p>

    <p>          ASSIGNEE:  (University of Georgia Research Foundation, Inc. USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>2.     60864   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Immunogenic peptide compositions and their use in the pharmaceutical compositions active against hepatitis C virus</p>

    <p>          Inchauspe, Genevieve, Fournillier, Anne, Himoudi, Nourredine, and Martin, Perrine</p>

    <p>          PATENT:  US <b>2007072176</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2006-44493  PP: 116pp., Cont.-in-part of U.S. Ser. No. 514,762.</p>

    <p>          ASSIGNEE:  (Fr.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>3.     60865   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Anti-viral agent, and method for treatment of virus-infected cell</p>

    <p>          Yoshinaka, Yoshiyuki, Yamamoto, Naoki, Ozawa, Satoshi, and Arai, Jun-Ichiro</p>

    <p>          PATENT:  WO <b>2007034741</b>  ISSUE DATE:  20070329</p>

    <p>          APPLICATION: 2006  PP: 25pp.</p>

    <p>          ASSIGNEE:  (Daikin Industries, Ltd. Japan</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>4.     60866   DMID-LS-145; WOS-DMID-5/18/2007</p>

    <p class="memofmt1-2">          Genetics, genomics, and proteomics: Implications for the diagnosis and the treatment of chronic hepatitis C</p>

    <p>          Asselah, T, Bieche, I, Paradis, V, Bedossa, P, Vidaud, M, and Marcellin, P</p>

    <p>          SEMINARS IN LIVER DISEASE <b>2007</b>.  27(1): 13-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245438100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245438100003</a> </p><br />

    <p>5.     60867   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of 6-substituted uridine analogs as Odcase inhibitors used in the treatment of viral and bacterial infections</p>

    <p>          Kotra, Lakshmi P and Pai, Emil F</p>

    <p>          PATENT:  WO <b>2007038860</b>  ISSUE DATE:  20070412</p>

    <p>          APPLICATION: 2006  PP: 54pp.</p>

    <p>          ASSIGNEE:  (Can.)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>6.     60868   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Inhibition of filovirus replication by the zinc finger antiviral protein</p>

    <p>          Mueller, Stefanie, Moeller, Peggy, Bick, Matthew J, Wurr, Stephanie, Becker, Stephan, Guenther, Stephan, and Kuemmerer, Beate M</p>

    <p>          J. Virol. <b>2007</b>.  81(5): 2391-2400</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>7.     60869   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Antiviral profiles of novel iminocyclitol compounds against bovine viral diarrhea virus, West Nile virus, dengue virus and hepatitis B virus</p>

    <p>          Gu Baohua, Mason Peter, Wang Lijuan, Norton Pamela, Bourne Nigel, Moriarty Robert, Mehta Anand, Despande Mehendra, Shah Rajendra, and Block Timothy</p>

    <p>          Antivir Chem Chemother <b>2007</b>.  18(1): 49-59</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>8.     60870   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Galactan sulfate of Grateloupia indica: Isolation, structural features and antiviral activity</p>

    <p>          Chattopadhyay, Kausik, Mateu, Cecilia G, Mandal, Pinaki, Pujol, Carlos A, Damonte, Elsa B, and Ray, Bimalendu</p>

    <p>          Phytochemistry (Elsevier) <b>2007</b>.  68(10): 1428-1435</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     60871   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Use of 2-substituted-4-heteroarylpyrimidines that are protein kinase inhibitors for treating a variety of disorders</p>

    <p>          Wood, Gavin, Meades, Christopher Keith, Fischer, Peter, Wang, Shudong, Duncan, Kenneth, Zheleva, Daniella, McInnes, Campbell, and Thomas, Mark</p>

    <p>          PATENT:  WO <b>2007042786</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2006  PP: 81pp.</p>

    <p>          ASSIGNEE:  (Cyclacel Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   60872   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          The in vitro activity of geraniin and 1,3,4,6-tetra-O-galloyl-b--glucose isolated from Phyllanthus urinaria against herpes simplex virus type 1 and type 2 infection</p>

    <p>          Yang, Chien-Min, Cheng, Hua-Yew, Lin, Ta-Chen, Chiang, Lien-Chai, and Lin, Chun-Ching</p>

    <p>          J. Ethnopharmacol. <b>2007</b>.  110(3): 555-558</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>11.   60873   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Preparation of 6,6-bicyclic ring-substituted sulfur-containing heterobicyclic protein kinase inhibitors</p>

    <p>          Ji, Qun-Sheng, Mulvihill, Mark Joseph, Steinig, Arno G, and Weng, Qinghua</p>

    <p>          PATENT:  US <b>2007032512</b>  ISSUE DATE:  20070208</p>

    <p>          APPLICATION: 2006-40223  PP: 54pp.</p>

    <p>          ASSIGNEE:  (USA)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>12.   60874   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          pharmaceutical compositions having a high antiviral and antibacterial efficacy</p>

    <p>          Fuls, Janice Lynn, Taylor, Timothy J, Fox, Priscilla S, Rodgers, Nancy Day, Towner, Harry Ernest, and Dalton, James</p>

    <p>          PATENT:  WO <b>2007044032</b>  ISSUE DATE:  20070419</p>

    <p>          APPLICATION: 2005  PP: 70pp.</p>

    <p>          ASSIGNEE:  (The Dial Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>13.   60875   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Anti-enterovirus activity and structure-activity relationship of a series of 2,6-dihalophenyl-substituted 1H,3H-thiazolo[3,4-a]benzimidazoles</p>

    <p>          De Palma Armando M, Heggermont Ward, Leyssen Pieter, Purstinger Gerhard, Wimmer Eva, De Clercq Erik, Rao Angela, Monforte Anna-Maria, Chimirri Alba, and Neyts Johan</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.  353(3): 628-32.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   60876   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Inhibitors of poxvirus protein kinases and their use as antiviral agents</p>

    <p>          Sefton, Bartholomew M and Schulte, Roberta J</p>

    <p>          PATENT:  WO <b>2007047339</b>  ISSUE DATE:  20070426</p>

    <p>          APPLICATION: 2006  PP: 259pp.</p>

    <p>          ASSIGNEE:  (The Salk Institute for Biological Studies, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>15.   60877   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Recent developments in the virology and antiviral research of severe acute respiratory syndrome coronavirus</p>

    <p>          Yeung, Kap-Sun and Meanwell, Nicholas A</p>

    <p>          Infect. Disord.: Drug Targets <b>2007</b>.  7(1): 29-41</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>16.   60878   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Development and validation of a high-throughput screen for inhibitors of SARS CoV and its application in screening of a 100,000-compound library</p>

    <p>          Severson, William E, Shindo, Nice, Sosa, Mindy, Fletcher, Thomas III, White, ELucile, Ananthan, Subramaniam, and Jonsson, Colleen B</p>

    <p>          J. Biomol. Screening <b>2007</b>.  12(1): 33-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>17.   60879   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          The emergence of adamantane resistance in influenza A(H1) viruses in Australia and regionally in 2006</p>

    <p>          Barr, IG, Hurt, AC, Deed, N, Iannello, P, Tomasov, C, and Komadina, N</p>

    <p>          Antiviral Res.  <b>2007</b>.  75(2): 173-176</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   60880   DMID-LS-145; SCIFINDER-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Influenza viruses and MAP kinase cascades - Novel targets for an antiviral intervention?</p>

    <p>          Ludwig, Stephan</p>

    <p>          Signal Transduction <b>2007</b>.  7(1): 81-88</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   60881   DMID-LS-145; WOS-DMID-5/18/2007</p>

    <p class="memofmt1-2">          The lamivudine resistant mutation rtM204I and its effect on HBV replication in vitro</p>

    <p>          Isom, HC, Heipertz, RA, Miller, TG, and Kelley, CM</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(5): A72</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708500341">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708500341</a> </p><br />

    <p>20.   60882   DMID-LS-145; WOS-DMID-5/18/2007</p>

    <p class="memofmt1-2">          In vitro studies of initiation of replication by the HCV RNA dependent RNA polymerase NS5B and the impact of NS5A</p>

    <p>          Quezada, EM and Kane, CM</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(5): A282</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708502200">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708502200</a> </p><br />
    <br clear="all">

    <p>21.   60883   DMID-LS-145; WOS-DMID-5/18/2007</p>

    <p class="memofmt1-2">          Systemic distribution of West Nile virus during encephalitis with a review of the literature</p>

    <p>          Armah, HB, Chute, DJ, Dulai, MP, Gyure, KA, Kleinschmidt-Demasters, B, Omalu, BI, Smith, R, Vinters, H, Wang, GJ, and Wiley, CA</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(5): A403-A404</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708503247">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708503247</a> </p><br />

    <p>22.   60884   DMID-LS-145; WOS-DMID-5/18/2007</p>

    <p class="memofmt1-2">          Arachidonic acid- and docosahexaenoic acid-enriched formulas modulate antigen-specific T cell responses against influenza virus in neonatal piglets</p>

    <p>          Bassaganya-Riera, J, Guri, AJ, Noble, AM, Reynolds, KA, King, J, Wood, CM, Ashby, M, Rai, D, and Hontecillas, R</p>

    <p>          FASEB JOURNAL <b>2007</b>.  21(5): A377</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708503124">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245708503124</a> </p><br />

    <p>23.   60885   DMID-LS-145; WOS-DMID-5/11/2007</p>

    <p class="memofmt1-2">          Synthesis, reactions, and antiviral activity of 6 &#39;-amino-2 &#39;-thioxo-1 &#39;,2 &#39;-dihydro-3,4 &#39;-bipyridine-3 &#39;,5 &#39;-dicarbonitrile</p>

    <p>          Attaby, FA, Elghandour, AHH, Ali, MA, and Ibrahem, YM</p>

    <p>          PHOSPHORUS SULFUR AND SILICON AND THE RELATED ELEMENTS <b>2007</b>.  182(4): 695-709</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245316700001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245316700001</a> </p><br />

    <p>24.   60886   DMID-LS-145; WOS-DMID-5/11/2007</p>

    <p class="memofmt1-2">          Recent expansion of highly pathogenic avian influenza H5N1: a critical review</p>

    <p>          Gauthier-Clerc, M, Lebarbenchon, C, and Thomas, F</p>

    <p>          IBIS <b>2007</b>.  149(2): 202-214</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312000002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245312000002</a> </p><br />

    <p>25.   60887   DMID-LS-145; WOS-DMID-5/11/2007</p>

    <p class="memofmt1-2">          Acyclic nucleoside phosphonates: Past, present and future - Bridging chemistry to HIV, HBV, HCV, HPV, adeno-, herpes-, and poxvirus infections: The phosphonate bridge</p>

    <p>          De Clercq, E</p>

    <p>          BIOCHEMICAL PHARMACOLOGY <b>2007</b>.  73(7): 911-922</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245401300001">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000245401300001</a> </p><br />

    <p>26.   60888   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Emergence of drug resistance: implications for antiviral control of pandemic influenza</p>

    <p>          Alexander, ME, Bowman, CS, Feng, Z, Gardam, M, Moghadas, SM, Rost, G, Wu, J, and Yan, P</p>

    <p>          Proc Biol Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507331&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507331&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>27.   60889   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Chemical and biological effects of substitution of the 2-position of ring-expanded (&#39;fat&#39;) nucleosides containing the imidazo[4,5-e][1,3]diazepine-4,8-dione ring system: The role of electronic and steric factors on glycosidic bond stability and anti-HCV activity</p>

    <p>          Zhang, P, Zhang, N, Buckwold, VE, and Hosmane, RS</p>

    <p>          Bioorg Med Chem <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507230&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17507230&amp;dopt=abstract</a> </p><br />

    <p>28.   60890   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Whole genome expression profiling of hepatitis B virus-transfected cell line reveals the potential targets of anti-HBV drugs</p>

    <p>          Ding, XR, Yang, J, Sun, DC, Lou, SK, and Wang, SQ</p>

    <p>          Pharmacogenomics J <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17505500&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17505500&amp;dopt=abstract</a> </p><br />

    <p>29.   60891   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Combinatorial computational approaches to identify tetracycline derivatives as flavivirus inhibitors</p>

    <p>          Yang, JM, Chen, YF, Tu, YY, Yen, KR, and Yang, YL</p>

    <p>          PLoS ONE <b>2007</b>.  2: e428</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17502914&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17502914&amp;dopt=abstract</a> </p><br />

    <p>30.   60892   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Hopes dashed for HCV drug</p>

    <p>          Anon</p>

    <p>          AIDS Patient Care STDS <b>2007</b>.  21(4): 290</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17500107&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17500107&amp;dopt=abstract</a> </p><br />

    <p>31.   60893   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Antiviral activity of arbidol against influenza A virus, respiratory syncytial virus, rhinovirus, coxsackie virus and adenovirus in vitro and in vivo</p>

    <p>          Shi, L, Xiong, H, He, J, Deng, H, Li, Q, Zhong, Q, Hou, W, Cheng, L, Xiao, H, and Yang, Z</p>

    <p>          Arch Virol <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497238&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17497238&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>32.   60894   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Complete inactivation of Venezuelan equine encephalitis virus by 1,5-iodonaphthylazide</p>

    <p>          Sharma, A, Raviv, Y, Puri, A, Viard, M, Blumenthal, R, and Maheshwari, RK</p>

    <p>          Biochem Biophys Res Commun <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17493582&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17493582&amp;dopt=abstract</a> </p><br />

    <p>33.   60895   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Avian influenza A (H5N1) infection: targets and strategies for chemotherapeutic intervention</p>

    <p>          De Clercq, E and Neyts, J</p>

    <p>          Trends Pharmacol Sci <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17481739&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17481739&amp;dopt=abstract</a> </p><br />

    <p>34.   60896   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          The anti-papillomavirus activity of human and bovine lactoferricin</p>

    <p>          Mistry, N, Drobni, P, Naslund, J, Sunkari, VG, Jenssen, H, and Evander, M</p>

    <p>          Antiviral Res <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17481742&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17481742&amp;dopt=abstract</a> </p><br />

    <p>35.   60897   DMID-LS-145; PUBMED-DMID-5/21/2007</p>

    <p class="memofmt1-2">          Derivatives of oxoisoaporphine alkaloids: A novel class of selective acetylcholinesterase inhibitors</p>

    <p>          Tang, H, Ning, FX, Wei, YB, Huang, SL, Huang, ZS, Chan, AS, and Gu, LQ</p>

    <p>          Bioorg Med Chem Lett <b>2007</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17451950&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=17451950&amp;dopt=abstract</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
