

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-07-04.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="mr/q9DcG9Ni9L1CqjpnRuAY0W24moHlduvlq3B0xYaNR8nsgD3VyBgNg64eAgUSpgVuQILDeMEgxR6bJPlU7XA6qatNyL3fzX0ul//I9BTEFoIn8QHXmMFqm6As=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="176F709D" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: June 21 - July 4, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23565693">Isolation and Characterization of a Proteinaceous Antifungal Compound from Lactobacillus plantarum YML007 and Its Application as a Food Preservative.</a> Ahmad Rather, I., B.J. Seo, V.J. Rejish Kumar, U.H. Choi, K.H. Choi, J.H. Lim, and Y.H. Park. Letters in Applied Microbiology, 2013. 57(1): p. 69-76. PMID[23565693].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />
    
    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23794598">Effects of Amphotericin B on Aspergillus flavus Clinical Isolates with Variable Susceptibilities to the Polyene in an Experimental Model of Systemic Aspergillosis.</a> Barchiesi, F., E. Spreghini, M. Sanguinetti, D. Giannini, E. Manso, P. Castelli, and C. Girmenia. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23794598].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">3.<a href="http://www.ncbi.nlm.nih.gov/pubmed/23290431">Search for a Potent Microbicidal Spermicide from the Isolates of Shorea robusta Resin.</a> Bharitkar, Y.P., M. Banerjee, S. Kumar, R. Paira, R. Meda, K. Kuotsu, and N.B. Mondal. Contraception, 2013. 88(1): p. 133-140. PMID[23290431].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23791641">Amphotericin B-Copper(II) Complex as a Potential Agent with Higher Antifungal Activity against Candida albicans.</a>Chudzik, B., I. Tracz, G. Czernel, M. Fiolka, G. Borsuk, and M. Gagos. European Journal of Pharmaceutical Sciences, 2013. <b>[Epub ahead of print]</b>. PMID[23791641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23804388">Isavuconazole Activity against Emerging Fungal Pathogens with Reduced Azole-susceptibility: Aspergillus lentulus, Neosartorya udagawae and Cryptococcus gattii.</a> Datta, K., P. Rhee, E. Byrnes, 3rd, G. Garcia-Effron, D.S. Perlin, J.F. Staab, and K.A. Marr. Journal of Clinical Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23804388].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23796919">Antifungal Activity of the Non Cytotoxic Human Peptide Hepcidin 20 against Fluconazole Resistant Candida glabrata in Human Vaginal Fluid.</a> Del Gaudio, G., L. Lombardi, G. Maisetta, S. Esin, G. Batoni, M. Sanguinetti, S. Senesi, and A. Tavanti. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23796919].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23793863">Antifungal Activities of Diphenyl diselenide Alone and in Combination with Fluconazole or Amphotericin B against Candida glabrata.</a>Denardi, L.B., D.A. Mario, E.S. de Loreto, C.W. Nogueira, J.M. Santurio, and S.H. Alves. Mycopathologia, 2013. <b>[Epub ahead of print]</b>. PMID[23793863].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23794449">Antifungal and Anticancer Effects of a Polysaccharide-protein Complex from the Gut Bacterium Raoultella ornithinolytica Isolated from the Earthworm Dendrobaena veneta.</a> Fiolka, M.J., K. Lewtak, J. Rzymowska, K. Grzywnowicz, M. Hulas-Stasiak, W. Sofinska-Chmiel, and K. Skrzypiec. Pathogens and Disease, 2013. <b>[Epub ahead of print]</b>. PMID[23794449].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23778117">KB425796-A, a Novel Antifungal Antibiotic Produced by Paenibacillus sp. 530603.</a> Kai, H., M. Yamashita, S. Takase, M. Hashimoto, H. Muramatsu, I. Nakamura, K. Yoshikawa, M. Ezaki, K. Nitta, M. Watanabe, N. Inamura, and A. Fujie. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23778117].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23778114">Identification of Ten KB425796-A Congeners from Paenibacillus sp. 530603 Using an Antifungal Assay against Aspergillus fumigatus in Combination with Micafungin.</a> Kai, H., M. Yamashita, S. Takase, M. Hashimoto, H. Muramatsu, I. Nakamura, K. Yoshikawa, R. Kanasaki, M. Ezaki, K. Nitta, M. Watanabe, N. Inamura, and A. Fujie. The Journal of Antibiotics, 2013. <b>[Epub ahead of print]</b>. PMID[23778114].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23801249">Antimicrobial Activity of the Synthetic Peptide Scolopendrasin II from the Centipede Scolopendra subspinipes mutilans.</a> Kwon, Y.N., J.H. Lee, I.W. Kim, S.H. Kim, E.Y. Yun, S.H. Nam, M.Y. Ahn, M. Jeong, D.C. Kang, I.H. Lee, and J.S. Hwang. Journal of Microbiology and Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23801249].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23581401">Antimicrobial Efficacy of Liposome-encapsulated Silver Ions and Tea Tree Oil against Pseudomonas aeruginosa, Staphylococcus aureus and Candida albicans.</a> Low, W.L., C. Martin, D.J. Hill, and M.A. Kenward. Letters in Applied Microbiology, 2013. 57(1): p. 33-39. PMID[23581401].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23545456">Antimicrobial Activity of Southern African Medicinal Plants with Dermatological Relevance: From an Ethnopharmacological Screening Approach, to Combination Studies and the Isolation of a Bioactive Compound.</a> Mabona, U., A. Viljoen, E. Shikanga, A. Marston, and S. Van Vuuren. Journal of Ethnopharmacology, 2013. 148(1): p. 45-55. PMID[23545456].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23806662">Evaluation of the Correlation of Caspofungin MICs and Treatment Outcome in Murine Infections by Wild Type Strains of Candida parapsilosis.</a> Salas, V., F.J. Pastor, J. Capilla, D.A. Sutton, E. Mayayo, A.W. Fothergill, M.G. Rinaldi, and J. Guarro. Diagnostic Microbiology and Infectious Disease, 2013. <b>[Epub ahead of print]</b>. PMID[23806662]</p>

    <p class="plaintext"> <b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23796928">Efficacy of Amphotericin B at Suboptimal Dose Combined with Voriconazole in a Murine Infection by Aspergillus fumigatus with Poor in Vivo Response to the Azole.</a> Sandoval-Denis, M., F.J. Pastor, J. Capilla, and J. Guarro. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23796928].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23800587">Antifungal Efficacy of Some Natural Phenolic Compounds against Significant Pathogenic and Toxinogenic Filamentous Fungi.</a> Zabka, M. and R. Pavela. Chemosphere, 2013. <b>[Epub ahead of print]</b>. PMID[23800587].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23800952">Detection of Inhibitors of Phenotypically Drug-tolerant Mycobacterium tuberculosis Using an in Vitro Bactericidal Screen.</a> Bassett, I.M., S. Lun, W.R. Bishai, H. Guo, J.R. Kirman, M. Altaf, and R.F. O&#39;Toole. Journal of Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23800952].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23805993">Synthesis of Chromone, Quinolone, and Benzoxazinone Sulfonamide Nucleosides as Conformationally Constrained Inhibitors of Adenylating Enzymes Required for Siderophore Biosynthesis.</a> Engelhart, C.A. and C.C. Aldrich. The Journal of Organic Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23805993].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23650159">Transition Metal-alpha-amino acid Complexes with Antibiotic Activity against Mycobacterium spp.</a> Karpin, G.W., J.S. Merola, and J.O. Falkinham, 3rd. Antimicrobial Agents and Chemotherapy, 2013. 57(7): p. 3434-3436. PMID[23650159].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23790070">Synthesis and 3D-QSAR Analysis of 2-Chloroquinoline Derivatives as H37RV MTB Inhibitors.</a> Khunt, R.C., V.M. Khedkar, and E.C. Coutinho. Chemical Biology &amp; Drug Design, 2013. <b>[Epub ahead of print]</b>. PMID[23790070].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23789822">Growth-inhibitory Activity of Natural and Synthetic Isothiocyanates against Representative Human Microbial Pathogens.</a> Kurepina, N., B.N. Kreiswirth, and A. Mustaev. Journal of Applied Microbiology, 2013. <b>[Epub ahead of print]</b>. PMID[23789822].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23796926">Radiologic Responses in Cynomolgous Macaques for Assessing Tuberculosis Chemotherapy Regimens.</a> Lin, P.L., T. Coleman, J.P. Carney, B.J. Lopresti, J. Tomko, D. Fillmore, V. Dartois, C. Scanga, L.J. Frye, C. Janssen, E. Klein, C.E. Barry, 3rd, and J.L. Flynn. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23796926].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23647650">A Novel Quinoline Derivative That Inhibits Mycobacterial FtsZ.</a> Mathew, B., L. Ross, and R.C. Reynolds. Tuberculosis, 2013. 93(4): p. 398-400. PMID[23647650].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23816500">Antimycobacterial Activity of Citrullus colocynthis (L.) Schrad. against Drug Sensitive and Drug Resistant Mycobacterium. tuberculosis and MOTT Clinical Isolates.</a> Mehta, A., G. Srivastva, S. Kachhwaha, M. Sharma, and S.L. Kothari. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23816500].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23798446">Diarylcoumarins Inhibit Mycolic acid Biosynthesis and Kill Mycobacterium tuberculosis by Targeting FadD32.</a> Stanley, S.A., T. Kawate, N. Iwase, M. Shimizu, A.E. Clatworthy, E. Kazyanskaya, J.C. Sacchettini, T.R. Ioerger, N.A. Siddiqi, S. Minami, J.A. Aquadro, S. Schmidt Grant, E.J. Rubin, and D.T. Hung. Proceedings of the National Academy of Sciences of the United States of America, 2013. <b>[Epub ahead of print]</b>. PMID[23798446].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23792125">In Vivo Antimalarial Activity of Keetia leucantha Twigs Extracts and in Vitro Antiplasmodial Effect of Their Constituents.</a> Bero, J., M.F. Herent, G. Schmeda-Hirschmann, M. Frederich, and J. Quetin-Leclercq. Journal of Ethnopharmacology, 2013. <b>[Epub ahead of print]</b>. PMID[23792125].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23653352">In Silico Screening on the Three-dimensional Model of the Plasmodium vivax SUB1 Protease Leads to the Validation of a Novel Anti-parasite Compound.</a> Bouillon, A., D. Giganti, C. Benedet, O. Gorgette, S. Petres, E. Crublet, C. Girard-Blanc, B. Witkowski, D. Menard, M. Nilges, O. Mercereau-Puijalon, V. Stoven, and J.C. Barale. The Journal of Biological Chemistry, 2013. 288(25): p. 18561-18573. PMID[23653352].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23804074">Chemical and Genetic Validation of Thiamine Utilization as an Antimalarial Drug Target.</a> Chan, X.W., C. Wrenger, K. Stahl, B. Bergmann, M. Winterberg, I.B. Muller, and K.J. Saliba. Nature Communications, 2013. 4: p. 2060. PMID[23804074].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23629719">In Vitro and in Vivo Antimalarial Efficacies of Optimized Tetracyclines.</a> Draper, M.P., B. Bhatia, H. Assefa, L. Honeyman, L.K. Garrity-Ryan, A.K. Verma, J. Gut, K. Larson, J. Donatelli, A. Macone, K. Klausner, R.G. Leahy, A. Odinecs, K. Ohemeng, P.J. Rosenthal, and M.L. Nelson. Antimicrobial Agents and Chemotherapy, 2013. 57(7): p. 3131-3136. PMID[23629719].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23801363">In Vivo Antimalarial Activity of Trichilia megalantha Harms Extracts and Fractions in Animal Models.</a> Fadare, D.A., O.O. Abiodun, and E.O. Ajaiyeoba. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23801363].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23539746">HIV Treatments Have Malaria Gametocyte Killing and Transmission Blocking Activity.</a> Hobbs, C.V., T.Q. Tanaka, O. Muratova, J. Van Vliet, W. Borkowsky, K.C. Williamson, and P.E. Duffy. The Journal of Infectious Diseases, 2013. 208(1): p. 139-148. PMID[23539746].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23806111">Potential of Lichen Secondary Metabolites against Plasmodium Liver Stage Parasites with FAS-II as the Potential Target.</a> Lauinger, I.L., L. Vivas, R. Perozzo, C. Stairiker, A. Tarun, M. Zloh, X. Zhang, H. Xu, P.J. Tonge, S.G. Franzblau, D.H. Pham, C.V. Esguerra, A.D. Crawford, L. Maes, and D. Tasdemir. Journal of Natural Products, 2013. 76(6): p. 1064-1070. PMID[23806111].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23680445">Falcipain-2 Inhibition by Suramin and Suramin Analogues.</a> Marques, A.F., D. Esser, P.J. Rosenthal, M.U. Kassack, and L.M. Lima. Bioorganic &amp; Medicinal Chemistry, 2013. 21(13): p. 3667-3673. PMID[23680445].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23604568">Effect of Chloroquine, Methylene Blue and Artemether on Red Cell and Hepatic Antioxidant Defence System in Mice Infected with Plasmodium yoelii nigeriensis.</a> Nneji, C.M., O.A. Adaramoye, C.O. Falade, and O.G. Ademowo. Parasitology Research, 2013. 112(7): p. 2619-2625. PMID[23604568].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23795673">Synthesis and Antiprotozoal Activity of Dicationic m-Terphenyl and 1,3-Dipyridylbenzene Derivatives.</a> Patrick, D.A., M.A. Ismail, R.K. Arafa, T. Wenzler, X. Zhu, T. Pandharkar, S.K. Jones, K.A. Werbovetz, R. Brun, D.W. Boykin, and R.R. Tidwell. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23795673].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">37. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23806014">Bioactive Compounds from the Roots of Strophioblachia fimbricalyx.</a> Seephonkai, P., S.G. Pyne, A.C. Willis, and W. Lie. Journal of Natural Products, 2013. <b>[Epub ahead of print]</b>. PMID[23806014].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p><br />

    <p class="plaintext">38. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23793360">Antimalarial Activity of Bergenia ciliata (Haw.) Sternb. against Plasmodium berghei</a>. Walter, N.S., U. Bagai, and S. Kalia. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23793360].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0621-070413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">39. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000318870600010">N &#39;- 4- (Substituted imino)methyl benzylidene -substituted benzohydrazides: Synthesis, Antimicrobial, Antiviral, and Anticancer Evaluation, and QSAR Studies.</a> Kumar, P., B. Narasimhan, K. Ramasamy, V. Mani, R.K. Mishra, A.A. Majeed, and E. De Clercq. Monatshefte fur Chemie, 2013. 144(6): p. 825-849. ISI[000318870600010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0621-070413.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
