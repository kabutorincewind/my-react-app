

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-68.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="qAAFYhjaOg2M981vpeYjvEzktCNgqgWioetnXkHz45KvYowe8GDD3byVFlfDWx//8mbrr8fqVp2qdepNqMmkcKM/7okqyTYmp6PwIZJypVdKtyg0+fw1HdX0lI0=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="10C0EFF6" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-68-MEMO</p>

<p>    1.    6033   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Anti-herpes simplex virus activity of sulfated galactans from the red seaweeds Gymnogongrus griffithsiae and Cryptonemia crenulata</p>

<p>           Talarico, LB, Zibetti, RG, Faria, PC, Scolaro, LA, Duarte, ME, Noseda, MD, Pujol, CA, and Damonte, EB</p>

<p>           Int J Biol Macromol 2004. 34(1-2): 63-71</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178011&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15178011&amp;dopt=abstract</a> </p><br />

<p>    2.    6034   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Synthesis of new 2(&#39;)-beta-C-methyl related triciribine analogues as anti-HCV agents</p>

<p>           Smith, KL, Lai, VC, Prigaro, BJ, Ding, Y, Gunic, E, Girardet, JL, Zhong, W, Hong, Z, Lang, S, and An, H</p>

<p>           Bioorg Med Chem Lett 2004. 14(13):  3517-20</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15177464&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15177464&amp;dopt=abstract</a> </p><br />

<p>    3.    6035   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Antiviral Activity of Antimicrobial Cationic Peptides Against Junin Virus and Herpes Simplex Virus</b></p>

<p>           Matanic, VCA and Castilla, V</p>

<p>           International Journal of Antimicrobial Agents 2004. 23(4): 382-389</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    4.    6036   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p><b>           Thiourea inhibitors of herpes viruses. Part 2: N-Benzyl-N&#39;-arylthiourea inhibitors of CMV</b> </p>

<p>           Bloom, JD, Dushin, RG, Curran, KJ, Donahue, F, Norton, EB, Terefenko, E, Jones, TR, Ross, AA, Feld, B, Lang, SA, and DiGrandi, MJ</p>

<p>           Bioorg Med Chem Lett 2004. 14(13):  3401-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15177441&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15177441&amp;dopt=abstract</a> </p><br />

<p>    5.    6037   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Clinical and biologic aspects of human cytomegalovirus resistance to antiviral drugs</p>

<p>           Baldanti, F, Lurain, N, and Gerna, G</p>

<p>           Hum Immunol 2004. 65(5): 403-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15172438&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15172438&amp;dopt=abstract</a> </p><br />

<p>    6.    6038   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Selective human enterovirus and rhinovirus inhibitors: An overview of capsid-binding and protease-inhibiting molecules</p>

<p>           Shih, SR, Chen, SJ, Hakimelahi, GH, Liu, HJ, Tseng, CT, and Shia, KS</p>

<p>           Med Res Rev 2004. 24(4): 449-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170592&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15170592&amp;dopt=abstract</a> </p><br />

<p>    7.    6039   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           The significance of interferon and ribavirin combination therapy followed by interferon monotherapy for patients with chronic hepatitis C in Japan</p>

<p>           Hiramatsu, Naoki, Kasahara, Akinori, Nakanishi, Fumihiko, Toyama, Takashi, Tsujii, Masahiko, Tsuji, Shingo, Kanto, Tatsuya, Takehara, Tetsuo, Kato, Michio, and Yoshihara, Harumasa</p>

<p>           Hepatology Research 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T74-4CGMFY9-1/2/104b9f6d9024d6ca128c1c6625d93ba2">http://www.sciencedirect.com/science/article/B6T74-4CGMFY9-1/2/104b9f6d9024d6ca128c1c6625d93ba2</a> </p><br />

<p>    8.    6040   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Antiviral prophylaxis of smallpox</p>

<p>           Bray, M and Roy, CJ</p>

<p>           J Antimicrob Chemother 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163655&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163655&amp;dopt=abstract</a> </p><br />

<p>    9.    6041   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           Unfolding targets for dengue fever</p>

<p>           Shadan, Sadaf </p>

<p>           Drug Discovery Today 2004. 9(8): 344</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T64-4C46X7B-2/2/600f41c464bac95b1f7204db47ab6050">http://www.sciencedirect.com/science/article/B6T64-4C46X7B-2/2/600f41c464bac95b1f7204db47ab6050</a> </p><br />

<p>  10.    6042   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Toxic effects of bis(thiosemicarbazone) compounds and its palladium(II) complexes on herpes simplex virus growth</p>

<p>           Genova, P, Varadinova, T, Matesanz, AI, Marinova, D, and Souza, P</p>

<p>           Toxicol Appl Pharmacol 2004. 197(2):  107-12</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163546&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163546&amp;dopt=abstract</a> </p><br />

<p>  11.    6043   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Surveillance of influenza isolates for susceptibility to neuraminidase inhibitors during the 2000-2002 influenza seasons</p>

<p>           Mungall, BA, Xu, X, and Klimov, A</p>

<p>           Virus Res 2004. 103(1-2): 195-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163509&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163509&amp;dopt=abstract</a> </p><br />

<p>  12.    6044   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           A novel means of identifying the neuraminidase type of currently circulating human A(H1) influenza viruses</p>

<p>           Hurt, AC, Barr, IG, Komadina, N, and Hampson, AW</p>

<p>           Virus Res 2004. 103(1-2): 79-83</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163493&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15163493&amp;dopt=abstract</a> </p><br />

<p>  13.    6045   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Identification of a Small Molecule that Inhibits Herpes Simplex Virus DNA Polymerase Subunit Interactions and Viral Replication</p>

<p>           Pilger, BD, Cui, C, and Coen, DM</p>

<p>           Chem Biol 2004. 11(5): 647-54</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15157875&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15157875&amp;dopt=abstract</a> </p><br />

<p>  14.    6046   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Testing the Antiviral Activity of Novel Ribavirin Derivatives and Ifn Alpha Variants in the Hcv Replicon System</b></p>

<p>           Escuret, V, Durantel, D, Agrofoglio, L, Maral, J, Escary, JL, Trepo, C, and Zoulim, F</p>

<p>           Journal of Hepatology 2004. 40: 29</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  15.    6047   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Vx-950, a Novel Hcv Protease Inhibitor, Retains Potency Against Biln-2061 Resistant Replicon Cells: Computational Analysis Indicates That Resistance Develops Via Different Mechanisms</b></p>

<p>           Lin, C, Lin, K, Luong, YP, Rao, BG, Wei, YY, Brennan, D, Fulghum, J, Frantz, D, Hsiao, HM, Ma, S, Maxwell, J, Perni, RB, Gates, CA, and Kwong, AD</p>

<p>           Journal of Hepatology 2004. 40: 59</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  16.    6048   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Inhibition of Hbv-Particle Assembly and Viral Replication by Cell Permeable Peptides Competing the Nucleocapsid/Surface Protein Interaction</b></p>

<p>           Malkowski, B, Huser, H, and Hildt, E</p>

<p>           Journal of Hepatology 2004. 40: 371</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  17.    6049   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Characterization of SARS main protease and inhibitor assay using a fluorogenic substrate</p>

<p>           Kuo, CJ, Chi, YH, Hsu, JT, and Liang, PH</p>

<p>           Biochem Biophys Res Commun 2004. 318(4):  862-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15147951&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15147951&amp;dopt=abstract</a> </p><br />

<p>  18.    6050   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           A phase II, placebo-controlled study of merimepodib (XV-497), in combination with pegylated interferon-alfa, and ribavirin in patients with chronic hepatitis C non-responsive to previous therapy with interferon-alfa and ribavirin</p>

<p>           Marcellin, P, Horsmans, Y, Nevens, F, Grange, JD, Bronowicki, JP, Vetter, D, Kauffman, R, Knox, S, McNair, L, Moseley, S, and Alam, J</p>

<p>           Journal of Hepatology 2004. 40(Supplement 1): 145</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-KK/2/c80c36029583978229fa14ff8d13f6c6">http://www.sciencedirect.com/science/article/B6W7C-4C5WW3G-KK/2/c80c36029583978229fa14ff8d13f6c6</a> </p><br />

<p>  19.    6051   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Inactivation of human adenovirus genome by different groups of disinfectants</p>

<p>           Sauerbrei, A, Sehr, K, Eichhorn, U, Reimer, K, and Wutzler, P</p>

<p>           J Hosp Infect  2004. 57(1): 67-72</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142718&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142718&amp;dopt=abstract</a> </p><br />

<p>  20.    6052   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Sensitivity of human adenoviruses to different groups of chemical biocides</p>

<p>           Sauerbrei, A, Sehr, K, Brandstadt, A, Heim, A, Reimer, K, and Wutzler, P</p>

<p>           J Hosp Infect  2004. 57(1): 59-66</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142717&amp;dopt=abstract</a> </p><br />

<p>  21.    6053   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Drug combination treats HIV and HCV</p>

<p>           Anon</p>

<p>           AIDS Patient Care STDS 2004. 18(4): 251</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142356&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15142356&amp;dopt=abstract</a> </p><br />

<p>  22.    6054   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Design, Synthesis and Characterization of Photolabeling Probes for the Study of the Mechanisms of the Antiviral Effects of Ribavirin</b></p>

<p>           Wu, QG, Qu, FQ, Wan, JQ, Zhu, X, Xin, Y, and Peng, L</p>

<p>           Helvetica Chimica Acta 2004. 87(4): 811-819</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    6055   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Bicyclic Anti-Vzv Nucleosides: Thieno Analogues Bearing an Alkylphenyl Side Chain Have Reduced Antiviral Activity</b></p>

<p>           Angell, A, Mcguigan, C, Sevillano, LG , Snoeck, R, Andrei, G, De Clercq, E, and Balzarini, J</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(10): 2397-2399</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  24.    6056   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           Use of siRNAs to prevent and treat influenza virus infection</p>

<p>           Ge, Qing, Eisen, Herman N, and Chen, Jianzhu</p>

<p>           Virus Research 2004. 102(1): 37-42</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T32-4BWYHBC-1/2/de5713a217fcc40dc510e11f3e71685c">http://www.sciencedirect.com/science/article/B6T32-4BWYHBC-1/2/de5713a217fcc40dc510e11f3e71685c</a> </p><br />

<p>  25.    6057   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>The Virological Basis of the Hypersensitivity of Hbv, Hcv and Pestiviruses Sensitivity to Glucosidase Inhibitors</b></p>

<p>           Block, TM, Simek, E, Mehta, AS, Norton, P, Gu, B, Gong, J, and Zhou, T</p>

<p>           Antiviral Research 2004. 62(2): 53</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  26.    6058   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>A Cell-Based Assay for Screening of Potential Antiviral Agents Against Hcv</b></p>

<p>           Zauberman, A, Ilan, E, Aviel, S, Slama, D, Tsionas, I, Shochat, K, Landstein, D, Eren, R, Miyamura, T, Nagamori, S, and Dagan, S</p>

<p>           Antiviral Research 2004. 62(2): 55</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  27.    6059   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Multiple enzymatic activities associated with severe acute respiratory syndrome coronavirus helicase</p>

<p>           Ivanov, KA, Thiel, V, Dobbe, JC, van, der Meer Y, Snijder, EJ, and Ziebuhr, J</p>

<p>           J Virol 2004. 78(11): 5619-32</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140959&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15140959&amp;dopt=abstract</a> </p><br />

<p>  28.    6060   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Screening for Hepatitis C Virus Antiviral Activity With a Cell- Based Secreted Alkaline Phosphatase Reporter Replicon System</b></p>

<p>           Bourne, N, Pyles, R, Yi, MK, Veselenak, R, Davis, M, and Lemon, S</p>

<p>           Antiviral Research 2004. 62(2): 59</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  29.    6061   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           Synthesis of 2&#39;-deoxy-2&#39;-[18F]fluoro-5-bromo-1-[beta]-D-arabinofuranosyluracil ([18F]-FBAU) and 2&#39;-deoxy-2&#39;-[18F]fluoro-5-chloro-1-[beta]-D-arabinofuranosyl-uracil ([18F]-FCAU), and their biological evaluation as markers for gene expression</p>

<p>           Alauddin, Mian M, Shahinian, Antranic, Park, Ryan, Tohme, Michel, Fissekis, John D, and Conti, Peter S</p>

<p>           Nuclear Medicine and Biology 2004.  31(4): 399-405</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T9Y-4C5FTGP-2/2/6c4ed6d73be4a0eacdb806e6452cd931">http://www.sciencedirect.com/science/article/B6T9Y-4C5FTGP-2/2/6c4ed6d73be4a0eacdb806e6452cd931</a> </p><br />

<p>  30.    6062   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>In Vitro Combination Studies of Vx-950 (a Hcv Protease Inhibitor) or Vx-497 (an Impdh Inhibitor), Two Novel Anti-Hcv Clinical Candidates, With Ifn-Alpha</b></p>

<p>           Lin, K, Lin, C, and Kwong, AD</p>

<p>           Antiviral Research 2004. 62(2): 64</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    6063   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Inhibitors of Herpes Simplex Virus Studied in a Murine Infection Model of the Immunocompromised Host</b></p>

<p>           Lakra, G and  Field, HJ</p>

<p>           Antiviral Research 2004. 62(2): 89</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    6064   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Although the Bicyclic Nucleoside Analogues (Bcnas) Are Highly Active Against Varicella-Zoster Virus (Vzv), They Are Inactive Against Simian Varicella Virus (Svv) Despite Efficient Phosphorylation of the Bcnas by Svv-Encoded Thymidine Kinase</b></p>

<p>           Sienaert, R,  Andrei, G, Snoeck, R, De Clercq, E, Luoni, G, Mcguigan, C, and Balzarini, J</p>

<p>           Antiviral Research 2004. 62(2): 94</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    6065   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Antiviral Activity of Glycyrrhizic Acid (Gl) Derivatives Against Sars-Coronavirus (Sars-Cov) and Human Cytomegalovirus (Hcmv)</b></p>

<p>           Hoever, G, Baltina, L, Kondratenko, R , Baltina, L, Tolstikov, G, Doerr, HW, and Cinatl, J</p>

<p>           Antiviral Research 2004. 62(2): 120</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  34.    6066   DMID-LS-68; PUBMED-DMID-6/8/2004</p>

<p class="memofmt1-2">           Cost Effectiveness of Peginterferon alpha-2a Plus Ribavirin versus Interferon alpha-2b Plus Ribavirin as Initial Therapy for Treatment-Naive Chronic Hepatitis C</p>

<p>           Arguedas, M</p>

<p>           Pharmacoeconomics 2004. 22(7): 477-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15137884&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15137884&amp;dopt=abstract</a> </p><br />

<p>  35.    6067   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Influenza-Inhibitory Effects of Viramidine in Cell and Animal Systems</b></p>

<p>           Sidwell, RW,  Bailey, KW, Wong, MH, and Smee, DF</p>

<p>           Antiviral Research 2004. 62(2): 125</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    6068   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Protease Inhibitors From Streptomyces Inhibit Influenza Virus Replication</b></p>

<p>           Serkedjieva, J, Angelova, L, and Ivanova, I</p>

<p>           Antiviral Research 2004. 62(2): 128</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  37.    6069   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Nucleoside Inhibitors of Hcv Replication and the Corresponding Nucleoside Triphosphate Inhibitors as Chain Terminators of Hcvns5b Polymerase</b></p>

<p>           Lou, L, Hancock, C, Latour, D, Pouliot, J, Roberts, C, Keicher, J, Dyatkina, N, Griffith, R, Schmitz, U, and Michelotti, E</p>

<p>           Antiviral Research 2004. 62(2): 142</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  38.    6070   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>In Vitro and in Vivo Evaluation of Hcv Polymerase Inhibitors as Potential Drug Candidates for Treatment of Chronic Hepatitis C Infection</b></p>

<p>           Ilan, E, Zauberman, A, Aviel, S, Nussbaum, O, Arazi, E, Ben-Moshe, O, Slama, D, Tzionas, I, Shoshany, Y, Lee, SW, Han, JJ, Park, SJ, Lee, GH, Park, EY, Shin, JC, Suh, JW, Kim, JW, and Dagan, S</p>

<p>           Antiviral Research 2004. 62(2): 143</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  39.    6071   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Susceptibility of Different Genotypes of Hepatitis C Virus to Inhibition by Nucleoside and Non-Nucleoside Inhibitors</b></p>

<p>           Carroll, S, Bailey, C, Bosserman, M, Burlein, C, Simcoe, A, Fay, J, Ludmerer, S, Graham, D, Lafemina, R, Flores, O, Hazuda, D, Altamura, S, Migliaccio, G, Tomei, L, De Francesco, R, Summa, V, and Olsen, D</p>

<p>           Antiviral Research 2004. 62(2): 144</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  40.    6072   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Pg 301029 Inhibits the Hcv Surrogate Bvdv Through a Novel Late Stage Mechanism of Action</b></p>

<p>           Buckheit, RW, Parsley, TB, and Brenner, TL</p>

<p>           Antiviral Research 2004. 62(2): 150</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  41.    6073   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Synthesis and Anti-Hcmv Activity of 1-Acyl-Beta-Lactams and 1- Acylazetidines Derived From Phenylalanine</b></p>

<p>           Gerona-Navarro, G, De Vega, MJP, Garcia-Lopez, MT, Andrei, G, Snoeck, R, Balzarini, J, De Clercq, E, and Gonzalez-Muniz, R</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(9): 2253-2256</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.    6074   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           Synthesis and activity of new aryl- and heteroaryl-substituted 5,6-dihydro-4H-pyrrolo[1,2-b]pyrazole inhibitors of the transforming growth factor-[beta] type I receptor kinase domain</p>

<p>           Scott Sawyer, J, Beight, Douglas W, Britt, Karen S, Anderson, Bryan D, Campbell, Robert M, Goodson, Jr Theodore, Herron, David K, Li, Hong-Yu, and McMillen, William T</p>

<p>           Bioorganic &amp; Medicinal Chemistry Letters 2004. 14(13): 3581-3584</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TF9-4CBVKY1-6/2/6720513dff75329cf08d625c46b8881d">http://www.sciencedirect.com/science/article/B6TF9-4CBVKY1-6/2/6720513dff75329cf08d625c46b8881d</a> </p><br />

<p>  43.    6075   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Design, Synthesis, and Characterization of a Series of Cytochrome P-450 3a-Activated Prodrugs (Hepdirect Prodrugs) Useful for Targeting Phosph(on)Ate-Based Drugs to the Liver</b></p>

<p>           Erion, MD, Reddy, KR, Boyer, SH, Matelich, MC, Gornez-Galeno, J, Lemus, RH, Ugarkar, BG, Colby, TJ, Schanzer, J, and Van Poelje, PD</p>

<p>           Journal of the American Chemical Society 2004. 126(16): 5154-5163</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  44.    6076   DMID-LS-68; EMBASE-DMID-6/8/2004</p>

<p class="memofmt1-2">           In vitro susceptibility of 10 clinical isolates of SARS coronavirus to selected antiviral compounds</p>

<p>           Chen, F, Chan, KH, Jiang, Y, Kao, RYT, Lu, HT, Fan, KW, Cheng, VCC, Tsui, WHW, Hung, IFN, and Lee, TSW</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4CDJJVD-1/2/cda32a3bb2a690dbbbb7a5b094fd6d01">http://www.sciencedirect.com/science/article/B6VJV-4CDJJVD-1/2/cda32a3bb2a690dbbbb7a5b094fd6d01</a> </p><br />

<p>  45.    6077   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>New Treatments for Cutaneous Human Papillomavirus Infection</b></p>

<p>           Majewski, S and Jablonska, S</p>

<p>           Journal of the European Academy of Dermatology and Venereology 2004. 18(3): 262-264</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  46.    6078   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Inhibition of Orthopoxvirus Dna Polymerases by Cidofovir Diphosphate: in Vitro Enzymatic Studies Using Highly Purified Vaccinia Virus Dna Polymerase</b></p>

<p>           Evans, DH, Magee, W, and Hostetler, KY</p>

<p>           Antiviral Research 2004. 62(2): 74</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  47.    6079   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Inhibition of Sars Coronavirus in Vitro by Human Interferons</b></p>

<p>           Barnard, DL,  Hubbard, VD, Burton, J, Smee, DF, Morrey, JD, and Sidwell, RW</p>

<p>           Antiviral Research 2004. 62(2): 77</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  48.    6080   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Inhibitory Activity of Vancomycin, Eremomycin and Teicoplanin Aglycon Derivatives Against Feline and Human (I.e. Sars) Coronaviruses</b></p>

<p>           Balzarini, J, Keyaerts, E, Vijgen, L, De Clercq, E, Printsevskaya, SS, Preobrazhenskaya, M, and Van Ranst, M</p>

<p>           Antiviral Research 2004. 62(2): 78</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  49.    6081   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>In Vitro Investigation of Potential Therapeutics for the Severe Acute Respiratory Syndrome (Sars)</b></p>

<p>           Doerr, HW, Michaelis, M, Preiser, W, and Cinatl, J</p>

<p>           Antiviral Research 2004. 62(2): 79</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  50.    6082   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Towards a Model for Small Molecule Inhibition of Virus Fusion: Photoaffinity Labeling Identifies the Binding Pocket of an Rsv Inhibitor</b></p>

<p>           Roach, J, Cianci, C, Dischino, D, Langley, D, Sun, Y, Yu, KL, Meanwell, N, and Krystal, M</p>

<p>           Antiviral Research 2004. 62(2): 81</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  51.    6083   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>A Novel Respiratory Syncytial Virus Inhibitor</b></p>

<p>           Alber, D, Wilson, L, Baxter, B, Henderson, E, Dowdell, V, Kelsey, R, Keegan, S, Harris, R, Mcnamara, D, Bithell, S, Weerasekera, N, Harland, R, Stables, J, Cockerill, S, Powell, K, and  Carter, M</p>

<p>           Antiviral Research 2004. 62(2): 82</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  52.    6084   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Antiviral and Virucidal Activities of Oreganol P73-Based Spice Extracts Against Human Coronavirus in Vitro</b></p>

<p>           Ijaz, MK, Chen, Z, Raja, SS, Suchmann, DB, Royt, PW, Ingram, C, Gray, JK, and Paolilli, G</p>

<p>           Antiviral Research 2004. 62(2): 121</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  53.    6085   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Mannose-Specific Plant Lectins Are Potent Inhibitors of Coronavirus Infection Including the Virus Causing Sars</b></p>

<p>           Balzarini, J, Vijgen, L, Keyaerts, E, Van Damme, E, Peumans, W, De Clercq, E, Egberink, H, and Van Ranst, M</p>

<p>           Antiviral Research 2004. 62(2): 122</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  54.    6086   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Antiviral Effect of Plant Compounds of the Alliaceae Family Against the Sars Coronavirus</b></p>

<p>           Vijgen, L, Keyaerts, B, Van Damme, E, Peumans, W, De Clercq, E, Balzarini, J, and Van Ranst, M</p>

<p>           Antiviral Research 2004. 62(2): 123</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  55.    6087   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>The Identification of Novel Small Molecule Inhibitors of Respiratory Syncytial Virus</b></p>

<p>           Stabies, J, Carter, M, Alber, D, Henderson, E, Dowdell, V, Kelsey, R, Keegan, S, Baxter, B, Cockerill, S, Taylor, D, Chambers, P, Tymms, S, Powell, K, and Wilson, L</p>

<p>           Antiviral Research 2004. 62(2): 129</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  56.    6088   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Identification and Characterization of a Potent and Specific Small Molecule Inhibitor of Human Rotavirus</b></p>

<p>           Kealey, M, Flay, L, Demenczuk, T, Bailey, T, Rhodes, G, Young, DC, Pevear, DC, and Laquerre, S</p>

<p>           Antiviral Research 2004. 62(2): 139</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  57.    6089   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Antiviral Action of Interferons on Human Coronavirus</b></p>

<p>           Dianzani, E,  Scagnolari, C, Vincenzi, E, Bellomi, F, Clementi, M, and Antonelli, G</p>

<p>           Antiviral Research 2004. 62(2): 152</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  58.    6090   DMID-LS-68; WOS-DMID-6/10/2004</p>

<p>           <b>Novel Small Molecule Inhibitors of Rhinovirus Replication That Target the Hrv 2b Nonstructural Protein</b></p>

<p>           Tenney, DJ, Bergstrom, CP, Pokornowski, KP, Levine, SM, Hernandez, D, Zhang, S, He, F, Zhai, G, Mcphee, F, Weinheimer, SP, Romine, JL, and Colonno, RJ</p>

<p>           Antiviral Research 2004. 62(2): 154</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
