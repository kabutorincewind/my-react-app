

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="DMID-LS-61.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MeqXT7rwBgAh5Y6Oce1TOuSExc8YBP7eg7UZxJlUcL4h6OYawXuy6dqKRXApyDORmkSk3w/cxkY8cWPzVAbCcKEDdRaYdna0ZuRsFFXYfbxVVc6NeTeq1h39ToM=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="9C441BF1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1"><b>ONLINE DATABASE SEARCH - DMID-LS-61-MEMO</b> </p>

<p>    1.    5674   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Antiviral action of ribavirin in chronic hepatitis C</p>

<p>           Pawlotsky, JM, Dahari, H, Neumann, AU, HezoDe C, Germanidis, G, Lonjon, I, Castera, L, and Dhumeaux, D</p>

<p>           Gastroenterology 2004. 126(3): 703-14</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14988824&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14988824&amp;dopt=abstract</a> </p><br />

<p>    2.    5675   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           A new modified DNA enzyme that targets influenza virus A mRNA inhibits viral infection in cultured cells</p>

<p>           Takahashi, H, Hamazaki, H, Habu, Y, Hayashi, M, Abe, T, Miyano-Kurosaki, N, and Takaku, H</p>

<p>           FEBS Lett 2004. 560(1-3): 69-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14988000&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14988000&amp;dopt=abstract</a> </p><br />

<p>    3.    5676   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Antiviral activity of caspase inhibitors: effect on picornaviral 2A proteinase</p>

<p>           Deszcz, L, Seipelt, J, Vassilieva, E, Roetzer, A, and Kuechler, E</p>

<p>           FEBS Lett 2004. 560(1-3): 51-5</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987997&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14987997&amp;dopt=abstract</a> </p><br />

<p>    4.    5677   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Antiviral pyrazines and pharmaceutical compositions containing them</b>((Toyama Chemical Co., Ltd. Japan)</p>

<p>           Egawa, Hiroyuki, Sugita, Atsushi, and Furuta, Yosuke</p>

<p>           PATENT: JP 2004043371 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2002-7517; PP: 41 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    5.    5678   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Methods for identification of inhibitors of Ebola virus nucleocapsid assembly for use in vaccines</b> ((The Government of the United States of America, as Represented by the Secretary Department of Health and Human Services USA)</p>

<p>           Nabel, Gary and Huang, Yue</p>

<p>           PATENT: WO 2004007747 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 97 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>    6.    5679   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Role of lopinavir/ritonavir in the treatment of SARS: initial virological and clinical findings</p>

<p>           Chu, CM, Cheng, VC, Hung, IF, Wong, MM, Chan, KH, Chan, KS, Kao, RY, Poon, LL, Wong, CL, Guan, Y, Peiris, JS, and Yuen, KY</p>

<p>           Thorax 2004. 59(3): 252-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985565&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985565&amp;dopt=abstract</a> </p><br />

<p>    7.    5680   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Inactivity of the bicyclic pyrimidine nucleoside analogues against simian varicella virus (SVV) does not correlate with their substrate activity for SVV-encoded thymidine kinase</p>

<p>           Sienaert, R, Andrei, G, Snoeck, R, De Clercq, E, McGuigan, C, and Balzarini, J</p>

<p>           Biochem Biophys Res Commun 2004. 315(4):  877-83</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985094&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14985094&amp;dopt=abstract</a> </p><br />

<p>    8.    5681   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Hepatitis C NS3 protease inhibition by peptidyl-alpha-ketoamide inhibitors: kinetic mechanism and structure</p>

<p>           Liu, Y, Stoll, VS, Richardson, PL, Saldivar, A, Klaus, JL, MolLa A, Kohlbrenner, W, and Kati, WM</p>

<p>           Arch Biochem Biophys 2004. 421(2):  207-16</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14984200&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14984200&amp;dopt=abstract</a> </p><br />

<p>    9.    5682   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Geldanamycin, a Ligand of Heat Shock Protein 90, Inhibits the Replication of Herpes Simplex Virus Type 1 In Vitro</p>

<p>           Li, YH, Tao, PZ, Liu, YZ, and Jiang, JD</p>

<p>           Antimicrob Agents Chemother 2004.  48(3): 867-872</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982777&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982777&amp;dopt=abstract</a> </p><br />

<p>  10.    5683   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Effect of zinc salts on respiratory syncytial virus replication</p>

<p>           Suara, RO and Crowe, JE Jr</p>

<p>           Antimicrob Agents Chemother 2004.  48(3): 783-90</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982765&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14982765&amp;dopt=abstract</a> </p><br />

<p>  11.    5684   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Pegylated interferon-alpha protects type 1 pneumocytes against SARS coronavirus infection in macaques</p>

<p>           Haagmans, BL, Kuiken, T, Martina, BE, Fouchier, RA, Rimmelzwaan, GF, Van Amerongen, G, Van Riel, D, De Jong, T, Itamura, S, Chan, KH, Tashiro, M, and Osterhaus, AD</p>

<p>           Nat Med 2004. 10(3): 290-3</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14981511&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14981511&amp;dopt=abstract</a> </p><br />

<p>  12.    5685   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Respiratory syncytial virus inhibitors. Part 2: Benzimidazol-2-one derivatives</p>

<p>           Yu, KL, Zhang, Y, Civiello, RL, Trehan, AK, Pearce, BC, Yin, Z, Combrink, KD, Gulgeze, HB, Wang, XA, Kadow, KF, Cianci, CW, Krystal, M, and Meanwell, NA</p>

<p>           Bioorg Med Chem Lett 2004. 14(5): 1133-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980651&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980651&amp;dopt=abstract</a> </p><br />

<p>  13.    5686   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Spirobipyridopyrans, spirobinaphthopyrans, indolinospiropyridopyrans, indolinospironaphthopyrans and indolinospironaphtho-1,4-oxazines: synthesis, study of X-ray crystal structure, antitumoral and antiviral evaluation</p>

<p>           Raic-Malic, S, Tomaskovic, L, Mrvos-Sermek, D, Prugovecki, B, Cetina, M, Grdisa, M, Pavelic, K, Mannschreck, A, Balzarini, J, De Clercq, E, and Mintas, M</p>

<p>           Bioorg Med Chem 2004. 12(5): 1037-45</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980617&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980617&amp;dopt=abstract</a> </p><br />

<p>  14.    5687   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           TFIIH Transcription Factor, a Target for the Rift Valley Hemorrhagic Fever Virus</p>

<p>           Le May, N, DubaeLe S, De Santis, LP, Billecocq, A, Bouloy, M, and Egly, JM</p>

<p>           Cell 2004. 116(4): 541-50</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980221&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14980221&amp;dopt=abstract</a> </p><br />

<p>  15.    5688   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Human herpesvirus 6 (HHV6) infection</p>

<p>           Abdel-Haq, NM and Asmar, BI</p>

<p>           Indian J Pediatr 2004. 71(1): 89-96</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14979393&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14979393&amp;dopt=abstract</a> </p><br />

<p>  16.    5689   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Selective inhibition of anthrax edema factor by adefovir, a drug for chronic hepatitis B virus infection</p>

<p>           Shen, Y, Zhukovskaya, NL, Zimmer, MI, Soelaiman, S, Bergson, P, Wang, CR, Gibbs, CS, and Tang, WJ</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978283&amp;dopt=abstract</a> </p><br />

<p>  17.    5690   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Alternatively spliced soluble coxsackie- adenovirus receptors inhibit coxsackievirus infection</p>

<p>           Dorner, A, Xiong, D, Couch, K, Yajima, T, and Knowlton, KU</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978041&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14978041&amp;dopt=abstract</a> </p><br />

<p>  18.    5691   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Severe acute respiratory syndrome (SARS) - an emerging infection of the 21st century</p>

<p>           Hsueh, PR and Yang, PC</p>

<p>           J Formos Med Assoc 2003. 102(12): 825-39</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14976561&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14976561&amp;dopt=abstract</a> </p><br />

<p>  19.    5692   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Synthesis of Aldehydo-Sugar Derivatives of Pyrazoloquinoline as Inhibitors of Herpes Simplex Virus Type 1 Replication</b></p>

<p>           Bekhit, AA, El-Sayed, OA, Aboul-Enein, HY, Siddiqui, YM, and Al-Ahdal, MN</p>

<p>           Journal of Enzyme Inhibition and Medicinal Chemistry 2004. 19(1): 33-38</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  20.    5693   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Preparation of bicyclo[4.2.1]nonane nucleoside analogs for the treatment of Flaviviridae infections</b> ((Pharmasset, Ltd. Barbados)</p>

<p>           Wang, Peiyuan, Stuyver, Lieven J, Watanabe, Kyoichi A, Hassan, Abdalla, Chun, Byoung-Known, and Hollecker, Laurent</p>

<p>           PATENT: WO 2004013300 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2003; PP: 147 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  21.    5694   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Efficacy and duration of immunity of a combined equine influenza and equine herpesvirus vaccine against challenge with an American-like equine influenza virus (A/equi-2/Kentucky/95)</p>

<p>           Heldens, JG, Pouwels, HG, and Van Loon, AA</p>

<p>           Vet J 2004. 167(2): 150-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14975389&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14975389&amp;dopt=abstract</a> </p><br />

<p>  22.    5695   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           2&#39;,3&#39;-dideoxynucleoside analogs for the treatment or prevention of flaviviridae infections</b>((Pharmasset Ltd., Barbados, Emory University, and Board of Trustees of the Leland Stanford Junior University))</p>

<p>           Shi, Junxing, Schinazi, Raymond F, and Striker, Robert</p>

<p>           PATENT: WO 2004013298 A2;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2003; PP: 86 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  23.    5696   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Pharmacokinetics and safety of viramidine, a prodrug of ribavirin, in healthy volunteers</p>

<p>           Lin, CC, Philips, L, Xu, C, and Yeh, LT</p>

<p>           J Clin Pharmacol 2004. 44(3): 265-75</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14973309&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14973309&amp;dopt=abstract</a> </p><br />

<p>  24.    5697   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Telbivudine. Anti-Hbv Agent</b></p>

<p>           Sorbera, LA, Castaner, J, Castaner, RM, and Bayes, M</p>

<p>           Drugs of the Future 2003. 28(9): 870-879</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  25.    5698   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Efficacy of oral active ether lipid analogs of cidofovir in a lethal mousepox model</p>

<p>           Buller, RM, Owens, G, Schriewer, J, Melman, L, BeadLe JR, and Hostetler, KY</p>

<p>           Virology 2004. 318(2): 474-81</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14972516&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14972516&amp;dopt=abstract</a> </p><br />

<p>  26.    5699   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Preparation of cyanothiazolylpyrrolidinecarboxylates and related compounds as antivirals</b>((Glaxo Group Limited, UK)</p>

<p>           Burton, George, Goodland, Helen Susanne, Haigh, David, Kiesow, Terence John, Ku, Thomas W, and Slater, Martin John</p>

<p>           PATENT: WO 2004009543 A2;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 27 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  27.    5700   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Sabadinine: A Potential Non-Peptide Anti-Severe Acute-Respiratory-Syndrome Agent Identified Using Structure-Aided Design</p>

<p>           Toney, JH, Navas-Martin, S, Weiss, SR, and Koeller, A</p>

<p>           J Med Chem 2004. 47(5): 1079-1080</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971887&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14971887&amp;dopt=abstract</a> </p><br />

<p>  28.    5701   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Mechanism of antiviral activities of 3&#39;-substituted L-nucleosides against 3TC-resistant HBV polymerase: a molecular modelling approach</p>

<p>           Chong, Y, Stuyver, L, Otto, MJ, Schinazi, RF, and Chu, CK</p>

<p>           Antivir Chem Chemother 2003. 14(6): 309-19</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968937&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968937&amp;dopt=abstract</a> </p><br />

<p>  29.    5702   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Development of new antivirals for herpesviruses</p>

<p>           Eizuru, Y</p>

<p>           Antivir Chem Chemother 2003. 14(6): 299-308</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968936&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14968936&amp;dopt=abstract</a> </p><br />

<p>  30.    5703   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Prospects for Antiviral Ribozymes and Deoxyribozymes</b></p>

<p>           Peracchi, A</p>

<p>           Reviews in Medical Virology 2004.  14(1): 47-64</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  31.    5704   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Dramatic Effects of 2-Bromo-5,6-Dichloro-1-Beta-D-Ribofuranosyl Benzimidazole Riboside on the Genome Structure, Packaging, and Egress of Guinea Pig Cytomegalovirus</b></p>

<p>           Nixon, DE and Mcvoy, MA</p>

<p>           Journal of Virology 2004. 78(4): 1623-1635</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  32.    5705   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Method for screening EBNA-1 inhibitors, and use in the treatment of latent Epstein-Barr virus infections</b> ((Fujisawa Pharmaceutical Co., Ltd. Japan)</p>

<p>           Yanagi, Kazuo and Itou, Sayuri</p>

<p>           PATENT: WO 2004008152 A1;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 34 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  33.    5706   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Preparation of 2-amino-9-(2-hydroxymethylcyclopropylidenemethyl)-purines as antiviral agents</b>((Wayne State University, USA and The Regents of the University of Michigan))</p>

<p>           Zemlicka, Jiri and Drach, John C</p>

<p>           PATENT: WO 2004006867 A2;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003; PP: 36 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  34.    5707   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Role of indoleamine-2,3-dioxygenase in alpha/beta and gamma interferon-mediated antiviral effects against herpes simplex virus infections</p>

<p>           Adams, O, Besken, K, Oberdorfer, C, MacKenzie, CR, Takikawa, O, and Daubener, W</p>

<p>           J Virol 2004. 78(5): 2632-2636</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963171&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963171&amp;dopt=abstract</a> </p><br />

<p>  35.    5708   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Pyrimidones with antiviral properties</b> ((Axxima Pharmaceuticals Aktiengesellschaft, Germany and 4SC AG))</p>

<p>           Missio, Andrea, Herget, Thomas, Aschenbrenner, Andrea, Kramer, Bernd, Leban, Johann, and Wolf, Kristina</p>

<p>           PATENT: EP 1389461 A1;  ISSUE DATE: 20040218</p>

<p>           APPLICATION: 2002-18357; PP: 46 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  36.    5709   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           The matrix protein of Marburg virus is transported to the plasma membrane along cellular membranes: exploiting the retrograde late endosomal pathway</p>

<p>           Kolesnikova, L, Bamberg, S, Berghofer, B, and Becker, S</p>

<p>           J Virol 2004. 78(5): 2382-93</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963134&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14963134&amp;dopt=abstract</a> </p><br />

<p>  37.    5710   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Hydroxymethyl-Glutaryl Coenzyme a Reductase Inhibition Limits Cytomegalovirus Infection in Human Endothelial Cells</b></p>

<p>           Potena, L, Frascaroli, G, Grigioni, F, Lazzarotto, T, Magnani, G, Tomasi, L, Coccolo, F, Gabrielli, L, Magelli, C, Landini, MP, and Branzi, A</p>

<p>           Circulation 2004. 109(4): 532-536</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  38.    5711   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           The nsp9 Replicase Protein of SARS-Coronavirus, Structure and Functional Insights</p>

<p>           Sutton, G, Fry, E, Carter, L, Sainsbury, S, Walter, T, Nettleship, J, Berrow, N, Owens, R, Gilbert, R, Davidson, A, Siddell, S, Poon, LL, Diprose, J, Alderton, D, Walsh, M, Grimes, JM, and Stuart, DI</p>

<p>           Structure (Camb) 2004. 12(2): 341-53</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14962394&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14962394&amp;dopt=abstract</a> </p><br />

<p>  39.    5712   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           West Nile Virus inactivation by the solvent/detergent steps of the second and third generation manufacturing processes for B-domain deleted recombinant factor VIII</p>

<p>           Jakubik, JJ, Vicik, SM, Tannatt, MM, and Kelley, BD</p>

<p>           Haemophilia 2004. 10(1): 69-74</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14962223&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14962223&amp;dopt=abstract</a> </p><br />

<p>  40.    5713   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Stacking up the armory against viruses</p>

<p>           Klebl, BM</p>

<p>           Drug Discov Today 2004. 9(4): 162-4</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14960395&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14960395&amp;dopt=abstract</a> </p><br />

<p>  41.    5714   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Albumin Nanoparticles Improved the Stability, Nuclear Accumulation and Anticytomegaloviral Activity of a Phosphodiester Oligonucleotide</b></p>

<p>           Arnedo, A, Irache, JM, Merodio, M, and Millan, MSE</p>

<p>           Journal of Controlled Release 2004.  94(1): 217-227</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  42.    5715   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Severe acute respiratory syndrome: developing a research response</p>

<p>           La Montagne, JR, Simonsen, L, Taylor, RJ, and Turnbull, J</p>

<p>           J Infect Dis 2004. 189(4): 634-41</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767816&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14767816&amp;dopt=abstract</a> </p><br />

<p>  43.    5716   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p class="memofmt1-2">           Genetic Resistance to Flaviviruses</p>

<p>           Brinton, MA and Perelygin, AA</p>

<p>           Advances in Virus Research. 60: 43-85</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  44.    5717   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Emtricitabine: a New Nucleoside Analogue for Once-Daily Antiretroviral Therapy</b></p>

<p>           Cahn, P</p>

<p>           Expert Opinion on Investigational Drugs 2004. 13(1): 55-68</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  45.    5718   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           In vitro resistance studies of hepatitis C virus serine protease inhibitors, VX-950 and BILN 2061: Structural analysis indicates different resistance mechanisms</p>

<p>           Lin, C, Lin, K, Luong, YP, Rao, BG, Wei, YY, Brennan, DL, Fulghum, JR, Hsiao, HM, Ma, S, Maxwell, JP, Cottrell, KM, Perni, RB, Gates, CA, and Kwong, AD</p>

<p>           J Biol Chem 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766754&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14766754&amp;dopt=abstract</a> </p><br />

<p>  46.    5719   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Genotyping anti-hepatitis B virus drug resistance</p>

<p>           Bourne, EJ, Gauthier, J, Lopez, VA, and Condreay, LD</p>

<p>           Methods Mol Med 2004. 96: 379-386</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762283&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762283&amp;dopt=abstract</a> </p><br />

<p>  47.    5720   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Ebola vaccines tested in humans, monkeys</p>

<p>           Vastag, B</p>

<p>           JAMA 2004. 291(5): 549-50</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762022&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14762022&amp;dopt=abstract</a> </p><br />

<p>  48.    5721   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           The potentiating effect of ribavirin on interferon in the treatment of hepatitis C: lack of evidence for ribavirin-induced viral mutagenesis</p>

<p>           Schinkel, J, De Jong, MD, Bruning, B, Van Hoek, B, Spaan, WJ, and Kroes, AC</p>

<p>           Antivir Ther 2003. 8(6): 535-40</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14760887&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14760887&amp;dopt=abstract</a> </p><br />

<p>  49.    5722   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Inhibition of glycogen breakdown by imino sugars in vitro and in vivo</p>

<p>           Andersson, U, Reinkensmeier, G, Butters, TD, Dwek, RA, and Platt, FM</p>

<p>           Biochem Pharmacol 2004. 67(4): 697-705</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14757169&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14757169&amp;dopt=abstract</a> </p><br />

<p>  50.    5723   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Human Cytomegalovirus Elicits a Coordinated Cellular Antiviral Response Via Envelope Glycoprotein B</b></p>

<p>           Boehme, KW, Singh, J, Perry, ST, and Compton, T</p>

<p>           Journal of Virology 2004. 78(3): 1202-1211</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  51.    5724   DMID-LS-61; PUBMED-DMID-3/2/2004</p>

<p class="memofmt1-2">           Smallpox: clinical features, prevention, and management</p>

<p>           Guharoy, R, Panzik, R, Noviasky, JA, Krenzelok, EP, and Blair, DC</p>

<p>           Ann Pharmacother 2004. 38(3): 440-7</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14755066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=14755066&amp;dopt=abstract</a> </p><br />

<p>  52.    5725   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Small interfering RNAs with backbone or base modifications directed against hepatitis C virus and their use in treatment of infection</b> ((Chiron Corporation, USA)</p>

<p>           Han, Jang, Seo, Mi Young, and Houghton, Michael</p>

<p>           PATENT: WO 2004011647 A1;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 74 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  53.    5726   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Two New Bicoumarins From Clausena Excavata</b></p>

<p>           Takemura, Y, Kanao, K, Konoshima, A, Ju-Ichi, M, Ito, C, Furukawa, H, Tokuda, H, and Nishino, H</p>

<p>           Heterocycles 2004. 63(1): 115-+</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  54.    5727   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p class="memofmt1-2">           The DNA-binding domain of human papillomavirus type 18 E1: Crystal structure, dimerization, and DNA binding</p>

<p>           Auster, Anitra S and Joshua-Tor, Leemor</p>

<p>           Journal of Biological Chemistry 2004. 279(5): 3733-3742</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  55.    5728   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Rapid-acting broad spectrum protection against biological threat agents</b>((USA))</p>

<p>           Alibek, Ken, Bailey, Charles, Carron, Edith Grene, Popov, Serguei G, Wu, Aiguo G, Popova, Taissia, Klotz, Francis W, Hayford, Alice, Karginov, Vladimir, Zhai, Qingzhu, and Liu, Ge</p>

<p>           PATENT: US 2004018193 A1;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003-6846; PP: 58 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  56.    5729   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Nucleotides, preparation thereof and use as antiviral agents and inhibitors of RNA viral polymerases</b> ((Biocryst Pharmaceuticals, Inc. USA)</p>

<p>           Babu, Yarlagadda S, Chand, Pooran, El-Kattan, Yahya, and Wu, Minwan</p>

<p>           PATENT: US 2004014722 A1;  ISSUE DATE: 20040122</p>

<p>           APPLICATION: 2003-43963; PP: 22 pp., Cont.-in-part of Appl. No. PCT/US02-36621.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  57.    5730   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Phosphonate nucleotide and thiadiazole compounds for the treatment of smallpox</b>((USA))</p>

<p>           Colacino, Joseph Matthew</p>

<p>           PATENT: US 2004023928 A1;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2002-11665; PP: 3 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  58.    5731   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Systemic administration of NAC as an adjunct in the treatment of bioterror exposures such as anthrax, smallpox or radiation and for vaccination prophylaxis, and use in combination with DHEA for the treatment of smallpox and other viruses</b>((USA))</p>

<p>           Guilford, FTimothy and Schumm, Brooke</p>

<p>           PATENT: US 2004022873 A1;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2002-27790; PP: 15 pp., Cont.-in-part of U.S. Provisional Ser. No. 371,590.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  59.    5732   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Established and Potential Strategies Against Papillomavirus Infections</b></p>

<p>           Bernard, HU</p>

<p>           Journal of Antimicrobial Chemotherapy 2004. 53(2): 137-139</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  60.    5733   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Polio Control After Certification: Major Issues Outstanding</b></p>

<p>           Fine, PEM, Oblapenko, G, and Sutter, RW</p>

<p>           Bulletin of the World Health Organization 2004. 82(1): 47-52</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  61.    5734   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Combination therapy with 1,3-dioxolanes and inosine monophosphate dehydrogenase inhibitors</b>((Triangle Pharmaceuticals, Inc. USA )</p>

<p>           Furman, Phillip A and Borroto-Esoda, Katyna</p>

<p>           PATENT: WO 2004009595 A1;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 91 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  62.    5735   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Will Containment of Wild Poliovirus in Laboratories and, Inactivated Poliovirus Vaccine Production Sites Be Effective for Global Certification?</b></p>

<p>           DowdLe WR, Wolff, C, Sanders, R, Lambert, S, and Best, M</p>

<p>           Bulletin of the World Health Organization 2004. 82(1): 59-62</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  63.    5736   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Pharmaceutical compositions comprising hepatitis C viral protease inhibitors</b>((Boehringer Ingelheim Pharmaceuticals, Inc. USA)</p>

<p>           Chen, Shirlynn and Mei, Xiaohui</p>

<p>           PATENT: WO 2004009121 A1;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 66 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  64.    5737   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Thionucleoside derivatives as inhibitors of RNA-dependent RNA viral polymerase</b>((Merck &amp; Co., Inc. USA and Isis Pharmaceuticals, Inc.)</p>

<p>           Olsen, David B, Bhat, Balkrishen, and Cook, Phillip Dan</p>

<p>           PATENT: WO 2004009020 A2;  ISSUE DATE: 20040129</p>

<p>           APPLICATION: 2003; PP: 48 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  65.    5738   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Chimeric GB virus B (GBV-B) containing partial HCV 5&#39;-NTR as persistent infectious surrogate viruses or a model for HCV vaccine development and drug screening</b>((Board of Regents, the University of Texas System USA and Institut Pasteur))</p>

<p>           Martin, Annette, Sangar, David V, Lemon, Stanley M, and Rijnbrand, Rene</p>

<p>           PATENT: WO 2004005498 A1;  ISSUE DATE: 20040115</p>

<p>           APPLICATION: 2003; PP: 108 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  66.    5739   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Preparation of N-alkenyl carboxamide inhibitors of HCV NS5b polymerase for treating hepatitis C viral infections and associated diseases</b>((Pharmacia &amp; Upjohn Company, USA and Finzel, Barry C.)</p>

<p>           Gao, Hua, Greene, Meredith L, Gross, Rebecca J, Nugent, Richard A, and Pfefferkorn, Jeffrey</p>

<p>           PATENT: WO 2004002977 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 186 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  67.    5740   DMID-LS-61; WOS-DMID-3/2/2004</p>

<p>           <b>Oral Treatment of Cowpox and Vaccinia Virus Infections in Mice With Ether Lipid Esters of Cidofovir</b></p>

<p>           Quenelle DC,  Collins, DJ, Wan, WB, BeadLe JR, Hostetler, KY, and Kern, ER</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(2): 404-412</p>

<p>&nbsp;           <!--HYPERLNK:--></p>

<p>  68.    5741   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Preparation of N-(phenylaminophenyl)carboxamide and related compound inhibitors of HCV NS5b polymerase for treating hepatitis C viral infections and associated diseases</b>((Pharmacia &amp; Upjohn Company, USA)</p>

<p>           Finzel, Barry C, Funk, Lee A, Kelly, Robert C, Reding, Matthew T, and Wicnienski, Nancy Anne</p>

<p>           PATENT: WO 2004002944 A1;  ISSUE DATE: 20040108</p>

<p>           APPLICATION: 2003; PP: 45 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  69.    5742   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p class="memofmt1-2">           The hepatitis C virus replicase: Insights into RNA-dependent RNA replication and prospects for rational drug design</p>

<p>           Frick, David N</p>

<p>           Current Organic Chemistry 2004. 8(3): 223-241</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  70.    5743   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Method for inducing complete hepatitis C virus (HCV) replication in vitro</b>((McGill University, Can.)</p>

<p>           Sonenberg, Nahum and Lopez-Lastra, Marcelo</p>

<p>           PATENT: WO 2004013318 A1;  ISSUE DATE: 20040212</p>

<p>           APPLICATION: 2003; PP: 124 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  71.    5744   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p class="memofmt1-2">           Inhibition of the subgenomic hepatitis C virus replicon in Huh-7 cells by 2&#39;-deoxy-2&#39;-fluorocytidine</p>

<p>           Stuyver, Lieven J, McBrayer, Tamara R, Whitaker, Tony, Tharnish, Phillip M, Ramesh, Mangala, Lostia, Stefania, Cartee, Leanne, Shi, Junxing, Hobbs, Ann, Schinazi, Raymond F, Watanabe, Kyoichi A, and Otto, Michael J</p>

<p>           Antimicrobial Agents and Chemotherapy 2004. 48(2): 651-654</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>

<p>  72.    5745   DMID-LS-61; SCIFINDER-DMID-3/2/2004</p>

<p><b>           Anti-viral 7-deaza D-nucleosides for HBV infection therapy</b>((Micrologix Biotech Inc., Can.)</p>

<p>           Mekouar, Khalid and Deziel, Robert</p>

<p>           PATENT: WO 2004011478 A2;  ISSUE DATE: 20040205</p>

<p>           APPLICATION: 2003; PP: 36 pp.</p>

<p>&nbsp;           <!--HYPERLNK:--> </p>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
