

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-08-29.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Q99JdyYsMlZLbAs64ynLFYR2DlktzNFZF/OxbfNtzTUiHfelb8rSZ8EkZQPSGWU7Y+9NzbrSk8J6QmKnHvZUlUePlAsMvehx0t8IaACW64e7R2yMbhMONYIwAxw=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6A12EDEF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: August 16 - August 29, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23924243">Spiro Fused Diterpene-indole Alkaloids from a Creek-bottom-derived Aspergillus terreus.</a> Cai, S., L. Du, A.L. Gerea, J.B. King, J. You, and R.H. Cichewicz. Organic Letters, 2013. 15(16): p. 4186-4189. PMID[23924243].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23791641">Amphotericin B-copper(II) Complex as a Potential Agent with Higher Antifungal Activity against Candida albicans.</a> Chudzik, B., I.B. Tracz, G. Czernel, M.J. Fiolka, G. Borsuk, and M. Gagos. European Journal of Pharmaceutical Sciences, 2013. 49(5): p. 850-857. PMID[23791641].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23945270">Cytotoxicity of Brazilian Plant Extracts against Oral Microorganisms of Interest to Dentistry.</a> De Oliveira, J.R., V.C. de Castro, P. das Gracas Figueiredo Vilela, S.E. Camargo, C.A. Carvalho, A.O. Jorge, and L.D. de Oliveira. BMC Complementary and Alternative Medicine, 2013. 13: p. 208. PMID[23945270].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23838261">Evaluation of (4-Aminobutyloxy)quinolines as a Novel Class of Antifungal Agents.</a> Vandekerckhove, S., H.G. Tran, T. Desmet, and M. D&#39;Hooghe. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4641-4643. PMID[23838261].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23977248">Comparing Adjuvanted H28 and Modified Vaccinia Virus Ankara Expressing H28 in a Mouse and a Non-human Primate Tuberculosis Model.</a> Billeskov, R., J.P. Christensen, C. Aagaard, P. Andersen, and J. Dietrich. Plos One, 2013. 8(8): p. e72185. PMID[23977248].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23968826">Naphthoquinones Isolated from Diospyros anisandra Exhibit Potent Activity against Pan-resistant First-line Drugs Mycobacterium tuberculosis Strains.</a> Uc-Cachon, A.H., R. Borges-Argaez, S. Said-Fernandez, J. Vargas-Villarreal, F. Gonzalez-Salazar, M. Mendez-Gonzalez, M. Caceres-Farfan, and G.M. Molina-Salinas. Pulmonary Pharmacology &amp; Therapeutics, 2013. <b>[Epub ahead of print]</b>. PMID[23968826].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23835293">Antimalarial and Cytotoxic Activities of Chiral Triamines</a>. Dellai, A., J. Appel, A. Bouraoui, S. Croft, and A. Nefzi. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4579-4582. PMID[23835293].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br />  

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23974147">Small Molecule Plasmodium FKBP35 Inhibitor as a Potential Antimalaria Agent.</a> Harikishore, A., M. Niang, S. Rajan, P.R. Preiser, and H.S. Yoon. Scientific Reports, 2013. 3: p. 2501. PMID[23974147].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23979751">Transdermal Glyceryl Trinitrate as an Effective Adjunctive Treatment with Artemether for Late Stage Experimental Cerebral Malaria.</a> Orjuela-Sanchez, P., P.K. Ong, G.M. Zanini, B. Melchior, Y.C. Martins, D. Meays, J.A. Frangos, and L.J. Carvalho. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b><b>.</b> PMID[23979751].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23949312">Fusidic acid Is an Effective Treatment against Toxoplasma gondii and Listeria Monocytogenes in Vitro, but Not in Mice.</a> Payne, A.J., L.M. Neal, and L.J. Knoll. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23949312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23850202">In Vivo Antimalarial Activity of Novel 2-Hydroxy-3-anilino-1,4-naphthoquinones Obtained by Epoxide Ring-opening Reaction.</a> Rezende, L.C., F. Fumagalli, M.S. Bortolin, M.G. Oliveira, M.H. Paula, V.F. Andrade-Neto, and S. Emery Fda. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4583-4586. PMID[23850202].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p>

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23979747">In Vitro Effects of Novel Ruthenium Complexes in Neospora caninum and Toxoplasma gondii Tachyzoites.</a> Barna, F., K. Debache, C.A. Vock, T. Kuster, and A. Hemphill. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23979747].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23973195">Effect of Nitaxozanide and Pyrimethamine on Astrocytes Infected by Toxoplasma gondii in Vitro.</a> Galvan-Ramirez, M.D., J. Duenas-Jimenez, L. Rodriguez-Perez, R. Troyo-Sanroman, M. Ramirez-Herrera, and T. Garcia-Iglesias. Archives of Medical Research, 2013. <b>[Epub ahead of print]</b>. PMID[23973195].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23949312">Fusidic Acid Is an Effective Treatment against Toxoplasma gondii and Listeria monocytogenes in Vitro, but Not in Mice.</a> Payne, A.J., L.M. Neal, and L.J. Knoll. Parasitology Research, 2013. <b>[Epub ahead of print]</b>. PMID[23949312].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0816-082913.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">15. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320673200003">Synthesis of Some New 3,5-Diamino-4-(4 &#39;-fluorophenylazo)-1-aryl/heteroarylpyrazoles as Antimicrobial Agents.</a> Aggarwal, R., V. Kumar, G.K. Gupta, and V. Kumar. Medicinal Chemistry Research, 2013. 22(8): p. 3566-3573. ISI[000320673200003].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br />     

    <p class="plaintext">16. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321994100009">Differences in Composition of Honey Samples and Their Impact on the Antimicrobial Activities against Drug Multiresistant Bacteria and Pathogenic Fungi.</a> Al-Waili, N., A. Al Ghamdi, M.J. Ansari, Y. Al-Attal, A. Al-Mubarak, and K. Salom. Archives of Medical Research, 2013. 44(4): p. 307-316. ISI[000321994100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">17. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322181300048">Synthesis, Characterization, and in Vitro anti-Mycobacterium tuberculosis Activity of Terpene Schiff Bases.</a> Bhat, M.A. and M.A. Al-Omar. Medicinal Chemistry Research, 2013. 22(9): p. 4522-4528. ISI[000322181300048].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">18. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322181300042">Antimicrobial Activity of Schiff Bases of Coumarin-Incorporated 1,3,4-Oxadiazole Derivatives: An in Vitro Evaluation.</a> Bhat, M.A., M.A. Al-Omar, and N. Siddiqui. Medicinal Chemistry Research, 2013. 22(9): p. 4455-4458. ISI[000322181300042].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">19. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320838500038">Isolation of (-)-Olivil-9 &#39;-O-beta-D-glucopyranoside from Sambucus williamsii and Its Antifungal 5ffects with Membrane-disruptive Action.</a> Choi, H., J. Lee, Y.S. Chang, E.R. Woo, and D.G. Lee. Biochimica Et Biophysica Acta-Biomembranes, 2013. 1828(8): p. 2002-2006. ISI[000320838500038].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">20. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320673200012">Synthesis of New Quinoline-2-pyrazoline-based Thiazolinone Derivatives as Potential Antimicrobial Agents.</a> Desai, N.C., V.V. Joshi, and K.M. Rajpara. Medicinal Chemistry Research, 2013. 22(8): p. 3663-3674. ISI[000320673200012].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">21. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800046">Multicenter Study of Isavuconazole MIC Distributions and Epidemiological Cutoff Values for Aspergillus spp. for the CLSI M38-A2 Broth Microdilution Method.</a> Espinel-Ingroff, A., A. Chowdhary, G.M. Gonzalez, C. Lass-Florl, E. Martin-Mazuelos, J. Meis, T. Pelaez, M.A. Pfaller, and J. Turnidge. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3823-3828. ISI[000321761800046].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">22. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321025000010">Antimicrobial Polymer Films Functionalized with Cyclodextrins.</a> Grigoriu, A.M., C. Luca, E. Horoba, and S. Dunca. Revista de Chimie, 2013. 64(6): p. 606-611. ISI[000321025000010].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">23. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320673200032">Synthesis, Antitubercular Activity, and QSAR Analysis of Substituted Nitroaryl Analogs: Chalcone, Pyrazole, Isoxazole, and Pyrimidines.</a> Gupta, R.A. and S.G. Kaskhedikar. Medicinal Chemistry Research, 2013. 22(8): p. 3863-3880. ISI[000320673200032].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">24. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800026">Identification and Mechanism of Action of the Plant Defensin NaD1 as a New Member of the Antifungal Drug Arsenal against Candida albicans.</a> Hayes, B.M.E., M.R. Bleackley, J.L. Wiltshire, M.A. Anderson, A. Traven, and N.L. van der Weerden. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3667-3675. ISI[000321761800026].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">25. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800054">Cell Wall Perturbation Sensitizes Fungi to the Antimalarial Drug Chloroquine.</a> Islahudin, F., C. Khozoie, S. Bates, K.N. Ting, R.J. Pleass, and S.V. Avery. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3889-3896. ISI[000321761800054].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">26. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322181300041">Synthesis, Characterization, and SAR Studies of New (1H-Indol-3-yl)alkyl-3-(1H-indol-3-yl)propanamide Derivatives as Possible Antimicrobial and Antitubercular Agents.</a> Karuvalam, R.P., R. Pakkath, K.R. Haridas, R. Rishikesan, and N.S. Kumari. Medicinal Chemistry Research, 2013. 22(9): p. 4437-4454. ISI[000322181300041].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">27. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321237100016">Development of Flavonoid-based Inverse Agonists of the Key Signaling Receptor US28 of Human Cytomegalovirus.</a> Kralj, A., M.T. Nguyen, N. Tschammer, N. Ocampo, Q. Gesiotto, M.R. Heinrich, and O. Phanstiel. Journal of Medicinal Chemistry, 2013. 56(12): p. 5019-5032. ISI[000321237100016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">28. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322181300020">Synthesis and Pharmacological Evaluation of Some Novel 4-Isopropyl thiazole-based sulfonyl Derivatives as Potent Antimicrobial and Antitubercular Agents.</a> Kumar, G.V.S., Y.R. Prasad, and S.M. Chandrashekar. Medicinal Chemistry Research, 2013. 22(9): p. 4239-4252. ISI[000322181300020].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">29. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322181300043">Synthesis and Antimicrobial Activity of Novel bis-Azaphenothiazines.</a> Kushwaha, K., R. Sakhuja, and S.C. Jain. Medicinal Chemistry Research, 2013. 22(9): p. 4459-4467. ISI[000322181300043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">30. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800016">Potentiation of Azole Antifungals by 2-Adamantanamine.</a> LaFleur, M.D., L.M. Sun, I. Lister, J. Keating, A. Nantel, L.S. Long, M. Ghannoum, J. North, R.E. Lee, K. Coleman, T. Dahl, and K. Lewis. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3585-3592. ISI[000321761800016].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">31. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321522900005">Free Radicals Scavenging Activity, Cytotoxicity and Anti-parasitic Activity of Essential Oil of Psidium guajava L. Leaves against Toxoplasma gondii.</a> Lee, W.C., R. Mahmud, R. Noordin, S.P. Piaru, S. Perumal, and S. Ismail. Journal of Essential Oil Bearing Plants, 2013. 16(1): p. 32-38. ISI[000321522900005].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">32. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321637700014">Enhanced Efficacy of Synergistic Combinations of Antimicrobial Peptides with Caspofungin Versus Candida albicans in Insect and Murine Models of Systemic Infection.</a> MacCallum, D.M., A.P. Desbois, and P.J. Coote. European Journal of Clinical Microbiology &amp; Infectious Diseases, 2013. 32(8): p. 1055-1062. ISI[000321637700014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">33. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322466500001">Validation of Antimycobacterial Plants Used by Traditional Healers in Three Districts of the Limpopo Province (South Africa).</a> Masoko, P. and K.M. Nxumalo. Evidence-Based Complementary and Alternative Medicine, 2013. <b>[Epub ahead of print]</b>. ISI[000322466500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">34. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321522900014">Identification of Volatile Oil Components of Nepeta binaludensis Jamzad by GC-MS and C-13-NMR Methods and Evaluation of Its Antimicrobial Activity.</a> Mohammadpour, N., S.A. Emami, and J. Asili. Journal of Essential Oil Bearing Plants, 2013. 16(1): p. 102-107. ISI[000321522900014].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br />    

    <p class="plaintext">35. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800029">Cationic Antimicrobial Peptides and Biogenic Silver Nanoparticles Kill Mycobacteria without Eliciting DNA Damage and Cytotoxicity in Mouse Macrophages.</a> Mohanty, S., P. Jena, R. Mehta, R. Pati, B. Banerjee, S. Patil, and A. Sonawane. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3688-3698. ISI[000321761800029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">36. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321922600028">Synthesis and Biological Evaluation of Cationic Fullerene quinazolinone Conjugates and Their Binding Mode with Modeled Mycobacterium tuberculosis Hypoxanthine-Guanine Phosphoribosyltransferase Enzyme.</a> Patel, M.B., S.P. Kumar, N.N. Valand, Y.T. Jasrai, and S.K. Menon. Journal of Molecular Modeling, 2013. 19(8): p. 3201-3217. ISI[000321922600028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800043">Two Human Host Defense Ribonucleases against Mycobacteria, the Eosinophil Cationic Protein (RNase 3) and RNase 7.</a> Pulido, D., M. Torrent, D. Andreu, M.V. Nogues, and E. Boix. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3797-3805. ISI[000321761800043].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000320673200029">Microwave-assisted Synthesis of Novel 4h-Chromene Derivatives Bearing 2-Aryloxyquinoline and Their Antimicrobial Activity Assessment.</a> Sangani, C.B., N.M. Shah, M.P. Patel, and R.G. Patel. Medicinal Chemistry Research, 2013. 22(8): p. 3831-3842. ISI[000320673200029].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321761800028">High-throughput Screening of a Collection of Known Pharmacologically Active Small Compounds for Identification of Candida albicans Biofilm Inhibitors.</a> Siles, S.A., A. Srinivasan, C.G. Pierce, J.L. Lopez-Ribot, and A.K. Ramasubramanian. Antimicrobial Agents and Chemotherapy, 2013. 57(8): p. 3681-3687. ISI[000321761800028].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000321883100009">Anti-aspergillus Activity of Green Coffee 5-O-Caffeoyl quinic acid and Its Alkyl Esters.</a> Suarez-Quiroz, M.L., A.A. Campos, G.V. Alfaro, O. Gonzalez-Rios, P. Villeneuve, and M.C. Figueroa-Espinoza. Microbial Pathogenesis, 2013. 61-62: p. 51-56. ISI[000321883100009].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000322178900004">Synthesis and Antimicrobial Activity of Amines with Azaxanthene Fragments.</a> Yunnikova, L.P., V.Y. Gorokhov, T.V. Makhova, and G.A. Aleksandrova. Pharmaceutical Chemistry Journal, 2013. 47(3): p. 139-141. ISI[000322178900004].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0816-082913.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
