

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2013-08-15.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="B4/hvEfvOjZyJYCw2v0BK+afayQM0arXOFvrGqUyANROEsbp3O07h4OhOAJYjt/B7aBSu02xGSKZtQYlwZVJxtMddiMjax4h7VuLK03e+SmRQNACIVSziuN+aBs=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="4CB410DD" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">HIV Citation List: August 2 - August 15, 2013</h1>

    <h2>PubMed citations</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23921229">Design, Synthesis and Evaluation of a Potent Substrate-analog Peptide-inhibitor Identified by Scanning Ala/Phe Chemical Mutagenesis, Mimicking Substrate Co-evolution, against Multidrug-resistant HIV-1 Protease.</a> Yedidi, R.S., J.M. Muhuhi, Z. Liu, K.Z. Bencze, K.M. Koupparis, C.E. O&#39;Connor, I.A. Kovari, M.R. Spaller, and L.C. Kovari. Biochemical and Biophysical Research Communications, 2013. <b>[Epub ahead of print]</b>. PMID[23921229].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23922365">Targeting of the Purine Biosynthesis Host Cell Pathway Enhances the Activity of Tenofovir against Sensitive and Drug-resistant HIV-1.</a> Heredia, A., C.E. Davis, M.S. Reitz, N.M. Le, M.A. Wainberg, J.S. Foulke, L.X. Wang, and R.R. Redfield. Journal of Infectious Disease, 2013. <b>[Epub ahead of print]</b>. PMID[23922365].</p>

    <p class="plaintext"><b>[PubMed]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23849880">Cyclophilin Inhibitors as Antiviral Agents.</a> Peel, M. and A. Scribner. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4485-4492. PMID[23849880].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed?term=23845222">Structure-Activity Relationship Study of Phenylpyrazole Derivatives as a Novel Class of anti-HIV Agents.</a> Mizuhara, T., T. Kato, A. Hirai, H. Kurihara, Y. Shimada, M. Taniguchi, H. Maeta, H. Togami, K. Shimura, M. Matsuoka, S. Okazaki, T. Takeuchi, H. Ohno, S. Oishi, and N. Fujii. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(16): p. 4557-4561. PMID[23845222].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <h2>ISI Web of Knowledge Citations</h2>

    <p class="plaintext">5. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000321025800024">A Synthetic Bivalent Ligand of CXCR4 Inhibits HIV Infection.</a> Xu, Y., S. Duggineni, S. Espitia, D.D. Richman, J. An, and Z.W. Huang. Biochemical and Biophysical Research Communications, 2013. 435(4): p. 646-650. ISI[000321025800024].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">6. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320576300023">Stereoselective Synthesis of the Key Intermediates of the HIV Protease Inhibitor Fosamprenavir and Its Diastereomer.</a> Panov, I., P. Drabina, J. Hanusek, and M. Sedlak. Synlett, 2013(10): p. 1280-1282. ISI[000320576300023].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">7. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000322051500662">XLHX-124-50 as a Potent HIV-1 Entry Inhibitor Targeting on HIV-1 gp41 Envelope.</a> Li, L., J.Y. Qiu, J. Yang, J.Q. Chen, X.X. Zhang, and S.W. Liu. Acta Pharmacologica Sinica, 2013. 34: p. 151-152. ISI[000322051500662].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">8. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320654200131">Interaction of Antiretroviral Medications with Finasteride.</a> Ward, D. Journal of the International AIDS Society, 2012. 15: p. 63-63. ISI[000320654200131].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">9. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320149400202">Potent Inhibition of HIV Replication by RNA-binding Peptide Mimetics with Unprecedented Specificity.</a> Varani, G. Journal of Biomolecular Structure &amp; Dynamics, 2013. 31: p. 131-131. ISI[000320149400202].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">10. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320654200251">The Discovery of S/GSK1265744: A Carbamoyl Pyridone HIV-1 Integrase Inhibitor.</a> Taoda, Y., B. Johns, J. Weatherhead, D. Temelkoff, T. Kawasuji, H. Yoshida, T. Taishi, R. Kiyama, M. Fuji, H. Murai, T. Yoshinaga, A. Sato, and T. Fujiwara. Journal of the International AIDS Society, 2012. 15: p. 122-122. ISI[000320654200251].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">11. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320702900055">Identification of Potent and Orally Bioavailable Nucleotide Competing Reverse Transcriptase Inhibitors: In Vitro and in Vivo Optimization of a Series of Benzofurano[3,2-d]pyrimidin-2-one Derived Inhibitors.</a> Sturino, C.F., Y. Bousquet, C.A. James, P. DeRoy, M. Duplessis, P.J. Edwards, T. Halmos, J. Minville, L. Morency, S. Morin, B. Thavonekham, M. Tremblay, J.M. Duan, M. Ribadeneira, M. Garneau, A. Pelletier, S. Tremblay, L. Lamorte, R. Bethell, M.G. Cordingley, D. Rajotte, and B. Simoneau. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(13): p. 3967-3975. ISI[000320702900055].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">12. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320654200324">Etravirine in Protease Inhibitor-free Antiretroviral Combination Therapies.</a> Schuelter, E., N. Luebke, B. Jensen, M. Zazzi, A. Sonnerborg, T. Lengauer, F. Incardona, R. Camacho, J. Schmit, B. Clotet, R. Kaiser, and A. Pironti. Journal of the International AIDS Society, 2012. 15: p. 155-156. ISI[000320654200324].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">13. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320020900003">Synthesis and Characterization of New Nickel(II) Chelates with S-Alkyl-salicylaldehyde thiosemicarbazones.</a> Sahin, M. and B. Ulkuseven. Phosphorus Sulfur and Silicon and the Related Elements, 2013. 188(5): p. 545-554. ISI[000320020900003].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">14. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320706800008">HIV-1 Associated Topoisomerase II Beta Kinase: A Potential Pharmacological Target for Viral Replication.</a> Ponraj, K., M. Prabhakar, R.S. Rathore, A. Bommakanti, and A.K. Kondapi. Current Pharmaceutical Design, 2013. 19(26): p. 4776-4786. ISI[000320706800008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">15. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320704600022">Notable Difference in anti-HIV Activity of Integrase Inhibitors as a Consequence of Geometric and Enantiomeric Configurations.</a> Okello, M., S. Mishra, M. Nishonov, and V. Nair. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(14): p. 4112-4116. ISI[000320704600022].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">16. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320770600016">Synthesis of Betulin Derivatives Containing Triazole Fragments.</a> Chi, W.F., L. Jin, F.Y. Piao, and R.B. Han. Chemistry of Natural Compounds, 2013. 49(2): p. 264-267. ISI[000320770600016].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">17. <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000320928400008">Discovery of Piperidine-linked Pyridine Analogues as Potent Non-nucleoside HIV-1 Reverse Transcriptase Inhibitors.</a> Chen, X.W., Y.Y. Li, S.F. Ding, J. Balzarini, C. Pannecouque, E. De Clercq, H.Q. Liu, and X.Y. Liu. ChemMedChem, 2013. 8(7): p. 1117-1126. ISI[000320928400008].</p>

    <p class="plaintext"><b>[WOS]</b>. HIV_0802-081513.</p>

    <h2>Patent Citations</h2>

    <p class="plaintext">18. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130711&amp;CC=WO&amp;NR=2013103724A1&amp;KC=A1">2-(tert-Butoxy)-2-(7-methylquinolin-6-yl)acetic acid Derivatives as Antiviral Agents and Their Preparation and Use in the Treatment of AIDS.</a> Babaoglu, K., K.L. Bjornson, P. Hrvatin, E. Lansdon, J.O. Link, H. Liu, R. McFadden, M.L. Mitchell, Y. Qi, P.A. Roethle, and L. Xu. Patent. 2013. 2013-US20151 2013103724: 184pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">19. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130605&amp;CC=CN&amp;NR=103130620A&amp;KC=A">Lignin Compounds Extracted from Chinese Medicine Magnolia and Their Application Thereof.</a> Chen, W., C. Yang, Y. Guo, H. Zhou, T. Zhang, Y. Tang, H. Ding, J. Li, and J. Feng. Patent. 2013. 2011-10386272 103130620: 11pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">20. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130718&amp;CC=US&amp;NR=2013184202A1&amp;KC=A1">Peptide Inhibitors and Therapeutic Uses Thereof.</a> Guo, C. and A.J. Gow. Patent. 2013. 2012-368833 20130184202: 31pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">21. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130627&amp;CC=WO&amp;NR=2013091144A1&amp;KC=A1">Preparation of Propenoate Derivatives of Betulin Useful for the Treatment of HIV.</a> Han, N., B.A. Johns, and J. Tang. Patent. 2013. 2011-CN2159 2013091144: 106pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">22. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130620&amp;CC=WO&amp;NR=2013090683A1&amp;KC=A1">Preparation of Betulin Propenoate Derivatives for the Treatment of HIV.</a> Han, N., B.A. Johns, and J. Tang. Patent. 2013. 2012-US69688 2013090683: 105pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">23. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130627&amp;CC=WO&amp;NR=2013092591A1&amp;KC=A1">HIV Membrane Fusion Inhibitors.</a> Malcolm, B.A., J.W.J. Thuring, C.F.R.N. Buyck, W.B.G. Schepens, M.A.J. Kriek, W.M.M. Schaaper, J.W. Slootstra, and P. Timmerman. Patent. 2013. 2012-EP75956 2013092591: 33pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">24. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130619&amp;CC=CN&amp;NR=103156827A&amp;KC=A">Application of Stilbene-based Compound for Treating and Preventing AIDS.</a> Rao, Z., C. Yang, J. Li, T. Zhang, J. Feng, Y. Tang, Y. Guo, and W. Chen. Patent. 2013. 2011-10411373 103156827: 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">25. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130619&amp;CC=CN&amp;NR=103156868A&amp;KC=A">Application of Stilbene Glycoside-based Compound for Treating and Preventing AIDS.</a> Rao, Z., C. Yang, J. Li, T. Zhang, J. Feng, Y. Tang, C. Peng, and W. Chen. Patent. 2013. 2011-10409315 103156868: 10pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">26. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130605&amp;CC=CN&amp;NR=103127040A&amp;KC=A">Application of Cortex Magnoliae officinalis Extract in Preparing Drugs for Preventing and Treating AIDS.</a> Rao, Z., C. Yang, Z. Lou, Y. Guo, H. Zhou, W. Chen, T. Zhang, Y. Tang, J. Li, and J. Feng. Patent. 2013. 2011-10387541 103127040: 11pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">27. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130620&amp;CC=WO&amp;NR=2013090696A1&amp;KC=A1">Preparation of Indene Compounds as CD4-mimetic Inhibitors of HIV-1 Entry and Methods of Therapeutic Use Thereof.</a> Sodroski, J., J.M. Lalonde, A.B. Smith, III, P.D. Kwong, Y.D. Kwon, D.M. Jones, A.W. Sun, J.R. Courter, T. Soeta, T. Kobayashi, A.M. Princiotto, X. Wu, J.R. Mascola, A. Schon, E. Freire, N. Madani, M. Le-Khac, and W.A. Hendrickson. Patent. 2013. 2012-US69708 2013090696: 135pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>

    <br />

    <p class="plaintext">28. <a href="http://worldwide.espacenet.com/publicationDetails/biblio?DB=EPODOC&amp;II=0&amp;ND=3&amp;adjacent=true&amp;locale=en_EP&amp;FT=D&amp;date=20130627&amp;CC=WO&amp;NR=2013091096A1&amp;KC=A1">Preparation of Condensed Tricyclic Compounds as Inhibitors of HIV-1 Replication.</a> Sturino, C., P. Beaulieu, P. Deroy, M. Duplessis, C. James, J.-E. Lacoste, J. Minville, L. Morency, S. Morin, B. Simoneau, and M. Tremblay. Patent. 2013. 2012-CA50909 2013091096: 154pp.</p>

    <p class="plaintext"><b>[Patent]</b>. HIV_0802-081513.</p>
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
