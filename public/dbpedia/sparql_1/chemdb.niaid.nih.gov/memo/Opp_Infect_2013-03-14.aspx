

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2013-03-14.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RML9lGYFfQ9nERaYLy0etpkyr7EXCnPn0wpJO82KVeVbLIZP5dcRuZFLuIjguGHOtwTiBuVUWzwr8lQDkLnnu/e5l+VF70eAVdiEY3C4Gfr8/0Gg88AZy/FLLdg=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="47D815E1" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="WordSection1">
    <h1 class="memofmt2-h1">Opportunistic Infections Citation List: March 1 - March 14, 2013</h1>

    <h2>ANTIFUNGAL COMPOUNDS (includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</h2>

    <p class="plaintext">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23455192">Inhibitory Effects of Olea ferruginea Crude Leaves Extract against Some Bacterial and Fungal Pathogen.</a> Amin, A., M.A. Khan, S. Shah, M. Ahmad, M. Zafar, and A. Hameed. Pakistan Journal of Pharmaceutical Sciences, 2013. 26(2): p. 251-254. PMID[23455192].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23107040">Homoisocitrate Dehydrogenase from Candida albicans: Properties, Inhibition, and Targeting by an Antifungal Pro-Drug.</a> Gabriel, I., N.D. Vetter, D.R. Palmer, M.J. Milewska, M. Wojciechowski, and S. Milewski. FEMS Yeast Research, 2013. 13(2): p. 143-155. PMID[23107040].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23470196">Chitosan-thioglycolic <b>a</b>cid as a Versatile Antimicrobial Agent.</a> Geisberger, G., E.B. Gyenge, D. Hinger, A. Kach, C. Maake, and G.R. Patzke. Biomacromolecules, 2013. <b>[Epub ahead of print]</b>. PMID[23470196].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">4. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23449067">Synthesis, Antibacterial and Antifungal Activity of Some New Pyrazoline and Pyrazole Derivatives.</a> Hassan, S.Y. Molecules, 2013. 18(3): p. 2683-2711. PMID[23449067].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">5. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23287649">Rapid Microwave Assisted Synthesis and Antimicrobial Bioevaluation of Novel Steroidal Chalcones.</a> Kakati, D., R.K. Sarma, R. Saikia, N.C. Barua, and J.C. Sarma. Steroids, 2013. 78(3): p. 321-326. PMID[23287649].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">6. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23065392">Anthelmintic and Antimicrobial Activity of Methanolic and Aqueous Extracts of Euphorbia helioscopia L.</a> Lone, B.A., S.A. Bandh, M.Z. Chishti, F.A. Bhat, H. Tak, and H. Nisa. Tropical Animal Health and Production, 2013. 45(3): p. 743-749. PMID[23065392].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">7. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23423648">Copper(II) Triflate Promoted Multicomponent Catalytic Clubbing of Piperazinyl-thiazoloquinolines and Thiazolocoumarins as Antimicrobials.</a> Patel, R.V., J.K. Patel, S.H. Nile, and S.W. Park. Archiv der Pharmazie, 2013. 346(3): p. 221-231. PMID[23423648].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">8. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23478965">Identification and Evaluation of Novel Acetolactate Synthase Inhibitors as Antifungal Agents.</a> Richie, D.L., K.V. Thompson, C. Studer, V. Prindle, T. Aust, R. Riedl, D. Estoppey, J. Tao, J.A. Sexton, T. Zabawa, J. Drumm, S. Cotesta, J. Eichenberger, S. Schuierer, N. Hartmann, N.R. Movva, J.A. Tallarico, N.S. Ryder, and D. Hoepfner. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23478965].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br />  

    <p class="plaintext">9. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23295929">MIC Values of Voriconazole Are Predictive of Treatment Results in Murine Infections by Aspergillus terreus Species Complex.</a> Salas, V., F.J. Pastor, D.A. Sutton, E. Calvo, E. Mayayo, A.W. Fothergill, M.G. Rinaldi, and J. Guarro. Antimicrobial Agents and Chemotherapy, 2013. 57(3): p. 1532-1534. PMID[23295929].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">10. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23474654">New Water-<b>s</b>oluble Polypyridine Silver(I) Derivatives of 1,3,5-Triaza-7-phosphaadamantane (PTA) with Significant Antimicrobial and Antiproliferative Activities.</a> Smolenski, P., S.W. Jaros, C. Pettinari, G. Lupidi, L. Quassinti, M. Bramucci, L.A. Vitali, D. Petrelli, A. Kochel, and A.M. Kirillov. Dalton Transactions, 2013. <b>[Epub ahead of print]</b>. PMID[23474654].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">11. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23459486">Mechanism of Action of Efinaconazole, a Novel Triazole Antifungal Agent.</a> Tatsumi, Y., M. Nagashima, T. Shibanushi, A. Iwata, Y. Kangawa, F. Inui, W.J. Jo Siu, R. Pillai, and Y. Nishiyama. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23459486].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">12. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23441911">Isolation and Characterization of New p-Terphenyls with Antifungal, Antibacterial, and Antioxidant Activities from Halophilic Actinomycete Nocardiopsis gilva Yim 90087.</a> Tian, S.Z., X. Pu, G. Luo, L.X. Zhao, L.H. Xu, W.J. Li, and Y. Luo. Journal of Agricultural and Food Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23441911].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">13. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23468248">Identification and Characterization of Antifungal Active Substances of Streptomyces hygroscopicus BS-112.</a> Zhang, N., Z. Song, Y. Xie, P. Cui, H. Jiang, T. Yang, R. Ju, Y. Zhao, J. Li, and X. Liu. World Journal of Microbiology &amp; Biotechnology, 2013. <b>[Epub ahead of print]</b>. PMID[23468248].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p>

    <h2>ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp., Drug Resistant, MAC, MDR)</h2>

    <p class="plaintext">14. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23472097">Posaconazole Exhibits in Vitro and in Vivo Synergistic Antifungal Activity with Caspofungin or FK506 against Candida albicans.</a> Chen, Y.L., V.N. Lehman, A.F. Averette, J.R. Perfect, and J. Heitman. Plos One, 2013. 8(3): p. e57672. PMID[23472097].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">15. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23295923">Preliminary Pharmacokinetic Study of Repeated Doses of Rifampin and Rifapentine in Guinea Pigs.</a> Dutta, N.K., A. Alsultan, C.A. Peloquin, and P.C. Karakousis. Antimicrobial Agents and Chemotherapy, 2013. 57(3): p. 1535-1537. PMID[23295923].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">16. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23463208">Thioridazine Lacks Bactericidal Activity in an Animal Model of Extracellular Tuberculosis.</a> Dutta, N.K., M.L. Pinn, M. Zhao, M.A. Rudek, and P.C. Karakousis. The Journal of Antimicrobial Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23463208].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">17. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23164166">Boron-containing Peptidomimetics - a Novel Class of Selective Anti-tubercular Drugs.</a> Gorovoy, A.S., O.V. Gozhina, J.S. Svendsen, A.A. Domorad, G.V. Tetz, V.V. Tetz, and T. Lejon. Chemical Biology &amp; Drug Design, 2013. 81(3): p. 408-413. PMID[23164166].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">18. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23391589">Discovery of Novel Inhibitors Targeting the Mycobacterium tuberculosis O-acetylserine Sulfhydrylase (CysK1) Using Virtual High-throughput Screening.</a> Jean Kumar, V.U., O. Poyraz, S. Saxena, R. Schnell, P. Yogeeswari, G. Schneider, and D. Sriram. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1182-1186. PMID[23391589].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">19. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23463335">Aromatic Polyketides from a Sponge-derived Fungus Metarhizium Anisopliae mxh-99 and Their Antitubercular Activities.</a> Kong, X., X. Ma, Y. Xie, S. Cai, T. Zhu, Q. Gu, and D. Li. Archives of Pharmacal Research, 2013. <b>[Epub ahead of print]</b>. PMID[23463335].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">20. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23357633">Antimycobacterial Activity Evaluation, Time-kill Kinetic and 3d-Qsar Study of C-(3-Aminomethyl-cyclohexyl)-methylamine Derivatives.</a> Kumar, D., K.K. Raj, M. Bailey, T. Alling, T. Parish, and D.S. Rawat. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1365-1369. PMID[23357633]. <b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">21. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23464516">Discovery of Novel InhA Reductase Inhibitors: Application of Pharmacophore- and Shape-based Screening Approach.</a> Kumar, U.C., S.K. Bvs, S. Mahmood, S. D, P. Kumar-Sahu, S. Pulakanam, L. Ballell, D. Alvarez-Gomez, S. Malik, and S. Jarp. Future Medicinal Chemistry, 2013. 5(3): p. 249-259. PMID[23464516].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23337743">Zanthoxylum Capense Constituents with Antimycobacterial Activity against Mycobacterium tuberculosis in Vitro and ex Vivo within Human Macrophages.</a> Luo, X., D. Pires, J.A. Ainsa, B. Gracia, N. Duarte, S. Mulhovo, E. Anes, and M.J. Ferreira. Journal of Ethnopharmacology, 2013. 146(1): p. 417-422. PMID[23337743].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">23. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23225597">Studies of Inositol 1-Phosphate Analogues as Inhibitors of the Phosphatidylinositol Phosphate Synthase in Mycobacteria.</a> Morii, H., T. Okauchi, H. Nomiya, M. Ogawa, K. Fukuda, and H. Taniguchi. Journal of Biochemistry, 2013. 153(3): p. 257-266. PMID[23225597].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">24. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23357635">Synthesis and Antitubercular Activity of Amino Alcohol Fused Spirochromone Conjugates.</a> Mujahid, M., R.G. Gonnade, P. Yogeeswari, D. Sriram, and M. Muthukrishnan. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1416-1419. PMID[23357635].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">25. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23478953">Targeting Mycobacterium tuberculosis and Other Microbial Pathogens Using Improved Synthetic Antibacterial Peptides.</a> Ramon-Garcia, S., R. Mikut, C. Ng, S. Ruden, R. Volkmer, M. Reischl, K. Hilpert, and C.J. Thompson. Antimicrobial Agents and Chemotherapy, 2013. <b>[Epub ahead of print]</b>. PMID[23478953].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">26. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23474218">Anti-tuberculosis Neolignans from Piper regnellii.</a> Scodro, R.B., C.T. Pires, V.S. Carrara, C.O. Lemos, L. Cardozo-Filho, V.A. Souza, A.G. Correa, V.L. Siqueira, M.V. Lonardoni, R.F. Cardoso, and D.A. Cortez. Phytomedicine, 2013. <b>[Epub ahead of print]</b>. PMID[23474218].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">27. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23352268">A Facile Three-component [3+2]-cycloaddition for the Regioselective Synthesis of Highly Functionalised Dispiropyrrolidines Acting as Antimycobacterial Agents.</a> Wei, A.C., M.A. Ali, Y.K. Yoon, R. Ismail, T.S. Choon, and R.S. Kumar. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1383-1386. PMID[23352268].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">28. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23467917">Synthesis of Novel Spirooxindolo-pyrrolidines, Pyrrolizidines, and Pyrrolothiazoles Via a Regioselective Three-component [3+2] cycloaddition and Their Preliminary Antimicrobial Evaluation.</a> Wu, G., L. Ouyang, J. Liu, S. Zeng, W. Huang, B. Han, F. Wu, G. He, and M. Xiang. Molecular Diversity, 2013. <b>[Epub ahead of print]</b>. PMID[23467917].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">29. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23337602">3H-1,2,4-dithiazol-3-one Compounds as Novel Potential Affordable Antitubercular Agents.</a> Yang, J., W. Pi, L. Xiong, W. Ang, T. Yang, J. He, Y. Liu, Y. Chang, W. Ye, Z. Wang, Y. Luo, and Y. Wei. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1424-1427. PMID[23337602].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p>

    <h2>ANTIMALARIAL COMPOUNDS (Plasmodium spp.)</h2>

    <p class="plaintext">30. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23357632">Anti-malarial Activity of New N-Acetyl-L-leucyl-L-leucyl-L-norleucinal (ALLN) Derivatives against Plasmodium falciparum.</a> Choi, H.J., M. Cui, D.Y. Li, H.O. Song, H.S. Kim, and H. Park. Bioorganic &amp; Medicinal Chemistry Letters, 2013. 23(5): p. 1293-1296. PMID[23357632].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">31. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23410043">Antimalarial Activities of 6-Iodouridine and Its Prodrugs and Potential for Combination Therapy.</a> Crandall, I.E., E. Wasilewski, A.M. Bello, A. Mohmmed, P. Malhotra, E.F. Pai, K.C. Kain, and L.P. Kotra. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23410043].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">32. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23444099">Selective Inhibition of an Apicoplastic Aminoacyl-tRNA Synthetase from Plasmodium falciparum.</a> Hoen, R., E.M. Novoa, A. Lopez, N. Camacho, L. Cubells, P. Vieira, M. Santos, P. Marin-Garcia, J.M. Bautista, A. Cortes, L. Ribas de Pouplana, and M. Royo. ChemBioChem, 2013. 14(4): p. 499-509. PMID[23444099].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23448281">Acyclic Nucleoside Phosphonates Containing a Second Phosphonate Group Are Potent Inhibitors of 6-Oxopurine phosphoribosyltransferases and Have Antimalarial Activity.</a> Keough, D.T., P. Spacek, D. Hockova, T. Tichy, S. Vrbkova, L. Slavetinska, Z. Janeba, L. Naesens, M.D. Edstein, M. Chavchich, T.H. Wang, J.D. Jersey, and L.W. Guddat. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23448281].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">34. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23448281">Antimalarial Interaction of Quinine and Quinidine with Clarithromycin.</a> Pandey, S.K., H. Dwivedi, S. Singh, W.A. Siddiqui, and R. Tripathi. Parasitology, 2013. 140(3): p. 406-413. PMID[23137860].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p> 

    <h2>ANTIPROTOZOAL COMPOUNDS</h2>

    <p class="plaintext">35. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23470217">Optimizing Small Molecule Inhibitors of Calcium-dependent Protein Kinase 1 to Prevent Infection by Toxoplasma gondii.</a> Lourido, S., C. Zhang, M. Lopez, K. Tang, J. Barks, Q. Wang, S.A. Wildman, K. Shokat, and L.D. Sibley. Journal of Medicinal Chemistry, 2013. <b>[Epub ahead of print]</b>. PMID[23470217].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">36. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23270807">Toxoplasma gondii: The Effect of Fluconazole Combined with Sulfadiazine and Pyrimethamine against Acute Toxoplasmosis in Murine Model.</a> Martins-Duarte, E.S., W. de Souza, and R.C. Vommaro. Experimental Parasitology, 2013. 133(3): p. 294-299. PMID[23270807].</p>

    <p class="plaintext"><b>[PubMed]</b>. OI_0301-031413.</p>

    <h2>Citations from the ISI Web of Knowledge Listings for O.I.</h2>

    <p class="plaintext">37. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500001">Quinolone Derivatives as Antitubercular Drugs.</a>  Asif, M., A.A. Siddiqui, and A. Husain. Medicinal Chemistry Research, 2013. 22(3): p. 1029-1042. ISI[000314283500001].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">38. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313711700031">Synthesis and Biological Evaluation of Novel Benzoquinones as Potential Antimicrobial Agents.</a> Chaaban, I., E.M. El Khawass, M.A. Mahran, H.A. Abd El Razik, N.S. El Salamouni, and A.E.A. Wahab. Medicinal Chemistry Research, 2013. 22(2): p. 841-851. ISI[000313711700031].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">39. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314048800075">Synthesis, Structure-Activity Relationships (SAR) and in Silico Studies of Coumarin Derivatives with Antifungal Activity.</a> de Araujo, R.S.A., F.Q.S. Guerra, E.D. Lima, C.A. de Simone, J.F. Tavares, L. Scotti, M.T. Scotti, T.M. de Aquino, R.O. de Moura, F.J.B. Mendonca, and J.M. Barbosa. International Journal of Molecular Sciences, 2013. 14(1): p. 1293-1309. ISI[000314048800075].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">40. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500015">Synthesis of Promising Antimicrobial Agents: A Novel Series of N-(4-(2,6-dichloroquinolin-3-yl)-6-(aryl)pyrimidin-2-yl)-2-morpholinoace tamides.</a> Desai, N.C., K.M. Rajpara, V.V. Joshi, H.V. Vaghani, and H.M. Satodiya. Medicinal Chemistry Research, 2013. 22(3): p. 1172-1183. ISI[000314283500015].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">41. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500006">Synthesis of New 4-(2,5-Dimethylpyrrol-1-yl)/4-pyrrol-1-yl benzoic acid Hydrazide Analogs and Some Derived Oxadiazole, Triazole and Pyrrole Ring Systems: A Novel Class of Potential Antibacterial, Antifungal and Antitubercular Agents.</a> Joshi, S.D., Y. More, H.M. Vagdevi, V.P. Vaidya, G.S. Gadaginamath, and V.H. Kulkarni. Medicinal Chemistry Research, 2013. 22(3): p. 1073-1089. ISI[000314283500006].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">42. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313711700024">Synthesis and Antimicrobial Activity of Coumarin Pyrazole Pyrimidine 2,4,6(1H,3H,5H)Triones and Thioxopyrimidine4,6(1H,5H)diones.</a> Laxmi, S.V., B.S. Kuarm, and B. Rajitha. Medicinal Chemistry Research, 2013. 22(2): p. 768-774. ISI[000313711700024].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">43. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000313711700013">Quinoline-based Azetidinone and Thiazolidinone Analogues as Antimicrobial and Antituberculosis Agents.</a> Mistry, B.M. and S. Jauhari. Medicinal Chemistry Research, 2013. 22(2): p. 647-658. ISI[000313711700013].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">44. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314444200011">Antibacterial and Antifungal Activities of Crude Plant Extracts from Colombian Biodiversity.</a>. Nino, J., O.M. Mosquera, and Y.M. Correa. Revista de Biologia Tropical, 2012. 60(4): p. 1535-1542. ISI[000314444200011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">45. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500030">Anti-HIV, Antimycobacterial and Antimicrobial Studies of Newly Synthesized 1,2,4-Triazole clubbed benzothiazoles.</a> Patel, N.B., I.H. Khan, C. Pannecouque, and E. De Clercq. Medicinal Chemistry Research, 2013. 22(3): p. 1320-1329. ISI[000314283500030].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">46. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500011">Design, Synthesis and Molecular Modelling of Novel 4-Thiazolidinones of Potential Activity against Gram Positive Bacteria.</a> Radwan, A.A. Medicinal Chemistry Research, 2013. 22(3): p. 1131-1141. ISI[000314283500011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">47. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314283500021">Design and Synthesis of Positional Isomers of 1-Alkyl-2-trifluoromethyl-5 or 6-substituted benzimidazoles and Their Antimicrobial Activity.</a> Sathaiah, G., A.R. Kumar, A.C. Shekhar, K. Raju, P.S. Rao, B. Narsaiah, A.R. Reddy, D. Lakshmi, and B. Sridhar. Medicinal Chemistry Research, 2013. 22(3): p. 1229-1237. ISI[000314283500021].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">48. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314383100011">Potent Activity against Multidrug-resistant Mycobacterium tuberculosis of alpha-Mangostin Analogs.</a> Sudta, P., P. Jiarawapi, A. Suksamrarn, P. Hongmanee, and S. Suksamrarn. Chemical &amp; Pharmaceutical Bulletin, 2013. 61(2): p. 194-203. ISI[000314383100011].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">49. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314311600008">Structure-Activity Relationships of Tetrahydrocarbazole Derivatives as Antifungal Lead Compounds.</a> Wang, W.Y., G.Q. Dong, J.L. Gu, Y.Q. Zhang, S.Z. Wang, S.P. Zhu, Y. Liu, Z.Y. Miao, J.Z. Yao, W.N.A. Zhang, and C.Q. Sheng. MedChemComm, 2013. 4(2): p. 353-362. ISI[000314311600008].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br /> 

    <p class="plaintext">50. <a href="http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=WOS:000314630100007">Evaluation of the Anticryptococcal Activity of the Antibiotic Polymyxin B in Vitro and in Vivo.</a> Zhai, B. and X.R. Lin. International Journal of Antimicrobial Agents, 2013. 41(3): p. 250-254. ISI[000314630100007].</p>

    <p class="plaintext"><b>[WOS]</b>. OI_0301-031413.</p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
