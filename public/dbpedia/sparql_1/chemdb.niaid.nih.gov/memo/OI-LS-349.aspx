

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-349.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="bkJBqhWMTbL5EGDgtj/avmry8r9DN5V4KER9friyqbA+QOcm16TT2AXAb6nM+ig1b3v1sP7sPj3Hu/2BIQfZWiKLrVDORj/Pnieay0KYvPvSiQkQEF+/1BXMsmI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="84F4DD92" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-349-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     48940   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Antimicrosporidial Activities of Fumagillin, TNP-470, Ovalicin, and Ovalicin Derivatives In Vitro and In Vivo</p>

    <p>          Didier, PJ, Phillips, JN, Kuebler, DJ, Nasr, M, Brindley, PJ, Stovall, ME, Bowers, LC, and Didier, ES</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(6): 2146-55</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723577&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723577&amp;dopt=abstract</a> </p><br />

    <p>2.     48941   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Novel Conjugate of Moxifloxacin and Carboxymethylated Glucan with Enhanced Activity against Mycobacterium tuberculosis</p>

    <p>          Schwartz, YS, Dushkin, MI, Vavilin, VA, Melnikova, EV, Khoschenko, OM, Kozlov, VA, Agafonov, AP, Alekseev, AY, Rassadkin, Y, Shestapalov, AM, Azaev, MS, Saraev, DV, Filimonov, PN, Kurunov, Y, Svistelnik, AV, Krasnov, VA, Pathak, A, Derrick, SC, Reynolds, RC, Morris, S, and Blinov, VM</p>

    <p>          Antimicrob Agents Chemother <b>2006</b>.  50(6): 1982-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723555&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16723555&amp;dopt=abstract</a> </p><br />

    <p>3.     48942   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Discovery of Proline Sulfonamides as Potent and Selective Hepatitis C Virus NS5b Polymerase Inhibitors. Evidence for a New NS5b Polymerase Binding Site</p>

    <p>          Gopalsamy, A, Chopra, R, Lim, K, Ciszewski, G, Shi, M, Curran, KJ, Sukits, SF, Svenson, K, Bard, J, Ellingboe, JW, Agarwal, A, Krishnamurthy, G, Howe, AY, Orlowski, M, Feld, B, O&#39;connell, J, and Mansour, TS</p>

    <p>          J Med Chem <b>2006</b>.  49(11): 3052-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722622&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722622&amp;dopt=abstract</a> </p><br />

    <p>4.     48943   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Identification of New Diamine Scaffolds with Activity against Mycobacterium tuberculosis</p>

    <p>          Bogatcheva, E, Hanrahan, C, Nikonenko, B, Samala, R, Chen, P, Gearhart, J, Barbosa, F, Einck, L, Nacy, CA, and Protopopova, M</p>

    <p>          J Med Chem <b>2006</b>.  49(11): 3045-8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722620&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16722620&amp;dopt=abstract</a> </p><br />

    <p>5.     48944   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Small molecule and biologic modulators of the immune response to hepatitis C virus</p>

    <p>          Mbow, ML, Eaton-Bassiri, A, Glass, WG, Del, Vecchio AM, and Sarisky, RT</p>

    <p>          Mini Rev Med Chem <b>2006</b>.  6(5): 527-31</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16719827&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16719827&amp;dopt=abstract</a> </p><br />

    <p>6.     48945   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Susceptibility of Mycobacterium tuberculosis Strains to Gatifloxacin and Moxifloxacin by Different Methods</p>

    <p>          Somasundaram, S and Paramasivan, NC</p>

    <p>          Chemotherapy <b>2006</b>.  52(4): 190-195</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16714850&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16714850&amp;dopt=abstract</a> </p><br />

    <p>7.     48946   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Brevinin-1BYa: a naturally occurring peptide from frog skin with broad-spectrum antibacterial and antifungal properties</p>

    <p>          Pal, T, Abraham, B, Sonnevend, A, Jumaa, P, and Conlon, JM</p>

    <p>          Int J Antimicrob Agents <b>2006</b>.  27(6): 525-529</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713189&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16713189&amp;dopt=abstract</a> </p><br />

    <p>8.     48947   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          MyD88-dependent pathways mediate resistance to Cryptosporidium parvum infection in mice</p>

    <p>          Rogers, KA, Rogers, AB, Leav, BA, Sanchez, A, Vannier, E, Uematsu, S, Akira, S, Golenbock, D, and Ward, HD</p>

    <p>          Infection and Immunity <b>2006</b>.  74(1): 549-556</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>9.     48948   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Interaction of Cryptosporidium hominis and Cryptosporidium parvum with primary human and bovine intestinal cells</p>

    <p>          Hashim, Amna, Mulcahy, Grace, Bourke, Billy, and Clyne, Marguerite</p>

    <p>          Infection and Immunity <b>2006</b>.  74(1): 99-107</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>10.   48949   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Antifungal and antimycobacterial activity of new imidazole and triazole derivatives. A combined experimental and computational approach</p>

    <p>          Banfi, E, Scialino, G, Zampieri, D, Mamolo, MG, Vio, L, Ferrone, M, Fermeglia, M, Paneni, MS, and Pricl, S</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16709593&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16709593&amp;dopt=abstract</a> </p><br />

    <p>11.   48950   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Antibodies enhance the protective effect of CD4 + T lymphocytes in SCID mice perorally infected with Encephalitozoon cuniculi</p>

    <p>          Sak, B, Salat, J, Horka, H, Sakova, K, and Ditrich, O</p>

    <p>          Parasite Immunology <b>2006</b>.  28(3): 95-99</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>12.   48951   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Three-dimensional modeling of cytomegalovirus DNA polymerase and preliminary analysis of drug resistance</p>

    <p>          Shi, R, Azzi, A, Gilbert, C, Boivin, G, and Lin, SX</p>

    <p>          Proteins <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16705640&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16705640&amp;dopt=abstract</a> </p><br />

    <p>13.   48952   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Variability in infection efficiency in vitro of different strains of the microsporidian Encephalitozoon hellem</p>

    <p>          Haro Maria, Aguila Carmen, Fenoy Soledad, and Henriques-Gil Nuno</p>

    <p>          The Journal of eukaryotic microbiology <b>2006</b>.  53(1): 46-8.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>14.   48953   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Antiproliferative activities of two novel quinuclidine inhibitors against Toxoplasma gondii tachyzoites in vitro</p>

    <p>          Martins-Duarte, ES, Urbina, JA, de, Souza W, and Vommaro, RC</p>

    <p>          J Antimicrob Chemother <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16702175&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16702175&amp;dopt=abstract</a> </p><br />

    <p>15.   48954   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Comparative effects of ertapenem, imipenem, and meropenem on the colonization of the gastrointestinal tract of mice by Candida albicans</p>

    <p>          Samonis, G, Maraki, S, Leventakos, K, Spanaki, AM, Kateifidis, A, Galanakis, E, Tselentis, Y, Falagas, ME, and Mantadakis, E</p>

    <p>          Med Mycol <b>2006</b>.  44(3): 233-5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16702102&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16702102&amp;dopt=abstract</a> </p><br />

    <p>16.   48955   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          New plan for tuberculosis</p>

    <p>          Anon</p>

    <p>          S Afr Med J <b>2006</b>.  96(4): 278, 280</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16696121&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16696121&amp;dopt=abstract</a> </p><br />

    <p>17.   48956   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Polymers with fungicidal activity</p>

    <p>          Schmidt, Oskar </p>

    <p>          PATENT:  WO <b>2006047800</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 15 pp.</p>

    <p>          ASSIGNEE:  (Austria)</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>18.   48957   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Antifungal properties of some arylsuloanylpyrazine-2-carboxylic acid derivatives</p>

    <p>          Jampilek, Josef, Dolezal, Martin, Palek, Lukas, Silva, Luis, and Buchta, Vladimir</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2006</b>.  33: 23-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>19.   48958   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Antifungal activity of 1-phenyl-5-benzylsulfanyltetrazoles</p>

    <p>          Adamec, Jan, Waisser, Karel, Silva, Luis, and Buchta, Vladimir</p>

    <p>          Folia Pharmaceutica Universitatis Carolinae <b>2006</b>.  33: 13-17</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>20.   48959   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Leflunomide: a possible alternative for gangciclovir sensitive and resistant cytomegalovirus infections</p>

    <p>          Sudarsanam, TD, Sahni, RD, and John, GT</p>

    <p>          Postgrad Med J  <b>2006</b>.  82(967): 313-4</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16679469&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16679469&amp;dopt=abstract</a> </p><br />

    <p>21.   48960   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Moxifloxacin versus Ethambutol in the First Two Months of Treatment for Pulmonary Tuberculosis</p>

    <p>          Burman, WJ, Goldberg, S, Johnson, JL, Muzanye, G, Engle, M, Mosher, AW, Choudhri, S, Daley, CL, Munsiff, SS, Zhao, Z, Vernon, A, and Chaisson, RE</p>

    <p>          Am J Respir Crit Care Med <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675781&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675781&amp;dopt=abstract</a> </p><br />

    <p>22.   48961   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          COX-1, COX-2 inhibitors and antifungal agents from Croton hutchinsonianus</p>

    <p>          Athikomkulchai, Sirivan, Prawat, Hunsa, Thasana, Nopporn, Ruangrungsi, Nijsiri, and Ruchirawat, Somsak</p>

    <p>          Chemical &amp; Pharmaceutical Bulletin <b>2006</b>.  54(2): 262-264</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>23.   48962   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          Cytomegalovirus disease in the era of highly active antiretroviral therapy (HAART)</p>

    <p>          Steininger, C, Puchhammer-Stockl, E, and Popow-Kraupp, T</p>

    <p>          J Clin Virol <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675299&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16675299&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>24.   48963   OI-LS-349; PUBMED-OI-5/30/2006</p>

    <p class="memofmt1-2">          The structure of PknB in complex with mitoxantrone, an ATP-competitive inhibitor, suggests a mode of protein kinase regulation in mycobacteria</p>

    <p>          Wehenkel, A, Fernandez, P, Bellinzoni, M, Catherinot, V, Barilone, N, Labesse, G, Jackson, M, and Alzari, PM</p>

    <p>          FEBS Lett <b>2006</b>.  580(13): 3018-22</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16674948&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16674948&amp;dopt=abstract</a> </p><br />

    <p>25.   48964   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Antifungal properties of new series of quinoline derivatives</p>

    <p>          Musiol, Robert, Jampilek, Josef, Buchta, Vladimir, Silva, Luis, Niedbala, Halina, Podeszwa, Barbara, Palka, Anna, Majerz-Maniecka, Katarzyna, Oleksyn, Barbara, and Polanski, Jaroslaw</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2006</b>.  14(10): 3592-3598</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>26.   48965   OI-LS-349; WOS-OI-5/21/2006</p>

    <p class="memofmt1-2">          Antimycobacterial brominated metabolites from two species of marine sponges</p>

    <p>          De Oliveira, MF, de Oliveira, JHHL, Galetti, FCS, De Souza, AO, Silva, CL, Hajdu, E, Peixinho, S, and Berlinck, RGS</p>

    <p>          PLANTA MEDICA <b>2006</b>.  72(5): 437-441, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237192200009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237192200009</a> </p><br />

    <p>27.   48966   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Synthesis of 1-acyl-2-alkylthio-1,2,4-triazolobenzimidazoles with antifungal, anti-inflammatory and analgesic effects</p>

    <p>          Mohamed, Bahaa G, Abdel-Alim, Abdel-Alim M, and Hussein, Mostafa A</p>

    <p>          Acta Pharmaceutica (Zagreb, Croatia) <b>2006</b>.  56(1): 31-48</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>28.   48967   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          N-Methyl-4-hydroxy-2-pyridinone analogues from Fusarium oxysporum</p>

    <p>          Jayasinghe, Lalith, Abbas, Hamed K, Jacob, Melissa R, Herath, Wimal HMW, and Nanayakkara, NPDhammika</p>

    <p>          Journal of Natural Products <b>2006</b>.  69(3): 439-442</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>29.   48968   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Antifungal bis[bibenzyls] from the chinese liverwort Marchantia polymorpha L</p>

    <p>          Niu, Chong, Qu, Jian-Bo, and Lou, Hong-Xiang</p>

    <p>          Chemistry &amp; Biodiversity <b>2006</b>.  3(1): 34-40</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>30.   48969   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Synthesis and biological evaluation of some pyrazolinylpyridines and pyrazolylpyridines</p>

    <p>          Singh Tripti, Sharma Shalabh, Srivastava Virendra Kishore, and Kumar Ashok</p>

    <p>          Archiv der Pharmazie <b>2006</b>.  339(1): 24-31.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>31.   48970   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Synthesis and antimicrobial evaluation of some t(3)-alkyl and t(3),t(5)-dimethyl-r(2),c(6)-di-2&#39;- furfurylpiperidin-4-one and its derivatives</p>

    <p>          Jayabharathi, J, Sivakumar, R, and Praveena, A</p>

    <p>          Medicinal Chemistry Research <b>2006</b>.  14(4): 198-210</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>32.   48971   OI-LS-349; WOS-OI-5/21/2006</p>

    <p class="memofmt1-2">          Synthesis of 1-[3-(4-benz triazol-1/2- l-3-fluoro-p enyl)-2-oxo- xazolidin-5-ylmethyl]-3-substituted-thiourea derivatives as antituberculosis agents</p>

    <p>          Dixit, PP, Patil, VJ, Nair, PS, Jain, S, Sinha, N, and Arora, SK</p>

    <p>          EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY <b>2006</b>.  41(3): 423-428, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237184000017">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237184000017</a> </p><br />

    <p>33.   48972   OI-LS-349; WOS-OI-5/21/2006</p>

    <p class="memofmt1-2">          Conserved fungal genes as potential targets for broad-spectrum antifungal drug discovery</p>

    <p>          Liu, MP, Healy, MD, Dougherty, BA, Esposito, KM, Maurice, TC, Mazzucco, CE, Bruccoleri, RE, Davison, DB, Frosco, M, Barrett, JF, and Wang, YK</p>

    <p>          EUKARYOTIC CELL <b>2006</b>.  5(4): 638-649, 12</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237184300004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237184300004</a> </p><br />

    <p>34.   48973   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Oxazolidinone derivatives, particularly benzoxadiazole phenyloxazolidinones, useful as antimicrobials, and processes for their preparation, pharmaceutical compositions containing them, and methods of their use for treating microbial infections</p>

    <p>          Das Biswajit,  Rudra, Sonali, Salman, Mohammad, and Rattan, Ashok</p>

    <p>          PATENT:  WO <b>2006035283</b>  ISSUE DATE: 20060406</p>

    <p>          APPLICATION: 2005  PP: 79 pp.</p>

    <p>          ASSIGNEE:  (Ranbaxy Laboratories Limited, India</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>35.   48974   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Crystallizing new approaches for antimicrobial drug discovery</p>

    <p>          Schmid, Molly B</p>

    <p>          Biochemical Pharmacology <b>2006</b>.  71(7): 1048-1056</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>36.   48975   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Rational Development of b-Peptide Inhibitors of Human Cytomegalovirus Entry</p>

    <p>          English, Emily Payne, Chumanov, Robert S, Gellman, Samuel H, and Compton, Teresa</p>

    <p>          Journal of Biological Chemistry <b>2006</b>.  281(5): 2661-2667</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>37.   48976   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Cyclic cidofovir (HPMPC) prevents congenital cytomegalovirus infection in a guinea pig model</p>

    <p>          Schleiss, Mark R, Anderson, Jodi L, and McGregor, Alistair</p>

    <p>          Virology Journal <b>2006</b>.  3: No pp. given</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>38.   48977   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Inhibition of ganciclovir-resistant human cytomegalovirus replication by Kampo (Japanese herbal medicine)</p>

    <p>          Murayama, Tsugiya, Yamaguchi, Nobuo, Iwamoto, Kozo, and Eizuru, Yoshito</p>

    <p>          Antiviral Chemistry &amp; Chemotherapy <b>2006</b>.  17(1): 11-16</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>39.   48978   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Substituted 5-carboxyamide pyrazoles and [1,2,4]triazoles as antiviral agents</p>

    <p>          Shipps, Gerald W Jr, Curran, Patrick J, Annis, DAllen, Nash, Huw M, Cooper, Alan B, Zhu, Hugh Y, Wang, James J-S, Desai, Jagdish A, and Girijavallabhan, Viyyoor Moopil</p>

    <p>          PATENT:  WO <b>2006050034</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 104 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>40.   48979   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Substituted 5-oxo pyrazoles and [1,2,4]triazoles as antiviral agents</p>

    <p>          Shipps, Gerald W Jr, Wang, Tong, Rosner, Kristen E, Curran, Patrick J, Cooper, Alan B, and Girijavallabhan, Viyyoor Moopil</p>

    <p>          PATENT:  WO <b>2006050035</b>  ISSUE DATE:  20060511</p>

    <p>          APPLICATION: 2005  PP: 80 pp.</p>

    <p>          ASSIGNEE:  (Schering Corporation, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>41.   48980   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Nucleoside analog inhibitors of hepatitis C virus replication</p>

    <p>          Carroll, SS and Olsen, DB</p>

    <p>          Infectious Disorders: Drug Targets <b>2006</b>.  6(1): 17-29</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>42.   48981   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Preparation of tetracyclic indole derivatives as antiviral agents</p>

    <p>          Conte, Immacolata, Ercolani, Caterina, Narjes, Frank, Pompei, Marco, Rowley, Michael, and Stansfield, Ian</p>

    <p>          PATENT:  WO <b>2006046030</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 79 pp.</p>

    <p>          ASSIGNEE:  (Istituto di Ricerche di Biologia Molecolare p Angeletti SpA, Italy</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>43.   48982   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Preparation of 4-(methoxycarbonyl)pyrrolidine-2-carboxylic acid derivatives as hepatitis C virus inhibitors</p>

    <p>          Guidetti, Rossella, Haigh, David, Hartley, Charles David, Howes, Peter David, Nerozzi, Fabrizio, and Smith, Stephen Allan</p>

    <p>          PATENT:  WO <b>2006045613</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 58 pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Ltd., UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>
    <br clear="all">

    <p>44.   48983   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Preparation of 4-(pyrazin-2-yl)pyrrolidine-2-carboxylic acid derivatives as hepatitis C virus inhibitors</p>

    <p>          Guidetti, Rossella, Haigh, David, Hartley, Charles David, Howes, Peter David, Nerozzi, Fabrizio, and Smith, Stephen Allan</p>

    <p>          PATENT:  WO <b>2006045615</b>  ISSUE DATE:  20060504</p>

    <p>          APPLICATION: 2005  PP: 55 pp.</p>

    <p>          ASSIGNEE:  (Glaxo Group Limited, UK</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>45.   48984   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus NS3-NS4A protease activity with VX-950</p>

    <p>          Lin, Chao and Taylor, William P</p>

    <p>          PATENT:  WO <b>2006039488</b>  ISSUE DATE:  20060413</p>

    <p>          APPLICATION: 2005  PP: 57 pp.</p>

    <p>          ASSIGNEE:  (Vertex Pharmaceuticals Incorporated, USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>46.   48985   OI-LS-349; SCIFINDER-OI-5/22/2006</p>

    <p class="memofmt1-2">          Preparation of 4-hydroxy-3-heterocyclylalkyl-5,6-dihydro-2H-pyran-2-ones as inhibitors of hepatitis C virus RNA-dependent RNA polymerase, and compositions and treatments using the same</p>

    <p>          Gonzalez, Javier, Jewell, Tanya Michelle, Li, Hui, Linton, Angelica, and Tatlock, John Howard</p>

    <p>          PATENT:  WO <b>2006018725</b>  ISSUE DATE:  20060223</p>

    <p>          APPLICATION: 2005  PP: 315 pp.</p>

    <p>          ASSIGNEE:  (Pfizer Inc., USA</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>47.   48986   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          Fighting viral hepatitis with small RNAS</p>

    <p>          Will, H and Grundhoff, A</p>

    <p>          JOURNAL OF HEPATOLOGY <b>2006</b>.  44(5): 1009-1011, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237327800024">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237327800024</a> </p><br />

    <p>48.   48987   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          Synthesis of 3-cyanoflavones and their biological evaluation</p>

    <p>          Lassagne, F, Bombarda, I, Dherbomez, M, Sinbandhit, S, and Gaydou, EM</p>

    <p>          HETEROCYCLES <b>2006</b>.  68(4): 787-799, 13</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237379900013">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237379900013</a> </p><br />

    <p>49.   48988   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          The metabolism, pharmacokinetics and mechanisms of antiviral activity of ribavirin against hepatitis C virus</p>

    <p>          Dixit, NM and Perelson, AS</p>

    <p>          CELLULAR AND MOLECULAR LIFE SCIENCES <b>2006</b>.  63(7-8): 832-842, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237359800008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237359800008</a> </p><br />

    <p>50.   48989   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          DBMODELING - A database applied to the study of protein targets from genome projects</p>

    <p>          da Silveira, NJF, Bonalumi, CE, Uchao, HB, Pereira, JH, Canduri, F, and de Azevedo, WF</p>

    <p>          CELL BIOCHEMISTRY AND BIOPHYSICS <b>2006</b>.  44(3): 366-374, 9</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237403800006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237403800006</a> </p><br />

    <p>51.   48990   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          Molecular models of tryptophan synthase from Mycobacterium tuberculosis complexed with inhibitors</p>

    <p>          Dias, MVB, Canduri, F, da Silveira, NJF, Czekster, CM, Basso, LA, Palma, MS, Santos, DS, and de Azevedo, WF</p>

    <p>          CELL BIOCHEMISTRY AND BIOPHYSICS <b>2006</b>.  44(3): 375-384, 10</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237403800007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237403800007</a> </p><br />

    <p>52.   48991   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          Antifungal activity of C-27 steroidal saponins</p>

    <p>          Yang, CR, Zhang, Y, Jacob, MR, Khan, SI, Zhang, YJ, and Li, XC</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(5): 1710-1714, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237365300014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237365300014</a> </p><br />

    <p>53.   48992   OI-LS-349; WOS-OI-5/28/2006</p>

    <p class="memofmt1-2">          Significance of mutations in embB codon 306 for prediction of ethambutol resistance in clinical Mycobactetium tuberculosis isolates</p>

    <p>          Plinke, C, Rusch-Gerdes, S, and Niemann, S</p>

    <p>          ANTIMICROBIAL AGENTS AND CHEMOTHERAPY <b>2006</b>.  50(5): 1900-1902, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237365300049">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000237365300049</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
