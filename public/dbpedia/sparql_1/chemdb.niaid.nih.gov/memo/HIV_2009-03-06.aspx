

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV_2009-03-06.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="labl4I3nMb4t1awR/E19GttK8QaKV5XSXefi/rEpGEypMe09vxkffts+eQS/jX4yoptgERYpQe9+TljSou04QetHMlFvPUKCUWUo4WgqQHFVwEEOjB+Tdwq8vj4=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8E9876A2" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <h1 class="memofmt2-h1">HIV Citation List:  February 18 - March 3, 2009</h1>

    <p class="memofmt2-2">Pubmed citations:</p> 

    <p class="title">1.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19255545?ordinalpos=7&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, Synthesis and Anti-HIV Integrase Evaluation of 4-Oxo-4H-quinolizine-3-carboxylic Acid Derivatives.</a>  Xu YS, Zeng CC, Jiao ZG, Hu LM, Zhong RG.  Molecules. 2009 Feb 19;14(2):868-83.</p>

    <p class="title">PMID: 19255545</p> 

    <p class="title">2.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19245436?ordinalpos=17&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Efficient inhibition of SDF-1alpha-mediated chemotaxis and HIV-1 infection by novel CXCR4 antagonists.</a>  Iwasaki Y, Akari H, Murakami T, Kumakura S, Dewan MZ, Yanaka M, Yamamoto N.  Cancer Sci. 2009 Feb 23. [Epub ahead of print]</p>

    <p class="title">PMID: 19245436</p> 

    <p class="title">3.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19231980?ordinalpos=36&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Raltegravir: The First HIV Type 1 Integrase Inhibitor.</a>  Hicks C, Gulick RM.  Clin Infect Dis. 2009 Feb 20. [Epub ahead of print]</p>

    <p class="title">PMID: 19231980</p> 

    <p class="title">4.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19217787?ordinalpos=49&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design, synthesis, and biological evaluation of novel quinoline derivatives as HIV-1 Tat-TAR interaction inhibitors.</a>  Chen S, Chen R, He M, Pang R, Tan Z, Yang M.  Bioorg Med Chem. 2009 Mar 1;17(5):1948-56. Epub 2009 Jan 23.</p>

    <p class="title">PMID: 19217787</p> 

    <p class="title">5.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/19185490?ordinalpos=62&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">1-Amido-1-phenyl-3-piperidinylbutanes--CCR5 antagonists for the treatment of HIV: Part 2.</a>  Barber CG, Blakemore DC, Chiva JY, Eastwood RL, Middleton DS, Paradowski KA.  Bioorg Med Chem Lett. 2009 Mar 1;19(5):1499-503. Epub 2009 Jan 10.</p>

    <p class="title">PMID: 19185490</p> 

    <p class="title">6.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18955518?ordinalpos=125&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">GRL-02031, a novel nonpeptidic protease inhibitor (PI) containing a stereochemically defined fused cyclopentanyltetrahydrofuran potent against multi-PI-resistant human immunodeficiency virus type 1 in vitro.</a>  Koh Y, Das D, Leschenko S, Nakata H, Ogata-Aoki H, Amano M, Nakayama M, Ghosh AK, Mitsuya H.  Antimicrob Agents Chemother. 2009 Mar;53(3):997-1006. Epub 2008 Oct 27.</p>

    <p class="title">PMID: 18955518</p> 

    <p class="title"><b>7.     </b> <a href="http://www.ncbi.nlm.nih.gov/pubmed/18952324?ordinalpos=126&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis of alkenyldiarylmethanes (ADAMs) containing benzo[d]isoxazole and oxazolidin-2-one rings, a new series of potent non-nucleoside HIV-1 reverse transcriptase inhibitors.</a>  Deng BL, Zhao Y, Hartman TL, Watson K, Buckheit RW Jr, Pannecouque C, De Clercq E, Cushman M.  Eur J Med Chem. 2009 Mar;44(3):1210-4. Epub 2008 Sep 19.</p>

    <p class="title">PMID: 18952324</p> 

    <p class="title">8.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18722099?ordinalpos=130&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Nebrodeolysin, a novel hemolytic protein from mushroom Pleurotus nebrodensis with apoptosis-inducing and anti-HIV-1 effects.</a>  Lv H, Kong Y, Yao Q, Zhang B, Leng FW, Bian HJ, Balzarini J, Van Damme E, Bao JK.  Phytomedicine. 2009 Mar;16(2-3):198-205. Epub 2008 Aug 21.</p>

    <p class="title">PMID: 18722099</p> 

    <p class="title">9.      <a href="http://www.ncbi.nlm.nih.gov/pubmed/18692274?ordinalpos=135&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Synthesis and in vitro anti-HIV evaluation of a new series of 6-arylmethyl-substituted S-DABOs as potential non-nucleoside HIV-1 reverse transcriptase inhibitors.</a>  Wang YP, Chen FE, De Clercq E, Balzarini J, Pannecouque C.  Eur J Med Chem. 2009 Mar;44(3):1016-23. Epub 2008 Jul 4.</p>

    <p class="title">PMID: 18692274</p> 

    <p class="title">10.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19073606?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Design of Peptide-based Inhibitors for Human Immunodeficiency Virus Type 1 Strains Resistant to T-20.</a>  Izumi K, Kodama E, Shimura K, Sakagami Y, Watanabe K, Ito S, Watabe T, Terakawa Y, Nishikawa H, Sarafianos SG, Kitaura K, Oishi S, Fujii N, Matsuoka M.  J Biol Chem. 2009 Feb 20;284(8):4914-20. Epub 2008 Dec 10.</p>

    <p class="title">PMID: 19073606</p> 

    <p class="title">11.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19106116?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">5&#39;-aminocarbonyl phosphonates as new zidovudine depot forms: antiviral properties, intracellular transformations, and pharmacokinetic parameters.</a>  Khandazhinskaya AL, Yanvarev DV, Jasko MV, Shipitsin AV, Khalizev VA, Shram SI, Skoblov YS, Shirokova EA, Kukhanova MK.  Drug Metab Dispos. 2009 Mar;37(3):494-501. Epub 2008 Dec 23.</p>

    <p class="title">PMID: 19106116</p> 

    <p class="title">12.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19235124?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">A Trypsin-Chymotrypsin Inhibitor with Antiproliferative Activity from Small Glossy Black Soybeans.</a>  Ye X, Bun Ng T.  Planta Med. 2009 Feb 20. [Epub ahead of print]</p>

    <p class="title">PMID: 19235124</p> 

    <p class="title">13.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19235857?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Multivalent interactions with gp120 are required for the anti-HIV activity of cyanovirin.</a>  Liu Y, Carroll JR, Holt LA, Mc Mahon J, Giomarelli B, Ghirlanda G.  Biopolymers. 2009 Feb 23. [Epub ahead of print]</p>

    <p class="title">PMID: 19235857</p> 

    <p class="title">14.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/19245240?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Lycojapodine A, a Novel Alkaloid from Lycopodium japonicum.</a>  He J, Chen XQ, Li MM, Zhao Y, Xu G, Cheng X, Peng LY, Xie MJ, Zheng YT, Wang YP, Zhao QS.  Org Lett. 2009 Feb 26. [Epub ahead of print]</p>

    <p class="title">PMID: 19245240</p> 

    <p class="title">15.  <a href="http://www.ncbi.nlm.nih.gov/pubmed/18687505?ordinalpos=1&amp;itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_DefaultReportPanel.Pubmed_RVDocSum">Predicting anti-HIV activity of 1,3,4-thiazolidinone derivatives: 3D-QSAR approach.</a>  Ravichandran V, Prashantha Kumar BR, Sankar S, Agrawal RK.  Eur J Med Chem. 2009 Mar;44(3):1180-7. Epub 2008 Jun 12.</p>

    <p class="title">PMID: 18687505</p> 

    <p class="memofmt2-2">ISI Web of Science citations:</p> 

    <p class="title">16.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263295200037">Investigation on the role of the tetrazole in the binding of thiotetrazolylacetanilides with HIV-1 wild type and K103N/Y181C double mutant reverse transcriptases</a>.  Gagnon A, Landry S, Coulombe R, Jakalian A, Guse I, Thavonekham B, Bonneau PR, Yoakim C, Simoneau B.  SO BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009;19(4):1199-1205</p>

    <p class="title">  </p>

    <p class="title">17.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263295200019">Synthesis of 5-isoxazol-5-yl-2 &#39;-deoxyuridines exhibiting antiviral activity against HSV and several RNA viruses.</a>  Lee YS, Park SM, Kim BH.  BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS.  2009;19(4):1126-1128. </p> 

    <p class="title">18.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263368800003">Synthesis of Zidovudine Derivatives with Anti-HIV-1 and Antibacterial Activities.</a>  Senthilkumar P, Long J, Swetha R, Shruthi V, Wang RR, Preethi S, Yogeeswari P, Zheng YT, Sriram D.  NUCLEOSIDES NUCLEOTIDES &amp; NUCLEIC ACIDS.  2009;28(2):89-102.</p> 

    <p class="title">19.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263319900013">A facile synthesis of 2,3-disubstituted furo[2,3-b]pyridines.</a>  Beutner GL, Kuethe JT, Yasuda N.  TETRAHEDRON LETTERS.  2009;50(7):781-784. </p> 

    <p class="title">20.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263383600014">A novel and efficient synthesis of chiral C-2-symmetric 1,4-diamines.</a>  Xu LH, Desai MC, Liu HT. TETRAHEDRON LETTERS.  2009;50(5):552-554. </p> 

    <p class="title">21.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263218700002">Routes to HIV-integrase inhibitors: efficient synthesis of bicyclic pyrimidones by ring expansion or amination at a benzylic position.</a>  Ferreira MDR, Cecere G, Pace P, Summa V.  TETRAHEDRON LETTERS.  2009;50(2):148-151.</p> 

    <p class="title">22.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263045700029">BIOLOGICAL ACTIVITY OF METHYL 2-[5-OXO-3,4-DI-(2-PIRYDYL)-1,4,5,6-TETRAHYDRO-1,2,4-TRIAZINE-6-YLIDENE] ACETATE.</a>  Ucherek MM, Wroblewska J, Modzelewska-Banachiewicz B, Gospodarek E.  ACTA POLONIAE PHARMACEUTICA.  2008;65(6):789-791. </p> 

    <p class="title">23.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262980500005">Sidechain-linked inhibitors of HIV-1 protease dimerization.</a>  Bowman MJ, Chmielewski J.  BIOORGANIC &amp; MEDICINAL CHEMISTRY.  2009;17(3):967-976.</p> 

    <p class="title">24.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262931800037">Ring-Opening Polymerization of Benzylated 1,6-Anhydro-beta-D-lactose and Specific Biological Activities of Sulfated (1 -&gt; 6)-alpha-D-Lactopyranans.</a>  Han S, Kanematsu Y, Hattori K, Nakashima H, Yoshida T.  JOURNAL OF POLYMER SCIENCE PART A-POLYMER CHEMISTRY.  2009;47(3):913-924.</p> 

    <p class="title">25.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000262913500037">A New Strategy for the Synthesis of Optically Pure beta-Fluoroalkyl beta-Amino Acid Derivatives.</a>  Fustero S, del Pozo C, Catalan S, Aleman J, Parra A, Marcos V, Ruano JLG.  ORGANIC LETTERS.  2009;11(3):641-644.</p> 

    <p class="title">26.  <a href="http://gateway.isiknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=Alerting&amp;SrcApp=Alerting&amp;DestApp=WOS&amp;DestLinkType=FullRecord;UT=000263098500010">Polymethylene derivatives of nucleic bases with omega-functional groups: VII. Cytotoxicity in the series of N-(2-oxocyclohexyl)-omega-oxoalkyl-substituted purines and pyrimidines.</a>  Komissarov VV, Volgareva GM, Ol&#39;shanskaya YS, Chernyshova ME, Zavalishina LE, Frank GA, Shtil&#39; AA, Kritzyn AM.  RUSSIAN JOURNAL OF BIOORGANIC CHEMISTRY.  2009;35(1):75-85.</p> 

    <p class="title">27.  <a href="http://apps.isiknowledge.com/full_record.do?product=UA&amp;search_mode=GeneralSearch&amp;qid=22&amp;SID=2C2eJMLLjniiE4Jli@a&amp;page=1&amp;doc=1&amp;colname=WOS">Synthesis and Antiviral Evaluation of (-)-3 &#39;-Methylcarbovir, (-)-3 &#39;-Methylabacavir, and Modified Purine Analogues.</a>  Bremond P, Audran G, Monti H, De Clercq E, Pannecouque C.  SYNTHESIS-STUTTGART.  2009;2:290-296. </p> 
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
