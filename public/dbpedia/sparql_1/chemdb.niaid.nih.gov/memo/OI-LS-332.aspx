

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="OI-LS-332.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vm8U+8Ygbf8TQA7A0mE2oG4wT3uVSHIWLrwf/XFYP4eGTEtWfVo2dTX6gHB561/ffgk6Z1vYZfZWTGUD9NphftiUL7HK97fvYYprR5Kj+tEuSjjKBSNcQ3Eie3s=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="964D14FF" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - OI-LS-332-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     47947   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Design, Synthesis, and Biological Activity of m-Tyrosine-Based 16- and 17-Membered Macrocyclic Inhibitors of Hepatitis C Virus NS3 Serine Protease</p>

    <p>          Chen, KX, Njoroge, FG, Pichardo, J, Prongay, A, Butkiewicz, N, Yao, N, Madison, V, and Girijavallabhan, V</p>

    <p>          J Med Chem <b>2005</b>.  48(20): 6229-35</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190750&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16190750&amp;dopt=abstract</a> </p><br />

    <p>2.     47948   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          In Vitro Susceptibility of Mycobacterium tuberculosis Clinical Isolates to Garenoxacin and DA-7867</p>

    <p>          Vera-Cabrera, L, Castro-Garza, J, Rendon, A, Ocampo-Candiani, J, Welsh, O, Choi, SH, Blackwood, K, and Molina-Torres, C</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4351-3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189119&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189119&amp;dopt=abstract</a> </p><br />

    <p>3.     47949   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Inhibition of hepatitis C virus replication by antimonial compounds</p>

    <p>          Hwang, DR, Lin, RK, Leu, GZ, Lin, TY, Lien, TW, Yu, MC, Yeh, CT, and Hsu, JT</p>

    <p>          Antimicrob Agents Chemother <b>2005</b>.  49(10): 4197-202</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189098&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16189098&amp;dopt=abstract</a> </p><br />

    <p>4.     47950   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Determination of in vitro synergy when three antimicrobial agents are combined against Mycobacterium tuberculosis</p>

    <p>          Bhusal, Y, Shiohira, CM, and Yamane, N</p>

    <p>          International Journal of Antimicrobial Agents <b>2005</b>.  26(4): 292-297</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T7H-4H27C0S-1/2/313c07b01d1629c226537550c331bb96">http://www.sciencedirect.com/science/article/B6T7H-4H27C0S-1/2/313c07b01d1629c226537550c331bb96</a> </p><br />

    <p>5.     47951   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Human cytomegalovirus-inhibitory flavonoids: Studies on antiviral activity and mechanism of action</p>

    <p>          Evers, DL, Chao, CF, Wang, X, Zhang, Z, Huong, SM, and Huang, ES</p>

    <p>          Antiviral Res <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188329&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16188329&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>6.     47952   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Viral and cellular RNA helicases as antiviral targets</p>

    <p>          Kwong, AD, Rao, BG, and Jeang, KT</p>

    <p>          Nat Rev Drug Discov <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184083&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16184083&amp;dopt=abstract</a> </p><br />

    <p>7.     47953   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          The discovery of moriniafungin, a novel sordarin derivative produced by Morinia pestalozzioides</p>

    <p>          Basilio, A, Justice, M, Harris, G, Bills, G, Collado, J, de, la Cruz M, Diez, MT, Hernandez, P, Liberator, P, Nielsen, Kahn J, Pelaez, F, Platas, G, Schmatz, D, Shastry, M, Tormo, JR, Andersen, GR, and Vicente, F</p>

    <p>          Bioorg Med Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16183294&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16183294&amp;dopt=abstract</a> </p><br />

    <p>8.     47954   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          The Biosynthesis of Mycolic Acids in Mycobacterium tuberculosis Relies on Multiple Specialized Elongation Complexes Interconnected by Specific Protein-Protein Interactions</p>

    <p>          Veyron-Churlet, Romain, Bigot, Sarah, Guerrini, Olivier, Verdoux, Sebastien, Malaga, Wladimir, Daffe, Mamadou, and Zerbib, Didier</p>

    <p>          Journal of Molecular Biology <b>2005</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WK7-4H5N32X-5/2/230e1e022ee179ed6c14532c58e47881">http://www.sciencedirect.com/science/article/B6WK7-4H5N32X-5/2/230e1e022ee179ed6c14532c58e47881</a> </p><br />

    <p>9.     47955   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Discovery of small-molecule inhibitors of HCV NS3-4A protease as potential therapeutic agents against HCV infection</p>

    <p>          Chen, SH and Tan, SL</p>

    <p>          Curr Med Chem <b>2005</b>.  12(20): 2317-42</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16181135&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16181135&amp;dopt=abstract</a> </p><br />

    <p>10.   47956   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          High-Throughput Cell-Based Screening for Hepatitis C Virus NS3/4A Protease Inhibitors</p>

    <p>          Lee, JC, Yu, MC, Lien, TW, Chang, CF, and Hsu, JT</p>

    <p>          Assay Drug Dev Technol <b>2005</b>.  3(4): 385-392</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180993&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180993&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>11.   47957   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Synthesis and antitubercular activity of 2-hydroxy-aminoalkyl derivatives of diaryloxy methano phenanthrenes</p>

    <p>          Panda, Gautam, Shagufta, Srivastava, Anil K, and Sinha, Sudhir</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4H4T316-2/2/f25971a67253c8006492736d8334055c">http://www.sciencedirect.com/science/article/B6TF9-4H4T316-2/2/f25971a67253c8006492736d8334055c</a> </p><br />

    <p>12.   47958   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Antitubercular Constituents from the Stem Wood of Cinnamomum kotoense</p>

    <p>          Chen, FC, Peng, CF, Tsai, IL, and Chen, IS</p>

    <p>          J Nat Prod <b>2005</b>.  68(9): 1318-23</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180806&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16180806&amp;dopt=abstract</a> </p><br />

    <p>13.   47959   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Rapid, inexpensive MIC determination of Mycobacterium tuberculosis isolates by using microplate nitrate reductase assay</p>

    <p>          Kumar, Manoj, Khan, Inshad Ali, Verma, Vijashoor, Kalyan, Narender, and Qazi, Giulam Nabi</p>

    <p>          Diagnostic Microbiology and Infectious Disease <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T60-4H3Y9PG-4/2/733d6aa1774711f6ccd219900a48e6ba">http://www.sciencedirect.com/science/article/B6T60-4H3Y9PG-4/2/733d6aa1774711f6ccd219900a48e6ba</a> </p><br />

    <p>14.   47960   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Antifungal activity of Lavandula angustifolia essential oil against Candida albicans yeast and mycelial form</p>

    <p>          D&#39;Auria, FD, Tecca, M, Strippoli, V, Salvatore, G, Battinelli, L, and Mazzanti, G</p>

    <p>          Med Mycol <b>2005</b>.  43(5): 391-6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16178366&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16178366&amp;dopt=abstract</a> </p><br />

    <p>15.   47961   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Methyltransferase Erm(37) slips on rRNA to confer atypical resistance in Mycobacterium tuberculosis</p>

    <p>          Madsen, CT, Jakobsen, L, Buriankova, K, Doucet-Populaire, F, Pernodet, JL, and Douthwaite, S</p>

    <p>          J Biol Chem <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174779&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16174779&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>16.   47962   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Solid support synthesis of 6-aryl-2-substituted pyrimidin-4-yl phenols as anti-infective agents</p>

    <p>          Agarwal, A, Srivastava, K, Puri, SK, Sinha, S, and Chauhan, PM</p>

    <p>          Bioorg Med Chem Lett <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16169721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16169721&amp;dopt=abstract</a> </p><br />

    <p>17.   47963   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Simple 1,4-benzoquinones with antibacterial activity from stems and leaves of Gunnera perpensa</p>

    <p>          Drewes, Siegfried E, Khan, Fatima, van Vuuren, Sandy F, and Viljoen, Alvaro M</p>

    <p>          Phytochemistry  <b>2005</b>.  66(15): 1812-1816</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TH7-4GMGWG0-2/2/fba4a2d1d85154af81d4e2e9df583f5c">http://www.sciencedirect.com/science/article/B6TH7-4GMGWG0-2/2/fba4a2d1d85154af81d4e2e9df583f5c</a> </p><br />

    <p>18.   47964   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Hepatitis C virus inhibits intracellular interferon alpha expression in human hepatic cell lines</p>

    <p>          Zhang, T, Lin, RT, Li, Y, Douglas, SD, Maxcey, C, Ho, C, Lai, JP, Wang, YJ, Wan, Q, and Ho, WZ </p>

    <p>          Hepatology <b>2005</b>.  42(4): 819-27</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16175599&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16175599&amp;dopt=abstract</a> </p><br />

    <p>19.   47965   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          Rifapentine, Moxifloxacin or DNA Vaccine Improves Treatment of Latent Tuberculosis in a Mouse Model</p>

    <p>          Nuermberger, E, Tyagi, S, Williams, KN, Rosenthal, I, Bishai, WR, and Grosset, JH</p>

    <p>          Am J Respir Crit Care Med <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16151038&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16151038&amp;dopt=abstract</a> </p><br />

    <p>20.   47966   OI-LS-332; PUBMED-OI-10/3/2005</p>

    <p class="memofmt1-2">          The susceptibility of certain microbial strains to fused 1,2,4-triazole derivatives</p>

    <p>          Sztanke, K, Sidor-Wojtowicz, A, Truchlinska, J, Pasternak, K, and Sztanke, M</p>

    <p>          Ann Univ Mariae Curie Sklodowska [Med] <b>2004</b>.  59(2): 100-3</p>

    <p>           
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16146058&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16146058&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>21.   47967   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Synthesis and anti-viral activity of a series of sesquiterpene lactones and analogues in the subgenomic HCV replicon system</p>

    <p>          Hwang, Der-Ren, Wu, Yu-Shan, Chang, Chun-Wei, Lien, Tzu-Wen, Chen, Wei-Cheng, Tan, Uan-Kang, Hsu, John TA, and Hsieh, Hsing-Pang</p>

    <p>          Bioorganic &amp; Medicinal Chemistry <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF8-4H16NXJ-6/2/d4083b3d549f281b7d96e174c8bcdef8">http://www.sciencedirect.com/science/article/B6TF8-4H16NXJ-6/2/d4083b3d549f281b7d96e174c8bcdef8</a> </p><br />

    <p>22.   47968   OI-LS-332; EMBASE-OI-10/3/2005</p>

    <p class="memofmt1-2">          Broad-spectrum inhibitor of viruses in the Flaviviridae family</p>

    <p>          Ojwang, Joshua O, Ali, Shoukath, Smee, Donald F, Morrey, John D, Shimasaki, Craig D, and Sidwell, Robert W</p>

    <p>          Antiviral Research <b>2005</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4GRH62W-1/2/a8d8400d802d871f10ee8c3b45db5beb">http://www.sciencedirect.com/science/article/B6T2H-4GRH62W-1/2/a8d8400d802d871f10ee8c3b45db5beb</a> </p><br />

    <p>23.   47969   OI-LS-332; WOS-OI-9/25/2005</p>

    <p class="memofmt1-2">          Anti-metabolic activity of caspofungin against Candida albicans and Candida parapsilosis biofilms</p>

    <p>          Cocuaud, C, Rodier, MH, Daniault, G, and Imbert, C</p>

    <p>          JOURNAL OF ANTIMICROBIAL CHEMOTHERAPY <b>2005</b>.  56(3): 507-512, 6</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231692400011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231692400011</a> </p><br />

    <p>24.   47970   OI-LS-332; WOS-OI-9/25/2005</p>

    <p class="memofmt1-2">          Computational chemistry at novartis</p>

    <p>          Lewis, R, Ertl, P, Jacoby, E, Tintelnot-Blomley, M, Gedeck, P, Wolf, RM, and Peitsch, MC</p>

    <p>          CHIMIA <b>2005</b>.  59 (7-8): 545-549, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615300012">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231615300012</a> </p><br />

    <p>25.   47971   OI-LS-332; WOS-OI-9/25/2005</p>

    <p class="memofmt1-2">          Resistance to antituberculous drugs</p>

    <p>          Veziris, N, Cambau, E, Sougakoff, W, Robert, J, and Jarlier, V</p>

    <p>          ARCHIVES DE PEDIATRIE <b>2005</b>.  12: S102-S109, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231770900007">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231770900007</a> </p><br />

    <p>26.   47972   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          Protozoan genomics for drug discovery</p>

    <p>          Chaudhary, K and Roos, DS</p>

    <p>          NATURE BIOTECHNOLOGY <b>2005</b>.  23(9): 1089-1091, 3</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231790600025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231790600025</a> </p><br />
    <br clear="all">

    <p>27.   47973   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          The 19-kDa antigen of Mycobacterium tuberculosis is a major adhesin that binds the mannose receptor of THP-1 monocytic cells and promotes phagocytosis of mycobacteria</p>

    <p>          Diaz-Silvestre, H, Espinosa-Cueto, P, Sanchez-Gonzalez, A, Esparza-Ceron, MA, Pereira-Suarez, AL, Bernal-Fernandez, G, Espitia, C, and Mancilla, R</p>

    <p>          MICROBIAL PATHOGENESIS <b>2005</b>.  39(3): 97-107, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231837100005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231837100005</a> </p><br />

    <p>28.   47974   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          Synthesis of three carbon atom bridged 2,4-diaminopyrrolo[2,3-d]-pyrimidines as nonclassical dihydrofolate reductase inhibitors</p>

    <p>          Gangjee, A, Ye, ZQ, and Queener, SF</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2005</b>.  42(6): 1127-1133, 7</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231871400014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231871400014</a> </p><br />

    <p>29.   47975   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          Preparation and properties of antitubercular 1-piperidino-3-arylthioureas</p>

    <p>          Hearn, MJ, Webster, ER, and Cynamon, MH</p>

    <p>          JOURNAL OF HETEROCYCLIC CHEMISTRY <b>2005</b>.  42(6): 1225-1229, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231871400032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231871400032</a> </p><br />

    <p>30.   47976   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          In-vitro antibacterial, antifungal and cytotoxic activities of some coumarins and their metal complexes</p>

    <p>          Rehman, SU, Chohan, ZH, Gulnaz, F, and Supuran, CT</p>

    <p>          JOURNAL OF ENZYME INHIBITION AND MEDICINAL CHEMISTRY <b>2005</b>.  20(4): 333-340, 8</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231960400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231960400004</a> </p><br />

    <p>31.   47977   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          Antisense phosphorothioate oligonucleotide inhibition of hepatitis C virus genotype 4 replication in HepG2 cells</p>

    <p>          El Awady, MK</p>

    <p>          JOURNAL OF BIOTECHNOLOGY <b>2005</b>.  118: S79-1</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231195200268">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231195200268</a> </p><br />

    <p>32.   47978   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          Suppression of hepatitis C virus replication by cyclosporin A is mediated by blockade of cyclophilins</p>

    <p>          Nakagawa, M, Sakamoto, N, Tanabe, Y, Koyama, T, Itsui, Y, Takeda, Y, Chen, CH, Kakinuma, S, Oooka, S, Maekawa, S, Enomoto, N, and Watanabe, M</p>

    <p>          GASTROENTEROLOGY <b>2005</b>.  129(3): 1031-1041, 11</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231816500028">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231816500028</a> </p><br />
    <br clear="all">

    <p>33.   47979   OI-LS-332; WOS-OI-10/2/2005</p>

    <p class="memofmt1-2">          An efficient synthesis of aryloxyphenyl cyclopropyl methanones: a new class of anti-mycobacterial agents</p>

    <p>          Dwivedi, N, Tewari, N, Tiwari, VK, Chaturvedi, V, Manju, YK, Srivastava, A, Giakwad, A, Sinha, S, and Tripathi, RP</p>

    <p>          BIOORGANIC &amp; MEDICINAL CHEMISTRY LETTERS <b>2005</b>.  15(20): 4526-4530, 5</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231936700025">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000231936700025</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
