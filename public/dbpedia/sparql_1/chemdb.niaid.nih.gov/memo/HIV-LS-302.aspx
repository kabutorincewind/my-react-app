

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="HIV-LS-302.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="pvxhRh4JipTONM6vxqETwDU6utBFnPDTGEc3MqciZ6uiENxLvvkp8fnxcDEuKbVlja4cshtyUfgIppTwn8Pjvpdpdw3H8VzM+bv+PnQOdGQe3k+nRApzSFZRs8c=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F021244A" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<p class="memofmt1-1">ONLINE DATABASE SEARCH - HIV-LS-302-MEMO</p>

<p class="memofmt1-2"> </p>

<p>     1.    43847   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Inhibitory Effects of Some Traditional Medicines on Proliferation of HIV-1 and Its Protease</p>

<p>           Nakamura, N</p>

<p>           Yakugaku Zasshi 2004. 124(8): 519-29</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297721&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297721&amp;dopt=abstract</a> </p><br />

<p>     2.    43848   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Metabolites produced by actinomycetes -antiviral antibiotics and enzyme inhibitors-</p>

<p>           Uyeda, M</p>

<p>           Yakugaku Zasshi 2004. 124(8): 469-79</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297717&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15297717&amp;dopt=abstract</a> </p><br />

<p>     3.    43849   HIV-LS-302; EMBASE-HIV-8/10/2004</p>

<p class="memofmt1-3">           Primer unblocking by HIV-1 reverse transcriptase and resistance to nucleoside RT inhibitors (NRTIs)</p>

<p>           Goldschmidt, Valerie and Marquet, Roland</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. 36(9): 1687-1705</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CBVS67-5/2/9c6a6ada8af67fe7c343416e7211f9f4">http://www.sciencedirect.com/science/article/B6TCH-4CBVS67-5/2/9c6a6ada8af67fe7c343416e7211f9f4</a> </p><br />

<p>     4.    43850   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Screening assay for the identification of deoxyhypusine synthase inhibitors</p>

<p>           Sommer, MN, Bevec, D, Klebl, B, Flicke, B, Holscher, K, Freudenreich, T, Hauber, I, Hauber, J, and Mett, H</p>

<p>           J Biomol Screen 2004. 9(5): 434-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296643&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15296643&amp;dopt=abstract</a> </p><br />

<p>     5.    43851   HIV-LS-302; EMBASE-HIV-8/10/2004</p>

<p class="memofmt1-3">           Nucleoside-analog resistance mutations in HIV-1 reverse transcriptase and their influence on polymerase fidelity and viral mutation rates</p>

<p>           Rezende, Lisa F and Prasad, Vinayaka R</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. 36(9): 1716-1734</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CCNSCT-1/2/938ce7ca91fb2f824fbcae862b6182ac">http://www.sciencedirect.com/science/article/B6TCH-4CCNSCT-1/2/938ce7ca91fb2f824fbcae862b6182ac</a> </p><br />

<p>     6.    43852   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Inhibitor design by wrapping packing defects in HIV-1 proteins</p>

<p>           Fernandez, A, Rogale, K, Scott, R, and Scheraga, HA</p>

<p>           Proc Natl Acad Sci U S A 2004</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15289598&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15289598&amp;dopt=abstract</a> </p><br />

<p>     7.    43853   HIV-LS-302; EMBASE-HIV-8/10/2004</p>

<p class="memofmt1-3">           Concerted inhibitory activities of Phyllanthus amarus on HIV replication in vitro and ex vivo</p>

<p>           Notka, Frank, Meier, Georg, and Wagner, Ralf</p>

<p>           Antiviral Research 2004. In Press, Uncorrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6T2H-4D1MTW2-1/2/056d693b4ebe293c9df208555d351151">http://www.sciencedirect.com/science/article/B6T2H-4D1MTW2-1/2/056d693b4ebe293c9df208555d351151</a> </p><br />

<p>     8.    43854   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Phenotypic impact of HIV reverse transcriptase M184I/V mutations in combination with single thymidine analog mutations on nucleoside reverse transcriptase inhibitor resistance</p>

<p>           Ross, L, Parkin, N, Chappey, C, Fisher, R, Clair, MS, Bates, M, Tisdale, M, and Lanier, ER</p>

<p>           AIDS 2004. 18(12): 1691-6</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280780&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280780&amp;dopt=abstract</a> </p><br />

<p>     9.    43855   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Antiretroviral drug resistance mutations in human immunodeficiency virus type 1 reverse transcriptase increase template-switching frequency</p>

<p>           Nikolenko, GN, Svarovskaia, ES, Delviks, KA, and Pathak, VK</p>

<p>           J Virol 2004. 78(16): 8761-70</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280484&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280484&amp;dopt=abstract</a> </p><br />

<p>   10.    43856   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Spirodiketopiperazine-Based CCR5 Inhibitor Which Preserves CC-Chemokine/CCR5 Interactions and Exerts Potent Activity against R5 Human Immunodeficiency Virus Type 1 In Vitro</p>

<p>           Maeda, K, Nakata, H, Koh, Y, Miyakawa, T, Ogata, H, Takaoka, Y, Shibayama, S, Sagawa, K, Fukushima, D, Moravek, J, Koyanagi, Y, and Mitsuya, H</p>

<p>           J Virol 2004. 78(16): 8654-62</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280474&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280474&amp;dopt=abstract</a> </p><br />

<p>   11.    43857   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Initial Cleavage of the Human Immunodeficiency Virus Type 1 GagPol Precursor by Its Activated Protease Occurs by an Intramolecular Mechanism</p>

<p>           Pettit, SC, Everitt, LE, Choudhury, S, Dunn, BM, and Kaplan, AH</p>

<p>           J Virol 2004. 78(16): 8477-85</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280456&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15280456&amp;dopt=abstract</a> </p><br />

<p>   12.    43858   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           The challenge of antiretroviral-drug-resistant HIV: is there any possible clinical advantage?</p>

<p>           Zaccarelli, M, Tozzi, V, Perno, CF, and Antinori, A</p>

<p>           Curr HIV Res 2004. 2(3): 283-92</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279592&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15279592&amp;dopt=abstract</a> </p><br />

<p>   13.    43859   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           From the Cover: A naphthyridine carboxamide provides evidence for discordant resistance between mechanistically identical inhibitors of HIV-1 integrase</p>

<p>           Hazuda, DJ, Anthony, NJ, Gomez, RP, Jolly, SM, Wai, JS, Zhuang, L, Fisher, TE, Embrey, M, Guare, JP Jr, Egbertson, MS, Vacca, JP, Huff, JR, Felock, PJ, Witmer, MV, Stillmock, KA, Danovich, R, Grobler, J, Miller, MD, Espeseth, AS, Jin, L, Chen, IW, Lin, JH, Kassahun, K, Ellis, JD, Wong, BK, Xu, W, Pearson, PG, Schleif, WA, Cortese, R, Emini, E, Summa, V, Holloway, MK, and Young, SD</p>

<p>           Proc Natl Acad Sci U S A 2004. 101(31): 11233-8</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277684&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15277684&amp;dopt=abstract</a> </p><br />

<p>   14.    43860   HIV-LS-302; EMBASE-HIV-8/10/2004</p>

<p class="memofmt1-3">           A polymeric immunoglobulin receptor-like milk protein with inhibitory activity on human immunodeficiency virus type 1 reverse transcriptase</p>

<p>           Ng, TB and Ye, XY</p>

<p>           The International Journal of Biochemistry &amp; Cell Biology 2004. In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6TCH-4CVX2KS-1/2/958ddeb20d03be20e2a5378cbb8b08db">http://www.sciencedirect.com/science/article/B6TCH-4CVX2KS-1/2/958ddeb20d03be20e2a5378cbb8b08db</a> </p><br />

<p>   15.    43861   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Sulfated polymannuroguluronate, a novel anti-acquired immune deficiency syndrome (AIDS) drug candidate, targeting CD4 in lymphocytes</p>

<p>           Miao, B, Geng, M, Li, J, Li, F, Chen, H, Guan, H, and Ding, J</p>

<p>           Biochem Pharmacol 2004. 68(4): 641-9</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15276071&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15276071&amp;dopt=abstract</a> </p><br />

<p>   16.    43862   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Zidovudine, lamivudine, and abacavir have different effects on resting cells infected with human immunodeficiency virus in vitro</p>

<p>           Saavedra-Lozano, J, McCoig, CC, Cao, Y, Vitetta, ES, and Ramilo, O</p>

<p>           Antimicrob Agents Chemother 2004.  48(8): 2825-30</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273087&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15273087&amp;dopt=abstract</a> </p><br />

<p>   17.    43863   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Efficient 3D Database Screening for Novel HIV-1 IN Inhibitors</p>

<p>           Barreca, ML, Rao, A, De Luca, L, Zappala, M, Gurnari, C, Monforte, P, De Clercq, E, Van Maele, B, Debyser, Z, Witvrouw, M, Briggs, JM, and Chimirri, A</p>

<p>           J Chem Inf Comput Sci 2004. 44(4): 1450-1455</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272853&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15272853&amp;dopt=abstract</a> </p><br />

<p>   18.    43864   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Systemic and mucosal immunity in rhesus macaques immunized with HIV-1 peptide and gp120 conjugated to Brucella abortus</p>

<p>           Eller, N, Golding, H, Inoue, S, Beining, P, Inman, J, Matthews, N, Scott, DE, and Golding, B</p>

<p>           J Med Primatol 2004. 33(4): 167-174</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15271066&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15271066&amp;dopt=abstract</a> </p><br />

<p>   19.    43865   HIV-LS-302; EMBASE-HIV-8/10/2004</p>

<p class="memofmt1-3">           Impact of frequent natural polymorphisms at the protease gene on the in vitro susceptibility to protease inhibitors in HIV-1 non-B subtypes</p>

<p>           Holguin, Africa, Paxinos, Ellen, Hertogs, Kurt, Womac, Chad, and Soriano, Vincent</p>

<p>           Journal of Clinical Virology 2004.  In Press, Corrected Proof</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.sciencedirect.com/science/article/B6VJV-4CF3C93-1/2/98086dc01c8d6eaf303cd4fcc7c51bc1">http://www.sciencedirect.com/science/article/B6VJV-4CF3C93-1/2/98086dc01c8d6eaf303cd4fcc7c51bc1</a> </p><br />

<p>        </p>

<p>  20.     43866   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           New Antiretroviral Agents for the Treatment of HIV Infection</p>

<p>           Marks, K and Gulick, RM</p>

<p>           Curr Infect Dis Rep 2004. 6(4): 333-339</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15265463&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15265463&amp;dopt=abstract</a> </p><br />

<p>   21.    43867   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Drug resistance mutations and newly recognized treatment-related substitutions in the HIV-1 protease gene: prevalence and associations with drug exposure and real or virtual phenotypic resistance to protease inhibitors in two clinical cohorts of antiretroviral experienced patients</p>

<p>           Torti, C, Quiros-Roldan, E, Monno, L, Patroni, A, Saracino, A, Angarano, G, Tinelli, C, Lo, Caputo S, Tirelli, V, Mazzotta, F, and Carosi, G</p>

<p>           J Med Virol 2004. 74(1): 29-33</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15258965&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15258965&amp;dopt=abstract</a> </p><br />

<p>   22.    43868   HIV-LS-302; PUBMED-HIV-8/10/2004</p>

<p class="memofmt1-3">           Antiretroviral and immunosuppressive drug-drug interactions: An update</p>

<p>           Izzedine, H,  Launay-Vacher, V, Baumelou, A, and Deray, G</p>

<p>           Kidney Int 2004. 66(2): 532-541</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15253704&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=15253704&amp;dopt=abstract</a> </p><br />

<p>   23.    43869   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Tryptophan scanning mutagenesis of aromatic residues within the polymerase domain of HIV-l reverse transcriptase: critical role of Phe-130 for p51 function and second-site revertant restoring viral replication capacity</p>

<p>           Olivares, I, Gutierrez-Rivas, M, Lopez-Galindez, C, and Menendez-Arias, L</p>

<p>           VIROLOGY 2004. 324(2): 400-411</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222376800014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222376800014</a> </p><br />

<p>   24.    43870   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Preparation and pharmacokinetics of C-11 labeled stavudine (d4T)</p>

<p>           Livni, E, Berker, M, Hillier, S, Waller, SC, Ogan, MD, Discordia, RP, Rienhart, JK, Rubin, RH, and Fischman, AJ</p>

<p>           NUCL MED BIOL  2004. 31(5): 613-621</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222425900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222425900011</a> </p><br />

<p>   25.    43871   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Elevated expression of GM3 in receptor-bearing targets confers resistance to human immunodeficiency virus type 1 fusion</p>

<p>           Rawat, SS, Gallo, SA, Eaton, J, Martin, TD, Ablan, S, KewalRamani, VN, Wang, JM, Blumenthal, R, and Puri, A</p>

<p>           J VIROL 2004. 78(14): 7360-7368</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222407200008">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222407200008</a> </p><br />

<p>   26.    43872   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Determinants of human immunodeficiency virus type 1 baseline susceptibility to the fusion inhibitors enfuvirtide and T-649 reside outside the peptide interaction site</p>

<p>           Heil, ML, Decker, JM, Sfakianos, JN, Shaw, GM, Hunter, E, and Derdeyn, CA</p>

<p>           J VIROL 2004. 78(14): 7582-7589</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222407200032">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222407200032</a> </p><br />

<p>   27.    43873   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Recent advances in the development of next generation non-nucleoside reverse transcriptase inhibitors</p>

<p>           Tarby, CM</p>

<p>           CURR TOP MED CHEM 2004. 4(10): 1045-1057</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222455200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222455200004</a> </p><br />

<p>   28.    43874   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           Ampelopsin, a small molecule inhibitor of HIV-1 infection targeting HIV entry</p>

<p>           Liu, DY, Ye, JT, Yang, WH, Yan, J, Zeng, CH, and Zeng, S</p>

<p>           BIOMED ENVIRON SCI 2004. 17(2): 153-164</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222414600004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222414600004</a> </p><br />

<p>   29.    43875   HIV-LS-302; WOS-HIV-8/2/2004</p>

<p class="memofmt1-3">           gp340 (SAG) binds to the V3 sequence of gp120 important for chemokine receptor interaction</p>

<p>           Wu, ZW, Golub, E, Abrams, WR, and Malamud, D</p>

<p>           AIDS RES HUM RETROV 2004. 20(6): 600-607</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222347000005">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222347000005</a> </p><br />

<p>   30.    43876   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Cyclophilin A retrotransposition into TRIM5 explains owl monkey resistance to HIV-1</p>

<p>           Sayah, DM, Sokolskaja, E, Berthoux, L, and Luban, J</p>

<p>           NATURE 2004. 430(6999): 569-573</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222946100050">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222946100050</a> </p><br />

<p>   31.    43877   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Camelized rabbit-derived VH single-domain Intrabodies Against vif strongly neutralize HIV-1 infectivity</p>

<p>           da Silva, FA, Santa-Marta, M, Freitas-Vieira, A, Mascarenhas, P, Barahona, I, Moniz-Pereira, J, Gabuzda, D, and Goncalves, J</p>

<p>           J MOL BIOL 2004. 340(3): 525-542</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222560000011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222560000011</a> </p><br />

<p>   32.    43878   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Initiation of antiretroviral therapy during recent HIV-1 infection results in lower residual viral reservoirs</p>

<p>           Pires, A, Hardy, G, Gazzard, B, Gotch, F, and Imami, N</p>

<p>           JAIDS-J ACQ IMM DEF 2004. 36(3): 783-790</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222481700003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222481700003</a> </p><br />

<p>   33.    43879   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Chloroquine and hydroxychloroquine as inhibitors of human immunodeficiency virus (HIV-1) activity</p>

<p>           Romanelli, F, Smith, KM, and Hoven, AD</p>

<p>           CURR PHARM DESIGN 2004. 10(21): 2643-2648</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222539600009">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222539600009</a> </p><br />

<p>   34.    43880   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Increased expression of Toll-like receptor 2 on monocytes in HIV infection: Possible roles in inflammation and viral replication</p>

<p>           Heggelund, L, Muller, F, Lien, E, Yndestad, A, Ueland, T, Kristiansen, KI, Espevik, T, Aukrust, P, and Froland, SS</p>

<p>           CLIN INFECT DIS 2004. 39(2): 264-269</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222474300018">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222474300018</a> </p><br />

<p>        </p>

<p>  35.     43881   HIV-LS-302; WOS-HIV-8/8/2004</p>

<p class="memofmt1-3">           Intracellular expression of antisense RNA. transcripts complementary to the human immunodeficiency virus type-1 vif gene inhibits viral replication in infected T-lymphoblastoid cells</p>

<p>           Barnor, JS, Miyano-Kurosaki, N, Yamaguchi, K, Sakamoto, A, Ishikawa, K, Inagaki, Y, Yamamoto, N, Osei-Kwasi, M, Ofori-Adjei, D, and Takaku, H</p>

<p>           BIOCHEM BIOPH RES CO 2004. 320(2):  544-550</p>

<p>HYPERLINK:&nbsp;&nbsp;<a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222567700040">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000222567700040</a> </p><br />
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
