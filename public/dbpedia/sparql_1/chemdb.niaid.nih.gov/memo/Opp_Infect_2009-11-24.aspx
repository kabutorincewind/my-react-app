

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="Opp_Infect_2009-11-24.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RtBFjaEV4bTJzh63LKvoQfM4WJZk6OCj4WuwRlEsOnoYmqAmcpFWBi/UrIIvFlPAVh0x23UD5C5WxKwXZdYT6n8S/bSjf/ecxAlNf7Q57loju/fT/7UNiyxNjQQ=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="C79C801E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopage">
	
<div class="Section1">
    <p class="memofmt2-h1">Opportunistic Infections  Citation List: </p>

    <p class="memofmt2-h1">November 11, 2009 -November 24, 2009</p> 

    <p class="memofmt2-2">ANTIFUNGAL COMPOUNDS ( includes Cryptococcus, Aspergillus, Candida, &amp; Histoplasma)</p>

    <p class="ListParagraph">1. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19924565?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=16">In Vitro Investigation of Antifungal Activity of Allicin Alone and in Combination with Azoles Against Candida Species.</a></p>

    <p class="NoSpacing">Khodavandi A, Alizadeh F, Aala F, Sekawi Z, Chong PP.</p>

    <p class="NoSpacing">Mycopathologia. 2009 Nov 19. [Epub ahead of print]PMID: 19924565 [PubMed - as supplied by publisher]</p>

    <p class="ListParagraph">2. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19927310?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=12">Fungicide Activity of 5-(4-Chlorobenzylidene)-(Z)-2-dimethylamino-1,3-thiazol-4-one against Cryptococcus Neoformans.</a></p>

    <p class="NoSpacing">Insuasty B, Gutiérrez A, Quiroga J, Abonia R, Nogueras M, Cobo J, Svetaz L, Raimondi M, Zacchino S.</p>

    <p class="NoSpacing">Arch Pharm (Weinheim). 2009 Nov 19. [Epub ahead of print]PMID: 19927310 [PubMed - as supplied by publisher]</p> 

    <p class="memofmt2-2">ANTIBACTERIAL COMPOUNDS (includes Mycobacterium spp.,  Drug Resistant, MAC, MDR)</p> 

    <p class="ListParagraph">3. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19921681?itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum&amp;ordinalpos=30">2-Aryl-3-(1H-Azol-1-yl)-1H-Indole Derivatives: A New Class of Antimycobacterial Compounds - Conventional Heating in Comparison with MW-Assisted Synthesis.</a></p>

    <p class="NoSpacing">Zampieri, D., M.G. Mamolo, E. Laurini, G. Scialino, E. Banfi, and L. Vio</p>

    <p class="NoSpacing">Arch Pharm (Weinheim), 2009. 342(12): p. 716-722; PMID[19921681]</p>

    <p class="memofmt2-2">Citations from the ISI Web of  Knowledge Listings for  O.I.</p>  

    <p class="ListParagraph">4. Maddry, JA, et al.</p>

    <p class="NoSpacing">Antituberculosis activity of the molecular libraries screening center network library</p>

    <p class="NoSpacing">TUBERCULOSIS  2009 (September) 89: 354 - 363</p> 

    <p class="ListParagraph">5. Budha, NR, et al.</p>

    <p class="NoSpacing">A simple in vitro PK/PD model system to determine time-kill curves of drugs against Mycobacteria</p>

    <p class="NoSpacing">TUBERCULOSIS   2009 (September) 89: 378 - 385</p> 

    <p class="ListParagraph">6. Dolezal, M, et al.</p>

    <p class="NoSpacing">Substituted N-Phenylpyrazine-2-carboxamides: Synthesis and Antimycobacterial Evaluation</p>

    <p class="NoSpacing">MOLECULES   2009 (October) 14: 4180 - 4189</p> 

    <p class="ListParagraph">7. Bijev, A, et al.</p>

    <p class="NoSpacing">New Pyrrole Hydrazones Synthesized and Evaluated In Vitro as Potential Tuberculostatics</p>

    <p class="NoSpacing">LETTERS IN DRUG DESIGN &amp; DISCOVERY   2009 (October) 6: 508 - 517</p> 

    <p class="ListParagraph">8. Moro, AC, et al.</p>

    <p class="NoSpacing">Antitumor and antimycobacterial activities of cyclopalladated complexes: X-ray structure of [Pd(C-2,N-dmba)(Br)(tu)] (dmba =N,N-dimethylbenzylamine, tu = thiourea)</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY   2009 (Nov.) 44: 4611 - 4615</p> 

    <p class="ListParagraph">9. Biava, M, et al.</p>

    <p class="NoSpacing">1,5-Diaryl-2-ethyl pyrrole derivatives as antimycobacterial agents: Design, synthesis, and microbiological evaluation</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (Nov.) 44: 4734 - 4738</p> 

    <p class="ListParagraph">10. Mallikarjuna, BP, et al.</p>

    <p class="NoSpacing">Synthesis of new 4-isopropylthiazole hydrazide analogs and some derived clubbed triazole, oxadiazole ring systems - A novel class of potential antibacterial, antifungal and antitubercular agents</p>

    <p class="NoSpacing">EUROPEAN JOURNAL OF MEDICINAL CHEMISTRY  2009 (Nov.) 44: 4739 - 4746</p> 

    <p class="ListParagraph">11. Baser, K.H.C., M. Kurkcuoglu, T. Askun, and G. Tumen</p>

    <p class="NoSpacing">Anti-tuberculosis Activity of Daucus littoralis Sibth. et Sm. (Apiaceae) From Turkey.</p>

    <p class="NoSpacing">Journal of Essential Oil Research, 2009. 21(6): p. 572-575; PMID[ISI:000271408400026]</p> 

    <p class="ListParagraph">12. Carta, F., A. Maresca, A.S. Covarrubias, S.L. Mowbray, T.A. Jones, and C.T. Supuran</p>

    <p class="NoSpacing">Carbonic anhydrase inhibitors. Characterization and inhibition studies of the most active beta-carbonic anhydrase from Mycobacterium tuberculosis, Rv3588c.</p>

    <p class="NoSpacing">Bioorganic &amp; Medicinal Chemistry Letters, 2009. 19(23): p. 6649-6654; PMID[ISI:000271430400034]</p> 

    <p class="ListParagraph">13. Caldas, LA, et al.</p>

    <p class="NoSpacing">Dynamin inhibitor impairs Toxoplasma gondii invasion</p>

    <p class="NoSpacing">FEMS MICROBIOLOGY LETTERS  2009 (Dec.) 301: 103 - 108</p>  
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
