

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Division of AIDS Anti-HIV/OI/TB Therapeutics Database - Surveillance Memo</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/styles.css" type="text/css"></link>
	<!--[if lte IE 7]>
        <link rel="stylesheet" href="/css/iecss.css" type="text/css"></link>
	<![endif]-->
	<script type="text/JavaScript" src="/js/ClientFunctions.js"></script>
</head>

<body onload="Init();">
<form name="aspnetForm" method="post" action="dmid-ls-110.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Z0ZKUVEcA2Y6iKafIaqHkDSvGpjWuy1DmQKjoMNktOx6Y3XSi1etvGEjiUDHUsVdupVqAhrG3x3lWHIB6pK5bBKkceBDjd5tFxisAZDpjkjXLaxAZwD9vUxcdRY=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="14D2137E" />
<div id="bodycontent">
	<a href="#skipnav" class="hidden">Skip to Main Content</a>
	<a name="TOP"></a>
    <div id="niaid_header_links">
        <a href="https://www.hhs.gov/" target="_blank">U.S. Department of Health and Human Services </a>• <a href="https://www.nih.gov/" target="_blank">National Institutes of Health</a>
    </div>
    	
	<div id="niaid_header"><a name="top" class="hidden"></a>
		<a href="https://www.niaid.nih.gov/" target="_blank"><img src="../images/niaidHeader.png" alt="NIAID Banner Logo Image" /></a>
	</div>

    <div id="navwrapper">
	    <div id="header">
    		<h1><a href="">Division of AIDS Anti-HIV/OI/TB Therapeutics Database</a></h1>
	    </div>
    	<div id="sidenav">
	    	<p class="sidenav_head1"><a href="../SimpleSearch.aspx">HOME</a></p>
		    <p class="sidenav_head2"> About ChemDB</p>			
			    <ul class="sidenav_bullets">
        			<li><a href="../AboutChemDB.aspx">About ChemDB</a></li>
        			<li><a href="../help/UserGuide.aspx">User Guide</a></li>
		    		<li><a href="Highlights.aspx">Announcements</a> <img src="../images/new_item.gif" alt="New Item" /></li>    			
    			</ul>
	    	<p class="sidenav_head2"> Advanced Search</p>
		    	<ul class="sidenav_bullets">
			    	<li><a href="../CompoundSearch.aspx?v=C">Chemical</a></li>
				    <li><a href="../CompoundSearch.aspx?v=B">Biological</a></li>
    				<li><a href="../LitSearch.aspx">Literature</a></li>
	    		</ul>
		    <p class="sidenav_head2"> Drugs in Development</p>
			    <ul class="sidenav_bullets">		
    				<li><a href="../DrugDevelopmentHIV.aspx">HIV</a></li>
    				<li><a href="../DrugDevelopmentTB.aspx" id="ctl00_A1">TB</a></li>	
	    			<li><a href="../DrugDevelopmentHCV.aspx">HCV (Archived)</a></li>		    			
		    		<li><a href="../DrugDevelopmentHerpes.aspx">Herpes (Archived)</a></li>		
		    		<li><a href="../DrugDevelopmentProto.aspx">Protozoal (Archived)</a></li>		
		    	    <li><a href="../DrugDevelopmentFungal.aspx">Fungal (Archived)</a></li>		
			    </ul>				
		    <p class="sidenav_head2"><a href="memos.aspx"> Surveillance Memos</a></p>
		    <p class="sidenav_head2"> Other Links</p>
		        <ul class="sidenav_bullets">
                    <li><a href="https://www.niaid.nih.gov/about/daids" target="_blank">NIAID/Division of AIDS Home Page</a></li>
                    <li><a href="https://chem.nlm.nih.gov/chemidplus/" target="_blank">NLM/ChemID Plus<br />Chemical Database</a></li>
                    <li><a href="https://dtp.cancer.gov/organization/dscb/obtaining/default.htm" target="_blank">NCI/DTP Open Chemical Repository</a></li>
                    <li><a href="http://xpdb.nist.gov/hiv2_d/hivsdb.html" target="_blank">NIST HIV Structural Database</a></li>               
                    <li><a href="https://www.nih.gov/" target="_blank">NIH Home Page</a></li>     
                </ul>
	    </div> <!-- sidenav -->
    </div> <!-- navwrapper -->
    <div id="contentwrapper">	
        <a name="skipnav"></a>
	    

<div id="content">

	<div id="memopagels">
	
<div class="Section1">
    <p class="memofmt1-1">ONLINE DATABASE SEARCH - DMID-LS-110-MEMO</p>

    <p class="memofmt1-1"> </p>

    <p>1.     58132   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Host sphingolipid biosynthesis as a target for hepatitis C virus therapy</p>

    <p>          Sakamoto, H, Okamoto, K, Aoki, M, Kato, H, Katsume, A, Ohta, A, Tsukuda, T, Shimma, N, Aoki, Y, Arisawa, M, Kohara, M, and Sudoh, M</p>

    <p>          Nat Chem Biol <b>2005</b>.  1(6): 333-337</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16408072&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16408072&amp;dopt=abstract</a> </p><br />

    <p>2.     58133   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Anti-Hcv Activity of N-9,5 &#39;-Cyclo-3-(Beta-D-Ribofuranosyl)-8-Azapurin-2-One Derivatives</p>

    <p>          Hassan, A. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1531-1542</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900010">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900010</a> </p><br />

    <p>3.     58134   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of 9-[1-(Substituted)-3-(Phosphonomethoxy)Propyl]Adenine Derivatives as Possible Antiviral Agents</p>

    <p>          Wu, M. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1543-1568</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900011">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900011</a> </p><br />

    <p>4.     58135   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of N-6-Substituted 9-[3-(Phosphonomethoxy)Propyl]Adenine Derivatives as Possible Antiviral Agents</p>

    <p>          El-Kattan, Y. <i> et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1597-1611</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900014">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900014</a> </p><br />

    <p>5.     58136   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and screening of bicyclic carbohydrate-based compounds: A novel type of antivirals</p>

    <p>          Hoof, SV, Ruttens, B, Hubrecht, I, Smans, G, Blom, P, Sas, B, Van Hemel, J, Vandenkerckhove, J, and Van der Eycken, J</p>

    <p>          Bioorg Med Chem Lett <b>2006</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16403628&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16403628&amp;dopt=abstract</a> </p><br />

    <p>6.     58137   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Isatoribine - Anti-Hepatitis C Virus Drug Tlr7 Receptor Agonist</p>

    <p>          Davies, S., Castaner, J., and Fernandez, D.</p>

    <p>          Drugs of the Future <b>2005</b>.  30(9): 886-891</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234133100003">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234133100003</a> </p><br />
    <br clear="all">

    <p>7.     58138   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Effect of metal complexes of acyclovir and its acetylated derivative on Herpes simplex virus 1 and Herpes simplex virus 2 replication</p>

    <p>          Varadinova, T, Vilhelmova, N, Badenas, F, Terron, A, Fiol, J, Garcia-Raso, A, and Genova, P</p>

    <p>          Acta Virol <b>2005</b>.  49(4): 251-260</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16402682&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16402682&amp;dopt=abstract</a> </p><br />

    <p>8.     58139   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibition of hepatitis B virus replication by recombinant small interfering RNAs</p>

    <p>          Wu, K, Gong, Y, Zhang, X, Zhang, Y, Mu, Y, Liu, F, Song, D, Zhu, Y, and Wu, J</p>

    <p>          Acta Virol <b>2005</b>.  49(4): 235-241</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16402680&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16402680&amp;dopt=abstract</a> </p><br />

    <p>9.     58140   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Searching for More Effective Hcvns3 Protease Inhibitors Via Modification of Corilagin</p>

    <p>          Wang, Y. <i>et al.</i></p>

    <p>          Progress in Natural Science <b>2005</b>.  15(10): 896-901</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234007400004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234007400004</a> </p><br />

    <p>10.   58141  DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Five-Membered Iminocyclitol Derivatives as Selective and Potent Glycosidase Inhibitors: New Structures for Antivirals and Osteoarthritis</p>

    <p>          Liang, PH, Cheng, WC, Lee, YL, Yu, HP, Wu, YT, Lin, YL, and Wong, CH</p>

    <p>          Chembiochem <b>2006</b>.  7(1): 165-173</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16397876&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16397876&amp;dopt=abstract</a> </p><br />

    <p>11.   58142   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Evaluation of Antiviral Activity of Compounds Isolated from Ranunculus sieboldii and Ranunculus sceleratus</p>

    <p>          Li, H, Zhou, C, Pan, Y, Gao, X, Wu, X, Bai, H, Zhou, L, Chen, Z, Zhang, S, Shi, S, Luo, J, Xu, J, Chen, L, Zheng, X, and Zhao, Y</p>

    <p>          Planta Med <b>2005</b>.  71(12): 1128-1133</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16395649&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16395649&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>12.   58143   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Biological Evaluation of 6-(Alkyn-1-yl)furo[2,3-d]pyrimidin-2(3H)-one Base and Nucleoside Derivatives(1)</p>

    <p>          Robins, MJ, Miranda, K, Rajwanshi, VK, Peterson, MA, Andrei, G, Snoeck, R, De Clercq, E, and Balzarini, J</p>

    <p>          J Med Chem <b>2006</b>.  49(1): 391-398</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392824&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392824&amp;dopt=abstract</a> </p><br />

    <p>13.   58144   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Evaluation of Cyclic and Acyclic 2-Methyl-3-hydroxy-4-pyridinone Nucleoside Derivatives</p>

    <p>          Barral, K, Balzarini, J, Neyts, J, De Clercq, E, Hider, RC, and Camplo, M</p>

    <p>          J Med Chem <b>2006</b>.  49(1): 43-50</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392791&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16392791&amp;dopt=abstract</a> </p><br />

    <p>14.   58145   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Inhibitory Effect of Rnai on Japanese Encephalitis Virus Replication in Vitro and in Vivo</p>

    <p>          Murakami, M. <i>et al.</i></p>

    <p>          Microbiology and Immunology <b>2005</b>.  49(12): 1047-1056</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233880200004">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233880200004</a> </p><br />

    <p>15.   58146   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel Antiviral Strategies to Combat Human Arenavirus Infections</p>

    <p>          Kunz, S. and De La Torre, J.</p>

    <p>          Current Molecular Medicine <b>2005</b>.  5(8): 735-751</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233918400002">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000233918400002</a> </p><br />

    <p>16.   58147   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis and Antiviral Activity of Some 2-Substituted 3-Formyl-and 3-Cyano-5,6-Dichloroindole Nucleosides</p>

    <p>          Williams, J., Drach, J., and Townsend, L.</p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1613-1626</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900015">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900015</a> </p><br />

    <p>17.   58148   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Avian influenza: an omnipresent pandemic threat</p>

    <p>          Perez, DR, Sorrell, EM, and Donis, RO</p>

    <p>          Pediatr Infect Dis J <b>2005</b>.  24(11 Suppl): S208-S216, discussion S215</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378048&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16378048&amp;dopt=abstract</a> </p><br />
    <br clear="all">

    <p>18.   58149   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Phosphoralaninate Pronucleotides of Pyrimidine Methylenecyclopropane Analogues of Nucleosides: Synthesis and Antiviral Activity</p>

    <p>          Ambrose, A. <i>et al.</i></p>

    <p>          Nucleosides Nucleotides &amp; Nucleic Acids <b>2005</b>.  24(10-12): 1763-1774</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900023">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234060900023</a> </p><br />

    <p>19.   58150   DMID-LS-110; WOS-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Neuraminidase as a Target for Drugs for the Treatment of Influenza</p>

    <p>          Liu, A., Wang, Y., and Du, G.</p>

    <p>          Drugs of the Future <b>2005</b>.  30(8): 799-806</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234154400006">http://publishorperish.nih.gov/Gateway.cgi?KeyUT=000234154400006</a> </p><br />

    <p>20.   58151   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Type I interferon inhibits Crimean-Congo hemorrhagic fever virus in human target cells</p>

    <p>          Andersson, I,  Lundkvist, A, Haller, O, and Mirazimi, A</p>

    <p>          J Med Virol <b>2006</b>.  78(2): 216-222</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16372299&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16372299&amp;dopt=abstract</a> </p><br />

    <p>21.   58152   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Oseltamivir resistance during treatment of influenza A (H5N1) infection</p>

    <p>          de Jong, MD, Tran, TT, Truong, HK, Vo, MH, Smith, GJ, Nguyen, VC, Bach, VC, Phan, TQ, Do, QH, Guan, Y, Peiris, JS, Tran, TH, and Farrar, J</p>

    <p>          N Engl J Med <b>2005</b>.  353(25): 2667-2672</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371632&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16371632&amp;dopt=abstract</a> </p><br />

    <p>22.   58153   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis, biological activity, and preliminary pharmacokinetic evaluation of analogues of a phosphosulfomannan angiogenesis inhibitor (PI-88)</p>

    <p>          Karoli, T, Liu, L, Fairweather, JK, Hammond, E, Li, CP, Cochran, S, Bergefall, K, Trybala, E, Addison, RS, and Ferro, V</p>

    <p>          J Med Chem <b>2005</b>.  48(26): 8229-8236</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16366604&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16366604&amp;dopt=abstract</a> </p><br />

    <p>23.   58154   DMID-LS-110; PUBMED-DMID-1/17/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          The face of future hepatitis C antiviral drug development: Recent biological and virologic advances and their translation to drug development and clinical practice</p>

    <p>          McHutchison, JG, Bartenschlager, R, Patel, K, and Pawlotsky, JM</p>

    <p>          J Hepatol <b>2005</b>.</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364491&amp;dopt=abstract">http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&amp;db=PubMed&amp;list_uids=16364491&amp;dopt=abstract</a> </p><br />

    <p>24.   58155   DMID-LS-110; EMBASE-DMID-1/18/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel inhibitors of hepatitis C NS3-NS4A serine protease derived from 2-aza-bicyclo[2.2.1]heptane-3-carboxylic acid</p>

    <p>          Venkatraman, Srikanth, Njoroge, FGeorge, Wu, Wanli, Girijavallabhan, Viyyoor, Prongay, Andrew J, Butkiewicz, Nancy, and Pichardo, John</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4J2KTBF-3/2/da0b8547c32e3db760a55f36ad27c7b1">http://www.sciencedirect.com/science/article/B6TF9-4J2KTBF-3/2/da0b8547c32e3db760a55f36ad27c7b1</a> </p><br />

    <p>25.   58156   DMID-LS-110; EMBASE-DMID-1/18/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Searching for a new anti-HCV therapy: Synthesis and properties of tropolone derivatives</p>

    <p>          Boguszewska-Chachulska, Anna M, Krawczyk, Mariusz, Najda, Andzelika, Kopanska, Katarzyna, Stankiewicz-Drogon, Anna, Zagorski-Ostoja, Wlodzimierz, and Bretner, Maria</p>

    <p>          Biochemical and Biophysical Research Communications <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6WBK-4J2CMTG-6/2/391fe977bbb3542e84475ffa4243440e">http://www.sciencedirect.com/science/article/B6WBK-4J2CMTG-6/2/391fe977bbb3542e84475ffa4243440e</a> </p><br />

    <p>26.   58157   DMID-LS-110; EMBASE-DMID-1/18/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Identification and analysis of fitness of resistance mutations against the HCV protease inhibitor SCH 503034</p>

    <p>          Tong, Xiao, Chase, Robert, Skelton, Angela, Chen, Tong, Wright-Minogue, Jackie, and Malcolm, Bruce A</p>

    <p>          Antiviral Research <b>2006</b>.  In Press, Uncorrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6T2H-4J1JFFY-1/2/36f05a3cc9605ac85f561328483e6540">http://www.sciencedirect.com/science/article/B6T2H-4J1JFFY-1/2/36f05a3cc9605ac85f561328483e6540</a> </p><br />

    <p>27.   58158   DMID-LS-110; EMBASE-DMID-1/18/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Synthesis of methylenecyclopropane analogues of antiviral nucleoside phosphonates</p>

    <p>          Yan, Zhaohua, Zhou, Shaoman, Kern, Earl R, and Zemlicka, Jiri</p>

    <p>          Tetrahedron <b>2006</b>.  In Press, Corrected Proof</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6THR-4J2KT79-3/2/423a77ce44f17675604096e4fb9d8c49">http://www.sciencedirect.com/science/article/B6THR-4J2KT79-3/2/423a77ce44f17675604096e4fb9d8c49</a> </p><br />

    <p>28.   58159   DMID-LS-110; EMBASE-DMID-1/18/2006 ; DMID-LSLOAD</p>

    <p class="memofmt1-2">          Novel 1,3-dipropyl-8-(1-heteroarylmethyl-1H-pyrazol-4-yl)-xanthine derivatives as high affinity and selective A2B adenosine receptor antagonists</p>

    <p>          Elzein, Elfatih, Kalla, Rao, Li, Xiaofen, Perry, Thao, Parkhill, Eric, Palle, Venkata, Varkhedkar, Vaibahv, Gimbel, Art, Zeng, Dewan, and Lustig, David</p>

    <p>          Bioorganic &amp; Medicinal Chemistry Letters <b>2006</b>.  16(2): 302-306</p>

    <p>          
    <br />
    <!--HYPERLNK:--></p>

    <p>HYPERLINK:</p><p>          <a href="http://www.sciencedirect.com/science/article/B6TF9-4HGM75K-1/2/02622dc59a573557c7321afe646e8d63">http://www.sciencedirect.com/science/article/B6TF9-4HGM75K-1/2/02622dc59a573557c7321afe646e8d63</a> </p><br />
</div>
	</div>
</div>
    

	    <div class="niaid_clear"></div>
    	<p id="update">Database last updated: September 2017&nbsp;&nbsp;</p>	
        <div id="niaid_footer">
	        <a href="../SimpleSearch.aspx">Home</a> | 
	        <a href="https://www.niaid.nih.gov/global/privacy-policy" target="_blank">Privacy Policy</a> | 
	        <a href="https://www.niaid.nih.gov/global/website-disclaimer" target="_blank">Disclaimer</a> | 
	        <a href="https://www.niaid.nih.gov/global/web-accessibility" target="_blank">Accessibility</a> | 
            <a href="../SiteMap.aspx">Site Map</a> | 
        	<a href="../help/Help.aspx">Help</a> | 
        	<a href="../ContactUs.aspx">Contact Us</a>  
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_img" title="HHS Logo" target="_blank"><img src="../images/HHSfooterLogo.gif" alt="HHS Logo" /></a>
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_img" title="NIH Logo" target="_blank"><img src="../images/NIHfooterLogo.gif" alt="NIH Logo" /></a>	   
	            <a href="https://www.usa.gov/" id="niaid_footer_usa_img" title="USA.gov Logo" target="_blank"><img src="../images/USAgovFooterLogo.gif" alt="USA Gov Logo" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.hhs.gov/" id="niaid_footer_hhs_text" title="U.S. Department of Health and Human Services" target="_blank"><img src="../images/HHSfooterText.gif" alt="U.S. Department of Health and Human Services Text" /></a>
	        </div>
	        <div class="niaid_clear"></div>
	        <div class="niaid_footer_logos">
	            <a href="https://www.nih.gov/" id="niaid_footer_nih_text" title="National Institutes of Health" target="_blank"><img src="../images/NIHfooterText.gif" alt="National Institutes of Health Text" /></a>
	        </div>
        </div> <!-- niaid_footer -->
    </div> <!-- contentwrapper -->
</div> <!-- bodycontent -->
</form>
</body>
</html>
